<?
$MESS["VOTE_SUBMIT_BUTTON"] = "Votar";
$MESS["F_CAPTCHA_PROMT"] = "Caracteres de imagem CAPTCHA";
$MESS["VOTE_RESUBMIT_BUTTON"] = "Votar novamente";
$MESS["VOTE_RESULTS"] = "Total de votos:";
$MESS["VOTE_STOP_BUTTON"] = "Interromper";
$MESS["VOTE_RESUME_BUTTON"] = "Retornar";
$MESS["VOTE_ERROR_DEFAULT"] = "Erro desconhecido.";
$MESS["VOTE_RESULTS_BUTTON"] = "Resultados";
$MESS["VOTE_EXPORT_BUTTON"] = "Resultados (xls)";
?>