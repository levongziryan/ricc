<?
$MESS["SALE_QWH_ERROR_CODE_0"] = "Erfolgreich";
$MESS["SALE_QWH_ERROR_CODE_5"] = "Ungültige Daten in den Parametern der Anfrage";
$MESS["SALE_QWH_ERROR_CODE_13"] = "Server antwortet nicht, versuchen Sie bitte später erneut";
$MESS["SALE_QWH_ERROR_CODE_78"] = "Unzulässiger Vorgang";
$MESS["SALE_QWH_ERROR_CODE_150"] = "Autorisierungsfehler";
$MESS["SALE_QWH_ERROR_CODE_152"] = "Protokoll ist entweder nicht angeschlossen oder deaktiviert";
$MESS["SALE_QWH_ERROR_CODE_210"] = "Rechnung wurde nicht gefunden";
$MESS["SALE_QWH_ERROR_CODE_215"] = "Rechnung mit dieser bill_id existiert bereits";
$MESS["SALE_QWH_ERROR_CODE_241"] = "Betrag zu klein";
$MESS["SALE_QWH_ERROR_CODE_242"] = "Betrag zu hoch";
$MESS["SALE_QWH_ERROR_CODE_298"] = "Es existiert kein Wallet mit dieser ID";
$MESS["SALE_QWH_ERROR_CODE_300"] = "Technischer Fehler";
$MESS["SALE_QWH_ERROR_CODE_303"] = "Telefonnummer ist nicht korrekt";
$MESS["SALE_QWH_ERROR_CODE_316"] = "Autorisierungsversuch wurde durch Provider gesperrt";
$MESS["SALE_QWH_ERROR_CODE_319"] = "Sie haben nicht genügend Rechte für diesen Vorgang";
$MESS["SALE_QWH_ERROR_CODE_341"] = "Entweder wurde ein erforderlicher Parameter nicht korrekt angegeben oder er fehlt in der Anfrage";
$MESS["SALE_QWH_ERROR_CODE_1001"] = "Unzulässige Währung für Provider";
$MESS["SALE_QWH_ERROR_CODE_1003"] = "Kurs für Währungskonvertierung für diese zwei Währungen konnte nicht angegeben werden";
$MESS["SALE_QWH_ERROR_CODE_1019"] = "Mobilfunk-Provider für mobile Kommerz konnte nicht definiert werden";
$MESS["SALE_QWH_STATUS_MESSAGE_WAITING"] = "Rechnung ausgestellt, Bezahlung wird erwartet";
$MESS["SALE_QWH_STATUS_MESSAGE_PAID"] = "Rechnung bezahlt";
$MESS["SALE_QWH_STATUS_MESSAGE_REJECTED"] = "Rechnung abgelehnt";
$MESS["SALE_QWH_STATUS_MESSAGE_UNPAID"] = "Buchung der Bezahlung ist fehlgeschlagen. Die Rechnung wurde nicht bezahlt.";
$MESS["SALE_QWH_STATUS_MESSAGE_EXPIRED"] = "Die Rechnung ist nicht mehr gültig. Sie wurde nicht bezahlt.";
?>