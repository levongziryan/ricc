<?
$MESS["SALE_DLV_SRV_SPSR_RESP"] = "RESP";
$MESS["SALE_DLV_SRV_SPSR_REPUBLIC"] = "REPUBLIK";
$MESS["SALE_DLV_SRV_SPSR_AUT"] = "AUT";
$MESS["SALE_DLV_SRV_SPSR_AUTONOMOUS"] = "AUTONOM";
$MESS["SALE_DLV_SRV_SPSR_LOC_EX_CHUV_1"] = "Tschuwaschische Republik";
$MESS["SALE_DLV_SRV_SPSR_LOC_EX_CHUV_2"] = "Tschuwaschische Republik";
$MESS["SALE_DLV_SRV_SPSR_LOC_EX_KR_1"] = "Krim";
$MESS["SALE_DLV_SRV_SPSR_LOC_EX_KR_2"] = "Krim";
$MESS["SALE_DLV_SRV_SPSR_LOC_EX_HM_1"] = "Autonomer Kreis der Chanten und Mansen/Jugra";
$MESS["SALE_DLV_SRV_SPSR_LOC_EX_HM_2"] = "Autonomer Kreis der Chanten und Mansen/Jugra";
$MESS["SALE_DLV_SRV_SPSR_LOC_EX_EAO_1"] = "Jüdische Autonome Oblast";
$MESS["SALE_DLV_SRV_SPSR_LOC_EX_EAO_2"] = "Jüdische Autonome Oblast";
$MESS["SALE_DLV_SRV_SPSR_LOC_INST_ERROR"] = "Fehler der Übereinstimmung von SPSR-Standortcodes";
$MESS["SALE_DLV_SRV_SPSR_RUSSIA"] = " ";
$MESS["SALE_DLV_SRV_SPSR_TMP_TBL_ERROR"] = "Fehler beim Speichern der SPSR-Standorte in einer temporären Tabelle";
$MESS["SALE_DLV_SRV_SPSR_LOC_CSV"] = "Abgleich der Standorte mithilfe der Codes in einer CSV-Datei";
$MESS["SALE_DLV_SRV_SPSR_LOC_NORM"] = "Normalisierung der Standortnamen";
$MESS["SALE_DLV_SRV_SPSR_LOC_CHECK_MAPPED"] = "Gemachten Abgleich überprüfen";
$MESS["SALE_DLV_SRV_SPSR_LOC_MAP_BY_NAME"] = "Abgleich der Standorte nach Namen";
$MESS["SALE_DLV_SRV_SPSR_LOC_MAP_FINISHED"] = "Abgleich der Standorte abgeschlossen.";
$MESS["SALE_DLV_SRV_SPSR_LOC_MAP_STAGE_ERROR"] = "Unbekannte Aktion im Abgleich der Standorte";
?>