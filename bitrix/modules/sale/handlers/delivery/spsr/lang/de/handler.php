<?
$MESS["SALE_DLV_SRV_SPSR_TITLE"] = "SPSR EXPRESS";
$MESS["SALE_DLV_SRV_SPSR_DESCRIPTION"] = "#A1#SPSR EXPRESS#A2# bietet Dienstleistungen im Bereich Lieferung von Dokumenten innerhalb von Russland sowie im Ausland.";
$MESS["SALE_DLV_SRV_SPSR_MAIN_TITLE"] = "Einstellungen";
$MESS["SALE_DLV_SRV_SPSR_MAIN_DSCR"] = "Parameter des Lieferservices";
$MESS["SALE_DLV_SRV_SPSR_LOGIN"] = "Login";
$MESS["SALE_DLV_SRV_SPSR_PASS"] = "Passwort";
$MESS["SALE_DLV_SRV_SPSR_ICN"] = "Kunden-ID";
$MESS["SALE_DLV_SRV_SPSR_ERROR_HTTP_PUBLIC"] = "Fehler beim Erhalten von Daten";
$MESS["SALE_DLV_SRV_SPSR_ERROR_SESSION"] = "Fehler beim Erhalten der Sitzung";
$MESS["SALE_DLV_SRV_SPSR_ERROR_HTTP"] = "Fehler der HTTP-Anfrage";
$MESS["SALE_DLV_SRV_SPSR_ERROR_HTTP_STATUS"] = "Code des Status der HTTP-Antwort";
$MESS["SALE_DLV_SRV_SPSR_ERROR"] = "Fehler";
$MESS["SALE_DLV_SRV_SPSR_ERROR_CALCULATE"] = "Fehler bei Kalkulation des Lieferpreises";
$MESS["SALE_DLV_SRV_SPSR_ERROR_LOC_FROM_B"] = "Standort des Shops kann nicht erhalten werden";
$MESS["SALE_DLV_SRV_SPSR_ERROR_LOC_FROM_S"] = "Der SPSR-Code des Standortes für Shop kann nicht erhalten werden";
$MESS["SALE_DLV_SRV_SPSR_ERROR_LOC_TO_B"] = "Standort des Kunden kann nicht erhalten werden";
$MESS["SALE_DLV_SRV_SPSR_ERROR_LOC_TO_S"] = "Der SPSR-Code des Standortes für Kunden kann nicht erhalten werden";
$MESS["SALE_DLV_SRV_SPSR_ERROR_WEIGHT"] = "Gewicht der Lieferung ist Null oder weniger";
$MESS["SALE_DLV_SRV_SPSR_ERROR_REQUEST"] = "Anfrage kann nicht erstellt werden, um den Lieferpreis zu kalkulieren";
$MESS["SALE_DLV_SRV_SPSR_ERROR_SERVICE_TYPES"] = "Fehler bei Anfrage der Service-Typen";
$MESS["SALE_DLV_SRV_SPSR_DAYS"] = "Tage";
$MESS["SALE_DLV_SRV_SPSR_ERROR_TARIF_CALCULATE"] = "Service ist für diese Produkte nicht verfügbar";
$MESS["SALE_DLV_SRV_SPSR_SMS"] = "SMS-Benachrichtigung des Absenders";
$MESS["SALE_DLV_SRV_SPSR_SMS_DESCR"] = "Benachrichtigt den Absender per SMS über den Status der Lieferung";
$MESS["SALE_DLV_SRV_SPSR_SMS_RECV"] = "SMS-Benachrichtigung des Kunden";
$MESS["SALE_DLV_SRV_SPSR_SMS_RECV_DESCR"] = "Benachrichtigt den Kunden per SMS über den Status der Lieferung";
$MESS["SALE_DLV_SRV_SPSR_BEFORE_SIGNAL"] = "Datum und Uhrzeit der Lieferung abstimmen";
$MESS["SALE_DLV_SRV_SPSR_BEFORE_SIGNAL_DESCR"] = "Ermöglicht es einem Absender, bestimmtes Datum und bestimmte Uhrzeit der Lieferung an Kunden abzustimmen. Ein SPSR-Mitarbeiter wird dann den Kunden anrufen, um das Datum und die Zeit mit dem Kunden festzulegen, wann das Paket geliefert werden soll.";
$MESS["SALE_DLV_SRV_SPSR_BY_HAND"] = "Lieferung persönlich";
$MESS["SALE_DLV_SRV_SPSR_BY_HAND_DESCR"] = "Stellt sicher, dass die Lieferung nur an die Person ausgehändigt wird, die in Lieferdokumenten als Empfänger vermerkt ist. Geeignet für vertrauliche Informationen oder Dokumente.";
$MESS["SALE_DLV_SRV_SPSR_ICD"] = "Individuelle Lieferkontrolle";
$MESS["SALE_DLV_SRV_SPSR_ICD_DESCR"] = "Bietet eine persönliche Kontrolle über Lieferung durch einen SPSR-Mitarbeiter an. Ermöglicht eine hohe Priorität und möglichst kurze Lieferzeit.";
$MESS["SALE_DLV_SRV_SPSR_TO_BE_CALLED_FOR"] = "Postlagernd";
$MESS["SALE_DLV_SRV_SPSR_PLAT_TYPE"] = "Zahlung bei Lieferung";
$MESS["SALE_DLV_SRV_SPSR_PLAT_TYPE_DESCR"] = "Mit diesem Service kann die Lieferung direkt beim Empfang bezahlt werden. Bedarf eines unterzeichneten Vertrags zwischen Absender und SPSR.";
$MESS["SALE_DLV_SRV_SPSR_ERROR_SERVICES_LIST"] = "Fehler beim Erhalten der Liste der Services";
$MESS["SALE_DLV_SRV_SPSR_PELICAN_ONLINE"] = "Pelican online";
$MESS["SALE_DLV_SRV_SPSR_PELICAN_ONLINE_SDESCR"] = "Klassischer Lieferservice";
$MESS["SALE_DLV_SRV_SPSR_PELICAN_ONLINE_DESCR"] = "Schneller Lieferservice für Onlineshops (vorausbezahlt oder bezahlt beim Empfang). Meistens per Lufttransport.";
$MESS["SALE_DLV_SRV_SPSR_GEPARD_ONLINE"] = "Gepard online";
$MESS["SALE_DLV_SRV_SPSR_GEPARD_ONLINE_SDESCR"] = "Express-Lieferservice";
$MESS["SALE_DLV_SRV_SPSR_GEPARD_ONLINE_DESCR"] = "Extrem schneller Lieferservice für Onlineshops (vorausbezahlt oder bezahlt beim Empfang). Maximal schnelle verfügbare Transportwege werden genutzt.";
$MESS["SALE_DLV_SRV_SPSR_ZEBRA_ONLINE"] = "Zebra online";
$MESS["SALE_DLV_SRV_SPSR_ZEBRA_ONLINE_SDESCR"] = "Economy-Lieferung";
$MESS["SALE_DLV_SRV_SPSR_ZEBRA_ONLINE_DESCR"] = "Economy-Lieferung am Boden für Onlineshops (vorausbezahlt oder bezahlt beim Empfang). Diese Option ist für Shops verfügbar, welche über 500 Lieferung monatlich senden.";
$MESS["SALE_DLV_SRV_SPSR_CALCULATE_IMMEDIATELY"] = "Preis sofort kalkulieren";
$MESS["SALE_DLV_SRV_SPSR_DEFAULT_WEIGHT"] = "Standardgewicht der Auslieferung (Gramm)";
$MESS["SALE_DLV_SRV_SPSR_NATURE"] = "Frachtguttyp";
$MESS["SALE_DLV_SRV_SPSR_NATURE_1"] = "Dokumente";
$MESS["SALE_DLV_SRV_SPSR_NATURE_2"] = "Konsumgüter (Ausrüstung)";
$MESS["SALE_DLV_SRV_SPSR_NATURE_17"] = "Ausrüstung und Unterhaltungselektronik ohne Brennstoff bzw. Batterien";
$MESS["SALE_DLV_SRV_SPSR_NATURE_18"] = "Schmuck";
$MESS["SALE_DLV_SRV_SPSR_NATURE_19"] = "Medikamente und Nahrungsergänzungsmittel";
$MESS["SALE_DLV_SRV_SPSR_NATURE_20"] = "Kosmetik und Parfüm";
$MESS["SALE_DLV_SRV_SPSR_NATURE_21"] = "Lebensmittel";
$MESS["SALE_DLV_SRV_SPSR_NATURE_22"] = "Ausrüstung und Unterhaltungselektronik ohne Brennstoff mit Batterien";
$MESS["SALE_DLV_SRV_SPSR_NATURE_23"] = "Gefahrgüter";
$MESS["SALE_DLV_SRV_SPSR_NATURE_24"] = "Konsumgüter (Charge, ohne Ausrüstung)";
$MESS["SALE_DLV_SRV_SPSR_AMOUNT_CHECK"] = "Versicherung oder Wert";
$MESS["SALE_DLV_SRV_SPSR_AMOUNT_CHECK_1"] = "Versicherung";
$MESS["SALE_DLV_SRV_SPSR_AMOUNT_CHECK_0"] = "Erklärter Wert";
$MESS["SALE_DLV_SRV_SPSR_AMOUNT_CHECK__1"] = "Ohne Versicherung";
$MESS["SALE_DLV_SRV_SPSR_BV_AUTH"] = "Authentifizierung des Lieferservices SPSR (für Holding)";
?>