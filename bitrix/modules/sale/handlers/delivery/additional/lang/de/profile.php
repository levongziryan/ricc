<?
$MESS["SALE_DLVRS_ADDP_NAME"] = "Profil für zusätzliche Lieferservices";
$MESS["SALE_DLVRS_ADDP_DESCRIPTION"] = "Gibt das Profil für zusätzliche Lieferservices an";
$MESS["SALE_DLVRS_ADDP_MAIN_TITLE"] = "Einstellungen";
$MESS["SALE_DLVRS_ADDP_MAIN_DESCRIPTION"] = "Einstellungen des Profils für zusätzliche Lieferservices";
$MESS["SALE_DLVRS_ADDP_MAIN_TYPE"] = "Servicetyp";
$MESS["SALE_DLVRS_ADDP_MAIN_DESC"] = "Servicebeschreibung";
?>