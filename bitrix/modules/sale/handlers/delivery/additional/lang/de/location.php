<?
$MESS["SALE_DLVRS_ADDL_LOCATIONS_ERROR"] = "Datei mit Standorten kann nicht erhalten werden";
$MESS["SALE_DLVRS_ADDL_LOCATIONS_ERROR_TMP"] = "Muster-Standorte könnten in einer temporären Tabelle nicht gespeichert werden";
$MESS["SALE_DLVRS_ADDL_LOCATIONS_ERROR_SID"] = "ID des Lieferservices konnte nicht erhalten werden";
$MESS["SALE_DLVRS_ADDL_LOCATIONS_ERROR_PATH"] = "Pfad zur Datei mit Standorten konnte nicht erhalten werden";
$MESS["SALE_DLVRS_ADDL_LOCATIONS_ERROR_TMP_TABLE"] = "Temporäre Tabelle konnte nicht erstellt werden";
$MESS["SALE_DLVRS_ADDL_LOCATIONS_CREATE_TMP_TABLE"] = "Temporäre Tabelle erstellen";
$MESS["SALE_DLVRS_ADDL_LOCATIONS_CHECK_COMPARED"] = "Abgeglichene Standorte überprüfen";
$MESS["SALE_DLVRS_ADDL_LOCATIONS_COMP_BY_CODES"] = "Standorte nach Codes abgleichen";
$MESS["SALE_DLVRS_ADDL_LOCATIONS_NORM"] = "Standortnamen normalisieren";
$MESS["SALE_DLVRS_ADDL_LOCATIONS_COMP_BY_NAMES"] = "Standorte nach Namen abgleichen";
$MESS["SALE_DLVRS_ADDL_LOCATIONS_COMP_COMPLETE"] = "Standorte wurde abgeglichen.";
$MESS["SALE_DLVRS_ADDL_LOCATIONS_ERROR_STAGE"] = "Unbekannter Schritt";
?>