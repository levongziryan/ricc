<?
$MESS["SALE_HPS_PAYPAL_T1"] = "Danke für Ihren Einkauf.";
$MESS["SALE_HPS_PAYPAL_T2"] = "Zahlungsdaten";
$MESS["SALE_HPS_PAYPAL_T3"] = "Name";
$MESS["SALE_HPS_PAYPAL_T4"] = "Produkt";
$MESS["SALE_HPS_PAYPAL_T5"] = "Betrag";
$MESS["SALE_HPS_PAYPAL_I1"] = "Zahlung fehlgeschlagen.";
$MESS["SALE_HPS_PAYPAL_I3"] = "Ihre Transaktion wurde abgeschlossen, eine Zahlungsbestätigung wurde an Ihre E-Mail-Adresse gesendet.<br>Sie können sich in Ihrem Account auf <a href=\"https://www.paypal.com\">www.paypal.com</a> einloggen, um Informationen zu dieser Transaktion anzuzeigen.";
$MESS["SALE_HPS_PAYPAL_I4"] = "Sie können den Bestellstatus im persönlichen Bereich auf <a href=\"/personal/\">unserer Website</a> anzeigen.";
