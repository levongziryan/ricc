<?
$MESS["SALE_HPS_PAYMASTER"] = "Bezahlung via PayMaster (Russisches Zahlungssystem)";
$MESS["SALE_HPS_PAYMASTER_NUMBER"] = "ID Ihrer Website";
$MESS["SALE_HPS_PAYMASTER_TEST"] = "Testmodus";
$MESS["SALE_HPS_PAYMASTER_KEY"] = "Geheimschlüssel";
$MESS["SALE_HPS_PAYMASTER_ORDER_ID"] = "Zahlung #";
$MESS["SALE_HPS_PAYMASTER_DATE"] = "Zahlungsdatum";
$MESS["SALE_HPS_PAYMASTER_HASH_ALGO"] = "Algorithmus der Verschlüsselung";
$MESS["SALE_HPS_PAYMASTER_SUMMA"] = "Gesamtbetrag";
$MESS["SALE_HPS_PAYMASTER_CURRENCY"] = "Währung der Rechnung";
$MESS["SALE_HPS_PAYMASTER_URL"] = "URL der Benachrichtigung";
$MESS["SALE_HPS_PAYMASTER_URL_OK"] = "Handler-URL, falls erfolgreich";
$MESS["SALE_HPS_PAYMASTER_URL_ERROR"] = "Handler-URL, falls fehlgeschlagen";
$MESS["SALE_HPS_PAYMASTER_PHONE"] = "Telefonnummer des Kunden";
$MESS["SALE_HPS_PAYMASTER_MAIL"] = "E-Mail-Adresse des Kunden";
?>