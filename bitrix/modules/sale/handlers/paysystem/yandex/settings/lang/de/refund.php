<?
$MESS["SALE_YANDEX_RETURN_TITLE"] = "Rückgabemechanismus in Yandex.Checkout konfigurieren";
$MESS["SALE_YANDEX_RETURN_SUBTITLE"] = "MWS konfigurieren (<a target=\"_blank\" href=\"https://tech.yandex.ru/money/doc/payment-solution/payment-management/payment-management-about-docpage/\">Merchant Web Services</a>)";
$MESS["SALE_YANDEX_RETURN_HELP"] = "Bevor Sie MWS nutzen, müssen Sie ein Yandex.Money Zertifikat anfordern und es auf diese Seite hochladen.";
$MESS["SALE_YANDEX_RETURN_IP_DESC"] = "Anfragen wegen Rückgabe werden von dieser IP-Adresse gesendet.";
$MESS["SALE_YANDEX_RETURN_PT"] = "Rückgaben";
$MESS["SALE_YANDEX_RETURN_ERROR_SHOP_ID"] = "Bevor Sie Rückgaben konfigurieren, müssen Sie den Handler der Zahlungssysteme für folgende Kundengruppe konfigurieren:<br> <ul><li>Shop-Identifikator im Sammelsystem der Zahlungen (ShopID)</li><li>Unternehmensname</li></ul>";
$MESS["SALE_YANDEX_RETURN_CERT"] = "SSL-Zertifikat";
$MESS["SALE_YANDEX_RETURN_TEXT_SUCCESS"] = "Zertifikat wurde hochgeladen.";
$MESS["SALE_YANDEX_RETURN_TEXT_CLEAR"] = "Zertifikat entfernen";
$MESS["SALE_YANDEX_RETURN_TEXT_CLEAR_ALL"] = "MWS-Daten aktualisieren";
$MESS["SALE_YANDEX_RETURN_HOW"] = "Wie kann ich ein Zertifikat anfordern?";
$MESS["SALE_YANDEX_RETURN_HOW_ITEM1"] = "<a href='%s'>Eine komplette Zertifikatanfrage</a> herunterladen (eine .csr Datei).";
$MESS["SALE_YANDEX_RETURN_HOW_ITEM2"] = "<a target=\"_blank\" href=\"https://money.yandex.ru/i/html-letters/SSL_Cert_Form.doc\">Ein Anfrageformular für Zertifikat</a> herunterladen.";
$MESS["SALE_YANDEX_RETURN_HOW_ITEM3"] = "Füllen Sie das Formular aus, indem Sie Daten von dieser Seite nutzen, fügen Sie eine Signatur und einen Firmenstempel hinzu.";
$MESS["SALE_YANDEX_RETURN_HOW_ITEM4"] = "Senden Sie eine Nachricht an Mitarbeiter von Yandex.Checkout an <a href=\"mailto:merchants@yamoney.ru\">merchants@yamoney.ru</a> um sie darüber zu informieren, dass Sie ein MWS-Rückgabezertifikat anfordern. Hängen Sie folgende Dateien an die Nachricht an:<ul>
                                    <li>die Datei der Zertifikatanfrage ,</li>
                                    <li>das Anfrageformular (eingescannt),</li>
                                    <li>die IP-Adresse, die zum Senden von Rückgabeanfragen genutzt wird.</li>
                                </ul>
";
$MESS["SALE_YANDEX_RETURN_HOW_ITEM5"] = "Warten Sie bitte ab, bis das Zertifikat an Sie gesendet wird, dann können Sie es auf diese Seite hochladen.";
$MESS["SALE_YANDEX_RETURN_STATEMENT"] = "Daten für das Anfrageformular";
$MESS["SALE_YANDEX_RETURN_STATEMENT_CN"] = "CN";
$MESS["SALE_YANDEX_RETURN_STATEMENT_SIGN"] = "Elektronische Signatur für das Zertifikat";
$MESS["SALE_YANDEX_RETURN_STATEMENT_CAUSE"] = "Grund der Anfrage";
$MESS["SALE_YANDEX_RETURN_STATEMENT_CAUSE_VAL"] = "Ursprünglich";
$MESS["SPSN_2FLIST"] = "Zurück zum Zahlungssystem";
$MESS["SALE_YANDEX_RETURN_SAVE"] = "Speichern";
$MESS["SALE_YANDEX_RETURN_GENERATE"] = "Generieren";
?>