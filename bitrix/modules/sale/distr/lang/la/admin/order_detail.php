<?
$MESS["SALE_EDIT_RECORD"] = "Detalles de la orden #ID#";
$MESS["SALE_RECORDS_LIST"] = "Para la lista de ordenes";
$MESS["P_ORDER_ID"] = "Orden";
$MESS["P_ORDER_DATE"] = "Fecha de la orden";
$MESS["P_ORDER_LANG"] = "Orden del sitioweb:";
$MESS["P_ORDER_STATUS"] = "Estado";
$MESS["P_ORDER_STATUS_DATE"] = "cambiado";
$MESS["P_ORDER_PRICE"] = "Precio de la orden";
$MESS["P_ORDER_UPDATE_DATE"] = "ultima actualizacion";
$MESS["P_ORDER_CANCELED"] = "Cancelado:";
$MESS["P_ORDER_DATE_CANCELED"] = "cancelado";
$MESS["P_ORDER_USER_ACC"] = "Cuenta de usuario";
$MESS["P_ORDER_PERS_TYPE"] = "Tipo de pagador";
$MESS["P_ORDER_ACCOUNT"] = "Usuario:";
$MESS["P_ORDER_USER_LOGIN"] = "Autenticación de usuario:";
$MESS["P_ORDER_USER_EMAIL"] = "E-mail del usuario:";
$MESS["P_ORDER_USER"] = "Preferencias del cliente";
$MESS["P_ORDER_PAYMENT"] = "Pago";
$MESS["P_ORDER_PAY_SYSTEM"] = "Sistema de pago";
$MESS["P_ORDER_PAYED"] = "Orden pagada:";
$MESS["P_ORDER_DATE_PAYED"] = "pagada";
$MESS["P_ORDER_DELIVERY"] = "Servicio de envios";
?>