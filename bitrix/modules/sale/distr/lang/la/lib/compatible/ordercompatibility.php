<?
$MESS["SALE_COMPATIBLE_ORDER_CANCEL_NO_PERMISSION"] = "Usted no tiene permiso para cancelar la orden.";
$MESS["SALE_COMPATIBLE_ORDER_MARKED_NO_PERMISSION"] = "Usted no tiene permiso para marcar una orden como problemática.";
$MESS["SALE_COMPATIBLE_ORDER_DEDUCT_NO_PERMISSION"] = "Usted no tiene permiso para enviar el pedido.";
$MESS["SALE_COMPATIBLE_ORDER_NOT_FOUND"] = "La orden no fue encontrado";
$MESS["SALE_COMPATIBLE_ORDER_ID_NOT_FOUND"] = "ID de la orden no se ha especificado.";
?>