<?
$MESS["SALE_EMAIL_ORDER"] = "Departamento de ventas e-mail:";
$MESS["SALE_EMAIL_REGISTER"] = "E-mail de registro predeterminado";
$MESS["SALE_ADDRESS_POST"] = "Valor 1";
$MESS["SALE_SBERBANK_REC"] = "Valor 2";
$MESS["SALE_BANK_REC"] = "Valor 3";
$MESS["SALE_SHOPID"] = "Valor 4";
$MESS["SALE_WBP_SHOPKEYID"] = "Valor 5";
$MESS["SALE_WBP_SHOPENCRYPTIONKEY"] = "Valor 6";
$MESS["SALE_SHOP_IDP"] = "Valor 7";
$MESS["SALE_ASS_LOGIN"] = "Valor 8";
$MESS["SALE_ASS_PASS"] = "Valor 9";
$MESS["SALE_DEF_CURR"] = "Moneda predeterminada:";
$MESS["SALE_LANG"] = "Sitio web";
$MESS["SALE_CURRENCY"] = "Moneda de la orden";
$MESS["SALE_NOT_SET"] = "<no establecida>";
$MESS["SALE_DELETE_AFTER"] = "Número de días para guardar elementos en el carrito:";
$MESS["CO_PAR_NAME"] = "Nombre del parámetro";
$MESS["CO_PAR_VAL"] = "Valor del parámetro";
$MESS["SALE_PATH2UPSF"] = "Ruta al procesador del sistema de pago personalizado con letras:";
$MESS["SMO_LOCK_CATALOG"] = "Vender elementos desde el catálogo comercial con un solo módulo";
$MESS["SMO_NO_VALID_PASSWORD"] = "Proporcione una contraseña que se utiliza para cifrar los números de tarjetas de crédito.";
$MESS["SMO_SALE_PATH2ORDER"] = "Ver formulario de pedido personalizado:";
$MESS["SMO_PAYMENT_FLAG"] = "Pago";
$MESS["SMO_CANCEL_FLAG"] = "Cancelar";
$MESS["SMO_DELIVERY_FLAG"] = "Entrega permitida";
$MESS["SMO_CRYPT_TITLE"] = "Cifrado de la información de la tarjeta de crédito";
$MESS["SMO_PATH2CRYPT_FILE"] = "Ruta completa al archivos de cifrado de clave:";
$MESS["SMO_CRYPT_ALGORITHM"] = "Utilice el algoritmo de cifrado:<br><small>Los datos brutos producidos por diferentes algoritmos es mutuamente incompatibles.<br>Nunca se debe cambiar el algoritmo si ya existe cualquier información de tarjeta de crédito cifrada.</small>";
$MESS["SMO_NEED_MCRYPT"] = "Se requiere el módulo PHP Mcrypt";
$MESS["SMO_ADDITIONAL_SITE_PARAMS"] = "Parámetros específicos del sitio web";
$MESS["SMO_GROUPS2SITE"] = "Grupos de usuarios autorizados a gestionar las ordenes";
$MESS["SMO_STATE_TITLE"] = "Permisos para cambiar el estado de la orden";
$MESS["SMO_STATUS1"] = "Estado";
$MESS["SMO_GROUP2STATUS"] = "Grupos autorizados a cambiar el estado";
$MESS["SALE_SITE_ALT"] = "Ver información del sitio";
$MESS["SALE_TAB_2"] = "Tarjetas de crédito";
$MESS["SALE_TAB_3"] = "Acceso y moneda de la orden";
$MESS["SALE_TAB_3_TITLE"] = "Control de acceso a la orden. Moneda de la orden";
$MESS["SMO_AFFILIATE_PLAN_TYPE"] = "Rango de afiliados";
$MESS["SMO_AFFILIATE_LIFE_TIME"] = "Número de días para mantener la asociación de cliente afiliado:";
$MESS["SMO_AFFILIATE_PLAN_TYPE_N"] = "ventas";
$MESS["SMO_AFFILIATE_PLAN_TYPE_S"] = "producto";
$MESS["SMOS_AFFILIATE_PARAM"] = "Parámetro de la URL que contiene el ID del afiliado:";
$MESS["SMO_SHOW_ORDER_SUM"] = "Mostrar monto total de la órden a todos los gerentes de ventas";
$MESS["SALE_TAB_WEIGHT"] = "Unidades Peso";
$MESS["SALE_TAB_WEIGHT_TITLE"] = "Unidades de peso para el uso en el catálogo comercial";
$MESS["SALE_TAB_ADDRESS"] = "Dirección de la tienda";
$MESS["SALE_TAB_ADDRESS_TITLE"] = "Dirección de la tienda";
$MESS["SMO_LOCATION_ZIP"] = "Código postal:";
$MESS["SMO_LOCATION_CITY"] = "Ubicación:";
$MESS["SMO_DIF_SETTINGS"] = "Utilice dirección individual para cada sitio web";
$MESS["SMO_SITE_LIST"] = "Editar dirección para el sitio web:";
$MESS["SMO_SITE_SET_TITLE"] = "Parámetro de dirección";
$MESS["SMO_SITE_VALUE_TITLE"] = "Valor del parámetro";
$MESS["SMO_MEASUREMENT_PATH"] = "Ruta al archivo: pesos y medidas";
$MESS["SMO_DELIVERY_HANDLERS_CUSTOM_PATH"] = "Carpeta con los scripts de los sistemas personalizados de entrega:";
$MESS["SMO_PAR_WEIGHT"] = "Parámetros de peso";
$MESS["SMO_PAR_DIF_SETTINGS"] = "Utilice unidades individuales para cada sitio web";
$MESS["SMO_PAR_SITE_LIST"] = "Edite unidades para el sitio web:";
$MESS["SMO_PAR_SITE_SETTINGS_TITLE"] = "Ajustes:";
$MESS["SMO_PAR_SITE_SET_TITLE"] = "Unidad de parámetro";
$MESS["SMO_PAR_SITE_VALUE_TITLE"] = "Valor del parámetro";
$MESS["SMO_PAR_WEIGHT_UNIT"] = "Símbolo de la unidad de peso:";
$MESS["SMO_PAR_WEIGHT_UNIT_GRAMM"] = "g";
$MESS["SMO_PAR_WEIGHT_KOEF"] = "Número de gramos en una unidad:";
$MESS["SMO_PAR_SITE_WEIGHT_UNIT_SALE"] = "Nombre de la unidad:";
$MESS["SALE_MAX_LOCK_TIME"] = "Tiempo máximo de bloqueo para la orden (min):";
$MESS["SALE_ORDER_LIST_DATE"] = "Número de los últimos días para mostrar las órdenes en el informe:";
$MESS["SALE_PAY_TO_STATUS"] = "Al recibir el pago, cambiar el estado a fin de:";
$MESS["SALE_ALLOW_DELIVERY_TO_STATUS"] = "A la entrega, cambiar el estado a fin de:";
$MESS["SMO_STATUS"] = "No cambiar";
$MESS["SALE_SERVICE_AREA"] = "Ajustes del servicio";
$MESS["SALE_STORES_AREA"] = "Inventario";
$MESS["SALE_AMOUNT_VAL"] = "Cantidad";
$MESS["SALE_AMOUNT_CURRENCY"] = "Moneda";
$MESS["SALE_AMOUNT_NAME"] = "Puntos adquiribles (para su uso por bitrix:sale.account.pay component)";
$MESS["SMO_USE_SECURE_COOKIES"] = "Utilice cookies seguras";
$MESS["SMO_ORDER_PAY_REMINDER"] = "Aviso de orden no pagada";
$MESS["SMO_ORDER_OPTIONS"] = "Opciones de tiendas Web";
$MESS["SMO_ORDER_PAY_REMINDER_USE"] = "Activar recordatorios";
$MESS["SMO_ORDER_PAY_REMINDER_AFTER"] = "Enviar primer recordatorio después de (días)";
$MESS["SMO_ORDER_PAY_REMINDER_FREQUENCY"] = "Intervalo entre recordatorios posteriores (días)";
$MESS["SMO_ORDER_PAY_REMINDER_PERIOD"] = "Detener recordatorios después (días)";
$MESS["SALE_GRAPH_WEIGHT"] = "Ancho del diagrama de estadísticas de la orden:";
$MESS["SALE_GRAPH_HEIGHT"] = "Alto del diagrama de estadísticas de la orden:";
$MESS["SALE_RECALC_PRODUCT_LIST"] = "Crear \"Otros clientes también compraron\" sugerencias para cada producto";
$MESS["SALE_RECALC_PRODUCT_LIST_PERIOD"] = "Volver a crear \"Otros clientes también compraron\" lista de cada uno (días):";
$MESS["SMO_SHOW_ORDER_PRODUCT_XML_ID"] = "Mostrar producto XML_ID en la vista del formulario de la orden";
$MESS["SMO_SHOW_PAYSYSTEM_ACTION_ID"] = "Mostrar el ID de los servicios del sistema de pago";
$MESS["SALE_OPT_COUNT_DISCOUNT_4_ALL_QUANTITY"] = "Mostrar descuento calculado para cada elemento del carrito";
$MESS["SALE_OPT_COUNT_DELIVERY_TAX"] = "Incluya gastos de envío en la base de impuestos";
$MESS["SALE_IS_SHOP"] = "Habilitar la capacidad de almacén para los sitios web (selecciona una o más):";
$MESS["SALE_OPT_QUANTITY_FACTORIAL"] = "Use fracciones en el editor de ordenes en el Panel de Control";
$MESS["SALE_NOTIFY_PRODUCT_USE"] = "Utilizar notificaciones volver al stock";
$MESS["SALE_NOTIFY_PRODUCT"] = "Mantener notificaciones (días):";
$MESS["SALE_VIEWED_CAPABILITY"] = "Utilice los datos del módulo Catálogo Comercial para mostrar productos vistos:";
$MESS["SALE_VIEWED_TIME"] = "Mantener información de productos vistos (días):";
$MESS["SALE_VIEWED_COUNT"] = "Máximo productos vistos recientemente:";
$MESS["SMO_PRODUCT_SUBSCRIBE"] = "Suscripciones de producto";
$MESS["SALE_ADMIN_NEW_PRODUCT"] = "Se pueden agregar nuevos elementos directamente en el editar del formulario de la orden:";
$MESS["SALE_PAYED_2_ALLOW_DELIVERY"] = "Aprobar la entrega a cambio del pago";
$MESS["SALE_ALLOW_DEDUCTION_ON_DELIVERY"] = "Aprobar el cumplimiento tras la aprobación de la entrega";
$MESS["SALE_ADMIN_USE_CARDS"] = "Utilice tarjetas de crédito";
$MESS["SALE_SHOW_BASKET_PROPS_IN_ORDER_LIST"] = "Mostrar las propiedades de producto en la lista de órdenes";
$MESS["SMO_PAR_SITE_PARAMETERS"] = "Parámetros de peso";
$MESS["SMO_PAR_SITE_ADRES"] = "Dirección de la tienda web";
$MESS["SALE_ACCOUNT_NUMBER_TEMPLATE"] = "Plantilla de número de la orden:";
$MESS["SALE_ACCOUNT_NUMBER_TEMPLATE_0"] = "No usado";
$MESS["SALE_ACCOUNT_NUMBER_TEMPLATE_1"] = "Iniciar la numeración de";
$MESS["SALE_ACCOUNT_NUMBER_TEMPLATE_2"] = "Uso del prefijo";
$MESS["SALE_ACCOUNT_NUMBER_TEMPLATE_3"] = "Número al azar";
$MESS["SALE_ACCOUNT_NUMBER_TEMPLATE_4"] = "IDs de usuario y de la orden";
$MESS["SALE_ACCOUNT_NUMBER_TEMPLATE_5"] = "Reinicie numeración periódicamente";
$MESS["SALE_ACCOUNT_NUMBER_TEMPLATE_EXAMPLE"] = "Ejemplo:";
$MESS["SALE_ACCOUNT_NUMBER_NUMBER"] = "Número inicial:";
$MESS["SALE_ACCOUNT_NUMBER_NUMBER_DESC"] = "1 a 7 caracteres. El nuevo valor debe ser mayor que la anterior.";
$MESS["SALE_ACCOUNT_NUMBER_NUMBER_WARNING"] = "El número de orden inicial \"#NUMBER#\" no es válido.";
$MESS["SALE_ACCOUNT_NUMBER_PREFIX"] = "Prefijo:";
$MESS["SALE_ACCOUNT_NUMBER_PREFIX_DESC"] = "1 a 7 caracteres (letras latinas, números, guiones, guiones bajos). Ejemplo: TEST1234";
$MESS["SALE_ACCOUNT_NUMBER_PREFIX_WARNING"] = "El prefijo de la orden \"#PREFIX#\" no es válido.";
$MESS["SALE_ACCOUNT_NUMBER_DATE"] = "Período:";
$MESS["SALE_ACCOUNT_NUMBER_DATE_1"] = "Dentro de días";
$MESS["SALE_ACCOUNT_NUMBER_DATE_2"] = "Dentro de meses";
$MESS["SALE_ACCOUNT_NUMBER_DATE_3"] = "Dentro de años";
$MESS["SALE_ACCOUNT_NUMBER_RANDOM"] = "Número de caracteres:";
$MESS["SALE_ACCOUNT_NUMBER_WARNING"] = "Prefijo:";
$MESS["SALE_DEDUCT_STORE"] = "Almacén de envío por defecto:";
$MESS["SALE_PS_SUCCESS_PATH"] = "Pagina de pagos realizados";
$MESS["SALE_PS_FAIL_PATH"] = "Pagina de pagos no realizados";
$MESS["SMO_ENCODE_FUSER_ID"] = "ID de usuario de e-store codificado";
$MESS["SMO_LOCATION_SHOP_CITY"] = "Ubicación de la tienda";
$MESS["SMO_LOCATION_SALES_ZONE"] = "rea de servicio";
$MESS["SMO_LOCATION_SALES_ZONE_SELECT"] = "Seleccione las áreas de servicio";
$MESS["SMO_LOCATION_ALL"] = "Todas";
$MESS["SMO_LOCATION_COUNTRIES"] = "Países";
$MESS["SMO_LOCATION_REGIONS"] = "Regiones";
$MESS["SMO_LOCATION_CITIES"] = "Ciudades";
$MESS["SMO_LOCATION_JS_GET_DATA_ERROR"] = "Error al obtener los datos";
$MESS["SMO_LOCATION_NO_COUNTRY"] = "Ningún país";
$MESS["SMO_LOCATION_NO_REGION"] = "Ninguna región";
$MESS["SALE_LOCATION_SELECTOR_APPEARANCE"] = "Diseño del  selector de ubicación";
$MESS["SALE_LOCATION_SELECTOR_APPEARANCE_SEARCH"] = "Campo de búsqeda";
$MESS["SALE_LOCATION_SELECTOR_APPEARANCE_STEPS"] = "Lista desplegable";
$MESS["SALE_P2P"] = "Preferencia \"Otros clientes tambien compraron";
$MESS["SALE_P2P_STATUS_LIST"] = "Agregar productos a la lista \"Otros Clientes Tambien Compraron\" cuando las ordenes entrar en estos estados:";
$MESS["SALE_P2P_STATUS_PERIOD"] = "Eliminar elementos obsoletos después de (días):";
$MESS["SALE_P2P_EXP_DATE"] = "Producto continúa siendo importante dentro de (días):";
$MESS["F_CANCELED"] = "Bandera \"Cancelado";
$MESS["F_DELIVERY"] = "Bandera \"Entregado";
$MESS["F_PAY"] = "Bandera \"Pagado";
$MESS["F_OUT"] = "Bandera \"Enviado";
$MESS["SALE_LOCATION_WIDGET_APPEARANCE"] = "Herramienta de tipo de ubicación";
$MESS["BX_SALE_SETTINGS_SECTION_DISCOUNT"] = "Ajustes de descuento";
$MESS["BX_SALE_SETTINGS_OPTION_USE_SALE_DISCOUNT_ONLY"] = "Utilice sólo reglas del carrito de compras:";
$MESS["BX_SALE_SETTINGS_OPTION_PERCENT_FROM_BASE_PRICE"] = "Utilice el precio completo del producto para calcular el porcentaje en base al descuento/marca";
$MESS["BX_SALE_SETTINGS_OPTION_PRODUCT_RESERVE_CLEAR_PERIOD"] = "Días para mantener la reserva:";
$MESS["BX_SALE_SETTINGS_OPTION_PRODUCT_RESERVE_CONDITION"] = "Elementos de la reserva:";
$MESS["BX_SALE_SETTINGS_SECTION_RESERVATION"] = "Preferencias de reserva del producto";
$MESS["BX_SALE_SETTINGS_SECTION_LOCATIONS"] = "Configuración de ubicación";
$MESS["BX_SALE_SETTINGS_OPTION_DISCOUNT_APPLY_MODE"] = "Comportamiento de las reglas del carrito si otros módulos también tienen descuentos";
$MESS["SALE_EXPIRATION_PROCESSING_EVENTS"] = "Legado de proceso de eventos:";
$MESS["SALE_SYSTEM_PROCEDURES"] = "Operaciones del servicio";
$MESS["SALE_SYSTEM_TAB_REINDEX"] = "ndice de datos";
$MESS["SALE_SYSTEM_TAB_REINDEX_TITLE"] = "Indexación de datos";
$MESS["SALE_SYSTEM_TAB_CONVERT"] = "Conversión de datos";
$MESS["SALE_SYSTEM_TAB_CONVERT_TITLE"] = "Conversión de datos";
$MESS["SALE_SYS_PROC_REINDEX_DISCOUNT"] = "ndice de la regla del carrito de compras";
$MESS["SALE_SYS_PROC_REINDEX_DISCOUNT_BTN"] = "Iniciar";
$MESS["SALE_SYS_PROC_REINDEX_DISCOUNT_ALERT"] = "Atención! Use esta operación sólo si hay problemas con la regla del carrito de compras, o cuando sea recomendado por el servicio de asistencia o por el sistema de actualización.";
$MESS["SALE_SYS_PROC_CONVERT_BASKET_DISCOUNT"] = "Convertir datos de los cupones del catalogo comercial desde las ordenes";
$MESS["SALE_SYS_PROC_CONVERT_BASKET_DISCOUNT_BTN"] = "Iniciar conversión";
$MESS["SALE_SYS_PROC_CONVERT_BASKET_DISCOUNT_ALERT"] = "Atención! Esta operación sólo es necesaria para los pedidos de legado convertidos desde el e-store.";
$MESS["SALE_OPTIONS_POPUP_WINDOW_CLOSE_BTN"] = "Cerrar ventana";
$MESS["SMO_FORMAT_QUANTITY_AUTO"] = "Auto";
$MESS["SMO_FORMAT_QUANTITY_TITLE"] = "Puntos decimales visibles";
?>