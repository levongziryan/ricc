<?
$MESS["SOC_EMPTY_ORDER_ID"] = "ID de la orden no existe al actualizar la orden.";
$MESS["SOC_EMPTY_USER_ID"] = "ID del usuario no existe al actualizar la orden.";
$MESS["SOC_EMPTY_TYPE"] = "Tipo de registro no existe al actualizar la orden.";
$MESS["SOC_BASKET_ADDED"] = "Agregar producto";
$MESS["SOC_BASKET_ADDED_INFO"] = "#QUANTITY# pcs. de #NAME# (##PRODUCT_ID#) ha sido añadido a su carrito de compras.";
$MESS["SOC_BASKET_REMOVED"] = "Eliminar producto";
$MESS["SOC_BASKET_REMOVED_INFO"] = "#NAME#\" (##PRODUCT_ID#) se ha eliminado.";
$MESS["SOC_BASKET_QUANTITY_CHANGED"] = "La cantidad de productos ha cambiado";
$MESS["SOC_BASKET_QUANTITY_CHANGED_INFO"] = "Cantidad de \"#NAME#\" (##PRODUCT_ID#) cambiado a #QUANTITY#.";
$MESS["SOC_BASKET_PRICE_CHANGED"] = "Cambio de precio";
$MESS["SOC_BASKET_PRICE_CHANGED_INFO"] = "El precio de \"#NAME#\" (##PRODUCT_ID#) cambiado a #AMOUNT#.";
$MESS["SOC_ORDER_CANCELED"] = "Orden cancelada";
$MESS["SOC_ORDER_CANCELED_Y"] = "La orden fue cancelada. Motivo: #REASON_CANCELED#";
$MESS["SOC_ORDER_CANCELED_N"] = "Orden sin cancelarse";
$MESS["SOC_ORDER_RESERVED"] = "Ordenes reservadas";
$MESS["SOC_ORDER_RESERVED_Y"] = "Orden fue reservada";
$MESS["SOC_ORDER_RESERVED_N"] = "Cancelar reserva de orden";
$MESS["SOC_ORDER_DEDUCTED"] = "Pedido enviado";
$MESS["SOC_ORDER_DEDUCTED_N"] = "Envío de la orden fue cancelada. Motivo: #REASON_UNDO_DEDUCTED#";
$MESS["SOC_ORDER_DEDUCTED_Y"] = "Orden realizada";
$MESS["SOC_ORDER_MARKED"] = "Problema de la orden";
$MESS["SOC_ORDER_MARKED_INFO"] = "Descripción del problema: #REASON_MARKED#";
$MESS["SOC_ORDER_NOT_MARKED"] = "Problema de la orden indicada fue revocada";
$MESS["SOC_ORDER_COMMENTED"] = "Comentarios de la orden";
$MESS["SOC_ORDER_COMMENTED_INFO"] = "#COMMENTS#";
$MESS["SOC_ORDER_STATUS_CHANGED"] = "Ha cambiado el estado de la orden";
$MESS["SOC_ORDER_STATUS_CHANGED_INFO"] = "Estado cambiado a: #STATUS_ID#";
$MESS["SOC_ORDER_DELIVERY_ALLOWED"] = "Envío aprobado";
$MESS["SOC_ORDER_DELIVERY_ALLOWED_Y"] = "Envío aprobado";
$MESS["SOC_ORDER_DELIVERY_ALLOWED_N"] = "Aprobación del envío fue revocado.";
$MESS["SOC_ORDER_DELIVERY_DOC_CHANGED"] = "Documentación de entrega ha cambiado";
$MESS["SOC_ORDER_DELIVERY_DOC_CHANGED_INFO"] = "Documentación de ref.: #DELIVERY_DOC_NUM#. Fecha: #DELIVERY_DOC_DATE#";
$MESS["SOC_ORDER_DELIVERY_SYSTEM_CHANGED"] = "Servicio de entrega ha cambiado";
$MESS["SOC_ORDER_DELIVERY_SYSTEM_CHANGED_INFO"] = "Cambiado a: #DELIVERY_ID#";
$MESS["SOC_ORDER_PAYMENT_SYSTEM_CHANGED"] = "Cambio en el sistema de pago";
$MESS["SOC_ORDER_PAYMENT_SYSTEM_CHANGED_INFO"] = "Cambiado a: #PAY_SYSTEM_ID#";
$MESS["SOC_ORDER_PERSON_TYPE_CHANGED"] = "Cambio de tipo de pagador";
$MESS["SOC_ORDER_PERSON_TYPE_CHANGED_INFO"] = "Cambio de tipo de pagador a: PERSON_TYPE_ID#";
$MESS["SOC_ORDER_PAYMENT_VOUCHER_CHANGED"] = "Cambio de factura";
$MESS["SOC_ORDER_PAYMENT_VOUCHER_CHANGED_INFO"] = "Factura de ref.: #PAY_VOUCHER_NUM#. Fecha: #PAY_VOUCHER_DATE#";
$MESS["SOC_ORDER_PAYED"] = "Orden de pago";
$MESS["SOC_ORDER_PAYED_Y"] = "Orden de pago";
$MESS["SOC_ORDER_PAYED_N"] = "Pago de la orden fue revocada";
$MESS["SOC_ORDER_TRACKING_NUMBER_CHANGED"] = "Cambio de número de seguimiento";
$MESS["SOC_ORDER_TRACKING_NUMBER_CHANGED_INFO"] = "#TRACKING_NUMBER#";
$MESS["SOC_ORDER_USER_DESCRIPTION_CHANGED"] = "Comentario de usuario cambiado";
$MESS["SOC_ORDER_USER_DESCRIPTION_CHANGED_INFO"] = "#USER_DESCRIPTION#";
$MESS["SOC_ORDER_PRICE_DELIVERY_CHANGED"] = "Costo de entrega cambiado";
$MESS["SOC_ORDER_PRICE_DELIVERY_CHANGED_INFO"] = "Costo de entrega: #AMOUNT#";
$MESS["SOC_ORDER_PRICE_CHANGED"] = "Total de la orden cambiado";
$MESS["SOC_ORDER_PRICE_CHANGED_INFO"] = "Total de la orden:  #AMOUNT#";
$MESS["SOC_ORDER_ADDED"] = "Orden creada";
$MESS["SOC_ORDER_DELIVERY_REQUEST_SENT_ERROR"] = "Error";
$MESS["SOC_ORDER_DELIVERY_REQUEST_SENT_SUCCESS"] = "Enviado correctamente";
$MESS["SOC_ORDER_DELIVERY_REQUEST_SENT_ADD_INFO"] = "Información Adicional";
$MESS["SOC_ORDER_DELIVERY_REQUEST_SENT"] = "Requerimiento de servicio de entrega";
$MESS["SOC_PAYMENT_PAID"] = "Orden de pago parcial";
$MESS["SOC_PAYMENT_PAID_Y"] = "Pago completado";
$MESS["SOC_PAYMENT_PAID_N"] = "Cancelar el pago parcial";
$MESS["SOC_PAYMENT_CREATE_INFO"] = "Sistema de Pago '#PAY_SYSTEM#'";
$MESS["SOC_PAYMENT_CREATE"] = "Crear pago";
$MESS["SOC_PAYMENT_DELETE"] = "Eliminar el pago";
$MESS["SOC_PAYMENT_DELETE_INFO"] = "Pago #PAY_SYSTEM# de #SUM# USD eliminar";
$MESS["SOC_PAYMENT_PAY_SYSTEM_CHANGE"] = "Editar sistema de pago";
$MESS["SOC_PAYMENT_PAY_SYSTEM_CHANGE_INFO"] = "Sistema de pago #PAY_SYSTEM_ID#";
$MESS["SOC_SHIPMENT_ALLOWED"] = "Estado de aprobación de entrega";
$MESS["SOC_SHIPMENT_ALLOWED_Y"] = "Entrega aprobada";
$MESS["SOC_SHIPMENT_ALLOWED_N"] = "Aprobación de entrega revocada";
$MESS["SOC_SHIPMENT_CREATE_INFO"] = "'#DELIVERY_SERVICE#'";
$MESS["SOC_SHIPMENT_CREATE"] = "Crear envío";
$MESS["SOC_SHIPMENT_DEDUCTED"] = "Envío";
$MESS["SOC_SHIPMENT_DEDUCTED_Y"] = "Enviado";
$MESS["SOC_SHIPMENT_DEDUCTED_N"] = "Envío cancelado";
$MESS["SOC_SHIPMENT_RESERVED"] = "Reservar envío";
$MESS["SOC_SHIPMENT_RESERVED_Y"] = "Envío reservadO";
$MESS["SOC_SHIPMENT_RESERVED_N"] = "Cancelar reserva de envío";
$MESS["SOC_SHIPMENT_PRICE_DELIVERY_CHANGED"] = "Cambio de costo de entrega";
$MESS["SOC_SHIPMENT_PRICE_DELIVERY_CHANGED_INFO"] = "Costo de entrega  #PRICE_DELIVERY#";
$MESS["SOC_SHIPMENT_STATUS_CHANGE"] = "Estado de la entrega";
$MESS["SOC_SHIPMENT_STATUS_CHANGE_INFO"] = "Servicio de entrega #STATUS_ID#";
$MESS["SOC_SHIPMENT_ITEM_BASKET_ADDED_INFO"] = "#QUANTITY# pcs. de #NAME# (##PRODUCT_ID#) ha sido agregado a su carrito de compras.";
$MESS["SOC_SHIPMENT_ITEM_BASKET_ADDED"] = "Producto nuevo";
$MESS["SOC_SHIPMENT_ITEM_BASKET_REMOVED_INFO"] = "#NAME# (##PRODUCT_ID#) se ha eliminado.";
$MESS["SOC_SHIPMENT_ITEM_BASKET_REMOVED"] = "Eliminar producto";
?>