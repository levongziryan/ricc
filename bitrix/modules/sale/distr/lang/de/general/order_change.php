<?
$MESS["SOC_EMPTY_ORDER_ID"] = "Bei der Aktualisierung der Bestellung wurde die Bestell-ID nicht angegeben.";
$MESS["SOC_EMPTY_USER_ID"] = "Bei der Aktualisierung der Bestellung  wurde die Nutzer-ID nicht angegeben.";
$MESS["SOC_EMPTY_TYPE"] = "Bei der Aktualisierung der Bestellung wurde der Eintragstyp nicht angegeben.";
$MESS["SOC_BASKET_ADDED"] = "Produkt hinzugefügt";
$MESS["SOC_BASKET_ADDED_INFO"] = "#QUANTITY# Stück von #NAME# (##PRODUCT_ID#) wurden zu Ihrem Warenkorb hinzugefügt.";
$MESS["SOC_BASKET_REMOVED"] = "Produkt entfernt";
$MESS["SOC_BASKET_REMOVED_INFO"] = "\"#NAME#\" (##PRODUCT_ID#) wurde entfernt.";
$MESS["SOC_BASKET_QUANTITY_CHANGED"] = "Produktmenge geändert";
$MESS["SOC_BASKET_QUANTITY_CHANGED_INFO"] = "Menge von \"#NAME#\" (##PRODUCT_ID#) wurde auf #QUANTITY# geändert.";
$MESS["SOC_BASKET_PRICE_CHANGED"] = "Preis geändert";
$MESS["SOC_BASKET_PRICE_CHANGED_INFO"] = "Der Preis von \"#NAME#\" (##PRODUCT_ID#) auf #AMOUNT# geändert.";
$MESS["SOC_ORDER_CANCELED"] = "Bestellung storniert";
$MESS["SOC_ORDER_CANCELED_Y"] = "Die Bestellung wurde storniert. Der Grund: #REASON_CANCELED#";
$MESS["SOC_ORDER_CANCELED_N"] = "Stornierung der Bestellung aufheben";
$MESS["SOC_ORDER_RESERVED"] = "Bestellung reserviert";
$MESS["SOC_ORDER_RESERVED_Y"] = "Die Bestellung wurde reserviert.";
$MESS["SOC_ORDER_RESERVED_N"] = "Reservierung der Bestellung wurde storniert";
$MESS["SOC_ORDER_DEDUCTED"] = "Bestellung ausgeliefert";
$MESS["SOC_ORDER_DEDUCTED_N"] = "Auslieferung der Bestellung wurde storniert. Der Grund: #REASON_UNDO_DEDUCTED#";
$MESS["SOC_ORDER_DEDUCTED_Y"] = "Bestellung ausgeführt";
$MESS["SOC_ORDER_MARKED"] = "Problem mit der Bestellung";
$MESS["SOC_ORDER_MARKED_INFO"] = "Problembeschreibung: #REASON_MARKED#";
$MESS["SOC_ORDER_NOT_MARKED"] = "Die Markierung über das Bestellproblem wurde entfernt.";
$MESS["SOC_ORDER_COMMENTED"] = "Kommentare zur Bestellung";
$MESS["SOC_ORDER_COMMENTED_INFO"] = "#COMMENTS#";
$MESS["SOC_ORDER_STATUS_CHANGED"] = "Bestellstatus geändert";
$MESS["SOC_ORDER_STATUS_CHANGED_INFO"] = "Status geändert auf: #STATUS_ID#";
$MESS["SOC_ORDER_DELIVERY_ALLOWED"] = "Lieferung genehmigt";
$MESS["SOC_ORDER_DELIVERY_ALLOWED_Y"] = "Lieferung genehmigt";
$MESS["SOC_ORDER_DELIVERY_ALLOWED_N"] = "Lieferungsgenehmigung wurde aufgehoben.";
$MESS["SOC_ORDER_DELIVERY_DOC_CHANGED"] = "Lieferschein geändert";
$MESS["SOC_ORDER_DELIVERY_DOC_CHANGED_INFO"] = "Lieferschein-Nummer: #DELIVERY_DOC_NUM#. Datum: #DELIVERY_DOC_DATE#";
$MESS["SOC_ORDER_DELIVERY_SYSTEM_CHANGED"] = "Versandservice geändert";
$MESS["SOC_ORDER_DELIVERY_SYSTEM_CHANGED_INFO"] = "Geändert auf: #DELIVERY_ID#";
$MESS["SOC_ORDER_PAYMENT_SYSTEM_CHANGED"] = "Änderung des Zahlungssystems";
$MESS["SOC_ORDER_PAYMENT_SYSTEM_CHANGED_INFO"] = "Geändert auf: #PAY_SYSTEM_ID#";
$MESS["SOC_ORDER_PERSON_TYPE_CHANGED"] = "Kundengruppe geändert";
$MESS["SOC_ORDER_PERSON_TYPE_CHANGED_INFO"] = "Kundengruppe geändert auf: #PERSON_TYPE_ID#";
$MESS["SOC_ORDER_PAYMENT_VOUCHER_CHANGED"] = "Rechnung geändert";
$MESS["SOC_ORDER_PAYMENT_VOUCHER_CHANGED_INFO"] = "Rechnungsnummer: #PAY_VOUCHER_NUM#. Datum: #PAY_VOUCHER_DATE#";
$MESS["SOC_ORDER_PAYED"] = "Bezahlung der Bestellung";
$MESS["SOC_ORDER_PAYED_Y"] = "Bestellung bezahlt";
$MESS["SOC_ORDER_PAYED_N"] = "Die Bezahlung der Bestellung wurde storniert";
$MESS["SOC_ORDER_TRACKING_NUMBER_CHANGED"] = "Auftragsnummer geändert";
$MESS["SOC_ORDER_TRACKING_NUMBER_CHANGED_INFO"] = "#TRACKING_NUMBER#";
$MESS["SOC_ORDER_USER_DESCRIPTION_CHANGED"] = "Änderung des Nutzerkommentars";
$MESS["SOC_ORDER_USER_DESCRIPTION_CHANGED_INFO"] = "#USER_DESCRIPTION#";
$MESS["SOC_ORDER_PRICE_DELIVERY_CHANGED"] = "Lieferkosten geändert";
$MESS["SOC_ORDER_PRICE_DELIVERY_CHANGED_INFO"] = "Lieferkosten: #AMOUNT#";
$MESS["SOC_ORDER_PRICE_CHANGED"] = "Änderung des Gesamtbetrags der Bestellung";
$MESS["SOC_ORDER_PRICE_CHANGED_INFO"] = "Gesamtbetrag: #AMOUNT#";
$MESS["SOC_ORDER_1C_IMPORT"] = "Import von 1C";
$MESS["SOC_ORDER_ADDED"] = "Bestellung erstellt";
$MESS["SOC_ORDER_DELIVERY_REQUEST_SENT_ERROR"] = "Fehler";
$MESS["SOC_ORDER_DELIVERY_REQUEST_SENT_SUCCESS"] = "Erfolgreich gesendet";
$MESS["SOC_ORDER_DELIVERY_REQUEST_SENT_ADD_INFO"] = "Zusätzliche Informationen";
$MESS["SOC_ORDER_DELIVERY_REQUEST_SENT"] = "Lieferauftrag an Transportunternehmen";
$MESS["SOC_PAYMENT_PAID"] = "Teilbezahlung der Bestellung";
$MESS["SOC_PAYMENT_PAID_Y"] = "Bezahlung durchgeführt";
$MESS["SOC_PAYMENT_PAID_N"] = "Teilbezahlung storniert";
$MESS["SOC_PAYMENT_CREATE_INFO"] = "Zahlungssystem '#PAY_SYSTEM#'";
$MESS["SOC_PAYMENT_CREATE"] = "Bezahlung erstellen";
$MESS["SOC_PAYMENT_DELETE"] = "Bezahlung löschen";
$MESS["SOC_PAYMENT_DELETE_INFO"] = "Bezahlung #PAY_SYSTEM# des Betrags #SUM# EUR wurde gelöscht";
$MESS["SOC_PAYMENT_PAY_SYSTEM_CHANGE"] = "Zahlungssystem ändern";
$MESS["SOC_PAYMENT_PAY_SYSTEM_CHANGE_INFO"] = "Zahlungssystem #PAY_SYSTEM_ID#";
$MESS["SOC_SHIPMENT_ALLOWED"] = "Lieferung freigeben";
$MESS["SOC_SHIPMENT_ALLOWED_Y"] = "Lieferung freigegeben";
$MESS["SOC_SHIPMENT_ALLOWED_N"] = "Lieferfreigabe aufheben";
$MESS["SOC_SHIPMENT_CREATE_INFO"] = "'#DELIVERY_SERVICE#'";
$MESS["SOC_SHIPMENT_CREATE"] = "Auslieferung erstellen";
$MESS["SOC_SHIPMENT_DEDUCTED"] = "Auslieferung";
$MESS["SOC_SHIPMENT_DEDUCTED_Y"] = "Ausgeliefert";
$MESS["SOC_SHIPMENT_DEDUCTED_N"] = "Auslieferung stornieren";
$MESS["SOC_SHIPMENT_RESERVED"] = "Auslieferung reservieren";
$MESS["SOC_SHIPMENT_RESERVED_Y"] = "Auslieferung wurde reserviert";
$MESS["SOC_SHIPMENT_RESERVED_N"] = "Reservierung der Auslieferung stornieren";
$MESS["SOC_SHIPMENT_PRICE_DELIVERY_CHANGED"] = "Lieferkosten ändern";
$MESS["SOC_SHIPMENT_PRICE_DELIVERY_CHANGED_INFO"] = "Lieferkosten #PRICE_DELIVERY#";
$MESS["SOC_SHIPMENT_STATUS_CHANGE"] = "Lieferstatus";
$MESS["SOC_SHIPMENT_STATUS_CHANGE_INFO"] = "Lieferservice #STATUS_ID#";
$MESS["SOC_SHIPMENT_ITEM_BASKET_ADDED_INFO"] = "Hinzugefügt wurde das Produkt \"#NAME#\" (##PRODUCT_ID#), Menge #QUANTITY#.";
$MESS["SOC_SHIPMENT_ITEM_BASKET_ADDED"] = "Produkt hinzufügen";
$MESS["SOC_SHIPMENT_ITEM_BASKET_REMOVED_INFO"] = "Gelöscht wurde das Produkt \"#NAME#\" (##PRODUCT_ID#).";
$MESS["SOC_SHIPMENT_ITEM_BASKET_REMOVED"] = "Produkt löschen";
?>