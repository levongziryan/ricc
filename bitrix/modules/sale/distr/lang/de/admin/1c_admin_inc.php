<?
$MESS["SALE_1C_NO"] = "<Nicht ausgewählt>";
$MESS["SALE_1C_ALL_SITES"] = "Alle Seiten";
$MESS["SALE_1C_EXPORT_FINAL_ORDERS"] = "Bestellungen ab dem Status exportieren";
$MESS["SALE_1C_GROUP_PERMISSIONS"] = "Gruppen mit Export-Berechtigung";
$MESS["SALE_1C_EXPORT_PAYED_ORDERS"] = "Nur bezahlte Bestellungen exportieren";
$MESS["SALE_1C_EXPORT_ALLOW_DELIVERY_ORDERS"] = "Nur für den Versand freigeschaltete Bestellungen exportieren";
$MESS["SALE_1C_RUB"] = "Rub.";
$MESS["SALE_1C_USE_ZIP"] = "Wenn möglich, Zip-Komprimierung benutzen";
$MESS["SALE_1C_SITE_LIST"] = "Bestellungen von dieser Website in 1C exportieren ";
$MESS["SALE_1C_FINAL_STATUS_ON_DELIVERY"] = "Der Bestellungsstatus  nach dem Import aus 1? ";
$MESS["SALE_1C_REPLACE_CURRENCY"] = "Die Währung beim Import aus 1C wechseln zu";
$MESS["SALE_1C_SALE_ACCOUNT_NUMBER_SHOP_PREFIX"] = "Bestellnummer-Präfix (für Export)";
$MESS["SALE_1C_INTERVAL"] = "Dauer des Importschrittes in Sek. (0  alles auf einmal importieren)";
$MESS["SALE_1C_FILE_SIZE_LIMIT"] = "Maximale Größe des zu importierenden Dateiteils (Bytes)";
$MESS["SALE_1C_SITE_NEW_ORDERS"] = "Website, wo neue Bestellungen und Auftragnehmer hinzugefügt werden";
$MESS["SALE_1C_IMPORT_NEW_ORDERS"] = "Neue Bestellungen und Auftragnehmer aus 1C erstellen";
$MESS["SALE_1C_CHANGE_STATUS_FROM_1C"] = "Benutzen Sie 1C-Daten, um den Bestellstatus zu ändern ";
$MESS["SALE_1C_IMPORT_DEFAULT_PS"] = "Zahlungssystem für neue Bestellungen";
?>