<?
$MESS["SALE_OK_TITLE_SITE"] = "Bestellung hinzufügen. Website: ##SITE##";
$MESS["SALE_OK_TITLE_NO_SITE"] = "Bestellung hinzufügen";
$MESS["SALE_OK_ACCESS_DENIED"] = "Zugriff verweigert";
$MESS["SALE_OK_LIST"] = "Liste der Bestellungen";
$MESS["SALE_OK_LIST_TITLE"] = "Zur Liste der Bestellungen";
$MESS["SALE_OK_STATUS"] = "Bestellstatus";
$MESS["SALE_OK_TAB_ORDER"] = "Bestellung";
$MESS["SALE_OK_BLOCK_TITLE_STATUSORDER"] = "Bestellstatus";
$MESS["SALE_OK_BLOCK_TITLE_BUYER"] = "Kunde";
$MESS["SALE_OK_BLOCK_TITLE_BIGDATA"] = "Informationen über Kunden [BIG DATA]";
$MESS["SALE_OK_BLOCK_TITLE_DELIVERY"] = "Ausführung";
$MESS["SALE_OK_BLOCK_TITLE_PAYMENT"] = "Bezahlung";
$MESS["SALE_OK_BLOCK_TITLE_RELPROPS"] = "Eigenschaften bezüglich Bezahlung und Lieferung";
$MESS["SALE_OK_BLOCK_TITLE_BASKET"] = "Bestellung";
$MESS["SALE_OK_BLOCK_TITLE_ADDITIONAL"] = "Zusätzliche Informationen";
$MESS["SALE_OK_BLOCK_TITLE_FINANCEINFO"] = "Finanzinformationen über Bestellung";
?>