<?
$MESS["BX_SALE_DISCOUNT_APPLY_MODE_ADD"] = "Angewendete Rabatte aktualisieren";
$MESS["BX_SALE_DISCOUNT_APPLY_MODE_LAST"] = "nicht anwenden, wenn die Option \"Weitere Rabatte nicht anwenden\" aktiviert ist";
$MESS["BX_SALE_DISCOUNT_APPLY_MODE_DISABLE"] = "nicht anwenden, wenn andere Rabatte angewendet wurden";
$MESS["BX_SALE_DISCOUNT_ERR_BASKET_EMPTY"] = "Es gibt keine Elemente im Warenkorb";
$MESS["BX_SALE_DISCOUNT_ERR_BASKET_BUNDLE_EMPTY"] = "Eins oder mehrere Elemente des Pakets fehlen";
$MESS["BX_SALE_DISCOUNT_ERR_TOO_MANY_SHIPMENT"] = "Die Bestellung hat mehrere Auslieferungen";
$MESS["BX_SALE_DISCOUNT_ERR_SHIPMENT_IS_ABSENT"] = "Auslieferung wurde nicht gefunden";
$MESS["BX_SALE_DISCOUNT_ERR_NEW_ORDER_IS_NULL"] = "Daten können nicht gespeichert werden: es wurde keine Kalkulation durchgeführt";
$MESS["BX_SALE_DISCOUNT_ERR_SAVE_APPLY_RULES"] = "Fehler beim Speichern der Rabattergebnisse und Warenkorbregeln";
$MESS["BX_SALE_DISCOUNT_ERR_COUPON_NOT_FOUND"] = "Gutschein wurde nicht gefunden";
$MESS["BX_SALE_DISCOUNT_ERR_DISCOUNT_WITHOUT_COUPON"] = "Gutschein fehlt";
$MESS["BX_SALE_DISCOUNT_ERR_ORDER_ID_IS_EMPTY"] = "Bestell-ID fehlt";
$MESS["BX_SALE_DISCOUNT_ERR_APPLY_WITHOUT_EXT_DISCOUNT"] = "Der angewendete Rabatt wurde nicht gefunden. Die komplette Bestellung muss neu kalkuliert werden.";
$MESS["BX_SALE_DISCOUNT_ERR_APPLY_WITHOUT_SALE_DISCOUNT"] = "Die angewendete Regel wurde nicht gefunden. Die komplette Bestellung muss neu kalkuliert werden.";
$MESS["BX_SALE_DISCOUNT_ERR_EMPTY_RULE_ID_EXT_DISCOUNT"] = "Die ID der Rabattanwendung fehlt.";
$MESS["BX_SALE_DISCOUNT_ERR_EMPTY_RULE_ID_SALE_DISCOUNT"] = "Die ID der Warenkorbregel fehlt.";
$MESS["BX_SALE_DISCOUNT_ERR_BAD_USE_MODE"] = "Unbekanntes Verfahren zur Rabattkalkulation.";
$MESS["BX_SALE_DISCOUNT_ERR_SALE_DISCOUNT_MODULES_ABSENT"] = "Die in der Bestellung enthaltene Regel kann nicht neu kalkuliert werden: es fehlen erforderliche Module.";
?>