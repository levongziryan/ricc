<?
$MESS["SALE_PROVIDER_RESERVE_SHIPMENT_ITEM_QUANTITY_NOT_ENOUGH"] = "Produktmenge ist nicht ausreichend";
$MESS["SALE_PROVIDER_RESERVE_BASKET_ITEM_ERROR"] = "Fehler beim Reservieren der Positionen im Warenkorb";
$MESS["SALE_PROVIDER_RESERVE_BASKET_ITEM_WRONG_QUANTITY"] = "Reservierte Produktmenge ist nicht korrekt";
$MESS["SALE_PROVIDER_RESERVE_BASKET_ITEM_QUANTITY_NOT_ENOUGH"] = "Menge von reservierten Produkten ist größer als Menge im Warenkorb";
$MESS["SALE_PROVIDER_RESERVE_BASKET_ITEM_QUANTITY_WRONG_RESIDUE"] = "Der Restbestand von reservierten Produkten ist nicht korrekt";
$MESS["SALE_PROVIDER_STORE_DATA_BARCODE_NOT_FOUND"] = "Strichcode für Produkt \"#PRODUCT_NAME#\" wurde nicht gefunden";
$MESS["SALE_PROVIDER_SHIPMENT_QUANTITY_NOT_ENOUGH"] = "Menge vom reservierten Produkt \"#PRODUCT_NAME#\" im Warenkorb ist größer als die Menge im Katalog";
$MESS["SALE_PROVIDER_SHIPMENT_SHIPPED_LESS_QUANTITY"] = "Menge des auszuliefernden Produktes \"#PRODUCT_NAME#\" ist weniger als die Gesamtmenge des Produktes zur Auslieferung";
$MESS["SALE_PROVIDER_SHIPMENT_SHIPPED_MORE_QUANTITY"] = "Menge des auszuliefernden Produktes \"#PRODUCT_NAME#\" ist größer als die Gesamtmenge des Produktes zur Auslieferung";
?>