<?
$MESS["SALE_ORDER_CANCEL_SHIPMENT_EXIST_SHIPPED"] = "Die Bestellung hat ausgelieferte Produkte";
$MESS["SALE_ORDER_SYSTEM_SHIPMENT_LESS_QUANTITY"] = "Sie können die Menge des Produktes \"#PRODUCT_NAME#\" im Warenkorb nicht reduzieren, solange dieses Produkt nach Auslieferungen verteilt ist. (#QUANTITY#)";
?>