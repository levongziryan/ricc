<?
$MESS["SALE_DLVR_RSTR_BY_PAYSYSTEM_NAME"] = "nach Zahlungssystemen";
$MESS["SALE_DLVR_RSTR_BY_PAYSYSTEM_DESCRIPT"] = "Verwendung des Lieferservices nach Zahlungssystemen einschränken. Lieferservice wird nur mit ausgewählten Zahlungssystemen verfügbar sein. Wurde kein einziges Zahlungssystem ausgewählt, wird der Lieferservice mit allen Zahlungssystemen verfügbar sein.";
$MESS["SALE_DLVR_RSTR_BY_PAYSYSTEM_PRM_PS"] = "Zahlungssysteme";
?>