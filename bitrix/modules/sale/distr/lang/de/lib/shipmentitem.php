<?
$MESS["SALE_SHIPMENT_ITEM_BARCODE_MORE_ITEM_QUANTITY"] = "Es gibt mehr Strichcodes als Produkte";
$MESS["SALE_SHIPMENT_ITEM_SHIPMENT_ALREADY_SHIPPED_CANNOT_EDIT"] = "Teilauslieferung erfolgte bereits. Änderungen sind nicht möglich.";
$MESS["SALE_SHIPMENT_ITEM_SHIPMENT_ALREADY_SHIPPED_CANNOT_DELETE"] = "Teilauslieferung erfolgte bereits. Das Produkt \"#PRODUCT_NAME#\" kann nicht gelöscht werden.";
$MESS["SALE_SHIPMENT_ITEM_LESS_AVAILABLE_QUANTITY"] = "Die Menge des nicht verteilten Produktes \"#PRODUCT_NAME#\" im Warenkorb ist nicht ausreichend.";
$MESS["SALE_EVENT_ON_BEFORE_SALESHIPMENTITEM_SET_FIELD_ERROR"] = "Fehler des Events zum Definieren des Wertes für Feld der Lieferung";
?>