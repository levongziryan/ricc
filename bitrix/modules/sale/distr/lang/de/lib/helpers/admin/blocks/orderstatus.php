<?
$MESS["SALE_ORDER_STATUS_CREATED"] = "Erstellt am";
$MESS["SALE_ORDER_STATUS_LAST_MODIF"] = "Zuletzt geändert am";
$MESS["SALE_ORDER_STATUS"] = "Bestellstatus";
$MESS["SALE_ORDER_STATUS_SAVE"] = "Speichern";
$MESS["SALE_ORDER_STATUS_CANCEL"] = "Bestellung stornieren";
$MESS["SALE_ORDER_STATUS_CANCEL_CANCEL"] = "Stornierung der Bestellung aufheben";
$MESS["SALE_ORDER_STATUS_CANCEL_ERROR"] = "Fehler der Bestellstornierung";
$MESS["SALE_ORDER_STATUS_CHANGED_SUCCESS"] = "Bestellstatus wurde erfolgreich geändert";
$MESS["SALE_ORDER_STATUS_CHANGE_ERROR"] = "Fehler beim Ändern des Bestellstatus";
$MESS["SALE_ORDER_STATUS_CANCELED"] = "Bestellung storniert";
$MESS["SALE_ORDER_STATUS_CANCELING"] = "Bestellung stornieren";
$MESS["SALE_ORDER_STATUS_COMMENT"] = "Kommentar";
$MESS["SALE_ORDER_STATUS_CANCELING_REASON"] = "Grund der Stornierung";
$MESS["SALE_ORDER_STATUS_USER_CAN_VIEW"] = "Für Kunden zur Ansicht verfügbar";
$MESS["SALE_ORDER_STATUS_TOGGLE"] = "ausblenden";
$MESS["SALE_ORDER_STATUS_SITE"] = "Website";
?>