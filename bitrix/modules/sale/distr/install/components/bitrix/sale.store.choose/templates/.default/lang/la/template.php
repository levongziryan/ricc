<?
$MESS["SALE_SSC_PHONE"] = "Teléfono";
$MESS["SALE_SSC_ADRES"] = "Dirección";
$MESS["SALE_SSC_WORK"] = "Horas de trabajo";
$MESS["SALE_SSC_STORE"] = "Almacén";
$MESS["SALE_SSC_EMAIL"] = "Correo electrónico";
$MESS["SALE_SSC_DESC"] = "Descripción";
$MESS["SALE_SSC_CHANGE"] = "Editar";
$MESS["SALE_SSC_STORE_EXPORT"] = "Recoger desde la direccion";
$MESS["SALE_SSC_ADD_INFO"] = "Información del almacén adicional";
?>