<?
$MESS["SALE_SLS_SUPPRESS_ERRORS_PARAMETER"] = "Fehler ausblenden, wenn diese beim Laden der Komponente aufgetreten wurden.";
$MESS["SALE_SLS_INITIALIZE_BY_GLOBAL_EVENT_PARAMETER"] = "Die Komponente nur dann initialisieren, wenn das angegebene JavaScript-Event im Objekt window.document erscheint.";
$MESS["SALE_SLS_ID_PARAMETER"] = "Standort-ID";
$MESS["SALE_SLS_CODE_PARAMETER"] = "Symbolischer Code des Standortes";
$MESS["SALE_SLS_INPUT_NAME_PARAMETER"] = "Name des Eingabefeldes";
$MESS["SALE_SLS_JSCONTROL_GLOBAL_ID_PARAMETER"] = "Control-ID für JavaScript ";
$MESS["SALE_SLS_EXCLUDE_SUBTREE_PARAMETER"] = "Unterbaum des Elementes ausschließen";
$MESS["SALE_SLS_FILTER_BY_SITE_PARAMETER"] = "Nach Website filtern";
$MESS["SALE_SLS_FILTER_SITE_ID_PARAMETER"] = "Website";
$MESS["SALE_SLS_FILTER_SITE_ID_CURRENT"] = "aktuelle";
$MESS["SALE_SLS_JS_CALLBACK"] = "JavaScript-Funktion zum Rückaufruf";
$MESS["SALE_SLS_SHOW_DEFAULT_LOCATIONS_PARAMETER"] = "Standardmäßige Standorte anzeigen";
$MESS["SALE_SLS_SEARCH_BY_PRIMARY_PARAMETER"] = "Nach ID und Code suchen";
$MESS["SALE_SLS_PROVIDE_LINK_BY_PARAMETER"] = "Link behalten via";
$MESS["SALE_SLS_PROVIDE_LINK_BY_PARAMETER_ID"] = "ID";
$MESS["SALE_SLS_PROVIDE_LINK_BY_PARAMETER_CODE"] = "Symbolischen Code (code)";
?>