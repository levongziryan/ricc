<?
$MESS["SALE_BASKET_ENTITY_BASKET"] = "Кошик";
$MESS["SALE_BASKET_ENTITY_ID"] = "ID записи";
$MESS["SALE_BASKET_ENTITY_FUSER_ID"] = "ID відвідувача";
$MESS["SALE_BASKET_ENTITY_DATE_INSERT"] = "Дата додавання позиції";
$MESS["SALE_BASKET_ENTITY_DATE_UPDATE"] = "Дата зміни позиції";
$MESS["SALE_BASKET_ENTITY_DATE_INS"] = "Дата додавання позиції";
$MESS["SALE_BASKET_ENTITY_DATE_UPD"] = "Дата зміни позиції";
$MESS["SALE_BASKET_ENTITY_PRODUCT_ID"] = "ID товару";
$MESS["SALE_BASKET_ENTITY_NAME"] = "Найменування товару";
$MESS["SALE_BASKET_ENTITY_ORDER_ID"] = "ID замовлення";
$MESS["SALE_BASKET_ENTITY_PRICE"] = "Ціна товару";
$MESS["SALE_BASKET_ENTITY_QUANTITY"] = "Кількість товару";
$MESS["SALE_BASKET_ENTITY_SUMMARY_PRICE"] = "Сума товару";
$MESS["SALE_BASKET_ENTITY_SUBSCRIBE"] = "Є підписка на товар";
$MESS["SALE_BASKET_ENTITY_N_SUBSCRIBE"] = "Кількість підписок";
?>