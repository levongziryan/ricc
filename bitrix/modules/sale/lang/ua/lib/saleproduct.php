<?
$MESS["SALE_PRODUCT_ENTITY_ID"] = "ID";
$MESS["SALE_PRODUCT_ENTITY_QUANTITY"] = "Загальний залишок";
$MESS["SALE_PRODUCT_ENTITY_NAME"] = "Найменування";
$MESS["SALE_PRODUCT_ENTITY_ACTIVE"] = "Активний";
$MESS["SALE_PRODUCT_ENTITY_PRICE_IN_SITE_CURRENCY"] = "Базова ціна у валюті сайту";
$MESS["SALE_PRODUCT_ENTITY_SUMMARY_PRICE_IN_SITE_CURRENCY"] = "Вартість залишку в базовій ціні і валюті сайту";
$MESS["SALE_PRODUCT_ENTITY_VIEWS_IN_PERIOD_BY_SHOP"] = "Перегляди за період по магазину";
$MESS["SALE_PRODUCT_ENTITY_ORDERS_IN_PERIOD_BY_SHOP"] = "Замовлень за період по магазину";
$MESS["SALE_PRODUCT_ENTITY_SALED_PRODUCTS_IN_PERIOD_BY_SHOP"] = "Продано товару за період по магазину";
$MESS["SALE_PRODUCT_ENTITY_CONVERSION"] = "Конверсія, %";
?>