<?
$MESS["SALE_CASHBOX_NOT_FOUND"] = "Вільна каса не знайдена";
$MESS["SALE_CASHBOX_ACCESS_DENIED"] = "Каса '#CASHBOX_NAME#' недоступна";
$MESS["SALE_CASHBOX_ERROR_EMPTY_CHECK_TYPE"] = "Не вказано тип чека";
$MESS["SALE_CASHBOX_ERROR_CHECK"] = "Некоректний тип чека";
?>