<?
$MESS["SALE_CASHBOX_BITRIX_TITLE"] = "1С-Бітрікс.Каси";
$MESS["SALE_CASHBOX_BITRIX_CHECK_ERROR"] = "Помилка під час друку чека ##CHECK_ID#";
$MESS["SALE_CASHBOX_BITRIX_REPORT_ERROR"] = "Помилка при друку звіту ##REPORT_ID#";
$MESS["SALE_CASHBOX_BITRIX_ERR"] = "Помилка при друку чеку";
$MESS["SALE_CASHBOX_BITRIX_ERR-3800"] = "в ККТ немає готівки для повернення";
$MESS["SALE_CASHBOX_BITRIX_ERR-3803"] = "невірна ціна (сума)";
$MESS["SALE_CASHBOX_BITRIX_ERR-3804"] = "невірна кількість";
$MESS["SALE_CASHBOX_BITRIX_ERR-3805"] = "нульова ціна";
$MESS["SALE_CASHBOX_BITRIX_ERR-3816"] = "невірний вид оплати";
$MESS["SALE_CASHBOX_BITRIX_ERR-3807"] = "немає паперу";
$MESS["SALE_CASHBOX_BITRIX_ERR-3896"] = "сума не готівкових оплат перевищує суму чеку";
$MESS["SALE_CASHBOX_BITRIX_ERR-3897"] = "чек сплачений не повністю";
$MESS["SALE_CASHBOX_BITRIX_SETTINGS_P_TYPE"] = "Налаштування оплат";
$MESS["SALE_CASHBOX_BITRIX_SETTINGS_VAT"] = "Налаштування ставок ПДВ";
$MESS["SALE_CASHBOX_BITRIX_SETTINGS_P_TYPE_LABEL_Y"] = "Готівковий";
$MESS["SALE_CASHBOX_BITRIX_SETTINGS_P_TYPE_LABEL_N"] = "Безготівковий";
$MESS["SALE_CASHBOX_BITRIX_SETTINGS_P_TYPE_LABEL_A"] = "Еквайрингова операція";
$MESS["SALE_CASHBOX_BITRIX_SETTINGS_VAT_LABEL_NOT_VAT"] = "Без ПДВ [за замовчуванням]";
$MESS["SALE_CASHBOX_BITRIX_SETTINGS_Z_REPORT"] = "Налаштування друку Z-звітів";
$MESS["SALE_CASHBOX_BITRIX_SETTINGS_Z_REPORT_LABEL"] = "Час закриття касової зміни та друку Z-звіту";
$MESS["SALE_CASHBOX_BITRIX_VALIDATE_E_KKM_ID"] = "Не зазначена марка ККМ";
$MESS["SALE_CASHBOX_BITRIX_VALIDATE_E_NUMBER_KKM"] = "Не зазначено зовнішній ідентифікатор каси";
?>