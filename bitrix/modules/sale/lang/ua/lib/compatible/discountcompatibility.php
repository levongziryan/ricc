<?
$MESS["BX_SALE_DCL_ERR_BAD_MODE"] = "Неправильний режим збереження знижок";
$MESS["BX_SALE_DCL_ERR_SITE_ABSENT"] = "Не зазначений сайт";
$MESS["BX_SALE_DCL_ERR_CURRENCY_ABSENT"] = "Не вказана валюта";
$MESS["BX_SALE_DCL_ERR_ORDER_ID_ABSENT"] = "Не вказано ідентифікатор замовлення";
$MESS["BX_SALE_DCL_MESS_SIMPLE_DESCRIPTION_BASKET"] = "Зміна вартості кошика";
$MESS["BX_SALE_DCL_MESS_SIMPLE_DESCRIPTION_DELIVERY"] = "Зміна вартості доставки";
$MESS["BX_SALE_DCL_MESS_SIMPLE_DESCRIPTION_UNKNOWN"] = "Невідоме правило";
?>