<?
$MESS["SALE_OA_SHIPMENT_STATUS_ERROR"] = "Error";
$MESS["SALE_OA_ERROR_HAPPENED"] = "Error al procesar la solicitud";
$MESS["SALE_OA_ERROR_DELIVERY_SERVICE"] = "Servicio de entrega no coincide con las restricciones";
$MESS["SALE_OA_ERROR_PAY_SYSTEM_INFO"] = "Respuesta del sistema de pago incorrecto";
$MESS["SALE_OA_ERROR_INCORRECT_DATE"] = "Formato de fecha incorrecto";
$MESS["SALE_OA_ERROR_PAYSYSTEM_SERVICE"] = "El sistema de pago no está sujeto a restricción.";
$MESS["SALE_OA_ERROR_CONNECT_PAY_SYS"] = "Error al conectar con el sistema de pago";
$MESS["SALE_OA_ERROR_CANCEL_ORDER"] = "Permisos insuficientes para cancelar el pedido";
$MESS["SALE_OA_ERROR_LOAD_ORDER"] = "ID # de la orden no fue encontrada";
$MESS["SALE_OA_ERROR_CANCEL_ORDER_ALREADY"] = "La orden ya ha sido cancelada.";
$MESS["SALE_OA_ERROR_CANCEL_ORDER_NOT_YET"] = "La orden no ha sido cancelado.";
$MESS["SALE_OA_ERROR_CREATE_ORDER"] = "No se puede crear la orden.";
$MESS["SALE_OA_ERROR_ORDER_ID_WRONG"] = "ID de la orden inválida.";
$MESS["SALE_OA_ERROR_SHIPMENT_ID_WRONG"] = "ID del envío inválido.";
$MESS["SALE_OA_ERROR_PAYMENT_ID_WRONG"] = "ID del envío inválido.";
$MESS["SALE_OA_ERROR_LOAD_PAYMENT"] = "ID # del pago no fue encontrado";
$MESS["SALE_OA_ERROR_LOAD_SHIPMENT"] = "ID# del envío no fue encontrado";
$MESS["SALE_OA_ERROR_UNMARK_RIGHTS"] = "Permisos insuficientes para quitar la banderilla de problema sobre la orden.";
$MESS["SALE_OA_ERROR_HAPPENED2"] = "Error al procesar la solicitud. La mayoría de las posibles razones incluyen: un usuario no tiene permisos suficientes; el almacenamiento de sesión PHP está experimentando problemas; solicitud POST truncado por PHP o del servidor web.";
$MESS["SALE_OA_ERROR_DELETE_SHIPMENT_PERMISSION"] = "Usted no tiene permiso para eliminar este envío.";
$MESS["SALE_OA_ERROR_DELETE_PAYMENT_PERMISSION"] = "Usted no tiene permiso para eliminar este pago.";
$MESS["SALE_OA_PERMISSION"] = "Permisos insuficientes.";
$MESS["SALE_CASHBOX_SELECT_TYPE"] = "Seleccionar tipo de recibo";
$MESS["SALE_CASHBOX_SELECT_MESSAGE"] = "Para agregar un recibo nuevo, seleccione el tipo de recibo y seleccione el envío disponible";
$MESS["SALE_CASHBOX_SELECT_SHIPMENT"] = "Seleccionar envío vinculado";
$MESS["CASHBOX_ADD_CHECK_TITLE"] = "Para agregar un recibo nuevo, ingrese el ID de la orden, luego seleccione el pago y el envío";
$MESS["CASHBOX_ADD_CHECK_INPUT_ORDER"] = "Introducir ID de la orden";
$MESS["CASHBOX_ADD_CHECK_SELECT_PAYMENT"] = "Método de pago";
$MESS["CASHBOX_ADD_CHECK_SELECT_SHIPMENT"] = "Envío";
$MESS["CASHBOX_ADD_CHECK_SELECT_TYPE"] = "Seleccionar tipo de recibo";
$MESS["CASHBOX_ADD_CHECK_NOT_SELECTED"] = "No seleccionado";
$MESS["CASHBOX_CREATE_CHECK_ERROR_ORDER_ID"] = "No se puede crear recibo porque el ID de la orde es incorrecto.";
$MESS["CASHBOX_CREATE_CHECK_ERROR_EMPTY_PAYMENT"] = "No se puede crear recibo porque no se selecciona ningún pago.";
$MESS["CASHBOX_CREATE_CHECK_ERROR_PAYMENT_ID"] = "No se puede crear el recibo porque el pago seleccionado es incorrecto.";
$MESS["CASHBOX_CREATE_CHECK_ERROR_EMPTY_SHIPMENT"] = "No se puede crear el recibo porque el envío no está seleccionado.";
$MESS["CASHBOX_CREATE_CHECK_ERROR_SHIPMENT_ID"] = "No se puede crear el recibo porque el envío seleccionado es incorrecto.";
$MESS["CASHBOX_CREATE_CHECK_ERROR_TYPE"] = "No se puede crear el recibo porque el tipo no es válido.";
$MESS["CASHBOX_ADD_ZREPORT_WRONG_CHECKBOX"] = "Se ha seleccionado una caja registradora incorrecta";
?>