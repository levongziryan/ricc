<?
$MESS["SMOT_DATE"] = "Date";
$MESS["SMOT_ORDER"] = "Ordre";
$MESS["SMOT_ORDER_N"] = "Commande #";
$MESS["SMOT_HISTORY"] = "Histoire de la compagnie";
$MESS["SMOT_COMMENTS"] = "Commentaire";
$MESS["SMOT_DESCRIPTION"] = "Description";
$MESS["SMOT_ORDER_FROM"] = "de";
$MESS["SMOT_USER"] = "Utilisateur";
$MESS["SMOT_SUMM"] = "Total";
$MESS["SMOT_TRANSACT"] = "Transactions";
$MESS["SMOT_TRANS_EMPTY"] = "Aucune transaction";
?>