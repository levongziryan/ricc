<?
$MESS["SMOT_ORDER_N"] = "Pedido #";
$MESS["SMOT_ORDER_FROM"] = "de";
$MESS["SMOT_ORDER"] = "Pedido";
$MESS["SMOT_HISTORY"] = "Histórico";
$MESS["SMOT_TRANSACT"] = "Transações";
$MESS["SMOT_DATE"] = "Data";
$MESS["SMOT_USER"] = "Usuário";
$MESS["SMOT_SUMM"] = "Total";
$MESS["SMOT_DESCRIPTION"] = "Descrição";
$MESS["SMOT_COMMENTS"] = "Comentários";
$MESS["SMOT_TRANS_EMPTY"] = "Nenhuma transação encontrada";
?>