<?
$MESS["STPSL_YES"] = "Oui";
$MESS["STPSL_ACTIONS"] = "Actions";
$MESS["STPSL_NO"] = "Non";
$MESS["STPSL_CANCELED"] = "Annulé";
$MESS["STPSL_CANCEL1"] = "Annuler";
$MESS["STPSL_PRODUCT"] = "Article";
$MESS["STPSL_DATE_NEXT"] = "<br />Suivant paiement<br />date de";
$MESS["STPSL_DATE_LAST"] = "Dernière<br />date de paiement<br />de";
$MESS["STPSL_CANCEL"] = "Annuler abonnement";
$MESS["STPSL_PERIOD_BETW"] = "Périodes<br />entre<br />les paiements";
$MESS["STPSL_LAST_SUCCESS"] = "Dernière<br>paiement<br />a réussi";
$MESS["STPSL_PERIOD_TYPE"] = "Paiement <br />période<br />type";
?>