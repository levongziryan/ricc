<?
$MESS["STPC_TO_LIST"] = "Voltar para a lista";
$MESS["STPC_TIMESTAMP"] = "Data da última moficação:";
$MESS["STPC_ACTIV"] = "Ativo:";
$MESS["STPC_SORT"] = "ndice de classificação:";
$MESS["STPC_PAY_SYSTEM"] = "Sistema de pagamento:";
$MESS["STPC_CURRENCY"] = "Moeda na qual os pagamentos são permitidos:";
$MESS["STPC_ANY"] = "(qualquer um)";
$MESS["STPC_TYPE"] = "Tipo de cartão:";
$MESS["STPC_CNUM"] = "Número do cartão de crédito:";
$MESS["STPC_CEXP"] = "Data de vencimento:";
$MESS["STPC_MIN_SUM"] = "Pagamento mínimo permitido:";
$MESS["STPC_MAX_SUM"] = "Pagamento máximo permitido:";
$MESS["STPC_SUM_CURR"] = "Moeda de pagamento:";
$MESS["STPC_SAVE"] = "Salvar";
$MESS["STPC_APPLY"] = "Aplicar";
$MESS["STPC_CANCEL"] = "Cancelar";
?>