<?
$MESS["SMOP_ALL_Y"] = "Oui";
$MESS["SMOP_TITLE"] = "Boutique en ligne";
$MESS["SMOP_BACK"] = "Précédent";
$MESS["SMOP_ALL_N"] = "Non";
$MESS["SMOP_JS_SAVE_ERROR"] = "Erreur de sauvegarde.";
$MESS["SMOP_HEAD"] = "Abonnement à PUSH notifications";
$MESS["SMOP_ALL_SUBSCRIBES"] = "Abonnez-vous à des catégories:";
$MESS["SMOP_SUBSCRIBES"] = "Souscriptions";
$MESS["SMOP_SAVE"] = "Sauvegarder";
$MESS["SMOP_JS_SAVING"] = "Sauvegarde";
?>