<?
$MESS["ACTIVE"] = "Actif :";
$MESS["PROFILE_DATA_SAVED"] = "Toutes les modifications enregistrées";
$MESS["NEW_PASSWORD_CONFIRM"] = "Confirmer le nouveau mot de passe :";
$MESS["EMAIL"] = "E-mail :";
$MESS["LAST_LOGIN"] = "Dernière autorisation :";
$MESS["LAST_NAME"] = "Nom :";
$MESS["LAST_UPDATE"] = "Dernière mise à jour :";
$MESS["LOGIN"] = "Identifiant (min. 3 caractères) :";
$MESS["NAME"] = "Nom :";
$MESS["MAIN_RESET"] = "Annuler";
$MESS["NEW_PASSWORD"] = "Nouveau mot de passe (min. 6 caractères) :";
$MESS["NEW_PASSWORD_REQ"] = "Nouveau mot de passe :";
$MESS["RESET"] = "Réinitialiser";
$MESS["SAVE"] = "Enregistrer les modifications";
$MESS["SECOND_NAME"] = "Deuxième prénom :";
?>