<?
$MESS["PROFILE_DATA_SAVED"] = "Guardar todos los cambios";
$MESS["LAST_UPDATE"] = "Última actualización:";
$MESS["ACTIVE"] = "Activo:";
$MESS["NAME"] = "Nombre:";
$MESS["LAST_NAME"] = "Apellido:";
$MESS["SECOND_NAME"] = "Segundo nombre:";
$MESS["EMAIL"] = "E-mail:";
$MESS["MAIN_RESET"] = "Cancelar";
$MESS["LOGIN"] = "Inicio de sesión (mínimo 3 caracteres):";
$MESS["NEW_PASSWORD"] = "Nueva contraseña: (minimo 6 caracteres):";
$MESS["NEW_PASSWORD_CONFIRM"] = "Confirmar nueva contraseña:";
$MESS["SAVE"] = "Guardar cambios";
$MESS["RESET"] = "Reiniciar";
$MESS["LAST_LOGIN"] = "Última autorización:";
$MESS["NEW_PASSWORD_REQ"] = "Nueva Contraseña:";
?>