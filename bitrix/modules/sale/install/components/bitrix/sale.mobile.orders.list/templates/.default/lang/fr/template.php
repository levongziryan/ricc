<?
$MESS["SMOL_N"] = "#";
$MESS["SMOL_NO_ORDERS"] = "A ce moment il n'y a pas de commandes";
$MESS["SMOL_VISIBLE_FIELDS"] = "champs visibles";
$MESS["SMOL_ALL_ORDERS"] = "Toutes les commandes";
$MESS["SMOL_CHOOSE_FIELDS"] = "Choix des champs";
$MESS["SMOL_ORDERS"] = "Commandes";
$MESS["SMOL_NO_ORDERS2"] = "Pas de commandes";
$MESS["SMOL_PRODUCTS_COUNT"] = "noms de marchandises";
$MESS["SMOL_FILTER_TUNE"] = "Rédaction du filtre";
$MESS["SMOL_FILTER_SETT"] = "Réglage de la filtration";
$MESS["SMOL_MOBILEAPP_NOT_INSTALLED"] = "Le module iblock n'a pas été installé.";
$MESS["LOAD_TEXT"] = "Mise à jour...";
$MESS["SMOL_WAITING_FOR_DELIVERY"] = "En attente de livraison";
$MESS["SMOL_WAITING_FOR_PAY"] = "Attendent le paiement";
$MESS["SMOL_FROM"] = "de";
$MESS["DOWN_TEXT"] = "Relâcher pour mettre à jour...";
$MESS["SMOL_FILTER_DELIVERY"] = "Par livraison";
$MESS["SMOL_FILTER_PAY"] = "Sur le paiement";
$MESS["SMOL_FILTER_USER"] = "Filtre utilisateur";
$MESS["PULL_TEXT"] = "Tirer pour mettre à jour...";
$MESS["SMOL_SUMM"] = "Total";
?>