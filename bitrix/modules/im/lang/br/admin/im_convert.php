<?
$MESS["IM_CONVERT_BUTTON"] = "Converter";
$MESS["IM_CONVERT_STOP"] = "Parar";
$MESS["IM_CONVERT_STEP"] = "Duração máxima da etapa de conversão:";
$MESS["IM_CONVERT_STEP_sec"] = "seg.";
$MESS["IM_CONVERT_TAB"] = "Conversão";
$MESS["IM_CONVERT_TAB_TITLE"] = "Parâmentros de conversão";
$MESS["IM_CONVERT_TITLE"] = "Converter histórico de mensagens";
$MESS["IM_CONVERT_COMPLETE"] = "A conversão foi concluída.";
$MESS["IM_CONVERT_COMPLETE_ALL_OK"] = "Todos os dados foram convertidos com sucesso, nenhuma conversão adicional necessária.";
$MESS["IM_CONVERT_NEXT_STEP"] = "Próximo passo";
$MESS["IM_CONVERT_IN_PROGRESS"] = "Convertendo...";
$MESS["IM_CONVERT_TOTAL"] = "Mensagens convertidas:";
$MESS["IM_CONVERT_ABOUT_TIME"] = "aprox.";
$MESS["IM_CONVERT_ABOUT_TIME_MINUTE"] = "minutos";
$MESS["IM_CONVERT_ABOUT_TIME_SEC"] = "segundos";
?>