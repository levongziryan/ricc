<?
$MESS["IM_MODULE_NAME"] = "Mensageiro Instantâneo";
$MESS["IM_MODULE_DESCRIPTION"] = "Oferece suporte para mensagens instantâneas e serviço de notificações";
$MESS["IM_INSTALL_TITLE"] = "Instalação do módulo de mensagens instantâneas";
$MESS["IM_UNINSTALL_TITLE"] = "Desinstalação do módulo de mensagens instantâneas";
$MESS["IM_CONVERT_MESSAGE"] = "Você instalou o módulo de \"Mensagens Instantâneas\". Para acessar o histórico de mensagens, você tem que #A_TAG_START#converter o histórico de dados#A_TAG_END#";
?>