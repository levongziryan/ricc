<?
$MESS["IM_MODULE_NAME"] = "Instant Messenger";
$MESS["IM_MODULE_DESCRIPTION"] = "Fornece suporte para mensagens instantâneas e serviços de notificação.";
$MESS["IM_INSTALL_TITLE"] = "Instalação do módulo Instant Messenger";
$MESS["IM_UNINSTALL_TITLE"] = "Desinstalação do módulo Instant Messenger";
$MESS["IM_CONVERT_MESSAGE"] = "Você instalou o módulo \"Instant Messenger\". Para acessar o histórico de mensagens, você tem que #A_TAG_START#converter o histórico de dados#A_TAG_END#.";
?>