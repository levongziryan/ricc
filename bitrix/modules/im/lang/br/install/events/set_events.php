<?
$MESS["IM_NEW_NOTIFY_NAME"] = "Nova notificação";
$MESS["IM_NEW_NOTIFY_DESC"] = "#MESSAGE_ID# - ID da mensagem
#USER_ID# - ID do usuário
#USER_LOGIN# - Login do usuário
#USER_NAME# - primeiro nome do usuário
#USER_LAST_NAME# - Último nome do usuário
#FROM_USER_ID# - ID do remetente da mensagem
#FROM_USER# - nome do remetente da mensagem
#DATE_CREATE# - a data em que a mensagem foi criada
#TITLE# - título da mensagem
#MESSAGE# - corpo da mensagem
#MESSAGE_50# - primeiros 50 caracteres do texto da mensagem
#EMAIL_TO# - endereço de email do destinatário da mensagem
";
$MESS["IM_NEW_NOTIFY_SUBJECT"] = "#SITE_NAME#: Notificação \"#MESSAGE_50#\"";
$MESS["IM_NEW_NOTIFY_MESSAGE"] = "Olá #USER_NAME#!

Você tem uma nova notificação de #FROM_USER# 

------------------------------------------

 #MESSAGE# 

------------------------------------------

 Exibir notificações: http://#SERVER_NAME#/?IM_NOTIFY=Y
 Editar preferências de notificação: http://#SERVER_NAME#/?IM_SETTINGS=NOTIFY

 Esta é uma mensagem automática, não responda.";
$MESS["IM_NEW_NOTIFY_GROUP_NAME"] = "Nova notificação (grupo)";
$MESS["IM_NEW_NOTIFY_GROUP_DESC"] = "#MESSAGE_ID# - ID da mensagem
#USER_ID# - ID do usuário
#USER_LOGIN# - Login do usuário
#USER_NAME# - primeiro nome do usuário
#USER_LAST_NAME# - Último nome do usuário
#FROM_USERS# - nomes do rementente da mensagem
#DATE_CREATE# - a data em que a mensagem foi criada
#TITLE# - título da mensagem
#MESSAGE# - corpo da mensagem
#MESSAGE_50# - primeiros 50 caracteres do texto da mensagem
#EMAIL_TO# - endereço de email do destinatário da mensagem ";
$MESS["IM_NEW_NOTIFY_GROUP_SUBJECT"] = "#SITE_NAME#: Notificação \"#MESSAGE_50#\"";
$MESS["IM_NEW_NOTIFY_GROUP_MESSAGE"] = "Olá #USER_NAME#!

Você tem uma nova notificação de #FROM_USERS# 

------------------------------------------

 #MESSAGE# 

------------------------------------------

 Exibir notificações: http://#SERVER_NAME#/?IM_NOTIFY=Y
 Editar preferências de notificação: http://#SERVER_NAME#/?IM_SETTINGS=NOTIFY

 Esta é uma mensagem automática, não responda.";
$MESS["IM_NEW_MESSAGE_NAME"] = "Nova mensagem";
$MESS["IM_NEW_MESSAGE_DESC"] = "#USER_ID# - ID do usuário
#USER_LOGIN# - login do usuário
#USER_NAME# - primeiro nome do usuário
#USER_LAST_NAME# - Último nome do usuário
#FROM_USER# - nome do remetente da mensagem
#MESSAGES# - mensagens
#EMAIL_TO# - endereço de email do destinatário ";
$MESS["IM_NEW_MESSAGE_SUBJECT"] = "#SITE_NAME#: Mensagens instantâneas de #FROM_USER#";
$MESS["IM_NEW_MESSAGE_MESSAGE"] = "Olá #USER_NAME#!

Você tem novas mensagens instantâneas de #FROM_USER#.

------------------------------------------
 #MESSAGES# 
------------------------------------------

 Clique para iniciar conversa:  http://#SERVER_NAME#/?IM_DIALOG=#USER_ID#
Editar preferências de notificação: http://#SERVER_NAME#/?IM_SETTINGS=NOTIFY

Esta é uma mensagem automática, não responda.";
$MESS["IM_NEW_MESSAGE_GROUP_NAME"] = "Nova mensagem (grupo)";
$MESS["IM_NEW_MESSAGE_GROUP_DESC"] = "#USER_ID# - ID do usuário
#USER_LOGIN# - login do usuário
#USER_NAME# - primeiro nome do usuário
#USER_LAST_NAME# - Último nome do usuário
#FROM_USER# - nome do remetente da mensagem
#MESSAGES# - mensagens
#EMAIL_TO# - endereço de email do destinatário ";
$MESS["IM_NEW_MESSAGE_GROUP_SUBJECT"] = "#SITE_NAME#: Mensagens instantâneas de #FROM_USERS#";
$MESS["IM_NEW_MESSAGE_GROUP_MESSAGE"] = "Olá #USER_NAME#!

Você tem novas mensagens instantâneas de #FROM_USERS#.

#MESSAGES# 

Clique para visualizar as conversas: http://#SERVER_NAME#/?IM_DIALOG=Y
Editar preferências de notificação: http://#SERVER_NAME#/?IM_SETTINGS=NOTIFY

Esta é uma mensagem automática, não responda.";
?>