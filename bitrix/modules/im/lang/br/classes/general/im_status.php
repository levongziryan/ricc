<?
$MESS["IM_STATUS_ONLINE"] = "On-line";
$MESS["IM_STATUS_MOBILE"] = "Aplicativo para celular";
$MESS["IM_STATUS_IDLE"] = "Ocioso";
$MESS["IM_STATUS_DND"] = "Não perturbe";
$MESS["IM_STATUS_AWAY"] = "Ausente";
$MESS["IM_STATUS_VACATION"] = "De licença";
$MESS["IM_STATUS_EAID_BOT"] = "Chat Bot";
$MESS["IM_STATUS_EAID_EMAIL"] = "Usuário de e-mail";
$MESS["IM_STATUS_EAID_NETWORK"] = "Bitrix24.Network";
$MESS["IM_STATUS_EAID_REPLICA"] = "Bitrix24.Network";
$MESS["IM_STATUS_EAID_CONTROLLER"] = "Assistência Técnica";
$MESS["IM_STATUS_EAID_IMCONNECTOR"] = "Usuário do Canal Aberto";
?>