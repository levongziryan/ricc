<?
$MESS["IM_NS_DEFAULT"] = "Notificações indefinidas";
$MESS["IM_NS_MAIN"] = "Avaliações e \"Curtidas\"";
$MESS["IM_NS_MAIN_RATING_VOTE"] = "Notificação de voto (\"Curtir\")";
$MESS["IM_NS_BIZPROC_ACTIVITY"] = "Notificação sobre atividade em processos de negócios";
$MESS["IM_NS_MAIN_RATING_VOTE_MENTIONED"] = "Notificação sobre votar em postagens que mencionam você";
$MESS["IM_NS_IM"] = "Bate-papo e Chamadas";
$MESS["IM_NS_LIKE"] = "Curtidas do bate-papo";
$MESS["IM_NS_MENTION"] = "Você foi mencionado num bate-papo público";
$MESS["IM_NS_OPEN"] = "Mensagens em bate-papos públicos";
$MESS["IM_NS_MESSAGE_2"] = "Mensagens em <nobr>bate-papo de pessoa para pessoa</nobr>";
$MESS["IM_NS_CHAT_2"] = "Mensagens em bate-papos privados";
$MESS["IM_NS_MESSAGE"] = "Mensagens privadas";
$MESS["IM_NS_CHAT"] = "Mensagens de bate-papo privado";
?>