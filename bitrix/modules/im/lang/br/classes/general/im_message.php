<?
$MESS["IM_ERROR_EMPTY_FROM_USER_ID"] = "O remetente da mensagem não está especificado.";
$MESS["IM_ERROR_EMPTY_TO_USER_ID"] = "O destinatário da mensagem não está especificado.";
$MESS["IM_ERROR_EMPTY_MESSAGE"] = "O texto da mensagem está vazio.";
$MESS["IM_ERROR_EMPTY_USER_ID"] = "Nenhum ID de usuário especificado.";
$MESS["IM_MESSAGE_FORMAT_DATE"] = "g: i a, d F Y";
$MESS["IM_MESSAGE_FORMAT_TIME"] = "g: i a";
?>