<?
$MESS["IM_DISK_STORAGE_TITLE"] = "Armazenamento no Instant Messenger";
$MESS["IM_DISK_ERR_AVATAR_1"] = "Você não pode mudar seu avatar neste bate-papo.";
$MESS["IM_DISK_AVATAR_CHANGE_M"] = "#USER_NAME# mudou o ícone de bate-papo";
$MESS["IM_DISK_AVATAR_CHANGE_F"] = "#USER_NAME# mudou o ícone de bate-papo";
$MESS["IM_DISK_ERR_UPLOAD"] = "Você não pode carregar arquivos para este bate-papo.";
$MESS["IM_DISK_LOCAL_FOLDER_B24_TITLE"] = "imessenger";
$MESS["IM_DISK_LOCAL_FOLDER_TITLE"] = "imessenger";
?>