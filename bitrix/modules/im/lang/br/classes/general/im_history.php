<?
$MESS["IM_HISTORY_ERROR_TO_USER_ID"] = "O ID do usuário está faltando.";
$MESS["IM_HISTORY_SEARCH_EMPTY"] = "A consulta de pesquisa está vazia.";
$MESS["IM_HISTORY_ERROR_MESSAGE_ID"] = "A consulta de pesquisa está vazia.";
$MESS["IM_HISTORY_FORMAT_DATE"] = "g: i a, d F Y";
$MESS["IM_HISTORY_FORMAT_TIME"] = "g: i a";
$MESS["IM_HISTORY_SEARCH_DATE_EMPTY"] = "Não foi especificada nenhuma data dentro da qual pesquisar mensagens.";
?>