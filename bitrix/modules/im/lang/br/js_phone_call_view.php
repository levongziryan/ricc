<?
$MESS["IM_PHONE_CALL_VIEW_CALL_LIST_TITLE"] = "Lista de espera de chamada";
$MESS["IM_PHONE_CALL_VIEW_WEBFORM_TITLE"] = "Formulário";
$MESS["IM_PHONE_CALL_VIEW_CALL_LIST_DEFER_15_MIN"] = "Suspender por 15 min.";
$MESS["IM_PHONE_CALL_VIEW_CALL_LIST_DEFER_HOUR"] = "Suspender por uma hora";
$MESS["IM_PHONE_CALL_VIEW_CALL_LIST_TO_END"] = "Fim da lista";
$MESS["IM_PHONE_CALL_VIEW_FOLDED_CALL_LIST_TO_NUMBER"] = "Ligando para o número";
$MESS["IM_PHONE_CALL_VIEW_FOLDED_BUTTON_CALL"] = "Ligação";
$MESS["IM_PHONE_CALL_VIEW_FOLDED_BUTTON_NEXT"] = "Próximo";
$MESS["IM_PHONE_CALL_LIST_MORE"] = "#COUNT# mais";
$MESS["IM_PHONE_CALL_VIEW_NUMBER_UNKNOWN"] = "Nenhum número especificado";
$MESS["IM_PHONE_CALL_VIEW_MORE"] = "Mais";
$MESS["IM_PHONE_CALL_VIEW_RATE_QUALITY"] = "Avalie a qualidade de conexão";
$MESS["IM_PHONE_CALL_VIEW_DONT_LEAVE"] = "Sair da página encerrará a chamada";
$MESS["IM_PHONE_CALL_VIEW_FOLD"] = "Minimizar";
$MESS["IM_PHONE_CALL_VIEW_UNFOLD"] = "Restaurar";
$MESS["IM_PHONE_CALL_VIEW_INTERCEPT"] = "Interceptar";
$MESS["IM_PHONE_CALL_VIEW_SAVE"] = "Salvar";
$MESS["IM_PHONE_CALL_COMMENT_PLACEHOLDER"] = "Digite o texto do comentário";
?>