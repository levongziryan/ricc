<?
$MESS["BOT_ENTITY_BOT_ID_FIELD"] = "ID do bot";
$MESS["BOT_ENTITY_MODULE_ID_FIELD"] = "ID do módulo";
$MESS["BOT_ENTITY_TO_CLASS_FIELD"] = "Classe do manipulador do bot";
$MESS["BOT_ENTITY_TO_METHOD_FIELD"] = "Método de classe do manipulador do bot";
$MESS["BOT_ENTITY_APP_ID_FIELD"] = "ID do app REST";
$MESS["BOT_ENTITY_BOT_TYPE_FIELD"] = "Tipo de bot";
$MESS["BOT_ENTITY_METHOD_MESSAGE_ADD_FIELD"] = "Método de classe do manipulador de mensagens recebidas";
$MESS["BOT_ENTITY_METHOD_WELCOME_MESSAGE_FIELD"] = "Método de classe adicionar mensagem de boas-vindas";
$MESS["BOT_ENTITY_METHOD_BOT_DELETE_FIELD"] = "Método de classe do manipulador de exclusão de bot";
$MESS["BOT_ENTITY_COUNT_MESSAGE_FIELD"] = "Contador de mensagens processadas";
$MESS["BOT_ENTITY_COUNT_SLASH_FIELD"] = "Contador de comandos processados";
$MESS["BOT_ENTITY_COUNT_CHAT_FIELD"] = "Convites para o contador do bate-papo em grupo";
$MESS["BOT_ENTITY_COUNT_USER_FIELD"] = "Convites para o contador do bate-papo público";
$MESS["BOT_ENTITY_LANGUAGE_FIELD"] = "Idioma do bot";
$MESS["BOT_ENTITY_TEXT_PRIVATE_WELCOME_MESSAGE_FIELD"] = "Texto da mensagem de boas-vindas";
$MESS["BOT_ENTITY_TEXT_CHAT_WELCOME_MESSAGE_FIELD"] = "Método de classe para adicionar uma mensagem de boas-vindas (bate-papo em grupo)";
$MESS["BOT_ENTITY_VERIFIED_FIELD"] = "Sinalizador de identificação";
$MESS["BOT_ENTITY_OPENLINE_FIELD"] = "Sinalizador de suporte de canal aberto";
?>