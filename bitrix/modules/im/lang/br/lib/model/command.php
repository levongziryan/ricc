<?
$MESS["COMMAND_ENTITY_ID_FIELD"] = "ID do comando";
$MESS["COMMAND_ENTITY_BOT_ID_FIELD"] = "ID do bot do bate-papo";
$MESS["COMMAND_ENTITY_COMMAND_FIELD"] = "Comando";
$MESS["COMMAND_ENTITY_HIDDEN_FIELD"] = "Status ocultar comando";
$MESS["COMMAND_ENTITY_SONET_SUPPORT_FIELD"] = "Status de suporte de Fluxo de Atividade";
$MESS["COMMAND_ENTITY_CLASS_FIELD"] = "Classe de manipulador do comando";
$MESS["COMMAND_ENTITY_METHOD_COMMAND_ADD_FIELD"] = "Método de classe de manipulador para comandos de entrada";
$MESS["COMMAND_ENTITY_METHOD_LANG_GET_FIELD"] = "Método de classe para receber localizações";
$MESS["COMMAND_ENTITY_APP_ID_FIELD"] = "ID do app REST";
$MESS["COMMAND_ENTITY_MODULE_ID_FIELD"] = "ID do módulo";
$MESS["COMMAND_ENTITY_COMMON_FIELD"] = "Status global do comando";
?>