<?
$MESS["BOT_ENTITY_BOT_ID_FIELD"] = "ID de Bot";
$MESS["BOT_ENTITY_MODULE_ID_FIELD"] = "ID do Módulo";
$MESS["BOT_ENTITY_TO_CLASS_FIELD"] = "Classe de manipulador de bot";
$MESS["BOT_ENTITY_TO_METHOD_FIELD"] = "Método da classe de manipulador de bot";
$MESS["BOT_ENTITY_APP_ID_FIELD"] = "ID de app REST";
$MESS["BOT_ENTITY_BOT_TYPE_FIELD"] = "Tipo de bot";
$MESS["BOT_ENTITY_METHOD_MESSAGE_ADD_FIELD"] = "Método da classe de manipulador de mensagens recebidas";
$MESS["BOT_ENTITY_METHOD_WELCOME_MESSAGE_FIELD"] = "Mensagem de boas-vindas adiciona método da classe";
$MESS["BOT_DEFAULT_WORK_POSITION"] = "Bot de bate-papo";
$MESS["BOT_ENTITY_METHOD_BOT_DELETE_FIELD"] = "Método da classe de manipulador de exclusão de bot";
$MESS["BOT_MESSAGE_INSTALL_SYSTEM"] = "Bot de bate-papo foi adicionado à sua conta Bitrix24";
$MESS["BOT_MESSAGE_INSTALL_USER"] = "#USER_NAME# adicionou um novo bot de bate-papo";
$MESS["BOT_MESSAGE_INSTALL_USER_F"] = "#USER_NAME# adicionou um novo bot de bate-papo";
$MESS["BOT_MESSAGE_FROM"] = "#BOT_NAME# Bot de Bate-papo";
?>