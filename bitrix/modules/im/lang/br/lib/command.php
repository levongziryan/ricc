<?
$MESS["COMMAND_IM_CATEGORY"] = "Instant Messenger";
$MESS["COMMAND_DEF_ME_TITLE"] = "Aplica o estilo itálico";
$MESS["COMMAND_DEF_ME_PARAMS"] = "texto";
$MESS["COMMAND_DEF_LOUD_TITLE"] = "Aplica o estilo negrito";
$MESS["COMMAND_DEF_LOUD_PARAMS"] = "texto";
$MESS["COMMAND_DEF_QUOTE_TITLE"] = "Interpreta o texto como citação";
$MESS["COMMAND_DEF_QUOTE_PARAMS"] = "texto";
$MESS["COMMAND_DEF_RENAME_TITLE"] = "Editar título do bate-papo";
$MESS["COMMAND_DEF_RENAME_PARAMS"] = "novo título";
$MESS["COMMAND_DEF_WD_TITLE"] = "Ativar o modo de depuração de chamada de vídeo";
$MESS["COMMAND_DEF_CATEGORY_CHAT"] = "Bate-papos";
$MESS["COMMAND_DEF_CATEGORY_DEBUG"] = "Depuração";
$MESS["COMMAND_BOT_ANSWER"] = "#BOT_NAME# Bot de Bate-papo";
$MESS["COMMAND_SYSTEM_ANSWER"] = "Responder ao comando #COMMAND#";
$MESS["COMMAND_DEF_STTS_TITLE"] = "Ativar rastreamento de status \"online\"";
$MESS["COMMAND_DEF_SPTS_TITLE"] = "Desativar rastreamento de status \"online\"";
$MESS["COMMAND_DEF_CATEGORY_DIALOG"] = "Conversa";
?>