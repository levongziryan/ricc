<?
$MESS["STATUS_ENTITY_USER_ID_FIELD"] = "ID do Usuário";
$MESS["STATUS_ENTITY_STATUS_FIELD"] = "Status";
$MESS["STATUS_ENTITY_IDLE_FIELD"] = "Tempo ocioso";
$MESS["STATUS_ENTITY_DESKTOP_LAST_DATE_FIELD"] = "Última atividade registrada no (desktop)";
$MESS["STATUS_ENTITY_MOBILE_LAST_DATE_FIELD"] = "Última atividade registrada no (aplicativo de celular)";
$MESS["STATUS_ENTITY_EVENT_ID_FIELD"] = "ID do evento";
$MESS["STATUS_ENTITY_EVENT_UNTIL_DATE_FIELD"] = "Evento válido até";
$MESS["STATUS_ENTITY_COLOR_FIELD"] = "Definido pelo usuário";
?>