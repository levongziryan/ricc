<?
$MESS["IM_SHARE_CHAT"] = "Чат";
$MESS["IM_SHARE_FILE"] = "Файл";
$MESS["IM_SHARE_POST_WELCOME"] = "Пропоную обговорити :)";
$MESS["IM_SHARE_CHAT_TASK"] = "На підставі повідомлення створено [URL=#LINK#]завдання[/URL].";
$MESS["IM_SHARE_CHAT_POST"] = "На підставі повідомлення створено [URL=#LINK#]обговорення[/URL] в Живій стрічці.";
$MESS["IM_SHARE_CHAT_CALEND"] = "На підставі повідомлення створено [URL=#LINK#]подію в календарі[/URL].";
$MESS["IM_SHARE_CHAT_CHAT"] = "На підставі повідомлення було створено чат.";
$MESS["IM_SHARE_CHAT_CHAT_WELCOME"] = "Цей чат було створений на підставі повідомлення в чаті #CHAT#.";
$MESS["IM_SHARE_CHAT_CHAT_JOIN"] = "приєднатися";
?>