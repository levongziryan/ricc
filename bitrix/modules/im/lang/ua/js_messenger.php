<?
$MESS["IM_STATUS_ONLINE"] = "Доступний";
$MESS["IM_STATUS_OFFLINE"] = "Не в мережі";
$MESS["IM_STATUS_DND"] = "Зайнятий";
$MESS["IM_MESSENGER_NEW_MESSAGE"] = "Нове повідомлення";
$MESS["IM_MESSENGER_NO_MESSAGE"] = "Немає повідомлень";
$MESS["IM_MESSENGER_HISTORY"] = "Історія повідомлень";
$MESS["IM_MESSENGER_HISTORY_DELETE_ALL"] = "Видалити всі повідомлення";
$MESS["IM_MESSENGER_HISTORY_DELETE_ALL_CONFIRM"] = "Ви впевнені, що хочете видалити всю історію повідомлень?";
$MESS["IM_MESSENGER_HISTORY_DELETE"] = "Видалити повідомлення";
$MESS["IM_MESSENGER_HISTORY_DELETE_CONFIRM"] = "Ви впевнені, що хочете видалити це повідомлення?";
$MESS["IM_MESSENGER_LOAD_MESSAGE"] = "Завантаження повідомлень";
$MESS["IM_MESSENGER_SEND_FILE"] = "Передати файл";
$MESS["IM_MESSENGER_SEND_MESSAGE"] = "Відправити";
$MESS["IM_MESSENGER_NOT_DELIVERED"] = "повідомлення не доставлено";
$MESS["IM_MESSENGER_DELIVERED"] = "доставка повідомлення";
$MESS["IM_MESSENGER_CONTACT_LIST"] = "Список контактів";
$MESS["IM_MESSENGER_VIEW_OFFLINE"] = "Показати / сховати відсутніх користувачів";
$MESS["IM_MESSENGER_VIEW_GROUP"] = "Показати / сховати групи користувачів";
$MESS["IM_MESSENGER_WRITE_MESSAGE"] = "Написати повідомлення";
$MESS["IM_MESSENGER_OPEN_HISTORY"] = "Відкрити історію";
$MESS["IM_MESSENGER_OPEN_VIDEO"] = "Відеодзвінок";
$MESS["IM_MESSENGER_OPEN_PROFILE"] = "Профіль користувача";
$MESS["IM_MESSENGER_MESSAGES"] = "Діалоги";
$MESS["IM_MESSENGER_CL_EMPTY"] = "— Немає контактів —";
?>