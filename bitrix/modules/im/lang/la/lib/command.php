<?
$MESS["COMMAND_IM_CATEGORY"] = "Mensajería instantánea";
$MESS["COMMAND_DEF_ME_TITLE"] = "Aplica el estilo de cursiva";
$MESS["COMMAND_DEF_ME_PARAMS"] = "texto";
$MESS["COMMAND_DEF_LOUD_TITLE"] = "Se aplica un estilo atrevido";
$MESS["COMMAND_DEF_LOUD_PARAMS"] = "texto";
$MESS["COMMAND_DEF_QUOTE_TITLE"] = "procesa el texto como cotización";
$MESS["COMMAND_DEF_QUOTE_PARAMS"] = "texto";
$MESS["COMMAND_BOT_ANSWER"] = "#BOT_NAME# Chat Bot";
$MESS["COMMAND_SYSTEM_ANSWER"] = "Respuesta al comando #COMMAND#";
$MESS["COMMAND_DEF_RENAME_TITLE"] = "Editar título del chat";
$MESS["COMMAND_DEF_RENAME_PARAMS"] = "nuevo título";
$MESS["COMMAND_DEF_WD_TITLE"] = "Activar el modo de depuración de videollamada";
$MESS["COMMAND_DEF_CATEGORY_CHAT"] = "Chats";
$MESS["COMMAND_DEF_CATEGORY_DEBUG"] = "Depurar";
$MESS["COMMAND_DEF_STTS_TITLE"] = "Activar el seguimiento de estado \"en línea\"";
$MESS["COMMAND_DEF_SPTS_TITLE"] = "Desactivar el seguimiento de estado \"en línea\"";
$MESS["COMMAND_DEF_CATEGORY_DIALOG"] = "Conversación";
?>