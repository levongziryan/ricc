<?
$MESS["BOT_ENTITY_BOT_ID_FIELD"] = "ID del Bot";
$MESS["BOT_ENTITY_MODULE_ID_FIELD"] = "ID del Módulo ";
$MESS["BOT_ENTITY_TO_CLASS_FIELD"] = "Clase de controlador bot";
$MESS["BOT_ENTITY_APP_ID_FIELD"] = "ID de la aplicación REST";
$MESS["BOT_ENTITY_BOT_TYPE_FIELD"] = "Tipo de bot";
$MESS["BOT_ENTITY_METHOD_MESSAGE_ADD_FIELD"] = "Método de la clase controlador de mensaje entrante.";
$MESS["BOT_ENTITY_METHOD_WELCOME_MESSAGE_FIELD"] = "Mensaje de bienvenida al agregar el método de clase";
$MESS["BOT_ENTITY_TO_METHOD_FIELD"] = "Método de clase controlador de robot";
$MESS["BOT_ENTITY_METHOD_BOT_DELETE_FIELD"] = "Eliminación del método de clase controlador de robot";
$MESS["BOT_ENTITY_COUNT_MESSAGE_FIELD"] = "Contador de mensajes procesados";
$MESS["BOT_ENTITY_COUNT_SLASH_FIELD"] = "Contador de comandos procesados";
$MESS["BOT_ENTITY_COUNT_CHAT_FIELD"] = "Invitar al contador de chat de grupo";
$MESS["BOT_ENTITY_COUNT_USER_FIELD"] = "Invitar al contador de chat público";
$MESS["BOT_ENTITY_LANGUAGE_FIELD"] = "Idioma del bot";
$MESS["BOT_ENTITY_TEXT_PRIVATE_WELCOME_MESSAGE_FIELD"] = "Texto del mensaje de bienvenida";
$MESS["BOT_ENTITY_TEXT_CHAT_WELCOME_MESSAGE_FIELD"] = "Clase de métodos para agregar un mensaje de bienvenida (chat en grupo)";
$MESS["BOT_ENTITY_VERIFIED_FIELD"] = "Bandera de identificación";
$MESS["BOT_ENTITY_OPENLINE_FIELD"] = "Indicador de soporte de canal abierto";
$MESS["BOT_ENTITY_METHOD_MESSAGE_UPDATE_FIELD"] = "Handler de actualización de mensajes (método de clase)";
$MESS["BOT_ENTITY_METHOD_MESSAGE_DELETE_FIELD"] = "Handler de eliminación de mensajes (método de clase)";
?>