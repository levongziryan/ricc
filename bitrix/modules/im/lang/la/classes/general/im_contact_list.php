<?
$MESS["IM_CL_GROUP_LAST"] = "Contactos recientes";
$MESS["IM_CL_GROUP_OTHER"] = "Contactos externos";
$MESS["IM_CL_GROUP_OTHER_2"] = "Otros contactos";
$MESS["IM_CL_GROUP_ALL"] = "Todos los contactos";
$MESS["IM_CL_GROUP_EXTRANET"] = "Extranet";
$MESS["IM_CL_GROUP_FRIENDS"] = "Amigos";
$MESS["IM_CL_GROUP_SG"] = "Extranet:";
$MESS["IM_QUOTE"] = "Cotización";
$MESS["IM_CL_SEARCH_EMPTY"] = "La consulta de búsqueda está vacía.";
$MESS["IM_FILE"] = "Archivo";
$MESS["IM_CL_GROUP_CHATS"] = "Chats de grupo";
$MESS["IM_CL_GROUP_SEARCH"] = "Resultados de búsqueda";
$MESS["IM_SEARCH_OL"] = "Canal Abierto";
?>