<?
$MESS["IM_STATUS_ONLINE"] = "En línea";
$MESS["IM_STATUS_MOBILE"] = "Aplicación movil";
$MESS["IM_STATUS_IDLE"] = "Desocupado";
$MESS["IM_STATUS_DND"] = "No molestar";
$MESS["IM_STATUS_AWAY"] = "Fuera";
$MESS["IM_STATUS_VACATION"] = "De permiso";
$MESS["IM_STATUS_EAID_BOT"] = "Chat Bot";
$MESS["IM_STATUS_EAID_EMAIL"] = "E-mail del Usuario";
$MESS["IM_STATUS_EAID_NETWORK"] = "Bitrix24.Network";
$MESS["IM_STATUS_EAID_REPLICA"] = "Bitrix24.Network";
$MESS["IM_STATUS_EAID_CONTROLLER"] = "Helpdesk";
$MESS["IM_STATUS_EAID_IMCONNECTOR"] = "Usuario de Canal Abierto";
?>