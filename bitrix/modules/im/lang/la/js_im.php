<?
$MESS["IM_WN_WIN"] = "Su navegador soporta notificaciones en la barra de tareas de Windows.";
$MESS["IM_WN_MAC"] = "El navegador soporta notificaciones en el escritorio.";
$MESS["IM_WN_TEXT"] = "Para recibir notificaciones externas, tiene que habilitar esta opción.";
$MESS["IM_WN_ACCEPT"] = "Permitir";
$MESS["IM_WN_CANCEL"] = "No volver a preguntar";
$MESS["IM_CL_TAB_LIST"] = "Contactos";
$MESS["IM_CL_TAB_RECENT"] = "Último";
$MESS["IM_ICON_SET"] = "Conjuntos de iconos";
$MESS["IM_MENTION_MENU"] = "Mencionar un usuario o chat público";
$MESS["IM_COMMAND_MENU"] = "Abrir ista de comandos disponibles";
$MESS["IM_HIDDEN_MODE_MENU"] = "Modo silencioso on/off";
$MESS["IM_ANSWERS_MENU"] = "Abrir lista de respuestas conservadas";
$MESS["IM_FORMS_MENU"] = "Abrir formulario de la lista del CRM";
$MESS["IM_SMILE_MENU"] = "Haga clic para seleccionar smiley";
$MESS["IM_SMILE_NA"] = "Smiles no están disponibles.";
$MESS["IM_SMILE_SET"] = "fijar";
$MESS["IM_RECENT_TODAY"] = "hoy";
$MESS["IM_RECENT_YESTERDAY"] = "ayer";
$MESS["IM_M_SEARCH"] = "Buscar";
$MESS["IM_M_SEARCH_PLACEHOLDER"] = "Nombre o apellido";
$MESS["IM_M_SEARCH_PLACEHOLDER_CP"] = "Nombre o apellido o departamento";
$MESS["IM_M_NEW_MESSAGE"] = "Nuevo mensaje";
$MESS["IM_M_HISTORY"] = "Historial de mensajes";
$MESS["IM_M_HISTORY_DELETE_ALL"] = "Eliminar todos los mensajes";
$MESS["IM_M_HISTORY_DELETE_ALL_CONFIRM"] = "¿Está seguro de que desea eliminar todos los registros?";
$MESS["IM_M_HISTORY_DELETE"] = "Eliminar mensaje";
$MESS["IM_M_HISTORY_DELETE_CONFIRM"] = "¿Está seguro que quiere borrar este mensaje?";
$MESS["IM_M_HISTORY_LINES_VOTE"] = "Calificación del supervisor";
$MESS["IM_M_HISTORY_LINES_JOIN"] = "Abrir conversación";
$MESS["IM_M_SEND_FILE"] = "Enviando archivo";
$MESS["IM_M_SEND_MESSAGE"] = "Enviar";
$MESS["IM_M_AVATAR_UPLOAD"] = "Haga clic para cambiar el icono de chat";
$MESS["IM_M_AVATAR_UPLOAD_2"] = "Haga clic para cambiar el icono de llamada";
$MESS["IM_M_CONTACT_LIST"] = "Contactos";
$MESS["IM_M_USER_COLOR"] = "Color de su conversación en el chat móvil";
$MESS["IM_M_OPEN_DESKTOP_FROM_PANEL"] = "Ejecutar la aplicación en la barra de acceso rápido haga clic en";
$MESS["IM_M_DESKTOP_AUTORUN_ON"] = "Habilitar la ejecución automática al iniciar";
$MESS["IM_M_DESKTOP_BIG_SMILE_ON"] = "Use smails grandes";
$MESS["IM_M_RICH_LINK_ON"] = "Mostrar enlaces";
$MESS["IM_M_VIEW_LAST_MESSAGE_OFF"] = "Ocultar mensajes en la lista de \"Reciente\"";
$MESS["IM_M_VIEW_OFFLINE_OFF"] = "Ocultar usuarios fuera de línea";
$MESS["IM_M_VIEW_OL_LIST"] = "Mostrar Canales Abiertos por separado";
$MESS["IM_M_VIEW_OL_NEW"] = "Mostrar conversaciones de \"Esperar la primera respuesta\" en un grupo individual";
$MESS["IM_M_VIEW_OL_SORT"] = "Formato de la lista de Canales Abiertos";
$MESS["IM_M_VIEW_OL_SORT_1"] = "Primero: Sin respuesta";
$MESS["IM_M_VIEW_OL_SORT_1_TIP"] = "<b>Sin respuesta</b> - muestra las conversaciones que están pendientes de respuesta en la parte superior. Estas son las conversaciones que pueden haber sido vistas pero nunca respondidas.";
$MESS["IM_M_VIEW_OL_SORT_2"] = "Primero: conversaciones más antiguas";
$MESS["IM_M_VIEW_OL_SORT_2_TIP"] = "<b>Conversaciones más antiguas</b> - ordena las conversaciones por la primera fecha del mensaje inicial. Las conversaciones más recientes aparecen en la parte inferior de la lista.";
$MESS["IM_M_VIEW_OL_SORT_3"] = "Primero: respuestas más recientes";
$MESS["IM_M_VIEW_OL_SORT_3_TIP"] = "<b>Respuestas más recientes</b> - ordena las conversaciones por la fecha de la última respuesta.";
$MESS["IM_M_VIEW_GROUP_OFF"] = "Ocultar grupos de usuarios";
$MESS["IM_M_NOTIFY_AUTO_READ"] = "Desactivar la notificación de lectura automática";
$MESS["IM_M_ENABLE_SOUND"] = "Activar sonido";
$MESS["IM_M_ENABLE_BIRTHDAY"] = "Enviar notificaciones acerca de los cumpleaños";
$MESS["IM_M_ENABLE_ONLINE_NOTIFY"] = "Activar notificación \"usuario está en línea\"";
$MESS["IM_M_LLM"] = "Mostrar mensajes recientes";
$MESS["IM_M_LLN"] = "Mostrar notificaciones más recientes";
$MESS["IM_M_NAR"] = "Marcar automáticamente notificaciones como leídas";
$MESS["IM_M_KEY_SEND"] = "Enviar mensaje";
$MESS["IM_M_AUTO_CORRECT"] = "Habilitar corrección automática";
$MESS["IM_M_QUOTE_TITLE"] = "Haga clic para citar este mensaje";
$MESS["IM_M_OPEN_SETTINGS"] = "Editar configuración";
$MESS["IM_M_SEND_TYPE_TITLE"] = "Haga clic para cambiar el método abreviado del teclado";
$MESS["IM_M_ERROR"] = "Error de importación de datos";
$MESS["IM_SETTINGS"] = "Configuración";
$MESS["IM_FULLSCREEN"] = "Ir a la pantalla completa";
$MESS["IM_FULLSCREEN_EXIT"] = "Salir de la pantalla completa";
$MESS["IM_SETTINGS_SAVE"] = "Guardar";
$MESS["IM_SETTINGS_CLOSE"] = "Cerrar";
$MESS["IM_SETTINGS_WAIT"] = "Guardando...";
$MESS["IM_SETTINGS_LOAD"] = "Cargando la configuración...";
$MESS["IM_SETTINGS_COMMON"] = "General";
$MESS["IM_SETTINGS_SNOTIFY"] = "Desactivar notificaciones";
$MESS["IM_SETTINGS_SNOTIFY_ENABLE"] = "Permitir";
$MESS["IM_SETTINGS_NOTIFY"] = "Notificaciones";
$MESS["IM_SETTINGS_NOTIFY_SITE"] = "Sitio y<br>aplicaciones";
$MESS["IM_SETTINGS_NOTIFY_XMPP"] = "Cliente externo";
$MESS["IM_SETTINGS_NOTIFY_EMAIL"] = "Correo electrónico";
$MESS["IM_SETTINGS_NOTIFY_PUSH"] = "Notificaciones push";
$MESS["IM_SETTINGS_PRIVACY"] = "Privacidad";
$MESS["IM_SETTINGS_PRIVACY_CALL"] = "Aceptar llamadas de";
$MESS["IM_SETTINGS_PRIVACY_MESS"] = "Aceptar mensajes de";
$MESS["IM_SETTINGS_PRIVACY_CHAT"] = "Permitir invitaciones de chat de";
$MESS["IM_SETTINGS_PRIVACY_SEARCH"] = "Mostrar mi contacto en la búsqueda";
$MESS["IM_SETTINGS_PRIVACY_PROFILE"] = "Mostrar mi perfil completo";
$MESS["IM_SETTINGS_SELECT_1"] = "todos los usuarios";
$MESS["IM_SETTINGS_SELECT_2"] = "mis contactos";
$MESS["IM_SETTINGS_SELECT_3"] = "ninguno";
$MESS["IM_SETTINGS_SELECT_1_2"] = "todos los usuarios";
$MESS["IM_SETTINGS_SELECT_2_2"] = "mis contactos";
$MESS["IM_SETTINGS_SELECT_3_2"] = "ninguno";
$MESS["IM_SETTINGS_SELECT_1_3"] = "todos los usuarios";
$MESS["IM_SETTINGS_SELECT_2_3"] = "mis contactos";
$MESS["IM_SETTINGS_SELECT_3_3"] = "ninguno";
$MESS["IM_SETTINGS_NS_1"] = "Modo simple";
$MESS["IM_SETTINGS_NS_2"] = "Modo avanzado";
$MESS["IM_SETTINGS_NSL_1"] = "Habilitar sólo notificaciones importantes ";
$MESS["IM_SETTINGS_NSL_2"] = "Habilitar todas las notificaciones";
$MESS["IM_SETTINGS_NC_1_NEW"] = "Recibir notificaciones estándar para:";
$MESS["IM_SETTINGS_NC_2"] = "Sitio Web, aplicaciones móviles y de escritorio";
$MESS["IM_SETTINGS_NC_3"] = "Clientes externos (XMPP, Jabber)";
$MESS["IM_SETTINGS_NC_4"] = "Correo electrónico (#MAIL#)";
$MESS["IM_SETTINGS_NC_5"] = "Notificaciones push (aplicación móvil)";
$MESS["IM_SETTINGS_HARDWARE"] = "Hardware";
$MESS["IM_SETTINGS_HARDWARE_MICROPHONE"] = "Micrófono";
$MESS["IM_SETTINGS_HARDWARE_DEFAULT_MICROPHONE"] = "Por Defecto";
$MESS["IM_SETTINGS_HARDWARE_AUTO_PARAMETERS_MICROPHONE"] = "Ajuste automático de los parámetros del micrófono";
$MESS["IM_SETTINGS_HARDWARE_CAMERA"] = "Cámara";
$MESS["IM_SETTINGS_HARDWARE_SPEAKER"] = "Altavoz";
$MESS["IM_SETTINGS_HARDWARE_NOT_FOUND"] = "No se encontró hardware";
$MESS["IM_M_OPEN_CHAT"] = "Abrir chat";
$MESS["IM_M_WRITE_MESSAGE"] = "Enviar mensaje";
$MESS["IM_M_OPEN_HISTORY"] = "Historial de mensajes";
$MESS["IM_M_OPEN_HISTORY_2"] = "Historial del mensaje";
$MESS["IM_M_OPEN_PROFILE"] = "Perfil del usuario";
$MESS["IM_M_HIDE_CALL"] = "Eliminar llamada";
$MESS["IM_M_HIDE_DIALOG"] = "Ocultar";
$MESS["IM_M_HIDE_CHAT"] = "Ocultar chat";
$MESS["IM_M_CL_CHAT"] = "Mis chats";
$MESS["IM_M_OLD_REVISION"] = "Para continuar trabajando con el web messenger, por favor volver a cargar esta página.";
$MESS["IM_M_OLD_BROWSER"] = "El Web Messenger no soporta Internet Explorer 7 o versiones anteriores. Por favor actualice su navegador o instale la aplicación de escritorio.";
$MESS["IM_M_OLD_BROWSER_2"] = "Internet Explorer 10 y las versiones anteriores no son compatibles. Por favor, actualice su navegador o instale Desktop App.";
$MESS["IM_M_S_ON"] = "Opción habilitada";
$MESS["IM_M_S_OFF"] = "Opción deshabilitado";
$MESS["IM_M_CL_NOT_IN"] = "El usuario no está en la lista de contactos. El o ella tiene que agregar a sus amigos para enviar un mensaje.";
$MESS["IM_M_CL_NOT_IN_ADD"] = "Agregar amigos";
$MESS["IM_M_EMPTY"] = "Iniciar conversación seleccionando un contacto de la lista.";
$MESS["IM_M_SYSTEM_USER"] = "Mensaje del Sistema";
$MESS["IM_M_BIRTHDAY_MESSAGE"] = "¡Hoy es el cumpleaños de #USER_NAME#!";
$MESS["IM_M_CALL_EFP"] = "La llamada terminará si deja esta página!";
$MESS["IM_M_CALL_SCREEN"] = "Compartir pantalla";
$MESS["IM_M_CALL_SCREEN_ERROR"] = "El uso compartido de la pantalla sólo está disponible en la nueva versión de aplicación de escritorio de Bitrix24. Instalar ahora para poder utilizar esta función.";
$MESS["IM_M_CALL_VOICE"] = "Llamada de voz";
$MESS["IM_M_CALL_VIDEO"] = "Vídeo llamada";
$MESS["IM_M_CALL_CANCEL"] = "Finalizar llamada";
$MESS["IM_M_CALL_VIDEO_FROM"] = "Video llamada de #USER#";
$MESS["IM_M_CALL_VIDEO_TO"] = "Video llamada a #USER#";
$MESS["IM_CALL_GROUP_VIDEO_FROM"] = "Llamada grupal desde \"#CHAT#\"";
$MESS["IM_CALL_GROUP_VIDEO_TO"] = "Video llamada grupal con \"#CHAT#\"";
$MESS["IM_CALL_GROUP_VOICE_FROM"] = "Llamada grupal desde \"#CHAT#\"";
$MESS["IM_CALL_GROUP_VOICE_TO"] = "Llamada grupal con \"#CHAT#\"";
$MESS["IM_M_CALL_ST_NO_WEBRTC"] = "El usuario no puede recibir la llamada, porque él o ella está fuera de línea o su navegador no soporta llamadas.";
$MESS["IM_M_CALL_ST_NO_WEBRTC_1"] = "El usuario no puede aceptar la llamada: él/ella está fuera de línea o está en una aplicación que no admite llamadas";
$MESS["IM_M_CALL_ST_NO_WEBRTC_2"] = "Usted no puede recibir la llamada debido a que su navegador no soporta esta característica.";
$MESS["IM_M_CALL_ST_NO_WEBRTC_3"] = "No se puede aceptar llamadas, su aplicación no admite llamadas.";
$MESS["IM_M_CALL_ST_CALL_INIT"] = "Inicializar llamada";
$MESS["IM_M_CALL_ST_PHONE_NOTICE"] = "Inicializar la llamada telefónica";
$MESS["IM_M_CALL_ST_WAIT_PHONE"] = "Por favor, tome el teléfono para iniciar la llamada";
$MESS["IM_M_CALL_ST_WAIT_ACCESS"] = "Validar el acceso a los dispositivos de audio y vídeo";
$MESS["IM_M_CALL_ST_CON_ERROR"] = "No se puede hacer llamadas ya que el servidor multimedia es inaccesible.";
$MESS["IM_M_CALL_ST_NO_ACCESS_SSH"] = "No se puede compartir la pantalla porque el acceso a la pantalla no está disponible. Por favor, complete los pasos en las instrucciones.";
$MESS["IM_M_CALL_ST_RECALL"] = "Volver a marcar en #MINUTE# min.";
$MESS["IM_M_CALL_ST_ALFA"] = "Experimental";
$MESS["IM_M_CALL_ST_FINISHED"] = "Llamada finalizada";
$MESS["IM_M_CALL_BTN_PLUS"] = "Agregar usuario para llamar";
$MESS["IM_M_CALL_BTN_DOWNLOAD"] = "Descarga la aplicación de escritorio";
$MESS["IM_M_CALL_BTN_UPDATE"] = "Actualización";
$MESS["IM_M_CALL_BTN_RECALL_2"] = "en 5 minutos";
$MESS["IM_M_CALL_BTN_RECALL_3"] = "Llamar nuevamente";
$MESS["IM_M_CALL_BTN_HISTORY"] = "Historial";
$MESS["IM_M_CALL_BTN_HISTORY_2"] = "Registro de mensajes de usuario";
$MESS["IM_M_CALL_BTN_CHAT"] = "Chat";
$MESS["IM_M_CALL_BTN_CHAT_2"] = "Abrir chat con el usuario";
$MESS["IM_M_CALL_BTN_MAXI"] = "Maximizar ventana de llamada";
$MESS["IM_M_CALL_BTN_FULL"] = "Cambiar a pantalla completa";
$MESS["IM_M_CALL_BTN_SCREEN_TITLE"] = "Activar o desactivar la pantalla compartida";
$MESS["IM_M_CALL_BTN_DEVICE_TITLE"] = "Ahora llamando a través del teléfono. Haga clic para llamar a través del navegador o aplicación.";
$MESS["IM_M_CALL_BTN_DEVICE_OFF_TITLE"] = "Ahora llame a través del navegador o aplicación. Haga clic para llamar a través del teléfono.";
$MESS["IM_M_CALL_BTN_HOLD_TITLE"] = "Colocar en espera / Restaurar llamada";
$MESS["IM_M_CALL_TRANSFER_TEXT"] = "Redirigir llamada actual al usuario";
$MESS["IM_M_CALL_ALLOW_BTN"] = "Permitir";
$MESS["IM_M_CALL_ALLOW_TEXT"] = "Haga clic <b>Permitir</b><br>en la notificación de su navegador para establecer la conexión.
";
$MESS["IM_CALL_CHAT_LARGE"] = "No se puede hacer una llamada debido a que hay más de cuatro personas en el chat.";
$MESS["IM_CALL_GROUP_MAX_USERS"] = "No puedes invitar a más usuarios porque el número máximo de participantes es de 4.";
$MESS["IM_CALL_NO_WEBRT"] = "No se puede hacer una llamada debido a que su navegador no soporta esta característica.";
$MESS["IM_CALL_DESKTOP_2"] = "No se puede hacer una llamada porque su aplicación no soporta esta característica.";
$MESS["IM_CALL_MAGNIFY"] = "Ampliar el área de visualización";
$MESS["IM_PHONE_CALL_RECORD"] = "Llamar y grabar";
$MESS["IM_PHONE_CALL_NO_RECORD"] = "Llamada sin grabación";
$MESS["IM_PHONE_CALL"] = "Llamar";
$MESS["IM_PHONE_DIAL_CURRENT"] = "Llamada desde la negociación";
$MESS["IM_PHONE_PUT_NUMBER"] = "Ingrese el número";
$MESS["IM_PHONE_PUT_DIGIT"] = "Introduzca el dígito";
$MESS["IM_PHONE_ERROR_NA_PHONE"] = "El teléfono no está disponible, compruebe los parámetros de conexión.";
$MESS["IM_PHONE_OPEN_KEYPAD"] = "Mostrar teclado";
$MESS["IM_PHONE_DESC"] = "Llamada telefónica";
$MESS["IM_PHONE_WORK_PHONE"] = "Teléfono del Trabajo";
$MESS["IM_PHONE_PERSONAL_MOBILE"] = "Móvil";
$MESS["IM_PHONE_PERSONAL_PHONE"] = "Otro Teléfono";
$MESS["IM_PHONE_INNER_PHONE"] = "Ext.";
$MESS["IM_PHONE_RECALL"] = "Llamada telefónica";
$MESS["IM_PHONE_INNER_CALL"] = "Llamada interna";
$MESS["IM_PHONE_STATUS_HARDWARE"] = "Solicitar acceso de micrófono";
$MESS["IM_PHONE_STATUS_CONNECTING"] = "Conectando...";
$MESS["IM_PHONE_STATUS_CONNECTED"] = "Conexión establecida.";
$MESS["IM_PHONE_STATUS_ERROR_USE_HTTPS"] = "No se puede acceder al micrófono. La política de Google Chrome permite el acceso al micrófono sólo cuando se utiliza el protocolo HTTPS.";
$MESS["IM_PHONE_STATUS_ERROR_UNKNOWN"] = "Error desconocido.";
$MESS["IM_PHONE_TIMER_WITH_RECORD"] = "Grabación #MIN#:#SEC#";
$MESS["IM_PHONE_TIMER_WITHOUT_RECORD"] = "Longitud de la conversación: #MIN#:#SEC#";
$MESS["IM_M_CHAT_TITLE"] = "Invitar a usuarios a chatear";
$MESS["IM_M_MENTION_TITLE"] = "Mencionar usuario";
$MESS["IM_M_CHAT_SHOW_HISTORY"] = "Mostrar historial";
$MESS["IM_M_CHAT_BTN_JOIN"] = "Invitar";
$MESS["IM_M_CHAT_BTN_EDIT"] = "Editar";
$MESS["IM_M_CHAT_BTN_CANCEL"] = "Cancelar";
$MESS["IM_M_CHAT_MORE_USER"] = "y #USER_COUNT# más usuarios";
$MESS["IM_M_CHAT_RENAME"] = "Cambiar el nombre del chat";
$MESS["IM_M_CHAT_MUTE_ON"] = "Activar notificaciones";
$MESS["IM_M_CHAT_MUTE_OFF"] = "Desactivar notificaciones";
$MESS["IM_M_CHAT_MUTE_ON_2"] = "Haga clic para activar las notificaciones de chat";
$MESS["IM_M_CHAT_MUTE_OFF_2"] = "Haga clic para desactivar las notificaciones de chat";
$MESS["IM_M_USER_BLOCK_ON"] = "Haga clic para bloquear al usuario.";
$MESS["IM_M_USER_BLOCK_OFF"] = "Haga clic para desbloquear al usuario.";
$MESS["IM_M_CHAT_EXIT"] = "Salir del chat";
$MESS["IM_M_CHAT_KICK"] = "Quitar";
$MESS["IM_M_CHAT_PUT"] = "Mencionar al usuario";
$MESS["IM_M_CHAT_ERROR_1"] = "Por favor, seleccione los usuarios antes de crear un nuevo chat.";
$MESS["IM_NM_MESSAGE_1"] = "Nuevos mensajes (#COUNT#)";
$MESS["IM_NM_MESSAGE_2"] = "Tiene mensajes no leídos de #USERS#";
$MESS["IM_NM_NOTIFY_1"] = "Nueva notificación (#COUNT#)";
$MESS["IM_NM_NOTIFY_2"] = "Usted tiene notificaciones no leídas de #USERS#";
$MESS["IM_NM_NOTIFY_3"] = "Usted tiene notificaciones no leídas";
$MESS["IM_NOTIFY_DELETE_1"] = "Eliminar notificación";
$MESS["IM_NOTIFY_DELETE_2"] = "Desactivar estas notificaciones";
$MESS["IM_NOTIFY_DELETE_3"] = "Habilitar estas notificaciones";
$MESS["IM_NOTIFY_CENTER"] = "Centro de Notificaciones";
$MESS["IM_NOTIFY_NEW_MESSAGE"] = "Usted tiene una nueva notificación";
$MESS["IM_NOTIFY_NEW_MESSAGE_MORE"] = "Usted tiene #COUNT# nuevas notificaciones ";
$MESS["IM_PHONE_BUTTON_TITLE"] = "Teléfono";
$MESS["IM_NOTIFY_BUTTON_TITLE"] = "Notificaciones";
$MESS["IM_NOTIFY_WINDOW_TITLE"] = "Notificaciones";
$MESS["IM_NOTIFY_WINDOW_NEW_TITLE"] = "Nuevas notificaciones";
$MESS["IM_NOTIFY_GROUP_NOTIFY"] = "#USER_NAME# y #U_START##COUNT# más usuarios(s)#U_END#";
$MESS["IM_NOTIFY_EMPTY"] = "No hay ninguna notificación.";
$MESS["IM_NOTIFY_EMPTY_2"] = "No hay nuevas notificaciones.";
$MESS["IM_NOTIFY_EMPTY_3"] = "Ninguna notificación en los últimos 30 días";
$MESS["IM_NOTIFY_ERROR"] = "Error al cargar las notificaciones. Por favor, compruebe la configuración de red.";
$MESS["IM_NOTIFY_HISTORY"] = "Notificaciones vistas";
$MESS["IM_NOTIFY_HISTORY_2"] = "Cargando notificaciones...";
$MESS["IM_NOTIFY_HISTORY_LATE"] = "Más notificaciones";
$MESS["IM_NOTIFY_HISTORY_MORE"] = "Más";
$MESS["IM_NOTIFY_CONFIRM_CLOSE"] = "Cerrar";
$MESS["IM_NOTIFY_LOAD_NOTIFY"] = "Cargando notificaciones...";
$MESS["IM_MESSAGE_EDIT_TEXT"] = "Editar mensaje";
$MESS["IM_PANEL_FILTER_NAME"] = "Filtro:";
$MESS["IM_PANEL_FILTER_DATE"] = "Seleccione la fecha";
$MESS["IM_PANEL_FILTER_TEXT"] = "Ingresar texto de búsqueda";
$MESS["IM_PANEL_FILTER_ON"] = "Activar filtro";
$MESS["IM_PANEL_FILTER_OFF"] = "Desactivar filtro";
$MESS["IM_HISTORY_FILTER_NAME"] = "Buscar:";
$MESS["IM_HISTORY_FILTER_ON"] = "Habilitar búsqueda";
$MESS["IM_HISTORY_FILTER_OFF"] = "Deshabilitar búsqueda";
$MESS["IM_HISTORY_JUMP"] = "Abrir conversación";
$MESS["IM_HISTORY_RELATED"] = "Mostrar relacionados";
$MESS["IM_HISTORY_NEARBY"] = "mensajes siguientes y anteriores";
$MESS["IM_DESKTOP_B24_TITLE"] = "Bitrix24 para Windows";
$MESS["IM_DESKTOP_B24_OSX_TITLE"] = "Bitrix24 para OS X";
$MESS["IM_DESKTOP_B24_LINUX_TITLE"] = "Bitrix24 para Linux";
$MESS["IM_DESKTOP_OPEN_MESSENGER"] = "Mensaje(s) #COUNTER#";
$MESS["IM_DESKTOP_OPEN_NOTIFY"] = "Notificación(s) #COUNTER#";
$MESS["IM_DESKTOP_GO_SITE"] = "Flujo de actividad #COUNTER#";
$MESS["IM_DESKTOP_STATUS_OFFLINE"] = "Cerrar sesión";
$MESS["IM_DESKTOP_SETTINGS"] = "Configuraciones";
$MESS["IM_DESKTOP_LOGOUT"] = "Cambiar de usuario";
$MESS["IM_DESKTOP_BDISK"] = "Bitrix24.Drive";
$MESS["IM_DESKTOP_INSTALL"] = "Utilice \"#WM_NAME#\" de manera más eficaz mediante la instalación de la aplicación para #OS#";
$MESS["IM_DESKTOP_INSTALL_Y"] = "Instalar";
$MESS["IM_DESKTOP_INSTALL_N"] = "No, gracias";
$MESS["IM_DESKTOP_UNREAD_EMPTY"] = "No hay mensajes ni notificaciones nuevas";
$MESS["IM_DESKTOP_UNREAD_MESSAGES_NOTIFY"] = "Tiene nuevos mensajes y notificaciones";
$MESS["IM_DESKTOP_UNREAD_NOTIFY"] = "Tiene nuevas notificaciones";
$MESS["IM_DESKTOP_UNREAD_MESSAGES"] = "Tiene nuevos mensajes";
$MESS["IM_DESKTOP_UNREAD_LF"] = "Tiene mensajes no leídos en el Flujo de Actividad";
$MESS["IM_DESKTOP_NEED_UPDATE"] = "Está utilizando una versión antigua de la aplicación. Debe actualizarla para seguir trabajando.";
$MESS["IM_DESKTOP_NEED_UPDATE_BTN"] = "Actualización";
$MESS["IM_DESKTOP_RECONNECT"] = "Volver a conectarse";
$MESS["IM_SSH_TEXT"] = "Para mostrar su pantalla, usted necesita cambiar  <br> \"<i>enable-usermedia-screen-capture</i>\" la opción en las configuraciones del sistema. <br><br> Al hacerlo, usted necesitará abrir, en una nueva página, la dirección: <b>chrome://flags/#enable-usermedia-screen-capture</b>, y habilite la opción, luego vuelva a cargar su navegador.";
$MESS["IM_SSH_OK"] = "Continuar";
$MESS["IM_SSH_CANCEL"] = "Cancelar";
$MESS["IM_WM"] = "Web Messenger";
$MESS["IM_BC"] = "Mensajería Instantánea";
$MESS["IM_CNC"] = "Chat y Llamadas";
$MESS["IM_MENU_SUGGEST_EMPTY"] = "no hay sugerencias";
$MESS["IM_MENU_COPY"] = "Copiar";
$MESS["IM_MENU_COPY2"] = "Copiar mensaje";
$MESS["IM_MENU_ANSWER"] = "Responder";
$MESS["IM_MENU_QUOTE"] = "Citar selección";
$MESS["IM_MENU_QUOTE2"] = "Citar mensaje";
$MESS["IM_MENU_READ"] = "Marcar como leído";
$MESS["IM_MENU_UNREAD"] = "Marcar como no leído";
$MESS["IM_MENU_CUT"] = "Cortar";
$MESS["IM_MENU_PASTE"] = "Copiar";
$MESS["IM_MENU_DELETE"] = "Eliminar";
$MESS["IM_MENU_UNDO"] = "Deshacer";
$MESS["IM_MENU_REDO"] = "Rehacer";
$MESS["IM_MENU_COPY3"] = "Copiar URL";
$MESS["IM_MENU_COPY_FILE"] = "Copiar archivo";
$MESS["IM_MENU_EDIT"] = "Editar mensaje";
$MESS["IM_TIP_OL_SYSTEM"] = "Su contacto no ve los mensajes marcados con un icono de candado.";
$MESS["IM_TIP_WEBRTC_ON"] = "El modo de depuración de llamada de video es <b>on</b>.";
$MESS["IM_TIP_WEBRTC_OFF"] = "El modo de depuración de llamada de video es <b>off</b>.";
$MESS["IM_TIP_AC_ON"] = "Autocorrección del texto es <b>on</b>.";
$MESS["IM_TIP_AC_OFF"] = "Autocorrección del texto es <b>on</b>.";
$MESS["IM_F_ERR_NC"] = "Las operaciones de archivo no están disponibles hasta que se crea un chat";
$MESS["IM_F_FILE_SEARCH"] = "Introduzca el nombre del archivo";
$MESS["IM_F_NO_FILES"] = "No cargar archivos";
$MESS["IM_F_NO_FILES_2"] = "No hay archivos";
$MESS["IM_F_LOAD_FILES"] = "Cargando archivos...";
$MESS["IM_F_MENU"] = "Abrir menú de acción";
$MESS["IM_F_UPLOAD_MENU_1"] = "Seleccione en la computadora";
$MESS["IM_F_UPLOAD_MENU_1_M"] = "Seleccionar en mi dispositivo";
$MESS["IM_F_UPLOAD_MENU_2"] = "Seleccionar en My Drive";
$MESS["IM_F_UPLOAD_MENU"] = "Haga clic aquí para cargar el archivo";
$MESS["IM_F_SAVE_OK"] = "El archivo se ha guardado en el Drive.";
$MESS["IM_F_SAVE_ERR"] = "Error al guardar el archivo. Inténtalo de nuevo más tarde.";
$MESS["IM_CS_ONLINE"] = "La conexión con el servidor se ha establecido correctamente";
$MESS["IM_CS_OFFLINE"] = "No se pudo conectar con el servidor";
$MESS["IM_CS_CONNECTING"] = "Conexión con el servidor";
$MESS["IM_CS_RELOAD"] = "Volver a cargar";
$MESS["IM_SAVING"] = "Guardando...";
$MESS["IM_DELETING"] = "Eliminando...";
$MESS["IM_CHAT_ID_IS"] = "ID del chat: #CHAT_ID#";
$MESS["IM_M_TA_TEXT"] = "Ingresar mensaje";
$MESS["IM_P_LINK"] = "Enlace:";
$MESS["IM_P_OPEN"] = "Descargar archivo original";
$MESS["IM_P_CLOSE"] = "Cerrar";
$MESS["IM_PF_TITLE"] = "Archivo";
$MESS["IM_PM_TITLE"] = "Se te ha mencionado";
$MESS["IM_COMMAND_TITLE"] = "Comandos";
$MESS["IM_COMMAND_H_1"] = "Tab o &uarr;&darr; para navegar";
$MESS["IM_COMMAND_H_2"] = "Enter o barra espaciadora para seleccionar";
$MESS["IM_COMMAND_H_3"] = "Esc para cancelar";
$MESS["IM_OL_TRANSFER_TEXT"] = "Transferir la conversación actual";
$MESS["IM_CALL_ERROR_HARDWARE"] = "Erro: No se puede acceder a la cámara ni al micrófono.";
$MESS["IM_CALL_ERROR_CONNECTION"] = "Error al conectarse con el servidor de medios:";
$MESS["IM_CALL_HARDWARE_REQUEST"] = "Intentando acceder a la cámara y al micrófono";
$MESS["IM_CALL_ACCESS_GRANTED"] = "Acceso permitido.";
$MESS["IM_CALL_USER_DISCONNECTED"] = "desconectado";
$MESS["IM_CALL_USER_CONNECTED"] = "conectado";
$MESS["IM_CALL_CONNECTING"] = "Conectando al servidor multimedia";
$MESS["IM_CALL_CONNECTED"] = "Conexión establecida.";
$MESS["IM_CALL_PUBLISHING"] = "Publicación de flujo de medios";
$MESS["IM_CALL_PUBLISHING_STOPPED"] = "La publicación de secuencias de medios se canceló.";
$MESS["IM_CALL_PUBLISHING_ERROR"] = "Error al publicar flujo de medios.";
$MESS["IM_CALL_WAITING_CONNECT"] = "Esperando a otros para la conferencia en...";
$MESS["IM_CALL_HARDWARE_MIC"] = "Micrófono";
$MESS["IM_CALL_HARDWARE_CAMERA"] = "Cámara";
$MESS["IM_CALL_HARDWARE_RESOLUTION"] = "Resolución";
$MESS["IM_CALL_EXPERIMENTAL_BUTTON"] = "Conectar";
$MESS["IM_CALL_MIC_TEST_BUTTON"] = "Prueba de Micrófono";
$MESS["IM_CALL_MIC_TEST_RECORD_START"] = "Iniciar Registro";
$MESS["IM_CALL_MIC_TEST_RECORD_STOP"] = "Detener Registro";
$MESS["IM_CALL_MIC_TEST_PLAY_START"] = "Iniciar Playback";
$MESS["IM_CALL_MIC_TEST_PLAY_STOP"] = "Parar Playback";
$MESS["IM_CALL_MIC_TEST_CLOSE"] = "Cerrar";
$MESS["IM_OL_VOTE_TEXT"] = "Evalúa nuestro servicio";
$MESS["IM_OL_VOTE_TNX"] = "Gracias!";
$MESS["IM_OL_VOTE_LIKE"] = "Me gusta";
$MESS["IM_OL_VOTE_DISLIKE"] = "Ya no me gusta";
?>