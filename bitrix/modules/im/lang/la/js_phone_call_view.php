<?
$MESS["IM_PHONE_CALL_VIEW_CALL_LIST_TITLE"] = "Lista de llamadas en frío";
$MESS["IM_PHONE_CALL_VIEW_WEBFORM_TITLE"] = "Formulario";
$MESS["IM_PHONE_CALL_VIEW_CALL_LIST_DEFER_15_MIN"] = "Suspender durante 15 min.";
$MESS["IM_PHONE_CALL_VIEW_CALL_LIST_DEFER_HOUR"] = "Suspender durante una hora";
$MESS["IM_PHONE_CALL_VIEW_CALL_LIST_TO_END"] = "Fin de la lista";
$MESS["IM_PHONE_CALL_VIEW_FOLDED_CALL_LIST_TO_NUMBER"] = "Número de llamada";
$MESS["IM_PHONE_CALL_VIEW_FOLDED_BUTTON_CALL"] = "Llamada";
$MESS["IM_PHONE_CALL_VIEW_FOLDED_BUTTON_NEXT"] = "Siguiente";
$MESS["IM_PHONE_CALL_LIST_MORE"] = "#COUNT# más";
$MESS["IM_PHONE_CALL_VIEW_NUMBER_UNKNOWN"] = "No se especifica ningún número";
$MESS["IM_PHONE_CALL_VIEW_MORE"] = "Más";
$MESS["IM_PHONE_CALL_VIEW_RATE_QUALITY"] = "Califique la calidad de la conexión";
$MESS["IM_PHONE_CALL_VIEW_DONT_LEAVE"] = "Dejando la página terminará la llamada";
$MESS["IM_PHONE_CALL_VIEW_FOLD"] = "Minimizar";
$MESS["IM_PHONE_CALL_VIEW_UNFOLD"] = "Restaurar";
$MESS["IM_PHONE_CALL_VIEW_INTERCEPT"] = "Interceptar";
$MESS["IM_PHONE_CALL_VIEW_SAVE"] = "Guardar";
$MESS["IM_PHONE_CALL_COMMENT_PLACEHOLDER"] = "Ingrese el texto del comentario";
?>