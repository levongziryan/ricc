<?
$MESS["CHAT_ENTITY_ID_FIELD"] = "ID";
$MESS["CHAT_ENTITY_AUTHOR_ID_FIELD"] = "ID d'auteur du chat";
$MESS["CHAT_ENTITY_ENTITY_ID_FIELD"] = "ID de l'élément de l'entité";
$MESS["CHAT_ENTITY_TITLE_FIELD"] = "Titre du chat";
$MESS["CHAT_ENTITY_CALL_NUMBER_FIELD"] = "Chiffre";
$MESS["CHAT_ENTITY_ENTITY_TYPE_FIELD"] = "Bloc Highload";
$MESS["CHAT_ENTITY_CALL_TYPE_FIELD"] = "Type d'appel";
$MESS["CHAT_ENTITY_TYPE_FIELD"] = "Type de chat";
$MESS["CHAT_ENTITY_EXTRANET_FIELD"] = "Extranet";
$MESS["CHAT_ENTITY_COLOR_FIELD"] = "Couleur du chat";
$MESS["CHAT_ENTITY_DESCRIPTION_FIELD"] = "Description du chat";
?>