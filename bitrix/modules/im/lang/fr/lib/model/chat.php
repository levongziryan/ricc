<?
$MESS["CHAT_ENTITY_ID_FIELD"] = "ID";
$MESS["CHAT_ENTITY_TITLE_FIELD"] = "Titre du chat";
$MESS["CHAT_ENTITY_AUTHOR_ID_FIELD"] = "ID du créateur du chat";
$MESS["CHAT_ENTITY_CALL_TYPE_FIELD"] = "Type d'appel";
$MESS["CHAT_ENTITY_CALL_NUMBER_FIELD"] = "Numéro";
$MESS["CHAT_ENTITY_ENTITY_TYPE_FIELD"] = "Entité";
$MESS["CHAT_ENTITY_ENTITY_ID_FIELD"] = "ID d'entité";
$MESS["CHAT_ENTITY_TYPE_FIELD"] = "Type de chat";
$MESS["CHAT_ENTITY_EXTRANET_FIELD"] = "Extranet";
$MESS["CHAT_ENTITY_COLOR_FIELD"] = "Couleur du chat";
$MESS["CHAT_ENTITY_DESCRIPTION_FIELD"] = "Description du chat";
$MESS["CHAT_ENTITY_ENTITY_DATA_1_FIELD"] = "Données pour entité 1";
$MESS["CHAT_ENTITY_ENTITY_DATA_2_FIELD"] = "Données pour entité 2";
$MESS["CHAT_ENTITY_ENTITY_DATA_3_FIELD"] = "Données pour entité 3";
?>