<?
$MESS["COMMAND_ENTITY_ID_FIELD"] = "ID de commande";
$MESS["COMMAND_ENTITY_BOT_ID_FIELD"] = "ID du chat bot";
$MESS["COMMAND_ENTITY_COMMAND_FIELD"] = "Commande";
$MESS["COMMAND_ENTITY_HIDDEN_FIELD"] = "Commande masquer le statut";
$MESS["COMMAND_ENTITY_SONET_SUPPORT_FIELD"] = "Statut du support du Flux d'activité";
$MESS["COMMAND_ENTITY_CLASS_FIELD"] = "Classe de handler de commande";
$MESS["COMMAND_ENTITY_METHOD_COMMAND_ADD_FIELD"] = "Méthode de classe de handler pour les commandes entrantes";
$MESS["COMMAND_ENTITY_METHOD_LANG_GET_FIELD"] = "Méthode de classe pour recevoir les localisations";
$MESS["COMMAND_ENTITY_APP_ID_FIELD"] = "ID d'app REST";
$MESS["COMMAND_ENTITY_MODULE_ID_FIELD"] = "ID du module";
$MESS["COMMAND_ENTITY_COMMON_FIELD"] = "Statut global de commande";
?>