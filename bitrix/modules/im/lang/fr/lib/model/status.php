<?
$MESS["STATUS_ENTITY_USER_ID_FIELD"] = "ID de l'utilisateur";
$MESS["STATUS_ENTITY_STATUS_FIELD"] = "Statut";
$MESS["STATUS_ENTITY_IDLE_FIELD"] = "Temps d'inactivité";
$MESS["STATUS_ENTITY_DESKTOP_LAST_DATE_FIELD"] = "Dernière activité enregistré sur (bureau)";
$MESS["STATUS_ENTITY_MOBILE_LAST_DATE_FIELD"] = "Dernière activité enregistré sur (application mobile)";
$MESS["STATUS_ENTITY_EVENT_ID_FIELD"] = "ID événement";
$MESS["STATUS_ENTITY_EVENT_UNTIL_DATE_FIELD"] = "L'événement valide jusqu'à";
$MESS["STATUS_ENTITY_COLOR_FIELD"] = "User défini";
?>