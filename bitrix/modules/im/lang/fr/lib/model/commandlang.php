<?
$MESS["COMMAND_LANG_ENTITY_ID_FIELD"] = "ID du message localisé";
$MESS["COMMAND_LANG_ENTITY_COMMAND_ID_FIELD"] = "ID de la commande";
$MESS["COMMAND_LANG_ENTITY_LANGUAGE_ID_FIELD"] = "ID de la langue";
$MESS["COMMAND_LANG_ENTITY_TITLE_FIELD"] = "Description de la commande";
$MESS["COMMAND_LANG_ENTITY_PARAMS_FIELD"] = "Descriptions des paramètres de la commande";
?>