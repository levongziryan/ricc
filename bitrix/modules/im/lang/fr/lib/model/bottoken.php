<?
$MESS["BOT_TOKEN_ENTITY_ID_FIELD"] = "ID du jeton de permission de publier";
$MESS["BOT_TOKEN_ENTITY_TOKEN_FIELD"] = "Jeton de permission";
$MESS["BOT_TOKEN_ENTITY_DATE_CREATE_FIELD"] = "Date de création";
$MESS["BOT_TOKEN_ENTITY_DATE_EXPIRE_FIELD"] = "Date d'expiration";
$MESS["BOT_TOKEN_ENTITY_BOT_ID_FIELD"] = "ID du robot";
$MESS["BOT_TOKEN_ENTITY_DIALOG_ID_FIELD"] = "ID du dialogue";
?>