<?
$MESS["BOT_ENTITY_BOT_ID_FIELD"] = "ID du bot";
$MESS["BOT_ENTITY_MODULE_ID_FIELD"] = "ID du module";
$MESS["BOT_ENTITY_TO_CLASS_FIELD"] = "Classe de handler du bot";
$MESS["BOT_ENTITY_TO_METHOD_FIELD"] = "Méthode de classe de handler du bot";
$MESS["BOT_ENTITY_APP_ID_FIELD"] = "ID d'app REST";
$MESS["BOT_ENTITY_BOT_TYPE_FIELD"] = "Type de bot";
$MESS["BOT_ENTITY_METHOD_MESSAGE_ADD_FIELD"] = "Méthode de classe du handler message entrant";
$MESS["BOT_ENTITY_METHOD_WELCOME_MESSAGE_FIELD"] = "Méthode de classe du handler message de bienvenue";
$MESS["BOT_ENTITY_METHOD_BOT_DELETE_FIELD"] = "Méthode de classe du handler suppression du bot";
$MESS["BOT_ENTITY_COUNT_MESSAGE_FIELD"] = "Compteur de messages traités";
$MESS["BOT_ENTITY_COUNT_SLASH_FIELD"] = "Compteur de commandes traitées";
$MESS["BOT_ENTITY_COUNT_CHAT_FIELD"] = "Compteur d'invitations au chat de groupe";
$MESS["BOT_ENTITY_COUNT_USER_FIELD"] = "Compteur d'invitations au chat public";
$MESS["BOT_ENTITY_LANGUAGE_FIELD"] = "Langue du bot";
$MESS["BOT_ENTITY_TEXT_PRIVATE_WELCOME_MESSAGE_FIELD"] = "Texte du message de bienvenue";
$MESS["BOT_ENTITY_TEXT_CHAT_WELCOME_MESSAGE_FIELD"] = "Méthode de classe pour ajouter un message de bienvenue (chat de groupe)";
$MESS["BOT_ENTITY_VERIFIED_FIELD"] = "Drapeau d'identification";
$MESS["BOT_ENTITY_OPENLINE_FIELD"] = "Drapeau de support du Canal ouvert";
$MESS["BOT_ENTITY_METHOD_MESSAGE_UPDATE_FIELD"] = "Gestionnaire des mises à jour des messages (méthode de classe)";
$MESS["BOT_ENTITY_METHOD_MESSAGE_DELETE_FIELD"] = "Gestionnaire des suppressions des messages (méthode de classe)";
?>