<?
$MESS["STATUS_ENTITY_USER_ID_FIELD"] = "Identifiant de l'utilisateur";
$MESS["STATUS_ENTITY_EVENT_ID_FIELD"] = "Identificateur de l'événement";
$MESS["STATUS_ENTITY_STATUS_FIELD"] = "Statut";
$MESS["STATUS_ENTITY_IDLE_FIELD"] = "Temps d'inactivité";
$MESS["STATUS_ENTITY_DESKTOP_LAST_DATE_FIELD"] = "Dernière activité enregistré sur (bureau)";
$MESS["STATUS_ENTITY_MOBILE_LAST_DATE_FIELD"] = "Dernière activité enregistré sur (application mobile)";
$MESS["STATUS_ENTITY_EVENT_UNTIL_DATE_FIELD"] = "L'événement valide jusqu'à ce que";
$MESS["STATUS_ENTITY_COLOR_FIELD"] = "Utilisateur défini";
?>