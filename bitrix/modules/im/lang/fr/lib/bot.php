<?
$MESS["BOT_ENTITY_BOT_ID_FIELD"] = "ID du bot";
$MESS["BOT_ENTITY_MODULE_ID_FIELD"] = "ID du module";
$MESS["BOT_ENTITY_TO_CLASS_FIELD"] = "Classe du gestionnaire de bot";
$MESS["BOT_ENTITY_TO_METHOD_FIELD"] = "Méthode de la classe du gestionnaire de bot";
$MESS["BOT_ENTITY_APP_ID_FIELD"] = "ID de l'appli REST";
$MESS["BOT_ENTITY_BOT_TYPE_FIELD"] = "Type de bot";
$MESS["BOT_ENTITY_METHOD_MESSAGE_ADD_FIELD"] = "Méthode de la classe du gestionnaire de messages entrants";
$MESS["BOT_ENTITY_METHOD_WELCOME_MESSAGE_FIELD"] = "Méthode de la classe du gestionnaire de messages de bienvenue";
$MESS["BOT_DEFAULT_WORK_POSITION"] = "Bot de chat";
$MESS["BOT_ENTITY_METHOD_BOT_DELETE_FIELD"] = "Méthode de la classe du gestionnaire de la suppression de bot";
$MESS["BOT_MESSAGE_INSTALL_SYSTEM"] = "Un bot de chat a été ajouté à votre compte Bitrix24";
$MESS["BOT_MESSAGE_INSTALL_USER"] = "#USER_NAME# a ajouté un nouveau bot de chat";
$MESS["BOT_MESSAGE_INSTALL_USER_F"] = "#USER_NAME# a ajouté un nouveau bot de chat";
$MESS["BOT_ENTITY_COUNT_MESSAGE_FIELD"] = "Décompte des messages traités";
$MESS["BOT_ENTITY_COUNT_SLASH_FIELD"] = "Décompte des commandes traitées";
$MESS["BOT_ENTITY_COUNT_CHAT_FIELD"] = "Décompte des invitations au chat de groupe";
$MESS["BOT_ENTITY_COUNT_USER_FIELD"] = "Décompte des invitations au chat public";
$MESS["BOT_MESSAGE_FROM"] = "Chat Bot #BOT_NAME#";
?>