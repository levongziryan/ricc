<?
$MESS["IM_COLOR_RED"] = "Rouge";
$MESS["IM_COLOR_ORANGE"] = "Orange";
$MESS["IM_COLOR_BROWN"] = "Marron";
$MESS["IM_COLOR_GREEN"] = "Vert";
$MESS["IM_COLOR_LIGHT_BLUE"] = "Bleu ciel";
$MESS["IM_COLOR_DARK_BLUE"] = "Bleu";
$MESS["IM_COLOR_PURPLE"] = "Violet";
$MESS["IM_COLOR_AQUA"] = "Aqua";
$MESS["IM_COLOR_PINK"] = "Rose";
$MESS["IM_COLOR_LIME"] = "Citron vert";
$MESS["IM_COLOR_GRAY"] = "Gris";
$MESS["IM_COLOR_MINT"] = "Menthe";
$MESS["IM_COLOR_AZURE"] = "Azure";
$MESS["IM_COLOR_KHAKI"] = "Kaki";
$MESS["IM_COLOR_SAND"] = "Sable";
$MESS["IM_COLOR_MARENGO"] = "Mangue";
$MESS["IM_COLOR_GRAPHITE"] = "Graphite";
?>