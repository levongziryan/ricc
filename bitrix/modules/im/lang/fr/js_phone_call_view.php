<?
$MESS["IM_PHONE_CALL_VIEW_CALL_LIST_TITLE"] = "Liste d’appels à froid";
$MESS["IM_PHONE_CALL_VIEW_WEBFORM_TITLE"] = "Formulaire";
$MESS["IM_PHONE_CALL_VIEW_CALL_LIST_DEFER_15_MIN"] = "Suspendre pour 15 min.";
$MESS["IM_PHONE_CALL_VIEW_CALL_LIST_DEFER_HOUR"] = "Suspendre pour une heure";
$MESS["IM_PHONE_CALL_VIEW_CALL_LIST_TO_END"] = "Fin de la liste";
$MESS["IM_PHONE_CALL_VIEW_FOLDED_CALL_LIST_TO_NUMBER"] = "Numéro d'appel";
$MESS["IM_PHONE_CALL_VIEW_FOLDED_BUTTON_CALL"] = "Appel";
$MESS["IM_PHONE_CALL_VIEW_FOLDED_BUTTON_NEXT"] = "Suivant";
$MESS["IM_PHONE_CALL_LIST_MORE"] = "#COUNT# en plus";
$MESS["IM_PHONE_CALL_VIEW_NUMBER_UNKNOWN"] = "Aucun numéro spécifié";
$MESS["IM_PHONE_CALL_VIEW_MORE"] = "Plus";
$MESS["IM_PHONE_CALL_VIEW_RATE_QUALITY"] = "Évaluer la qualité de la connexion";
$MESS["IM_PHONE_CALL_VIEW_DONT_LEAVE"] = "Quitter la page va mettre fin à l’appel";
$MESS["IM_PHONE_CALL_VIEW_FOLD"] = "Minimiser";
$MESS["IM_PHONE_CALL_VIEW_UNFOLD"] = "Restaurer";
$MESS["IM_PHONE_CALL_VIEW_INTERCEPT"] = "Intercepter";
$MESS["IM_PHONE_CALL_VIEW_SAVE"] = "Enregistrer";
$MESS["IM_PHONE_CALL_COMMENT_PLACEHOLDER"] = "Entrer le texte du commentaire";
?>