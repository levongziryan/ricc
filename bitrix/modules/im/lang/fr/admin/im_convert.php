<?
$MESS["IM_CONVERT_COMPLETE_ALL_OK"] = "Tous les fichiers ont été convertis avec succès, aucune conversion n'est plus nécessaire.";
$MESS["IM_CONVERT_TAB"] = "Conversion";
$MESS["IM_CONVERT_COMPLETE"] = "Conversion est terminée";
$MESS["IM_CONVERT_TITLE"] = "Conversion de l'historique de messages";
$MESS["IM_CONVERT_IN_PROGRESS"] = "Conversion...";
$MESS["IM_CONVERT_STEP"] = "Date maximale:";
$MESS["IM_CONVERT_ABOUT_TIME_MINUTE"] = "minutes";
$MESS["IM_CONVERT_BUTTON"] = "Commencer la conversion";
$MESS["IM_CONVERT_STOP"] = "Arrêter";
$MESS["IM_CONVERT_TAB_TITLE"] = "Paramètres de la conversion";
$MESS["IM_CONVERT_ABOUT_TIME"] = "environ";
$MESS["IM_CONVERT_STEP_sec"] = "(sec.)";
$MESS["IM_CONVERT_ABOUT_TIME_SEC"] = "secondes";
$MESS["IM_CONVERT_TOTAL"] = "Nombre de messages convertis:";
$MESS["IM_CONVERT_NEXT_STEP"] = "L'étape suivante";
?>