<?
$MESS["IM_NS_DEFAULT"] = "Notifications non reconnues";
$MESS["IM_NS_MAIN"] = "Ratings, J'aime";
$MESS["IM_NS_MAIN_RATING_VOTE"] = "Notification de vote (j'aime)";
$MESS["IM_NS_BIZPROC_ACTIVITY"] = "Notifications depuis les processus d'affaires";
$MESS["IM_NS_MAIN_RATING_VOTE_MENTIONED"] = "Notification relative vote dans les messages que vous mentionnez";
$MESS["IM_NS_IM"] = "Chat et appels";
$MESS["IM_NS_LIKE"] = "Chat likes";
$MESS["IM_NS_MENTION"] = "On a parlé de vous dans un chat public";
$MESS["IM_NS_OPEN"] = "Messages de chats publics";
$MESS["IM_NS_MESSAGE_2"] = "Messages de <nobr>chat tête-à-tête</nobr>";
$MESS["IM_NS_CHAT_2"] = "Messages de chats privés";
$MESS["IM_NS_MESSAGE"] = "messages";
$MESS["IM_NS_CHAT"] = "Messages de chat privé";
?>