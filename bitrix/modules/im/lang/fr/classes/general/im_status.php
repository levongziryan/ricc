<?
$MESS["IM_STATUS_ONLINE"] = "En ligne";
$MESS["IM_STATUS_MOBILE"] = "Application mobile";
$MESS["IM_STATUS_IDLE"] = "Inactif";
$MESS["IM_STATUS_DND"] = "Ne pas déranger";
$MESS["IM_STATUS_AWAY"] = "Absent";
$MESS["IM_STATUS_VACATION"] = "En congé";
$MESS["IM_STATUS_EAID_BOT"] = "Chat Bot";
$MESS["IM_STATUS_EAID_EMAIL"] = "Utilisateur d'adresse e-mail";
$MESS["IM_STATUS_EAID_NETWORK"] = "Bitrix24.Network";
$MESS["IM_STATUS_EAID_REPLICA"] = "Bitrix24.Network";
$MESS["IM_STATUS_EAID_CONTROLLER"] = "Support technique";
$MESS["IM_STATUS_EAID_IMCONNECTOR"] = "Utilisateur du Canal ouvert";
?>