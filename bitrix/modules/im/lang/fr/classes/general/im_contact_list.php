<?
$MESS["IM_CL_GROUP_LAST"] = "Contacts récents";
$MESS["IM_CL_GROUP_OTHER"] = "En dehors de la structure";
$MESS["IM_CL_GROUP_OTHER_2"] = "Hors liste";
$MESS["IM_CL_GROUP_ALL"] = "Listes des contacts";
$MESS["IM_CL_GROUP_CHATS"] = "Chats de groupe";
$MESS["IM_CL_GROUP_EXTRANET"] = "Extranet";
$MESS["IM_CL_GROUP_FRIENDS"] = "Amis";
$MESS["IM_CL_GROUP_SG"] = "Extranet:";
$MESS["IM_CL_SEARCH_EMPTY"] = "Ligne de recherche non renseignée.";
$MESS["IM_QUOTE"] = "Offre";
$MESS["IM_FILE"] = "Fichier";
$MESS["IM_CL_GROUP_SEARCH"] = "Résultats de la recherche";
$MESS["IM_SEARCH_OL"] = "Canal ouvert";
?>