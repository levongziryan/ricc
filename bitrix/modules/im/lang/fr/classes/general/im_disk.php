<?
$MESS["IM_DISK_AVATAR_CHANGE_M"] = "#USER_NAME# changé l'icône de chat";
$MESS["IM_DISK_AVATAR_CHANGE_F"] = "#USER_NAME# changé l'icône de chat";
$MESS["IM_DISK_LOCAL_FOLDER_B24_TITLE"] = "imessenger";
$MESS["IM_DISK_LOCAL_FOLDER_TITLE"] = "imessenger";
$MESS["IM_DISK_ERR_UPLOAD"] = "Vous ne pouvez pas télécharger des fichiers sur ce chat.";
$MESS["IM_DISK_ERR_AVATAR_1"] = "Vous ne pouvez pas changer votre avatar dans ce chat.";
$MESS["IM_DISK_STORAGE_TITLE"] = "Stockage Instant Messenger";
?>