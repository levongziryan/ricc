<?
$MESS["IM_MESSAGE_FORMAT_TIME"] = "g:i a";
$MESS["IM_MESSAGE_FORMAT_DATE"] = "g:i a, d F Y";
$MESS["IM_ERROR_EMPTY_USER_ID"] = "Identifiant de l'utilisateur non renseigné";
$MESS["IM_ERROR_EMPTY_FROM_USER_ID"] = "Expéditeur du message n'est pas indiqué.";
$MESS["IM_ERROR_EMPTY_TO_USER_ID"] = "Destinataire du message non renseigné.";
$MESS["IM_ERROR_EMPTY_MESSAGE"] = "Le texte du message n'est pas indiqué.";
?>