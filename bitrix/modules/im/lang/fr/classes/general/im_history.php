<?
$MESS["IM_HISTORY_FORMAT_TIME"] = "g:i a";
$MESS["IM_HISTORY_FORMAT_DATE"] = "g:i a, d F Y";
$MESS["IM_HISTORY_SEARCH_EMPTY"] = "Ligne de recherche non renseignée.";
$MESS["IM_HISTORY_ERROR_MESSAGE_ID"] = "Ligne de recherche non renseignée.";
$MESS["IM_HISTORY_ERROR_TO_USER_ID"] = "ID de l'utilisateur non renseigné";
$MESS["IM_HISTORY_SEARCH_DATE_EMPTY"] = "Aucune date dans lequel rechercher des messages a été spécifié.";
?>