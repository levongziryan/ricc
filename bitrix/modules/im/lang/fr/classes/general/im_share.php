<?
$MESS["IM_SHARE_CHAT"] = "Chat";
$MESS["IM_SHARE_FILE"] = "Fichier";
$MESS["IM_SHARE_POST_WELCOME"] = "Discutons :)";
$MESS["IM_SHARE_CHAT_TASK"] = "Une [URL=#LINK#]tâche[/URL] a été créée à partir du message.";
$MESS["IM_SHARE_CHAT_POST"] = "Une [URL=#LINK#]publication[/URL] sur le flux d'activité a été créée à partir du message.";
$MESS["IM_SHARE_CHAT_CALEND"] = "Un [URL=#LINK#]événement de calendrier[/URL] a été créé à partir de l'événement.";
?>