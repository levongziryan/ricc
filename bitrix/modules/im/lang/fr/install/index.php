<?
$MESS["IM_MODULE_NAME"] = "Chat d'affaires";
$MESS["IM_MODULE_DESCRIPTION"] = "Module de messagerie et notifications instantanées.";
$MESS["IM_INSTALL_TITLE"] = "Installation de module 'Messager Web'";
$MESS["IM_UNINSTALL_TITLE"] = "Elimination du module 'WEB Messenger'";
$MESS["IM_CONVERT_MESSAGE"] = "Vous avez installé le module 'Web-Messenger'. Pour travailler avec l'historique des messages, vous devez effectuer #A_TAG_START#la conversion des données#A_TAG_END#.";
?>