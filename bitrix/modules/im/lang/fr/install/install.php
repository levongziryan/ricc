<?
$MESS["IM_MODULE_NAME"] = "Messagerie instantanée";
$MESS["IM_MODULE_DESCRIPTION"] = "Fournit la prise en charge des services de messagerie instantanée et des notifications.";
$MESS["IM_INSTALL_TITLE"] = "Installation du module de messagerie instantanée";
$MESS["IM_UNINSTALL_TITLE"] = "Désinstallation du module de messagerie instantanée";
?>