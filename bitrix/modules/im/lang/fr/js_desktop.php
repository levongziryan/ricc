<?
$MESS["BXD_DEFAULT_TITLE"] = "Bitrix: Application de bureau (Version: #VERSION#)";
$MESS["BXD_NEED_UPDATE"] = "La version de votre application est très vieille, pour le travail ultérieur il est nécessaire d'installer la nouvelle version.";
$MESS["BXD_RECONNECT"] = "Reconnexion";
$MESS["BXD_CONFIRM_CLOSE"] = "Fermer";
$MESS["BXD_NEED_UPDATE_BTN"] = "Mettre à jour";
$MESS["BXD_LOGOUT"] = "Changer l'utilisateur";
$MESS["BXD_QUOTE_BLOCK"] = "Offre";
?>