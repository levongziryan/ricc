<?
$MESS["IM_STATUS_ONLINE"] = "En ligne";
$MESS["IM_STATUS_OFFLINE"] = "Désactivé";
$MESS["IM_STATUS_DND"] = "Occupé";
$MESS["IM_MESSENGER_NEW_MESSAGE"] = "Nouveau message";
$MESS["IM_MESSENGER_NO_MESSAGE"] = "Aucun message";
$MESS["IM_MESSENGER_HISTORY"] = "Historique des messages";
$MESS["IM_MESSENGER_HISTORY_DELETE_ALL"] = "Supprimer tous les messages";
$MESS["IM_MESSENGER_HISTORY_DELETE_ALL_CONFIRM"] = "Tes-vous sûr de vouloir effacer toute l'historique de messages?";
$MESS["IM_MESSENGER_HISTORY_DELETE"] = "Supprimer le message";
$MESS["IM_MESSENGER_HISTORY_DELETE_CONFIRM"] = "tes-vous sûr de vouloir supprimer ce message?";
$MESS["IM_MESSENGER_LOAD_MESSAGE"] = "Charger modèle";
$MESS["IM_MESSENGER_SEND_FILE"] = "Transférer le fichier";
$MESS["IM_MESSENGER_SEND_MESSAGE"] = "Envoyer";
$MESS["IM_MESSENGER_NOT_DELIVERED"] = "Le message n'a pas été délivré";
$MESS["IM_MESSENGER_DELIVERED"] = "Le type de l'organisme";
$MESS["IM_MESSENGER_CONTACT_LIST"] = "Contacts";
$MESS["IM_MESSENGER_VIEW_OFFLINE"] = "Afficher/cacher les utilisateurs manquants";
$MESS["IM_MESSENGER_VIEW_GROUP"] = "Afficher/Masquer les groupes d'utilisateurs";
$MESS["IM_MESSENGER_WRITE_MESSAGE"] = "Écrire un message";
$MESS["IM_MESSENGER_OPEN_HISTORY"] = "Ouvrir l'historique";
$MESS["IM_MESSENGER_OPEN_VIDEO"] = "Appel vidéo";
$MESS["IM_MESSENGER_OPEN_PROFILE"] = "Evènements du profil utilisateur";
$MESS["IM_MESSENGER_MESSAGES"] = "...messages";
$MESS["IM_MESSENGER_CL_EMPTY"] = "- Pas de contacts -";
?>