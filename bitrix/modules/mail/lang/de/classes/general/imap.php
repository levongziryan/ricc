<?
$MESS["MAIL_IMAP_ERR_CONNECT"] = "Fehler der Verbindung zum Server";
$MESS["MAIL_IMAP_ERR_BAD_SERVER"] = "Der Server hat eine unbekannte Antwort gesendet.";
$MESS["MAIL_IMAP_ERR_AUTH"] = "Autorisierungsfehler";
$MESS["MAIL_IMAP_ERR_AUTH_MECH"] = "Der Server unterstützt nicht die erforderliche Autorisierungsmethode.";
$MESS["MAIL_IMAP_ERR_COMMUNICATE"] = "Fehler der Datenübertragung.";
?>