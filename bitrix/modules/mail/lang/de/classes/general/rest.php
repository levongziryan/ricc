<?
$MESS["MAIL_MAILSERVICE_SITE_ID"] = "Website";
$MESS["MAIL_MAILSERVICE_ACTIVE"] = "Aktiv";
$MESS["MAIL_MAILSERVICE_NAME"] = "Name";
$MESS["MAIL_MAILSERVICE_SERVER"] = "Serveradresse";
$MESS["MAIL_MAILSERVICE_PORT"] = "Port";
$MESS["MAIL_MAILSERVICE_ENCRYPTION"] = "Sichere Verbindung ";
$MESS["MAIL_MAILSERVICE_LINK"] = "URL der Webschnittstelle";
$MESS["MAIL_MAILSERVICE_ICON"] = "Logo";
$MESS["MAIL_MAILSERVICE_SORT"] = "Sortierung";
$MESS["MAIL_MAILSERVICE_EMPTY_ID"] = "ID des E-Mail-Services ist nicht angegeben.";
$MESS["MAIL_MAILSERVICE_EMPTY"] = "E-Mail-Service wurde nicht gefunden.";
$MESS["MAIL_MAILSERVICE_LIST_EMPTY"] = "E-Mail-Services wurden nicht gefunden.";
?>