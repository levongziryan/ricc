<?
$MESS["mail_mailservice_entity_site_field"] = "Website";
$MESS["mail_mailservice_entity_name_field"] = "Name";
$MESS["mail_mailservice_entity_icon_field"] = "Logo";
$MESS["mail_mailservice_entity_active_field"] = "Aktiv";
$MESS["mail_mailservice_entity_server_field"] = "Serveradresse";
$MESS["mail_mailservice_entity_port_field"] = "Port";
$MESS["mail_mailservice_entity_encryption_field"] = "Sichere Verbindung";
$MESS["mail_mailservice_entity_link_field"] = "URL der Webschnittstelle";
$MESS["mail_mailservice_entity_sort_field"] = "Sortierung";
$MESS["mail_mailservice_not_found"] = "E-Mail-Service wurde nicht gefunden.";
$MESS["mail_mailservice_bitrix24_icon"] = "post-bitrix24-icon-en.png";
$MESS["mail_mailservice_entity_type_field"] = "Typ";
$MESS["mail_mailservice_entity_token_field"] = "Token";
$MESS["mail_mailservice_entity_flags_field"] = "Optionen";
?>