<?
$MESS["MAIL_IMAP_ERR_DEFAULT"] = "Unbekannter Fehler";
$MESS["MAIL_IMAP_ERR_CONNECT"] = "Fehler der Verbindung zum Server";
$MESS["MAIL_IMAP_ERR_COMMUNICATE"] = "Fehler der Datenübertragung.";
$MESS["MAIL_IMAP_ERR_BAD_SERVER"] = "Der Server hat eine unbekannte Antwort gesendet.";
$MESS["MAIL_IMAP_ERR_AUTH"] = "Autorisierungsfehler";
$MESS["MAIL_IMAP_ERR_AUTH_MECH"] = "Der Server unterstützt nicht die erforderliche Autorisierungsmethode.";
$MESS["MAIL_IMAP_ERR_REJECTED"] = "Verbindung verweigert";
$MESS["MAIL_IMAP_ERR_EMPTY_RESPONSE"] = "Keine Antwort vom Server";
$MESS["MAIL_IMAP_ERR_STARTTLS"] = "Fehler bei Initialisierung der API-Verschlüsselung";
$MESS["MAIL_IMAP_ERR_COMMAND_REJECTED"] = "Befehl abgelehnt";
$MESS["MAIL_IMAP_ERR_CAPABILITY"] = "Fehler beim Abrufen der Fähigkeiten";
$MESS["MAIL_IMAP_ERR_LIST"] = "Fehler beim Abrufen der Ordnerliste";
$MESS["MAIL_IMAP_ERR_SELECT"] = "Fehler bei der Auswahl eines Ordners";
$MESS["MAIL_IMAP_ERR_SEARCH"] = "Durchsuchen der Mails ist fehlgeschlagen";
$MESS["MAIL_IMAP_ERR_FETCH"] = "Fehler beim Abrufen der Nachrichten";
$MESS["MAIL_IMAP_ERR_APPEND"] = "Fehler beim Speichern der Nachricht";
$MESS["MAIL_IMAP_ERR_STORE"] = "Fehler bei Aktualisierung der Nachricht";
$MESS["MAIL_IMAP_ERR_AUTH_OAUTH"] = "Genehmigung konnte nicht erhalten werden";
?>