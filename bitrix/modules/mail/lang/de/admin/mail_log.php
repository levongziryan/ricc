<?
$MESS["MAIL_LOG_FILT_ANY"] = "(alle)";
$MESS["MAIL_LOG_LISTEMPTY"] = "Die Liste ist leer";
$MESS["MAIL_LOG_TITLE"] = "E-Mail-Protokoll";
$MESS["MAIL_LOG_LISTTOTAL"] = "Gesamt:";
$MESS["MAIL_LOG_FILT_MSG"] = "Nachricht ";
$MESS["MAIL_LOG_MSG"] = "Nachricht ";
$MESS["MAIL_LOG_FILT_MBOX"] = "Postfach";
$MESS["MAIL_LOG_MBOX"] = "Postfach";
$MESS["MAIL_LOG_FILT_RULE"] = "Regel";
$MESS["MAIL_LOG_RULE"] = "Regel";
$MESS["MAIL_LOG_FILT_SHOW_COLUMN"] = "Spalte zeigen";
$MESS["MAIL_LOG_TEXT"] = "Text";
$MESS["MAIL_LOG_NAVIGATION"] = "Zeilen";
$MESS["MAIL_LOG_TIME"] = "Zeit";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Gesamt:";
?>