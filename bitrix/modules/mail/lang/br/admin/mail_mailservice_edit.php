<?
$MESS["MAIL_MSERVICE_EDT_TITLE_1"] = "Editar serviço de e-mail ##ID#";
$MESS["MAIL_MSERVICE_EDT_TITLE_2"] = "Novo serviço de e-mail";
$MESS["MAIL_MSERVICE_EDT_BACK_LINK"] = "Serviços de e-mail";
$MESS["MAIL_MSERVICE_EDT_NEW"] = "Novo serviço de e-mail";
$MESS["MAIL_MSERVICE_EDT_DELETE"] = "Excluir este";
$MESS["MAIL_MSERVICE_EDT_DELETE_CONFIRM"] = "Excluir este serviço de e-mail?";
$MESS["MAIL_MSERVICE_EDT_TAB"] = "Serviço de E-mail";
$MESS["MAIL_MSERVICE_EDT_ID"] = "ID:";
$MESS["MAIL_MSERVICE_EDT_SITE_ID"] = "Site:";
$MESS["MAIL_MSERVICE_EDT_ACT"] = "Ativo:";
$MESS["MAIL_MSERVICE_EDT_ICON"] = "Logotipo:";
$MESS["MAIL_MSERVICE_EDT_ICON_REMOVE"] = "Excluir arquivo";
$MESS["MAIL_MSERVICE_EDT_NAME"] = "Nome:";
$MESS["MAIL_MSERVICE_EDT_SERVER"] = "Servidor de E-mail (IMAP)/porta:";
$MESS["MAIL_MSERVICE_EDT_ENCRYPTION"] = "Usar conexão segura (TLS):";
$MESS["MAIL_MSERVICE_EDT_LINK"] = "URL da interface Web:";
$MESS["MAIL_MSERVICE_EDT_SORT"] = "Classificar por:";
$MESS["MAIL_MSERVICE_EDT_ERROR"] = "Erro ao salvar o serviço de e-mail";
$MESS["MAIL_MSERVICE_EDT_COMMENT1"] = "OpenSSL para PHP é necessário para usar uma conexão segura.";
$MESS["MAIL_MSERVICE_EDT_DOMAIN"] = "Nome do domínio:";
$MESS["MAIL_MSERVICE_EDT_TOKEN"] = "Token:";
$MESS["MAIL_MSERVICE_EDT_TYPE"] = "Tipo:";
$MESS["MAIL_MSERVICE_EDT_PUBLIC"] = "Permitir aos funcionários cadastrar e-mails com domínio";
?>