<?
$MESS["MAIL_MENU_MAIL"] = "Correio";
$MESS["MAIL_MENU_MSG"] = "Mensagens";
$MESS["MAIL_MENU_MSG_ALT"] = "Visualizar mensagens de correio";
$MESS["MAIL_MENU_LOG"] = "Registro de correio";
$MESS["MAIL_MENU_LOG_ALT"] = "Registro de processamento de correio";
$MESS["MAIL_MENU_RULES"] = "Regras";
$MESS["MAIL_MENU_RULES_ALT"] = "Definição das regras para mensagens de correio";
$MESS["MAIL_MENU_MAILBOXES"] = "Caixas de correio";
$MESS["MAIL_MENU_MAILBOXES_ALT"] = "Configurações e regras de correio";
$MESS["MAIL_MENU_MAILSERVICES"] = "Serviços de e-mail";
$MESS["MAIL_MENU_MAILSERVICES_ALT"] = "Configurar serviços de e-mail";
$MESS["MAIL_MENU_MAIL_TITLE"] = "Gerenciamento de correio";
?>