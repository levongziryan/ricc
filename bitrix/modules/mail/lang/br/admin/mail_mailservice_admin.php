<?
$MESS["MAIL_MSERVICE_ADM_TITLE"] = "Serviços de e-mail";
$MESS["MAIL_MSERVICE_ADM_SITE_ID"] = "Site";
$MESS["MAIL_MSERVICE_ADM_ACTIVE"] = "Ativo";
$MESS["MAIL_MSERVICE_ADM_NAME"] = "Nome";
$MESS["MAIL_MSERVICE_ADM_SERVER"] = "Endereço do servidor";
$MESS["MAIL_MSERVICE_ADM_FILT_ANY"] = "(Qualquer)";
$MESS["MAIL_MSERVICE_ADM_CHANGE"] = "Editar";
$MESS["MAIL_MSERVICE_ADM_DELETE"] = "Excluir";
$MESS["MAIL_MSERVICE_ADM_DELETE_CONFIRM"] = "Você quer excluir o serviço de e-mail?";
$MESS["MAIL_MSERVICE_SAVE_ERROR"] = "Erro ao atualizar o serviço de e-mail";
$MESS["MAIL_MSERVICE_DELETE_ERROR"] = "Erro ao excluir o serviço de e-mail";
$MESS["MAIL_MSERVICE_ADM_TYPE"] = "Tipo";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Selecionado:";
$MESS["MAIN_ADD"] = "Adicionar";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "Verificado:";
$MESS["MAIN_ADMIN_LIST_ACTIVATE"] = "ativar";
$MESS["MAIN_ADMIN_LIST_DEACTIVATE"] = "desativar";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "excluir";
?>