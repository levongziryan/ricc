<?
$MESS["MAIL_UNINSTALL_WARNING"] = "Atenção! O módulo será desinstalado do sistema.";
$MESS["MAIL_MODULE_DESC"] = "O módulo Email é destinado a recepção de mensagens, filtragem e execução de ações específicas.";
$MESS["MAIL_UNINSTALL_COMPLETE"] = "Desinstalação concluída.";
$MESS["MAIL_UNINSTALL_DEL"] = "Desinstalar";
$MESS["MAIL_MODULE_NAME"] = "Correio";
$MESS["MAIL_UNINSTALL_ERROR"] = "Erros de desinstalação:";
$MESS["MAIL_INSTALL_TITLE"] = "Desinstalação do módulo Email";
$MESS["MAIL_UNINSTALL_SAVEDATA"] = "Para salvar os dados armazenados nas tabelas de banco de dados, marque a opção \"Salvar tabelas\".";
$MESS["MAIL_UNINSTALL_SAVETABLE"] = "Salvar tabelas";
$MESS["MAIL_INSTALL_BACK"] = "Voltar para a seção de gerenciamento de módulo";
?>