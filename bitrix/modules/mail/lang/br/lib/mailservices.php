<?
$MESS["mail_mailservice_entity_site_field"] = "Site";
$MESS["mail_mailservice_entity_name_field"] = "Nome";
$MESS["mail_mailservice_entity_icon_field"] = "Logotipo";
$MESS["mail_mailservice_entity_active_field"] = "Ativo";
$MESS["mail_mailservice_entity_server_field"] = "Endereço do servidor";
$MESS["mail_mailservice_entity_port_field"] = "Porta";
$MESS["mail_mailservice_entity_encryption_field"] = "Conexão segura";
$MESS["mail_mailservice_entity_link_field"] = "URL da interface Web";
$MESS["mail_mailservice_entity_sort_field"] = "Classificar";
$MESS["mail_mailservice_not_found"] = "Serviço de E-mail não encontrado.";
$MESS["mail_mailservice_bitrix24_icon"] = "post-bitrix24-icon-en.png";
$MESS["mail_mailservice_entity_type_field"] = "Tipo";
$MESS["mail_mailservice_entity_token_field"] = "Token";
$MESS["mail_mailservice_entity_flags_field"] = "Sinalizadores";
?>