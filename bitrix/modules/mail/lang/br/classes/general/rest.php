<?
$MESS["MAIL_MAILSERVICE_SITE_ID"] = "Site";
$MESS["MAIL_MAILSERVICE_ACTIVE"] = "Ativo";
$MESS["MAIL_MAILSERVICE_NAME"] = "Nome";
$MESS["MAIL_MAILSERVICE_SERVER"] = "Endereço do servidor";
$MESS["MAIL_MAILSERVICE_PORT"] = "Porta";
$MESS["MAIL_MAILSERVICE_ENCRYPTION"] = "Conexão segura";
$MESS["MAIL_MAILSERVICE_LINK"] = "URL da interface Web";
$MESS["MAIL_MAILSERVICE_ICON"] = "Logotipo";
$MESS["MAIL_MAILSERVICE_SORT"] = "Classificar";
$MESS["MAIL_MAILSERVICE_EMPTY_ID"] = "ID do serviço de e-mail não especificado.";
$MESS["MAIL_MAILSERVICE_EMPTY"] = "Serviço de E-mail não encontrado.";
$MESS["MAIL_MAILSERVICE_LIST_EMPTY"] = "Os serviços de e-mail não foram encontrados.";
?>