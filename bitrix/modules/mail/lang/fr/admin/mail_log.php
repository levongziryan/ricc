<?
$MESS["MAIL_LOG_FILT_ANY"] = "(n'importe lesquel(el)s)";
$MESS["MAIL_LOG_TIME"] = "Temps";
$MESS["MAIL_LOG_LISTTOTAL"] = "Au total:";
$MESS["MAIL_LOG_FILT_MSG"] = "Lettre";
$MESS["MAIL_LOG_MSG"] = "Lettre";
$MESS["MAIL_LOG_FILT_MBOX"] = "Boîte aux lettres";
$MESS["MAIL_LOG_FILT_RULE"] = "Les règles de messagerie";
$MESS["MAIL_LOG_RULE"] = "Règle";
$MESS["MAIL_LOG_LISTEMPTY"] = "La liste est vide";
$MESS["MAIL_LOG_NAVIGATION"] = "Lignes";
$MESS["MAIL_LOG_TEXT"] = "inscription (texte)";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Sélectionné:";
$MESS["MAIL_LOG_MBOX"] = "Boîte aux lettres";
$MESS["MAIL_LOG_TITLE"] = "Journal de traitement du courrier";
$MESS["MAIL_LOG_FILT_SHOW_COLUMN"] = "colonnes affichées";
?>