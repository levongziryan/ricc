<?
$MESS["MAIL_MENU_LOG_ALT"] = "Journal de traitement du courrier";
$MESS["MAIL_MENU_LOG"] = "Journal Mail";
$MESS["MAIL_MENU_MAILSERVICES_ALT"] = "Réglage des services postaux";
$MESS["MAIL_MENU_MAILBOXES_ALT"] = "Paramètres de messagerie et les règles";
$MESS["MAIL_MENU_RULES_ALT"] = "Cadre des règles de messages électroniques";
$MESS["MAIL_MENU_MSG"] = "Messages";
$MESS["MAIL_MENU_MAIL"] = "Mail";
$MESS["MAIL_MENU_MAILSERVICES"] = "Liste des services courriers";
$MESS["MAIL_MENU_MAILBOXES"] = "Boîtes aux lettres";
$MESS["MAIL_MENU_RULES"] = "Règles";
$MESS["MAIL_MENU_MSG_ALT"] = "Afficher des messages de courrier";
$MESS["MAIL_MENU_MAIL_TITLE"] = "La gestion du courrier";
?>