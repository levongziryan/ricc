<?
$MESS["MAIL_CHECK_CHECK_OK"] = "OK";
$MESS["MAIL_CHECK_LOG"] = "Revoir les enregistrements";
$MESS["MAIL_CHECK_CHECK_ALL"] = "(toutes les boîtes aux lettres)";
$MESS["MAIL_CHECK_MBOX_PARAMS"] = "Paramètres de messagerie";
$MESS["MAIL_CHECK_CNT_NEW"] = "les nouveaux messages.";
$MESS["MAIL_CHECK_ERR"] = "Erreur lors de la réception du courrier.";
$MESS["MAIL_CHECK_TITLE"] = "Vérifier les nouveaux messages";
$MESS["MAIL_CHECK_CNT"] = "Recu";
$MESS["MAIL_CHECK_CHECK"] = "Recevoir du courrier:";
$MESS["MAIL_CHECK_TEXT"] = "Vérifier boîte aux lettres";
$MESS["MAIL_CHECK_VIEW"] = "Voir les messages";
?>