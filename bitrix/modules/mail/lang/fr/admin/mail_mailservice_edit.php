<?
$MESS["MAIL_MSERVICE_EDT_LINK"] = "Adresse de l'interface web:";
$MESS["MAIL_MSERVICE_EDT_ACT"] = "Actif(ve):";
$MESS["MAIL_MSERVICE_EDT_COMMENT1"] = "Pour utiliser une connexion sécurisée en PHP l'extension OpenSSL doit être installée.";
$MESS["MAIL_MSERVICE_EDT_NEW"] = "Ajouter un nouveau service";
$MESS["MAIL_MSERVICE_EDT_DOMAIN"] = "Nom du domaine:";
$MESS["MAIL_MSERVICE_EDT_TITLE_1"] = "Modification des paramètres du service courrier ##ID#";
$MESS["MAIL_MSERVICE_EDT_ENCRYPTION"] = "Utiliser une connexion sécurisée (TLS):";
$MESS["MAIL_MSERVICE_EDT_ID"] = "ID:";
$MESS["MAIL_MSERVICE_EDT_ICON"] = "Logo:";
$MESS["MAIL_MSERVICE_EDT_NAME"] = "Dénomination:";
$MESS["MAIL_MSERVICE_EDT_TITLE_2"] = "Ajouter un nouveau service";
$MESS["MAIL_MSERVICE_EDT_ERROR"] = "Erreur survenue lors de la sauvegarde du service postal";
$MESS["MAIL_MSERVICE_EDT_SERVER"] = "Serveur de messagerie (IMAP) / port:";
$MESS["MAIL_MSERVICE_EDT_TAB"] = "Service postal";
$MESS["MAIL_MSERVICE_EDT_SITE_ID"] = "Site:";
$MESS["MAIL_MSERVICE_EDT_SORT"] = "Triage:";
$MESS["MAIL_MSERVICE_EDT_BACK_LINK"] = "Liste des services courriers";
$MESS["MAIL_MSERVICE_EDT_TYPE"] = "Destination:";
$MESS["MAIL_MSERVICE_EDT_TOKEN"] = "Token:";
$MESS["MAIL_MSERVICE_EDT_DELETE"] = "Supprimer le service";
$MESS["MAIL_MSERVICE_EDT_DELETE_CONFIRM"] = "Supprimer le service courrier?";
$MESS["MAIL_MSERVICE_EDT_ICON_REMOVE"] = "Suppression du fichier";
$MESS["MAIL_MSERVICE_EDT_PUBLIC"] = "Permettre aux employés d'enregistrer des e-mails avec domaine";
?>