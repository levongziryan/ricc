<?
$MESS["MAIL_MSERVICE_ADM_FILT_ANY"] = "(n'importe lesquel(el)s)";
$MESS["MAIL_MSERVICE_ADM_SERVER"] = "Adresse du serveur";
$MESS["MAIN_ADMIN_LIST_ACTIVATE"] = "activer";
$MESS["MAIL_MSERVICE_ADM_ACTIVE"] = "Actif(ve)";
$MESS["MAIN_ADD"] = "Ajouter";
$MESS["MAIL_MSERVICE_ADM_CHANGE"] = "Editer";
$MESS["MAIL_MSERVICE_ADM_NAME"] = "Dénomination";
$MESS["MAIL_MSERVICE_SAVE_ERROR"] = "Erreur lors de la mise à jour du service postal";
$MESS["MAIL_MSERVICE_DELETE_ERROR"] = "Erreur de suppression du service postal";
$MESS["MAIL_MSERVICE_ADM_TITLE"] = "Liste des services courriers";
$MESS["MAIL_MSERVICE_ADM_DELETE_CONFIRM"] = "Voulez-vous supprimer le service e-mail?";
$MESS["MAIL_MSERVICE_ADM_SITE_ID"] = "Site web";
$MESS["MAIL_MSERVICE_ADM_TYPE"] = "Entité";
$MESS["MAIL_MSERVICE_ADM_DELETE"] = "Supprimer";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "supprimer";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Sélectionné:";
$MESS["MAIN_ADMIN_LIST_DEACTIVATE"] = "Désactiver";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "Coché:";
?>