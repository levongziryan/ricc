<?
$MESS["MAIL_IMAP_ERR_DEFAULT"] = "Erreur inconnue";
$MESS["MAIL_IMAP_ERR_CONNECT"] = "Erreur de connexion au serveur";
$MESS["MAIL_IMAP_ERR_COMMUNICATE"] = "Erreur de communication.";
$MESS["MAIL_IMAP_ERR_BAD_SERVER"] = "Le serveur a renvoyé une réponse inconnue.";
$MESS["MAIL_IMAP_ERR_AUTH"] = "Erreur d'authentification";
$MESS["MAIL_IMAP_ERR_AUTH_MECH"] = "Le serveur ne prend pas en charge la méthode d'authentification requise.";
$MESS["MAIL_IMAP_ERR_REJECTED"] = "Connexion refusée";
$MESS["MAIL_IMAP_ERR_EMPTY_RESPONSE"] = "Le serveur n'a renvoyé aucune réponse";
$MESS["MAIL_IMAP_ERR_STARTTLS"] = "Erreur lors de l'initialisation de l'API crypto";
$MESS["MAIL_IMAP_ERR_COMMAND_REJECTED"] = "Commande refusée";
$MESS["MAIL_IMAP_ERR_CAPABILITY"] = "Erreur lors de la récupération des capacités";
$MESS["MAIL_IMAP_ERR_LIST"] = "Erreur lors de la récupération de la liste des dossiers";
$MESS["MAIL_IMAP_ERR_SELECT"] = "Erreur lors de la sélection d'un dossier";
$MESS["MAIL_IMAP_ERR_SEARCH"] = "Échec de la recherche d'e-mail";
$MESS["MAIL_IMAP_ERR_FETCH"] = "Erreur lors de la récupération des messages";
$MESS["MAIL_IMAP_ERR_APPEND"] = "Erreur lors de l'enregistrement du message";
$MESS["MAIL_IMAP_ERR_STORE"] = "Erreur lors de la mise à jour du message";
$MESS["MAIL_IMAP_ERR_AUTH_OAUTH"] = "Impossible de récupérer la permission";
?>