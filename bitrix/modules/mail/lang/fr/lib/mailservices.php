<?
$MESS["mail_mailservice_bitrix24_icon"] = "post-bitrix24-icon-ru.png";
$MESS["mail_mailservice_entity_link_field"] = "Adresse de l'interface web";
$MESS["mail_mailservice_entity_server_field"] = "Adresse du serveur";
$MESS["mail_mailservice_entity_active_field"] = "Actif(ve)";
$MESS["mail_mailservice_entity_encryption_field"] = "Connexion protégée";
$MESS["mail_mailservice_entity_icon_field"] = "Logo";
$MESS["mail_mailservice_entity_name_field"] = "Dénomination";
$MESS["mail_mailservice_entity_port_field"] = "Port";
$MESS["mail_mailservice_not_found"] = "Service postal introuvable";
$MESS["mail_mailservice_entity_site_field"] = "Site web";
$MESS["mail_mailservice_entity_sort_field"] = "Trier";
$MESS["mail_mailservice_entity_type_field"] = "Entité";
$MESS["mail_mailservice_entity_token_field"] = "Jeton";
$MESS["mail_mailservice_entity_flags_field"] = "Drapeaux";
?>