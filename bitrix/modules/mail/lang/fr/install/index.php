<?
$MESS["MAIL_INSTALL_BACK"] = "Revenir à la commande des modules";
$MESS["MAIL_UNINSTALL_WARNING"] = "Attention ! Le module sera supprimé du système.";
$MESS["MAIL_UNINSTALL_SAVEDATA"] = "Vous pouvez stocker des données dans les tables de la base de données, si vous installez le drapeau &quot;Sauvegarder les tables&quot;.";
$MESS["MAIL_UNINSTALL_ERROR"] = "Erreurs lors de la suppression:";
$MESS["MAIL_MODULE_NAME"] = "Mail";
$MESS["MAIL_UNINSTALL_SAVETABLE"] = "Sauvegarder les tables";
$MESS["MAIL_UNINSTALL_COMPLETE"] = "La suppression est terminée";
$MESS["MAIL_UNINSTALL_DEL"] = "Supprimer";
$MESS["MAIL_INSTALL_TITLE"] = "Module de courrier désinstallation";
$MESS["MAIL_MODULE_DESC"] = "Le module de courrier est destiné à recevoir des messages, le filtrage et effectuer des actions spécifiques.";
$MESS["MAIL_GROUP_NAME"] = "Utilisateurs d'e-mail";
$MESS["MAIL_GROUP_DESC"] = "Utilisateurs, identifiés par le système depuis un lien direct contenu dans un e-mail";
?>