<?
$MESS["MAIL_MAILSERVICE_LINK"] = "Adresse de l'interface web";
$MESS["MAIL_MAILSERVICE_SERVER"] = "Adresse du serveur";
$MESS["MAIL_MAILSERVICE_ACTIVE"] = "Actif(ve)";
$MESS["MAIL_MAILSERVICE_ENCRYPTION"] = "Connexion protégée";
$MESS["MAIL_MAILSERVICE_ICON"] = "Logo";
$MESS["MAIL_MAILSERVICE_NAME"] = "Dénomination";
$MESS["MAIL_MAILSERVICE_EMPTY"] = "Service postal introuvable";
$MESS["MAIL_MAILSERVICE_LIST_EMPTY"] = "Aucun service postal  n'a été trouvé";
$MESS["MAIL_MAILSERVICE_EMPTY_ID"] = "L'ID du service postal n'est pas indiqué";
$MESS["MAIL_MAILSERVICE_PORT"] = "Port";
$MESS["MAIL_MAILSERVICE_SITE_ID"] = "Site web";
$MESS["MAIL_MAILSERVICE_SORT"] = "Trier";
?>