<?
$MESS["MAIL_IMAP_ERR_BAD_SERVER"] = "Réponse du serveur inconnue";
$MESS["MAIL_IMAP_ERR_AUTH"] = "Erreur d'autorisation";
$MESS["MAIL_IMAP_ERR_COMMUNICATE"] = "Erreur de transfert de données.";
$MESS["MAIL_IMAP_ERR_CONNECT"] = "La connexion au serveur établie";
$MESS["MAIL_IMAP_ERR_AUTH_MECH"] = "Le serveur ne supporte pas le mécanisme d'authentification nécessaire";
?>