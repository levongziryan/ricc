<?
$MESS["MAIL_IMAP_ERR_DEFAULT"] = "Error desconocido";
$MESS["MAIL_IMAP_ERR_CONNECT"] = "Error de conexión del servidor";
$MESS["MAIL_IMAP_ERR_COMMUNICATE"] = "Error de comunicación.";
$MESS["MAIL_IMAP_ERR_BAD_SERVER"] = "El servidor ha devuelto una respuesta desconocida.";
$MESS["MAIL_IMAP_ERR_AUTH"] = "Error de autenticación";
$MESS["MAIL_IMAP_ERR_AUTH_MECH"] = "El servidor no es compatible con el método de autenticación requerido.";
$MESS["MAIL_IMAP_ERR_REJECTED"] = "Conexión rechazada";
$MESS["MAIL_IMAP_ERR_EMPTY_RESPONSE"] = "El servidor no devolvió ninguna respuesta";
$MESS["MAIL_IMAP_ERR_STARTTLS"] = "Error al inicializar crypto API";
$MESS["MAIL_IMAP_ERR_COMMAND_REJECTED"] = "Comando declinado";
$MESS["MAIL_IMAP_ERR_CAPABILITY"] = "Error al obtener capacidades";
$MESS["MAIL_IMAP_ERR_LIST"] = "Error al obtener lista de carpetas";
$MESS["MAIL_IMAP_ERR_SELECT"] = "Error al seleccionar una carpeta";
$MESS["MAIL_IMAP_ERR_SEARCH"] = "Error en la búsqueda de correo";
$MESS["MAIL_IMAP_ERR_FETCH"] = "Obtener mensajes de error";
$MESS["MAIL_IMAP_ERR_APPEND"] = "Error al guardar el mensaje";
$MESS["MAIL_IMAP_ERR_STORE"] = "Error al actualizar el mensaje";
$MESS["MAIL_IMAP_ERR_AUTH_OAUTH"] = "No se puede obtener permiso";
?>