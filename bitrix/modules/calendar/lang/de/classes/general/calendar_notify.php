<?
$MESS["EC_MESS_INVITE"] = "#OWNER_NAME# lädt Sie zum Termin \"#TITLE#\" ein, der am #ACTIVE_FROM# stattfindet";
$MESS["EC_MESS_INVITE_SITE"] = "Ich möchte Sie zum Termin \"[B]#TITLE#[/B]\" einladen. Der Termin findet statt am [B]#ACTIVE_FROM#[/B].";
$MESS["EC_MESS_REC_INVITE"] = "#OWNER_NAME# lädt Sie zum Serientermin \"#TITLE#\" #ACTIVE_FROM#, #RRULE# ein";
$MESS["EC_MESS_REC_INVITE_SITE"] = "Ich möchte Sie zum Serientermin \"[B]#TITLE#[/B]\" [B]#ACTIVE_FROM#, #RRULE#[/B] einladen";
$MESS["EC_MESS_INVITE_CHANGED"] = "#OWNER_NAME# hat den Termin \"#TITLE#\" geändert, an dem Sie am #ACTIVE_FROM# teilnehmen";
$MESS["EC_MESS_INVITE_CHANGED_SITE"] = "Der Termin \"[B]#TITLE#[/B]\", an dem Sie am [B]#ACTIVE_FROM#[/B] teilnehmen, wurde geändert";
$MESS["EC_MESS_INVITE_CANCEL"] = "#OWNER_NAME# hat den Termin \"#TITLE#\" storniert, an dem Sie am #ACTIVE_FROM# teilnehmen sollten";
$MESS["EC_MESS_INVITE_CANCEL_SITE"] = "Der Termin \"[B]#TITLE#[/B]\", an dem Sie am [B]#ACTIVE_FROM#[/B] teilnehmen sollten, wurde storniert";
$MESS["EC_MESS_REC_ALL_CANCEL"] = "#OWNER_NAME# hat alle Termine vom Serientermin \"[B]#TITLE#[/B]\" vom [B]#ACTIVE_FROM#[/B] storniert, an dem Sie teilnehmen sollten";
$MESS["EC_MESS_REC_ALL_CANCEL_SITE"] = "Alle Termine vom Serientermin \"[B]#TITLE#[/B]\" vom [B]#ACTIVE_FROM#[/B], an denen Sie teilnehmen sollten, wurden storniert";
$MESS["EC_MESS_REC_THIS_CANCEL"] = "Alle Termine von \"[B]#TITLE#[/B]\" ([B]#ACTIVE_FROM#[/B]), an denen Sie teilnehmen sollten, wurden von #OWNER_NAME# storniert";
$MESS["EC_MESS_REC_THIS_CANCEL_SITE"] = "Alle Termine von \"[B]#TITLE#[/B]\" ([B]#ACTIVE_FROM#[/B]), an denen Sie teilnehmen sollten, wurden storniert";
$MESS["EC_MESS_INVITE_DETAILS"] = "Termindetails in Ihrem Kalender anzeigen: #LINK#";
$MESS["EC_MESS_INVITE_DETAILS_SITE"] = "Termindetails in Ihrem [url=#LINK#]Kalender[/url] anzeigen.";
$MESS["EC_MESS_INVITE_CONF_Y"] = "Teilnahme bestätigen: #LINK#";
$MESS["EC_MESS_INVITE_CONF_N"] = "Teilnahme ablehnen: #LINK#";
$MESS["EC_MESS_INVITE_CONF_Y_SITE"] = "Bestätigen";
$MESS["EC_MESS_INVITE_CONF_N_SITE"] = "Ablehnen";
$MESS["EC_MESS_VIEW_OWN_CALENDAR"] = "Sie können andere Termine in Ihrem [url=#LINK#]persönlichen Kalender[/url] anzeigen";
$MESS["EC_MESS_VIEW_OWN_CALENDAR_OUT"] = "Sie können andere Termine und Besprechungen in Ihrem persönlichen Kalender anzeigen: #LINK#";
$MESS["EC_MESS_INVITE_TITLE"] = "Einladung zum Termin \"#TITLE#\" von #OWNER_NAME#";
$MESS["EC_MESS_INVITE_CHANGED_TITLE"] = "Änderungen am Termin \"#TITLE#\", an dem Sie teilnehmen";
$MESS["EC_MESS_INVITE_CANCEL_TITLE"] = "Termin \"#TITLE#\" storniert";
$MESS["EC_MESS_INVITE_ACCEPTED"] = "#GUEST_NAME# wird an Ihrem Termin \"#TITLE#\" teilnehmen";
$MESS["EC_MESS_INVITE_ACCEPTED_SITE"] = "Ich werde an Ihrem Termin \"[B]#TITLE#[/B]\" teilnehmen";
$MESS["EC_MESS_INVITE_DECLINED"] = "#GUEST_NAME# wird an Ihrem Termin \"#TITLE#\" nicht teilnehmen";
$MESS["EC_MESS_INVITE_DECLINED_SITE"] = "Ich werde an Ihrem Termin \"[B]#TITLE#[/B]\" nicht teilnehmen";
?>