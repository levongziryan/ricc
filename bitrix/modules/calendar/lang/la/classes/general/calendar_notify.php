<?
$MESS["EC_MESS_INVITE"] = "#OWNER_NAME# te invita a asistir al evento\"#TITLE#\" para tener lugar en#ACTIVE_FROM#";
$MESS["EC_MESS_INVITE_SITE"] = "Me gustaría invitarte al evento \"[B]#TITLE#[/B]\". Dicho evento tendrá lugar en[B]#ACTIVE_FROM#[/B].";
$MESS["EC_MESS_REC_INVITE"] = "#OWNER_NAME# invitación al evento recurrente\"#TITLE#\" #ACTIVE_FROM#, #RRULE#";
$MESS["EC_MESS_REC_INVITE_SITE"] = "Me gustaría invitarlo al evento recurrente \"[B]#TITLE#[/B]\" [B]#ACTIVE_FROM#, #RRULE#[/B]";
$MESS["EC_MESS_INVITE_CHANGED"] = "#OWNER_NAME# actualizar el evento \"#TITLE#\" al que asistirás#ACTIVE_FROM#";
$MESS["EC_MESS_INVITE_CHANGED_SITE"] = "El evento \"[B]#TITLE#[/B]\" al que asistirá [B]#ACTIVE_FROM#[/B] fue actualizado";
$MESS["EC_MESS_INVITE_CANCEL"] = "#OWNER_NAME# canceló el evento \"#TITLE#\" al que asistirá el #ACTIVE_FROM#";
$MESS["EC_MESS_INVITE_CANCEL_SITE"] = "El evento \"[B]#TITLE#[/B]\" al que asistirá el [B]#ACTIVE_FROM#[/B] fue cancelado";
$MESS["EC_MESS_REC_ALL_CANCEL"] = "#OWNER_NAME# canceló todas las instancias de eventos recurrentes \"[B]#TITLE#[/B]\" de [B]#ACTIVE_FROM#[/B] al que asistirá";
$MESS["EC_MESS_REC_ALL_CANCEL_SITE"] = "Todas las instancias de eventos recurrentes \"[B]#TITLE#[/B]\" de [B]#ACTIVE_FROM#[/B] al que asistirá fue cancelado";
$MESS["EC_MESS_REC_THIS_CANCEL"] = "Una instancia de evento \"[B]#TITLE#[/B]\" ([B]#ACTIVE_FROM#[/B]) al que asistirá fue cancelado #OWNER_NAME#";
$MESS["EC_MESS_REC_THIS_CANCEL_SITE"] = "Una instancia \"[B]#TITLE#[/B]\" ([B]#ACTIVE_FROM#[/B]) de evento al que asistirá fue cancelado";
$MESS["EC_MESS_INVITE_DETAILS"] = "Ver los detalles del evento en tu calendario: #LINK#";
$MESS["EC_MESS_INVITE_DETAILS_SITE"] = "Vea los detalles del evento en su [url=#LINK#]calendar[/url].";
$MESS["EC_MESS_INVITE_CONF_Y"] = "Confirmar la asistencia: #LINK#";
$MESS["EC_MESS_INVITE_CONF_N"] = "Declinar su asistencia: #LINK#";
$MESS["EC_MESS_INVITE_CONF_Y_SITE"] = "Confirmar";
$MESS["EC_MESS_INVITE_CONF_N_SITE"] = "Declinar";
$MESS["EC_MESS_VIEW_OWN_CALENDAR"] = "Puede ver otros eventos en su [url=#LINK#]calendario personal[/url]";
$MESS["EC_MESS_VIEW_OWN_CALENDAR_OUT"] = "Puede ver otros eventos y reuniones en su calendario personal: #LINK#";
$MESS["EC_MESS_INVITE_TITLE"] = "Invitación al evento \"#TITLE#\" from #OWNER_NAME#";
$MESS["EC_MESS_INVITE_CHANGED_TITLE"] = "Actualizaciones en el evento \"#TITLE#\" al que asistirá";
$MESS["EC_MESS_INVITE_CANCEL_TITLE"] = "Evento \"#TITLE#\" cancelado";
$MESS["EC_MESS_INVITE_ACCEPTED"] = "#GUEST_NAME# participará en su evento \"#TITLE#\"";
$MESS["EC_MESS_INVITE_ACCEPTED_SITE"] = "Asistiré a su evento \"[B]#TITLE#[/B]\"";
$MESS["EC_MESS_INVITE_DECLINED"] = "#GUEST_NAME# no asistirá a su evento \"#TITLE#\"";
$MESS["EC_MESS_INVITE_DECLINED_SITE"] = "No asistiré a su evento \"[B]#TITLE#[/B]\"";
?>