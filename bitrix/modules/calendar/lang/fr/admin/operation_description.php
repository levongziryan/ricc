<?
$MESS["OP_NAME_CALENDAR_TYPE_ADD"] = "Ajouter de nouveaux événements de ce type";
$MESS["OP_NAME_CALENDAR_ADD"] = "Ajouter des événements";
$MESS["OP_NAME_CALENDAR_TYPE_EDIT_SECTION"] = "Réglage des droits d'accès pour le type donné";
$MESS["OP_NAME_CALENDAR_VIEW_TITLE"] = "Visualiser les emplois et les noms";
$MESS["OP_NAME_CALENDAR_TYPE_VIEW"] = "Afficher les calendriers et événements de ce type";
$MESS["OP_NAME_CALENDAR_VIEW_FULL"] = "Affichage des événements";
$MESS["OP_NAME_CALENDAR_VIEW_TIME"] = "Affichage des emplois seulement";
$MESS["OP_NAME_CALENDAR_EDIT_SECTION"] = "Edition des calendriers";
$MESS["OP_NAME_CALENDAR_TYPE_EDIT_ACCESS"] = "Modification de calendriers et d'événements de ce type";
$MESS["OP_NAME_CALENDAR_TYPE_EDIT"] = "dition des événements de ce type";
$MESS["OP_NAME_CALENDAR_EDIT"] = "Modification des événements";
$MESS["OP_NAME_CALENDAR_EDIT_ACCESS"] = "Gestion des droits d'accès";
?>