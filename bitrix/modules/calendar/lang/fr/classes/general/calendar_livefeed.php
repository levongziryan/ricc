<?
$MESS["EC_LF_COMMENT_MESSAGE_ADD_M"] = "commentaires sur un événement '#EVENT_TITLE#' que vous allez assister à";
$MESS["EC_LF_COMMENT_MESSAGE_ADD"] = "commentaires sur un événement '#EVENT_TITLE#' que vous allez assister à";
$MESS["EC_LF_COMMENT_MESSAGE_ADD_F"] = "commentaires sur un événement '#EVENT_TITLE#' que vous allez assister à";
$MESS["EC_LF_COMMENT_MESSAGE_ADD_Q"] = "A ajouté un commentaire à l'événement \"#EVENT_TITLE#\" auquel vous êtes invité";
$MESS["EC_LF_COMMENT_MESSAGE_ADD_Q_F"] = "A ajouté un commentaire à l'événement \"#EVENT_TITLE#\" auquel vous êtes invité";
$MESS["EC_LF_COMMENT_MESSAGE_ADD_Q_M"] = "A ajouté un commentaire à l'événement \"#EVENT_TITLE#\" auquel vous êtes invité";
?>