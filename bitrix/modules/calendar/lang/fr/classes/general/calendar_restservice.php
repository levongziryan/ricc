<?
$MESS["CAL_REST_SECTION_ERROR"] = "Vous avez donné un ID erroné de la section du calendrier ou bien cet utilisateur n'a pas d'accès à cette section";
$MESS["CAL_REST_ACCESS_DENIED"] = "Accès interdit";
$MESS["CAL_REST_SECT_ID_EXCEPTION"] = "Section id non renseignée";
$MESS["CAL_REST_EVENT_ID_EXCEPTION"] = "ID d'événement non renseignée";
$MESS["CAL_REST_PARAM_EXCEPTION"] = "Le paramètre obligatoire #PARAM_NAME#' pour la méthode '#REST_METHOD#' n'a pas été renseigné";
$MESS["CAL_REST_PARAM_ERROR"] = "La valeur du paramètre '#PARAM_NAME#' n'est pas valide";
$MESS["CAL_REST_GET_DATA_ERROR"] = "Une erreur est survenue lors de la sélection de données.";
$MESS["CAL_REST_GET_STATUS_ERROR"] = "Une erreur s'est produite lors du choix du statut";
$MESS["CAL_REST_SECTION_SAVE_ERROR"] = "Erreur survenue au cours du changement de la section";
$MESS["CAL_REST_EVENT_UPDATE_ERROR"] = "Lors de modification d'événement une erreur s'est produite";
$MESS["CAL_REST_SECTION_NEW_ERROR"] = "Une erreur est intervenue pendant la création de la section";
$MESS["CAL_REST_EVENT_NEW_ERROR"] = "Lors de la création de l'événement une erreur s'est produite";
$MESS["CAL_REST_SECTION_DELETE_ERROR"] = "Lors de suppression de section une erreur s'est produite";
$MESS["CAL_REST_EVENT_DELETE_ERROR"] = "Lors de la suppression de l'événement une erreur s'est produite";
?>