<?
$MESS["EC_NS_CHANGE"] = "L'événement valide jusqu'à ce que";
$MESS["EC_NS_INFO"] = "Informations sur la confirmation/refus de participation à l'évènement";
$MESS["EC_NS_REMINDER"] = "Rappel concernant l'événement";
$MESS["EC_NS_INVITE"] = "Invitation pour participer à l'événement";
$MESS["EC_NS_EVENT_COMMENT"] = "Nouveau commentaire sur un événement";
?>