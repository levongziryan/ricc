<?
$MESS["EC_MESS_INVITE"] = "Користувач #OWNER_NAME# запрошує вас взяти участь у зустрічі \"#TITLE#\", яка відбудеться #ACTIVE_FROM#";
$MESS["EC_MESS_INVITE_SITE"] = "Запрошую вас взяти участь у зустрічі \"[B]#TITLE#[/B]\", яка відбудеться [B]#ACTIVE_FROM#[/B]";
$MESS["EC_MESS_REC_INVITE"] = "Користува #OWNER_NAME# запрошує вас взяти участь у зустрічі, що повторюється \"#TITLE#\" #ACTIVE_FROM#, #RRULE#";
$MESS["EC_MESS_REC_INVITE_SITE"] = "Запрошую вас взяти участь в зустрічі, що повторюється \"[B]#TITLE#[/B]\" [B]#ACTIVE_FROM#, #RRULE#[/B]";
$MESS["EC_MESS_INVITE_CHANGED"] = "Користувач  #OWNER_NAME# змінив зустріч \"#TITLE#\" з Вашою участю, яка відбудеться#ACTIVE_FROM#";
$MESS["EC_MESS_INVITE_CHANGED_SITE"] = "Змінено зустріч \"[B]#TITLE#[/B]\" з Вашою участю, яка відбудеться [B]#ACTIVE_FROM#[/B]";
$MESS["EC_MESS_INVITE_CANCEL"] = "Користувач #OWNER_NAME# скасував зустріч \"#TITLE#\"з Вашою участю, яка повинна була відбутися #ACTIVE_FROM#";
$MESS["EC_MESS_INVITE_CANCEL_SITE"] = "Скасовано зустріч  \"[B]#TITLE#[/B]\"з Вашою участю, яка повинна була відбутися [B]#ACTIVE_FROM#[/B]";
$MESS["EC_MESS_REC_ALL_CANCEL"] = "Користувач #OWNER_NAME# скасував всі повторення зустрічі  \"[B]#TITLE#[/B]\"з Вашою участю, від [B]#ACTIVE_FROM#[/B]";
$MESS["EC_MESS_REC_ALL_CANCEL_SITE"] = "Скасовано всі повторення зустрічі \"[B]#TITLE#[/B]\" з Вашою участю, від [B]#ACTIVE_FROM#[/B]";
$MESS["EC_MESS_REC_THIS_CANCEL"] = "Повторення зустрічі \"[B]#TITLE#[/B]\" ([B]#ACTIVE_FROM#[/B]) з Вашою участю було скасовано #OWNER_NAME#";
$MESS["EC_MESS_REC_THIS_CANCEL_SITE"] = "Повторення зустрічі \"[B]#TITLE#[/B]\" ([B]#ACTIVE_FROM#[/B]) з Вашою участю було скасовано";
$MESS["EC_MESS_INVITE_DETAILS"] = "Деталі зустрічі Ви можете подивитися в своєму календарі: #LINK#";
$MESS["EC_MESS_INVITE_DETAILS_SITE"] = "Деталі зустрічі Ви можете подивитися в [url=#LINK#] своєму календарі[/url]";
$MESS["EC_MESS_INVITE_CONF_Y"] = "Підтвердити участь: #LINK#";
$MESS["EC_MESS_INVITE_CONF_N"] = "Відмовитися від участі: #LINK#";
$MESS["EC_MESS_INVITE_CONF_Y_SITE"] = "Підтвердити участь";
$MESS["EC_MESS_INVITE_CONF_N_SITE"] = "Відмовитися";
$MESS["EC_MESS_VIEW_OWN_CALENDAR"] = "Переглянути інші події та зустрічі Ви можете в своєму [url=#LINK#]персональному календарі [/url]";
$MESS["EC_MESS_VIEW_OWN_CALENDAR_OUT"] = "Переглянути інші події та зустрічі Ви можете в своєму персональному календарі #LINK#";
$MESS["EC_MESS_INVITE_TITLE"] = "Запрошення на зустріч \"#TITLE#\"  від користувача #OWNER_NAME#";
$MESS["EC_MESS_INVITE_CHANGED_TITLE"] = "Зміна зустрічі\"#TITLE#\" з Вашою участю";
$MESS["EC_MESS_INVITE_CANCEL_TITLE"] = "Зустріч \"#TITLE#\" скасовано";
$MESS["EC_MESS_INVITE_ACCEPTED"] = "Користувач #GUEST_NAME# братиме участь в організованій вами зустрічі \"#TITLE#\"";
$MESS["EC_MESS_INVITE_ACCEPTED_SITE"] = "Я візьму участь в організованій вами зустрічі \"[B]#TITLE#[/B]\"";
$MESS["EC_MESS_INVITE_DECLINED"] = "Користувач #GUEST_NAME# не прийме участь в організованій вами зустрічі \"#TITLE#\"";
$MESS["EC_MESS_INVITE_DECLINED_SITE"] = "Я не візьму участь в організованій вами зустрічі \"[B]#TITLE#[/B]\"";
?>