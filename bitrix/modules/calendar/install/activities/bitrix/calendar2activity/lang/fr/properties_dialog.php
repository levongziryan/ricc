<?
$MESS["BPSNMA_PD_CALENDAR_OWNER"] = "ID du propriétaire du calendrier (facultatif)";
$MESS["BPSNMA_PD_CFROM"] = "A partir de la date";
$MESS["BPSNMA_PD_CTO"] = "Date de clôture";
$MESS["BPSNMA_PD_CNAME"] = "Le nom de l'événement";
$MESS["BPSNMA_PD_CDESCR"] = "Description de l'événement";
$MESS["BPSNMA_PD_CUSER"] = "Utilisateur";
$MESS["BPSNMA_PD_CALENDAR_TYPE"] = "Type du calendrier (non obligatoire)";
$MESS["BPSNMA_PD_CALENDAR_SECTION"] = "ID de la section calendrier (facultatif)";
$MESS["BPSNMA_PD_TIMEZONE"] = "Fuseau horaire";
?>