<?
$MESS["BPSNMA_EMPTY_CALENDARFROM"] = "Le champ 'Date du début' n'est pas rempli.";
$MESS["BPSNMA_EMPTY_CALENDARTO"] = "La champ 'Date de fin' n'est pas rempli";
$MESS["BPSNMA_EMPTY_CALENDARNAME"] = "Le champ 'Nom de l'événement' n'est pas rempli";
$MESS["BPSNMA_EMPTY_CALENDARUSER"] = "La propriété 'Utilisateur' n'est pas indiquée.";
?>