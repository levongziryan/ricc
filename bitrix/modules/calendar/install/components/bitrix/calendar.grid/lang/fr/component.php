<?
$MESS["EC_GROUP_NOT_FOUND"] = "Groupe introuvable.";
$MESS["EC_IBLOCK_ACCESS_DENIED"] = "Accès interdit";
$MESS["EC_GROUP_ID_NOT_FOUND"] = "Le calendrier du groupe ne peut pas être affiché. ID du groupe n'est pas indiqué.";
$MESS["EC_USER_ID_NOT_FOUND"] = "Le calendrier utilisateur ne peut pas être visualisé. L'ID utilisateur n'est pas indiqué.";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "Le module des blocs d'information n'a pas été installé.";
$MESS["EC_INTRANET_MODULE_NOT_INSTALLED"] = "Module du portail corporatif non installé.";
$MESS["EC_USER_NOT_FOUND"] = "L'utilisateur n'est pas trouvé.";
?>