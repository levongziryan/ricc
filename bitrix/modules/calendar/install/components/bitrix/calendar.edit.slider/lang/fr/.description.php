<?
$MESS["EVENT_CALENDAR"] = "Calendrier d'événements";
$MESS["EVENT_CALENDAR_EDIT_FORM"] = "Formulaire d'édition d'un événement";
$MESS["EVENT_CALENDAR_EDIT_FORM_DESCRIPTION"] = "Le formulaire d'édition d'un événement est un composant de service utilisé uniquement comme élément du composant composite.";
?>