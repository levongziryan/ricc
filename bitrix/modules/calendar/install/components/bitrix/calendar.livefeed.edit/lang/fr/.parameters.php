<?
$MESS["ECL_P_DETAIL_URL"] = "Adresse de la page pour une consultation détaillée";
$MESS["ECL_P_CACHE_TIME"] = "Temps de mise en cache (sec).";
$MESS["EC_CALENDAR_SECTION_ALL"] = "liste complète de sections";
$MESS["ECL_P_INIT_DATE"] = "Date de l'initialisation";
$MESS["ECL_P_LOAD_MODE"] = "Chargement des événements";
$MESS["ECL_P_USER_IBLOCK_ID"] = "Bloc d'information de calendriers utilisateurs";
$MESS["ECL_P_EVENTS_COUNT"] = "Quantité d'événements dans la liste";
$MESS["ECL_P_EVENT_LIST_MODE"] = "Afficher uniquement la liste des événements";
$MESS["ECL_P_FUTURE_MONTH_COUNT"] = "Montrer les événements les plus proches pour nombre de mois";
$MESS["ECL_P_CUR_USER_EVENT_LIST"] = "Afficher les événements d'un utilisateur courant";
$MESS["ECL_P_ALLOW_SUPERPOSE"] = "Autoriser l'utilisation de calendriers choisis";
$MESS["EC_CALENDAR_SECTION"] = "Section du calendrier";
$MESS["ECL_P_SHOW_CUR_DATE"] = "-date actuelle-";
$MESS["EC_TYPE"] = "Type de calendrier";
?>