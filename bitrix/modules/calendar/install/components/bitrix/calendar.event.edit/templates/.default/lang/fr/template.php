<?
$MESS["EC_EDEV_EVENT_TITLE"] = "Paramètres de l'événement";
$MESS["EC_EVENT_TYPE"] = "Type d'événement";
$MESS["EC_EVENT_TYPE_NO"] = "non spécifié";
$MESS["EC_EVENT_TYPE_CALL"] = "Appel";
$MESS["EC_EVENT_TYPE_BUSINESS_TRIP"] = "Déplacement professionnel";
$MESS["EC_EVENT_TYPE_MEETING"] = "Réunion";
$MESS["EC_EVENT_TYPE_DISCUSSION"] = "Discussion";
$MESS["EC_EVENT_TYPE_CONFERENCE"] = "Conférence";
$MESS["EC_EVENT_TYPE_VACATION"] = "Congé";
$MESS["EC_EVENT_TYPE_SICK"] = "Congé maladie";
?>