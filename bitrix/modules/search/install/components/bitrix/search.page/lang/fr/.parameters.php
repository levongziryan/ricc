<?
$MESS["CP_BSP_USE_LANGUAGE_GUESS"] = "Activer l'autodétection de la configuration de clavier";
$MESS["SEARCH_PAGER_SHOW_ALWAYS"] = "Afficher toujours";
$MESS["CP_BSP_DISPLAY_TOP_PAGER"] = "Afficher au-dessus des résultats";
$MESS["CP_BSP_DISPLAY_BOTTOM_PAGER"] = "Afficher au-dessous des résultats";
$MESS["CP_BSP_FILTER_NAME"] = "Filtre supplémentaire";
$MESS["SEARCH_RESTART"] = "Rechercher sans tenir compte de la morphologie (en absence du résultat de la recherche)";
$MESS["SEARCH_CHECK_DATES"] = "Ne chercher que dans les documents actifs d'après la date";
$MESS["SEARCH_PAGER_TEMPLATE"] = "Dénomination du modèle";
$MESS["SEARCH_PAGER_SETTINGS"] = "Réglages de la navigation par page";
$MESS["CP_BSP_NO_WORD_LOGIC"] = "Désactiver le traitement des mots en tant que des opérateurs logiques";
$MESS["CP_SP_DEFAULT_SORT_DATE"] = "par date";
$MESS["CP_SP_DEFAULT_SORT_RANK"] = "selon la pertinence";
$MESS["CP_BSP_SHOW_WHEN"] = "Montrer le filtre d'après les dates";
$MESS["SEARCH_USE_TITLE_RANK"] = "Au cours de rangement du résultat tenir compte des titres";
$MESS["SEARCH_RESULTS"] = "Résultats de la recherche";
$MESS["CP_SP_DEFAULT_SORT"] = "Tri par défaut";
$MESS["SEARCH_WHERE_DROPDOWN"] = "Les valeurs de la liste déroulante 'Où chercher'";
$MESS["SEARCH_PAGE_RESULT_COUNT"] = "Résultats par page";
$MESS["SEARCH_PAGER_TITLE"] = "Résultats de la recherche par nom";
$MESS["SEARCH_SHOW_DROPDOWN"] = "Montrer la liste déroulante 'Où chercher'";
?>