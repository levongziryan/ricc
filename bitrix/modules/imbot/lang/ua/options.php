<?
$MESS["IMBOT_TAB_SETTINGS"] = "Налаштування";
$MESS["IMBOT_TAB_TITLE_SETTINGS_2"] = "Параметри підключення";
$MESS["IMBOT_ACCOUNT_ERROR_PUBLIC"] = "Ви не вказали коректну публічну адресу";
$MESS["IMBOT_ACCOUNT_URL"] = "Публічна адреса сайту";
$MESS["IMBOT_WAIT_RESPONSE"] = "Довге очікування відповіді";
$MESS["IMBOT_WAIT_RESPONSE_DESC"] = "Цю опцію необхідно активувати, якщо із-за налаштувань оточення ви не отримуєте відповіді від сервера чат-ботів";
$MESS["IMBOT_ACCOUNT_DEBUG"] = "Режим налагодження";
$MESS["IMBOT_HEADER_BOTS"] = "Чат-боти";
$MESS["IMBOT_BOT_NOTICE"] = "Увага: якщо ви відключите чат-бота, він не буде доступний на порталі, і вся історія листування з ним буде втрачена.";
$MESS["IMBOT_BOT_POSTFIX_UA"] = "Україна";
$MESS["IMBOT_SUPPORT_BOT_NAME"] = "Підтримка Бітрікс24 в коробці";
$MESS["SUPPORT_ERROR_URL"] = "Публічна адреса сайту не доступна з Інтернету";
?>