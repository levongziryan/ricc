<?
$MESS["IMBOT_MODULE_NAME"] = "Bots del chat Bitrix24";
$MESS["IMBOT_MODULE_DESCRIPTION"] = "El módulo Chat Bots para su uso con Bitrix24.";
$MESS["IMBOT_INSTALL_TITLE"] = "Instalación del módulo \"Bitrix24 Chat Bots\"";
$MESS["IMBOT_UNINSTALL_TITLE"] = "Desinstalación del módulo \"Bitrix24 Chat Bots\"";
$MESS["IMBOT_UNINSTALL_QUESTION"] = "¿Seguro que quieres eliminar el módulo? <br> Esto desactivará todos los robots de chat y borrar su historial de mensajes.";
$MESS["IMBOT_CHECK_PULL"] = "El módulo \"Push and Pull\" no está instalado o servidor de espera no está configurado.";
$MESS["IMBOT_CHECK_IM"] = "El módulo de \"Mensajería Instantánea\" no está instalado.";
$MESS["IMBOT_CHECK_PUBLIC_PATH"] = "Dirección pública especificada es incorrecta.";
$MESS["IMBOT_CHECK_IM_VERSION"] = "Por favor, actualice el módulo de \"Mensajería Instantánea\" a la versión 16.1.0";
?>