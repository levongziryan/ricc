<?
$MESS["IMBOT_PUBLIC_PATH_DESC_2"] = "Si el acceso externo a la red está restringido, sólo se permitirá el acceso a ciertas páginas. Por favor, consulte a #LINK_START#documentación#LINK_END# para detalles.";
$MESS["IMBOT_PUBLIC_PATH"] = "Dirección pública de su sitio web:";
$MESS["IMBOT_PUBLIC_PATH_DESC"] = "Se requiere una dirección pública para la página web de chat bots de Bitrix24 para ejecutar.";
?>