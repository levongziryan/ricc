<?
$MESS["IMBOT_PROPERTIES_BOT_COLOR"] = "AZUL";
$MESS["IMBOT_PROPERTIES_BOT_EMAIL"] = "support@bitrix24.ru";
$MESS["IMBOT_PROPERTIES_BOT_WORK_POSITION"] = "Este bot comprueba la IP Rusa y la información INN/OGRN";
$MESS["IMBOT_PROPERTIES_NOT_FOUND_MESSAGE"] = "Lo siento, no se encontró nada";
$MESS["IMBOT_REQUEST_INVALID"] = "Para encontrar detalles de la empresa, introduzca IN o OGRN. Por ejemplo, 7717586110";
$MESS["IMBOT_PROPERTIES_NAME"] = "Nombre completo";
$MESS["IMBOT_PROPERTIES_TERMINATION_DATE"] = "Fecha de finalización de negocios";
$MESS["IMBOT_PROPERTIES_TERMITATION_METHOD_NAME"] = "Método de finalización de negocio";
$MESS["IMBOT_PROPERTIES_INN_KPP"] = "INN/KPP";
$MESS["IMBOT_PROPERTIES_OGRN"] = "OGRN";
$MESS["IMBOT_PROPERTIES_MANAGER"] = "CEO";
$MESS["IMBOT_PROPERTIES_ADDRESS"] = "Dirección";
$MESS["IMBOT_PROPERTIES_OKVED"] = "OKVED";
$MESS["IMBOT_PROPERTIES_STATUS"] = "Estados";
$MESS["IMBOT_PROPERTIES_IP"] = "Propietario único";
$MESS["IMBOT_PROPERTIES_INN"] = "INN";
$MESS["IMBOT_PROPERTIES_OGRNIP"] = "OGRNIP";
$MESS["IMBOT_PROPERTIES_WELCOME_MESSAGE"] = "Puedo encontrar detalles de la compañía y la información sobre los propietarios de negocios. Simplemente ingrese INN o OGRN y yo le mostrará toda la información disponible . Por ejemplo, 7717586110.";
$MESS["IMBOT_PROPERTIES_WELCOME_MESSAGE_CHAT"] = "Puedo encontrar detalles de la compañía y la información sobre los propietarios de negocios. [br] Para iniciar la búsqueda, simplemente me menciona o haga clic en mi avatar";
$MESS["IMBOT_PROPERTIES_SEARCH_RESULTS"] = "Las siguientes organizaciones y propietarios de negocios fueron encontrados";
$MESS["IMBOT_PROPERTIES_SHOW_MORE"] = "Más";
$MESS["IMBOT_PROPERTIES_BOT_NAME"] = "Revisar socio comercial";
?>