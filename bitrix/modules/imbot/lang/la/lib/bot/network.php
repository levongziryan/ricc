<?
$MESS["IMBOT_NETWORK_BOT_WORK_POSITION"] = "Canal abierto";
$MESS["IMBOT_NETWORK_ERROR_BOT_NOT_FOUND"] = "Mensaje no enviado! [br] Este canal abierto se bloquea si hay nuevos mensajes.";
$MESS["IMBOT_NETWORK_ERROR_NOT_FOUND"] = "Mensaje no enviado! [br] Este canal abierto no está disponible actualmente";
$MESS["IMBOT_NETWORK_FDC_END_WELCOME_1"] = "Hola, #USER_NAME#!

La sesión de chat con el Asistente de navegación está terminada. Si usted tiene cualquier preguntas más adelante, es posible que desee utilizar:

- #LINK_START_2#Support24#LINK_END_2# - acceda a la sección Support24 para preguntas frecuentes, manuales de formación en línea y tutoriales en vídeo. 
- #LINK_START_3#Free Webinars#LINK_END_3# - los webinars semanales y las grabaciones en webinarars cubren todos los aspectos de Bitrix24.

Gracias por elegir Bitrix24 :)

";
$MESS["IMBOT_NETWORK_FDC_END_WELCOME_7"] = "Hola, #USER_NAME#!

La sesión de chat con el Asistente de navegación está terminada. Si usted tiene cualquier preguntas más adelante, es posible que desee utilizar:

- #LINK_START_2#Support24#LINK_END_2# - acceda a la sección Support24 para preguntas frecuentes, manuales de formación en línea y tutoriales en vídeo. 
- #LINK_START_3#Free Webinars#LINK_END_3# - los webinars semanales y las grabaciones en webinarars cubren todos los aspectos de Bitrix24.

Gracias por elegir Bitrix24 :)
";
$MESS["IMBOT_NETWORK_FDC_END_MESSAGE_1"] = "El Asistente de navegación está disponible durante las primeras 24 horas para ayudarle a aprender Bitrix24. Si tiene otras preguntas más adelante, puede usar:

- #LINK_START_2#Support24#LINK_END_2# - acceda a la sección Support24 para preguntas frecuentes, manuales de formación en línea y tutoriales en vídeo. 
- #LINK_START_3#Free Webinars#LINK_END_3# - los webinars semanales y las grabaciones en webinarars cubren todos los aspectos de Bitrix24.

Gracias por elegir Bitrix24 :)";
$MESS["IMBOT_NETWORK_FDC_END_MESSAGE_7"] = "El Asistente de navegación está disponible durante las primeras 24 horas para ayudarle a aprender Bitrix24. Si tiene otras preguntas más adelante, puede usar:

- #LINK_START_2#Support24#LINK_END_2# - acceda a la sección Support24 para preguntas frecuentes, manuales de formación en línea y tutoriales en vídeo. 
- #LINK_START_3#Free Webinars#LINK_END_3# - los webinars semanales y las grabaciones en webinarars cubren todos los aspectos de Bitrix24.

Gracias por elegir Bitrix24 :)";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_1"] = "Hola, #USER_NAME#! Te daremos una mano con los primeros treinta días de tu experiencia Bitrix24 y responderemos a cualquier pregunta que pueda tener. Llámenos en cualquier momento :)";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_1_2"] = "Cuéntanos acerca de usted :) ¿Qué tipo de negocio tiene? ¿Cómo desea utilizar Bitrix24 en primer lugar? Esto nos ayudará a elegir casos de uso para usted y mejorar su experiencia de usuario.";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_2"] = "¡Gracias por elegir Bitrix24! Lo último que queremos es que te pierdas las características más útiles:)[br]
¿Sabías que puedes instalar un chat en línea gratis en tu sitio en sólo un par de clics? Cualquier persona que publique mensajes en el chat será automáticamente registrada en el CRM, sus mensajes guardados para un análisis posterior. Estamos aquí, sólo llámenos cuando nos necesite.";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_7"] = "Ha pasado una semana desde que nos conocimos y eso es genial :) ¿Cómo encuentras Bitrix24? ¿Necesitas alguna ayuda?";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_15"] = "¡Gracias por estar con nosotros! Bitrix24 ha sido de su ayuda por dos semanas ya :)[br]
¿Podría ayudarnos a mejorar nuestro servicio? Tenemos tres preguntas sencillas para usted.[br]
1. ¿Encontraste las características del sistema de forma claras y completas? [br]
2. ¿Esta todo a su gusto? [br]
3. ¿Hay que mejorar algo?";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_23"] = "Todavía tengo otra semana para pasar contigo en este chat. Pregúntame si necesitas algo :)";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_30"] = "Su sesión de chat con el experto ha terminado. Esperamos que te ayude a aprender algo nuevo. Siempre que tenga alguna pregunta, estos chicos le ayudarán a salir:

- #LINK_START_1#MIA#LINK_END_1#Es su ayuda personal. Pregúntale algo sobre Bitrix24, ella encontrará la respuesta.
- #LINK_START_4#Helpdesk Chat#LINK_END_4#: disponible en planes comerciales. Disfrutará de una conversación directa con un representante de Helpdesk.
- #LINK_START_2#Support24#LINK_END_2#: cualquier cosa sobre Bitrix24, una extensa guía de las características del sistema.
- #LINK_START_3#Free Webinars#LINK_END_3#: estamos en el aire cada semana con una nueva receta para que te ayude a resolver tus problemas con Bitrix24.

¡Gracias por elegir Bitrix24!";
?>