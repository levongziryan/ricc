<?
$MESS["IMBOT_GIPHY_BOT_NAME"] = "Giphy";
$MESS["IMBOT_GIPHY_BOT_COLOR"] = "AGUA";
$MESS["IMBOT_GIPHY_BOT_GENDER"] = "M";
$MESS["IMBOT_GIPHY_BOT_EMAIL"] = "giphy@bitrix24.com";
$MESS["IMBOT_GIPHY_BOT_WORK_POSITION"] = "Este bot le ayudará a encontrar imágenes que coinciden con su solicitud.";
$MESS["IMBOT_GIPHY_NOT_FOUND_MESSAGE"] = "No se encontró nada :(";
$MESS["IMBOT_GIPHY_FOUND_ALTER_MESSAGE"] = "No he podido encontrar nada que estabas buscando :( [br] Sin embargo, he encontrado algo similar:";
$MESS["IMBOT_GIPHY_WELCOME_MESSAGE"] = "Puedo ayudarle a encontrar imágenes relevantes a su solicitud! [br] Escriba algo y yo elegiré uno para usted! :)";
$MESS["IMBOT_GIPHY_WELCOME_MESSAGE_CHAT"] = "Puedo ayudarle a encontrar imágenes relevantes a su solicitud! [br] Mencione en el mensaje o haga clic en mi avatar para ayudarme a empezar :).";
$MESS["IMBOT_GIPHY_COMMAND_GIPHY_TITLE"] = "Publicar una imagen que coincidan con su consulta";
$MESS["IMBOT_GIPHY_COMMAND_GIPHY_PARAMS"] = "Asunto de la imagen";
$MESS["IMBOT_GIPHY_COMMAND_GIPHY_RETRY"] = "Más!";
$MESS["IMBOT_GIPHY_ICON_BROWSE_TITLE"] = "Examinar imágenes GIF";
$MESS["IMBOT_GIPHY_ICON_BROWSE_DESCRIPTION"] = "Examinar y seleccionar imágenes GIF relevantes";
$MESS["IMBOT_GIPHY_ICON_COPYRIGHT"] = "Giphy.com";
?>