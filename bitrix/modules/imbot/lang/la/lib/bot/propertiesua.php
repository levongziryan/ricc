<?
$MESS["IMBOT_PROPERTIESUA_BOT_NAME"] = "Búsqueda de Compañía";
$MESS["IMBOT_PROPERTIESUA_BOT_COLOR"] = "AZURE";
$MESS["IMBOT_PROPERTIESUA_BOT_EMAIL"] = "support@bitrix24.es";
$MESS["IMBOT_PROPERTIESUA_BOT_WORK_POSITION"] = "Este bot te ayudará a encontrar una compañía con el código EDROPU.";
$MESS["IMBOT_PROPERTIESUA_NOT_FOUND_MESSAGE"] = "Desafortunadamente no se pudo encontrar.";
$MESS["IMBOT_PROPERTIESUA_REQUEST_INVALID"] = "Introduzca el código EDROPU para encontrar una compañía. Por ejemplo: 36149063";
$MESS["IMBOT_PROPERTIESUA_COMPANY_FULL_NAME"] = "Nombre completo";
$MESS["IMBOT_PROPERTIESUA_EDRPOU"] = "EDROPU";
$MESS["IMBOT_PROPERTIESUA_ADDRESS"] = "Dirección";
$MESS["IMBOT_PROPERTIESUA_CEO_NAME"] = "CEO";
$MESS["IMBOT_PROPERTIESUA_PRIMARY_ACTIVITY"] = "Negocio";
$MESS["IMBOT_PROPERTIESUA_STATUS"] = "Estado";
$MESS["IMBOT_PROPERTIESUA_WELCOME_MESSAGE"] = "Te ayudaré a encontrar la compañía que estás buscando. Introduzca el código EDROPU para mostrar la información de la compañía. Por ejemplo: 36149063.";
$MESS["IMBOT_PROPERTIESUA_WELCOME_MESSAGE_CHAT"] = "Te ayudaré a encontrar la compañía que estás buscando. Introduzca el código EDROPU para mostrar la información de la compañía.[br] 
Me menciona en un mensaje o haga clic en mi avatar para empezar.";
$MESS["IMBOT_PROPERTIESUA_SEARCH_RESULTS"] = "He encontrado las siguientes compañías:";
$MESS["IMBOT_PROPERTIESUA_SHOW_MORE"] = "Más...";
?>