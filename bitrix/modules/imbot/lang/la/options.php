<?
$MESS["IMBOT_TAB_SETTINGS"] = "Confiuración";
$MESS["IMBOT_TAB_TITLE_SETTINGS_2"] = "Parámetros de conexión";
$MESS["IMBOT_ACCOUNT_ERROR_PUBLIC"] = "Dirección pública especifica es incorrecta.";
$MESS["IMBOT_ACCOUNT_URL"] = "Dirección pública de su sitio web";
$MESS["IMBOT_WAIT_RESPONSE"] = "Permitir mayor tiempo de espera";
$MESS["IMBOT_WAIT_RESPONSE_DESC"] = "Activar esta opción si no recibe respuesta del servidor chat bot debido a las restricciones ambientales.";
$MESS["IMBOT_ACCOUNT_DEBUG"] = "Modo de depuración";
$MESS["IMBOT_HEADER_BOTS"] = "Chat Bots";
$MESS["IMBOT_BOT_NOTICE"] = "Atención: al deshabilitar el chat bot, éste no estará disponible en los chats y eliminará todos los registros de mensajes asociados.";
$MESS["IMBOT_BOT_POSTFIX_UA"] = "Ucrania";
$MESS["IMBOT_SUPPORT_BOT_NAME"] = "Soporte Bitrix24 en las ediciones locales";
$MESS["SUPPORT_ERROR_URL"] = "La dirección del sitio público no es accesible a través de Internet.";
?>