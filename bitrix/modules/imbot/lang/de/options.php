<?
$MESS["IMBOT_TAB_SETTINGS"] = "Einstellungen";
$MESS["IMBOT_TAB_TITLE_SETTINGS_2"] = "Parameter der Verbindung";
$MESS["IMBOT_ACCOUNT_ERROR_PUBLIC"] = "Die angegebene öffentliche Adresse ist nicht korrekt.";
$MESS["IMBOT_ACCOUNT_URL"] = "Öffentliche Website-Adresse:";
$MESS["IMBOT_WAIT_RESPONSE"] = "Längere Wartezeit erlauben";
$MESS["IMBOT_WAIT_RESPONSE_DESC"] = "Aktivieren Sie diese Option, wenn Sie keine Antwort vom Chat-Bot-Server aufgrund der Einschränkungen Ihrer Umgebung erhalten.";
$MESS["IMBOT_ACCOUNT_DEBUG"] = "Debug-Modus";
$MESS["IMBOT_HEADER_BOTS"] = "Chat-Bots";
$MESS["IMBOT_BOT_NOTICE"] = "Wichtig: Deaktivieren eines Chat-Bots führt dazu, dass es nicht mehr verfügbar sein wird. Außerdem werden alle Nachrichteneinträge gelöscht, die damit zusammenhängen.";
$MESS["IMBOT_BOT_POSTFIX_UA"] = "Ukraine";
$MESS["IMBOT_SUPPORT_BOT_NAME"] = "Support für Bitrix24 On-Premise Editionen";
$MESS["SUPPORT_ERROR_URL"] = "Die öffentliche Website-Adresse kann über Internet nicht erreicht werden.";
?>