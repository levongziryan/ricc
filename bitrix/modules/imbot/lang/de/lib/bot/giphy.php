<?
$MESS["IMBOT_GIPHY_BOT_NAME"] = "Giphy";
$MESS["IMBOT_GIPHY_BOT_COLOR"] = "AQUA";
$MESS["IMBOT_GIPHY_BOT_GENDER"] = "M";
$MESS["IMBOT_GIPHY_BOT_EMAIL"] = "giphy@bitrix24.de";
$MESS["IMBOT_GIPHY_BOT_WORK_POSITION"] = "Dieses Bot hilft Ihnen bei der Suche nach Bildern, die Ihrer Anfrage entsprechen.";
$MESS["IMBOT_GIPHY_NOT_FOUND_MESSAGE"] = "Es wurde nichts gefunden :(";
$MESS["IMBOT_GIPHY_FOUND_ALTER_MESSAGE"] = "Genau das, was Sie suchen, konnte ich leider nicht finden :( [br] Aber ich habe etwas Ähnliches gefunden:";
$MESS["IMBOT_GIPHY_WELCOME_MESSAGE"] = "Ich kann Ihnen helfen, Bilder zu finden, die Ihrer Anfrage ähnlich sind. [br] Geben Sie etwas ein, und ich werde schon eins für Sie finden :)";
$MESS["IMBOT_GIPHY_WELCOME_MESSAGE_CHAT"] = "Ich kann Ihnen helfen, Bilder zu finden, die Ihrer Anfrage ähnlich sind. [br] Erwähnen Sie mich in der Nachricht oder klicken Sie auf meinen Avatar, damit ich anfangen kann :)";
$MESS["IMBOT_GIPHY_COMMAND_GIPHY_TITLE"] = "Bild veröffentlichen, welches Ihrer Anfrage entspricht";
$MESS["IMBOT_GIPHY_COMMAND_GIPHY_PARAMS"] = "Thema des Bildes";
$MESS["IMBOT_GIPHY_COMMAND_GIPHY_RETRY"] = "Mehr!";
$MESS["IMBOT_GIPHY_ICON_BROWSE_TITLE"] = "GIF-Bilder durchsuchen";
$MESS["IMBOT_GIPHY_ICON_BROWSE_DESCRIPTION"] = "GIF-Bilder durchsuchen und passende auswählen";
$MESS["IMBOT_GIPHY_ICON_COPYRIGHT"] = "Giphy.com";
?>