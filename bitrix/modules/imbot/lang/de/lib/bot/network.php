<?
$MESS["IMBOT_NETWORK_BOT_WORK_POSITION"] = "Kommunikationskanal";
$MESS["IMBOT_NETWORK_ERROR_BOT_NOT_FOUND"] = "Nachricht nicht gesendet![br] Dieser Kommunikationskanal ist für neue Nachrichten gesperrt.";
$MESS["IMBOT_NETWORK_ERROR_NOT_FOUND"] = "Nachricht nicht gesendet![br] Dieser Kommunikationskanal ist momentan nicht verfügbar.";
$MESS["IMBOT_NETWORK_FDC_END_WELCOME_1"] = "Hallo #USER_NAME#!

Chat-Sitzung mit dem Onboarding-Assistent ist beendet. Bei evtl. weiteren Fragen können Sie folgendes tun:

- [URL=https://helpdesk.bitrix24.de/?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Support24[/URL] - sich den Bereich Support24 mit FAQ, Trainingskursen und Videos anschauen.
- [URL=https://www.bitrix24.de/support/webinare.php?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Kostenlose Webinare[/URL] - sich regelmäßige Webinare sowie Webinaraufzeichnungen anschauen, diese bieten Informationen praktisch zu allen Aspekten von Bitrix24.
- [URL=https://www.bitrix24.de/partners/?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Partner[/URL] - sich an einen unserer Partner wenden, die Ihnen Hilfe bei der Software-Implementierung sowie Schulung anbieten können.

Danke dafür, dass Sie sich für Bitrix24 entschieden haben :)";
$MESS["IMBOT_NETWORK_FDC_END_WELCOME_7"] = "Hallo, #USER_NAME#!

Die Chat-Sitzung mit dem Online-Assistent ist beendet. Haben Sie später noch weitere Fragen, können Sie folgende Informationsquellen nutzen:

- [URL=https://helpdesk.bitrix24.de/?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Support24[/URL] - im Bereich Support24 finden Sie Häufig Gestellte Fragen (FAQ), Online-Trainings und Videos.
- [URL=https://www.bitrix24.de/support/webinare.php?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Kostenlose Webinsre[/URL] - in regelmäßigen Webinaren sowie Webinaraufzeichnungen werden verschiedene Aspekte von Bitrix24 vorgestellt und erläutert.
- [URL=https://www.bitrix24.de/partners/?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Partner[/URL] - sich an einen unserer Partner wenden, die Ihnen Hilfe bei der Software-Implementierung sowie Schulung anbieten können.

Danke dafür, dass Sie sich für Bitrix24 entschieden haben :)";
$MESS["IMBOT_NETWORK_FDC_END_MESSAGE_1"] = "Der Online-Assistent steht Ihnen innerhalb von ersten 24 Stunden zur Verfügung, damit Sie mehr über Bitrix24 erfahren können. Bei evtl. weiteren Fragen können Sie folgendes tun:

- [URL=https://helpdesk.bitrix24.de/?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Support24[/URL] - sich den Bereich Support24 mit FAQ, Trainingskursen und Videos anschauen.
- [URL=https://www.bitrix24.de/support/webinare.php?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Kostenlose Webinare[/URL] - sich regelmäßige Webinare sowie Webinaraufzeichnungen anschauen, diese bieten Informationen praktisch zu allen Aspekten von Bitrix24.
- [URL=https://www.bitrix24.de/partners/?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Partner[/URL] - sich an einen unserer Partner wenden, die Ihnen Hilfe bei der Software-Implementierung sowie Schulung anbieten können.

Danke dafür, dass Sie sich für Bitrix24 entschieden haben :)";
$MESS["IMBOT_NETWORK_FDC_END_MESSAGE_7"] = "Der Online-Assistent steht Ihnen innerhalb von ersten 7 Tagen zur Verfügung, um Sie beim Kennenlernen von Bitrix24 zu unterstützen. Haben Sie später noch weitere Fragen, können Sie folgende Informationsquellen nutzen:

- [URL=https://helpdesk.bitrix24.de/?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Support24[/URL] - im Bereich Support24 finden Sie Häufig Gestellte Fragen (FAQ), Online-Trainings und Videos.
- [URL=https://www.bitrix24.de/support/webinare.php?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Free Webinars[/URL] - in regelmäßigen Webinaren sowie Webinaraufzeichnungen werden verschiedene Aspekte von Bitrix24 vorgestellt und erläutert.
- [URL=https://www.bitrix24.de/partners/?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Partner[/URL] - sich an einen unserer Partner wenden, die Ihnen Hilfe bei der Software-Implementierung sowie Schulung anbieten können.

Danke dafür, dass Sie für Bitrix24 entschieden haben :)";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_1"] = "Hallo #USER_NAME#! Unser Online-Assistent wird Sie die ersten 30 Tage nach der Erstellung Ihres Bitrix24 unterstützen und alle Fragen beantworten. Sie können uns also jederzeit fragen :) Beachten Sie bitte, dass wir Ihnen mit technischen Fragen in diesem Chat nicht helfen können.";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_1_2"] = "Haben Sie bereits unseren [url=https://www.youtube.com/channel/UC8G6EN8RSb3N_FRO8oFjDMQ]YouTube Kanal[/url] besucht? Dort gibt es mehrere Webinaraufzeichnungen und Videos über Bitrix24, einige von ihnen können für Sie bestimmt interessant sein.";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_2"] = "Danke dafür, dass Sie sich für Bitrix24 entschieden haben. Besuchen Sie doch unseren [url=https://helpdesk.bitrix24.de/?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Helpdesk Bereich[/url], um Bitrix24 besser zu verstehen.";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_7"] = "Wir kennen uns schon seit einer Woche, das ist eigentlich Klasse! :) Sie können übrigens unser [url=https://www.bitrix24.de/partners/?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Partner-Verzeichnis[/url] besuchen, um einen Partner zu finden. Unsere Partner bieten Schulungen an und können Sie bei Implementierung der Software unterstützen.";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_15"] = "Hallo, wir sind schon zwei Wochen zusammen :)[br]
Sagen Sie uns Bescheid, wenn wir irgendwie behilflich sein können.";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_23"] = "Ich muss Ihnen leider sagen, dass Sie nur noch 7 Tage lang mit Ihrem Online-Assistent chatten können.";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_25"] = "Sie nutzen Bitrix24 seit 25 Tagen. 
Vielleicht wäre es für Sie interessant zu wissen, dass Bitrix Inc. ein Partnernetzwerk hat. Zu diesem Netzwerk gehören IT-Unternehmen, die ihre Dienstleistungen in Bereichen Installation, Systemkonfiguration und -Implementierung anbieten. 
Unsere Partner werden Sie gern dabei unterstützen, Bitrix24 möglichst effektiv einzusetzen, sie können Ihr Bitrix24 System anpassen und Ihre Mitarbeiter schulen. 

Senden Sie ein Formular, wenn Sie professionelle Unterstützung unserer Partner benötigen. Dann bekommen Sie mehrere Angebote von unseren Partnern, von denen Sie etwas auswählen können.
";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_25_BUTTON"] = "Formular ausfüllen";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_30"] = "Ihre Chat-Sitzung mit dem Online-Assistent ist vorbei. Wir hoffen, das hat Ihnen geholfen, etwas Neues zu erfahren. Werden Sie ab jetzt noch Fragen haben, können Sie Hilfe hier finden:

- #LINK_START_4#Helpdesk Chat#LINK_END_4#: verfügbar in kostenpflichtigen Tarifen. Sie können mit Helpdesk-Mitarbeitern direkt kommunizieren.
- [URL=https://helpdesk.bitrix24.de/?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Support24[/URL]: alles über Bitrix24, eine ausführliche Anleitung über alle Funktionen des Systems.
- [URL=https://www.bitrix24.de/support/webinare.php?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Kostenlose Webinare[/URL]: wir führen regelmäßig neue Webinare durch, diese werden aufgezeichnet und auf unserem YouTube Kanal veröffentlicht.
- [URL=https://www.bitrix24.de/partners/?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Partner[/URL] - sich an einen unserer Partner wenden, die Ihnen Hilfe bei der Software-Implementierung sowie Schulung anbieten können.

Danke, dass Sie sich für Bitrix24 entschieden haben!
";
$MESS["IMBOT_NETWORK_FDC_30_WITH_SUPPORT_BOT"] = "Ihre Konversation wurde beendet. Wenn Sie Fragen haben, können Sie diese senden an:

- #LINK_START_4#Helpdesk Chat#LINK_END_4#: verfügbar in kostenpflichtigen Tarifen. Sie können mit Helpdesk-Mitarbeitern direkt kommunizieren.
- [URL=https://helpdesk.bitrix24.de/?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Support24[/URL]: alles über Bitrix24, eine ausführliche Anleitung über alle Funktionen des Systems.
- [URL=https://www.bitrix24.de/support/webinare.php?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Kostenlose Webinare[/URL]: wir führen regelmäßig neue Webinare durch, diese werden aufgezeichnet und auf unserem YouTube Kanal veröffentlicht.
- [URL=https://www.bitrix24.de/partners/?utm_source=online-chat&utm_medium=referral&utm_campaign=chat-bot]Partner[/URL] - sich an einen unserer Partner wenden, die Ihnen Hilfe bei der Software-Implementierung sowie Schulung anbieten können.

Danke dafür, dass Sie Bitrix24 nutzen!
";
$MESS["IMBOT_NETWORK_FDC_30_WITH_SUPPORT_BOT_2"] = "Danke, dass Sie sich für den Tarif \"#TARIFF_NAME#\" entschieden haben. Ihre Fragen werden ab jetzt an den neuen #LINK_START_4#Support-Onlinechat#LINK_END_4# gesendet, welcher Bitrix24-Nutzern mit kostenpflichtigen Accounts zur Verfügung steht. Gern werden wir Ihnen helfen!

Der Chat hier wird nun geschlossen, um jegliche Verwirrung zu vermeiden. Aber machen Sie sich keine Sorgen, Sie werden auf Ihre früheren Konversationen jederzeit zugreifen können.";
?>