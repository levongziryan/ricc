<?
$MESS["IMBOT_PROPERTIESUA_BOT_NAME"] = "Suche nach Unternehmen";
$MESS["IMBOT_PROPERTIESUA_BOT_COLOR"] = "AZURE";
$MESS["IMBOT_PROPERTIESUA_BOT_EMAIL"] = "support@bitrix24.ua";
$MESS["IMBOT_PROPERTIESUA_BOT_WORK_POSITION"] = "Dieses Bor wird Ihnen helfen, Unternahmen nach dem EDROPU Code zu suchen.";
$MESS["IMBOT_PROPERTIESUA_NOT_FOUND_MESSAGE"] = "Es wurde leider nichts gefunden.";
$MESS["IMBOT_PROPERTIESUA_REQUEST_INVALID"] = "Geben Sie den EDROPU Code ein, um ein Unternehmen zu finden. Zum Beispiel: 36149063";
$MESS["IMBOT_PROPERTIESUA_COMPANY_FULL_NAME"] = "Vollständiger Name";
$MESS["IMBOT_PROPERTIESUA_EDRPOU"] = "EDROPU";
$MESS["IMBOT_PROPERTIESUA_ADDRESS"] = "Adresse";
$MESS["IMBOT_PROPERTIESUA_CEO_NAME"] = "CEO";
$MESS["IMBOT_PROPERTIESUA_PRIMARY_ACTIVITY"] = "Haupttätigkeit";
$MESS["IMBOT_PROPERTIESUA_STATUS"] = "Status";
$MESS["IMBOT_PROPERTIESUA_WELCOME_MESSAGE"] = "Ich werde Ihnen helfen, das Unternehmen zu finden, nach welchem Sie suchen. Geben Sie den EDROPU Code ein, um Informationen zum Unternehmen anzuzeigen. Zum Beispiel: 36149063.";
$MESS["IMBOT_PROPERTIESUA_WELCOME_MESSAGE_CHAT"] = "Ich werde Ihnen helfen, das Unternehmen zu finden, nach welchem Sie suchen. Geben Sie den EDROPU Code ein, um Informationen zum Unternehmen anzuzeigen.[br] Erwähnen Sie mich in einer Nachricht oder klicken Sie auf mein Avatar, um zu starten.";
$MESS["IMBOT_PROPERTIESUA_SEARCH_RESULTS"] = "Ich habe folgende Unternehmen gefunden:";
$MESS["IMBOT_PROPERTIESUA_SHOW_MORE"] = "Mehr...";
?>