<?
$MESS["IMBOT_MODULE_NAME"] = "Bitrix24 Chat-Bots";
$MESS["IMBOT_MODULE_DESCRIPTION"] = "Das Modul der Chat-Bots für Bitrix24.";
$MESS["IMBOT_INSTALL_TITLE"] = "Das Modul \"Bitrix24 Chat-Bots\" installieren";
$MESS["IMBOT_UNINSTALL_TITLE"] = "Das Modul \"Bitrix24 Chat-Bots\" deinstallieren";
$MESS["IMBOT_UNINSTALL_QUESTION"] = "Möchten Sie dieses Modul wirklich löschen?<br>Alle Chat-Bots werden somit deaktiviert, und ihre Nachrichten-History wird gelöscht.";
$MESS["IMBOT_CHECK_PULL"] = "Das Modul \"Push and Pull\" ist nicht installiert oder Queue-Server ist nicht konfiguriert.";
$MESS["IMBOT_CHECK_IM"] = "Das Modul \"Instant Messenger\" ist nicht installiert.";
$MESS["IMBOT_CHECK_PUBLIC_PATH"] = "Die angegebene öffentliche Adresse ist nicht korrekt.";
$MESS["IMBOT_CHECK_IM_VERSION"] = "Aktualisieren Sie bitte das Modul \"Instant Messenger\" bis zur Version 16.1.0";
$MESS["IMBOT_UNINSTALL_HAS_BOTS"] = "Alle registrierten Chat-Bots müssen deinstalliert werden, bevor das Modul deinstalliert wird.";
$MESS["IMBOT_USERS_LINK"] = "Nutzerliste";
?>