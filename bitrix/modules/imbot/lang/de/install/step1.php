<?
$MESS["IMBOT_PUBLIC_PATH_DESC"] = "Eine öffentliche Website-Adresse ist erforderlich, damit die Bitrix24 Chat-Bots funktionieren können.";
$MESS["IMBOT_PUBLIC_PATH_DESC_2"] = "Wenn externer Zugriff auf Ihr Network eingeschränkt ist, sollten Sie den Zugriff auf bestimmte Seiten gewähren. Nähere Informationen finden Sie in der #LINK_START#Dokumentation#LINK_END#.";
$MESS["IMBOT_PUBLIC_PATH"] = "Öffentliche Website-Adresse:";
?>