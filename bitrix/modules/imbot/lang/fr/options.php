<?
$MESS["IMBOT_TAB_SETTINGS"] = "Paramètres";
$MESS["IMBOT_TAB_TITLE_SETTINGS_2"] = "Paramètres de connexion";
$MESS["IMBOT_ACCOUNT_ERROR_PUBLIC"] = "Aucune adresse correcte n'a été spécifiée.";
$MESS["IMBOT_ACCOUNT_URL"] = "Adresse publique du site";
$MESS["IMBOT_WAIT_RESPONSE"] = "Permettre plus de temps d'attente";
$MESS["IMBOT_WAIT_RESPONSE_DESC"] = "Activez cette option si vous n'obtenez pas de réponse du serveur du bot de chat à cause de restrictions environnementales.";
$MESS["IMBOT_ACCOUNT_DEBUG"] = "Mode debogage";
$MESS["IMBOT_HEADER_BOTS"] = "Bots de chat";
$MESS["IMBOT_BOT_NOTICE"] = "Attention : désactiver le bot de chat le rendra indisponible dans les chats et supprimera tous les journaux de messages associés.";
$MESS["IMBOT_BOT_POSTFIX_UA"] = "Ukraine";
$MESS["IMBOT_SUPPORT_BOT_NAME"] = "Prise en charge des éditions domiciles de Bitrix24";
$MESS["SUPPORT_ERROR_URL"] = "L'adresse du site public n'est pas accessible via internet.";
?>