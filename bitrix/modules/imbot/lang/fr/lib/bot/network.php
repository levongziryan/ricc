<?
$MESS["IMBOT_NETWORK_BOT_WORK_POSITION"] = "Canal ouvert";
$MESS["IMBOT_NETWORK_ERROR_BOT_NOT_FOUND"] = "Message non envoyé ![br] Ce canal ouvert est bloqué pour les nouveaux messages.";
$MESS["IMBOT_NETWORK_ERROR_NOT_FOUND"] = "Message non envoyé ![br] Ce canal ouvert n’est actuellement pas disponible";
$MESS["IMBOT_NETWORK_FDC_END_WELCOME_1"] = "Bonjour, #USER_NAME#!

La session de chat avec l'assistant d'intégration est terminée. Si vous avez des questions à l'avenir, vous pouvez :

- #LINK_START_2#Support24#LINK_END_2# - accéder à la section Support24 pour les questions les plus fréquemment posées, les manuels de formation en ligne et les tutoriels vidéo.
- #LINK_START_3#Webinaires gratuits#LINK_END_3# - consulter les webinaires hebdomadaires et les webinaires enregistrés couvrant chaque aspect de Bitrix24.
- [URL=https://www.bitrix24.com/partners/]Partenaires locaux[/URL] - trouver un revendeur Bitrix24 dans votre pays. Les partenaires locaux peuvent fournir des services de formation, configuration, personnalisation et d'intégration. 

 Merci d'avoir choisir Bitrix24 :)";
$MESS["IMBOT_NETWORK_FDC_END_WELCOME_7"] = "Bonjour, #USER_NAME#!

La session de chat avec l'assistant en ligne est terminée. Si vous avez des questions à l'avenir, vous pouvez :

- #LINK_START_2#Support24#LINK_END_2# - accéder à la section Support24 pour les questions les plus fréquemment posées, les manuels de formation en ligne et les tutoriels vidéo.
- #LINK_START_3#Webinaires gratuits#LINK_END_3# - consulter les webinaires hebdomadaires et les webinaires enregistrés couvrant chaque aspect de Bitrix24.
- [URL=https://www.bitrix24.com/partners/]Partenaires locaux[/URL] - trouver un revendeur Bitrix24 dans votre pays. Les partenaires locaux peuvent fournir des services de formation, configuration, personnalisation et d'intégration. 

 Merci d'avoir choisir Bitrix24 :)";
$MESS["IMBOT_NETWORK_FDC_END_MESSAGE_1"] = "L'assistant en ligne est à votre disposition pendant les premières 24 heures afin de vous aider à en savoir plus sur l'utilisation de Bitrix24. Si vous avez des questions à l'avenir, vous pouvez :

- #LINK_START_2#Support24#LINK_END_2# - accéder à la section Support24 pour les questions les plus fréquemment posées, les manuels de formation en ligne et les tutoriels vidéo.
- #LINK_START_3#Webinaires gratuits#LINK_END_3# - consulter les webinaires hebdomadaires et les webinaires enregistrés couvrant chaque aspect de Bitrix24.
- [URL=https://www.bitrix24.com/partners/]Partenaires locaux[/URL] - trouver un revendeur Bitrix24 dans votre pays. Les partenaires locaux peuvent fournir des services de formation, configuration, personnalisation et d'intégration. 

 Merci d'avoir choisir Bitrix24 :)";
$MESS["IMBOT_NETWORK_FDC_END_MESSAGE_7"] = "L'assistant en ligne est à votre disposition pendant les premières 7 heures afin de vous aider à en savoir plus sur l'utilisation de Bitrix24. Si vous avez des questions à l'avenir, vous pouvez :

- #LINK_START_2#Support24#LINK_END_2# - accéder à la section Support24 pour les questions les plus fréquemment posées, les manuels de formation en ligne et les tutoriels vidéo.
- #LINK_START_3#Webinaires gratuits#LINK_END_3# - consulter les webinaires hebdomadaires et les webinaires enregistrés couvrant chaque aspect de Bitrix24.
- [URL=https://www.bitrix24.com/partners/]Partenaires locaux[/URL] - trouver un revendeur Bitrix24 dans votre pays. Les partenaires locaux peuvent fournir des services de formation, configuration, personnalisation et d'intégration. 

 Merci d'avoir choisir Bitrix24 :)";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_1_2"] = "Avez-vous eu l'occasion de jeter un œil à notre [url=https://www.youtube.com/user/Bitrix24/videos]chaîne YouTube[/url]? Elle comprend toute une série de webinaires et de vidéos de formation sur Bitrix24 en général et sur des outils spécifiques tels que le CRM, la téléphonie, les tâches et la gestion de projet. Il est conseillé de partager ces vidéos avec le reste des utilisateurs de votre Bitrix24.";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_2"] = "Merci d'avoir choisi Bitrix24 ! Ne manquez pas de visiter notre [url=https://helpdesk.bitrix24.com/]section du Support technique[/url] pour apprendre comment tout fonctionne dans Bitrix24.";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_7"] = "Cela fait une semaine que nous avons fait connaissance, et c'est plutôt sympa :) Au fait, avez-vous visité notre [url=https://www.bitrix24.com/partners/]section Partenaire[/url] afin de voir si un revendeur Bitrix24 se trouve dans votre pays ? Les partenaires locaux peuvent fournir des services de formation, configuration, personnalisation et d'intégration.";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_15"] = "Je n'arrive pas à croire cela fait déjà deux semaines :)[br] N'hésitez-pas à nous dire si vous avez besoin d'aide.";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_23"] = "Je n'ai pas envie de vous ennuyer, mais je souhaite simplement vous dire qu'il vous reste 7 jours pour discuter avec votre Assistant en ligne :)";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_30"] = "Votre accès à l'Assistant en ligne est maintenant terminé. Nous espérons qu'il aura contribué à vous apprendre quelque chose de nouveau. Si vous avez des questions dans le futur, les ressources suivantes sont à votre disponibilité :

- #LINK_START_4#Chat d'assistance technique#LINK_END_4#: disponible dans les abonnements commerciaux. Vous pourrez directement parler à un représentant de l'assistance technique.
- #LINK_START_2#Support24#LINK_END_2#: tout à propos de Bitrix24, un guide complet des fonctionnalités du système.
- #LINK_START_3#Webinaires gratuits#LINK_END_3#: nous sommes en ligne chaque semaine avec une nouvelle recette pour vous aider à résoudre vos problèmes lié à l'utilisation de Bitrix24.
- [URL=https://www.bitrix24.com/partners/]Local partners[/URL] - trouvez un revendeur Bitrix24 dans votre pays. Les partenaires locaux peuvent fournir des services de formation, configuration, personnalisation et d'intégration. Merci d'avoir choisir Bitrix24 !";
$MESS["IMBOT_NETWORK_FDC_30_MESSAGE_1"] = "Bonjour, #USER_NAME# ! Bienvenue sur Bitrix24. Pendant les 30 premiers jours, je serai avec vous pour vous aider à comprendre Bitrix24. Veuillez m'en dire plus sur votre entreprise et sur ce que vous attendez de Bitrix24. Quels outils souhaitez-vous utiliser ?";
?>