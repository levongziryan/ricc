<?
$MESS["IMBOT_PROPERTIESUA_BOT_NAME"] = "Recherche de société";
$MESS["IMBOT_PROPERTIESUA_BOT_COLOR"] = "AZURE";
$MESS["IMBOT_PROPERTIESUA_BOT_WORK_POSITION"] = "Ce robot vous aidera à trouver une société à partir du code EDROPU.";
$MESS["IMBOT_PROPERTIESUA_NOT_FOUND_MESSAGE"] = "Malheureusement, rien n'a été trouvé.";
$MESS["IMBOT_PROPERTIESUA_REQUEST_INVALID"] = "Saisissez le code EDROPU pour trouver une société. Par exemple : 36149063";
$MESS["IMBOT_PROPERTIESUA_COMPANY_FULL_NAME"] = "Nom complet";
$MESS["IMBOT_PROPERTIESUA_EDRPOU"] = "EDROPU";
$MESS["IMBOT_PROPERTIESUA_ADDRESS"] = "Adresse";
$MESS["IMBOT_PROPERTIESUA_CEO_NAME"] = "PDG";
$MESS["IMBOT_PROPERTIESUA_PRIMARY_ACTIVITY"] = "Société";
$MESS["IMBOT_PROPERTIESUA_STATUS"] = "Statut";
$MESS["IMBOT_PROPERTIESUA_WELCOME_MESSAGE"] = "Je vais vous aider à trouver la société que vous cherchez. Saisissez le code EDROPU pour afficher les informations sur l'entreprise. Par exemple : 36149063.";
$MESS["IMBOT_PROPERTIESUA_WELCOME_MESSAGE_CHAT"] = "Je vais vous aider à trouver la société que vous cherchez. Saisissez le code EDROPU pour afficher les infos de l'entreprise.[br] Mentionnez-moi dans un message ou cliquez sur mon avatar pour démarrer.";
$MESS["IMBOT_PROPERTIESUA_SEARCH_RESULTS"] = "J'ai trouvé les sociétés suivantes :";
$MESS["IMBOT_PROPERTIESUA_SHOW_MORE"] = "Plus...";
?>