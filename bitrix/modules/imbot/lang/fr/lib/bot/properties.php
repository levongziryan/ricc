<?
$MESS["IMBOT_PROPERTIES_BOT_NAME"] = "Vérification du partenaire commercial";
$MESS["IMBOT_PROPERTIES_BOT_EMAIL"] = "support@bitrix24.ru";
$MESS["IMBOT_PROPERTIES_BOT_WORK_POSITION"] = "Ce bot vérifie les IP russes et les informations INN/OGRN";
$MESS["IMBOT_PROPERTIES_NOT_FOUND_MESSAGE"] = "Désolé, rien n'a été trouvé";
$MESS["IMBOT_REQUEST_INVALID"] = "Pour trouver les informations d'une société, saisissez l'INN ou l'OGRN. Par exemple, 7717586110";
$MESS["IMBOT_PROPERTIES_NAME"] = "Nom complet";
$MESS["IMBOT_PROPERTIES_TERMINATION_DATE"] = "Date de cessation des activités";
$MESS["IMBOT_PROPERTIES_TERMITATION_METHOD_NAME"] = "Méthode de cessation des activités";
$MESS["IMBOT_PROPERTIES_INN_KPP"] = "INN/KPP";
$MESS["IMBOT_PROPERTIES_OGRN"] = "OGRN";
$MESS["IMBOT_PROPERTIES_MANAGER"] = "PDG";
$MESS["IMBOT_PROPERTIES_ADDRESS"] = "Adresse";
$MESS["IMBOT_PROPERTIES_OKVED"] = "OKVED";
$MESS["IMBOT_PROPERTIES_STATUS"] = "Statut";
$MESS["IMBOT_PROPERTIES_IP"] = "Seul propriétaire";
$MESS["IMBOT_PROPERTIES_INN"] = "NIF";
$MESS["IMBOT_PROPERTIES_OGRNIP"] = "OGRNIP";
$MESS["IMBOT_PROPERTIES_WELCOME_MESSAGE"] = "Je peux trouver les détails de la société et des informations sur des propriétaires de société. Entre simplement l'INN ou l'OGRN  et je vous présenterai toutes les informations disponibles. Par exemple, 7717586110.";
$MESS["IMBOT_PROPERTIES_WELCOME_MESSAGE_CHAT"] = "Je peux trouver les détails de la société et des informations sur des propriétaires de société. [br] Pour lancer une recherche, mentionnez-moi ou cliquez sur mon avatar";
$MESS["IMBOT_PROPERTIES_SEARCH_RESULTS"] = "Les organisations et propriétaires de société suivants ont été trouvés";
$MESS["IMBOT_PROPERTIES_SHOW_MORE"] = "Plus";
$MESS["IMBOT_PROPERTIES_BOT_COLOR"] = "AZURE";
?>