<?
$MESS["IMBOT_GIPHY_BOT_NAME"] = "Giphy";
$MESS["IMBOT_GIPHY_BOT_GENDER"] = "M";
$MESS["IMBOT_GIPHY_BOT_WORK_POSITION"] = "Ce bot va vous aider à trouver les images correspondant à votre requête.";
$MESS["IMBOT_GIPHY_NOT_FOUND_MESSAGE"] = "Rien n'a été trouvé :(";
$MESS["IMBOT_GIPHY_FOUND_ALTER_MESSAGE"] = "Je n'ai pas pu trouver ce que vous cherchiez :( [br] Mais j'ai trouvé quelque chose de similaire :";
$MESS["IMBOT_GIPHY_WELCOME_MESSAGE"] = "Je peux vous aider à trouver des images correspondant à votre requête ! [br] Saisissez quelque chose et je vais choisir pour vous ! :)";
$MESS["IMBOT_GIPHY_WELCOME_MESSAGE_CHAT"] = "Je peux vous aider à trouver des images correspondant à votre requête ! [br] Mentionnez-moi dans le message ou cliquez sur mon avatar pour me mettre au boulot :)";
$MESS["IMBOT_GIPHY_COMMAND_GIPHY_TITLE"] = "Postez une image correspondant à votre demande";
$MESS["IMBOT_GIPHY_COMMAND_GIPHY_PARAMS"] = "sujet de l'image";
$MESS["IMBOT_GIPHY_BOT_EMAIL"] = "giphy@bitrix24.com";
$MESS["IMBOT_GIPHY_COMMAND_GIPHY_RETRY"] = "Plus!";
$MESS["IMBOT_GIPHY_BOT_COLOR"] = "AQUA";
$MESS["IMBOT_GIPHY_ICON_BROWSE_TITLE"] = "Parcourir les images en GIF";
$MESS["IMBOT_GIPHY_ICON_BROWSE_DESCRIPTION"] = "Parcourir et sélectionner les images en GIF pertinentes";
?>