<?
$MESS["IMBOT_PUBLIC_PATH_DESC"] = "Une adresse public du site est nécessaire pour faire fonctionner les bots de chat Bitrix24.";
$MESS["IMBOT_PUBLIC_PATH_DESC_2"] = "Si l'accès externe à votre réseau est restreint, activez l'accès seulement à certaines pages. Veuillez consulter #LINK_START#la documentation#LINK_END# pour connaître plus de détails.";
$MESS["IMBOT_PUBLIC_PATH"] = "Adresse public du site :";
?>