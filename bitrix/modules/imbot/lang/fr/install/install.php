<?
$MESS["IMBOT_MODULE_NAME"] = "Bots de chat Bitrix24";
$MESS["IMBOT_MODULE_DESCRIPTION"] = "Le module Bots de chat à utiliser avec Bitrix24.";
$MESS["IMBOT_INSTALL_TITLE"] = "Installation du module \"Bots de chat Bitrix24\"";
$MESS["IMBOT_UNINSTALL_TITLE"] = "Désinstallation du module \"Bots de chat Bitrix24\"";
$MESS["IMBOT_UNINSTALL_QUESTION"] = "Voulez-vous vraiment supprimer le module ?<br>Cette action désactivera tous les bots de chat et supprimera l'historique des messages.";
$MESS["IMBOT_CHECK_PULL"] = "Le module \"Pousser et tirer\" n'est pas installé ou le serveur de file d'attente n'est pas configuré.";
$MESS["IMBOT_CHECK_IM"] = "Le module \"Messagerie instantanée\" n'est pas installé.";
$MESS["IMBOT_CHECK_PUBLIC_PATH"] = "Aucune adresse correcte n'a été spécifiée.";
$MESS["IMBOT_CHECK_IM_VERSION"] = "Veuillez passer le module \"Messagerie instantanée\" en version 16.1.0";
?>