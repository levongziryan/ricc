<?
$MESS["IMBOT_GIPHY_BOT_NAME"] = "Giphy";
$MESS["IMBOT_GIPHY_BOT_EMAIL"] = "giphy@bitrix24.com";
$MESS["IMBOT_GIPHY_BOT_WORK_POSITION"] = "Este bot ajudará você a encontrar imagens correspondentes à sua solicitação.";
$MESS["IMBOT_GIPHY_NOT_FOUND_MESSAGE"] = "Nada foi encontrado :(";
$MESS["IMBOT_GIPHY_FOUND_ALTER_MESSAGE"] = "Não encontrei nada do que você estava procurando :( [br] No entanto, encontrei algo semelhante:";
$MESS["IMBOT_GIPHY_WELCOME_MESSAGE"] = "Posso ajudar você a encontrar imagens relevantes à sua solicitação! [br] Digite alguma coisa e vou escolher uma para você! :)";
$MESS["IMBOT_GIPHY_WELCOME_MESSAGE_CHAT"] = "Posso ajudar você a encontrar imagens relevantes à sua solicitação! [br] Me mencione na mensagem ou clique no meu avatar para me fazer começar :)";
$MESS["IMBOT_GIPHY_COMMAND_GIPHY_TITLE"] = "Coloque uma imagem que corresponde à sua consulta";
$MESS["IMBOT_GIPHY_COMMAND_GIPHY_PARAMS"] = "Tema da imagem";
$MESS["IMBOT_GIPHY_COMMAND_GIPHY_RETRY"] = "Mais!";
?>