<?
$MESS["CT_BLL_SELECTED"] = "Число записів";
$MESS["OL_STAT_BACK"] = "Назад";
$MESS["OL_STAT_BACK_TITLE"] = "Повернутися назад";
$MESS["OL_STAT_USER_ID_CANCEL"] = "Скасувати фільтр по співробітнику";
$MESS["OL_STAT_USER_ID_CANCEL_TITLE"] = "Скасувати фільтр по співробітнику";
$MESS["OL_STAT_FILTER_CANCEL"] = "Відмінити фільтр";
$MESS["OL_STAT_FILTER_CANCEL_TITLE"] = "Відмінити фільтр";
$MESS["OL_STAT_EXCEL"] = "Експорт в Excel";
$MESS["OL_STAT_TITLE"] = "#LINE_NAME# - Статистика";
?>