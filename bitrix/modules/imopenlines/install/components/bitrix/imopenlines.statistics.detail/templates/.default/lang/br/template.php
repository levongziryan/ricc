<?
$MESS["CT_BLL_SELECTED"] = "Contagem de registros";
$MESS["OL_STAT_BACK"] = "Voltar";
$MESS["OL_STAT_BACK_TITLE"] = "Voltar";
$MESS["OL_STAT_USER_ID_CANCEL"] = "Redefinir filtro do funcionário";
$MESS["OL_STAT_USER_ID_CANCEL_TITLE"] = "Redefinir filtro do funcionário";
$MESS["OL_STAT_FILTER_CANCEL"] = "Redefinir filtro";
$MESS["OL_STAT_FILTER_CANCEL_TITLE"] = "Redefinir filtro";
$MESS["OL_STAT_EXCEL"] = "Exportar para o Microsoft Excel";
$MESS["OL_STAT_TITLE"] = "#LINE_NAME# - Estatísticas";
?>