<?
$MESS["CT_BLL_SELECTED"] = "Anzahl der Einträge";
$MESS["OL_STAT_BACK"] = "Zurück";
$MESS["OL_STAT_BACK_TITLE"] = "Zurück";
$MESS["OL_STAT_USER_ID_CANCEL"] = "Mitarbeiterfilter zurücksetzen";
$MESS["OL_STAT_USER_ID_CANCEL_TITLE"] = "Mitarbeiterfilter zurücksetzen";
$MESS["OL_STAT_FILTER_CANCEL"] = "Filter zurücksetzen";
$MESS["OL_STAT_FILTER_CANCEL_TITLE"] = "Filter zurücksetzen";
$MESS["OL_STAT_EXCEL"] = "Export nach Microsoft Excel";
$MESS["OL_STAT_TITLE"] = "#LINE_NAME# - Statistik";
?>