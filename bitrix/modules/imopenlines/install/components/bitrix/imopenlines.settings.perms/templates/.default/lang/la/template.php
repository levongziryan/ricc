<?
$MESS["IMOL_PERM_ROLE"] = "Rol";
$MESS["IMOL_PERM_ADD_ACCESS_CODE"] = "Agregar permiso de acceso";
$MESS["IMOL_PERM_ROLE_LIST"] = "Roles";
$MESS["IMOL_PERM_DELETE"] = "Eliminar";
$MESS["IMOL_PERM_EDIT"] = "Editar";
$MESS["IMOL_PERM_ADD"] = "Agregar";
$MESS["IMOL_PERM_SAVE"] = "Guardar";
$MESS["IMOL_PERM_ERROR"] = "Error";
$MESS["IMOL_PERM_ROLE_DELETE_ERROR"] = "Error al eliminar la función.";
$MESS["IMOL_PERM_ROLE_DELETE"] = "Eliminar rol";
$MESS["IMOL_PERM_ROLE_DELETE_CONFIRM"] = "¿Está seguro de que quieres eliminar el rol?";
$MESS["IMOL_PERM_ROLE_OK"] = "OK";
$MESS["IMOL_PERM_ROLE_CANCEL"] = "Cancelar";
?>