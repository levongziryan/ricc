<?
$MESS["IMOL_PERM_UNKNOWN_ACCESS_CODE"] = "(ID de acesso desconhecido)";
$MESS["IMOL_PERM_UNKNOWN_SAVE_ERROR"] = "Erro ao salvar os dados";
$MESS["IMOL_PERM_ACCESS_DENIED"] = "Permissões de acesso insuficientes";
$MESS["IMOL_PERM_LICENSING_ERROR"] = "Seu plano não permite permissões de acesso de gerenciamento a Canais Abertos.";
?>