<?
$MESS["IMOL_QUICK_ANSWERS_EDIT_SECTION_TITLE"] = "Créer une réponse préfabriquée dans la section";
$MESS["IMOL_QUICK_ANSWERS_EDIT_TEXT_PLACEHOLDER"] = "Texte de la réponse préfabriquée";
$MESS["IMOL_QUICK_ANSWERS_EDIT_CREATE"] = "Créer";
$MESS["IMOL_QUICK_ANSWERS_EDIT_CANCEL"] = "Annuler";
$MESS["IMOL_QUICK_ANSWERS_EDIT_UPDATE"] = "Mise à jour";
$MESS["IMOL_QUICK_ANSWERS_EDIT_SUCCESS"] = "Votre réponse préfabriquée a été enregistrée !";
$MESS["IMOL_QUICK_ANSWERS_EDIT_ERROR"] = "Impossible d'enregistrer la réponse préfabriquée. Veuillez réessayer plus tard.";
$MESS["IMOL_QUICK_ANSWERS_EDIT_ERROR_EMPTY_TEXT"] = "Le texte ne peut être vide";
$MESS["IMOL_QUICK_ANSWERS_EDIT_ALL"] = "Toutes";
$MESS["IMOL_QUICK_ANSWERS_NOT_FOUND"] = "Désolé, mais nous n'avons rien trouvé";
$MESS["IMOL_QUICK_ANSWERS_SEARCH_PROGRESS"] = "Recherche en cours...";
$MESS["IMOL_QUICK_ANSWERS_INFO_TITLE"] = "Utilisez des réponses préfabriquées pour répondre rapidement aux questions fréquemment posées par vos clients";
$MESS["IMOL_QUICK_ANSWERS_INFO_LIST_1"] = "enregistrez un message publié en tant que réponse préfabriquée en cliquant sur le bouton <span class=\"imopenlines-iframe-quick-check-icon\"></span> à côté du message";
$MESS["IMOL_QUICK_ANSWERS_INFO_LIST_2"] = "créez-en une nouvelle à partir de rien";
$MESS["IMOL_QUICK_ANSWERS_INFO_LIST_3"] = "ou ouvrez le <a id=\"quick-info-all-url\" class=\"imopenlines-iframe-quick-link\">formulaire des réponses préfabriquées</a>";
?>