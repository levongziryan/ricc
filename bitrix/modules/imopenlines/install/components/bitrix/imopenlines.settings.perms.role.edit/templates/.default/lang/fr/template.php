<?
$MESS["IMOL_ROLE_LABEL"] = "Rôle";
$MESS["IMOL_ROLE_SAVE"] = "Enregistrer";
$MESS["IMOL_ROLE_CANCEL"] = "Annuler";
$MESS["IMOL_ROLE_ENTITY"] = "Entité";
$MESS["IMOL_ROLE_ACTION"] = "Action";
$MESS["IMOL_ROLE_PERMISSION"] = "Autorisation";
$MESS["IMOL_ROLE_POPUP_LIMITED_TITLE"] = "Canaux ouverts avancés";
$MESS["IMOL_ROLE_POPUP_LIMITED_VOTE_HEAD"] = "Les superviseurs peuvent évaluer les conversations dans les abonnements suivants : Plus, Standard, Professional.<br><br>Les évaluations du superviseur permettent de donner un feedback à chaque agent individuellement et d'améliorer sa performance. ";
$MESS["IMOL_ROLE_LOCK_ALT"] = "Certaines limites sont d'application. Cliquez ici pour en savoir plus.";
?>