<?
$MESS["IMOL_ROLE_LABEL"] = "Função";
$MESS["IMOL_ROLE_SAVE"] = "Salvar";
$MESS["IMOL_ROLE_CANCEL"] = "Cancelar";
$MESS["IMOL_ROLE_ENTITY"] = "Entidade";
$MESS["IMOL_ROLE_ACTION"] = "Ação";
$MESS["IMOL_ROLE_PERMISSION"] = "Permissão";
$MESS["IMOL_ROLE_POPUP_LIMITED_TITLE"] = "Canais Abertos Avançados";
$MESS["IMOL_ROLE_POPUP_LIMITED_VOTE_HEAD"] = "Supervisores podem avaliar conversas nos seguintes planos: Plus, Standard, Profissional.<br><br>Avaliações de supervisor tornam possível fornecer feedback a cada agente, individualmente, e melhorar seu desempenho. ";
$MESS["IMOL_ROLE_LOCK_ALT"] = "Alguns limites se aplicam. Clique, aqui, para saber mais.";
?>