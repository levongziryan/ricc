<?
$MESS["IMOL_ROLE_LABEL"] = "Rol";
$MESS["IMOL_ROLE_SAVE"] = "Guardar";
$MESS["IMOL_ROLE_CANCEL"] = "Cancelar";
$MESS["IMOL_ROLE_ENTITY"] = "Entidad";
$MESS["IMOL_ROLE_ACTION"] = "Acción";
$MESS["IMOL_ROLE_PERMISSION"] = "Permiso";
$MESS["IMOL_ROLE_POPUP_LIMITED_TITLE"] = "Canales Abiertos Avanzados";
$MESS["IMOL_ROLE_POPUP_LIMITED_VOTE_HEAD"] = "Los supervisores son capaces de calificar las conversaciones en los siguientes planes: Plus, Standard, Professional.<br><br> Las calificaciones del Supervisor hacen posible proporcionar retroalimentación a cada agente individualmente y mejorar su rendimiento.";
$MESS["IMOL_ROLE_LOCK_ALT"] = "Se aplican algunos límites. Haga clic aquí para obtener más información.";
?>