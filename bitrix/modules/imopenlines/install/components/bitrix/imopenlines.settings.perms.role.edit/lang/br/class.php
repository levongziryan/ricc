<?
$MESS["IMOL_ROLE_NOT_FOUND"] = "A função não foi encontrada. Preencha o formulário para criar uma nova função.";
$MESS["IMOL_ROLE_SAVE_ERROR"] = "Erro ao salvar a função.";
$MESS["IMOL_ROLE_ERROR_EMPTY_NAME"] = "O nome da função está não especificado";
$MESS["IMOL_ROLE_ERROR_INSUFFICIENT_RIGHTS"] = "Permissões de acesso insuficientes";
$MESS["IMOL_ROLE_LICENSE_ERROR"] = "Erro ao salvar a função.";
?>