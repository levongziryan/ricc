<?
$MESS["OL_COMPONENT_MODULE_NOT_INSTALLED"] = "El módulo de Open Channels no está instalado.";
$MESS["OL_COMPONENT_LIVECHAT_DESCRIPTION"] = "Canal abierto para comunicarse con los empleados de la empresa.";
$MESS["OL_COMPONENT_MODULE_IM_NOT_INSTALLED"] = "El módulo Web Messenger no está instalado.";
$MESS["OL_COMPONENT_MODULE_IMCONNECTOR_NOT_INSTALLED"] = "El módulo \"External IM Connectors\" no está instalado.";
?>