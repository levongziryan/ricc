<?
$MESS["IMOL_LIVECHAT_FORM_INPUT_EMAIL"] = "E-Mail";
$MESS["IMOL_LIVECHAT_FORM_INPUT_EMAIL_ERROR"] = "Geben Sie bitte Ihre E-Mail-Adresse an, damit wir Ihnen antworten können.";
$MESS["IMOL_LIVECHAT_FORM_INPUT_EMAIL_ERROR_2"] = "Geben Sie bitte Ihre E-Mail-Adresse an, damit wir Ihnen eine Kopie der Konversation zuschicken können.";
$MESS["IMOL_LIVECHAT_FORM_INPUT_PHONE"] = "Telefon";
$MESS["IMOL_LIVECHAT_FORM_INPUT_NAME"] = "Vorname";
$MESS["IMOL_LIVECHAT_FORM_BUTTON_SEND"] = "Senden";
$MESS["IMOL_LIVECHAT_FORM_BUTTON_YES"] = "Senden";
$MESS["IMOL_LIVECHAT_FORM_BUTTON_NO"] = "Nein, danke";
$MESS["IMOL_LIVECHAT_FORM_TITLE_HISTORY"] = "Möchten Sie eine Kopie der Konversation per E-Mail zugeschickt bekommen?";
$MESS["IMOL_LIVECHAT_FORM_TITLE_OFFLINE"] = "Geben Sie bitte Ihre E-Mail an, damit wir Sie über eine Antwort benachrichtigen können";
$MESS["IMOL_LIVECHAT_FORM_TITLE_WELCOME"] = "Geben Sie bitte Ihren Namen ein";
$MESS["IMOL_LIVECHAT_FORM_RESULT_HISTORY"] = "Wir haben eine Kopie der Konversation an Ihre E-Mail gesendet.";
$MESS["IMOL_LIVECHAT_FORM_RESULT_OFFLINE"] = "Danke!";
$MESS["IMOL_LIVECHAT_FORM_RESULT_WELCOME"] = "Schön, Sie kennenzulernen.";
?>