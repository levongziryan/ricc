<?
$MESS["IMOL_LIVECHAT_FORM_INPUT_EMAIL"] = "E-mail";
$MESS["IMOL_LIVECHAT_FORM_INPUT_EMAIL_ERROR"] = "Veuillez fournir votre adresse e-mail afin de recevoir des réponses.";
$MESS["IMOL_LIVECHAT_FORM_INPUT_EMAIL_ERROR_2"] = "Veuillez fournir votre adresse e-mail afin de recevoir une copie de la conversation.";
$MESS["IMOL_LIVECHAT_FORM_INPUT_PHONE"] = "Téléphone";
$MESS["IMOL_LIVECHAT_FORM_INPUT_NAME"] = "Prénom";
$MESS["IMOL_LIVECHAT_FORM_BUTTON_SEND"] = "Envoyer";
$MESS["IMOL_LIVECHAT_FORM_BUTTON_YES"] = "Envoyer";
$MESS["IMOL_LIVECHAT_FORM_BUTTON_NO"] = "Non, merci";
$MESS["IMOL_LIVECHAT_FORM_TITLE_HISTORY"] = "Souhaitez-vous recevoir une copie de la conversation sur votre adresse e-mail ?";
$MESS["IMOL_LIVECHAT_FORM_TITLE_OFFLINE"] = "Veuillez indiquer votre adresse e-mail afin d’être notifié lorsque vous recevez une réponse";
$MESS["IMOL_LIVECHAT_FORM_TITLE_WELCOME"] = "Veuillez entrer votre nom";
$MESS["IMOL_LIVECHAT_FORM_RESULT_HISTORY"] = "Nous avons envoyé une copie de la conversation à votre adresse e-mail.";
$MESS["IMOL_LIVECHAT_FORM_RESULT_OFFLINE"] = "Merci !";
$MESS["IMOL_LIVECHAT_FORM_RESULT_WELCOME"] = "Enchanté !";
?>