<?
$MESS["OL_COMPONENT_LIST_ITEM_FILL_START"] = "total de intentos";
$MESS["OL_COMPONENT_LIST_ITEM_FILL_END"] = "completado correctamente";
$MESS["OL_COMPONENT_LIST_ITEM_DATE_CREATE"] = "Creado el ";
$MESS["OL_COMPONENT_LIST_ITEM_PUBLIC_LINK"] = "Enlace público";
$MESS["OL_COMPONENT_LIST_ITEM_PUBLIC_LINK_COPY"] = "Copiar al portapapeles";
$MESS["OL_COMPONENT_LIST_ITEM_START_EDIT_BUT_STOPPED"] = "No se completo";
$MESS["OL_COMPONENT_LIST_ITEM_ACTIVE_ON"] = "on";
$MESS["OL_COMPONENT_LIST_ITEM_ACTIVE_OFF"] = "off";
$MESS["OL_COMPONENT_LIST_ITEM_ACTIVE_ON_NOW"] = "activado";
$MESS["OL_COMPONENT_LIST_ITEM_ACTIVE_OFF_NOW"] = "desactivado en este momento";
$MESS["OL_COMPONENT_LIST_ITEM_ACTIVE_ACTIVATED"] = "activado";
$MESS["OL_COMPONENT_LIST_ITEM_ACTIVE_DEACTIVATED"] = "desactivado";
$MESS["OL_COMPONENT_LIST_ITEM_ACTIVE_ACT_ON"] = "on";
$MESS["OL_COMPONENT_LIST_ERROR_ACTION"] = "La acción ha sido cancelada debido a un error.";
$MESS["OL_COMPONENT_LIST_DELETE_CONFIRM"] = "¿Quieres eliminar esta canal abierto?";
$MESS["OL_COMPONENT_LIST_CLOSE"] = "Cerrar";
$MESS["OL_COMPONENT_LIST_APPLY"] = "Ejecutar";
$MESS["OL_COMPONENT_LIST_CANCEL"] = "Cancelar";
$MESS["OL_COMPONENT_LIST_POPUP_LIMITED_TITLE"] = "Canales abiertos avanzados";
$MESS["OL_COMPONENT_LIST_POPUP_LIMITED_TEXT"] = "Usted tiene un plan actual que limita cuántos canales abiertos puede tener. Para agregar otro canal abierto, actualice su plan.
<br><br>
TIP: Bitrix24 Professional viene con un número ilimitado de canales abiertos.
<br><br>
Prueba gratuita de Bitrix24 Professional durante 30 días.";
$MESS["OL_COMPONENT_LIST_ACTIONS_REMOVE"] = "Quitar Canales Abiertos";
$MESS["OL_COMPONENT_LIST_ADD_LINE"] = "Crear Canales Abiertos";
$MESS["OL_COMPONENT_LIST_ADD_LINE_DESC"] = "Haga clic en la casilla para crear un nuevo canal abierto";
$MESS["OL_COMPONENT_LIST_ADD_LINE_LIMIT"] = "No se pueden crear más canales abiertos con su plan actual.";
$MESS["OL_COMPONENT_LIST_HEADER"] = "Canales Abiertos";
$MESS["OL_COMPONENT_LIST_COUNT_LEAD"] = "Prospectos: #COUNT#";
$MESS["OL_COMPONENT_LIST_CONNECTORS"] = "Conectado:";
$MESS["OL_COMPONENT_LIST_STATS"] = "Estadística:";
$MESS["OL_COMPONENT_LIST_GOTO"] = "abrir";
$MESS["OL_COMPONENT_LIST_QUEUE"] = "Operadores en la cola:";
$MESS["OL_COMPONENT_LIST_ACTIVE_Y"] = "Canal activado";
$MESS["OL_COMPONENT_LIST_ACTIVE_N"] = "Canal desactivado";
$MESS["OL_COMPONENT_LIST_COUNT_DIALOG"] = "Total de comunicaciones";
$MESS["OL_COMPONENT_LIST_COUNT_CLOSE"] = "cerrar";
$MESS["OL_COMPONENT_LIST_COUNT_IN_WORK"] = "en progreso";
$MESS["OL_COMPONENT_LIST_COUNT_MESSAGE"] = "comunicaciones";
$MESS["OL_COMPONENT_LIST_MODIFY_DATE_NOW"] = "actualizar ahora";
$MESS["OL_COMPONENT_LIST_MODIFY_DATE"] = "actualizado";
$MESS["OL_COMPONENT_LIST_PROMO_1"] = "Los Canales Abiertos consolidan diversos medios de comunicaciones digitales con sus clientes en Bitrix24.";
$MESS["OL_COMPONENT_LIST_PROMO_2"] = "Impresionar a sus clientes con su habilidad y enfoque personal.";
$MESS["OL_COMPONENT_LIST_PROMO_3"] = "<b>Consultoría de preventas</b> mediante canales digitales";
$MESS["OL_COMPONENT_LIST_PROMO_4"] = "<b>Varios canales de venta</b> que incluye chat en línea, redes sociales y mensajería instantánea";
$MESS["OL_COMPONENT_LIST_PROMO_5"] = "<b>Atención al cliente</b> (de una y múltiples niveles)";
$MESS["OL_COMPONENT_LIST_PROMO_6"] = "<b>Fuente extra de cables</b> para su CRM";
$MESS["OL_COMPONENT_LIST_PROMO_7"] = "<b>Repetir las ventas</b> utilizando los datos del CRM";
$MESS["OL_COMPONENT_LIST_PROMO_8"] = "Convertirse en líder de la compañía orientado al cliente con Canales Abiertos!";
$MESS["OL_COMPONENT_INDEX_PICTURE"] = "/images/index_en.png";
$MESS["OL_COMPONENT_LIST_ITEM_ACTIVE_BTN_ON"] = "Habilitar";
$MESS["OL_COMPONENT_LIST_ITEM_ACTIVE_BTN_OFF"] = "deshabilitar";
$MESS["OL_COMPONENT_LIST_ACTIONS_VIEW"] = "Ver";
$MESS["OL_COMPONENT_LIST_ACTIONS_EDIT"] = "Editar";
$MESS["COL_COMPONENT_LIST_HIDE_DESC"] = "Ocultar descripción";
?>