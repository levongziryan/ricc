<?
$MESS["OL_COMPONENT_MODULE_NOT_INSTALLED"] = "Das Modul Kommunikationskanäle ist nicht installiert.";
$MESS["OL_COMPONENT_LE_CRM_SOURCE_CREATE"] = "Quelle des Kommunikationskanals";
$MESS["OL_COMPONENT_LE_BOT_LIST"] = "Chat-Bot wurde nicht ausgewählt";
$MESS["OL_COMPONENT_LE_OPTION_FORM"] = "CRM-Formular senden";
$MESS["OL_COMPONENT_LE_OPTION_FORM_ID"] = "CRM-Formular";
$MESS["OL_COMPONENT_LE_OPTION_TEXT"] = "Text senden";
$MESS["OL_COMPONENT_LE_OPTION_QUALITY"] = "Text und Qualitätsbewertung senden";
$MESS["OL_COMPONENT_LE_OPTION_BOT"] = "Chat-Bot verbinden";
$MESS["OL_COMPONENT_LE_OPTION_BOT_ID"] = "Chat-Bot ";
$MESS["OL_COMPONENT_LE_OPTION_QUEUE"] = "Zurück in die Warteschlange";
$MESS["OL_COMPONENT_LE_OPTION_NONE"] = "Keine Aktion erforderlich";
$MESS["OL_COMPONENT_LE_QUICK_ANSWERS_STORAGE_CREATE"] = "Automatisch erstellen";
$MESS["OL_COMPONENT_LE_QUICK_ANSWERS_NO_ACCESS_CREATE"] = "Sie haben nicht genügend Rechte, um diesen Kommunikationskanal zu bearbeiten";
$MESS["OL_COMPONENT_LE_QUICK_ANSWERS_STORAGE_CREATE_ERROR"] = "Fehler beim Erstellen der Liste mit vordefinierten Antworten. Kontaktieren Sie bitte den Technischen Support.";
$MESS["OL_COMPONENT_LE_QUICK_ANSWERS_STORAGE_CREATE_ERROR_UNIQUE"] = "Für diesen Kommunikationskanal existiert bereits eine Liste mit vordefinierten Antworten.";
?>