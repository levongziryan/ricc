<?
$MESS["OL_COMPONENT_MODULE_NOT_INSTALLED"] = "O módulo Canais Abertos não está instalado.";
$MESS["OL_COMPONENT_LE_CRM_SOURCE_CREATE"] = "Fonte Canal Aberto";
$MESS["OL_COMPONENT_LE_OPTION_FORM"] = "Enviar formulário de CRM";
$MESS["OL_COMPONENT_LE_OPTION_TEXT"] = "Enviar texto";
$MESS["OL_COMPONENT_LE_OPTION_QUEUE"] = "Enfileirar";
$MESS["OL_COMPONENT_LE_OPTION_NONE"] = "Não fazer nada";
$MESS["OL_COMPONENT_LE_BOT_LIST"] = "O bot de bate-papo não está selecionado";
$MESS["OL_COMPONENT_LE_OPTION_FORM_ID"] = "Formulário de CRM";
$MESS["OL_COMPONENT_LE_OPTION_BOT"] = "Conectar bor de bate-papo";
$MESS["OL_COMPONENT_LE_OPTION_BOT_ID"] = "Bot de bate-papo ";
$MESS["OL_COMPONENT_LE_OPTION_QUALITY"] = "Enviar texto e avaliação de qualidade";
$MESS["OL_COMPONENT_LE_QUICK_ANSWERS_STORAGE_CREATE"] = "Criar automaticamente";
$MESS["OL_COMPONENT_LE_QUICK_ANSWERS_NO_ACCESS_CREATE"] = "Você não tem permissão para editar este Canal Aberto";
$MESS["OL_COMPONENT_LE_QUICK_ANSWERS_STORAGE_CREATE_ERROR"] = "Erro ao criar lista de Respostas Definidas. Entre em contato com a Assistência Técnica.";
$MESS["OL_COMPONENT_LE_QUICK_ANSWERS_STORAGE_CREATE_ERROR_UNIQUE"] = "Já existe uma lista de Respostas Definidas para este Canal Aberto.";
?>