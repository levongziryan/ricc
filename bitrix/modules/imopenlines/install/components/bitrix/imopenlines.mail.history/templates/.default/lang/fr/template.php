<?
$MESS["IMOL_MAIL_MESSAGE_SYSTEM"] = "Message système";
$MESS["IMOL_MAIL_FORMAT_ERROR"] = "Erreur lors de la création du message.";
$MESS["IMOL_MAIL_WRITE_TO_LINE"] = "Contacter de nouveau";
$MESS["IMOL_MAIL_BACK_TO_TALK"] = "Poursuivre la conversation";
$MESS["IMOL_MAIL_DONT_REPLY"] = "L'e-mail a été envoyé par le système. Veuillez ne pas y répondre.";
?>