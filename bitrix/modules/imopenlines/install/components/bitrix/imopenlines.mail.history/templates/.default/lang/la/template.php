<?
$MESS["IMOL_MAIL_MESSAGE_SYSTEM"] = "Mensaje del sistema";
$MESS["IMOL_MAIL_FORMAT_ERROR"] = "Error al crear el mensaje.";
$MESS["IMOL_MAIL_WRITE_TO_LINE"] = "Contactar de nuevo";
$MESS["IMOL_MAIL_BACK_TO_TALK"] = "Continuar la conversación";
$MESS["IMOL_MAIL_DONT_REPLY"] = "El mensaje de e-mail ha sido enviado por el sistema. Por favor, no responda a este.";
?>