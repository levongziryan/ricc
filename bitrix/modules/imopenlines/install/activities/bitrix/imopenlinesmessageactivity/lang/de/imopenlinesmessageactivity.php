<?
$MESS["IMOL_MA_EMPTY_MESSAGE"] = "Der Parameter 'Nachrichtentext' fehlt.";
$MESS["IMOL_MA_MESSAGE"] = "Nachrichtentext";
$MESS["IMOL_MA_IS_SYSTEM"] = "Ausgeblendete Nachricht (versteckter Modus)";
$MESS["IMOL_MA_IS_SYSTEM_DESCRIPTION"] = "Veröffentlichte Nachricht wird für externen Kontakt nicht sichtbar sein (versteckter Modus)";
$MESS["IMOL_MA_UNSUPPORTED_DOCUMENT"] = "Aktuelles Element unterstützt nicht diesen Aktivitätstyp";
$MESS["IMOL_MA_NO_SESSION_CODE"] = "Es wurden keine Kunden mit verbundenen Kommunikationskanälen gefunden";
$MESS["IMOL_MA_NO_CHAT"] = "Kunde wurde nicht gefunden";
?>