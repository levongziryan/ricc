<?
$MESS["IMOL_MA_EMPTY_MESSAGE"] = "Le paramètre 'Texte du message' n'est pas indiqué.";
$MESS["IMOL_MA_MESSAGE"] = "Texte du message";
$MESS["IMOL_MA_IS_SYSTEM"] = "Message masqué (mode murmure)";
$MESS["IMOL_MA_IS_SYSTEM_DESCRIPTION"] = "Le message publié ne sera pas visible pour le contact externe (mode murmure)";
$MESS["IMOL_MA_UNSUPPORTED_DOCUMENT"] = "L'élément actuel n'est pas compatible avec ce type d’activité";
$MESS["IMOL_MA_NO_SESSION_CODE"] = "Aucun client avec un Canal ouvert connecté n'a été trouvé";
$MESS["IMOL_MA_NO_CHAT"] = "Le chat du client est introuvable";
?>