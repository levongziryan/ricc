<?
$MESS["IMOPENLINES_TAB_SETTINGS"] = "Einstellungen";
$MESS["IMOPENLINES_TAB_TITLE_SETTINGS_2"] = "Moduleinstellungen";
$MESS["IMOPENLINES_ACCOUNT_DEBUG"] = "Debugging-Modus";
$MESS["IMOPENLINES_ACCOUNT_ERROR_PUBLIC"] = "Es wurde eine nicht korrekte öffentliche Adresse angegeben.";
$MESS["IMOPENLINES_ACCOUNT_URL"] = "Öffentliche Adresse der Website";
$MESS["IMOPENLINES_QUICK_IBLOCK_ID"] = "ID des Informationsblocks der vorgefertigten Antworten";
$MESS["IMOPENLINES_QUICK_IBLOCK_ID_EMPTY"] = "Nicht ausgewählt";
?>