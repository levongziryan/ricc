<?
$MESS["DEFAULT_TITLE"] = "Kommunikationskanal";
$MESS["READY_TO_RESPOND"] = "Unsere Mitarbeiter sind gern bereit, Ihnen zu helfen.";
$MESS["RESPOND_LATER"] = "Einer unserer Mitarbeiter wird Ihnen schnellstmöglich antworten.";
$MESS["LOADING_MESSAGE"] = "Schreiben Sie uns eine Nachricht, und wir werden darauf schnellstmöglich antworten.";
$MESS["SONET_ICONS"] = "Bevorzugen Sie andere Messenger?";
$MESS["SONET_ICONS_CLICK"] = "Klicken Sie auf das Icon, um einen Instant Messenger nach Ihrer Wahl zu nutzen";
$MESS["TEXTAREA_HOTKEY"] = "Klicken Sie, um die Tastenkombination zu ändern";
$MESS["TEXTAREA_PLACEHOLDER"] = "Nachricht eingeben";
$MESS["TEXTAREA_SEND"] = "Nachrichten senden";
$MESS["TEXTAREA_FILE"] = "Datei senden";
$MESS["TEXTAREA_SMILE"] = "Emoticon auswählen";
$MESS["POWERED_BY"] = "Erstellt mit";
$MESS["ERROR_TITLE"] = "Onlinechat konnte leider nicht geladen werden.";
$MESS["ERROR_3RD_PARTY_COOKIE_DESC"] = "Stellen Sie bitte Ihren Browser so ein, dass er Cookies der Dritt-Anbieter akzeptiert, oder nutzen Sie andere Komminikationskanäle.";
$MESS["ERROR_INTRANET_USER_DESC"] = "Sie müssen sich aus Ihrem aktuellen Bitrix24 Account #URL# ausloggen, um diesen Chat nutzen zu können.";
$MESS["ERROR_INTRANET_USER_DESC_2"] = "Sie können nicht in diesem Chat schreiben, weil Sie in diesem Bitrix24 in diesem Browser bereits als Mitarbeiter eingeloggt sind. Sie können jedoch in diesem Kommunikationskanal von #URL_START#Ihrem Portal#URL_END# schreiben oder einen der folgenden Kommunikationskanäle nutzen:";
$MESS["ERROR_UNKNOWN"] = "Nutzen Sie bitte einen anderen Kommunikationskanal.";
?>