<?
$MESS["IMOL_TRIAL_S_TITLE"] = "Zugriffsrechte festlegen";
$MESS["IMOL_TRIAL_S_P1"] = "Nur in den Tarifen Standard und Professional können Sie Zugriffsrechte Ihrer Mitarbeiter für Kommunikationskanäle bearbeiten.";
$MESS["IMOL_TRIAL_S_P2"] = "Sie können auswählen:";
$MESS["IMOL_TRIAL_S_F1"] = "Nutzer, die Kommunikationskanäle erstellen können";
$MESS["IMOL_TRIAL_S_F3"] = "Nutzer, die Nachrichtenhistory anzeigen können";
$MESS["IMOL_TRIAL_S_F4"] = "Nutzer, die Kommunikationskanäle konfigurieren und Nachrichten weiterleiten können und vieles andere mehr.";
$MESS["IMOL_ROLE_ADMIN"] = "Administrator";
$MESS["IMOL_ROLE_CHIEF"] = "Geschäftsführer";
$MESS["IMOL_ROLE_DEPARTMENT_HEAD"] = "Abteilungsleiter";
$MESS["IMOL_ROLE_MANAGER"] = "Manager";
?>