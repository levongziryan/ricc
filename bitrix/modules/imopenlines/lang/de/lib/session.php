<?
$MESS["IMOL_SESSION_LINE_IS_CLOSED"] = "Kommunikationskanal ist nicht aktiv, es werden keine Nachrichten an Kunden gesendet.";
$MESS["IMOL_SESSION_SKIP_ALONE"] = "Sie können nicht die Konversation ablehnen, da Sie momentan der einzige in der Warteschleife sind.";
$MESS["IMOL_SESSION_LEAD_ADD"] = "Im CRM wurde ein neuer Lead erstellt: \"#LEAD_NAME#\"";
$MESS["IMOL_SESSION_CRM_FOUND"] = "Nutzer wurde zur CRM-Einheit \"#ENTITY_NAME#\" hinzugefügt";
$MESS["IMOL_SESSION_CLOSE_M"] = "#USER# hat die Sitzung geschlossen";
$MESS["IMOL_SESSION_CLOSE_F"] = "#USER# hat die Sitzung geschlossen";
$MESS["IMOL_SESSION_CLOSE_AUTO"] = "Sitzung automatisch geschlossen";
$MESS["IMOL_SESSION_LEAD_ADD_NEW"] = "Ein neuer Lead wurde erstellt";
$MESS["IMOL_SESSION_LEAD_EXTEND"] = "Kontaktinformationen im Lead gespeichert";
$MESS["IMOL_SESSION_CONTACT_EXTEND"] = "Kontaktinformationen im Kontakt gespeichert";
$MESS["IMOL_SESSION_COMPANY_EXTEND"] = "Kontaktinformationen im Unternehmen gespeichert";
$MESS["IMOL_SESSION_BUTTON_CHANGE"] = "Ändern";
$MESS["IMOL_SESSION_CLOSE_FINAL"] = "Konversation beendet.";
$MESS["IMOL_SESSION_START_SESSION"] = "Konversation ##LINK# gestartet";
$MESS["IMOL_SESSION_REOPEN_SESSION"] = "Konversation ##LINK# wiederaufgenommen";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE"] = "Beim Fortfahren der Nutzung dieses Chats, stimmen Sie der #LINK_START#Verarbeitung Ihrer persönlichen Daten#LINK_END# zu.";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE_NAME"] = "Vor- und Nachname";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE_EMAIL"] = "E-Mail";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE_PHONE"] = "Telefon";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE_PHOTO"] = "Foto";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE_OPERATOR"] = "Sie müssen persönliche Daten der Kunden aus dem CRM entfernen, wenn sie der Verarbeitung ihrer persönlichen Daten nicht zugestimmt haben.";
$MESS["IMOL_SESSION_START_SESSION_BY_MESSAGE"] = "Neue Konversation ##LINK# gestartet (abgezweigt von ##LINK2#)";
?>