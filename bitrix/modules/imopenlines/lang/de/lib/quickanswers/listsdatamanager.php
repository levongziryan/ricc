<?
$MESS["IMOL_QA_IBLOCK_NAME"] = "Vordefinierte Antworten";
$MESS["IMOL_QA_IBLOCK_DESCRIPTION"] = "Bietet vordefinierte Antworten zur Nutzung in Kommunikationskanälen an";
$MESS["IMOL_QA_IBLOCK_ELEMENTS_NAME"] = "Antworten";
$MESS["IMOL_QA_IBLOCK_ELEMENT_NAME"] = "Antwort";
$MESS["IMOL_QA_IBLOCK_ELEMENT_ADD"] = "Antwort hinzufügen";
$MESS["IMOL_QA_IBLOCK_ELEMENT_EDIT"] = "Antwort bearbeiten";
$MESS["IMOL_QA_IBLOCK_ELEMENT_DELETE"] = "Antwort löschen";
$MESS["IMOL_QA_IBLOCK_SECTIONS_NAME"] = "Bereiche";
$MESS["IMOL_QA_IBLOCK_SECTION_NAME"] = "Bereich";
$MESS["IMOL_QA_IBLOCK_SECTION_ADD"] = "Bereich hinzufügen";
$MESS["IMOL_QA_IBLOCK_SECTION_EDIT"] = "Bereich bearbeiten";
$MESS["IMOL_QA_IBLOCK_SECTION_DELETE"] = "Bereich löschen";
$MESS["IMOL_QA_IBLOCK_NAME_FIELD"] = "Name";
$MESS["IMOL_QA_IBLOCK_TEXT_FIELD"] = "Text der vordefinierten Antwort";
$MESS["IMOL_QA_IBLOCK_RATING_FIELD"] = "Ranking";
$MESS["IMOL_QA_IBLOCK_GREETING_SECTION"] = "Begrüßungsnachricht";
$MESS["IMOL_QA_IBLOCK_PAYMENT_SECTION"] = "Bezahlung";
$MESS["IMOL_QA_IBLOCK_DELIVERY_SECTION"] = "Lieferung";
$MESS["IMOL_QA_IBLOCK_COMMON_SECTION"] = "Allgemein";
?>