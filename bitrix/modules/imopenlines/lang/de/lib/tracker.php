<?
$MESS["IMOL_TRACKER_LEAD_ADD"] = "Ein neuer Lead wurde aufgrund der Kontaktinformationen erstellt";
$MESS["IMOL_TRACKER_LEAD_EXTEND"] = "Kontaktinformationen wurden im Lead abgespeichert";
$MESS["IMOL_TRACKER_CONTACT_EXTEND"] = "Kontaktinformationen wurden im Kontakt abgespeichert";
$MESS["IMOL_TRACKER_COMPANY_EXTEND"] = "Kontaktinformationen wurden im Unternehmen abgespeichert";
$MESS["IMOL_TRACKER_BUTTON_CHANGE"] = "Ändern";
$MESS["IMOL_TRACKER_BUTTON_CANCEL"] = "Abbrechen";
$MESS["IMOL_TRACKER_LIMIT_1"] = "Kontaktinformationen wurde im CRM nicht abgespeichert, weil das Erkennungslimit, welches für Ihren aktuellen Tarif festgelegt ist, überschritten wurde. #LINK_START#Tarif upgraden#LINK_END#";
$MESS["IMOL_TRACKER_LIMIT_2"] = "Suche im CRM wurde nicht ausgeführt, weil das monatliche Erkennungslimit, welches für Ihren aktuellen Tarif festgelegt ist, überschritten wurde. #LINK_START#Tarif upgraden#LINK_END#";
$MESS["IMOL_TRACKER_LIMIT_BUTTON"] = "Upgraden";
?>