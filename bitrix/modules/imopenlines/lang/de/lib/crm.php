<?
$MESS["IMOL_CRM_LINE_TYPE_TELEGRAMBOT"] = "Telegram";
$MESS["IMOL_CRM_LINE_TYPE_FACEBOOK"] = "Facebook";
$MESS["IMOL_CRM_LINE_TYPE_VKGROUP"] = "VK";
$MESS["IMOL_CRM_LINE_TYPE_SKYPEBOT"] = "Skype";
$MESS["IMOL_CRM_CREATE_LEAD_COMMENTS"] = "Der Lead wurde erstellt aufgrund einer Anfrage im Kommunikationskanal \"#LINE_NAME#\" via \"#CONNECTOR_NAME#\"";
$MESS["IMOL_CRM_CREATE_ACTIVITY_2"] = "Chat im Kommunikationskanal: \"#LEAD_NAME#\" (#CONNECTOR_NAME#)";
$MESS["IMOL_CRM_LINE_TYPE_LIVECHAT"] = "Onlinechat";
$MESS["IMOL_CRM_LINE_TYPE_NETWORK"] = "Bitrix24.Network";
$MESS["IMOL_CRM_CARD_FULL_NAME"] = "Name";
$MESS["IMOL_CRM_CARD_COMPANY_TITLE"] = "Unternehmen";
$MESS["IMOL_CRM_CARD_POST"] = "Position";
$MESS["IMOL_CRM_CARD_PHONE"] = "Telefon";
$MESS["IMOL_CRM_CARD_EMAIL"] = "E-Mail";
$MESS["IMOL_CRM_LEAD_ADD"] = "Ein neuer Lead wurde aus den Kontaktinformationen erstellt";
$MESS["IMOL_CRM_LEAD_EXTEND"] = "Kontaktinformationen wurden im Lead gespeichert";
$MESS["IMOL_CRM_CONTACT_EXTEND"] = "Kontaktinformationen wurden im Kontakt gespeichert";
$MESS["IMOL_CRM_COMPANY_EXTEND"] = "Kontaktinformationen wurden im Unternehmen gespeichert";
$MESS["IMOL_CRM_BUTTON_CHANGE"] = "Ändern";
$MESS["IMOL_CRM_BUTTON_CANCEL"] = "Abbrechen";
?>