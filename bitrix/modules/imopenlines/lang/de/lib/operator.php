<?
$MESS["IMOL_OPERATOR_ERROR_IM_LOAD"] = "Download des Moduls Instant Messaging ist fehlgeschlagen";
$MESS["IMOL_OPERATOR_ERROR_PULL_LOAD"] = "Download des Moduls Push & Pull ist fehlgeschlagen";
$MESS["IMOL_OPERATOR_ERROR_USER_ID"] = "Die ID des Nutzers ist nicht korrekt";
$MESS["IMOL_OPERATOR_ERROR_CHAT_ID"] = "Die ID des Chats ist nicht korrekt";
$MESS["IMOL_OPERATOR_ERROR_CHAT_TYPE"] = "Dieser Chat ist nicht ein Kommunikationskanal";
$MESS["IMOL_OPERATOR_ERROR_ACCESS_DENIED"] = "Sie haben keinen Zugriff auf diesen Chat";
$MESS["IMOL_OPERATOR_ERROR_CANT_SAVE_QUICK_ANSWER"] = "Fehler beim Speichern einer vordefinierten Antwort";
?>