<?
$MESS["IMOL_CONFIG_LINE_NAME"] = "Kommunikationskanal #NAME#";
$MESS["IMOL_CONFIG_WELCOME_MESSAGE"] = "Willkommen im Kommunikationskanal von [b]#COMPANY_NAME#![/b][br]Warten Sie bitte Augenblick, Ihre Anfrage wird bald beantwortet.";
$MESS["IMOL_CONFIG_NO_ANSWER"] = "Leider sind zurzeit alle Mitarbeiter besetzt, warten Sie bitte noch einen Augenblick.";
$MESS["IMOL_CONFIG_WORKTIME_DAYOFF_2"] = "Momentan können wir Ihnen leider nicht antworten.[br][br]Geben Sie bitte Ihre Frage ein, wir werden uns so schnell wie möglich mit Ihnen in Verbindung setzen.";
$MESS["IMOL_CONFIG_WORKTIME_DAYOFF_3"] = "Willkommen im Kommunikationskanal von [b]#COMPANY_NAME#![/b][br]Momentan können wir Ihnen leider nicht antworten.[br][br]Geben Sie bitte Ihre Frage ein, wir werden uns so schnell wie möglich mit Ihnen in Verbindung setzen.";
$MESS["IMOL_CONFIG_CLOSE_TEXT"] = "Danke für Ihre Anfrage. Bewerten Sie bitte die Qualität unserer Antworten.";
$MESS["IMOL_CONFIG_CLOSE_TEXT_2"] = "Danke für Ihre Anfrage.";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_1_TEXT"] = "Bewerten Sie bitte unseren Service.";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_1_LIKE"] = "Danke!";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_1_DISLIKE"] = "Es tut uns leid, dass wir Ihnen nicht helfen konnten. Ihre Meinung hilft uns aber, unseren Service zu verbessern.";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_2_TEXT"] = "Teilen Sie uns bitte Ihre Bewertung mit.[br][br]Senden Sie eine 1, wenn Sie zufrieden sind, oder ein 0, wenn unzufrieden.";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_2_LIKE"] = "Danke!";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_2_DISLIKE"] = "Es tut uns leid, dass wir Ihnen nicht helfen konnten. Ihre Meinung hilft uns aber, unseren Service zu verbessern.";
$MESS["IMOL_ADD_ERROR"] = "Fehler beim Erstellen eines Kommunikationskanals";
$MESS["IMOL_UPDATE_ERROR"] = "Fehler beim Aktualisieren des Kommunikationskanals";
?>