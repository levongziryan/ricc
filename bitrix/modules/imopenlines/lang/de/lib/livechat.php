<?
$MESS["IMOL_LC_GUEST_NAME"] = "Besucher";
$MESS["IMOL_LC_CHAT_NAME_COLOR_GUEST"] = "#COLOR# Besucher ##NUMBER#";
$MESS["IMOL_LC_CHAT_NAME_GUEST"] = "Besucher ##NUMBER#";
$MESS["IMOL_LC_CHAT_NAME"] = "#USER_NAME# - #LINE_NAME#";
$MESS["IMOL_LC_GENERATE_KEY"] = "Schlüssel für einen Chat im Kommunikationskanal wurde generiert";
$MESS["IMOL_LC_GUEST_URL"] = "Website-Seite";
?>