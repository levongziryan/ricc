<?
$MESS["IMOL_MAIL_HISTORY_TITLE"] = "Ihre Konversation ##SESSION_ID# mit dem Mitarbeiter auf #SITE_URL#";
$MESS["IMOL_MAIL_HISTORY_ACTION_TITLE"] = "Danke dafür, dass Sie uns kontaktiert haben";
$MESS["IMOL_MAIL_HISTORY_ACTION_DESC"] = "Ihre Konversation ##SESSION_ID# mit dem Mitarbeiter auf #SITE_URL#";
$MESS["IMOL_MAIL_ANSWER_ACTION_TITLE"] = "Danke dafür, dass Sie uns kontaktiert haben";
$MESS["IMOL_MAIL_AUTHOR_YOU"] = "Sie";
$MESS["IMOL_MAIL_FILE"] = "Datei";
$MESS["IMOL_MAIL_TIME_FORMAT"] = "H:i";
$MESS["IMOL_MAIL_DATETIME_FORMAT"] = "d. F, H:i";
?>