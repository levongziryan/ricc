<?
$MESS["IMOL_LCC_ERROR_IM_LOAD"] = "Fehler bei der Initialisierung des Moduls Instant Messenger.";
$MESS["IMOL_LCC_ERROR_PULL_LOAD"] = "Fehler bei der Initialisierung des Moduls Push & Pull.";
$MESS["IMOL_LCC_ERROR_USER_ID"] = "ID des Nutzers ist ungültig.";
$MESS["IMOL_LCC_ERROR_CHAT_ID"] = "ID des Chats ist ungültig.";
$MESS["IMOL_LCC_ERROR_CHAT_TYPE"] = "Dieser Chat ist nicht ein Kommunikationskanal";
$MESS["IMOL_LCC_ERROR_FORM_ID"] = "ID des Formulars wurde nicht übermittelt";
$MESS["IMOL_LCC_GUEST_NAME"] = "Besucher";
$MESS["IMOL_LCC_FORM_SUBMIT"] = "Formular ausgefüllt";
$MESS["IMOL_LCC_FORM_HISTORY"] = "Kunde hat ein Protokoll der Konversation ##LINK# angefordert";
$MESS["IMOL_LCC_FORM_NAME"] = "Name";
$MESS["IMOL_LCC_FORM_EMAIL"] = "E-Mail";
$MESS["IMOL_LCC_FORM_PHONE"] = "Telefon";
$MESS["IMOL_LCC_FORM_NONE"] = "keine";
$MESS["IMOL_LCC_ERROR_ACCESS_DENIED"] = "Sie haben nicht genügend Rechte, um auf diese Konversation zuzugreifen.";
$MESS["IMOL_LCC_FORM_HISTORY_2"] = "Konversationshistory ##LINK# wurde an den Kunden auf seine Anfrage hin gesendet";
?>