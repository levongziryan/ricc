<?
$MESS["IMOL_HISTORY_LOG_NAME"] = "Protokoll der Konversation im Kommunikationskanal";
$MESS["IMOL_OPERATOR_ANSWER_NAME"] = "Nachrichten des Kunden und Antworten des Mitarbeiters in der Sitzung des Kommunikationskanals";
$MESS["IMOL_MAIL_PARAMS_DESC"] = "#EMAIL_TO# - E-Mail des Empfängers
#TITLE# - Betreff der Nachricht
#TEMPLATE_SESSION_ID# -ID der Sitzung
#TEMPLATE_ACTION_TITLE# - Aktionsüberschrift
#TEMPLATE_ACTION_DESC# - Aktionsbeschreibung
#TEMPLATE_WIDGET_DOMAIN# - Domain der Widget-Website
#TEMPLATE_WIDGET_URL# - URL der Seite des Widgets
#TEMPLATE_LINE_NAME# - Name des Kommunikationskanals
";
?>