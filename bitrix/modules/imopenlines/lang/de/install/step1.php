<?
$MESS["IMOPENLINES_PUBLIC_PATH_DESC"] = "Eine öffentliche Adresse der Website ist erforderlich, damit die Kommunikationskanäle korrekt funktionieren.";
$MESS["IMOPENLINES_PUBLIC_PATH_DESC_2"] = "Ist der externe Zugriff auf Ihr Netzwerk begrenzt, aktivieren Sie den Zugriff nur für bestimmte Seiten. Details dazu entnehmen Sie bitte der #LINK_START#Dokumentation#LINK_END#.";
$MESS["IMOPENLINES_PUBLIC_PATH"] = "Öffentliche Adresse der Website:";
?>