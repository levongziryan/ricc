<?
$MESS["IMOL_QA_IBLOCK_NAME"] = "Respostas definidas";
$MESS["IMOL_QA_IBLOCK_DESCRIPTION"] = "Contém respostas definidas para uso com Canais Abertos";
$MESS["IMOL_QA_IBLOCK_ELEMENTS_NAME"] = "Respostas";
$MESS["IMOL_QA_IBLOCK_ELEMENT_NAME"] = "Resposta";
$MESS["IMOL_QA_IBLOCK_ELEMENT_ADD"] = "Adicionar resposta";
$MESS["IMOL_QA_IBLOCK_ELEMENT_EDIT"] = "Editar resposta";
$MESS["IMOL_QA_IBLOCK_ELEMENT_DELETE"] = "Excluir resposta";
$MESS["IMOL_QA_IBLOCK_SECTIONS_NAME"] = "Seções";
$MESS["IMOL_QA_IBLOCK_SECTION_NAME"] = "Seção";
$MESS["IMOL_QA_IBLOCK_SECTION_ADD"] = "Adicionar seção";
$MESS["IMOL_QA_IBLOCK_SECTION_EDIT"] = "Editar seção";
$MESS["IMOL_QA_IBLOCK_SECTION_DELETE"] = "Excluir seção";
$MESS["IMOL_QA_IBLOCK_NAME_FIELD"] = "Nome";
$MESS["IMOL_QA_IBLOCK_TEXT_FIELD"] = "Texto da resposta definida";
$MESS["IMOL_QA_IBLOCK_RATING_FIELD"] = "Classificação";
$MESS["IMOL_QA_IBLOCK_GREETING_SECTION"] = "Mensagem de boas-vindas";
$MESS["IMOL_QA_IBLOCK_PAYMENT_SECTION"] = "Pagamento";
$MESS["IMOL_QA_IBLOCK_DELIVERY_SECTION"] = "Entrega";
$MESS["IMOL_QA_IBLOCK_COMMON_SECTION"] = "Comum";
?>