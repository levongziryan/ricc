<?
$MESS["IMOL_CONFIG_LINE_NAME"] = "Canal Aberto #NAME#";
$MESS["IMOL_CONFIG_WELCOME_MESSAGE"] = "Bem-vindo ao Canal Aberto da [b]#COMPANY_NAME#[/b].[br]Você receberá uma resposta em breve, aguarde.";
$MESS["IMOL_CONFIG_NO_ANSWER"] = "Infelizmente, no momento, não podemos atender sua solicitação. Daremos retorno assim que recebermos sua mensagem.";
$MESS["IMOL_CONFIG_WORKTIME_DAYOFF_2"] = "Infelizmente, não podemos falar com você no momento.[br][br]Digite sua pergunta e daremos retorno assim que possível.";
$MESS["IMOL_CONFIG_CLOSE_TEXT"] = "Obrigado por entrar em contato conosco! Comente o que achou do nosso serviço.";
$MESS["IMOL_ADD_ERROR"] = "Erro ao criar Canal Aberto";
$MESS["IMOL_UPDATE_ERROR"] = "Erro ao atualizar Canal Aberto";
$MESS["IMOL_CONFIG_WORKTIME_DAYOFF"] = "Infelizmente, no momento, não podemos atender sua solicitação. Daremos retorno dentro do horário comercial assim que possível.";
$MESS["IMOL_CONFIG_WORKTIME_DAYOFF_3"] = "Bem-vindo ao Canal Aberto [b]#COMPANY_NAME #![/b] ![br]Infelizmente, não podemos falar com você no momento.[br][br]Digite sua pergunta e daremos retorno assim que possível.";
$MESS["IMOL_CONFIG_CLOSE_TEXT_2"] = "Obrigado por fazer negócios conosco.";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_1_TEXT"] = "Diga-nos como nos saímos.";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_1_LIKE"] = "Obrigado!";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_1_DISLIKE"] = "Lamentamos que não pudemos ajudá-lo. Sua opinião nos ajudará a melhorar o nosso serviço.";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_2_TEXT"] = "Diga-nos como nos saímos.[br][br]Basta enviar 1, se você estiver satisfeito, ou 0, se poderíamos fazer melhor.";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_2_LIKE"] = "Obrigado!";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_2_DISLIKE"] = "Lamentamos que não pudemos ajudá-lo. Sua opinião nos ajudará a melhorar o nosso serviço.";
?>