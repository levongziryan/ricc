<?
$MESS["IMOL_CRM_LINE_TYPE_TELEGRAMBOT"] = "Telegram";
$MESS["IMOL_CRM_LINE_TYPE_FACEBOOK"] = "Facebook";
$MESS["IMOL_CRM_LINE_TYPE_VKGROUP"] = "VK";
$MESS["IMOL_CRM_LINE_TYPE_SKYPEBOT"] = "Skype";
$MESS["IMOL_CRM_CREATE_LEAD_COMMENTS"] = "O cliente potencial foi criado com base no envio do cliente para \"#LINE_NAME#\" por meio de \"#CONNECTOR_NAME\"#";
$MESS["IMOL_CRM_CREATE_ACTIVITY"] = "Bate-papo do Canal Aberto: \"#LEAD_NAME#\"";
$MESS["IMOL_CRM_LINE_TYPE_LIVECHAT"] = "Bate-papo ao vivo";
$MESS["IMOL_CRM_LINE_TYPE_NETWORK"] = "Bitrix24.Network";
$MESS["IMOL_CRM_CARD_FULL_NAME"] = "Nome";
$MESS["IMOL_CRM_CARD_COMPANY_TITLE"] = "Empresa";
$MESS["IMOL_CRM_CARD_POST"] = "Cargo";
$MESS["IMOL_CRM_CARD_PHONE"] = "Telefone";
$MESS["IMOL_CRM_CARD_EMAIL"] = "Login (e-mail)";
$MESS["IMOL_CRM_LEAD_ADD"] = "Um novo cliente potencial foi criado usando as informações de contato";
$MESS["IMOL_CRM_LEAD_EXTEND"] = "Informações de contato salvas no cliente potencial";
$MESS["IMOL_CRM_CONTACT_EXTEND"] = "Informações de contato salvas no contato";
$MESS["IMOL_CRM_COMPANY_EXTEND"] = "Informações de contato salvas na empresa";
$MESS["IMOL_CRM_BUTTON_CHANGE"] = "Alterar";
$MESS["IMOL_CRM_BUTTON_CANCEL"] = "Cancelar";
$MESS["IMOL_CRM_CREATE_ACTIVITY_2"] = "Bate-papo do Canal Aberto: \"#LEAD_NAME#\" (#CONNECTOR_NAME#)";
?>