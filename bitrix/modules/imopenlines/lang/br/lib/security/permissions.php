<?
$MESS["IMOL_SECURITY_ENTITY_LINES"] = "Canais Abertos";
$MESS["IMOL_SECURITY_ENTITY_CONNECTORS"] = "Conectar canais de comunicação";
$MESS["IMOL_SECURITY_ENTITY_SESSION"] = "Estatísticas de comunicação";
$MESS["IMOL_SECURITY_ENTITY_HISTORY"] = "Histórico de comunicação";
$MESS["IMOL_SECURITY_ENTITY_SETTINGS"] = "Parâmetros comuns";
$MESS["IMOL_SECURITY_ENTITY_JOIN"] = "Participar da conversa";
$MESS["IMOL_SECURITY_ACTION_VIEW"] = "Visualizar";
$MESS["IMOL_SECURITY_ACTION_MODIFY"] = "Editar";
$MESS["IMOL_SECURITY_ACTION_PERFORM"] = "Executar";
$MESS["IMOL_SECURITY_PERMISSION_NONE"] = "Acesso negado.";
$MESS["IMOL_SECURITY_PERMISSION_SELF"] = "Pessoal";
$MESS["IMOL_SECURITY_PERMISSION_DEPARTMENT"] = "Pessoal e departamento";
$MESS["IMOL_SECURITY_PERMISSION_ANY"] = "Qualquer";
$MESS["IMOL_SECURITY_PERMISSION_ALLOW"] = "Acesso concedido";
$MESS["IMOL_SECURITY_ENTITY_VOTE_HEAD"] = "Avaliação da conversa";
?>