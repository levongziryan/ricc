<?
$MESS["IMOL_TRIAL_S_TITLE"] = "Atribuir permissões de acesso";
$MESS["IMOL_TRIAL_S_P1"] = "Você pode editar as permissões de acesso ao Canal Aberto de seus funcionários apenas nos planos Standard e Professional.";
$MESS["IMOL_TRIAL_S_P2"] = "Você pode selecionar:";
$MESS["IMOL_TRIAL_S_F1"] = "pessoas que podem criar Canais Abertos";
$MESS["IMOL_TRIAL_S_F3"] = "pessoas que podem visualizar histórico de mensagens";
$MESS["IMOL_TRIAL_S_F4"] = "pessoas que podem configurar canais de comunicação e encaminhamento de mensagens, e muitos mais.";
$MESS["IMOL_ROLE_ADMIN"] = "Administrador";
$MESS["IMOL_ROLE_CHIEF"] = "Diretor executivo";
$MESS["IMOL_ROLE_DEPARTMENT_HEAD"] = "Chefe do departamento";
$MESS["IMOL_ROLE_MANAGER"] = "Gerente";
?>