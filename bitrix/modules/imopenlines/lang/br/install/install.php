<?
$MESS["IMOPENLINES_MODULE_NAME"] = "Canais Abertos";
$MESS["IMOPENLINES_MODULE_DESCRIPTION"] = "O módulo dos Canais Abertos.";
$MESS["IMOPENLINES_INSTALL_TITLE"] = "Instalação do Módulo \"Canais Abertos\"";
$MESS["IMOPENLINES_UNINSTALL_TITLE"] = "Desinstalação do Módulo \"Canais Abertos\"";
$MESS["IMOPENLINES_UNINSTALL_QUESTION"] = "Tem certeza de que deseja excluir o módulo?";
$MESS["IMOPENLINES_CHECK_PULL"] = "O módulo \"Push and Pull\" não está instalado ou o servidor da fila não está configurado.";
$MESS["IMOPENLINES_CHECK_CONNECTOR"] = "O módulo \"Conectores IM Externos\" não está instalado.";
$MESS["IMOPENLINES_CHECK_IM"] = "O módulo \"Instant Messenger\" não está instalado.";
$MESS["IMOPENLINES_CHECK_IM_VERSION"] = "Atualizar o módulo \"Instant Messenger\" para versão 16.5.0";
$MESS["IMOPENLINES_DB_NOT_SUPPORTED"] = "Este módulo é compatível somente com MySQL.";
$MESS["IMOPENLINES_CHECK_PUBLIC_PATH"] = "Nenhum endereço público correto especificado.";
?>