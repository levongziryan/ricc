<?
$MESS["DEFAULT_TITLE"] = "Canal Aberto";
$MESS["READY_TO_RESPOND"] = "Nossa equipe de profissionais qualificados está aqui para ajudar!";
$MESS["RESPOND_LATER"] = "Um de nossos representantes vai lhe dar retorno assim que possível.";
$MESS["LOADING_MESSAGE"] = "Envie-nos uma mensagem e responderemos assim que possível.";
$MESS["SONET_ICONS"] = "Você se sente mais confortável com outros messengers?";
$MESS["SONET_ICONS_CLICK"] = "clique no ícone para utilizar um instant messenger de sua escolha";
$MESS["TEXTAREA_HOTKEY"] = "Clique para alterar o atalho de teclado";
$MESS["TEXTAREA_PLACEHOLDER"] = "Inserir mensagem";
$MESS["TEXTAREA_SEND"] = "Enviar Mensagem";
$MESS["TEXTAREA_FILE"] = "Enviar Arquivo";
$MESS["TEXTAREA_SMILE"] = "Selecionar emoticon";
$MESS["POWERED_BY"] = "Fornecido por";
$MESS["ERROR_TITLE"] = "Infelizmente, não conseguimos carregar o bate-papo ao vivo.";
$MESS["ERROR_3RD_PARTY_COOKIE_DESC"] = "Configure o seu navegador para aceitar cookies de terceiros ou utilize outros canais de comunicação.";
$MESS["ERROR_INTRANET_USER_DESC"] = "Para usar este bate-papo, você precisa sair da sua atual conta Bitrix24: #URL#";
$MESS["ERROR_INTRANET_USER_DESC_2"] = "Você não pode escrever neste bate-papo, porque você já entrou neste Bitrix24 neste navegador como funcionário. No entanto, você pode postar neste Canal Aberto do #URL_START#seu portal#URL_END# ou usar um destes canais de comunicação:";
$MESS["ERROR_UNKNOWN"] = "Utilize outros meios de comunicação.";
?>