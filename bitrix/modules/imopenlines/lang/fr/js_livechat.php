<?
$MESS["DEFAULT_TITLE"] = "Canal ouvert";
$MESS["READY_TO_RESPOND"] = "Notre équipe de professionnels qualifiés est là pour vous aider !";
$MESS["RESPOND_LATER"] = "Un de nos représentants reviendra vers vous le plus vite possible.";
$MESS["LOADING_MESSAGE"] = "Écrivez-nous et nous vous répondrons dès que possible.";
$MESS["SONET_ICONS"] = "Vous vous sentez plus à l'aise avec d'autres services de messagerie ?";
$MESS["SONET_ICONS_CLICK"] = "cliquez sur l’icône pour utiliser une messagerie instantanée de votre choix";
$MESS["TEXTAREA_HOTKEY"] = "Cliquez pour changer le raccourci clavier";
$MESS["TEXTAREA_PLACEHOLDER"] = "Entrer un message";
$MESS["TEXTAREA_SEND"] = "Envoyer un message";
$MESS["TEXTAREA_FILE"] = "Envoyer le fichier";
$MESS["TEXTAREA_SMILE"] = "Sélectionner le smiley";
$MESS["POWERED_BY"] = "Fourni par";
$MESS["ERROR_TITLE"] = "Malheureusement, nous n'avons pas réussi à charger le chat live.";
$MESS["ERROR_3RD_PARTY_COOKIE_DESC"] = "Configurez votre navigateur pour accepter les cookies tiers ou utilisez d’autres canaux de communication.";
$MESS["ERROR_INTRANET_USER_DESC"] = "Afin de pouvoir utiliser ce chat, vous devez être déconnecté de votre compte Bitrix24 actuel : #URL#";
$MESS["ERROR_INTRANET_USER_DESC_2"] = "Vous ne pouvez pas écrire dans ce chat, car vous êtes déjà connecté à ce Bitrix24 dans ce navigateur en tant qu'employé. Toutefois, vous pouvez publier ce Canal ouvert depuis #URL_START#votre portail#URL_END# ou utiliser l’un des canaux de communication suivants :";
$MESS["ERROR_UNKNOWN"] = "Veuillez utiliser d’autres moyens de communication.";
?>