<?
$MESS["IMOPENLINES_TAB_SETTINGS"] = "Paramètres";
$MESS["IMOPENLINES_TAB_TITLE_SETTINGS_2"] = "Paramètres du module";
$MESS["IMOPENLINES_ACCOUNT_DEBUG"] = "Mode de débogage";
$MESS["IMOPENLINES_ACCOUNT_ERROR_PUBLIC"] = "Aucune adresse publique correcte spécifiée.";
$MESS["IMOPENLINES_ACCOUNT_URL"] = "Adresse public du site";
$MESS["IMOPENLINES_QUICK_IBLOCK_ID"] = "ID du bloc d'information des réponses préfabriquées";
$MESS["IMOPENLINES_QUICK_IBLOCK_ID_EMPTY"] = "Non sélectionné";
?>