<?
$MESS["IMOL_LC_GUEST_NAME"] = "Invité";
$MESS["IMOL_LC_CHAT_NAME_COLOR_GUEST"] = "#COLOR# invité ##NUMBER#";
$MESS["IMOL_LC_CHAT_NAME_GUEST"] = "Invité ##NUMBER#";
$MESS["IMOL_LC_CHAT_NAME"] = "#USER_NAME# - #LINE_NAME#";
$MESS["IMOL_LC_GENERATE_KEY"] = "La clé du chat du Canal ouvert a été générée";
$MESS["IMOL_LC_GUEST_URL"] = "Page du site";
?>