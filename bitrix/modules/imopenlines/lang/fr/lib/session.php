<?
$MESS["IMOL_SESSION_LINE_IS_CLOSED"] = "Le Canal ouvert est actif, aucun message ne sera envoyé au client.";
$MESS["IMOL_SESSION_SKIP_ALONE"] = "Vous ne pouvez refuser la conversation parce que vous êtes actuellement le seul dans la file d'attente.";
$MESS["IMOL_SESSION_LEAD_ADD"] = "Nouvelle piste créée dans la GRC : \"#LEAD_NAME#\"";
$MESS["IMOL_SESSION_CRM_FOUND"] = "L'utilisateur a été ajouté à l'entité GRC : \"#ENTITY_NAME#\"";
$MESS["IMOL_SESSION_CLOSE_M"] = "#USER# a fermé la session";
$MESS["IMOL_SESSION_CLOSE_F"] = "#USER# a fermé la session";
$MESS["IMOL_SESSION_CLOSE_AUTO"] = "La session s'est fermée automatiquement";
$MESS["IMOL_SESSION_LEAD_ADD_NEW"] = "Une nouvelle piste a été créée";
$MESS["IMOL_SESSION_LEAD_EXTEND"] = "Les informations de contact ont été enregistrées dans la piste";
$MESS["IMOL_SESSION_CONTACT_EXTEND"] = "Les informations de contact ont été enregistrées dans le contact";
$MESS["IMOL_SESSION_COMPANY_EXTEND"] = "Les informations de contact ont été enregistrées dans la société";
$MESS["IMOL_SESSION_BUTTON_CHANGE"] = "Modifier";
$MESS["IMOL_SESSION_CLOSE_FINAL"] = "Conversation fermée.";
$MESS["IMOL_SESSION_START_SESSION"] = "La conversation ##LINK# a démarré";
$MESS["IMOL_SESSION_REOPEN_SESSION"] = "La conversation ##LINK# a repris";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE"] = "En continuer d'utiliser ce chat, vous consentez au #LINK_START#traitement de vos données personnelles#LINK_END#.";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE_NAME"] = "Nom et prénoms";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE_EMAIL"] = "E-mail";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE_PHONE"] = "Numéro de téléphone";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE_PHOTO"] = "Photo";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE_OPERATOR"] = "Vous devez supprimer les données personnelles du client depuis la GRC s'il n'accepte pas leur traitement.";
$MESS["IMOL_SESSION_START_SESSION_BY_MESSAGE"] = "Nouvelle conversation ##LINK# démarrée (dérivée de ##LINK2#)";
?>