<?
$MESS["IMOL_CRM_LINE_TYPE_TELEGRAMBOT"] = "Telegram";
$MESS["IMOL_CRM_LINE_TYPE_FACEBOOK"] = "Facebook";
$MESS["IMOL_CRM_LINE_TYPE_VKGROUP"] = "VK";
$MESS["IMOL_CRM_LINE_TYPE_SKYPEBOT"] = "Skype";
$MESS["IMOL_CRM_CREATE_LEAD_COMMENTS"] = "Le client potentiel a été créé sur base de l'envoi du client à \"#LINE_NAME#\" via \"#CONNECTOR_NAME#\"";
$MESS["IMOL_CRM_CREATE_ACTIVITY"] = "Chat du Canal ouvert : \"#LEAD_NAME#\"";
$MESS["IMOL_CRM_LINE_TYPE_LIVECHAT"] = "Chat live";
$MESS["IMOL_CRM_LINE_TYPE_NETWORK"] = "Bitrix24.Network";
$MESS["IMOL_CRM_CARD_FULL_NAME"] = "Nom";
$MESS["IMOL_CRM_CARD_COMPANY_TITLE"] = "Entreprise";
$MESS["IMOL_CRM_CARD_POST"] = "Fonction";
$MESS["IMOL_CRM_CARD_PHONE"] = "Numéro de téléphone";
$MESS["IMOL_CRM_CARD_EMAIL"] = "Login (e-mail)";
$MESS["IMOL_CRM_LEAD_ADD"] = "Un nouveau client potentiel a été créé à l’aide des informations de contact";
$MESS["IMOL_CRM_LEAD_EXTEND"] = "Informations de contact enregistrées pour le client potentiel";
$MESS["IMOL_CRM_CONTACT_EXTEND"] = "Informations de contact enregistrées pour le contact";
$MESS["IMOL_CRM_COMPANY_EXTEND"] = "Informations de contact enregistrées pour l’entreprise";
$MESS["IMOL_CRM_BUTTON_CHANGE"] = "Modifier";
$MESS["IMOL_CRM_BUTTON_CANCEL"] = "Annuler";
$MESS["IMOL_CRM_CREATE_ACTIVITY_2"] = "Chat du Canal ouvert \"#LEAD_NAME#\" (#CONNECTOR_NAME#)";
?>