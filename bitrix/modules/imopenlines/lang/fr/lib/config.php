<?
$MESS["IMOL_CONFIG_LINE_NAME"] = "Canal ouvert #NAME#";
$MESS["IMOL_CONFIG_WELCOME_MESSAGE"] = "Bienvenue dans le Canal ouvert [b]#COMPANY_NAME#[/b]![br]Vous recevrez une réponse rapidement, veuillez patienter.";
$MESS["IMOL_CONFIG_NO_ANSWER"] = "Malheureusement, nous ne pouvons pas répondre à votre demande à l'heure actuelle. Nous vous contacterons dès que nous aurons reçu votre message.";
$MESS["IMOL_CONFIG_WORKTIME_DAYOFF_2"] = "Malheureusement, nous sommes actuellement pas en mesure discuter avec vous.[br][br]Veuillez entrer votre question et nous vous répondrons dès que possible.";
$MESS["IMOL_CONFIG_CLOSE_TEXT"] = "Merci de nous avoir contactés! Dites-nous ce que vous pensez de notre service.";
$MESS["IMOL_ADD_ERROR"] = "Erreur de création du Canal ouvert";
$MESS["IMOL_UPDATE_ERROR"] = "Erreur de misse à jour du Canal ouvert";
$MESS["IMOL_CONFIG_WORKTIME_DAYOFF"] = "Malheureusement, nous ne pouvons pas répondre à votre demande à l'heure actuelle. Nous vous contacterons pendant les heures de bureau dès que possible.";
$MESS["IMOL_CONFIG_WORKTIME_DAYOFF_3"] = "Bienvenue dans le Canal ouvert de [b]#COMPANY_NAME#![/b] ![br]Malheureusement, nous ne pouvons pas vous parler pour le moment.[br][br]Entrez votre question et nous reviendrons vers vous le plus rapidement possible.";
$MESS["IMOL_CONFIG_CLOSE_TEXT_2"] = "Nous vous remercions pour votre collaboration.";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_1_TEXT"] = "Veuillez évaluer notre service.";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_1_LIKE"] = "Merci !";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_1_DISLIKE"] = "Nous sommes désolés de ne pas avoir réussi à vous aider. Votre opinion nous aidera à améliorer notre service.";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_2_TEXT"] = "Veuillez évaluer notre service.[br][br]Envoyez 1 si vous êtes satisfait, ou 0 si nous pouvons faire mieux.";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_2_LIKE"] = "Merci !";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_2_DISLIKE"] = "Nous sommes désolés de ne pas avoir réussi à vous aider. Votre opinion nous aidera à améliorer notre service.";
?>