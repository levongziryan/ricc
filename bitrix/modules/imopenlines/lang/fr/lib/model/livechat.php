<?
$MESS["LIVECHAT_ENTITY_CONFIG_ID_FIELD"] = "ID de configuration";
$MESS["LIVECHAT_ENTITY_URL_CODE_FIELD"] = "Code URL (interne)";
$MESS["LIVECHAT_ENTITY_URL_CODE_ID_FIELD"] = "ID de code URL (interne)";
$MESS["LIVECHAT_ENTITY_URL_CODE_PUBLIC_FIELD"] = "Code URL";
$MESS["LIVECHAT_ENTITY_URL_CODE_PUBLIC_ID_FIELD"] = "ID de code URL";
$MESS["LIVECHAT_ENTITY_TEMPLATE_ID_FIELD"] = "Type de template";
$MESS["LIVECHAT_ENTITY_BACKGROUND_IMAGE_FIELD"] = "ID d'arrière-plan";
$MESS["LIVECHAT_ENTITY_CSS_ACTIVE_FIELD"] = "CSS actif";
$MESS["LIVECHAT_ENTITY_CSS_PATH_FIELD"] = "Chemin de fichier CSS";
$MESS["LIVECHAT_ENTITY_CSS_TEXT_FIELD"] = "Code CSS";
$MESS["LIVECHAT_ENTITY_COPYRIGHT_REMOVED_FIELD"] = "Autoriser la suppression du copyright";
$MESS["LIVECHAT_ENTITY_CACHE_WIDGET_ID_FIELD"] = "ID de fichier du cache du widget";
$MESS["LIVECHAT_ENTITY_CACHE_BUTTON_ID_FIELD"] = "ID de fichier cache du bouton";
?>