<?
$MESS["TRACKER_ENTITY_ID_FIELD"] = "ID";
$MESS["TRACKER_ENTITY_SESSION_ID_FIELD"] = "ID de la session";
$MESS["TRACKER_ENTITY_CHAT_ID_FIELD"] = "ID du chat";
$MESS["TRACKER_ENTITY_MESSAGE_ID_FIELD"] = "ID du message";
$MESS["TRACKER_ENTITY_MESSAGE_ORIGIN_ID_FIELD"] = "ID du message source";
$MESS["TRACKER_ENTITY_USER_ID_FIELD"] = "ID de l'utilisateur";
$MESS["TRACKER_ENTITY_ACTION_FIELD"] = "Action";
$MESS["TRACKER_ENTITY_CRM_ENTITY_TYPE_FIELD"] = "Type d'entité GRC";
$MESS["TRACKER_ENTITY_CRM_ENTITY_ID_FIELD"] = "ID d'entité GRC";
$MESS["TRACKER_ENTITY_FIELD_TYPE_FIELD"] = "Type de champ";
$MESS["TRACKER_ENTITY_FIELD_VALUE_FIELD"] = "Valeur du champ";
$MESS["TRACKER_ENTITY_DATE_CREATE_FIELD"] = "Date de création";
?>