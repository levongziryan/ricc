<?
$MESS["IMOL_UI_HELPER_STAT_INDEX"] = "Ajouter les statistiques au module d'index de recherche";
$MESS["IMOL_UI_HELPER_ENTITY_SEL_BTN"] = "Sélectionner";
$MESS["IMOL_UI_HELPER_ENTITY_SEL_SEARCH"] = "Rechercher";
$MESS["IMOL_UI_HELPER_SEL_SEARCH_NO_RESULT"] = "Aucune entrée n'a été trouvée.";
$MESS["IMOL_UI_HELPER_ENTITY_SEL_LAST"] = "Dernier";
?>