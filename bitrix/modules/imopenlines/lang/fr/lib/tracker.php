<?
$MESS["IMOL_TRACKER_LEAD_ADD"] = "Une nouvelle piste a été créée à partir des informations de contact";
$MESS["IMOL_TRACKER_LEAD_EXTEND"] = "Les informations de contact ont été enregistrées dans la piste";
$MESS["IMOL_TRACKER_CONTACT_EXTEND"] = "Les informations de contact ont été enregistrées dans le contact";
$MESS["IMOL_TRACKER_COMPANY_EXTEND"] = "Les informations de contact ont été enregistrées dans la société";
$MESS["IMOL_TRACKER_BUTTON_CHANGE"] = "Modifier";
$MESS["IMOL_TRACKER_BUTTON_CANCEL"] = "Annuler";
$MESS["IMOL_TRACKER_LIMIT_1"] = "Les informations de contact n'ont pas été enregistrées dans la GRC parce que vous avez dépassé la limite de reconnaissance de votre offre.
#LINK_START#Changez d'offre maintenant#LINK_END#";
$MESS["IMOL_TRACKER_LIMIT_2"] = "La recherche GRC n'a pu être effectuée parce que vous avez dépassé la limite de reconnaissance mensuelle de votre offre. #LINK_START#Changez d'offre maintenant#LINK_END#";
$MESS["IMOL_TRACKER_LIMIT_BUTTON"] = "Changez d'offre";
?>