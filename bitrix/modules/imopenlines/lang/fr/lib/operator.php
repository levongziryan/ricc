<?
$MESS["IMOL_OPERATOR_ERROR_IM_LOAD"] = "Erreur lors de l'initialisation du module Messagerie instantanée.";
$MESS["IMOL_OPERATOR_ERROR_PULL_LOAD"] = "Erreur lors de l'initialisation du module Push & Pull.";
$MESS["IMOL_OPERATOR_ERROR_USER_ID"] = "ID d'utilisateur non valide.";
$MESS["IMOL_OPERATOR_ERROR_CHAT_ID"] = "ID du chat non valide.";
$MESS["IMOL_OPERATOR_ERROR_CHAT_TYPE"] = "Ce chat n'est pas un Canal ouvert";
$MESS["IMOL_OPERATOR_ERROR_ACCESS_DENIED"] = "Vous ne disposez pas des permissions pour accéder à ce chat.";
$MESS["IMOL_OPERATOR_ERROR_CANT_SAVE_QUICK_ANSWER"] = "Erreur lors de l'enregistrement de la réponse préfabriquée";
?>