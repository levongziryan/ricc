<?
$MESS["IMOL_LCC_ERROR_IM_LOAD"] = "Erreur lors de l'initialisation du module Messagerie instantanée.";
$MESS["IMOL_LCC_ERROR_PULL_LOAD"] = "Erreur lors de l'initialisation du module Push & Pull.";
$MESS["IMOL_LCC_ERROR_USER_ID"] = "ID d'utilisateur non valide.";
$MESS["IMOL_LCC_ERROR_CHAT_ID"] = "ID du chat non valide.";
$MESS["IMOL_LCC_ERROR_CHAT_TYPE"] = "Ce chat n'est pas un Canal ouvert";
$MESS["IMOL_LCC_ERROR_FORM_ID"] = "L'ID du formulaire n'a pas été envoyée";
$MESS["IMOL_LCC_GUEST_NAME"] = "Invité";
$MESS["IMOL_LCC_FORM_SUBMIT"] = "Formulaire envoyé";
$MESS["IMOL_LCC_FORM_HISTORY"] = "Le client a demandé le journal de la conversation ##LINK#";
$MESS["IMOL_LCC_FORM_NAME"] = "Nom";
$MESS["IMOL_LCC_FORM_EMAIL"] = "E-mail";
$MESS["IMOL_LCC_FORM_PHONE"] = "Numéro de téléphone";
$MESS["IMOL_LCC_FORM_NONE"] = "aucun";
$MESS["IMOL_LCC_ERROR_ACCESS_DENIED"] = "Vous ne disposez pas des permissions suffisantes pour accéder à cette conversation.";
$MESS["IMOL_LCC_FORM_HISTORY_2"] = "Un historique de la conversation ##LINK# a été envoyé au client à sa demande";
?>