<?
$MESS["IMOL_SECURITY_ENTITY_LINES"] = "CANAUX OUVERTS";
$MESS["IMOL_SECURITY_ENTITY_CONNECTORS"] = "Connecter les canaux de communication";
$MESS["IMOL_SECURITY_ENTITY_SESSION"] = "Statistiques de communication";
$MESS["IMOL_SECURITY_ENTITY_HISTORY"] = "Historique de communication";
$MESS["IMOL_SECURITY_ENTITY_SETTINGS"] = "Paramètres communs";
$MESS["IMOL_SECURITY_ENTITY_JOIN"] = "Rejoindre la conversation";
$MESS["IMOL_SECURITY_ACTION_VIEW"] = "Afficher";
$MESS["IMOL_SECURITY_ACTION_MODIFY"] = "Modifier";
$MESS["IMOL_SECURITY_ACTION_PERFORM"] = "Exécuter";
$MESS["IMOL_SECURITY_PERMISSION_NONE"] = "Accès refusé.";
$MESS["IMOL_SECURITY_PERMISSION_SELF"] = "Personnel";
$MESS["IMOL_SECURITY_PERMISSION_DEPARTMENT"] = "Personnel et département";
$MESS["IMOL_SECURITY_PERMISSION_ANY"] = "N'importe quel";
$MESS["IMOL_SECURITY_PERMISSION_ALLOW"] = "Accès accordé";
$MESS["IMOL_SECURITY_ENTITY_VOTE_HEAD"] = "Évaluation de la conversation";
?>