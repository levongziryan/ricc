<?
$MESS["IMOL_TRIAL_S_TITLE"] = "Assigner des autorisations d'accès";
$MESS["IMOL_TRIAL_S_P1"] = "Vous pouvez modifier les autorisations d'accès au Canal ouvert pour vos employés uniquement dans les abonnements Standard et Professional.";
$MESS["IMOL_TRIAL_S_P2"] = "Vous pouvez sélectionner:";
$MESS["IMOL_TRIAL_S_F1"] = "personnes pouvant créer des Canaux ouverts";
$MESS["IMOL_TRIAL_S_F3"] = "personnes pouvant voir l'historique des messages";
$MESS["IMOL_TRIAL_S_F4"] = "personnes pouvant configurer des canaux de communication et l'acheminement de messages, et bien plus encore.";
$MESS["IMOL_ROLE_ADMIN"] = "Administrateur";
$MESS["IMOL_ROLE_CHIEF"] = "Directeur général";
$MESS["IMOL_ROLE_DEPARTMENT_HEAD"] = "Responsable de département";
$MESS["IMOL_ROLE_MANAGER"] = "Manager";
?>