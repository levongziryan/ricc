<?
$MESS["IMOL_MAIL_HISTORY_TITLE"] = "Votre conversation ##SESSION_ID# avec un représentant sur #SITE_URL#";
$MESS["IMOL_MAIL_HISTORY_ACTION_TITLE"] = "Merci d'avoir pris contact avec nous";
$MESS["IMOL_MAIL_HISTORY_ACTION_DESC"] = "Votre conversation ##SESSION_ID# avec un représentant sur #SITE_URL#";
$MESS["IMOL_MAIL_ANSWER_ACTION_TITLE"] = "Merci de nous avoir contactés";
$MESS["IMOL_MAIL_AUTHOR_YOU"] = "Vous";
$MESS["IMOL_MAIL_FILE"] = "Fichier";
$MESS["IMOL_MAIL_TIME_FORMAT"] = "g:i a";
$MESS["IMOL_MAIL_DATETIME_FORMAT"] = "F d, g:i a";
?>