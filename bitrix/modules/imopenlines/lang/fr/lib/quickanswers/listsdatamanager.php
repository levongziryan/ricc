<?
$MESS["IMOL_QA_IBLOCK_NAME"] = "Réponses prédéfinies";
$MESS["IMOL_QA_IBLOCK_DESCRIPTION"] = "Contient des réponses prédéfinies à utiliser avec les Canaux ouverts";
$MESS["IMOL_QA_IBLOCK_ELEMENTS_NAME"] = "Réponses";
$MESS["IMOL_QA_IBLOCK_ELEMENT_NAME"] = "Réponse";
$MESS["IMOL_QA_IBLOCK_ELEMENT_ADD"] = "Ajouter une réponse";
$MESS["IMOL_QA_IBLOCK_ELEMENT_EDIT"] = "Modifier la réponse";
$MESS["IMOL_QA_IBLOCK_ELEMENT_DELETE"] = "Supprimer la réponse";
$MESS["IMOL_QA_IBLOCK_SECTIONS_NAME"] = "Sections";
$MESS["IMOL_QA_IBLOCK_SECTION_NAME"] = "Section";
$MESS["IMOL_QA_IBLOCK_SECTION_ADD"] = "Ajouter une section";
$MESS["IMOL_QA_IBLOCK_SECTION_EDIT"] = "Modifier la section";
$MESS["IMOL_QA_IBLOCK_SECTION_DELETE"] = "Supprimer la section";
$MESS["IMOL_QA_IBLOCK_NAME_FIELD"] = "Prénom";
$MESS["IMOL_QA_IBLOCK_TEXT_FIELD"] = "Texte de la réponse prédéfinie";
$MESS["IMOL_QA_IBLOCK_RATING_FIELD"] = "Évaluation";
$MESS["IMOL_QA_IBLOCK_GREETING_SECTION"] = "Message de bienvenue";
$MESS["IMOL_QA_IBLOCK_PAYMENT_SECTION"] = "Paiement";
$MESS["IMOL_QA_IBLOCK_DELIVERY_SECTION"] = "Livraison";
$MESS["IMOL_QA_IBLOCK_COMMON_SECTION"] = "Commun";
?>