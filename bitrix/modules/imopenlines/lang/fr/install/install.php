<?
$MESS["IMOPENLINES_MODULE_NAME"] = "Canaux ouverts";
$MESS["IMOPENLINES_MODULE_DESCRIPTION"] = "Le module Canaux ouverts.";
$MESS["IMOPENLINES_INSTALL_TITLE"] = "Installation du module \"Canaux ouverts\"";
$MESS["IMOPENLINES_UNINSTALL_TITLE"] = "Désinstallation du module \"Canaux ouverts\"";
$MESS["IMOPENLINES_UNINSTALL_QUESTION"] = "Voulez-vous vraiment supprimer le module ?";
$MESS["IMOPENLINES_CHECK_PULL"] = "Le module \"Push and Pull\" n'est pas installé ou le serveur de file d'attente n'est pas configuré.";
$MESS["IMOPENLINES_CHECK_CONNECTOR"] = "Le module \"Connecteurs de messagerie instantanée externe\" n'est pas installé.";
$MESS["IMOPENLINES_CHECK_IM"] = "Le module \"Messagerie instantanée\" n'est pas installé.";
$MESS["IMOPENLINES_CHECK_IM_VERSION"] = "Veuillez mettre à jour le module \"Messagerie instantanée\" pour la version 16.5.0";
$MESS["IMOPENLINES_DB_NOT_SUPPORTED"] = "Ce module est uniquement compatible avec MySQL.";
$MESS["IMOPENLINES_CHECK_PUBLIC_PATH"] = "Aucune adresse publique correcte spécifiée.";
?>