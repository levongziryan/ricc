<?
$MESS["IMOPENLINES_PUBLIC_PATH_DESC"] = "Une adresse de site public est nécessaire pour que le Canal ouvert fonctionne correctement.";
$MESS["IMOPENLINES_PUBLIC_PATH_DESC_2"] = "Si l'accès externe à votre réseau est restreint, activez-le seulement pour certaines pages. Veuillez consulter la #LINK_START#documentation#LINK_END# pour plus de détails.";
$MESS["IMOPENLINES_PUBLIC_PATH"] = "Adresse public du site :";
?>