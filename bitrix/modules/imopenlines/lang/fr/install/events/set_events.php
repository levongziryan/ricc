<?
$MESS["IMOL_HISTORY_LOG_NAME"] = "Journal des conversations du Canal ouvert";
$MESS["IMOL_OPERATOR_ANSWER_NAME"] = "Les messages des clients et les réponses du représentant lors d'une session Canal ouvert";
$MESS["IMOL_MAIL_PARAMS_DESC"] = "#EMAIL_TO# - E-mail du destinataire
#TITLE# - Objet du message
#TEMPLATE_SESSION_ID# - ID de la session
#TEMPLATE_ACTION_TITLE# - Intitulé de l'action
#TEMPLATE_ACTION_DESC# - Description de l'action
#TEMPLATE_WIDGET_DOMAIN# - Domaine du site du widget
#TEMPLATE_WIDGET_URL# - URL de la page du widget
#TEMPLATE_LINE_NAME# - Nom du Canal ouvert";
?>