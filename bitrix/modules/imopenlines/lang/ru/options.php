<?
$MESS["IMOPENLINES_TAB_SETTINGS"] = "Настройки";
$MESS["IMOPENLINES_TAB_TITLE_SETTINGS_2"] = "Параметры модуля";
$MESS["IMOPENLINES_ACCOUNT_DEBUG"] = "Режим отладки";
$MESS["IMOPENLINES_ACCOUNT_ERROR_PUBLIC"] = "Вы указали не корректный публичный адрес";
$MESS["IMOPENLINES_ACCOUNT_URL"] = "Публичный адрес сайта";
$MESS["IMOPENLINES_QUICK_IBLOCK_ID"] = "Инфоблок для хранения быстрых ответов";
$MESS["IMOPENLINES_QUICK_IBLOCK_ID_EMPTY"] = "Не выбрано";
?>