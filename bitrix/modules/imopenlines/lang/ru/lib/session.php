<?
$MESS["IMOL_SESSION_LINE_IS_CLOSED"] = "Открытая линия не активна, сообщения не будут отправлены клиенту.";
$MESS["IMOL_SESSION_SKIP_ALONE"] = "Вы не можете отказаться от диалога, так как в данный момент в очереди больше никого нет.";
$MESS["IMOL_SESSION_LEAD_ADD"] = "В CRM был создан лид \"#LEAD_NAME#\"";
$MESS["IMOL_SESSION_CRM_FOUND"] = "Пользователь добавлен к CRM записи: \"#ENTITY_NAME#\"";
$MESS["IMOL_SESSION_CLOSE_M"] = "#USER# завершил работу с диалогом";
$MESS["IMOL_SESSION_CLOSE_F"] = "#USER# завершила работу с диалогом";
$MESS["IMOL_SESSION_CLOSE_AUTO"] = "Автоматическое завершение диалога";
$MESS["IMOL_SESSION_CLOSE_FINAL"] = "Диалог завершен.";
$MESS["IMOL_SESSION_LEAD_ADD_NEW"] = "Создан новый лид";
$MESS["IMOL_SESSION_LEAD_EXTEND"] = "Контактная информация сохранена в лид";
$MESS["IMOL_SESSION_CONTACT_EXTEND"] = "Контактная информация сохранена в контакт";
$MESS["IMOL_SESSION_COMPANY_EXTEND"] = "Контактная информация сохранена в компанию";
$MESS["IMOL_SESSION_BUTTON_CHANGE"] = "Сменить";
$MESS["IMOL_SESSION_START_SESSION"] = "Начат новый диалог №#LINK#";
$MESS["IMOL_SESSION_START_SESSION_BY_MESSAGE"] = "Начат новый диалог №#LINK# (выделен из диалога №#LINK2#)";
$MESS["IMOL_SESSION_REOPEN_SESSION"] = "Переоткрыт диалог №#LINK#";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE"] = "Продолжая общение в этом чате, вы даете согласие на #LINK_START#обработку персональных данных.#LINK_END#";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE_NAME"] = "Имя и Фамилия";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE_EMAIL"] = "Адрес электронной почты";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE_PHONE"] = "Телефон";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE_PHOTO"] = "Фотография";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE_OPERATOR"] = "Если клиент не дал согласие на обработку своих персональных данных, вам необходимо удалить его данные из CRM.";
?>