<?
$MESS["IMOL_SESSION_LINE_IS_CLOSED"] = "Відкрита лінія не активна, повідомлення не будуть відправлені клієнту.";
$MESS["IMOL_SESSION_LEAD_ADD"] = "В CRM був створений лід \"#LEAD_NAME#\"";
$MESS["IMOL_SESSION_SKIP_ALONE"] = "Ви не можете відмовитися від діалогу, так як в даний момент в черзі більше нікого немає.";
$MESS["IMOL_SESSION_CLOSE_M"] = "#USER# завершив роботу з діалогом";
$MESS["IMOL_SESSION_CLOSE_F"] = "#USER# завершила роботу з діалогом";
$MESS["IMOL_SESSION_CLOSE_AUTO"] = "Автоматичне завершення діалогу";
$MESS["IMOL_SESSION_CRM_FOUND"] = "Користувач доданий до CRM запису: \"#ENTITY_NAME#\"";
$MESS["IMOL_SESSION_LEAD_ADD_NEW"] = "Створено новий лід";
$MESS["IMOL_SESSION_LEAD_EXTEND"] = "Контактна інформація збережена в лід";
$MESS["IMOL_SESSION_CONTACT_EXTEND"] = "Контактна інформація збережена в контакт";
$MESS["IMOL_SESSION_COMPANY_EXTEND"] = "Контактна інформація збережена в компанію";
$MESS["IMOL_SESSION_BUTTON_CHANGE"] = "Змінити";
$MESS["IMOL_SESSION_CLOSE_FINAL"] = "Діалог завершено.";
$MESS["IMOL_SESSION_START_SESSION"] = "Розпочато новий діалог №#LINK#";
$MESS["IMOL_SESSION_REOPEN_SESSION"] = "Перевідкрито діалог №#LINK#";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE"] = "Продовжуючи спілкування в цьому чаті, ви даєте згоду на #LINK_START#обробку персональних даних.#LINK_END#";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE_NAME"] = "Ім'я та прізвище";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE_EMAIL"] = "Адреса електронної пошти";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE_PHONE"] = "Телефон";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE_PHOTO"] = "Фотографія";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE_OPERATOR"] = "Якщо клієнт не дав згоду на обробку своїх персональних даних, вам необхідно видалити його дані з CRM.";
$MESS["IMOL_SESSION_START_SESSION_BY_MESSAGE"] = "Розпочато новий діалог №#LINK# (виділений з діалогу  №#LINK2#)";
?>