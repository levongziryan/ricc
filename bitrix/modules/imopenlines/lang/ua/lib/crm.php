<?
$MESS["IMOL_CRM_LINE_TYPE_TELEGRAMBOT"] = "Telegram";
$MESS["IMOL_CRM_LINE_TYPE_FACEBOOK"] = "Facebook";
$MESS["IMOL_CRM_LINE_TYPE_VKGROUP"] = "ВКонтакте";
$MESS["IMOL_CRM_LINE_TYPE_SKYPEBOT"] = "Skype";
$MESS["IMOL_CRM_CREATE_LEAD_COMMENTS"] = "Лід був створений на основі звернення користувача у відкриту лінію \"#LINE_NAME#\" через канал \"#CONNECTOR_NAME#\"";
$MESS["IMOL_CRM_CREATE_ACTIVITY_2"] = "Чат відкритої лінії - \"#LEAD_NAME#\" (#CONNECTOR_NAME#)";
$MESS["IMOL_CRM_LINE_TYPE_LIVECHAT"] = "Онлайн-чат";
$MESS["IMOL_CRM_LINE_TYPE_NETWORK"] = "Бітрікс24.Network";
$MESS["IMOL_CRM_CARD_FULL_NAME"] = "Ім'я";
$MESS["IMOL_CRM_CARD_COMPANY_TITLE"] = "Компанія";
$MESS["IMOL_CRM_CARD_POST"] = "Посада";
$MESS["IMOL_CRM_CARD_PHONE"] = "Телефон";
$MESS["IMOL_CRM_CARD_EMAIL"] = "Email";
$MESS["IMOL_CRM_LEAD_ADD"] = "На основі контактної інформації створено новий лід";
$MESS["IMOL_CRM_LEAD_EXTEND"] = "Контактна інформація збережена в лід";
$MESS["IMOL_CRM_CONTACT_EXTEND"] = "Контактна інформація збережена в контакт";
$MESS["IMOL_CRM_COMPANY_EXTEND"] = "Контактна інформація збережена в компанію";
$MESS["IMOL_CRM_BUTTON_CHANGE"] = "Змінити";
$MESS["IMOL_CRM_BUTTON_CANCEL"] = "Скасувати";
?>