<?
$MESS["LIVECHAT_ENTITY_CONFIG_ID_FIELD"] = "ID налаштування";
$MESS["LIVECHAT_ENTITY_URL_CODE_FIELD"] = "Код посилання (внутрішній)";
$MESS["LIVECHAT_ENTITY_URL_CODE_ID_FIELD"] = "Ідентифікатор коду посилання (внутрішній)";
$MESS["LIVECHAT_ENTITY_URL_CODE_PUBLIC_FIELD"] = "Код посилання";
$MESS["LIVECHAT_ENTITY_URL_CODE_PUBLIC_ID_FIELD"] = "Ідентифікатор коду посилання";
$MESS["LIVECHAT_ENTITY_TEMPLATE_ID_FIELD"] = "Тип шаблону";
$MESS["LIVECHAT_ENTITY_BACKGROUND_IMAGE_FIELD"] = "Ідентифікатор фону";
$MESS["LIVECHAT_ENTITY_CSS_ACTIVE_FIELD"] = "Прапор активності CSS";
$MESS["LIVECHAT_ENTITY_CSS_PATH_FIELD"] = "Шлях до CSS файлу";
$MESS["LIVECHAT_ENTITY_CSS_TEXT_FIELD"] = "Текст CSS";
$MESS["LIVECHAT_ENTITY_COPYRIGHT_REMOVED_FIELD"] = "Дозвіл на відключення підпису";
$MESS["LIVECHAT_ENTITY_CACHE_WIDGET_ID_FIELD"] = "ID файлу кешу для віджету";
$MESS["LIVECHAT_ENTITY_CACHE_BUTTON_ID_FIELD"] = "ID файлу кешу для кнопки";
?>