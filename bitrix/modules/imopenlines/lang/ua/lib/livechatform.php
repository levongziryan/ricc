<?
$MESS["IMOL_LCC_ERROR_IM_LOAD"] = "Помилка завантаження модуля Веб-месенджер";
$MESS["IMOL_LCC_ERROR_PULL_LOAD"] = "Помилка завантаження модуля Push & Pull";
$MESS["IMOL_LCC_ERROR_USER_ID"] = "Зазначений не коректний ідентифікатор користувача";
$MESS["IMOL_LCC_ERROR_CHAT_ID"] = "Зазначений не коректний ідентифікатор чату";
$MESS["IMOL_LCC_ERROR_CHAT_TYPE"] = "Зазначений чат не є відкритою лінією";
$MESS["IMOL_LCC_ERROR_FORM_ID"] = "Не переданий ідентифікатор форми";
$MESS["IMOL_LCC_GUEST_NAME"] = "Гість";
$MESS["IMOL_LCC_FORM_SUBMIT"] = "Заповнена форма";
$MESS["IMOL_LCC_FORM_HISTORY"] = "Клієнт запитав історію діалогу №#LINK#";
$MESS["IMOL_LCC_FORM_NAME"] = "Ім'я";
$MESS["IMOL_LCC_FORM_EMAIL"] = "E-mail";
$MESS["IMOL_LCC_FORM_PHONE"] = "Телефон";
$MESS["IMOL_LCC_FORM_NONE"] = "не заповнено";
$MESS["IMOL_LCC_ERROR_ACCESS_DENIED"] = "Ви не можете відкрити цю розмову тому, що у вас недостатньо прав.";
$MESS["IMOL_LCC_FORM_HISTORY_2"] = "За запитом клієнта була відправлена історія діалогу №#LINK#";
?>