<?
$MESS["IMOL_TRIAL_S_TITLE"] = "Розподіл прав доступу";
$MESS["IMOL_TRIAL_S_P1"] = "Розподіл прав доступу ваших співробітників до роботи з відкритими лініями доступно в тарифах «Команда» і «Компанія».";
$MESS["IMOL_TRIAL_S_P2"] = "Ви самі зможете вибрати:";
$MESS["IMOL_TRIAL_S_F1"] = "хто може створювати відкриті лінії";
$MESS["IMOL_TRIAL_S_F3"] = "хто може переглядати історію розмов";
$MESS["IMOL_TRIAL_S_F4"] = "хто має право налаштовувати канали комунікацій, маршрутизацію повідомлень і багато чого іншого.";
$MESS["IMOL_ROLE_ADMIN"] = "Адміністратор";
$MESS["IMOL_ROLE_CHIEF"] = "Директор";
$MESS["IMOL_ROLE_DEPARTMENT_HEAD"] = "Начальник відділу";
$MESS["IMOL_ROLE_MANAGER"] = "Менеджер";
?>