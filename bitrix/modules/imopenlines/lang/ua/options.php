<?
$MESS["IMOPENLINES_TAB_SETTINGS"] = "Налаштування";
$MESS["IMOPENLINES_TAB_TITLE_SETTINGS_2"] = "Параметри модуля";
$MESS["IMOPENLINES_ACCOUNT_DEBUG"] = "Режим налагодження";
$MESS["IMOPENLINES_ACCOUNT_ERROR_PUBLIC"] = "Ви не вказали коректну публічну адресу";
$MESS["IMOPENLINES_ACCOUNT_URL"] = "Публічна адреса сайту";
$MESS["IMOPENLINES_QUICK_IBLOCK_ID"] = "Інфоблок для зберігання швидких відповідей";
$MESS["IMOPENLINES_QUICK_IBLOCK_ID_EMPTY"] = "Не обрано";
?>