<?
$MESS["IMOPENLINES_TAB_SETTINGS"] = "Ajustes";
$MESS["IMOPENLINES_TAB_TITLE_SETTINGS_2"] = "Ajustes del módulo";
$MESS["IMOPENLINES_ACCOUNT_DEBUG"] = "Modo de depuración";
$MESS["IMOPENLINES_ACCOUNT_ERROR_PUBLIC"] = "No se ha especificado una dirección pública correcta.";
$MESS["IMOPENLINES_ACCOUNT_URL"] = "Dirección pública de su sitio web";
$MESS["IMOPENLINES_QUICK_IBLOCK_ID"] = "ID del block de información de respuestas predeterminadas";
$MESS["IMOPENLINES_QUICK_IBLOCK_ID_EMPTY"] = "No seleccionado";
?>