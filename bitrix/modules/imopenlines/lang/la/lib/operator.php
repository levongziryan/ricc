<?
$MESS["IMOL_OPERATOR_ERROR_IM_LOAD"] = "Error al inicializar el módulo de mensajería instantánea.";
$MESS["IMOL_OPERATOR_ERROR_PULL_LOAD"] = "Error al inicializar el módulo Push & Pull";
$MESS["IMOL_OPERATOR_ERROR_USER_ID"] = "ID de usuario no válido.";
$MESS["IMOL_OPERATOR_ERROR_CHAT_ID"] = "ID de chat no válido.";
$MESS["IMOL_OPERATOR_ERROR_CHAT_TYPE"] = "Este chat no está en canal abierto";
$MESS["IMOL_OPERATOR_ERROR_ACCESS_DENIED"] = "Usted no tiene permiso para acceder a este chat.";
$MESS["IMOL_OPERATOR_ERROR_CANT_SAVE_QUICK_ANSWER"] = "Error al guardar la respuesta predeterminada";
?>