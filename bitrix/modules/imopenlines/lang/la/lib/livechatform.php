<?
$MESS["IMOL_LCC_ERROR_IM_LOAD"] = "Error al inicializar el módulo de Instant Messenger.";
$MESS["IMOL_LCC_ERROR_PULL_LOAD"] = "Error al inicializar el módulo Push & Pull.";
$MESS["IMOL_LCC_ERROR_USER_ID"] = "El ID de usuario no es válido.";
$MESS["IMOL_LCC_ERROR_CHAT_ID"] = "El ID de chat no es válido.";
$MESS["IMOL_LCC_ERROR_CHAT_TYPE"] = "Este chat no es Canal Abierto";
$MESS["IMOL_LCC_ERROR_FORM_ID"] = "No se envió el ID del formulario";
$MESS["IMOL_LCC_GUEST_NAME"] = "Invitados";
$MESS["IMOL_LCC_FORM_SUBMIT"] = "Formulario enviado";
$MESS["IMOL_LCC_FORM_HISTORY"] = "Registro de conversación solicitado por el cliente ##LINK#";
$MESS["IMOL_LCC_FORM_NAME"] = "Nombre";
$MESS["IMOL_LCC_FORM_EMAIL"] = "E-mail";
$MESS["IMOL_LCC_FORM_PHONE"] = "Teléfono";
$MESS["IMOL_LCC_FORM_NONE"] = "ninguna";
$MESS["IMOL_LCC_ERROR_ACCESS_DENIED"] = "Usted no tienes suficientes permisos para acceder a esta conversación.";
$MESS["IMOL_LCC_FORM_HISTORY_2"] = "Se ha enviado un historial de la conversación ##LINK# al cliente según su solicitud";
?>