<?
$MESS["IMOL_SECURITY_ENTITY_LINES"] = "Canales Abiertos";
$MESS["IMOL_SECURITY_ENTITY_CONNECTORS"] = "Conectar canales de comunicación";
$MESS["IMOL_SECURITY_ENTITY_SESSION"] = "Estadísticas de comunicación";
$MESS["IMOL_SECURITY_ENTITY_HISTORY"] = "Historial de la comunicación";
$MESS["IMOL_SECURITY_ENTITY_SETTINGS"] = "Parámetros comunes";
$MESS["IMOL_SECURITY_ENTITY_JOIN"] = "Únete a la conversación";
$MESS["IMOL_SECURITY_ACTION_VIEW"] = "Ver";
$MESS["IMOL_SECURITY_ACTION_MODIFY"] = "Editar";
$MESS["IMOL_SECURITY_ACTION_PERFORM"] = "Ejecutar";
$MESS["IMOL_SECURITY_PERMISSION_NONE"] = "Acceso denegado.";
$MESS["IMOL_SECURITY_PERMISSION_SELF"] = "Personal";
$MESS["IMOL_SECURITY_PERMISSION_DEPARTMENT"] = "Personal y departamento";
$MESS["IMOL_SECURITY_PERMISSION_ANY"] = "Cualquiera";
$MESS["IMOL_SECURITY_PERMISSION_ALLOW"] = "Acceso permitido";
$MESS["IMOL_SECURITY_ENTITY_VOTE_HEAD"] = "Calificación de conversación";
?>