<?
$MESS["IMOL_TRIAL_S_TITLE"] = "Asignar permisos de acceso";
$MESS["IMOL_TRIAL_S_P1"] = "Puede editar los permisos de acceso del Canal Abierto para sus empleados sólo en planes Standard y Professional.";
$MESS["IMOL_TRIAL_S_P2"] = "Puede seleccionar:";
$MESS["IMOL_TRIAL_S_F1"] = "personas que pueden crear canales abiertos";
$MESS["IMOL_TRIAL_S_F3"] = "personas que pueden ver el historial de mensajes";
$MESS["IMOL_TRIAL_S_F4"] = "personas que pueden configurar canales de comunicación y enrutamiento de mensajes, y muchos más.";
$MESS["IMOL_ROLE_ADMIN"] = "Administrator";
$MESS["IMOL_ROLE_CHIEF"] = "Director ejecutivo";
$MESS["IMOL_ROLE_DEPARTMENT_HEAD"] = "Jefe del Departamento";
$MESS["IMOL_ROLE_MANAGER"] = "Gerente";
?>