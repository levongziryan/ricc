<?
$MESS["IMOL_LCC_ERROR_IM_LOAD"] = "Error al inicializar el módulo de Instant Messenger.";
$MESS["IMOL_LCC_ERROR_PULL_LOAD"] = "Error al inicializar el módulo de Push & Pull.";
$MESS["IMOL_LCC_ERROR_USER_ID"] = "El ID de usuario no es válido.";
$MESS["IMOL_LCC_ERROR_CHAT_ID"] = "El ID del Chat no es válido.";
$MESS["IMOL_LCC_ERROR_CHAT_TYPE"] = "Este chat no es un Canal Abierto";
$MESS["IMOL_LCC_ERROR_FORM_ID"] = "El ID del formulario no fue enviado";
$MESS["IMOL_LCC_GUEST_NAME"] = "Invitado";
$MESS["IMOL_LCC_FORM_SUBMIT"] = "Formulario enviado";
$MESS["IMOL_LCC_FORM_HISTORY"] = "Cliente solicita el registro de la conversación ##LINK#";
$MESS["IMOL_LCC_FORM_NAME"] = "Nombre";
$MESS["IMOL_LCC_FORM_EMAIL"] = "E-mail";
$MESS["IMOL_LCC_FORM_PHONE"] = "Teléfono";
$MESS["IMOL_LCC_FORM_NONE"] = "ninguno";
$MESS["IMOL_LCC_ERROR_ACCESS_DENIED"] = "No tiene suficientes permisos para acceder a esta conversación.";
$MESS["IMOL_LCC_FORM_HISTORY_2"] = "Un historial de la conversación ##LINK# se ha enviado al cliente según su solicitud";
?>