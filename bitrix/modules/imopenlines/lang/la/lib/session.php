<?
$MESS["IMOL_SESSION_LINE_IS_CLOSED"] = "El canal abierto está inactivo, no se enviarán mensajes al cliente.";
$MESS["IMOL_SESSION_SKIP_ALONE"] = "No puedes rechazar una conversación porque actualmente es el único en la cola.";
$MESS["IMOL_SESSION_LEAD_ADD"] = "Nuevo prospecto creado el CRM: \"#LEAD_NAME#\"";
$MESS["IMOL_SESSION_CRM_FOUND"] = "Se agregó usuario a la entidad del CRM: \"#ENTITY_NAME#\"";
$MESS["IMOL_SESSION_CLOSE_M"] = "#USER# ha cerrado sesión";
$MESS["IMOL_SESSION_CLOSE_F"] = "#USER# ha cerrado sesión";
$MESS["IMOL_SESSION_CLOSE_AUTO"] = "Cerrar automáticamente la sesión";
$MESS["IMOL_SESSION_LEAD_ADD_NEW"] = "Se creó un nuevo prospecto";
$MESS["IMOL_SESSION_LEAD_EXTEND"] = "Información de contacto guardada en el prospecto";
$MESS["IMOL_SESSION_CONTACT_EXTEND"] = "Información de contacto guardada en el contacto";
$MESS["IMOL_SESSION_COMPANY_EXTEND"] = "Información de contacto guardada en la compañía";
$MESS["IMOL_SESSION_BUTTON_CHANGE"] = "Cambiar";
$MESS["IMOL_SESSION_CLOSE_FINAL"] = "Conversación cerrada.";
$MESS["IMOL_SESSION_START_SESSION"] = "Conversación ##LINK# comenzada";
$MESS["IMOL_SESSION_REOPEN_SESSION"] = "Conversación ##LINK# resumida";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE"] = "Al continuar usando este chat, usted acepta #LINK_START#procesamiento de sus datos personales#LINK_END#.";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE_NAME"] = "Nombre y apellidos";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE_EMAIL"] = "E-mail";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE_PHONE"] = "Teléfono";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE_PHOTO"] = "Foto";
$MESS["IMOL_SESSION_AGREEMENT_MESSAGE_OPERATOR"] = "Debe eliminar los datos personales del cliente del CRM si no consienten el procesamiento de sus datos personales.";
$MESS["IMOL_SESSION_START_SESSION_BY_MESSAGE"] = "Nueva conversación ##LINK# iniciar (bifurcado desde ##LINK2#)";
?>