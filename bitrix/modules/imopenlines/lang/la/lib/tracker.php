<?
$MESS["IMOL_TRACKER_LEAD_ADD"] = "Se creó un nuevo prospecto utilizando la información de contacto";
$MESS["IMOL_TRACKER_LEAD_EXTEND"] = "Información de contacto guardada en el prospecto";
$MESS["IMOL_TRACKER_CONTACT_EXTEND"] = "Información de contacto guardada en el contacto";
$MESS["IMOL_TRACKER_COMPANY_EXTEND"] = "Información de contacto guardada en la compañía";
$MESS["IMOL_TRACKER_BUTTON_CHANGE"] = "Cambiar";
$MESS["IMOL_TRACKER_BUTTON_CANCEL"] = "Cancelar";
$MESS["IMOL_TRACKER_LIMIT_1"] = "La información de contacto no se guardó en el CRM porque se excedió el límite de reconocimiento estipulado por su plan actual. #LINK_START#Upgrade now#LINK_END#";
$MESS["IMOL_TRACKER_LIMIT_2"] = "La búsqueda del CRM no se realizó porque se excedió el límite de reconocimiento mensual estipulado por su plan actual. #LINK_START#Upgrade now#LINK_END#";
$MESS["IMOL_TRACKER_LIMIT_BUTTON"] = "Mejorar";
?>