<?
$MESS["IMOL_MAIL_HISTORY_TITLE"] = "Su conversacion ##SESSION_ID# con el representante el #SITE_URL#";
$MESS["IMOL_MAIL_HISTORY_ACTION_TITLE"] = "Gracias por ponerse en contacto con nosotros";
$MESS["IMOL_MAIL_HISTORY_ACTION_DESC"] = "Su conversacion ##SESSION_ID# con el representante el #SITE_URL#";
$MESS["IMOL_MAIL_ANSWER_ACTION_TITLE"] = "Gracias por contactarnos";
$MESS["IMOL_MAIL_AUTHOR_YOU"] = "Su";
$MESS["IMOL_MAIL_FILE"] = "Archivo";
$MESS["IMOL_MAIL_TIME_FORMAT"] = "g:i a";
$MESS["IMOL_MAIL_DATETIME_FORMAT"] = "F d, g:i a";
?>