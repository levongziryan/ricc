<?
$MESS["IMOL_CRM_LINE_TYPE_TELEGRAMBOT"] = "Telegrama";
$MESS["IMOL_CRM_LINE_TYPE_FACEBOOK"] = "Facebook";
$MESS["IMOL_CRM_LINE_TYPE_VKGROUP"] = "VK";
$MESS["IMOL_CRM_LINE_TYPE_SKYPEBOT"] = "Skype";
$MESS["IMOL_CRM_CREATE_LEAD_COMMENTS"] = "El prospecto fue creado en base a la comunicación del cliente para \"#LINE_NAME#\" via \"#CONNECTOR_NAME#\"";
$MESS["IMOL_CRM_CREATE_ACTIVITY_2"] = "Chat de Canal Abierto: \"#LEAD_NAME#\" (#CONNECTOR_NAME#)";
$MESS["IMOL_CRM_LINE_TYPE_LIVECHAT"] = "Chat en vivo";
$MESS["IMOL_CRM_LINE_TYPE_NETWORK"] = "Bitrix24.Network";
$MESS["IMOL_CRM_CARD_FULL_NAME"] = "Nombre";
$MESS["IMOL_CRM_CARD_COMPANY_TITLE"] = "Compañía";
$MESS["IMOL_CRM_CARD_POST"] = "Cargo";
$MESS["IMOL_CRM_CARD_PHONE"] = "Teléfono";
$MESS["IMOL_CRM_CARD_EMAIL"] = "E-mail";
$MESS["IMOL_CRM_LEAD_ADD"] = "Se creó un nuevo prospecto utilizando la información de contacto";
$MESS["IMOL_CRM_LEAD_EXTEND"] = "Información de contacto guardada en el prospecto";
$MESS["IMOL_CRM_CONTACT_EXTEND"] = "Información de contacto guardada en el contacto";
$MESS["IMOL_CRM_COMPANY_EXTEND"] = "Información de contacto guardada en la compañía";
$MESS["IMOL_CRM_BUTTON_CHANGE"] = "Cambiar";
$MESS["IMOL_CRM_BUTTON_CANCEL"] = "Cancelar";
?>