<?
$MESS["IMOL_CONFIG_LINE_NAME"] = "Canal Abierto #NAME#";
$MESS["IMOL_CONFIG_WELCOME_MESSAGE"] = "Bienvenido al [b]#COMPANY_NAME#[/b]! Canal Abierto![br]Usted recibirá una respuesta en breve, por favor espere.";
$MESS["IMOL_CONFIG_NO_ANSWER"] = "Desafortunadamente, no podemos atender actualmente su solicitud. Nos pondremos en contacto con usted tan pronto como recibamos su mensaje.";
$MESS["IMOL_CONFIG_WORKTIME_DAYOFF_2"] = "Lamentablemente, actualmente no podemos hablar con usted.[br][br]Por favor escriba su pregunta y nos pondremos en contacto con usted tan pronto como sea posible.";
$MESS["IMOL_CONFIG_WORKTIME_DAYOFF_3"] = "Bienvenido a [b]#COMPANY_NAME#![/b] Canal Abierto![br]Por desgracia, actualmente no podemos hablar con usted.[br][br]Por favor, escriba su pregunta y nos pondremos en contacto con usted tan pronto como sea posible. ";
$MESS["IMOL_CONFIG_CLOSE_TEXT"] = "Gracias por ponerse en contacto con nosotros! Por favor, díganos cómo le gustaría que fuera nuestro servicio.";
$MESS["IMOL_CONFIG_CLOSE_TEXT_2"] = "Gracias por hacer negocios con nosotros.";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_1_TEXT"] = "Por favor díganos cómo lo hicimos.";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_1_LIKE"] = "¡Gracias!";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_1_DISLIKE"] = "Sentimos no haber podido ayudarte. Su opinión nos ayudará a mejorar nuestro servicio.";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_2_TEXT"] = "Por favor, díganos cómo lo hicimos.[br][br]Sólo envíe 1 si está satisfecho, o 0 si pudiéramos hacerlo mejor.";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_2_LIKE"] = "¡Gracias!";
$MESS["IMOL_CONFIG_VOTE_MESSAGE_2_DISLIKE"] = "Sentimos no haber podido ayudarte. Su opinión nos ayudará a mejorar nuestro servicio.";
$MESS["IMOL_ADD_ERROR"] = "Error al crear el canal abierto";
$MESS["IMOL_UPDATE_ERROR"] = "Error al actualizar el Canal Abierto";
?>