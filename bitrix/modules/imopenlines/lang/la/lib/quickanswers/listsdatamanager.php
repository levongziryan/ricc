<?
$MESS["IMOL_QA_IBLOCK_NAME"] = "Respuestas preparadas";
$MESS["IMOL_QA_IBLOCK_DESCRIPTION"] = "Contiene respuestas preparadas para su uso con Canales Abiertos.";
$MESS["IMOL_QA_IBLOCK_ELEMENTS_NAME"] = "Respuestas";
$MESS["IMOL_QA_IBLOCK_ELEMENT_NAME"] = "Respuesta";
$MESS["IMOL_QA_IBLOCK_ELEMENT_ADD"] = "Agregar respuesta";
$MESS["IMOL_QA_IBLOCK_ELEMENT_EDIT"] = "Editar respuesta";
$MESS["IMOL_QA_IBLOCK_ELEMENT_DELETE"] = "Eliminar respuesta";
$MESS["IMOL_QA_IBLOCK_SECTIONS_NAME"] = "Secciones";
$MESS["IMOL_QA_IBLOCK_SECTION_NAME"] = "Sección";
$MESS["IMOL_QA_IBLOCK_SECTION_ADD"] = "agregar sección";
$MESS["IMOL_QA_IBLOCK_SECTION_EDIT"] = "Editar sección";
$MESS["IMOL_QA_IBLOCK_SECTION_DELETE"] = "Eliminar sección";
$MESS["IMOL_QA_IBLOCK_NAME_FIELD"] = "Nombre";
$MESS["IMOL_QA_IBLOCK_TEXT_FIELD"] = "Texto de respuesta preparada";
$MESS["IMOL_QA_IBLOCK_RATING_FIELD"] = "Clasificación";
$MESS["IMOL_QA_IBLOCK_GREETING_SECTION"] = "Mensaje de bienvenida";
$MESS["IMOL_QA_IBLOCK_PAYMENT_SECTION"] = "Pago";
$MESS["IMOL_QA_IBLOCK_DELIVERY_SECTION"] = "Entrega";
$MESS["IMOL_QA_IBLOCK_COMMON_SECTION"] = "Común";
?>