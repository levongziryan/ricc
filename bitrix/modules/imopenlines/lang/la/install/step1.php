<?
$MESS["IMOPENLINES_PUBLIC_PATH_DESC"] = "Se requiere una dirección de sitio web público de canal abierto para funcionar correctamente.";
$MESS["IMOPENLINES_PUBLIC_PATH_DESC_2"] = "Si el acceso externo a la red está restringido, sólo se permitirá el acceso a ciertas páginas. Por favor refiérase a la #LINK_START#documentación#LINK_END# para más detalles.";
$MESS["IMOPENLINES_PUBLIC_PATH"] = "Dirección pública de su sitio web:";
?>