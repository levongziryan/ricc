<?
$MESS["IMOL_HISTORY_LOG_NAME"] = "Registro de conversación del Canal Abierto";
$MESS["IMOL_OPERATOR_ANSWER_NAME"] = "Mensajes del cliente y respuestas de los representantes en la sesión Open Channel";
$MESS["IMOL_MAIL_PARAMS_DESC"] = "#EMAIL_TO# - E-mail del destinatario
#TITLE# - Asunto del mensaje
#TEMPLATE_SESSION_ID# - ID de la sesión
#TEMPLATE_ACTION_TITLE# - Título de la acción
#TEMPLATE_ACTION_DESC# - Descripción de la acción
#TEMPLATE_WIDGET_DOMAIN# - Dominio del sitio de Widget's
#TEMPLATE_WIDGET_URL# - URL de la página de Widget's
#TEMPLATE_LINE_NAME# - Nombre del Canal Abierto";
?>