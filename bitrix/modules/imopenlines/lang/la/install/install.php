<?
$MESS["IMOPENLINES_MODULE_NAME"] = "Canales Abiertos";
$MESS["IMOPENLINES_MODULE_DESCRIPTION"] = "El módulo de Canales Abiertos.";
$MESS["IMOPENLINES_INSTALL_TITLE"] = "Instalación del módulo \"Canales Abiertos\" ";
$MESS["IMOPENLINES_UNINSTALL_TITLE"] = "Desinstalación del módulo \"Canales Abiertos\" ";
$MESS["IMOPENLINES_UNINSTALL_QUESTION"] = "¿Usted está seguro de quierer eliminar el módulo?";
$MESS["IMOPENLINES_CHECK_PULL"] = "El módulo \"Push and Pull\" no está instalado o no está configurado el servidor en cola.";
$MESS["IMOPENLINES_CHECK_CONNECTOR"] = "El módulo \"External IM Connectors\" no está instalado.";
$MESS["IMOPENLINES_CHECK_IM"] = "El módulo \"Instant Messenger\" no está instalado.";
$MESS["IMOPENLINES_CHECK_IM_VERSION"] = "Por favor, actualice el módulo de \"Instant Messenger\" a la versión 16.5.0";
$MESS["IMOPENLINES_DB_NOT_SUPPORTED"] = "Este módulo sólo soporta MySQL.";
$MESS["IMOPENLINES_CHECK_PUBLIC_PATH"] = "No se ha especificado la dirección pública correcta.";
?>