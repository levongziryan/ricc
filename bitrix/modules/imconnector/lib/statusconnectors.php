<?php
namespace Bitrix\ImConnector;

use \Bitrix\Main\Entity\DataManager,
	\Bitrix\Main\Entity\IntegerField,
	\Bitrix\Main\Entity\StringField,
	\Bitrix\Main\Entity\BooleanField;

/**
 * Class StatusConnectorsTable
 * @package Bitrix\ImConnector
 */
class StatusConnectorsTable extends DataManager
{
	public static function getTableName()
	{
		return 'b_imconnectors_status';
	}

	public static function getMap()
	{
		return array(
			/**
			 * ID
			 */
			new IntegerField('ID', array(
				'primary' => true,
				'autocomplete' => true,
			)),
			/**
			 * ID connector
			 */
			new StringField('CONNECTOR', array(
				'required' => true,
			)),
			/**
			 * ID line
			 */
			new StringField('LINE'),
			/**
			 * A sign of activity connector
			 */
			new BooleanField('ACTIVE', array(
				'values' => array('N', 'Y'),
				'default_value' => 'N'
			)),
			/**
			 * The connection tested successfully
			 */
			new BooleanField('CONNECTION', array(
				'values' => array('N', 'Y'),
				'default_value' => 'N'
			)),
			/**
			 * Registration was successful
			 */
			new BooleanField('REGISTER', array(
				'values' => array('N', 'Y'),
				'default_value' => 'N'
			)),
			/**
			 * The signal errors in the process error
			 */
			new BooleanField('ERROR', array(
				'values' => array('N', 'Y'),
				'default_value' => 'N'
			))
		);
	}
}