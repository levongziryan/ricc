<?
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_INSTAGRAM"] = "<p class=\"im-connector-settings-header-description\">Conecte su cuenta de Instagram a Open Channels y administre los comentarios de Instagram desde la cuenta de Bitrix24.</p>

    <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">Los comentarios de Instagram se muestran en Bitrix24 messenger</li>

     <li class=\"im-connector-settings-header-list-item\">distribución automática de mensajes entrantes según la reglas de cola </li>

     <li class=\"im-connector-settings-header-list-item\">familiar Bitrix24 chat interface</li>

     <li class=\"im-connector-settings-header-list-item\">todas las conversaciones guardadas en el historial del CRM</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_BOTFRAMEWORK"] = "<p class=\"im-connector-settings-header-description\">Conecte los servicios compatibles con Microsoft Bot Framework como Skype, Slack, Kik, GroupMe, SMS a Open Channels. Cada mensaje se reenviará a su cuenta Bitrix24.</p>

    <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">distribución automática de mensajes entrantes según la reglas de cola </li>

     <li class=\"im-connector-settings-header-list-item\">familiar Bitrix24 chat interface</li>

     <li class=\"im-connector-settings-header-list-item\">todas las conversaciones guardadas en el historial del CRM</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CLOSE"] = "Cerrar";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_BOTFRAMEWORK"] = "Bitrix24 le encanta Microsoft Bot Framework";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CONFIGURE_CHANNEL"] = "Preferencias de los Canales Abiertos";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_OPEN_LINE"] = "Canal Abierto:";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CREATE_OPEN_LINE"] = "Crear Canal Abierto";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CONFIGURE"] = "configurar";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_NO_OPEN_LINE"] = "No tiene ningún Canal Abierto cuyas comunicaciones puedan ser configuradas. Por favor, cree un nuevo canal abierto.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_NO_OPEN_LINE_AND_NOT_ADD_OPEN_LINE"] = "No tiene ningún Canal Abierto cuyas comunicaciones puedan ser configuradas. No tiene permiso suficiente para crear Canales Abiertos. <br> Póngase en contacto con el administrador del portal.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_POPUP_LIMITED_TITLE"] = "Extender Canales Abiertos";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_POPUP_LIMITED_TEXT"] = "Su plan actual restringe el número de Canales Abiertos disponibles. Para agregar otro Canal Abierto, actualice a un mejor plan.
<br> <br>
El plan \"Professional\" no tiene ninguna restricción en el número de Canales Abiertos en absoluto.";
$MESS["IIMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_ERROR_ACTION"] = "Se ha cancelado la acción porque se ha producido un error.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CONNECT"] = "Configuración del canal";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_LIVECHAT"] = "Sitio web chat en vivo";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_LIVECHAT"] = "<p class=\"im-connector-settings-header-description\">Instalar widget de chat en vivo gratis en su sitio web y comunicarse con sus clientes en tiempo real desde cualquier ubicación.</p>

    <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">funciona para cualquier sitio web, inresa código proporcionado</li>

     <li class=\"im-connector-settings-header-list-item\">fácil de personalizar y establecer condiciones de visualización </li>

     <li class=\"im-connector-settings-header-list-item\">mensajes entrantes distribuidos entre agentes según sus reglas de cola</li>

     <li class=\"im-connector-settings-header-list-item\">todas las conversaciones guardadas en el historial del CRM</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_NETWORK"] = "Comuníquese con sus clientes, colegas y socios comerciales fácilmente";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_NETWORK"] = "<p class=\"im-connector-settings-header-description\">Conecte Bitrix24.Network para abrir canales y comunicarse con sus clientes, socios y colegas que utilizan Bitrix24 fácilmente. </p>

    <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">distribución automática de mensajes entrantes según reglas de cola </li>

     <li class=\"im-connector-settings-header-list-item\">comunicaciones en tiempo real</li>

     <li class=\"im-connector-settings-header-list-item\">interfaz de chat familiar de Bitrix24</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_VIBER"] = "Administre su cuenta pública de Viber";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_VIBER"] = "<p class=\"im-connector-settings-header-description\"> Conecte su cuenta pública de Viber a Canales Abiertos y sus empleados podrán comunicarse con los usuarios de Viber directamente desde Bitrix24. </p>

    <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">distribución automática de mensajes entrantes según reglas de cola </li>

     <li class=\"im-connector-settings-header-list-item\">interfaz de chat familiar de Bitrix24</li>

     <li class=\"im-connector-settings-header-list-item\">todas las conversaciones guardadas en el historial del CRM</li>

</ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_TELEGRAMBOT"] = "Bitrix24 le encanta Telegram";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_TELEGRAMBOT"] = "<p class=\"im-connector-settings-header-description\">Conecta tu cuenta Telegram a Canales Abiertos y comunícala desde tu cuenta Bitrix24.</p>

<ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">distribución automática de mensajes entrantes según reglas de cola </li>

     <li class=\"im-connector-settings-header-list-item\">distribución automática de mensajes entrantes según reglas de cola </li>

     <li class=\"im-connector-settings-header-list-item\">Interfaz de chat familiar de Bitrix24</li>

     <li class=\"im-connector-settings-header-list-item\">todas las conversaciones guardadas en el historial del CRM</li>

</ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_VKGROUP"] = "Comunicarse con VK a través de Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_VKGROUP"] = "<p class=\"im-connector-settings-header-description\">Conecte su grupo VK a Canales Abiertos y comuníquese desde su cuenta Bitrix24.</p>

    <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">automatic incoming message distribution according to queue rules </li>

     <li class=\"im-connector-settings-header-list-item\">interfaz de chat familiar de Bitrix24</li>

     <li class=\"im-connector-settings-header-list-item\">todas las conversaciones guardadas en el historial del CRM</li>

    </ul>";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_FACEBOOK"] = "Comunícate con usuarios de Facebook a través de Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_FACEBOOK"] = "<p class=\"im-connector-settings-header-description\">Conecta tu página Facebook de la empresa con Canales Abiertos y comunicarte con usuarios de Facebook a través de Bitrix24.</p>

     <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">distribución automática de mensajes entrantes según reglas de cola </li>

     <li class=\"im-connector-settings-header-list-item\">interfaz de chat familiar de Bitrix24</li>

     <li class=\"im-connector-settings-header-list-item\">todas las conversaciones guardadas en el historial del CRM</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_FACEBOOKCOMMENTS"] = "Administre su página de Facebook desde Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_FACEBOOKCOMMENTS"] = "<p class=\"im-connector-settings-header-description\">Conecta tu página de Facebook a Canales abiertos y administra comentarios de tus publicaciones, fotos y videos de Facebook, ya quees tu cuenta de Bitrix24.</p>

     <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">comunicaciones en tiempo real</li>

     <li class=\"im-connector-settings-header-list-item\">distribución automática de mensajes entrantes según reglas de cola </li>

     <li class=\"im-connector-settings-header-list-item\">Interfaz de chat familiar de Bitrix24</li>

     <li class=\"im-connector-settings-header-list-item\">todas las conversaciones guardadas en el historial del CRM</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_INSTAGRAM"] = "Bitrix24 le encanta Instagram";
?>