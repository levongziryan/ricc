<?
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CONNECT"] = "Einstellungen des Kanals";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_LIVECHAT"] = "Onlinechat auf der Website";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_LIVECHAT"] = "<p class=\"im-connector-settings-header-description\">Installieren Sie ein kostenloses Onlinechat-Widget auf Ihre Website und kommunizieren Sie mit Ihren Kunden in Echtzeit von einem beliebigen Standort.</p>

    <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">funktioniert für jede Website, Einbettungscode wird bereitgestellt</li>

     <li class=\"im-connector-settings-header-list-item\">kann einfach angepasst und angezeigt werden </li>

     <li class=\"im-connector-settings-header-list-item\">eingehende Nachrichten werden unter Mitarbeitern entsprechend den Regeln der Warteschlange verteilt</li>

     <li class=\"im-connector-settings-header-list-item\">alle Konversationen werden in der CRM-History abgesoeichert</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_NETWORK"] = "Kommunikation mit Ihren Kunden, Kollegen und Geschäftspartnern ist einfach";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_NETWORK"] = "<p class=\"im-connector-settings-header-description\">Verbinden Sie Bitrix24.Network mit Kommunikationskanälen und kommunizieren Sie mit Ihren Kunden, Geschäftspartnern und Kollegen, welche Bitrix24 auch nutzen. </p>

    <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">automatische Verteilung von eingehenden Nachrichten entsprechend den Regeln der Warteschlange </li>

     <li class=\"im-connector-settings-header-list-item\">Kommunikation in Echtzeit</li>

     <li class=\"im-connector-settings-header-list-item\">Benutzeroberfläche wie im Bitrix24 Chat</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_VIBER"] = "Verwalten Sie Ihren öffentlichen Viber Account";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_VIBER"] = "<p class=\"im-connector-settings-header-description\"> Verknüpfen Sie Ihren öffentlichen Viber Account mit Kommunikationskanälen, sodass Ihre Mitarbeiter mit Viber Nutzern direkt aus BItrix24 kommunizieren können. </p>

    <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">automatische Verteilung der eingehenden Nachrichten entsprechend den Regeln der Warteschlange </li>

     <li class=\"im-connector-settings-header-list-item\">Benutzeroberfläche wie im Bitrix24 Chat</li>

     <li class=\"im-connector-settings-header-list-item\">alle Konversationen werden in der CRM-History abgespeichert/li>

</ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_TELEGRAMBOT"] = "Bitrix24 und Telegram";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_TELEGRAMBOT"] = "<p class=\"im-connector-settings-header-description\">Verknüpfen Sie Ihren Telegram Account mit Kommunikationskanälen und kommunizieren Sie direkt aus Ihrem Bitrix24.</p>

<ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">automatische Verteilung von eingehenden Nachrichten entsprechend den Regeln der Warteschlange </li>

     <li class=\"im-connector-settings-header-list-item\">automatische Verteilung von eingehenden Nachrichten entsprechend den Regeln der Warteschlange </li>

     <li class=\"im-connector-settings-header-list-item\">Benutzeroberfläche wie im Bitrix24 Chat</li>

     <li class=\"im-connector-settings-header-list-item\">alle Konversationen werden in der CRM-History abgespeichert</li>

</ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_VKGROUP"] = "Kommunikation mit VK via BItrix24";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_VKGROUP"] = "<p class=\"im-connector-settings-header-description\">Verknüpfen Sie Ihre VK Gruppe mit Kommunikationskanälen und kommunizieren Sie mit VK Nutzern aus Ihrem Bitrix24.</p>

    <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">automatische Verteilung von eingehenden Nachrichten entsprechend den Regeln der Warteschlange </li>

     <li class=\"im-connector-settings-header-list-item\">Benutzeroberfläche wie im Bitrix24 Chat</li>

     <li class=\"im-connector-settings-header-list-item\">alle Konversationen werden in der CRM-History abgespeichert</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_FACEBOOK"] = "Kommunizieren Sie mit Facebook Nutzern via Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_FACEBOOK"] = "<p class=\"im-connector-settings-header-description\">Verknüpfen Sie die Facebook-Seite Ihres Unternehmens mit Kommunikationskanälen und kommunizieren Sie mit Facebook Nutzern aus Ihrem Bitrix24 Account.</p>

     <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">automatische Verteilung von eingehenden Nachrichten entsprechend den Regeln der Warteschlange </li>

     <li class=\"im-connector-settings-header-list-item\">Benutzeroberfläche wie im Bitrix24 Chat</li>

     <li class=\"im-connector-settings-header-list-item\">alle Konversationen werden in der CRM-History abgespeichert</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_FACEBOOKCOMMENTS"] = "Verwalten Sie Ihre Facebook Seite von Bitrix24 aus";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_FACEBOOKCOMMENTS"] = "<p class=\"im-connector-settings-header-description\">Verknüpfen Sie Ihre Facebook Seite mit Kommunikationskanälen und verwalten Sie Kommentare zu Beiträgen in Ihrem Facebook sowie Bilder und Videos direkt in Ihrem BItrix24.</p>

     <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">Kommunikation in Echtzeit</li>

     <li class=\"im-connector-settings-header-list-item\">automatische Verteilung von eingehenden Nachrichten entsprechend den Regeln der Warteschlange </li>

     <li class=\"im-connector-settings-header-list-item\">Benutzeroberfläche wie im Bitrix24 Chat</li>

     <li class=\"im-connector-settings-header-list-item\">alle Konversationen werden in der CRM-History abgespeichert</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_INSTAGRAM"] = "Bitrix24 und Instagram";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_INSTAGRAM"] = "<p class=\"im-connector-settings-header-description\">Verknüpfen Sie Ihren Instagram Account mit Kommunikationskanälen und verwalten Sie Instagram Kommentare direkt in Ihrem BItrix24.</p>

    <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">Instagram Kommentare werden im Bitrix24 Messenger angezeigt</li>

     <li class=\"im-connector-settings-header-list-item\">automatische Verteilung von eingehenden Nachrichten entsprechend den Regeln der Warteschlange </li>

     <li class=\"im-connector-settings-header-list-item\">Benutzeroberfläche wie im Bitrix24 Chat</li>

     <li class=\"im-connector-settings-header-list-item\">alle Konversationen werden in der CRM-History abgespeichert</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_BOTFRAMEWORK"] = "Bitrix24 und Microsoft Bot Framework";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_BOTFRAMEWORK"] = "<p class=\"im-connector-settings-header-description\">Verknüpfen Sie Services wie Skype, Slack, Kik, GroupMe, SMS, die vom Microsoft Bot Framework unterstützt sind, mit Kommunikationskanälen. Die jeweiligen Nachrichten werden an Ihren Bitrix24 Account weitergeleitet.</p>

    <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">automatische Verteilung von eingehenden Nachrichten entsprechend den Regeln der Warteschlange </li>

     <li class=\"im-connector-settings-header-list-item\">Benutzeroberfläche wie im Bitrix24 Chat</li>

     <li class=\"im-connector-settings-header-list-item\">alle Konversationen werden in der CRM-History abgespeichert</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CONFIGURE_CHANNEL"] = "Einstellungen der Kommunikationskanäle";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_OPEN_LINE"] = "Kommunikationskanal:";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CREATE_OPEN_LINE"] = "Kommunikationskanal erstellen";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CONFIGURE"] = "konfigurieren";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_NO_OPEN_LINE"] = "Sie haben keinen Kanal, dessen Kommunikation eingestellt werden kann. Erstellen Sie bitte einen neuen Kommunikationskanal.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_NO_OPEN_LINE_AND_NOT_ADD_OPEN_LINE"] = "Sie haben keinen Kanal, dessen Kommunikation eingestellt werden kann. Sie haben nicht genügend Rechte, um Kommunikationskanäle zu erstellen.<br>Wenden Sie sich bitte an Administrator Ihres Portals.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_POPUP_LIMITED_TITLE"] = "Erweiterte Kommunikationskanäle";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_POPUP_LIMITED_TEXT"] = "Ihr aktueller Tarif erlaubt nur eine eingeschränkte Anzahl von Kommunikationskanälen. Sie sollten Ihren Tarif upgraden, wenn Sie einem weiteren Kommunikationskanal hinzufügen möchten.
<br><br>
Der Tarif \"Professional\" bietet unbegrenzte Kommunikationskanäle an.
";
$MESS["IIMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_ERROR_ACTION"] = "Aktion wurde abgebrochen, weil ein Fehler aufgetreten war.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CLOSE"] = "Schließen";
?>