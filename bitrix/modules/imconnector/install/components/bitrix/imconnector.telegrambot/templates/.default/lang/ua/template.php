<?
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_API_TOKEN_NAME"] = "Токен доступу:";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_REGISTER"] = "Реєстрація бота";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_TESTED"] = "Тестування з'єднання";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_CONNECTOR_ERROR_STATUS"] = "У процесі роботи вашого бота трапилася помилка. Вам необхідно перевірити зазначені дані і протестувати бота повторно.";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_CREATE_NEW_BOT"] = "Створити новий бот";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_THERE_IS_A_BOT_TO_KNOW_THE_TOKEN"] = "Є бот, дізнатися токен";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_I_KNOW_TOKEN"] = "Я знаю токен";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_NAME_BOT"] = "Ім'я бота";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_LINK_BOT"] = "Посилання на бота";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_6_STEPS_TITLE"] = "Пройдіть 6 кроків, щоб створити свого бота в Telegram";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_1_OF_6"] = "крок 1 з 6";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_2_OF_6"] = "крок 2 з 6";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_3_OF_6"] = "крок 3 з 6";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_4_OF_6"] = "крок 4 з 6";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_5_OF_6"] = "крок 5 з 6";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_6_OF_6"] = "крок 6 з 6";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_1_OF_6_TITLE"] = "З чого почати";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_1_OF_6_DESCRIPTION_1"] = "Перейдіть за посиланням та натисніть Start <span class=\"imconnector-link\"><a href=\"https://telegram.me/BotFather\" target=\"_blank\">https://telegram.me/BotFather</a></span>";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_2_OF_6_TITLE"] = "Створюємо бота";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_2_OF_6_DESCRIPTION_1"] = "Виберіть команду створити нового бота <span class=\"imconnector-link\">/newbot</span>";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_3_OF_6_TITLE"] = "Ім'я бота";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_3_OF_6_DESCRIPTION_1"] = "Вкажіть ім'я вашого бота, за яким клієнти зможуть знайти і звернутися до вашої компанії через Telegram. Ім'я буде відображатися в контакт листі, а також у відкритих чатах Telegram";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_4_OF_6_TITLE"] = "Логін бота";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_4_OF_6_DESCRIPTION_1"] = "Це унікальне ім'я вашого бота, за яким клієнти зможуть знайти вас в Telegram, а також на підставі якого буде сформовано посилання на вашого бота";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_5_OF_6_TITLE"] = "Отримання токена";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_5_OF_6_DESCRIPTION_1"] = "Скопіюйте отриманий токен";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_5_OF_6_DESCRIPTION_2"] = "І вставити скопійований токен в поле нижче та натисніть «Зберегти»";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_6_OF_6_TITLE"] = "Завершення";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_6_OF_6_DESCRIPTION_1"] = "Telegram успішно підключений до вашої Відкритої лінії. Тепер всі звернення до вашого боту будуть автоматично спрямовані на ваш Бітрікс24.";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_1_OF_3"] = "крок 1 з 3";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_2_OF_3"] = "крок 2 з 3";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_3_OF_3"] = "крок 3 з 3";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_1_OF_3_TITLE"] = "Отримання токена вашого бота";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_1_OF_3_DESCRIPTION_1"] = "Перейдіть за посиланням <span class=\"imconnector-link\"><a href=\"https://telegram.me/BotFather\" target=\"_blank\">https://telegram.me/BotFather</a></span> та виберіть команду <span class=\"imconnector-link\">/token</span><br><br>
На екрані будуть показані всі боти, створені вами. Виберіть бота, котрого ви хочете підключити до відкритої лінії Бітрікс24";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_2_OF_3_DESCRIPTION_1"] = "Скопіюйте отриманий токен";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_2_OF_3_DESCRIPTION_2"] = "І вставите скопійований токен в поле нижче та натисніть «Зберегти»";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_3_OF_3_TITLE"] = "Завершення";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_3_OF_3_DESCRIPTION_1"] = "Telegram успішно підключений до вашої Відкритої лінії. Тепер всі звернення до вашого боту будуть автоматично спрямовані на ваш Бітрікс24.";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_SIMPLE_FORM_DESCRIPTION_1"] = "Якщо у вас вже є бот в Telegram та ви знаєте його токен, просто введіть його в поле нижче та натисніть кнопку \"Зберегти\"";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_SIMPLE_FORM_DESCRIPTION_TESTED"] = "Вам потрібно протестувати правильність ваших даних.";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_SIMPLE_FORM_DESCRIPTION_REGISTER"] = "Тепер ви можете зареєструвати свого бота.";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_INDEX_DESCRIPTION"] = "Підключіть Telegram до Відкритої лінії щоб приймати звернення ваших клієнтів в робочому чаті Бітрікс24. Для підключення необхідно створити свого бота в Telegram або підключити вже існуючого.
<br>Якщо у вас ще немає бота, ми допоможемо створити його в кілька кроків і підключити до вашого Бітрікс24";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_3_STEPS_TITLE"] = "Пройдіть 3 кроки щоб отримати токен вашого бота в Telegram";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_2_OF_3_TITLE"] = "Підключення бота";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_FINAL_FORM_DESCRIPTION_1"] = "Telegram успішно підключений до вашої Відкритої лінії. Тепер всі звернення до вашого боту будуть автоматично спрямовані на ваш Бітрікс24.";
?>