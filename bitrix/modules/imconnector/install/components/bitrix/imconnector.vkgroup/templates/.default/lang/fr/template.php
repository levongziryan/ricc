<?
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_DEL_REFERENCE"] = "Dissocier";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_STEP_1_OF_3_TITLE"] = "Se connecter";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_STEP_3_OF_3_TITLE"] = "Terminer";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_STEP_N_OF_3"] = "étape #STEP# de 3";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_GROUP"] = "Groupe";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_GROUP_IM"] = "Messagerie";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECT_GROUP"] = "Groupe connecté";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECTED_GROUP"] = "Groupe connecté";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_STEP_2_OF_3_TITLE"] = "Sélectionnez le groupe, la page publique ou l'évènement";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_INDEX_DESCRIPTION_NEW"] = "Connectez VK au Canal ouvert pour recevoir les messages de vos clients dans le chat Bitrix24. Pour effectuer la connexion, vous devez posséder un groupe, une page ou un évènement VK qui existe déjà ou en créer un(e). Vous devez être un administrateur de cette entité.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_ENTITY"] = "Connectez-vous avec un compte auquel un groupe, une page ou un évènement VK pertinent est lié afin de recevoir les messages de vos clients Bitrix24:";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_THERE_IS_NO_ENTITY_WHERE_THE_ADMINISTRATOR"] = "Vous n'avez aucun groupe, page ou évènement  VK pour lesquels vous êtes un administrateur. <br>Créez-en un(e) dès maintenant.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_TO_CREATE"] = "Créer";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_SELECT_THE_ENTITY"] = "Sélectionnez un groupe, une page publique ou un évènement VK à connecter au Canal ouvert Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECT_PUBLIC_PAGE"] = "Connectez la page publique";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECTED_PUBLIC_PAGE"] = "Page publique connectée";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECT_PUBLIC_EVENT"] = "Connectez l'évènement";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECTED_PUBLIC_EVENT"] = "Évènement connecté";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CHANGE"] = "Modifier";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_MY_OTHER_ENTITY"] = "Mes autres groupes, pages publiques ou évènements VK";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_OTHER_ENTITY"] = "Autre groupes,<br>pages publiques<br>ou évènements";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECT"] = "Connecter";
?>