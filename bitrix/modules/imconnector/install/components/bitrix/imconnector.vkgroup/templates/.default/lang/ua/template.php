<?
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_DEL_REFERENCE"] = "Видалити прив'язку";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_STEP_1_OF_3_TITLE"] = "Авторизація";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_STEP_3_OF_3_TITLE"] = "Завершення";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_STEP_N_OF_3"] = "крок #STEP# 3";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_GROUP"] = "Група";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECT_GROUP"] = "Підключити групу";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECTED_GROUP"] = "Підключена група";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_STEP_2_OF_3_TITLE"] = "Вибір групи або публічної сторінки або заходу";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_INDEX_DESCRIPTION_NEW"] = "Підключіть Вконтакті до Відкритої лінії щоб приймати звернення ваших клієнтів в робочому чаті Бітрікс24. Для підключення необхідно створити групу, публічну сторінку або захід Вконтакті або підключити вже існуючі. Ви повинні бути адміністратором.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_ENTITY"] = "Авторизуйтесь під своїм обліковим записом в якому у вас є група або публічна сторінка або захід Вконтакті, аби приймати повідомлення від ваших клієнтів усередині Бітрікс24:";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_THERE_IS_NO_ENTITY_WHERE_THE_ADMINISTRATOR"] = "У вас поки немає ні однієї групи або публічної сторінки або заходу Вконтакті, де ви є адміністратором.<br>Ви можете створити прямо зараз.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_TO_CREATE"] = "Створити";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_SELECT_THE_ENTITY"] = "Виберіть групу або публічну сторінку або захід Вконтакті, які необхідно підключити до Відкритої лінії Бітрікс24";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECT_PUBLIC_PAGE"] = "Підключити публічну сторінку";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECTED_PUBLIC_PAGE"] = "Підключена публічна сторінка";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECT_PUBLIC_EVENT"] = "Підключити захід";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECTED_PUBLIC_EVENT"] = "Підключено захід";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CHANGE"] = "Змінити";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_MY_OTHER_ENTITY"] = "Інші мої групи або публічні сторінки або заходи у Вконтакті";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_OTHER_ENTITY"] = "Інші групи або<br>публічні сторінки<br>або заходи";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECT"] = "Підключити";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_GROUP_IM"] = "Мессенджер";
?>