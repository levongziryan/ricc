<?
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_DEL_REFERENCE"] = "Desvincular";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_STEP_1_OF_3_TITLE"] = "Iniciar sesión";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_STEP_2_OF_3_TITLE"] = "Seleccionar grupo";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_STEP_3_OF_3_TITLE"] = "Terminar";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_STEP_N_OF_3"] = "paso #STEP# 3";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_GROUP"] = "Grupo";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECT_GROUP"] = "Grupo conectado";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECTED_GROUP"] = "Grupo conectado";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_INDEX_DESCRIPTION_NEW"] = "Conectar VK al Canal Abierto para recibir los mensajes de sus clientes al chat de Bitrix24. Para conectarse, necesita poseer un grupo existente de VK, página pública o evento, o crear uno nuevo. Usted tiene que ser un administrador de esa entidad.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_ENTITY"] = "Iniciar sesión utilizando una cuenta de un grupo VK, página pública o evento de interés que esté vinculada a recibir mensajes de sus clientes a Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_THERE_IS_NO_ENTITY_WHERE_THE_ADMINISTRATOR"] = "No es necesario que un grupo VK, página pública o evento del cual usted era un administrador. <br> Crear uno ahora mismo.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_TO_CREATE"] = "Crear";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_SELECT_THE_ENTITY"] = "Seleccionar un grupo VK, página pública o evento para conectarse al Canal Abierto de Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECT_PUBLIC_PAGE"] = "Conectar la página pública";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECTED_PUBLIC_PAGE"] = "Página pública conectada";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECT_PUBLIC_EVENT"] = "Conectar evento";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECTED_PUBLIC_EVENT"] = "Evento conectado";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CHANGE"] = "Cambiar";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_MY_OTHER_ENTITY"] = "Mis otros grupos VK, páginas o eventos públicos";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_OTHER_ENTITY"] = "Otros grupos,<br>páginas públicas<br>o eventos";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECT"] = "Conectar";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_GROUP_IM"] = "Messenger";
?>