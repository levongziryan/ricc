<?
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_DEL_REFERENCE"] = "Verknüpfung löschen";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_STEP_1_OF_3_TITLE"] = "Einloggen";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_STEP_3_OF_3_TITLE"] = "Abschließen";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_STEP_N_OF_3"] = "Schritt #STEP# von 3";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_GROUP"] = "Gruppe";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_GROUP_IM"] = "Messenger";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECT_GROUP"] = "Gruppe verbinden";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECTED_GROUP"] = "Gruppe verbunden";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_STEP_2_OF_3_TITLE"] = "Die Verknüpfung mit einer Gruppe, öffentlichen Seite oder einem Termin kann nicht gelöscht werden.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_INDEX_DESCRIPTION_NEW"] = "Gruppe, öffentliche Seite oder Termin auswählen";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_ENTITY"] = "Verbinden Sie VK mit Ihrem Kommunikationskanal, um Nachrichten von Ihren Kunden in einem Bitrix24 Chat zu erhalten. Um diese Verbindung herzustellen, müssen Sie eine Gruppe, öffentliche Seite oder einen Termin in VK besitzen, oder diese erstellen. Sie müssen ein Administrator für diese Elemente sein:";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_THERE_IS_NO_ENTITY_WHERE_THE_ADMINISTRATOR"] = "Einloggen via Account, mit dem eine VK Gruppe, öffentliche Seite oder ein VK Termin verknüpft sind, um Nachrichten von Kunden in Bitrix24 zu erhalten.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_TO_CREATE"] = "Sie haben noch keine VK Gruppe, öffentliche Seite oder keinen VK Termin, für welche Sie ein Administrator wären. <br>Sie können das sofort erstellen.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_SELECT_THE_ENTITY"] = "Erstellen";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECT_PUBLIC_PAGE"] = "Wählen Sie eine VK Gruppe, öffentliche Seite oder einen VK Termin aus, um es mit einem Kommunikationskanal in Bitrix24 zu verbinden.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECTED_PUBLIC_PAGE"] = "Öffentliche Seite verbinden";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECT_PUBLIC_EVENT"] = "Öffentliche Seite verbunden";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECTED_PUBLIC_EVENT"] = "Termin verbinden";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CHANGE"] = "Termin verbunden";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_MY_OTHER_ENTITY"] = "Ändern";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_OTHER_ENTITY"] = "Andere VK Gruppen, öffentliche Seiten oder Termine von mir";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECT"] = "Andere Gruppen,<br>öffentliche Seiten<br>oder Termine";
?>