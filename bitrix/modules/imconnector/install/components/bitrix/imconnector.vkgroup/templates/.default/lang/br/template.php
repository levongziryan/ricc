<?
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_DEL_REFERENCE"] = "Desvincular";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_STEP_1_OF_3_TITLE"] = "Fazer login";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_STEP_3_OF_3_TITLE"] = "Concluir";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_STEP_N_OF_3"] = "etapa #STEP# de 3";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_GROUP"] = "Grupo";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_GROUP_IM"] = "Messenger";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECT_GROUP"] = "Grupo conectado";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECTED_GROUP"] = "Grupo conectado";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_STEP_2_OF_3_TITLE"] = "Selecione o grupo, página pública ou evento";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_INDEX_DESCRIPTION_NEW"] = "Conecte o VK ao Canal Aberto para receber mensagens de seus clientes no bate-papo Bitrix 24. Para conectar, você precisa ter um grupo VK existente, página pública ou evento, ou criar um. Você tem que ser administrador dessa entidade.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_ENTITY"] = "Faça login usando uma conta a qual um grupo VK, página pública ou evento de interesse está vinculado para receber mensagens de seus clientes no Bitrix24:";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_THERE_IS_NO_ENTITY_WHERE_THE_ADMINISTRATOR"] = "Você não tem um grupo VK, página pública ou evento no qual você é administrador. <br>Crie um agora mesmo.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_TO_CREATE"] = "Criar";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_SELECT_THE_ENTITY"] = "Selecione um grupo VK, página pública ou evento para conectar ao Canal Aberto Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECT_PUBLIC_PAGE"] = "Conecte a página pública";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECTED_PUBLIC_PAGE"] = "Página pública conectada";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECT_PUBLIC_EVENT"] = "Conecte o evento";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECTED_PUBLIC_EVENT"] = "Evento conectado";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CHANGE"] = "Alterar";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_MY_OTHER_ENTITY"] = "Meus outros grupos VK, páginas públicas ou eventos";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_OTHER_ENTITY"] = "Outros grupos,<br>páginas públicas<br>ou eventos";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECT"] = "Conectar";
?>