<?
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_MODULE_NOT_INSTALLED"] = "El módulo \"External IM Connectors\" no está instalado.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_NO_ACTIVE_CONNECTOR"] = "Esta conexión está inactiva.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_SESSION_HAS_EXPIRED"] = "Su sesión ha caducado. Por favor envíe el formulario de nuevo.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_OK_DEL_USER"] = "Su cuenta de usuario ha sido desvinculada";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_NO_DEL_USER"] = "No se puede desvincular su cuenta de usuario";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_CONNECTOR_ERROR_STATUS"] = "Se ha producido un error. Por favor, compruebe sus preferencias.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_ERROR_REQUEST_INFORMATION_FROM_SERVER"] = "Error al recuperar la información del servidor";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_REMOVED_REFERENCE_TO_ENTITY"] = "Este conector se ha configurado para utilizarse con el grupo, página pública o suceso que actualmente no tiene acceso administrativo.<br>
Por favor, configure el conector de nuevo.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_OK_DEL_ENTITY"] = "Grupo, página pública o evento se ha desvinculado.";
$MESS["IMCONNECTOR_COMPONENT_VKGROUP_NO_DEL_ENTITY"] = "No se puede desvincular del grupo, página pública o evento.";
?>