<?
$MESS["IMCONNECTOR_COMPONENT_VIBER_API_KEY"] = "Chave:";
$MESS["IMCONNECTOR_COMPONENT_VIBER_REGISTER"] = "Registrar conta pública";
$MESS["IMCONNECTOR_COMPONENT_VIBER_TESTED"] = "Conexão de teste";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CONNECTOR_ERROR_STATUS"] = "Houve um problema com a sua conta pública. Verifique se os parâmetros fornecidos estão corretos e teste a conta novamente.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CREATE_A_PUBLIC_ACCOUNT"] = "Criar conta pública";
$MESS["IMCONNECTOR_COMPONENT_VIBER_AVAILABLE_PUBLIC_ACCOUNT"] = "Tenho uma conta pública";
$MESS["IMCONNECTOR_COMPONENT_VIBER_I_KNOW_KEY"] = "Tenho a chave";
$MESS["IMCONNECTOR_COMPONENT_VIBER_NAME_BOT"] = "Nome da conta pública";
$MESS["IMCONNECTOR_COMPONENT_VIBER_LINK_PUBLIC_ACCOUNT"] = "Link da conta pública";
$MESS["IMCONNECTOR_COMPONENT_VIBER_LINK_CHAT_ONE_TO_ONE"] = "Link do bate-papo de pessoa para pessoa";
$MESS["IMCONNECTOR_COMPONENT_VIBER_PUBLIC_ACCOUNT"] = "Conta Pública";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CHAT_ONE_TO_ONE"] = "Bate-papo de pessoa para pessoa";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEPS_TITLE"] = "Siga estas etapas para conectar sua conta pública Viber";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_5"] = "etapa 1 de 5";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5"] = "etapa 2 de 5";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_3_OF_5"] = "etapa 3 de 5";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_4_OF_5"] = "etapa 4 de 5";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_5_OF_5"] = "etapa 5 de 5";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_DESCRIPTION_2_PIC_ANDROID"] = "/images/imconnector-viber-2-android-en.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_DESCRIPTION_2_PIC_IOS"] = "/images/imconnector-viber-2-ios-en.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_DESCRIPTION_3_PIC"] = "/images/imconnector-viber-3-en.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_4_PIC_ANDROID"] = "/images/imconnector-viber-4-android-en.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_4_PIC_IOS"] = "/images/imconnector-viber-4-ios-en.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_5_PIC_ANDROID"] = "/images/imconnector-viber-5-android-en.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_5_PIC_IOS"] = "/images/imconnector-viber-5-ios-en.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_6_PIC"] = "/images/imconnector-viber-6-en.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_GENERAL_DESCRIPTION_7_PIC"] = "/images/imconnector-viber-7-en.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_3"] = "etapa 1 de 3";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_3"] = "etapa 2 de 3";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_3_OF_3"] = "etapa 3 de 3";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_TITLE"] = "Obtenha a chave";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_1"] = "Execute o app móvel (somente a versão 6.5 e posteriores dos apps Android e iOS são combatíveis)
 <br><br><br>
 Siga para a área \"Contas Públicas\"";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_2"] = "Selecione sua conta pública.
<br><br><br>
Siga para a área de informação.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_3"] = "Selecione \"Editar Info\"";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_GENERAL_TITLE"] = "Conectar Conta Pública";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_GENERAL_DESCRIPTION_1"] = "Copie a chave:";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_GENERAL_DESCRIPTION_2"] = "Cole-a no campo abaixo e clique em \"Salvar\"";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_GENERAL_DESCRIPTION_3"] = "Se necessário, altere mais preferências da conta pública aqui: avatar visível aos seus contatos; nome da conta; descrição; etiquetas etc.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_3_OF_GENERAL_TITLE"] = "Concluir";
$MESS["IMCONNECTOR_COMPONENT_VIBER_SIMPLE_FORM_DESCRIPTION_1"] = "Se você já tem uma conta pública Viber e tem a chave, basta colá-la no campo abaixo e clicar em \"Salvar\"";
$MESS["IMCONNECTOR_COMPONENT_VIBER_SIMPLE_FORM_DESCRIPTION_TESTED"] = "Agora, você tem que verificar se os seus dados estão corretos.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_SIMPLE_FORM_DESCRIPTION_REGISTER"] = "Agora, você pode registrar sua conta pública.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_INDEX_DESCRIPTION"] = "Conecte o Viber ao Canal Aberto para receber mensagens de seus clientes no bate-papo Bitrix24. Você pode criar uma nova conta Viber ou conectar uma conta já existente.
<br>Se você não tiver uma conta pública, ajudaremos você a criar uma e conectá-la ao seu Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_FINAL_FORM_DESCRIPTION_1"] = "O Viber foi conectado ao seu Canal Aberto. Agora, todas as mensagens publicadas na sua conta pública serão redirecionadas para o seu Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEPS_TITLE_2"] = "Você encontrará mais informações sobre mudança de dados do Viber <a href=\"#URL#\" target=\"_blank\">neste artigo</a>.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_DESCRIPTION_ANDROID"] = "No Android:";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_DESCRIPTION_IOS"] = "No iOS:";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_5_TITLE"] = "Solicitação de Conta Pública";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_5_DESCRIPTION_1"] = "<strong>Atenção!</strong><br>
Se você enviou uma solicitação anteriormente e ela foi aprovada, agora você pode prosseguir para a <label style=\"cursor: pointer\" for=\"imconnector-viber-step-item-2-5\">etapa 2</label><br>
<br>
Caso contrário, siga este link e envie sua solicitação de conta pública:<br>
<a href=\"#URL#\" target=\"_blank\">#URL#</a><br>
<br>
Digite seu nome completo no campo \"Nome Completo\". <br>
\"Número de Telefone Viber\": digite o número de telefone que você especificou ao cadastrar sua conta Viber. Esta conta será usada como o administrador para a nova conta pública. Você pode adicionar mais administradores a qualquer momento mais tarde. <br>
\"Endereço de e-mail\": Digite seu endereço de e-mail válido. <br>
\"País\": especifique seu país. <br>
Aceite o Contrato de Licença. <br>
<hr>
Uma vez enviada, sua solicitação levará algum tempo para ser aprovada. Como regra geral, geralmente, leva 5 minutos.<br>
<hr>
Uma vez que sua solicitação for aprovada, uma notificação será enviada para o seu Viber (apenas para app iOS ou Android):
";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_TITLE"] = "Criar Conta Pública";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_DESCRIPTION_1"] = "Agora, clique no botão para criar uma conta pública.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_DESCRIPTION_2"] = "<strong>Se sua solicitação foi aprovada mas você não vê o botão, você precisa reiniciar o app móvel.</strong>";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_DESCRIPTION_3"] = "Preencha todos os campos para obter sua conta pública registrada:";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_4"] = "etapa 1 de 4";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_4"] = "etapa 2 de 4";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_3_OF_4"] = "etapa 3 de 4";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_4_OF_4"] = "etapa 4 de 4";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_4_TITLE"] = "Nova Solicitação de Conta Pública";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_4_DESCRIPTION_1"] = "Siga este link para enviar uma solicitação para conectar uma conta pública:<br>
<a href=\"https://support.viber.com/customer/widget/emails/new?interaction_name=NEWEA\" target=\"_blank\">https://support.viber.com/customer/widget/emails/new?interaction_name=NEWEA</a><br>
<br>
Após um curto período, a conta pública será conectada à sua conta Viber.<br>
Você receberá uma notificação no Viber assim que isso for feito.<br>
<br>
<strong>Atenção!</strong><br>
1. Sua solicitação de conta pública pode demorar vários dias para obter aprovação, que depende exclusivamente do Viber.<br>
 2. O número de telefone deve estar no formato internacional. Use apenas números.";
?>