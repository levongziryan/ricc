<?
$MESS["IMCONNECTOR_COMPONENT_VIBER_API_KEY"] = "Clé:";
$MESS["IMCONNECTOR_COMPONENT_VIBER_REGISTER"] = "Inscrire un compte public";
$MESS["IMCONNECTOR_COMPONENT_VIBER_TESTED"] = "Tester la connexion";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CONNECTOR_ERROR_STATUS"] = "Un problème est survenu avec votre compte public. Veuillez vous assurer que les paramètres fournis sont corrects et refaire le test du compte.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CREATE_A_PUBLIC_ACCOUNT"] = "Créer un compte public";
$MESS["IMCONNECTOR_COMPONENT_VIBER_AVAILABLE_PUBLIC_ACCOUNT"] = "J'ai un compte public";
$MESS["IMCONNECTOR_COMPONENT_VIBER_I_KNOW_KEY"] = "J'ai la clé";
$MESS["IMCONNECTOR_COMPONENT_VIBER_NAME_BOT"] = "Nom du compte public";
$MESS["IMCONNECTOR_COMPONENT_VIBER_LINK_PUBLIC_ACCOUNT"] = "Lien du compte public";
$MESS["IMCONNECTOR_COMPONENT_VIBER_LINK_CHAT_ONE_TO_ONE"] = "Lien du chat de personne à personne";
$MESS["IMCONNECTOR_COMPONENT_VIBER_PUBLIC_ACCOUNT"] = "Compte public";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CHAT_ONE_TO_ONE"] = "Chat de personne à personne";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEPS_TITLE"] = "Suivez ces étapes pour connecter votre compte public Viber";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_5"] = "étape 1 de 5";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5"] = "étape 2 de 5";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_3_OF_5"] = "étape 3 de 5";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_4_OF_5"] = "étape 4 de 5";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_5_OF_5"] = "étape 5 de 5";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_DESCRIPTION_2_PIC_ANDROID"] = "/images/imconnector-viber-2-android-en.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_DESCRIPTION_2_PIC_IOS"] = "/images/imconnector-viber-2-ios-en.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_DESCRIPTION_3_PIC"] = "/images/imconnector-viber-3-en.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_4_PIC_ANDROID"] = "/images/imconnector-viber-4-android-en.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_4_PIC_IOS"] = "/images/imconnector-viber-4-ios-en.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_5_PIC_ANDROID"] = "/images/imconnector-viber-5-android-en.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_5_PIC_IOS"] = "/images/imconnector-viber-5-ios-en.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_6_PIC"] = "/images/imconnector-viber-6-en.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_GENERAL_DESCRIPTION_7_PIC"] = "/images/imconnector-viber-7-en.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_3"] = "étape 1 de 3";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_3"] = "étape 2 de 3";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_3_OF_3"] = "étape 3 de 3";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_TITLE"] = "Obtenez la clé";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_1"] = "Exécutez l'application mobile (seules les applications Android et iOS version 6.5 et ultérieure sont prises en charge)
<br><br><br>
Allez dans la partie \"Comptes publics\"";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_2"] = "Sélectionnez votre compte public.
<br><br><br>
Allez dans la partie informations.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_3"] = "Sélectionnez \"Modifier les infos\"";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_GENERAL_TITLE"] = "Connectez le compte public";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_GENERAL_DESCRIPTION_1"] = "Copiez la clé:";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_GENERAL_DESCRIPTION_2"] = "Collez-la dans le champ ci-dessous et cliquez sur \"Enregistrer\"";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_GENERAL_DESCRIPTION_3"] = "Si nécessaire, modifiez plus de préférences du compte public ici: avatar visible pour vos contacts; nom du compte; description; balises, etc.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_3_OF_GENERAL_TITLE"] = "Terminer";
$MESS["IMCONNECTOR_COMPONENT_VIBER_SIMPLE_FORM_DESCRIPTION_1"] = "Si vous avez déjà un compte public Viber et que vous connaissez la clé, collez-la simplement dans le champ ci-dessous et cliquez sur \"Enregistrer\".";
$MESS["IMCONNECTOR_COMPONENT_VIBER_SIMPLE_FORM_DESCRIPTION_TESTED"] = "Vous devez maintenant vérifier que vos données sont correctes.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_SIMPLE_FORM_DESCRIPTION_REGISTER"] = "Vous pouvez maintenant inscrire votre compte public.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_INDEX_DESCRIPTION"] = "Connectez Viber au Canal ouvert pour recevoir les messages de vos clients dans le chat Bitrix24. Vous pouvez créer un nouveau compte Viber ou connecter un compte qui existe déjà.
<br>Si vous n'avez pas de compte public, nous vous aiderons à le créer et à le connecter à Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_FINAL_FORM_DESCRIPTION_1"] = "Viber a été connecté à votre Canal Ouvert. Tous les messages publiés seront désormais redirigés vers votre Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEPS_TITLE_2"] = "Vous trouverez plus de détails sur l'échange des données Viber <a href=\"#URL#\" target=\"_blank\">dans cet article</a>.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_DESCRIPTION_ANDROID"] = "Sur Android :";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_DESCRIPTION_IOS"] = "Sur iOS :";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_5_TITLE"] = "Demande de compte public";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_5_DESCRIPTION_1"] = "<strong>Attention !</strong><br>Si vous avez déjà envoyé une demande et que celle-ci a été approuvée, vous pouvez passer à <label style=\"cursor: pointer\" for=\"imconnector-viber-step-item-2-5\">l'étape 2</label><br><br>Dans le cas contraire, suivez ce lien et envoyez votre demande de compte public :<br><a href=\"#URL#\" target=\"_blank\">#URL#</a><br><br>Entrez votre nom complet dans le champ \"Nom complet\".<br>\"Numéro de téléphone Viber\" : entrez le numéro de téléphone que vous avez indiqué lors de l'enregistrement de votre compte Viber. Ce compte sera utilisé comme administrateur du nouveau compte public. Vous pouvez ajouter d'autres d'administrateurs à tout moment ultérieurement. <br>\"Adresse e-mail\" : entrez votre adresse e-mail valide. <br>\"Pays\" : indiquez votre pays. <br>Acceptez le Contrat de licence.<br><hr>Une fois envoyée, votre demande prendra quelque temps pour être approuvée. En règle générale, ce processus prend 5 minutes.<br><hr>Lorsque votre demande a été acceptée, une notification sera envoyée sur votre Viber (uniquement pour l'application iOS ou Android) :
";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_TITLE"] = "Créer un compte public";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_DESCRIPTION_1"] = "Maintenant, cliquez sur le bouton pour créer votre compte public.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_DESCRIPTION_2"] = "<strong>Si votre demande a été acceptée mais que vous ne voyez pas le bouton, vous devez redémarrer l’application mobile.</strong>";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_DESCRIPTION_3"] = "Remplissez tous les champs pour obtenir l'inscription de votre compte public :";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_4"] = "étape 1 de 4";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_4"] = "étape 2 de 4";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_3_OF_4"] = "étape 3 de 4";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_4_OF_4"] = "étape 4 de 4";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_4_TITLE"] = "Nouvelle demande de compte public";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_4_DESCRIPTION_1"] = "Suivez ce lien pour soumettre une demande de connexion d'un compte public:<br>
<a href=\"https://support.viber.com/customer/widget/emails/new?interaction_name=NEWEA\" target=\"_blank\">https://support.viber.com/customer/widget/emails/new?interaction_name=NEWEA</a><br>
<br>
Au bout d'un moment, le compte public sera connecté à votre compte Viber.<br>
Vous recevrez une notification lorsque cela aura été effectué.<br>
<br>
<strong>Attention!</strong><br>
1. Votre demande de compte public peut prendre plusieurs jours pour être approuvée, et dépend uniquement de Viber.<br>
2. Le numéro de téléphone doit être au format international. Utilisez des chiffres uniquement.";
?>