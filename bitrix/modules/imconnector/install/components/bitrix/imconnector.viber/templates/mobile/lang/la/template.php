<?
$MESS["IMCONNECTOR_COMPONENT_VIBER_API_KEY"] = "Clave:";
$MESS["IMCONNECTOR_COMPONENT_VIBER_LINK_CHAT_ONE_TO_ONE"] = "Enlace de persona a persona";
$MESS["IMCONNECTOR_COMPONENT_VIBER_SIMPLE_FORM_DESCRIPTION_1"] = "Pegue la clave Viber desde el Portapapeles y haga clic en \"Guardar\".";
$MESS["IMCONNECTOR_COMPONENT_VIBER_FINAL_FORM_DESCRIPTION_1"] = "Viber se ha conectado a su Canal Abierto. Ahora todos los mensajes enviados a su cuenta pública serán redirigidos a su Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_FINAL_FORM_DESCRIPTION_2"] = "<br>Ahora puede acceder a <a href='#URL_MOBILE#'>aplicación móvil Bitrix24</a> or <a href='#URL#'>abrir su cuenta Bitrix24 en el navegador</a>.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_NAME_BOT"] = "Cuenta pública conectada";
$MESS["IMCONNECTOR_COMPONENT_VIBER_LINK_PUBLIC_ACCOUNT"] = "Enlace de la cuenta pública";
?>