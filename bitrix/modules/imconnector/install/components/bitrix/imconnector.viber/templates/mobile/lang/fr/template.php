<?
$MESS["IMCONNECTOR_COMPONENT_VIBER_API_KEY"] = "Clé :";
$MESS["IMCONNECTOR_COMPONENT_VIBER_LINK_CHAT_ONE_TO_ONE"] = "Lien de chat tête-à-tête";
$MESS["IMCONNECTOR_COMPONENT_VIBER_SIMPLE_FORM_DESCRIPTION_1"] = "Collez la clé Viber depuis le presse-papiers et cliquez sur \"Enregistrer\".";
$MESS["IMCONNECTOR_COMPONENT_VIBER_FINAL_FORM_DESCRIPTION_1"] = "Viber a été connecté à votre Canal Ouvert. Tous les messages publiés sur votre compte public seront désormais redirigés vers votre Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_FINAL_FORM_DESCRIPTION_2"] = "<br>Vous pouvez maintenant accéder à <a href='#URL_MOBILE#'>l'application mobile Bitrix24</a> ou <a href='#URL#'>ouvrez votre compte Bitrix24 dans un navigateur</a>.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_NAME_BOT"] = "Compte public connecté";
$MESS["IMCONNECTOR_COMPONENT_VIBER_LINK_PUBLIC_ACCOUNT"] = "Lien vers le compte public";
?>