<?
$MESS["IMCONNECTOR_COMPONENT_VIBER_REGISTER"] = "Реєстрація публічного акаунту";
$MESS["IMCONNECTOR_COMPONENT_VIBER_TESTED"] = "Тестування з'єднання";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CONNECTOR_ERROR_STATUS"] = "У процесі роботи вашого публічного акаунта виникла помилка. Вам необхідно перевірити зазначені дані та протестувати публічний акаунт повторно.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CREATE_A_PUBLIC_ACCOUNT"] = "Створити публічний акаунт";
$MESS["IMCONNECTOR_COMPONENT_VIBER_NAME_BOT"] = "Ім'я публічного акаунту";
$MESS["IMCONNECTOR_COMPONENT_VIBER_SIMPLE_FORM_DESCRIPTION_TESTED"] = "Вам потрібно протестувати коректність ваших даних.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_SIMPLE_FORM_DESCRIPTION_REGISTER"] = "Тепер ви можете зареєструвати свій публічний аккаунт.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_INDEX_DESCRIPTION"] = "Підключіть Viber до Відкритої лінії щоб приймати звернення ваших клієнтів в робочому чаті Бітрікс24. Для підключення необхідно створити публічний акаунт у Viber або підключити вже існуючий.
<br>Якщо у вас ще немає публічного аккаунта, ми допоможемо створити його в кілька кроків і підключити до вашого Бітрікс24";
$MESS["IMCONNECTOR_COMPONENT_VIBER_FINAL_FORM_DESCRIPTION_1"] = "Viber успішно підключений до вашої Відкритої лінії. Тепер всі звернення до вашого публічного аккаунту будуть автоматично спрямовані на ваш Бітрікс24.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_API_KEY"] = "Ключ:";
$MESS["IMCONNECTOR_COMPONENT_VIBER_AVAILABLE_PUBLIC_ACCOUNT"] = "Є публічний акаунт";
$MESS["IMCONNECTOR_COMPONENT_VIBER_I_KNOW_KEY"] = "Я знаю ключ";
$MESS["IMCONNECTOR_COMPONENT_VIBER_LINK_PUBLIC_ACCOUNT"] = "Посилання на публічний запис";
$MESS["IMCONNECTOR_COMPONENT_VIBER_LINK_CHAT_ONE_TO_ONE"] = "Посилання на чат один на один";
$MESS["IMCONNECTOR_COMPONENT_VIBER_PUBLIC_ACCOUNT"] = "Публічний акаунт";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CHAT_ONE_TO_ONE"] = "Чат один на один";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEPS_TITLE"] = "Пройдіть кілька кроків, щоб підключити ваш публічний акаунт у Viber";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_5"] = "крок 1 з 5";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5"] = "крок 2 з 5";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_3_OF_5"] = "крок 3 з 5";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_4_OF_5"] = "крок 4 з 5";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_5_OF_5"] = "крок 5 з 5";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_3"] = "крок 1 з 3";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_3"] = "крок 2 з 3";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_3_OF_3"] = "крок 3 з 3";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_TITLE"] = "Отримання ключа";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_1"] = "Запустіть мобільний додаток (підтримується тільки Android і iOS додатки версії 6.5 і старше)
<br><br><br>
Перейдіть в розділ \"публічні акаунти\"";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_2"] = "Виберіть свій публічний аккаунт.
<br><br><br>
Перейдіть в розділ інформації.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_3"] = "Виберіть \"змінити\"";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_GENERAL_TITLE"] = "Підключення публічного акаунта";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_GENERAL_DESCRIPTION_1"] = "Скопіюйте ключ:";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_GENERAL_DESCRIPTION_2"] = "Вставте його в поле нижче та натисніть кнопку \"Зберегти\"";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_GENERAL_DESCRIPTION_3"] = "У цьому ж розділі ви можете налаштувати додаткові параметри публічного акаунта, такі як:
Аватарка, яка буде показуватися вашим клієнтам, ім'я акаунту, опис, теги, за яким вас зможуть знайти і т. п.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_3_OF_GENERAL_TITLE"] = "Завершення";
$MESS["IMCONNECTOR_COMPONENT_VIBER_SIMPLE_FORM_DESCRIPTION_1"] = "Якщо у вас вже є публічний акаунт у Viber і ви знаєте його ключ, просто введіть його в поле нижче та натисніть кнопку \"Зберегти\"";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEPS_TITLE_2_URL"] = "https://www.bitrix24.ru/blogs/community_blog/viber_and_bitrix24.php";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_DESCRIPTION_2_PIC_ANDROID"] = "/images/imconnector-viber-2-android-ru.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_DESCRIPTION_2_PIC_IOS"] = "/images/imconnector-viber-2-ios-ru.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_DESCRIPTION_3_PIC"] = "/images/imconnector-viber-3-ru.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_4_PIC_ANDROID"] = "/images/imconnector-viber-4-android-ru.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_4_PIC_IOS"] = "/images/imconnector-viber-4-ios-ru.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_5_PIC_ANDROID"] = "/images/imconnector-viber-5-android-ru.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_5_PIC_IOS"] = "/images/imconnector-viber-5-ios-ru.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_6_PIC"] = "/images/imconnector-viber-6-ru.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_GENERAL_DESCRIPTION_7_PIC"] = "/images/imconnector-viber-7-ru.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEPS_TITLE_2"] = "Ви можете отримати більш детальну інформацію про роботу з viber <a href=\"#URL#\" target=\"_blank\">в нашій статті.</a>";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_DESCRIPTION_ANDROID"] = "На Android:";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_DESCRIPTION_IOS"] = "На iOS:";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_5_TITLE"] = "Заявка на паблік аккаунт";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_5_DESCRIPTION_1"] = "<strong>Увага!</strong><br>
Якщо ви вже залишали заявку і вона була схвалена: можна відразу переходити до <label style=\"cursor: pointer\" for=\"imconnector-viber-step-item-2-5\">кроку 2</label><br>
<br>
Перейдіть за вказаним посиланням і залиште заявку на паблік акаунт:<br>
<a href=\"#URL#\" target=\"_blank\">#URL#</a><br>
<br>
У полі \"Full Name\" вкажіть повне ім'я. <br>
Поле \"Viber Phone Number\" - необхідно вказати номер телефону, на який зареєстрований діючий акаунт Viber. Цей аккаунт буде адміністратором створюваного публічного облікового запису. Пізніше ви зможете додати ще адміністраторів. <br>
Поле \"Email address\" - вкажіть адресу електронної пошти. <br>
Поле \"Country\" - це вкажіть країну. <br>
І погодьтеся з ліцензійними умовами. <br>
<hr>
Після відправки потрібен деякий час на розгляд заявки. Як правило все відбувається протягом 5 хвилин.<br> 
<hr>
Після того, як ваша заявка буде схвалена, ви отримаєте відповідне повідомлення в свій viber (тільки в мобільний застосунок під iOS або android): ";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_TITLE"] = "Створення паблік акаунту";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_DESCRIPTION_1"] = "Необхідно натиснути на кнопку створення \"Паблік акаунту\".";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_DESCRIPTION_2"] = "<strong>Якщо у вас немає цієї кнопки, але заявка схвалена: вам необхідно перезавантажити ваш мобільний застосунок!</strong>";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_DESCRIPTION_3"] = "Заповніть всі поля і ви отримаєте зареєстрований паблік аккаунт:";
?>