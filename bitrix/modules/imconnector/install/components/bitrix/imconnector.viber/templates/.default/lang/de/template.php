<?
$MESS["IMCONNECTOR_COMPONENT_VIBER_API_KEY"] = "Schlüssel:";
$MESS["IMCONNECTOR_COMPONENT_VIBER_REGISTER"] = "Öffentlichen Account registrieren";
$MESS["IMCONNECTOR_COMPONENT_VIBER_TESTED"] = "Testverbindung";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CONNECTOR_ERROR_STATUS"] = "Es gab ein Problem mit Ihrem öffentlichen Account. Überprüfen Sie die angegebenen Parameter, dann testen Sie den Account erneut.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CREATE_A_PUBLIC_ACCOUNT"] = "Öffentlichen Account erstellen";
$MESS["IMCONNECTOR_COMPONENT_VIBER_AVAILABLE_PUBLIC_ACCOUNT"] = "Ich habe einen öffentlichen Account";
$MESS["IMCONNECTOR_COMPONENT_VIBER_I_KNOW_KEY"] = "Ich habe den Schlüssel";
$MESS["IMCONNECTOR_COMPONENT_VIBER_NAME_BOT"] = "Name des öffentlichen Accounts";
$MESS["IMCONNECTOR_COMPONENT_VIBER_LINK_PUBLIC_ACCOUNT"] = "Link zum öffentlichen Account";
$MESS["IMCONNECTOR_COMPONENT_VIBER_LINK_CHAT_ONE_TO_ONE"] = "Link zum Unter-vier-Augen Chat";
$MESS["IMCONNECTOR_COMPONENT_VIBER_PUBLIC_ACCOUNT"] = "Öffentlicher Account";
$MESS["IMCONNECTOR_COMPONENT_VIBER_CHAT_ONE_TO_ONE"] = "Unter-vier-Augen Chat";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEPS_TITLE"] = "Folgen Sie diesen Schritten, um Ihren öffentlichen Viber Account anzubinden";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEPS_TITLE_2"] = "Weitere Informationen über den Datenaustausch mit Viber finden Sie <a href=\"#URL#\" target=\"_blank\">in diesem Artikel</a>.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEPS_TITLE_2_URL"] = "https://www.bitrix24.de/about/blogs/tipps/integrieren-sie-ihr-bitrix24-account-mit-viber.php";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_5"] = "Schritt 1 von 5";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5"] = "Schritt 2 von 5";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_3_OF_5"] = "Schritt 3 von 5";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_4_OF_5"] = "Schritt 4 von 5";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_5_OF_5"] = "Schritt 5 von 5";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_DESCRIPTION_ANDROID"] = "Auf Android:";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_DESCRIPTION_IOS"] = "Auf iOS:";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_5_TITLE"] = "Anfrage bezüglich eines öffentlichen Accounts";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_5_DESCRIPTION_1"] = "<strong>Achtung!</strong><br>
Wenn Sie eine Anfrage bereits gesendet haben und sie genehmigt wurde, machen Sie den <label style=\"cursor: pointer\" for=\"imconnector-viber-step-item-2-5\">Schritt 2</label><br>
<br>
Ansonsten können Sie diesem Link folgen und eine Anfrage bezüglich eines öffentlichen Accounts einreichen:<br>
<a href=\"#URL#\" target=\"_blank\">#URL#</a><br>
<br>
Geben Sie Ihren vollständigen Namen im Feld \"Full Name\" ein. <br>
\"Viber Phone Number\": geben Sie hier die Telefonnummer ein, die Sie bei Registrierung Ihres Viber Accounts angegeben haben. Dieser Account wird als ein administrativer für einen neuen öffentlichen Account genutzt. Sie können später weitere Administratoren hinzufügen. <br>
\"Email address\": geben Sie Ihre gültige E-Mail-Adresse ein. <br>
\"Country\": Geben Sie Ihr Land an. <br>
Akzeptieren Sie die Nutzungsbedingungen. <br>
<hr>
Die Bearbeitung Ihrer Anfrage wird einige Zeit in Anspruch nehmen. In der Regel dauert das 5 Minuten.<br> 
<hr>
Nachdem Ihre Anfrage genehmigt wurde, erhalten Sie eine Benachrichtigung, die an Ihr Viber gesendet wird (nur an iOS oder Android Apps): 
";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_5_DESCRIPTION_1_URL"] = "https://www.viber.com/de/public-accounts";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_TITLE"] = "Öffentlichen Account erstellen";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_DESCRIPTION_1"] = "Klicken Sie auf die Schaltfläche, um Ihren öffentlichen Account zu erstellen.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_DESCRIPTION_2_PIC_ANDROID"] = "/images/imconnector-viber-2-android-de.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_DESCRIPTION_2_PIC_IOS"] = "/images/imconnector-viber-2-ios-de.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_DESCRIPTION_2"] = "<strong>Wurde Ihre Anfrage genehmigt, Sie sehen aber keine Schaltfläche, sollten Sie Ihre mobile App neu starten.</strong>";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_DESCRIPTION_3"] = "Füllen Sie alle Felder aus, um Ihren öffentlichen Account zu registrieren:";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_5_DESCRIPTION_3_PIC"] = "/images/imconnector-viber-3-de.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_4_PIC_ANDROID"] = "/images/imconnector-viber-4-android-de.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_4_PIC_IOS"] = "/images/imconnector-viber-4-ios-de.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_5_PIC_ANDROID"] = "/images/imconnector-viber-5-android-de.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_5_PIC_IOS"] = "/images/imconnector-viber-5-ios-de.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_6_PIC"] = "/images/imconnector-viber-6-de.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_GENERAL_DESCRIPTION_7_PIC"] = "/images/imconnector-viber-7-de.jpg";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_3"] = "Schritt 1 von 3";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_3"] = "Schritt 2 von 3";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_3_OF_3"] = "Schritt 3 von 3";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_TITLE"] = "Schlüssel anfordern";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_1"] = "Die mobile App starten (unterstützt werden nur die Android und iOS Apps ab Version 6.5)
<br><br><br>
Wechseln Sie in den Bereich \"Öffentliche Accounts\"
";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_2"] = "Wählen Sie Ihren öffentlichen Account aus.
<br><br><br>
Wechseln Sie in den Informationsbereich.
";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_1_OF_GENERAL_DESCRIPTION_3"] = "Klicken Sie auf \"Infos bearbeiten\"";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_GENERAL_TITLE"] = "Öffentlichen Account anbinden";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_GENERAL_DESCRIPTION_1"] = "Den Schlüssel kopieren:";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_GENERAL_DESCRIPTION_2"] = "Fügen Sie ihn ins Feld unterhalb ein und klicken auf \"Speichern\"";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_2_OF_GENERAL_DESCRIPTION_3"] = "Falls erforderlich, können Sie weitere Einstellungen für Ihren öffentlichen Account vornehmen: Avatar, welcher für Ihre Kontakte sichtbar sein wird; Account-Name; Beschreibung; Tags etc.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_STEP_3_OF_GENERAL_TITLE"] = "Fertigstellen";
$MESS["IMCONNECTOR_COMPONENT_VIBER_SIMPLE_FORM_DESCRIPTION_1"] = "Wenn Sie einen öffentlichen Viber Account und den Schlüssel bereits haben, fügen Sie ihn einfach ins Feld unterhalb ein und klicken auf \"Speichern\"";
$MESS["IMCONNECTOR_COMPONENT_VIBER_SIMPLE_FORM_DESCRIPTION_TESTED"] = "Sie müssen jetzt überprüfen, ob Ihre Daten korrekt sind.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_SIMPLE_FORM_DESCRIPTION_REGISTER"] = "Jetzt können Sie Ihren öffentlichen Account registrieren.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_INDEX_DESCRIPTION"] = "Verbinden Sie Viber mit Ihrem Kommunikationskanal, um die Nachrichten Ihrer Kunden im Bitrix24 Chat zu erhalten. Sie können einen neuen Viber Account erstellen oder den bereits existierenden verbinden.
<br>Wenn Sie noch keinen öffentlichen Account haben, werden wir Ihnen helfen, einen zu erstellen und ihn mit Ihrem Bitrix24 zu verbinden.
";
$MESS["IMCONNECTOR_COMPONENT_VIBER_FINAL_FORM_DESCRIPTION_1"] = "Viber wurde mit Ihrem Kommunikationskanal verbunden. Alle Nachrichten, die jetzt in Ihrem öffentlichen Account erscheinen, werden an Ihr Bitrix24 weitergeleitet.";
?>