<?
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_DEL_REFERENCE"] = "Desconectar";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_STEP_1_OF_3_TITLE"] = "Autenticación";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_STEP_2_OF_3_TITLE"] = "Seleccionar página";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_STEP_3_OF_3_TITLE"] = "Terminar";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_STEP_N_OF_3"] = "Paso #STEP# de 3";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_PAGE"] = "Página";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_DESCRIPTION"] = "Conecta la página de Facebook de tu empresa a Canal Abierto para administrar comentarios dejados a publicaciones, fotos y videos de tu Bitrix24. Para conectarse, necesitas tener una página de Facebook existente o crear una. Tienes que ser el administrador de esa página.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CONNECT_PAGE"] = "Página de conexión";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_PAGE"] = "Inicie sesión con la cuenta de administración de la página de Facebook para administrar los comentarios directamente desde Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_EXPIRED_ACCOUNT_TOKEN"] = "Inicie sesión con su cuenta de Facebook para hacer modificaciones.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_EXPIRED_ACCOUNT_TOKEN_WARNING"] = "Observe que la página se desconectará de Bitrix24 si inicia sesión con una cuenta que no está vinculada a esta página.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_THERE_IS_NO_PAGE_WHERE_THE_ADMINISTRATOR"] = "Usted no tiene una página de Facebook de la que usted es un administrador. <br>Cree uno ahora mismo.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_TO_CREATE_A_PAGE"] = "Crear";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CONNECTED_PAGE"] = "Página conectada";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CHANGE_PAGE"] = "Cambiar";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_MY_OTHER_PAGES"] = "Mis otras páginas de Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_SELECT_THE_PAGE"] = "Seleccione una página de Facebook pública para conectarse a Bitrix24 Canal Abierto";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_OTHER_PAGES"] = "Otras páginas";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CONNECTOR_ERROR_STATUS"] = "Ocurrió un error. Por favor revisa tu configuración.";
?>