<?
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_DEL_REFERENCE"] = "Desvincular";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_STEP_1_OF_3_TITLE"] = "Autenticação";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_STEP_2_OF_3_TITLE"] = "Selecionar página";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_STEP_3_OF_3_TITLE"] = "Concluir";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_STEP_N_OF_3"] = "Etapa #STEP# de 3";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_PAGE"] = "Página";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_DESCRIPTION"] = "Conecte a página do Facebook da sua empresa ao Canal Aberto para gerenciar comentários deixados nas postagens, fotos e vídeos do seu Bitrix24 . Para conectar, você precisa ter uma página do Facebook já existente ou criar uma. Você tem que ser o administrador da página.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CONNECT_PAGE"] = "Conectar página";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_PAGE"] = "Fazer login usando a conta de administrador da página do Facebook para gerenciar comentários diretamente a partir do Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_EXPIRED_ACCOUNT_TOKEN"] = "Fazer login usando sua conta do Facebook para fazer alterações.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_EXPIRED_ACCOUNT_TOKEN_WARNING"] = "Observe que a página será desconectada do Bitrix24 se você fizer login usando uma conta que não está vinculada a esta página.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_THERE_IS_NO_PAGE_WHERE_THE_ADMINISTRATOR"] = "Você não tem uma página do Facebook na qual você é administrador. <br>Crie uma agora mesmo.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_TO_CREATE_A_PAGE"] = "Criar";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CONNECTED_PAGE"] = "Página conectada";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CHANGE_PAGE"] = "Alterar";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_MY_OTHER_PAGES"] = "Minhas outras páginas do Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_SELECT_THE_PAGE"] = "Selecione uma página pública do Facebook para conectar ao Canal Aberto Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_OTHER_PAGES"] = "Outras páginas";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_REPEATING_ERROR"] = "Se o problema persistir, talvez você precise desconectar o canal e configurá-lo novamente.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CONNECTOR_ERROR_STATUS"] = "Ocorreu um erro. Verifique suas configurações.";
?>