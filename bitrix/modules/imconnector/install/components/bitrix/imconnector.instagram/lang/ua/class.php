<?
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_MODULE_NOT_INSTALLED"] = "Модуль коннекторів месенджерів не встановлено";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_NO_ACTIVE_CONNECTOR"] = "Даний коннектор не активний";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_SESSION_HAS_EXPIRED"] = "Ваша сесія закінчилася. Надішліть форму ще раз";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_CONNECTOR_ERROR_STATUS"] = "В ході роботи виникла помилка. Будь ласка, перевірте налаштування.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_ERROR_REQUEST_INFORMATION_FROM_SERVER"] = "Виникла помилка на сервері";
?>