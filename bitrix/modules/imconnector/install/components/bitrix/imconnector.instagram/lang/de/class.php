<?
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_MODULE_NOT_INSTALLED"] = "Das Modul \"Externe IM Connectors\" ist nicht installiert.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_NO_ACTIVE_CONNECTOR"] = "Dieser Connector ist inaktiv.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_SESSION_HAS_EXPIRED"] = "Ihre Sitzung ist abgelaufen. Senden Sie bitte das Formular erneut.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_CONNECTOR_ERROR_STATUS"] = "Es gab einen Fehler. Überprüfen Sie bitte Ihre Einstellungen.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_ERROR_REQUEST_INFORMATION_FROM_SERVER"] = "Server-Fehler.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_OK_DEL_USER"] = "Die Verknüpfung mit Ihrem Account wurde erfolgreich gelöscht.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_NO_DEL_USER"] = "Die Verknüpfung mit dem Account kann nicht gelöscht werden.";
?>