<?
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_MODULE_NOT_INSTALLED"] = "El módulo \"External IM Connectors\" no está instalado.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_NO_ACTIVE_CONNECTOR"] = "Este conector está inactivo.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_SESSION_HAS_EXPIRED"] = "Su sesión ha caducado. Envíe el formulario nuevamente.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_REMOVED_REFERENCE_TO_ENTITY"] = "Este conector se ha configurado para su uso en grupo, página pública o evento al que actualmente no tiene acceso administrativo.<br>
Configure el conector de nuevo.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_OK_DEL_USER"] = "Su cuenta de usuario se ha desvinculado";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_NO_DEL_USER"] = "No se puede desvincular su cuenta de usuario";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_OK_DEL_ENTITY"] = "Grupo, página pública o evento se ha desvinculado.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_NO_DEL_ENTITY"] = "No se puede desvincular grupo, página pública o evento.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_CONNECTOR_ERROR_STATUS"] = "Hubo un error. Compruebe sus preferencias.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_ERROR_REQUEST_INFORMATION_FROM_SERVER"] = "Error del Servidor.";
?>