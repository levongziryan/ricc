<?
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_MODULE_NOT_INSTALLED"] = "Le module \"Connecteurs de messagerie instantanée externe\" n'est pas installé.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_NO_ACTIVE_CONNECTOR"] = "Ce connecteur est inactif.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_SESSION_HAS_EXPIRED"] = "Votre session a expiré. Veuillez renvoyer le formulaire.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_REMOVED_REFERENCE_TO_ENTITY"] = "Ce connecteur a été configuré pour être utilisé avec un groupe, une page publique ou un évènement pour lesquels vous n'avez actuellement pas d'accès administrateur.<br>
Veuillez reconfigurer le connecteur.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_OK_DEL_USER"] = "Votre compte utilisateur a été dissocié";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_NO_DEL_USER"] = "Impossible de dissocier votre compte utilisateur";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_OK_DEL_ENTITY"] = "Le groupe, la page publique ou l'évènement a été dissocié(e).";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_NO_DEL_ENTITY"] = "Impossible de dissocier le groupe, la page publique ou l'évènement.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_CONNECTOR_ERROR_STATUS"] = "Il y a eu une erreur. Veuillez vérifier vos préférences.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_ERROR_REQUEST_INFORMATION_FROM_SERVER"] = "Erreur du serveur.";
?>