<?
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_DEL_REFERENCE"] = "Видалити прив'язку";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_STEP_1_OF_2_TITLE"] = "Авторизація";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_STEP_2_OF_2_TITLE"] = "Завершення";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_STEP_N_OF_2"] = "крок #STEP# 2";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_INDEX_DESCRIPTION_NEW"] = "Підключіть Instagram до Відкритої лінії щоб приймати коментарі ваших клієнтів, залишені до ваших публікацій, в робочому чаті Бітрікс24.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_USER"] = "Акаунт";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_ENTITY"] = "Увійдіть під акаунтом Instagram, щоб приймати коментарі до ваших постів від ваших клієнтів усередині Бітрікс24.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_CONNECT"] = "Підключити";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_FINAL_DESCRIPTION_1_URL"] = "https://helpdesk.bitrix24.ua/open/4779109/#limitations";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_FINAL_DESCRIPTION_1"] = "Зверніть увагу, що коментарі з Instagram забираються не безперервно, а раз в 15 хвилин!<br>
Більш детальніше про особливості роботи з Instagram можна прочитати <a href=\"#URL#\" target=\"_blank\">в нашій статті</a>.";
?>