<?
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_DEL_REFERENCE"] = "Desconectar";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_STEP_1_OF_2_TITLE"] = "Iniciar sesión";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_STEP_2_OF_2_TITLE"] = "Terminar";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_STEP_N_OF_2"] = "paso #STEP# de 2";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_INDEX_DESCRIPTION_NEW"] = "Conecte Instagram a Open Channel para recibir en Bitrix 24 comentarios de chat dejados por sus clientes.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_USER"] = "Cuenta";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_ENTITY"] = "Inicie sesión con su cuenta Instagram para recibir comentarios de sus clientes";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_CONNECT"] = "Conectar";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_FINAL_DESCRIPTION_1_URL"] = "https://helpdesk.bitrix24.com/open/4779313/#limitations";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_FINAL_DESCRIPTION_1"] = "Tenga en cuenta que el sistema toma los comentarios de Instagram cada 15 minutos en vez de que sea instantáneamente.<br>
Encontrará más detalles sobre el intercambio de datos Instagram <a href=\"#URL#\" target=\"_blank\">in this article</a>.";
?>