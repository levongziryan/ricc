<?
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_DEL_REFERENCE"] = "Desconectar";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_STEP_1_OF_2_TITLE"] = "Fazer login";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_STEP_2_OF_2_TITLE"] = "Concluir";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_STEP_N_OF_2"] = "etapa #STEP# de 2";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_INDEX_DESCRIPTION_NEW"] = "Conecte o Instagram ao Canal Aberto para receber no bate-papo Bitrix 24 os comentários deixados por seus clientes.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_USER"] = "Conta";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_ENTITY"] = "Faça login usando sua conta do Instagram para receber os comentários de seus clientes";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_CONNECT"] = "Conectar";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_FINAL_DESCRIPTION_1"] = "Observe que o sistema pega comentários do Instagram a cada 15 minutos e não instantaneamente.<br>Você encontrará mais informações sobre mudança de dados do Instagram <a href=\"#URL#\" target=\"_blank\">neste artigo</a>.";
?>