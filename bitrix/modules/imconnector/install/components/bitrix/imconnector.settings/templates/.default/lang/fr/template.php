<?
$MESS["IMCONNECTOR_COMPONENT_SETTINGS_RELOAD"] = "recharger";
$MESS["IMCONNECTOR_COMPONENT_SETTINGS_NO_CONNECT_CONNECTOR"] = "Il n'y a aucun connecteur auquel se connecter.";
$MESS["IMCONNECTOR_COMPONENT_SETTINGS_SOON"] = "bientôt";
$MESS["IMCONNECTOR_COMPONENT_SETTINGS_SOON_VIBER"] = "Viber";
$MESS["IMCONNECTOR_COMPONENT_SETTINGS_CONFIG_EDIT_CONNECTORS"] = "Connecter les canaux de communication";
$MESS["IMCONNECTOR_COMPONENT_SETTINGS_COPIED_TO_CLIPBOARD"] = "Copié dans le presse-papiers";
$MESS["IMCONNECTOR_COMPONENT_SETTINGS_FAILED_TO_COPY"] = "Impossible de copier dans le presse-papiers";
$MESS["IMCONNECTOR_COMPONENT_SETTINGS_VERY_SOON"] = "bientôt";
$MESS["IMCONNECTOR_COMPONENT_SETTINGS_CONFIRM_DISABLE_BUTTON_OK"] = "OK";
$MESS["IMCONNECTOR_COMPONENT_SETTINGS_CONFIRM_DISABLE_BUTTON_CANCEL"] = "Annuler";
$MESS["IMCONNECTOR_COMPONENT_SETTINGS_CONFIRM_DISABLE"] = "Ce canal de communication va être déconnecté, toutes vos préférences seront supprimées. Continuer?";
$MESS["IMCONNECTOR_COMPONENT_SETTINGS_CONFIRM_DISABLE_TITLE"] = "Confirmer la déconnexion";
?>