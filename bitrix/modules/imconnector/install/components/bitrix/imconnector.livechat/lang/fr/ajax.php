<?
$MESS["IMCONNECTOR_PERMISSION_DENIED"] = "Vous n'avez pas les permissions pour accéder à cette fonction.";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_MODULE_IMOPENLINES_NOT_INSTALLED"] = "Le module \"Canaux ouverts\" n'est pas installé.";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_NO_ACTIVE_CONNECTOR"] = "Le connecteur est inactif.";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SESSION_HAS_EXPIRED"] = "Votre session a expiré. Veuillez renvoyer le formulaire.";
?>