<?
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_MODULE_IMCONNECTOR_NOT_INSTALLED"] = "El módulo \"External IM Connectors\" no está instalado.";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_MODULE_IMOPENLINES_NOT_INSTALLED"] = "El módulo \"Open Channels\" no está instalado.";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_NO_ACTIVE_CONNECTOR"] = "Esta conexión está inactiva.";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SESSION_HAS_EXPIRED"] = "Su sesión ha caducado. Por favor envíe el formulario de nuevo.";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_OK_SAVE"] = "La información se ha guardado correctamente.";
?>