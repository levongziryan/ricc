<?
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_BOT_HANDLE_NAME"] = "ID du bot (bot handle):";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_APP_ID_NAME"] = "ID d'App (Microsoft App ID):";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_APP_SECRET_NAME"] = "Mot de passe secret d'App:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_TESTED"] = "Tester la connexion";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_INDEX_DESCRIPTION"] = "Connectez Microsoft Bot Framework au Canal Ouvert pour recevoir les messages de vos clients depuis Skype et d'autres sources supportées (Slack, Kik, GroupMe, SMS, e-mail etc.) directement dans votre Bitrix24.<br><br>Nous vous aiderons à créer un bot et à y connecter votre Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CREATE_NEW_BOT"] = "Créer un nouveau bot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_FORM_SETTINGS"] = "J'ai déjà un bot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECTOR_ERROR_STATUS"] = "Votre bot a rencontré une erreur. Veuillez vérifier les préférences et refaire le test du bot.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_GET_LINKS"] = "Obtenir les liens sur le site Microsoft";
$MESS["IMCONNECTOR_COMPONENT_FINAL_FORM_DESCRIPTION_OK_1"] = "Microsoft Bot Framework a été connecté à votre Canal Ouvert. Désormais, tous les messages envoyés sur le bot seront transférés vers votre Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FINAL_FORM_DESCRIPTION_OK_2"] = "Vous pouvez connecter d'autres sources de communication au bot ou configurer les sources existantes pour transférer tous les messages vers le chat.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_10_STEPS_TITLE"] = "Suivez ces étapes pour connecter Microsoft Bot Framework au Canal ouvert";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_1_OF_10_TITLE"] = "Se connecter au site Microsoft";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_2_OF_10_TITLE"] = "Inscrire un nouveau bot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_3_OF_10_TITLE"] = "Création d'App";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_4_OF_10_TITLE"] = "Obtenir un mot de passe";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_5_OF_10_TITLE"] = "Informations supplémentaires";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_6_OF_10_TITLE"] = "Accord";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_7_OF_10_TITLE"] = "Publier le bot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_8_OF_10_TITLE"] = "Options";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_9_OF_10_TITLE"] = "Le bot est en cours d'examen";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_10_OF_10_TITLE"] = "Prêt!";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_1_OF_10"] = "étape 1 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_2_OF_10"] = "étape 2 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_3_OF_10"] = "étape 3 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_4_OF_10"] = "étape 4 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_5_OF_10"] = "étape 5 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_6_OF_10"] = "étape 6 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_7_OF_10"] = "étape 7 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_8_OF_10"] = "étape 8 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_9_OF_10"] = "étape 9 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_10_OF_10"] = "étape 10 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_1_OF_10_DESCRIPTION_1"] = "Allez sur le site Microsoft et connectez-vous en utilisant votre compte:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_1_OF_10_DESCRIPTION_2"] = "Inscrivez le bot en sélectionnant \"Inscrire un bot\" dans le menu supérieur";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_2_OF_10_DESCRIPTION_1"] = "Indiquez les informations requises sur votre nouveau bot.<br>
<br>
- Téléchargez une image de logo.
Seuls les fichiers PNG inférieurs à 30 Ko sont acceptés<br>
- Entrez le nom de votre bot<br>
- Indiquez un ID de bot unique<br>
- Entrez la description du bot<br>
<br>
Ces données seront disponibles après la publication de votre bot sur le site Microsoft.<br>
Tous les champs sont requis.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_2_OF_10_DESCRIPTION_2"] = "Spécifiez le nom du bot (bot handle) que vous avez indiqué:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_2_OF_10_DESCRIPTION_3"] = "Copiez l'adresse spécifiée ci-dessous et collez-la dans le champ \"Messaging endpoint\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_3_OF_10_DESCRIPTION_1"] = "Appuyez sur le bouton \"Créer un Microsoft App ID et un mot de passe\"";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_3_OF_10_DESCRIPTION_2"] = "Une nouvelle fenêtre apparaîtra. Copiez l'App ID et entrez-le dans le champ ci-dessous";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_4_OF_10_DESCRIPTION_1"] = "Appuyez sur le bouton \"Créer un mot de passe pour continuer\"";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_4_OF_10_DESCRIPTION_2"] = "Copiez le mot de passe dans la nouvelle fenêtre qui s'ouvre";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_4_OF_10_DESCRIPTION_3"] = "Important! Le mot de passe vous sera montré une seule fois. Nous vous conseillons de le conserver dans un endroit sûr. <br><br>Entrez le mot de passe dans le champ ci-dessous.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_4_OF_10_DESCRIPTION_4"] = "Appuyez sur le bouton \"Terminer l'opération et retourner au Bot Framework\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_5_OF_10_DESCRIPTION_1"] = "Indiquez les informations qui seront disponibles sur le site Microsoft dans la description de votre bot.
Les champs suivants sont requis.<br>
<br>
Nom de l'éditeur - votre nom<br>
E-mail de l'éditeur - votre e-mail<br>
Déclaration de confidentialité - URL vers votre déclaration de confidentialité<br>
Conditions d'utilisation - URL vers la page de vos Conditions d'utilisation";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_6_OF_10_DESCRIPTION_1"] = "Lisez les conditions d'utilisation, confirmez votre accord en cochant la case et cliquez sur \"Inscrire\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_7_OF_10_DESCRIPTION_1"] = "Le bot a été créé. Vous devez maintenant le publier. Passez en revue les informations et, si tout est correct, cliquez sur \"Publier\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_8_OF_10_DESCRIPTION_1"] = "Votre bot doit maintenant être vérifié. Cochez l'option de publication de votre bot dans la liste de bot pour le rendre visible aux autres, et cliquez sur \"Soumettre pour révision\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_9_OF_10_DESCRIPTION_1"] = "Assurez-vous que votre bot a été soumis pour vérification: le statut devrait être \"En cours de révision\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_9_OF_10_DESCRIPTION_2"] = "Notez que vous pouvez obtenir les liens vers les applications connectées à votre bot uniquement sur cette page.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_9_OF_10_DESCRIPTION_3"] = "Skype et le Chat Live sont déjà connectés. Ils ne nécessitent aucune configuration.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SIMPLE_FORM_DESCRIPTION_1"] = "Si votre bot a déjà été inscrit, connectez-le à Bitirix24.<br><br>Spécifiez cette adresse dans le champ \"Messaging Webhook\" sur le site Microsoft:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SIMPLE_FORM_DESCRIPTION_TESTED"] = "Vous pouvez vérifier l'exactitude de vos données.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_COPY"] = "Copier";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TWILIO_PLACEHOLDER"] = "15615155866";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_DESCRIPTION"] = "Vous pouvez spécifier des liens publics vers les canaux que vous avez connectés à Microsoft pour les afficher dans le widget (chat live) sur le site.<br><br>Pour obtenir les liens, allez dans LINK_BEGIN#votre compte Microsoft Bot Framework#LINK_END# et cliquez sur \"Obtenir les codes embarqués du bot\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_TITLE"] = "spécifiez les liens vers les canaux de communication";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_CHANNELS_DESCRIPTION"] = "Dans votre #LINK_BEGIN#compte Microsoft Bot Framework#LINK_END#, copiez les liens vers les canaux que vous souhaitez afficher à vos clients dans le Chat Live et collez-les ci-dessous.<br><br>
Notez que seule une partie d'un lien doit être copiée. Vous trouverez des exemples de ce que vous devez copier et coller.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TWILIO_EXAMPLE"] = "Vous pouvez spécifier des liens publics vers les canaux que vous avez connectés à Microsoft pour les afficher dans le widget (chat live) sur le site dans le #LINK_BEGIN#formulaire des paramètres du canal#LINK_END#.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_WEBCHAT_EXAMPLE"] = "Obtenez le lien:<br>
&lt;iframe src=\"<strong>https://webchat.botframework.com/embed/bitrix?s=</strong>YOUR_SECRET_HERE\" style=\"height:
502px; max-height:
502px;\"&gt;&lt;/iframe&gt;<br>Remplacez YOUR_SECRET_HERE par le contenu du champ \"Secret\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_EMAILOFFICE365_PLACEHOLDER"] = "pokoev@bitrix.onmicrosoft.com";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_EMAILOFFICE365_EXAMPLE"] = "&lt;a href=\"mailto:<strong>pokoev@bitrix.onmicrosoft.com</strong>\"&gt;pokoev@bitrix.onmicrosoft.com&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_DESCRIPTION_MASTER"] = "Vous pouvez spécifier des liens publics vers les canaux que vous avez connectés à Microsoft pour les afficher dans le widget (chat live) sur le site dans le #LINK_BEGIN#formulaire des paramètres du canal#LINK_END#.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TELEGRAM_PLACEHOLDER"] = "https://telegram.me/pokoevBot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TELEGRAM_EXAMPLE"] = "&lt;a href='<strong>https://telegram.me/pokoevBot</strong>'&gt;@pokoevBot&lt;/a&gt;";
?>