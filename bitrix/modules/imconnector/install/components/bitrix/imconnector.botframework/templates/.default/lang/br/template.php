<?
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_BOT_HANDLE_NAME"] = "ID do Bot (Identificador do Bot):";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_APP_ID_NAME"] = "ID do App (ID do App Microsoft):";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_APP_SECRET_NAME"] = "Senha secreta do App:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_TESTED"] = "Conexão de teste";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_INDEX_DESCRIPTION"] = "Conecte o Microsoft Bot Framework ao Canal Aberto para receber mensagens de seus clientes pelo Skype e outras fontes compatíveis (Slack, Kik, GroupMe, SMS, e-mail etc) diretamente no seu Bitrix24.<br><br>
Vamos ajudar você a criar um bot e conectá-lo ao seu Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CREATE_NEW_BOT"] = "Crie um novo bot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_FORM_SETTINGS"] = "Eu já tenho um bot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECTOR_ERROR_STATUS"] = "O seu bot encontrou um erro. Verifique as preferências e testar o bot novamente.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_GET_LINKS"] = "Obtenha links no site da Microsoft";
$MESS["IMCONNECTOR_COMPONENT_FINAL_FORM_DESCRIPTION_OK_1"] = "O Microsoft Bot Framework foi conectado ao seu Canal Aberto.
A partir de agora, todas as mensagens enviadas ao bot serão encaminhadas para o seu Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FINAL_FORM_DESCRIPTION_OK_2"] = "Você pode conectar outras fontes de comunicação ao bot ou configurar as existentes para encaminhar todas as mensagens ao bate-papo.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_10_STEPS_TITLE"] = "Siga estas etapas para conectar o Microsoft Bot Framework ao Canal Aberto";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_1_OF_10_TITLE"] = "Faça logon no site Microsoft";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_2_OF_10_TITLE"] = "Registre o novo bot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_3_OF_10_TITLE"] = "Criação de app";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_4_OF_10_TITLE"] = "Obter senha";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_5_OF_10_TITLE"] = "Informações adicionais";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_6_OF_10_TITLE"] = "Acordo";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_7_OF_10_TITLE"] = "Publicar o bot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_8_OF_10_TITLE"] = "Opções";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_9_OF_10_TITLE"] = "O bot está sendo revisado";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_10_OF_10_TITLE"] = "Pronto!";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_1_OF_10"] = "etapa 1 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_2_OF_10"] = "etapa 2 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_3_OF_10"] = "etapa 3 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_4_OF_10"] = "etapa 4 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_5_OF_10"] = "etapa 5 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_6_OF_10"] = "etapa 6 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_7_OF_10"] = "etapa 7 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_8_OF_10"] = "etapa 8 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_9_OF_10"] = "etapa 9 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_10_OF_10"] = "etapa 10 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_1_OF_10_DESCRIPTION_1"] = "Acesse o site da Microsoft e faça login usando sua conta:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_1_OF_10_DESCRIPTION_2"] = "Registre o bot selecionando \"Registrar um bot\" no menu superior";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_2_OF_10_DESCRIPTION_1"] = "Forneça as informações necessárias sobre o seu novo bot.<br><br>
							- Carregue uma imagem de logotipo. São aceitos somente os arquivos PNG não maiores do que 30 kB<br>
							- Digite o nome do seu bot<br>
							- Forneça um ID exclusivo do bot<br>
							- Digite a descrição do bot<br><br>
							Estes dados estarão disponíveis depois que o seu bot for publicado no site da Microsoft.<br>
							Todos os campos são obrigatórios.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_2_OF_10_DESCRIPTION_2"] = "Especifique o nome do bot (identificador do bot) que você forneceu:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_2_OF_10_DESCRIPTION_3"] = "Copie o endereço especificado abaixo e cole-o no campo \"Mensagens de Pontos de extremidade\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_3_OF_10_DESCRIPTION_1"] = "Aperte o botão \"Criar ID e senha do App Microsoft\"";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_3_OF_10_DESCRIPTION_2"] = "Uma nova janela será exibida. Copie o ID do App e digite-o no campo abaixo";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_4_OF_10_DESCRIPTION_1"] = "Aperte o botão \"Criar senha para continuar\"";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_4_OF_10_DESCRIPTION_2"] = "Copie a senha na nova janela que aparece";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_4_OF_10_DESCRIPTION_3"] = "Importante! A senha será mostrada para você apenas uma vez. Recomendamos que você a salve num lugar seguro. <br><br> Digite a senha no campo abaixo.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_4_OF_10_DESCRIPTION_4"] = "Aperte o botão \"Concluir a operação e voltar ao Bot Framework\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_5_OF_10_DESCRIPTION_1"] = "Forneça informações que estarão disponíveis no site da Microsoft na descrição do seu bot. São necessários os seguintes campos.<br><br>
							Nome do editor - seu nome<br>
							E-mail do editor - seu e-mail<br>
							Declaração de privacidade - URL da sua declaração de privacidade<br>
							Termos de Uso - URL da sua página de termos de uso";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_6_OF_10_DESCRIPTION_1"] = "Leia os termos de uso, confirme estar de acordo, selecionando a caixa de seleção, e clique em \"Registrar\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_7_OF_10_DESCRIPTION_1"] = "O bot foi criado. Agora, você tem que publicá-lo.
Revise as informações fornecidas e, se tudo estiver correto, clique em \"Publicar\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_8_OF_10_DESCRIPTION_1"] = "Agora, o seu bot precisa ser verificado. Marque a opção de publicação do seu bot na lista do bot, para torná-lo visível aos outros, e clique em \"Enviar para avaliação\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_9_OF_10_DESCRIPTION_1"] = "Certifique-se de que o seu bot foi enviado para verificação: o status deve ser \"Em avaliação\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_9_OF_10_DESCRIPTION_2"] = "Observe que apenas nesta página você pode obter links para apps conectados ao seu bot.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_9_OF_10_DESCRIPTION_3"] = "Skype e Bate-papo ao vivo já estão conectados. Eles não precisam de configuração.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SIMPLE_FORM_DESCRIPTION_1"] = "Se o seu bot já tiver sido registado, conecte-o ao Bitrix24.<br><br>
Especifique esse endereço no campo \"Mensagens Webhook\" no site da Microsoft:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SIMPLE_FORM_DESCRIPTION_TESTED"] = "Você pode verificar seus dados para correção.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_COPY"] = "Copiar";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TWILIO_PLACEHOLDER"] = "15615155866";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_DESCRIPTION"] = "Você pode especificar links públicos para os canais que você conectou com a Microsoft para mostrá-los no widget (bate-papo ao vivo) no site.
<br><br>
Para obter os links, acesse #LINK_BEGIN#sua conta Microsoft Bot Framework#LINK_END# e clique em \"Obter códigos de inserção do bot\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_TITLE"] = "especifique links para canais de comunicação";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_CHANNELS_DESCRIPTION"] = "Na sua #LINK_BEGIN#conta Microsoft Bot Framework#LINK_END#, copie os links para os canais que você deseja mostrar a seus clientes no Bate-papo ao vivo e cole-os abaixo.
<br><br>
Observe que apenas uma parte do link precisa ser copiada. Você vai encontrar exemplos do que você tem de copiar e colar.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_DESCRIPTION_MASTER"] = "Você pode especificar links públicos para os canais que você conectou com a Microsoft para mostrá-los no widget (bate-papo ao vivo) no site no #LINK_BEGIN#formulário de configurações do canal#LINK_END#.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TWILIO_EXAMPLE"] = "&lt;a href=\"tel:<strong>+15615155866</strong>\"&gt;(561) 515-5866&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_WEBCHAT_EXAMPLE"] = "Obtenha o link:<br>
									&lt;iframe src=\"<strong>https://webchat.botframework.com/embed/bitrix?s=</strong>YOUR_SECRET_HERE\" style=\"height: 502px; max-height: 502px;\"&gt;&lt;/iframe&gt;<br>Substitua SEU_SEGREDO_AQUI pelo conteúdo do campo \"Segredo\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_EMAILOFFICE365_PLACEHOLDER"] = "pokoev@bitrix.onmicrosoft.com";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_EMAILOFFICE365_EXAMPLE"] = "&lt;a href=\"mailto:<strong>pokoev@bitrix.onmicrosoft.com</strong>\"&gt;pokoev@bitrix.onmicrosoft.com&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TELEGRAM_PLACEHOLDER"] = "https://telegram.me/pokoevBot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TELEGRAM_EXAMPLE"] = "&lt;a href='<strong>https://telegram.me/pokoevBot</strong>'&gt;@pokoevBot&lt;/a&gt;";
?>