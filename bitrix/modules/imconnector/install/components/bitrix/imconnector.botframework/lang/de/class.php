<?
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_MODULE_NOT_INSTALLED"] = "Das Modul \"Externe IM-Connector\" ist nicht installiert.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_NO_ACTIVE_CONNECTOR"] = "Dieser Connector ist nicht aktiv.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SESSION_HAS_EXPIRED"] = "Ihre Sitzung ist abgelaufen. Senden Sie bitte das Formular erneut.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_OK_SAVE"] = "Information wurde erfolgreich gespeichert.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_NO_DATA_SAVE"] = "Es gibt keine Daten zum Speichern";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_NO_SAVE"] = "Fehler beim Speichern von Daten. Überprüfen Sie bitte Ihre eingegebenen Daten und versuchen Sie erneut.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_OK_CONNECT"] = "Testverbindung wurde erfolgreich hergestellt";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_NO_CONNECT"] = "Die Testverbindung konnte nicht mit angegebenen Parametern hergestellt werden";
?>