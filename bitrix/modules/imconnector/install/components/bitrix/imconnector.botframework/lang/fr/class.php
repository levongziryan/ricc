<?
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_MODULE_NOT_INSTALLED"] = "Le module \"Connecteurs de messagerie instantanée externe\" n'est pas installé.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_NO_ACTIVE_CONNECTOR"] = "Ce connecteur est inactif.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SESSION_HAS_EXPIRED"] = "Votre session a expiré. Veuillez renvoyer le formulaire.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_OK_SAVE"] = "Les informations ont été enregistrées avec succès.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_NO_DATA_SAVE"] = "Il n'y a aucune donnée à enregistrer";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_NO_SAVE"] = "Erreur d'enregistrement des données. Veuillez vérifier votre entrée et réessayer.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_OK_CONNECT"] = "La connexion test a été établie avec succès";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_NO_CONNECT"] = "Impossible d'établir la connexion test avec les paramètres fournis";
?>