<?
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_MODULE_NOT_INSTALLED"] = "Модуль коннекторів месенджерів не встановлено";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_NO_ACTIVE_CONNECTOR"] = "Даний коннектор не активний";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SESSION_HAS_EXPIRED"] = "Ваша сесія минула. Надішліть форму ще раз";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_OK_SAVE"] = "Дані успішно збережено";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_NO_DATA_SAVE"] = "Немає даних для збереження";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_NO_SAVE"] = "Виникла помилка при збереженні. Перевірте коректність введених даних і спробуйте ще раз";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_OK_CONNECT"] = "Тестове з'єднання успішне";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_NO_CONNECT"] = "Тестове з'єднання із зазначеними даними не вдалося зробити";
?>