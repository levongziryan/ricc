<?
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_MODULE_NOT_INSTALLED"] = "El módulo \"External IM Connectors\" no está instalado.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_NO_ACTIVE_CONNECTOR"] = "Esta conexión está inactiva.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SESSION_HAS_EXPIRED"] = "Su sesión ha caducado. Por favor envíe el formulario de nuevo.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_OK_SAVE"] = "La información se ha guardado correctamente.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_NO_DATA_SAVE"] = "No hay datos para guardar";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_NO_SAVE"] = "Error al guardar los datos. Compruebe la entrada e inténtelo de nuevo.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_OK_CONNECT"] = "Prueba de conexión se ha establecido correctamente";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_NO_CONNECT"] = "No se pudo establecer la prueba de conexión utilizando los parámetros proporcionados";
?>