<?
$MESS["IMCONNECTOR_COMPONENT_BASECONNECTOR_MODULE_NOT_INSTALLED"] = "Das Modul \"External IM Connectors\" ist nicht installiert.";
$MESS["IMCONNECTOR_COMPONENT_BASECONNECTOR_NO_ACTIVE_CONNECTOR"] = "Dieser Connector ist nicht aktiv.";
$MESS["IMCONNECTOR_COMPONENT_BASECONNECTOR_SESSION_HAS_EXPIRED"] = "Ihre Sitzung ist abgelaufen. Senden Sie bitte das Formular erneut.";
$MESS["IMCONNECTOR_COMPONENT_BASECONNECTOR_CONNECTOR_ERROR_STATUS"] = "Es ist ein Fehler aufgetreten. Überprüfen Sie bitte Ihre Einstellungen.";
?>