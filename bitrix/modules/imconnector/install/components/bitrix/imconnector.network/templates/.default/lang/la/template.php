<?
$MESS["IMCONNECTOR_COMPONENT_NETWORK_CONNECTOR_ERROR_STATUS"] = "Bot error de procesamiento. Por favor, compruebe todos los ajustes y pruebe su bot de nuevo.";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_NAME"] = "Nombre";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_CODE"] = "Código";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_SIMPLE_FORM_DESCRIPTION_1"] = "El canal abierto ya está conectado a Bitrix24.Network.
<br> Puede cambiar la configuración, si es necesario.";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_INDEX_DESCRIPTION"] = "Conectar Bitrix24.Network para Canales Abiertos con el fin de enviar y recibir mensajes de otros usuarios de Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_CONNECT"] = "Conectar";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_COPY"] = "Copiar";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_1"] = "Nombre";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_2"] = "Breve descripción";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_3"] = "Mensaje de saludo";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_4"] = "Avatar";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_4_DESCRIPTION_1"] = "único avatar que permitirá que sus clientes se encuentran más rápido.";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_4_DESCRIPTION_2"] = "Cargar avatar";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_4_DESCRIPTION_3"] = "Eliminar imagen";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_5"] = "Búsqueda";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_6"] = "Código de búsqueda";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_6_TIP"] = "Este código permite a los usuarios encontrar su canal abierto en el messanger, incluso si no está disponible para la búsqueda.";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_REST_HELP"] = "No se puede crear el canal de soporte privado de búsqueda.<br> Por favor proporcione el código a sus clientes para que puedan ponerse en contacto con usted o conectarse al canal a través de REST API.<br> Canales abiertos REST API detalles están disponibles en #LINK_START#documentos#LINK_END#";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_REST_LINK"] = "https://bitrix24.com/~rest-join-network-en";
?>