<?
$MESS["IMCONNECTOR_COMPONENT_NETWORK_CONNECTOR_ERROR_STATUS"] = "Erro de processamento de bot. Verifique todas as configurações e teste seu bot novamente.";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_NAME"] = "Nome";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_CODE"] = "Código";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_SIMPLE_FORM_DESCRIPTION_1"] = "O canal aberto já está conectado à Bitrix24.Network.
<br>Você pode alterar as configurações, se necessário.";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_INDEX_DESCRIPTION"] = "Conecte Bitrix24.Network a Canais abertos para enviar e receber mensagens de outros usuários Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_CONNECT"] = "Conectar";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_COPY"] = "Copiar";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_1"] = "Nome";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_2"] = "Breve descrição";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_3"] = "Mensagem de saudação";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_4"] = "Avatar";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_4_DESCRIPTION_1"] = "O avatar exclusivo vai permitir que seus clientes encontrem você mais rápido.";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_4_DESCRIPTION_2"] = "Carregar avatar";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_4_DESCRIPTION_3"] = "Excluir imagem";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_5"] = "Pesquisável";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_6"] = "Código da pesquisa";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_6_TIP"] = "Este código permite aos usuários encontrar seu canal aberto no messenger, mesmo se não estiver disponível para pesquisa.";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_REST_HELP"] = "Você pode criar canal de suporte privado não pesquisável.<br> Forneça o código para seus clientes para que eles possam entrar em contato com você ou conecte o canal através do API REST.<br> Informações sobre API REST de canais abertos estão disponíveis em ";
?>