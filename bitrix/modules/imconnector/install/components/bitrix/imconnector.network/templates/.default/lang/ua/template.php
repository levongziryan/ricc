<?
$MESS["IMCONNECTOR_COMPONENT_NETWORK_REST_LINK"] = "https://bitrix24.ua/~rest-join-network";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_CONNECTOR_ERROR_STATUS"] = "У процесі роботи вашого бота виникла помилка. Вам необхідно перевірити зазначені дані і протестувати бота повторно.";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_NAME"] = "Ім'я";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_CODE"] = "Код";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_SIMPLE_FORM_DESCRIPTION_1"] = "Відкрита лінія вашої компанії вже підключена до Бітрікс24.Network.
<br>Якщо необхідно, ви можете змінити налаштування.";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_INDEX_DESCRIPTION"] = "Підключіть Бітрікс24.Network до Відкритої лінії щоб приймати звернення ваших клієнтів, партнерів та колег з інших компаній, які працюють з Бітрікс24.";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_CONNECT"] = "Підключити";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_COPY"] = "Копіювати";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_1"] = "Назва";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_2"] = "Короткий опис";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_3"] = "Вітальне повідомлення";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_4"] = "Аватар";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_4_DESCRIPTION_1"] = "Унікальний аватар, допоможе вашим клієнтам швидше знайти вас.";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_4_DESCRIPTION_2"] = "Завантажити аватар";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_4_DESCRIPTION_3"] = "Видалити картинку";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_5"] = "Доступний в пошуку";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_6"] = "Код для пошуку";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_6_TIP"] = "Вказавши даний код в пошуку веб-месенджера, користувач знайде вашу лінію, навіть якщо вона не доступна в пошуку.";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_REST_HELP"] = "Ви можете організувати закриту лінію підтримки, прибравши відображення її в пошуку.<br>Повідомте вашим клієнтам код, що б вони змогли написати вам або підключіть її за допомогою вашої програми використовуючи REST API.<br>Детальніше про підключення через REST API ви можете прочитати #LINK_START#документації#LINK_END#.";
?>