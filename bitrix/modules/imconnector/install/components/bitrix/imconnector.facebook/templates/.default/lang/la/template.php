<?
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_DEL_REFERENCE"] = "Desvincular";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_STEP_1_OF_3_TITLE"] = "Iniciar sesión";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_STEP_2_OF_3_TITLE"] = "Seleccionar página";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_STEP_3_OF_3_TITLE"] = "Terminar";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_STEP_N_OF_3"] = "paso #STEP# de 3";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_PAGE"] = "Página";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_DESCRIPTION"] = "Conecte la página de Facebook de su compañía a un Canal Abierto de Bitrix24 y empiece a comunicarse con sus clientes a través del chat Bitrix24. Es necesario crear una página de Facebook de su compañía o utilizar una existente. Usted necesita ser un administrador para esta página.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECT_PAGE"] = "Página de conexión";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_PAGE"] = "Por favor, autorice con la cuenta de Facebook que gestiona sus páginas para recibir mensajes de su clientes desde Bitrix24

";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_EXPIRED_ACCOUNT_TOKEN"] = "Es necesario estar autorizado a su cuenta de Facebook para modificar los ajustes";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_EXPIRED_ACCOUNT_TOKEN_WARNING"] = "Por favor tenga en cuenta que cuando se autoriza con una cuenta de Facebook que no administra la página actual se desconectará del Canal Abierto de Bitrix24 .";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_THERE_IS_NO_PAGE_WHERE_THE_ADMINISTRATOR"] = "Usted no es administrador de ninguna página de Facebook.<br>Usted puede crear su página de Facebook en este momento.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TO_CREATE_A_PAGE"] = "Crear";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECTED_PAGE"] = "Página conectada";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CHANGE_PAGE"] = "Cambiar";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_MY_OTHER_PAGES"] = "Mis otras páginas de Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_SELECT_THE_PAGE"] = "Por favor seleccione la página de Facebook que debe ser conectada al Canal Abierto de Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OTHER_PAGES"] = "Otras páginas";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_PAGE_IM"] = "Messenger";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECTOR_ERROR_STATUS"] = "Ocurrió un error. Por favor revisa su configuración.";
?>