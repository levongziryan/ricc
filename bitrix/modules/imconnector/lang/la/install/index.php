<?
$MESS["IMCONNECTOR_MODULE_NAME"] = "Conectores Externos de Messenger";
$MESS["IMCONNECTOR_MODULE_DESC"] = "Conectar messengers externos a través de este módulo.";
$MESS["IMCONNECTOR_INSTALL_TITLE"] = "Instalación de módulo";
$MESS["IMCONNECTOR_UNINSTALL_TITLE"] = "Desinstalación de módulo";
$MESS["IMCONNECTOR_DB_NOT_SUPPORTED"] = "Este módulo sólo soporta MySQL.";
?>