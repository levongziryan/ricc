<?
$MESS["IMCONNECTOR_PUBLIC_PATH_DESC"] = "Conectores requieren dirección de un sitio público para funcionar correctamente.";
$MESS["IMCONNECTOR_PUBLIC_PATH_DESC_2"] = "Si el acceso externo a la red está restringido, sólo se permitirá el acceso a ciertas páginas. Por favor refiérase a #LINK_START#documentación#LINK_END# para más detalle.";
$MESS["IMCONNECTOR_PUBLIC_PATH"] = "Dirección pública de su sitio web:";
?>