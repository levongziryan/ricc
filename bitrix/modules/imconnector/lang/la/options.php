<?
$MESS["IMCONNECTOR_TAB_SETTINGS"] = "Conectores de Messenger";
$MESS["IMCONNECTOR_FIELD_DEBUG_TITLE"] = "Activar el modo de depuración";
$MESS["IMCONNECTOR_FIELD_URI_CLIENT_TITLE"] = "Dirección pública de su sitio web";
$MESS["IMCONNECTOR_FIELD_LIST_CONNECTOR_TITLE"] = "Conectores en uso";
$MESS["IMCONNECTOR_FIELD_URI_SERVER_TITLE"] = "Dirección del servidor (ejemplo: gate.office.bitrix.com)";
?>