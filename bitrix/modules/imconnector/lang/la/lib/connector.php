<?
$MESS["IMCONNECTOR_NAME_CONNECTOR_LIVECHAT"] = "Chat en vivo";
$MESS["IMCONNECTOR_NAME_CONNECTOR_NETWORK"] = "Bitrix24.Network";
$MESS["IMCONNECTOR_NAME_CONNECTOR_TELEGRAM_BOT"] = "Telegrama";
$MESS["IMCONNECTOR_NAME_CONNECTOR_VK_GROUP"] = "VK";
$MESS["IMCONNECTOR_NAME_CONNECTOR_FACEBOOK_PAGE"] = "Facebook";
$MESS["IMCONNECTOR_NAME_CONNECTOR_FACEBOOK_COMMENTS_PAGE"] = "Facebook: Comentarios";
$MESS["IMCONNECTOR_NAME_CONNECTOR_INSTAGRAM"] = "Instagram";
$MESS["IMCONNECTOR_ERROR_MAIN_MODULE_URL_SITE_USE"] = "Los nombres de dominio especificado en la configuración del módulo del kernel y el dominio actual son diferentes. El nombre del dominio #DOMAIN# especificado en la configuración del módulo del kernel se utiliza para la respuesta del servidor. Para cambiar el nombre de dominio, <a href=\"/bitrix/admin/settings.php?lang=ru&mid=main&mid_menu=1\">modificar el campo \"URL del Sitio\".</a>";
$MESS["IMCONNECTOR_ERROR_SERVER_NAME_USE"] = "El campo de módulos del kernel \"URL del sitio\" está vacía. El nombre de dominio actual #DOMAIN# se utilizará para la respuesta del servidor. Para cambiar el nombre de dominio, <a href=\"/bitrix/admin/settings.php?lang=ru&mid=main&mid_menu=1\">modificar el campo \"URL del Sitio\".</a>";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK"] = "Skype, Slack, Twilio (SMS), Kik, GroupMe, Office365 e-mail, Web-chat, Telegrama, Facebook - Microsoft Bot Framework";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_SKYPE"] = "Skype";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_SLACK"] = "Slack";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_KIK"] = "Kik";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_GROUPME"] = "GroupMe";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_TWILIO"] = "Twilio (SMS)";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_WEBCHAT"] = "Chat web";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_EMAILOFFICE365"] = "Office365 email";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_TELEGRAM"] = "Telegrama";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_FACEBOOKMESSENGER"] = "Facebook Messenger";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_DIRECTLINE"] = "Línea directa";
$MESS["IMCONNECTOR_NAME_CONNECTOR_VIBER_BOT"] = "Viber";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_MSTEAMS"] = "Equipos de Microsoft (MS: Bot Framework)";
?>