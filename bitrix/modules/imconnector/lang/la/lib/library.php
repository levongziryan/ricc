<?
$MESS["IMCONNECTOR_PROXY_NO_USER_IM"] = "Error al obtener el ID de usuario de mensajería instantánea.";
$MESS["IMCONNECTOR_PROXY_NO_ADD_USER"] = "No puede crear ni obtener el usuario asignado al usuario de mensajería externa.";
$MESS["IMCONNECTOR_NOT_SPECIFIED_CORRECT_CONNECTOR"] = "El conector no está especificado.";
$MESS["IMCONNECTOR_NOT_SPECIFIED_CORRECT_COMMAND"] = "Comando proporcionado incorrecto";
$MESS["IMCONNECTOR_NOT_ALL_THE_REQUIRED_DATA"] = "Datos incompletos";
$MESS["IMCONNECTOR_NOT_AVAILABLE_CONNECTOR"] = "Intenta conectarse a un conector inactivo o no disponible";
$MESS["IMCONNECTOR_FEATURE_IS_NOT_SUPPORTED"] = "Esta función no es compatible";
$MESS["IMCONNECTOR_ADD_EXISTING_CONNECTOR"] = "Intenta agregar un conector que ya existen";
$MESS["IMCONNECTOR_UPDATE_NOT_EXISTING_CONNECTOR"] = "Intenta actualizar un conector inactivo";
$MESS["IMCONNECTOR_DELETE_NOT_EXISTING_CONNECTOR"] = "Intenta eliminar un conector inactivo";
$MESS["IMCONNECTOR_FAILED_TO_ADD_CONNECTOR"] = "No se puede agregar un canal abierto para el conector";
$MESS["IMCONNECTOR_FAILED_TO_UPDATE_CONNECTOR"] = "No se puede actualizar el conector de canal abierto";
$MESS["IMCONNECTOR_FAILED_TO_DELETE_CONNECTOR"] = "No se puede eliminar el conector de Canal Abierto";
$MESS["IMCONNECTOR_FAILED_TO_LOAD_MODULE_OPEN_LINES"] = "No se puede cargar el módulo de Canal Abierto";
$MESS["IMCONNECTOR_FAILED_TO_SAVE_SETTINGS_CONNECTOR"] = "No se pueden guardar las preferencias del conector";
$MESS["IMCONNECTOR_FAILED_TO_TEST_CONNECTOR"] = "No se puede probar la conectividad del conector";
$MESS["IMCONNECTOR_FAILED_REGISTER_CONNECTOR"] = "No se puede registrar el conector";
$MESS["IMCONNECTOR_EMPTY_PARAMETRS"] = "Parametros vacios proporcionados";
?>