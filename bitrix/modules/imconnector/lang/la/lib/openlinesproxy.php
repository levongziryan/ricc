<?
$MESS["CONNECTOR_PROXY_NO_ADD_USER"] = "No puede crear ni obtener el usuario asignado al usuario de mensajería externa.";
$MESS["CONNECTOR_PROXY_TITLE_NEW_BOT_CHAT"] = "Nuevo chat con #USER_NAME# través #CONNECTOR#";
$MESS["CONNECTOR_PROXY_NO_ADD_CHAT"] = "No se puede crear chat sobre el mensaje entrante";
$MESS["CONNECTOR_PROXY_NO_ADD_MESSAGE"] = "No se puede agregar mensajes";
$MESS["CONNECTOR_PROXY_NO_USER_IM"] = "No se pudo obtener el ID del usuario IM.";
?>