<?
$MESS["IMCONNECTOR_NO_ACTIVE_CONNECTOR"] = "El conector está inactivo";
$MESS["IMCONNECTOR_GENERAL_REQUEST_NOT_DYNAMIC_METHOD"] = "Intentar llamar con un método estático de forma dinámica";
$MESS["IMCONNECTOR_INCORRECT_INCOMING_DATA"] = "Los datos de entrada no son válidos";
?>