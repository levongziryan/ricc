<?
$MESS["IMCONNECTOR_MAPS_NAME"] = "Mapa";
$MESS["IMCONNECTOR_FORWARDED_MESSAGE"] = "Mensaje reenviado:";
$MESS["IMCONNECTOR_CONTACT_NAME"] = "Nombre completo:";
$MESS["IMCONNECTOR_CONTACT_PHONE"] = "Teléfono:";
$MESS["IMCONNECTOR_WARNING_LARGE_FILE"] = "[b]Atención![/b]
[i]Un usuario ha hecho un intento de enviar un archivo grande que no se puede entregar.
Por favor, les pedimos que envíe el archivo utilizando cualquier otro método (por ejemplo: usar un servicio de intercambio de archivos).[/i]";
$MESS["IMCONNECTOR_COMMENT_IN_FACEBOOK"] = "Comentario en Facebook";
$MESS["IMCONNECTOR_LINK_TO_ORIGINAL_POST_IN_FACEBOOK"] = "Enlace a la entrada original de Facebook: #LINK#";
$MESS["IMCONNECTOR_LINK_TO_ORIGINAL_POST_IN_INSTAGRAM"] = "Enlace a la publicación original de Instagram: #LINK#";
$MESS["IMCONNECTOR_LINK_TO_ORIGINAL_POST"] = "Enlace a la publicación original: #LINK#";
$MESS["IMCONNECTOR_GUEST_USER"] = "Invitado";
$MESS["IMCONNECTOR_WALL_TEXT"] = "Publicado en el muro por";
$MESS["IMCONNECTOR_WALL_DATE_TEXT"] = "Publicado en";
?>