<?
$MESS["IMCONNECTOR_COMPONENT_SETTINGS_CONFIRM_DISABLE_TITLE"] = "Confirmar desconexão";
$MESS["IMCONNECTOR_COMPONENT_SETTINGS_CONFIRM_DISABLE"] = "Este canal de comunicação será desconectado, todas suas preferências serão excluídas. Continuar?";
$MESS["IMCONNECTOR_COMPONENT_SETTINGS_CONFIRM_DISABLE_BUTTON_OK"] = "OK";
$MESS["IMCONNECTOR_COMPONENT_SETTINGS_CONFIRM_DISABLE_BUTTON_CANCEL"] = "Cancelar";
$MESS["IMCONNECTOR_COMPONENT_SETTINGS_COPIED_TO_CLIPBOARD"] = "Copiado para a área de transferência";
$MESS["IMCONNECTOR_COMPONENT_SETTINGS_FAILED_TO_COPY"] = "Não é possível copiar para a área de transferência";
?>