<?
$MESS["IMCONNECTOR_PROXY_NO_USER_IM"] = "Falha ao obter o ID do usuário de IM.";
$MESS["IMCONNECTOR_PROXY_NO_ADD_USER"] = "Não é possível criar ou obter usuário mapeado para usuário messenger externo.";
$MESS["IMCONNECTOR_NOT_SPECIFIED_CORRECT_CONNECTOR"] = "O conector não está especificado.";
$MESS["IMCONNECTOR_NOT_SPECIFIED_CORRECT_COMMAND"] = "Fornecido comando incorreto";
$MESS["IMCONNECTOR_NOT_ALL_THE_REQUIRED_DATA"] = "Dados incompletos";
$MESS["IMCONNECTOR_EMPTY_PARAMETRS"] = "Fornecidos parâmetros vazios";
$MESS["IMCONNECTOR_NOT_AVAILABLE_CONNECTOR"] = "Tentativa de conectar um conector inativo ou indisponível";
$MESS["IMCONNECTOR_FEATURE_IS_NOT_SUPPORTED"] = "Não é compatível com este recurso";
$MESS["IMCONNECTOR_ADD_EXISTING_CONNECTOR"] = "Tentativa de adicionar um conector que já existe";
$MESS["IMCONNECTOR_UPDATE_NOT_EXISTING_CONNECTOR"] = "Tentativa de atualizar um conector inativo";
$MESS["IMCONNECTOR_DELETE_NOT_EXISTING_CONNECTOR"] = "Tentativa de excluir um conector inativo";
$MESS["IMCONNECTOR_FAILED_TO_ADD_CONNECTOR"] = "Não é possível adicionar conector de Canal Aberto";
$MESS["IMCONNECTOR_FAILED_TO_UPDATE_CONNECTOR"] = "Não é possível atualizar conector de Canal Aberto";
$MESS["IMCONNECTOR_FAILED_TO_DELETE_CONNECTOR"] = "Não é possível excluir conector de Canal Aberto";
$MESS["IMCONNECTOR_FAILED_TO_LOAD_MODULE_OPEN_LINES"] = "Não é possível carregar o módulo de Canal Aberto";
$MESS["IMCONNECTOR_FAILED_TO_SAVE_SETTINGS_CONNECTOR"] = "Não é possível salvar preferências de conector";
$MESS["IMCONNECTOR_FAILED_TO_TEST_CONNECTOR"] = "Não é possível testar a conectividade do conector";
$MESS["IMCONNECTOR_FAILED_REGISTER_CONNECTOR"] = "Não é possível registrar o conector";
?>