<?
$MESS["IMCONNECTOR_NAME_CONNECTOR_LIVECHAT"] = "Bate-papo ao vivo";
$MESS["IMCONNECTOR_NAME_CONNECTOR_NETWORK"] = "Bitrix24.Network";
$MESS["IMCONNECTOR_NAME_CONNECTOR_TELEGRAM_BOT"] = "Telegram";
$MESS["IMCONNECTOR_NAME_CONNECTOR_VK_GROUP"] = "VK";
$MESS["IMCONNECTOR_NAME_CONNECTOR_FACEBOOK_PAGE"] = "Facebook";
$MESS["IMCONNECTOR_NAME_CONNECTOR_FACEBOOK_COMMENTS_PAGE"] = "Facebook: Comentários";
$MESS["IMCONNECTOR_NAME_CONNECTOR_INSTAGRAM"] = "Instagram";
$MESS["IMCONNECTOR_ERROR_MAIN_MODULE_URL_SITE_USE"] = "Os nomes de domínio especificados nas configurações do módulo Kernel e o domínio atual são diferentes. O nome de domínio #DOMAIN# especificado nas configurações do módulo Kernel é utilizado para resposta do servidor. Para alterar o nome de domínio, <a href=\"/bitrix/admin/settings.php?lang=ru&mid=main&mid_menu=1\">modifique o campo \"URL do site\".</a>";
$MESS["IMCONNECTOR_ERROR_SERVER_NAME_USE"] = "O campo \"URL do site\" do módulo Kernel está vazio. O nome de domínio atual #DOMAIN# será utilizado para resposta do servidor. Para alterar o nome de domínio, <a href=\"/bitrix/admin/settings.php?lang=ru&mid=main&mid_menu=1\">modifique o campo \"URL do site\".</a>";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK"] = "Skype, Slack, Twilio (SMS), Kik, GroupMe, e-mail Office365, Web-chat, Telegram, Facebook - Microsoft Bot Framework";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_SKYPE"] = "Skype (MS: Bot Framework)";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_SLACK"] = "Slack (MS: Bot Framework)";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_KIK"] = "Kik (MS: Bot Framework)";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_GROUPME"] = "GroupMe (MS: Bot Framework)";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_TWILIO"] = "Twilio: SMS (MS: Bot Framework)";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_MSTEAMS"] = "Equipes da Microsoft (MS: Estrutura do Bot)";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_WEBCHAT"] = "Web Chat (MS: Bot Framework)";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_EMAILOFFICE365"] = "E-mail Office365 Bot Framework)";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_TELEGRAM"] = "Telegram (MS: Bot Framework)";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_FACEBOOKMESSENGER"] = "Facebook Messenger (MS: Bot Framework)";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_DIRECTLINE"] = "Direct Line (MS: Bot Framework)";
$MESS["IMCONNECTOR_NAME_CONNECTOR_VIBER_BOT"] = "Viber";
?>