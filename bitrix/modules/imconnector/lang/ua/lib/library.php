<?
$MESS["IMCONNECTOR_PROXY_NO_USER_IM"] = "Не отримано id користувача месенджера";
$MESS["IMCONNECTOR_PROXY_NO_ADD_USER"] = "Не вдалося створити або отримати користувача системи, пов'язаного з користувачем віддаленого месенджера";
$MESS["IMCONNECTOR_NOT_SPECIFIED_CORRECT_CONNECTOR"] = "Не вказано коннектор";
$MESS["IMCONNECTOR_NOT_SPECIFIED_CORRECT_COMMAND"] = "Не вказана коректна команда";
$MESS["IMCONNECTOR_NOT_ALL_THE_REQUIRED_DATA"] = "Передані не всі необхідні дані";
$MESS["IMCONNECTOR_EMPTY_PARAMETRS"] = "Передані пусті параметри";
$MESS["IMCONNECTOR_NOT_AVAILABLE_CONNECTOR"] = "Спроба звернення до неіснуючого або не активного коннектору";
$MESS["IMCONNECTOR_FEATURE_IS_NOT_SUPPORTED"] = "Дана можливість не підтримується";
$MESS["IMCONNECTOR_ADD_EXISTING_CONNECTOR"] = "Спроба додати вже існуючого коннектора";
$MESS["IMCONNECTOR_UPDATE_NOT_EXISTING_CONNECTOR"] = "Спроба оновлення, не активованого коннектора";
$MESS["IMCONNECTOR_DELETE_NOT_EXISTING_CONNECTOR"] = "Спроба видалення, не активованого коннектора";
$MESS["IMCONNECTOR_FAILED_TO_ADD_CONNECTOR"] = "Не вдалося додати коннектор відкритої лінії";
$MESS["IMCONNECTOR_FAILED_TO_UPDATE_CONNECTOR"] = "Не вдалося оновити коннектор відкритої лінії";
$MESS["IMCONNECTOR_FAILED_TO_DELETE_CONNECTOR"] = "Не вдалося видалити коннектор відкритої лінії";
$MESS["IMCONNECTOR_FAILED_TO_LOAD_MODULE_OPEN_LINES"] = "Не вдалося завантажити модуль відкритих ліній";
$MESS["IMCONNECTOR_FAILED_TO_SAVE_SETTINGS_CONNECTOR"] = "Не вдалося зберегти налаштування коннектора";
$MESS["IMCONNECTOR_FAILED_TO_TEST_CONNECTOR"] = "Не вдалося протестувати з'єднання коннектора";
$MESS["IMCONNECTOR_FAILED_REGISTER_CONNECTOR"] = "Не вдалося зареєструвати коннектор";
?>