<?
$MESS["IMCONNECTOR_MODULE_NAME"] = "Конектори для зовнішніх месенджерів";
$MESS["IMCONNECTOR_MODULE_DESC"] = "Модуль дозволяє організувати роботу із зовнішніми месенджерами.";
$MESS["IMCONNECTOR_INSTALL_TITLE"] = "Установка модуля";
$MESS["IMCONNECTOR_UNINSTALL_TITLE"] = "Видалення модуля";
$MESS["IMCONNECTOR_DB_NOT_SUPPORTED"] = "Модуль підтримує роботу тільки з СУБД MySQL";
?>