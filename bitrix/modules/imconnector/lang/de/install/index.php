<?
$MESS["IMCONNECTOR_MODULE_NAME"] = "Connectors für externe Messenger";
$MESS["IMCONNECTOR_MODULE_DESC"] = "Das Modul ermöglicht die Nutzung von externen Messenger.";
$MESS["IMCONNECTOR_INSTALL_TITLE"] = "Modul installieren";
$MESS["IMCONNECTOR_UNINSTALL_TITLE"] = "Modul deinstallieren";
$MESS["IMCONNECTOR_DB_NOT_SUPPORTED"] = "Dieses Modul unterstützt nur MySQL.";
?>