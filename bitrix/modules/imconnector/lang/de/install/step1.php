<?
$MESS["IMCONNECTOR_PUBLIC_PATH_DESC"] = "Sie müssen öffentliche Adresse der Website angeben, damit Connectors korrekt funktionieren können.";
$MESS["IMCONNECTOR_PUBLIC_PATH_DESC_2"] = "Ist der Zugriff auf Website aus dem Netz nicht möglich, müssen bestimmte Seiten zugänglich gemacht werden, mehr erfahren Sie aus der #LINK_START# Dokumentation. #LINK_END#";
$MESS["IMCONNECTOR_PUBLIC_PATH"] = "Öffentliche Adresse der Website:";
?>