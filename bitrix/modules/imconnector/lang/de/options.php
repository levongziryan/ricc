<?
$MESS["IMCONNECTOR_TAB_SETTINGS"] = "Einstellungen der Messenger-Connectors";
$MESS["IMCONNECTOR_FIELD_DEBUG_TITLE"] = "Debugging-Modus aktivieren";
$MESS["IMCONNECTOR_FIELD_URI_CLIENT_TITLE"] = "Öffentliche Adresse der Website";
$MESS["IMCONNECTOR_FIELD_LIST_CONNECTOR_TITLE"] = "Liste der verwendeten Connectors";
$MESS["IMCONNECTOR_FIELD_URI_SERVER_TITLE"] = "Server-Adresse (z.B.: im.bitrix.info)";
?>