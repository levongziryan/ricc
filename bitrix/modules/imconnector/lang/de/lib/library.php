<?
$MESS["IMCONNECTOR_PROXY_NO_USER_IM"] = "Die ID des Messenger-Nutzers fehlt.";
$MESS["IMCONNECTOR_PROXY_NO_ADD_USER"] = "Ein Systemnutzer, der dem Nutzer des gelöschten Messenger entspricht, konnte nicht erstellt bzw. erhalten werden.";
$MESS["IMCONNECTOR_NOT_SPECIFIED_CORRECT_CONNECTOR"] = "Connector ist nicht angegeben.";
$MESS["IMCONNECTOR_NOT_SPECIFIED_CORRECT_COMMAND"] = "Der Befehl fehlt";
$MESS["IMCONNECTOR_NOT_ALL_THE_REQUIRED_DATA"] = "Daten nicht vollständig";
$MESS["IMCONNECTOR_EMPTY_PARAMETRS"] = "Leere Parameter angegeben";
$MESS["IMCONNECTOR_NOT_AVAILABLE_CONNECTOR"] = "Versuch der Verbindung mit einem inaktiven oder ungültigen Connector";
$MESS["IMCONNECTOR_FEATURE_IS_NOT_SUPPORTED"] = "Diese Funktion wird nicht unterstützt";
$MESS["IMCONNECTOR_ADD_EXISTING_CONNECTOR"] = "Versuch, einen Connector hinzuzufügen, der bereits existiert";
$MESS["IMCONNECTOR_UPDATE_NOT_EXISTING_CONNECTOR"] = "Versuch der Aktualisierung eines inaktiven Connectors";
$MESS["IMCONNECTOR_DELETE_NOT_EXISTING_CONNECTOR"] = "Versuch, einen inaktiven Connector zu löschen";
$MESS["IMCONNECTOR_FAILED_TO_ADD_CONNECTOR"] = "Connector der Kommunikationskanäle kann nicht hinzugefügt werden";
$MESS["IMCONNECTOR_FAILED_TO_UPDATE_CONNECTOR"] = "Connector der Kommunikationskanäle kann nicht aktualisiert werden";
$MESS["IMCONNECTOR_FAILED_TO_DELETE_CONNECTOR"] = "Connector der Kommunikationskanäle kann nicht gelöscht werden";
$MESS["IMCONNECTOR_FAILED_TO_LOAD_MODULE_OPEN_LINES"] = "Das Modul Kommunikationskanäle kann nicht geladen werden";
$MESS["IMCONNECTOR_FAILED_TO_SAVE_SETTINGS_CONNECTOR"] = "Connector-Einstellungen können nicht gespeichert werden";
$MESS["IMCONNECTOR_FAILED_TO_TEST_CONNECTOR"] = "Connector-Konnektivität kann nicht getestet werden";
$MESS["IMCONNECTOR_FAILED_REGISTER_CONNECTOR"] = "Connector kann nicht registriert werden";
?>