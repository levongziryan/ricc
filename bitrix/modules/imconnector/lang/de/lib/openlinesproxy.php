<?
$MESS["CONNECTOR_PROXY_NO_ADD_USER"] = "Ein Systemnutzer, der dem Nutzer des gelöschten Messenger entspricht, konnte nicht erstellt bzw. erhalten werden.";
$MESS["CONNECTOR_PROXY_TITLE_NEW_BOT_CHAT"] = "Neuer Chat mit dem Nutzer #USER_NAME# aus dem Messenger #CONNECTOR#";
$MESS["CONNECTOR_PROXY_NO_ADD_CHAT"] = "Bei eingehender Nachricht konnte der Chat nicht erstellt werden";
$MESS["CONNECTOR_PROXY_NO_ADD_MESSAGE"] = "Nachricht konnte nicht hinzugefügt werden.";
$MESS["CONNECTOR_PROXY_NO_USER_IM"] = "Die ID des Messenger-Nutzers fehlt.";
?>