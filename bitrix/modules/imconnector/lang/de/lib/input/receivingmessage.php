<?
$MESS["IMCONNECTOR_MAPS_NAME"] = "Karte";
$MESS["IMCONNECTOR_FORWARDED_MESSAGE"] = "Weitergeleitete Nachricht: ";
$MESS["IMCONNECTOR_CONTACT_NAME"] = "Vollständiger Name: ";
$MESS["IMCONNECTOR_CONTACT_PHONE"] = "Telefonnummer: ";
$MESS["IMCONNECTOR_WARNING_LARGE_FILE"] = "[b]Achtung![/b]
[i]Ein Nutzer hat versucht, eine große Datei zu senden, die wir nicht zustellen können. Er/sie sollte bitte diese Datei mit einer anderen Methode senden (bspw. via einen Filesharing-Service).[/i]
";
$MESS["IMCONNECTOR_COMMENT_IN_FACEBOOK"] = "Facebook-Kommentar";
$MESS["IMCONNECTOR_LINK_TO_ORIGINAL_POST_IN_FACEBOOK"] = "Link zum ursprünglichen Facebook-Beitrag: #LINK#";
$MESS["IMCONNECTOR_LINK_TO_ORIGINAL_POST_IN_INSTAGRAM"] = "Link zum ursprünglichen Instagram-Beitrag: #LINK#";
$MESS["IMCONNECTOR_LINK_TO_ORIGINAL_POST"] = "Link zum ursprünglichen Beitrag: #LINK#";
$MESS["IMCONNECTOR_GUEST_USER"] = "Besucher";
$MESS["IMCONNECTOR_WALL_TEXT"] = "Beitrag an der Wand von";
$MESS["IMCONNECTOR_WALL_DATE_TEXT"] = "Veröffentlicht am";
?>