<?
$MESS["IMCONNECTOR_TAB_SETTINGS"] = "Connecteurs de la messagerie";
$MESS["IMCONNECTOR_FIELD_DEBUG_TITLE"] = "Activer le mode débogage";
$MESS["IMCONNECTOR_FIELD_URI_CLIENT_TITLE"] = "Adresse public du site";
$MESS["IMCONNECTOR_FIELD_LIST_CONNECTOR_TITLE"] = "Connecteurs utilisés";
$MESS["IMCONNECTOR_FIELD_URI_SERVER_TITLE"] = "Adresse du serveur (exemple : im.bitrix.info)";
?>