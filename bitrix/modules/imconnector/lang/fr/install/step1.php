<?
$MESS["IM_CONNECTOR_PUBLIC_PATH_DESC"] = "Les connecteurs nécessite une adresse de site public pour être exécutés comme prévu.";
$MESS["IM_CONNECTOR_PUBLIC_PATH_DESC_2"] = "Si l'accès externe à votre réseau est restreint, activez-le seulement pour certaines pages. Veuillez consulter la #LINK_START#documentation#LINK_END# pour plus de détails.";
$MESS["IM_CONNECTOR_PUBLIC_PATH"] = "Adresse public du site :";
?>