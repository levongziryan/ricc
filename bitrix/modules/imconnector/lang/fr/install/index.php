<?
$MESS["IM_CONNECTOR_MODULE_NAME"] = "Connecteurs de messagerie externes";
$MESS["IM_CONNECTOR_MODULE_DESC"] = "Connectez des messageries externes en utilisant ce module.";
$MESS["IM_CONNECTOR_INSTALL_TITLE"] = "Installation du module";
$MESS["IM_CONNECTOR_UNINSTALL_TITLE"] = "Désinstallation du module";
?>