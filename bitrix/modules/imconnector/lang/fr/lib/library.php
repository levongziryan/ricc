<?
$MESS["IMCONNECTOR_PROXY_NO_USER_IM"] = "Impossible d'obtenir l'ID de l'utilisateur de messagerie instantanée.";
$MESS["IMCONNECTOR_PROXY_NO_ADD_USER"] = "Impossible de créer l'utilisateur ou de le tracer vers un utilisateur de messagerie externe.";
$MESS["IMCONNECTOR_NOT_SPECIFIED_CORRECT_CONNECTOR"] = "Le connecteur n'est pas spécifié.";
$MESS["IMCONNECTOR_NOT_SPECIFIED_CORRECT_COMMAND"] = "Commande fournie incorrecte.";
$MESS["IMCONNECTOR_NOT_ALL_THE_REQUIRED_DATA"] = "Données incomplètes";
$MESS["IMCONNECTOR_EMPTY_PARAMETRS"] = "Paramètres vides fournis";
$MESS["IMCONNECTOR_NOT_AVAILABLE_CONNECTOR"] = "Tentative de connexion à un connecteur inactif ou indisponible";
$MESS["IMCONNECTOR_FEATURE_IS_NOT_SUPPORTED"] = "Cette fonctionnalité n'est pas supportée";
$MESS["IMCONNECTOR_ADD_EXISTING_CONNECTOR"] = "Tentative d'ajout d'un connecteur qui existe déjà";
$MESS["IMCONNECTOR_UPDATE_NOT_EXISTING_CONNECTOR"] = "Tentative de mise à jour d'un connecteur inactif";
$MESS["IMCONNECTOR_DELETE_NOT_EXISTING_CONNECTOR"] = "Tentative de suppression d'un connecteur inactif";
$MESS["IMCONNECTOR_FAILED_TO_ADD_CONNECTOR"] = "Impossible d'ajouter le connecteur de Canaux ouverts";
$MESS["IMCONNECTOR_FAILED_TO_UPDATE_CONNECTOR"] = "Impossible de mettre à jour le connecteur de Canaux ouverts";
$MESS["IMCONNECTOR_FAILED_TO_DELETE_CONNECTOR"] = "Impossible de supprimer le connecteur de Canaux ouverts";
$MESS["IMCONNECTOR_FAILED_TO_LOAD_MODULE_OPEN_LINES"] = "Impossible de charger le connecteur de Canaux ouverts";
$MESS["IMCONNECTOR_FAILED_TO_SAVE_SETTINGS_CONNECTOR"] = "Impossible d'enregistrer les préférences du connecteur";
$MESS["IMCONNECTOR_FAILED_TO_TEST_CONNECTOR"] = "Impossible de tester la connectivité du connecteur";
$MESS["IMCONNECTOR_FAILED_REGISTER_CONNECTOR"] = "Impossible d'inscrire le connecteur";
?>