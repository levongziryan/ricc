<?
$MESS["CONNECTOR_PROXY_NO_ADD_USER"] = "Impossible de créer l'utilisateur ou de le tracer vers un utilisateur de messagerie externe.";
$MESS["CONNECTOR_PROXY_TITLE_NEW_BOT_CHAT"] = "Nouveau chat avec #USER_NAME# via #CONNECTOR#";
$MESS["CONNECTOR_PROXY_NO_ADD_CHAT"] = "Impossible de créer un chat sur un message reçu";
$MESS["CONNECTOR_PROXY_NO_ADD_MESSAGE"] = "Impossible d'ajouter le message";
$MESS["CONNECTOR_PROXY_NO_USER_IM"] = "Échec de la récupération de l'ID d'utilisateur IM.";
?>