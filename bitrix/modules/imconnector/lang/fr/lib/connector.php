<?
$MESS["IMCONNECTOR_NAME_CONNECTOR_LIVECHAT"] = "Chat live";
$MESS["IMCONNECTOR_NAME_CONNECTOR_NETWORK"] = "Bitrix24.Network";
$MESS["IMCONNECTOR_NAME_CONNECTOR_TELEGRAM_BOT"] = "Telegram";
$MESS["IMCONNECTOR_NAME_CONNECTOR_VK_GROUP"] = "VK";
$MESS["IMCONNECTOR_NAME_CONNECTOR_FACEBOOK_PAGE"] = "Facebook";
$MESS["IMCONNECTOR_NAME_CONNECTOR_FACEBOOK_COMMENTS_PAGE"] = "Facebook: Commentaires";
$MESS["IMCONNECTOR_NAME_CONNECTOR_INSTAGRAM"] = "Instagram";
$MESS["IMCONNECTOR_ERROR_MAIN_MODULE_URL_SITE_USE"] = "Les noms de domaine spécifiés dans les paramètres du module Noyau et le domaine actuel sont différents. Le nom de domaine #DOMAIN# spécifié dans les paramètres du module Noyau est utilisé pour la réponse du serveur. Pour changer le nom de domaine, <a href=\"/bitrix/admin/settings.php?lang=ru&mid=main&mid_menu=1\">modifiez le champ \"URL du site\".</a>";
$MESS["IMCONNECTOR_ERROR_SERVER_NAME_USE"] = "Le champ \"URL du site\" du module Noyeau est vide. Le nom du domaine actuel #DOMAIN# sera utilisé pour la réponse du serveur. Pour changer le nom de domaine, <a href=\"/bitrix/admin/settings.php?lang=ru&mid=main&mid_menu=1\">modifiez le champ \"URL du site\".</a>";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK"] = "Skype, Slack, Twilio (SMS), Kik, GroupMe, Office365 e-mail, Web-chat, Telegram, Facebook - Microsoft Bot Framework";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_SKYPE"] = "Skype (MS: Bot Framework)";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_SLACK"] = "Slack (MS: Bot Framework)";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_KIK"] = "Kik (MS: Bot Framework)";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_GROUPME"] = "GroupMe (MS: Bot Framework)";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_TWILIO"] = "Twilio: SMS (MS: Bot Framework)";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_MSTEAMS"] = "Équipes de Microsoft (MS : Bot Framework)";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_WEBCHAT"] = "Web Chat (MS: Bot Framework)";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_EMAILOFFICE365"] = "Office365 email (MS: Bot Framework)";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_TELEGRAM"] = "Telegram (MS: Bot Framework)";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_FACEBOOKMESSENGER"] = "Facebook Messenger (MS: Bot Framework)";
$MESS["IMCONNECTOR_NAME_CONNECTOR_BOTFRAMEWORK_DIRECTLINE"] = "Direct Line (MS: Bot Framework)";
$MESS["IMCONNECTOR_NAME_CONNECTOR_VIBER_BOT"] = "Viber";
?>