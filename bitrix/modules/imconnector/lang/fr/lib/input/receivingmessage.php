<?
$MESS["IMCONNECTOR_MAPS_NAME"] = "Carte";
$MESS["IMCONNECTOR_FORWARDED_MESSAGE"] = "Message redirigé : ";
$MESS["IMCONNECTOR_CONTACT_NAME"] = "Nom complet : ";
$MESS["IMCONNECTOR_CONTACT_PHONE"] = "Numéro de téléphone : ";
$MESS["IMCONNECTOR_WARNING_LARGE_FILE"] = "[b]Attention ![/b]
[i]Un utilisateur a essayé d'envoyer un fichier volumineux que nous ne pouvons livrer.
Veuillez lui demander d'envoyer le fichier via une autre méthode (par exemple : un service de partage de fichiers).[/i]

";
$MESS["IMCONNECTOR_COMMENT_IN_FACEBOOK"] = "Commentaire Facebook";
$MESS["IMCONNECTOR_LINK_TO_ORIGINAL_POST_IN_FACEBOOK"] = "Lien vers la publication Facebook originale : #LINK#";
$MESS["IMCONNECTOR_LINK_TO_ORIGINAL_POST_IN_INSTAGRAM"] = "Lien vers la publication Instagram originale : #LINK#";
$MESS["IMCONNECTOR_LINK_TO_ORIGINAL_POST"] = "Lien vers la publication originale : #LINK#";
$MESS["IMCONNECTOR_GUEST_USER"] = "Invité";
$MESS["IMCONNECTOR_WALL_TEXT"] = "Publication de mur par";
$MESS["IMCONNECTOR_WALL_DATE_TEXT"] = "publié sur";
?>