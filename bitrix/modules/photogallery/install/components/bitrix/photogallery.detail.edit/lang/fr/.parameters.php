<?
$MESS["IBLOCK_SECTION_ID"] = "ID de la section";
$MESS["IBLOCK_ELEMENT_ID"] = "Identifiant de l'élément";
$MESS["T_IBLOCK_DESC_NEWS_PANEL"] = "Ajouter les boutons au panneau admin. Pour ce composant";
$MESS["IBLOCK_IBLOCK"] = "Bloc d'information";
$MESS["IBLOCK_USER_ALIAS"] = "Code Gallery";
$MESS["IBLOCK_BEHAVIOUR_USER"] = "Multiutilisateur galerie photo";
$MESS["IBLOCK_BEHAVIOUR_SIMPLE"] = "ordinaire";
$MESS["P_GALLERY_SIZE"] = "Galerie taille";
$MESS["IBLOCK_BEHAVIOUR"] = "Mode Galerie";
$MESS["IBLOCK_GALLERY_URL"] = "Contenu de la galerie";
$MESS["IBLOCK_TYPE"] = "Type du bloc d'information";
$MESS["P_SET_STATUS_404"] = "Tablir le statut 404 si l'élément ou la section ne sont pas trouvés";
$MESS["T_DATE_TIME_FORMAT"] = "Format de la date";
$MESS["IBLOCK_SECTION_URL"] = "URL menant vers la page avec le contenu de la section";
$MESS["IBLOCK_DETAIL_URL"] = "L'URL guidant vers la page avec le contenu de l'élément de la rubrique";
$MESS["P_ACTION"] = "Action";
?>