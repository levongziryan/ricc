<?
$MESS["F_FORUM_ID"] = "Forum ID";
$MESS["IBLOCK_ELEMENT_ID"] = "Identifiant de l'élément";
$MESS["F_BLOG_URL"] = "Blogue pour les commentaires";
$MESS["P_COMMENTS_TYPE_BLOG"] = "Blog";
$MESS["F_PREORDER"] = "Afficher les messages en ordre direct";
$MESS["IBLOCK_IBLOCK"] = "Bloc d'information";
$MESS["F_USE_CAPTCHA"] = "Utiliser CAPTCHA";
$MESS["F_COMMENTS_COUNT"] = "Quantité de commentaires sur une page";
$MESS["P_COMMENTS_TYPE"] = "Composant des commentaires";
$MESS["P_PATH_TO_BLOG"] = "Chemin vers le blogue";
$MESS["P_PATH_TO_USER"] = "Chemin vers le profil d'utilisateur";
$MESS["F_PATH_TO_SMILE"] = "Chemin par rapport à la racine du site vers le dossier avec des smileys";
$MESS["IBLOCK_DETAIL_URL"] = "Page de l'examen détaillé";
$MESS["F_READ_TEMPLATE"] = "Page de lecture du message";
$MESS["IBLOCK_TYPE"] = "Type du bloc d'information";
$MESS["P_COMMENTS_TYPE_FORUM"] = "Forum";
?>