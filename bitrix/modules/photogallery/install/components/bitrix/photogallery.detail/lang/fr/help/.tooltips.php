<?
$MESS["CACHE_TYPE_TIP"] = "<i>Auto</i>: cache est valide pendant un temps prédéterminé dans les paramètres;<br /><i>Mettre en cache</i>: pour la mise en cache il faut déterminer seulement le temps de la mise en cache;<br /><i>Ne pas mettre en cache</i>: pas de mise en cache en tout cas.";
$MESS["ELEMENT_SORT_ORDER_TIP"] = "Choisissez l'ordre du tri des photos: par ascension ou par diminution.";
$MESS["IBLOCK_TYPE_TIP"] = "Sélectionnez ici l'un des types de blocs d'information existants. Cliquez sur <b><i>OK</i></b> pour charger des blocs d'information du type sélectionné.";
$MESS["CACHE_TIME_TIP"] = "Veuillez indiquer la période de temps durant laquelle la mémoire cache reste valide.";
$MESS["SECTION_ID_TIP"] = "Le champ comprend le code avec ID de la section (album).";
$MESS["ELEMENT_ID_TIP"] = "Le champ contient le code par lequel ID de l'élément (de la photo) est transmis.";
$MESS["SET_TITLE_TIP"] = "Si l'option est cochée, alors le nom de l'album sera spécifié en qualité de l'en-tête de la page.";
$MESS["DISPLAY_PANEL_TIP"] = "Si elle est cochée, le gestionnaire sera de retour l'ID de l'emplacement.";
$MESS["SECTION_CODE_TIP"] = "Identificateur symbolique de l'album.";
$MESS["ELEMENT_CODE_TIP"] = "Identificateur symbole de la photo.";
$MESS["IBLOCK_ID_TIP"] = "Spécifiez le bloc d'information où on va sauvegarder les photos. Si le point <i>(autre)-></i> est choisi il faut spécifier ID du bloc d'information dans le champ adjacent.";
$MESS["DETAIL_URL_TIP"] = "Indique l'adresse de la page pleine de vue photo.";
$MESS["SECTION_URL_TIP"] = "Indique l'adresse de la page de présentation de l'album.";
$MESS["DETAIL_EDIT_URL_TIP"] = "Indique l'adresse de la page des paramètres de photo.";
$MESS["DATE_TIME_FORMAT_TIP"] = "Indique le format d'affichage de la date.";
$MESS["UPLOAD_URL_TIP"] = "Indique l'adresse de la page de téléchargement d'image.";
$MESS["ELEMENT_SORT_FIELD_TIP"] = "Indique le champ permettant de trier les photos.";
?>