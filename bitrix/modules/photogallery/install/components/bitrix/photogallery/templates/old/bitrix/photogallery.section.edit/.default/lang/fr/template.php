<?
$MESS["P_ALBUM_DATE"] = "Date";
$MESS["P_ALBUM_NAME"] = "Dénomination";
$MESS["P_SET_PASSWORD"] = "Limiter l'accès à l'album par le mot de passe";
$MESS["P_ALBUM_DESCRIPTION"] = "Description";
$MESS["P_CANCEL"] = "Annuler";
$MESS["P_PASSWORD"] = "Mot de passe";
$MESS["P_UP_TITLE"] = "Accéder à la liste des albums";
$MESS["P_EDIT_SECTION"] = "dition des qualités de l'album";
$MESS["P_SUBMIT"] = "Sauvegarder";
$MESS["P_UP"] = "Albums";
?>