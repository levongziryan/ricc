<?
$MESS ['P_GALLERY_DELETE_ASK'] = "Wollen Sie diese Fotogalerie wirklich unwiderruflich löschen?";
$MESS ['P_GALLEY_BY_USER'] = "Autor:";
$MESS ['P_GALLERY_CREATE'] = "Galerie erstellen";
$MESS ['P_GALLERY_CREATE_TITLE'] = "Meine Galerie erstellen";
$MESS ['P_GALLERY_ACTIVE'] = "Als Standard";
$MESS ['P_GALLERY_DELETE'] = "Galerie löschen";
$MESS ['P_GALLERY_EDIT'] = "Album-Einstellungen bearbeiten";
$MESS ['P_GALLERY_EDIT_TITLE'] = "Galerieeinstellungen bearbeiten";
$MESS ['P_ERROR_CODE'] = "Der Galeriecode wurde falsch angegeben. Möglicherweise wird die Galerie nicht richtig funktionieren.";
$MESS ['P_UPLOAD'] = "Fotos hochladen";
$MESS ['P_UPLOAD_TITLE'] = "Fotos hochladen";
$MESS ['P_GALLERY_VIEW_TITLE'] = "Alben in der &laquo;#GALLERY#&raquo; anzeigen";
?>