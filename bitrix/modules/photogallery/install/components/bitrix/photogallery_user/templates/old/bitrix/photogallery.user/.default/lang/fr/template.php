<?
$MESS["P_LOGIN_TITLE"] = "Identifiez-vous sur le site";
$MESS["P_LOGIN"] = "Connexion";
$MESS["P_GALLERY"] = "Accéder à la page principale de la galerie";
$MESS["P_UPLOAD"] = "Charger une photo";
$MESS["P_GALLERY_SIZE"] = "Rempli";
$MESS["P_GALLERIES_VIEW"] = "Mes galeries";
$MESS["P_PHOTO_VIEW"] = "Mes photos";
$MESS["P_GALLERY_VIEW"] = "Ma galerie";
$MESS["P_GALLERY_TITLE"] = "Accéder à la page principale de la galerie";
$MESS["P_GALLERY_SIZE_RECALC"] = "Conversion";
$MESS["P_GALLERY_SIZE_RECALC_NEW"] = "Conversion";
$MESS["P_GALLERY_SIZE_RECOUNT"] = "Conversion";
$MESS["P_GALLERY_SIZE_RECALC_CONTINUE"] = "Continuer";
$MESS["P_GALLERY_VIEW_TITLE"] = "Editer ma galerie";
$MESS["P_PHOTO_VIEW_TITLE"] = "Consulter mes photos";
$MESS["P_GALLERIES_VIEW_TITLE"] = "Afficher la liste de mes galeries";
$MESS["P_GALLERY_CREATE"] = "Créer ma galerie";
$MESS["P_GALLERY_CREATE_TITLE"] = "Créer ma galerie";
?>