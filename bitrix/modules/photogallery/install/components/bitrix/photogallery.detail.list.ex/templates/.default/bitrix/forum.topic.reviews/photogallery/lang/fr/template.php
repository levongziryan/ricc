<?
$MESS["COMMENTS_N_FROM_M"] = "#N# de #M#";
$MESS["OPINIONS_EMAIL"] = "Votre e-mail";
$MESS["OPINIONS_NAME"] = "Votre prénom";
$MESS["JERROR_NO_TOPIC_NAME"] = "Vous devez saisir la dénomination du sujet.";
$MESS["JERROR_NO_MESSAGE"] = "Insérer le message, s'il vous plaît.";
$MESS["ADD_COMMENT"] = "Ajouter un commentaire";
$MESS["ADD_COMMENT_TITLE"] = "Ajouter un commentaire (Ctrl + Enter)";
$MESS["F_CAPTCHA_TITLE"] = "Protection contre les messages automatiques";
$MESS["JERROR_MAX_LEN"] = "Longueur maximale du message #MAX_LENGTH# symboles. Total de symboles: #LENGTH#.";
$MESS["REQUIRED_FIELD"] = "Champ obligatoire";
$MESS["JQOUTE_AUTHOR_WRITES"] = "a écrit";
$MESS["MORE_COMMENTS"] = "Afficher les commentaires précédents";
$MESS["F_CAPTCHA_PROMT"] = "Symboles sur l'image";
?>