<?
$MESS["OPINIONS_NAME"] = "Seu nome";
$MESS["OPINIONS_EMAIL"] = "Seu e-mail";
$MESS["REQUIRED_FIELD"] = "Campo obrigatório";
$MESS["F_CAPTCHA_TITLE"] = "Proteção contra robô de spam (CAPTCHA)";
$MESS["F_CAPTCHA_PROMT"] = "Caracteres de imagem CAPTCHA";
$MESS["JQOUTE_AUTHOR_WRITES"] = "escreveu";
$MESS["JERROR_NO_TOPIC_NAME"] = "Por favor, forneça o título da mensagem.";
$MESS["JERROR_NO_MESSAGE"] = "Por favor, escreva a sua mensagem.";
$MESS["JERROR_MAX_LEN"] = "O comprimento máximo da mensagem é de #MAX_LENGTH# símbolos. Total de símbolos: #LENGTH#.";
$MESS["ADD_COMMENT"] = "Adicionar comentário";
$MESS["ADD_COMMENT_TITLE"] = "Adicionar comentário (CTRL+Enter)";
$MESS["MORE_COMMENTS"] = "Exibir outros comentários";
$MESS["COMMENTS_N_FROM_M"] = "#N# de #M#";
?>