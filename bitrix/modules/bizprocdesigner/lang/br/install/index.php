<?
$MESS["BIZPROCDESIGNER_INSTALL_NAME"] = "Processos de Negócios Designer";
$MESS["BIZPROCDESIGNER_INSTALL_DESCRIPTION"] = "Um módulo para projetar, criar e editar os processos de negócios.";
$MESS["BIZPROC_INSTALL_TITLE"] = "Instalação do Módulo";
$MESS["BIZPROC_PHP_L439"] = "Você está usando a versão PHP #VERS#, mas o módulo requer a versão 5.0.0 ou superior. Por favor, atualize sua instalação do PHP ou em contato com o suporte técnico.";
$MESS["BIZPROC_ERROR_BPM"] = "O módulo de processos de negócio é necessário.";
$MESS["BIZPROC_ERROR_EDITABLE"] = "Sua edição não fornece este módulo.";
?>