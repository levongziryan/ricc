<?
$MESS["BIZPROC_AS_TITLE"] = "Parâmetros de ação";
$MESS["BIZPROC_AS_DESC"] = "Por favor configure os parâmetros de ação.";
$MESS["BIZPROC_AS_SEL_FIELD_BUTTON"] = "Selecionar Campos";
$MESS["BIZPROC_AS_ACT_TITLE"] = "Título:";
$MESS["BP_ACT_SET_ID_EMPTY"] = "O ID de ação não pode ser vazio.";
$MESS["BP_ACT_SET_ID_DUP"] = "A ação ID #ID# já está em uso por este modelo.";
$MESS["BP_ACT_SET_ID_SHOWHIDE"] = "Mostrar/ocultar ID da ação";
$MESS["BP_ACT_SET_ID"] = "ID";
$MESS["BP_ACT_SET_ID_ROW"] = "ID da Ação:";
?>