<?
$MESS["BIZPROC_WFS_TITLE"] = "Parâmetros de Modelos";
$MESS["BIZPROC_WFS_PROP_STRING"] = "String";
$MESS["BIZPROC_WFS_PROP_TEXT"] = "Texto multilinha";
$MESS["BIZPROC_WFS_PROP_INT"] = "Número inteiro";
$MESS["BIZPROC_WFS_PROP_DOUBLE"] = "Número";
$MESS["BIZPROC_WFS_PROP_SELECT"] = "Lista";
$MESS["BIZPROC_WFS_PROP_BOOL"] = "Sim / Não";
$MESS["BIZPROC_WFS_PROP_DATA"] = "Data";
$MESS["BIZPROC_WFS_PROP_DATETIME"] = "Data/Hora";
$MESS["BIZPROC_WFS_PROP_USER"] = "Usuário";
$MESS["BIZPROC_WFS_YES"] = "Sim";
$MESS["BIZPROC_WFS_NO"] = "Não";
$MESS["BIZPROC_WFS_PARAM_REQ"] = "O nome do parâmetro é obrigatório.";
$MESS["BIZPROC_WFS_PARAM_ID"] = "O ID do parâmetro é obrigatório.";
$MESS["BIZPROC_WFS_PARAM_ID1"] = "O ID do parâmetro pode conter apenas letras latinas e algarismos.";
$MESS["BIZPROC_WFS_CHANGE_PARAM"] = "Editar";
$MESS["BIZPROC_WFS_DEL_PARAM"] = "Excluir";
$MESS["BIZPROC_WFS_TAB_MAIN"] = "Geral";
$MESS["BIZPROC_WFS_TAB_MAIN_TITLE"] = "Configurações Gerais";
$MESS["BIZPROC_WFS_TAB_PARAM"] = "Parâmetros";
$MESS["BIZPROC_WFS_TAB_PARAM_TITLE"] = "Parâmetros de Execução de Processos de Negócios";
$MESS["BIZPROC_WFS_PAR_NAME"] = "Título:";
$MESS["BIZPROC_WFS_PAR_DESC"] = "Descrição:";
$MESS["BIZPROC_WFS_PAR_AUTO"] = "Autorun:";
$MESS["BIZPROC_WFS_PAR_AUTO_ADD"] = "Quando adicionado";
$MESS["BIZPROC_WFS_PAR_AUTO_UPD"] = "Quando mudou";
$MESS["BIZPROC_WFS_PARAM_NAME"] = "Nome";
$MESS["BIZPROC_WFS_PARAM_TYPE"] = "Tipo";
$MESS["BIZPROC_WFS_PARAM_REQUIRED"] = "Necessário";
$MESS["BIZPROC_WFS_PARAM_MULT"] = "Múltiplo";
$MESS["BIZPROC_WFS_PARAM_DEF"] = "Padrçai";
$MESS["BIZPROC_WFS_PARAM_ACT"] = "Ações";
$MESS["BIZPROC_WFS_ADD_PARAM"] = "Adicionar parâmetro ...";
$MESS["BIZPROC_WFS_PARAMDESC"] = "Descrição";
$MESS["BIZPROC_WFS_PARAMLIST"] = "Valores possíveis:";
$MESS["BIZPROC_WFS_PARAMLIST_DESC"] = "(Um valor por linha, se um valor variante e o nome são diferentes, especificar a variante entre colchetes à direita do nome, por exemplo: [v1] Variant 1)";
$MESS["BIZPROC_WFS_PARAMDEF"] = "Valor padrão";
$MESS["BIZPROC_WFS_BUTTON_CANCEL"] = "Cancelar";
$MESS["BIZPROC_WFS_BUTTON_SAVE"] = "Salvar";
$MESS["BP_WF_SAVEERR"] = "Ocorreu um erro ao salvar um objeto.";
$MESS["BP_WF_TAB_VARS"] = "Variáveis";
$MESS["BP_WF_TAB_VARS_TITLE"] = "Variáveis de Processos de Negócios";
$MESS["BP_WF_TAB_PERM"] = "Acessar";
$MESS["BP_WF_TAB_PERM_TITLE"] = "Permissão de Acesso de Processos de Negócios";
$MESS["BP_WF_VAR_ADD"] = "Adicionar variável";
$MESS["BIZPROC_WFS_PARAMID"] = "ID";
$MESS["BP_WF_UP"] = "Para cima";
$MESS["BP_WF_DOWN"] = "Para baixo";
$MESS["BIZPROC_WFS_PARAM_ID_EXISTS"] = "Um parâmetro com o ID \"#ID#\" já existe.";
$MESS["BP_WF_TAB_CONSTANTS"] = "Constantes";
$MESS["BP_WF_TAB_CONSTANTS_TITLE"] = "Constantes do Fluxo de Trabalho";
$MESS["BP_WF_CONSTANT_ADD"] = "Adicionar uma constante";
$MESS["BIZPROC_WFS_CONSTANTDEF"] = "Valor da constante";
$MESS["BP_WF_TAB_PERM_NOSETTINGS"] = "Não é possível estabelecer permissões de acesso para este tipo de documento.";
?>