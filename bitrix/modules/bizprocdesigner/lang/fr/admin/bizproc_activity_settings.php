<?
$MESS["BIZPROC_AS_SEL_FIELD_BUTTON"] = "Sélectionner des champs";
$MESS["BIZPROC_AS_ACT_TITLE"] = "Dénomination:";
$MESS["BP_ACT_SET_ID"] = "ID";
$MESS["BP_ACT_SET_ID_DUP"] = "L'identificateur de l'action #ID# est déjà utilisé dans ce modèle.";
$MESS["BP_ACT_SET_ID_EMPTY"] = "L'identificateur de l'action ne peut pas être vide.";
$MESS["BP_ACT_SET_ID_ROW"] = "ID de l'action:";
$MESS["BIZPROC_AS_TITLE"] = "Configuration des paramètres de l'action";
$MESS["BIZPROC_AS_DESC"] = "Configurez, s'il vous plaît, les paramètres de l'action.";
$MESS["BP_ACT_SET_ID_SHOWHIDE"] = "Montrer/cacher l'identifiant de l'action";
?>