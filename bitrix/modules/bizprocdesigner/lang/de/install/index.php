<?
$MESS["BIZPROCDESIGNER_INSTALL_NAME"] = "Geschäftsprozess-Designer";
$MESS["BIZPROCDESIGNER_INSTALL_DESCRIPTION"] = "Das Modul zum Entwerfen, Erstellen und Bearbeiten von Geschäftsprozessen.";
$MESS["BIZPROC_INSTALL_TITLE"] = "Modulinstallation";
$MESS["BIZPROC_PHP_L439"] = "Sie verwenden PHP Version #VERS#, das Modul jedoch erfordert die Version 5.0.0 oder höher. Bitte aktualisieren Sie Ihre PHP Installation oder wenden Sie sich an den technischen Support.";
$MESS["BIZPROC_ERROR_BPM"] = "Das Geschäftsprozess-Modul muss installiert sein.";
$MESS["BIZPROC_ERROR_EDITABLE"] = "Dieses Modul ist in Ihrer Edition nicht verfügbar.";
?>