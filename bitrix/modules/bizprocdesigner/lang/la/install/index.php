<?
$MESS["BIZPROCDESIGNER_INSTALL_NAME"] = "Diseñador de los proceso de negocios";
$MESS["BIZPROCDESIGNER_INSTALL_DESCRIPTION"] = "Un modulo a diseñar, crear y editar procesos de negocios.";
$MESS["BIZPROC_INSTALL_TITLE"] = "Modulo de instalación";
$MESS["BIZPROC_PHP_L439"] = "Usted esta usando el PHP versión #VERS#, pero el modulo requiere la versión 5.0.0 o superior. Por favor actualice su instalación de PHP o contacte al soporte técnico.";
$MESS["BIZPROC_ERROR_BPM"] = "El modulo de procesos de negocios es requerido.";
$MESS["BIZPROC_ERROR_EDITABLE"] = "Su edición no provee a este módulo.";
?>