<?
$MESS["VIDEO_CONF_USER_INVITE_NAME"] = "Запрошення на відеоконференцію";
$MESS["VIDEO_CONF_USER_INVITE_DESC"] = "#USER_NAME# — Ім'я запрошуваного
#USER_LAST_NAME# — Прізвище запрошуваного
#SENDER_NAME# — Ім'я запрошувача
#SENDER_LAST_NAME# — Прізвище запрошувача
#EMAIL_TO# — E-mail одержувача листа
#MESSAGE# — Текст повідомлення
#TITLE# — Заголовок повідомлення
";
$MESS["VIDEO_CONF_USER_INVITE_SUBJECT"] = "#SITE_NAME#: [V] #SENDER_NAME# #SENDER_LAST_NAME# запрошує вас прийняти участь у відеоконференції";
$MESS["VIDEO_CONF_USER_INVITE_MESSAGE"] = "Інформаційне повідомлення сайта #SITE_NAME#
------------------------------------------

#MESSAGE#

Повідомлення сгенеровано автоматично.
";
$MESS["VIDEO_CALL_USER_INVITE_NAME"] = "Відеодзвінок";
$MESS["VIDEO_CALL_USER_INVITE_DESC"] = "#USER_NAME# — Ім'я запрошуваного
#USER_LAST_NAME# — Прізвище запрошуваного
#SENDER_NAME# — Ім'я запрошувача
#SENDER_LAST_NAME# — Прізвище запрошувача
#EMAIL_TO# — E-mail одержувача листа
#MESSAGE# — Текст повідомлення
#TITLE# — Заголовок повідомлення
";
$MESS["VIDEO_CALL_USER_INVITE_SUBJECT"] = "#SITE_NAME#: [V] Відеодзвінок від #SENDER_NAME# #SENDER_LAST_NAME#";
$MESS["VIDEO_CALL_USER_INVITE_MESSAGE"] = "Інформаційне повідомлення сайта #SITE_NAME#
------------------------------------------

#MESSAGE#

Повідомлення сгенеровано автоматично.
";
?>