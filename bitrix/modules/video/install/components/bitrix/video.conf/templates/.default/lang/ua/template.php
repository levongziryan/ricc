<?
$MESS["VCT_NEED_AUTH"] = "Необхідно авторизуватися для участі у відеоконференції";
$MESS["VCT_CONF_NOT_FOUND"] = "Конференцію не знайдено";
$MESS["VCT_CHAT"] = "Чат";
$MESS["VCT_MEMBERS"] = "Учасники";
$MESS["VCT_OWNER"] = "(Організатор)";
$MESS["VCT_TITLE_JOIN"] = "Запросити";
$MESS["VCT_SETTINGS"] = "Налаштування";
$MESS["VCT_INFORMATION"] = "Інформація";
$MESS["VCT_REMAIN"] = "Залишилось:";
$MESS["VCT_PROLONG"] = "Подовжити на ";
$MESS["VCT_PROLONG_MIN"] = "хв.";
$MESS["VCT_COUNT_MEMBERS"] = "Учасників:";
$MESS["VCT_COUNT_ACTIVE_MEMBERS"] = "Онлйан учасників:";
$MESS["VCT_CAMERA"] = "Камера";
$MESS["VCT_LOGOUT"] = "Покинути конференцію";
$MESS["VCT_COUNT_MAX_USERS"] = "Залишити конференцію";
$MESS["VCT_LINK_WRITE_MESSAGE"] = "Написати особисте повідомлення";
$MESS["VCT_LINK_VIEW_PROFILE"] = "Переглянути особисту сторінку";
$MESS["VCT_LINK_EXPEL_USER"] = "Видалити із конференції";
$MESS["VCT_END_CONF"] = "Закінчити конференцію";
$MESS["VCT_LOADING"] = "Завантаження…";
$MESS["VCT_CALL_TO_CONF"] = "Розширити відеодзвінок до відеоконференції";
?>