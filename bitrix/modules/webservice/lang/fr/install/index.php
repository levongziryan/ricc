<?
$MESS["WEBS_MODULE_NAME"] = "Enquêtes, votes";
$MESS["WS_GADGET_DESCR"] = "Les utilisateurs de Windows Vista peuvent installer dans la barre latérale ou sur le bureau d'ordinateur une application mini qui permettra d'afficher les statistiques sommaires du site.";
$MESS["WEBS_MODULE_DESCRIPTION"] = "Module permettant d'organiser le système des sondages et des votes des visiteurs du site.";
$MESS["WEBS_UNINSTALL_TITLE"] = "Suppression du module d'interrogations";
$MESS["WEBS_INSTALL_TITLE"] = "Installation du module pour sondages";
$MESS["WS_GADGET_LINK"] = "Télécharger Vista Sidebar gadget";
?>