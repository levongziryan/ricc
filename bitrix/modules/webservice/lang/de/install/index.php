<?
$MESS ['WEBS_MODULE_DESCRIPTION'] = "Das Modul ermöglicht die Oranisation der Web-Services und SOAP.";
$MESS ['WEBS_UNINSTALL_TITLE'] = "Deinstallation des Moduls \"Web-Service\"";
$MESS ['WEBS_INSTALL_TITLE'] = "Installation des Moduls \"Web-Service\"";
$MESS ['WS_GADGET_LINK'] = "Vista Sidebar gadget download";
$MESS ['WEBS_MODULE_NAME'] = "Web-Service";
$MESS ['WS_GADGET_DESCR'] = "Windows Vista User können spezielles Vista Sidebar Gadget installieren, um die zusammengefassten Statistiken für die Seite auf der Sidebar oder auf dem Desktop anzuzeigen oder herunterzuladen.";
?>