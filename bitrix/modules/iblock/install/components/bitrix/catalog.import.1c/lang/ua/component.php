<?
$MESS["CC_BCI1_GENERATE_PREVIEW"] = "Автоматично генерувати картинку анонсу";
$MESS["CC_BCI1_USE_OFFERS"] = "Вести пропозиції в окремому інфоблоці";
$MESS["CC_BCI1_DELETE"] = "видалити";
$MESS["CC_BCI1_USE_CRC"] = "Використовувати контрольні суми елементів для оптимізації оновлення каталога";
$MESS["CC_BCI1_USE_ZIP"] = "Використовувати стиснення zip, якщо доступно";
$MESS["CC_BSC1_SECTIONS_IMPORTED"] = "Групи імпортовані.";
$MESS["CC_BSC1_SECTION_DEA_DONE"] = "Деактивація/Видалення груп завершено";
$MESS["CC_BSC1_DEA_DONE"] = "Деактивація/Видалення елементів завершені";
$MESS["CC_BCI1_DEACTIVATE"] = "деактивувати";
$MESS["CC_BSC1_ZIP_PROGRESS"] = "Йде розпакування архіву.";
$MESS["CC_BSC1_IMPORT_SUCCESS"] = "Імпорт успішно завершений.";
$MESS["CC_BCI1_INTERVAL"] = "Інтервал одного кроку в секундах (0 — виконувати завантаження за один крок)";
$MESS["CC_BSC1_METADATA_IMPORTED"] = "Метадані імпортовані успішно.";
$MESS["CC_BSC1_ERROR_MODULE"] = "Модуль Інформаційних блоків не встановлено";
$MESS["CC_BSC1_ERROR_UNKNOWN_COMMAND"] = "Невідома команда.";
$MESS["CC_BSC1_ERROR_FILE_OPEN"] = "Помилка відкриття файла #FILE_NAME# для запису.";
$MESS["CC_BSC1_TABLES_DROPPED"] = "Тимчасові таблиці видалені.";
$MESS["CC_BSC1_INDEX_CREATED"] = "Тимчасові таблиці проіндексовані.";
$MESS["CC_BSC1_TABLES_CREATED"] = "Тимчасові таблиці створені.";
$MESS["CC_BCI1_IBLOCK_TYPE"] = "Тип інфоблоку";
$MESS["CC_BSC1_PERMISSION_DENIED"] = "У вас немає прав для імпорту каталогу. Перевірте налаштування компоненту імпорту.";
$MESS["CC_BSC1_FILE_READ"] = "Файл імпорту прочитаний.";
$MESS["CC_BCI1_ELEMENT_ACTION"] = "Що робити з елементами, відсутніми у файлі імпорту";
$MESS["CC_BCI1_SECTION_ACTION"] = "Що робити з розділами, відсутніми у файлі імпорту";
?>