<?
$MESS["BPDT_BOOL_YES"] = "Oui";
$MESS["BPDT_BOOL_NO"] = "Non";
$MESS["BPDT_BOOL_NOT_SET"] = "Non installé";
$MESS["BPDT_BOOL_INVALID"] = "La valeur du champ n'est pas une valeur Oui/Non valide.";
?>