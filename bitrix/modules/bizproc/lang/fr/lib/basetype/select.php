<?
$MESS["BPDT_SELECT_INVALID"] = "L'élément sélectionné n'est pas un élément de liste.";
$MESS["BPDT_SELECT_OPTIONS1"] = "Précisez chaque variante sur une nouvelle ligne. Si la valeur de la variante et le nom de la variante sont différents, ajoutez la valeur entre crochets avant le nom. Par exemple : [v1]Variante 1";
$MESS["BPDT_SELECT_OPTIONS2"] = "Cliquez sur \"Définir\" une fois terminé";
$MESS["BPDT_SELECT_OPTIONS3"] = "Définir";
?>