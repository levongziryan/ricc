<?
$MESS["BPWTL_ERROR_MESSAGE_PREFIX"] = "Action '#TITLE#':";
$MESS["BPCGWTL_INVALID6"] = "Le paramètre '# NAME#' du type 'Utilisateur' nécessite l'installation du module pour réseau social.";
$MESS["BPCGWTL_INVALID5"] = "La valeur de la date dans le champ '#NAME#' ne correspond pas au format '#FORMAT#'.";
$MESS["BPCGWTL_INVALID3"] = "La valeur du champ '#NAME#' n'est pas correcte.";
$MESS["BPCGWTL_INVALID4"] = "La valeur du champ '#NAME#' ne peut pas être interprétée comme 'oui' ou 'non'.";
$MESS["BPCGWTL_INVALID2"] = "La valeur du champ '#NAME# 'est pas un nombre valide.";
$MESS["BPCGWTL_INVALID1"] = "La valeur du champ '#NAME#' n'est pas un nombre entier.";
$MESS["BPCGWTL_UNKNOWN_ERROR"] = "Erreur inconnue.";
$MESS["BPCGWTL_WRONG_TEMPLATE"] = "Modèle du processus d'affaires incorrect";
$MESS["BPCGWTL_INVALID8"] = "Le champ '#NAME#' doit être obligatoirement rempli";
$MESS["BPCGWTL_INVALID7"] = "Type du paramètre '#NAME# 'n'est pas défini.";
$MESS["BPCGWTL_CANT_DELETE"] = "Le modèle de procédure d'entreprise ne peut pas être supprimé, puisqu'il y a déjà des procédures d'entreprise lancées sous ce modèle.";
$MESS["BPCGWTL_EMPTY_TEMPLATE"] = "Le modèle de procédure d'entreprise ayant pour code '#ID#' a un corps vide.";
$MESS["BPCGWTL_INVALID_WF_ID"] = "Le modèle du procédure d'entreprise avec le code '#ID#' est introuvable.";
?>