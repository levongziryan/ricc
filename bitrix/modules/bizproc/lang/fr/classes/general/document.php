<?
$MESS["BPCGDOC_INVALID_WF"] = "Le processus d'affaires est lancé, mais pour un autre document.";
$MESS["BPCGDOC_YES"] = "Oui";
$MESS["BPCGDOC_ADD"] = "Ajouter";
$MESS["BPCGDOC_WAIT"] = "attendez...";
$MESS["BPCGDOC_AUTO_EXECUTE_EDIT"] = "Mettre à jour";
$MESS["BPCGDOC_EMPTY_WD_ID"] = "Le code du modèle du processus métier n'est pas indiqué.";
$MESS["BPCGDOC_AUTO_EXECUTE_NONE"] = "Non";
$MESS["BPCGDOC_NO"] = "Non";
$MESS["BPCGDOC_AUTO_EXECUTE_CREATE"] = "Ajouter";
$MESS["BPCGDOC_INVALID_TYPE"] = "Le type de paramètre n'a pas été défini.";
$MESS["BPCGDOC_AUTO_EXECUTE_DELETE"] = "Supprimer";
$MESS["BPCGDOC_INVALID_WF_ID"] = "Le modèle de procédure d'entreprise ayant pour code #ID# est introuvable.";
$MESS["BPCGDOC_ERROR_DELEGATE"] = "La tâche '#NAME#' ne peut être déléguée, car l'employé sélectionné est déjà affecté à la tâche.";
$MESS["BPCGDOC_ERROR_ACTION"] = "Tâche '#NAME#': #ERROR#";
$MESS["BPCGDOC_ERROR_TASK_IS_NOT_INLINE"] = "La tâche '#NAME#' ne peut pas être exécuté de cette manière.";
$MESS["BPCGDOC_WI_LOCKED_NOTICE_TITLE"] = "Assistant du portail";
$MESS["BPCGDOC_WI_LOCKED_NOTICE_MESSAGE"] = "Certains flux de travail que vous avez démarrés (#CNT#) ont renvoyé une erreur ou se sont terminés incorrectement.
[URL=#PATH#]Afficher ces flux de travail[/URL].";
$MESS["BPCGDOC_DELEGATE_LOG_TITLE"] = "Délégué";
$MESS["BPCGDOC_DELEGATE_LOG"] = "L'utilisateur #FROM# a délégué la tâche \"#NAME#\" à #TO#";
$MESS["BPCGDOC_WI_B24_LIMITS_MESSAGE"] = "Certaines des entités sur votre portail exécutent plus de deux flux de travail simultanés. Une restriction de maximum deux flux de travail simultanés par entité sera bientôt introduite. Il est recommandé de modifier la logique de vos flux de travail. [URL=https://helpdesk.bitrix24.com/open/4838471/]En savoir plus[/URL]";
$MESS["BPCGDOC_DELEGATE_NOTIFY_TEXT"] = "Une tâche du flux de travail vous a été déléguée : [URL=#TASK_URL#]#TASK_NAME#[/URL]";
$MESS["BPCGDOC_ERROR_DELEGATE_0"] = "La tâche \"#NAME#\" ne peut pas être déléguée, car l'employé sélectionné n'est pas votre subordonné.";
$MESS["BPCGDOC_ERROR_DELEGATE_1"] = "La tâche \"#NAME#\" ne peut pas être déléguée, car l'utilisateur sélectionné ne travaille pas pour cette société.";
$MESS["BPCGDOC_ERROR_DELEGATE_2"] = "La tâche \"#NAME#\" ne peut pas être déléguée, car la délégation est interdite dans les préférences de la tâche.";
?>