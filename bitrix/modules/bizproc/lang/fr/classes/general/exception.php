<?
$MESS["BPCGERR_INVALID_TYPE1"] = "L'argument '#PARAM#' doit avoir un type '#VALUE#'";
$MESS["BPCGERR_INVALID_ARG"] = "L'argument '#PARAM#' a une valeur inadmissible.";
$MESS["BPCGERR_INVALID_ARG1"] = "L'argument '#PARAM#' a une valeur inadmissible '#VALUE#'";
$MESS["BPCGERR_INVALID_TYPE"] = "Argument '#PARAM#' a un type invalide.";
$MESS["BPCGERR_NULL_ARG"] = "L'argument '#PARAM#' n'est pas déterminé.";
?>