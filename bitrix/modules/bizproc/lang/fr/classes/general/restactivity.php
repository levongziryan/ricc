<?
$MESS["BPRA_DEFAULT_STATUS_MESSAGE"] = "En attente d'une réponse de l'application";
$MESS["BPRA_DEFAULT_LOG_MESSAGE"] = "Réponse de l'application reçue";
$MESS["BPRA_PD_USER_ID"] = "Exécuté en tant que";
$MESS["BPRA_PD_SET_STATUS_MESSAGE"] = "Écrire un message de statut";
$MESS["BPRA_PD_STATUS_MESSAGE"] = "Message de statut";
$MESS["BPRA_PD_YES"] = "Oui";
$MESS["BPRA_PD_NO"] = "Non";
$MESS["BPRA_PD_USE_SUBSCRIPTION"] = "En attente d'une réponse";
$MESS["BPRA_PD_TIMEOUT_DURATION"] = "Délai d'attente dépassé";
$MESS["BPRA_PD_TIMEOUT_DURATION_HINT"] = "Temps d'attente d'une réponse de l'application";
$MESS["BPRA_PD_TIME_D"] = "jours";
$MESS["BPRA_PD_TIME_H"] = "heures";
$MESS["BPRA_PD_TIME_M"] = "minutes";
$MESS["BPRA_PD_TIME_S"] = "secondes";
$MESS["BPRA_PD_ERROR_EMPTY_PROPERTY"] = "Ce champ est obligatoire : #NAME#";
$MESS["BPRA_PD_TIMEOUT_LIMIT"] = "Temps d'attente minimum";
$MESS["BPRA_NOT_FOUND_ERROR"] = "L'activité d'application n'est pas installée.";
?>