<?
$MESS["BPT_SYNC_DESC"] = "Permet de publier l'élément du bloc d'informations sur les sites liés. Le document est précédemment publié sur le site du contrôleur.";
$MESS["BPT_SYNC_SEQ"] = "Processus d'affaires consécutif";
$MESS["BPT_SYNC_PUBLISH"] = "Publication du document";
$MESS["BPT_SYNC_NAME"] = "Publication sur les sites connectés";
$MESS["BPT_SYNC_SYNC"] = "Publication sur les sites connectés";
?>