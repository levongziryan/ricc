<?
$MESS["BPT_SM_APPROVE_DESC"] = "Vous devez voter pour le document' '{=Document:NAME}'.

Auteur: {=Document:CREATED_BY_PRINTABLE}'";
$MESS["BPT_SM_TASK1_TEXT"] = "Vous devez approuver ou rejeter le document '{=Document:NAME}'.

Pour l'approbation du document cliquez http://#HTTP_HOST##TASK_URL#

Auteur: {=Document:CREATED_BY_PRINTABLE}";
$MESS["BPT_SM_APPROVE_TITLE"] = "Vote pour le document";
$MESS["BPT_SM_MAIL2_SUBJ"] = "Vote pour '{= Document: NAME}: Document refusé.";
$MESS["BPT_SM_MAIL1_SUBJ"] = "Vote par '{=Document:NAME}: Le document est accepté.";
$MESS["BPT_SM_MAIL2_TEXT"] = "Un vote pour le document '{=Document:NAME}' est achevé.

Le document est rejété.

Le document a été validé par: {=ApproveActivity1:ApprovedCount}
Le document a été rejeté par: {=ApproveActivity1:NotApprovedCount}";
$MESS["BPT_SM_MAIL1_TEXT"] = "Le vote du document '{=Document:NAME}' est achevé.

Le document est adopté par {=ApproveActivity1:ApprovedPercent} % de voix.


Ont approuvé le document: {=ApproveActivity1:ApprovedCount}
Ont décliné le document: {=ApproveActivity1:NotApprovedCount}";
$MESS["BPT_SM_PARAM_NAME"] = "Les votants";
$MESS["BPT_SM_MAIL2_TITLE"] = "Le document est rejeté.";
$MESS["BPT_SM_MAIL1_TITLE"] = "Le document est accepté";
$MESS["BPT_SM_TASK1_TITLE"] = "Approuver le document: '{= Document: NAME}'";
$MESS["BPT_SM_MAIL2_STATUS"] = "Refusé";
$MESS["BPT_SM_ACT_NAME"] = "Ordre de succession d'actions";
$MESS["BPT_SM_TITLE1"] = "Processus d'affaires consécutif";
$MESS["BPT_SM_ACT_TITLE"] = "Message e-mail";
$MESS["BPT_SM_APPROVE_NAME"] = "Votez pour le document, s'il vous plaît!";
$MESS["BPT_SM_NAME"] = "Approbation simple/Vote";
$MESS["BPT_SM_PUB"] = "Publication du document";
$MESS["BPT_SM_DESC"] = "Conseillé pour les situations où vous devez prendre la décision par une simple majorité de voix. Dans ce cas-là vous pouvez inclure dans la liste des électeurs des bons collaborateurs, pour permettre commentaire votre décision aux personnes votées. A la fin de vote vous communiquez à tous les participants votre décision.";
$MESS["BPT_SM_PARAM_DESC"] = "Liste d'utilisateurs participant au vote.";
$MESS["BPT_SM_MAIL2_STATUS2"] = "Statut: Rejeté";
$MESS["BPT_SM_STATUS2"] = "Statut: Validé";
$MESS["BPT_SM_STATUS"] = "Approuvé";
?>