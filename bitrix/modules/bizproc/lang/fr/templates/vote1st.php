<?
$MESS["BP_V1ST_TASK_DESC"] = "Vous devez voter pour le document' '{=Document:NAME}'.

Auteur: {=Document:CREATED_BY_PRINTABLE}'";
$MESS["BP_V1ST_TASK_TEXT"] = "Vous devez approuver ou rejeter le document '{=Document:NAME}'.

Pour l'approbation du document cliquez http://#HTTP_HOST##TASK_URL#

Auteur: {=Document:CREATED_BY_PRINTABLE}";
$MESS["BP_V1ST_VNAME"] = "Vote pour le document";
$MESS["BP_V1ST_PARAM1"] = "Les votants";
$MESS["BP_V1ST_TNA"] = "Le document est rejeté.";
$MESS["BP_V1ST_TASK_NAME"] = "Approuver le document: '{= Document: NAME}'";
$MESS["BP_V1ST_STAT_NA"] = "Refusé";
$MESS["BP_V1ST_S2"] = "Ordre de succession d'actions";
$MESS["BP_V1ST_SEQ"] = "Processus d'affaires consécutif";
$MESS["BP_V1ST_MAIL_NAME"] = "message e-mail";
$MESS["BP_V1ST_TASK_T"] = "Votez pour le document, s'il vous plaît!";
$MESS["BP_V1ST_T3"] = "Publication du document";
$MESS["BP_V1ST_DESC"] = "Conseillé pour les situations où un seul avis d'experts d'une communauté est suffisant. Dans le cadre de ce processus on définit la liste de collaborateurs qui ont le droit de prendre une décision dans l'avis sur le processus entamé. Le processus est considéré comme terminé avec le premier collaborateur qui a donné son avis.";
$MESS["BP_V1ST_PARAM1_DESC"] = "Liste d'utilisateurs participant au vote.";
$MESS["BP_V1ST_STAT_NA_T"] = "Statut: Rejeté";
$MESS["BP_V1ST_APPR_S"] = "Statut: Validé";
$MESS["BP_V1ST_APPR"] = "Approuvé";
$MESS["BP_V1ST_MAIL2_NA"] = "Validation {=Document:NAME}: Le document est rejeté.";
$MESS["BP_V1ST_MAIL_SUBJ"] = "Confirmation {=Document:NAME}: Document accepté.";
$MESS["BP_V1ST_MAIL2_NA_TEXT"] = "La validation du document '{=Document:NAME}' est achevée.

Document rejeté.
{=ApproveActivity1:Comments}";
$MESS["BP_V1ST_MAIL_TEXT"] = "Validation du document '=Document:NAME}' est terminée.

Le document est accepté.
{=ApproveActivity1:Comments}";
$MESS["BP_V1ST_NAME"] = "Confirmation par la première voix";
?>