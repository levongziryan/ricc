<?
$MESS["BPT_ST_AUTHOR"] = "Auteur;";
$MESS["BPT_ST_BP_NAME"] = "Processus business avec des statuts";
$MESS["BPT_ST_INIT"] = "Accès au statut";
$MESS["BPT_ST_INS"] = "Accès au statut";
$MESS["BPT_ST_APPROVE_DESC"] = "Vous devez valider le document '{=Document:NAME}':
 
Contenu du document:
{=Document:PREVIEW_TEXT}
 
Auteur:
{=Document:CREATED_BY_PRINTABLE}";
$MESS["BPT_ST_TEXT"] = "Vous devez valider ou rejeter le document '{=Document:NAME}'.
 
Pour valider le document suivez le lien http://#HTTP_HOST##TASK_URL#
 
Contenu du document:
{=Document:DETAIL_TEXT}
 
Auteur: {=Document:CREATED_BY_PRINTABLE}";
$MESS["BPT_ST_CREATORS"] = "Qui peut modifier le document";
$MESS["BPT_ST_APPROVERS"] = "Qui approuve le document";
$MESS["BPT_ST_SUBJECT"] = "Approuver le document: '{= Document: NAME}'";
$MESS["BPT_ST_F"] = "Enveloppe de l'événement";
$MESS["BPT_ST_TP"] = "Publié";
$MESS["BPT_ST_CMD_PUBLISH"] = "Publier";
$MESS["BPT_ST_CMD_APPR"] = "Envoyer pour l'approbation";
$MESS["BPT_ST_SEQ"] = "Séquence des activités";
$MESS["BPT_ST_MAIL_TITLE"] = "Message e-mail";
$MESS["BPT_ST_DESC"] = "Le document doit passer la chaîne d'approbation avec des statuts";
$MESS["BPT_ST_PUBDC"] = "Publication du document";
$MESS["BPT_ST_SAVEH"] = "Sauvegarde de l'historique";
$MESS["BPT_ST_SETSTATE"] = "Etablir le statut";
$MESS["BPT_ST_SET_PUB"] = "Mettre le statut Publication";
$MESS["BPT_ST_SET_DRAFT"] = "Tablir le statut Brouillon";
$MESS["BPT_ST_CMD_APP"] = "Approbation";
$MESS["BPT_ST_APPROVE_TITLE"] = "Approbation du document";
$MESS["BPT_ST_APPROVE_NAME"] = "Approuver le document '{= Document: NAME}'";
$MESS["BPT_ST_NAME"] = "Approuver le document avec les Etats";
$MESS["BPT_ST_ST_DRAFT"] = "Brouillon";
?>