<?
$MESS["BP_EXPR_APP2_DESC"] = "Tous les employés concernés ont accédé au document et ont laissé leurs avis.                          
                          
{=ApproveActivity1:Comments}                          
                          
Maintenant il vous faut valider le document.";
$MESS["BP_EXPR_MAIL2_TEXT"] = "Tous les employés assignés ont consulté le document et ont donné leurs avis.
Maintenant il faut valider le document.

Pour la validation du document passez par ce lien: http://#HTTP_HOST##TASK_URL#

{=ApproveActivity1:Comments}";
$MESS["BP_EXPR_APPR1_DESC"] = "Le traitement du document '{=Document:NAME}' nécessite votre avis.";
$MESS["BP_EXPR_TASK1_MAIL"] = "Pour traiter le document '{=Document:NAME}' il faut connaître votre opinion sur lui.

Pour valider le document, cliquez sur le lien http://#HTTP_HOST##TASK_URL#";
$MESS["BP_EXPR_TASK1"] = "Il est nécessaire d'analyser le document '{=Document:NAME}'.";
$MESS["BP_EXPR_APPR1"] = "Il est nécessaire d'analyser le document '{=Document:NAME}'.";
$MESS["BP_EXPR_MAIL2_SUBJ"] = "Approuver le document: '{= Document: NAME}'";
$MESS["BP_EXPR_APP2_TEXT"] = "Approuver le document: '{= Document: NAME}'";
$MESS["BP_EXPR_NA"] = "Refusé";
$MESS["BP_EXPR_ST"] = "Ordre de succession d'actions";
$MESS["BP_EXPR_S"] = "Processus d'affaires consécutif";
$MESS["BP_EXPR_M"] = "message e-mail";
$MESS["BP_EXPR_PUB"] = "Publication du document";
$MESS["BP_EXPR_DESC"] = "Cela est recommandé dans les situations où le décideur a besoin, pour prendre une décision, d'une évaluation du document par des experts. Dans le cadre du processus, on désigne un groupe d'experts dont chacun peut dire son avis du document. Les avis sont transmis au décideur qui approuvera ou non le document.";
$MESS["BP_EXPR_NA_ST"] = "Statut: Rejeté";
$MESS["BP_EXPR_ST3_TIT"] = "Statut: Validé";
$MESS["BP_EXPR_PARAM1"] = "Confirme";
$MESS["BP_EXPR_ST3_T"] = "Approuvé";
$MESS["BP_EXPR_TAPP"] = "approuver le document";
$MESS["BP_EXPR_MAIL4_TEXT"] = "la validation du document '{=Document:NAME}' est achée, le document est rejeté.

{=ApproveActivity2:Comments}";
$MESS["BP_EXPR_MAIL3_TEXT"] = "L'approbation du document '{=Document:NAME}' est terminé, le document est approuvé.

{=ApproveActivity2:Comments}";
$MESS["BP_EXPR_MAIL4_SUBJ"] = "Confirmation du document '{=Document:NAME}': document est rejeté";
$MESS["BP_EXPR_MAIL3_SUBJ"] = "Approbation des '{=Document:NAME}': le document a passé.";
$MESS["BP_EXPR_PARAM2_DESC"] = "Groupe d'experts dont chacun des membres peut donner don avis sur le document.";
$MESS["BP_EXPR_NAME"] = "Evaluation experte";
$MESS["BP_EXPR_PARAM2"] = "Experts";
?>