<?
$MESS["BP_REVW_REVIEW_DESC"] = "Vous devez vous familiariser avec le document '{=Document:NAME}'.

Auteur: {=Document:CREATED_BY_PRINTABLE}'";
$MESS["BP_REVW_TASK_DESC"] = "Vous devez prendre connaissance du document '{=Document:NAME}'.

Pour prendre connaissance du document, suivez le lien: http://#HTTP_HOST##TASK_URL#

Auteur: {=Document:CREATED_BY_PRINTABLE}";
$MESS["BP_REVW_TASK"] = "Il faut étudier le document '{=Document:NAME}'";
$MESS["BP_REVW_PARAM1"] = "Faire savoir";
$MESS["BP_REVW_TITLE"] = "Lecture du document";
$MESS["BP_REVW_MAIL_SUBJ"] = "La revue du document '{=Document:NAME}' est achevée.";
$MESS["BP_REVW_MAIL_TEXT"] = "La revue du document '{=Document:NAME}' est achevée.";
$MESS["BP_REVW_REVIEW"] = "Faites connaissance du document {=Document:NAME}";
$MESS["BP_REVW_T"] = "Processus d'affaires consécutif";
$MESS["BP_REVW_MAIL"] = "message e-mail";
$MESS["BP_REVW_DESC"] = "Destiné aux situations lorsqu'un document doit être vu par un groupe précis d'utilisateurs. Dans le cadre de la procédure, les d'utilisateurs devant être informés sont sélectionnés. Leur donne le droit de publier des commentaires.";
?>