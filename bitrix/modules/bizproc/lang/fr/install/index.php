<?
$MESS["BIZPROC_INSTALL_NAME"] = "De processus business";
$MESS["BIZPROC_PHP_L439"] = "Vous utilisez la version PHP #VERS#, pour fonctionner le module a besoin d'une version 5.0.0 ou plus récente. Veuillez mettre à jour PHP ou contactez le service d'appui technique de votre hébergement.";
$MESS["BIZPROC_PERM_D"] = "accès refusé";
$MESS["BIZPROC_PERM_W"] = "note";
$MESS["BIZPROC_INSTALL_DESCRIPTION"] = "Le module pur créer et travailler avec des workflows.";
$MESS["BIZPROC_INSTALL_TITLE"] = "Installation de la solution";
$MESS["BIZPROC_BIZPROCDESIGNER_INSTALLED"] = "L'appel fait à votre nouveau numéro de téléphone entrant pour fournir le code de confirmation est un appel payé. Votre compte sera facturé selon le tarif pour un appel sortant pour cet appel.";
$MESS["BIZPROC_PERM_R"] = "lecture";
?>