<?
$MESS["BIZPROC_JS_BP_STARTER_REQUEST_FAILURE"] = "Erreur lors de l'exécution de la demande. Essayez d'actualiser la page.";
$MESS["BIZPROC_JS_BP_STARTER_CANCEL"] = "Annuler";
$MESS["BIZPROC_JS_BP_STARTER_START"] = "Résumé du temps de travail";
$MESS["BIZPROC_JS_BP_STARTER_SAVE"] = "Enregistrer";
$MESS["BIZPROC_JS_BP_STARTER_AUTOSTART"] = "Exécuter automatiquement les flux de travail";
$MESS["BIZPROC_JS_BP_STARTER_NO_TEMPLATES"] = "Il n’y a aucun modèle de flux de travail";
$MESS["BIZPROC_JS_BP_STARTER_DESTINATION_CHOOSE"] = "sélectionner";
$MESS["BIZPROC_JS_BP_STARTER_DESTINATION_EDIT"] = "éditer";
$MESS["BIZPROC_JS_BP_STARTER_FILE_CHOOSE"] = "Sélectionnez un fichier";
$MESS["BIZPROC_JS_BP_STARTER_CONTROL_CLONE"] = "ajouter";
?>