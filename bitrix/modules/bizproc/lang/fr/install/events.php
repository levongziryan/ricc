<?
$MESS["BIZPROC_MAIL_TEMPLATE_DESC"] = "#SENDER# - Expéditeur du message
#RECEIVER# - Destinataire du message
#TITLE# - Intitulé du message
#MESSAGE# - Message";
$MESS["BIZPROC_MAIL_TEMPLATE_NAME"] = "Message de l'action d'envoyer du courrier";
$MESS["BIZPROC_HTML_MAIL_TEMPLATE_NAME"] = "Notification de l'action d'envoi d'e-mail (HTML)";
?>