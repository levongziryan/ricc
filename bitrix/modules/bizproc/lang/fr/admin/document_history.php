<?
$MESS["BPADH_AUTHOR"] = "Auteur";
$MESS["BPADH_F_AUTHOR"] = "Auteur";
$MESS["BPADH_RECOVERY_DOC"] = "Récupérer le document";
$MESS["BPADH_RECOVERY_DOC_CONFIRM"] = "tes-vous sûr de souhaiter récupérer le document à partir de cette entrée?";
$MESS["BPADH_DELETE_DOC_CONFIRM"] = "tes-vous sûr de vouloir supprimer cette inscription?";
$MESS["BPADH_RECOVERY_SUCCESS"] = "Entrée restaurée avec succès à partir de l'historique.";
$MESS["BPADH_F_MODIFIED"] = "Modifié";
$MESS["BPADH_MODIFIED"] = "Modifié";
$MESS["BPADH_TITLE"] = "Historique des documents";
$MESS["BPADH_F_AUTHOR_ANY"] = "aléatoire";
$MESS["BPADH_NAME"] = "Dénomination";
$MESS["BPADH_RECOVERY_ERROR"] = "Chec de restauration de l'inscription de l'historique.";
$MESS["BPADH_NO_DOC_ID"] = "Le code du document n'est pas indiqué.";
$MESS["BPADH_NO_ENTITY"] = "Entité du document non définie.";
$MESS["BPADH_ERROR"] = "Erreur";
$MESS["BPADH_VIEW_DOC"] = "Accéder au document";
$MESS["BPADH_USER_PROFILE"] = "Profil de l'utilisateur";
$MESS["BPADH_NO_PERMS"] = "Vous n'avez pas de droits d'accès à l'historique de ce document.";
$MESS["BPADH_DELETE_DOC"] = "Supprimer l'Enregistrement";
?>