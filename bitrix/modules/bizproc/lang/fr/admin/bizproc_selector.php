<?
$MESS["BIZPROC_SEL_TITLEBAR"] = "Ajouter des champs";
$MESS["BIZPROC_SEL_CANCEL"] = "Annuler";
$MESS["BIZPROC_SEL_FIELDS_TAB"] = "Champs du document";
$MESS["BIZPROC_SEL_TITLEBAR_DESC"] = "Veuillez choisir le champ nécessaire par un double clic ou cliquez sur &quot;Insérer&quot;.";
$MESS["BIZPROC_SEL_INSERT"] = "Insérer";
$MESS["BIZPROC_SEL_TITLE"] = "Sélectionnez un champ";
$MESS["BIZPROC_SEL_PARAMS_TAB"] = "Paramètres de modèle";
$MESS["BIZPROC_SEL_USERS_TAB"] = "Utilisateurs";
$MESS["BIZPROC_SEL_ERR"] = "Vous devez sélectionner un champ pour y insérer une des listes!";
$MESS["BP_SEL_VARS"] = "Variables";
$MESS["BP_SEL_ADDIT"] = "Résultats supplémentaires";
$MESS["BIZPROC_AS_TITLE_TOOLBAR"] = "Insertion de la valeur";
$MESS["BIZPROC_AS_TITLE"] = "Insertion de la valeur";
$MESS["BP_SEL_CONSTANTS"] = "Constantes";
$MESS["BIZPROC_SEL_USERS_TAB_GROUPS"] = "- Groupes";
$MESS["BIZPROC_SEL_USERS_TAB_USERS"] = "- Utilisateurs";
$MESS["BIZPROC_SEL_GROUPS_TAB"] = "Catégories d'utilisateurs";
?>