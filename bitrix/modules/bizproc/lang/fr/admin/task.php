<?
$MESS["BPAT_BACK"] = "Précédent";
$MESS["BPAT_GOTO_DOC"] = "Accéder au document";
$MESS["BPAT_TAB"] = "La tâche";
$MESS["BPAT_TAB_TITLE"] = "La tâche";
$MESS["BPAT_TITLE"] = "Tâche #ID#";
$MESS["BPAT_DESCR"] = "Description de la tâche";
$MESS["BPAT_NAME"] = "Dénomination de la tâche";
$MESS["BPAT_NO_TASK"] = "La tâche n'est pas trouvée.";
$MESS["BPAT_USER"] = "Utilisateur";
$MESS["BPAT_USER_NOT_FOUND"] = "(introuvable)";
$MESS["BPAT_NO_STATE"] = "Le processus d'entreprise est introuvable.";
$MESS["BPAT_ACTION_DELEGATE"] = "Déléguer";
?>