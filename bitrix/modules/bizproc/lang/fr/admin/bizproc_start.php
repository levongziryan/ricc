<?
$MESS["BPABS_TAB"] = "les processus d'affaires";
$MESS["BPABS_TAB1"] = "les processus d'affaires";
$MESS["BPABS_MESSAGE_ERROR"] = "La procédure d'entreprise de type '#TEMPLATE#' n'a pas été lancée.";
$MESS["BPABS_MESSAGE_SUCCESS"] = "Le workflow de type '#TEMPLATE#' a été lancée avec succès.";
$MESS["BPABS_BACK"] = "Précédent";
$MESS["BPABS_TAB1_TITLE"] = "Sélectionnez un modèle d'un processus d'affaires pour démarrer.";
$MESS["BPABS_YES"] = "Oui";
$MESS["BPABS_NO_TEMPLATES"] = "Il n'y a aucun modèle de processus d'affaire.";
$MESS["BPABS_ADD"] = "Ajouter";
$MESS["BPABS_WAIT"] = "veuillez patienter...";
$MESS["BPABS_TAB_TITLE"] = "Définissez les paramètres de démarrage du processus d'affaires";
$MESS["BPABS_TITLE"] = "Démarrer un processus business";
$MESS["BPABS_DO_START"] = "Commencer";
$MESS["BPABS_NAME"] = "Nom du modèle de procédure d'entreprise";
$MESS["BPABS_EMPTY_DOC_ID"] = "Aucun document ID pour lequel la procédure d'entreprise doit être créée n'a été spécifiée.";
$MESS["BPABS_EMPTY_DOC_TYPE"] = "Le type de document n'a pas été indiqué.";
$MESS["BPABS_EMPTY_ENTITY"] = "Aucune entité pour laquelle la procédure d'entreprise doit être crée n'a été spécifiée.";
$MESS["BPABS_NO"] = "Non";
$MESS["BPABS_DESCRIPTION"] = "Description du modèle de procédure d'entreprise";
$MESS["BPABS_DO_CANCEL"] = "Annuler";
$MESS["BPABS_ERROR"] = "Erreur";
$MESS["BPABS_INVALID_TYPE"] = "Le type de paramètre n'a pas été défini.";
$MESS["BPABS_NO_PERMS"] = "Accès interdit.";
?>