<?
$MESS["BPT_BT_PARAM_BOOK"] = "Service de comptabilité";
$MESS["BPT_BT_RIA1_DESCR"] = "Rapport post-Voyage. Fournir un rapport de dépenses à la comptabilité avec les reçus originaux. Rédigez un résumé dans l'espace prévu ci-dessous et inclure un lien vers votre rapport complet, le cas échéant.";
$MESS["BPT_BT_SNMA1_TEXT"] = "Votre déplacement a été validé par votre supérieur.";
$MESS["BPT_BT_SNMA2_TEXT"] = "Votre déplacement professionnel n'a pas été validé par votre supérieur.";
$MESS["BPT_BT_T_CITY"] = "Ville de résidence préférée";
$MESS["BPT_BT_DF_CITY"] = "Ville de résidence préférée";
$MESS["BPT_BT_RIA1_DATE_END_REAL"] = "Date de fin du déplacement professionnel";
$MESS["BPT_BT_T_DATE_START"] = "Date du début";
$MESS["BPT_BT_T_DATE_END"] = "Date d'achèvement prévue";
$MESS["BPT_BT_RA2_NAME"] = "Documents pour le déplacement professionnel";
$MESS["BPT_BT_RA2_TITLE1"] = "Documents pour le déplacement professionnel";
$MESS["BPT_BT_SSTA4_STATE_TITLE"] = "Achevé(e)s";
$MESS["BPT_BT_RA2_STATUS_MESSAGE"] = "Instructions pour le collaborateur en mission";
$MESS["BPT_BT_AA7_TITLE"] = "Tableau des Absences";
$MESS["BPT_BT_P_TARGET"] = "Employé";
$MESS["BPT_BT_RA3_DESCR"] = "Rapport de Voyage à partir de {=Template:TargetUser_printable}

Dates prévues: {=Template:date_start} - {=Template:date_end}
Jours de voyage réels: {=Variable:date_end_real}

Dépenses:
{=Variable:expenditures_real}

Objectif:
{=Template:purpose}

Post-Voyage Rapport:
{=Variable:report}";
$MESS["BPT_BT_AA7_NAME"] = "Déplacement professionnel";
$MESS["BPT_BT_AA7_STATE"] = "Déplacement professionnel";
$MESS["BPT_BT_SFA1_NAME"] = "Mission d'affaires {=Template:TargetUser_printable}, {=Template:country}-{=Template:city}";
$MESS["BPT_BT_RA1_TBM"] = "Déplacement enregistré";
$MESS["BPT_BT_AA7_DESCR"] = "Mission avec le but: {=Template:purpose}";
$MESS["BPT_TTITLE"] = "Déplacements";
$MESS["BPT_BT_T_TICKETS"] = "Copies des billets";
$MESS["BPT_BT_DF_TICKETS"] = "Copies des billets";
$MESS["BPT_BT_RA4_DESCR"] = "Il est nécessaire de clôturer le déplacement terminé. La Direction a examiné le rapport sur le déplacement.

Collaborateur en déplacement: {=Template:TargetUser_printable}

Période de déplacement: {=Template:date_start} - {=Variable:date_end_real}

Frais réels:
{=Variable:expenditures_real}

Rapport sur le déplacement:
{=Variable:report}";
$MESS["BPT_BT_RA1_DESCR"] = "Il est nécessaire de préparer les documents pour une mission sanctionnée par le directeur.

employé: {=Template:TargetUser_printable}

Pays de séjour: {=Template:country}
Ville de séjour: {=Template:city}
Durée de la mission planifiée: {=Template:date_start} - {=Template:date_end}
Dépenses envisagées: {=Template:expenditures}

Objectif de la mission:
{=Template:purpose}

Billets:
{=Template:tickets_printable}";
$MESS["BPT_BT_AA1_DESCR"] = "Il faut valider la mission de l'employé {=Template:TargetUser_printable}

Pays de séjour préférentiel: {=Template:country}
Ville de séjour préférentiel: {=Template:city}
Période de la mission: {=Template:date_start} - {=Template:date_end}
Dépenses prévues: {=Template:expenditures}

But de la mission:
{=Template:purpose}";
$MESS["BPT_BT_AA1_STATUS_MESSAGE"] = "Attente de l'approbation par les chefs";
$MESS["BPT_BT_RA2_TBM"] = "Vu(e)/Accepté(e)";
$MESS["BPT_BT_RA3_TBM"] = "Vu(e)/Accepté(e)";
$MESS["BPT_BT_RA2_TITLE"] = "Communication par les administration";
$MESS["BPT_BT_RA3_STATUS_MESSAGE"] = "Mise au courant de l'administration avec le rapport";
$MESS["BPT_BT_RA3_NAME"] = "Examen du rapport sur le voyage {=Template:TargetUser_printable}";
$MESS["BPT_BT_SSTA5_STATE_TITLE"] = "Rejetée par le chef";
$MESS["BPT_BT_RIA1_REPORT"] = "Le rapport sur la mission";
$MESS["BPT_BT_RIA1_TITLE"] = "Le rapport sur la mission";
$MESS["BPT_BT_RIA1_NAME"] = "Rapport sur déplacement (à compléter après le déplacement)";
$MESS["BPT_BT_SSTA3_STATE_TITLE"] = "Le rapport sur la mission";
$MESS["BPT_BT_RA4_NAME"] = "Rapport sur le déplacement professionnel {=Template:TargetUser_printable}";
$MESS["BPT_BT_RA1_STATUS_MESSAGE"] = "Etablissement dans le service de comptabilité";
$MESS["BPT_BT_RA1_TITLE"] = "Etablissement dans le service de comptabilité";
$MESS["BPT_BT_RA4_STATUS_MESSAGE"] = "Etablissement dans le service de comptabilité";
$MESS["BPT_BT_RA4_TITLE"] = "Etablissement dans le service de comptabilité";
$MESS["BPT_BT_SSTA2_STATE_TITLE"] = "Formalisation du déplacement professionnel";
$MESS["BPT_BT_RA1_NAME"] = "Délivrance d'une mission {=Template:TargetUser_printable}, {=Template:country}-{=Template:city}";
$MESS["BPT_BT_RA4_TMB"] = "Enregistré";
$MESS["BPT_BT_RA2_DESCR"] = "Avant de partir pour un voyage d'affaires:

1. [url={=Variable:ParameterForm1}]Download[/url] a forme de Frais de Déplacement.

Ce formulaire doit être rempli à la fin de votre voyage et donnée à la Direction de la Comptabilité.

2.Rappelez-vous que tout paiement survenant pendant votre voyage d'affaires doit être confirmée par un projet de loi ou de la réception correspondante.";
$MESS["BPT_BT_T_EXP"] = "Frais planifiés";
$MESS["BPT_BT_SA1_TITLE"] = "Ordre de succession d'actions";
$MESS["BPT_BT_SA3_TITLE"] = "Ordre de succession d'actions";
$MESS["BPT_BT_SWA"] = "Processus d'affaires consécutif";
$MESS["BPT_BT_STA1_STATE_TITLE"] = "Projet";
$MESS["BPT_BT_AA7_FSTATE"] = "Fonctionne";
$MESS["BPT_BT_RIA1_EXP_REAL"] = "Dépense";
$MESS["BPT_BT_T_COUNTRY_DEF"] = "USA";
$MESS["BPT_BT_SNMA1_TITLE"] = "Message du réseau social";
$MESS["BPT_BT_SNMA2_TITLE"] = "Message du réseau social";
$MESS["BPT_BT_PARAM_BOSS"] = "Collaborateurs qui approuvent les déplacements";
$MESS["BPT_BT_PARAM_OP_READ"] = "Employés ayant le droit de Voir Tous les Processus business";
$MESS["BPT_BT_PARAM_OP_CREATE"] = "Collaborateurs qui ont le droit de Créer de Nouveaux Processus";
$MESS["BPT_BT_PARAM_OP_ADMIN"] = "Employés ayant le droit de Gestion des Processus";
$MESS["BPT_BT_SFA2_TITLE"] = "Enregistrer le rapport";
$MESS["BPT_BT_SFA1_TITLE"] = "Enregistrer, envoyer et aller à la liste";
$MESS["BPT_BT_PARAM_FORM1"] = "Lien sur le formulaire 'Certificat de déplacement'";
$MESS["BPT_BT_PARAM_FORM2"] = "Lien au formulaire 'Tâche de service'";
$MESS["BPT_BT_T_COUNTRY"] = "Pays de séjour";
$MESS["BPT_BT_DF_COUNTRY"] = "Pays de séjour";
$MESS["BPT_BT_STA1_TITLE"] = "Configurer le texte du statut";
$MESS["BPT_BT_SSTA3_TITLE"] = "Configurer le texte du statut";
$MESS["BPT_BT_SSTA4_TITLE"] = "Configurer le texte du statut";
$MESS["BPT_BT_SSTA5_TITLE"] = "Installation du statut: rejeté";
$MESS["BPT_BT_SSTA2_TITLE"] = "Installation du statut: formalités";
$MESS["BPT_BT_AA1_NAME"] = "Approuver voyage d'affaires  {=Template:TargetUser_printable},{=Template:COUNTRY} - {=Template:CITY}";
$MESS["BPT_BT_AA1_TITLE"] = "Approbation du déplacement par le responsable";
$MESS["BPT_BT_DF_DATE_END_REAL"] = "Date réelle d'achèvement";
$MESS["BPT_BT_DF_EXP_REAL"] = "Dépense";
$MESS["BPT_BT_T_PURPOSE"] = "But du déplacement d'affaires";
?>