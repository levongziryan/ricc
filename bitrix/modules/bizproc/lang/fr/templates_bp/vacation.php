<?
$MESS["BPT1_BTF_P_APPS"] = "[x]Non coordonné
[y]Coordonné
[n]Refusé";
$MESS["BPT1_BT_PARAM_BOOK"] = "Service de comptabilité";
$MESS["BPT1_BT_SNMA18_TEXT"] = "Vous avez été refusé sur votre congé. {=A94751_67978_49922_99999:Comments}";
$MESS["BPT1_BT_SNMA16_TEXT"] = "Votre congé est approuvé par la direction.";
$MESS["BPT1_BT_T_DATE_START"] = "Date de Début du congé";
$MESS["BPT1_BT_T_DATE_END"] = "Date de Fin du congé";
$MESS["BPT1_BT_RIA11_P2"] = "En supplément, coordonner avec";
$MESS["BPT1_BT_RIA11_TITLE"] = "Règlement supplémentaire";
$MESS["BPT1_BT_SFA12_TITLE"] = "Accéder à la page de la modification du document";
$MESS["BPT_BT_AA7_TITLE"] = "Tableau des Absences";
$MESS["BPT1_BT_P_TARGET"] = "Employé";
$MESS["BPT1_BT_IEBA2_V2"] = "Règlement supplémentaire n'est pas requis";
$MESS["BPT1_BT_SSTA14_ST"] = "Refusé";
$MESS["BPT1_TTITLE"] = "Congé";
$MESS["BPT_BT_AA7_NAME"] = "Congé";
$MESS["BPT_BT_AA7_STATE"] = "Congé";
$MESS["BPT_BT_AA7_DESCR"] = "Congé annuel";
$MESS["BPT1_BT_IEBA15_V2"] = "Congé non accordé";
$MESS["BPT1_BT_SSTA18_ST"] = "Le congé payé est enregistré.";
$MESS["BPT1_BT_IEBA15_V1"] = "Congé approuvé.";
$MESS["BPT1_BT_SFTA12_ST"] = "Le congé a été approuvé par l'administration.";
$MESS["BPT1_BT_RA17_DESCR"] = "Le chef a approuvé le congé. Vous devez rédiger l'ordre correspondant et verser l'indemnité de congés payés.

Employé: {=Template:TargetUser_printable}
Date de début: {=Template:date_start}
Date de fin: {=Template:date_end}";
$MESS["BPT1_BT_RIA11_DESCR"] = "Congé approuvé. Faut-il une validation supplémentaire?

Employé: {=Template:TargetUser_printable}
Date du début: {=Template:date_start}
Date de la fin: {=Template:date_end}";
$MESS["BPT1_BT_RA17_STATUS_MESSAGE"] = "Etablissement dans le service de comptabilité";
$MESS["BPT1_BT_RA17_TITLE"] = "Etablissement dans le service de comptabilité";
$MESS["BPT1_BT_RA17_TBM"] = "Présentation achevée.";
$MESS["BPT1_BT_RA17_NAME"] = "Création d'un congé {=Template:TargetUser_printable}, {=Template:date_start} - {=Template:date_end}";
$MESS["BPT1_BT_SA1_TITLE"] = "Ordre de succession d'actions";
$MESS["BPT1_BT_SWA"] = "Processus d'affaires consécutif";
$MESS["BPT_BT_AA7_FSTATE"] = "Fonctionne";
$MESS["BPT1_BT_STA1_STATE_TITLE"] = "Approbation";
$MESS["BPT1_BTF_P_APP"] = "Approbation";
$MESS["BPT1_BT_AA11_STATUS_MESSAGE"] = "Concertation avec la Direction";
$MESS["BPT1_BT_AA11_NAME"] = "Approuver {=Template:TargetUser_printable}' congé de {=Template:date_start} à {=Template:date_end}";
$MESS["BPT1_BT_SNMA16_TITLE"] = "Message du réseau social";
$MESS["BPT1_BT_SNMA18_TITLE"] = "Message du réseau social";
$MESS["BPT1_BT_PARAM_BOSS"] = "Employé(s) qui est (sont) en train d'obtenir une approbation pour son (leur) congé(s)";
$MESS["BPT1_BT_AA11_DESCR"] = "Collaborateur: {=Template:TargetUser_printable}
Date de début du congé: {=Template:date_start}
Date de fin du congé: {=Template:date_end}";
$MESS["BPT1_BT_PARAM_OP_READ"] = "Employés ayant le droit de Voir Tous les Processus business";
$MESS["BPT1_BT_PARAM_OP_CREATE"] = "Collaborateurs qui ont le droit de Créer de Nouveaux Processus";
$MESS["BPT1_BT_PARAM_OP_ADMIN"] = "Employés ayant le droit de Gestion des Processus";
$MESS["BPT1_BT_SFA1_TITLE"] = "Sauvegarde des paramètres du congé";
$MESS["BPT1_BT_IEBA1_V1"] = "Est-ce qu'il y a encore des approbations qu'il faut obtenir";
$MESS["BPT1_BT_RIA11_P1"] = "Est-ce qu'il y a encore des approbations qu'il faut obtenir";
$MESS["BPT1_BT_RIA11_NAME"] = "Faut-il encore valider le congé {=Template:TargetUser_printable} du {=Template:date_start} au {=Template:date_end}?";
$MESS["BPT1_BT_IF11_N"] = "Condition";
$MESS["BPT1_BT_STA1_TITLE"] = "Configurer le texte du statut";
$MESS["BPT1_BT_SFTA12_T"] = "Configurer le texte du statut";
$MESS["BPT1_BT_SSTA14_T"] = "Configurer le texte du statut";
$MESS["BPT1_BT_SSTA18_T"] = "Configurer le texte du statut";
$MESS["BPT1_BT_AA11_TITLE"] = "Approuver les congés";
$MESS["BPT1_BT_CYCLE"] = "Cycle d'approbation";
?>