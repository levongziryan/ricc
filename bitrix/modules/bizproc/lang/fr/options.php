<?
$MESS["BIZPROC_EMPLOYEE_COMPATIBLE_MODE"] = "Activer le mode de compatibilité pour le type 'Lier à l'employé'";
$MESS["BIZPROC_LOG_CLEANUP_DAYS"] = "La durée de conservation du journal d'exécution d'un processus d'affaires (jours)";
$MESS["BIZPROC_OPT_USE_GZIP_COMPRESSION_Y"] = "oui";
$MESS["BIZPROC_LOG_SKIP_TYPES_2"] = "Exécution de l'action";
$MESS["BIZPROC_LOG_SKIP_TYPES_1"] = "démarrage de l'action";
$MESS["BIZPROC_LIMIT_SIMULTANEOUS_PROCESSES"] = "Taille maximale (octets)";
$MESS["BIZPROC_TAB_SET"] = "Paramètres";
$MESS["BIZPROC_TAB_SET_ALT"] = "Paramétrage du module";
$MESS["BIZPROC_LOG_SKIP_TYPES"] = "Ne pas consigner les évènements";
$MESS["BIZPROC_OPT_USE_GZIP_COMPRESSION_EMPTY"] = "ordinaire";
$MESS["BIZPROC_NAME_TEMPLATE"] = "Format d'affichage du nom";
$MESS["BIZPROC_OPTIONS_NAME_IN_SITE_FORMAT"] = "Format du site";
$MESS["BIZPROC_OPT_USE_GZIP_COMPRESSION"] = "Compresser les données archivées";
$MESS["BIZPROC_OPT_USE_GZIP_COMPRESSION_N"] = "aucun";
$MESS["BIZPROC_OPT_LOCKED_WI_PATH"] = "Chemin d'accès de la liste des flux de travail en suspend";
$MESS["BIZPROC_OPT_TIME_LIMIT"] = "Délai d'attente minimum d'une réponse";
$MESS["BIZPROC_OPT_TIME_LIMIT_S"] = "secondes";
$MESS["BIZPROC_OPT_TIME_LIMIT_M"] = "minutes";
$MESS["BIZPROC_OPT_TIME_LIMIT_H"] = "heures";
$MESS["BIZPROC_OPT_TIME_LIMIT_D"] = "jours";
?>