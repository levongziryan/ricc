<?
$MESS["BPT_TTITLE"] = "Dienstreisen";
$MESS["BPT_BT_PARAM_OP_READ"] = "Mitarbeiter, die alle Geschäftsprozesse ansehen dürfen";
$MESS["BPT_BT_PARAM_OP_CREATE"] = "Mitarbeiter, die neue Geschäftsprozesse erstellen dürfen";
$MESS["BPT_BT_PARAM_OP_ADMIN"] = "Mitarbeiter, die Geschäftsprozesse verwalten dürfen";
$MESS["BPT_BT_PARAM_BOSS"] = "Mitarbeiter, die Dienstreisen genehmigen dürfen";
$MESS["BPT_BT_PARAM_BOOK"] = "Buchhaltung";
$MESS["BPT_BT_PARAM_FORM1"] = "Reisekostenabrechnungsformular";
$MESS["BPT_BT_PARAM_FORM2"] = "Formular \"Dienstaufgabe\"";
$MESS["BPT_BT_P_TARGET"] = "Der Dienstreisende";
$MESS["BPT_BT_T_PURPOSE"] = "Zweck der Dienstreise";
$MESS["BPT_BT_T_COUNTRY"] = "Land";
$MESS["BPT_BT_T_COUNTRY_DEF"] = "Deutschland";
$MESS["BPT_BT_T_CITY"] = "Stadt";
$MESS["BPT_BT_T_DATE_START"] = "Geplantes Startdatum";
$MESS["BPT_BT_T_DATE_END"] = "Geplantes Enddatum";
$MESS["BPT_BT_T_EXP"] = "Geplante Ausgaben";
$MESS["BPT_BT_T_TICKETS"] = "Datei hochladen";
$MESS["BPT_BT_SWA"] = "Regelmäßiger Geschäftsprozess";
$MESS["BPT_BT_SFA1_NAME"] = "Dienstreise  {=Template:TargetUser_printable}, {=Template:COUNTRY}-{=Template:CITY}";
$MESS["BPT_BT_SFA1_TITLE"] = "Dienstreiseeigenschaften speichern";
$MESS["BPT_BT_STA1_STATE_TITLE"] = "Geplante Ausgaben";
$MESS["BPT_BT_STA1_TITLE"] = "Statustext eingeben";
$MESS["BPT_BT_AA1_NAME"] = "Bestätigung der Dienstreise {=Template:TargetUser_printable}, {=Template:COUNTRY} - {=Template:CITY}";
$MESS["BPT_BT_AA1_DESCR"] = "Die Dienstreise des Mitarbeiters {=Template:TargetUser_printable} muss genehmigt werden

Land: {=Template:COUNTRY}
Stadt: {=Template:CITY}
Reisedauer: {=Template:date_start} - {=Template:date_end}
Geplante Ausgaben: {=Template:expenditures}

Zweck der Dienstreise:
{=Template:purpose}";
$MESS["BPT_BT_AA1_STATUS_MESSAGE"] = "Wartet auf die Genehmigung";
$MESS["BPT_BT_AA1_TITLE"] = "Vorgesetzter genehmigt Dienstreise";
$MESS["BPT_BT_SA1_TITLE"] = "Reihenfolge der Aktivitäten";
$MESS["BPT_BT_SSTA2_STATE_TITLE"] = "Abwicklung der Dienstreise";
$MESS["BPT_BT_SSTA2_TITLE"] = "Statuseinstellung: Abwicklung";
$MESS["BPT_BT_SNMA1_TEXT"] = "Ihre Dienstreise wurde genehmigt.";
$MESS["BPT_BT_SNMA1_TITLE"] = "Nachricht im sozialen Netzwerk";
$MESS["BPT_BT_RA1_NAME"] = "Abwicklung der Dienstreise{=Template:TargetUser_printable}, {=Template:COUNTRY}-{=Template:CITY}";
$MESS["BPT_BT_RA1_DESCR"] = "Genehmigte Dienstreise muss abgewickelt werden. 

Mitarbeiter: {=Template:TargetUser_printable}

Land: {=Template:COUNTRY}
Stadt: {=Template:CITY}
Geplante Dauer: {=Template:date_start} - {=Template:date_end}
Geplante Ausgaben: {=Template:expenditures}

Zweck der Dienstreise:
{=Template:purpose}

Tickets:
{=Template:tickets_printable}";
$MESS["BPT_BT_RA1_STATUS_MESSAGE"] = "Abwicklung durch die Buchhaltung";
$MESS["BPT_BT_RA1_TBM"] = "Dienstreise wurde abgewickelt.";
$MESS["BPT_BT_RA1_TITLE"] = "Abwicklung durch die Buchhaltung";
$MESS["BPT_BT_RA2_NAME"] = "Dokumente für die Dienstreise";
$MESS["BPT_BT_RA2_DESCR"] = "Bitte dieses Reisekostenabrechnungsformular vor Antritt der Dienstreise herunterladen.
[url={=Variable:ParameterForm1}]Formular herunterladen[/url]";
$MESS["BPT_BT_RA2_STATUS_MESSAGE"] = "Einweisung des Dienstreisenden";
$MESS["BPT_BT_RA2_TBM"] = "Zur Kenntnis genommen";
$MESS["BPT_BT_RA2_TITLE1"] = "Dukumentation für die Geschäftsreise";
$MESS["BPT_BT_RA2_TITLE"] = "Kenntnisnahme durch die Geschäftsführung";
$MESS["BPT_BT_RIA1_NAME"] = "Dienstreisebericht (muss nach der Dienstreise ausgefüllt werden)";
$MESS["BPT_BT_RIA1_DESCR"] = "Spätestens drei Tage nach Rückkehr von Ihrer Dienstreise sind Sie verpflichtet einen Bericht über Ihre Reise anzufertigen. Dazu muss dieses Formular ausgefüllt werden.

Zunächst geben Sie Ihr tatsächliches Rückkehrdatum ein. 
Unter Dienstreisebericht sind der Zweck der Reise und  eine Zusammenfassung über den Verlauf der Dienstreise  und die Ergebnisse der Treffen oder Veranstaltungen anzugeben. Sie können an dieser Stelle auch den Link zu Ihrem ausführlichen Dienstreisebericht einfügen. 
Bei den Gesamtsumme Ausgaben tragen Sie bitte alle angefallenen Ausgaben und deren Gesamtsumme ein. Um Ihre Auslagen erstattet zu bekommen, müssen Sie die ausgefüllte Reisekostenabrechnung mit den Originalbelegen separat einreichen.";
$MESS["BPT_BT_RIA1_DATE_END_REAL"] = "Enddatum der Dienstreise";
$MESS["BPT_BT_RIA1_REPORT"] = "Dienstreisebericht";
$MESS["BPT_BT_RIA1_EXP_REAL"] = "Gesamtsumme Ausgaben";
$MESS["BPT_BT_RIA1_TITLE"] = "Dienstreisebericht";
$MESS["BPT_BT_SSTA3_STATE_TITLE"] = "Dienstreisebericht";
$MESS["BPT_BT_SSTA3_TITLE"] = "Statustext eingeben";
$MESS["BPT_BT_SFA2_TITLE"] = "Bericht speichern";
$MESS["BPT_BT_RA3_NAME"] = "Kenntnisnahme des Dienstreiseberichtes {=Template:TargetUser_printable}";
$MESS["BPT_BT_RA3_DESCR"] = "Der Dienstreisende: {=Template:TargetUser_printable}

Geplante Zeitdauer: {=Template:date_start} - {=Template:date_end}
Tatsächliches Enddatum: {=Variable:date_end_real}

Tatsächliche Ausgaben:
{=Variable:expenditures_real}

Zweck der Dienstreise
{=Template:purpose}

Diensreisebericht:
{=Variable:report}";
$MESS["BPT_BT_RA3_STATUS_MESSAGE"] = "Bericht für den Vorgesetzten";
$MESS["BPT_BT_RA3_TBM"] = "Zur Kenntnis genommen";
$MESS["BPT_BT_RA4_NAME"] = "Dienstreisebericht {=Template:TargetUser_printable}";
$MESS["BPT_BT_RA4_DESCR"] = "Die beendete Dienstreise muss abgewickelt werden. Die Vorgesetzten wurden mit dem Bericht vertraut gemacht.

Der Dienstreisende: {=Template:TargetUser_printable}

Tatsächliche Zeitdauer: {=Template:date_start} - {=Variable:date_end_real}

Tatsächliche Ausgaben:
{=Variable:expenditures_real}

Dienstreisebericht:
{=Variable:report}";
$MESS["BPT_BT_RA4_STATUS_MESSAGE"] = "Abwicklung durch die Buchhaltung";
$MESS["BPT_BT_RA4_TMB"] = "Registriert";
$MESS["BPT_BT_RA4_TITLE"] = "Abwicklung durch die Buchhaltung";
$MESS["BPT_BT_SSTA4_STATE_TITLE"] = "Erledigt";
$MESS["BPT_BT_SSTA4_TITLE"] = "Statustext eingeben";
$MESS["BPT_BT_SA3_TITLE"] = "Reihenfolge der Aktivitäten";
$MESS["BPT_BT_SSTA5_STATE_TITLE"] = "Abgelehnt durch den Vorgesetzten";
$MESS["BPT_BT_SSTA5_TITLE"] = "Statuseinstellung: Abgelehnt";
$MESS["BPT_BT_SNMA2_TEXT"] = "Ihre Dienstreise wurde von dem Vorgesetzten nicht genehmigt";
$MESS["BPT_BT_SNMA2_TITLE"] = "Nachricht im sozialen Netzwerk";
$MESS["BPT_BT_DF_CITY"] = "Stadt";
$MESS["BPT_BT_DF_COUNTRY"] = "Land";
$MESS["BPT_BT_DF_TICKETS"] = "Datei hochladen";
$MESS["BPT_BT_DF_DATE_END_REAL"] = "Tatsächliches Enddatum";
$MESS["BPT_BT_DF_EXP_REAL"] = "Gesamtsumme Ausgaben";
$MESS["BPT_BT_AA7_TITLE"] = "Abwesenheitsdiagramm";
$MESS["BPT_BT_AA7_NAME"] = "Geschäftsreise";
$MESS["BPT_BT_AA7_DESCR"] = "Zweck: {=Template:purpose}";
$MESS["BPT_BT_AA7_STATE"] = "Geschäftsreise";
$MESS["BPT_BT_AA7_FSTATE"] = "Arbeitet";
?>