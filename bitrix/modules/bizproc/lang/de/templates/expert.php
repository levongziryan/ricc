<?
$MESS["BP_EXPR_NAME"] = "Expertenmeinung";
$MESS["BP_EXPR_DESC"] = "Empfohlen für Situationen, wenn eine Person, die das Dokument bestätigen oder ablehnen soll und Expertenmeinungen dafür benötigt. Die Meinungen werden an die Person überreicht, die die endgültige Entscheidung trifft.";
$MESS["BP_EXPR_S"] = "Regelmäßiger Geschäftsprozess";
$MESS["BP_EXPR_TASK1"] = "Das Dokument \"{=Document:NAME}\" erfordert Ihren Kommentar.";
$MESS["BP_EXPR_TASK1_MAIL"] = "Ihre Meinung ist erforderlich, um eine Entscheidung zum Dokument \"{=Document:NAME}\" zu treffen.

Klicken Sie auf den Link um fortzuführen: #BASE_HREF##TASK_URL#";
$MESS["BP_EXPR_M"] = "E-Mail-Nachricht";
$MESS["BP_EXPR_APPR1"] = "Das Dokument \"{=Document:NAME}\" erfordert Ihren Kommentar.";
$MESS["BP_EXPR_APPR1_DESC"] = "Ihre Meinung ist erforderlich, um eine Entscheidung zum Dokument \"{=Document:NAME}\" zu treffen.";
$MESS["BP_EXPR_ST"] = "Reihenfolge der Aktivitäten";
$MESS["BP_EXPR_MAIL2_SUBJ"] = "Dokument \"{=Document:NAME}\" genehmigen";
$MESS["BP_EXPR_MAIL2_TEXT"] = "Alle benachrichtigten Personen haben das Dokument durchgesehen und ihre Meinung dazu abgegeben.
Jetzt müssen Sie das Dokument bestätigen oder ablehnen.

Bitte führen Sie fort unter dem Link: #BASE_HREF##TASK_URL#

{=ApproveActivity1:Comments}";
$MESS["BP_EXPR_APP2_TEXT"] = "Dokument \"{=Document:NAME}\" genehmigen";
$MESS["BP_EXPR_APP2_DESC"] = "Alle benachrichtigten Personen haben das Dokument durchgesehen und ihre Meinung dazu abgegeben.

{=ApproveActivity1:Comments}

Jetzt müssen Sie das Dokument bestätigen oder ablehnen.";
$MESS["BP_EXPR_TAPP"] = "Dokument genehmigen";
$MESS["BP_EXPR_MAIL3_SUBJ"] = "Bestätigung von \"{=Document:NAME}\": Durchlaufen.";
$MESS["BP_EXPR_MAIL3_TEXT"] = "Diskussionen zu \"\"{=Document:NAME}\"\" sind abgeschlossen, das Dokument wurde bestätigt.

{=ApproveActivity2:Comments}";
$MESS["BP_EXPR_ST3_T"] = "Genehmigt";
$MESS["BP_EXPR_ST3_TIT"] = "Status: Bestätigt";
$MESS["BP_EXPR_PUB"] = "Dokument veröffentlichen";
$MESS["BP_EXPR_MAIL4_SUBJ"] = "Diskussionen zu \"{=Document:NAME}\": das Dokument wurde abgelehnt.";
$MESS["BP_EXPR_MAIL4_TEXT"] = "Diskussionen zu \"\"{=Document:NAME}\"\" sind abgeschlossen, das Dokument wurde abgelehnt.

{=ApproveActivity2:Comments}";
$MESS["BP_EXPR_NA"] = "Abgelehnt";
$MESS["BP_EXPR_NA_ST"] = "Status: Abgelehnt";
$MESS["BP_EXPR_PARAM2"] = "Experten";
$MESS["BP_EXPR_PARAM2_DESC"] = "Expertengruppe, deren Mitglieder ihre Meinung zum Dokument ausdrucken können.";
$MESS["BP_EXPR_PARAM1"] = "Genehmigende Person";
?>