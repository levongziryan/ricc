<?
$MESS["BP_REVW_TITLE"] = "Dokument lesen";
$MESS["BP_REVW_T"] = "Regelmäßiger Geschäftsprozess";
$MESS["BP_REVW_TASK"] = "Bitte lesen Sie das Dokument: \"{=Document:NAME}\"";
$MESS["BP_REVW_TASK_DESC"] = "Sie müssen das Dokument \"\"{=Document:NAME}\"\" lesen.

Klicken Sie auf den Link um fortzuführen: #BASE_HREF##TASK_URL#

Autor: {=Document:CREATED_BY}";
$MESS["BP_REVW_MAIL"] = "E-Mail-Nachricht";
$MESS["BP_REVW_REVIEW"] = "Dokument {=Document:NAME} lesen";
$MESS["BP_REVW_REVIEW_DESC"] = "Sie müssen das Dokument \"\"{=Document:NAME}\"\" lesen.

Autor: {=Document:CREATED_BY}";
$MESS["BP_REVW_MAIL_SUBJ"] = "Das Dokument \"{=Document:NAME}\" wurde gelesen.";
$MESS["BP_REVW_MAIL_TEXT"] = "Das Dokument \"{=Document:NAME}\" wurde gelesen.";
$MESS["BP_REVW_PARAM1"] = "Leserliste";
$MESS["BP_REVW_DESC"] = "Dieser Prozess passt ideal für Situation, wenn Sie sicher gehen müssen, dass alle, die mit einem Dokument vertraut gemacht werden sollen, es auch wirklich gelesen haben.";
?>