<?
$MESS ['BPT_SYNC_DESC'] = "Ermöglich die Veröffentlichung von Informationsblock-Elementen auf den kontrollierten Sites. Das Dokument wird zuerst auf der Controller-Site veröffentlicht.";
$MESS ['BPT_SYNC_PUBLISH'] = "Dokument veröffentlicht";
$MESS ['BPT_SYNC_NAME'] = "Veröffentlichung auf kontrollierten Sites";
$MESS ['BPT_SYNC_SYNC'] = "Veröffentlichung auf kontrollierten Sites";
$MESS ['BPT_SYNC_SEQ'] = "Regelmäßiger Geschäftsprozess";
?>