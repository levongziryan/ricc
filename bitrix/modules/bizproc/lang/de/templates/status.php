<?
$MESS ['BPT_ST_SEQ'] = "Aktivitätsreihenfolge";
$MESS ['BPT_ST_CMD_APP'] = "Genehmigung";
$MESS ['BPT_ST_APPROVE_NAME'] = "Dokument \"{=Document:NAME}\" genehmigen";
$MESS ['BPT_ST_NAME'] = "Dokument mit Status genehmigen";
$MESS ['BPT_ST_SUBJECT'] = "Dokument \"{=Document:NAME}\" genehmigen";
$MESS ['BPT_ST_DESC'] = "Genehmigung von Dokumenten mit Status durch mehrere Personen.";
$MESS ['BPT_ST_AUTHOR'] = "Autor;";
$MESS ['BPT_ST_APPROVE_TITLE'] = "Genehmigung des Dokuments";
$MESS ['BPT_ST_PUBDC'] = "Dokumentveröffentlichung";
$MESS ['BPT_ST_ST_DRAFT'] = "Entwurf";
$MESS ['BPT_ST_MAIL_TITLE'] = "E-Mail-Nachricht";
$MESS ['BPT_ST_F'] = "Ereignisgerüst";
$MESS ['BPT_ST_SAVEH'] = "Speichern der History";
$MESS ['BPT_ST_INIT'] = "Status initialisieren";
$MESS ['BPT_ST_APPROVERS'] = "Personen, die das Dokument genehmigen müssen";
$MESS ['BPT_ST_CREATORS'] = "Personen, die das Dokument bearbeiten können";
$MESS ['BPT_ST_CMD_PUBLISH'] = "Veröffentlichen";
$MESS ['BPT_ST_TP'] = "Veröffentlichung";
$MESS ['BPT_ST_CMD_APPR'] = "Zur Genehmigung senden";
$MESS ['BPT_ST_SET_DRAFT'] = "Als Entwurf definieren";
$MESS ['BPT_ST_SET_PUB'] = "Als Veröffentlichung definieren";
$MESS ['BPT_ST_SETSTATE'] = "Status definieren";
$MESS ['BPT_ST_BP_NAME'] = "Geschäftsprozess mit Status";
$MESS ['BPT_ST_INS'] = "Statuseintritt";
$MESS ['BPT_ST_TEXT'] = "Sie müssen das Dokument \"\"{=Document:NAME}\"\" genehmigen oder ablehnen.

Öffnen dazu den Link: #BASE_HREF##TASK_URL#

Das Dokument enthält:

{=Document:DETAIL_TEXT}

Autor: {=Document:CREATED_BY}";
$MESS ['BPT_ST_APPROVE_DESC'] = "Sie müssen das Dokument \"\"{=Document:NAME}\"\" genehmigen.

Öffnen dazu den Link: #BASE_HREF##TASK_URL#

Das Dokument enthält:

{=Document:PREVIEW_TEXT}

Autor: {=Document:CREATED_BY}";
?>