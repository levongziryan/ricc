<?
$MESS["BPCGWTL_CANT_DELETE"] = "Sie können die Geschäftsprozess-Vorlage nicht löschen, da mindestens ein Geschäftsprozess diese Vorlage verwendet.";
$MESS["BPCGWTL_UNKNOWN_ERROR"] = "Unbekannter Fehler.";
$MESS["BPCGWTL_INVALID_WF_ID"] = "Die Geschäftsprozessvorlage '#ID#' wurde nicht gefunden.";
$MESS["BPCGWTL_EMPTY_TEMPLATE"] = "Die Geschäftsprozessvorlage '#ID#' ist leer.";
$MESS["BPCGWTL_INVALID1"] = "Der Wert von '#NAME#' ist kein ganzer Wert.";
$MESS["BPCGWTL_INVALID2"] = "Der Wert von '#NAME#' ist keine echte Zahl.";
$MESS["BPCGWTL_INVALID3"] = "Der Wert von '#NAME#' ist falsch.";
$MESS["BPCGWTL_INVALID4"] = "Der Wert von '#NAME#' kann nicht als 'Ja' oder 'Nein' interpretiert werden.";
$MESS["BPCGWTL_INVALID5"] = "Das Datumsformat im Feld '#NAME#' entspricht nicht dem Format '#FORMAT#'.";
$MESS["BPCGWTL_INVALID6"] = "Der Parameter '#NAME#' des 'User'-Typs erfordert das Soziales-Netzwerk-Modul.";
$MESS["BPCGWTL_INVALID7"] = "Der Typ des '#NAME#' Parameters ist nicht definiert.";
$MESS["BPCGWTL_INVALID8"] = "Das Feld '#NAME#' ist erforderlich.";
$MESS["BPCGWTL_WRONG_TEMPLATE"] = "Falsche Geschäftsprozessvorlage";
$MESS["BPWTL_ERROR_MESSAGE_PREFIX"] = "Aktion '#TITLE#':";
?>