<?
$MESS["BPRA_DEFAULT_STATUS_MESSAGE"] = "Warten auf Antwort der Anwendung";
$MESS["BPRA_DEFAULT_LOG_MESSAGE"] = "Antwort der Anwendung erhalten";
$MESS["BPRA_PD_USER_ID"] = "Starten als";
$MESS["BPRA_PD_SET_STATUS_MESSAGE"] = "Nachrichtenstatus definieren";
$MESS["BPRA_PD_STATUS_MESSAGE"] = "Statustext";
$MESS["BPRA_PD_YES"] = "Ja";
$MESS["BPRA_PD_NO"] = "Nein";
$MESS["BPRA_PD_USE_SUBSCRIPTION"] = "Auf Antwort warten";
$MESS["BPRA_PD_TIMEOUT_DURATION"] = "Timeout";
$MESS["BPRA_PD_TIMEOUT_DURATION_HINT"] = "Wie lange soll auf die Antwort der Anwendung gewartet werden";
$MESS["BPRA_PD_TIME_D"] = "Tage";
$MESS["BPRA_PD_TIME_H"] = "Stunden";
$MESS["BPRA_PD_TIME_M"] = "Minuten";
$MESS["BPRA_PD_TIME_S"] = "Sekunden";
$MESS["BPRA_PD_ERROR_EMPTY_PROPERTY"] = "Dieses Fed ist erforderlich: #NAME#";
$MESS["BPRA_PD_TIMEOUT_LIMIT"] = "Minimale Wartezeit";
$MESS["BPRA_NOT_FOUND_ERROR"] = "Aktivität der Anwendung ist nicht installiert.";
?>