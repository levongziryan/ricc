<?
$MESS["BPCGDOC_ADD"] = "Neu";
$MESS["BPCGDOC_INVALID_WF_ID"] = "Kann keine Vorlage für den Geschäftsprozess #ID# finden.";
$MESS["BPCGDOC_AUTO_EXECUTE_CREATE"] = "Erstellen";
$MESS["BPCGDOC_AUTO_EXECUTE_DELETE"] = "Löschen";
$MESS["BPCGDOC_AUTO_EXECUTE_NONE"] = "Nein";
$MESS["BPCGDOC_NO"] = "Nein";
$MESS["BPCGDOC_INVALID_WF"] = "Kein arbeitender Geschäftsprozess für dieses Element gefunden.";
$MESS["BPCGDOC_EMPTY_WD_ID"] = "Die Geschäftsprozessvorlage '#ID#' ist nicht vorhanden.";
$MESS["BPCGDOC_INVALID_TYPE"] = "Der Parametertyp ist nicht definiert.";
$MESS["BPCGDOC_AUTO_EXECUTE_EDIT"] = "Aktualisieren";
$MESS["BPCGDOC_WAIT"] = "bitte warten...";
$MESS["BPCGDOC_YES"] = "Ja";
$MESS["BPCGDOC_ERROR_DELEGATE"] = "Die Aufgabe \"#NAME#\" kann nicht delegiert werden: der ausgewählte Mitarbeiter ist an der Aufgabenerledigung bereits beteiligt";
$MESS["BPCGDOC_ERROR_ACTION"] = "Aufgabe \"#NAME#\": #ERROR#";
$MESS["BPCGDOC_ERROR_TASK_IS_NOT_INLINE"] = "Die Aufgabe \"#NAME#\" kann mit dieser Aktion nicht ausgeführt werden";
$MESS["BPCGDOC_WI_LOCKED_NOTICE_TITLE"] = "Portal-Assistent";
$MESS["BPCGDOC_WI_LOCKED_NOTICE_MESSAGE"] = "Einige von den Workflows (#CNT#), die Sie gestartet haben, wurden nicht korrekt abgeschlossen.
[URL=#PATH#]Diese Workflows anzeigen[/URL].
";
$MESS["BPCGDOC_DELEGATE_LOG_TITLE"] = "Delegiert";
$MESS["BPCGDOC_DELEGATE_LOG"] = "Nutzer #FROM# delegierte Aufgabe \"#NAME#\" dem Nutzer #TO#";
$MESS["BPCGDOC_WI_B24_LIMITS_MESSAGE"] = "Für einige Elemente auf Ihrem Portal wurden mehr als zwei gleichzeitige Workflows gestartet. Bald wird eine Einschränkung in Kraft treten, der zufolge maximal zwei gleichzeitige Workflows pro Element zugelassen sein werden. Wir empfehlen Ihnen, die Logik Ihrer Workflows zu ändern. [URL=https://helpdesk.bitrix24.de/open/4838473/]Mehr erfahren[/URL]";
$MESS["BPCGDOC_DELEGATE_NOTIFY_TEXT"] = "Eine Workflow-Aufgabe wurde an Sie delegiert: [URL=#TASK_URL#]#TASK_NAME#[/URL]";
$MESS["BPCGDOC_ERROR_DELEGATE_0"] = "Aufgabe \"#NAME#\" kann nicht delegiert werden, weil der ausgewählte Mitarbeiter Ihnen nicht unterstellt ist.";
$MESS["BPCGDOC_ERROR_DELEGATE_1"] = "Aufgabe \"#NAME#\" kann nicht delegiert werden, weil der ausgewählte Nutzer bei diesem Unternehmen nicht arbeitet.";
$MESS["BPCGDOC_ERROR_DELEGATE_2"] = "Aufgabe \"#NAME#\" kann nicht delegiert werden, weil Delegieren in den Aufgabeneinstellungen untersagt ist.";
?>