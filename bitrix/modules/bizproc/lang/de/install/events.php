<?
$MESS["BIZPROC_MAIL_TEMPLATE_NAME"] = "Benachrichtigung der Sendeaktion für E-Mail";
$MESS["BIZPROC_MAIL_TEMPLATE_DESC"] = "#SENDER# - Nachrichtenabsender
#RECEIVER# - Nachrichtenempfänger
#TITLE# - Nachrichtenüberschrift
#MESSAGE# - Nachrichtentext
";
$MESS["BIZPROC_HTML_MAIL_TEMPLATE_NAME"] = "Benachrichtigung der Aktion zum E-Mail-Versand (HTML)";
?>