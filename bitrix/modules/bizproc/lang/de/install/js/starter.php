<?
$MESS["BIZPROC_JS_BP_STARTER_REQUEST_FAILURE"] = "Fehler bei Ausführung der Anfrage. Versuchen Sie die Seite neu zu laden.";
$MESS["BIZPROC_JS_BP_STARTER_CANCEL"] = "Abbrechen";
$MESS["BIZPROC_JS_BP_STARTER_START"] = "Start";
$MESS["BIZPROC_JS_BP_STARTER_SAVE"] = "Speichern";
$MESS["BIZPROC_JS_BP_STARTER_AUTOSTART"] = "Workflows automatisch starten";
$MESS["BIZPROC_JS_BP_STARTER_NO_TEMPLATES"] = "Es gibt keine Workflow-Vorlagen";
$MESS["BIZPROC_JS_BP_STARTER_DESTINATION_CHOOSE"] = "auswählen";
$MESS["BIZPROC_JS_BP_STARTER_DESTINATION_EDIT"] = "bearbeiten";
$MESS["BIZPROC_JS_BP_STARTER_FILE_CHOOSE"] = "Datei auswählen";
$MESS["BIZPROC_JS_BP_STARTER_CONTROL_CLONE"] = "hinzufügen";
?>