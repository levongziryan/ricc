<?
$MESS["BPAT_BACK"] = "Atrás";
$MESS["BPAT_GOTO_DOC"] = "Ir al Documento";
$MESS["BPAT_TAB"] = "Tarea";
$MESS["BPAT_TAB_TITLE"] = "Tarea";
$MESS["BPAT_TITLE"] = "Tarea #ID#";
$MESS["BPAT_DESCR"] = "Descripción de tarea";
$MESS["BPAT_NAME"] = "Nombre de la tarea";
$MESS["BPAT_NO_TASK"] = "La tarea no fue encontrada.";
$MESS["BPAT_USER"] = "Usuario";
$MESS["BPAT_USER_NOT_FOUND"] = "(No encontrado)";
$MESS["BPAT_NO_STATE"] = "No se encontró el proceso de negocio.";
$MESS["BPAT_ACTION_DELEGATE"] = "Delegar";
?>