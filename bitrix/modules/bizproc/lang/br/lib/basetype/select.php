<?
$MESS["BPDT_SELECT_INVALID"] = "O item selecionado não é um item da lista.";
$MESS["BPDT_SELECT_OPTIONS1"] = "Especificar cada variante em uma nova linha. Se o valor da variante e o nome da variante são diferentes, preceda o nome com o valor entre colchetes. Por exemplo: [v1]Variante 1";
$MESS["BPDT_SELECT_OPTIONS2"] = "Clique \"Definir\" quando terminar.";
$MESS["BPDT_SELECT_OPTIONS3"] = "Estabelecer";
?>