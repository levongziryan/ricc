<?
$MESS["BIZPROC_TAB_RIGHTS"] = "Acesso";
$MESS["BIZPROC_TAB_RIGHTS_ALT"] = "Módulo de acesso";
$MESS["BIZPROC_TAB_SET_ALT"] = "Configurações do Módulo";
$MESS["BIZPROC_TAB_SET"] = "Configurações";
$MESS["BIZPROC_LOG_CLEANUP_DAYS"] = "Número de dias para manter o log do processo de negócio";
$MESS["BIZPROC_NAME_TEMPLATE"] = "Formato de exibição do nome";
$MESS["BIZPROC_OPTIONS_NAME_IN_SITE_FORMAT"] = "Formato do website";
$MESS["BIZPROC_EMPLOYEE_COMPATIBLE_MODE"] = "Ativar o modo de compatibilidade para \"vincular o usuário\" tipo";
$MESS["BIZPROC_LOG_SKIP_TYPES"] = "Não registrar eventos ";
$MESS["BIZPROC_LOG_SKIP_TYPES_1"] = "início";
$MESS["BIZPROC_LOG_SKIP_TYPES_2"] = "fim";
$MESS["BIZPROC_LIMIT_SIMULTANEOUS_PROCESSES"] = "Processos Máximo simultâneos para documento";
?>