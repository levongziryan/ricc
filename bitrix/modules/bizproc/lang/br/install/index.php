<?
$MESS["BIZPROC_INSTALL_DESCRIPTION"] = "Módulo para criar, administrar e iniciar processos de negócio. ";
$MESS["BIZPROC_PERM_D"] = "acesso negado";
$MESS["BIZPROC_INSTALL_NAME"] = "Processos de Negócio";
$MESS["BIZPROC_INSTALL_TITLE"] = "Instalação do Módulo";
$MESS["BIZPROC_PERM_R"] = "ler";
$MESS["BIZPROC_PERM_W"] = "Escrever";
$MESS["BIZPROC_PHP_L439"] = "Você está usando a versão PHP #VERS#, porém o módulo exige a versão 5.0.0 ou superior. Por favor faça update da sua instalação PHP ou contate o suporte técnico. ";
$MESS["BIZPROC_BIZPROCDESIGNER_INSTALLED"] = "O módulo de design do processo de negócio está instalado. Por favor, desinstale este módulo antes de prosseguir. ";
?>