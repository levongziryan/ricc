<?
$MESS["BIZPROC_JS_BP_STARTER_REQUEST_FAILURE"] = "Erro ao executar pedido. Tente atualizar a página.";
$MESS["BIZPROC_JS_BP_STARTER_CANCEL"] = "Cancelar";
$MESS["BIZPROC_JS_BP_STARTER_START"] = "Resumo de Horas Trabalhadas";
$MESS["BIZPROC_JS_BP_STARTER_SAVE"] = "Salvar";
$MESS["BIZPROC_JS_BP_STARTER_AUTOSTART"] = "Fluxos de trabalho autoexecutáveis";
$MESS["BIZPROC_JS_BP_STARTER_NO_TEMPLATES"] = "Não há nenhum modelo de fluxo de trabalho";
?>