<?
$MESS["BIZPROC_JS_BP_SELECTOR_PARAMETERS"] = "Parâmetros";
$MESS["BIZPROC_JS_BP_SELECTOR_VARIABLES"] = "Variáveis";
$MESS["BIZPROC_JS_BP_SELECTOR_CONSTANTS"] = "Constantes";
$MESS["BIZPROC_JS_BP_SELECTOR_DOCUMENT"] = "Documento";
$MESS["BIZPROC_JS_BP_SELECTOR_ACTIVITIES"] = "Resultados adicionais";
$MESS["BIZPROC_JS_BP_SELECTOR_SYSTEM"] = "Sistema";
$MESS["BIZPROC_JS_BP_SELECTOR_WORKFLOW_ID"] = "ID do fluxo de trabalho";
$MESS["BIZPROC_JS_BP_SELECTOR_USER_ID"] = "Usuário atual";
$MESS["BIZPROC_JS_BP_SELECTOR_NOW"] = "Horário do servidor";
$MESS["BIZPROC_JS_BP_SELECTOR_NOW_LOCAL"] = "Horário local";
$MESS["BIZPROC_JS_BP_SELECTOR_DATE"] = "Data atual";
$MESS["BIZPROC_JS_BP_SELECTOR_EMPTY_LIST"] = "Não foram encontradas entradas.";
$MESS["BIZPROC_JS_BP_SELECTOR_TARGET_USER"] = "Fluxo de trabalho iniciado por";
$MESS["BIZPROC_JS_BP_SELECTOR_FUNCTIONS"] = "Funções";
$MESS["BIZPROC_JS_BP_SELECTOR_FUNCTION_ABS_DESCRIPTION"] = "Valor absoluto";
$MESS["BIZPROC_JS_BP_SELECTOR_FUNCTION_DATEADD_DESCRIPTION"] = "Adicionar período de tempo até a data";
$MESS["BIZPROC_JS_BP_SELECTOR_FUNCTION_DATEDIFF_DESCRIPTION"] = "Subtrair datas";
$MESS["BIZPROC_JS_BP_SELECTOR_FUNCTION_IF_DESCRIPTION"] = "Afirmação condicional (se)";
$MESS["BIZPROC_JS_BP_SELECTOR_FUNCTION_INTVAL_DESCRIPTION"] = "Converter para número inteiro";
$MESS["BIZPROC_JS_BP_SELECTOR_FUNCTION_MIN_DESCRIPTION"] = "Obter menor número (min)";
$MESS["BIZPROC_JS_BP_SELECTOR_FUNCTION_SUBSTR_DESCRIPTION"] = "Obter subsequência (subseq)";
$MESS["BIZPROC_JS_BP_SELECTOR_FUNCTION_MERGE_DESCRIPTION"] = "Mesclar vários valores";
?>