<?
$MESS["BPT1_TTITLE"] = "Licença";
$MESS["BPT1_BT_PARAM_OP_READ"] = "Colaboradores com permissão para visualizar todos os processos";
$MESS["BPT1_BT_PARAM_OP_CREATE"] = "Colaboradores com permissão para criar novos processos";
$MESS["BPT1_BT_PARAM_OP_ADMIN"] = "Colaboradores com permissão para administrar processos";
$MESS["BPT1_BT_PARAM_BOSS"] = "Colaboradores com permissão para aprovar licença";
$MESS["BPT1_BT_PARAM_BOOK"] = "Departamento de Contabilidade";
$MESS["BPT1_BT_P_TARGET"] = "Colaborador";
$MESS["BPT1_BT_T_DATE_START"] = "Licença começa em";
$MESS["BPT1_BT_T_DATE_END"] = "Licença termina em";
$MESS["BPT1_BT_SWA"] = "Processo de Negócio Sequencial";
$MESS["BPT1_BT_SFA1_TITLE"] = "Salvar Parâmetros de Licença";
$MESS["BPT1_BT_STA1_STATE_TITLE"] = "Aprovação";
$MESS["BPT1_BT_STA1_TITLE"] = "Ajustar Status de Texto";
$MESS["BPT1_BT_CYCLE"] = "Ciclo de Aprovação";
$MESS["BPT1_BT_SA1_TITLE"] = "Sequência de Ações";
$MESS["BPT1_BT_AA11_NAME"] = "Aprovar a licença do colaborador {=Template:TargetUser_printable} desde {=Template:date_start} até {=Template:date_end}";
$MESS["BPT1_BT_AA11_DESCR"] = "Colaborador: {=Template:TargetUser_printable}
Licença começa em: {=Template:date_start}
Licença termina em: {=Template:date_end}";
$MESS["BPT1_BT_AA11_STATUS_MESSAGE"] = "A ser aprovado pelo Administrador Sênior";
$MESS["BPT1_BT_AA11_TITLE"] = "Aprovar Licença";
$MESS["BPT1_BT_RIA11_NAME"] = "O administrador {=Template:TargetUser_printable} aprova a licença de {=Template:date_start} ainda que a data de volta ( {=Template:date_end} ) necessite de aprovação adicional posterior?";
$MESS["BPT1_BT_RIA11_DESCR"] = "A licença foi aprovada. Necessita de aprovação adicional?

Colaborador: {=Template:TargetUser_printable}
Licença começa em: {=Template:date_start}
Licença termina em: {=Template:date_end}";
$MESS["BPT1_BT_RIA11_P1"] = "Necessita de aprovação adicional";
$MESS["BPT1_BT_RIA11_P2"] = "A ser aprovado por";
$MESS["BPT1_BT_RIA11_TITLE"] = "Aprovação Adicional";
$MESS["BPT1_BT_IF11_N"] = "Condição";
$MESS["BPT1_BT_IEBA1_V1"] = "Necessita de Aprovação Adicional";
$MESS["BPT1_BT_IEBA2_V2"] = "Não necessita de aprovação adicional";
$MESS["BPT1_BT_SFA12_TITLE"] = "Editar Documento";
$MESS["BPT1_BT_SFTA12_ST"] = "A licença foi aprovada pelo Administrador Sênior";
$MESS["BPT1_BT_SFTA12_T"] = "Ajustar Status do Texto";
$MESS["BPT1_BT_SSTA14_ST"] = "Recusado";
$MESS["BPT1_BT_SSTA14_T"] = "Ajustar Status do Texto";
$MESS["BPT1_BT_IEBA15_V1"] = "A licença foi aprovada";
$MESS["BPT1_BT_SNMA16_TEXT"] = "Sua licença foi aprovada pelo Administrador Sênios";
$MESS["BPT1_BT_SNMA16_TITLE"] = "Mensagem da Rede Social";
$MESS["BPT1_BT_RA17_NAME"] = "Registrando a licença de {=Template:TargetUser_printable},  {=Template:date_start} - {=Template:date_end}";
$MESS["BPT1_BT_RA17_DESCR"] = "Uma licença foi aprovada pelo Administrador Sênior. Uma ordem escrita correspondente deverá ser registrada, as férias devem ser pagas ao colaborador.

Colaborador: {=Template:TargetUser_printable}
Licença começa em: {=Template:date_start}
Licença termina em: {=Template:date_end}";
$MESS["BPT1_BT_RA17_STATUS_MESSAGE"] = "Processando no Departamento de Contabilidade";
$MESS["BPT1_BT_RA17_TBM"] = "A licença foi aprovada e registrada.";
$MESS["BPT1_BT_RA17_TITLE"] = "Processando no Departamento de Contabilidade";
$MESS["BPT1_BT_SSTA18_ST"] = "A licença foi registrada.";
$MESS["BPT1_BT_SSTA18_T"] = "Ajustar Status do Texto";
$MESS["BPT1_BT_IEBA15_V2"] = "A licença não foi aprovada.";
$MESS["BPT1_BT_SNMA18_TEXT"] = "A sua icença não foi aprovada. {=A94751_67978_49922_99999:Comments}";
$MESS["BPT1_BT_SNMA18_TITLE"] = "Mensagem da Rede Social";
$MESS["BPT1_BTF_P_APP"] = "Aprovação";
$MESS["BPT1_BTF_P_APPS"] = "[x]Não aprovada
[y]Aprovada
[n]Recusada";
$MESS["BPT_BT_AA7_TITLE"] = "Gráfico de Ausência";
$MESS["BPT_BT_AA7_NAME"] = "Licença";
$MESS["BPT_BT_AA7_DESCR"] = "Licença Anual";
$MESS["BPT_BT_AA7_STATE"] = "Licença";
$MESS["BPT_BT_AA7_FSTATE"] = "Trabalhos";
?>