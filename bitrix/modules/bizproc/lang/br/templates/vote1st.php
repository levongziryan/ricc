<?
$MESS["BP_V1ST_NAME"] = "Primeira Aprovação";
$MESS["BP_V1ST_DESC"] = "Recomendado quando a aprovação de um único colaborador é suficiente. Cria uma lista de colaboradores que poderão ser sugeridos para participar da votação. A votação estará concluída quando o primeiro voto for recebido. ";
$MESS["BP_V1ST_SEQ"] = "Processo de Negócio Sequencial";
$MESS["BP_V1ST_TASK_NAME"] = "Aprovar documento: \"{=Document:NAME}\"";
$MESS["BP_V1ST_TASK_TEXT"] = "Você deve aprovar ou recusar o documento \"{=Document:NAME}\".

Prossiga abrindo o seguinte link: #BASE_HREF##TASK_URL#

Autor: {=Document:CREATED_BY_PRINTABLE}";
$MESS["BP_V1ST_MAIL_NAME"] = "Mensagem de email";
$MESS["BP_V1ST_TASK_T"] = "Por favor, aprove ou recuse o documento. ";
$MESS["BP_V1ST_TASK_DESC"] = "Você deve aprovar ou recusar o documento \"{=Document:NAME}\".

Autor: {=Document:CREATED_BY_PRINTABLE}";
$MESS["BP_V1ST_VNAME"] = "Responder sobre um documento";
$MESS["BP_V1ST_S2"] = "Sequência de Ações";
$MESS["BP_V1ST_MAIL_SUBJ"] = "Votação de {=Document:NAME}: O documento foi aprovado. ";
$MESS["BP_V1ST_MAIL_TEXT"] = "A votação do documento \"{=Document:NAME}\" foi concluída.

O documento foi aprovado.
{=ApproveActivity1:Comments}";
$MESS["BP_V1ST_APPR"] = "Aprovado";
$MESS["BP_V1ST_APPR_S"] = "Status: Aprovado";
$MESS["BP_V1ST_T3"] = "Publicar Documento";
$MESS["BP_V1ST_MAIL2_NA"] = "Votação de {=Document:NAME}: O documento foi recusado.";
$MESS["BP_V1ST_MAIL2_NA_TEXT"] = "A votação do documento \"{=Document:NAME}\" foi concluída.

O documento foi recusado.
{=ApproveActivity1:Comments}";
$MESS["BP_V1ST_TNA"] = "O documento foi recusado.";
$MESS["BP_V1ST_STAT_NA"] = "Recusado";
$MESS["BP_V1ST_STAT_NA_T"] = "Status: Recusado";
$MESS["BP_V1ST_PARAM1"] = "Colaboradores Votantes";
$MESS["BP_V1ST_PARAM1_DESC"] = "Usuários participantes do processo de aprovação.";
?>