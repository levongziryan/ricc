<?
$MESS["BPT_SYNC_NAME"] = "Publicação em locais controlados";
$MESS["BPT_SYNC_DESC"] = "Permite a publicação de bloco de elementos de informação em locais controlados. Os documentos serão publicados primeiro no local de controle.";
$MESS["BPT_SYNC_SEQ"] = "Processo de Negócio Sequencial";
$MESS["BPT_SYNC_SYNC"] = "Publicação em locais controlados";
$MESS["BPT_SYNC_PUBLISH"] = "Publicar Documento";
?>