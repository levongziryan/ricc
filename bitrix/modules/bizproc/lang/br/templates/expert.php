<?
$MESS["BP_EXPR_NAME"] = "Opinião de Especialista";
$MESS["BP_EXPR_DESC"] = "Recomendado para situações em que o colaborador encarregado de aprovar ou recusar um documento necessita da opinião de especialistas no assunto. Este processo cria um grupo de especialistas no qual cada um poderá expressar a sua opinião pessoal para o documento em questão. Deste modo, as opiniões poderão ser analisadas pelo colaborador, auxiliando-o na tomada final de decisão.";
$MESS["BP_EXPR_S"] = "Processo de Negócio Sequencial";
$MESS["BP_EXPR_TASK1"] = "É necessário que você comente sobre o documento \"{=Document:NAME}\" .";
$MESS["BP_EXPR_TASK1_MAIL"] = "Sua opinião é necessária para que seja tomada uma decisão sobre o documento \"{=Document:NAME}\".

Por favor, prossiga abrindo o seguindo link: #BASE_HREF##TASK_URL#";
$MESS["BP_EXPR_M"] = "Mensagem de email";
$MESS["BP_EXPR_APPR1"] = "É necessário que você comente sobre o documento \"{=Document:NAME}\" .";
$MESS["BP_EXPR_APPR1_DESC"] = "Sua opinião é necessária para que seja tomada uma decisão sobre o documento \"{=Document:NAME}\".";
$MESS["BP_EXPR_ST"] = "Sequência de Ações";
$MESS["BP_EXPR_MAIL2_SUBJ"] = "Aprovar documento: \"{=Document:NAME}\"";
$MESS["BP_EXPR_MAIL2_TEXT"] = "Todos os colaboradores selecionados examinaram o documento e expressaram suas opiniões. 

Você deve agora aprovar ou recusar o documento.																										
																										
Por favor, prossiga abrindo o seguinte link: #BASE_HREF##TASK_URL#																									
																										
{=ApproveActivity1:Comments}";
$MESS["BP_EXPR_APP2_TEXT"] = "Aprovar documento: \"{=Document:NAME}\"";
$MESS["BP_EXPR_APP2_DESC"] = "Todos os colaboradores selecionados examinaram o documento e expressaram suas opiniões. 										
																										
{=ApproveActivity1:Comments}

Você deve agora aprovar ou recusar o documento.";
$MESS["BP_EXPR_TAPP"] = "Aprovar Documento";
$MESS["BP_EXPR_MAIL3_SUBJ"] = "Aprovação de \"{=Document:NAME}\": o documento passou.";
$MESS["BP_EXPR_MAIL3_TEXT"] = "O debate sobre \"{=Document:NAME}\" foi concluído; o documento foi aprovado.																										
																										
{=ApproveActivity2:Comments}";
$MESS["BP_EXPR_ST3_T"] = "Aprovado";
$MESS["BP_EXPR_ST3_TIT"] = "Status: Aprovado";
$MESS["BP_EXPR_PUB"] = "Publicar Documento";
$MESS["BP_EXPR_MAIL4_SUBJ"] = "Discussão sobre \"{=Document:NAME}\": o documento foi recusado.																	
																										

";
$MESS["BP_EXPR_MAIL4_TEXT"] = "As discussões sobre \"{=Document:NAME}\" foram concluídas; o documento foi recusado.																										
																										
{=ApproveActivity2:Comments}";
$MESS["BP_EXPR_NA"] = "Recusado";
$MESS["BP_EXPR_NA_ST"] = "Status: Recusado";
$MESS["BP_EXPR_PARAM2"] = "Especialistas";
$MESS["BP_EXPR_PARAM2_DESC"] = "Grupo de especialistas em que os membros podem expressar suas opiniões sobre um documento. ";
$MESS["BP_EXPR_PARAM1"] = "Colaborador moderador";
?>