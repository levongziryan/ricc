<?
$MESS["BPCGDOC_ADD"] = "Adicionar";
$MESS["BPCGDOC_INVALID_WF_ID"] = "Não foi encontrado o template para o processo de negócio #ID#.";
$MESS["BPCGDOC_AUTO_EXECUTE_CREATE"] = "Criar";
$MESS["BPCGDOC_AUTO_EXECUTE_DELETE"] = "Deletar";
$MESS["BPCGDOC_AUTO_EXECUTE_NONE"] = "Não";
$MESS["BPCGDOC_NO"] = "Não";
$MESS["BPCGDOC_INVALID_WF"] = "Não foi encontrado trabalho em processo de negócio para este documento. ";
$MESS["BPCGDOC_EMPTY_WD_ID"] = "O template de ID do processo de negócio está faltando. ";
$MESS["BPCGDOC_INVALID_TYPE"] = "O tipo de parâmetro não está definido. ";
$MESS["BPCGDOC_AUTO_EXECUTE_EDIT"] = "Update";
$MESS["BPCGDOC_WAIT"] = "aguarde...";
$MESS["BPCGDOC_YES"] = "Sim";
$MESS["BPCGDOC_ERROR_DELEGATE"] = "A tarefa \"#NAME#\" não pode ser delegada porque o funcionário selecionado já está designado para a tarefa.";
$MESS["BPCGDOC_ERROR_ACTION"] = "Tarefa \"#NAME#\": #ERROR#";
$MESS["BPCGDOC_ERROR_TASK_IS_NOT_INLINE"] = "A tarefa \"#NAME#\" não pode ser executada desta forma.";
$MESS["BPCGDOC_WI_LOCKED_NOTICE_TITLE"] = "Assistente de portal";
$MESS["BPCGDOC_WI_LOCKED_NOTICE_MESSAGE"] = "Alguns dos fluxos de trabalho que você iniciou (#CNT#) tiveram erros ou foram concluídos incorretamente.
[URL=#PATH#]Visualizar esses fluxos de trabalho[/URL].";
$MESS["BPCGDOC_DELEGATE_LOG_TITLE"] = "Delegado";
$MESS["BPCGDOC_DELEGATE_LOG"] = "O usuário #FROM# delegou a tarefa \"#NAME#\" para #TO#";
$MESS["BPCGDOC_WI_B24_LIMITS_MESSAGE"] = "Algumas das entidades no seu portal executam mais de dois fluxos de trabalho simultâneos. Em breve, será introduzida uma restrição de no máximo dois fluxos de trabalho simultâneos por entidade. É recomendável que você altere a lógica de seus fluxos de trabalho. [URL=https://helpdesk.bitrix24.com/open/4838471/]Saiba mais[/URL]";
$MESS["BPCGDOC_DELEGATE_NOTIFY_TEXT"] = "Uma tarefa de fluxo de trabalho foi delegada a você: [URL=#TASK_URL#]#TASK_NAME#[/URL]";
$MESS["BPCGDOC_ERROR_DELEGATE_0"] = "A tarefa \"#NAME#\" não pode ser delegada porque o funcionário selecionado não é seu subordinado.";
$MESS["BPCGDOC_ERROR_DELEGATE_1"] = "A tarefa \"#NAME#\" não pode ser delegada porque o usuário selecionado não trabalha para esta empresa.";
$MESS["BPCGDOC_ERROR_DELEGATE_2"] = "A tarefa \"#NAME#\" não pode ser delegada porque a delegação foi proibida nas preferências de tarefa.";
?>