<?
$MESS["BPCGERR_INVALID_TYPE1"] = "O termo do '#PARAM#' deverá ser do tipo '#VALUE#'.";
$MESS["BPCGERR_INVALID_ARG1"] = "O termo do '#PARAM#' tem um valor inválido '#VALUE#'.";
$MESS["BPCGERR_INVALID_ARG"] = "O termo do '#PARAM#' tem um valor inválido '#VALUE#'.";
$MESS["BPCGERR_NULL_ARG"] = "O termo do '#PARAM#' não está definido.";
$MESS["BPCGERR_INVALID_TYPE"] = "O tipo de  '#PARAM#' do termo é inválido. ";
?>