<?
$MESS["BPAT_BACK"] = "Voltar";
$MESS["BPAT_GOTO_DOC"] = "Ir para o Documento";
$MESS["BPAT_TAB"] = "Tarefa";
$MESS["BPAT_TAB_TITLE"] = "Tarefa";
$MESS["BPAT_TITLE"] = "#ID# da Tarefa";
$MESS["BPAT_DESCR"] = "Descrição da Tarefa";
$MESS["BPAT_NAME"] = "Nome da Tarefa";
$MESS["BPAT_NO_TASK"] = "A tarefa não foi encontrada.";
$MESS["BPAT_USER"] = "Usuário.";
$MESS["BPAT_USER_NOT_FOUND"] = "(Não encontrado)";
$MESS["BPAT_ACTION_DELEGATE"] = "Delegar";
?>