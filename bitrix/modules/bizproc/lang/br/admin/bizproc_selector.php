<?
$MESS["BIZPROC_SEL_USERS_TAB_GROUPS"] = "- Grupos";
$MESS["BIZPROC_SEL_USERS_TAB_USERS"] = "- Usuários";
$MESS["BIZPROC_SEL_TITLEBAR"] = "Adicionar Campos";
$MESS["BIZPROC_SEL_CANCEL"] = "Cancelado";
$MESS["BIZPROC_SEL_FIELDS_TAB"] = "Campos de Documento";
$MESS["BIZPROC_SEL_TITLEBAR_DESC"] = "Duplo clique no campo requerido para selecionar ou clique &quot;Inserir&quot;";
$MESS["BIZPROC_SEL_INSERT"] = "Inserir";
$MESS["BIZPROC_SEL_TITLE"] = "Selecionar Campo";
$MESS["BIZPROC_SEL_PARAMS_TAB"] = "Parâmetros de Template";
$MESS["BIZPROC_SEL_USERS_TAB"] = "Usuários";
$MESS["BIZPROC_SEL_ERR"] = "Você tem que selecionar um campo em uma das listas!";
$MESS["BP_SEL_VARS"] = "Variáveis";
$MESS["BP_SEL_ADDIT"] = "Resultados adicionais";
$MESS["BIZPROC_AS_TITLE_TOOLBAR"] = "Atribuindo valores";
$MESS["BIZPROC_AS_TITLE"] = "Inserir Valor";
$MESS["BP_SEL_CONSTANTS"] = "Constantes";
$MESS["BIZPROC_SEL_GROUPS_TAB"] = "Categoria de usuário";
?>