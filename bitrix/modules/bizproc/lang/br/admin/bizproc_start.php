<?
$MESS["BPABS_MESSAGE_SUCCESS"] = "Um processo de negócio do tipo '#TEMPLATE#' foi iniciado com sucesso.";
$MESS["BPABS_ADD"] = "Adicionar";
$MESS["BPABS_BACK"] = "Voltar";
$MESS["BPABS_TAB"] = "Processo de Negócio";
$MESS["BPABS_TAB1"] = "Processo de Negócio";
$MESS["BPABS_TAB_TITLE"] = "Iniciar Parâmetros de Processo de Negócio";
$MESS["BPABS_DESCRIPTION"] = "Template de Descrição do Processo de Negócio";
$MESS["BPABS_NAME"] = "Nome do Template do Processo de Negócio";
$MESS["BPABS_DO_CANCEL"] = "Cancelar";
$MESS["BPABS_MESSAGE_ERROR"] = "Não foi possível iniciar o processo de negócio do tipo  '#TEMPLATE#'.";
$MESS["BPABS_ERROR"] = "Erro";
$MESS["BPABS_NO"] = "Não";
$MESS["BPABS_NO_TEMPLATES"] = "Não foi encontrado um template de processo de negócio para este tipo de documento. ";
$MESS["BPABS_EMPTY_DOC_ID"] = "Não há ID de documento especificado para o qual o processo de negócio será criado. ";
$MESS["BPABS_EMPTY_ENTITY"] = "Não foi especificada uma entidade para a qual o processo de negócio será criado.";
$MESS["BPABS_TITLE"] = "Iniciar Processo de Negócio";
$MESS["BPABS_TAB1_TITLE"] = "Selecionar o template do processo de negócio para iniciar.";
$MESS["BPABS_DO_START"] = "Iniciar";
$MESS["BPABS_EMPTY_DOC_TYPE"] = "O tipo de documento é necessário.";
$MESS["BPABS_INVALID_TYPE"] = "O parâmetro tipo não está definido.";
$MESS["BPABS_WAIT"] = "aguarde...";
$MESS["BPABS_YES"] = "Sim";
$MESS["BPABS_NO_PERMS"] = "Você não tem permissão para iniciar um processo de negócio para este documento. ";
?>