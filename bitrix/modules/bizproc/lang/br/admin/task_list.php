<?
$MESS["BPATL_DESCR"] = "Descrição";
$MESS["BPATL_DESCR_FULL"] = "Descrição Completa";
$MESS["BPATL_F_MODIFIED"] = "Modificado";
$MESS["BPATL_MODIFIED"] = "Modificado";
$MESS["BPATL_F_NAME"] = "Nome";
$MESS["BPATL_NAME"] = "Nome";
$MESS["BPATL_NAV"] = "Tarefas";
$MESS["BPATL_TITLE"] = "Tarefas";
$MESS["BPATL_VIEW"] = "Ver Tarefa";
$MESS["BPATL_WORKFLOW_NAME"] = "Processo de Negócio";
$MESS["BPATL_WORKFLOW_STATE"] = "Status";
$MESS["BPATL_USER"] = "Usuário";
$MESS["BPATL_USER_ID"] = "ID do Usuário";
$MESS["BPATL_USER_NOT_FOUND"] = "O usuário ##USER_ID# não foi encontrado. ";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Selecionado:";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "Verificado:";
$MESS["BPATL_GROUP_ACTION_DELEGATE"] = "Delegar";
?>