<?
$MESS["BPWC_WIC_EMPTY_IBLOCK_TYPE"] = "O modelo do bloco de informação não está especificado.";
$MESS["BPWC_WIC_WRONG_IBLOCK_TYPE"] = "O modelo do bloco de informação especificado nos parâmetros do componente não foi encontrado.";
$MESS["BPWC_WIC_ERROR"] = "Erro";
?>