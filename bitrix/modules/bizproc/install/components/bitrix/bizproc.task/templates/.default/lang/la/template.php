<?
$MESS["BPATL_SUCCESS"] = "Asignación completada.";
$MESS["BPAT_COMMENT"] = "Comentarios";
$MESS["BPAT_GOTO_DOC"] = "Ir al documento";
$MESS["BPATL_DOC_HISTORY"] = "Registro de Business Process";
$MESS["BPATL_COMMENTS"] = "Comentarios";
$MESS["BPATL_TASK_TITLE"] = "Tareas";
$MESS["BPATL_USER_STATUS_YES"] = "Usted aprubó el documento";
$MESS["BPATL_USER_STATUS_NO"] = "Usted rechazó el documento";
$MESS["BPATL_USER_STATUS_OK"] = "Usted ya leyó el documento";
?>