<?
$MESS["BPAT_EMPTY_TASK"] = "Le code de la tâche n'est pas indiqué.";
$MESS["BPAT_TITLE"] = "Tâche";
$MESS["BPAT_NO_TASK"] = "Aucune tâche trouvée.";
$MESS["BPAT_NO_ACCESS"] = "Vous ne pouvez pas voir la tâche sélectionnée.";
$MESS["BPAT_NO_STATE"] = "Le processus d'entreprise est introuvable.";
$MESS["BPAT_DELEGATE_LABEL"] = "Déléguer";
$MESS["BPAT_DELEGATE_SELECT"] = "Sélectionner";
$MESS["BPAT_DELEGATE_CANCEL"] = "Annuler";
?>