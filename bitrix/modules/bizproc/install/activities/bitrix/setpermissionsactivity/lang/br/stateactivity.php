<?
$MESS["BPSA_TRACK1"] = "Permissão para um documento nesta condição #VAL#";
$MESS["BPSA_INVALID_CHILD"] = "Uma 'Atividade de Condição' poderá conter somente estas atividades 'Atividade de Iniciadora de Condição', 'Atividade Finalizadora de Condição' ou 'Atividade Dirigida por um Evento'.";
$MESS["BPSA_EMPTY_PERMS"] = "A permissão para um documento nesta condição não está especificada.";
?>