<?
$MESS["STATEACT_ADD"] = "Adicionar";
$MESS["STATEACT_EDITBP"] = "Editar este processo de negócio";
$MESS["STATEACT_SETT"] = "Configurações do Gerente";
$MESS["STATEACT_DEL"] = "Remover Gerente";
$MESS["STATEACT_BACK"] = "<< Voltar para estados";
$MESS["STATEACT_MENU_COMMAND"] = "Comandar";
$MESS["STATEACT_MENU_DELAY"] = "Adiar Execução";
$MESS["STATEACT_MENU_INIT"] = "Entrada de situação pelo Gerente";
$MESS["STATEACT_MENU_FIN"] = "Saída de situação pelo Gerente";
?>