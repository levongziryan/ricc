<?
$MESS["BPMA_PD_CP"] = "Codage";
$MESS["BPMA_PD_BODY"] = "Texte du message";
$MESS["BPMA_PD_MESS_TYPE"] = "Type de message";
$MESS["BPMA_PD_TO"] = "Adresse du destinataire";
$MESS["BPMA_PD_FROM"] = "Expéditeur";
$MESS["BPMA_PD_SUBJECT"] = "En-tête";
$MESS["BPMA_PD_TEXT"] = "Texte";
$MESS["BPMA_PD_DIRRECT_MAIL"] = "Moyen d'expédition";
$MESS["BPMA_PD_DIRRECT_MAIL_Y"] = "Expédition/envoi en direct";
$MESS["BPMA_PD_DIRRECT_MAIL_N"] = "Envoi par le système de messages";
$MESS["BPMA_PD_MAIL_SITE"] = "Site du modèle de message";
$MESS["BPMA_PD_MAIL_SITE_OTHER"] = "autre";
$MESS["BPMA_PD_MAIL_SEPARATOR"] = "Séparateur d'adresses e-mail";
$MESS["BPMA_PD_FILE"] = "Pièces jointes";
$MESS["BPMA_PD_FILE_DESCRIPTION"] = "Uniquement pour l’envoi via le sous-système de messagerie";
?>