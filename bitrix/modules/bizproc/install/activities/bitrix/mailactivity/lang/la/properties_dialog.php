<?
$MESS["BPMA_PD_CP"] = "Codificación";
$MESS["BPMA_PD_BODY"] = "Texto del Mensaje";
$MESS["BPMA_PD_MESS_TYPE"] = "Tipo de Mensaje";
$MESS["BPMA_PD_TO"] = "Receptor";
$MESS["BPMA_PD_FROM"] = "Remitente";
$MESS["BPMA_PD_SUBJECT"] = "Asunto";
$MESS["BPMA_PD_TEXT"] = "Texto";
$MESS["BPMA_PD_DIRRECT_MAIL"] = "Modo de envío de mensajes";
$MESS["BPMA_PD_DIRRECT_MAIL_Y"] = "Envío directo";
$MESS["BPMA_PD_DIRRECT_MAIL_N"] = "Enviar Mediante Subsistema de Correos (eventos de los medios comprometidos y plantillas)";
$MESS["BPMA_PD_MAIL_SITE"] = "Usar plantillas de mensaje del website";
$MESS["BPMA_PD_MAIL_SITE_OTHER"] = "otros";
$MESS["BPMA_PD_MAIL_SEPARATOR"] = "Separador de direcciones de e-mail";
$MESS["BPMA_PD_FILE"] = "Adjunto";
$MESS["BPMA_PD_FILE_DESCRIPTION"] = "Sólo para enviar a través del subsistema de correo";
?>