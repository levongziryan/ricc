<?
$MESS["BPMA_EMPTY_PROP6"] = "Valeur de la propriété ' Type de message' est incorrecte.";
$MESS["BPMA_EMPTY_PROP4"] = "La propriété 'Encodage de message' n'est pas indiquée.";
$MESS["BPMA_EMPTY_PROP1"] = "Le paramètre 'Expéditeur' n'est pas indiqué.";
$MESS["BPMA_EMPTY_PROP2"] = "La propriété 'Destinataire' n'est pas indiquée.";
$MESS["BPMA_EMPTY_PROP7"] = "Propriété 'Texte du message' n'est pas indiqué.";
$MESS["BPMA_EMPTY_PROP3"] = "La propriété 'Thème du message' n'est pas indiquée.";
$MESS["BPMA_EMPTY_PROP5"] = "La propriété 'Type de message' n'est pas indiqué.";
?>