<?
$MESS["BPSFA_PD_ADD"] = "Adicionar Condição";
$MESS["BPSFA_PD_CALENDAR"] = "Calendário";
$MESS["BPSFA_PD_DELETE"] = "Deletar";
$MESS["BPSFA_PD_NO"] = "Não";
$MESS["BPSFA_PD_YES"] = "Sim";
$MESS["BPSFA_PD_CREATE"] = "Adicicionar Campo";
$MESS["BPSFA_PD_EMPTY_NAME"] = "O campo \"nome\" está faltando";
$MESS["BPSFA_PD_EMPTY_CODE"] = "O campo \"código\" está faltando";
$MESS["BPSFA_PD_WRONG_CODE"] = "O campo código poderá conter somente letras e algarismos.";
$MESS["BPSFA_PD_FIELD"] = "Campo";
$MESS["BPSFA_PD_F_NAME"] = "Nome";
$MESS["BPSFA_PD_F_CODE"] = "ID";
$MESS["BPSFA_PD_F_TYPE"] = "Modelo";
$MESS["BPSFA_PD_F_MULT"] = "Múltiplo";
$MESS["BPSFA_PD_F_REQ"] = "Necessário";
$MESS["BPSFA_PD_F_LIST"] = "Valores";
$MESS["BPSFA_PD_SAVE"] = "Salvar";
$MESS["BPSFA_PD_SAVE_HINT"] = "Criar Campo";
$MESS["BPSFA_PD_CANCEL"] = "Cancelar";
$MESS["BPSFA_PD_CANCEL_HINT"] = "Cancelar";
?>