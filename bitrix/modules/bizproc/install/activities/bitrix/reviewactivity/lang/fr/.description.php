<?
$MESS["BPAR_DESCR_TA1"] = "Introduction automatique";
$MESS["BPAA_DESCR_CM"] = "Commentaire";
$MESS["BPAR_DESCR_NAME"] = "Lecture du document";
$MESS["BPAR_DESCR_DESCR"] = "Etude du document avec la possibilité de laisser un commentaire sur lui";
$MESS["BPAR_DESCR_TC"] = "Nombre de ceux qui doivent être informés";
$MESS["BPAR_DESCR_RC"] = "Combien de personnes en ont pris connaissance";
$MESS["BPAR_DESCR_LR"] = "Dernière lu par";
$MESS["BPAR_DESCR_LR_COMMENT"] = "Commentaire du dernier lecteur";
$MESS["BPAR_DESCR_TASKS"] = "Tâches";
?>