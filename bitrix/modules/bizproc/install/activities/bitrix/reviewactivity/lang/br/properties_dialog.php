<?
$MESS["BPAR_PD_REVIEWERS"] = "Revisores";
$MESS["BPAR_PD_DESCR"] = "Descrição da Tarefa";
$MESS["BPAR_PD_NAME"] = "Nome da Tarefa";
$MESS["BPAR_PD_APPROVE_TYPE"] = "Solicitar leitura por";
$MESS["BPAR_PD_APPROVE_TYPE_ALL"] = "Todos os Colaboradores";
$MESS["BPAR_PD_APPROVE_TYPE_ANY"] = "Qualquer Colaborador";
$MESS["BPAR_PD_SET_STATUS_MESSAGE"] = "Configurar Mensagem de Status";
$MESS["BPAR_PD_YES"] = "Sim";
$MESS["BPAR_PD_NO"] = "Não";
$MESS["BPAR_PD_STATUS_MESSAGE"] = "Texto de Status";
$MESS["BPAR_PD_STATUS_MESSAGE_HINT"] = "Os seguintes macros estão disponíveis:  #PERC# - percentual, #REV# - pessoas cientes atualmente, #TOT# - total de pessoas ainda não cientes.";
$MESS["BPAR_PD_TASK_BUTTON_MESSAGE"] = "Título do botão de tarefas";
$MESS["BPAR_PD_TIMEOUT_DURATION"] = "Período de Exame";
$MESS["BPAR_PD_TIMEOUT_DURATION_HINT"] = "O processo de exame de um documento sertá automaticamente encerrado quando o período de espera terminar. Um valor não preenchido (vazio) ou \"zero\" especifica que não há período de aprovação a ser aplicado. ";
$MESS["BPAR_PD_TIME_D"] = "dias";
$MESS["BPAR_PD_TIME_H"] = "horas";
$MESS["BPAR_PD_TIME_M"] = "minutos";
$MESS["BPAR_PD_TIME_S"] = "segundos";
$MESS["BPAR_PD_STATUS_MESSAGE_HINT1"] = "Os seguintes macros estão disponíveis:  #PERCENT# - relação de conhecimento do item, #REVIEWED# - número de pessoas cientes atualmente, #TOTAL# - total de pessoas necessário , #REVIEWERS# - usuários que visualizaram o item.";
$MESS["BPAR_PD_SHOW_COMMENT"] = "Mostrar campo de entrada de comentários";
$MESS["BPAR_PD_COMMENT_LABEL_MESSAGE"] = "Rótulo de campo de entrada de comentários";
$MESS["BPAR_PD_ACCESS_CONTROL"] = "Mostrar a descrição de atribuição apenas para as pessoas responsáveis";
$MESS["BPAR_PD_TIMEOUT_LIMIT"] = "Tempo mínimo de leitura";
$MESS["BPAR_PD_COMMENT_REQUIRED"] = "É necessário comentário.";
$MESS["BPAR_PD_DELEGATION_TYPE"] = "Delegar para";
?>