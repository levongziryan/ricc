<?
$MESS["BPAR_PD_APPROVE_TYPE_ALL"] = "A tous les employés";
$MESS["BPAR_PD_YES"] = "Oui";
$MESS["BPAR_PD_TIME_D"] = "jours";
$MESS["BPAR_PD_APPROVE_TYPE"] = "Doivent prendre connaissance";
$MESS["BPAR_PD_APPROVE_TYPE_ANY"] = "Tout employé";
$MESS["BPAR_PD_COMMENT_LABEL_MESSAGE"] = "Repère pour le champ du commentaire";
$MESS["BPAR_PD_TIME_M"] = "minutes";
$MESS["BPAR_PD_STATUS_MESSAGE_HINT"] = "Il est possible d'utiliser les modificateurs #PERC# - pourcentage, #REV# - utilisateurs en ayant pris connaissance, #TOT# - utilisateurs à en prendre connaissance au total";
$MESS["BPAR_PD_STATUS_MESSAGE_HINT1"] = "Vous pouvez utiliser les modificateurs #PERCENT# - pourcentage, #REVIEWED# - personnes qui ont pris la connaissance, #TOTAL# - total de personnes qui ont pris la connaissance, #REVIEWERS# - qui a pris la connaissance";
$MESS["BPAR_PD_NAME"] = "Nom de l'objectif";
$MESS["BPAR_PD_NO"] = "Non";
$MESS["BPAR_PD_REVIEWERS"] = "Réviseurs";
$MESS["BPAR_PD_DESCR"] = "Description de la tâche";
$MESS["BPAR_PD_TIMEOUT_DURATION"] = "Limite dans le temps";
$MESS["BPAR_PD_TIMEOUT_DURATION_HINT"] = "A la fin de la période, l'initiation sera automatiquement achevée. Valeur vide ou 0 - absence de période.";
$MESS["BPAR_PD_SHOW_COMMENT"] = "Afficher le champ de saisie d'un commentaire";
$MESS["BPAR_PD_TIME_S"] = "secondes";
$MESS["BPAR_PD_TASK_BUTTON_MESSAGE"] = "Texte du bouton dans la tâche";
$MESS["BPAR_PD_STATUS_MESSAGE"] = "Texte du statut";
$MESS["BPAR_PD_SET_STATUS_MESSAGE"] = "Entrer le texte du statut";
$MESS["BPAR_PD_TIME_H"] = "heures";
$MESS["BPAR_PD_ACCESS_CONTROL"] = "Voir cession description qu'aux personnes responsables";
$MESS["BPAR_PD_TIMEOUT_LIMIT"] = "Temps de lecture minimum";
$MESS["BPAR_PD_COMMENT_REQUIRED"] = "Commentaire obligatoire";
$MESS["BPAR_PD_DELEGATION_TYPE"] = "Déléguer à";
?>