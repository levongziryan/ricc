<?
$MESS["BPAR_ACT_COMMENT"] = "Comentarios";
$MESS["BPAR_ACT_INFO"] = "Completado el #PERC#% (#REV# de #TOT#)";
$MESS["BPAR_ACT_BUTTON2"] = "Listo";
$MESS["BPAR_ACT_REVIEWED"] = "La lectura del documento se ha completado.";
$MESS["BPAR_ACT_TRACK2"] = "El documento ha sido leído por #VAL# usuarios ";
$MESS["BPAR_ACT_PROP_EMPTY4"] = "La propiedad 'Nombre' esta desaparecida.";
$MESS["BPAR_ACT_PROP_EMPTY1"] = "La propiedad 'Usuarios' no esta especificada.";
$MESS["BPAR_ACT_REVIEW_TRACK"] = "El usuario #PERSON# ha leído el documento #COMMENT#";
$MESS["BPAA_ACT_APPROVERS_NONE"] = "ninguno";
$MESS["BPAA_ACT_NO_ACTION"] = "Acción seleccionada inválida.";
$MESS["BPAA_ACT_COMMENT_ERROR"] = "El campo '#COMMENT_LABEL#' es requerido.";
?>