<?
$MESS["BPWHA_HANDLER_NAME"] = "Manejador";
$MESS["BPWHA_EMPTY_TEXT"] = "El manejador no está especificado";
$MESS["BPWHA_HANDLER_WRONG_URL"] = "La URL del manejador es incorrecta";
$MESS["BPWHA_HANDLER_WRONG_PROTOCOL"] = "La URL del protocolo del manejador no es compatible";
?>