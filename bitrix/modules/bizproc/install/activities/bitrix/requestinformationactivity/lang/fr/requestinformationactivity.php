<?
$MESS["BPRIA_ACT_TRACK1"] = "C'est #VAL# qui doit fournir ders renseignements supplémentaires";
$MESS["BPRIA_ACT_INFO"] = "En attendant pour plus d'informations";
$MESS["BPRIA_LOG_Y"] = "Information a été introduite";
$MESS["BPRIA_LOG_COMMENTS"] = "Commentaire";
$MESS["BPRIA_ACT_APPROVE_TRACK"] = "L'utilisateur #PERSON# a ajouté une information supplémentaire #COMMENT#";
$MESS["BPRIA_ACT_COMMENT"] = "Commentaire";
$MESS["BPRIA_ACT_BUTTON1"] = "Sauvegarder";
$MESS["BPRIA_ACT_YES"] = "Oui";
$MESS["BPRIA_ACT_NO"] = "Non";
$MESS["BPRIA_ARGUMENT_NULL"] = "Valeur obligatoire '#PARAM#' non renseignée.";
$MESS["BPRIA_ACT_PROP_EMPTY1"] = "La propriété 'Utilisateurs' n'est pas indiquée.";
$MESS["BPRIA_ACT_PROP_EMPTY4"] = "La propriété 'Nom' n'est pas spécifiée.";
$MESS["BPRIA_ACT_PROP_EMPTY2"] = "Aucun champ n'est indiqué.";
$MESS["BPRIA_ACT_APPROVE"] = "Le document est accepté";
$MESS["BPRIA_ACT_NONAPPROVE"] = "Le document est rejeté.";
$MESS["BPRIA_ACT_NONAPPROVE_TRACK"] = "L'utilisateur #PERSON# a décliné le document #COMMENT#";
$MESS["BPRIA_ACT_BUTTON2"] = "Refuser";
$MESS["BPRIA_ACT_NO_ACTION"] = "L'action disponible n'est pas indiquée.";
$MESS["BPRIA_LOG_N"] = "Refusé";
$MESS["BPRIA_ACT_COMMENT_ERROR"] = "Le champ '#COMMENT_LABEL#'  est obligatoire.";
?>