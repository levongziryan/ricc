<?
$MESS["BPRIA_ACT_TRACK1"] = "Informações adicionais serão apresentadas por #VAL#";
$MESS["BPRIA_ACT_INFO"] = "Aguardando informações adicionais";
$MESS["BPRIA_LOG_Y"] = "Informação obtida";
$MESS["BPRIA_LOG_COMMENTS"] = "Comentar";
$MESS["BPRIA_ACT_APPROVE_TRACK"] = "Um usuário #PERSON# adicionou nova informação #COMMENT#";
$MESS["BPRIA_ACT_COMMENT"] = "Comentar";
$MESS["BPRIA_ACT_BUTTON1"] = "Salvar";
$MESS["BPRIA_ACT_YES"] = "Sim";
$MESS["BPRIA_ACT_NO"] = "Não";
$MESS["BPRIA_ARGUMENT_NULL"] = "O valor requerido ('#PARAM#\") está faltando.";
$MESS["BPRIA_ACT_PROP_EMPTY1"] = "A propriedade \" Usuários\" não está especificada.";
$MESS["BPRIA_ACT_PROP_EMPTY4"] = "A propriedade \"Nome\" está faltando.";
$MESS["BPRIA_ACT_PROP_EMPTY2"] = "Pelo menos um dos campos é necessário. ";
$MESS["BPRIA_ACT_APPROVE"] = "O documento foi aprovado ";
$MESS["BPRIA_ACT_NONAPPROVE"] = "O documento foi recusado. ";
$MESS["BPRIA_ACT_NONAPPROVE_TRACK"] = "O usuário #PERSON# não aprovou o documento #COMMENT#";
$MESS["BPRIA_ACT_BUTTON2"] = "Recusar";
$MESS["BPRIA_ACT_NO_ACTION"] = "Não há ação especificada";
$MESS["BPRIA_LOG_N"] = "Recusado";
$MESS["BPRIA_ACT_COMMENT_ERROR"] = "O campo '#COMMENT_LABEL#' é obrigatório.";
?>