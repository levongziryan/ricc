<?
$MESS["BPSWFA_PD_DOCUMENT_ID"] = "ID do documento";
$MESS["BPSWFA_PD_ENTITY"] = "Entidade";
$MESS["BPSWFA_PD_DOCUMENT_TYPE"] = "Tipo de documento";
$MESS["BPSWFA_PD_TEMPLATE"] = "Modelo";
$MESS["BPSWFA_PD_USE_SUBSCRIPTION"] = "Aguarde o fluxo de trabalho terminar";
$MESS["BPSWFA_PD_ACCESS_DENIED"] = "Apenas os administradores do portal podem acessar propriedades de ação.";
?>