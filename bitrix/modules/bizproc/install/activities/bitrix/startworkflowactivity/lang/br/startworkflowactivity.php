<?
$MESS["BPSWFA_ERROR_DOCUMENT_ID"] = "O ID do documento não está especificado";
$MESS["BPSWFA_ERROR_TEMPLATE"] = "Nenhum modelo selecionado.";
$MESS["BPSWFA_TEMPLATE_PARAMETERS"] = "Parâmetros do fluxo de trabalho";
$MESS["BPSWFA_TEMPLATE_PARAMETERS_ERROR"] = "O parâmetro obrigatório está vazio: #NAME#";
$MESS["BPSWFA_START_ERROR"] = "Erro de inicialização: #MESSAGE#";
$MESS["BPSWFA_SELFSTART_ERROR"] = "O modelo não pode ser executado recursivamente";
$MESS["BPSWFA_ACCESS_DENIED"] = "Apenas os administradores do portal podem acessar propriedades de ação.";
?>