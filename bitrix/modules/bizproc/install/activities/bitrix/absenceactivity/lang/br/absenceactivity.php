<?
$MESS["BPSNMA_EMPTY_ABSENCEUSER"] = "A propriedade \"Usuário\" está faltando.";
$MESS["BPSNMA_EMPTY_ABSENCENAME"] = "A propriedade \" Nome do Evento\" está faltando. ";
$MESS["BPSNMA_EMPTY_ABSENCEFROM"] = "A propriedade \"Data de Início\" está faltando. ";
$MESS["BPSNMA_EMPTY_ABSENCETO"] = "A propriedade \"Data de Finalização\" está faltando.";
$MESS["BPAA2_NO_PERMS"] = "Você não tem permissão escrita para o gráfico de ausências. ";
?>