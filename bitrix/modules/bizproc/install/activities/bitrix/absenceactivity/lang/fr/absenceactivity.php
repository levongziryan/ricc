<?
$MESS["BPSNMA_EMPTY_ABSENCEFROM"] = "Le champ 'Date du début' n'est pas rempli.";
$MESS["BPSNMA_EMPTY_ABSENCETO"] = "La champ 'Date de fin' n'est pas rempli.";
$MESS["BPSNMA_EMPTY_ABSENCENAME"] = "Le champ 'Nom de l'événement' n'est pas rempli.";
$MESS["BPSNMA_EMPTY_ABSENCEUSER"] = "La propriété 'Utilisateur' n'est pas indiquée.";
$MESS["BPAA2_NO_PERMS"] = "Vous n'avez pas  l'autorisation de modifier le tableau des absences.";
?>