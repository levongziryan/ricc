<?
$MESS["BPTA1A_TASKPRIORITY"] = "criticité";
$MESS["BPTA1A_TASKOWNERID"] = "Organisateur";
$MESS["BPTA1A_TASK_TYPE_G"] = "Groupe";
$MESS["BPTA1A_TASKCREATEDBY"] = "Créateur";
$MESS["BPTA1A_TASKNAME"] = "Dénomination de la tâche";
$MESS["BPTA1A_TASKACTIVEFROM"] = "Commencer";
$MESS["BPTA1A_TASKACTIVETO"] = "Achèvement";
$MESS["BPTA1A_TASKDETAILTEXT"] = "Description de la tâche";
$MESS["BPTA1A_TASKASSIGNEDTO"] = "Responsable";
$MESS["BPTA1A_TASK_TYPE_U"] = "Utilisateur";
$MESS["BPTA1A_TASKTRACKERS"] = "???";
$MESS["BPTA1A_TASK_TYPE"] = "Type de la tâche";
$MESS["BPTA1A_TASKFORUM"] = "Forum pour commentaires";
?>