<?
$MESS["BPSNMA_EMPTY_TASKASSIGNEDTO"] = "O valor 'Responsável\" não está especificado.";
$MESS["BPSNMA_EMPTY_TASKNAME"] = "O valor 'Nome da Tarefa' não está especificado.";
$MESS["BPSNMA_EMPTY_TASKPRIORITY"] = "O valor 'Prioridade' não está especificado.";
$MESS["BPSNMA_EMPTY_TASKTYPE"] = "O valor 'Tipo de Tarefa' não está especificado.";
$MESS["BPSNMA_EMPTY_TASKOWNERID"] = "O valor 'Proprietário' não está especificado.";
?>