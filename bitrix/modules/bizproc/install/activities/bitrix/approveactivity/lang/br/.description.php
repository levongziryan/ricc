<?
$MESS["BPAA_DESCR_DESCR"] = "Aprovar Documento";
$MESS["BPAA_DESCR_NAME"] = "Aprovar Documento";
$MESS["BPAA_DESCR_VC"] = "Pessoas Votadas";
$MESS["BPAA_DESCR_TC"] = "Pessoas para Votar";
$MESS["BPAA_DESCR_VP"] = "Classificação de Votos";
$MESS["BPAA_DESCR_AP"] = "Aprovar Classificação";
$MESS["BPAA_DESCR_NAP"] = "Recusar Classificação";
$MESS["BPAA_DESCR_AC"] = "Aprovado";
$MESS["BPAA_DESCR_NAC"] = "Recusado";
$MESS["BPAA_DESCR_LA"] = "ltimo Voto Por";
$MESS["BPAA_DESCR_TA1"] = "Recusar Automaticamente";
$MESS["BPAA_DESCR_APPROVERS"] = "Aprovado Por";
$MESS["BPAA_DESCR_REJECTERS"] = "Rejeitado Por";
$MESS["BPAA_DESCR_CM"] = "Comentar";
$MESS["BPAA_DESCR_LA_COMMENT"] = "Último comentário do eleitor";
$MESS["BPAA_DESCR_TASKS"] = "Tarefas";
?>