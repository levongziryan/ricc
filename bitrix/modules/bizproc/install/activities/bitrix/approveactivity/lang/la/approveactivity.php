<?
$MESS["BPAA_ACT_BUTTON1"] = "Aceptar";
$MESS["BPAA_LOG_Y"] = "Aprobado";
$MESS["BPAA_ACT_COMMENT"] = "Comentarios";
$MESS["BPAA_LOG_COMMENTS"] = "Comentarios";
$MESS["BPAA_ACT_BUTTON2"] = "Rechazar";
$MESS["BPAA_LOG_N"] = "Rechazado";
$MESS["BPAA_ACT_NO_ACTION"] = "Sin acción especificada.";
$MESS["BPAA_ACT_INFO"] = "Participó el #PERC#% (#REV# para #TOT#)";
$MESS["BPAA_ACT_APPROVE"] = "El documento ha sido aprobado";
$MESS["BPAA_ACT_NONAPPROVE"] = "El documento no ha sido aprobado";
$MESS["BPAA_ACT_TRACK2"] = "El documento debe ser aprobado por los #VAL# (todos)";
$MESS["BPAA_ACT_TRACK1"] = "El documento debe ser aprobado por cualquiera de los #VAL#";
$MESS["BPAA_ACT_TRACK3"] = "El documento sera aprobado por el voto de #VAL#";
$MESS["BPAA_ACT_PROP_EMPTY4"] = "La propiedad 'Nombre' no fue encontrada.";
$MESS["BPAA_ACT_PROP_EMPTY2"] = "La propiedad 'Tipo de aprobación' no está especificada.";
$MESS["BPAA_ACT_APPROVE_TRACK"] = "El usuario #PERSON# ha aprobado el documento #COMMENT#";
$MESS["BPAA_ACT_NONAPPROVE_TRACK"] = "El usuario #PERSON# no ha aprobado el documento #COMMENT#";
$MESS["BPAA_ACT_PROP_EMPTY1"] = "La propiedad 'Usuario' no está especificada.";
$MESS["BPAA_ACT_PROP_EMPTY3"] = "El valor de la propiedad 'Tipo de aprobación' es inválida.";
$MESS["BPAA_ACT_APPROVERS_NONE"] = "ninguno";
$MESS["BPAA_ACT_COMMENT_ERROR"] = "El campo '#COMMENT_LABEL#' es requerido.";
?>