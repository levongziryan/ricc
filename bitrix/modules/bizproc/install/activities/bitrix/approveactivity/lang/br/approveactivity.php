<?
$MESS["BPAA_ACT_BUTTON1"] = "Aceitar";
$MESS["BPAA_LOG_Y"] = "Aprovado";
$MESS["BPAA_ACT_COMMENT"] = "Comentários";
$MESS["BPAA_LOG_COMMENTS"] = "Comentários";
$MESS["BPAA_ACT_BUTTON2"] = "Recusar";
$MESS["BPAA_LOG_N"] = "Recusado";
$MESS["BPAA_ACT_NO_ACTION"] = "Não foi especificada uma ação.";
$MESS["BPAA_ACT_INFO"] = "Participaram #PERCENT#% (#VOTED# de #TOTAL#)";
$MESS["BPAA_ACT_APPROVE"] = "O documento foi aprovado";
$MESS["BPAA_ACT_NONAPPROVE"] = "O documento não foi aprovado";
$MESS["BPAA_ACT_TRACK2"] = "O documento deve ser aprovado por todos de #VAL#";
$MESS["BPAA_ACT_TRACK1"] = "O documento deve ser aprovado por qualquer um de #VAL#";
$MESS["BPAA_ACT_TRACK3"] = "O documento será aprovado pelos votos de #VAL#";
$MESS["BPAA_ACT_PROP_EMPTY4"] = "A propriedade Nome\" está faltando.";
$MESS["BPAA_ACT_PROP_EMPTY2"] = "A propriedade \"Tipo de Aprovação\" não está especificada. ";
$MESS["BPAA_ACT_APPROVE_TRACK"] = "O usuário #PERSON# aprovou o documento #COMMENT#";
$MESS["BPAA_ACT_NONAPPROVE_TRACK"] = "O usuário #PERSON# não aprovou o documento #COMMENT#";
$MESS["BPAA_ACT_PROP_EMPTY1"] = "A propriedade \" Usuários\" não está especificada. ";
$MESS["BPAA_ACT_PROP_EMPTY3"] = "O valor da propriedade \" Tipo de Aprovação\" é inválido. ";
$MESS["BPAA_ACT_APPROVERS_NONE"] = "nenhum";
$MESS["BPAA_ACT_COMMENT_ERROR"] = "O campo '#COMMENT_LABEL#' é obrigatório.";
?>