<?
$MESS["BPAA_ACT_BUTTON1"] = "Accepter";
$MESS["BPAA_LOG_Y"] = "Approuvé";
$MESS["BPAA_ACT_COMMENT"] = "Commentaire";
$MESS["BPAA_LOG_COMMENTS"] = "Commentaire";
$MESS["BPAA_ACT_BUTTON2"] = "Refuser";
$MESS["BPAA_LOG_N"] = "Refusé(e)s";
$MESS["BPAA_ACT_NO_ACTION"] = "L'action disponible n'est pas indiquée.";
$MESS["BPAA_ACT_INFO"] = "Voté #PERCENT#% (#VOTED# sur #TOTAL#)";
$MESS["BPAA_ACT_APPROVE"] = "Le document est accepté";
$MESS["BPAA_ACT_NONAPPROVE"] = "Le document est rejeté";
$MESS["BPAA_ACT_TRACK2"] = "Le document doit être accepté par tous les utilisateurs de la liste: #VAL#";
$MESS["BPAA_ACT_TRACK1"] = "Le document doit être accepté par n'importe quel utilisateur de la liste: #VAL#";
$MESS["BPAA_ACT_TRACK3"] = "Le document sera voté par les utilisateurs de la liste: #VAL#";
$MESS["BPAA_ACT_PROP_EMPTY4"] = "La propriété 'Nom' n'est pas spécifiée.";
$MESS["BPAA_ACT_PROP_EMPTY2"] = "La propriété 'Type d'approbation' n'est pas indiquée.";
$MESS["BPAA_ACT_APPROVE_TRACK"] = "L'utilisateur #PERSON# a approuvé le document#COMMENT#";
$MESS["BPAA_ACT_NONAPPROVE_TRACK"] = "L'utilisateur #PERSON# a décliné le document #COMMENT#";
$MESS["BPAA_ACT_PROP_EMPTY1"] = "La propriété 'Utilisateurs' n'est pas indiquée.";
$MESS["BPAA_ACT_PROP_EMPTY3"] = "La valeur de la propriété 'Type de validation' n'est pas correcte.";
$MESS["BPAA_ACT_APPROVERS_NONE"] = "aucun";
$MESS["BPAA_ACT_COMMENT_ERROR"] = "Le champ '#COMMENT_LABEL#'  est obligatoire.";
?>