<?
$MESS["SEQWF_END"] = "Achèvement";
$MESS["SEQWF_BEG"] = "Commencer";
$MESS["SEQWF_CONF"] = "L'action choisie sera enlevée de la liste de vos actions. Continuer?";
$MESS["SEQWF_SNIP_NAME"] = "Dénomination:";
$MESS["SEQWF_SNIP_DEL"] = "Supprimer cette action";
$MESS["SEQWF_SNIP_TITLE"] = "Propriétés de l'action sélectionnée";
$MESS["SEQWF_SNIP_DD"] = "Faites glisser ici cette action pour l'enregistrer";
$MESS["SEQWF_SNIP"] = "Mes actions";
?>