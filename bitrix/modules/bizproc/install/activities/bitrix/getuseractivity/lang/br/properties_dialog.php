<?
$MESS["BPCRU_PD_TYPE"] = "Tipo";
$MESS["BPCRU_PD_USER_RANDOM"] = "De Usuários";
$MESS["BPCRU_PD_USER_BOSS"] = "Para Usuários";
$MESS["BPCRU_PD_USER1"] = "Usuários Reservados";
$MESS["BPCRU_PD_MAX_LEVEL"] = "Nível de Supervisor (quanto mais alto, maior)";
$MESS["BPCRU_PD_TYPE_RANDOM"] = "qualquer";
$MESS["BPCRU_PD_TYPE_BOSS"] = "supervisor";
$MESS["BPCRU_PD_MAX_LEVEL_1"] = "1 (supervisor imediato)";
$MESS["BPCRU_PD_SKIP_ABSENT"] = "Pular ausentes";
$MESS["BPCRU_PD_YES"] = "Sim";
$MESS["BPCRU_PD_NO"] = "Não";
$MESS["BPCRU_PD_TYPE_ORDER"] = "sequencial";
$MESS["BPCRU_PD_SKIP_TIMEMAN"] = "Pular funcionários desmarcados";
?>