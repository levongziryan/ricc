<?
$MESS["BPCRU_PD_MAX_LEVEL_1"] = "1 (supérieur immédiat)";
$MESS["BPCRU_PD_YES"] = "Oui";
$MESS["BPCRU_PD_USER_BOSS"] = "Pour l'utilisateur";
$MESS["BPCRU_PD_USER_RANDOM"] = "Des utilisateurs";
$MESS["BPCRU_PD_TYPE_BOSS"] = "approbateur";
$MESS["BPCRU_PD_NO"] = "Non";
$MESS["BPCRU_PD_SKIP_ABSENT"] = "Ignorer les absents";
$MESS["BPCRU_PD_USER1"] = "Utilisateurs en réserve";
$MESS["BPCRU_PD_TYPE_RANDOM"] = "aléatoire";
$MESS["BPCRU_PD_TYPE"] = "Entité";
$MESS["BPCRU_PD_MAX_LEVEL"] = "Niveau du responsable (plus la valeur est importante, plus haut il se situe dans la hiérarchie)";
$MESS["BPCRU_PD_TYPE_ORDER"] = "séquentiel";
$MESS["BPCRU_PD_SKIP_TIMEMAN"] = "Passer les employés vérifiés";
?>