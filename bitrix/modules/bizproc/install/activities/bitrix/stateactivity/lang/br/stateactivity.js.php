<?
$MESS["STATEACT_BACK"] = "<< Voltar para Status";
$MESS["STATEACT_ADD"] = "Adicionar";
$MESS["STATEACT_MENU_COMMAND"] = "Comandar";
$MESS["STATEACT_MENU_DELAY"] = "Adiar Execução";
$MESS["STATEACT_EDITBP"] = "Editar este processo de negócio";
$MESS["STATEACT_SETT"] = "Configurações de Gerente";
$MESS["STATEACT_DEL"] = "Remover Gerente";
$MESS["STATEACT_MENU_INIT"] = "Status de Entrada do Gerente";
$MESS["STATEACT_MENU_FIN"] = "Status de Saída do Gerente";
?>