<?
$MESS["BPWA_INVALID_CONDITION_TYPE"] = "Le type de la condition n'est pas retrouvé.";
$MESS["BPWA_NO_CONDITION"] = "La condition n'est pas indiquée.";
$MESS["BPWA_CYCLE_LIMIT"] = "Maximum d'itération de la boucle dépassé";
$MESS["BPWA_CONDITION_NOT_SET"] = "Condition de la boucle non spécifiée";
?>