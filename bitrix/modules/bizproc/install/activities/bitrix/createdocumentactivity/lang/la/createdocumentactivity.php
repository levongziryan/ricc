<?
$MESS["BPCDA_WRONG_TYPE"] = "El tipo de parámetro '#PARAM#' no está definido";
$MESS["BPCDA_FIELD_REQUIED"] = "El campo '#FIELD#' es requerido.";
$MESS["BPCDA_RECURSION_ERROR"] = "El intento de ejecutar la acción de forma recurrente fue bloqueada";
?>