<?
$MESS["BPCDA_FIELD_REQUIED"] = "Le champ '#FIELD#' doit être rempli.";
$MESS["BPCDA_WRONG_TYPE"] = "Le type du paramètre '#PARAM#' n'est pas défini.";
$MESS["BPCDA_RECURSION_ERROR"] = "La tentative d'exécution de l'action de manière récursive a été bloquée";
?>