<?
$MESS["BPRIOA_ACT_TRACK1"] = "#VAL# doit fournir des informations supplémentaires";
$MESS["BPRIOA_ACT_INFO"] = "En attente d'informations supplémentaires";
$MESS["BPRIOA_LOG_COMMENTS"] = "Commentaire";
$MESS["BPRIOA_ACT_APPROVE_TRACK"] = "L'utilisateur #PERSON# a fourni des informations supplémentaires #COMMENT#";
$MESS["BPRIOA_ACT_CANCEL_TRACK"] = "L'utilisateur #PERSON# a annulé les informations supplémentaires #COMMENT#";
$MESS["BPRIOA_ACT_COMMENT"] = "Commentaire";
$MESS["BPRIOA_ACT_BUTTON1"] = "Enregistrer";
$MESS["BPRIOA_ARGUMENT_NULL"] = "Valeur obligatoire '#PARAM#' non renseignée.";
$MESS["BPRIOA_ACT_PROP_EMPTY1"] = "La propriété 'Utilisateurs' n'est pas indiquée.";
$MESS["BPRIOA_ACT_PROP_EMPTY4"] = "La propriété 'Nom' n'est pas spécifiée.";
$MESS["BPRIOA_ACT_PROP_EMPTY2"] = "Au moins un champ est obligatoire.";
$MESS["BPRIOA_ACT_BUTTON2"] = "Refuser";
$MESS["BPRIOA_ACT_COMMENT_ERROR"] = "Le champ '#COMMENT_LABEL#'  est obligatoire.";
?>