<?
$MESS["TIMEMAN_NS_REPORT"] = "Neuer Eintrag im Bericht über die Arbeitszeit";
$MESS["TIMEMAN_NS_REPORT_COMMENT"] = "Kommentar zum Bericht über die Arbeitszeit";
$MESS["TIMEMAN_NS_REPORT_APPROVE"] = "Bestätigung der Änderung im Bericht über die Arbeitszeit";
$MESS["TIMEMAN_NS_ENTRY"] = "Neuer Arbeitsbericht";
$MESS["TIMEMAN_NS_ENTRY_COMMENT"] = "Kommentare zum neuen Arbeitsbericht";
$MESS["TIMEMAN_NS_ENTRY_APPROVE"] = "Arbeitsbericht wurde bestätigt/Bestätigung wurde rückgängig gemacht";
?>