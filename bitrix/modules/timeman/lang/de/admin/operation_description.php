<?
$MESS["OP_NAME_TM_MANAGE"] = "Arbeitsbeginn und Arbeitsende";
$MESS["OP_NAME_TM_MANAGE_ALL"] = "Arbeitsbeginn und Arbeitsende aller Mitarbeiter";
$MESS["OP_NAME_TM_READ_SUBORDINATE"] = "Durch die Position bestimmte Leserechte";
$MESS["OP_NAME_TM_READ"] = "Alle Einträge lesen";
$MESS["OP_NAME_TM_WRITE_SUBORDINATE"] = "Durch die Position bestimmte Bearbeitungsrechte";
$MESS["OP_NAME_TM_WRITE"] = "Alle Einträge bearbeiten";
$MESS["OP_NAME_TM_SETTINGS"] = "Moduleinstellungen bearbeiten";
$MESS["OP_DESC_TM_MANAGE"] = "Kontrolle über persönliche Arbeitszeit";
$MESS["OP_DESC_TM_MANAGE_ALL"] = "Kontrolle über die Arbeitszeit aller Mitarbeiters";
$MESS["OP_DESC_TM_READ_SUBORDINATE"] = "Durch die Position bestimmter Zugriff auf Berichte";
$MESS["OP_DESC_TM_READ"] = "Zugriff auf alle Berichte";
$MESS["OP_DESC_TM_WRITE_SUBORDINATE"] = "Durch die Position bestimmter Zugriff auf Bearbeitung und Bestätigung";
$MESS["OP_DESC_TM_WRITE"] = "Zugriff auf Bearbeitung und Bestätigung aller Einträge";
$MESS["OP_DESC_TM_SETTINGS"] = "Moduleinstellungen bearbeiten";
?>