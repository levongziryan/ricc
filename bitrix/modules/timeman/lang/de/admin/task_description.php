<?
$MESS["TASK_NAME_TIMEMAN_DENIED"] = "Zugriff verweigert";
$MESS["TASK_NAME_TIMEMAN_SUBORDINATE"] = "Leiter oder Mitarbeiter der Abteilung";
$MESS["TASK_NAME_TIMEMAN_READ"] = "Personalabteilung";
$MESS["TASK_NAME_TIMEMAN_WRITE"] = "Unternehmensvorstand";
$MESS["TASK_NAME_TIMEMAN_FULL_ACCESS"] = "Voller Zugriff";
$MESS["TASK_DESC_TIMEMAN_DENIED"] = "Zugriff auf den Bereich Arbeitszeitmanagement ist verweigert";
$MESS["TASK_DESC_TIMEMAN_SUBORDINATE"] = "Durch die Position bestimmter Zugriff";
$MESS["TASK_DESC_TIMEMAN_READ"] = "Voller Lesezugriff, durch die Position bestimmter Schreibzugriff";
$MESS["TASK_DESC_TIMEMAN_WRITE"] = "Voller Lese- und Schreibzugriff";
$MESS["TASK_DESC_TIMEMAN_FULL_ACCESS"] = "Voller Zugriff auf den Bereich Arbeitszeitmanagement";
?>