<?
$MESS["TM_FIELD_UF_TIMEMAN"] = "Arbeitszeitmanagement";
$MESS["TM_FIELD_UF_TIMEMAN_Y"] = "Aktivieren";
$MESS["TM_FIELD_UF_TIMEMAN_N"] = "Deaktivieren";
$MESS["TM_FIELD_UF_TM_MAX_START"] = "Arbeitsbeginn spätestens um";
$MESS["TM_FIELD_UF_TM_MIN_FINISH"] = "Arbeitsende frühestens um";
$MESS["TM_FIELD_UF_TM_MIN_DURATION"] = "Minimale Arbeitstagdauer";
$MESS["TM_FIELD_UF_TM_REPORT_REQ"] = "Tagesbericht";
$MESS["TM_FIELD_UF_TM_REPORT_REQ_Y"] = "erforderlich";
$MESS["TM_FIELD_UF_TM_REPORT_REQ_N"] = "nicht erforderlich";
$MESS["TM_FIELD_UF_TM_REPORT_REQ_A"] = "Tagesberichte nicht anzeigen";
$MESS["TM_FIELD_UF_TM_REPORT_TPL"] = "Berichtsvorlagen";
$MESS["TM_FIELD_UF_TM_FREE"] = "Flexible Arbeitszeiten";
$MESS["TM_FIELD_UF_TM_FREE_Y"] = "aktivieren";
$MESS["TM_FIELD_UF_TM_FREE_N"] = "deaktivieren";
$MESS["TM_FIELD_UF_TM_NOVALUE"] = "(erben)";
$MESS["TM_FIELD_UF_TM_DAY"] = "Tag";
$MESS["TM_FIELD_UF_TM_WEEK"] = "Woche";
$MESS["TM_FIELD_UF_TM_MONTH"] = "Monat";
$MESS["TM_FIELD_UF_LAST_REPORT_DATE"] = "Datum des letzten Berichts";
$MESS["TM_FIELD_UF_DELAY_TIME"] = "Verschobene Berichtszeit";
$MESS["TM_FIELD_UF_TM_REPORT_DATE"] = "Tag des Monats";
$MESS["TM_FIELD_UF_TM_TIME"] = "Termin der Berichtsabgabe";
$MESS["TM_FIELD_UF_REPORT_PERIOD"] = "Berichtsturnus";
$MESS["TM_FIELD_UF_SETTING_DATE"] = "Einstellungen gespeichert am";
$MESS["TM_FIELD_UF_NONE"] = "Bericht ist nicht erforderlich";
$MESS["TM_FIELD_UF_TM_ALLOWED_DELTA"] = "Erlaubter Zeitabstand für Zeiteinstellungen";
?>