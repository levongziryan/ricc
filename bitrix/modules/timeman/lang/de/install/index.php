<?
$MESS["TIMEMAN_MODULE_NAME"] = "Arbeitszeitmanagement";
$MESS["TIMEMAN_MODULE_DESCRIPTION"] = "Ein Modul zur Nachverfolgung und Kontrolle über die Arbeitszeit.";
$MESS["TIMEMAN_PHP_L439"] = "Sie benutzen die PHP-Version #VERS#, während das Modul die Version ab 5.0.0 erfordert. Aktualisieren Sie bitte Ihre PHP-Installation oder wenden Sie sich an den Technischen Support.";
$MESS["TIMEMAN_INSTALL_TITLE"] = "Installation des Moduls Arbeitszeitmanagement";
$MESS["TIMEMAN_UNINSTALL_TITLE"] = "Deinstallation des Moduls Arbeitszeitmanagement";
?>