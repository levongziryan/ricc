<?
$MESS["TM_FIELD_UF_TM_NOVALUE"] = "(hériter la valeur)";
$MESS["TM_FIELD_UF_TIMEMAN_Y"] = "activé";
$MESS["TM_FIELD_UF_TM_FREE_Y"] = "activé";
$MESS["TM_FIELD_UF_TM_TIME"] = "Délai de livraison du rapport";
$MESS["TM_FIELD_UF_TM_FREE_N"] = "désactivé";
$MESS["TM_FIELD_UF_LAST_REPORT_DATE"] = "Date du dernier rapport";
$MESS["TM_FIELD_UF_SETTING_DATE"] = "Date d'installation de paramètres";
$MESS["TM_FIELD_UF_TM_DAY"] = "Jour";
$MESS["TM_FIELD_UF_TM_ALLOWED_DELTA"] = "Intervalle admissible de modification du temps";
$MESS["TM_FIELD_UF_TM_MAX_START"] = "Temps max du commencement";
$MESS["TM_FIELD_UF_TM_MONTH"] = "Mois";
$MESS["TM_FIELD_UF_TM_MIN_DURATION"] = "Durée minimale";
$MESS["TM_FIELD_UF_TM_MIN_FINISH"] = "Temps min de l'achèvement";
$MESS["TM_FIELD_UF_TIMEMAN_N"] = "désactivé";
$MESS["TM_FIELD_UF_TM_REPORT_REQ_N"] = "pas nécessaire";
$MESS["TM_FIELD_UF_TM_REPORT_REQ_A"] = "ne pas afficher le formulaire du rapport";
$MESS["TM_FIELD_UF_TM_WEEK"] = "Semaine";
$MESS["TM_FIELD_UF_TM_REPORT_REQ_Y"] = "obligatoire";
$MESS["TM_FIELD_UF_DELAY_TIME"] = "Le temps du rapport est reporté";
$MESS["TM_FIELD_UF_TM_REPORT_REQ"] = "Rapport pour une journée";
$MESS["TM_FIELD_UF_NONE"] = "Aucun rapport exigé";
$MESS["TM_FIELD_UF_TM_FREE"] = "Horaire flexible";
$MESS["TM_FIELD_UF_TIMEMAN"] = "Gestion du temps de travail";
$MESS["TM_FIELD_UF_REPORT_PERIOD"] = "Périodicité de présentation du rapport";
$MESS["TM_FIELD_UF_TM_REPORT_DATE"] = "Jour du mois";
$MESS["TM_FIELD_UF_TM_REPORT_TPL"] = "Modèles de rapport";
?>