<?
$MESS["TIMEMAN_PHP_L439"] = "Vous utilisez la version PHP #VERS#, pour fonctionner le module a besoin d'une version 5.0.0 ou plus récente. Veuillez mettre à jour PHP ou contactez le service d'appui technique de votre hébergement.";
$MESS["TIMEMAN_MODULE_DESCRIPTION"] = "Module pour contrôler le temps de travail.";
$MESS["TIMEMAN_UNINSTALL_TITLE"] = "Suppression du module 'Gestion du temps de travail'";
$MESS["TIMEMAN_INSTALL_TITLE"] = "Installation du module 'Gestion du temps de travail'";
$MESS["TIMEMAN_MODULE_NAME"] = "Gestion du temps de travail";
?>