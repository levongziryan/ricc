<?
$MESS["TIMEMAN_NS_ENTRY"] = "Ajout d'un rapport de travail";
$MESS["TIMEMAN_NS_REPORT_COMMENT"] = "Commentaires au compte-rendu sur le temps de travail";
$MESS["TIMEMAN_NS_ENTRY_COMMENT"] = "Commentaires sur le rapport du travail";
$MESS["TIMEMAN_NS_REPORT"] = "Nouvelles inscriptions dans le rapport sur le temps de travail";
$MESS["TIMEMAN_NS_REPORT_APPROVE"] = "Confirmation de changement de l'enregistrement dans le rapport sur le temps de travail";
$MESS["TIMEMAN_NS_ENTRY_APPROVE"] = "Confirmation/annulation de la validation du rapport de travail";
?>