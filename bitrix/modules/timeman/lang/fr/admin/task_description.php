<?
$MESS["TASK_DESC_TIMEMAN_SUBORDINATE"] = "L'accès en conformité avec les paramètres de subordination";
$MESS["TASK_NAME_TIMEMAN_DENIED"] = "Accès interdit";
$MESS["TASK_DESC_TIMEMAN_DENIED"] = "L'accès au fonctions pour calcul du temps de travail est interdit";
$MESS["TASK_DESC_TIMEMAN_READ"] = "Lire l'article complet access, subordination controlee acces en ecriture";
$MESS["TASK_DESC_TIMEMAN_WRITE"] = "Forme détaillée";
$MESS["TASK_NAME_TIMEMAN_READ"] = "Service des Ressources Humaines";
$MESS["TASK_NAME_TIMEMAN_FULL_ACCESS"] = "Accès complet";
$MESS["TASK_DESC_TIMEMAN_FULL_ACCESS"] = "Accès absolu au module du contrôle du temps de travail";
$MESS["TASK_NAME_TIMEMAN_WRITE"] = "Tutoriel";
$MESS["TASK_NAME_TIMEMAN_SUBORDINATE"] = "Employé ou chef de service";
?>