<?
$MESS["OP_DESC_TM_MANAGE"] = "Possibilité de gérer personellement le temps de travail";
$MESS["OP_DESC_TM_READ_SUBORDINATE"] = "L'accès aux rapports conformément aux réglages de la subordination";
$MESS["OP_DESC_TM_READ"] = "Accès à tous les rapports";
$MESS["OP_DESC_TM_SETTINGS"] = "Accès à la modification des réglages du module";
$MESS["OP_DESC_TM_WRITE"] = "Accès pour l'édition et la confirmation de tous commentaires";
$MESS["OP_DESC_TM_WRITE_SUBORDINATE"] = "Accès à l'édition et la validation des documents en conformité avec les paramètres de subordination";
$MESS["OP_NAME_TM_SETTINGS"] = "Accès à la modification des réglages du module";
$MESS["OP_NAME_TM_MANAGE"] = "Ouverture / fermeture de la journée de travail";
$MESS["OP_NAME_TM_MANAGE_ALL"] = "Ouverture/fermeture d'une journée de travail d'autrui";
$MESS["OP_NAME_TM_WRITE_SUBORDINATE"] = "Modifier conformément aux paramètres de subordination";
$MESS["OP_NAME_TM_WRITE"] = "Edition de tous les enregistrements";
$MESS["OP_DESC_TM_MANAGE_ALL"] = "Gestion du contrôle du temps de travail d'un autre employé";
$MESS["OP_NAME_TM_READ_SUBORDINATE"] = "Lecture en fonction de paramètres de subordination";
$MESS["OP_NAME_TM_READ"] = "Lecture de toutes les notes";
?>