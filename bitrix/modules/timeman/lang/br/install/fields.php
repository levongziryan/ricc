<?
$MESS["TM_FIELD_UF_TIMEMAN"] = "Gestão do tempo de trabalho";
$MESS["TM_FIELD_UF_TIMEMAN_Y"] = "habilitar";
$MESS["TM_FIELD_UF_TIMEMAN_N"] = "desabilitar";
$MESS["TM_FIELD_UF_TM_MAX_START"] = "Ultimo ponto(entrada)";
$MESS["TM_FIELD_UF_TM_MIN_FINISH"] = "Ponto(saida) mais recente";
$MESS["TM_FIELD_UF_TM_MIN_DURATION"] = "Duração mínima do dia de trabalho";
$MESS["TM_FIELD_UF_TM_REPORT_REQ"] = "Relatório Diário";
$MESS["TM_FIELD_UF_TM_REPORT_REQ_Y"] = "exigido";
$MESS["TM_FIELD_UF_TM_REPORT_REQ_N"] = "não é exigido";
$MESS["TM_FIELD_UF_TM_REPORT_REQ_A"] = "Não mostrar relatórios de fomulário";
$MESS["TM_FIELD_UF_TM_REPORT_TPL"] = "Modelos de Relatórios";
$MESS["TM_FIELD_UF_TM_FREE"] = "Horário de trabalho flexível";
$MESS["TM_FIELD_UF_TM_FREE_Y"] = "habilitar";
$MESS["TM_FIELD_UF_TM_FREE_N"] = "desabilitar";
$MESS["TM_FIELD_UF_TM_NOVALUE"] = "(Herdar)";
$MESS["TM_FIELD_UF_TM_DAY"] = "Dia";
$MESS["TM_FIELD_UF_TM_WEEK"] = "Semana";
$MESS["TM_FIELD_UF_TM_MONTH"] = "Mês";
$MESS["TM_FIELD_UF_LAST_REPORT_DATE"] = "Data do Último relatório";
$MESS["TM_FIELD_UF_DELAY_TIME"] = "Relatório de tempo adiado";
$MESS["TM_FIELD_UF_TM_REPORT_DATE"] = "Dia do mês";
$MESS["TM_FIELD_UF_TM_TIME"] = "Relatório que deverá ser";
$MESS["TM_FIELD_UF_REPORT_PERIOD"] = "Período de relatório";
$MESS["TM_FIELD_UF_SETTING_DATE"] = "Configurações salvas em";
$MESS["TM_FIELD_UF_NONE"] = "Relatório não é necessário";
$MESS["TM_FIELD_UF_TM_ALLOWED_DELTA"] = "Intervalo de ajuste de tempo permitido";
?>