<?
$MESS["OP_NAME_TM_MANAGE"] = "Ponto(entrada) e Ponto(saída)";
$MESS["OP_NAME_TM_MANAGE_ALL"] = "Ninguém bateu o Ponto(entrada) e Ponto (saída)";
$MESS["OP_NAME_TM_READ_SUBORDINATE"] = "Ler subordinação controlada";
$MESS["OP_NAME_TM_READ"] = "Ler todos os registros";
$MESS["OP_NAME_TM_WRITE_SUBORDINATE"] = "Editar subordinação controlada";
$MESS["OP_NAME_TM_WRITE"] = "Editar todos os registros";
$MESS["OP_NAME_TM_SETTINGS"] = "Editar configurações do módulo";
$MESS["OP_DESC_TM_MANAGE"] = "Controle de Tempo de trabalho pessoal";
$MESS["OP_DESC_TM_MANAGE_ALL"] = "Controle de tempo de trabalho de qualquer um";
$MESS["OP_DESC_TM_READ_SUBORDINATE"] = "Relatório de acesso a subordinação controlada";
$MESS["OP_DESC_TM_READ"] = "Todos os relatórios de acesso";
$MESS["OP_DESC_TM_WRITE_SUBORDINATE"] = "Editar e confirmar acesso de subordinação controlada";
$MESS["OP_DESC_TM_WRITE"] = "Editar qualquer registro de confirmação de acesso";
$MESS["OP_DESC_TM_SETTINGS"] = "Editar configurações do módulo";
?>