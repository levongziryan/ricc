<?
$MESS["TIMEMAN_MODULE_DESCRIPTION"] = "El módulo para rastrear y controlar el tiempo de trabajo.";
$MESS["TIMEMAN_INSTALL_TITLE"] = "Instalación del módulo del Manejo del tiempo de trabajo";
$MESS["TIMEMAN_UNINSTALL_TITLE"] = "Desinstalación del módulo de Manejo del trabajo ";
$MESS["TIMEMAN_MODULE_NAME"] = "Administración del tiempo de trabajo";
$MESS["TIMEMAN_PHP_L439"] = "Usted está usando una versión PHP #VERS# mientras el módulo requiere la versión 5.0.0 o una mayor. Por favor actualize la instalación de su PHP o contacte a soporte técnico.";
?>