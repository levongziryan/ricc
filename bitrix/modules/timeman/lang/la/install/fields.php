<?
$MESS["TM_FIELD_UF_TIMEMAN"] = "Administración del tiempo de trabajo";
$MESS["TM_FIELD_UF_TIMEMAN_Y"] = "permitir";
$MESS["TM_FIELD_UF_TIMEMAN_N"] = "deshabilitar";
$MESS["TM_FIELD_UF_TM_MIN_DURATION"] = "Mínima duración del día de trabajo";
$MESS["TM_FIELD_UF_TM_REPORT_REQ"] = "Reporte Diario";
$MESS["TM_FIELD_UF_TM_REPORT_REQ_Y"] = "requerido";
$MESS["TM_FIELD_UF_TM_REPORT_REQ_N"] = "no requerido";
$MESS["TM_FIELD_UF_TM_REPORT_REQ_A"] = "no mostrar formulario de reporte";
$MESS["TM_FIELD_UF_TM_REPORT_TPL"] = "Plantillas del reporte";
$MESS["TM_FIELD_UF_TM_FREE"] = "Calendario de trabajo flexible";
$MESS["TM_FIELD_UF_TM_FREE_Y"] = "habilitar";
$MESS["TM_FIELD_UF_TM_FREE_N"] = "deshabilitar";
$MESS["TM_FIELD_UF_TM_NOVALUE"] = "(inherente)";
$MESS["TM_FIELD_UF_TM_DAY"] = "Día";
$MESS["TM_FIELD_UF_TM_WEEK"] = "Semana";
$MESS["TM_FIELD_UF_TM_MONTH"] = "Mes";
$MESS["TM_FIELD_UF_LAST_REPORT_DATE"] = "Último día de reporte";
$MESS["TM_FIELD_UF_DELAY_TIME"] = "hora diferida del reporte";
$MESS["TM_FIELD_UF_TM_REPORT_DATE"] = "Día del mes";
$MESS["TM_FIELD_UF_REPORT_PERIOD"] = "Reportar período";
$MESS["TM_FIELD_UF_SETTING_DATE"] = "Las configuraciones se guardaron en";
$MESS["TM_FIELD_UF_TM_MAX_START"] = "Última hora posible de marcado de entrada";
$MESS["TM_FIELD_UF_TM_MIN_FINISH"] = "Primera hora posible de marcado de salida";
$MESS["TM_FIELD_UF_TM_TIME"] = "El reporte es debido a";
$MESS["TM_FIELD_UF_NONE"] = "Informe no es necesario";
$MESS["TM_FIELD_UF_TM_ALLOWED_DELTA"] = "Intervalo de ajuste de tiempo permitido";
?>