<?
$MESS["REPORT_FROM"] = "De";
$MESS["REPORT_TO"] = "Para";
$MESS["REPORT_PERIOD"] = "Reporte del ";
$MESS["REPORT_TITLE"] = "Reportes de trabajo";
$MESS["REPORT_TITLE_FOR_MAIL"] = "reportes de trabajo";
$MESS["REPORT_WORK_REPORT"] = "un reporte de trabajo";
$MESS["REPORT_TITLE2"] = "Reporte de trabajo de ";
$MESS["REPORT_DONE"] = "Reporte creado";
$MESS["REPORT_CHANGE"] = "modificado";
$MESS["REPORT_CHANGE_W"] = "modificado";
$MESS["REPORT_CHANGE_24_M"] = "Modificado";
$MESS["REPORT_CHANGE_24_F"] = "Modificado";
$MESS["REPORT_CHANGE_24"] = "Modificado";
$MESS["REPORT_ADD"] = "enviado a";
$MESS["REPORT_ADD_W"] = "enviado a";
$MESS["REPORT_ADD_24_M"] = "enviado a";
$MESS["REPORT_ADD_24_F"] = "enviado a";
$MESS["REPORT_ADD_24"] = "enviado a";
$MESS["REPORT_APPROVE"] = "reporte confirmado";
$MESS["REPORT_APPROVE_SIMPLE"] = "reportes";
$MESS["REPORT_NEW_COMMENT"] = "Un comentario agregado a ";
$MESS["REPORT_WITH_G"] = "puntaje <span class='tm-mark-log-G'>positivo</span>";
$MESS["REPORT_WITH_X"] = "<span class='tm-mark-log-X'>not confirmado</span>";
$MESS["REPORT_WITH_B"] = "puntaje <span class='tm-mark-log-B'>negativo</span>";
$MESS["REPORT_WITH_N"] = "<span class='tm-mark-log-N'>sin puntaje</span>";
$MESS["COMMENT_AUTHOR"] = "Autor:";
$MESS["COMMENT_TEXT"] = "Comentario:";
$MESS["REPORT_FULL_COMMENT_CONFIRM_W_MARK"] = "Informe confirmado, marcado #VALUE#";
$MESS["REPORT_FULL_COMMENT_CONFIRM_WO_MARK"] = "Informe confirmado, sin marcar";
$MESS["REPORT_FULL_COMMENT_CONFIRM_VALUE_G"] = "positivo";
$MESS["REPORT_FULL_COMMENT_CONFIRM_VALUE_B"] = "negativo";
$MESS["REPORT_FULL_COMMENT_CONFIRM_MOBILE"] = "Informe confirmado";
$MESS["REPORT_FULL_COMMENT_CONFIRM_MOBILE_VALUE_G"] = "resultados positivos";
$MESS["REPORT_FULL_COMMENT_CONFIRM_MOBILE_VALUE_B"] = "resultados negativos";
$MESS["REPORT_FULL_IM_ADD_M"] = "agregó un informe de trabajo para #period#";
$MESS["REPORT_FULL_IM_ADD_F"] = "agregar un informe de trabajo de #period#";
$MESS["REPORT_FULL_IM_ADD"] = "agregar un informe de trabajo del #period#";
$MESS["REPORT_FULL_IM_COMMENT_1_M"] = "comentarios en su informe de trabajo del #period#";
$MESS["REPORT_FULL_IM_COMMENT_1_F"] = "comentarios en su informe de trabajo del #period#";
$MESS["REPORT_FULL_IM_COMMENT_1"] = "comentarios en su informe de trabajo del #period#";
$MESS["REPORT_FULL_IM_COMMENT_2_M"] = "comentarios de su informe de trabajo del #period#";
$MESS["REPORT_FULL_IM_COMMENT_2_F"] = "comentarios de su informe de trabajo del #period#";
$MESS["REPORT_FULL_IM_COMMENT_2"] = "comentarios de su informe de trabajo del #period#";
$MESS["REPORT_FULL_IM_COMMENT_3_M"] = "comentarios sobre el informe de trabajo del #period#";
$MESS["REPORT_FULL_IM_COMMENT_3_F"] = "comentarios sobre el informe de trabajo del #period#";
$MESS["REPORT_FULL_IM_COMMENT_3"] = "comentarios sobre el informe de trabajo del #period#";
$MESS["REPORT_FULL_IM_APPROVE_M_G"] = "confirmado su informe de trabajo para #period#, puntaje positivo";
$MESS["REPORT_FULL_IM_APPROVE_M_B"] = "confirmado su informe de trabajo para #period#, puntaje negativo";
$MESS["REPORT_FULL_IM_APPROVE_M_N"] = "confirmado su informe de trabajo para #period#, sin calificación";
$MESS["REPORT_FULL_IM_APPROVE_F_G"] = "confirmado su informe de trabajo para #period#, puntaje positivo";
$MESS["REPORT_FULL_IM_APPROVE_F_B"] = "confirmado su informe de trabajo para #period#, puntaje negativo";
$MESS["REPORT_FULL_IM_APPROVE_F_N"] = "confirmado su informe de trabajo para #period#, sin calificación";
$MESS["REPORT_FULL_IM_APPROVE_G"] = "confirmado su informe de trabajo para #period#, puntaje positivo";
$MESS["REPORT_FULL_IM_APPROVE_B"] = "confirmado su informe de trabajo para #period#, puntaje negativo";
$MESS["REPORT_FULL_IM_APPROVE_N"] = "confirmado su informe de trabajo para #period#, sin calificación";
$MESS["REPORT_FULL_IM_APPROVE_M_X"] = "No confirmado para su informe de trabajo #period#";
$MESS["REPORT_FULL_IM_APPROVE_F_X"] = "No confirmado para su informe de trabajo #period#";
$MESS["REPORT_FULL_IM_APPROVE_X"] = "No confirmado para su informe de trabajo #period#";
?>