<?
$MESS["TIMEMAN_NS_REPORT"] = "Nueva entrada en el reporte de tiempo de trabajo";
$MESS["TIMEMAN_NS_REPORT_COMMENT"] = "Comentarios del reporte de tiempo de trabajo";
$MESS["TIMEMAN_NS_REPORT_APPROVE"] = "Informe de actualización del tiempo de trabajo aprobado";
$MESS["TIMEMAN_NS_ENTRY"] = "Nuevo reporte del tiempo de trabajo";
$MESS["TIMEMAN_NS_ENTRY_COMMENT"] = "Nuevos comentarios del reporte de tiempo de trabajo";
$MESS["TIMEMAN_NS_ENTRY_APPROVE"] = "Reporte diario de trabajo aprobado o no aprobado";
?>