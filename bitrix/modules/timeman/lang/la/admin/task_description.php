<?
$MESS["TASK_NAME_TIMEMAN_DENIED"] = "Acceso Denegado";
$MESS["TASK_NAME_TIMEMAN_SUBORDINATE"] = "Jefe o Empleado del Departamento";
$MESS["TASK_NAME_TIMEMAN_READ"] = "Departamento de RRHH";
$MESS["TASK_NAME_TIMEMAN_FULL_ACCESS"] = "Acceso Total";
$MESS["TASK_DESC_TIMEMAN_DENIED"] = "El acceso al manejo de trabajo está denegado";
$MESS["TASK_DESC_TIMEMAN_WRITE"] = "Lectura completa / Escribir Acceso";
$MESS["TASK_DESC_TIMEMAN_FULL_ACCESS"] = "Acceso completo al Manejo de tiempo de trabajo";
$MESS["TASK_NAME_TIMEMAN_WRITE"] = "Pizarra de Administración de la Compañía";
$MESS["TASK_DESC_TIMEMAN_SUBORDINATE"] = "Acceso de la Subordinación Controlada";
$MESS["TASK_DESC_TIMEMAN_READ"] = "Leer Acceso completo, Acceso a la escritura de la subordinación controlada";
?>