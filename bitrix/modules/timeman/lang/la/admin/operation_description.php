<?
$MESS["OP_NAME_TM_READ"] = "Leer Todos los Registros";
$MESS["OP_NAME_TM_WRITE"] = "Editar Todos los Registros";
$MESS["OP_NAME_TM_SETTINGS"] = "Editar las Configuraciones del Módulo";
$MESS["OP_DESC_TM_READ"] = "Acceso a Todos los Reportes";
$MESS["OP_DESC_TM_WRITE"] = "Editar Cualquier Registro y Confirmar el Acceso";
$MESS["OP_DESC_TM_SETTINGS"] = "Editar Configuraciones del Módulo";
$MESS["OP_NAME_TM_MANAGE"] = "Marcar entrada Y Salida";
$MESS["OP_NAME_TM_MANAGE_ALL"] = "Nadie ha marcado entrada y salida";
$MESS["OP_NAME_TM_READ_SUBORDINATE"] = "Lectura de Subordinación Controlada";
$MESS["OP_NAME_TM_WRITE_SUBORDINATE"] = "Edición de Subordinación Controlada";
$MESS["OP_DESC_TM_MANAGE"] = "Control del tiempo de trabajo del Personal";
$MESS["OP_DESC_TM_MANAGE_ALL"] = "Nadie controla el tiempo de trabajo";
$MESS["OP_DESC_TM_READ_SUBORDINATE"] = "Acceso al Reporte Controlado de la subordinación";
$MESS["OP_DESC_TM_WRITE_SUBORDINATE"] = "Editar Subordinación controlada y confirmar el acceso";
?>