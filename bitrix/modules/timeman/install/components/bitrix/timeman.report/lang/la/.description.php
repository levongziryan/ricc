<?
$MESS["TM_TMR_COMPONENT_NAME"] = "Resumen del Horario de Trabajo";
$MESS["TM_TMR_COMPONENT_DESCRIPTION"] = "Mostrar reporte de tiempo de trabajo a los administradores y empleados de la compañía.";
$MESS["INTR_GROUP_NAME"] = "Portal de Intranet";
$MESS["TM_GROUP_NAME"] = "Administración del Horario de Trabajo";
?>