<?
$MESS["BLG_NAME"] = "Démarré par";
$MESS["BLG_SHARE_ALL"] = "Tous les employés";
$MESS["BLG_SHARE_ALL_BUS"] = "Tous les utilisateurs";
$MESS["BLG_SHARE"] = "Partagé avec : ";
$MESS["BLG_SHARE_1"] = "Partagé avec : ";
$MESS["BLG_SHARE_HIDDEN_1"] = "Destinataire caché";
$MESS["SONET_HELPER_CREATED_BY_ANONYMOUS"] = "Utilisateur non-autorisé";
$MESS["BLG_TASK_CREATED_POST"] = "La tâche \"#TASK_NAME#\" a été créée sur un message du Flux d'activités.";
$MESS["BLG_TASK_CREATED_COMMENT"] = "La tâche \"#TASK_NAME#\" a été créée sur la base de #LINK#.";
$MESS["BLG_TASK_CREATED_COMMENT_LINK"] = "un commentaire du Flux d'activités";
$MESS["SONET_HELPER_STEPPER_LIVEFEED"] = "Réindexation du Flux d'activités";
?>