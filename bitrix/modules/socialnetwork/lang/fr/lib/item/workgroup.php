<?
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_OPEN"] = "Projet ouvert";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_OPEN_DESC"] = "Le projet est visible par tout le monde. Tout le monde peut devenir un membre du projet.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_CLOSED"] = "Projet privé";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_CLOSED_DESC"] = "Le projet est visible uniquement par les membres du projet. Une invitation est requise pour devenir un membre du projet.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_EXTERNAL"] = "Projet externe";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_EXTERNAL_DESC"] = "Le projet est visible uniquement par les membres du projet. Les utilisateurs externes peuvent être invités à rejoindre le projet.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_OPEN"] = "Groupe de travail ouvert";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_OPEN_DESC"] = "Le groupe de travail est visible par tout le monde. Tout le monde peut devenir un membre du groupe de travail.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_CLOSED"] = "Groupe de travail privé";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_CLOSED_DESC"] = "Le groupe de travail est visible uniquement par les membres du groupe de travail. Une invitation est requise pour devenir un membre du groupe de travail.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_EXTERNAL"] = "Groupe de travail externe";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_EXTERNAL_DESC"] = "Le groupe de travail est visible uniquement par les membres du groupe de travail. Les utilisateurs externes peuvent être invités à rejoindre le groupe de travail.";
?>