<?
$MESS["SONET_COMMENTAUX_CREATETASK_BLOG_POST"] = "La tâche \"#TASK_NAME#\" a été créée sur un message du Flux d'activité.";
$MESS["SONET_COMMENTAUX_CREATETASK_BLOG_COMMENT"] = "La tâche \"#TASK_NAME#\" a été créée sur base de #COMMENT_LINK#.";
$MESS["SONET_COMMENTAUX_CREATETASK_BLOG_COMMENT_LINK"] = "un commentaire du flux d'activité";
$MESS["SONET_COMMENTAUX_CREATETASK_NOT_FOUND"] = "&lt;tâche introuvable.&gt;";
?>