<?
$MESS["SONET_INSTALL_NAME"] = "Réseau social";
$MESS["SONET_INSTALL_DESCRIPTION"] = "Le module permet de créer un réseau social sur le site.";
$MESS["SONET_INSTALL_TITLE"] = "Installation du module de réseau social";
$MESS["SONETP_PERM_D"] = "le travail dans la partie publique sans le droit de créer des groupes";
$MESS["SONETP_PERM_K"] = "travailler dans la partie publique avec le droit de créer des groupes";
$MESS["SONETP_PERM_R"] = "voir la partie administration";
$MESS["SONETP_PERM_W"] = "accès complet";
$MESS["SONETP_COPY_PUBLIC_FILES"] = "Installer la partie publique et les données de démonstration";
$MESS["SONETP_COPY_FOLDER"] = "Dossier dans lequel les fichiers seront copiés (par rapport à la racine du site)";
$MESS["SONET_INSTALL_PUBLIC_REW"] = "Remplacer les fichiers existants";
$MESS["SONETP_REWRITE_ADD"] = "Réécrire les fichiers auxiliaires";
$MESS["SONETP_INSTALL_EMAIL"] = "Créer les modèles e-mail";
$MESS["SONETP_DELETE_EMAIL"] = "Supprimer les modèles e-mail";
$MESS["SONETP_INSTALL_404"] = "Activer la partie publique dans le mode CNC";
$MESS["SONETP_RW_DEF_IMAGES"] = "Enregistrer les images dans le modèle &quot;.default&quot;";
$MESS["SONETP_INSTALL_SMILES"] = "Installer les smileys";
$MESS["SONETP_NOT_INSTALL_P"] = "ne pas installer";
$MESS["SONETP_EDIT_FORM_LABEL"] = "Annonce";
$MESS["SONETP_LIST_COLUMN_LABEL"] = "Annonce";
$MESS["SONETP_LIST_FILTER_LABEL"] = "Annonce";
$MESS["SONET_UF_SG_DEPT_EDIT_FORM_LABEL"] = "Services";
$MESS["SONET_UF_SG_DEPT_LIST_COLUMN_LABEL"] = "Services";
$MESS["SONET_UF_SG_DEPT_LIST_FILTER_LABEL"] = "Services";
?>