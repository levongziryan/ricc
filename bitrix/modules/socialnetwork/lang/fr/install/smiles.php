<?
$MESS["FICON_QUESTION"] = "Question";
$MESS["FICON_EXCLAIM"] = "Exclamation";
$MESS["FICON_COOL"] = "Bien/Salut!";
$MESS["FICON_IDEA"] = "L'idée";
$MESS["FICON_CRY"] = "Très triste";
$MESS["FICON_SAD"] = "Tristement";
$MESS["FICON_KISS"] = "Bisous";
$MESS["FICON_SMILE"] = "Avec un sourire";
$MESS["FICON_NEUTRAL"] = "Sceptique";
$MESS["FICON_REDFACE"] = "Confus";
$MESS["FICON_EVIL"] = "En colère";
$MESS["FICON_EEK"] = "Surpris";
$MESS["FICON_BIGGRIN"] = "Large sourire";
$MESS["FICON_WINK"] = "Facétieusement";
?>