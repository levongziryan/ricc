<?
$MESS["SONET_NEW_EVENT_GROUP_DESC"] = "#ENTITY_ID# - ID groupe
#LOG_DATE# - Date de l'enregistrement
#TITLE# - Intitulé
#MESSAGE# - Message
#URL# - Adresse
#GROUP_NAME# - Nom du groupe
#SUBSCRIBER_NAME# - Prénom du destinataire
#SUBSCRIBER_LAST_NAME# - Nom du destinataire
#SUBSCRIBER_EMAIL# - Adresse email du destinataire
#SUBSCRIBER_ID# - ID destinataire";
$MESS["SONET_NEW_EVENT_DESC"] = "#ENTITY_ID# - ID source de l'événement
#LOG_DATE# - Date d'enregistrement
#TITLE# - Intitulé
#MESSAGE# - Message
#URL# - Adresse
#ENTITY# - Lieu d'enregistrement de l'événement
#SUBSCRIBER_NAME# - Prénom du destinataire
#SUBSCRIBER_LAST_NAME# - Nom du destinataire
#SUBSCRIBER_EMAIL# - Adresse emaildu destinataire
#SUBSCRIBER_ID# - ID destinataire";
$MESS["SONET_NEW_EVENT_USER_DESC"] = "#ENTITY_ID# - ID utilisateur
#LOG_DATE# - Date de l'enregistrement
#TITLE# - Intitulé
#MESSAGE# - Message
#URL# - Adresse
#USER_NAME# - Nom de l'utilisateur
#SUBSCRIBER_NAME# - Prénom du destinataire
#SUBSCRIBER_LAST_NAME# - Nom du destinataire
#SUBSCRIBER_EMAIL# - Email du destinataire
#SUBSCRIBER_ID# - ID destinataire";
$MESS["SONET_REQUEST_GROUP_DESC"] = "#MESSAGE_ID# - ID du message
#USER_ID# - ID du destinataire de la requête
#USER_NAME# - prénom du destinataire de la requête
#USER_LAST_NAME# - nom du destinataire de la requête
#SENDER_ID# - ID de l'envoyeur de la requête
#SENDER_NAME# - prénom de l'expéditeur de la requête
#SENDER_LAST_NAME# - nom de l'expéditeur de la requête
#TITLE# - titre
#MESSAGE# - message
#EMAIL_TO# - adresse emaildu destinataire de la lettre";
$MESS["SONET_NEW_MESSAGE_DESC"] = "#MESSAGE_ID# - ID message
#USER_ID# - ID utilisateur
#USER_NAME# - Prénom de l'utilisateur
#USER_LAST_NAME# - Nom de l'utilisateur
#SENDER_ID# - ID expéditeur du message
#SENDER_NAME# - Prénom de l'expéditeur du message
#SENDER_LAST_NAME# - Nom de l'expéditeur du message
#TITLE# - Titre du message
#MESSAGE# - Message
#EMAIL_TO# - Adresse email du destinataire de la lettre";
$MESS["SONET_INVITE_GROUP_DESC"] = "#RELATION_ID# - ID du contact
#GROUP_ID# - ID du groupe
#USER_ID# - ID del'utilisateur
#GROUP_NAME# - Nom du groupe
#USER_NAME# - Prénom de l'utilisateur
#USER_LAST_NAME# - Nom de l'utilisateur
#USER_EMAIL# - Adresse email de l'utilisateur
#INITIATED_USER_NAME# - Prénom de l'utilisateur ayant envoyé l'invitation
#INITIATED_USER_LAST_NAME# - Nom de l'utilisateur ayant envoyé l'invitation
#URL# - Chemin d'accès vers la page des messages de l'utilisateur
#MESSAGE# - Message";
$MESS["SONET_INVITE_FRIEND_DESC"] = "#RELATION_ID# - ID de la relation
#SENDER_USER_ID# - ID de l'expéditeur
#SENDER_USER_NAME# - Prénom de l'expéditeur
#SENDER_USER_LAST_NAME# - Nom de l'expéditeur
#SENDER_EMAIL_TO# - Adresse email de l'expéditeur
#RECIPIENT_USER_ID# - ID du destinataire
#RECIPIENT_USER_NAME# - Prénom du destinataire
#RECIPIENT_USER_LAST_NAME# - Nom du destinataire
#RECIPIENT_USER_EMAIL_TO# -adresse email du destinataire
#MESSAGE# - Message";
$MESS["SONET_AGREE_FRIEND_DESC"] = "#RELATION_ID# - ID de la relation
#SENDER_USER_ID# - ID de l'expéditeur
#SENDER_USER_NAME# - Prénom de l'expéditeur
#SENDER_USER_LAST_NAME# - Nom de l'expéditeur
#SENDER_EMAIL_TO# - Adresse email de l'expéditeur
#RECIPIENT_USER_ID# - ID du destinataire
#RECIPIENT_USER_NAME# - Prénom du destinataire
#RECIPIENT_USER_LAST_NAME# - Nom du destinataire
#RECIPIENT_USER_EMAIL_TO# -adresse email du destinataire
#MESSAGE# - Message";
$MESS["SONET_BAN_FRIEND_DESC"] = "#RELATION_ID# - ID de la relation
#SENDER_USER_ID# - ID de l'expéditeur
#SENDER_USER_NAME# - Prénom de l'expéditeur
#SENDER_USER_LAST_NAME# - Nom de l'expéditeur
#SENDER_EMAIL_TO# - Adresse email de l'expéditeur
#RECIPIENT_USER_ID# - ID du destinataire
#RECIPIENT_USER_NAME# - Prénom du destinataire
#RECIPIENT_USER_LAST_NAME# - Nom du destinataire
#RECIPIENT_USER_EMAIL_TO# -adresse email du destinataire
#MESSAGE# - Message";
$MESS["SONET_NEW_EVENT_SUBJECT"] = "#SITE_NAME#: #ENTITY# - Nouvel évènement dans #ENTITY_TYPE#";
$MESS["SONET_NEW_EVENT_GROUP_SUBJECT"] = "#SITE_NAME#: Nouvel évènement du groupe";
$MESS["SONET_NEW_EVENT_USER_SUBJECT"] = "#SITE_NAME#: Nouvel évènement utilisateur";
$MESS["SONET_BAN_FRIEND_SUBJECT"] = "#SITE_NAME#: Ajouter dans liste noire";
$MESS["SONET_AGREE_FRIEND_SUBJECT"] = "#SITE_NAME#: Confirmation d'ajout à la liste d'amis";
$MESS["SONET_INVITE_FRIEND_SUBJECT"] = "#SITE_NAME#: Invitation à rejoindre la liste d'amis";
$MESS["SONET_INVITE_GROUP_SUBJECT"] = "#SITE_NAME#: Invitation à devenir membre du groupe";
$MESS["SONET_NEW_MESSAGE_SUBJECT"] = "#SITE_NAME#: Vous avez un nouveau message";
$MESS["SONET_REQUEST_GROUP_SUBJECT"] = "#TITLE#";
$MESS["SONET_BAN_FRIEND_NAME"] = "Porter à la liste noire";
$MESS["SONET_REQUEST_GROUP_NAME"] = "Demande d'accès dans un groupe";
$MESS["SONET_BAN_FRIEND_MESSAGE"] = "Message d'information du site #SITE_NAME#
------------------------------------------

Bonjour, #RECIPIENT_USER_NAME#!

#SENDER_USER_NAME# #SENDER_USER_LAST_NAME# vous a inclus dans la liste noire.

Ce message a été généré automatiquement.";
$MESS["SONET_INVITE_FRIEND_MESSAGE"] = "Message d'information du site #SITE_NAME#
------------------------------------------

Bonjour, #RECIPIENT_USER_NAME#!

#SENDER_USER_NAME# #SENDER_USER_LAST_NAME# vous invite à rejoindre sa liste d'amis

Pour répondre à l'invitation, veuillez suivre ce lien:

http://#SERVER_NAME##URL#

Message de l'utilisateur:
------------------------------------------
#MESSAGE#
------------------------------------------

Ce message a été généré automatiquement.";
$MESS["SONET_AGREE_FRIEND_MESSAGE"] = "Message d'information du site #SITE_NAME#
------------------------------------------

Bonjour, #RECIPIENT_USER_NAME#!

#SENDER_USER_NAME# #SENDER_USER_LAST_NAME# a accepté de se joindre à la liste de Vos amis.

Le message est généré automatiquement.";
$MESS["SONET_NEW_EVENT_GROUP_MESSAGE"] = "Message d'information du site #SITE_NAME#
------------------------------------------

Bonjour, #SUBSCRIBER_NAME#!

Dans le groupe #GROUP_NAME#, ont été effectuées les modifications suivantes:

#TITLE#

------------------------------------------
#MESSAGE#
------------------------------------------

Pour se rendre au site, Veuillez suivre ce lien:

http://#SERVER_NAME##URL#

Ce message a été généré automatiquement.";
$MESS["SONET_NEW_EVENT_MESSAGE"] = "Message venu du #SITE_NAME#
------------------------------------------

Bonjour, #SUBSCRIBER_NAME#!

Le site a subi les modifications suivantes:

#TITLE#

------------------------------------------
#MESSAGE#
------------------------------------------

Pour accéder à la page de l'événement, veuillez cliquez sur le lien:

#URL#
Ce message a été généré automatiquement.";
$MESS["SONET_NEW_EVENT_USER_MESSAGE"] = "Message venu du #SITE_NAME#
------------------------------------------

Bonjour, #SUBSCRIBER_NAME#!

L'utilisateur #USER_NAME# a subi les modifications suivantes:

#TITLE#

------------------------------------------
#MESSAGE#
------------------------------------------
Pour visiter le site veuillez suivre le lien:

http://#SERVER_NAME##URL#

This is an automatically generated notification.";
$MESS["SONET_REQUEST_GROUP_MESSAGE"] = "Message d'information du site #SITE_NAME#
------------------------------------------
Bonjour #USER_NAME#!

------------------------------------------
#MESSAGE#
------------------------------------------

Ce message a été généré automatiquement.";
$MESS["SONET_NEW_MESSAGE_MESSAGE"] = "Message d'information du site #SITE_NAME#
------------------------------------------

Bonjour, #USER_NAME#!

Un nouveau message est arrivé pour vous de l'utilisateur #SENDER_NAME# #SENDER_LAST_NAME#:

------------------------------------------
#MESSAGE#
------------------------------------------

Lien pour aller aux messages de l'utilisateur:

http://#SERVER_NAME#/company/personal/messages/chat/#SENDER_ID#/

Ce message a été généré automatiquement.";
$MESS["SONET_INVITE_GROUP_MESSAGE"] = "Message venu du #SITE_NAME#
------------------------------------------

Bonjour, #USER_NAME#!

L'utilisateur #INITIATED_USER_NAME# #INITIATED_USER_LAST_NAME# vous invite à rejoindre le groupe #GROUP_NAME#.
Pour accepter l'invitation, veuillez suivre le lien:

http://#SERVER_NAME##URL#

Message du groupe:
------------------------------------------
#MESSAGE#
------------------------------------------

Ce message a été généré automatiquement.";
$MESS["SONET_NEW_EVENT_NAME"] = "Ajouter un événement";
$MESS["SONET_NEW_EVENT_GROUP_NAME"] = "Un nouvel Evénement du Groupe";
$MESS["SONET_NEW_EVENT_USER_NAME"] = "Nouvel événement de l'utilisateur";
$MESS["SONET_INVITE_GROUP_NAME"] = "Invitation de rejoindre le groupe";
$MESS["SONET_INVITE_FRIEND_NAME"] = "Invitation d'Adhérer à la liste d'Amis";
$MESS["SONET_AGREE_FRIEND_NAME"] = "Accord de Devenir un Ami";
$MESS["SONET_NEW_MESSAGE_NAME"] = "Vous avez un nouveau message";
$MESS["SONET_LOG_NEW_ENTRY_NAME"] = "Nouveau message ajouté";
$MESS["SONET_LOG_NEW_ENTRY_DESC"] = "#EMAIL_TO# - e-mail du destinataire du message 
#LOG_ENTRY_ID# - ID du message
#RECIPIENT_ID# - ID du destinataire
#URL_ID# - URL d'affichage du message
";
$MESS["SONET_LOG_NEW_COMMENT_NAME"] = "Nouveau commentaire ajouté";
$MESS["SONET_LOG_NEW_COMMENT_DESC"] = "#EMAIL_TO# - e-mail du destinataire du message 
#COMMENT_ID# - ID du commentaire
#LOG_ENTRY_ID# - ID du message
#RECIPIENT_ID# - ID du destinataire
#URL_ID# - URL d'affichage du message
";
?>