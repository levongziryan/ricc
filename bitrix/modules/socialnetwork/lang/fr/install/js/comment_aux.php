<?
$MESS["SONET_COMMENTAUX_JS_SHARE_TEXT"] = "Partagé avec: #SHARE_LIST#";
$MESS["SONET_COMMENTAUX_JS_SHARE_TEXT_1"] = "Partagé avec: #SHARE_LIST#";
$MESS["SONET_COMMENTAUX_JS_CREATETASK_BLOG_POST"] = "La tâche \"#TASK_NAME#\" a été créée sur un message du Flux d'activité.";
$MESS["SONET_COMMENTAUX_JS_CREATETASK_BLOG_COMMENT"] = "La tâche \"#TASK_NAME#\" a été créée sur base de #COMMENT_LINK#.";
$MESS["SONET_COMMENTAUX_JS_CREATETASK_BLOG_COMMENT_LINK"] = "un commentaire du flux d'activité";
$MESS["SONET_COMMENTAUX_JS_FILEVERSION_TEXT"] = "a téléchargé une nouvelle version du fichier";
$MESS["SONET_COMMENTAUX_JS_FILEVERSION_TEXT_M"] = "a téléchargé une nouvelle version du fichier";
$MESS["SONET_COMMENTAUX_JS_FILEVERSION_TEXT_F"] = "a téléchargé une nouvelle version du fichier";
$MESS["SONET_COMMENTAUX_JS_HEAD_FILEVERSION_TEXT"] = "a modifié le fichier";
$MESS["SONET_COMMENTAUX_JS_HEAD_FILEVERSION_TEXT_M"] = "a modifié le fichier";
$MESS["SONET_COMMENTAUX_JS_HEAD_FILEVERSION_TEXT_F"] = "a modifié le fichier";
?>