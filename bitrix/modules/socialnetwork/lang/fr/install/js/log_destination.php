<?
$MESS["LM_POPUP_TITLE"] = "Titre";
$MESS["LM_POPUP_TAB_LAST"] = "Derniers";
$MESS["LM_POPUP_TAB_SG"] = "Groupes";
$MESS["LM_POPUP_TAB_STRUCTURE"] = "Les employés et les départements";
$MESS["LM_POPUP_TAB_STRUCTURE_EXTRANET"] = "Utilisateurs externes";
$MESS["LM_POPUP_TAB_LAST_USERS"] = "Personnes";
$MESS["LM_POPUP_TAB_LAST_CONTACTS"] = "Contacts";
$MESS["LM_POPUP_TAB_LAST_COMPANIES"] = "Entreprise";
$MESS["LM_POPUP_TAB_LAST_LEADS"] = "Conversion";
$MESS["LM_POPUP_TAB_LAST_DEALS"] = "Affaires";
$MESS["LM_POPUP_TAB_LAST_SG"] = "Groupes";
$MESS["LM_POPUP_TAB_LAST_STRUCTURE"] = "Départements";
$MESS["LM_POPUP_CHECK_STRUCTURE"] = "Tous les collaborateurs du département avec la subdivision";
$MESS["LM_SEARCH_PLEASE_WAIT"] = "Veuillez patienter, recherche en cours";
$MESS["LM_EMPTY_LIST"] = "Aucun élément à afficher";
$MESS["LM_PLEASE_WAIT"] = "Chargement, veuillez patienter";
$MESS["LM_CREATE_SONETGROUP_TITLE"] = "Le groupe avec le même nom n'est pas trouvé, créer un groupe '#TITLE#'?";
$MESS["LM_CREATE_SONETGROUP_BUTTON_CREATE"] = "Ajouter";
$MESS["LM_CREATE_SONETGROUP_BUTTON_CANCEL"] = "Annuler";
$MESS["LM_INVITE_EMAIL_USER_BUTTON_OK"] = "OK";
$MESS["LM_INVITE_EMAIL_USER_TITLE"] = "Ce message sera partagé par e-mail avec";
$MESS["LM_INVITE_EMAIL_USER_PLACEHOLDER_NAME"] = "Nom";
$MESS["LM_INVITE_EMAIL_USER_PLACEHOLDER_LAST_NAME"] = "Nom";
$MESS["LM_POPUP_WAITER_TEXT"] = "Recherche de résultats supplémentaires...";
$MESS["LM_POPUP_TAB_SEARCH"] = "Rechercher";
$MESS["LM_POPUP_TAB_EMAIL"] = "Utilisateurs d'e-mail";
$MESS["LM_POPUP_TAB_CRMEMAIL"] = "Utilisateurs d'e-mail CRM";
$MESS["LM_POPUP_TAB_LAST_CRMEMAILS"] = "Utilisateurs d'e-mail CRM";
$MESS["LM_INVITE_EMAIL_CRM_CREATE_CONTACT"] = "Créer un contact CRM";
$MESS["LM_POPUP_SEARCH_NETWORK"] = "Rechercher dans Bitrix24.Network";
$MESS["LM_POPUP_TAB_SG_PROJECT"] = "Mes Projets";
$MESS["LM_POPUP_TAB_LAST_SG_PROJECT"] = "Projets";
?>