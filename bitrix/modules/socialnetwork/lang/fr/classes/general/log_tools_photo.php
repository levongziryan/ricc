<?
$MESS["SONET_PHOTO_LOG_1"] = "#AUTHOR_NAME# a ajouté des images #TITLE#";
$MESS["SONET_PHOTOPHOTO_LOG_1"] = "#AUTHOR_NAME# a ajouté (e) une photo #TITLE#";
$MESS["SONET_PHOTO_LOG_MAIL_TEXT"] = "Des nouvelles photographies ont été chargées dans l'album '#TITLE#'";
$MESS["SONET_PHOTO_LOG_GUEST"] = "invité";
$MESS["SONET_IM_NEW_PHOTO"] = "Une nouvelle photo a été chargée dans l'album '#title#' du groupe '#group_name#'.";
$MESS["SONET_PHOTO_ADD_COMMENT_SOURCE_ERROR"] = "Chec d'ajout du commentaire à la source de l'événement.";
$MESS["SONET_PHOTOALBUM_IM_COMMENT"] = "a(i) commenté votre album '#album_title#'";
$MESS["SONET_PHOTO_IM_COMMENT"] = "a commenté votre photo '#photo_title#' dans l'album '#album_title#'";
$MESS["SONET_PHOTO_LOG_2"] = "Photos (#COUNT#)";
$MESS["SONET_PHOTO_UPDATE_COMMENT_SOURCE_ERROR"] = "Impossible de mettre à jour commentaire à la source de l'événement";
$MESS["SONET_PHOTO_DELETE_COMMENT_SOURCE_ERROR"] = "Impossible de supprimer le commentaire à la source de l'événement";
?>