<?
$MESS["SONET_EUV_NO_ENTITY"] = "Enregistrement n'est pas trouvé.";
$MESS["SONET_EUV_RECORD_EXISTS"] = "Cette entrée existe déjà.";
$MESS["SONET_EUV_ERROR_SET"] = "Impossible de sauvegarder l'enregistrement sur les droits d'accès.";
$MESS["SONET_EUV_ERROR_DELETE"] = "La suppression de l'enregistrement sur les droits d'accès a échoué.";
$MESS["SONET_EUV_EMPTY_ENTITY_ID"] = "Le code de l'entité non indiqué";
$MESS["SONET_EUV_EMPTY_USER_ID"] = "Un utilisateur n'est pas indiqué";
$MESS["SONET_EUV_EMPTY_ROLE"] = "Le rôle n'est pas spécifié";
$MESS["SONET_EUV_INCORRECT_ENTITY_TYPE"] = "Type d'entité est incorrect";
$MESS["SONET_EUV_EMPTY_ENTITY_TYPE"] = "Le type de l'entité n'est pas spécifié";
?>