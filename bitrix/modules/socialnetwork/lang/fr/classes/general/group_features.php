<?
$MESS["SONET_GB_EMPTY_DATE_UPDATE"] = "La date de modification des paramètres est incorrecte.";
$MESS["SONET_GB_EMPTY_DATE_CREATE"] = "La date de création de l'entrée est incorrecte.";
$MESS["SONET_GF_ERROR_CALC_ENTITY_TYPE"] = "Impossible de déterminer le type d'entité.";
$MESS["SONET_GF_ERROR_SET"] = "La sauvegarde de l'entrée a échoué.";
$MESS["SONET_GF_EMPTY_ENTITY_ID"] = "Identifiant de l'entité inconnu.";
$MESS["SONET_GF_EMPTY_FEATURE_ID"] = "La fonctionnalité n'est pas indiquée.";
$MESS["SONET_GF_ERROR_NO_ENTITY_ID"] = "L'entité n'est pas correctement spécifiée.";
$MESS["SONET_GF_ERROR_NO_ENTITY_TYPE"] = "Type d'entité est incorrect.";
$MESS["SONET_GF_EMPTY_ENTITY_TYPE"] = "Le type de l'entité n'est pas spécifié.";
$MESS["SONET_GF_ERROR_NO_FEATURE_ID"] = "La fonctionnalité n'est pas spécifiée correctement.";
?>