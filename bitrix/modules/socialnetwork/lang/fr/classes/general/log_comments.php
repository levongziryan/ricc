<?
$MESS["SONET_GLC_EMPTY_ENTITY_TYPE"] = "Le type de l'entité n'est pas spécifié.";
$MESS["SONET_GLC_ERROR_NO_ENTITY_TYPE"] = "Type d'entité est incorrect.";
$MESS["SONET_GLC_EMPTY_ENTITY_ID"] = "Identifiant de l'entité inconnu.";
$MESS["SONET_GLC_ERROR_CALC_ENTITY_TYPE"] = "Erreur de calcul du type de l'entité.";
$MESS["SONET_GLC_ERROR_NO_ENTITY_ID"] = "ID incorrect de l'entité est indiqué.";
$MESS["SONET_GLC_EMPTY_LOG_ID"] = "Le code de l'évènement du volet des activités est incorrect.";
$MESS["SONET_GLC_ERROR_NO_USER_ID"] = "Code d'utilisateur incorrect.";
$MESS["SONET_GLC_EMPTY_EVENT_ID"] = "L'événement n'est pas indiqué.";
$MESS["SONET_GLC_ERROR_NO_FEATURE_ID"] = "Le code de la galerie spécifié existe déjà.";
$MESS["SONET_GLC_EMPTY_DATE_CREATE"] = "Date de création est incorrecte.";
$MESS["SONET_GLC_WRONG_PARAMETER_ID"] = "Le paramètre ID n'est pas correct.";
$MESS["SONET_GLC_SEND_EVENT_LINK"] = "Accéder:";
$MESS["SONET_GLC_ERROR_CHECKFIELDS_FAILED"] = "Des données incorrectes";
$MESS["SONET_GLC_FORUM_MENTION"] = "vous avez mentionné dans un commentaire à un post sur un forum \"#titre#\"";
$MESS["SONET_GLC_FORUM_MENTION_M"] = "vous avez mentionné dans le commentaire de post sur le forum \"#titre#\"";
$MESS["SONET_GLC_FORUM_MENTION_F"] = "vous avez mentionné dans un commentaire à un post sur un forum \"#titre#\"";
?>