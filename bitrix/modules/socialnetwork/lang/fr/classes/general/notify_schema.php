<?
$MESS["SONET_NS_SONET_GROUP_EVENT"] = "Activité et mises à jour dans le groupe";
$MESS["SONET_NS_FRIEND"] = "Inclusion/exclusion à/de votre liste d'amis";
$MESS["SONET_NS_MODERATORS_GROUP"] = "Inclure / supprimer de la liste de modérateurs du groupe";
$MESS["SONET_NS_INOUT_GROUP"] = "Adhésion/retraite du groupe (pour les modérateurs)";
$MESS["SONET_NS_INVITE_USER"] = "Demande d'ami";
$MESS["SONET_NS_INVITE_GROUP"] = "Invitation/demande de rejoindre le groupe, exclusion du groupe";
$MESS["SONET_NS_OWNER_GROUP"] = "Changement du propriétaire de groupe";
?>