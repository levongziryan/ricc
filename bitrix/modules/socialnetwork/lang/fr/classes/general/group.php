<?
$MESS["SONET_WRONG_PARAMETER_ID"] = "Un paramètre ID invalide est transmit dans la méthode.";
$MESS["SONET_GB_EMPTY_DATE_UPDATE"] = "La date de modification des paramètres est incorrecte.";
$MESS["SONET_GB_EMPTY_DATE_ACTIVITY"] = "La date de la dernière activité est indiquée incorrectement.";
$MESS["SONET_GB_EMPTY_DATE_CREATE"] = "Date de création est indiquée inexactement.";
$MESS["SONET_NO_GROUP"] = "Enregistrement n'est pas trouvé.";
$MESS["SONET_GP_ERROR_IMAGE_ID"] = "Le script d'importation";
$MESS["SONET_GB_ERROR_NO_OWNER_ID"] = "Code du propriétaire invalide.";
$MESS["SONET_GB_EMPTY_OWNER_ID"] = "Propriétaire inconnu";
$MESS["SONET_UR_EMPTY_OWNERID"] = "Le code de propriétaire du groupe est inconnu.";
$MESS["SONET_GG_EMPTY_SITE_ID"] = "Le site nest pas indiqué.";
$MESS["SONET_GB_EMPTY_SUBJECT_ID"] = "Le sujet n'est pas indiqué.";
$MESS["SONET_GB_EMPTY_NAME"] = "Nom non spécifié.";
$MESS["SONET_UR_EMPTY_FIELDS"] = "Les paramètres du groupe ne sont pas indiqués.";
$MESS["SONET_UG_EMPTY_SPAM_PERMS"] = "Les droits pour l'envoi des messages dans le groupe ne sont pas indiqués";
$MESS["SONET_UG_EMPTY_INITIATE_PERMS"] = "Les droits d'invitation des utilisateurs au groupe ne sont pas indiqués.";
$MESS["SONET_UR_ERROR_CREATE_U_GROUP"] = "Chec de la liaison de l'utilisateur au groupe";
$MESS["SONET_UR_ERROR_CREATE_GROUP"] = "Erreur de création d'un groupe.";
$MESS["SONET_GG_ERROR_CANNOT_DELETE_USER_2"] = "Veuillez vous rendre à la section Services> Réseau social> Groupes et changer le propriétaire du groupe ou bien veuillez supprimer les groupes.";
$MESS["SONET_GG_ERROR_CANNOT_DELETE_USER_1"] = "L'utilisateur est un propriétaire des groupes du réseau social:<br>";
$MESS["SONET_UG_ERROR_NO_SPAM_PERMS"] = "Autorisations ont été déléguées avec succès";
$MESS["SONET_UG_ERROR_NO_INITIATE_PERMS"] = "Les droits d'invitation des utilisateurs au groupe sont incorrectement indiqués";
$MESS["SONET_GG_ERROR_NO_SITE"] = "Le site est indiqué incorrectement.";
$MESS["SONET_GB_ERROR_NO_SUBJECT_ID"] = "Le sujet est indiqué inexactement";
?>