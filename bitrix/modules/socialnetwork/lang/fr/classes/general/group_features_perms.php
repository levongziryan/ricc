<?
$MESS["SONET_GF_EMPTY_ENTITY_ID"] = "Identifiant de l'entité inconnu.";
$MESS["SONET_GFP_EMPTY_GROUP_FEATURE_ID"] = "La fonctionnalité n'est pas indiquée.";
$MESS["SONET_GF_EMPTY_FEATURE_ID"] = "La fonctionnalité n'est pas indiquée.";
$MESS["SONET_GFP_EMPTY_OPERATION_ID"] = "Opération est pas spécifié.";
$MESS["SONET_GFP_EMPTY_ROLE"] = "Le rôle n'est pas spécifié.";
$MESS["SONET_GFP_BAD_OPERATION_ID"] = "L'opération ne peut pas être vérifiée.";
$MESS["SONET_GFP_NO_OPERATION_ID"] = "L'opération des paramètres est indiquée incorrectement.";
$MESS["SONET_GF_ERROR_SET"] = "Erreur de création de l'inscription.";
$MESS["SONET_GFP_ERROR_NO_ROLE"] = "Rôle indiqué incorrectement.";
$MESS["SONET_GF_ERROR_NO_ENTITY_ID"] = "L'entité n'est pas correctement spécifiée.";
$MESS["SONET_GF_ERROR_NO_ENTITY_TYPE"] = "Type d'entité est incorrect.";
$MESS["SONET_GF_EMPTY_ENTITY_TYPE"] = "Le type de l'entité n'est pas spécifié.";
$MESS["SONET_GFP_ERROR_NO_GROUP_FEATURE_ID"] = "La fonctionnalité n'est pas spécifiée correctement.";
$MESS["SONET_GF_ERROR_NO_FEATURE_ID"] = "La fonctionnalité n'est pas spécifiée correctement.";
?>