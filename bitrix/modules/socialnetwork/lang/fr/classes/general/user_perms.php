<?
$MESS["SONET_GB_EMPTY_USER_ID"] = "Un utilisateur n'est pas indiqué.";
$MESS["SONET_GG_EMPTY_RELATION_TYPE"] = "Le type de communication n'est pas indiqué.";
$MESS["SONET_GG_EMPTY_OPERATION_ID"] = "Transaction inconnue.";
$MESS["SONET_GG_ERROR_NO_OPERATION_ID"] = "L'opération est inexactement indiquée.";
$MESS["SONET_GB_ERROR_NO_USER_ID"] = "L'utilisateur est indiqué inexactement.";
$MESS["SONET_GG_ERROR_NO_RELATION_TYPE"] = "Type de connexion n'est pas valide.";
?>