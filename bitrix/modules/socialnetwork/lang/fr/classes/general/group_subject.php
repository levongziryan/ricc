<?
$MESS["SONET_GS_NOT_EMPTY_SUBJECT"] = "Ce thème n'est pas vide.";
$MESS["SONET_GS_EMPTY_SITE_ID"] = "Le site du thème n'a pas été indiqué.";
$MESS["SONET_GS_EMPTY_NAME"] = "Nom du thème n'est pas spécifié.";
$MESS["SONET_GS_ERROR_NO_SITE"] = "Le site des thèmes est indiqué incorrectement.";
?>