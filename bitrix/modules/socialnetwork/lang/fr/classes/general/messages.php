<?
$MESS["SONET_MM_EMPTY_DATE_VIEW"] = "Date de lecture est indiquée inexactement.";
$MESS["SONET_GB_EMPTY_DATE_CREATE"] = "Date de création est indiquée inexactement.";
$MESS["SONET_UR_ERROR_UPDATE_MESSAGE"] = "La mise à jour du message est impossible.";
$MESS["SONET_M_ERROR_DELETE_MESSAGE"] = "Impossible de supprimer le message.";
$MESS["SONET_UR_EMPTY_MESSAGE_ID"] = "La date de publication du message est incorrect.";
$MESS["SONET_M_EMPTY_FROM_USER_ID"] = "Taille incorrecte de l'archive.";
$MESS["SONET_UR_EMPTY_SENDER_USER_ID"] = "Expéditeur du message n'est pas indiqué.";
$MESS["SONET_M_EMPTY_TO_USER_ID"] = "Aucun destinataire spécifié";
$MESS["SONET_UR_EMPTY_TARGET_USER_ID"] = "Destinataire du message non renseigné.";
$MESS["SONET_UR_EMPTY_MESSAGE"] = "Le message n'est pas spécifié.";
$MESS["SONET_M_ERROR_NO_FROM_USER_ID"] = "L'expéditeur n'est pas valable.";
$MESS["SONET_UR_ERROR_CREATE_MESSAGE"] = "Erreur de création de messages.";
$MESS["SONET_M_ERROR_NO_TO_USER_ID"] = "Destinataire incorrect.";
$MESS["SONET_UR_NO_MESSAGE"] = "Message introuvable.";
?>