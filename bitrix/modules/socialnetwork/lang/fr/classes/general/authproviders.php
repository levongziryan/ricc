<?
$MESS["authprov_sg_panel_search_text"] = "Veuillez saisir le nom du groupe.";
$MESS["authprov_sg_a"] = "Propriétaire du groupe";
$MESS["authprov_sg_k"] = "Tous les membres du groupe";
$MESS["authprov_sg_name_out"] = "Groupe de réseau social";
$MESS["authprov_sg_socnet_group"] = "Groupe du réseau social";
$MESS["authprov_sg_name"] = "Groupe du réseau social";
$MESS["authprov_sg_e"] = "Modérateurs du groupe";
$MESS["authprov_sg_panel_my_group"] = "Mes groupes";
$MESS["authprov_sg_panel_search"] = "Recherche";
$MESS["authprov_sg_panel_last"] = "Dernier";
$MESS["authprov_sg_current"] = "Groupe actuel";
?>