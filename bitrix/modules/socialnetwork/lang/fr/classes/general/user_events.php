<?
$MESS["SONET_GB_EMPTY_USER_ID"] = "Un utilisateur n'est pas indiqué.";
$MESS["SONET_UE_EMPTY_SITE_ID"] = "Le site nest pas indiqué.";
$MESS["SONET_UE_EMPTY_EVENT_ID"] = "L'événement n'est pas indiqué.";
$MESS["SONET_GB_ERROR_NO_USER_ID"] = "L'utilisateur est indiqué inexactement.";
$MESS["SONET_UE_ERROR_NO_SITE"] = "Le site est indiqué incorrectement.";
$MESS["SONET_UE_ERROR_NO_EVENT_ID"] = "Evènement invalide.";
?>