<?
$MESS["SONET_LE_ERROR_NO_ENTITY_ID"] = "ID incorrect de l'entité est indiqué.";
$MESS["SONET_LE_EMPTY_ENTITY_ID"] = "Identifiant de l'entité inconnu.";
$MESS["SONET_LE_EMPTY_SITE_ID"] = "Le site nest pas indiqué.";
$MESS["SONET_LE_EMPTY_ENTITY_TYPE"] = "Le type de l'entité n'est pas spécifié.";
$MESS["SONET_LE_EMPTY_EVENT_ID"] = "L'événement n'est pas indiqué.";
$MESS["SONET_LE_ERROR_CALC_ENTITY_TYPE"] = "Erreur de calcul du type de l'entité.";
$MESS["SONET_LE_WRONG_PARAMETER_ID"] = "Le paramètre ID n'est pas correct.";
$MESS["SONET_LE_ERROR_NO_SITE"] = "Le site est indiqué incorrectement.";
$MESS["SONET_LE_ERROR_NO_FEATURE_ID"] = "Le code de la galerie spécifié existe déjà.";
$MESS["SONET_LE_ERROR_NO_ENTITY_TYPE"] = "Type d'entité est incorrect.";
?>