<?
$MESS["SONET_LF_UNFOLLOW_IM_BUTTON_Y"] = "Activer le mode Suivi intelligent";
$MESS["SONET_LF_UNFOLLOW_IM_MESSAGE"] = "Vous recevez des tas de messages sur le Flux d'activités ? Activez le mode suivi intelligent pour ne voir que les événements importants pour vous. Seuls les messages dont vous êtes l'auteur, ceux qui vous sont adressés et ceux qui vous mentionnent seront au-dessus. Quand vous ajoutez un commentaire à un message, vous commencez à suivre ce dernier.";
$MESS["SONET_LF_UNFOLLOW_IM_BUTTON_N"] = "Non, merci";
?>