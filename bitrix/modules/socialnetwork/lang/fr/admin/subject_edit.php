<?
$MESS["SONETE_DELETE_SUBJECT_CONFIRM"] = "tes-vous sûr de vouloir supprimer ce sujet? Si au sujet il y a des groupes, alors le sujet ne sera pas supprimé.";
$MESS["SONETE_UPDATING"] = "Modification des paramètres du sujet";
$MESS["SONETE_NAME"] = "Titre du thème";
$MESS["SONETE_NO_PERMS2ADD"] = "Droits insuffisants pour ajouter un sujet.";
$MESS["SONETE_NEW_SUBJECT"] = "Nouveau sujet";
$MESS["SONETE_ERROR_SAVING"] = "Erreur de sauvegarde du thème.";
$MESS["SONETE_TAB_SUBJECT_DESCR"] = "Paramètres du sujet des groupes";
$MESS["SONETE_SITE"] = "Site du thème";
$MESS["SONETE_ADDING"] = "Création d'un nouveau thème";
$MESS["SONETE_SORT"] = "Classification";
$MESS["SONETE_2FLIST"] = "Gestion des thèmes";
$MESS["SONETE_TAB_SUBJECT"] = "Sujet du groupe";
$MESS["SONETE_DELETE_SUBJECT"] = "Supprimer le sujet";
?>