<?
$MESS["SONET_SPT_ALL"] = "[tou(te)s]";
$MESS["SONET_DELETE_CONF"] = "tes-vous sûr de vouloir supprimer ce sujet?";
$MESS["SONET_UPDATE_ALT"] = "Modification des paramètres du sujet";
$MESS["SONET_ADD_NEW_ALT"] = "Appuyer pour ajouter un nouveau sujet";
$MESS["SONET_SUBJECT_NAME"] = "Dénomination";
$MESS["SONET_ADD_NEW"] = "Nouveau sujet";
$MESS["SONET_ERROR_UPDATE"] = "Erreur de rafraîchissement des paramètres du thème.";
$MESS["SONET_DELETE_ERROR"] = "Impossible de supprimer les discussions.";
$MESS["SONET_SUBJECT_SITE_ID"] = "Site";
$MESS["SONET_FILTER_SITE_ID"] = "Site";
$MESS["SONET_SUBJECT_SORT"] = "Classification";
$MESS["SONET_SUBJECT_NAV"] = "Gestion des thèmes";
$MESS["SONET_TITLE"] = "Thèmes des groupes";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "supprimer";
$MESS["SONET_DELETE_ALT"] = "Supprimer le sujet";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Sélectionné:";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "Coché:";
?>