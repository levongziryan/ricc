<?
$MESS["SONET_MENU_GROUP"] = "Groupes";
$MESS["SONET_MENU_SMILES"] = "Dans la liste de smileys";
$MESS["BLG_AM_SONETS"] = "Réseau social";
$MESS["SONET_MENU_SUBJECT"] = "Gestion des thèmes";
$MESS["SONET_MENU_GROUP_ALT"] = "Gestion des groupes";
$MESS["SONET_MENU_SMILES_ALT"] = "Gestion des smileys et des icônes";
$MESS["BLG_AM_SONETS_ALT"] = "Gestion de réseau social";
$MESS["SONET_MENU_SUBJECT_ALT"] = "Gestion des thèmes des groupes";
?>