<?
$MESS["USER_NAME_TEMPLATE"] = "[#ID#] #LAST_NAME# #NAME#";
$MESS["SONET_SPT_ALL"] = "[tou(te)s]";
$MESS["SONET_OWNER_ID"] = "Code de l'utilisateur";
$MESS["SONET_GROUP_OWNER_ID"] = "Organisateur";
$MESS["SONET_OWNER_USER"] = "Organisateur";
$MESS["SONET_DELETE_CONF"] = "tes-vous sûr de vouloir supprimer ce groupe ?";
$MESS["SONET_GROUP_NAV"] = "Groupes";
$MESS["SONET_TITLE"] = "Groupes";
$MESS["SONET_ADMIN_LIST_CHANGE_OWNER"] = "Changer de propriétaire";
$MESS["SONET_UPDATE_ALT"] = "Modifiez les paramètres du groupe";
$MESS["SONET_GROUP_NAME"] = "Dénomination";
$MESS["SONET_ERROR_UPDATE"] = "Erreur de mise à jour des paramètres du groupe.";
$MESS["SONET_DELETE_ERROR"] = "Impossible de supprimer le groupe.";
$MESS["SONET_FILTER_SITE_ID"] = "Site";
$MESS["SONET_SUBJECT_SORT"] = "Trier";
$MESS["SONET_GROUP_SUBJECT_ID"] = "d'après les sujets";
$MESS["SONET_FILTER_SUBJECT_ID"] = "d'après les sujets";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "supprimer";
$MESS["SONET_DELETE_ALT"] = "Suppression du Groupe";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Sélectionné:";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "Coché:";
?>