<?
$MESS ['SONET_GF_ERROR_CALC_ENTITY_TYPE'] = "Der Elementtyp kann nicht bestimmt werden";
$MESS ['SONET_GF_ERROR_SET'] = "Der Eintrag konnte nicht gespeichert werden";
$MESS ['SONET_GF_ERROR_NO_FEATURE_ID'] = "Die Funktion ist falsch angegeben";
$MESS ['SONET_GF_EMPTY_FEATURE_ID'] = "Die Funktion wurde nicht angegeben";
$MESS ['SONET_GB_EMPTY_DATE_UPDATE'] = "Das Datum der Parameteränderung wurde falsch angegeben";
$MESS ['SONET_GF_EMPTY_ENTITY_ID'] = "Die Elementen ID wurde nicht angegeben";
$MESS ['SONET_GF_ERROR_NO_ENTITY_ID'] = "Das Element wurde falsch angegeben";
$MESS ['SONET_GF_ERROR_NO_ENTITY_TYPE'] = "Der Elementtyp ist falsch";
$MESS ['SONET_GF_EMPTY_ENTITY_TYPE'] = "Der Elementtyp ist falsch";
$MESS ['SONET_GB_EMPTY_DATE_CREATE'] = "Das Erstellungsdatum des Eintrags wurde falsch angegeben";
?>