<?
$MESS ['SONET_GFP_BAD_OPERATION_ID'] = "Die Operation kann nicht überprüft werden";
$MESS ['SONET_GF_ERROR_SET'] = "Es ist ein Fehler beim Erstellen des Eintrags aufgetreten";
$MESS ['SONET_GF_ERROR_NO_FEATURE_ID'] = "Die Funktion ist falsch angegeben";
$MESS ['SONET_GFP_ERROR_NO_GROUP_FEATURE_ID'] = "Die Funktion ist falsch angegeben";
$MESS ['SONET_GF_EMPTY_FEATURE_ID'] = "Die Funktion wurde nicht angegeben";
$MESS ['SONET_GFP_EMPTY_GROUP_FEATURE_ID'] = "Die Funktion wurde nicht angegeben";
$MESS ['SONET_GFP_EMPTY_OPERATION_ID'] = "Die Operation wurde nicht angegeben";
$MESS ['SONET_GFP_EMPTY_ROLE'] = "Die Rolle wurde nicht angegeben";
$MESS ['SONET_GF_EMPTY_ENTITY_ID'] = "Die Elementen ID wurde nicht angegeben";
$MESS ['SONET_GF_ERROR_NO_ENTITY_ID'] = "Das Element wurde falsch angegeben";
$MESS ['SONET_GF_ERROR_NO_ENTITY_TYPE'] = "Der Elementtyp ist falsch";
$MESS ['SONET_GF_EMPTY_ENTITY_TYPE'] = "Der Elementtyp ist falsch";
$MESS ['SONET_GFP_NO_OPERATION_ID'] = "Die Operation der Parameter ist falsch";
$MESS ['SONET_GFP_ERROR_NO_ROLE'] = "Die Rolle wurde falsch angegeben";
?>