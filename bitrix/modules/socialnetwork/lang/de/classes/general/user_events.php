<?
$MESS ['SONET_UE_EMPTY_EVENT_ID'] = "Das Ereignis wurde nicht angegeben";
$MESS ['SONET_UE_ERROR_NO_EVENT_ID'] = "Das Event wurde falsch angegeben";
$MESS ['SONET_UE_ERROR_NO_SITE'] = "Die Seite wurde falsch angegeben";
$MESS ['SONET_UE_EMPTY_SITE_ID'] = "Die Seite wurde nicht angegeben";
$MESS ['SONET_GB_EMPTY_USER_ID'] = "Der Nutzer wurde nicht angegeben";
$MESS ['SONET_GB_ERROR_NO_USER_ID'] = "Der Nutzer wurde falsch angegeben";
?>