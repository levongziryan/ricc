<?
$MESS ['SONET_M_ERROR_DELETE_MESSAGE'] = "Die Nachricht konnte nicht gelöscht werden";
$MESS ['SONET_UR_ERROR_UPDATE_MESSAGE'] = "Die Nachricht konnte nicht aktualisiert werden";
$MESS ['SONET_UR_ERROR_CREATE_MESSAGE'] = "Beim Erstellen einer Nachricht ist ein Fehler aufgetreten";
$MESS ['SONET_GB_EMPTY_DATE_CREATE'] = "Das Erstellungsdatum wurde falsch angegeben";
$MESS ['SONET_UR_EMPTY_MESSAGE_ID'] = "Die Nachrichten ID wurde nicht angegeben";
$MESS ['SONET_UR_EMPTY_MESSAGE'] = "Die Nachricht wurde nicht angegeben";
$MESS ['SONET_UR_EMPTY_TARGET_USER_ID'] = "Der Empfänger der Nachricht wurde nicht angegeben";
$MESS ['SONET_UR_EMPTY_SENDER_USER_ID'] = "Der Absender der Nachricht wurde nicht angegeben";
$MESS ['SONET_UR_NO_MESSAGE'] = "Die Nachricht wurde nicht gefunden";
$MESS ['SONET_M_EMPTY_TO_USER_ID'] = "Der Empfänger wurde nicht angegeben";
$MESS ['SONET_M_ERROR_NO_TO_USER_ID'] = "Der Empfänger wurde falsch angegeben";
$MESS ['SONET_M_EMPTY_FROM_USER_ID'] = "Der Absender wurde nicht angegeben";
$MESS ['SONET_M_ERROR_NO_FROM_USER_ID'] = "Der Absender wurde falsch angegeben";
$MESS ['SONET_MM_EMPTY_DATE_VIEW'] = "Das Datum wurde falsch angegeben";
?>