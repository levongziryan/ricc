<?
$MESS["SONET_NS_INVITE_USER"] = "Einladung zu Freunden";
$MESS["SONET_NS_INVITE_GROUP"] = "Einladungen und Anfragen wegen Gruppenbeitritt";
$MESS["SONET_NS_INOUT_GROUP"] = "Änderungen bei Gruppenteilnehmern";
$MESS["SONET_NS_MODERATORS_GROUP"] = "Zum Moderator ernannt/aus Moderatoren ausgeschlossen";
$MESS["SONET_NS_OWNER_GROUP"] = "Gruppenbesitzer ändern";
$MESS["SONET_NS_FRIEND"] = "Ist in der Freundesliste/ist nicht in der Freundesliste";
$MESS["SONET_NS_SONET_GROUP_EVENT"] = "Aktualisierungen in der Arbeitsgruppe";
?>