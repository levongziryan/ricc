<?
$MESS["authprov_sg_a"] = "Gruppenbesitzer";
$MESS["authprov_sg_e"] = "Gruppenmoderatoren";
$MESS["authprov_sg_k"] = "Alle Gruppenmitglieder";
$MESS["authprov_sg_panel_last"] = "Letzte";
$MESS["authprov_sg_panel_my_group"] = "Meine Gruppen ";
$MESS["authprov_sg_panel_search"] = "Suchen";
$MESS["authprov_sg_panel_search_text"] = "Geben Sie den Namen der Gruppe ein.";
$MESS["authprov_sg_name"] = "Soziales Netzwerk: Gruppen";
$MESS["authprov_sg_name_out"] = "Gruppe des Sozialen Netzwerks";
$MESS["authprov_sg_socnet_group"] = "Gruppe des Sozialen Netzwerks";
$MESS["authprov_sg_current"] = "Aktuelle Gruppe";
?>