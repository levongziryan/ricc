<?
$MESS ['SONETE_DELETE_SUBJECT_CONFIRM'] = "Wollen Sie dieses Thema wirklich löschen? Wenn in diesem Thema noch Gruppen existieren, wird sie nicht gelöscht.";
$MESS ['SONETE_ADDING'] = "Ein neues Thema erstellen";
$MESS ['SONETE_DELETE_SUBJECT'] = "Thema löschen";
$MESS ['SONETE_UPDATING'] = "Themenparameter bearbeiten";
$MESS ['SONETE_ERROR_SAVING'] = "Beim Speichern des Themas ist ein Fehler aufgetreten";
$MESS ['SONETE_TAB_SUBJECT'] = "Gruppenthema";
$MESS ['SONETE_TAB_SUBJECT_DESCR'] = "Gruppenthema-Parameter";
$MESS ['SONETE_NO_PERMS2ADD'] = "Sie haben nicht genügend Rechte, um ein Thema hinzuzufügen";
$MESS ['SONETE_NEW_SUBJECT'] = "Neues Thema";
$MESS ['SONETE_SITE'] = "Themenseite";
$MESS ['SONETE_SORT'] = "Themensortierung";
$MESS ['SONETE_NAME'] = "Themenüberschrift";
$MESS ['SONETE_2FLIST'] = "Themen";
?>