<?
$MESS["USER_NAME_TEMPLATE"] = "[#ID#] #NAME# #LAST_NAME#";
$MESS["SONET_ERROR_UPDATE"] = "Fehler der Aktualisierung der Gruppenparameter";
$MESS["SONET_DELETE_ALT"] = "Gruppe löschen ";
$MESS["SONET_DELETE_CONF"] = "Sind Sie sicher, dass Sie diese Gruppe löschen wollen?";
$MESS["SONET_DELETE_ERROR"] = "Fehler beim Löschen der Gruppe";
$MESS["SONET_GROUP_NAV"] = "Gruppen";
$MESS["SONET_GROUP_SUBJECT_ID"] = "Thema";
$MESS["SONET_GROUP_OWNER_ID"] = "Eigentümer ";
$MESS["SONET_SUBJECT_SORT"] = "Sort. ";
$MESS["SONET_TITLE"] = "Gruppen ";
$MESS["SONET_UPDATE_ALT"] = "Gruppenparameter ändern ";
$MESS["SONET_GROUP_NAME"] = "Name ";
$MESS["SONET_FILTER_SITE_ID"] = "Website ";
$MESS["SONET_FILTER_SUBJECT_ID"] = "Thema ";
$MESS["SONET_SPT_ALL"] = "[alle]";
$MESS["SONET_ADMIN_LIST_CHANGE_OWNER"] = "Eigentümer ändern ";
$MESS["SONET_OWNER_USER"] = "Eigentümer ";
$MESS["SONET_OWNER_ID"] = "Eigentümer-ID  ";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Gesamt:";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "Ausgewählt:";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "löschen";
?>