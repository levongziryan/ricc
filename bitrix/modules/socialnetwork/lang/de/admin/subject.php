<?
$MESS["SONET_SPT_ALL"] = "[alle]";
$MESS["SONET_DELETE_CONF"] = "Wollen Sie dieses Thema wirklich löschen?";
$MESS["SONET_ADD_NEW_ALT"] = "Klicken Sie hier, um ein neues Thema hinzuzufügen";
$MESS["SONET_DELETE_ALT"] = "Thema löschen";
$MESS["SONET_UPDATE_ALT"] = "Themenparameter bearbeiten";
$MESS["SONET_DELETE_ERROR"] = "Beim Löschen des Themas ist ein Fehler aufgetreten";
$MESS["SONET_ERROR_UPDATE"] = "Beim Aktualisieren der Themenparameter ist ein Fehler aufgetreten";
$MESS["SONET_TITLE"] = "Gruppenthemen";
$MESS["SONET_SUBJECT_NAME"] = "Überschrift";
$MESS["SONET_ADD_NEW"] = "Neues Thema";
$MESS["SONET_FILTER_SITE_ID"] = "Seite";
$MESS["SONET_SUBJECT_SITE_ID"] = "Seite";
$MESS["SONET_SUBJECT_SORT"] = "Sort.";
$MESS["SONET_SUBJECT_NAV"] = "Themen";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Gesamt:";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "Ausgewählt:";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "löschen";
?>