<?
$MESS["SONET_NEW_EVENT_GROUP_DESC"] = "#ENTITY_ID# - Gruppen-ID
#LOG_DATE# - Eintragsdatum
#TITLE# - Überschrift
#MESSAGE# - Nachricht
#URL# - Adresse
#GROUP_NAME# - Gruppenname
#SUBSCRIBER_NAME# - Vorname des Empfängers
#SUBSCRIBER_LAST_NAME# - Nachname des Empfängers
#SUBSCRIBER_EMAIL# - E-Mail des Empfängers
#SUBSCRIBER_ID# - Nutzer-ID";
$MESS["SONET_NEW_EVENT_USER_DESC"] = "#ENTITY_ID# - Gruppen-ID
#LOG_DATE# - Eintragsdatum
#TITLE# - Überschrift
#MESSAGE# - Nachricht
#URL# - Adresse
#USER_NAME# - Name des Nutzers
#SUBSCRIBER_NAME# - Vorname des Empfängers
#SUBSCRIBER_LAST_NAME# - Nachname des Empfängers
#SUBSCRIBER_EMAIL# - E-Mail des Empfängers
#SUBSCRIBER_ID# - Nutzer-ID";
$MESS["SONET_NEW_MESSAGE_DESC"] = "#MESSAGE_ID# - Nachrichten-ID
#USER_ID# - Nutzer-ID
#USER_NAME# - Vorname des Nutzers
#USER_LAST_NAME# - Nachname des Nutzers
#SENDER_ID# - ID des Absenders
#SENDER_NAME# - Vorname des Absenders
#SENDER_LAST_NAME# - Nachname des Absenders
#TITLE# - Nachrichtenüberschrift
#MESSAGE# - Nachricht
#EMAIL_TO# - E-Mail-Adresse des Empfängers";
$MESS["SONET_REQUEST_GROUP_DESC"] = "#MESSAGE_ID# - Nachricht-ID
#USER_ID# - Anfrageempfänger-ID
#USER_NAME# - Vorname des Anfrageempfängers
#USER_LAST_NAME# - Name des Anfrageempfängers
#SENDER_ID# - Anfrageabsender-ID
#SENDER_NAME# - Vorname des Anfrageabsenders
#SENDER_LAST_NAME# - Name des Anfrageabsenders
#TITLE# - Überschrift
#MESSAGE# - Nachricht
#EMAIL_TO# - Empfängeradresse";
$MESS["SONET_INVITE_GROUP_DESC"] = "#RELATION_ID# - Verbindungs ID
#GROUP_ID# - Gruppen ID
#USER_ID# - Nutzer-ID
#GROUP_NAME# - Gruppenname
#USER_NAME# - Vorname des Nutzers
#USER_LAST_NAME# - Nachname des Nutzers
#USER_EMAIL# - E-Mail-Adresse des Nutzers
#MESSAGE# - Nachricht";
$MESS["SONET_AGREE_FRIEND_DESC"] = "#RELATION_ID# - Verbindungs ID
#SENDER_USER_ID# - Absender ID
#SENDER_USER_NAME# - Vorname des Absenders
#SENDER_USER_LAST_NAME# - Nachname des Absenders
#SENDER_EMAIL_TO# - E-Mail-Adresse des Absenders
#RECIPIENT_USER_ID# - Empfänger ID
#RECIPIENT_USER_NAME# - Vorname des Empfängers
#RECIPIENT_USER_LAST_NAME# - Nachname des Empfängers
#RECIPIENT_USER_EMAIL_TO# - E-Mail-Adresse des Empfängers
#MESSAGE# - Nachricht";
$MESS["SONET_BAN_FRIEND_DESC"] = "#RELATION_ID# - Verbindungs ID
#SENDER_USER_ID# - Absender ID
#SENDER_USER_NAME# - Vorname des Absenders
#SENDER_USER_LAST_NAME# - Nachname des Absenders
#SENDER_EMAIL_TO# - E-Mail-Adresse des Absenders
#RECIPIENT_USER_ID# - Empfänger ID
#RECIPIENT_USER_NAME# - Vorname des Empfängers
#RECIPIENT_USER_LAST_NAME# - Nachname des Empfängers
#RECIPIENT_USER_EMAIL_TO# - E-Mail-Adresse des Empfängers
#MESSAGE# - Nachricht";
$MESS["SONET_INVITE_FRIEND_DESC"] = "#RELATION_ID# - Verbindungs ID
#SENDER_USER_ID# - Absender ID
#SENDER_USER_NAME# - Vorname des Absenders
#SENDER_USER_LAST_NAME# - Nachname des Absenders
#SENDER_EMAIL_TO# - E-Mail-Adresse des Absenders
#RECIPIENT_USER_ID# - Empfänger ID
#RECIPIENT_USER_NAME# - Vorname des Empfängers
#RECIPIENT_USER_LAST_NAME# - Nachname des Empfängers
#RECIPIENT_USER_EMAIL_TO# - E-Mail-Adresse des Empfängers
#MESSAGE# - Nachricht";
$MESS["SONET_BAN_FRIEND_SUBJECT"] = "#SITE_NAME#: Eintragung in die schwarze Liste";
$MESS["SONET_AGREE_FRIEND_SUBJECT"] = "#SITE_NAME#: Bestätigung der Freundschaftseinladung";
$MESS["SONET_INVITE_FRIEND_SUBJECT"] = "#SITE_NAME#: Einladung als Freund";
$MESS["SONET_INVITE_GROUP_SUBJECT"] = "#SITE_NAME#: Einladung der Gruppe beitreten";
$MESS["SONET_NEW_EVENT_GROUP_SUBJECT"] = "#SITE_NAME#: #GROUP_NAME# - Neues Gruppenevent";
$MESS["SONET_NEW_EVENT_USER_SUBJECT"] = "#SITE_NAME#: #USER_NAME# - Neuer Termin der Nutzers";
$MESS["SONET_NEW_MESSAGE_SUBJECT"] = "#SITE_NAME#: Sie haben eine neue Nachricht";
$MESS["SONET_REQUEST_GROUP_SUBJECT"] = "#TITLE#";
$MESS["SONET_BAN_FRIEND_NAME"] = "In die schwarze Liste eingetragen";
$MESS["SONET_AGREE_FRIEND_NAME"] = "Bestätigung der Freundeseinladung";
$MESS["SONET_AGREE_FRIEND_MESSAGE"] = "Informationsnachricht von #SITE_NAME#
------------------------------------------

Hallo, #RECIPIENT_USER_NAME#!

#SENDER_USER_NAME# #SENDER_USER_LAST_NAME# hat Ihre Einladung zum Freund akzeptiert.

Dies ist eine automatisch generierte Nachricht.";
$MESS["SONET_BAN_FRIEND_MESSAGE"] = "Informationsnachricht von #SITE_NAME#
------------------------------------------

Hallo, #RECIPIENT_USER_NAME#!

#SENDER_USER_NAME# #SENDER_USER_LAST_NAME# hat Sie in die schwarze Liste eingetragen.

Dies ist eine automatisch generierte Nachricht.";
$MESS["SONET_INVITE_FRIEND_MESSAGE"] = "Informationsnachricht von #SITE_NAME#
------------------------------------------

Hallo, #RECIPIENT_USER_NAME#!

#SENDER_USER_NAME# #SENDER_USER_LAST_NAME# möchte Sie zum Freund einladen.

Um auf die Einladung zu antworten, klicken Sie auf den folgenden Link:

http://#SERVER_NAME##URL#

Nachricht vom User:
------------------------------------------
#MESSAGE#
------------------------------------------

Dies ist eine automatisch generierte Nachricht.";
$MESS["SONET_NEW_EVENT_GROUP_MESSAGE"] = "Informationsnachricht von #SITE_NAME#
------------------------------------------

Hallo, #SUBSCRIBER_NAME#!

In der Gruppe #GROUP_NAME# haben sich folgende Veränderungen ereignet:

#TITLE#

------------------------------------------
#MESSAGE#
------------------------------------------

Um zur Seite zu gelangen, klicken Sie bitte auf den folgenden Link:

http://#SERVER_NAME##URL#

Dies ist eine automatisch generierte Nachricht.";
$MESS["SONET_NEW_EVENT_USER_MESSAGE"] = "Informationsnachricht von #SITE_NAME#
------------------------------------------

Hallo, #SUBSCRIBER_NAME#!

Beim User #USER_NAME# haben sich folgende Veränderungen ereignet:

#TITLE#

------------------------------------------
#MESSAGE#
------------------------------------------

Um zur Seite zu gelangen, klicken Sie bitte auf den folgenden Link:

http://#SERVER_NAME##URL#

Dies ist eine automatisch generierte Nachricht.";
$MESS["SONET_INVITE_GROUP_MESSAGE"] = "Informationsnachricht von #SITE_NAME#
------------------------------------------

Hallo, #USER_NAME#!

Gruppe #GROUP_NAME# läd Sie zur Mitgliedschaft ein.

Um auf die Einladung zu antworten, klicken Sie auf den folgenden Link:

http://#SERVER_NAME##URL#

Nachricht von der Gruppe:
------------------------------------------
#MESSAGE#
------------------------------------------

Dies ist eine automatisch generierte Nachricht.";
$MESS["SONET_NEW_MESSAGE_MESSAGE"] = "Informationsnachricht von #SITE_NAME#
------------------------------------------

Hallo, #USER_NAME#!

Sie haben eine neue Nachricht vom User #SENDER_NAME# #SENDER_LAST_NAME# erhalten:

------------------------------------------
#MESSAGE#
------------------------------------------

Link zur Nachricht:

http://#SERVER_NAME#/company/personal/messages/chat/#SENDER_ID#/

Dies ist eine automatisch generierte Nachricht.";
$MESS["SONET_REQUEST_GROUP_NAME"] = "Anfrage zum Gruppenbeitritt";
$MESS["SONET_INVITE_FRIEND_NAME"] = "Einladung zur Freundesliste";
$MESS["SONET_INVITE_GROUP_NAME"] = "Einladung zur Gruppe";
$MESS["SONET_REQUEST_GROUP_MESSAGE"] = "Nachricht von #SITE_NAME#
------------------------------------------

Hallo #USER_NAME#!

------------------------------------------
#MESSAGE#
------------------------------------------

Diese Nachricht wurde automatisch erstellt.";
$MESS["SONET_NEW_EVENT_GROUP_NAME"] = "Neue Gruppenevents";
$MESS["SONET_NEW_EVENT_USER_NAME"] = "Neue Termine des Nutzers";
$MESS["SONET_NEW_MESSAGE_NAME"] = "Sie haben eine neue Nachricht";
$MESS["SONET_NEW_EVENT_NAME"] = "Neuer Event";
$MESS["SONET_NEW_EVENT_MESSAGE"] = "Grüße von #SITE_NAME#!  Lieber Nutzer #SUBSCRIBER_NAME#!

Folgende Aktualisierung wurden auf der Website nach Ihrem letzten Besuch durchgeführt:

#TITLE#

------------------------------------------
#MESSAGE#
------------------------------------------

Nutzen Sie den folgenden Link, um den Event anzuzeigen:

#URL#

Diese Nachricht wurde automatisch erstellt.";
$MESS["SONET_NEW_EVENT_DESC"] = "#ENTITY_ID# - Die ID der Eventquelle
#LOG_DATE# - Das Datum, an dem Event ins Protokoll eingetragen wurde
#TITLE# - Die Überschrift
#MESSAGE# -Die Nachricht
#URL# - URL
#ENTITY# - Der Ort der Eventregistrierung
#SUBSCRIBER_NAME# - Der Vorname des Empfängers
#SUBSCRIBER_LAST_NAME# - Der Nachname des Empfängers
#SUBSCRIBER_EMAIL# - Die E-Mail-Adresse des Empfängers
#SUBSCRIBER_ID# -Die ID des Empfängers
";
$MESS["SONET_NEW_EVENT_SUBJECT"] = "#SITE_NAME#: #ENTITY# - Neuer Event in #ENTITY_TYPE#";
$MESS["SONET_LOG_NEW_ENTRY_NAME"] = "Neue Nachricht hinzugefügt";
$MESS["SONET_LOG_NEW_ENTRY_DESC"] = "#EMAIL_TO# - E-Mail des Nachrichtenempfängers 
#LOG_ENTRY_ID# - ID der Nachricht
#RECIPIENT_ID# - ID des Empfängers
#URL_ID# - URL der Nachrichtenansicht
";
$MESS["SONET_LOG_NEW_COMMENT_NAME"] = "Neuer Kommentar hinzugefügt";
$MESS["SONET_LOG_NEW_COMMENT_DESC"] = "#EMAIL_TO# - E-Mail des Nachrichtenempfängers 
#COMMENT_ID# - ID des Kommentars
#LOG_ENTRY_ID# - ID der Nachricht
#RECIPIENT_ID# - ID des Empfängers
#URL_ID# - URL der Nachrichtenansicht
";
?>