<?
$MESS["SONET_COMMENTAUX_JS_SHARE_TEXT"] = "Neue Empfänger: #SHARE_LIST#";
$MESS["SONET_COMMENTAUX_JS_SHARE_TEXT_1"] = "Neue Empfänger: #SHARE_LIST#";
$MESS["SONET_COMMENTAUX_JS_CREATETASK_BLOG_POST"] = "Die Aufgabe \"#TASK_NAME#\" wurde aus einer Nachricht im Activity Stream erstellt.";
$MESS["SONET_COMMENTAUX_JS_CREATETASK_BLOG_COMMENT"] = "Die Aufgabe \"#TASK_NAME#\" wurde aus #COMMENT_LINK# erstellt.";
$MESS["SONET_COMMENTAUX_JS_CREATETASK_BLOG_COMMENT_LINK"] = "Ein Kommentar im Activity Stream";
$MESS["SONET_COMMENTAUX_JS_FILEVERSION_TEXT"] = "hat eine neue Dateiversion hochgeladen";
$MESS["SONET_COMMENTAUX_JS_FILEVERSION_TEXT_M"] = "hat eine neue Dateiversion hochgeladen";
$MESS["SONET_COMMENTAUX_JS_FILEVERSION_TEXT_F"] = "hat eine neue Dateiversion hochgeladen";
$MESS["SONET_COMMENTAUX_JS_HEAD_FILEVERSION_TEXT"] = "hat die Datei bearbeitet";
$MESS["SONET_COMMENTAUX_JS_HEAD_FILEVERSION_TEXT_M"] = "hat die Datei bearbeitet";
$MESS["SONET_COMMENTAUX_JS_HEAD_FILEVERSION_TEXT_F"] = "hat die Datei bearbeitet";
?>