<?
$MESS["SONET_INSTALL_NAME"] = "Soziales Netzwerk";
$MESS["SONET_INSTALL_DESCRIPTION"] = "Das Modul ermöglicht ein soziales Netzwerk auf der Seite zu erstellen.";
$MESS["SONET_INSTALL_TITLE"] = "Das Modul \"Soziales Netz\" installieren";
$MESS["SONETP_PERM_D"] = "Arbeit im öffentlichen Bereich ohne der Berechtigung, Gruppen zu erstellen";
$MESS["SONETP_PERM_K"] = "Arbeit im öffentlichen Bereich mit der Berechtigung, Gruppen zu erstellen";
$MESS["SONETP_PERM_R"] = "administrativen Bereich anzeigen";
$MESS["SONETP_PERM_W"] = "voller Zugriff";
$MESS["SONETP_COPY_FOLDER"] = "Ordner, in den die Dateien kopiert werden (relativ zum Root-Verzeichnis)";
$MESS["SONET_INSTALL_PUBLIC_REW"] = "Vorhandene Dateien überschreiben";
$MESS["SONETP_REWRITE_ADD"] = "Hilfsdateien überschreiben";
$MESS["SONETP_INSTALL_EMAIL"] = "Eine E-Mail Vorlage erstellen";
$MESS["SONETP_DELETE_EMAIL"] = "E-Mail Vorlage löschen";
$MESS["SONETP_INSTALL_404"] = "Öffentlichen Bereich im SEO Modus installieren";
$MESS["SONETP_RW_DEF_IMAGES"] = "Das Bild in der Vorlage überschreiben .default";
$MESS["SONETP_INSTALL_SMILES"] = "Smileys installieren";
$MESS["SONETP_NOT_INSTALL_P"] = "nicht installieren";
$MESS["SONETP_COPY_PUBLIC_FILES"] = "Den Bereich Ansicht und die Demodaten installieren";
$MESS["SONETP_EDIT_FORM_LABEL"] = "Ankündigung";
$MESS["SONETP_LIST_COLUMN_LABEL"] = "Ankündigung";
$MESS["SONETP_LIST_FILTER_LABEL"] = "Ankündigung";
$MESS["SONET_UF_SG_DEPT_EDIT_FORM_LABEL"] = "Abteilungen";
$MESS["SONET_UF_SG_DEPT_LIST_COLUMN_LABEL"] = "Abteilungen";
$MESS["SONET_UF_SG_DEPT_LIST_FILTER_LABEL"] = "Abteilungen";
?>