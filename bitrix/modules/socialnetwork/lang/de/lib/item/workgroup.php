<?
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_OPEN"] = "Offenes Projekt";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_OPEN_DESC"] = "Das Projekt ist für alle sichtbar. Jeder kann Projektteilnehmer werden.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_CLOSED"] = "Geschlossenes Projekt";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_CLOSED_DESC"] = "Das Projekt ist nur für Projektteilnehmer sichtbar. Einladung ist erforderlich, um Projektteilnehmer zu werden.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_EXTERNAL"] = "Externes Projekt";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_EXTERNAL_DESC"] = "Das Projekt ist nur für Projektteilnehmer sichtbar. Externe Nutzer können zum Projekt eingeladen werden.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_OPEN"] = "Offene Gruppe";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_OPEN_DESC"] = "Die Gruppe ist für alle sichtbar. Jeder kann der Gruppe beitreten.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_CLOSED"] = "Geschlossene Gruppe";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_CLOSED_DESC"] = "Die Gruppe ist nur für Gruppenteilnehmer sichtbar. Einladung ist erforderlich, um Gruppenteilnehmer zu werden.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_EXTERNAL"] = "Externe Gruppe";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_EXTERNAL_DESC"] = "Die Gruppe ist nur für Gruppenteilnehmer sichtbar. Externe Nutzer können  in die Gruppe eingeladen werden.";
?>