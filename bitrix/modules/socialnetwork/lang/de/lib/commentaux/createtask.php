<?
$MESS["SONET_COMMENTAUX_CREATETASK_BLOG_POST"] = "Die Aufgabe \"#TASK_NAME#\" wurde aus einer Nachricht im Activity Stream erstellt.";
$MESS["SONET_COMMENTAUX_CREATETASK_BLOG_COMMENT"] = "Die Aufgabe \"#TASK_NAME#\" wurde aus #COMMENT_LINK# erstellt.";
$MESS["SONET_COMMENTAUX_CREATETASK_BLOG_COMMENT_LINK"] = "Ein Kommentar im Activity Stream";
$MESS["SONET_COMMENTAUX_CREATETASK_NOT_FOUND"] = "&lt;Aufgabe wurde nicht gefunden.&gt;";
?>