<?
$MESS["BLG_NAME"] = "Nutzernachrichten von";
$MESS["BLG_SHARE_ALL"] = "Alle Mitarbeiter";
$MESS["BLG_SHARE_ALL_BUS"] = "Alle Nutzer";
$MESS["BLG_SHARE"] = "Neue Empfänger: ";
$MESS["BLG_SHARE_1"] = "Neue Empfänger:";
$MESS["BLG_SHARE_HIDDEN_1"] = "Verborgener Empfänger";
$MESS["SONET_HELPER_CREATED_BY_ANONYMOUS"] = "Nicht autorisierter Besucher";
$MESS["BLG_TASK_CREATED_POST"] = "Die Aufgabe \"#TASK_NAME#\" wurde aus einer Nachricht im Activity Stream erstellt.";
$MESS["BLG_TASK_CREATED_COMMENT"] = "Die Aufgabe \"#TASK_NAME#\" wurde aus #LINK# erstellt.";
$MESS["BLG_TASK_CREATED_COMMENT_LINK"] = "Ein Kommentar im Activity Stream";
$MESS["SONET_HELPER_STEPPER_LIVEFEED"] = "Neuindexierung des Activity Streams";
?>