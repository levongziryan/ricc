<?
$MESS["SOCIALNETWORK_WORKGROUP_CHAT_TITLE"] = "Projektgruppe: \"#GROUP_NAME#\"";
$MESS["SOCIALNETWORK_WORKGROUP_CHAT_UNLINKED"] = "Arbeitsgruppe \"#GROUP_NAME#\" ist mit diesem Chat nicht mehr verknüpft.";
$MESS["SOCIALNETWORK_WORKGROUP_CHAT_TITLE_PROJECT"] = "Projekt: \"#GROUP_NAME#\"";
$MESS["SOCIALNETWORK_WORKGROUP_CHAT_UNLINKED_PROJECT"] = "Projekt \"#GROUP_NAME#\" ist mit diesem Chat nicht mehr verknüpft.";
?>