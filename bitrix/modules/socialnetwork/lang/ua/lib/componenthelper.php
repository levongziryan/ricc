<?
$MESS["BLG_NAME"] = "Блог користувача";
$MESS["BLG_SHARE_ALL"] = "Всі співробітники";
$MESS["BLG_SHARE_ALL_BUS"] = "Всі користувачі";
$MESS["BLG_SHARE"] = "Нові одержувачі:";
$MESS["BLG_SHARE_1"] = "Новий одержувач:";
$MESS["BLG_SHARE_HIDDEN_1"] = "Прихований одержувач";
$MESS["SONET_HELPER_CREATED_BY_ANONYMOUS"] = "Неавторизований користувач";
$MESS["BLG_TASK_CREATED_POST"] = "Створено завдання \"#TASK_NAME#\" на підставі повідомлення Живий стрічки.";
$MESS["BLG_TASK_CREATED_COMMENT"] = "Створено завдання \"#TASK_NAME#\" на підставі #LINK#.";
$MESS["BLG_TASK_CREATED_COMMENT_LINK"] = "коментаря до повідомлення Живої стрічки";
$MESS["SONET_HELPER_STEPPER_LIVEFEED"] = "Переіндексація повідомлень живої стрічки";
?>