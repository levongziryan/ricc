<?
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_OPEN"] = "Відкритий проект";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_OPEN_DESC"] = "Наявність проекту видно всім співробітникам, всі можуть вступити до нього.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_CLOSED"] = "Закритий проект";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_CLOSED_DESC"] = "Проект видимий лише його учасникам, вступити до нього можна лише на запрошення.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_EXTERNAL"] = "Зовнішній проект";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_EXTERNAL_DESC"] = "Проект видимий лише його учасникам, до нього можна запрошувати зовнішніх користувачів.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_OPEN"] = "Відкрита група";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_OPEN_DESC"] = "Наявність групи видно всім користувачам, всі можуть вступити до неї.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_CLOSED"] = "Закрита група";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_CLOSED_DESC"] = "Група видима лише її членам, вступити до неї можна лише після затвердження модератором.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_EXTERNAL"] = "Зовнішня група";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_EXTERNAL_DESC"] = "Група видима лише її членам, до неї можна запрошувати зовнішніх користувачів.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_OPEN_DESC2"] = "Наявність проекту видно всім співробітникам.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_OPEN_DESC2"] = "Наявність групи видно всім користувачам.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_OWNER"] = "Тільки власник групи";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_MOD"] = "Власник групи і модератори групи";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_IP_USER"] = "Всі члени групи";
?>