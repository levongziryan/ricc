<?
$MESS["SONET_COMMENTAUX_CREATETASK_BLOG_POST"] = "Створено завдання \"#TASK_NAME#\" на підставі повідомлення Живої стрічки.";
$MESS["SONET_COMMENTAUX_CREATETASK_BLOG_COMMENT"] = "Створено завдання \"#TASK_NAME#\" на підставі #COMMENT_LINK#.";
$MESS["SONET_COMMENTAUX_CREATETASK_BLOG_COMMENT_LINK"] = "коментар до повідомлення Живої стрічки";
$MESS["SONET_COMMENTAUX_CREATETASK_NOT_FOUND"] = "&lt;завдання не знайдено&gt;";
?>