<?
$MESS["SONET_COMMENTAUX_JS_SHARE_TEXT"] = "Новий одержувач: #SHARE_LIST#";
$MESS["SONET_COMMENTAUX_JS_SHARE_TEXT_1"] = "Нові одержувачі: #SHARE_LIST#";
$MESS["SONET_COMMENTAUX_JS_CREATETASK_BLOG_POST"] = "Створено завдання \"#TASK_NAME#\" на підставі повідомлення Живий стрічки.";
$MESS["SONET_COMMENTAUX_JS_CREATETASK_BLOG_COMMENT"] = "Створено завдання \"#TASK_NAME#\" на підставі #COMMENT_LINK#.";
$MESS["SONET_COMMENTAUX_JS_CREATETASK_BLOG_COMMENT_LINK"] = "коментар до повідомлення Живої стрічки";
$MESS["SONET_COMMENTAUX_JS_FILEVERSION_TEXT"] = "Завантажив(ла) нову версію файлу";
$MESS["SONET_COMMENTAUX_JS_FILEVERSION_TEXT_M"] = "Завантажив нову версію файлу";
$MESS["SONET_COMMENTAUX_JS_FILEVERSION_TEXT_F"] = "Завантажила нову версію файлу";
$MESS["SONET_COMMENTAUX_JS_HEAD_FILEVERSION_TEXT"] = "Відредагував(а) файл";
$MESS["SONET_COMMENTAUX_JS_HEAD_FILEVERSION_TEXT_M"] = "Відредагував файл";
$MESS["SONET_COMMENTAUX_JS_HEAD_FILEVERSION_TEXT_F"] = "Відредагувала файл";
?>