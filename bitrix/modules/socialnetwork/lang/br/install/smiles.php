<?
$MESS["FICON_EEK"] = "Maravilhado";
$MESS["FICON_EVIL"] = "Nervoso";
$MESS["FICON_BIGGRIN"] = "Big Grin";
$MESS["FICON_REDFACE"] = "ConfUsard";
$MESS["FICON_COOL"] = "Legal";
$MESS["FICON_CRY"] = "Chorando";
$MESS["FICON_EXCLAIM"] = "Exclamação";
$MESS["FICON_IDEA"] = "Idéia";
$MESS["FICON_KISS"] = "Beijo";
$MESS["FICON_QUESTION"] = "Pergunta";
$MESS["FICON_SAD"] = "Triste";
$MESS["FICON_NEUTRAL"] = "Céptico";
$MESS["FICON_SMILE"] = "Sorriso";
$MESS["FICON_WINK"] = "Piscar de olhos";
?>