<?
$MESS["SONET_INSTALL_NAME"] = "Rede Social";
$MESS["SONET_INSTALL_DESCRIPTION"] = "Adiciona o recurso de rede social ao seu site";
$MESS["SONET_INSTALL_TITLE"] = "Instalar Rede Social";
$MESS["SONETP_PERM_D"] = "Pode visualizar a sessão pública, não pode criar grupos";
$MESS["SONETP_PERM_K"] = "Pode visualizar a sessão pública, pode criar grupos";
$MESS["SONETP_PERM_R"] = "Acesso de leitura ao painel de controle";
$MESS["SONETP_PERM_W"] = "Acesso completo";
$MESS["SONETP_COPY_PUBLIC_FILES"] = "instalar sessão pública a data demo";
$MESS["SONETP_COPY_FOLDER"] = "Pasta de destino (relativo a raiz do site)";
$MESS["SONET_INSTALL_PUBLIC_REW"] = "Sobrescrever arquivos existentes";
$MESS["SONETP_REWRITE_ADD"] = "Sobrescrever arquivos do sistema";
$MESS["SONETP_INSTALL_EMAIL"] = "Criar modelos de e-mail";
$MESS["SONETP_DELETE_EMAIL"] = "Excluir modelos de e-mail";
$MESS["SONETP_INSTALL_404"] = "Instalar sessão pública no modo SEF";
$MESS["SONETP_RW_DEF_IMAGES"] = "Substituir imagens no modelo &quot;.Padrão&quot;";
$MESS["SONETP_INSTALL_SMILES"] = "Instalar Smileys";
$MESS["SONETP_NOT_INSTALL_P"] = "não instalar";
$MESS["SONETP_EDIT_FORM_LABEL"] = "Anúncio";
$MESS["SONETP_LIST_COLUMN_LABEL"] = "Anúncio";
$MESS["SONETP_LIST_FILTER_LABEL"] = "Anúncio";
?>