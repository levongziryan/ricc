<?
$MESS["SONET_NEW_MESSAGE_NAME"] = "Você tem uma nova mensagem";
$MESS["SONET_NEW_MESSAGE_DESC"] = "#MESSAGE_ID# - ID da Mensagem
#USER_ID# - ID do usuario
#USER_NAME# - primeiro nome do usuário
#USER_LAST_NAME# - sobrenome do usuário
#SENDER_ID# - identificação do remetente de mensagem
#SENDER_NAME# - primero nome do remetente da mensagem
#SENDER_LAST_NAME# - Ultimo nome do remetente da mensagem
#TITLE# - Título da mensagem
#MESSAGE# - O corpo da mensagem
#EMAIL_TO# - endereço de e-mail do destinatário";
$MESS["SONET_NEW_MESSAGE_SUBJECT"] = "#SITE_NAME#: Você tem uma nova mensagem";
$MESS["SONET_NEW_MESSAGE_MESSAGE"] = "Saudações de #SITE_NAME#!
------------------------------------------

Olá #USER_NAME#!

Você tem uma nova mensagem de #SENDER_NAME# #SENDER_LAST_NAME#:

------------------------------------------
#MESSAGE#
------------------------------------------

Link para a mensagem:

http://#SERVER_NAME#/company/personal/messages/chat/#SENDER_ID#/

Essa é uma notificação gerada automaticamente
";
$MESS["SONET_INVITE_FRIEND_NAME"] = "Convite para juntar-se aos amigos";
$MESS["SONET_INVITE_FRIEND_DESC"] = "#RELATION_ID# - ID da Relação
#SENDER_USER_ID# - ID do Remetente
#SENDER_USER_NAME# - primeiro nome do remetente
#SENDER_USER_LAST_NAME# - sobrenome do remetente
#SENDER_EMAIL_TO# - endereço de e-mail do remetente
#RECIPIENT_USER_ID# - ID do Destinatário
#RECIPIENT_USER_NAME# - primeiro nome do destinatário
#RECIPIENT_USER_LAST_NAME# - ultimo nome do destinatário
#RECIPIENT_USER_EMAIL_TO# - endereço de e-mail do destinatário 
#MESSAGE# - O corpo da mensagem";
$MESS["SONET_INVITE_FRIEND_SUBJECT"] = "#SITE_NAME#: Convite de Amigos";
$MESS["SONET_INVITE_FRIEND_MESSAGE"] = "Saudações de #SITE_NAME#!
------------------------------------------

Olá #RECIPIENT_USER_NAME#!

#SENDER_USER_NAME# #SENDER_USER_LAST_NAME# Lhe enviou uma solicitação de amizade.

Clique no link abaixo para responder a solicitação.

http://#SERVER_NAME##URL#

Mensagem:
------------------------------------------
#MESSAGE#
------------------------------------------

Essa é uma notificação gerada automaticamente";
$MESS["SONET_INVITE_GROUP_NAME"] = "Convite para juntar-se ao grupo";
$MESS["SONET_INVITE_GROUP_DESC"] = "#RELATION_ID# - ID da Relação
#GROUP_ID# - ID do Grupo
#USER_ID# - ID do usuario
#GROUP_NAME# - Nome do grupo
#USER_NAME# - primeiro nome do usuario
USER_LAST_NAME# - sobrenome
 do usuario
#USER_EMAIL# - endereço de e-mail do usuario
#INITIATED_USER_NAME# - Nome do remetente do convite
#INITIATED_USER_LAST_NAME# - Sobrenome do remetente do convite
#URL# - Caminho para a página de mensagens do usuário
#MESSAGE# - O corpo da mensagem";
$MESS["SONET_INVITE_GROUP_SUBJECT"] = "Convite para Participar do Grupo: #SITE_NAME#";
$MESS["SONET_INVITE_GROUP_MESSAGE"] = "Saudações de #SITE_NAME#!
------------------------------------------

Olá #USER_NAME#!

USER #INITIATED_USER_NAME# #INITIATED_USER_LAST_NAME# está te convidando para juntar-se ao grupo de trabalho #GROUP_NAME#.

Clique no link abaixo para responder a solicitação:

http://#SERVER_NAME##URL#

Mensagem:
------------------------------------------
#MESSAGE#
------------------------------------------

Essa é uma notificação gerada automaticamente";
$MESS["SONET_AGREE_FRIEND_NAME"] = "Confirmação de convite de amigo";
$MESS["SONET_AGREE_FRIEND_DESC"] = "#RELATION_ID# - ID da Relação
#SENDER_USER_ID# - ID do Remetente
#SENDER_USER_NAME# - primeiro nome do remetente
#SENDER_USER_LAST_NAME# - sobrenome do remetente
#SENDER_EMAIL_TO# - endereço de e-mail do remetente
#RECIPIENT_USER_ID# - ID do Destinatário
#RECIPIENT_USER_NAME# - primeiro nome do destinatário
#RECIPIENT_USER_LAST_NAME# - ultimo nome do destinatário
#RECIPIENT_USER_EMAIL_TO# - endereço de e-mail do destinatário 
#MESSAGE# - O corpo da mensagem";
$MESS["SONET_AGREE_FRIEND_SUBJECT"] = "Confirmação de convite de amigo: #SITE_NAME#";
$MESS["SONET_AGREE_FRIEND_MESSAGE"] = "Saudações de #SITE_NAME#!
------------------------------------------

Olá #RECIPIENT_USER_NAME#!

#SENDER_USER_NAME# #SENDER_USER_LAST_NAME# Confirmou sua solicitação de amizade.

Essa é uma notificação gerada automaticamente";
$MESS["SONET_BAN_FRIEND_NAME"] = "Lista negra";
$MESS["SONET_BAN_FRIEND_DESC"] = "#RELATION_ID# - ID da Relação
#SENDER_USER_ID# - ID do Remetente
#SENDER_USER_NAME# - primeiro nome do remetente
#SENDER_USER_LAST_NAME# - sobrenome do remetente
#SENDER_EMAIL_TO# - endereço de e-mail do remetente
#RECIPIENT_USER_ID# - ID do Destinatário
#RECIPIENT_USER_NAME# - primeiro nome do destinatário
#RECIPIENT_USER_LAST_NAME# - ultimo nome do destinatário
#RECIPIENT_USER_EMAIL_TO# - endereço de e-mail do destinatário 
#MESSAGE# - O corpo da mensagem";
$MESS["SONET_BAN_FRIEND_SUBJECT"] = "#SITE_NAME#: Lista Negra";
$MESS["SONET_BAN_FRIEND_MESSAGE"] = "Saudações de #SITE_NAME#!
------------------------------------------

Olá #RECIPIENT_USER_NAME#!

#SENDER_USER_NAME# #SENDER_USER_LAST_NAME# Adicionou você a lista negra.

Essa é uma notificação gerada automaticamente";
$MESS["SONET_NEW_EVENT_GROUP_NAME"] = "Novo evento de grupo";
$MESS["SONET_NEW_EVENT_GROUP_DESC"] = "#ENTITY_ID# - ID do Grupo
#LOG_DATE# - data de entrada
#TITLE# - Título
#MENSAGEM# - Mensagem
#URL# - Endereço (URL)
#GROUP_NAME# - Título do grupo
#SUBSCRIBER_NAME# - primeiro nome do Assinante
#SUBSCRIBER_LAST_NAME# - sobrenome do Assinante
#SUBSCRIBER_EMAIL# - endereço de e-mail do Assinante 
#SUBSCRIBER_ID# - ID do assinante ";
$MESS["SONET_NEW_EVENT_GROUP_SUBJECT"] = "#SITE_NAME#: Novo Evento do Grupo";
$MESS["SONET_NEW_EVENT_GROUP_MESSAGE"] = "Saudações de #SITE_NAME#!
------------------------------------------

Olá #SUBSCRIBER_NAME#!

As seguintes modificações foram feitas no grupo #GROUP_NAME#:

#TITLE#

------------------------------------------
#MESSAGE#
------------------------------------------

Você pode acessa-lo a partir do seguinte link:

http://#SERVER_NAME##URL#

Essa é uma notificação gerada automaticamente";
$MESS["SONET_NEW_EVENT_USER_NAME"] = "Novo evento de usuário";
$MESS["SONET_NEW_EVENT_USER_DESC"] = "#ENTITY_ID# - ID do Grupo
#LOG_DATE# - data de entrada
#TITLE# - Título
#MENSAGEM# - Mensagem
#URL# - Endereço (URL)
#GROUP_NAME# - Título do grupo
#SUBSCRIBER_NAME# - primeiro nome do Assinante
#SUBSCRIBER_LAST_NAME# - sobrenome do Assinante
#SUBSCRIBER_EMAIL# - endereço de e-mail do Assinante 
#SUBSCRIBER_ID# - ID do assinante ";
$MESS["SONET_NEW_EVENT_NAME"] = "Novo evento";
$MESS["SONET_NEW_EVENT_DESC"] = "#ENTITY_ID# - O ID da fonte do evento
#LOG_DATE# - A data a qual o evento foi logado
#TITLE# - O título
#MESSAGE# - A mensagem
#URL# - URL
#ENTITY# - A disposição de registro de evento 
#SUBSCRIBER_NAME# - O primeiro nome do remetente
#SUBSCRIBER_LAST_NAME# - O último nome do remetente
#SUBSCRIBER_EMAIL# - O endereço de e-mail do remetente
#SUBSCRIBER_ID# - O ID do remetente";
$MESS["SONET_NEW_EVENT_USER_SUBJECT"] = "#SITE_NAME#: Novo evento de usuário";
$MESS["SONET_NEW_EVENT_USER_MESSAGE"] = "Saudações de #SITE_NAME#!
------------------------------------------

Olá #SUBSCRIBER_NAME#!

O USER #USER_NAME# tem os seguintes novos eventos:

#TITLE#

------------------------------------------
#MESSAGE#
------------------------------------------

Você pode acessar o site clicando no link abaixo:

http://#SERVER_NAME##URL#

Essa é uma notificação gerada automaticamente";
$MESS["SONET_NEW_EVENT_SUBJECT"] = "#SITE_NAME#: #ENTITY# - Novo evento em #ENTITY_TYPE#";
$MESS["SONET_NEW_EVENT_MESSAGE"] = "Saudações de #SITE_NAME#!
------------------------------------------

Caro USER #SUBSCRIBER_NAME#!

Ocorreram as seguintes atualizações desde sua última visita:

#TITLE#

------------------------------------------
#MESSAGE#
------------------------------------------

Usar o seguinte link para visualizar os eventos

#URL#

Essa mensagem foi gerada automaticamente.";
$MESS["SONET_REQUEST_GROUP_NAME"] = "Pedido de participação no grupo";
$MESS["SONET_REQUEST_GROUP_DESC"] = "#MESSAGE_ID# - ID da Mensagem
#USER_ID# - ID do usuario
#USER_NAME# - primeiro nome do usuário
#USER_LAST_NAME# - sobrenome
 do usuário
#SENDER_ID# - identificação do remetente de mensagem
#SENDER_NAME# - primero nome do remetente da mensagem
#SENDER_LAST_NAME# - Ultimo nome do remetente da mensagem
#TITLE# - Título da mensagem
#MESSAGE# - O corpo da mensagem
#EMAIL_TO# - endereço de e-mail do destinatário";
$MESS["SONET_REQUEST_GROUP_SUBJECT"] = "#TITLE#";
$MESS["SONET_REQUEST_GROUP_MESSAGE"] = "Mensagem de #SITE_NAME#
------------------------------------------

Olá #USER_NAME#!

------------------------------------------
#MESSAGE#
------------------------------------------

Essa mensagem foi gerada automaticamente.";
?>