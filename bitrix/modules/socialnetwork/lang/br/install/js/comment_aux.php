<?
$MESS["SONET_COMMENTAUX_JS_SHARE_TEXT"] = "Compartilhado com: #SHARE_LIST#";
$MESS["SONET_COMMENTAUX_JS_SHARE_TEXT_1"] = "Compartilhado com: #SHARE_LIST#";
$MESS["SONET_COMMENTAUX_JS_CREATETASK_BLOG_POST"] = "A tarefa \"#TASK_NAME#\" foi criada numa mensagem de Fluxo de Atividade.";
$MESS["SONET_COMMENTAUX_JS_CREATETASK_BLOG_COMMENT"] = "A tarefa \"#TASK_NAME#\" foi criada com base em #COMMENT_LINK#.";
$MESS["SONET_COMMENTAUX_JS_CREATETASK_BLOG_COMMENT_LINK"] = "um comentário de Fluxo de Atividade";
$MESS["SONET_COMMENTAUX_JS_FILEVERSION_TEXT"] = "carregada uma nova versão do arquivo";
$MESS["SONET_COMMENTAUX_JS_FILEVERSION_TEXT_M"] = "carregada uma nova versão do arquivo";
$MESS["SONET_COMMENTAUX_JS_FILEVERSION_TEXT_F"] = "carregada uma nova versão do arquivo";
$MESS["SONET_COMMENTAUX_JS_HEAD_FILEVERSION_TEXT"] = "editou o arquivo";
$MESS["SONET_COMMENTAUX_JS_HEAD_FILEVERSION_TEXT_M"] = "editou o arquivo";
$MESS["SONET_COMMENTAUX_JS_HEAD_FILEVERSION_TEXT_F"] = "editou o arquivo";
?>