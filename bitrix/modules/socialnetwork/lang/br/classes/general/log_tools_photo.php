<?
$MESS["SONET_PHOTO_LOG_GUEST"] = "Convidado";
$MESS["SONET_PHOTOPHOTO_LOG_1"] = "#AUTHOR_NAME# adicionou uma nova foto \"#TITLE#\".";
$MESS["SONET_PHOTO_ADD_COMMENT_SOURCE_ERROR"] = "Falha ao adicionar um comentário a uma fonte de evento.";
$MESS["SONET_PHOTO_IM_COMMENT"] = "deixou um comentário em sua foto \"#photo_title#\" no álbum \"#album_title#\"";
$MESS["SONET_PHOTO_LOG_1"] = "#AUTHOR_NAME# adicionou um arquivo #TITLE#";
$MESS["SONET_PHOTO_LOG_2"] = "Fotos (#COUNT#)";
$MESS["SONET_PHOTO_LOG_MAIL_TEXT"] = "Novas fotos: #LINKS# e outras.";
$MESS["SONET_IM_NEW_PHOTO"] = "Carregou uma nova foto para o álbum \"#title#\" em \"#group_name#\".";
$MESS["SONET_PHOTOALBUM_IM_COMMENT"] = "deixou um comentário no seu álbum \"#album_title#\"";
$MESS["SONET_PHOTO_UPDATE_COMMENT_SOURCE_ERROR"] = "Não foi possível atualizar o comentário na fonte do evento";
$MESS["SONET_PHOTO_DELETE_COMMENT_SOURCE_ERROR"] = "Não foi possível excluir o comentário na fonte do evento";
?>