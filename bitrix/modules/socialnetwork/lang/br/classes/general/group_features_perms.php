<?
$MESS["SONET_GFP_BAD_OPERATION_ID"] = "Não é possível validar a operação.";
$MESS["SONET_GF_ERROR_SET"] = "Erro ao criar um registro.";
$MESS["SONET_GF_ERROR_NO_FEATURE_ID"] = "Recurso está incorreto.";
$MESS["SONET_GFP_ERROR_NO_GROUP_FEATURE_ID"] = "Recurso está incorreto.";
$MESS["SONET_GF_EMPTY_FEATURE_ID"] = "O Recurso não foi especificado.";
$MESS["SONET_GFP_EMPTY_GROUP_FEATURE_ID"] = "O Recurso não foi especificado.";
$MESS["SONET_GFP_EMPTY_OPERATION_ID"] = "A operação não foi especificado.";
$MESS["SONET_GFP_EMPTY_ROLE"] = "O Papel não foi especificado.";
$MESS["SONET_GF_EMPTY_ENTITY_ID"] = "O ID da entidade não foi especificada.";
$MESS["SONET_GF_ERROR_NO_ENTITY_ID"] = "A entidade está incorreta.";
$MESS["SONET_GF_ERROR_NO_ENTITY_TYPE"] = "O tipo de entidade está incorreta.";
$MESS["SONET_GF_EMPTY_ENTITY_TYPE"] = "O tipo de entidade não foi especificado.";
$MESS["SONET_GFP_NO_OPERATION_ID"] = "A operação está incorreta.";
$MESS["SONET_GFP_ERROR_NO_ROLE"] = "O papel está incorreto.";
?>