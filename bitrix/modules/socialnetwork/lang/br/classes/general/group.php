<?
$MESS["SONET_WRONG_PARAMETER_ID"] = "Uma identificação incorreta foi passada para a função.";
$MESS["SONET_UR_ERROR_CREATE_U_GROUP"] = "Erro ao adicionar um usuário a um grupo.";
$MESS["SONET_UR_ERROR_CREATE_GROUP"] = "Erro na criação de um grupo.";
$MESS["SONET_UR_EMPTY_FIELDS"] = "Parâmetros do grupo não foram especificadas.";
$MESS["SONET_GB_EMPTY_OWNER_ID"] = "Proprietário não foi especificado.";
$MESS["SONET_UG_ERROR_NO_SPAM_PERMS"] = "Permissões para enviar mensagens para o grupo estão incorretos";
$MESS["SONET_UG_EMPTY_SPAM_PERMS"] = "Permissões para enviar mensagens para o grupo não estão definidas";
$MESS["SONET_NO_GROUP"] = "Registro não encontrado.";
$MESS["SONET_GB_EMPTY_DATE_CREATE"] = "A data de criação está incorreta.";
$MESS["SONET_GB_EMPTY_DATE_UPDATE"] = "A data de atualização está incorreta.";
$MESS["SONET_UR_EMPTY_OWNERID"] = "O ID do proprietário do grupo não foi especificado.";
$MESS["SONET_GP_ERROR_IMAGE_ID"] = "A imagem é inválida.";
$MESS["SONET_UG_ERROR_NO_INITIATE_PERMS"] = "A permissão do convite está incorreta.";
$MESS["SONET_UG_EMPTY_INITIATE_PERMS"] = "A permissão do convite não foi especificada.";
$MESS["SONET_GB_EMPTY_DATE_ACTIVITY"] = "A data da última visita está incorreta.";
$MESS["SONET_GB_ERROR_NO_OWNER_ID"] = "A identificação de proprietário está incorreta.";
$MESS["SONET_GG_ERROR_NO_SITE"] = "O site está incorreto.";
$MESS["SONET_GG_EMPTY_SITE_ID"] = "O site não foi especificado.";
$MESS["SONET_GB_EMPTY_NAME"] = "O título não foi especificado.";
$MESS["SONET_GB_ERROR_NO_SUBJECT_ID"] = "O tópico está incorreto.";
$MESS["SONET_GB_EMPTY_SUBJECT_ID"] = "O Tópico não foi especificado.";
$MESS["SONET_GG_ERROR_CANNOT_DELETE_USER_1"] = "O usuário possui os seguintes grupos de redes sociais: <br>";
$MESS["SONET_GG_ERROR_CANNOT_DELETE_USER_2"] = "Por favor, abra Serviços> Rede Social> Grupos e mudar o proprietário do grupo ou excluir os grupos.";
?>