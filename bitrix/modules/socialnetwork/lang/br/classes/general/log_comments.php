<?
$MESS["SONET_GLC_EMPTY_ENTITY_TYPE"] = "O tipo de entidade não foi especificado.";
$MESS["SONET_GLC_ERROR_NO_ENTITY_TYPE"] = "O tipo de entidade está incorreto.";
$MESS["SONET_GLC_EMPTY_ENTITY_ID"] = "O ID da entidade não foi especificado.";
$MESS["SONET_GLC_ERROR_CALC_ENTITY_TYPE"] = "Erro na definição do tipo de entidade.";
$MESS["SONET_GLC_ERROR_NO_ENTITY_ID"] = "O ID entidade está incorreto.";
$MESS["SONET_GLC_EMPTY_LOG_ID"] = "O ID do evento do Fluxo de Atividades está incorreto.";
$MESS["SONET_GLC_ERROR_NO_USER_ID"] = "O ID de usuário está incorreto.";
$MESS["SONET_GLC_EMPTY_EVENT_ID"] = "Evento não foi especificado.";
$MESS["SONET_GLC_ERROR_NO_FEATURE_ID"] = "O evento especificado está incorreto.";
$MESS["SONET_GLC_EMPTY_DATE_CREATE"] = "A data de criação está incorreta.";
$MESS["SONET_GLC_WRONG_PARAMETER_ID"] = "O ID está incorreto.";
$MESS["SONET_GLC_SEND_EVENT_LINK"] = "Ir para:";
$MESS["SONET_GLC_ERROR_CHECKFIELDS_FAILED"] = "Dados incorretos";
$MESS["SONET_GLC_FORUM_MENTION"] = "mencionou você no comentário para o fórum de postagem \"#title#\"";
$MESS["SONET_GLC_FORUM_MENTION_M"] = "mencionou você no comentário para o fórum de postagem \"#title#\"";
$MESS["SONET_GLC_FORUM_MENTION_F"] = "mencionou você no comentário para o fórum de postagem \"#title#\"";
?>