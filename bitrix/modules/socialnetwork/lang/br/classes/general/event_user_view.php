<?
$MESS["SONET_EUV_EMPTY_ENTITY_ID"] = "A ID da entidade não foi especificado";
$MESS["SONET_EUV_NO_ENTITY"] = "Registro não encontrado.";
$MESS["SONET_EUV_EMPTY_ENTITY_TYPE"] = "O tipo de entidade não foi especificado";
$MESS["SONET_EUV_INCORRECT_ENTITY_TYPE"] = "O tipo de entidade está incorreto";
$MESS["SONET_EUV_EMPTY_USER_ID"] = "usuário não foi especificado";
$MESS["SONET_EUV_EMPTY_ROLE"] = "O papel não foi especificado";
$MESS["SONET_EUV_ERROR_DELETE"] = "Não é possivel excluir o registro de visualização de acesso de permissão";
$MESS["SONET_EUV_ERROR_SET"] = "Não é possivel atualizar o registro de visualização de acesso de permissão";
$MESS["SONET_EUV_RECORD_EXISTS"] = "O registro já existe.";
?>