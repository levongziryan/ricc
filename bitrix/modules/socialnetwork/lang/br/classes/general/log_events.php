<?
$MESS["SONET_LE_ERROR_CALC_ENTITY_TYPE"] = "Erro na definição do tipo de entidade.";
$MESS["SONET_LE_EMPTY_EVENT_ID"] = "O Evento não foi especificado.";
$MESS["SONET_LE_ERROR_NO_ENTITY_ID"] = "O ID da entidade está incorreto.";
$MESS["SONET_LE_EMPTY_ENTITY_ID"] = "O ID da entidade não foi especificado.";
$MESS["SONET_LE_ERROR_NO_ENTITY_TYPE"] = "O tipo de entidade está incorreto.";
$MESS["SONET_LE_EMPTY_ENTITY_TYPE"] = "O tipo de entidade não foi especificado.";
$MESS["SONET_LE_WRONG_PARAMETER_ID"] = "O ID está incorreto.";
$MESS["SONET_LE_EMPTY_SITE_ID"] = "O site não foi especificado.";
$MESS["SONET_LE_ERROR_NO_SITE"] = "O site especificado está incorreto.";
$MESS["SONET_LE_ERROR_NO_FEATURE_ID"] = "O evento especificado está incorreto.";
?>