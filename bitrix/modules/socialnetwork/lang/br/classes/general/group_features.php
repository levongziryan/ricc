<?
$MESS["SONET_GF_ERROR_CALC_ENTITY_TYPE"] = "Não é possível determinar o tipo de entidade.";
$MESS["SONET_GF_ERROR_SET"] = "Não é possível salvar o registro.";
$MESS["SONET_GF_ERROR_NO_FEATURE_ID"] = "Recurso está incorreto.";
$MESS["SONET_GF_EMPTY_FEATURE_ID"] = "Recurso não foi especificado.";
$MESS["SONET_GB_EMPTY_DATE_UPDATE"] = "A data de atualização está incorreta.";
$MESS["SONET_GF_EMPTY_ENTITY_ID"] = "O ID da entidade não foi especificado.";
$MESS["SONET_GF_ERROR_NO_ENTITY_ID"] = "A entidade está incorreta.";
$MESS["SONET_GF_ERROR_NO_ENTITY_TYPE"] = "O tipo de entidade está incorreta.";
$MESS["SONET_GF_EMPTY_ENTITY_TYPE"] = "O tipo de entidade não foi especificado.";
$MESS["SONET_GB_EMPTY_DATE_CREATE"] = "A data de criação do registro está incorreta.";
?>