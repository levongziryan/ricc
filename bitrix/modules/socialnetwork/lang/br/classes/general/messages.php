<?
$MESS["SONET_M_ERROR_DELETE_MESSAGE"] = "Não é possível excluir a mensagem.";
$MESS["SONET_UR_ERROR_UPDATE_MESSAGE"] = "Não é possível atualizar a mensagem.";
$MESS["SONET_UR_ERROR_CREATE_MESSAGE"] = "Erro ao criar uma mensagem.";
$MESS["SONET_GB_EMPTY_DATE_CREATE"] = "A data de criação está incorreta.";
$MESS["SONET_UR_EMPTY_MESSAGE_ID"] = "O ID da mensagem não foi especificado.";
$MESS["SONET_UR_EMPTY_MESSAGE"] = "A mensagem não foi especificada.";
$MESS["SONET_UR_EMPTY_TARGET_USER_ID"] = "O destinatário da mensagem não foi especificado.";
$MESS["SONET_UR_EMPTY_SENDER_USER_ID"] = "O remetente da mensagem não foi especificado.";
$MESS["SONET_UR_NO_MESSAGE"] = "A mensagem não foi encontrada.";
$MESS["SONET_M_EMPTY_TO_USER_ID"] = "O destinatário não foi especificado.";
$MESS["SONET_M_ERROR_NO_TO_USER_ID"] = "O destinatário especificado está incorreto.";
$MESS["SONET_M_EMPTY_FROM_USER_ID"] = "O remetente não foi especificado.";
$MESS["SONET_M_ERROR_NO_FROM_USER_ID"] = "O remetente especificado está incorreto.";
$MESS["SONET_MM_EMPTY_DATE_VIEW"] = "A data de visualização está incorreta.";
?>