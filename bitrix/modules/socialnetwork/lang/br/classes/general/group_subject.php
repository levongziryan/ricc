<?
$MESS["SONET_GS_ERROR_NO_SITE"] = "O tópico do site está incorreto.";
$MESS["SONET_GS_EMPTY_SITE_ID"] = "O tópico do site não foi especificado.";
$MESS["SONET_GS_EMPTY_NAME"] = "O título do tópico não foi especificado.";
$MESS["SONET_GS_NOT_EMPTY_SUBJECT"] = "Este tópico não está vazio.";
?>