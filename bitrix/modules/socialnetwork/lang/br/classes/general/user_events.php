<?
$MESS["SONET_UE_EMPTY_EVENT_ID"] = "O evento não foi especificado.";
$MESS["SONET_UE_ERROR_NO_EVENT_ID"] = "O evento especificado está incorreto.";
$MESS["SONET_UE_ERROR_NO_SITE"] = "O site está incorreto.";
$MESS["SONET_UE_EMPTY_SITE_ID"] = "O site não foi especificado.";
$MESS["SONET_GB_EMPTY_USER_ID"] = "O Usuario não foi especificado.";
$MESS["SONET_GB_ERROR_NO_USER_ID"] = "O Usuario especificado está incorreto.";
?>