<?
$MESS["SONET_LF_UNFOLLOW_IM_MESSAGE"] = "Fazendo carregamento de mensagens no Fluxo de Atividade? Ativar o modo de acompanhamento inteligente para visualizar apenas os eventos que são importantes para você. Apenas as mensagens que você é o autor, dirigidas a você ou que mencionam você serão movidas para o topo. Sempre que você adicionar um comentário a uma mensagem, você começa a acompanhá-la.";
$MESS["SONET_LF_UNFOLLOW_IM_BUTTON_Y"] = "Modo de acompanhamento inteligente";
$MESS["SONET_LF_UNFOLLOW_IM_BUTTON_N"] = "Não, obrigado";
?>