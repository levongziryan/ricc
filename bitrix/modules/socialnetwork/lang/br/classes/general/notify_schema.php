<?
$MESS["SONET_NS_INVITE_USER"] = "Notificação de Amizade";
$MESS["SONET_NS_INVITE_GROUP"] = "Notificação de convite de grupo";
$MESS["SONET_NS_INOUT_GROUP"] = "Alterações na adesão do grupo";
$MESS["SONET_NS_MODERATORS_GROUP"] = "Atribuído ou não atribuído como um moderador de grupo de trabalho";
$MESS["SONET_NS_OWNER_GROUP"] = "Mudança de proprietário do Grupo de Trabalho";
$MESS["SONET_NS_FRIEND"] = "Listado ou não listado como amigo";
$MESS["SONET_NS_SONET_GROUP_EVENT"] = "Atualizações de grupo de trabalho";
?>