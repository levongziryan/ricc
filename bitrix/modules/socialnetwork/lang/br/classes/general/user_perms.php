<?
$MESS["SONET_GB_EMPTY_USER_ID"] = "Usuario não foi especificado.";
$MESS["SONET_GB_ERROR_NO_USER_ID"] = "Usuario especificado está incorreto.";
$MESS["SONET_GG_EMPTY_OPERATION_ID"] = "A operação não foi especificada.";
$MESS["SONET_GG_ERROR_NO_OPERATION_ID"] = "A operação especificada está incorreta.";
$MESS["SONET_GG_EMPTY_RELATION_TYPE"] = "O tipo de relacionamento não foi especificado.";
$MESS["SONET_GG_ERROR_NO_RELATION_TYPE"] = "O tipo especificado de relacionamento está incorreto.";
?>