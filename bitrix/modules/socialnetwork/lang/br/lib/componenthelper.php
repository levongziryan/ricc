<?
$MESS["BLG_NAME"] = "Iniciado por";
$MESS["BLG_SHARE_ALL"] = "Todos os empregados";
$MESS["BLG_SHARE_ALL_BUS"] = "Todos os usuários";
$MESS["BLG_SHARE"] = "Compartilhado com: ";
$MESS["BLG_SHARE_1"] = "Compartilhado com: ";
$MESS["BLG_SHARE_HIDDEN_1"] = "Destinatário oculto";
$MESS["SONET_HELPER_CREATED_BY_ANONYMOUS"] = "Visitante não autorizado";
$MESS["BLG_TASK_CREATED_POST"] = "A tarefa \"#TASK_NAME#\" foi criada numa mensagem de Fluxo de Atividade.";
$MESS["BLG_TASK_CREATED_COMMENT"] = "A tarefa \"#TASK_NAME#\" foi criada com base em #LINK#.";
$MESS["BLG_TASK_CREATED_COMMENT_LINK"] = "um comentário do Fluxo de Atividade";
$MESS["SONET_HELPER_STEPPER_LIVEFEED"] = "Reindexação do Fluxo de Atividade";
?>