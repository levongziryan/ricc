<?
$MESS["SONET_COMMENTAUX_CREATETASK_BLOG_POST"] = "A tarefa \"#TASK_NAME#\" foi criada numa mensagem de Fluxo de Atividade.";
$MESS["SONET_COMMENTAUX_CREATETASK_BLOG_COMMENT"] = "A tarefa \"#TASK_NAME#\" foi criada com base em #COMMENT_LINK#.";
$MESS["SONET_COMMENTAUX_CREATETASK_BLOG_COMMENT_LINK"] = "um comentário de Fluxo de Atividade";
$MESS["SONET_COMMENTAUX_CREATETASK_NOT_FOUND"] = "&lt;a tarefa não foi encontrada.&gt;";
?>