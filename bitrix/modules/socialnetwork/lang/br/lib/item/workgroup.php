<?
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_OPEN"] = "Projeto Aberto";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_OPEN_DESC"] = "O projeto é visível para todos. Qualquer pessoa pode se tornar um membro do projeto.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_CLOSED"] = "Projeto Privado";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_CLOSED_DESC"] = "O projeto é visível apenas para membros do projeto. É necessário convite para se tornar um membro do projeto.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_EXTERNAL"] = "Projeto Externo";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_EXTERNAL_DESC"] = "O projeto é visível apenas para membros do projeto. Usuários externos podem ser convidados para o projeto.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_OPEN"] = "Grupo de trabalho Aberto";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_OPEN_DESC"] = "O grupo de trabalho é visível para todos. Qualquer pessoa pode se tornar um membro do grupo de trabalho.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_CLOSED"] = "Grupo de Trabalho Privado";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_CLOSED_DESC"] = "O grupo de trabalho é visível apenas para membros do grupo de trabalho. É necessário convite para se tornar um membro do grupo de trabalho.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_EXTERNAL"] = "Grupo de Trabalho Externo";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_EXTERNAL_DESC"] = "O grupo de trabalho é visível apenas para membros do grupo de trabalho. Usuários externos podem ser convidados para o grupo de trabalho.";
?>