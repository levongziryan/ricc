<?
$MESS["SONETE_DELETE_SUBJECT_CONFIRM"] = "Tem certeza de que quer apagar este tópico? O tópico não será apagado se ele conter grupos.";
$MESS["SONETE_ADDING"] = "Criar um novo tópico";
$MESS["SONETE_DELETE_SUBJECT"] = "excluir tópico";
$MESS["SONETE_UPDATING"] = "Editar os parâmetros de tópico";
$MESS["SONETE_ERROR_SAVING"] = "Erro ao salvar o tópico.";
$MESS["SONETE_TAB_SUBJECT"] = "Tópico do grupo";
$MESS["SONETE_TAB_SUBJECT_DESCR"] = "Parâmetros do tópico do grupo";
$MESS["SONETE_NO_PERMS2ADD"] = "Permissão insuficiente para criar um novo tópico.";
$MESS["SONETE_NEW_SUBJECT"] = "Novo tópico";
$MESS["SONETE_SITE"] = "Tópico do site";
$MESS["SONETE_SORT"] = "Classificação de tópico";
$MESS["SONETE_NAME"] = "Título do tópico";
$MESS["SONETE_2FLIST"] = "Tópicos";
?>