<?
$MESS["BLOG_VIDEO_RECORD_BUTTON"] = "Grabar un vídeo";
$MESS["BLOG_VIDEO_RECORD_CANCEL_BUTTON"] = "Cancelar";
$MESS["BLOG_VIDEO_RECORD_LOGO"] = "<span class=\"logo\"><span class=\"logo-text\">Bitrix</span><span class=\"logo-color\">24</span></span>";
$MESS["BLOG_VIDEO_RECORD_STOP_BUTTON"] = "Pausar";
$MESS["BLOG_VIDEO_RECORD_USE_BUTTON"] = "Utilizar Vídeo";
$MESS["BLOG_VIDEO_RECORD_IN_PROGRESS_LABEL"] = "Grabación en Progreso";
$MESS["BLOG_VIDEO_RECORD_AGREE"] = "Permitir";
$MESS["BLOG_VIDEO_RECORD_CLOSE"] = "Cerrar";
$MESS["BLOG_VIDEO_RECORD_ASK_PERMISSIONS"] = "Debe permitir el acceso a su cámara y micrófono para grabar un video.";
$MESS["BLOG_VIDEO_RECORD_REQUIREMENTS"] = "Lamentablemente, su navegador no es compatible con la grabación de video. <br/>Puede probar con otro navegador como la última versión de Firefox o Chrome.";
$MESS["BLOG_VIDEO_RECORD_REQUIREMENTS_TITLE"] = "Atención";
$MESS["BLOG_VIDEO_RECORD_PERMISSIONS_ERROR"] = "No se puede acceder a su cámara y micrófono.";
$MESS["BLOG_VIDEO_RECORD_PERMISSIONS_TITLE"] = "Acceso a Dispositivos";
$MESS["BLOG_VIDEO_RECORD_PERMISSIONS_ERROR_TITLE"] = "Error";
$MESS["BLOG_VIDEO_RECORD_SPOTLIGHT_MESSAGE"] = "<b>Record videos and share with your team.</b>";
$MESS["BLOG_VIDEO_RECORD_TRANFORM_LIMIT_TEXT"] = "Para que el video se pueda ver en Bitrix24, deje de grabar dentro de <span class=\"bx-videomessage-transform-time-tip\">60 segundos</span>.Los archivos de video más largos seguramente se guardarán, pero es posible que no se reproduzcan en todos los navegadores.";
$MESS["BLOG_VIDEO_RECORD_RESTART_BUTTON"] = "Grabar nuevamente";
$MESS["BLOG_VIDEO_RECORD_ERROR_CHROME_HTTPS"] = "Lamentablemente, su navegador no es compatible con el protocolo HTTP.<br /><br />Pruebe con otro navegador, por ejemplo, Firefox.";
?>