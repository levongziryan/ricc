<?
$MESS["SONET_COMMENTAUX_JS_SHARE_TEXT"] = "Compartido con: #SHARE_LIST#";
$MESS["SONET_COMMENTAUX_JS_SHARE_TEXT_1"] = "Compartido con: #SHARE_LIST#";
$MESS["SONET_COMMENTAUX_JS_CREATETASK_BLOG_POST"] = "La tarea \"#TASK_NAME#\" se ha creado un mensaje en el Flujo de Actividad.";
$MESS["SONET_COMMENTAUX_JS_CREATETASK_BLOG_COMMENT"] = "La tarea \"#TASK_NAME#\" se ha creado en base a #COMMENT_LINK#.";
$MESS["SONET_COMMENTAUX_JS_CREATETASK_BLOG_COMMENT_LINK"] = "un comentario en el Flujo de Actividad";
$MESS["SONET_COMMENTAUX_JS_FILEVERSION_TEXT"] = "cargó una nueva versión del archivo";
$MESS["SONET_COMMENTAUX_JS_FILEVERSION_TEXT_M"] = "cargó una nueva versión del archivo";
$MESS["SONET_COMMENTAUX_JS_FILEVERSION_TEXT_F"] = "cargó una nueva versión del archivo";
$MESS["SONET_COMMENTAUX_JS_HEAD_FILEVERSION_TEXT"] = "editar el archivo";
$MESS["SONET_COMMENTAUX_JS_HEAD_FILEVERSION_TEXT_M"] = "editar el archivo";
$MESS["SONET_COMMENTAUX_JS_HEAD_FILEVERSION_TEXT_F"] = "editar el archivo";
?>