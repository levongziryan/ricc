<?
$MESS["SONET_INSTALL_NAME"] = "Red Social";
$MESS["SONET_INSTALL_DESCRIPTION"] = "Agrega la característica de red social a su sitio.";
$MESS["SONET_INSTALL_TITLE"] = "Instalar Redes Sociales";
$MESS["SONETP_PERM_D"] = "usted puede ver la sección pública, no puede crear grupos";
$MESS["SONETP_PERM_K"] = "usted puede ver la sección pública, puede crear grupos";
$MESS["SONETP_PERM_R"] = "Acceso de lectura para controlar el panel";
$MESS["SONETP_PERM_W"] = "Acceso total";
$MESS["SONETP_COPY_PUBLIC_FILES"] = "Instalar sección pública";
$MESS["SONETP_COPY_FOLDER"] = "Carpeta del destino (relativo a la raíz del sitio)";
$MESS["SONET_INSTALL_PUBLIC_REW"] = "Sobre escribir los archivos existentes";
$MESS["SONETP_REWRITE_ADD"] = "Sobrescribir los archivos de sistema";
$MESS["SONETP_INSTALL_EMAIL"] = "Crear plantillas del correo";
$MESS["SONETP_DELETE_EMAIL"] = "Borrar plantillas del correo";
$MESS["SONETP_INSTALL_404"] = "Instalar sección pública en el modo SEF";
$MESS["SONETP_RW_DEF_IMAGES"] = "Sustituir las imágenes en la plantilla &quot;.default&quot;";
$MESS["SONETP_INSTALL_SMILES"] = "Instalar Smileys";
$MESS["SONETP_NOT_INSTALL_P"] = "no instalar";
$MESS["SONETP_EDIT_FORM_LABEL"] = "Anuncio";
$MESS["SONETP_LIST_COLUMN_LABEL"] = "Anuncio";
$MESS["SONETP_LIST_FILTER_LABEL"] = "Anuncio";
$MESS["SONET_UF_SG_DEPT_EDIT_FORM_LABEL"] = "Departamentos";
$MESS["SONET_UF_SG_DEPT_LIST_COLUMN_LABEL"] = "Departamentos";
$MESS["SONET_UF_SG_DEPT_LIST_FILTER_LABEL"] = "Departamentos";
?>