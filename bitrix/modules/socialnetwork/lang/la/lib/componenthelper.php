<?
$MESS["BLG_NAME"] = "Iniciado por";
$MESS["BLG_SHARE_ALL"] = "Todos los empleados";
$MESS["BLG_SHARE_ALL_BUS"] = "Todos los usuarios";
$MESS["BLG_SHARE"] = "Compartido con:";
$MESS["BLG_SHARE_1"] = "Compartido con:";
$MESS["BLG_SHARE_HIDDEN_1"] = "Ocultar destinatario";
$MESS["SONET_HELPER_CREATED_BY_ANONYMOUS"] = "Visitante no autorizado";
$MESS["BLG_TASK_CREATED_POST"] = "La tarea \"#TASK_NAME#\" ha creado en un mensaje en el Flujo de Actividad.";
$MESS["BLG_TASK_CREATED_COMMENT"] = "La tarea \"#TASK_NAME#\" se ha creado en base a #LINK#.";
$MESS["BLG_TASK_CREATED_COMMENT_LINK"] = "un comentario del Flujo de Actividad";
$MESS["SONET_HELPER_STEPPER_LIVEFEED"] = "Re-indexación del Flujo de actividades";
?>