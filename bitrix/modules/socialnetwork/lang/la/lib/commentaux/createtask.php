<?
$MESS["SONET_COMMENTAUX_CREATETASK_BLOG_POST"] = "La tarea \"#TASK_NAME#\" se ha creado un mensaje en el Flujo de Actividad.";
$MESS["SONET_COMMENTAUX_CREATETASK_BLOG_COMMENT"] = "La tarea \"#TASK_NAME#\" se ha creado sobre la base de #COMMENT_LINK#.";
$MESS["SONET_COMMENTAUX_CREATETASK_BLOG_COMMENT_LINK"] = "un comentario en el Flujo de Actividad";
$MESS["SONET_COMMENTAUX_CREATETASK_NOT_FOUND"] = "&lt;no se ha encontró la tarea.&gt;";
?>