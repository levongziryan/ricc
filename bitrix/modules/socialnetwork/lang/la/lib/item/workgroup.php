<?
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_OPEN"] = "Proyecto abierto";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_OPEN_DESC"] = "El proyecto es visible para todos. Cualquiera puede convertirse en un miembro del proyecto.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_CLOSED"] = "Proyecto Privado
";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_CLOSED_DESC"] = "El proyecto es visible solo para los miembros del proyecto. Se requiere invitación para convertirse en un miembro del proyecto.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_EXTERNAL"] = "Proyecto externo";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_PROJECT_EXTERNAL_DESC"] = "El proyecto es visible solo para los miembros del proyecto. Los usuarios externos pueden ser invitados al proyecto.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_OPEN"] = "Grupo de trabajo abierto";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_OPEN_DESC"] = "El grupo de trabajo es visible para todos. Cualquiera puede convertirse en un miembro del grupo de trabajo.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_CLOSED"] = "Grupo de trabajo privado";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_CLOSED_DESC"] = "El grupo de trabajo solo es visible para los miembros del grupo de trabajo. Se requiere invitación para convertirse en miembro del grupo de trabajo.";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_EXTERNAL"] = "Grupo de trabajo externo";
$MESS["SOCIALNETWORK_ITEM_WORKGROUP_TYPE_GROUP_EXTERNAL_DESC"] = "El grupo de trabajo solo es visible para los miembros del grupo de trabajo. Los usuarios externos pueden ser invitados al grupo de trabajo.";
?>