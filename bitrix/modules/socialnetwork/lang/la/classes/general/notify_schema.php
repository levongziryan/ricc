<?
$MESS["SONET_NS_INVITE_USER"] = "Notificación de amistad";
$MESS["SONET_NS_INVITE_GROUP"] = "Invitaciones de grupos de trabajo y solicitudes de membresía; prohibiciones";
$MESS["SONET_NS_INOUT_GROUP"] = "Cambio de membresía de grupo (para los moderadores)";
$MESS["SONET_NS_MODERATORS_GROUP"] = "Usted ha sido asignado o no asignado como moderador del grupo de trabajo";
$MESS["SONET_NS_OWNER_GROUP"] = "Cambio de propietario de grupo de trabajo";
$MESS["SONET_NS_FRIEND"] = "Estar o no estar en la lista de amigo";
$MESS["SONET_NS_SONET_GROUP_EVENT"] = "Actualizar grupo de trabajo";
?>