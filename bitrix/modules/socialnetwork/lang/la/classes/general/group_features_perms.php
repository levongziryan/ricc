<?
$MESS["SONET_GFP_EMPTY_GROUP_FEATURE_ID"] = "Función no está especificada.";
$MESS["SONET_GFP_ERROR_NO_GROUP_FEATURE_ID"] = "Función es incorrecta.";
$MESS["SONET_GFP_EMPTY_OPERATION_ID"] = "Operación no está especificada.";
$MESS["SONET_GFP_BAD_OPERATION_ID"] = "No puede validar la operación.";
$MESS["SONET_GFP_NO_OPERATION_ID"] = "La operación es incorrecta.";
$MESS["SONET_GFP_EMPTY_ROLE"] = "Rol no está especificado.";
$MESS["SONET_GFP_ERROR_NO_ROLE"] = "El rol es incorrecto.";
$MESS["SONET_GF_ERROR_SET"] = "Error al crear un registro.";
$MESS["SONET_GF_EMPTY_ENTITY_ID"] = "El ID de la entidad no está especificado.";
$MESS["SONET_GF_ERROR_NO_ENTITY_ID"] = "La entidad es incorrecta.";
$MESS["SONET_GF_EMPTY_FEATURE_ID"] = "La función no está especificada.";
$MESS["SONET_GF_ERROR_NO_FEATURE_ID"] = "La función es incorrecta.";
$MESS["SONET_GF_EMPTY_ENTITY_TYPE"] = "El tipo de la entidad no está especificada.";
$MESS["SONET_GF_ERROR_NO_ENTITY_TYPE"] = "El tipo de entidad es incorrecto.";
?>