<?
$MESS["SONET_GF_EMPTY_ENTITY_ID"] = "La entidad no está especificada.";
$MESS["SONET_GF_ERROR_NO_ENTITY_ID"] = "La entidad es incorrecta.";
$MESS["SONET_GF_EMPTY_FEATURE_ID"] = "La función no está especificada.";
$MESS["SONET_GF_ERROR_NO_FEATURE_ID"] = "La función es incorrecta.";
$MESS["SONET_GB_EMPTY_DATE_UPDATE"] = "La fecha de la actualización es incorrecta.";
$MESS["SONET_GB_EMPTY_DATE_CREATE"] = "La fecha de creación del registro es incorrecta.";
$MESS["SONET_GF_EMPTY_ENTITY_TYPE"] = "El tipo de entidad no está especificada.";
$MESS["SONET_GF_ERROR_NO_ENTITY_TYPE"] = "El tipo de entidad es incorrecta.";
$MESS["SONET_GF_ERROR_CALC_ENTITY_TYPE"] = "No puede determinar el tipo de la entidad.";
$MESS["SONET_GF_ERROR_SET"] = "No puede guardar el registro.";
?>