<?
$MESS["SONET_GB_EMPTY_USER_ID"] = "Usuario no está especificado.";
$MESS["SONET_GB_ERROR_NO_USER_ID"] = "Usuario especificado es incorrecto.";
$MESS["SONET_GG_EMPTY_OPERATION_ID"] = "La operación no está especificada.";
$MESS["SONET_GG_ERROR_NO_OPERATION_ID"] = "La operación especificada es incorrecta.";
$MESS["SONET_GG_EMPTY_RELATION_TYPE"] = "El tipo de relación de amistad no está especificado.";
$MESS["SONET_GG_ERROR_NO_RELATION_TYPE"] = "El tipo de relación especificado es incorrecto.";
?>