<?
$MESS["SNBPA_EMPTY_POST_MESSAGE"] = "El texto del mensaje está vacío.";
$MESS["SNBPA_EMPTY_OWNER"] = "No se especifica el autor del mensaje.";
$MESS["SNBPA_EMPTY_USERS"] = "Los destinatarios del mensaje no están especificados.";
$MESS["SNBPA_BLOG_NAME"] = "Blog";
$MESS["SNBPA_OWNER_ID"] = "Remitente";
$MESS["SNBPA_USERS_TO"] = "Destinatarios";
$MESS["SNBPA_POST_TITLE"] = "Título del mensaje";
$MESS["SNBPA_POST_MESSAGE"] = "Texto del mensaje";
$MESS["SNBPA_POST_SITE"] = "Sitio de destino";
?>