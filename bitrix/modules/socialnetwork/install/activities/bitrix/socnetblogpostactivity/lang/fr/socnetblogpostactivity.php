<?
$MESS["SNBPA_EMPTY_POST_MESSAGE"] = "Le texte du message est vide.";
$MESS["SNBPA_EMPTY_OWNER"] = "L'auteur du message n'est pas indiqué.";
$MESS["SNBPA_EMPTY_USERS"] = "Les destinataires du message ne sont pas indiqués.";
$MESS["SNBPA_BLOG_NAME"] = "Blog";
$MESS["SNBPA_OWNER_ID"] = "Expéditeur";
$MESS["SNBPA_USERS_TO"] = "Destinataires";
$MESS["SNBPA_POST_TITLE"] = "Titre du message";
$MESS["SNBPA_POST_MESSAGE"] = "Texte du message";
$MESS["SNBPA_POST_SITE"] = "Site cible";
?>