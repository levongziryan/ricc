<?
$MESS["SNBPA_EMPTY_POST_MESSAGE"] = "O texto da mensagem está vazio.";
$MESS["SNBPA_EMPTY_OWNER"] = "O autor da mensagem não está especificado.";
$MESS["SNBPA_EMPTY_USERS"] = "Os destinatários da mensagem não estão especificados.";
$MESS["SNBPA_BLOG_NAME"] = "Blog";
$MESS["SNBPA_OWNER_ID"] = "Remetente";
$MESS["SNBPA_USERS_TO"] = "Destinatários";
$MESS["SNBPA_POST_TITLE"] = "Título da mensagem";
$MESS["SNBPA_POST_MESSAGE"] = "Texto da mensagem";
$MESS["SNBPA_POST_SITE"] = "Site de destino";
?>