<?
$MESS["SNBPA_PD_OWNER_ID"] = "Expéditeur";
$MESS["SNBPA_PD_USERS_TO"] = "Destinataires";
$MESS["SNBPA_PD_POST_TITLE"] = "Titre du message";
$MESS["SNBPA_PD_POST_MESSAGE"] = "Texte du message";
$MESS["SNBPA_PD_MESS_TYPE"] = "Type de message";
$MESS["SNBPA_PD_MESS_TYPE_TEXT"] = "Texte";
$MESS["SNBPA_PD_POST_SITE"] = "Site cible";
$MESS["SNBPA_PD_POST_SITE_OTHER"] = "autre";
?>