<?
$MESS["BPE_ID"] = "Identificateur du message";
$MESS["B_VARIABLE_ALIASES"] = "Alias des variables";
$MESS["BPE_USER_VAR"] = "Nom de la variable pour le code d'utilisateur";
$MESS["BPE_POST_VAR"] = "Nom de la variable pour l'identificateur du message";
$MESS["BPE_PAGE_VAR"] = "Nom de la variable pour la page";
$MESS["BPC_ALLOW_POST_CODE"] = "Utiliser le code de symbole des messages comme identificateur";
$MESS["BPC_IMAGE_MAX_HEIGHT"] = "Hauteur maximale de l'image";
$MESS["BPC_IMAGE_MAX_WIDTH"] = "Largeur maximale de l'image";
$MESS["POST_PROPERTY"] = "Afficher les propriétés supplémentaires du message";
$MESS["BB_PATH_TO_SMILE"] = "Chemin vers le dossier avec les smileys par rapport à la racine du site";
$MESS["BC_DATE_TIME_FORMAT"] = "Format d'affichage de la date et de l'heure";
$MESS["BH_PATH_TO_DRAFT"] = "Modèle de chemin d'accès aux messages inachevés";
$MESS["BPE_PATH_TO_USER"] = "Modèle de chemin d'accès  au profil de l'utilisateur";
$MESS["BPE_PATH_TO_POST_EDIT"] = "Modèle de chemin d'accès à la page d'édition d'un message du blog";
$MESS["BPE_PATH_TO_BLOG"] = "Modèle de chemin d'accès à la page des messages";
$MESS["BPE_PATH_TO_POST"] = "Modèle de chemin d'accès à la page du message";
?>