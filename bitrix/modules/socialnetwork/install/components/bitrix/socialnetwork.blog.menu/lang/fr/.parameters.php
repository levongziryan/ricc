<?
$MESS["BH_SET_NAV_CHAIN"] = "Ajouter un élément dans le fil d'Ariane";
$MESS["B_VARIABLE_ALIASES"] = "Alias des variables";
$MESS["BH_USER_VAR"] = "Nom de la variable pour le code d'utilisateur";
$MESS["BH_POST_VAR"] = "Nom de la variable pour l'identificateur du message";
$MESS["BH_PAGE_VAR"] = "Nom de la variable pour la page";
$MESS["BH_PATH_TO_BLOG"] = "Modèle de chemin d'accès à la page des messages";
$MESS["BH_PATH_TO_DRAFT"] = "Modèle de chemin d'accès aux messages inachevés";
$MESS["BH_PATH_TO_USER"] = "Modèle de chemin d'accès  au profil de l'utilisateur";
$MESS["BH_PATH_TO_POST_EDIT"] = "Modèle de chemin d'accès à la page d'édition d'un message";
$MESS["BH_BLOG_VAR"] = "Variable d'identificateur du blog";
$MESS["BH_BLOG_URL"] = "URL du blog";
$MESS["BH_PATH_TO_USER_SETTINGS"] = "Modèle du chemin d'accès à la page de personnalisation des permissions d'accès";
$MESS["BH_PATH_TO_BLOG_EDIT"] = "Modèle du chemin d'accès à la page des paramètres du blog";
$MESS["BH_PATH_TO_USER_FRIENDS"] = "Modèle du chemin d'accès à la page d'amis";
$MESS["BH_PATH_TO_BLOG_INDEX"] = "Modèle du chemin d'accès à la page d'accueil";
$MESS["BH_PATH_TO_CATEGORY_EDIT"] = "Modèle du chemin d'accès à la page de personnalisation des balises";
$MESS["BH_PATH_TO_GROUP_EDIT"] = "Modèle du chemin d'accès à la page de personnalisation du groupe d'utilisateurs";
?>