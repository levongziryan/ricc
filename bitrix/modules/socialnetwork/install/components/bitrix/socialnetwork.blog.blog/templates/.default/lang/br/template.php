<?
$MESS["BLOG_BLOG_BLOG_NO_AVAIBLE_MES"] = "Nenhuma conversação ainda";
$MESS["BLOG_MES_DELETE_POST_CONFIRM"] = "Tem certeza de que deseja excluir esta postagem?";
$MESS["BLOG_BLOG_BLOG_COMMENTS"] = "Comentários:";
$MESS["BLOG_MES_DELETE"] = "Excluir";
$MESS["BLOG_MES_EDIT"] = "Editar";
$MESS["BLOG_BLOG_BLOG_MORE"] = "Leia mais...";
$MESS["BLOG_BLOG_BLOG_CATEGORY"] = "Marcadores:";
$MESS["BLOG_BLOG_BLOG_VIEWS"] = "Visualizações:";
?>