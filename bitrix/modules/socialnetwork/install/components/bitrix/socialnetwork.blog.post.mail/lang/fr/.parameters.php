<?
$MESS["SBPM_POST_ID"] = "ID de message";
$MESS["SBPM_URL"] = "URL du message";
$MESS["SBPM_EMAIL_TO"] = "Envoyer une notification à l'adresse e-mail";
$MESS["SBPM_AVATAR_SIZE_COMMENT"] = "Taille de l'avatar de l'auteur (px)";
$MESS["SBPM_RECIPIENT_ID"] = "ID du destinataire";
?>