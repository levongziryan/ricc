<?
$MESS["SONET_C4_FUNC_TITLE"] = "O recurso '#NAME#' está ativado.";
$MESS["SONET_C4_FUNC_TITLE_ON"] = "O recurso '#NAME#' está ativado.";
$MESS["SONET_C4_FUNC_TITLE_OFF"] = "O recurso '#NAME#' está desativado.";
$MESS["SONET_C4_SUBMIT"] = "Atualizar Parâmetros";
$MESS["SONET_C4_GR_SUCCESS"] = "Os parâmetros do grupo foram alterados com sucesso.";
$MESS["SONET_C4_US_SUCCESS"] = "Os parâmetros do usuário foram alterados com sucesso.";
$MESS["SONET_C4_T_CANCEL"] = "Cancelar";
$MESS["SONET_FEATURES_NAME"] = "Nome";
$MESS["SONET_C4_NO_FEATURES"] = "Este recurso está desativado. Você pode ativá-lo na página de parâmetros de grupo de trabalho. ";
?>