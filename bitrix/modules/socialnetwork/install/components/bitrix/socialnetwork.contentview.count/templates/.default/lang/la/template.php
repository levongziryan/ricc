<?
$MESS["SCVC_TEMPLATE_LICENSE_TITLE"] = "Ampliación del Flujo de Actividad";
$MESS["SCVC_TEMPLATE_LICENSE_TEXT"] = "El contador de vista del post del Flujo de Actividad está disponible en el plan Plus y superior. <br><br>Para ver la cantidad de vistas y lectores de una publicación en particular, desplace el puntero del mouse sobre el icono.";
$MESS["SCVC_TEMPLATE_POPUP_TITLE"] = "Vistas";
?>