<?
$MESS["WD_HOW_TO_INCREASE_QUOTA"] = "<a href='#HREF##maxfilesize'>Comment augmenter le quota?</a>";
$MESS["SONET_GROUP_NOT_EXISTS"] = "Groupe inexistant.";
$MESS["SONET_GROUP_PREFIX"] = "Groupe:";
$MESS["SONET_FILES"] = "Fichiers";
$MESS["SONET_WEBDAV_NOT_EXISTS"] = "Pas de documents.";
$MESS["SONET_WD_MODULE_IS_NOT_INSTALLED"] = "Module Bibliothèque de documents non installé.";
$MESS["SONET_IB_MODULE_IS_NOT_INSTALLED"] = "Module des blocs d'information non installé..";
$MESS["SONET_IBLOCK_ID_EMPTY"] = "Bloc d'information non spécifié.";
$MESS["SONET_FILES_IS_NOT_ACTIVE"] = "La fonction 'Mon disque' est désactivée";
$MESS["SONET_GROUP_FILES_ACCESS_DENIED"] = "L'accès aux fichiers de groupe de travail est refusé.";
$MESS["SONET_USER_FILES_ACCESS_DENIED"] = "L'accès aux fichiers de l'utilisateur est refusé.";
?>