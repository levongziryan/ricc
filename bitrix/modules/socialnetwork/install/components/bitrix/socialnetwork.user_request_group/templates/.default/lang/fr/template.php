<?
$MESS["SONET_C39_T_PROMT"] = "La demande sera envoyée au groupe pour vous inclure dans le nombre de ses membres. Après l'acceptation de la direction du groupe de vous inclure aux membres du groupe, ce dernier apparaîtra sur la liste.";
$MESS["SONET_C39_T_GROUP"] = "Groupe";
$MESS["SONET_C39_T_MESSAGE"] = "Message";
$MESS["SONET_C39_T_SEND"] = "Emettre le message";
$MESS["SONET_C39_T_SUCCESS"] = "Votre demande a été envoyée. Après que l'administration du groupe vous accepte comme membre du groupe, ce groupe sera affiché dans une liste de vos groupes.";
$MESS["SONET_C39_T_SUCCESS_ALT"] = "Vous avez rejoint le groupe, et maintenant vous êtes le membre. Afin de vous retirer du groupe, utilisez le lien sur le profil principal du groupe.";
$MESS["SONET_C39_T_SENDER"] = "Expéditeur";
$MESS["SONET_C39_T_ACTIONS"] = "Actions";
$MESS["SONET_C39_T_FRIEND_REQUEST"] = "L'utilisateur vous invite dans sa liste d'amis.";
$MESS["SONET_C39_T_USER"] = "Utilisateur";
$MESS["SONET_C39_T_INVITE"] = "vous êtes invité à entrer dans le groupe";
$MESS["SONET_C39_T_DO_FRIEND"] = "Accepter";
$MESS["SONET_C39_T_DO_DENY"] = "Refuser";
$MESS["SONET_C39_T_DO_AGREE"] = "Accepter";
$MESS["SONET_C39_T_REJECTED"] = "Vous avez refusé de joindre le groupe.";
?>