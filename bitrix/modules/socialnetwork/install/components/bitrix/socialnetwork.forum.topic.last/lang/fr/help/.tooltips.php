<?
$MESS["FID_TIP"] = "Forum ID";
$MESS["CACHE_TIME_TIP"] = "Temps de mise en cache (s).";
$MESS["PAGER_SHOW_ALWAYS_TIP"] = "Afficher toujours";
$MESS["DISPLAY_PANEL_TIP"] = "Ajouter les Boutons pour ce Composant sur le Panneau d'Administration";
$MESS["PAGER_DESC_NUMBERING_TIP"] = "Utiliser la navigation inverse";
$MESS["TOPICS_PER_PAGE_TIP"] = "Nombre de thèmes sur une page";
$MESS["PAGER_TITLE_TIP"] = "Nom des catégories";
$MESS["PAGER_TEMPLATE_TIP"] = "Dénomination du modèle";
$MESS["SORT_ORDER_TIP"] = "Ordre de triage";
$MESS["SET_NAVIGATION_TIP"] = "Afficher la navigation";
$MESS["SHOW_FORUM_ANOTHER_SITE_TIP"] = "Afficher les forums d'autres sites";
$MESS["SORT_BY_TIP"] = "Champ de triage";
$MESS["URL_TEMPLATES_PROFILE_VIEW_TIP"] = "Modèle d'un lien au profil d'utilisateur";
$MESS["URL_TEMPLATES_LIST_TIP"] = "Page de la liste des sujets";
$MESS["URL_TEMPLATES_INDEX_TIP"] = "Liste des forums";
$MESS["URL_TEMPLATES_READ_TIP"] = "Page de lecture du sujet";
$MESS["CACHE_TYPE_TIP"] = "Type de la mise en cache";
$MESS["SET_TITLE_TIP"] = "Installer l'en-tête de la page";
$MESS["DATE_TIME_FORMAT_TIP"] = "Format d'affichage de la date et de l'heure";
?>