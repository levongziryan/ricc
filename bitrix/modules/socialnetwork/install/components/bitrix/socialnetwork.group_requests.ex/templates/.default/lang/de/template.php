<?
$MESS["SONET_GRE_T_DO_SAVE"] = "Beitritt genehmigen";
$MESS["SONET_GRE_T_REJECT"] = "Ablehnen";
$MESS["SONET_GRE_T_REJECT_OUT"] = "Einladungen stornieren";
$MESS["SONET_GRE_T_CHECK_ALL"] = "Alle wählen/Alle abwählen";
$MESS["SONET_GRE_T_SUBTITLE_IN"] = "Anfragen zum Gruppenbeitritt";
$MESS["SONET_GRE_T_SUBTITLE_OUT"] = "Einladungen in die Gruppe ";
$MESS["SONET_GRE_T_SENDER"] = "Absender";
$MESS["SONET_GRE_T_RECIPIENT"] = "Empfänger";
$MESS["SONET_GRE_T_MESSAGE_IN"] = "Nachrichten von Nutzern";
$MESS["SONET_GRE_T_MESSAGE_OUT"] = "Nachrichten für Nutzer";
$MESS["SONET_GRE_T_NO_REQUESTS"] = "Keine Anfragen.";
$MESS["SONET_GRE_T_NO_REQUESTS_DESCR"] = "Zeigt Anfragen wegen Gruppenbeitritt an.";
$MESS["SONET_GRE_T_NO_REQUESTS_OUT"] = "Keine Einladungen.";
$MESS["SONET_GRE_T_NO_REQUESTS_OUT_DESCR"] = "Zeigt Einladungen in die Gruppe an.";
$MESS["SONET_GRE_T_INVITE"] = "Zur Gruppe einladen";
$MESS["SONET_GRE_T_SUBTITLE_IN_PROJECT"] = "Anfragen wegen Projektbeitritt";
$MESS["SONET_GRE_T_SUBTITLE_OUT_PROJECT"] = "Einladungen zum Projekt";
$MESS["SONET_GRE_T_NO_REQUESTS_DESCR_PROJECT"] = "Zeigt Anfragen wegen Projektbeitritt an.";
$MESS["SONET_GRE_T_NO_REQUESTS_OUT_DESCR_PROJECT"] = "Zeigt Einladungen zum Projekt an.";
$MESS["SONET_GRE_T_INVITE_PROJECT"] = "Zum Projekt einladen";
?>