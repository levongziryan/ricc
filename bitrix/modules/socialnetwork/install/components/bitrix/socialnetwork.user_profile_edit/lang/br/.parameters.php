<?
$MESS["SONET_SET_NAVCHAIN"] = "Definir barra de navegação";
$MESS["SONET_PATH_TO_USER"] = "Modelo de página de perfil do usuário";
$MESS["SONET_PATH_TO_USER_EDIT"] = "Modelo do caminho da página de perfil de usuário";
$MESS["SONET_PAGE_VAR"] = "Variável da página";
$MESS["SONET_USER_VAR"] = "Variável de usuário";
$MESS["SONET_ID"] = "ID de usuário";
$MESS["SONET_EDITABLE_FIELDS"] = "Propriedades editáveis";
$MESS["SONET_VARIABLE_ALIASES"] = "Aliases Variáveis";
$MESS["SONET_DATE_TIME_FORMAT"] = "Formato de data e hora";
$MESS["SONET_PATH_TO_SEARCH_EXTERNAL"] = "Caminho externo para o formulário de pesquisa de usuário";
$MESS["SONET_NAME_TEMPLATE"] = "Formato do nome";
$MESS["SONET_SHOW_LOGIN"] = "Mostrar o nome de login se o campo de nome de usuário estiver disponível porém não for requisitado";
$MESS["SONET_UP1_BLOG_AVATAR"] = "Avatar";
$MESS["SONET_UP1_FORUM_AVATAR"] = "Avatar";
$MESS["SONET_UP1_PERSONAL_BIRTHDAY"] = "Aniversário";
$MESS["SONET_UP1_BLOG_PREFIX"] = "Blog:";
$MESS["SONET_UP1_PERSONAL_CITY"] = "Cidade";
$MESS["SONET_UP1_WORK_COMPANY"] = "Empresa";
$MESS["SONET_UP1_WORK_CITY"] = "Cidade da empresa";
$MESS["SONET_UP1_WORK_COUNTRY"] = "País da empresa";
$MESS["SONET_UP1_WORK_LOGO"] = "Logo da empresa";
$MESS["SONET_UP1_WORK_PROFILE"] = "Perfil da empresa";
$MESS["SONET_UP1_WORK_WWW"] = "Web site da empresa";
$MESS["SONET_UP1_PERSONAL_COUNTRY"] = "País";
$MESS["SONET_UP1_WORK_DEPARTMENT"] = "Departamento";
$MESS["SONET_UP1_BLOG_DESCRIPTION"] = "Descrição";
$MESS["SONET_UP1_FORUM_DESCRIPTION"] = "Descrição";
$MESS["SONET_UP1_FORUM_HIDE_FROM_ONLINE"] = "Não mostrar na lista";
$MESS["SONET_UP1_EMAIL"] = "E-Mail";
$MESS["SONET_UP1_PERSONAL_FAX"] = "Fax";
$MESS["SONET_UP1_NAME"] = "Nome";
$MESS["SONET_UP1_FORUM_PREFIX"] = "Fórum:";
$MESS["SONET_UP1_PERSONAL_ICQ"] = "ICQ";
$MESS["SONET_UP1_FORUM_SUBSC_GET_MY_MESSAGE"] = "Incluir as próprias mensagens na inscrição";
$MESS["SONET_UP1_BLOG_INTERESTS"] = "Interesses";
$MESS["SONET_UP1_FORUM_INTERESTS"] = "Interesses";
$MESS["SONET_UP1_PERSONAL_PROFESSION"] = "Cargo";
$MESS["SONET_UP1_LAST_LOGIN"] = "Logado pela última vez";
$MESS["SONET_UP1_LAST_NAME"] = "Sobrenome";
$MESS["SONET_UP1_LOGIN"] = "Login";
$MESS["SONET_UP1_PERSONAL_MOBILE"] = "Celular";
$MESS["SONET_UP1_BLOG_ALIAS"] = "Apelido";
$MESS["SONET_UP1_PERSONAL_NOTES"] = "Notas";
$MESS["SONET_UP1_WORK_NOTES"] = "Notas sobre a empresa";
$MESS["SONET_UP1_PERSONAL_MAILBOX"] = "Caixa Postal";
$MESS["SONET_UP1_WORK_MAILBOX"] = "Caixa Postal";
$MESS["SONET_UP1_PERSONAL_PAGER"] = "Pager";
$MESS["SONET_UP1_PERSONAL_PHONE"] = "Telefone";
$MESS["SONET_UP1_PERSONAL_PHOTO"] = "Foto";
$MESS["SONET_UP1_WORK_POSITION"] = "Posição";
$MESS["SONET_UP1_PERSONAL_STATE"] = "Região/Estado";
$MESS["SONET_UP1_DATE_REGISTER"] = "Registrado em";
$MESS["SONET_UP1_SECOND_NAME"] = "Segundo nome";
$MESS["SONET_UP1_PERSONAL_GENDER"] = "Sexo";
$MESS["SONET_UP1_FORUM_SHOW_NAME"] = "Mostrar Nome";
$MESS["SONET_UP1_FORUM_SIGNATURE"] = "Assinatura";
$MESS["SONET_UP1_LID"] = "ID do site";
$MESS["SONET_UP1_WORK_STATE"] = "Estado/Região";
$MESS["SONET_UP1_PERSONAL_STREET"] = "Endereço da rua";
$MESS["SONET_UP1_WORK_STREET"] = "Endereço da rua";
$MESS["SONET_UP1_ID"] = "ID de usuário";
$MESS["SONET_UP1_PERSONAL_WWW"] = "Web Site";
$MESS["SONET_UP1_WORK_FAX"] = "Fax de trabalho";
$MESS["SONET_UP1_WORK_PAGER"] = "Pager de trabalho";
$MESS["SONET_UP1_WORK_PHONE"] = "Telefone de trabalho";
$MESS["SONET_UP1_PERSONAL_ZIP"] = "CEP";
$MESS["SONET_UP1_WORK_ZIP"] = "CEP";
$MESS["SONET_UP1_TIME_ZONE"] = "Fuso horário";
?>