<?
$MESS["FORUM_MAIN_WAIT"] = "...";
$MESS["PM_B"] = "B";
$MESS["PM_CODE"] = "Affichage du texte sous forme d'un code (alt+p)";
$MESS["PM_ACT_SAVE"] = "Sauvegarder";
$MESS["PM_COPY_TO_OUTBOX"] = "Enregistrer une copie du message dans le dossier 'Emis'";
$MESS["PM_TRANSLIT"] = "Translit (alt + t)";
$MESS["PM_HYPERLINK"] = "Lien hypertexte (alt + h)";
$MESS["PM_I"] = "I";
$MESS["PM_IMAGE"] = "Connexion de l'image (alt+g)";
$MESS["PM_LIST"] = "Affichage du texte sous forme de la liste (alt+l)";
$MESS["PM_QUOTE"] = "Présentation du texte sous forme d'une citation (alt+q)";
$MESS["PM_U"] = "U";
$MESS["SONET_MF_MESSAGE_SENT"] = "Votre message a été envoyé.";
$MESS["SONET_MF_MESSAGE"] = "Votre message:";
$MESS["PM_HYPERLINK_TITLE"] = "Lien hypertexte (alt + h)";
$MESS["PM_WANT_ALLOW_SMILES"] = "<B>Afficher</B> les smileys graphiques dans ce message?";
$MESS["PM_CLOSE_OPENED_TAGS"] = "Fermer tous les tags ouverts";
$MESS["PM_CLOSE_ALL_TAGS"] = "Fermer tous les tags";
$MESS["PM_POST_FULLY"] = "Rempissage de la boîte:";
$MESS["PM_REQUEST_IS_READ"] = "Demande de notification après lecture";
$MESS["PM_GREEN"] = "Vert";
$MESS["PM_HEAD_NAME_LOGIN"] = "Nom/Identifiant d'utilisateur";
$MESS["PM_RED"] = "Rouge";
$MESS["PM_ITAL"] = "Italique (alt+i)";
$MESS["SONET_C26_T_MY_INBOX"] = "Mes messages entrant";
$MESS["SONET_C26_T_MY_OUTBOX"] = "Mes Messages Envoyés";
$MESS["PM_SEARCH_USER"] = "Recherche de l'utilisateur";
$MESS["PM_OPENED_TAGS"] = "Tags ouverts:&nbsp;";
$MESS["PM_HEAD_FROM"] = "De la part de:";
$MESS["PM_ACT_SEND"] = "Envoyer";
$MESS["SONET_C26_T_SEND_MESSAGE"] = "Emettre le message";
$MESS["PM_CODE_TITLE"] = "Affichage du texte sous forme d'un code (alt+p)";
$MESS["PM_LIST_TITLE"] = "Affichage du texte sous forme de la liste (alt+l)";
$MESS["PM_QUOTE_TITLE"] = "Présentation du texte sous forme d'une citation (alt+q)";
$MESS["PM_TRANSLIT_TITLE"] = "Translit (alt + t)";
$MESS["PM_IMAGE_TITLE"] = "Connexion de l'image (alt+g)";
$MESS["PM_UNDER"] = "Souligné (alt+u)";
$MESS["PM_BOLD"] = "Demi-gras (alt+b)";
$MESS["PM_HEAD_TO"] = "A qui:";
$MESS["SONET_MF_USER"] = "Utilisateur:";
$MESS["PM_GRAY"] = "Gris menu de l'onglet";
$MESS["PM_BLUE"] = "Bleu";
$MESS["SONET_MF_SMILES"] = "Dans la liste de smileys";
$MESS["SONET_C26_T_SUCCESS"] = "Le message est envoyé à l'utilisateur.";
$MESS["PM_COLOR"] = "Couleur";
$MESS["PM_FONT"] = "Choix du type de caractères";
?>