<?
$MESS["GD_ACTIVITY_P_EVENT_ID_VALUE_BLOG"] = "blogues/rapports";
$MESS["GD_ACTIVITY_P_EVENT_ID_VALUE_ALL"] = "tous";
$MESS["GD_ACTIVITY_P_EVENT_ID_VALUE_SYSTEM_GROUPS"] = "groupe";
$MESS["GD_ACTIVITY_P_EVENT_ID_VALUE_SYSTEM_FRIENDS"] = "amis";
$MESS["GD_ACTIVITY_P_EVENT_ID_VALUE_TASKS"] = "tâches";
$MESS["GD_ACTIVITY_P_EVENT_ID_VALUE_CALENDAR"] = "calendriers";
$MESS["GD_ACTIVITY_P_LOG_CNT"] = "Nombre des enregistrements affichés";
$MESS["GD_ACTIVITY_P_EVENT_ID_VALUE_SYSTEM"] = "système";
$MESS["GD_ACTIVITY_P_EVENT_ID"] = "Type de message";
$MESS["GD_ACTIVITY_P_EVENT_ID_VALUE_FILES"] = "fichiers";
$MESS["GD_ACTIVITY_P_EVENT_ID_VALUE_FORUM"] = "forums / débats";
$MESS["GD_ACTIVITY_P_EVENT_ID_VALUE_PHOTO"] = "photo";
?>