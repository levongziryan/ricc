<?
$MESS["GD_SONET_USER_GROUPS_ALL_GROUPS"] = "Tous les groupes";
$MESS["GD_SONET_USER_GROUPS_SEARCH_GROUP"] = "Recherche des groupes";
$MESS["GD_SONET_USER_GROUPS_NO_GROUPS"] = "Pas de groupes.";
$MESS["GD_SONET_USER_GROUPS_LOG"] = "Listes de mises à jours";
$MESS["GD_SONET_USER_GROUPS_CREATE_GROUP"] = "Créer un groupe";
$MESS["GD_SONET_USER_GROUPS_GR_UNAVAIL"] = "Liste de groupe inaccessible.";
?>