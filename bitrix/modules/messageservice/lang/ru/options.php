<?php
$MESS["MESSAGESERVICE_SMSRU_PARTNER"] = "Идентификатор партнера компании SMS.RU";
$MESS["MESSAGESERVICE_SMSRU_SECRET_KEY"] = "Секретный ключ партнера компании SMS.RU";
$MESS["MESSAGESERVICE_CLEAN_UP_PERIOD"] = "Сколько дней хранить сообщения";
$MESS["MESSAGESERVICE_QUEUE_LIMIT"] = "Сколько сообщений отправлять за один хит";
?>