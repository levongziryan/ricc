<?
$MESS["MESSAGESERVICE_MESSAGE_ERROR_SENDER"] = "Service zum Nachrichtenversand ist nicht angegeben";
$MESS["MESSAGESERVICE_MESSAGE_ERROR_SENDER_CAN_USE"] = "Service zum Nachrichtenversand ist nicht bereit";
$MESS["MESSAGESERVICE_MESSAGE_ERROR_TYPE"] = "Der ausgewählte Service zum Nachrichtenversand unterstützt nicht diesen Nachrichtentyp";
$MESS["MESSAGESERVICE_MESSAGE_ERROR_FROM"] = "Der Wert des Feldes \"Von ist nicht korrekt";
$MESS["MESSAGESERVICE_MESSAGE_ERROR_LIMITATION"] = "Tägliches Limit beim Senden wurde für diesen Server erreicht.";
?>