<?
$MESS["WIKI_OPTIONS_SOCNET_FORUM_ID"] = "ID du forum pour les avis d'utilisateurs:";
$MESS["WIKI_OPTIONS_SOCNET_ENABLE"] = "Activer le support du réseau social";
$MESS["WIKI_OPTIONS_SOCNET_IBLOCK_ID"] = "Bloc de l'information:";
$MESS["WIKI_OPTIONS_SOCNET_USE_CAPTCHA"] = "Utiliser CAPTCHA";
$MESS["WIKI_OPTIONS_SOCNET_MESSAGE_PER_PAGE"] = "Nombre de messages sur une page";
$MESS["WIKI_OPTIONS_IMAGE_MAX_HEIGHT"] = "Hauteur limite des images autorisées (pixels):";
$MESS["WIKI_OPTIONS_IMAGE_MAX_WIDTH"] = "Largeur maximale de l'image, sans description (px):";
$MESS["WIKI_TAB_TITLE_SOCNET"] = "Réglages de l'intégration du module Wiki au réseau social";
$MESS["WIKI_OPTIONS_ALLOW_HTML"] = "Permettre l'utilisation de l'éditeur visuel et de HTML";
$MESS["WIKI_OPTIONS_SOCNET_USE_REVIEW"] = "Autoriser la discussion";
$MESS["WIKI_TAB_SOCNET"] = "Réseau social";
$MESS["WIKI_OPTIONS_IMAGE_DESCR"] = "Il s'agit de la taille d'images obtenues par le redimensionnement des images téléchargées.";
?>