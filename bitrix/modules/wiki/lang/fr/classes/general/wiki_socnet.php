<?
$MESS["WIKI_SOCNET_TAB"] = "Wiki";
$MESS["WIKI_PERM_WRITE"] = "Création/changement des pages";
$MESS["WIKI_PERM_WRITE_W"] = "Création/modification de la page dans l'éditeur visuel";
$MESS["WIKI_PERM_READ"] = "Lecture des pages";
$MESS["WIKI_PERM_DELETE"] = "Suppression des pages et de l'historique";
$MESS["SOCNET_LOG_WIKI_GROUP"] = "Wiki";
$MESS["SOCNET_LOG_WIKI_GROUP_SETTINGS"] = "Toutes les modifications dans le Wiki de ce groupe";
$MESS["SOCNET_LOG_WIKI_GROUP_SETTINGS_1"] = "Wiki du groupe #TITLE#";
$MESS["SOCNET_LOG_WIKI_GROUP_SETTINGS_2"] = "Wiki messages du groupe #TITLE#";
$MESS["SOCNET_LOG_WIKI_DEL_GROUP"] = "Wiki (suppression)";
$MESS["WIKI_SOCNET_LOG_TITLE"] = "Article Wiki #TITLE#";
$MESS["WIKI_SOCNET_LOG_TITLE_24"] = "Message Wiki";
$MESS["WIKI_DEL_SOCNET_LOG_TITLE_24"] = "Le message Wiki est supprimé";
$MESS["WIKI_SOCNET_LOG_COMMENT_TITLE"] = "commentaire pour l'article Wiki: #TITLE#";
$MESS["WIKI_SOCNET_LOG_TITLE_MAIL"] = "Dans le groupe '#ENTITY#' #CREATED_BY# a ajouté ou changé l'article Wiki '#TITLE#'";
$MESS["WIKI_SOCNET_LOG_COMMENT_TITLE_MAIL"] = "#CREATED_BY# a ajouté un nouveau commentaire à l'article Wiki '#TITLE#' du groupe '#ENTITY#'";
$MESS["WIKI_DEL_SOCNET_LOG_TITLE"] = "l'article Wiki #TITLE# a été supprimé";
$MESS["WIKI_DEL_SOCNET_LOG_TITLE_MAIL"] = "#CREATED_BY# a supprimé l'article Wiki '#TITLE#' dans le groupe '#ENTITY#'";
$MESS["WIKI_SOCNET_LOG_USER"] = "utilisateur";
$MESS["WIKI_SOCNET_LOG_ANONYMOUS_USER"] = "pour les utilisateurs anonymes";
$MESS["WIKI_SOCNET_LOG_ENTITY_G"] = "groupe";
$MESS["F_FORUM_TOPIC_ID"] = "Sujet du forum pour les commentaires";
$MESS["F_FORUM_MESSAGE_CNT"] = "Nombre des commentaires de l'élément";
$MESS["SONET_ADD_COMMENT_SOURCE_ERROR"] = "Chec d'ajout du commentaire à la source de l'événement.";
$MESS["WIKI_SONET_FROM_LOG_IM_COMMENT"] = "Vient de laisser un commentaire à votre article Wiki '#title#'";
?>