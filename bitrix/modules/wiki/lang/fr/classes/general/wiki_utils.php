<?
$MESS["WIKI_CATEGORY_ALL"] = "Toutes les pages";
$MESS["PAGE_ACCESS"] = "Protéger";
$MESS["PAGE_ACCESS_TITLE"] = "Protéger";
$MESS["PAGE_HISTORY"] = "Histoire de la compagnie";
$MESS["PAGE_HISTORY_TITLE"] = "Histoire de la compagnie";
$MESS["CATEGORY_NAME"] = "Catégorie";
$MESS["WIKI_NEW_PAGE_TITLE"] = "Nouvelle page";
$MESS["PAGE_DISCUSSION"] = "Discussion";
$MESS["PAGE_DISCUSSION_TITLE"] = "Discussion";
$MESS["WIKI_PAGE_RENAME"] = "Renommer";
$MESS["WIKI_PAGE_RENAME_TITLE"] = "Renommer et rafraîchir tous les liens vers cette page";
$MESS["PAGE_EDIT"] = "Editer";
$MESS["PAGE_EDIT_TITLE"] = "Editer";
$MESS["PAGE_SERVICE"] = "Page de service";
$MESS["PAGE_SERVICE_TITLE"] = "Page de service";
$MESS["PAGE_ADD"] = "Ajouter";
$MESS["PAGE_ADD_TITLE"] = "Ajouter";
$MESS["PAGE_ARTICLE"] = "Article";
$MESS["PAGE_ARTICLE_TITLE"] = "Article";
$MESS["WIKI_CATEGORY_NOCAT"] = "Pages sans catégorie";
$MESS["PAGE_DELETE"] = "Supprimer";
$MESS["PAGE_DELETE_TITLE"] = "Supprimer";
$MESS["FILE_NAME"] = "Fichier";
?>