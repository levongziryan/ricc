<?
$MESS["WIKI_SELECT"] = "Choisir";
$MESS["WIKI_ID"] = "ID";
$MESS["WIKI_CREATE_NEW_IBLOCK_NAME"] = "Nom du bloc d'information";
$MESS["WIKI_CREATE_NEW_SOCNET_IBLOCK_NAME"] = "Nom du bloc d'information pour le réseau social";
$MESS["WIKI_CREATE_NEW_FORUM_NAME"] = "Nom du forum";
$MESS["WIKI_CREATE_NEW_SOCNET_FORUM_NAME"] = "Nom du forum pour le réseau social";
$MESS["WIKI_CREATE"] = "Ajouter";
$MESS["WIKI_CREATE_NEW_IBLOCK"] = "Créer un bloc d'information";
$MESS["WIKI_CREATE_NEW_SOCNET_IBLOCK"] = "Créer un bloc d'information pour les réseaux sociaux";
$MESS["WIKI_CREATE_NEW_FORUM"] = "Créer le forum";
$MESS["WIKI_CREATE_NEW_SOCNET_FORUM"] = "Créer un forum pour le réseau social";
$MESS["WIKI_CREATE_NEW_IBLOCK_TYPE"] = "Type de bloc d'information";
$MESS["WIKI_CREATE_NEW_SOCNET_IBLOCK_TYPE"] = "Type du bloc d'information pour le réseau social";
?>