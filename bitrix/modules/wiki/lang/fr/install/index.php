<?
$MESS["WIKI_INSTALL_NAME"] = "Wiki";
$MESS["WIKI_INSTALL_DESCRIPTION"] = "Le module permet de créer des pages wiki sur le site.";
$MESS["WIKI_INSTALL_TITLE"] = "Installation de module wiki";
$MESS["WIKI_UNINSTALL_TITLE"] = "Elimination du module Wiki";
$MESS["WIKI_PERM_R"] = "Lecture des pages";
$MESS["WIKI_PERM_W"] = "Création/changement des pages";
$MESS["WIKI_PERM_D"] = "Accès interdit";
$MESS["WIKI_PERM_X"] = "Création/modification de la page dans l'éditeur visuel";
$MESS["WIKI_PERM_Y"] = "Suppression des pages et de l'historique";
$MESS["WIKI_PERM_Z"] = "Installation des droits";
?>