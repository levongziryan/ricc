<?
$MESS["WIKI_SOCNET_TAB"] = "Wiki";
$MESS["WIKI_PERM_WRITE"] = "Erstellung/Bearbeitung der Seiten";
$MESS["WIKI_PERM_WRITE_W"] = "Erstellung/Bearbeitung der Seiten im visuellen Editor";
$MESS["WIKI_PERM_READ"] = "Seiten lesen";
$MESS["WIKI_PERM_DELETE"] = "Seiten und Versionsgeschichte löschen";
$MESS["SOCNET_LOG_WIKI_GROUP"] = "Wiki";
$MESS["SOCNET_LOG_WIKI_GROUP_SETTINGS"] = "Alle Änderungen in der Wiki dieser Gruppe";
$MESS["SOCNET_LOG_WIKI_GROUP_SETTINGS_1"] = "Wiki der Gruppe #TITLE#";
$MESS["SOCNET_LOG_WIKI_GROUP_SETTINGS_2"] = "Wiki-Updates der Gruppe #TITLE#";
$MESS["SOCNET_LOG_WIKI_DEL_GROUP"] = "Wiki (Löschen)";
$MESS["WIKI_SOCNET_LOG_TITLE"] = "Wiki-Seite #TITLE#";
$MESS["WIKI_SOCNET_LOG_TITLE_24"] = "Wiki-Seite";
$MESS["WIKI_DEL_SOCNET_LOG_TITLE_24"] = "Wiki-Seite wurde gelöscht";
$MESS["WIKI_SOCNET_LOG_COMMENT_TITLE"] = "Kommentar zu einer Wiki-Seite: #TITLE#";
$MESS["WIKI_SOCNET_LOG_TITLE_MAIL"] = "#CREATED_BY# hat in der Gruppe \"#ENTITY#\" eine Wiki-Seite \"#TITLE#\" erstellt oder aktualisiert";
$MESS["WIKI_SOCNET_LOG_COMMENT_TITLE_MAIL"] = "#CREATED_BY# hat in der Gruppe \"#ENTITY#\" einen neuen Kommentar zu einer Wiki-Seite \"#TITLE#\" hinzugefügt";
$MESS["WIKI_DEL_SOCNET_LOG_TITLE"] = "Wiki-Seite #TITLE# wurde gelöscht";
$MESS["WIKI_DEL_SOCNET_LOG_TITLE_MAIL"] = "#CREATED_BY# hat in der Gruppe \"#ENTITY#\" eine Wiki-Seite \"#TITLE#\" gelöscht";
$MESS["WIKI_SOCNET_LOG_USER"] = "Nutzer";
$MESS["WIKI_SOCNET_LOG_ANONYMOUS_USER"] = "Anonym";
$MESS["WIKI_SOCNET_LOG_ENTITY_G"] = "Gruppe";
$MESS["F_FORUM_TOPIC_ID"] = "Kommentare zum Forumsthema";
$MESS["F_FORUM_MESSAGE_CNT"] = "Anzahl der Kommentare";
$MESS["SONET_ADD_COMMENT_SOURCE_ERROR"] = "Der Kommentar zu einer Ereignisquelle konnte nicht hinzugefügt werden";
$MESS["WIKI_SONET_FROM_LOG_IM_COMMENT"] = "Hat einen Kommentar zu Ihrem Wiki-Artikel #title# hinzugefügt";
?>