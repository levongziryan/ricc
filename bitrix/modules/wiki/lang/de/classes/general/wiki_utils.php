<?
$MESS["PAGE_SERVICE"] = "Service-Seite";
$MESS["PAGE_SERVICE_TITLE"] = "Service-Seite";
$MESS["PAGE_ARTICLE"] = "Artikel";
$MESS["PAGE_ARTICLE_TITLE"] = "Artikel";
$MESS["PAGE_DISCUSSION"] = "Diskussion";
$MESS["PAGE_DISCUSSION_TITLE"] = "Diskussion";
$MESS["PAGE_EDIT"] = "Ändern";
$MESS["PAGE_EDIT_TITLE"] = "Ändern";
$MESS["PAGE_HISTORY"] = "Versionsgeschichte";
$MESS["PAGE_HISTORY_TITLE"] = "Versionsgeschichte";
$MESS["PAGE_DELETE"] = "Löschen";
$MESS["PAGE_DELETE_TITLE"] = "Löschen";
$MESS["PAGE_ACCESS"] = "Schützen";
$MESS["PAGE_ACCESS_TITLE"] = "Schützen";
$MESS["PAGE_ADD"] = "Erstellen";
$MESS["PAGE_ADD_TITLE"] = "Erstellen";
$MESS["WIKI_NEW_PAGE_TITLE"] = "Eine neue Seite";
$MESS["FILE_NAME"] = "Datei";
$MESS["CATEGORY_NAME"] = "Kategorie";
$MESS["WIKI_PAGE_RENAME"] = "Umbenennen";
$MESS["WIKI_PAGE_RENAME_TITLE"] = "Alle Links zu dieser Seite umbenennen und aktualisieren";
$MESS["WIKI_CATEGORY_ALL"] = "Alle Seiten";
$MESS["WIKI_CATEGORY_NOCAT"] = "Seiten ohne Kategorie";
?>