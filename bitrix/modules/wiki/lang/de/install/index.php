<?
$MESS["WIKI_INSTALL_NAME"] = "Wiki";
$MESS["WIKI_INSTALL_DESCRIPTION"] = "Das Modul ermöglicht die Verwaltung der Wiki-Seiten auf der Website.";
$MESS["WIKI_INSTALL_TITLE"] = "Wiki-Modul installieren";
$MESS["WIKI_UNINSTALL_TITLE"] = "Wiki-Modul löschen";
$MESS["WIKI_PERM_R"] = "Seiten lesen";
$MESS["WIKI_PERM_W"] = "Erstellung/Bearbeitung der Seiten";
$MESS["WIKI_PERM_D"] = "Zugriff verweigert";
$MESS["WIKI_PERM_X"] = "Erstellung/Bearbeitung der Seiten im visuellen Editor";
$MESS["WIKI_PERM_Y"] = "Seiten und Versionsgeschichte löschen";
$MESS["WIKI_PERM_Z"] = "Zugriffsberechtigung festlegen";
?>