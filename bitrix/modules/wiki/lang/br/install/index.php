<?
$MESS["WIKI_INSTALL_NAME"] = "Wiki";
$MESS["WIKI_INSTALL_DESCRIPTION"] = "Esse módulo oferece recursos wiki para seu site";
$MESS["WIKI_INSTALL_TITLE"] = "Wiki de instalação de módulo";
$MESS["WIKI_UNINSTALL_TITLE"] = "Wiki de desinstalação de módulo";
$MESS["WIKI_PERM_R"] = "Visualizar páginas";
$MESS["WIKI_PERM_W"] = "Criar e editar páginas";
$MESS["WIKI_PERM_D"] = "Acesso negado.";
$MESS["WIKI_PERM_X"] = "Criar e editar páginas no editor visual";
$MESS["WIKI_PERM_Y"] = "Deletar páginas e histórico";
$MESS["WIKI_PERM_Z"] = "Configurar Permissões";
?>