<?
$MESS["PAGE_SERVICE"] = "Página de serviço";
$MESS["PAGE_SERVICE_TITLE"] = "Página de serviço";
$MESS["PAGE_ARTICLE"] = "Artigo";
$MESS["PAGE_ARTICLE_TITLE"] = "Artigo";
$MESS["PAGE_DISCUSSION"] = "Discussão";
$MESS["PAGE_DISCUSSION_TITLE"] = "Discussão";
$MESS["PAGE_EDIT"] = "Editar";
$MESS["PAGE_EDIT_TITLE"] = "Editar";
$MESS["PAGE_ADD"] = "Criar";
$MESS["PAGE_ADD_TITLE"] = "Criar";
$MESS["PAGE_HISTORY"] = "Histórico ";
$MESS["PAGE_HISTORY_TITLE"] = "Histórico";
$MESS["PAGE_DELETE"] = "Excluir";
$MESS["PAGE_DELETE_TITLE"] = "Excluir";
$MESS["PAGE_ACCESS"] = "Proteger";
$MESS["PAGE_ACCESS_TITLE"] = "Proteger";
$MESS["WIKI_NEW_PAGE_TITLE"] = "Nova página";
$MESS["FILE_NAME"] = "Arquivo";
$MESS["CATEGORY_NAME"] = "Categoria";
$MESS["WIKI_PAGE_RENAME"] = "Renomear";
$MESS["WIKI_PAGE_RENAME_TITLE"] = "Renomear e atualizar todos os links para esta página";
$MESS["WIKI_CATEGORY_ALL"] = "Todas as páginas";
$MESS["WIKI_CATEGORY_NOCAT"] = "Páginas não categorizadas";
?>