<?
$MESS["WIKI_OPTIONS_ALLOW_HTML"] = "Usar editor visual, permitir HTMKL";
$MESS["WIKI_OPTIONS_IMAGE_MAX_WIDTH"] = "Largura máxima de imagem (pixels):";
$MESS["WIKI_OPTIONS_IMAGE_MAX_HEIGHT"] = "Altura máxima de imagem (pixels):";
$MESS["WIKI_TAB_SOCNET"] = "Rede social";
$MESS["WIKI_TAB_TITLE_SOCNET"] = "Wiki para parâmetros de integração com redes sociais";
$MESS["WIKI_OPTIONS_SOCNET_ENABLE"] = "Habilitar suporte a redes sociais";
$MESS["WIKI_OPTIONS_SOCNET_IBLOCK_ID"] = "Bloco de informações:";
$MESS["WIKI_OPTIONS_SOCNET_USE_REVIEW"] = "Habilitar discussões:";
$MESS["WIKI_OPTIONS_SOCNET_FORUM_ID"] = "ID do forum de discussão:";
$MESS["WIKI_OPTIONS_SOCNET_MESSAGE_PER_PAGE"] = "Números de posts por página";
$MESS["WIKI_OPTIONS_SOCNET_USE_CAPTCHA"] = "Usar CAPTCHA";
$MESS["WIKI_OPTIONS_IMAGE_DESCR"] = "Essas são as dimensões de uma imagem redimensionada";
?>