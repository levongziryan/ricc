<?
$MESS["WIKI_OPTIONS_ALLOW_HTML"] = "Utilice el Editor Visual y HTML";
$MESS["WIKI_OPTIONS_IMAGE_MAX_WIDTH"] = "Max. Ancho de la imagen (píxeles)";
$MESS["WIKI_OPTIONS_IMAGE_MAX_HEIGHT"] = "Max. Altura de la imagen (píxeles)";
$MESS["WIKI_TAB_SOCNET"] = "Redes Sociales";
$MESS["WIKI_TAB_TITLE_SOCNET"] = "PArámetros de intregración de WIKI con Redes Sociales";
$MESS["WIKI_OPTIONS_SOCNET_ENABLE"] = "Habilitar el soporte de Redes Sociales";
$MESS["WIKI_OPTIONS_SOCNET_IBLOCK_ID"] = "Iblock";
$MESS["WIKI_OPTIONS_SOCNET_USE_REVIEW"] = "Habilitar las discusiones";
$MESS["WIKI_OPTIONS_SOCNET_FORUM_ID"] = "Id del Foro de debate";
$MESS["WIKI_OPTIONS_SOCNET_MESSAGE_PER_PAGE"] = "Número de mensajes por página";
$MESS["WIKI_OPTIONS_SOCNET_USE_CAPTCHA"] = "Usar CAPTCHA";
$MESS["WIKI_OPTIONS_IMAGE_DESCR"] = "Estas son las dimensiones de la imagen redimensionada.";
?>