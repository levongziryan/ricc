<?
$MESS["WIKI_INSTALL_NAME"] = "Wiki";
$MESS["WIKI_INSTALL_DESCRIPTION"] = "Este módulo brinda características de Wiki para su website.";
$MESS["WIKI_INSTALL_TITLE"] = "Instalación del módulo de Wiki";
$MESS["WIKI_UNINSTALL_TITLE"] = "Instalación del módulo de Wiki";
$MESS["WIKI_PERM_R"] = "Ver páginas";
$MESS["WIKI_PERM_W"] = "Crear y editar las páginas";
$MESS["WIKI_PERM_D"] = "Acceso denegado";
$MESS["WIKI_PERM_X"] = "Crear y editar las páginas en el editor visual";
$MESS["WIKI_PERM_Y"] = "Borrar las páginas y el historial";
$MESS["WIKI_PERM_Z"] = "Establecer permisos";
?>