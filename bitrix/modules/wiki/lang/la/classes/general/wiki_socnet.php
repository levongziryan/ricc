<?
$MESS["WIKI_SOCNET_TAB"] = "Wiki";
$MESS["WIKI_PERM_WRITE"] = "Crear y editar páginas";
$MESS["WIKI_PERM_WRITE_W"] = "Crear y editar páginas en el editor visual";
$MESS["WIKI_PERM_READ"] = "Ver páginas ";
$MESS["WIKI_PERM_DELETE"] = "Borrar las páginas y el historial";
$MESS["SOCNET_LOG_WIKI_GROUP"] = "Wiki";
$MESS["SOCNET_LOG_WIKI_GROUP_SETTINGS"] = "Todos los cambios en los grupos Wiki";
$MESS["SOCNET_LOG_WIKI_DEL_GROUP"] = "Wiki (borrar)";
$MESS["WIKI_SOCNET_LOG_TITLE_MAIL"] = "#CREATED_BY# creado o actualizado la página \"#TITLE#\" en el grupo \"#ENTITY#\"";
$MESS["WIKI_DEL_SOCNET_LOG_TITLE_MAIL"] = "#CREATED_BY# borrado de la página Wiki \"#TITLE#\" in el grupo \"#ENTITY#\"";
$MESS["WIKI_SOCNET_LOG_USER"] = "usuario";
$MESS["WIKI_SOCNET_LOG_ANONYMOUS_USER"] = "anónimo";
$MESS["WIKI_SOCNET_LOG_ENTITY_G"] = "grupo";
$MESS["SOCNET_LOG_WIKI_GROUP_SETTINGS_1"] = "#TITLE#: Wiki";
$MESS["SOCNET_LOG_WIKI_GROUP_SETTINGS_2"] = "Título de las actualizaciones de Wiki";
$MESS["WIKI_SOCNET_LOG_TITLE"] = "Articulo Wiki \"#TITLE#\" agregado o actualizado";
$MESS["WIKI_DEL_SOCNET_LOG_TITLE"] = "Articulo Wiki \"#TITLE#\" borrado";
$MESS["WIKI_SOCNET_LOG_COMMENT_TITLE"] = "comentar en la página de Wiki: #TITLE#";
$MESS["WIKI_SOCNET_LOG_COMMENT_TITLE_MAIL"] = "#CREATED_BY# se ha agregado un nuevo comentario a la página Wiki \"#TITLE#\" en el grupo\"#ENTITY#\"";
$MESS["F_FORUM_TOPIC_ID"] = "Temas de los comentarios del Foro";
$MESS["F_FORUM_MESSAGE_CNT"] = "Contar números";
$MESS["SONET_ADD_COMMENT_SOURCE_ERROR"] = "Falla al agregar un comentario a la fuente del evento.";
$MESS["WIKI_SOCNET_LOG_TITLE_24"] = "Página wiki";
$MESS["WIKI_DEL_SOCNET_LOG_TITLE_24"] = "Página Wiki eliminada";
$MESS["WIKI_SONET_FROM_LOG_IM_COMMENT"] = "Se ha añadido un comentario en el artículo de Wiki, \"#title#\"";
?>