<?
$MESS["WIKI_DEFAULT_PAGE_NAME"] = "Page de la liste commune";
$MESS["WIKI_ACCESS_DENIED"] = "Accès interdit.";
$MESS["CATEGORY_NAME"] = "Catégorie";
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "Le module des blocs d'informations n'a pas été installé.";
$MESS["SOCNET_MODULE_NOT_INSTALLED"] = "Le module du réseau social n'a pas été installé.";
$MESS["WIKI_MODULE_NOT_INSTALLED"] = "Module wiki n'a pas été installé.";
$MESS["IBLOCK_NOT_ASSIGNED"] = "Le bloc d'information n'a pas été sélectionné.";
?>