<?
$MESS["admin_authorize_required"] = "Всі поля обов'язкові для заповнення.";
$MESS["AUTH_CHANGE_PASSWORD"] = "Зміна паролю";
$MESS["AUTH_CHANGE"] = "Змінити пароль";
$MESS["admin_authorize_info"] = "Інформація";
$MESS["AUTH_CHECKWORD"] = "Контрольний рядок";
$MESS["AUTH_LOGIN"] = "Логін";
$MESS["AUTH_NEW_PASSWORD"] = "Новий пароль";
$MESS["AUTH_NEW_PASSWORD_CONFIRM"] = "Підтвердження паролю";
$MESS["admin_authorize_back"] = "Повернутися на";
$MESS["admin_authorize_error"] = "Помилка авторизації";
$MESS["admin_authorize_back_form"] = "форму авторизації";
$MESS["AUTH_SECURE_NOTE"] = "Перед відправкою форми пароль буде зашифрований в браузері. Це дозволить уникнути передачі пароля у відкритому вигляді.";
$MESS["AUTH_NONSECURE_NOTE"] = "Пароль буде відправлений у відкритому вигляді. Увімкніть  JavaScript в браузері, щоб зашифрувати пароль перед відправкою.";
$MESS["AUTH_CHANGE_PASSWORD_1"] = "Введіть контрольний рядок і новий пароль";
$MESS["AUTH_GOTO_FORGOT_FORM"] = "Перейти на форму для відновлення пароля";
$MESS["AUTH_GOTO_AUTH_FORM"] = "Перейти на форму авторизації";
$MESS["AUTH_NEW_PASSWORD_CONFIRM_WRONG"] = "Введені паролі не співпадають";
?>