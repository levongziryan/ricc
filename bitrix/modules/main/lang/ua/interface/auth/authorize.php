<?
$MESS["AUTH_GO_AUTH_FORM"] = "форму для запиту пароля";
$MESS["AUTH_AUTHORIZE"] = "Авторизуватися";
$MESS["AUTH_PLEASE_AUTH"] = "Будь ласка, авторизуйтеся";
$MESS["AUTH_CAPTCHA_PROMT"] = "Введіть слово на картинці";
$MESS["AUTH_FORGOT_PASSWORD_2"] = "Забули свій пароль?";
$MESS["AUTH_REMEMBER_ME"] = "Запам'ятати мене на цьому комп'ютері";
$MESS["admin_authorize_info"] = "Інформація";
$MESS["AUTH_LOGIN"] = "Логін";
$MESS["AUTH_PASSWORD"] = "Пароль";
$MESS["AUTH_MESS_1"] = "Після отримання контрольного рядка прямуйте до";
$MESS["admin_authorize_error"] = "Помилка авторизації";
$MESS["AUTH_GO"] = "Прямуйте на";
$MESS["AUTH_CHANGE_FORM"] = "форми для зміни паролю";
$MESS["AUTH_SECURE_NOTE"] = "Перед відправкою форми авторизації пароль буде зашифрований в браузері. Це дозволить уникнути передачі пароля у відкритому вигляді.";
$MESS["AUTH_NONSECURE_NOTE"] = "Пароль буде відправлений у відкритому вигляді. Увімкніть JavaScript в браузері, щоб зашифрувати пароль перед відправкою.";
?>