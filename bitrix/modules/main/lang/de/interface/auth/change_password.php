<?
$MESS["admin_authorize_required"] = "Alle Felder müssen ausgefüllt sein.";
$MESS["admin_authorize_error"] = "Es ist ein Fehler beim Autorisieren aufgetreten";
$MESS["admin_authorize_back_form"] = "Login-Formular";
$MESS["admin_authorize_back"] = "Zurück zu";
$MESS["AUTH_CHANGE"] = "Passwort ändern";
$MESS["AUTH_CHECKWORD"] = "Prüfcode:";
$MESS["admin_authorize_info"] = "Information";
$MESS["AUTH_LOGIN"] = "Loginname";
$MESS["AUTH_NEW_PASSWORD"] = "Neues Passwort";
$MESS["AUTH_CHANGE_PASSWORD"] = "Passwort ändern";
$MESS["AUTH_NEW_PASSWORD_CONFIRM"] = "Passwortbestätigung";
$MESS["AUTH_SECURE_NOTE"] = "Das Passwort wird verschlüsselt, bevor es versendet wird. So wird das Passwort während der Übertragung nicht offen angezeigt.";
$MESS["AUTH_NONSECURE_NOTE"] = "Das Passwort wird in offener Form versendet. Aktivieren Sie JavaScript  in Ihrem Web-Browser, um das Passwort vor dem Versenden zu verschlüsseln.";
?>