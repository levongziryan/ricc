<?
$MESS["admin_authorize_error"] = "Es ist ein Fehler beim Autorisieren aufgetreten";
$MESS["admin_authorize_back_form"] = "Login-Formular";
$MESS["admin_authorize_back"] = "Zurück zu";
$MESS["AUTH_CHANGE_FORM"] = "Formularpasswort ändern";
$MESS["AUTH_GET_CHECK_STRING"] = "Prüfcode anfordern";
$MESS["AUTH_FORGOT_PASSWORD_1"] = "Wenn Sie Ihr Passwort vergessen haben, geben Sie bitte Ihren Loginnamen oder Ihre E-Mail-Adresse ein.<br>Sie erhalten per E-Mail Informationen zu Ihrer Anmeldung.";
$MESS["admin_authorize_info"] = "Information";
$MESS["AUTH_LOGIN"] = "Loginname";
$MESS["AUTH_OR"] = "oder";
$MESS["AUTH_SEND"] = "Senden";
$MESS["AUTH_MESS_1"] = "Wenn Sie Ihren Prüfcode erhalten haben, gehen Sie bitte zum Formular";
?>