<?
$MESS["admin_authorize_error"] = "Es ist ein Fehler beim Autorisieren aufgetreten";
$MESS["AUTH_AUTHORIZE"] = "Anmelden";
$MESS["AUTH_CHANGE_FORM"] = "Formularpasswort ändern";
$MESS["AUTH_FORGOT_PASSWORD_2"] = "Passwort vergessen?";
$MESS["AUTH_GO"] = "Wechseln zum";
$MESS["admin_authorize_info"] = "Information";
$MESS["AUTH_LOGIN"] = "Loginname";
$MESS["AUTH_PASSWORD"] = "Passwort";
$MESS["AUTH_PLEASE_AUTH"] = "Bitte melden Sie sich an";
$MESS["AUTH_REMEMBER_ME"] = "Auf diesem Computer merken";
$MESS["AUTH_GO_AUTH_FORM"] = "Formular für die Passwortanforderung";
$MESS["AUTH_CAPTCHA_PROMT"] = "Geben Sie das Wort vom Bild ein";
$MESS["AUTH_MESS_1"] = "Wenn Sie Ihren Prüfcode erhalten haben, gehen Sie bitte zum Formular";
$MESS["AUTH_SECURE_NOTE"] = "Das Passwort wird verschlüsselt, bevor es versendet wird. So wird das Passwort während der Übertragung nicht offen angezeigt.";
$MESS["AUTH_NONSECURE_NOTE"] = "Das Passwort wird in offener Form versendet. Aktivieren Sie JavaScript  in Ihrem Web-Browser, um das Passwort vor dem Versenden zu verschlüsseln.";
?>