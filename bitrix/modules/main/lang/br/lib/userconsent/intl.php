<?
$MESS["MAIN_USER_CONSENT_INTL_HINT_FIELD_DEFAULT"] = "O texto padrão irá conter:";
$MESS["MAIN_USER_CONSENT_INTL_FIELD_COMPANY_NAME"] = "Nome da empresa";
$MESS["MAIN_USER_CONSENT_INTL_FIELD_COMPANY_NAME_HINT"] = "Exemplo: Smith LLC";
$MESS["MAIN_USER_CONSENT_INTL_TYPE_N"] = "Personalizado";
$MESS["MAIN_USER_CONSENT_INTL_TYPE_S"] = "Standard";
$MESS["MAIN_USER_CONSENT_INTL_NAME"] = "Política padrão de tratamento de dados pessoais «%language_name%»";
?>