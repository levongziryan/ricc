<?
$MESS["FILE_SIZE_b"] = "base";
$MESS["FILE_HEIGHT"] = "Hauteur";
$MESS["FILE_SIZE_Gb"] = "Gb";
$MESS["MAIN_BAD_FILENAME1"] = "Le nom de fichier contient des caractères invalides.";
$MESS["main_js_img_title"] = "Image";
$MESS["FILE_SIZE_Kb"] = "KB";
$MESS["FILE_SIZE_Mb"] = "mégaoctets";
$MESS["FILE_BAD_FILENAME"] = "Le nom du fichier n'est pas indiqué.";
$MESS["FILE_BAD_TYPE"] = "Type du fichier incorrect ou la taille maximale du fichier est surpassée";
$MESS["MAIN_FIELD_FILE_DESC"] = "Description";
$MESS["main_include_dots"] = "pixels";
$MESS["FILE_BAD_MAX_RESOLUTION"] = "La taille maximale admissible de l'image est dépassée";
$MESS["FILE_BAD_SIZE"] = "La taille maximale du fichier à charger est dépassée";
$MESS["FILE_BAD_QUOTA"] = "Limite de l'espace disque dépassée.";
$MESS["FILE_SIZE"] = "Rempli";
$MESS["FILE_DOWNLOAD"] = "Télécharger";
$MESS["FILE_FILE_DOWNLOAD"] = "Télécharger le document";
$MESS["MAIN_BAD_FILENAME_LEN"] = "Le nom de fichier est trop long.";
$MESS["FILE_SIZE_Tb"] = "TS";
$MESS["FILE_ENLARGE"] = "Faire agrandir";
$MESS["FILE_DELETE"] = "Suppression du fichier";
$MESS["FILE_TEXT"] = "Fichier";
$MESS["FILE_NOT_FOUND"] = "Fichier introuvable";
$MESS["FILE_BAD_FILE_TYPE"] = "L'objet n'est pas un fichier graphique";
$MESS["FILE_WIDTH"] = "Largeur";
?>