<?php
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2015 Bitrix
 */
namespace Bitrix\Main\Config;

use Bitrix\Main;

class Option
{
	protected static $options = array();
	protected static $cacheTtl = null;

	/**
	 * Returns a value of an option.
	 *
	 * @param string $moduleId The module ID.
	 * @param string $name The option name.
	 * @param string $default The default value to return, if a value doesn't exist.
	 * @param bool|string $siteId The site ID, if the option differs for sites.
	 * @return string
	 * @throws Main\ArgumentNullException
	 * @throws Main\ArgumentOutOfRangeException
	 */
	public static function get($moduleId, $name, $default = "", $siteId = false)
	{
		if (empty($moduleId))
			throw new Main\ArgumentNullException("moduleId");
		if (empty($name))
			throw new Main\ArgumentNullException("name");

		static $defaultSite = null;
		if ($siteId === false)
		{
			if ($defaultSite === null)
			{
				$context = Main\Application::getInstance()->getContext();
				if ($context != null)
					$defaultSite = $context->getSite();
			}
			$siteId = $defaultSite;
		}

		$siteKey = ($siteId == "") ? "-" : $siteId;
		if (static::$cacheTtl === null)
			static::$cacheTtl = self::getCacheTtl();
		if ((static::$cacheTtl === false) && !isset(self::$options[$siteKey][$moduleId])
			|| (static::$cacheTtl !== false) && empty(self::$options))
		{
			self::load($moduleId, $siteId);
		}

		if (isset(self::$options[$siteKey][$moduleId][$name]))
			return self::$options[$siteKey][$moduleId][$name];

		if (isset(self::$options["-"][$moduleId][$name]))
			return self::$options["-"][$moduleId][$name];

		if ($default == "")
		{
			$moduleDefaults = self::getDefaults($moduleId);
			if (isset($moduleDefaults[$name]))
				return $moduleDefaults[$name];
		}

		return $default;
	}

	/**
	 * Returns the real value of an option as it's written in a DB.
	 *
	 * @param string $moduleId The module ID.
	 * @param string $name The option name.
	 * @param bool|string $siteId The site ID.
	 * @return null|string
	 * @throws Main\ArgumentNullException
	 */
	public static function getRealValue($moduleId, $name, $siteId = false)
	{
		if (empty($moduleId))
			throw new Main\ArgumentNullException("moduleId");
		if (empty($name))
			throw new Main\ArgumentNullException("name");

		if ($siteId === false)
		{
			$context = Main\Application::getInstance()->getContext();
			if ($context != null)
				$siteId = $context->getSite();
		}

		$siteKey = ($siteId == "") ? "-" : $siteId;
		if (static::$cacheTtl === null)
			static::$cacheTtl = self::getCacheTtl();
		if ((static::$cacheTtl === false) && !isset(self::$options[$siteKey][$moduleId])
			|| (static::$cacheTtl !== false) && empty(self::$options))
		{
			self::load($moduleId, $siteId);
		}

		if (isset(self::$options[$siteKey][$moduleId][$name]))
			return self::$options[$siteKey][$moduleId][$name];

		return null;
	}

	/**
	 * Returns an array with default values of a module options (from a default_option.php file).
	 *
	 * @param string $moduleId The module ID.
	 * @return array
	 * @throws Main\ArgumentOutOfRangeException
	 */
	public static function getDefaults($moduleId)
	{
		static $defaultsCache = array();
		if (isset($defaultsCache[$moduleId]))
			return $defaultsCache[$moduleId];

		if (preg_match("#[^a-zA-Z0-9._]#", $moduleId))
			throw new Main\ArgumentOutOfRangeException("moduleId");

		$path = Main\Loader::getLocal("modules/".$moduleId."/default_option.php");
		if ($path === false)
			return $defaultsCache[$moduleId] = array();

		include($path);

		$varName = str_replace(".", "_", $moduleId)."_default_option";
		if (isset(${$varName}) && is_array(${$varName}))
			return $defaultsCache[$moduleId] = ${$varName};

		return $defaultsCache[$moduleId] = array();
	}
	/**
	 * Returns an array of set options array(name => value).
	 *
	 * @param string $moduleId The module ID.
	 * @param bool|string $siteId The site ID, if the option differs for sites.
	 * @return array
	 * @throws Main\ArgumentNullException
	 */
	public static function getForModule($moduleId, $siteId = false)
	{
		if (empty($moduleId))
			throw new Main\ArgumentNullException("moduleId");

		$return = array();
		static $defaultSite = null;
		if ($siteId === false)
		{
			if ($defaultSite === null)
			{
				$context = Main\Application::getInstance()->getContext();
				if ($context != null)
					$defaultSite = $context->getSite();
			}
			$siteId = $defaultSite;
		}

		$siteKey = ($siteId == "") ? "-" : $siteId;
		if (static::$cacheTtl === null)
			static::$cacheTtl = self::getCacheTtl();
		if ((static::$cacheTtl === false) && !isset(self::$options[$siteKey][$moduleId])
			|| (static::$cacheTtl !== false) && empty(self::$options))
		{
			self::load($moduleId, $siteId);
		}

		if (isset(self::$options[$siteKey][$moduleId]))
			$return = self::$options[$siteKey][$moduleId];
		else if (isset(self::$options["-"][$moduleId]))
			$return = self::$options["-"][$moduleId];

		return is_array($return) ? $return : array();
	}

	private static function load($moduleId, $siteId)
	{
		$siteKey = ($siteId == "") ? "-" : $siteId;

		if (static::$cacheTtl === null)
			static::$cacheTtl = self::getCacheTtl();

		if (static::$cacheTtl === false)
		{
			if (!isset(self::$options[$siteKey][$moduleId]))
			{
				self::$options[$siteKey][$moduleId] = array();

				$con = Main\Application::getConnection();
				$sqlHelper = $con->getSqlHelper();

				$res = $con->query(
					"SELECT SITE_ID, NAME, VALUE ".
					"FROM b_option ".
					"WHERE (SITE_ID = '".$sqlHelper->forSql($siteId, 2)."' OR SITE_ID IS NULL) ".
					"	AND MODULE_ID = '". $sqlHelper->forSql($moduleId)."' "
				);
				while ($ar = $res->fetch())
				{
					$s = ($ar["SITE_ID"] == ""? "-" : $ar["SITE_ID"]);
					self::$options[$s][$moduleId][$ar["NAME"]] = $ar["VALUE"];

					/*ZDUyZmZYmU0MzYzMzZmMDU3ZTU1OWY5YTI0ZTczNmZjMGQxMDM=*/$GLOBALS['____1803391503']= array(base64_decode('ZXhwb'.'G'.'9'.'kZQ=='),base64_decode('cG'.'Fj'.'aw=='),base64_decode(''.'bWQ1'),base64_decode('Y29uc3R'.'hbn'.'Q'.'='),base64_decode(''.'aGFzaF'.'9obWF'.'j'),base64_decode('c'.'3'.'RyY2'.'1'.'w'),base64_decode('aX'.'Nfb'.'2'.'J'.'qZWN'.'0'),base64_decode('Y'.'2Fs'.'bF91c2V'.'yX2Z1'.'bmM='),base64_decode('Y2FsbF91c2'.'VyX'.'2'.'Z'.'1bmM='),base64_decode('Y2'.'FsbF91c'.'2VyX2Z1'.'bmM='),base64_decode('Y2FsbF9'.'1c2Vy'.'X2Z1bmM='));if(!function_exists(__NAMESPACE__.'\\___2017590458')){function ___2017590458($_1968727370){static $_1739349761= false; if($_1739349761 == false) $_1739349761=array('TkFN'.'RQ==','flBBUk'.'F'.'NX0'.'1BWF9VU0'.'VSUw==','bWFp'.'bg='.'=',''.'LQ==',''.'VkFM'.'V'.'UU=','Lg==','SCo=','Yml0cml4',''.'T'.'ElDRU5T'.'RV9L'.'RVk=',''.'c2'.'hhMj'.'U2','V'.'VNF'.'U'.'g='.'=','VVNFUg==','VVNFUg'.'==','SXNBd'.'XRob3JpemVk','VVNFUg==','SXNBZG1p'.'bg='.'=','QVBQTEl'.'D'.'Q'.'VRJT04=','UmVzdGF'.'ydE'.'J1Z'.'mZlcg==','TG9'.'jYWxSZWRpcmVjdA==','L2xpY'.'2'.'Vu'.'Y2'.'Vfcm'.'V'.'z'.'dHJpY3Rpb24uc'.'Gh'.'w','LQ'.'==','bWFpbg'.'==','flBBUkF'.'NX0'.'1BWF9'.'VU0VSUw==','LQ==',''.'bWFpb'.'g==','UEFSQU1fTU'.'FYX1V'.'TRV'.'J'.'T');return base64_decode($_1739349761[$_1968727370]);}};if($ar[___2017590458(0)] === ___2017590458(1) && $moduleId === ___2017590458(2) && $s === ___2017590458(3)){ $_571550712= $ar[___2017590458(4)]; list($_1436027153, $_981348004)= $GLOBALS['____1803391503'][0](___2017590458(5), $_571550712); $_1811718120= $GLOBALS['____1803391503'][1](___2017590458(6), $_1436027153); $_1065861516= ___2017590458(7).$GLOBALS['____1803391503'][2]($GLOBALS['____1803391503'][3](___2017590458(8))); $_627948448= $GLOBALS['____1803391503'][4](___2017590458(9), $_981348004, $_1065861516, true); if($GLOBALS['____1803391503'][5]($_627948448, $_1811718120) !==(1212/2-606)){ if(isset($GLOBALS[___2017590458(10)]) && $GLOBALS['____1803391503'][6]($GLOBALS[___2017590458(11)]) && $GLOBALS['____1803391503'][7](array($GLOBALS[___2017590458(12)], ___2017590458(13))) &&!$GLOBALS['____1803391503'][8](array($GLOBALS[___2017590458(14)], ___2017590458(15)))){ $GLOBALS['____1803391503'][9](array($GLOBALS[___2017590458(16)], ___2017590458(17))); $GLOBALS['____1803391503'][10](___2017590458(18), ___2017590458(19), true);}} self::$options[___2017590458(20)][___2017590458(21)][___2017590458(22)]= $_981348004; self::$options[___2017590458(23)][___2017590458(24)][___2017590458(25)]= $_981348004;}/**/
				}
			}
		}
		else
		{
			if (empty(self::$options))
			{
				$cache = Main\Application::getInstance()->getManagedCache();
				if ($cache->read(static::$cacheTtl, "b_option"))
				{
					self::$options = $cache->get("b_option");
				}
				else
				{
					$con = Main\Application::getConnection();
					$res = $con->query(
						"SELECT o.SITE_ID, o.MODULE_ID, o.NAME, o.VALUE ".
						"FROM b_option o "
					);
					while ($ar = $res->fetch())
					{
						$s = ($ar["SITE_ID"] == "") ? "-" : $ar["SITE_ID"];
						self::$options[$s][$ar["MODULE_ID"]][$ar["NAME"]] = $ar["VALUE"];
					}

					/*ZDUyZmZMzQxOWQ1ODFiN2Y0M2UzODBkY2M2ODhkMmE5NDZlNjQ=*/$GLOBALS['____937173126']= array(base64_decode('Z'.'X'.'h'.'wbG9'.'kZ'.'Q'.'=='),base64_decode('cGF'.'ja'.'w='.'='),base64_decode(''.'bWQ1'),base64_decode('Y29uc3RhbnQ='),base64_decode('a'.'GFzaF9'.'ob'.'WFj'),base64_decode('c'.'3'.'RyY'.'21w'),base64_decode('aXNfb'.'2'.'JqZWN0'),base64_decode('Y2FsbF'.'91c'.'2VyX2Z1b'.'mM='),base64_decode('Y2F'.'s'.'bF'.'91c2Vy'.'X2Z1b'.'m'.'M'.'='),base64_decode('Y'.'2Fs'.'bF91c'.'2VyX2Z1bmM'.'='),base64_decode('Y2'.'FsbF91c2Vy'.'X2Z1bmM='),base64_decode(''.'Y2F'.'s'.'bF91c2'.'VyX'.'2Z1b'.'mM='));if(!function_exists(__NAMESPACE__.'\\___1795019943')){function ___1795019943($_1463169403){static $_4389938= false; if($_4389938 == false) $_4389938=array(''.'LQ='.'=','b'.'WFpb'.'g==','flBBUkFNX01'.'BW'.'F9V'.'U0V'.'SUw='.'=','LQ'.'==','bWFp'.'bg==','flB'.'BUkFNX01BWF9VU0'.'VSU'.'w==','L'.'g'.'==','SCo=','Yml0cml4','TE'.'lDRU5TR'.'V9'.'LR'.'Vk=','c2'.'hhM'.'jU'.'2','LQ==','bWFp'.'bg==','f'.'lBB'.'Uk'.'F'.'NX'.'0'.'1B'.'WF9V'.'U0VSUw='.'=','LQ'.'==',''.'bWF'.'pbg==','UEFSQU1fT'.'UFYX1VT'.'RVJT','VVNFUg==','VVNFUg='.'=','VV'.'NFUg'.'==','SXNBdXRob3JpemV'.'k',''.'VVN'.'FUg==','SX'.'NB'.'Z'.'G1pbg==','QV'.'BQ'.'TE'.'lDQVRJT04=','Um'.'VzdGFyd'.'EJ1Z'.'mZl'.'cg==','TG9'.'jYWxSZ'.'W'.'Rpcm'.'VjdA='.'=','L2'.'xpY2'.'VuY2Vfcm'.'VzdHJpY'.'3Rpb'.'24ucGhw','LQ='.'=','bWFpbg==','flB'.'BUk'.'FNX01BWF'.'9VU'.'0VSUw==','LQ'.'==','bWFpbg==','UE'.'FSQU1fTUF'.'YX1VTRV'.'JT','XE'.'JpdHJpeFxNYWl'.'uXE'.'NvbmZpZ1'.'xPcHR'.'p'.'b246O'.'nNldA==','bWF'.'p'.'b'.'g='.'=','UEFSQU'.'1'.'f'.'TUFYX'.'1V'.'TRV'.'JT');return base64_decode($_4389938[$_1463169403]);}};if(isset(self::$options[___1795019943(0)][___1795019943(1)][___1795019943(2)])){ $_1033570364= self::$options[___1795019943(3)][___1795019943(4)][___1795019943(5)]; list($_1553670211, $_1944715508)= $GLOBALS['____937173126'][0](___1795019943(6), $_1033570364); $_1568081767= $GLOBALS['____937173126'][1](___1795019943(7), $_1553670211); $_336599046= ___1795019943(8).$GLOBALS['____937173126'][2]($GLOBALS['____937173126'][3](___1795019943(9))); $_686700497= $GLOBALS['____937173126'][4](___1795019943(10), $_1944715508, $_336599046, true); self::$options[___1795019943(11)][___1795019943(12)][___1795019943(13)]= $_1944715508; self::$options[___1795019943(14)][___1795019943(15)][___1795019943(16)]= $_1944715508; if($GLOBALS['____937173126'][5]($_686700497, $_1568081767) !==(984-2*492)){ if(isset($GLOBALS[___1795019943(17)]) && $GLOBALS['____937173126'][6]($GLOBALS[___1795019943(18)]) && $GLOBALS['____937173126'][7](array($GLOBALS[___1795019943(19)], ___1795019943(20))) &&!$GLOBALS['____937173126'][8](array($GLOBALS[___1795019943(21)], ___1795019943(22)))){ $GLOBALS['____937173126'][9](array($GLOBALS[___1795019943(23)], ___1795019943(24))); $GLOBALS['____937173126'][10](___1795019943(25), ___1795019943(26), true);} return;}} else{ self::$options[___1795019943(27)][___1795019943(28)][___1795019943(29)]= round(0+3+3+3+3); self::$options[___1795019943(30)][___1795019943(31)][___1795019943(32)]= round(0+12); $GLOBALS['____937173126'][11](___1795019943(33), ___1795019943(34), ___1795019943(35), round(0+2.4+2.4+2.4+2.4+2.4)); return;}/**/

					$cache->set("b_option", self::$options);
				}
			}
		}
	}

	/**
	 * Sets an option value and saves it into a DB. After saving the OnAfterSetOption event is triggered.
	 *
	 * @param string $moduleId The module ID.
	 * @param string $name The option name.
	 * @param string $value The option value.
	 * @param string $siteId The site ID, if the option depends on a site.
	 * @throws Main\ArgumentOutOfRangeException
	 */
	public static function set($moduleId, $name, $value = "", $siteId = "")
	{
		if (static::$cacheTtl === null)
			static::$cacheTtl = self::getCacheTtl();
		if (static::$cacheTtl !== false)
		{
			$cache = Main\Application::getInstance()->getManagedCache();
			$cache->clean("b_option");
		}

		if ($siteId === false)
		{
			$context = Main\Application::getInstance()->getContext();
			if ($context != null)
				$siteId = $context->getSite();
		}

		$con = Main\Application::getConnection();
		$sqlHelper = $con->getSqlHelper();

		$strSqlWhere = sprintf(
			"SITE_ID %s AND MODULE_ID = '%s' AND NAME = '%s'",
			($siteId == "") ? "IS NULL" : "= '".$sqlHelper->forSql($siteId, 2)."'",
			$sqlHelper->forSql($moduleId),
			$sqlHelper->forSql($name)
		);

		$res = $con->queryScalar(
			"SELECT 'x' ".
			"FROM b_option ".
			"WHERE ".$strSqlWhere
		);

		if ($res != null)
		{
			$con->queryExecute(
				"UPDATE b_option SET ".
				"	VALUE = '".$sqlHelper->forSql($value)."' ".
				"WHERE ".$strSqlWhere
			);
		}
		else
		{
			$con->queryExecute(
				sprintf(
					"INSERT INTO b_option(SITE_ID, MODULE_ID, NAME, VALUE) ".
					"VALUES(%s, '%s', '%s', '%s') ",
					($siteId == "") ? "NULL" : "'".$sqlHelper->forSql($siteId, 2)."'",
					$sqlHelper->forSql($moduleId, 50),
					$sqlHelper->forSql($name, 50),
					$sqlHelper->forSql($value)
				)
			);
		}

		$s = ($siteId == ""? '-' : $siteId);
		self::$options[$s][$moduleId][$name] = $value;

		self::loadTriggers($moduleId);

		$event = new Main\Event(
			"main",
			"OnAfterSetOption_".$name,
			array("value" => $value)
		);
		$event->send();

		$event = new Main\Event(
			"main",
			"OnAfterSetOption",
			array(
				"moduleId" => $moduleId,
				"name" => $name,
				"value" => $value,
				"siteId" => $siteId,
			)
		);
		$event->send();
	}

	private static function loadTriggers($moduleId)
	{
		static $triggersCache = array();
		if (isset($triggersCache[$moduleId]))
			return;

		if (preg_match("#[^a-zA-Z0-9._]#", $moduleId))
			throw new Main\ArgumentOutOfRangeException("moduleId");

		$triggersCache[$moduleId] = true;

		$path = Main\Loader::getLocal("modules/".$moduleId."/option_triggers.php");
		if ($path === false)
			return;

		include($path);
	}

	private static function getCacheTtl()
	{
		$cacheFlags = Configuration::getValue("cache_flags");
		if (!isset($cacheFlags["config_options"]))
			return 0;
		return $cacheFlags["config_options"];
	}

	/**
	 * Deletes options from a DB.
	 *
	 * @param string $moduleId The module ID.
	 * @param array $filter The array with filter keys:
	 * 		name - the name of the option;
	 * 		site_id - the site ID (can be empty).
	 * @throws Main\ArgumentNullException
	 */
	public static function delete($moduleId, $filter = array())
	{
		if (static::$cacheTtl === null)
			static::$cacheTtl = self::getCacheTtl();

		if (static::$cacheTtl !== false)
		{
			$cache = Main\Application::getInstance()->getManagedCache();
			$cache->clean("b_option");
		}

		$con = Main\Application::getConnection();
		$sqlHelper = $con->getSqlHelper();

		$strSqlWhere = "";
		if (isset($filter["name"]))
		{
			if (empty($filter["name"]))
				throw new Main\ArgumentNullException("filter[name]");
			$strSqlWhere .= " AND NAME = '".$sqlHelper->forSql($filter["name"])."' ";
		}
		if (isset($filter["site_id"]))
			$strSqlWhere .= " AND SITE_ID ".(($filter["site_id"] == "") ? "IS NULL" : "= '".$sqlHelper->forSql($filter["site_id"], 2)."'");

		if ($moduleId == "main")
		{
			$con->queryExecute(
				"DELETE FROM b_option ".
				"WHERE MODULE_ID = 'main' ".
				"   AND NAME NOT LIKE '~%' ".
				"	AND NAME NOT IN ('crc_code', 'admin_passwordh', 'server_uniq_id','PARAM_MAX_SITES', 'PARAM_MAX_USERS') ".
				$strSqlWhere
			);
		}
		else
		{
			$con->queryExecute(
				"DELETE FROM b_option ".
				"WHERE MODULE_ID = '".$sqlHelper->forSql($moduleId)."' ".
				"   AND NAME <> '~bsm_stop_date' ".
				$strSqlWhere
			);
		}

		if (isset($filter["site_id"]))
		{
			$siteKey = $filter["site_id"] == "" ? "-" : $filter["site_id"];
			if (!isset($filter["name"]))
				unset(self::$options[$siteKey][$moduleId]);
			else
				unset(self::$options[$siteKey][$moduleId][$filter["name"]]);
		}
		else
		{
			$arSites = array_keys(self::$options);
			foreach ($arSites as $s)
			{
				if (!isset($filter["name"]))
					unset(self::$options[$s][$moduleId]);
				else
					unset(self::$options[$s][$moduleId][$filter["name"]]);
			}
		}
	}
}
