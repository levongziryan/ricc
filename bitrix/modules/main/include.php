<?php
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2013 Bitrix
 */

require_once(substr(__FILE__, 0, strlen(__FILE__) - strlen("/include.php"))."/bx_root.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/start.php");

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/virtual_io.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/virtual_file.php");


$application = \Bitrix\Main\Application::getInstance();
$application->initializeExtendedKernel(array(
	"get" => $_GET,
	"post" => $_POST,
	"files" => $_FILES,
	"cookie" => $_COOKIE,
	"server" => $_SERVER,
	"env" => $_ENV
));

//define global application object
$GLOBALS["APPLICATION"] = new CMain;

if(defined("SITE_ID"))
	define("LANG", SITE_ID);

if(defined("LANG"))
{
	if(defined("ADMIN_SECTION") && ADMIN_SECTION===true)
		$db_lang = CLangAdmin::GetByID(LANG);
	else
		$db_lang = CLang::GetByID(LANG);

	$arLang = $db_lang->Fetch();

	if(!$arLang)
	{
		throw new \Bitrix\Main\SystemException("Incorrect site: ".LANG.".");
	}
}
else
{
	$arLang = $GLOBALS["APPLICATION"]->GetLang();
	define("LANG", $arLang["LID"]);
}

$lang = $arLang["LID"];
if (!defined("SITE_ID"))
	define("SITE_ID", $arLang["LID"]);
define("SITE_DIR", $arLang["DIR"]);
define("SITE_SERVER_NAME", $arLang["SERVER_NAME"]);
define("SITE_CHARSET", $arLang["CHARSET"]);
define("FORMAT_DATE", $arLang["FORMAT_DATE"]);
define("FORMAT_DATETIME", $arLang["FORMAT_DATETIME"]);
define("LANG_DIR", $arLang["DIR"]);
define("LANG_CHARSET", $arLang["CHARSET"]);
define("LANG_ADMIN_LID", $arLang["LANGUAGE_ID"]);
define("LANGUAGE_ID", $arLang["LANGUAGE_ID"]);

$context = $application->getContext();
$context->setLanguage(LANGUAGE_ID);
$context->setCulture(new \Bitrix\Main\Context\Culture($arLang));

$request = $context->getRequest();
if (!$request->isAdminSection())
{
	$context->setSite(SITE_ID);
}

$application->start();

$GLOBALS["APPLICATION"]->reinitPath();

if (!defined("POST_FORM_ACTION_URI"))
{
	define("POST_FORM_ACTION_URI", htmlspecialcharsbx(GetRequestUri()));
}

$GLOBALS["MESS"] = array();
$GLOBALS["ALL_LANG_FILES"] = array();
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/tools.php");
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/classes/general/database.php");
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/classes/general/main.php");
IncludeModuleLangFile(__FILE__);

error_reporting(COption::GetOptionInt("main", "error_reporting", E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR|E_PARSE) & ~E_STRICT & ~E_DEPRECATED);

if(!defined("BX_COMP_MANAGED_CACHE") && COption::GetOptionString("main", "component_managed_cache_on", "Y") <> "N")
{
	define("BX_COMP_MANAGED_CACHE", true);
}

require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/filter_tools.php");
require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/ajax_tools.php");

/*ZDUyZmZNjg2YmNmYjZiNzE1YTk5YzkzMWY5Nzc1OGIwNzQxODA=*/$GLOBALS['_____1358592748']= array(base64_decode('R2V0TW9kdWxl'.'RXZ'.'l'.'bnRz'),base64_decode('RXhlY'.'3V0ZU1vZH'.'Vs'.'ZUV2ZW'.'50RX'.'g='));$GLOBALS['____1626057602']= array(base64_decode('Z'.'G'.'Vm'.'a'.'W5l'),base64_decode('c3RybG'.'Vu'),base64_decode('YmFzZTY'.'0'.'X2Rl'.'Y29k'.'ZQ='.'='),base64_decode('dW5zZX'.'JpYWx'.'pe'.'m'.'U='),base64_decode('aXNfYX'.'JyYXk='),base64_decode(''.'Y291bnQ='),base64_decode('a'.'W5fY'.'X'.'JyY'.'X'.'k='),base64_decode('c'.'2VyaWF'.'saX'.'p'.'l'),base64_decode(''.'Ym'.'FzZT'.'Y'.'0X2VuY29kZQ=='),base64_decode('c'.'3'.'RybG'.'Vu'),base64_decode('YXJy'.'YX'.'lfa'.'2V'.'5X2V4'.'aXN0'.'cw=='),base64_decode('YXJyYX'.'lf'.'a2V5'.'X'.'2V4aX'.'N0cw=='),base64_decode('bWt0aW1'.'l'),base64_decode('ZGF0ZQ'.'='.'='),base64_decode(''.'Z'.'GF0ZQ=='),base64_decode(''.'YXJy'.'YXlfa2V5X2'.'V4aXN'.'0cw'.'=='),base64_decode('c3R'.'y'.'b'.'GVu'),base64_decode(''.'YXJ'.'yYXlfa2V'.'5X'.'2V4aXN'.'0'.'cw=='),base64_decode(''.'c3RybGVu'),base64_decode('YXJyYXlfa2V5X'.'2'.'V4aXN0cw='.'='),base64_decode('YXJyYXlfa'.'2V'.'5X'.'2V4a'.'XN0cw=='),base64_decode('bWt'.'0aW1l'),base64_decode('Z'.'GF'.'0ZQ=='),base64_decode(''.'Z'.'GF0'.'ZQ=='),base64_decode(''.'bWV0a'.'G9kX'.'2V4'.'aXN0cw'.'=='),base64_decode(''.'Y'.'2Fsb'.'F9'.'1c2VyX2'.'Z1bmNfYXJyY'.'Xk='),base64_decode('c3RybG'.'Vu'),base64_decode('YXJyYXl'.'fa2V5X'.'2V4aX'.'N0cw=='),base64_decode('YXJyYXlfa2V5X2V4aXN0cw='.'='),base64_decode('c2Vy'.'aWFsaXp'.'l'),base64_decode('YmFzZTY0'.'X'.'2V'.'u'.'Y'.'29kZQ=='),base64_decode('c3Ry'.'bGVu'),base64_decode(''.'Y'.'XJyYX'.'lfa2V5'.'X2'.'V4aX'.'N'.'0'.'cw=='),base64_decode('YX'.'J'.'yYXlfa2V5X2V4a'.'XN0'.'cw=='),base64_decode('Y'.'XJyYXlfa2V5X2'.'V4'.'aXN0cw='.'='),base64_decode('aXNf'.'YXJy'.'YXk='),base64_decode('YXJyYXlfa2'.'V5X2V4aXN'.'0cw=='),base64_decode('c2VyaW'.'FsaX'.'pl'),base64_decode('YmFzZ'.'TY0'.'X2'.'V'.'uY'.'29kZQ=='),base64_decode('Y'.'XJyYXlfa2'.'V5X2V4'.'aXN0'.'c'.'w=='),base64_decode('YXJ'.'yYXlfa2V5X2'.'V4'.'aXN0'.'cw'.'=='),base64_decode('c2VyaW'.'Fsa'.'Xpl'),base64_decode(''.'YmFzZT'.'Y0X2VuY29'.'k'.'ZQ=='),base64_decode('aX'.'NfYXJyYXk='),base64_decode('a'.'XNfYXJyYXk='),base64_decode('aW5f'.'YXJy'.'YXk='),base64_decode('YXJy'.'YX'.'lfa2'.'V5X2V4aXN0'.'cw=='),base64_decode('aW5f'.'YXJyYXk='),base64_decode('b'.'W'.'t0aW'.'1'.'l'),base64_decode('ZGF0ZQ=='),base64_decode('ZGF0ZQ='.'='),base64_decode(''.'ZGF0ZQ=='),base64_decode(''.'b'.'W'.'t0aW1l'),base64_decode('ZG'.'F0ZQ=='),base64_decode('ZGF0ZQ=='),base64_decode(''.'aW5fYXJyY'.'Xk='),base64_decode(''.'YX'.'J'.'y'.'Y'.'Xlfa2V5X'.'2'.'V4aXN'.'0cw'.'=='),base64_decode('YXJyY'.'Xlfa2V5X2V4aXN0'.'cw=='),base64_decode(''.'c2Vy'.'aWF'.'sa'.'Xpl'),base64_decode('YmFzZTY0'.'X2VuY29kZQ=='),base64_decode('YXJy'.'YXlfa2V5X'.'2V4'.'a'.'XN0cw'.'=='),base64_decode('aW50dmFs'),base64_decode(''.'dGltZQ'.'=='),base64_decode('YXJy'.'Y'.'Xlfa2V5'.'X2V4a'.'X'.'N0'.'cw=='),base64_decode('Z'.'mlsZV9'.'leGlzd'.'HM='),base64_decode('c3'.'RyX'.'3JlcGxhY2U='),base64_decode('Y2xh'.'c3'.'N'.'f'.'ZXhpc3'.'Rz'),base64_decode('ZGVm'.'aW5l'));if(!function_exists(__NAMESPACE__.'\\___806067918')){function ___806067918($_1829819348){static $_1738406928= false; if($_1738406928 == false) $_1738406928=array(''.'S'.'U5UUkFORVR'.'fRUR'.'JVEl'.'P'.'Tg==','W'.'Q==','bW'.'Fpbg==','fm'.'Nw'.'Zl9tYXBfd'.'mFsdWU'.'=','','ZQ==','Zg==','ZQ==',''.'Rg==',''.'WA==','Zg==',''.'bWF'.'pb'.'g==','f'.'mNwZ'.'l9tYXBfdmFsd'.'W'.'U=','UG9ydGFs','Rg'.'==','ZQ='.'=','Z'.'Q==','WA==','R'.'g==','RA'.'==','R'.'A==',''.'b'.'Q==','ZA='.'=',''.'WQ==','Zg'.'==','Zg==','Z'.'g==','Zg==','UG9ydGF'.'s','Rg==','Z'.'Q==','ZQ==','WA==',''.'R'.'g==',''.'RA==','RA='.'=','bQ==','Z'.'A==','WQ='.'=','bWFpbg==','T24=',''.'U2V0dGl'.'uZ3NDa'.'G'.'FuZ2U=','Zg==','Zg'.'==','Zg==','Zg'.'='.'=','b'.'W'.'Fpb'.'g==','f'.'mNwZl9tYX'.'B'.'f'.'dmFsdWU'.'=',''.'Z'.'Q==','ZQ='.'=','ZQ'.'==','RA'.'==','ZQ==','ZQ==','Zg='.'=','Zg==','Zg'.'==','ZQ'.'==','bWFpbg='.'=','fmNwZl9tY'.'X'.'B'.'f'.'dmFsdWU=','ZQ==','Zg==','Z'.'g==','Z'.'g='.'=','Zg==','b'.'WFpbg='.'=','fmN'.'w'.'Z'.'l9tYX'.'BfdmFsdWU=','ZQ==','Zg='.'=','UG9ydGFs',''.'UG9'.'y'.'dGFs',''.'ZQ==','ZQ==','UG9yd'.'GFs','Rg'.'==','W'.'A==',''.'Rg==',''.'RA==',''.'ZQ==','ZQ==','RA==',''.'b'.'Q'.'==','ZA'.'==',''.'WQ'.'==','ZQ==','WA==','ZQ'.'='.'=',''.'Rg==','ZQ='.'=','R'.'A='.'=','Zg==','ZQ==','RA==',''.'ZQ'.'==','bQ'.'==',''.'Z'.'A==','WQ==','Z'.'g='.'=','Zg='.'=',''.'Zg==','Zg==',''.'Zg==','Z'.'g==','Zg==','Zg==','bWFpbg==',''.'fmN'.'wZ'.'l9t'.'YXBfdmFsdWU=',''.'ZQ==','ZQ==','UG9ydG'.'F'.'s','Rg='.'=',''.'WA==','VFlQRQ==','RE'.'F'.'UR'.'Q==','RkVBV'.'FV'.'SRVM=','RVhQSVJFR'.'A==','VFlQ'.'RQ==','RA='.'=','VFJZ'.'X'.'0R'.'B'.'WV'.'NfQ09'.'VT'.'lQ=',''.'R'.'EFURQ'.'==','VFJZ'.'X0'.'RBW'.'VNfQ'.'09V'.'TlQ=','RVhQ'.'SVJ'.'FRA==','RkVB'.'VFVS'.'RVM=','Zg'.'==','Zg==','R'.'E'.'9D'.'VU1FTl'.'R'.'fU'.'k'.'9PV'.'A'.'='.'=','L2JpdHJpeC9tb2R1'.'bG'.'VzLw='.'=','L2'.'luc'.'3'.'R'.'hbGw'.'vaW5kZXgucGh'.'w',''.'Lg='.'=','X'.'w='.'=','c'.'2V'.'hc'.'mNo','Tg==','','','QUNUSV'.'ZF',''.'WQ==','c2'.'9jaWFs'.'b'.'mV0d29yaw='.'=','YWxs'.'b3df'.'ZnJpZWx'.'kcw==','WQ='.'=','SUQ=','c29jaWFsb'.'mV0'.'d29'.'yaw==','YWxs'.'b3df'.'Z'.'n'.'J'.'pZW'.'xkcw==','SUQ'.'=',''.'c29'.'jaWF'.'sb'.'mV0d'.'29yaw==','YWxsb3d'.'fZnJ'.'pZWx'.'k'.'cw==',''.'Tg==','','','QU'.'NUSV'.'Z'.'F',''.'WQ='.'=','c29'.'jaWFsbmV0d29yaw==',''.'YWx'.'sb3'.'dfbW'.'l'.'jcm9ibG9'.'n'.'X3V'.'zZ'.'XI=','WQ==','SUQ=',''.'c29'.'jaWFsb'.'m'.'V0d29ya'.'w'.'==','Y'.'Wxsb3df'.'bWljcm'.'9ibG9'.'nX3V'.'zZX'.'I=','SU'.'Q'.'=','c'.'29'.'jaWF'.'s'.'b'.'mV'.'0d'.'29ya'.'w==','YWxsb3dfbWljcm9'.'i'.'b'.'G9'.'n'.'X'.'3Vz'.'ZXI=','c'.'29jaW'.'Fs'.'bmV0d'.'29yaw==','YW'.'xsb'.'3dfbWlj'.'cm9ibG9nX2dyb3Vw','WQ='.'=','S'.'U'.'Q=','c29ja'.'WFsb'.'m'.'V0'.'d29yaw'.'==','YWxsb3dfbWljc'.'m9ibG9nX2dyb3V'.'w','SUQ=','c29jaWFsbmV0'.'d29yaw==',''.'YWx'.'sb3dfb'.'Wlj'.'cm9ib'.'G'.'9'.'nX2dy'.'b3Vw',''.'Tg==','','','QUNUSVZF','W'.'Q='.'=','c29ja'.'WFsbmV'.'0'.'d29yaw==','YWx'.'s'.'b3df'.'ZmlsZXN'.'fdXNlcg==','W'.'Q==','SUQ=','c29jaWFsbmV0d29'.'y'.'aw==','YWxsb3dfZmlsZXNf'.'dXNlcg='.'=','SUQ=','c'.'29j'.'aWF'.'sbm'.'V0d'.'2'.'9yaw==','YWxs'.'b3dfZmlsZXN'.'f'.'d'.'X'.'Nl'.'c'.'g==',''.'T'.'g='.'=','','','QUN'.'U'.'SVZF',''.'W'.'Q='.'=','c2'.'9jaW'.'F'.'sbmV0d29y'.'aw'.'==','YWx'.'sb3df'.'Y'.'mxvZ1'.'9'.'1c2Vy','WQ==','SUQ'.'=','c'.'29'.'jaWF'.'sb'.'mV0d29'.'yaw'.'='.'=','YWxsb3dfYmxv'.'Z191c2'.'Vy','SUQ=','c29'.'jaW'.'F'.'s'.'bmV0d29yaw'.'==','Y'.'Wx'.'sb3'.'dfYm'.'xvZ19'.'1c2V'.'y','Tg'.'==','','','QUN'.'US'.'V'.'ZF',''.'WQ==',''.'c29jaWFsbmV'.'0d'.'29'.'yaw='.'=','Y'.'Wxsb'.'3dfcGhvdG9fdXN'.'lcg==','W'.'Q==',''.'S'.'UQ'.'=','c29jaWFsbmV0d'.'2'.'9yaw'.'==','YW'.'x'.'sb'.'3dfc'.'G'.'hvdG9fd'.'XNlcg==','SUQ=','c29jaWF'.'sbmV'.'0d29yaw==','YWxsb3dfc'.'Ghvd'.'G9fdX'.'Nl'.'cg==','Tg==','','','QUNUSVZF','WQ==','c29j'.'aWFsbmV0d29ya'.'w==','YWxsb'.'3dfZm'.'9y'.'dW1'.'fd'.'XNl'.'cg'.'==','WQ='.'=','SU'.'Q'.'=','c2'.'9'.'ja'.'W'.'FsbmV0d2'.'9'.'y'.'a'.'w==',''.'YWxsb3df'.'Zm'.'9ydW1fd'.'X'.'N'.'lcg==','SUQ=','c2'.'9jaWF'.'sb'.'mV0'.'d29'.'yaw'.'==','YWxs'.'b3dfZm9y'.'d'.'W1fdXNl'.'cg'.'='.'=',''.'Tg==','','','QUNUSVZF','WQ==',''.'c'.'29jaW'.'Fsbm'.'V0'.'d29ya'.'w'.'==','Y'.'Wxsb'.'3dfd'.'GF'.'za3NfdXNlcg==','WQ==','SU'.'Q=','c2'.'9jaWFsbm'.'V0d'.'2'.'9ya'.'w==','YWxsb3dfdGFz'.'a3Nfd'.'XNlc'.'g==','SUQ'.'=','c29jaWFsbmV0d29y'.'aw='.'=','YWxsb'.'3dfdGF'.'z'.'a3NfdX'.'Nlcg==','c'.'29ja'.'W'.'F'.'sbmV0'.'d29yaw==','YWxsb'.'3dfd'.'GF'.'za3NfZ3JvdXA'.'=','WQ==',''.'SUQ=','c29ja'.'WFsbmV'.'0d29ya'.'w='.'=','YWxsb'.'3dfd'.'GFza3NfZ3JvdXA=','SUQ=','c'.'29ja'.'WFsbm'.'V'.'0'.'d29'.'yaw'.'==','YWxsb3d'.'f'.'dG'.'Fza3N'.'fZ'.'3JvdXA=',''.'dGFza3M=','T'.'g==','','','QU'.'NU'.'S'.'V'.'ZF','WQ==','c29j'.'a'.'WFsbmV0d29yaw==','YWxsb3dfY2F'.'sZW5kYX'.'Jfd'.'XNlcg==',''.'WQ==','SUQ=','c29jaWFsbmV0d29yaw'.'='.'=','Y'.'Wxsb'.'3'.'d'.'f'.'Y2'.'FsZW5kYXJfd'.'XNl'.'c'.'g==','SUQ=','c29j'.'aWFs'.'bmV0d29yaw==','YWxsb3dfY2FsZ'.'W5'.'kYX'.'JfdXNl'.'cg==','c2'.'9ja'.'WFsbmV0'.'d2'.'9ya'.'w='.'=','YWx'.'sb3df'.'Y2FsZ'.'W5kYXJ'.'fZ3Jv'.'dXA=','WQ'.'==','SUQ=','c29ja'.'WFsbmV0'.'d'.'29yaw'.'==','YW'.'xsb3dfY'.'2F'.'sZW5k'.'YXJfZ'.'3J'.'vd'.'X'.'A=',''.'S'.'UQ=',''.'c29jaWFsbmV0d2'.'9yaw==','YWxsb3dfY2'.'F'.'sZ'.'W'.'5kYXJ'.'fZ3JvdX'.'A=','Y2FsZW5kYXI=','QUNUSVZF','W'.'Q='.'=','Tg='.'=','Z'.'Xh0cmFuZXQ'.'=','a'.'WJ'.'sb2N'.'r',''.'T25BZnRlcklCbG9ja'.'0VsZW1lb'.'nR'.'VcGRhdGU=','aW50c'.'mFuZXQ=','Q0ludHJhbmV0RXZ'.'l'.'bn'.'RI'.'YW'.'5k'.'bGVy'.'cw==','U1BS'.'Z'.'Wdpc3Rl'.'cl'.'VwZGF0'.'ZWR'.'JdGVt','Q0l'.'u'.'dHJhbmV0U2h'.'h'.'cmVwb'.'2ludDo6QWd'.'lbnRMaXN'.'0cygpOw==','aW50cmFu'.'ZXQ=','Tg==','Q0l'.'udHJhbmV0U2hhcm'.'Vwb2lu'.'dD'.'o6'.'QWdlbn'.'RRdWV1ZSgp'.'Ow==','aW5'.'0cmFuZXQ=','Tg==','Q0ludH'.'Jhb'.'mV'.'0'.'U2'.'hhc'.'mVwb2l'.'udDo'.'6QWd'.'l'.'b'.'nRVcGR'.'h'.'dGUoK'.'Ts=','aW50cmFuZXQ=','Tg'.'==','aWJsb2Nr',''.'T25'.'BZnRlcklCbG9ja0VsZW1lbn'.'RBZGQ=','aW50cm'.'F'.'uZXQ'.'=','Q0lud'.'HJhbm'.'V0RXZlbnRIYW5kbGVycw'.'==',''.'U1B'.'S'.'ZW'.'d'.'p'.'c3RlclVwZG'.'F0Z'.'W'.'R'.'JdG'.'Vt',''.'a'.'W'.'Jsb2'.'Nr','T25B'.'Zn'.'R'.'lcklCbG9ja0VsZW1'.'lbnR'.'VcG'.'RhdGU=','aW'.'50cmFuZXQ=','Q0l'.'udHJhbmV'.'0'.'RXZ'.'lbnRIYW5kbGVy'.'cw'.'==','U'.'1BSZWdpc3Rl'.'clVwZ'.'GF0ZWRJdGVt','Q0'.'ludHJhbmV0U2hhcmV'.'w'.'b2ludDo6QWdl'.'bnRMaXN0cygpOw'.'==','aW50'.'cmFuZX'.'Q=','Q0'.'lud'.'HJhb'.'mV0'.'U2hhcm'.'Vwb2l'.'u'.'dDo6Q'.'WdlbnRRd'.'WV1'.'ZSgpOw==','aW50cmFuZ'.'XQ=','Q0lud'.'HJ'.'hbmV0'.'U'.'2hh'.'cm'.'Vwb'.'2l'.'udDo6QWdlb'.'nRVcGRhdGUoKTs=','aW5'.'0cmF'.'uZXQ'.'=','Y3Jt','bWFpbg==',''.'T25CZWZvcmVQc'.'m9sb2'.'c=','bW'.'Fpbg==','Q1d'.'pemF'.'y'.'ZFNvbFBhbmV'.'sSW50cmF'.'uZXQ=','U2h'.'vd1B'.'hb'.'mVs',''.'L21vZHV'.'sZ'.'XMvaW'.'50cmFuZXQvc'.'GFuZWxfYn'.'V0dG9u'.'L'.'nBo'.'cA==','RU5D'.'T'.'0RF','WQ==');return base64_decode($_1738406928[$_1829819348]);}};$GLOBALS['____1626057602'][0](___806067918(0), ___806067918(1));class CBXFeatures{ private static $_791858012= 30; private static $_258687590= array( "Portal" => array( "CompanyCalendar", "CompanyPhoto", "CompanyVideo", "CompanyCareer", "StaffChanges", "StaffAbsence", "CommonDocuments", "MeetingRoomBookingSystem", "Wiki", "Learning", "Vote", "WebLink", "Subscribe", "Friends", "PersonalFiles", "PersonalBlog", "PersonalPhoto", "PersonalForum", "Blog", "Forum", "Gallery", "Board", "MicroBlog", "WebMessenger",), "Communications" => array( "Tasks", "Calendar", "Workgroups", "Jabber", "VideoConference", "Extranet", "SMTP", "Requests", "DAV", "intranet_sharepoint", "timeman", "Idea", "Meeting", "EventList", "Salary", "XDImport",), "Enterprise" => array( "BizProc", "Lists", "Support", "Analytics", "crm", "Controller",), "Holding" => array( "Cluster", "MultiSites",),); private static $_112389992= false; private static $_577213727= false; private static function __307619417(){ if(self::$_112389992 == false){ self::$_112389992= array(); foreach(self::$_258687590 as $_559201533 => $_1958255881){ foreach($_1958255881 as $_1522881513) self::$_112389992[$_1522881513]= $_559201533;}} if(self::$_577213727 == false){ self::$_577213727= array(); $_1579745955= COption::GetOptionString(___806067918(2), ___806067918(3), ___806067918(4)); if($GLOBALS['____1626057602'][1]($_1579745955)> min(160,0,53.333333333333)){ $_1579745955= $GLOBALS['____1626057602'][2]($_1579745955); self::$_577213727= $GLOBALS['____1626057602'][3]($_1579745955); if(!$GLOBALS['____1626057602'][4](self::$_577213727)) self::$_577213727= array();} if($GLOBALS['____1626057602'][5](self::$_577213727) <=(184*2-368)) self::$_577213727= array(___806067918(5) => array(), ___806067918(6) => array());}} public static function InitiateEditionsSettings($_2034845648){ self::__307619417(); $_548075963= array(); foreach(self::$_258687590 as $_559201533 => $_1958255881){ $_1203081781= $GLOBALS['____1626057602'][6]($_559201533, $_2034845648); self::$_577213727[___806067918(7)][$_559201533]=($_1203081781? array(___806067918(8)): array(___806067918(9))); foreach($_1958255881 as $_1522881513){ self::$_577213727[___806067918(10)][$_1522881513]= $_1203081781; if(!$_1203081781) $_548075963[]= array($_1522881513, false);}} $_21359702= $GLOBALS['____1626057602'][7](self::$_577213727); $_21359702= $GLOBALS['____1626057602'][8]($_21359702); COption::SetOptionString(___806067918(11), ___806067918(12), $_21359702); foreach($_548075963 as $_990263936) self::__945882755($_990263936[(1420/2-710)], $_990263936[round(0+0.5+0.5)]);} public static function IsFeatureEnabled($_1522881513){ if($GLOBALS['____1626057602'][9]($_1522881513) <= 0) return true; self::__307619417(); if(!$GLOBALS['____1626057602'][10]($_1522881513, self::$_112389992)) return true; if(self::$_112389992[$_1522881513] == ___806067918(13)) $_747947007= array(___806067918(14)); elseif($GLOBALS['____1626057602'][11](self::$_112389992[$_1522881513], self::$_577213727[___806067918(15)])) $_747947007= self::$_577213727[___806067918(16)][self::$_112389992[$_1522881513]]; else $_747947007= array(___806067918(17)); if($_747947007[(850-2*425)] != ___806067918(18) && $_747947007[(984-2*492)] != ___806067918(19)){ return false;} elseif($_747947007[(922-2*461)] == ___806067918(20)){ if($_747947007[round(0+0.2+0.2+0.2+0.2+0.2)]< $GLOBALS['____1626057602'][12]((1292/2-646),(916-2*458),(1124/2-562), Date(___806067918(21)), $GLOBALS['____1626057602'][13](___806067918(22))- self::$_791858012, $GLOBALS['____1626057602'][14](___806067918(23)))){ if(!isset($_747947007[round(0+0.5+0.5+0.5+0.5)]) ||!$_747947007[round(0+0.4+0.4+0.4+0.4+0.4)]) self::__1557431601(self::$_112389992[$_1522881513]); return false;}} return!$GLOBALS['____1626057602'][15]($_1522881513, self::$_577213727[___806067918(24)]) || self::$_577213727[___806067918(25)][$_1522881513];} public static function IsFeatureInstalled($_1522881513){ if($GLOBALS['____1626057602'][16]($_1522881513) <= 0) return true; self::__307619417(); return($GLOBALS['____1626057602'][17]($_1522881513, self::$_577213727[___806067918(26)]) && self::$_577213727[___806067918(27)][$_1522881513]);} public static function IsFeatureEditable($_1522881513){ if($GLOBALS['____1626057602'][18]($_1522881513) <= 0) return true; self::__307619417(); if(!$GLOBALS['____1626057602'][19]($_1522881513, self::$_112389992)) return true; if(self::$_112389992[$_1522881513] == ___806067918(28)) $_747947007= array(___806067918(29)); elseif($GLOBALS['____1626057602'][20](self::$_112389992[$_1522881513], self::$_577213727[___806067918(30)])) $_747947007= self::$_577213727[___806067918(31)][self::$_112389992[$_1522881513]]; else $_747947007= array(___806067918(32)); if($_747947007[(956-2*478)] != ___806067918(33) && $_747947007[(1260/2-630)] != ___806067918(34)){ return false;} elseif($_747947007[(1256/2-628)] == ___806067918(35)){ if($_747947007[round(0+0.2+0.2+0.2+0.2+0.2)]< $GLOBALS['____1626057602'][21]((848-2*424),(1004/2-502), min(198,0,66), Date(___806067918(36)), $GLOBALS['____1626057602'][22](___806067918(37))- self::$_791858012, $GLOBALS['____1626057602'][23](___806067918(38)))){ if(!isset($_747947007[round(0+0.4+0.4+0.4+0.4+0.4)]) ||!$_747947007[round(0+0.4+0.4+0.4+0.4+0.4)]) self::__1557431601(self::$_112389992[$_1522881513]); return false;}} return true;} private static function __945882755($_1522881513, $_1376677878){ if($GLOBALS['____1626057602'][24]("CBXFeatures", "On".$_1522881513."SettingsChange")) $GLOBALS['____1626057602'][25](array("CBXFeatures", "On".$_1522881513."SettingsChange"), array($_1522881513, $_1376677878)); $_1151697505= $GLOBALS['_____1358592748'][0](___806067918(39), ___806067918(40).$_1522881513.___806067918(41)); while($_453420385= $_1151697505->Fetch()) $GLOBALS['_____1358592748'][1]($_453420385, array($_1522881513, $_1376677878));} public static function SetFeatureEnabled($_1522881513, $_1376677878= true, $_1212635205= true){ if($GLOBALS['____1626057602'][26]($_1522881513) <= 0) return; if(!self::IsFeatureEditable($_1522881513)) $_1376677878= false; $_1376677878=($_1376677878? true: false); self::__307619417(); $_521799489=(!$GLOBALS['____1626057602'][27]($_1522881513, self::$_577213727[___806067918(42)]) && $_1376677878 || $GLOBALS['____1626057602'][28]($_1522881513, self::$_577213727[___806067918(43)]) && $_1376677878 != self::$_577213727[___806067918(44)][$_1522881513]); self::$_577213727[___806067918(45)][$_1522881513]= $_1376677878; $_21359702= $GLOBALS['____1626057602'][29](self::$_577213727); $_21359702= $GLOBALS['____1626057602'][30]($_21359702); COption::SetOptionString(___806067918(46), ___806067918(47), $_21359702); if($_521799489 && $_1212635205) self::__945882755($_1522881513, $_1376677878);} private static function __1557431601($_559201533){ if($GLOBALS['____1626057602'][31]($_559201533) <= 0 || $_559201533 == "Portal") return; self::__307619417(); if(!$GLOBALS['____1626057602'][32]($_559201533, self::$_577213727[___806067918(48)]) || $GLOBALS['____1626057602'][33]($_559201533, self::$_577213727[___806067918(49)]) && self::$_577213727[___806067918(50)][$_559201533][min(36,0,12)] != ___806067918(51)) return; if(isset(self::$_577213727[___806067918(52)][$_559201533][round(0+0.4+0.4+0.4+0.4+0.4)]) && self::$_577213727[___806067918(53)][$_559201533][round(0+1+1)]) return; $_548075963= array(); if($GLOBALS['____1626057602'][34]($_559201533, self::$_258687590) && $GLOBALS['____1626057602'][35](self::$_258687590[$_559201533])){ foreach(self::$_258687590[$_559201533] as $_1522881513){ if($GLOBALS['____1626057602'][36]($_1522881513, self::$_577213727[___806067918(54)]) && self::$_577213727[___806067918(55)][$_1522881513]){ self::$_577213727[___806067918(56)][$_1522881513]= false; $_548075963[]= array($_1522881513, false);}} self::$_577213727[___806067918(57)][$_559201533][round(0+0.5+0.5+0.5+0.5)]= true;} $_21359702= $GLOBALS['____1626057602'][37](self::$_577213727); $_21359702= $GLOBALS['____1626057602'][38]($_21359702); COption::SetOptionString(___806067918(58), ___806067918(59), $_21359702); foreach($_548075963 as $_990263936) self::__945882755($_990263936[(130*2-260)], $_990263936[round(0+0.2+0.2+0.2+0.2+0.2)]);} public static function ModifyFeaturesSettings($_2034845648, $_1958255881){ self::__307619417(); foreach($_2034845648 as $_559201533 => $_382073846) self::$_577213727[___806067918(60)][$_559201533]= $_382073846; $_548075963= array(); foreach($_1958255881 as $_1522881513 => $_1376677878){ if(!$GLOBALS['____1626057602'][39]($_1522881513, self::$_577213727[___806067918(61)]) && $_1376677878 || $GLOBALS['____1626057602'][40]($_1522881513, self::$_577213727[___806067918(62)]) && $_1376677878 != self::$_577213727[___806067918(63)][$_1522881513]) $_548075963[]= array($_1522881513, $_1376677878); self::$_577213727[___806067918(64)][$_1522881513]= $_1376677878;} $_21359702= $GLOBALS['____1626057602'][41](self::$_577213727); $_21359702= $GLOBALS['____1626057602'][42]($_21359702); COption::SetOptionString(___806067918(65), ___806067918(66), $_21359702); self::$_577213727= false; foreach($_548075963 as $_990263936) self::__945882755($_990263936[(229*2-458)], $_990263936[round(0+0.2+0.2+0.2+0.2+0.2)]);} public static function SaveFeaturesSettings($_404642384, $_295166930){ self::__307619417(); $_842427260= array(___806067918(67) => array(), ___806067918(68) => array()); if(!$GLOBALS['____1626057602'][43]($_404642384)) $_404642384= array(); if(!$GLOBALS['____1626057602'][44]($_295166930)) $_295166930= array(); if(!$GLOBALS['____1626057602'][45](___806067918(69), $_404642384)) $_404642384[]= ___806067918(70); foreach(self::$_258687590 as $_559201533 => $_1958255881){ if($GLOBALS['____1626057602'][46]($_559201533, self::$_577213727[___806067918(71)])) $_1874219481= self::$_577213727[___806067918(72)][$_559201533]; else $_1874219481=($_559201533 == ___806067918(73))? array(___806067918(74)): array(___806067918(75)); if($_1874219481[min(20,0,6.6666666666667)] == ___806067918(76) || $_1874219481[(182*2-364)] == ___806067918(77)){ $_842427260[___806067918(78)][$_559201533]= $_1874219481;} else{ if($GLOBALS['____1626057602'][47]($_559201533, $_404642384)) $_842427260[___806067918(79)][$_559201533]= array(___806067918(80), $GLOBALS['____1626057602'][48]((1092/2-546), min(100,0,33.333333333333), min(118,0,39.333333333333), $GLOBALS['____1626057602'][49](___806067918(81)), $GLOBALS['____1626057602'][50](___806067918(82)), $GLOBALS['____1626057602'][51](___806067918(83)))); else $_842427260[___806067918(84)][$_559201533]= array(___806067918(85));}} $_548075963= array(); foreach(self::$_112389992 as $_1522881513 => $_559201533){ if($_842427260[___806067918(86)][$_559201533][(1104/2-552)] != ___806067918(87) && $_842427260[___806067918(88)][$_559201533][(172*2-344)] != ___806067918(89)){ $_842427260[___806067918(90)][$_1522881513]= false;} else{ if($_842427260[___806067918(91)][$_559201533][(794-2*397)] == ___806067918(92) && $_842427260[___806067918(93)][$_559201533][round(0+0.25+0.25+0.25+0.25)]< $GLOBALS['____1626057602'][52]((250*2-500), min(238,0,79.333333333333),(137*2-274), Date(___806067918(94)), $GLOBALS['____1626057602'][53](___806067918(95))- self::$_791858012, $GLOBALS['____1626057602'][54](___806067918(96)))) $_842427260[___806067918(97)][$_1522881513]= false; else $_842427260[___806067918(98)][$_1522881513]= $GLOBALS['____1626057602'][55]($_1522881513, $_295166930); if(!$GLOBALS['____1626057602'][56]($_1522881513, self::$_577213727[___806067918(99)]) && $_842427260[___806067918(100)][$_1522881513] || $GLOBALS['____1626057602'][57]($_1522881513, self::$_577213727[___806067918(101)]) && $_842427260[___806067918(102)][$_1522881513] != self::$_577213727[___806067918(103)][$_1522881513]) $_548075963[]= array($_1522881513, $_842427260[___806067918(104)][$_1522881513]);}} $_21359702= $GLOBALS['____1626057602'][58]($_842427260); $_21359702= $GLOBALS['____1626057602'][59]($_21359702); COption::SetOptionString(___806067918(105), ___806067918(106), $_21359702); self::$_577213727= false; foreach($_548075963 as $_990263936) self::__945882755($_990263936[(1400/2-700)], $_990263936[round(0+0.2+0.2+0.2+0.2+0.2)]);} public static function GetFeaturesList(){ self::__307619417(); $_646143950= array(); foreach(self::$_258687590 as $_559201533 => $_1958255881){ if($GLOBALS['____1626057602'][60]($_559201533, self::$_577213727[___806067918(107)])) $_1874219481= self::$_577213727[___806067918(108)][$_559201533]; else $_1874219481=($_559201533 == ___806067918(109))? array(___806067918(110)): array(___806067918(111)); $_646143950[$_559201533]= array( ___806067918(112) => $_1874219481[(238*2-476)], ___806067918(113) => $_1874219481[round(0+0.33333333333333+0.33333333333333+0.33333333333333)], ___806067918(114) => array(),); $_646143950[$_559201533][___806067918(115)]= false; if($_646143950[$_559201533][___806067918(116)] == ___806067918(117)){ $_646143950[$_559201533][___806067918(118)]= $GLOBALS['____1626057602'][61](($GLOBALS['____1626057602'][62]()- $_646143950[$_559201533][___806067918(119)])/ round(0+28800+28800+28800)); if($_646143950[$_559201533][___806067918(120)]> self::$_791858012) $_646143950[$_559201533][___806067918(121)]= true;} foreach($_1958255881 as $_1522881513) $_646143950[$_559201533][___806067918(122)][$_1522881513]=(!$GLOBALS['____1626057602'][63]($_1522881513, self::$_577213727[___806067918(123)]) || self::$_577213727[___806067918(124)][$_1522881513]);} return $_646143950;} private static function __589573592($_1561736548, $_881389683){ if(IsModuleInstalled($_1561736548) == $_881389683) return true; $_752326864= $_SERVER[___806067918(125)].___806067918(126).$_1561736548.___806067918(127); if(!$GLOBALS['____1626057602'][64]($_752326864)) return false; include_once($_752326864); $_1963146767= $GLOBALS['____1626057602'][65](___806067918(128), ___806067918(129), $_1561736548); if(!$GLOBALS['____1626057602'][66]($_1963146767)) return false; $_1389883175= new $_1963146767; if($_881389683){ if(!$_1389883175->InstallDB()) return false; $_1389883175->InstallEvents(); if(!$_1389883175->InstallFiles()) return false;} else{ if(CModule::IncludeModule(___806067918(130))) CSearch::DeleteIndex($_1561736548); UnRegisterModule($_1561736548);     } return true;} private static function __418123908($_1522881513, $_1376677878){ self::__589573592("form", $_1376677878);} private static function __381981152($_1522881513, $_1376677878){ self::__589573592("learning", $_1376677878);} private static function __1675743186($_1522881513, $_1376677878){ self::__589573592("xmpp", $_1376677878);} private static function __2123357736($_1522881513, $_1376677878){ self::__589573592("video", $_1376677878);} private static function __2144285493($_1522881513, $_1376677878){ self::__589573592("bizprocdesigner", $_1376677878);} private static function __201251407($_1522881513, $_1376677878){ self::__589573592("lists", $_1376677878);} private static function __812213000($_1522881513, $_1376677878){ self::__589573592("wiki", $_1376677878);} private static function __12678773($_1522881513, $_1376677878){ self::__589573592("support", $_1376677878);} private static function __698957867($_1522881513, $_1376677878){ self::__589573592("controller", $_1376677878);} private static function __2018706405($_1522881513, $_1376677878){ self::__589573592("statistic", $_1376677878);} private static function __178274281($_1522881513, $_1376677878){ self::__589573592("vote", $_1376677878);} private static function __536554102($_1522881513, $_1376677878){ if($_1376677878) $_744015541= "Y"; else $_744015541= ___806067918(131); $_1147513629= CSite::GetList(($_1203081781= ___806067918(132)),($_2912837= ___806067918(133)), array(___806067918(134) => ___806067918(135))); while($_582957936= $_1147513629->Fetch()){ if(COption::GetOptionString(___806067918(136), ___806067918(137), ___806067918(138), $_582957936[___806067918(139)]) != $_744015541){ COption::SetOptionString(___806067918(140), ___806067918(141), $_744015541, false, $_582957936[___806067918(142)]); COption::SetOptionString(___806067918(143), ___806067918(144), $_744015541);}}} private static function __768329415($_1522881513, $_1376677878){ if($_1376677878) $_744015541= "Y"; else $_744015541= ___806067918(145); $_1147513629= CSite::GetList(($_1203081781= ___806067918(146)),($_2912837= ___806067918(147)), array(___806067918(148) => ___806067918(149))); while($_582957936= $_1147513629->Fetch()){ if(COption::GetOptionString(___806067918(150), ___806067918(151), ___806067918(152), $_582957936[___806067918(153)]) != $_744015541){ COption::SetOptionString(___806067918(154), ___806067918(155), $_744015541, false, $_582957936[___806067918(156)]); COption::SetOptionString(___806067918(157), ___806067918(158), $_744015541);} if(COption::GetOptionString(___806067918(159), ___806067918(160), ___806067918(161), $_582957936[___806067918(162)]) != $_744015541){ COption::SetOptionString(___806067918(163), ___806067918(164), $_744015541, false, $_582957936[___806067918(165)]); COption::SetOptionString(___806067918(166), ___806067918(167), $_744015541);}}} private static function __472345251($_1522881513, $_1376677878){ if($_1376677878) $_744015541= "Y"; else $_744015541= ___806067918(168); $_1147513629= CSite::GetList(($_1203081781= ___806067918(169)),($_2912837= ___806067918(170)), array(___806067918(171) => ___806067918(172))); while($_582957936= $_1147513629->Fetch()){ if(COption::GetOptionString(___806067918(173), ___806067918(174), ___806067918(175), $_582957936[___806067918(176)]) != $_744015541){ COption::SetOptionString(___806067918(177), ___806067918(178), $_744015541, false, $_582957936[___806067918(179)]); COption::SetOptionString(___806067918(180), ___806067918(181), $_744015541);}}} private static function __2092904945($_1522881513, $_1376677878){ if($_1376677878) $_744015541= "Y"; else $_744015541= ___806067918(182); $_1147513629= CSite::GetList(($_1203081781= ___806067918(183)),($_2912837= ___806067918(184)), array(___806067918(185) => ___806067918(186))); while($_582957936= $_1147513629->Fetch()){ if(COption::GetOptionString(___806067918(187), ___806067918(188), ___806067918(189), $_582957936[___806067918(190)]) != $_744015541){ COption::SetOptionString(___806067918(191), ___806067918(192), $_744015541, false, $_582957936[___806067918(193)]); COption::SetOptionString(___806067918(194), ___806067918(195), $_744015541);}}} private static function __1190752263($_1522881513, $_1376677878){ if($_1376677878) $_744015541= "Y"; else $_744015541= ___806067918(196); $_1147513629= CSite::GetList(($_1203081781= ___806067918(197)),($_2912837= ___806067918(198)), array(___806067918(199) => ___806067918(200))); while($_582957936= $_1147513629->Fetch()){ if(COption::GetOptionString(___806067918(201), ___806067918(202), ___806067918(203), $_582957936[___806067918(204)]) != $_744015541){ COption::SetOptionString(___806067918(205), ___806067918(206), $_744015541, false, $_582957936[___806067918(207)]); COption::SetOptionString(___806067918(208), ___806067918(209), $_744015541);}}} private static function __763991808($_1522881513, $_1376677878){ if($_1376677878) $_744015541= "Y"; else $_744015541= ___806067918(210); $_1147513629= CSite::GetList(($_1203081781= ___806067918(211)),($_2912837= ___806067918(212)), array(___806067918(213) => ___806067918(214))); while($_582957936= $_1147513629->Fetch()){ if(COption::GetOptionString(___806067918(215), ___806067918(216), ___806067918(217), $_582957936[___806067918(218)]) != $_744015541){ COption::SetOptionString(___806067918(219), ___806067918(220), $_744015541, false, $_582957936[___806067918(221)]); COption::SetOptionString(___806067918(222), ___806067918(223), $_744015541);}}} private static function __1809272686($_1522881513, $_1376677878){ if($_1376677878) $_744015541= "Y"; else $_744015541= ___806067918(224); $_1147513629= CSite::GetList(($_1203081781= ___806067918(225)),($_2912837= ___806067918(226)), array(___806067918(227) => ___806067918(228))); while($_582957936= $_1147513629->Fetch()){ if(COption::GetOptionString(___806067918(229), ___806067918(230), ___806067918(231), $_582957936[___806067918(232)]) != $_744015541){ COption::SetOptionString(___806067918(233), ___806067918(234), $_744015541, false, $_582957936[___806067918(235)]); COption::SetOptionString(___806067918(236), ___806067918(237), $_744015541);} if(COption::GetOptionString(___806067918(238), ___806067918(239), ___806067918(240), $_582957936[___806067918(241)]) != $_744015541){ COption::SetOptionString(___806067918(242), ___806067918(243), $_744015541, false, $_582957936[___806067918(244)]); COption::SetOptionString(___806067918(245), ___806067918(246), $_744015541);}} self::__589573592(___806067918(247), $_1376677878);} private static function __180887259($_1522881513, $_1376677878){ if($_1376677878) $_744015541= "Y"; else $_744015541= ___806067918(248); $_1147513629= CSite::GetList(($_1203081781= ___806067918(249)),($_2912837= ___806067918(250)), array(___806067918(251) => ___806067918(252))); while($_582957936= $_1147513629->Fetch()){ if(COption::GetOptionString(___806067918(253), ___806067918(254), ___806067918(255), $_582957936[___806067918(256)]) != $_744015541){ COption::SetOptionString(___806067918(257), ___806067918(258), $_744015541, false, $_582957936[___806067918(259)]); COption::SetOptionString(___806067918(260), ___806067918(261), $_744015541);} if(COption::GetOptionString(___806067918(262), ___806067918(263), ___806067918(264), $_582957936[___806067918(265)]) != $_744015541){ COption::SetOptionString(___806067918(266), ___806067918(267), $_744015541, false, $_582957936[___806067918(268)]); COption::SetOptionString(___806067918(269), ___806067918(270), $_744015541);}} self::__589573592(___806067918(271), $_1376677878);} private static function __653449005($_1522881513, $_1376677878){ self::__589573592("mail", $_1376677878);} private static function __1525081713($_1522881513, $_1376677878){ $_1331581713= COption::GetOptionString("extranet", "extranet_site", ""); if($_1331581713){ $_1372603048= new CSite; $_1372603048->Update($_1331581713, array(___806067918(272) =>($_1376677878? ___806067918(273): ___806067918(274))));} self::__589573592(___806067918(275), $_1376677878);} private static function __17073848($_1522881513, $_1376677878){ self::__589573592("dav", $_1376677878);} private static function __1982522843($_1522881513, $_1376677878){ self::__589573592("timeman", $_1376677878);} private static function __406625454($_1522881513, $_1376677878){ if($_1376677878){ RegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", "intranet", "CIntranetEventHandlers", "SPRegisterUpdatedItem"); RegisterModuleDependences(___806067918(276), ___806067918(277), ___806067918(278), ___806067918(279), ___806067918(280)); CAgent::AddAgent(___806067918(281), ___806067918(282), ___806067918(283), round(0+125+125+125+125)); CAgent::AddAgent(___806067918(284), ___806067918(285), ___806067918(286), round(0+75+75+75+75)); CAgent::AddAgent(___806067918(287), ___806067918(288), ___806067918(289), round(0+720+720+720+720+720));} else{ UnRegisterModuleDependences(___806067918(290), ___806067918(291), ___806067918(292), ___806067918(293), ___806067918(294)); UnRegisterModuleDependences(___806067918(295), ___806067918(296), ___806067918(297), ___806067918(298), ___806067918(299)); CAgent::RemoveAgent(___806067918(300), ___806067918(301)); CAgent::RemoveAgent(___806067918(302), ___806067918(303)); CAgent::RemoveAgent(___806067918(304), ___806067918(305));}} private static function __973720224($_1522881513, $_1376677878){ if($_1376677878) COption::SetOptionString("crm", "form_features", "Y"); self::__589573592(___806067918(306), $_1376677878);} private static function __256344665($_1522881513, $_1376677878){ self::__589573592("cluster", $_1376677878);} private static function __1417404287($_1522881513, $_1376677878){ if($_1376677878) RegisterModuleDependences("main", "OnBeforeProlog", "main", "CWizardSolPanelIntranet", "ShowPanel", 100, "/modules/intranet/panel_button.php"); else UnRegisterModuleDependences(___806067918(307), ___806067918(308), ___806067918(309), ___806067918(310), ___806067918(311), ___806067918(312));} private static function __2102918080($_1522881513, $_1376677878){ self::__589573592("idea", $_1376677878);} private static function __781000202($_1522881513, $_1376677878){ self::__589573592("meeting", $_1376677878);} private static function __473063169($_1522881513, $_1376677878){ self::__589573592("xdimport", $_1376677878);}} $GLOBALS['____1626057602'][67](___806067918(313), ___806067918(314));/**/			//Do not remove this

//component 2.0 template engines
$GLOBALS["arCustomTemplateEngines"] = array();

require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/classes/general/urlrewriter.php");

/**
 * Defined in dbconn.php
 * @param string $DBType
 */

\Bitrix\Main\Loader::registerAutoLoadClasses(
	"main",
	array(
		"CSiteTemplate" => "classes/general/site_template.php",
		"CBitrixComponent" => "classes/general/component.php",
		"CComponentEngine" => "classes/general/component_engine.php",
		"CComponentAjax" => "classes/general/component_ajax.php",
		"CBitrixComponentTemplate" => "classes/general/component_template.php",
		"CComponentUtil" => "classes/general/component_util.php",
		"CControllerClient" => "classes/general/controller_member.php",
		"PHPParser" => "classes/general/php_parser.php",
		"CDiskQuota" => "classes/".$DBType."/quota.php",
		"CEventLog" => "classes/general/event_log.php",
		"CEventMain" => "classes/general/event_log.php",
		"CAdminFileDialog" => "classes/general/file_dialog.php",
		"WLL_User" => "classes/general/liveid.php",
		"WLL_ConsentToken" => "classes/general/liveid.php",
		"WindowsLiveLogin" => "classes/general/liveid.php",
		"CAllFile" => "classes/general/file.php",
		"CFile" => "classes/".$DBType."/file.php",
		"CTempFile" => "classes/general/file_temp.php",
		"CFavorites" => "classes/".$DBType."/favorites.php",
		"CUserOptions" => "classes/general/user_options.php",
		"CGridOptions" => "classes/general/grids.php",
		"CUndo" => "/classes/general/undo.php",
		"CAutoSave" => "/classes/general/undo.php",
		"CRatings" => "classes/".$DBType."/ratings.php",
		"CRatingsComponentsMain" => "classes/".$DBType."/ratings_components.php",
		"CRatingRule" => "classes/general/rating_rule.php",
		"CRatingRulesMain" => "classes/".$DBType."/rating_rules.php",
		"CTopPanel" => "public/top_panel.php",
		"CEditArea" => "public/edit_area.php",
		"CComponentPanel" => "public/edit_area.php",
		"CTextParser" => "classes/general/textparser.php",
		"CPHPCacheFiles" => "classes/general/cache_files.php",
		"CDataXML" => "classes/general/xml.php",
		"CXMLFileStream" => "classes/general/xml.php",
		"CRsaProvider" => "classes/general/rsasecurity.php",
		"CRsaSecurity" => "classes/general/rsasecurity.php",
		"CRsaBcmathProvider" => "classes/general/rsabcmath.php",
		"CRsaOpensslProvider" => "classes/general/rsaopenssl.php",
		"CASNReader" => "classes/general/asn.php",
		"CBXShortUri" => "classes/".$DBType."/short_uri.php",
		"CFinder" => "classes/general/finder.php",
		"CAccess" => "classes/general/access.php",
		"CAuthProvider" => "classes/general/authproviders.php",
		"IProviderInterface" => "classes/general/authproviders.php",
		"CGroupAuthProvider" => "classes/general/authproviders.php",
		"CUserAuthProvider" => "classes/general/authproviders.php",
		"CTableSchema" => "classes/general/table_schema.php",
		"CCSVData" => "classes/general/csv_data.php",
		"CSmile" => "classes/general/smile.php",
		"CSmileGallery" => "classes/general/smile.php",
		"CSmileSet" => "classes/general/smile.php",
		"CGlobalCounter" => "classes/general/global_counter.php",
		"CUserCounter" => "classes/".$DBType."/user_counter.php",
		"CUserCounterPage" => "classes/".$DBType."/user_counter.php",
		"CHotKeys" => "classes/general/hot_keys.php",
		"CHotKeysCode" => "classes/general/hot_keys.php",
		"CBXSanitizer" => "classes/general/sanitizer.php",
		"CBXArchive" => "classes/general/archive.php",
		"CAdminNotify" => "classes/general/admin_notify.php",
		"CBXFavAdmMenu" => "classes/general/favorites.php",
		"CAdminInformer" => "classes/general/admin_informer.php",
		"CSiteCheckerTest" => "classes/general/site_checker.php",
		"CSqlUtil" => "classes/general/sql_util.php",
		"CFileUploader" => "classes/general/uploader.php",
		"LPA" => "classes/general/lpa.php",
		"CAdminFilter" => "interface/admin_filter.php",
		"CAdminList" => "interface/admin_list.php",
		"CAdminUiList" => "interface/admin_ui_list.php",
		"CAdminUiResult" => "interface/admin_ui_list.php",
		"CAdminUiContextMenu" => "interface/admin_ui_list.php",
		"CAdminListRow" => "interface/admin_list.php",
		"CAdminTabControl" => "interface/admin_tabcontrol.php",
		"CAdminForm" => "interface/admin_form.php",
		"CAdminFormSettings" => "interface/admin_form.php",
		"CAdminTabControlDrag" => "interface/admin_tabcontrol_drag.php",
		"CAdminDraggableBlockEngine" => "interface/admin_tabcontrol_drag.php",
		"CJSPopup" => "interface/jspopup.php",
		"CJSPopupOnPage" => "interface/jspopup.php",
		"CAdminCalendar" => "interface/admin_calendar.php",
		"CAdminViewTabControl" => "interface/admin_viewtabcontrol.php",
		"CAdminTabEngine" => "interface/admin_tabengine.php",
		"CCaptcha" => "classes/general/captcha.php",

		//deprecated
		"CHTMLPagesCache" => "lib/composite/helper.php",
		"StaticHtmlMemcachedResponse" => "lib/composite/responder.php",
		"StaticHtmlFileResponse" => "lib/composite/responder.php",
		"Bitrix\\Main\\Page\\Frame" => "lib/composite/engine.php",
		"Bitrix\\Main\\Page\\FrameStatic" => "lib/composite/staticarea.php",
		"Bitrix\\Main\\Page\\FrameBuffered" => "lib/composite/bufferarea.php",
		"Bitrix\\Main\\Page\\FrameHelper" => "lib/composite/bufferarea.php",
		"Bitrix\\Main\\Data\\StaticHtmlCache" => "lib/composite/page.php",
		"Bitrix\\Main\\Data\\StaticHtmlStorage" => "lib/composite/data/abstractstorage.php",
		"Bitrix\\Main\\Data\\StaticHtmlFileStorage" => "lib/composite/data/filestorage.php",
		"Bitrix\\Main\\Data\\StaticHtmlMemcachedStorage" => "lib/composite/data/memcachedstorage.php",
		"Bitrix\\Main\\Data\\StaticCacheProvider" => "lib/composite/data/cacheprovider.php",
		"Bitrix\\Main\\Data\\AppCacheManifest" => "lib/composite/appcache.php",
	)
);

require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/classes/".$DBType."/agent.php");
require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/classes/".$DBType."/user.php");
require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/classes/".$DBType."/event.php");
require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/classes/general/menu.php");
AddEventHandler("main", "OnAfterEpilog", array("\\Bitrix\\Main\\Data\\ManagedCache", "finalize"));
require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/classes/".$DBType."/usertype.php");

if(file_exists(($_fname = $_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/classes/general/update_db_updater.php")))
{
	$US_HOST_PROCESS_MAIN = False;
	include($_fname);
}

if(file_exists(($_fname = $_SERVER["DOCUMENT_ROOT"]."/bitrix/init.php")))
	include_once($_fname);

if(($_fname = getLocalPath("php_interface/init.php", BX_PERSONAL_ROOT)) !== false)
	include_once($_SERVER["DOCUMENT_ROOT"].$_fname);

if(($_fname = getLocalPath("php_interface/".SITE_ID."/init.php", BX_PERSONAL_ROOT)) !== false)
	include_once($_SERVER["DOCUMENT_ROOT"].$_fname);

if(!defined("BX_FILE_PERMISSIONS"))
	define("BX_FILE_PERMISSIONS", 0644);
if(!defined("BX_DIR_PERMISSIONS"))
	define("BX_DIR_PERMISSIONS", 0755);

//global var, is used somewhere
$GLOBALS["sDocPath"] = $GLOBALS["APPLICATION"]->GetCurPage();

if((!(defined("STATISTIC_ONLY") && STATISTIC_ONLY && substr($GLOBALS["APPLICATION"]->GetCurPage(), 0, strlen(BX_ROOT."/admin/"))!=BX_ROOT."/admin/")) && COption::GetOptionString("main", "include_charset", "Y")=="Y" && strlen(LANG_CHARSET)>0)
	header("Content-Type: text/html; charset=".LANG_CHARSET);

if(COption::GetOptionString("main", "set_p3p_header", "Y")=="Y")
	header("P3P: policyref=\"/bitrix/p3p.xml\", CP=\"NON DSP COR CUR ADM DEV PSA PSD OUR UNR BUS UNI COM NAV INT DEM STA\"");

header("X-Powered-CMS: Bitrix Site Manager (".(LICENSE_KEY == "DEMO"? "DEMO" : md5("BITRIX".LICENSE_KEY."LICENCE")).")");
if (COption::GetOptionString("main", "update_devsrv", "") == "Y")
	header("X-DevSrv-CMS: Bitrix");

define("BX_CRONTAB_SUPPORT", defined("BX_CRONTAB"));

if(COption::GetOptionString("main", "check_agents", "Y")=="Y")
{
	define("START_EXEC_AGENTS_1", microtime());
	$GLOBALS["BX_STATE"] = "AG";
	$GLOBALS["DB"]->StartUsingMasterOnly();
	CAgent::CheckAgents();
	$GLOBALS["DB"]->StopUsingMasterOnly();
	define("START_EXEC_AGENTS_2", microtime());
	$GLOBALS["BX_STATE"] = "PB";
}

//session initialization
ini_set("session.cookie_httponly", "1");

if($domain = $GLOBALS["APPLICATION"]->GetCookieDomain())
	ini_set("session.cookie_domain", $domain);

if(COption::GetOptionString("security", "session", "N") === "Y"	&& CModule::IncludeModule("security"))
	CSecuritySession::Init();

session_start();

foreach (GetModuleEvents("main", "OnPageStart", true) as $arEvent)
	ExecuteModuleEventEx($arEvent);

//define global user object
$GLOBALS["USER"] = new CUser;

//session control from group policy
$arPolicy = $GLOBALS["USER"]->GetSecurityPolicy();
$currTime = time();
if(
	(
		//IP address changed
		$_SESSION['SESS_IP']
		&& strlen($arPolicy["SESSION_IP_MASK"])>0
		&& (
			(ip2long($arPolicy["SESSION_IP_MASK"]) & ip2long($_SESSION['SESS_IP']))
			!=
			(ip2long($arPolicy["SESSION_IP_MASK"]) & ip2long($_SERVER['REMOTE_ADDR']))
		)
	)
	||
	(
		//session timeout
		$arPolicy["SESSION_TIMEOUT"]>0
		&& $_SESSION['SESS_TIME']>0
		&& $currTime-$arPolicy["SESSION_TIMEOUT"]*60 > $_SESSION['SESS_TIME']
	)
	||
	(
		//session expander control
		isset($_SESSION["BX_SESSION_TERMINATE_TIME"])
		&& $_SESSION["BX_SESSION_TERMINATE_TIME"] > 0
		&& $currTime > $_SESSION["BX_SESSION_TERMINATE_TIME"]
	)
	||
	(
		//signed session
		isset($_SESSION["BX_SESSION_SIGN"])
		&& $_SESSION["BX_SESSION_SIGN"] <> bitrix_sess_sign()
	)
	||
	(
		//session manually expired, e.g. in $User->LoginHitByHash
		isSessionExpired()
	)
)
{
	$_SESSION = array();
	@session_destroy();

	//session_destroy cleans user sesssion handles in some PHP versions
	//see http://bugs.php.net/bug.php?id=32330 discussion
	if(COption::GetOptionString("security", "session", "N") === "Y"	&& CModule::IncludeModule("security"))
		CSecuritySession::Init();

	session_id(md5(uniqid(rand(), true)));
	session_start();
	$GLOBALS["USER"] = new CUser;
}
$_SESSION['SESS_IP'] = $_SERVER['REMOTE_ADDR'];
$_SESSION['SESS_TIME'] = time();
if(!isset($_SESSION["BX_SESSION_SIGN"]))
	$_SESSION["BX_SESSION_SIGN"] = bitrix_sess_sign();

//session control from security module
if(
	(COption::GetOptionString("main", "use_session_id_ttl", "N") == "Y")
	&& (COption::GetOptionInt("main", "session_id_ttl", 0) > 0)
	&& !defined("BX_SESSION_ID_CHANGE")
)
{
	if(!array_key_exists('SESS_ID_TIME', $_SESSION))
	{
		$_SESSION['SESS_ID_TIME'] = $_SESSION['SESS_TIME'];
	}
	elseif(($_SESSION['SESS_ID_TIME'] + COption::GetOptionInt("main", "session_id_ttl")) < $_SESSION['SESS_TIME'])
	{
		if(COption::GetOptionString("security", "session", "N") === "Y" && CModule::IncludeModule("security"))
		{
			CSecuritySession::UpdateSessID();
		}
		else
		{
			session_regenerate_id();
		}
		$_SESSION['SESS_ID_TIME'] = $_SESSION['SESS_TIME'];
	}
}

define("BX_STARTED", true);

if (isset($_SESSION['BX_ADMIN_LOAD_AUTH']))
{
	define('ADMIN_SECTION_LOAD_AUTH', 1);
	unset($_SESSION['BX_ADMIN_LOAD_AUTH']);
}

if(!defined("NOT_CHECK_PERMISSIONS") || NOT_CHECK_PERMISSIONS!==true)
{
	$bLogout = isset($_REQUEST["logout"]) && (strtolower($_REQUEST["logout"]) == "yes");

	if($bLogout && $GLOBALS["USER"]->IsAuthorized())
	{
		$GLOBALS["USER"]->Logout();
		LocalRedirect($GLOBALS["APPLICATION"]->GetCurPageParam('', array('logout')));
	}

	// authorize by cookies
	if(!$GLOBALS["USER"]->IsAuthorized())
	{
		$GLOBALS["USER"]->LoginByCookies();
	}

	$arAuthResult = false;

	//http basic and digest authorization
	if(($httpAuth = $GLOBALS["USER"]->LoginByHttpAuth()) !== null)
	{
		$arAuthResult = $httpAuth;
		$GLOBALS["APPLICATION"]->SetAuthResult($arAuthResult);
	}

	//Authorize user from authorization html form
	if(isset($_REQUEST["AUTH_FORM"]) && $_REQUEST["AUTH_FORM"] <> '')
	{
		$bRsaError = false;
		if(COption::GetOptionString('main', 'use_encrypted_auth', 'N') == 'Y')
		{
			//possible encrypted user password
			$sec = new CRsaSecurity();
			if(($arKeys = $sec->LoadKeys()))
			{
				$sec->SetKeys($arKeys);
				$errno = $sec->AcceptFromForm(array('USER_PASSWORD', 'USER_CONFIRM_PASSWORD'));
				if($errno == CRsaSecurity::ERROR_SESS_CHECK)
					$arAuthResult = array("MESSAGE"=>GetMessage("main_include_decode_pass_sess"), "TYPE"=>"ERROR");
				elseif($errno < 0)
					$arAuthResult = array("MESSAGE"=>GetMessage("main_include_decode_pass_err", array("#ERRCODE#"=>$errno)), "TYPE"=>"ERROR");

				if($errno < 0)
					$bRsaError = true;
			}
		}

		if($bRsaError == false)
		{
			if(!defined("ADMIN_SECTION") || ADMIN_SECTION !== true)
				$USER_LID = LANG;
			else
				$USER_LID = false;

			if($_REQUEST["TYPE"] == "AUTH")
			{
				$arAuthResult = $GLOBALS["USER"]->Login($_REQUEST["USER_LOGIN"], $_REQUEST["USER_PASSWORD"], $_REQUEST["USER_REMEMBER"]);
			}
			elseif($_REQUEST["TYPE"] == "OTP")
			{
				$arAuthResult = $GLOBALS["USER"]->LoginByOtp($_REQUEST["USER_OTP"], $_REQUEST["OTP_REMEMBER"], $_REQUEST["captcha_word"], $_REQUEST["captcha_sid"]);
			}
			elseif($_REQUEST["TYPE"] == "SEND_PWD")
			{
				$arAuthResult = CUser::SendPassword($_REQUEST["USER_LOGIN"], $_REQUEST["USER_EMAIL"], $USER_LID, $_REQUEST["captcha_word"], $_REQUEST["captcha_sid"]);
			}
			elseif($_SERVER['REQUEST_METHOD'] == 'POST' && $_REQUEST["TYPE"] == "CHANGE_PWD")
			{
				$arAuthResult = $GLOBALS["USER"]->ChangePassword($_REQUEST["USER_LOGIN"], $_REQUEST["USER_CHECKWORD"], $_REQUEST["USER_PASSWORD"], $_REQUEST["USER_CONFIRM_PASSWORD"], $USER_LID, $_REQUEST["captcha_word"], $_REQUEST["captcha_sid"]);
			}
			elseif(COption::GetOptionString("main", "new_user_registration", "N") == "Y" && $_SERVER['REQUEST_METHOD'] == 'POST' && $_REQUEST["TYPE"] == "REGISTRATION" && (!defined("ADMIN_SECTION") || ADMIN_SECTION!==true))
			{
				$arAuthResult = $GLOBALS["USER"]->Register($_REQUEST["USER_LOGIN"], $_REQUEST["USER_NAME"], $_REQUEST["USER_LAST_NAME"], $_REQUEST["USER_PASSWORD"], $_REQUEST["USER_CONFIRM_PASSWORD"], $_REQUEST["USER_EMAIL"], $USER_LID, $_REQUEST["captcha_word"], $_REQUEST["captcha_sid"]);
			}

			if($_REQUEST["TYPE"] == "AUTH" || $_REQUEST["TYPE"] == "OTP")
			{
				//special login form in the control panel
				if($arAuthResult === true && defined('ADMIN_SECTION') && ADMIN_SECTION === true)
				{
					//store cookies for next hit (see CMain::GetSpreadCookieHTML())
					$GLOBALS["APPLICATION"]->StoreCookies();
					$_SESSION['BX_ADMIN_LOAD_AUTH'] = true;
					echo '<script type="text/javascript">window.onload=function(){top.BX.AUTHAGENT.setAuthResult(false);};</script>';
					die();
				}
			}
		}
		$GLOBALS["APPLICATION"]->SetAuthResult($arAuthResult);
	}
	elseif(!$GLOBALS["USER"]->IsAuthorized())
	{
		//Authorize by unique URL
		$GLOBALS["USER"]->LoginHitByHash();
	}
}

//logout or re-authorize the user if something importand has changed
$GLOBALS["USER"]->CheckAuthActions();

//application password scope control
if(($applicationID = $GLOBALS["USER"]->GetParam("APPLICATION_ID")) !== null)
{
	$appManager = \Bitrix\Main\Authentication\ApplicationManager::getInstance();
	if($appManager->checkScope($applicationID) !== true)
	{
		$event = new \Bitrix\Main\Event("main", "onApplicationScopeError", Array('APPLICATION_ID' => $applicationID));
		$event->send();

		CHTTP::SetStatus("403 Forbidden");
		die();
	}
}

//define the site template
if(!defined("ADMIN_SECTION") || ADMIN_SECTION !== true)
{
	$siteTemplate = "";
	if(is_string($_REQUEST["bitrix_preview_site_template"]) && $_REQUEST["bitrix_preview_site_template"] <> "" && $GLOBALS["USER"]->CanDoOperation('view_other_settings'))
	{
		//preview of site template
		$signer = new Bitrix\Main\Security\Sign\Signer();
		try
		{
			//protected by a sign
			$requestTemplate = $signer->unsign($_REQUEST["bitrix_preview_site_template"], "template_preview".bitrix_sessid());

			$aTemplates = CSiteTemplate::GetByID($requestTemplate);
			if($template = $aTemplates->Fetch())
			{
				$siteTemplate = $template["ID"];

				//preview of unsaved template
				if(isset($_GET['bx_template_preview_mode']) && $_GET['bx_template_preview_mode'] == 'Y' && $GLOBALS["USER"]->CanDoOperation('edit_other_settings'))
				{
					define("SITE_TEMPLATE_PREVIEW_MODE", true);
				}
			}
		}
		catch(\Bitrix\Main\Security\Sign\BadSignatureException $e)
		{
		}
	}
	if($siteTemplate == "")
	{
		$siteTemplate = CSite::GetCurTemplate();
	}
	define("SITE_TEMPLATE_ID", $siteTemplate);
	define("SITE_TEMPLATE_PATH", getLocalPath('templates/'.SITE_TEMPLATE_ID, BX_PERSONAL_ROOT));
}

//magic parameters: show page creation time
if(isset($_GET["show_page_exec_time"]))
{
	if($_GET["show_page_exec_time"]=="Y" || $_GET["show_page_exec_time"]=="N")
		$_SESSION["SESS_SHOW_TIME_EXEC"] = $_GET["show_page_exec_time"];
}

//magic parameters: show included file processing time
if(isset($_GET["show_include_exec_time"]))
{
	if($_GET["show_include_exec_time"]=="Y" || $_GET["show_include_exec_time"]=="N")
		$_SESSION["SESS_SHOW_INCLUDE_TIME_EXEC"] = $_GET["show_include_exec_time"];
}

//magic parameters: show include areas
if(isset($_GET["bitrix_include_areas"]) && $_GET["bitrix_include_areas"] <> "")
	$GLOBALS["APPLICATION"]->SetShowIncludeAreas($_GET["bitrix_include_areas"]=="Y");

//magic sound
if($GLOBALS["USER"]->IsAuthorized())
{
	$cookie_prefix = COption::GetOptionString('main', 'cookie_name', 'BITRIX_SM');
	if(!isset($_COOKIE[$cookie_prefix.'_SOUND_LOGIN_PLAYED']))
		$GLOBALS["APPLICATION"]->set_cookie('SOUND_LOGIN_PLAYED', 'Y', 0);
}

//magic cache
\Bitrix\Main\Composite\Engine::shouldBeEnabled();

//magic short URI
if(defined("BX_CHECK_SHORT_URI") && BX_CHECK_SHORT_URI && CBXShortUri::CheckUri())
{
	//local redirect inside
	die();
}

foreach(GetModuleEvents("main", "OnBeforeProlog", true) as $arEvent)
	ExecuteModuleEventEx($arEvent);

if((!defined("NOT_CHECK_PERMISSIONS") || NOT_CHECK_PERMISSIONS!==true) && (!defined("NOT_CHECK_FILE_PERMISSIONS") || NOT_CHECK_FILE_PERMISSIONS!==true))
{
	$real_path = $request->getScriptFile();

	if(!$GLOBALS["USER"]->CanDoFileOperation('fm_view_file', array(SITE_ID, $real_path)) || (defined("NEED_AUTH") && NEED_AUTH && !$GLOBALS["USER"]->IsAuthorized()))
	{
		/** @noinspection PhpUndefinedVariableInspection */
		if($GLOBALS["USER"]->IsAuthorized() && $arAuthResult["MESSAGE"] == '')
			$arAuthResult = array("MESSAGE"=>GetMessage("ACCESS_DENIED").' '.GetMessage("ACCESS_DENIED_FILE", array("#FILE#"=>$real_path)), "TYPE"=>"ERROR");

		if(defined("ADMIN_SECTION") && ADMIN_SECTION==true)
		{
			if ($_REQUEST["mode"]=="list" || $_REQUEST["mode"]=="settings")
			{
				echo "<script>top.location='".$GLOBALS["APPLICATION"]->GetCurPage()."?".DeleteParam(array("mode"))."';</script>";
				die();
			}
			elseif ($_REQUEST["mode"]=="frame")
			{
				echo "<script type=\"text/javascript\">
					var w = (opener? opener.window:parent.window);
					w.location.href='".$GLOBALS["APPLICATION"]->GetCurPage()."?".DeleteParam(array("mode"))."';
				</script>";
				die();
			}
			elseif(defined("MOBILE_APP_ADMIN") && MOBILE_APP_ADMIN==true)
			{
				echo json_encode(Array("status"=>"failed"));
				die();
			}
		}

		/** @noinspection PhpUndefinedVariableInspection */
		$GLOBALS["APPLICATION"]->AuthForm($arAuthResult);
	}
}

/*ZDUyZmZNzY0NDRmNDdkYjA3ZjZmMDE4NzExNDNiMDFlODE4N2I=*/$GLOBALS['____81434947']= array(base64_decode('b'.'XRfcmFuZA='.'='),base64_decode(''.'ZXh'.'wbG9kZQ=='),base64_decode('c'.'GFj'.'aw'.'=='),base64_decode('b'.'WQ1'),base64_decode(''.'Y29u'.'c3Rhb'.'nQ='),base64_decode('aGFza'.'F9obW'.'Fj'),base64_decode(''.'c3R'.'yY21w'),base64_decode('aXNf'.'b2JqZWN0'),base64_decode('Y2FsbF91c2VyX'.'2Z1bmM='),base64_decode('Y'.'2'.'FsbF9'.'1c'.'2VyX2Z1bmM='),base64_decode('Y2FsbF91'.'c2V'.'yX'.'2Z'.'1bmM='),base64_decode('Y2FsbF'.'91c2'.'VyX'.'2Z'.'1'.'bmM='),base64_decode(''.'Y2FsbF91c2VyX2Z1bmM='));if(!function_exists(__NAMESPACE__.'\\___2144843247')){function ___2144843247($_469684335){static $_459631100= false; if($_459631100 == false) $_459631100=array('REI'.'=','U0VMRUNUIFZ'.'BTFVFIE'.'ZS'.'T'.'00gYl9vcHRpb24gV0hF'.'UkUgTkFNRT0nflBBUk'.'FNX'.'01BWF9'.'V'.'U'.'0V'.'SUycg'.'QU5'.'EIE1PRFVMRV9JRD0nbWF'.'pbicgQU5EIFNJVEVf'.'SUQgSVMg'.'TlVMTA==','VkFMVUU=','Lg==','SC'.'o=',''.'Yml0cml4','TE'.'lD'.'RU5'.'TRV9LRVk=','c2hhMj'.'U2','V'.'VNFUg==','VVNFUg==','VV'.'NFUg==','SXN'.'B'.'dXRo'.'b'.'3Jp'.'emVk','V'.'VNF'.'U'.'g==','SX'.'NBZ'.'G1'.'pbg='.'=','QVBQTElDQ'.'VRJT0'.'4=','U'.'mVzd'.'GFyd'.'EJ1ZmZ'.'l'.'cg==',''.'T'.'G9'.'jY'.'WxSZWRpc'.'mV'.'jd'.'A==','L2xpY'.'2'.'Vu'.'Y2Vf'.'c'.'m'.'VzdHJpY'.'3'.'Rpb'.'2'.'4ucG'.'hw',''.'XEJpdHJpeFxN'.'Y'.'WluXE'.'Nvb'.'m'.'ZpZ'.'1xPcH'.'Rpb246'.'O'.'nN'.'ldA'.'==',''.'bWFpbg==','UEFSQU'.'1f'.'TUFYX1VTRVJT');return base64_decode($_459631100[$_469684335]);}};if($GLOBALS['____81434947'][0](round(0+0.2+0.2+0.2+0.2+0.2), round(0+5+5+5+5)) == round(0+7)){ $_1612050994= $GLOBALS[___2144843247(0)]->Query(___2144843247(1), true); if($_277180707= $_1612050994->Fetch()){ $_1302300872= $_277180707[___2144843247(2)]; list($_557114012, $_1203494896)= $GLOBALS['____81434947'][1](___2144843247(3), $_1302300872); $_499384563= $GLOBALS['____81434947'][2](___2144843247(4), $_557114012); $_2080965769= ___2144843247(5).$GLOBALS['____81434947'][3]($GLOBALS['____81434947'][4](___2144843247(6))); $_1549102720= $GLOBALS['____81434947'][5](___2144843247(7), $_1203494896, $_2080965769, true); if($GLOBALS['____81434947'][6]($_1549102720, $_499384563) !== min(2,0,0.66666666666667)){ if(isset($GLOBALS[___2144843247(8)]) && $GLOBALS['____81434947'][7]($GLOBALS[___2144843247(9)]) && $GLOBALS['____81434947'][8](array($GLOBALS[___2144843247(10)], ___2144843247(11))) &&!$GLOBALS['____81434947'][9](array($GLOBALS[___2144843247(12)], ___2144843247(13)))){ $GLOBALS['____81434947'][10](array($GLOBALS[___2144843247(14)], ___2144843247(15))); $GLOBALS['____81434947'][11](___2144843247(16), ___2144843247(17), true);}}} else{ $GLOBALS['____81434947'][12](___2144843247(18), ___2144843247(19), ___2144843247(20), round(0+12));}}/**/       //Do not remove this

if(isset($REDIRECT_STATUS) && $REDIRECT_STATUS==404)
{
	if(COption::GetOptionString("main", "header_200", "N")=="Y")
		CHTTP::SetStatus("200 OK");
}
