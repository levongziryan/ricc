<?
$MESS["COMP_SITE_MAP_VALUE_YES"] = "Oui";
$MESS["COMP_MAIN_SITE_MAP_COL_NUM_NAME"] = "Nombre de colonnes";
$MESS["COMP_MAIN_SITE_MAP_LEVEL_NAME"] = "Niveau maximal d'intériorité (0 - sans intériorité)";
$MESS["MAIN_SITE_MAP_PARAMS_NAME"] = "Paramètres du composant";
$MESS["COMP_SITE_MAP_VALUE_NO"] = "Non";
$MESS["COMP_MAIN_SITE_MAP_SHOW_DESCRIPTION"] = "Afficher les descriptions";
?>