<?
$MESS["DAV_EXP_NAME"] = "Name";
$MESS["DAV_EXP_ACCOUNT_TYPE"] = "Verbindungstyp";
$MESS["DAV_EXP_ENTITY_TYPE"] = "Einheitstyp";
$MESS["DAV_EXP_ENTITY_ID"] = "Einheit-ID";
$MESS["DAV_EXP_SERVER_SCHEME"] = "Schema";
$MESS["DAV_EXP_SERVER_HOST"] = "Adresse";
$MESS["DAV_EXP_SERVER_PORT"] = "Port";
$MESS["DAV_EXP_ACCOUNT_TYPE_OOR"] = "Für den Verbindungstyp sind folgende Werte möglich: caldav, ical, caldav_google_oauth.";
$MESS["DAV_EXP_ENTITY_TYPE_OOR"] = "Für den Einheitstyp sind folgende Werte möglich: Nutzer, Gruppe.";
$MESS["DAV_EXP_ENTITY_ID_TYPE"] = "Die Einheit-ID muss eine Ganzzahl sein.";
$MESS["DAV_EXP_SERVER_SCHEME_OOR"] = "Für das Verbindungsschema sind folgende Werte möglich: http, https.";
?>