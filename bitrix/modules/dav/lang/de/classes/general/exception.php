<?
$MESS["DAVCGERR_NULL_ARG"] = "Der Wert von '#PARAM#' ist nicht angegeben.";
$MESS["DAVCGERR_INVALID_ARG"] = "Der Wert von '#PARAM#' ist ungültig.";
$MESS["DAVCGERR_INVALID_ARG1"] = "Der Wert von '#PARAM#' muss #VALUE# sein.";
$MESS["DAVCGERR_INVALID_TYPE"] = "Der Wert von '#PARAM#' gehört einem ungültigen Typ an.";
$MESS["DAVCGERR_INVALID_TYPE1"] = "Der Wert von '#PARAM#' muss dem Typ '#VALUE#' angehören.";
?>