<?
$MESS["dav_app_calendar"] = "Kalender";
$MESS["dav_app_calendar_desc"] = "Synchronisieren Sie Ihre Kalendertermine, um geplante Ereignisse und Aktivitäten anzuzeigen. Wählen Sie den Zielort der Synchronisierung aus, den Sie konfigurieren möchten, dann klicken Sie auf \"Passwort anfordern\".";
$MESS["dav_app_calendar_phone"] = "Mobiltelefon";
$MESS["dav_app_card"] = "Kontakte";
$MESS["dav_app_card_desc"] = "Synchronisieren Sie die Liste Ihrer Kontakte mit den Kontakten im Mobiltelefon oder mit anderen Anwendungen.";
$MESS["dav_app_doc"] = "Dokumente";
$MESS["dav_app_doc_desc"] = "Einige Versionen von MS Office erfordern Login und Passwort, um Dokumente bearbeiten zu können. Benutzen Sie Ihren Login; um ein korrektes Passwort zu bekommen, klicken Sie auf \"Passwort anfordern\".";
$MESS["dav_app_doc_office"] = "MS Office";
$MESS["DAV_APP_SYSCOMMENT"] = "DAV-Synchronisierung";
$MESS["DAV_APP_SYSCOMMENT_TYPE"] = "DAV-Synchronisierung: #TYPE#";
$MESS["DAV_APP_COMMENT"] = "automatisch erstellt";
$MESS["DAV_APP_TYPE_carddav"] = "Kontakte";
$MESS["DAV_APP_TYPE_caldav"] = "Kalender";
?>