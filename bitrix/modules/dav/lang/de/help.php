<?
$MESS["DAV_HELP_NAME"] = "Modul DAV";
$MESS["DAV_HELP_TEXT"] = "Das Modul DAV ermöglicht es, Kontakte auf der Website mit einer beliebigen Software oder einem beliebigen Gerät zu synchronisieren, welche die Protokolle CalDAV und CardDAV unterstützen. iPhone und iPad unterstützen diese Protokolls. Software-Unterstützung erfolgt durch Mozilla Sunbird, eM Client und einige anderen Software Anwendungen.<br>
<ul>
<li><b><a href=\"#carddav\">Verbindung via CardDav</a></b></li>
<li><b><a href=\"#caldav\">Verbindung via CalDav</a></b>
<ul>
<li><a href=\"#caldavipad\">Verbinden </a><a href=\"#caldavipad\">iPhone/iPad</a></li>
<li><a href=\"#carddavsunbird\">Verbinden Mozilla Sunbird</a></li>
</ul>
</li>
</ul>

<br>

<h3><a name=\"carddav\"></a>Verbinden via CardDav</h3>

Um die Unterstützung für CardDAV auf Ihrem Apple Gerät einzustellen:
<ol>
<li>Öffnen Sie <b>Einstellungen</b> und wählen Sie <b>Mail, Kontakte, Kalender>Accounts</b>.</li>
<li>Tippen Sie auf <b>Account hinzufügen</b>.</li>
<li>Wählen Sie <b>Sonstiges</b> &gt; <b>CardDAV Account hinzufügen</b>.</li>
<li>Geben Sie diese Website-Adresse als Server an (#SERVER#). Nutzen Sie Ihren Login und Ihr Passwort.</li>
<li>Nutzen Sie die Zwei-Faktor-Authentifizierung, sollten Sie das Passwort nutzen, das im Nutzerprofil angegeben ist: <b>Passwörter der Anwendungen</b> - <b>Kontakte</b>.</li>
<li>Um die Portnummer anzugeben, speichern Sie den Account, dann öffnen Sie ihn zur Bearbeitung wieder, dann wechseln Sie in den Bereich <b>Mehr</b></li>
</ol>

Alle Ihre Kontakte erscheinen in der App Kontakte.<br>
Um Synchronisierung für ausgewählte Kontakte zu aktivieren bzw. zu deaktivieren, öffnen Sie Ihr Profil und wählen Sie <b>Synchronisieren </b>im Menü.

<br><br>

<h3><a name=\"caldav\"></a>Verbinden via CalDAV</h3>

<h4><a name=\"caldavipad\"></a>iPhone/iPad verbinden</h4>

Um die Unterstützung für CalDAV auf Ihrem Apple Gerät einzustellen:
<ol>
<li>Auf Ihrem Apple Gerät öffnen Sie das Menü <b>Einstellungen</b> &gt; <b>Kontakte</b> &gt; <b>Accounts</b>.</li>
<li>Wählen Sie <b>Account hinzufügen</b> in der Accountsliste.</li>
<li>Wählen Sie CalDAV als Accounttyp (<b>Sonstiges</b> &gt; <b>CalDAV Account</b>).</li>
<li>Geben Sie diese Website-Adresse als Server an (#SERVER#). Nutzen Sie Ihren Login und Ihr Passwort.</li>
<li>Nutzen Sie die Zwei-Faktor-Authentifizierung, sollten Sie das Passwort nutzen, das im Nutzerprofil angegeben ist: <b>Passwörter der Anwendungen</b>
  &gt; <b>Kalender</b>.</li>
<li>Um die Portnummer anzugeben, speichern Sie den Account, dann öffnen Sie ihn zur Bearbeitung wieder, dann wechseln Sie in den Bereich <b>More</b> </li>
</ol>

Ihre Kalender erscheinen in der App Kalender.&nbsp;<br>
Um die Kalender anderer Nutzer zu verbinden, nutzen Sie diese Links in <b>Mehr</b> &gt; <b>Account
URL</b>:<br>
<i>#SERVER#/bitrix/groupdav.php/site ID/user name/calendar/</i><br>
und<br>
<i>#SERVER#/bitrix/groupdav.php/</i><i>Site ID</i><i>/group-group ID/calendar/</i>

<br><br>

<h4><a name=\"carddavsunbird\"></a>Mozilla Sunbird verbinden</h4>

Um Mozilla Sunbird zur Nutzung mit CalDAV zu konfigurieren:
<ol>
<li>Starten Sie Sunbird und wählen Sie <b>Datei &gt; Neuer Kalender</b>.</li>
<li>Wählen Sie <b>Im Netzwerk</b> und tippen Sie auf <b>Weiter</b>.</li>
<li>Wählen Sie <b>CalDAV</b> Format.</li>
<li>Im Feld <b>Standort</b> eingeben:<br>
  <i>#SERVER#/bitrix/groupdav.php/site ID/user name/calendar/calendar ID/</i><br>
  oder<br>
  <i>#SERVER#/bitrix/groupdav.php/</i><i>site ID</i><i>/group-</i><i>Gruppe-ID</i><i>/calendar/calendar
  ID/</i><br>
dann tippen Sie auf <b>Weiter</b>.</li>
<li>Benennen Sie Ihren Kalender und wählen Sie eine Farbe dafür.</li>
<li>Geben Sie Ihren Nutzernamen und Ihr Passwort ein.</li>
<li>Nutzen Sie die Zwei-Faktor-Authentifizierung, sollten Sie das Passwort nutzen, das im Nutzerprofil angegeben ist: <b>Passwörter der Anwendungen</b>
  &gt; <b>Kalender</b>.</li>
</ol>

Ihre Kalender werden in Mozilla Sunbird verfügbar sein.
";
?>