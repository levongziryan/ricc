<?
$MESS["DAV_INSTALL_DESCRIPTION"] = "Modul der Zugriffe auf Objekte und Sammlungen";
$MESS["DAV_PERM_D"] = "Zugriff verweigert";
$MESS["DAV_INSTALL_NAME"] = "DAV";
$MESS["DAV_INSTALL_TITLE"] = "Modulinstallation";
$MESS["DAV_PERM_R"] = "Lesen";
$MESS["DAV_PERM_W"] = "Schreiben";
$MESS["DAV_PHP_L439"] = "Sie haben eine PHP-Version #VERS# installiert, während dieses Modul die Version ab 5.0.0 erfordert. Aktualisieren Sie bitte PHP oder wenden Sie sich an den Technischen Support Ihres Hosting-Anbieters.";
$MESS["DAV_ERROR_EDITABLE"] = "Dieses Modul ist in Ihrer Edition nicht verfügbar.";
?>