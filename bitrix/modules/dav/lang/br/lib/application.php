<?
$MESS["dav_app_calendar"] = "Calendário";
$MESS["dav_app_calendar_desc"] = "Sincronize seu calendário de eventos para ver eventos e atividades planejados. Selecione a meta de sincronização para configuração e clique em \"Obter Senha\".";
$MESS["dav_app_calendar_phone"] = "Telefone celular";
$MESS["dav_app_card"] = "Funcionários";
$MESS["dav_app_card_desc"] = "Sincronize sua lista de funcionários com contatos do telefone celular ou outros aplicativos.";
$MESS["dav_app_doc"] = "Documentos";
$MESS["dav_app_doc_desc"] = "Algumas versões do MS Office requerem login e senha para editar documentos. Utilize seu login; para obter uma senha correta clique em \"Obter Senha\".";
$MESS["dav_app_doc_office"] = "MS Office";
$MESS["DAV_APP_SYSCOMMENT"] = "Sincronização DAV";
$MESS["DAV_APP_SYSCOMMENT_TYPE"] = "Sincronização DAV: #TYPE#";
$MESS["DAV_APP_COMMENT"] = "Criado automaticamente";
$MESS["DAV_APP_TYPE_carddav"] = "contatos";
$MESS["DAV_APP_TYPE_caldav"] = "calendário";
?>