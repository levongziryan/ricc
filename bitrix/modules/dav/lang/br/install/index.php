<?
$MESS["DAV_INSTALL_DESCRIPTION"] = "Módulo de acesso a coleção e objeto";
$MESS["DAV_PERM_D"] = "acesso negado";
$MESS["DAV_INSTALL_NAME"] = "DAV";
$MESS["DAV_INSTALL_TITLE"] = "Instalação de módulo";
$MESS["DAV_PERM_R"] = "ler";
$MESS["DAV_PERM_W"] = "escrever";
$MESS["DAV_PHP_L439"] = "Você tem a versão PHP #VERS# instalada, onde que este módulo requer a versão 5.0.0 ou superior. Por favor, atualize seu PHP ou contate o serviço de suporte de hospedagem.";
?>