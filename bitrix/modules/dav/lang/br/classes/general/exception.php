<?
$MESS["DAVCGERR_NULL_ARG"] = "O valor de '#PARAM#' é indefinido.";
$MESS["DAVCGERR_INVALID_ARG"] = "O valor de '#PARAM#' está inválido.";
$MESS["DAVCGERR_INVALID_ARG1"] = "O valor de '#PARAM#'deve ser #VALUE#.";
$MESS["DAVCGERR_INVALID_TYPE"] = "O valor de '#PARAM#' é do tipo inválido.";
$MESS["DAVCGERR_INVALID_TYPE1"] = "O valor de '#PARAM#' deve ser do tipo '#VALUE#'.";
?>