<?
$MESS["DAV_EXP_NAME"] = "Nome";
$MESS["DAV_EXP_ACCOUNT_TYPE"] = "Tipo de conexão";
$MESS["DAV_EXP_ENTITY_TYPE"] = "Tipo de entidade";
$MESS["DAV_EXP_ENTITY_ID"] = "ID da entidade";
$MESS["DAV_EXP_SERVER_SCHEME"] = "Esquema";
$MESS["DAV_EXP_SERVER_HOST"] = "Endereço";
$MESS["DAV_EXP_SERVER_PORT"] = "Porta";
$MESS["DAV_EXP_ACCOUNT_TYPE_OOR"] = "Os valores de tipo de conexão a seguir são possíveis: iCal, CalDAV.";
$MESS["DAV_EXP_ENTITY_TYPE_OOR"] = "Os valores de tipo de entidade seguintes são possíveis: usuário, grupo.";
$MESS["DAV_EXP_ENTITY_ID_TYPE"] = "O ID da entidade deve ser um número inteiro.";
$MESS["DAV_EXP_SERVER_SCHEME_OOR"] = "Os seguintes valores de esquema de conexão são possíveis: http, https.";
?>