<?
$MESS["DAV_INSTALL_DESCRIPTION"] = "Objeto y Colección del Módulo de Acceso";
$MESS["DAV_PERM_D"] = "acceso denegado";
$MESS["DAV_INSTALL_NAME"] = "DAV";
$MESS["DAV_INSTALL_TITLE"] = "Instalación del Módulo";
$MESS["DAV_PERM_R"] = "leer";
$MESS["DAV_PERM_W"] = "escribir";
$MESS["DAV_PHP_L439"] = "Usted tiene la versión PHP #VERS# instalada donde sea que el módulo requiera la versión 5.0.0 o superior. Por favor actualizar el PHP o contactar con el soporte de su servicio de alojamiento.";
$MESS["DAV_ERROR_EDITABLE"] = "Su edición no proporciona este módulo.";
?>