<?
$MESS["dav_app_calendar"] = "Calendario";
$MESS["dav_app_calendar_desc"] = "Sincronizar los eventos de su calendario para ver los eventos y actividades programadas. Seleccione el objetivo de sincronización para configurar y haga clic en \"Get Password\".";
$MESS["dav_app_calendar_phone"] = "Teléfono móvil";
$MESS["dav_app_card"] = "Empleados";
$MESS["dav_app_card_desc"] = "Sincronizar la lista de empleados con los contactos del teléfono móvil u otras aplicaciones.";
$MESS["dav_app_doc"] = "Documentos";
$MESS["dav_app_doc_desc"] = "Algunas versiones de MS Office requieren inicio de sesión y contraseña para editar documentos. Utilice su nombre de usuario; para obtener una contraseña correcta, haga clic en \"Get Password\".";
$MESS["dav_app_doc_office"] = "MS Office";
$MESS["DAV_APP_SYSCOMMENT"] = "Sincronización de DAV";
$MESS["DAV_APP_SYSCOMMENT_TYPE"] = "Sincronización de DAV: #TYPE#";
$MESS["DAV_APP_COMMENT"] = "Creado automáticamente";
$MESS["DAV_APP_TYPE_carddav"] = "contactos";
$MESS["DAV_APP_TYPE_caldav"] = "calendario";
?>