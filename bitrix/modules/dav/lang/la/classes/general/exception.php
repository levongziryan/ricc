<?
$MESS["DAVCGERR_NULL_ARG"] = "El valor del '#PARAM#' es indefinido.";
$MESS["DAVCGERR_INVALID_ARG"] = "El valor del '#PARAM#' es inválido.";
$MESS["DAVCGERR_INVALID_ARG1"] = "El valor del '#PARAM#' debe ser #VALUE#.";
$MESS["DAVCGERR_INVALID_TYPE"] = "El valor del '#PARAM#' es de un tipo inválido.";
$MESS["DAVCGERR_INVALID_TYPE1"] = "El valor del '#PARAM#' debe ser del tipo '#VALUE#'.";
?>