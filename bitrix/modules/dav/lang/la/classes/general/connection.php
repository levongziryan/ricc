<?
$MESS["DAV_EXP_NAME"] = "Nombre";
$MESS["DAV_EXP_ACCOUNT_TYPE"] = "Tipo de Conexión";
$MESS["DAV_EXP_ENTITY_TYPE"] = "Tipo de Entidad";
$MESS["DAV_EXP_ENTITY_ID"] = "ID de la Entidad";
$MESS["DAV_EXP_SERVER_HOST"] = "Dirección";
$MESS["DAV_EXP_SERVER_PORT"] = "Puerto";
$MESS["DAV_EXP_ACCOUNT_TYPE_OOR"] = "Los siguientes tipos de valores de conexión son posibles: caldav, ical.";
$MESS["DAV_EXP_ENTITY_TYPE_OOR"] = "Los siguientes tipos de valores de la entidad son posibles: caldav, ical.";
$MESS["DAV_EXP_ENTITY_ID_TYPE"] = "El ID de la entidad debe ser una unidad.";
$MESS["DAV_EXP_SERVER_SCHEME"] = "Esquema";
$MESS["DAV_EXP_SERVER_SCHEME_OOR"] = "Los siguientes valores del esquema son posibles: http, https.";
?>