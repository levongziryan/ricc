<?
$MESS["dav_app_doc"] = "Documents";
$MESS["dav_app_calendar"] = "Calendrier";
$MESS["dav_app_calendar_phone"] = "Téléphone portable";
$MESS["dav_app_card"] = "Employés";
$MESS["dav_app_doc_office"] = "MS Office";
$MESS["dav_app_card_desc"] = "Synchronisez votre liste de contacts avec les employés de téléphones mobiles ou d'autres applications.";
$MESS["dav_app_doc_desc"] = "Certaines versions de MS Office nécessitent login et mot de passe pour éditer des documents. Utilisez votre connexion; pour obtenir un mot de passe, cliquez bon 'Recevoir Mot de passe'.";
$MESS["dav_app_calendar_desc"] = "Synchroniser les événements de votre calendrier pour voir les événements et les activités prévues. Sélectionnez la cible de synchronisation pour configurer et cliquez sur 'Obtenir mot de passe'.";
$MESS["DAV_APP_SYSCOMMENT"] = "Synchronisation DAV";
$MESS["DAV_APP_SYSCOMMENT_TYPE"] = "Synchronisation DAV : #TYPE#";
$MESS["DAV_APP_COMMENT"] = "Créé automatiquement";
$MESS["DAV_APP_TYPE_carddav"] = "contacts";
$MESS["DAV_APP_TYPE_caldav"] = "calendrier";
?>