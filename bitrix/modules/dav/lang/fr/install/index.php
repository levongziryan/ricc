<?
$MESS["DAV_INSTALL_NAME"] = "DAV";
$MESS["DAV_PHP_L439"] = "Vous utilisez la version PHP #VERS#, pour fonctionner le module a besoin d'une version 5.0.0 ou plus récente. Veuillez mettre à jour PHP ou contactez le service d'appui technique de votre hébergement.";
$MESS["DAV_PERM_D"] = "accès refusé";
$MESS["DAV_PERM_W"] = "note";
$MESS["DAV_INSTALL_DESCRIPTION"] = "Module du support d'accès aux objets et collections";
$MESS["DAV_INSTALL_TITLE"] = "Installation de la solution";
$MESS["DAV_PERM_R"] = "lecture";
$MESS["DAV_ERROR_EDITABLE"] = "Votre édition ne fournit pas ce module.";
?>