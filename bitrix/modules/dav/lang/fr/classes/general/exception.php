<?
$MESS["DAVCGERR_INVALID_ARG1"] = "Valeur '#PARAM#' doit être égale à #VALUE#.";
$MESS["DAVCGERR_INVALID_TYPE1"] = "La valeur '#PARAM#' doit être du type #VALUE#'.";
$MESS["DAVCGERR_INVALID_TYPE"] = "La valeur '#PARAM#' a un type invalide.";
$MESS["DAVCGERR_INVALID_ARG"] = "La valeur '#PARAM#' n'est pas permise.";
$MESS["DAVCGERR_NULL_ARG"] = "La valeur '#PARAM#' n'est pas définie.";
?>