<?
$MESS["DAV_EXP_ENTITY_ID_TYPE"] = "Le code de l'entité doit être nombre intégral.";
$MESS["DAV_EXP_SERVER_HOST"] = "Adresse";
$MESS["DAV_EXP_ENTITY_ID"] = "ID de l'élément de l'entité";
$MESS["DAV_EXP_ACCOUNT_TYPE"] = "Le type de connexion n'est pas spécifié";
$MESS["DAV_EXP_ENTITY_TYPE"] = "Le type de substance n'est pas spécifié";
$MESS["DAV_EXP_SERVER_SCHEME"] = "Le schéma de connexion avec le serveur CalDAV n'est pas indiqué";
$MESS["DAV_EXP_NAME"] = "Dénomination";
$MESS["DAV_EXP_SERVER_PORT"] = "Port";
$MESS["DAV_EXP_SERVER_SCHEME_OOR"] = "Les valeurs de schéma de connexion possibles sont les suivants: http, https.";
$MESS["DAV_EXP_ACCOUNT_TYPE_OOR"] = "Les valeurs de type de connexion suivants sont possibles: CalDAV, iCal.";
$MESS["DAV_EXP_ENTITY_TYPE_OOR"] = "Le type d'entité doit avoir une des valeurs suivantes: user, group.";
?>