<?
$MESS["DAV_SYNCHRONIZE_TITLE"] = "Paramètres de synchronisation";
$MESS["dav_app_synchronize_auth"] = "Vous devez être connecté avant de pouvoir utiliser les paramètres de synchronisation.";
$MESS["DAV_ACCOUNTS"] = "Utilisateurs du système";
$MESS["DAV_CONTACTS"] = "Contacts du CRM";
$MESS["DAV_COMPANIES"] = "Entreprises du CRM";
$MESS["DAV_EXTRANET_ACCOUNTS"] = "Utilisateurs Extranet";
?>