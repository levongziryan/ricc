<?
$MESS["DAV_SYNCHRONIZE_TITLE"] = "Parámetros de sincronización";
$MESS["dav_app_synchronize_auth"] = "Debe iniciar sesión antes de utilizar los parámetros de sincronización.";
$MESS["DAV_ACCOUNTS"] = "Usuarios del Sistema";
$MESS["DAV_CONTACTS"] = "Contactos del CRM";
$MESS["DAV_COMPANIES"] = "Compañías del CRM";
$MESS["DAV_EXTRANET_ACCOUNTS"] = "Usuarios de la Extranet";
?>