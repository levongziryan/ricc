<?
$MESS["main_app_pass_del"] = "Supprimer";
$MESS["main_app_pass_title"] = "Qu'est-ce que c'est ?";
$MESS["main_app_pass_text1"] = "Pour synchroniser vos données avec des applications externes en toute sécurité, utilisez un mot de passe spécial que vous pouvez obtenir sur cette page.";
$MESS["main_app_pass_text2"] = "Chacun de ces outils fonctionne avec différentes applications compatibles avec la synchronisation. Sélectionnez une application pour laquelle vous souhaitez configurer le transfert de données.";
$MESS["main_app_pass_created"] = "Créé le";
$MESS["main_app_pass_last"] = "Dernière connexion";
$MESS["main_app_pass_last_ip"] = "Dernière IP";
$MESS["main_app_pass_manage"] = "Gérer";
$MESS["main_app_pass_link"] = "Connecter";
$MESS["main_app_pass_comment"] = "Commentaire";
$MESS["main_app_pass_other"] = "Autre";
$MESS["main_app_pass_comment_ph"] = "Ajouter une description";
$MESS["main_app_pass_get_pass"] = "Demander le mot de passe";
$MESS["main_app_pass_create_pass"] = "Créer un mot de passe";
$MESS["main_app_pass_create_pass_text"] = "Utilisez ce mot de passe pour synchroniser avec l'application sélectionnée.
Le mot de passe ne peut être tenu sous forme ouverte; par conséquent, il est uniquement disponible lorsque il est obtenu par l'utilisateur.
Ne fermez pas la fenêtre jusqu'à ce que vous ayez copié ou entré votre mot de passe.";
$MESS["main_app_pass_create_pass_close"] = "Fermer";
$MESS["main_app_pass_del_pass"] = "Voulez-vous vraiment supprimer le mot de passe ?";
$MESS["main_app_pass_del_pass_text"] = "La synchronisation va être interrompue, car l'application ne pourra pas accéder aux données en raison d'une erreur d'authentification.";
$MESS["DAV_BUTTON_SAVE"] = "Enregistrer";
$MESS["DAV_ACCOUNTS_SECTION"] = "Utilisateurs du système";
$MESS["DAV_ENABLE"] = "Activer";
$MESS["DAV_ACCOUNTS_EXPORT_DEPARTMENT"] = "Département";
$MESS["DAV_CONTACTS"] = "Contacts du CRM";
$MESS["DAV_COMPANIES"] = "Entreprises du CRM";
$MESS["DAV_EXTRANET_ACCOUNTS"] = "Utilisateurs Extranet";
$MESS["DAV_MAX_COUNT"] = "Limite";
$MESS["DAV_EXPORT_FILTER"] = "Filtre";
$MESS["DAV_CARDDAV_SETTINGS_HELP"] = "Le module DAV offre la possibilité de synchroniser facilement les contacts du site avec les applications et appareils compatibles avec le protocole CardDAV. Par exemple, les iPhone et iPad sont totalement compatibles avec CardDAV.
<br><br>

<h3>Activer la connexion CardDAV sur votre appareil Apple</h3>

Pour configurer la compatibilité CardDAV sur les appareils Apple, suivez les étapes suivantes :
<ol>
<li>Sur votre appareil Apple, ouvrez les <b>Paramètres</b> et appuyez sur l'onglet <b>Comptes</b>.</li>
<li>Appuyez sur <b>Ajouter un compte</b>.</li>
<li>Sélectionnez <b>CardDAV</b> comme type de compte.</li>
<li>Dans les préférences du compte, entrez l'adresse du site (#SERVER#) comme serveur, et
  indiquez votre identifiant et votre mot de passe.</li>
<li>Sélectionnez <b>Autorisation de base</b> comme type d'authentification.</li>
<li>Pour spécifier un numéro de port optionnel, enregistrez le compte et ouvrez-le ensuite pour le modifier.</li>
</ol>

Tous vos contacts apparaîtront dans l'application Contacts. Pour activer ou désactiver la
synchronisation pour les contacts sélectionnés, utilisez les paramètres suivants.<br>";
$MESS["DAV_COMMON_SECTION"] = "Paramètres généraux";
$MESS["DAV_DEFAULT_COLLECTION_TO_SYNC"] = "Synchroniser par défaut";
?>