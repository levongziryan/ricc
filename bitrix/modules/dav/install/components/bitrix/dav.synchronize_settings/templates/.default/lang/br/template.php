<?
$MESS["main_app_pass_del"] = "Excluir";
$MESS["main_app_pass_title"] = "O que é isto?";
$MESS["main_app_pass_text1"] = "Para sincronizar seus dados com aplicativos externos com segurança, use uma senha especial que você pode obter nesta página.";
$MESS["main_app_pass_text2"] = "Cada uma destas ferramentas são compatíveis com vários aplicativos combatíveis com sincronização. Selecione um aplicativo para o qual você deseja configurar a transferência de dados.";
$MESS["main_app_pass_created"] = "Criada em";
$MESS["main_app_pass_last"] = "Última entrada";
$MESS["main_app_pass_last_ip"] = "Último IP";
$MESS["main_app_pass_manage"] = "Gerenciar";
$MESS["main_app_pass_link"] = "Conectar";
$MESS["main_app_pass_comment"] = "Comentário";
$MESS["main_app_pass_other"] = "Outro";
$MESS["main_app_pass_comment_ph"] = "Adicionar descrição";
$MESS["main_app_pass_get_pass"] = "Obter senha";
$MESS["main_app_pass_create_pass"] = "Criar senha";
$MESS["main_app_pass_create_pass_text"] = "Use esta senha para sincronizar com o aplicativo selecionado.
	A senha não será mantida na forma aberta; por isso, está disponível apenas quando obtida pelo usuário.
	Não feche a janela até que você tenha copiado ou inserido sua senha.";
$MESS["main_app_pass_create_pass_close"] = "Fechar";
$MESS["main_app_pass_del_pass"] = "Você tem certeza que deseja excluir a senha?";
$MESS["main_app_pass_del_pass_text"] = "A sincronização será abortada porque o aplicativo não poderá acessar os dados devido a erro de autenticação.";
$MESS["DAV_BUTTON_SAVE"] = "Salvar";
$MESS["DAV_ACCOUNTS_SECTION"] = "Usuários do Sistema";
$MESS["DAV_ENABLE"] = "Ativar";
$MESS["DAV_ACCOUNTS_EXPORT_DEPARTMENT"] = "Departamento";
$MESS["DAV_CONTACTS"] = "Contatos do CRM";
$MESS["DAV_COMPANIES"] = "Empresas do CRM";
$MESS["DAV_EXTRANET_ACCOUNTS"] = "Usuários da Extranet";
$MESS["DAV_MAX_COUNT"] = "Limite";
$MESS["DAV_EXPORT_FILTER"] = "Filtro";
$MESS["DAV_CARDDAV_SETTINGS_HELP"] = "O módulo DAV oferece a facilidade de sincronizar contatos do site com quaisquer aplicativos e dispositivos compatíveis com protocolo CardDAV. Por exemplo, iPhone e iPad são totalmente compatíveis com CardDAV.
<br><br>

 <h3>Ativar Conexão CardDAV no Seu Dispositivo Apple</h3>

Para configurar suporte CardDAV em dispositivos Apple, siga estes passos:
<ol>
<li>No seu dispositivo Apple, abra <b>Configurações</b> e clique na guia <b>Contas</b>.</li>
<li>Clique em <b>Adicionar Conta</b>.</li>
<li>Selecione <b>CardDAV</b> como tipo de conta.</li>
<li>Nas preferências da conta, digite o endereço do site (#SERVER#) como servidor e forneça seu login e senha.</li>
<li>Selecione <b>Autorização Básica</b> como tipo de autenticação.</li>
<li>Para especificar o número da porta opcional, salve a conta e, então, abra-a para edição.</li>
</ol>

Todos os seus contatos irão aparecer no aplicativo Contatos. Para ativar ou desativar a sincronização de contatos selecionados, use as seguintes configurações.<br>";
$MESS["DAV_COMMON_SECTION"] = "Configurações gerais";
$MESS["DAV_DEFAULT_COLLECTION_TO_SYNC"] = "Sincronizar por padrão";
?>