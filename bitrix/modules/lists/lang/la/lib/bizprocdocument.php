<?
$MESS["LISTS_BIZPROC_INVALID_INT"] = "El valor del campo no es un número entero.";
$MESS["LISTS_BIZPROC_INVALID_DATE"] = "El valor del campo no es una fecha válida.";
$MESS["LISTS_BIZPROC_INVALID_SELECT"] = "Valor del elemento de la lista no es válido.";
$MESS["LISTS_BIZPROC_ENTITY_NAME"] = "Flujos de trabajo";
?>