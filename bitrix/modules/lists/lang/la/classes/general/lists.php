<?
$MESS["LISTS_MODULE_BIZPROC_NOT_INSTALLED"] = "El módulo \"Workflows\" no está instalado.";
$MESS["LISTS_ACCESS_DENIED"] = "Acceso denegado.";
$MESS["LISTS_REQUIRED_PARAMETER"] = "Parámetro requerido \"#parameter#\" no se encuentra.";
$MESS["LISTS_COPY_IBLOCK_NAME_TITLE"] = "(Copiar)";
$MESS["LISTS_COPY_IBLOCK_ERROR_GET_DATA"] = "No se pudo obtener los datos a copiar.";
$MESS["LISTS_COPY_IBLOCK_ERROR_SET_RIGHT"] = "Error al asignar permisos para duplicar el block de información.";
$MESS["LISTS_COPY_IBLOCK_ERROR_ADD_FIELD"] = "Error al copiar el campo \"#field#\".";
?>