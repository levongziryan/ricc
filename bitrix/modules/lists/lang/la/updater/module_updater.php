<?
$MESS["NAME"] = "Flujos de trabajo";
$MESS["SECTION_NAME"] = "Secciones";
$MESS["ELEMENT_NAME"] = "Elementos";
$MESS["MU_IBLOCK_ELEMENTS_NAME"] = "Elementos";
$MESS["MU_IBLOCK_ELEMENT_NAME"] = "Elemento";
$MESS["MU_IBLOCK_ELEMENT_ADD"] = "Agregar elemento";
$MESS["MU_IBLOCK_ELEMENT_EDIT"] = "Editar elemento";
$MESS["MU_IBLOCK_ELEMENT_DELETE"] = "Eliminar elemento";
$MESS["MU_IBLOCK_SECTIONS_NAME"] = "Secciones";
$MESS["MU_IBLOCK_SECTION_NAME"] = "Sección";
$MESS["MU_IBLOCK_SECTION_ADD"] = "Agregar sección";
$MESS["MU_IBLOCK_SECTION_EDIT"] = "Editar sección";
$MESS["MU_IBLOCK_SECTION_DELETE"] = "Eliminar sección";
$MESS["MU_IBLOCK_NAME_FIELD"] = "Nombre";
$MESS["MU_MENU_TITLE_PROCESS"] = "Flujos de trabajo";
$MESS["MU_MENU_TITLE_MY_PROCESSES"] = "Mis Solicitudes";
$MESS["MU_IBLOCK_FIELD_LIST_YES"] = "Si";
$MESS["MU_IBLOCK_FIELD_LIST_NO"] = "No";
$MESS["MU_IBLOCK_NAME_HOLIDAY"] = "Solicitud de Vacaciones";
$MESS["MU_IBLOCK_DESCRIPTION_HOLIDAY"] = "No más burocracia. Enviar una solicitud de vacaciones con sólo para completar unos cuantos campos. La solicitud será enviada a su supervisor y, a continuación, pasar a través de las etapas necesarias para la completa aprobación. Una vez que su solicitud ha sido aprobada, recibirá un mensaje de notificación.

Inténtalo ahora!";
$MESS["MU_IBLOCK_NAME_MISSION"] = "Aplicación de Viaje de Negocios";
$MESS["MU_IBLOCK_DESCRIPTION_MISSION"] = "Seleccione las fechas de inicio y de finalización de su viaje de negocios, introduzca el nombre de la ciudad de destino, y describa el propósito de su viaje. El proceso de negocio va a enviar su solicitud a su supervisor. Una vez que su solicitud ha sido aprobada, recibirá un mensaje de notificación. 

Inténtelo ahora!";
$MESS["MU_IBLOCK_NAME_PAYING"] = "Factura por pagar";
$MESS["MU_IBLOCK_DESCRIPTION_PAYING"] = "Enviar una solicitud de pago mediante completando sólo unos pocos campos, o simplemente adjunte la imagen de una factura escaneada. El mismo será enviado a su supervisor y, a continuación, pasar todas las etapas necesarias para su aprobación final. Una vez que la factura ha sido pagada, recibirá una confirmación. 

Inténtalo ahora!";
$MESS["MU_IBLOCK_NAME_CASH"] = "Solicitud de Efectivo";
$MESS["MU_IBLOCK_DESCRIPTION_CASH"] = "Enviar una solicitud de dinero en efectivo completando unos pocos campos. Una vez que su solicitud ha sido aprobada o disminuida, usted recibirá un mensaje de notificación.

Inténtalo ahora!";
$MESS["MU_IBLOCK_NAME_INBOX"] = "Registrar un documento entrante";
$MESS["MU_IBLOCK_DESCRIPTION_INBOX"] = "Cargue un documento entrante o complete los campos obligatorios, y envielo a la administración. Se le asignará un número de entrada al docuemnto, y el socio administrativo será notificado inmediatamente del nuevo documento.

Inténtalo ahora!";
$MESS["MU_IBLOCK_NAME_OUTBOX"] = "Documento saliente";
$MESS["MU_IBLOCK_DESCRIPTION_OUTBOX"] = "Registrar la salida de un  documento, especifique el método sendign y el tiempo de respuesta.

Al documento se le asigna un número de salida; el secretario será notificado del nuevo documento y lo enviará al destinatario.

Inténtalo ahora!";
$MESS["MU_IBLOCK_FIELD_NAME"] = "Nombre";
?>