<?
$MESS["NAME"] = "Fluxos de trabalho";
$MESS["SECTION_NAME"] = "Seções";
$MESS["ELEMENT_NAME"] = "Elementos";
$MESS["MU_IBLOCK_ELEMENTS_NAME"] = "Elementos";
$MESS["MU_IBLOCK_ELEMENT_NAME"] = "Elemento";
$MESS["MU_IBLOCK_ELEMENT_ADD"] = "Adicionar elemento";
$MESS["MU_IBLOCK_ELEMENT_EDIT"] = "Editar elemento";
$MESS["MU_IBLOCK_ELEMENT_DELETE"] = "Excluir elemento";
$MESS["MU_IBLOCK_SECTIONS_NAME"] = "Seções";
$MESS["MU_IBLOCK_SECTION_NAME"] = "Seção";
$MESS["MU_IBLOCK_SECTION_ADD"] = "Adicionar seção";
$MESS["MU_IBLOCK_SECTION_EDIT"] = "Editar seção";
$MESS["MU_IBLOCK_SECTION_DELETE"] = "Excluir seção";
$MESS["MU_IBLOCK_NAME_FIELD"] = "Nome";
$MESS["MU_MENU_TITLE_PROCESS"] = "Fluxos de trabalho";
$MESS["MU_MENU_TITLE_MY_PROCESSES"] = "Meus Pedidos";
$MESS["MU_IBLOCK_FIELD_LIST_YES"] = "Sim";
$MESS["MU_IBLOCK_FIELD_LIST_NO"] = "Não";
$MESS["MU_IBLOCK_NAME_HOLIDAY"] = "Pedido de Férias";
$MESS["MU_IBLOCK_DESCRIPTION_HOLIDAY"] = "Sem burocracia! Envie um pedido de férias apenas preenchendo alguns campos. Seu pedido será enviado para o seu supervisor e, em seguida, passará pelas etapas necessárias para completar a aprovação. Uma vez que seu pedido for aprovado, você receberá uma mensagem de notificação.

Experimente agora!";
$MESS["MU_IBLOCK_NAME_MISSION"] = "Aplicativo de Viagem de Negócios";
$MESS["MU_IBLOCK_DESCRIPTION_MISSION"] = "Selecione as datas de início e de término para sua viagem de negócios, digite a cidade de destino e descreva a finalidade da sua viagem. O processo de negócio irá enviar seu pedido para o seu supervisor. Uma vez que seu pedido foi aprovado, você receberá uma mensagem de notificação.

Experimente agora!";
$MESS["MU_IBLOCK_NAME_PAYING"] = "Nota Fiscal Devida";
$MESS["MU_IBLOCK_DESCRIPTION_PAYING"] = "Envie um pedido de pagamento, preenchendo apenas alguns campos, ou simplesmente anexe uma imagem digitalizada da nota fiscal. Ele será enviado para o seu supervisor e, em seguida, passará por todas as etapas necessárias para a aprovação final. Uma vez que a nota fiscal for paga, você receberá uma confirmação.

Experimente agora!";
$MESS["MU_IBLOCK_NAME_CASH"] = "Pedido de Dinheiro";
$MESS["MU_IBLOCK_DESCRIPTION_CASH"] = "Envie um pedido de dinheiro, preenchendo apenas alguns campos. Uma vez que seu pedido for aprovado ou rejeitado, você receberá uma mensagem de notificação.

Experimente agora!";
$MESS["MU_IBLOCK_NAME_INBOX"] = "Registre um Documento Recebido";
$MESS["MU_IBLOCK_DESCRIPTION_INBOX"] = "Carregue um documento recebido ou preencha os campos necessários, e o envie para a administração. O documento receberá um número de inscrição e a área administrativa associada será imediatamente notificada de um novo documento.

Experimente agora!";
$MESS["MU_IBLOCK_NAME_OUTBOX"] = "Documento Emitido";
$MESS["MU_IBLOCK_DESCRIPTION_OUTBOX"] = "Registre um documento emitido, especifique o método de envio e o tempo de resposta.

O documento receberá um número de emissão; a secretária será notificada de um novo documento e irá enviá-lo ao destinatário.

Experimente agora!";
$MESS["MU_IBLOCK_FIELD_NAME"] = "Nome";
?>