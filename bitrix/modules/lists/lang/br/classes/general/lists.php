<?
$MESS["LISTS_MODULE_BIZPROC_NOT_INSTALLED"] = "O módulo \"Fluxos de trabalho\" não está instalado.";
$MESS["LISTS_ACCESS_DENIED"] = "Acesso negado.";
$MESS["LISTS_REQUIRED_PARAMETER"] = "O parâmetro obrigatório \"#parameter#\" está faltando.";
$MESS["LISTS_COPY_IBLOCK_NAME_TITLE"] = "(Copiar)";
$MESS["LISTS_COPY_IBLOCK_ERROR_GET_DATA"] = "Não foi possível obter dados para copiar.";
$MESS["LISTS_COPY_IBLOCK_ERROR_SET_RIGHT"] = "Erro ao atribuir permissões para bloquear informações duplicadas.";
$MESS["LISTS_COPY_IBLOCK_ERROR_ADD_FIELD"] = "Erro ao copiar o campo \"#CAMPO#\".";
?>