<?
$MESS["LISTS_SOCNET_TAB"] = "Listas";
$MESS["LISTS_SOCNET_LOG_GROUP"] = "Listas";
$MESS["LISTS_DEL_SOCNET_LOG_GROUP"] = "Listas (exclusão)";
$MESS["LISTS_SOCNET_LOG_GROUP_SETTINGS"] = "Todas as alterações nas listas deste grupo";
$MESS["LISTS_SOCNET_LOG_TITLE"] = "Item da lista \"#TITLE#\" adicionados ou atualizados";
$MESS["LISTS_SOCNET_LOG_TITLE_MAIL"] = "Item da lista \"#TITLE#\" adicionados ou atualizados no grupo #ENTITY#";
$MESS["LISTS_DEL_SOCNET_LOG_TITLE"] = "Item da lista \"#TITLE#\" excluído";
$MESS["LISTS_DEL_SOCNET_LOG_TITLE_MAIL"] = "Item da lista \"#TITLE#\" excluído no grupo #ENTITY#";
?>