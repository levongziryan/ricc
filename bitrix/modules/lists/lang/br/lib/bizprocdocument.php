<?
$MESS["LISTS_BIZPROC_INVALID_INT"] = "O valor do campo não é um número inteiro. ";
$MESS["LISTS_BIZPROC_INVALID_DATE"] = "O valor do campo não é uma data válida. ";
$MESS["LISTS_BIZPROC_INVALID_SELECT"] = "Valor do item da lista inválido.";
$MESS["LISTS_BIZPROC_ENTITY_NAME"] = "Fluxos de trabalho";
?>