<?
$MESS["LISTS_LF_ADD_COMMENT_SOURCE_ERROR"] = "Falha ao adicionar um comentário a uma fonte de evento..";
$MESS["LISTS_LF_MOBILE_DESTINATION"] = "Para";
$MESS["LISTS_LF_COMMENT_MESSAGE_ADD"] = "Adicionar um comentário ao seu '#PROCESS#' de fluxo de trabalho";
$MESS["LISTS_LF_COMMENT_MESSAGE_ADD_F"] = "Adicionar um comentário ao seu '#PROCESS#' de fluxo de trabalho";
$MESS["LISTS_LF_COMMENT_MESSAGE_ADD_M"] = "Adicionar um comentário ao seu '#PROCESS#' de fluxo de trabalho";
$MESS["LISTS_LF_COMMENT_MENTION_M"] = "Mencionou você em um comentário sobre o fluxo de trabalho #title#";
$MESS["LISTS_LF_COMMENT_MENTION_F"] = "Mencionou você em um comentário sobre o fluxo de trabalho #title#";
$MESS["LISTS_LF_COMMENT_MENTION"] = "Mencionou você em um comentário sobre o fluxo de trabalho #title#";
$MESS["LISTS_LF_COMMENT_MENTION_TITLE"] = "\"#PROCESS#\"";
?>