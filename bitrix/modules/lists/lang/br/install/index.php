<?
$MESS["LISTS_MODULE_NAME"] = "Listas comuns";
$MESS["LISTS_MODULE_DESCRIPTION"] = "Este módulo ajuda trabalhar com informações na seção pública.";
$MESS["LISTS_INSTALL_TITLE"] = "Instalação do módulo Listas Comuns";
$MESS["LISTS_UNINSTALL_TITLE"] = "Desinstalação do módulo Listas Comuns";
$MESS["LISTS_TYPE_NAME"] = "listas";
$MESS["LISTS_ELEMENT_NAME"] = "elementos";
$MESS["LISTS_SECTION_NAME"] = "Seções";
$MESS["LISTS_SOCNET_TYPE_NAME"] = "Listas Socialnetwork";
$MESS["LISTS_SOCNET_ELEMENT_NAME"] = "elementos";
$MESS["LISTS_SOCNET_SECTION_NAME"] = "Seções";
$MESS["BITRIX_PROCESSES_TYPE_NAME"] = "Processos";
$MESS["BITRIX_PROCESSES_ELEMENT_NAME"] = "Elementos";
$MESS["BITRIX_PROCESSES_SECTION_NAME"] = "Seções";
?>