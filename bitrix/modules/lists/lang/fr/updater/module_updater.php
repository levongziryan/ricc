<?
$MESS["SECTION_NAME"] = "Sections";
$MESS["ELEMENT_NAME"] = "Des documents";
$MESS["MU_IBLOCK_ELEMENTS_NAME"] = "Des documents";
$MESS["MU_IBLOCK_ELEMENT_NAME"] = "Elément";
$MESS["MU_IBLOCK_ELEMENT_DELETE"] = "Suppression d'un élément";
$MESS["MU_IBLOCK_SECTIONS_NAME"] = "Sections";
$MESS["MU_IBLOCK_SECTION_NAME"] = "Créer une section";
$MESS["MU_IBLOCK_SECTION_ADD"] = "Ajouter une section";
$MESS["MU_IBLOCK_SECTION_EDIT"] = "Modification de section";
$MESS["MU_IBLOCK_SECTION_DELETE"] = "Suppression de section";
$MESS["MU_IBLOCK_NAME_FIELD"] = "Dénomination";
$MESS["MU_IBLOCK_FIELD_LIST_YES"] = "Oui";
$MESS["MU_IBLOCK_FIELD_LIST_NO"] = "Non";
$MESS["MU_IBLOCK_FIELD_NAME"] = "Dénomination";
$MESS["NAME"] = "Workflows";
$MESS["MU_MENU_TITLE_PROCESS"] = "Workflows";
$MESS["MU_MENU_TITLE_MY_PROCESSES"] = "Mes Demandes";
$MESS["MU_IBLOCK_NAME_HOLIDAY"] = "Demande vacances";
$MESS["MU_IBLOCK_DESCRIPTION_HOLIDAY"] = "Tape Pas plus rouge! Envoyer une requête de vacances juste en remplissant quelques champs. Votre demande sera envoyée à votre superviseur et ensuite passer à travers les étapes nécessaires à l'approbation complète. Une fois que votre demande a été approuvée, vous recevrez une notification message. Essayez-le maintenant! ";
$MESS["MU_IBLOCK_NAME_MISSION"] = "Voyage d'affaires application";
$MESS["MU_IBLOCK_DESCRIPTION_MISSION"] = "Sélectionnez dates de début et de fin de votre voyage d'affaires, entrer dans la ville de destination, et de décrire le but de votre voyage. Le processus de gestion enverra votre demande à votre superviseur. Une fois que votre demande a été approuvée, vous recevrez un message de notification.

Essayez-le maintenant! 
";
$MESS["MU_IBLOCK_NAME_PAYING"] = "Payer la facture";
$MESS["MU_IBLOCK_DESCRIPTION_PAYING"] = "Envoyer une demande de paiement en remplissant quelques champs, ou tout simplement joindre l'image numérisée de la facture. Il sera envoyé à votre superviseur, puis passer toutes les étapes requises pour l'approbation finale. Une fois que la facture a été payée, vous recevrez une confirmation.

Essayez-le maintenant! 
";
$MESS["MU_IBLOCK_NAME_CASH"] = "Demande de trésorerie";
$MESS["MU_IBLOCK_DESCRIPTION_CASH"] = "Envoyer une demande pour de l'argent comptant en remplissant quelques champs. Une fois que votre demande a été acceptée ou rejetée, vous recevrez un message de notification.

Essayez-le maintenant! 
";
$MESS["MU_IBLOCK_NAME_INBOX"] = "Créer un document entrant";
$MESS["MU_IBLOCK_DESCRIPTION_INBOX"] = "Ajouter un document entrant ou remplissez les champs requis, et l'envoyer à l'administration. Le document sera attribué un numéro d'entrée, et l'associé administratif sera immédiatement informé du nouveau document.

Essayez-le maintenant!
";
$MESS["MU_IBLOCK_NAME_OUTBOX"] = "Document sortant";
$MESS["MU_IBLOCK_DESCRIPTION_OUTBOX"] = "Créer un document sortant, spécifier la méthode sendign et le temps de réponse.

Le document sera attribué un numéro sortant; le secrétaire sera avisé du nouveau document et de l'envoyer au destinataire.

Essayez-le maintenant! ";
$MESS["MU_IBLOCK_ELEMENT_ADD"] = "Ajouter élément";
$MESS["MU_IBLOCK_ELEMENT_EDIT"] = "Modifier élément";
?>