<?
$MESS["LISTS_MODULE_DESCRIPTION"] = "Module pour organiser le travail avec de différentes informations de la partie publique du site.";
$MESS["LISTS_SECTION_NAME"] = "Sections";
$MESS["LISTS_SOCNET_SECTION_NAME"] = "Sections";
$MESS["LISTS_TYPE_NAME"] = "Listes";
$MESS["LISTS_SOCNET_TYPE_NAME"] = "Les listes des groupes des réseaux sociaux";
$MESS["LISTS_UNINSTALL_TITLE"] = "Suppression des Listes universelles";
$MESS["LISTS_MODULE_NAME"] = "Listes universelles";
$MESS["LISTS_INSTALL_TITLE"] = "Installation de module Listes universelles";
$MESS["LISTS_ELEMENT_NAME"] = "Des documents";
$MESS["LISTS_SOCNET_ELEMENT_NAME"] = "Des documents";
$MESS["BITRIX_PROCESSES_TYPE_NAME"] = "Processus";
$MESS["BITRIX_PROCESSES_ELEMENT_NAME"] = "Des documents";
$MESS["BITRIX_PROCESSES_SECTION_NAME"] = "Sections";
?>