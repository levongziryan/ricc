<?
$MESS["LISTS_LIST_FIELD_TIMESTAMP_X"] = "Modifier élément:";
$MESS["LISTS_LIST_FIELD_DATE_CREATE"] = "Date de création";
$MESS["LISTS_LIST_FIELD_DETAIL_PICTURE"] = "Affichage détaillé";
$MESS["LISTS_LIST_FIELD_DETAIL_TEXT"] = "Description détaillée";
$MESS["LISTS_LIST_FIELD_PREVIEW_PICTURE"] = "Image de l'annonce";
$MESS["LISTS_LIST_FIELD_MODIFIED_BY"] = "Modifié(e)s par";
$MESS["LISTS_LIST_FIELD_CREATED_BY"] = "Créé par";
$MESS["LISTS_LIST_FIELD_NAME"] = "Dénomination";
$MESS["LISTS_LIST_FIELD_ACTIVE_FROM"] = "Début de l'activité";
$MESS["LISTS_LIST_FIELD_ACTIVE_TO"] = "Fin de l'activité";
$MESS["LISTS_LIST_FIELD_G"] = "Rattachement aux sections";
$MESS["LISTS_LIST_FIELD_E"] = "Rattachement aux éléments";
$MESS["LISTS_LIST_FIELD_SORT"] = "Trier";
$MESS["LISTS_LIST_FIELD_L"] = "Liste";
$MESS["LISTS_LIST_FIELD_S"] = "Ligne";
$MESS["LISTS_LIST_FIELD_PREVIEW_TEXT"] = "Texte de l'annonce";
$MESS["LISTS_LIST_FIELD_F"] = "Fichier";
$MESS["LISTS_LIST_FIELD_N"] = "Chiffre";
?>