<?
$MESS["LISTS_SOCNET_LOG_TITLE_MAIL"] = "Un élément de la liste '#TITLE#' est ajouté ou changé dans le groupe '#ENTITY#'";
$MESS["LISTS_DEL_SOCNET_LOG_TITLE_MAIL"] = "Dans le groupe '# ENTITY#' l'élément de la liste '# TITLE#' a été supprimé";
$MESS["LISTS_SOCNET_LOG_GROUP_SETTINGS"] = "Toutes les modifications dans les listes de ce groupe";
$MESS["LISTS_SOCNET_LOG_TITLE"] = "L'élément de la liste '#TITLE#' est ajouté ou modifié";
$MESS["LISTS_SOCNET_TAB"] = "Listes";
$MESS["LISTS_SOCNET_LOG_GROUP"] = "Listes";
$MESS["LISTS_DEL_SOCNET_LOG_GROUP"] = "Listes (suppression)";
$MESS["LISTS_DEL_SOCNET_LOG_TITLE"] = "Un élément a été éliminé de la liste '#TITLE#'";
?>