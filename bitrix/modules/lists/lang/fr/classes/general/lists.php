<?
$MESS["LISTS_MODULE_BIZPROC_NOT_INSTALLED"] = "Le module \"Flux de travail\" n'est pas installé.";
$MESS["LISTS_ACCESS_DENIED"] = "Accès refusé.";
$MESS["LISTS_REQUIRED_PARAMETER"] = "Le paramètre \"#parameter#\" requis est manquant.";
$MESS["LISTS_COPY_IBLOCK_NAME_TITLE"] = "(Copier)";
$MESS["LISTS_COPY_IBLOCK_ERROR_GET_DATA"] = "Impossible d'obtenir la donnée à copier.";
$MESS["LISTS_COPY_IBLOCK_ERROR_SET_RIGHT"] = "Erreur lors de l'assignation des permissions de dupliquer les blocs d'information.";
$MESS["LISTS_COPY_IBLOCK_ERROR_ADD_FIELD"] = "Erreur lors de la copie du champ \"#field#\".";
?>