<?
$MESS["LISTS_LF_ADD_COMMENT_SOURCE_ERROR"] = "Impossible d'ajouter un commentaire à une source d'événement..";
$MESS["LISTS_LF_MOBILE_DESTINATION"] = "Dans";
$MESS["LISTS_LF_COMMENT_MESSAGE_ADD"] = "Ajout d'un commentaire à votre '#PROCESSUS#' de flux de travail";
$MESS["LISTS_LF_COMMENT_MESSAGE_ADD_F"] = "Ajout d'un commentaire à votre '#PROCESSUS#' de flux de travail";
$MESS["LISTS_LF_COMMENT_MESSAGE_ADD_M"] = "Ajout d'un commentaire à votre '#PROCESSUS#' de flux de travail";
$MESS["LISTS_LF_COMMENT_MENTION_M"] = "Vous a mentionné dans un commentaire sur le flux de travail #title#";
$MESS["LISTS_LF_COMMENT_MENTION_F"] = "Vous a mentionné dans un commentaire sur le flux de travail #title#";
$MESS["LISTS_LF_COMMENT_MENTION"] = "Vous a mentionné dans un commentaire sur le flux de travail #title#";
$MESS["LISTS_LF_COMMENT_MENTION_TITLE"] = "\"#PROCESS#\"";
?>