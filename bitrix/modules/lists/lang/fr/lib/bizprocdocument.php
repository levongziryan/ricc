<?
$MESS["LISTS_BIZPROC_INVALID_INT"] = "La valeur du champ est pas un entier.";
$MESS["LISTS_BIZPROC_INVALID_DATE"] = "La valeur de champ est pas une date valide.";
$MESS["LISTS_BIZPROC_INVALID_SELECT"] = "Valeur d'élément de liste invalide.";
$MESS["LISTS_BIZPROC_ENTITY_NAME"] = "Flux de travail";
?>