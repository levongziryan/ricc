<?
$MESS["LISTS_OPTIONS_CHOOSE_GROUP"] = "(sélectionner le groupe)";
$MESS["LISTS_OPTIONS_CHOOSE_TYPE"] = "(selectionner un type un bloc d'informations)";
$MESS["LISTS_OPTIONS_SOCNET_ENABLE"] = "Activer le support du réseau social";
$MESS["LISTS_OPTIONS_USER_GROUPS"] = "Groupe d'utilisateurs";
$MESS["LISTS_OPTIONS_ADD_RIGHT"] = "Ajouter le droit d'accès";
$MESS["LISTS_OPTIONS_TAB_TITLE_SOCNET"] = "Réglages de l'intégration du module des listes universelles dans un réseau social";
$MESS["LISTS_OPTIONS_TAB_PERMISSIONS"] = "Droits d'accès";
$MESS["LISTS_OPTIONS_TAB_SOCNET"] = "Réseau social";
$MESS["LISTS_OPTIONS_SOCNET_IBLOCK_TYPE"] = "Type de bloc d'information";
$MESS["LISTS_OPTIONS_IBLOCK_TYPES"] = "Type de bloc d'information";
$MESS["LISTS_OPTIONS_TAB_TITLE_PERMISSIONS"] = "Indiquez les groupes d'utilisateurs qui peuvent gérer les listes des certains types de blocs d'information";
$MESS["LISTS_OPTIONS_TAB_LIVE_FEED"] = "Flux d'activités";
$MESS["LISTS_OPTIONS_TAB_TITLE_LIVE_FEED"] = "Liste configurer pour une utilisation avec actions";
$MESS["LISTS_OPTIONS_LIVE_FEED_IBLOCK_TYPE"] = "Informations type de bloc pour une utilisation dans Actions";
$MESS["LISTS_OPTIONS_LIVE_FEED_SEF_FOLDER"] = "Répertoire de travail Composant";
$MESS["LISTS_OPTIONS_LIVE_FEED_CHECK"] = "Activer l'intégration avec les Actions";
?>