<?
$MESS["LISTS_MODULE_NAME"] = "Allgemeine Listen";
$MESS["LISTS_MODULE_DESCRIPTION"] = "Dieses Modul hilft Ihnen mit Information im öffentlichen Bereich zu arbeiten.";
$MESS["LISTS_INSTALL_TITLE"] = "Installation des Listenmoduls ";
$MESS["LISTS_UNINSTALL_TITLE"] = "Entfernung des Listenmoduls";
$MESS["LISTS_TYPE_NAME"] = "Listen";
$MESS["LISTS_ELEMENT_NAME"] = "Elemente";
$MESS["LISTS_SECTION_NAME"] = "Bereiche";
$MESS["LISTS_SOCNET_TYPE_NAME"] = "Die Listen für das soziale Netzwerk";
$MESS["LISTS_SOCNET_ELEMENT_NAME"] = "Elemente";
$MESS["LISTS_SOCNET_SECTION_NAME"] = "Bereiche";
$MESS["BITRIX_PROCESSES_TYPE_NAME"] = "Prozesse";
$MESS["BITRIX_PROCESSES_ELEMENT_NAME"] = "Elemente";
$MESS["BITRIX_PROCESSES_SECTION_NAME"] = "Bereiche";
?>