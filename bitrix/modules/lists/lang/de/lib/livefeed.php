<?
$MESS["LISTS_LF_ADD_COMMENT_SOURCE_ERROR"] = "Ein Kommentar zur Ereignisquelle konnte hinzugefügt werden.";
$MESS["LISTS_LF_MOBILE_DESTINATION"] = "An";
$MESS["LISTS_LF_COMMENT_MESSAGE_ADD"] = "Hat einen Kommentar zum Workflow \"#PROCESS#\" hinzugefügt";
$MESS["LISTS_LF_COMMENT_MESSAGE_ADD_F"] = "Hat einen Kommentar zum Workflow \"#PROCESS#\" hinzugefügt";
$MESS["LISTS_LF_COMMENT_MESSAGE_ADD_M"] = "Hat einen Kommentar zum Workflow \"#PROCESS#\" hinzugefügt";
$MESS["LISTS_LF_COMMENT_MENTION_M"] = "Hat Sie im Kommentar zum Workflow #title# erwähnt";
$MESS["LISTS_LF_COMMENT_MENTION_F"] = "Hat Sie im Kommentar zum Workflow #title# erwähnt";
$MESS["LISTS_LF_COMMENT_MENTION"] = "Hat Sie im Kommentar zum Workflow #title# erwähnt";
$MESS["LISTS_LF_COMMENT_MENTION_TITLE"] = "\"#PROCESS#\"";
?>