<?
$MESS["LISTS_LIST_FIELD_NAME"] = "Name";
$MESS["LISTS_LIST_FIELD_SORT"] = "Sortieren";
$MESS["LISTS_LIST_FIELD_ACTIVE_FROM"] = "Aktiv seit";
$MESS["LISTS_LIST_FIELD_ACTIVE_TO"] = "Aktiv bis";
$MESS["LISTS_LIST_FIELD_PREVIEW_PICTURE"] = "Bildvorschau";
$MESS["LISTS_LIST_FIELD_PREVIEW_TEXT"] = "Textvorschau";
$MESS["LISTS_LIST_FIELD_DETAIL_PICTURE"] = "Bild";
$MESS["LISTS_LIST_FIELD_DETAIL_TEXT"] = "Text";
$MESS["LISTS_LIST_FIELD_DATE_CREATE"] = "Erstellt";
$MESS["LISTS_LIST_FIELD_CREATED_BY"] = "Erstellt von";
$MESS["LISTS_LIST_FIELD_TIMESTAMP_X"] = "Geändert";
$MESS["LISTS_LIST_FIELD_MODIFIED_BY"] = "Geändert von";
$MESS["LISTS_LIST_FIELD_S"] = "Zeile";
$MESS["LISTS_LIST_FIELD_N"] = "Nummer";
$MESS["LISTS_LIST_FIELD_L"] = "Liste";
$MESS["LISTS_LIST_FIELD_F"] = "Datei";
$MESS["LISTS_LIST_FIELD_G"] = "Anbinden an Bereiche";
$MESS["LISTS_LIST_FIELD_E"] = "Anbinden an Elemente";
?>