<?
$MESS["LISTS_OPTIONS_TAB_PERMISSIONS"] = "Zugriffsrechte";
$MESS["LISTS_OPTIONS_TAB_TITLE_PERMISSIONS"] = "Nutzerguppen auswählen, die die Listen in verschiedenen Informationsblocktypen verwalten dürfen.";
$MESS["LISTS_OPTIONS_CHOOSE_GROUP"] = "(Wählen Sie die Gruppe aus)";
$MESS["LISTS_OPTIONS_USER_GROUPS"] = "Nutzergruppe";
$MESS["LISTS_OPTIONS_CHOOSE_TYPE"] = "(Wählen Sie den Typ des Informationsblocks aus)";
$MESS["LISTS_OPTIONS_IBLOCK_TYPES"] = "Typen der Informationsblöcke";
$MESS["LISTS_OPTIONS_TAB_SOCNET"] = "Soziales Netzwerk";
$MESS["LISTS_OPTIONS_TAB_TITLE_SOCNET"] = "Einstellungen der Integration des Universallistemoduls in das soziale Netzwerk";
$MESS["LISTS_OPTIONS_SOCNET_ENABLE"] = "Unterstützung für soziales Netzwerk einschalten";
$MESS["LISTS_OPTIONS_SOCNET_IBLOCK_TYPE"] = "Typ des Informationsblocks";
$MESS["LISTS_OPTIONS_ADD_RIGHT"] = "Zugriffsberechtigung hinzufügen";
$MESS["LISTS_OPTIONS_TAB_LIVE_FEED"] = "Activity Stream";
$MESS["LISTS_OPTIONS_TAB_TITLE_LIVE_FEED"] = "Konfiguration der Liste zur Nutzung mit dem Activity Stream";
$MESS["LISTS_OPTIONS_LIVE_FEED_IBLOCK_TYPE"] = "Informationsblocktyp zur Nutzung mit dem Activity Stream";
$MESS["LISTS_OPTIONS_LIVE_FEED_SEF_FOLDER"] = "Verzeichnis, in dem die Komponente arbeteit";
$MESS["LISTS_OPTIONS_LIVE_FEED_CHECK"] = "Integration mit Activity Stream aktivieren";
?>