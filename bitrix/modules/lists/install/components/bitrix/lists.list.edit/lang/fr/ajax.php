<?
$MESS["LISTS_WRONG_IBLOCK_TYPE"] = "Type de bloc information incorrect.";
$MESS["LISTS_WRONG_IBLOCK"] = "La liste spécifiée est incorrecte.";
$MESS["LISTS_FOR_SONET_GROUP_DISABLED"] = "Les listes sont désactivées pour ce groupe.";
$MESS["LISTS_UNKNOWN_ERROR"] = "Erreur inconnue.";
$MESS["LISTS_ACCESS_DENIED"] = "Vous ne disposez pas des permissions pour voir et éditer la liste.";
$MESS["LISTS_MESSAGE_SUCCESS"] = "Réussi";
$MESS["LISTS_STATUS_ACTION_SUCCESS"] = "Réussi";
$MESS["LISTS_STATUS_ACTION_ERROR"] = "C'est une erreur.";
?>