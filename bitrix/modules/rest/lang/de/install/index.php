<?
$MESS["REST_MODULE_NAME"] = "REST API";
$MESS["REST_MODULE_DESCRIPTION"] = "Programmierschnittstelle für interne und externe Anwendungen.";
$MESS["REST_INSTALL_TITLE"] = "Installation des Moduls REST API";
$MESS["REST_UNINSTALL_TITLE"] = "Deinstallation des Moduls REST API";
$MESS["REST_DB_NOT_SUPPORTED"] = "Dieses Modul unterstützt nur MySQL.";
?>