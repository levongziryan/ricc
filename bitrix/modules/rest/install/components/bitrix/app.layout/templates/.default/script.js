;(function(){
BX.appLayout = {
	params: {},

	userSelectorControl: [null, null],
	userSelectorControlCallback: null,
	bAccessLoaded: false,

	_appOptionsStack: [],

	init: function(params)
	{
		BX.appLayout.params = {
			firstRun: !!params.firstRun,
			appHost: params.appHost,
			appProto: params.appProto,
			authId: params.authId,
			authExpires: params.authExpires,
			refreshId: params.refreshId,
			formName: params.formName,
			frameName: params.frameName,
			isAdmin: !!params.isAdmin,
			staticHtml: !!params.staticHtml,
			appId: params.appId,
			appV: params.appV,
			appI: params.appI,
			appSid: params.appSid,
			memberId: params.memberId,
			restPath: params.restPath,
			proto: params.proto,
			userOptions: params.userOptions,
			appOptions: params.appOptions
		};

		setTimeout(function(){
			BX.bind(BX(BX.appLayout.params.frameName), 'load', function(){

				BX.addClass(BX('appframe_loading'), 'app-loading-msg-loaded');
				BX.removeClass(this, 'app-loading');

				setTimeout(function(){
					BX.remove(BX('appframe_loading'));
				}, 300);
			});

			if(BX.appLayout.params.staticHtml)
			{
				window[BX.appLayout.params.frameName].src = document.forms[BX.appLayout.params.formName].action;
			}
			else
			{
				document.forms[BX.appLayout.params.formName].submit();
			}
		}, 20);
	},

	messageInterface: {
		refreshAuth: function(params, cb)
		{
			params = {action:'access_refresh',sessid:BX.bitrix_sessid()};
			BX.ajax.loadJSON(location.href, params, function(data){
				if(!!data['access_token'])
				{
					BX.appLayout.params.authId = data['access_token'];
					BX.appLayout.params.authExpires = data['expires_in'];
					BX.appLayout.params.refreshId = data['refresh_token'];
					cb({
						AUTH_ID:BX.appLayout.params.authId,
						AUTH_EXPIRES:BX.appLayout.params.authExpires,
						REFRESH_ID:BX.appLayout.params.refreshId
					});
				}
				else
				{
					alert('Unable to get new token! Reload page, please!');
				}
			});
		},
		resizeWindow: function(params, cb){
			var f = BX(BX.appLayout.params.frameName);
			params.width = params.width=='100%'?params.width:((parseInt(params.width)||100)+'px');
			params.height = parseInt(params.height);

			if(!!params.width)
				f.style.width = params.width;
			if(!!params.height)
				f.style.height = params.height + 'px';

			var p = BX.pos(f);
			cb({width: p.width, height: p.height});
		},
		setTitle: function(params, cb)
		{
			BX.ajax.UpdatePageTitle(params.title);
			cb(params);
		},
		setScroll: function(params, cb)
		{
			if(!!params && typeof params.scroll != 'undefined' && params.scroll >= 0)
			{
				window.scrollTo(BX.GetWindowScrollPos().scrollLeft, parseInt(params.scroll));
			}
			cb(params);
		},
		getInitData: function(params, cb)
		{
			cb({
				LANG: BX.message('LANGUAGE_ID'),
				DOMAIN: location.host,
				PROTOCOL: BX.appLayout.params.proto,
				PATH: BX.appLayout.params.restPath,
				AUTH_ID: BX.appLayout.params.authId,
				AUTH_EXPIRES: BX.appLayout.params.authExpires,
				REFRESH_ID: BX.appLayout.params.refreshId,
				MEMBER_ID: BX.appLayout.params.memberId,
				FIRST_RUN: BX.appLayout.params.firstRun,
				IS_ADMIN: BX.appLayout.params.isAdmin,
				INSTALL: BX.appLayout.params.appI,
				USER_OPTIONS: BX.appLayout.params.userOptions,
				APP_OPTIONS: BX.appLayout.params.appOptions
			});
			BX.appLayout.params.firstRun = false;
		},
		setUserOption: function(params, cb)
		{
			BX.appLayout.params.userOptions[params.name] = params.value;
			BX.userOptions.save('app_options', 'options_' + BX.appLayout.params.appId, params.name, params.value);
			cb(params);
		},
		setAppOption: function(params, cb)
		{
			if(BX.appLayout.params.isAdmin)
			{
				BX.appLayout._appOptionsStack.push([params.name,params.value, cb]);
				BX.defer(BX.appLayout.sendAppOptions)();
			}
		},
		setInstall: function(params, cb)
		{
			BX.userOptions.save('app_options', 'params_' + BX.appLayout.params.appId + '_' + BX.appLayout.params.appV, 'install', !!params['install'] ? 1 : 0);
			cb(params);
		},
		setInstallFinish: function(params, cb)
		{
			var p = {action:'set_installed',v:typeof params.value=='undefined'||params.value!==false?'Y':'N',sessid:BX.bitrix_sessid()};
			BX.ajax.loadJSON(location.href, p, function(data){
				var h=window.location.href.replace(/(\?|&)install_finished=[^&]*/ig,'$1');
				window.location=(h+(h.indexOf('?')==-1?'?':'&')+'install_finished='+(!!data.result?'Y':'N')).replace('&&','&').replace('?&','?');
			});
		},
		selectUser: function(params, cb)
		{
			BX.appLayout.userSelectorControlCallback = cb;

			var mult = parseInt(params.mult+0);

			if(!BX.appLayout.userSelectorControl[parseInt(params.mult+0)])
			{
				var p = {
					name: 'USER_' + mult,
					onchange: "user_selector_cb_" + (parseInt(Math.random() * 100000))
				};

				if(mult)
					p.mult = true;

				window[p.onchange] = BX.appLayout['selectUserCallback_' + mult];

				BX.appLayout.loadControl('user_selector', p, function(result){
					BX.appLayout.userSelectorControl[mult] = BX.PopupWindowManager.create("app-user-popup-" + mult, null, {
							autoHide: true,
							//titleBar: true,
							content: result
					});
					if(mult)
					{
						BX.appLayout.userSelectorControl[mult].setButtons([
							new BX.PopupWindowButton({
								text : BX.message('USER_SELECT'),
								className: "popup-window-button-accept",
								events: {
									click: function() {
										BX.appLayout.selectUserCallback_1(true)
									}
								}
							})
						]);
					}
					BX.appLayout.messageInterface.selectUser(params, cb);
				})
			}
			else
			{
				if(!BX.appLayout.userSelectorControl[parseInt(params.mult+0)].isShown())
				{
					BX.appLayout.userSelectorControl[parseInt(params.mult+0)].show();
				}

				BX('USER_' + mult + '_selector_content').style.display = 'block';
			}
		},
		selectAccess: function(params, cb)
		{
			if(!BX.appLayout.bAccessLoaded)
			{
				BX.appLayout.loadControl('access_selector', {}, function()
				{
					BX.appLayout.bAccessLoaded = true;
					BX.defer(BX.appLayout.messageInterface.selectAccess)(params, cb);
				})
			}
			else
			{
				BX.Access.Init({
					groups: {disabled:true}
				});

				params.value = params.value||[];
				var startValue = {};
				for(var i = 0; i < params.value.length; i++)
				{
					startValue[params.value[i]] = true;
				}

				BX.Access.SetSelected(startValue);
				BX.Access.ShowForm({callback:function(arRights)
				{
					var res = [];

					for(var provider in arRights)
					{
						if(arRights.hasOwnProperty(provider))
						{
							for(var id in arRights[provider])
							{
								if(arRights[provider].hasOwnProperty(id))
								{
									res.push(arRights[provider][id]);
								}
							}
						}
					}

					cb(res);
				}});
			}
		},
		selectCRM: function(params, cb, loaded)
		{
			if(!loaded)
			{
				BX.appLayout.loadControl('crm_selector', {
					entityType: params.entityType,
					multiple: !!params.multiple ? 'Y' : 'N',
					value:params.value
				}, BX.defer(function()
				{
					BX.appLayout.messageInterface.selectCRM(params, cb, true);
				}));

				return;
			}

			if(!window.obCrm)
			{
				setTimeout(function()
				{
					BX.appLayout.messageInterface.selectCRM(params, cb, true);
				}, 500);
			}
			else
			{
				obCrm['restCrmSelector'].Open();
				obCrm['restCrmSelector'].AddOnSaveListener(function(result)
				{
					cb(result);
				});
			}
		},
		reloadWindow: function()
		{
			window.location.reload();
		},
		imCallTo: function(params)
		{
			BXIM.callTo(params.userId, !!params.video)
		},
		imPhoneTo: function(params)
		{
			BXIM.phoneTo(params.phone)
		},
		imOpenMessenger: function(params)
		{
			BXIM.openMessenger(params.dialogId)
		},
		imOpenHistory: function(params)
		{
			BXIM.openHistory(params.dialogId)
		}
	},
	receiveMessage: function(e)
	{
		e = e||window.event;

		if(e.origin != BX.appLayout.params.appProto+'://'+BX.appLayout.params.appHost)
			return;

		var cmd = BX.appLayout.split(e.data, ':'),
			args = [];

		if(cmd[3] != BX.appLayout.params.appSid)
			return;

		if(cmd[1])
			args = JSON.parse(cmd[1]);

		if(!!BX.appLayout.messageInterface[cmd[0]])
		{
			var cb = cmd[2];
			var _cb = function(res)
			{
				if(!!cb)
				{
					var f = BX(BX.appLayout.params.frameName).contentWindow;
					cb += ':' + (typeof res == 'undefined' ? '' : JSON.stringify(res));
					f.postMessage(cb, BX.appLayout.params.appProto+'://'+BX.appLayout.params.appHost);
				}
			};

			BX.appLayout.messageInterface[cmd[0]].apply(BX.appLayout, [args, _cb]);
		}
	},

	split: function(s,ss)
	{
		var r = s.split(ss);
		return [r[0], r.slice(1,r.length-2).join(ss),r[r.length-2],r[r.length-1]];
	},

	reInstall: function()
	{
		BX.appLayout.messageInterface.setInstallFinish({value:false});
	},

	loadControl: function(name, params, cb)
	{
		if(!params)
			params = {};

		params.control = name;
		params.sessid = BX.bitrix_sessid();

		BX.ajax({
			method: 'POST',
			url: location.href,
			data: params,
			processScriptsConsecutive: true,
			onsuccess: BX.defer(cb)
		});
	},

	selectUserCallback_0: function(v)
	{
		var value = BX.util.array_values(v);
		if(!!value && value.length > 0)
		{
			BX.appLayout.userSelectorControl[0].close();

			if(!!BX.appLayout.userSelectorControlCallback)
				BX.appLayout.userSelectorControlCallback.apply(this, [value[0]]);
		}
	},

	selectUserCallback_1_value: [],
	selectUserCallback_1: function(v)
	{
		if(v === true)
		{
			var value = BX.util.array_values(BX.appLayout.selectUserCallback_1_value);

			BX.appLayout.userSelectorControl[1].close();

			if(!!BX.appLayout.userSelectorControlCallback)
				BX.appLayout.userSelectorControlCallback.apply(this, [value]);
		}
		else
		{
			BX.appLayout.selectUserCallback_1_value = v;
		}
	},

	hideUpdate: function(version, cb)
	{
		BX.userOptions.save('app_options', 'params_' + BX.appLayout.params.appId + '_' + BX.appLayout.params.appV, 'skip_update_' + version, 1);
		cb();
	},

	sendAppOptions: function()
	{
		if(BX.appLayout._appOptionsStack.length > 0)
		{
			var stack = BX.appLayout._appOptionsStack;
			BX.appLayout._appOptionsStack = [];

			var opts = [];
			for(var i = 0; i < stack.length; i++)
			{
				opts.push({name:stack[i][0],value:stack[i][1]});
			}

			var params = {action:'set_option',sessid:BX.bitrix_sessid(),options:opts};

			BX.ajax.loadJSON(location.href, params, function(data){
				for(var i = 0; i < stack.length; i++)
				{
					stack[i][2](data);
				}
			});
		}
	}
};

BX.bind(window, 'message', BX.appLayout.receiveMessage);
})();