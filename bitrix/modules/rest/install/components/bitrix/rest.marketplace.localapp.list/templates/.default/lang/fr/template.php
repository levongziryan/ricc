<?
$MESS["APPLIST_ADD_NEW"] = "Ajouter une application";
$MESS["CT_BLL_SELECTED"] = "Compteur d'enregistrements";
$MESS["APPLIST_DELETE_CONFIRM"] = "Voulez-vous vraiment supprimer l'application?";
$MESS["APPLIST_DELETE_ERROR"] = "Erreur! L'application n'a pas été désinstallée.";
?>