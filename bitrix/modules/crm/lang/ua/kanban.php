<?
$MESS["CRM_KANBAN_NOTIFY_TITLE"] = "Недостатньо прав";
$MESS["CRM_KANBAN_NOTIFY_HEADER"] = "Недостатньо прав на зміну стадій";
$MESS["CRM_KANBAN_NOTIFY_TEXT"] = "Створити нову стадію може адміністратор вашого Бітрікс24. Надішліть повідомлення або зверніться особисто та ваші колеги додадуть потрібні стадії.";
$MESS["CRM_KANBAN_NOTIFY_BUTTON"] = "Надіслати сповіщення";
$MESS["CRM_KANBAN_ACTIVITY_MY"] = "Справи";
$MESS["CRM_KANBAN_ACTIVITY_PLAN"] = "План";
$MESS["CRM_KANBAN_ACTIVITY_PLAN_CALL"] = "Дзвінок";
$MESS["CRM_KANBAN_ACTIVITY_PLAN_MEETING"] = "Зустріч";
$MESS["CRM_KANBAN_ACTIVITY_PLAN_VISIT"] = "Візит";
$MESS["CRM_KANBAN_ACTIVITY_PLAN_TASK"] = "Завдання";
$MESS["CRM_KANBAN_ACTIVITY_MORE"] = "Ще";
$MESS["CRM_KANBAN_ACTIVITY_LETSGO_LEAD"] = "Заплануйте справи, щоб просунути лід вперед по воронці.";
$MESS["CRM_KANBAN_ACTIVITY_LETSGO_DEAL"] = "Заплануйте справи, щоб просунути лід вперед по воронці.";
$MESS["CRM_KANBAN_NO_EMAIL"] = "E-mail відсутній";
$MESS["CRM_KANBAN_NO_PHONE"] = "Телефон відсутня";
$MESS["CRM_KANBAN_NO_IM"] = "Спілкування в чаті неможливо";
$MESS["MAIN_KANBAN_COLUMN_TITLE_PLACEHOLDER"] = "Назва";
$MESS["MAIN_KANBAN_DROPZONE_CANCEL"] = "Скасувати";
?>