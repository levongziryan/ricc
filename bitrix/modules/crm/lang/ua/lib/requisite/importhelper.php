<?
$MESS["CRM_RQ_IMP_HLPR_ERR_INVALID_ENTITY_TYPE"] = "Некоректний тип сутності для реквізитів";
$MESS["CRM_RQ_IMP_HLPR_ERR_INVALID_ENTITY_ID"] = "Не вказана сутність реквізитів для";
$MESS["CRM_RQ_IMP_HLPR_ERR_COMPANY_NOT_EXISTS"] = "Не знайдено компанію (ID: #ID#)";
$MESS["CRM_RQ_IMP_HLPR_ERR_CONTACT_NOT_EXISTS"] = "Не знайдено контакт (ID: #ID#)";
$MESS["CRM_RQ_IMP_HLPR_ERR_INVALID_IMP_PRESET_ID"] = "Невірний ідентифікатор шаблону для імпорту реквізитів";
$MESS["CRM_RQ_IMP_HLPR_ERR_IMP_PRESET_NOT_EXISTS"] = "Не знайдено шаблон для імпорту реквізитів (ID: #ID#)";
$MESS["CRM_RQ_IMP_HLPR_ERR_ACCESS_DENIED_COMPANY_UPDATE"] = "Немає доступу на зміну компанії (ID: #ID#)";
$MESS["CRM_RQ_IMP_HLPR_ERR_ACCESS_DENIED_CONTACT_UPDATE"] = "Немає доступу на зміну контакту (ID: #ID#)";
$MESS["CRM_RQ_IMP_HLPR_ERR_NO_ADDRESSES_TO_IMPORT"] = "Немає адрес для імпорту";
$MESS["CRM_RQ_IMP_HLPR_ERR_IMP_PRESET_HAS_NO_ADDR_FIELD"] = "Шаблон для імпорту реквізитів не містить поле адреси (ID: #ID#)";
$MESS["CRM_RQ_IMP_HLPR_ERR_CREATE_REQUISITE"] = "Помилка створення реквізитів для #ENTITY_TYPE_NAME_GENITIVE# (ID: #ID#)";
$MESS["CRM_RQ_IMP_HLPR_ERR_COMPANY_GENITIVE"] = "компанії";
$MESS["CRM_RQ_IMP_HLPR_ERR_CONTACT_GENITIVE"] = "контакту";
?>