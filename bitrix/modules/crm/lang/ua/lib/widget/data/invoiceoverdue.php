<?
$MESS["CRM_INVOICE_OVERDUE_PRESET_OVERALL_COUNT"] = "Кількість прострочених рахунків";
$MESS["CRM_INVOICE_OVERDUE_PRESET_OVERALL_SUM"] = "Сума прострочених рахунків";
$MESS["CRM_INVOICE_OVERDUE_PRESET_OVERALL_COUNT_SHORT"] = "Кількість рахунків";
$MESS["CRM_INVOICE_OVERDUE_PRESET_OVERALL_SUM_SHORT"] = "Сума рахунків";
$MESS["CRM_INVOICE_OVERDUE_CATEGORY"] = "Прострочені рахунки";
?>