<?
$MESS["CRM_BANKDETAIL_ERR_INVALID_ENTITY_TYPE"] = "Некоректний тип сутності для банківських реквізитів";
$MESS["CRM_BANKDETAIL_ERR_INVALID_ENTITY_ID"] = "Не вказана сутність для банківських реквізитів";
$MESS["CRM_BANKDETAIL_ERR_ON_DELETE"] = "Помилка при видаленні банківських реквізитів";
$MESS["CRM_BANKDETAIL_ERR_NOTHING_TO_DELETE"] = "Не знайдено банківські реквізити для видалення";
?>