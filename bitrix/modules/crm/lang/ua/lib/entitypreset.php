<?
$MESS["CRM_ENTITY_TYPE_REQUISITE"] = "Реквізити";
$MESS["CRM_ENTITY_TYPE_REQUISITE_DESC"] = "Шаблони реквізитів";
$MESS["CRM_ENTITY_PRESET_ERR_DELETE_PRESET_USED"] = "Ви не можете видалити шаблон, оскільки по ньому вже створені реквізити.";
$MESS["CRM_ENTITY_PRESET_ERR_PRESET_NOT_FOUND"] = "Шаблон не знайдено.";
$MESS["CRM_ENTITY_PRESET_ERR_INVALID_ENTITY_TYPE"] = "Некоректний тип сутності шаблону.";
$MESS["CRM_ENTITY_PRESET_ERR_DELETE_PRESET_DEF_FOR_COMPANY"] = "Ви не можете видалити шаблон, так як він є основним для компаній.";
$MESS["CRM_ENTITY_PRESET_ERR_DELETE_PRESET_DEF_FOR_CONTACT"] = "Ви не можете видалити шаблон, так як він є основним для контактів.";
$MESS["CRM_ENTITY_PRESET_NAME_EMPTY"] = "Шаблон без назви";
?>