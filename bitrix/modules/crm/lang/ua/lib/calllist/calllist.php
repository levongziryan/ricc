<?
$MESS["CRM_CALL_LIST_LIMIT_ERROR"] = "Перевищено ліміт кількості записів. Встановіть фільтр, так, щоб відібрати не більш #LIMIT# записів";
$MESS["CRM_CALL_LIST_UPDATE"] = "Додати до обдзвону";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_HEADER"] = "Доступно, починаючи з тарифу Проект+";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_CONTENT"] = "Обдзвін допоможе швидко сформувати список клієнтів, яким потрібно зателефонувати і поставить завдання відповідальному менеджеру.";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_ITEM_1"] = "Створення списку викликів в кілька кліків";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_ITEM_2"] = "Послідовне виконання дзвінків по всім клієнтам в одному вікні";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_ITEM_3"] = "Контроль виконання та результату викликів";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_SHOW_MORE"] = "Дізнатися детальніше";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_FOOTER"] = "Обдзвін та інші корисні можливості доступні, починаючи з тарифу \"Проект+\" всього за 399 грн/міс.";
$MESS["CRM_CALL_LIST_SUBJECT"] = "Обзвон";
?>