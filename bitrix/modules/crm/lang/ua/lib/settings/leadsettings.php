<?
$MESS["CRM_LEAD_SETTINGS_VIEW_WIDGET"] = "Аналітика по лідам";
$MESS["CRM_LEAD_SETTINGS_VIEW_LIST"] = "Звичайний список лідів";
$MESS["CRM_TYPE_TITLE"] = "Виберіть зручний спосіб роботи з CRM";
$MESS["CRM_TYPE_SAVE"] = "Зберегти";
$MESS["CRM_TYPE_CANCEL"] = "Скасувати";
$MESS["CRM_TYPE_TURN_ON"] = "Ввімкнути";
$MESS["CRM_ROBOTS_TITLE"] = "Простий режим";
$MESS["CRM_ROBOTS_TEXT"] = "Простий режим роботи з CRM автоматично конвертує кожен новий лід в угоду за допомогою роботів. <br/><br/>
У вас вже є налаштовані роботи для Лідов. При включенні простого режиму всі роботи на першій стадії будуть видалені. <br/><br/>
Ви впевнені, що бажаєте увімкнути Простий режим?";
$MESS["CRM_LEAD_SETTINGS_VIEW_KANBAN"] = "Канбан лідов";
?>