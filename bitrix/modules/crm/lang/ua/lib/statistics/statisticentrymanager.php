<?
$MESS["CRM_STAT_MGR_REBUILD_STATISTICS"] = "Для правильної роботи звітів необхідно <a id=\"#ID#\" href=\"#URL#\">провести оновлення</a> статистичних даних.";
$MESS["CRM_STAT_MGR_REBUILD_STATISTICS_DLG_TITLE"] = "Оновлення статистичних даних";
$MESS["CRM_STAT_MGR_REBUILD_STATISTICS_DLG_SUMMARY"] = "Буде проведено оновлення статистичних даних. Виконання цієї операції може зайняти тривалий час.";
?>