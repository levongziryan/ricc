<?
$MESS["CRM_ACTIVITY_VISIT_TITLE"] = "Visite";
$MESS["CRM_ACTIVITY_VISIT_BUTTON_FINISH"] = "Fermer";
$MESS["CRM_ACTIVITY_VISIT_OWNER_SELECT"] = "Sélectionner";
$MESS["CRM_ACTIVITY_VISIT_OWNER_CHANGE"] = "Modifier";
$MESS["CRM_ACTIVITY_VISIT_OWNER_TAKE_PICTURE"] = "Prendre une photo";
$MESS["CRM_ACTIVITY_VISIT_OWNER_RETAKE_PICTURE"] = "Reprendre";
$MESS["CRM_ACTIVITY_VISIT_ERROR_MIC_FAILURE"] = "Impossible d'accéder au micro. Description de l'erreur:";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_TITLE"] = "Conditions d'utilisation";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_BODY_1"] = "IMPORTANT! Ceci va enregistrer la conversation avec votre visiteur.";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_BODY_2"] = "Assurez-vous de respecter toutes les lois relatives à l'enregistrement des conversations dans votre pays.";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_AGREED"] = "J'accepte les Conditions";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_CLOSE"] = "Fermer";
$MESS["CRM_ACTIVITY_VISIT_DEFAULT_CAMERA"] = "Caméra par défaut";
$MESS["CRM_ACTIVITY_VISIT_FACEID_AGREEMENT"] = "<div class=\"tracker-agreement-popup-content\">
<ol class=\"tracker-agreement-popup-list\">
<li class=\"tracker-agreement-popup-list-item\">
Le Service FindFace (ci-après dénommé \"Service FindFace\") est fourni par N-TECH.LAB LTD. Les utilisateurs du Service FindFace sont tenus de conclure un accord avec le Service FindFace et d'accepter les <a href=\"https://saas.findface.pro/files/docs/license_us.pdf\" class=\"tracker-link\">conditions générales suivantes</a> en sélectionnant \"J'accepte\".
<br>En sélectionnant \"J'accepte\", l'Utilisateur reconnaît et garantit son adhésion aux obligations suivantes:
<ul class=\"tracker-agreement-popup-inner-list\">
<li class=\"tracker-agreement-popup-inner-list-item\">
Respecter strictement l'ensemble des lois et règlementations locales relatives à la vie privée et à l'utilisation des données personnelles du pays de résidence de l'Utilisateur.
</li>
<li class=\"tracker-agreement-popup-inner-list-item\">
Respecter strictement l'ensemble des lois et règlementations locales.
Cette obligation demeurera valide pour toute la durée de l'utilisation du Service FindFace.
</li>
<li class=\"tracker-agreement-popup-inner-list-item\">
Obtenir une autorisation écrite, ou tel qu'autrement prévu par les lois locales de chaque pays dans lequel opère l'Utilisateur.
</li>
<li class=\"tracker-agreement-popup-inner-list-item\">
L'Utilisateur est tenu d'aviser diligemment tout individu pour lequel l'Utilisateur a l'intention de traiter les données personnelles via l'utilisation du Service FindFace, y compris les photographies et images.
</li>
<li class=\"tracker-agreement-popup-inner-list-item\">
Solliciter un avis juridique AVANT d'avoir recours au Service FindFace.
</li>
<li class=\"tracker-agreement-popup-inner-list-item\">
Garantir que toute information téléchargée par l'Utilisateur a été autorisée par l'individu, et se conforme aux lois et règlementations de la juridiction de l'Utilisateur.
</li>
<li class=\"tracker-agreement-popup-inner-list-item\">
Confirmer que l'Utilisateur fait usage du Service FindFace à ses propres risques, sans garantie de quelque nature que ce soit.
</li>
</ul>
</li>
<li class=\"tracker-agreement-popup-list-item\">
Bitrix, Inc, n'est en aucun cas responsable du Service et ne formule aucune prétention ou garantie quant à l'exactitude ou la disponibilité du Service, et ne fournit aucune assistance technique ou consultation pour le Service.
</li>
</ol>
<div class=\"tracker-agreement-popup-description\">
Bitrix, Inc. n'est en aucun cas responsable du Service FindFace et ne formule aucune prétention ou garantie quant au Service FindFace.
En acceptant les termes et conditions du présent accord de service, l'Utilisateur convient et confirme que le Service FindFace, ni Bitrix, Inc. ne recueille ou traite toute donnée téléchargée par l’Utilisateur. L'utilisateur confirme que le Service FindFace, ni Bitrix,Inc. n'a aucune connaissance de la façon dont l'Utilisateur recueille les données, photographies ou images. L'utilisateur accepte d'exonérer le Service FindFace et Bitrix de toute action et inaction de l'Utilisateur.
<br>
<br>
Résolution des litiges; Arbitrage exécutoire:
Tout différend, toute controverse, interprétation ou réclamation, y compris mais sans s'y limiter les réclamations de rupture de contrat, toute forme de négligence, fraude ou fausse déclaration découlant de ou en lien avec le présent accord doivent être soumis pour arbitrage exécutoire.
</div>
</div>";
$MESS["CRM_ACTIVITY_VISIT_FACEID_SOCIAL_SEACH_TITLE"] = "Rechercher un profil VK";
$MESS["CRM_ACTIVITY_VISIT_FACEID_SOCIAL_SEACH_IN_PROCESS"] = "Recherche de données...";
?>