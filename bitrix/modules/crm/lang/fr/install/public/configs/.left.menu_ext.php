<?
$MESS["CRM_PERMS"] = "Droits d'accès";
$MESS["CRM_CURRENCIES"] = "Devises";
$MESS["CRM_GUIDES"] = "Indicatrices";
$MESS["CRM_FIELDS"] = "Champs d'utilisateur";
$MESS["CRM_SENDSAVE"] = "Intégration avec E-mail";
$MESS["CRM_CONFIG"] = "Autres paramètres";
$MESS["CRM_BP"] = "Processus d'affaires";
$MESS["CRM_EXTERNAL_SALE"] = "Boutiques en ligne";
?>