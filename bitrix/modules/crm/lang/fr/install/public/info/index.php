<?
$MESS["CRM_PAGE_TITLE"] = "Aide";
$MESS["CRM_PAGE_CONTENT"] = "<h3>Qu'est-ce que c'est la CRM?</h3>
 
<p><b>Le système de gestion des relations clients </b> (ou <b>CRM</b>) &mdash; est un système conçu pour augmenter les ventes, optimiser la commercialisation et améliorer le service à la clientèle. CRM stocke l'information sur les clients et l'historique de relations avec eux pour une analyse plus approfondie des résultats.</p>
 
<p>A l'aide de CRM le manager avec un prospect font un voyage de prospect à la transaction effectuée. </p>
 
<p>Définissons d'abord quelques notions:</p>
 
<ul>
 <li><b>Contact</b> Données sur la personne avec laquelle vous avez établi un dialogue sur les activités professionnelles, en d'autres termes c'est unecarte de visite. </li>
 
 <li><b>Entreprise</b> Données sur l'entreprise qui a une certaine forme de coopération (ou peut avoir la coopération) avec la vôtre. </li>
 
 <li><b>Prospect</b> Données sur toute forme de contact (téléphone, adresse email, rendez-vous d'affaire etc.) qui a le potentiel de se transformer en une affaire. </li>
 
 <li><b>Evènement</b> - tout changement dans les dossiers Contact, Prospect, Entreprise, par exemple, l'ajout d'une nouvelle adresse email. </li>
 
 <li><b>Transaction</b> - Données sur le travail avec le Client, l'Entreprise au sujet de la vente de produits. </li>
 </ul>
 
<h3>Comment fonctionne-t-elle?</h3>
 
<p><b>La CRM</b> peut être utilisée en 2 façons:</p>
 
<ol>
 <li>comme <b>base de données</b> de Contacts et Entreprises,</li>
 
 <li>comme <b>CRM</b></li> classique 
 </ol>
 
<h4>1. CRM comme base de données</h4>
 
<p>L'utilisation de CRM comme une base de données de contacts et d'entreprises permet mener l'historique de relations avec eux. L'objectif principal de cette affaire sont le Contact et l'Entreprise où à l'aide d'un Evènement on mène l'historique de relations. L'tilisation de CRM comme base de données n'empêche pas à la suite de certains Evènements la création de Prospects et leur conversion en Transaction.</p>
 
<p><img height='430' border='0' width='900' src='/upload/crm/cim/01.png' /></p>
 
<h4>2. CRM classique</h4>
 
<p> L'objectif principal de la CRM classique est le Prospect qui est ajouté au système à la main par le manager automatiquement à partir de &quot; 1C-Bitrix:Gestion du site &quot; ou d'autres moyens. Après l'ajout et le traitement le Prospect peut être converti à Contact, entreprise ou Transaction. Dans les deux premiers cas, le Prospect devient un élément ordinaire de la base de données. Dans le deuxième cas, en passant par l'Entonnoir de vente il devient une véritable Transaction.</p>
 
<p><img height='430' border='0' width='900' src='/upload/crm/cim/03.png' /></p>
";
?>