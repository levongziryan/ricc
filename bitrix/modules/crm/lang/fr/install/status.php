<?
$MESS["CRM_STATUS_P"] = "Achevé(e)s";
$MESS["CRM_STATUS_P_DESCR"] = "Achevé(e)s";
$MESS["CRM_STATUS_D"] = "Refusé(e)s";
$MESS["CRM_STATUS_D_DESCR"] = "Refusé(e)s";
$MESS["CRM_STATUS_S"] = "Envoyé au client";
$MESS["CRM_STATUS_S_DESCR"] = "Envoyé au client";
$MESS["CRM_STATUS_A"] = "Confirmé";
$MESS["CRM_STATUS_A_DESCR"] = "Confirmé";
$MESS["CRM_STATUS_N"] = "Brouillon";
$MESS["CRM_STATUS_N_DESCR"] = "Brouillon";
$MESS["CRM_STATUSN_D"] = "Non payé";
$MESS["CRM_STATUSN_D_DESCR"] = "Non payé";
$MESS["CRM_STATUSN_P"] = "Payée";
$MESS["CRM_STATUSN_P_DESCR"] = "Payée";
$MESS["CRM_STATUSN_S"] = "Envoyé au client";
$MESS["CRM_STATUSN_S_DESCR"] = "Envoyé au client";
$MESS["CRM_STATUSN_N"] = "Nouveau";
$MESS["CRM_STATUSN_N_DESCR"] = "Nouveau";
?>