<?
$MESS["CRM_BP_TITLE"] = "Traitement du contact";
$MESS["CRM_BP_TYPE"] = "Processus d'affaires consécutif";
$MESS["CRM_BP_TASK_NAME"] = "Traitement du contact: {=Document:NAME} {=Document:LAST_NAME} (Fourni par le processus d'affaires)";
$MESS["CRM_BP_TASK_TEXT"] = "Effectuer le traitement du contact: {=Document:NAME} {=Document:LAST_NAME}";
$MESS["CRM_BP_CONTACT_TITLE"] = "Traitement du contact";
$MESS["CRM_BP_CONTACT_TYPE"] = "Créer un processus d'affaires consécutif";
$MESS["CRM_BP_CONTACT_TASK_TEXT"] = "Effectuer le traitement du contact: {=Document:NAME} {=Document:LAST_NAME}";
$MESS["CRM_BP_CONTACT_TASK_NAME"] = "Contactez-traitement: {= Document: NOM} {= Document: LAST_NAME} (Créé par processus d'affaires)";
?>