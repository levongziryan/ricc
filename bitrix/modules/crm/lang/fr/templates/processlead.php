<?
$MESS["CRM_BP_LEAD_TITLE"] = "Traitement du prospect";
$MESS["CRM_BP_LEAD_TYPE"] = "Créer un processus d'affaires consécutif";
$MESS["CRM_BP_LEAD_LEADTASK_NAME"] = "Traitement du prospect: {=Document:TITLE} (Fournie par le processus d'affaires)";
$MESS["CRM_BP_LEAD_TASK_TEXT"] = "Effectuer le traitement du prospect: {=Document:TITLE}";
$MESS["CRM_BP_TITLE"] = "Traitement du prospect";
$MESS["CRM_BP_TYPE"] = "Processus d'affaires consécutif";
$MESS["CRM_BP_TASK_NAME"] = "Traitement du prospect: {=Document:TITLE} (Fournie par le processus d'affaires)";
$MESS["CRM_BP_TASK_TEXT"] = "Effectuer le traitement du prospect: {=Document:TITLE}";
?>