<?
$MESS["CRM_BP_DEAL_TITLE"] = "Exécution par étapes";
$MESS["CRM_BP_DEAL_PROPERTIES_1"] = "tat des processus d'affaires";
$MESS["CRM_BP_DEAL_PROPERTIES_1_TITLE"] = "Statut: Nouvelle (Analyse)";
$MESS["CRM_BP_DEAL_PROPERTIES_2"] = "Enveloppe de l'événement";
$MESS["CRM_BP_DEAL_PROPERTIES_2_TITLE"] = "Présentation de l'offre";
$MESS["CRM_BP_DEAL_PROPERTIES_3_TITLE"] = "Etablir le statut";
$MESS["CRM_BP_DEAL_PROPERTIES_4_TITLE"] = "Accès au statut";
$MESS["CRM_BP_DEAL_PROPERTIES_5_TITLE"] = "Modifier le document";
$MESS["CRM_BP_DEAL_PROPERTIES_6_TITLE"] = "En cours";
$MESS["CRM_BP_DEAL_PROPERTIES_7_TITLE"] = "Statut: une proposition/offre a été faite";
$MESS["CRM_BP_DEAL_PROPERTIES_8_TITLE"] = "Action";
$MESS["CRM_BP_DEAL_PROPERTIES_10_TITLE"] = "Chec";
$MESS["CRM_BP_DEAL_PROPERTIES_13_TITLE"] = "Statut: échec";
$MESS["CRM_BP_DEAL_PROPERTIES_14_TITLE"] = "Action";
$MESS["CRM_BP_DEAL_PROPERTIES_16_TITLE"] = "Réussite";
$MESS["CRM_BP_DEAL_PROPERTIES_17_TITLE"] = "Statut: Réussite";
$MESS["CRM_BP_DEAL_PROPERTIES_18_TITLE"] = "Code PHP";
$MESS["CRM_BP_DEAL_PROPERTIES_19_TITLE"] = "Jusqu'à...ventes";
$MESS["CRM_BP_DEAL_PROPERTIES_20_TITLE"] = "statistiques";
$MESS["CRM_BP_DEAL_TASK_NAME"] = "Avant l'opération de vente: {= Document: TITRE} (fourni par le processus d'affaires)";
$MESS["CRM_BP_DEAL_TASK_TEXT"] = "Effectuer la prévente de services d'après la transaction: {=Document:TITLE}";
$MESS["CRM_BP_DEAL_PROPERTIES_11_TITLE"] = "Facture";
$MESS["CRM_BP_DEAL_PROPERTIES_15_TITLE"] = "Statut: Facture";
?>