<?
$MESS["CRM_BP_SHOP_US_10000"] = "10 000";
$MESS["CRM_BP_SHOP_PBP"] = "Créer un processus d'affaires consécutif";
$MESS["CRM_BP_SHOP_TITLE"] = "Transaction de la boutique en ligne";
$MESS["CRM_BP_SHOP_10000_1"] = "Une nouvelle transaction dans le boutique en ligne a été créée.";
$MESS["CRM_BP_SHOP_10000"] = "Une transaction de la boutique en ligne est créée. Contacter le client pendant une heure. Le montant de la transaction est plus de 10000 {=Document:ACCOUNT_CURRENCY_ID}.";
$MESS["CRM_BP_SHOP_SS"] = "Message du réseau social";
$MESS["CRM_BP_SHOP_US"] = "Condition";
?>