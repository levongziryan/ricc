<?
$MESS["CRM_EXCH1C_PERMISSION_DENIED"] = "Accès interdit.";
$MESS["CATALOG_MODULE_NOT_INSTALLED"] = "Le module 'Catalogue de marchandises' n'a pas été installé.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "Le module des blocs d'informations n'a pas été installé.";
$MESS["SALE_MODULE_NOT_INSTALLED"] = "Le module 'Boutique en ligne' n'a pas été installé.";
$MESS["CRM_EXCH1C_UNKNOWN_XML_ID"] = "Identificateur non valide du répertoire.";
$MESS["CRM_EXCH1C_UNKNOWN_COMMAND_TYPE"] = "Ordre inconnu.";
$MESS["CRM_EXCH1C_NOT_ENABLED"] = "Echange de données avec '1C:Entreprise' est désactivé";
$MESS["CRM_EXCH1C_AUTH_ERROR"] = "Erreur d'autorisation. identifiant ou mot de passe est incorrect.";
?>