<?
$MESS["CRM_KANBAN_ACTIVITY_MY"] = "Activités";
$MESS["CRM_KANBAN_ACTIVITY_PLAN_CALL"] = "Appeler";
$MESS["CRM_KANBAN_ACTIVITY_PLAN_MEETING"] = "Réunion";
$MESS["CRM_KANBAN_ACTIVITY_PLAN_TASK"] = "Tâche";
$MESS["CRM_KANBAN_ACTIVITY_PLAN_VISIT"] = "Visite";
$MESS["CRM_KANBAN_NOTIFY_TITLE"] = "Autorisations insuffisantes.";
$MESS["CRM_KANBAN_NOTIFY_HEADER"] = "Autorisations insuffisantes pour modifier les étapes.";
$MESS["CRM_KANBAN_NOTIFY_TEXT"] = "Seul l’administrateur de votre Bitrix24 peut créer une nouvelle étape. Veuillez lui envoyer un message ou lui parler afin de pouvoir ajouter les étapes de la tâche dont vous avez besoin.";
$MESS["CRM_KANBAN_NOTIFY_BUTTON"] = "Envoyer une notification";
$MESS["CRM_KANBAN_ACTIVITY_PLAN"] = "Planifier";
$MESS["CRM_KANBAN_ACTIVITY_MORE"] = "Plus";
$MESS["CRM_KANBAN_ACTIVITY_LETSGO_LEAD"] = "Planifier les activités pour faire avancer le lead.";
$MESS["CRM_KANBAN_ACTIVITY_LETSGO_DEAL"] = "Planifier les activités pour faire avancer la transaction.";
$MESS["CRM_KANBAN_NO_EMAIL"] = "Pas d'adresse e-mail";
$MESS["CRM_KANBAN_NO_PHONE"] = "Aucun téléphone";
$MESS["CRM_KANBAN_NO_IM"] = "Aucune communication de chat";
$MESS["MAIN_KANBAN_COLUMN_TITLE_PLACEHOLDER"] = "Nom";
$MESS["MAIN_KANBAN_DROPZONE_CANCEL"] = "Annuler";
?>