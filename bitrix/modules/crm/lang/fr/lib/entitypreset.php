<?
$MESS["CRM_ENTITY_TYPE_REQUISITE"] = "Détails";
$MESS["CRM_ENTITY_TYPE_REQUISITE_DESC"] = "Modèles pour les détails de contact et de la société";
$MESS["CRM_ENTITY_PRESET_ERR_DELETE_PRESET_USED"] = "Le modèle ne peut être supprimé parce qu'il sert de base à des données.";
$MESS["CRM_ENTITY_PRESET_ERR_PRESET_NOT_FOUND"] = "Le template est introuvable.";
$MESS["CRM_ENTITY_PRESET_ERR_INVALID_ENTITY_TYPE"] = "Type d'entité invalide pour utilisation avec le template.";
$MESS["CRM_ENTITY_PRESET_ERR_DELETE_PRESET_DEF_FOR_COMPANY"] = "Vous ne pouvez pas supprimer ce template car il s'agit d'un template d'entreprise par défaut";
$MESS["CRM_ENTITY_PRESET_ERR_DELETE_PRESET_DEF_FOR_CONTACT"] = "Vous ne pouvez pas supprimer ce template car il s'agit d'un template de contact par défaut";
$MESS["CRM_ENTITY_PRESET_NAME_EMPTY"] = "Modèle sans titre";
?>