<?
$MESS["CRM_IMPORT_YAHOO_REQUIREMENTS"] = "Pour importer avec succès il faut effectuer des exigences suivantes. Codage du fichier: UTF-8. Langue pour les noms des champs: anglais. Séparateur des champs: virgule.";
$MESS["CRM_IMPORT_YAHOO_ERROR_FIELDS_NOT_FOUND"] = "Aucun des champs suivants: #FIELD_LIST# n'a pas été retrouvé";
?>