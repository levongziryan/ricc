<?
$MESS["CRM_CONTACT_MERGER_COLLISION_READ_UPDATE_PERMISSION"] = "#USER_NAME# a fusionné une entreprise'#SEED_TITLE#' [#SEED_ID#] with '#TARG_TITLE#' [#TARG_ID#] vous ne pouvez pas modifier en raison de l'autorisation d'accès à vos préférences.";
$MESS["CRM_CONTACT_MERGER_COLLISION_READ_PERMISSION"] = "#USER_NAME# a fusionné une entreprise'#SEED_TITLE#' [#SEED_ID#] with '#TARG_TITLE#' [#TARG_ID#] vous ne pouvez pas modifier en raison de l'autorisation d'accès à vos préférences.";
$MESS["CRM_CONTACT_MERGER_COLLISION_UPDATE_PERMISSION"] = "#USER_NAME# a fusionné une entreprise'#SEED_TITLE#' [#SEED_ID#] with '#TARG_TITLE#' [#TARG_ID#] vous ne pouvez pas modifier en raison de l'autorisation d'accès à vos préférences.";
?>