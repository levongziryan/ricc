<?
$MESS["CRM_RECUR_WRONG_ID"] = "ID de compte invalide";
$MESS["CRM_RECUR_ACTIVATE_LIMIT_REPEAT"] = "Erreur lors de l'activation de la commande, le nombre maximum de répétitions est dépassé. Essayez d'augmenter la valeur maximale.";
$MESS["CRM_RECUR_ACTIVATE_LIMIT_DATA"] = "Erreur lors de l'activation de la commande, la dernière date de facture récurrente est atteinte. Essayez de changer la date.";
?>