<?
$MESS["CRM_BANKDETAIL_ERR_INVALID_ENTITY_TYPE"] = "Type d'entité d'informations bancaires incorrect.";
$MESS["CRM_BANKDETAIL_ERR_INVALID_ENTITY_ID"] = "Aucune entité d'informations bancaires n'a été spécifié";
$MESS["CRM_BANKDETAIL_ERR_ON_DELETE"] = "Erreur lors de la suppression des informations bancaires";
$MESS["CRM_BANKDETAIL_ERR_NOTHING_TO_DELETE"] = "Aucune information bancaire à supprimer";
?>