<?
$MESS["CRM_B24_EMAIL_SIGNATURE_DISABLED"] = "Désactivé(e)s";
$MESS["CRM_B24_EMAIL_SIGNATURE_ENABLED"] = "Activer (peut être désactivé que sur la norme et les plans tarifaires professionnels)";
$MESS["CRM_B24_EMAIL_PAID_LICENSE_SIGNATURE"] = "Envoyé par [url=http://www.bitrix24.com]bitrix24.com[/url]";
$MESS["CRM_B24_EMAIL_FREE_LICENSE_SIGNATURE"] = "Envoyé par [url=http://www.bitrix24.com]bitrix24.com[/url] - solution la plus populaire au monde libre de CRM et de gestion de la clientèle";
?>