<?
$MESS["WS_CRMINTEGRATION_APP_TITLE"] = "CRM";
$MESS["WS_CRMINTEGRATION_APP_OPTIONS_TITLE_SALE"] = "Intégration avec e-boutique";
$MESS["WS_CRMINTEGRATION_APP_DESC"] = "Lorsque vous travaillez avec le CRM, vous pouvez configurer l'échange de données avec d'autres programmes. Choisissez l'une des options de connexion et cliquez sur le 'Get Mot de passe'.";
$MESS["WS_CRMINTEGRATION_APP_OPTIONS_CAPTION"] = "Connectez CRM";
?>