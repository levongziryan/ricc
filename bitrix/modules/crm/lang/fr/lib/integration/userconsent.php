<?
$MESS["CRM_USER_CONSENT_DATA_PROVIDER_NAME"] = "Données de votre société dans le CRM";
$MESS["CRM_USER_CONSENT_PROVIDER_NAME"] = "Activité CRM";
$MESS["CRM_USER_CONSENT_PROVIDER_ITEM_NAME"] = "Activité CRM #%id%";
$MESS["CRM_USER_CONSENT_NOTIFY_TEXT_BTN"] = "Plus";
$MESS["CRM_USER_CONSENT_DEF_NAME"] = "Exemple de consentement pour le traitement des données personnelles";
?>