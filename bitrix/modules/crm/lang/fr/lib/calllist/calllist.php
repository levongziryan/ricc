<?
$MESS["CRM_CALL_LIST_LIMIT_ERROR"] = "Le nombre maximum d'éléments est dépassé. Modifiez le filtre de façon à ne pas sélectionner plus de #LIMIT# éléments.";
$MESS["CRM_CALL_LIST_UPDATE"] = "Ajouter à la liste d'appel";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_HEADER"] = "Disponible dans le plan Plus et au-delà";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_CONTENT"] = "Utilisez les listes d'appel pour créer une liste des clients qui attendent votre appel et créer une tâche respective pour la personne responsable assignée.";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_ITEM_1"] = "Créez une liste d’appel en quelques clics";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_ITEM_2"] = "Appelez tous les clients un par un dans une seule fenêtre";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_ITEM_3"] = "Contrôlez le progrès et les résultats des appels";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_SHOW_MORE"] = "En savoir plus";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_FOOTER"] = "Les listes d’appels et d'autres fonctionnalités utiles sont disponibles à partir du plan Plus pour seulement 39\$/mois.";
$MESS["CRM_CALL_LIST_SUBJECT"] = "Liste d’appel";
?>