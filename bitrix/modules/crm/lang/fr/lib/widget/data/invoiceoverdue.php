<?
$MESS["CRM_INVOICE_OVERDUE_PRESET_OVERALL_COUNT"] = "Nombre de factures en retard";
$MESS["CRM_INVOICE_OVERDUE_PRESET_OVERALL_SUM"] = "Montant total des factures en retard";
$MESS["CRM_INVOICE_OVERDUE_PRESET_OVERALL_COUNT_SHORT"] = "Nombre de factures";
$MESS["CRM_INVOICE_OVERDUE_PRESET_OVERALL_SUM_SHORT"] = "Montant total des factures";
$MESS["CRM_INVOICE_OVERDUE_CATEGORY"] = "Factures en retard";
?>