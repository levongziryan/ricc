<?
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_COUNT"] = "Nombre de factures actives";
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_COUNT_SHORT"] = "Nombre de factures";
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_SUM"] = "Montant total des factures actives";
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_SUM_SHORT"] = "Montant total des factures";
$MESS["CRM_INVOICE_IN_WORK_CATEGORY"] = "Factures actives";
$MESS["CRM_INVOICE_OWED_CATEGORY"] = "Factures impayées";
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_COUNT_OWED"] = "Nombre de factures impayées";
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_SUM_OWED"] = "Montant total des factures impayées";
?>