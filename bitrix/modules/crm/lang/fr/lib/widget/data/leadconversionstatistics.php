<?
$MESS["CRM_LEAD_CONV_STAT_PRESET_OVERALL_COUNT"] = "Nombre de prospects converties";
$MESS["CRM_LEAD_CONV_STAT_PRESET_CONTACT_COUNT"] = "Nombre de contrats obtenus grâce aux prospects";
$MESS["CRM_LEAD_CONV_STAT_PRESET_COMPANY_COUNT"] = "Nombre de sociétés obtenues grâce aux prospects";
$MESS["CRM_LEAD_CONV_STAT_PRESET_DEAL_COUNT"] = "Nombre de transactions obtenues grâce aux prospects";
$MESS["CRM_LEAD_CONV_STAT_PRESET_OVERALL_SUM"] = "Montant total des prospects converties";
$MESS["CRM_LEAD_CONV_STAT_PRESET_OVERALL_COUNT_SHORT"] = "Nombre de prospects";
$MESS["CRM_LEAD_CONV_STAT_PRESET_OVERALL_SUM_SHORT"] = "Montant total des prospects";
$MESS["CRM_LEAD_CONV_CATEGORY"] = "Prospects converties";
?>