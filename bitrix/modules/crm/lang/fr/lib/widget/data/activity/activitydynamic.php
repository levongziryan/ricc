<?
$MESS["CRM_ACTIVITY_DYNAMIC_CHANNELS_ARE_NOT_FOUND"] = "Il n’y a aucun canal disponible.";
$MESS["CRM_ACTIVITY_DYNAMIC_CONNECTED_CHANNELS"] = "Canaux connectés";
$MESS["CRM_ACTIVITY_DYNAMIC_CONNECTED_CHANNELS_IN"] = "Entrant";
$MESS["CRM_ACTIVITY_DYNAMIC_CONNECTED_CHANNELS_OUT"] = "Sortant";
$MESS["CRM_ACTIVITY_DYNAMIC_UNCONNECTED_1"] = "et autres";
$MESS["CRM_ACTIVITY_DYNAMIC_UNCONNECTED_3"] = "Connecter plus de canaux : ";
$MESS["CRM_ACTIVITY_DYNAMIC_CONNECTED"] = "Connecté : ";
$MESS["CRM_ACTIVITY_DYNAMIC_CONNECT"] = "Connecter : ";
$MESS["CRM_CH_TRACKER_START_VIDEO_TITLE"] = "Vous pouvez regarder la vidéo à tout moment";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_IMOPENLINE"] = "Messageries et services sociaux";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_IMOPENLINE_1"] = "Premier Canal ouvert";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_WEBFORM"] = "Formulaire du CRM";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_LIVECHAT"] = "Chat live";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_VKGROUP"] = "VK";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_WEBFORM_1"] = "Informations de contact";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_WEBFORM_2"] = "Formulaire de commentaire";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_WEBFORM_4"] = "Rappel depuis le \"Principal numéro sortant\"";
$MESS["CRM_ACTIVITY_DYNAMIC_TITLE"] = "Aperçu de la communication";
$MESS["CRM_CH_TRACKER_START_VIDEO"] = "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/GipsvUe1gB0?rel=0&autoplay=1#VOLUME#\" frameborder=\"0\" allowfullscreen></iframe>";
?>