<?
$MESS["CRM_CONTACT_ACTIVITY_MARK_STAT_TOTAL"] = "Nombre d'activités (par évaluation)";
$MESS["CRM_CONTACT_ACTIVITY_MARK_STAT_NONE_QTY"] = "Nombre d'activités non évaluées";
$MESS["CRM_CONTACT_ACTIVITY_MARK_STAT_NEGATIVE_QTY"] = "Nombre d'activités évaluées négativement";
$MESS["CRM_CONTACT_ACTIVITY_MARK_STAT_POSITIVE_QTY"] = "Nombre d'activités évaluées positivement";
$MESS["CRM_CONTACT_ACTIVITY_MARK_STAT_GROUP_BY_SOURCE"] = "Source d'activité";
$MESS["CRM_CONTACT_ACTIVITY_MARK_STAT_GROUP_BY_MARK"] = "Évaluation d'activité";
?>