<?
$MESS["CRM_MANAGER_CNTR_SUCCEED"] = "Terminé";
$MESS["CRM_MANAGER_CNTR_FAILED"] = "Restant";
$MESS["CRM_MANAGER_CNTR_LEAD"] = "Leads";
$MESS["CRM_MANAGER_CNTR_CONTACT"] = "Contacts";
$MESS["CRM_MANAGER_CNTR_COMPANY"] = "Entreprises";
$MESS["CRM_MANAGER_CNTR_DEAL"] = "Transactions";
$MESS["CRM_MANAGER_CNTR_TITLE"] = "Compteurs du manager";
$MESS["CRM_MANAGER_CNTR_VIDEO"] = "<div style=\"width:400px;height:100px;font-size:15px;\">Utilisez votre compteur personnel afin d'évaluer votre performance : le CRM effectue le suivi de vos tâches terminées et celles qui restent à finir pour aujourd'hui. Votre objectif est de n'avoir aucune tâche à finir à la fin de la journée.</div>";
?>