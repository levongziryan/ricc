<?
$MESS["CRM_WEBFORM_PRESET_ITEM_CD_NAME"] = "Information de contact";
$MESS["CRM_WEBFORM_PRESET_ITEM_CD_CAPTION"] = "Information de contact";
$MESS["CRM_WEBFORM_PRESET_ITEM_CD_DESCRIPTION"] = "Veuillez laisser vos informations de contact pour que nous puissions vous joindre";
$MESS["CRM_WEBFORM_PRESET_ITEM_FB_NAME"] = "Formulaire de commentaire";
$MESS["CRM_WEBFORM_PRESET_ITEM_FB_CAPTION"] = "Commentaire";
$MESS["CRM_WEBFORM_PRESET_ITEM_FB_DESCRIPTION"] = "Veuillez partager vos retours dans le champ de commentaire";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_RESULT_SUCCESS_TEXT"] = "Merci, votre message a été envoyé.";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_RESULT_FAILURE_TEXT"] = "Malheureusement le message n'a pas été envoyé. Veuillez réessayer plus tard.";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_FIELD_NAME"] = "Prénom";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_FIELD_LAST_NAME"] = "Nom de famille";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_FIELD_PHONE"] = "Téléphone";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_FIELD_EMAIL"] = "E-mail";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_FIELD_COMMENTS"] = "Commentaire";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_NAME"] = "Rappel de \"#call_from#\"";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_CAPTION"] = "Vous n'avez pas trouvé ce que vous recherchez?";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_DESCRIPTION"] = "Vous avez encore des questions? Nous allons vous rappeler!";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_BUTTON_CAPTION"] = "Me rappeler";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_RESULT_SUCCESS_TEXT"] = "Notre représentant vous contactera dans quelques secondes.";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_CALL_TEXT"] = "Un rappel a été demandé; vous êtes en train d'être connecté au client.";
?>