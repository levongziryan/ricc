<?
$MESS["CRM_WEBFORM_FORM_ERROR_SCHEME"] = "Schéma de document invalide.";
$MESS["CRM_WEBFORM_FORM_BUTTON_CAPTION_DEFAULT"] = "Envoyer";
$MESS["CRM_WEBFORM_FORM_SCRIPT_BUTTON_TEXT"] = "Nom du bouton";
$MESS["CRM_WEBFORM_FORM_COPY_NAME_PREFIX"] = "Copier";
$MESS["CRM_WEBFORM_FORM_ERROR_CAPTCHA_KEY"] = "Une clé ainsi qu'une clé secrète sont requises pour utiliser reCAPTCHA";
$MESS["CRM_WEBFORM_FORM_PRESET_FIELDS"] = "Dans les champs par défaut";
?>