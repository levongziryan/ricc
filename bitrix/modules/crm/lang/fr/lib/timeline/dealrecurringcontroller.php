<?
$MESS["CRM_DEAL_RECURRING_NEXT_EXECUTION"] = "Prochaine date de création";
$MESS["CRM_DEAL_RECURRING_NEXT_EXECUTION_CHANGED"] = "Prochaine date de création modifiée";
$MESS["CRM_DEAL_RECURRING_NOT_ACTIVE"] = "La transaction récurrente n'est pas active";
$MESS["CRM_DEAL_RECURRING_ACTIVE"] = "La transaction récurrente est active";
$MESS["CRM_DEAL_CREATION"] = "Transaction ajoutée";
$MESS["CRM_DEAL_RECURRING_CREATION"] = "Modèle de transaction créé";
$MESS["CRM_RECURRING_CREATION_BASED_ON"] = "Créé sur la base de";
$MESS["CRM_DEAL_BASE_CAPTION_BASED_ON_DEAL"] = "Basé sur la transaction";
?>