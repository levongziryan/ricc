<?
$MESS["CRM_DEFAULT_TITLE"] = "Sans nom";
$MESS["CRM_INV_RQ_CONV_INTRO"] = "Vous n'avez plus besoin de saisir les données d'une société dans vos factures à chaque fois que vous en créez. Ajoutez les données des sociétés de vos clients dans le CRM, et vous serez en mesure de les utiliser quand vous travaillerez avec divers enregistrements CRM. Si vous avez déjà entré des données d'une société, vous pouvez les transférer automatiquement.  <a id=\"#ID#\" href=\"#URL#\">Transférer</a>.";
?>