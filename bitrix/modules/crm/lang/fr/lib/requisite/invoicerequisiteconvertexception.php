<?
$MESS["CRM_INV_RQ_CONV_ERROR_GENERAL"] = "Erreur générale.";
$MESS["CRM_INV_RQ_CONV_ERROR_PRESET_NOT_BOUND"] = "Le modèle sélectionné n'a pas de champ pouvant être rempli en utilisant des informations existantes.";
$MESS["CRM_INV_RQ_CONV_ERROR_PERSON_TYPE_NOT_FOUND"] = "Type de payeur introuvable.";
$MESS["CRM_INV_RQ_CONV_ERROR_PROPERTY_NOT_FOUND"] = "Transfert de données impossible parce que les détails de facture existants ont été supprimés.";
?>