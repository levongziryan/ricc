<?
$MESS["CRM_INV_RQ_CONV_ERROR_GENERAL"] = "Erreur générale";
$MESS["CRM_INV_RQ_CONV_ERROR_PRESET_NOT_BOUND"] = "Le modèle que vous avez choisi ne possède pas de champ pour y transférer les données des anciennes sociétés.";
$MESS["CRM_INV_RQ_CONV_ERROR_PERSON_TYPE_NOT_FOUND"] = "Le type de payeur ne peut être localisé.";
$MESS["CRM_INV_RQ_CONV_ERROR_PROPERTY_NOT_FOUND"] = "Les données des anciennes sociétés destinées aux factures ont été supprimées. Le transfert n'a pu être finalisé";
?>