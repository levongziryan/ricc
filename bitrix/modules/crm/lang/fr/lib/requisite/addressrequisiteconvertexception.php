<?
$MESS["CRM_ADDR_CONV_EX_COMPANY_ACCESS_DENIED"] = "L'accès à la société a été refusé";
$MESS["CRM_ADDR_CONV_EX_CONTACT_ACCESS_DENIED"] = "L'accès au contact a été refusé";
$MESS["CRM_ADDR_CONV_EX_COMPANY_CREATION_FAILED"] = "Création des informations de la société impossible";
$MESS["CRM_ADDR_CONV_EX_CONTACT_CREATION_FAILED"] = "Création de l'élément informations de contact impossible";
?>