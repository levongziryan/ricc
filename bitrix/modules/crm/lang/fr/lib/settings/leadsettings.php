<?
$MESS["CRM_LEAD_SETTINGS_VIEW_WIDGET"] = "Analytique des prospects";
$MESS["CRM_LEAD_SETTINGS_VIEW_LIST"] = "Liste des prospects";
$MESS["CRM_TYPE_TITLE"] = "Choisissez la façon dont vous souhaitez travailler avec votre CRM";
$MESS["CRM_TYPE_SAVE"] = "Enregistrer";
$MESS["CRM_TYPE_CANCEL"] = "Annuler";
$MESS["CRM_TYPE_TURN_ON"] = "Activer";
$MESS["CRM_ROBOTS_TITLE"] = "Mode CRM simple";
$MESS["CRM_ROBOTS_TEXT"] = "Dans le mode CRM simple, Bitrix24 convertit automatiquement les nouveaux leads en transactions selon des règles d'automatisation.<br/><br/> Vous avez déjà des règles d'automatisation actives pour le moment. En activant ce mode, celles-ci seront supprimées. <br/><br/>Voulez-vous vraiment activer le mode CRM simple ?";
$MESS["CRM_LEAD_SETTINGS_VIEW_KANBAN"] = "Kanban de lead";
?>