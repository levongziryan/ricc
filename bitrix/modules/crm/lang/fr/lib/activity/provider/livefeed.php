<?
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_NAME"] = "Flux d'activités";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TYPE_LOG_ENTRY"] = "Message du Flux d'activités";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TYPE_LOG_COMMENT"] = "Commentaire sur le message du Flux d'activités";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TYPE_LOG_COMMENT_OUT"] = "Commentaire sur le message du Flux d'activités (sortant)";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TYPE_LOG_COMMENT_IN"] = "Commentaire sur le message du Flux d'activités (entrant";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TITLE_ENTRY"] = "Message du Flux d'activités";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TITLE_COMMENT_IN"] = "Commentaire entrant sur le message du Flux d'activités";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TITLE_COMMENT_OUT"] = "Commentaire sortant sur le message du Flux d'activités";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_LINK_ENTRY"] = "Aller au message";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_LINK_COMMENT"] = "Aller au commentaire";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_AUTHOR_ACTION"] = "#USER_NAME# a écrit:";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_AUTHOR_ACTIONM"] = "#USER_NAME# a écrit:";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_AUTHOR_ACTIONF"] = "#USER_NAME# a écrit:";
?>