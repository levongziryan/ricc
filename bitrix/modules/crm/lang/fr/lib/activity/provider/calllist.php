<?
$MESS["CRM_ACTIVITY_PROVIDER_CALL_LIST_TITLE"] = "Liste d’appel";
$MESS["CRM_CALL_LIST_NOT_CREATED_ERROR"] = "La liste d’appel n’existe pas. Pour créer la liste, cliquez sur le lien dans la rubrique \"Liste d'appel\".";
$MESS["CRM_CALL_LIST_SUBJECT_EMPTY"] = "Aucun sujet d’activité spécifié";
$MESS["CRM_CALL_LIST_RESPONSIBLE_IM_NOTIFY"] = "Vous être maintenant responsable de la liste d'appel \"#title#\"";
$MESS["CRM_CALL_LIST_USE_WEBFORM"] = "Utiliser le formulaire";
?>