<?
$MESS["CRM_ACTIVITY_PROVIDER_MEETING_NAME"] = "Réunion";
$MESS["CRM_ACTIVITY_PROVIDER_MEETING_PLANNER_ACTION_NAME"] = "Réunion";
$MESS["CRM_ACTIVITY_PROVIDER_MEETING_PLANNER_SUBJECT_LABEL"] = "Objet de la réunion";
$MESS["CRM_ACTIVITY_PROVIDER_MEETING_PLANNER_LOCATION_LABEL"] = "Localisation";
$MESS["CRM_ACTIVITY_PROVIDER_MEETING_SUBJECT"] = "Réunion avec #TITLE#";
$MESS["CRM_ACTIVITY_PROVIDER_MEETING_PLANNER_DESCRIPTION_LABEL"] = "Description";
$MESS["CRM_ACTIVITY_PROVIDER_MEETING_PLANNER_PLACE_LABEL"] = "Localisation";
?>