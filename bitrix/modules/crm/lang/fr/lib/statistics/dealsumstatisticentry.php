<?
$MESS["CRM_DEAL_SUM_STAT_ENTRY_SLOT_SUM_TOTAL"] = "Somme totale";
$MESS["CRM_DEAL_SUM_STAT_ENTRY_TITLE"] = "Transactions";
$MESS["CRM_DEAL_SUM_STAT_ENTRY_REBUILD"] = "Les rapports nécessitent que vous <a id=\"#ID#\" href=\"#URL#\">mettiez à jour les statistiques des transactions</a> pour pouvoir fonctionner correctement.";
$MESS["CRM_DEAL_SUM_STAT_ENTRY_REBUILD_DLG_TITLE"] = "Mettre à jour les statistiques des transactions";
$MESS["CRM_DEAL_SUM_STAT_ENTRY_REBUILD_DLG_SUMMARY"] = "Cela va permettre de mettre à jour les statistiques des transactions. Cette opération peut prendre un peu de temps.";
?>