<?
$MESS["CRM_INVOICE_SUM_STAT_ENTRY_SLOT_SUM_TOTAL"] = "Montant total";
$MESS["CRM_INVOICE_SUM_STAT_ENTRY_TITLE"] = "Factures";
$MESS["CRM_INVOICE_SUM_STAT_ENTRY_REBUILD"] = "Veuillez <a id=\"#ID#\" href=\"#URL#\">mettre à jour les statistiques des factures</a> pour recevoir des rapports corrects.";
$MESS["CRM_INVOICE_SUM_STAT_ENTRY_REBUILD_DLG_TITLE"] = "Mettre à jour les statistiques des factures";
$MESS["CRM_INVOICE_SUM_STAT_ENTRY_REBUILD_DLG_SUMMARY"] = "Cela permet de mettre à jour les statistiques des factures. Cette opération peut prendre du temps.";
?>