<?
$MESS["CRM_STAT_MGR_REBUILD_STATISTICS"] = "Veuillez <a id=\"#ID#\" href=\"#URL#\">mettre à jour les statistiques</a> pour que les rapports montre les bonnes données.";
$MESS["CRM_STAT_MGR_REBUILD_STATISTICS_DLG_TITLE"] = "Mettre à jour les statistiques";
$MESS["CRM_STAT_MGR_REBUILD_STATISTICS_DLG_SUMMARY"] = "Cela permet de mettre à jour les données statisitiques. Cette opération peut prendre un peu de temps.";
?>