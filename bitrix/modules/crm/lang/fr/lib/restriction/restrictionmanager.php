<?
$MESS["CRM_RESTR_MGR_DUP_CTRL_MSG_CONTENT"] = "<div class=\"crm-duplicate-tab-content\">
<h3 class=\"crm-duplicate-tab-title\">Recherche de doubles avancée</h3>
<div class=\"crm-duplicate-tab-text\">
Quand un nouveau contact est créé (ou une prospect, ou une société), Bitrix24 recherche et affiche les potentiels doubles &mdash; ce qui empêche de créer des copies.
</div>
<div class=\"crm-duplicate-tab-text\">
Avec la recherche de doubles avancée, la CRM peut aussi trouver les doubles contenus dans les données importées et celles déjà entrées dans la base de données. Ces doubles peuvent être fusionnés 
</div>
<div class=\"crm-duplicate-tab-text\">
Encore une grande fonctionnalité que vous pouvez ajouter à votre Bitrix24 ! Téléphonie avancée + CRM avancée et d'autres fonctionnalités utiles sont disponibles dans l'offre Bitrix24 Plus pour seulement 39\$/mois. 
<a target=\"_blank\" href=\"https://www.bitrix24.com/pro/crm.php\">En savoir plus</a>
</div>
<div class=\"crm-history-tab-buttons\">
<span class=\"webform-button webform-button-create\" onclick=\"#LICENSE_LIST_SCRIPT#\">Profitez d'une offre étendue</span>
<span class=\"webform-button webform-button-transparent\" onclick=\"#DEMO_LICENSE_SCRIPT#\">Obtenez une offre d'essai 30 jours gratuite</span>
</div>
</div>
";
$MESS["CRM_RESTR_MGR_HX_VIEW_MSG_CONTENT"] = "<div class=\"crm-history-tab-content\">
	<h3 class=\"crm-history-tab-title\">L'historique CRM est uniquement disponible dans la CRM avancée</h3>
	<div class=\"crm-history-tab-text\">
		Bitrix24 garde un journal détaillé de toutes les modifications de CRM. Quand vous passez en CRM avancée, vous avez accès à l'historique des modifications (c'est-à-dire de qui a accédé et a changé une entrée CRM) et vous pouvez restaurer des valeurs modifiées, si nécessaire.
	</div>
	<div class=\"crm-history-tab-text\">Voici à quoi cela ressemble :</div>
	<img alt=\"Tab History\" class=\"crm-history-tab-img\" src=\"/bitrix/js/crm/images/history-en.png\"/>
	<div class=\"crm-history-tab-text\">
		Une autre grande fonctionnalité que vous pouvez ajouter à votre Bitrix24! Téléphonie avancée + CRM avancée et d'autres fonctionnalités utiles sont disponibles dans l'offre Bitrix24 Plus pour seulement 39\$/mois. 
	<a target=\"_blank\" href=\"https://www.bitrix24.com/pro/crm.php\">En savoir plus</a>
	</div>
	<div class=\"crm-history-tab-buttons\">
		<span class=\"webform-button webform-button-create\" onclick=\"#LICENSE_LIST_SCRIPT#\">Profitez d'une offre étendue</span>
		<span class=\"webform-button webform-button-transparent\" onclick=\"#DEMO_LICENSE_SCRIPT#\">Gratuit pendant 30 jours</span>
	</div>
</div>";
$MESS["CRM_RESTR_MGR_POPUP_TITLE"] = "CRM avancée dans Bitrix24";
$MESS["CRM_RESTR_MGR_POPUP_CONTENT"] = "Ajoutez ce qui suit à votre CRM:
<ul class=\"hide-features-list\">
 <li class=\"hide-features-list-item>Conversions entre transactions, factures et devis</li>
<li class=\"hide-features-list-item\">Recherche de doubles avancée</li>
<li class=\"hide-features-list-item\">Récupération et annulation des modifications apportées aux entrées CRM via l'historique</li>
<li class=\"hide-features-list-item\">Journal d'accès CRM</li>
<li class=\"hide-features-list-item\">Consultation de plus de 5000 enregistrements dans la CRM</li>
<li class=\"hide-features-list-item\">Appels basés sur une liste<sup class=\"hide-features-soon\">bientôt</sup></li>
<li class=\"hide-features-list-item\">Envoi de groupes d'e-mails aux clients<sup class=\"hide-features-soon\">bientôt</sup></li>
<li class=\"hide-features-list-item\">Prise en charge de la vente de services<sup class=\"hide-features-soon\">bientôt</sup>
<a target=\"_blank\" class=\"hide-features-more\" href=\"https://www.bitrix24.com/pro/crm.php\">Découvrez-en plus</a>
</li>
</ul>
<strong>Téléphonie avancée + CRM avancée et d'autres fonctionnalités utiles sont disponibles dans l'offre Bitrix24 Plus pour seulement 39\$/mois.</strong>

";
$MESS["CRM_RESTR_MGR_DEAL_CATEGORY_POPUP_TITLE"] = "Pipelines CRM multiples";
$MESS["CRM_RESTR_MGR_DEAL_CATEGORY_POPUP_CONTENT"] = "<div class=\"crm-deal-category-tab-content\">
 <div class=\"crm-deal-category-tab-text\">
  Les abonnements Bitrix24 Free et Plus sont limités à un seul pipeline/canal de ventes. Pour ajouter un second pipeline d'affaires, veuillez passer au plan Bitrix24 Standard. Pour déverrouiller des pipelines et canaux de ventes illimités, veuillez passer à Bitrix24 Professional.
 </div>
</div>";
?>