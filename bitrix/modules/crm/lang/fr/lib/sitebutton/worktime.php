<?
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_MO"] = "Lun";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_TU"] = "Mar";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_WE"] = "Mer";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_TH"] = "Jeu";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_FR"] = "Ven";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_SA"] = "Sam";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_SU"] = "Dim";
$MESS["CRM_SITE_BUTTON_WORK_TIME_ACTIONS_HIDE"] = "masquer le bouton de rappel";
$MESS["CRM_SITE_BUTTON_WORK_TIME_ACTIONS_TEXT"] = "enregistrer les informations du client et afficher un message";
$MESS["CRM_SITE_BUTTON_WORK_TIME_ACTIONS_TEXT_CALLBACK"] = "Malheureusement, nous ne pouvons actuellement pas vous rappeler. Nous vous contacterons dès que possible dans les heures de bureau.";
?>