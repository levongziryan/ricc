<?
$MESS["CRM_BUTTON_LOCATION_TOP_LEFT"] = "Supérieur gauche";
$MESS["CRM_BUTTON_LOCATION_TOP_MIDDLE"] = "Supérieur centre";
$MESS["CRM_BUTTON_LOCATION_TOP_RIGHT"] = "Supérieur droit";
$MESS["CRM_BUTTON_LOCATION_BOTTOM_RIGHT"] = "Inférieur droit";
$MESS["CRM_BUTTON_LOCATION_BOTTOM_MIDDLE"] = "Inférieur centre";
$MESS["CRM_BUTTON_LOCATION_BOTTOM_LEFT"] = "Inférieur gauche";
$MESS["CRM_BUTTON_FIELD_NAME_EMPTY"] = "Nom du bouton";
$MESS["CRM_BUTTON_FIELD_NAME_TITLE"] = "Nom du widget";
?>