<?
$MESS["CRM_LEAD_ENTITY_EMAIL"] = "Courrier électronique";
$MESS["CRM_LEAD_ENTITY_ID"] = "ID";
$MESS["CRM_LEAD_ENTITY_ADDRESS"] = "Adresse";
$MESS["CRM_LEAD_ENTITY_CURRENCY_ID"] = "Devises";
$MESS["CRM_LEAD_ENTITY_CURRENCY_BY"] = "Devises";
$MESS["CRM_LEAD_ENTITY_DATE_MODIFY"] = "Date de modification";
$MESS["CRM_LEAD_ENTITY_DATE_CREATE"] = "Créé le";
$MESS["CRM_LEAD_ENTITY_POST"] = "Emplacement";
$MESS["CRM_LEAD_ENTITY_STATUS_DESCRIPTION"] = "Information supplémentaire sur le statut";
$MESS["CRM_LEAD_ENTITY_SOURCE_DESCRIPTION"] = "Plus d'informations sur la source";
$MESS["CRM_LEAD_ENTITY_MODIFY_BY"] = "Modifié(e)s par";
$MESS["CRM_LEAD_ENTITY_MODIFY_BY_ID"] = "Modifié(e)s par";
$MESS["CRM_LEAD_ENTITY_NAME"] = "Prénom";
$MESS["CRM_LEAD_ENTITY_SOURCE_BY"] = "Chemin";
$MESS["CRM_LEAD_ENTITY_COMMENTS"] = "Commentaire";
$MESS["CRM_LEAD_ENTITY_COMPANY_ID"] = "Entreprise";
$MESS["CRM_LEAD_ENTITY_CONTACT_ID"] = "Client";
$MESS["CRM_LEAD_ENTITY_MESSENGER"] = "Messagerie";
$MESS["CRM_LEAD_ENTITY_TITLE"] = "Dénomination";
$MESS["CRM_LEAD_ENTITY_COMPANY_TITLE"] = "Nom de l'entreprise";
$MESS["CRM_LEAD_ENTITY_ASSIGNED_BY_ID"] = "Responsable";
$MESS["CRM_LEAD_ENTITY_ASSIGNED_BY"] = "Responsable";
$MESS["CRM_LEAD_ENTITY_SECOND_NAME"] = "Patronyme";
$MESS["CRM_LEAD_ENTITY_PRODUCT_ID"] = "Article";
$MESS["CRM_LEAD_ENTITY_PRODUCT_BY"] = "Article";
$MESS["CRM_LEAD_ENTITY_WEB"] = "Site web";
$MESS["CRM_LEAD_ENTITY_EVENT_RELATION"] = "Evénements";
$MESS["CRM_LEAD_ENTITY_CREATED_BY"] = "Créé par";
$MESS["CRM_LEAD_ENTITY_CREATED_BY_ID"] = "Créé par";
$MESS["CRM_LEAD_ENTITY_STATUS_BY"] = "Statut";
$MESS["CRM_LEAD_ENTITY_OPPORTUNITY"] = "Total";
$MESS["CRM_LEAD_ENTITY_PHONE"] = "Numéro de téléphone";
$MESS["CRM_LEAD_ENTITY_LAST_NAME"] = "Nom";
?>