<?
$MESS["CRM_ACTIVITY_TYPE_CALL"] = "Appel";
$MESS["CRM_ACTIVITY_TYPE_MEETING"] = "Rencontre";
$MESS["CRM_ACTIVITY_TYPE_TASK"] = "Tâche";
$MESS["CRM_ACTIVITY_TYPE_EMAIL"] = "Courrier électronique";
$MESS["CRM_ACTIVITY_TYPE_ACTIVITY"] = "Activité";
$MESS["CRM_NOTIFY_TYPE_MIN"] = "min.";
$MESS["CRM_NOTIFY_TYPE_HOUR"] = "h.";
$MESS["CRM_NOTIFY_TYPE_DAY"] = "jours";
$MESS["CRM_PRIORITY_LOW"] = "basse";
$MESS["CRM_PRIORITY_MEDIUM"] = "moyenne";
$MESS["CRM_PRIORITY_HIGH"] = "haut";
$MESS["CRM_ACTIVITY_STATUS_WAITING"] = "Attend l'exécution";
$MESS["CRM_ACTIVITY_STATUS_COMPLETED"] = "Achevé(e)s";
$MESS["CRM_ACTIVITY_CHANGE_EVENT_SUBJECT"] = "Sujet d'action est modifié";
$MESS["CRM_ACTIVITY_CHANGE_CALL_SUBJECT"] = "Le sujet de l'appel est modifié";
$MESS["CRM_ACTIVITY_CHANGE_MEETING_SUBJECT"] = "Le sujet de la réunion est changé";
$MESS["CRM_ACTIVITY_CHANGE_TASK_SUBJECT"] = "Le nom de tâche modifié";
$MESS["CRM_ACTIVITY_CHANGE_EVENT_START"] = "L'heure du début de l'action '#NAME#' est changée";
$MESS["CRM_ACTIVITY_CHANGE_CALL_START"] = "Le temps de début de l'appel '#NAME#' est remis";
$MESS["CRM_ACTIVITY_CHANGE_TASK_START"] = "Le début de la tâche '#NAME#' reporté";
$MESS["CRM_ACTIVITY_CHANGE_EVENT_END"] = "Il est changé le moment de la réalisation de l'action '#NAME#'";
$MESS["CRM_ACTIVITY_CHANGE_CALL_END"] = "La durée de l'appel '#NAME#' a été prolongée";
$MESS["CRM_ACTIVITY_CHANGE_TASK_END"] = "L'heure d'achèvement de la tâche '#NAME#' a été modifiée";
$MESS["CRM_ACTIVITY_CHANGE_EVENT_COMPLETED"] = "Le statut de l'action '#NAME#' a été changé";
$MESS["CRM_ACTIVITY_CHANGE_CALL_COMPLETED"] = "Le statut de l'appel '#NAME#' a été modifié";
$MESS["CRM_ACTIVITY_CHANGE_TASK_COMPLETED"] = "Le statut de la tâche '#NAME#' a été modifié";
$MESS["CRM_ACTIVITY_CHANGE_EVENT_PRIORITY"] = "L'importance de l'action '#NAME#' a été modifiée";
$MESS["CRM_ACTIVITY_CHANGE_CALL_PRIORITY"] = "La priorité de l'appel '#NAME#' a été modifiée";
$MESS["CRM_ACTIVITY_CHANGE_TASK_PRIORITY"] = "Le degré d'importance de la tâche '#NAME#'a été modifié";
$MESS["CRM_ACTIVITY_CHANGE_EVENT_NOTIFY"] = "La notification sur le commencement de l'action '#NAME#' est modifiée";
$MESS["CRM_ACTIVITY_CHANGE_CALL_NOTIFY"] = "La notification sur le commencement de l'appel '#NAME#' a été modifiée";
$MESS["CRM_ACTIVITY_CHANGE_EVENT_DESCRIPTION"] = "Description de l'opération '#NAME#' est changée";
$MESS["CRM_ACTIVITY_CHANGE_CALL_DESCRIPTION"] = "La description d'appel '#NAME#' a été modifiée";
$MESS["CRM_ACTIVITY_CHANGE_TASK_DESCRIPTION"] = "Description de la tâche '#NAME#' a été changée";
$MESS["CRM_ACTIVITY_CHANGE_MEETING_DESCRIPTION"] = "La description de la rencontre '#NAME#' est modifiée.";
$MESS["CRM_ACTIVITY_CHANGE_MEETING_LOCATION"] = "Le lieu de la réunion '#NAME#' a été changé";
$MESS["CRM_ACTIVITY_CHANGE_CALL_DIRECTION"] = "Le type d'appel '#NAME#' a été changé";
$MESS["CRM_ACTIVITY_CHANGE_MEETING_RESPONSIBLE_ID"] = "Le responsable du rendez-vous '#NAME#' a été modifié";
$MESS["CRM_ACTIVITY_CHANGE_CALL_RESPONSIBLE_ID"] = "Le responsable de l'appel '#NAME#' a été modifié";
$MESS["CRM_ACTIVITY_CALL_FILE_ADD"] = "Le fichier est joint à l'appel '#NAME#'";
$MESS["CRM_ACTIVITY_MEETING_FILE_ADD"] = "Un fichier a été joint à la rencontre '#NAME#'";
$MESS["CRM_ACTIVITY_EMAIL_FILE_ADD"] = "Un fichier a été rattaché au message '#NAME#'";
$MESS["CRM_ACTIVITY_TASK_FILE_ADD"] = "Un fichier a été joint à la tâche '#NAME#'";
$MESS["CRM_ACTIVITY_CALL_FILE_REMOVE"] = "Fichier supprimé de l'appel '#NAME#'";
$MESS["CRM_ACTIVITY_MEETING_FILE_REMOVE"] = "Fichier supprimé de la rencontre #NAME#";
$MESS["CRM_ACTIVITY_EMAIL_FILE_REMOVE"] = "Un fichier a été éliminé de la lettre '#NAME#'";
$MESS["CRM_ACTIVITY_TASK_FILE_REMOVE"] = "Le fichier a été supprimé de la tâche '#NAME#'";
$MESS["CRM_ACTIVITY_EVENT_ADD"] = "L'action a été créée";
$MESS["CRM_ACTIVITY_CALL_ADD"] = "Appel créé";
$MESS["CRM_ACTIVITY_MEETING_ADD"] = "L'entretien a été créé";
$MESS["CRM_ACTIVITY_TASK_ADD"] = "La tâche est créée";
$MESS["CRM_ACTIVITY_EMAIL_ADD"] = "La lettre a été créée";
$MESS["CRM_ACTIVITY_EVENT_REMOVE"] = "Action est supprimée";
$MESS["CRM_ACTIVITY_CALL_REMOVE"] = "Appel supprimé";
$MESS["CRM_ACTIVITY_MEETING_REMOVE"] = "Un rendez-vous a été supprimé";
$MESS["CRM_ACTIVITY_TASK_REMOVE"] = "Tâche supprimée";
$MESS["CRM_ACTIVITY_EMAIL_REMOVE"] = "Lettre supprimée";
$MESS["CRM_ACTIVITY_FILE_ADD_CALL"] = "Un fichier a été ajouté à l'appel '#NAME#'";
$MESS["CRM_ACTIVITY_FILE_ADD_MEETING"] = "Un fichier a été ajouté à la rencontre '#NAME#'";
$MESS["CRM_ACTIVITY_FILE_ADD_EMAIL"] = "Le fichier a été ajouté à la lettre '#NAME#'";
$MESS["CRM_ACTIVITY_FILE_REMOVE_CALL"] = "Le fichier est supprimé de l'appel '#NAME#'";
$MESS["CRM_ACTIVITY_FILE_REMOVE_MEETING"] = "Le fichier a été éliminé du rendez-vous '#NAME#'";
$MESS["CRM_ACTIVITY_COMM_PHONE_ADD_CALL"] = "Le numéro de téléphone est rajouté à l'appel '#NAME#'";
$MESS["CRM_ACTIVITY_COMM_PHONE_ADD_MEETING"] = "Le téléphone a été ajouté à l'entretien '#NAME#'";
$MESS["CRM_ACTIVITY_COMM_EMAIL_ADD_EMAIL"] = "Adresse ajouté au message '#NAME#'";
$MESS["CRM_ACTIVITY_COMM_PHONE_REMOVE_CALL"] = "Téléphone d'appel '#NAME#' supprimé";
$MESS["CRM_ACTIVITY_COMM_PHONE_REMOVE_MEETING"] = "Le téléphone est supprimé de la rencontre '#NAME#'";
$MESS["CRM_ACTIVITY_DIRECTION_INCOMING"] = "A/pour moi";
$MESS["CRM_ACTIVITY_DIRECTION_OUTGOING"] = "Emis";
$MESS["CRM_ACTIVITY_CALL_DIRECTION_INCOMING"] = "Appel reçu";
$MESS["CRM_ACTIVITY_CALL_DIRECTION_OUTGOING"] = "Appel sortant";
$MESS["CRM_ACTIVITY_EMAIL_DIRECTION_INCOMING"] = "Lettre reçue";
$MESS["CRM_ACTIVITY_EMAIL_DIRECTION_OUTGOING"] = "Adresse email sortant";
$MESS["CRM_ACTIVITY_FROM_DEAL_EVENT_CALL"] = "Appel téléphonique";
$MESS["CRM_ACTIVITY_FROM_DEAL_EVENT_INFO"] = "Information";
$MESS["CRM_ACTIVITY_NOTIFY_MESSAGE_INCOMING_EMAIL"] = "Vous avez reçu <a target='_ blank' href='#VIEW_URL#'>un nouveau message</a>.";
$MESS["CRM_ACTIVITY_NOTIFY_MESSAGE_INCOMING_EMAIL_EXT"] = "La lettre <a target='_blank' href='#VIEW_URL#'>#SUBJECT#</a> envoyée par #ADDRESSER# est arrivée à votre adresse.";
$MESS["CRM_ACTIVITY_SUBJECT"] = "En-tête";
$MESS["CRM_ACTIVITY_LOCATION"] = "Adresse domicile";
$MESS["CRM_ACTIVITY_FIELD_COMPARE_EMPTY"] = "- Vide -";
$MESS["CRM_ACTIVITY_EMAIL_SUBJECT"] = "En-tête";
$MESS["CRM_ACTIVITY_EMAIL_FROM"] = "De qui";
$MESS["CRM_ACTIVITY_EMAIL_TO"] = "Dans";
$MESS["CRM_ACTIVITY_MEETING_RESPONSIBLE_IM_NOTIFY"] = "Vous êtes nommé responsable de l'entretien '#title#'";
$MESS["CRM_ACTIVITY_EMAIL_RESPONSIBLE_IM_NOTIFY"] = "Vous êtes nommé responsable de la lettre '#title#'";
$MESS["CRM_ACTIVITY_CALL_RESPONSIBLE_IM_NOTIFY"] = "Vous êtes désignés responsable d'appel '#title#'";
$MESS["CRM_ACTIVITY_TASK_RESPONSIBLE_IM_NOTIFY"] = "Vous êtes mis en charge de la tâche '#title#'";
$MESS["CRM_ACTIVITY_MEETING_NOT_RESPONSIBLE_IM_NOTIFY"] = "Vous n'êtes plus responsable de la réunion '#title#'";
$MESS["CRM_ACTIVITY_EMAIL_NOT_RESPONSIBLE_IM_NOTIFY"] = "Vous n'êtes plus responsable de la lettre '# title#'";
$MESS["CRM_ACTIVITY_CALL_NOT_RESPONSIBLE_IM_NOTIFY"] = "Vous n'êtes plus responsable d'un appel '# titre#'";
$MESS["CRM_ACTIVITY_TASK_NOT_RESPONSIBLE_IM_NOTIFY"] = "Vous avez cessé d'être responsable de la tâche '#title#'";
$MESS["CRM_ACTIVITY_CALL_DURATION"] = "Durée de la conversation: #DURATION# secondes";
$MESS["CRM_ACTIVITY_OUTGOING_CALL_NO_RESPONSE"] = "L'abonné ne répond pas ou est provisoirement inaccessible.";
$MESS["CRM_ACTIVITY_TYPE_PROVIDER"] = "Action d'utilisateur";
$MESS["CRM_ACTIVITY_STATUS_AUTO_COMPLETED"] = "Terminé automatiquement";
?>