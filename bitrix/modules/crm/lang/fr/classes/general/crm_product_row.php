<?
$MESS["CRM_EVENT_PROD_ROW_ADD"] = "Ajout d'une marchandise";
$MESS["CRM_EVENT_PROD_ROW_UPD"] = "Le produit est modifié";
$MESS["CRM_EVENT_PROD_ROW_PRICE_UPD"] = "Le prix du produit '#NAME#' a été modifié";
$MESS["CRM_EVENT_PROD_ROW_QTY_UPD"] = "Le nombre du produit '#NAME#' a été modifié";
$MESS["CRM_EVENT_PROD_ROW_REM"] = "Produit a été supprimé";
$MESS["CRM_EVENT_PROD_ROW_NAME_UPD"] = "La dénomination de la marchandise a été changée";
$MESS["CRM_EVENT_PROD_ROW_DISCOUNT_UPD"] = "La rabais pour le produit '#NAME#' est modifiée";
$MESS["CRM_EVENT_PROD_ROW_TAX_UPD"] = "Taxe modifiée sur la marchandises '#NAME#'";
$MESS["CRM_EVENT_PROD_ROW_MEASURE_UPD"] = "L'unité de marchandise '#NAME#' a été modifiée";
?>