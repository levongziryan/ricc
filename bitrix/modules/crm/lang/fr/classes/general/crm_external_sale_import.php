<?
$MESS["CRM_EXT_SALE_IM_GROUP"] = "groupe";
$MESS["CRM_EXT_SALE_IMPORT_UNKNOWN_ANSW_PERMS"] = "Vous ne disposez pas des droits nécessaires. Veuillez vérifier si l'utilisateur a le droit d'exporter des commandes de la boutique.";
$MESS["CRM_EXT_SALE_IMPORT_UNKNOWN_ANSW_PERMS1"] = "Vous ne disposez pas des droits nécessaires. Veuillez vérifier si l'utilisateur a le droit d'exporter des commandes de la boutique.";
$MESS["CRM_EXT_SALE_IMPORT_UNKNOWN_ANSW"] = "La réponse du serveur n'est pas identifiée. Les premiers 100 symboles de la réponse:";
$MESS["CRM_EXT_SALE_TITLE_ERROR_SETTINGS"] = "Erreur d'importation";
$MESS["CRM_EXT_SALE_IMPORT_UNKNOWN_ANSW_F"] = "Erreur d'échange de données avec le magasin.";
$MESS["CRM_EXT_SALE_IMPORT_ERROR_XML"] = "Erreur de traitement de la réponse du serveur.";
$MESS["CRM_EXT_SALE_IMPORT_EMPTY_ANSW"] = "Réponse vide du serveur.";
$MESS["CRM_EXT_SALE_TITLE_SETTINGS"] = "Résultat de l'importation";
$MESS["CRM_GCES_NOTIFY_ERROR_MESSAGE"] = "Échec de la synchronisation:<br />tentatives de connexion: 10, dernière #DATE#<br />connexion convertie au statut: 'inactif'<br /><br /> Pour afficher les détails, accédez au paramètre de liaison de la CRM avec la boutique en ligne (<a href='#URL#'>CRM &gt; Paramètres &gt; Connexion avec la boutique en ligne</a>).";
$MESS["CRM_GCES_NOTIFY_MESSAGE"] = "La synchronisation est réussie, il est obtenu<br />#TOTALDEALS# <a href='#DEAL_URL#'>deals</a> (nouvelles #CREATEDDEALS#)<br />#TOTALCONTACTS# <a href='#CONTACT_URL#'>contacts</a> (nouvelles #CREATEDCONTACTS#)<br />#TOTALCOMPANIES# <a href='#COMPANY_URL#'>entreprises</a> (#CREATEDCOMPANIES# new)";
$MESS["CRM_GCES_NOTIFY_TITLE"] = "Synchronisation avec '#NAME#'";
$MESS["CRM_GCES_NOTIFY_ERROR_TITLE"] = "Synchronisation avec '#NAME#'";
?>