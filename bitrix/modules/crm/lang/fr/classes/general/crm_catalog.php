<?
$MESS["CRM_PRODUCT_CATALOG_TYPE_TITLE"] = "Catalogues CRM";
$MESS["CRM_PRODUCT_CATALOG_SECTION_NAME"] = "Catégorie";
$MESS["CRM_ERR_REGISTER_CATALOG"] = "Erreur d'enregistrement du catalogue des produits.";
$MESS["CRM_PRODUCT_CATALOG_PRODUCT_NAME"] = "Article";
$MESS["CRM_PRODUCT_CATALOG_TITLE"] = "Catalogue de produits CRM";
?>