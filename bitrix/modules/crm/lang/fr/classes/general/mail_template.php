<?
$MESS["CRM_MAIL_TEMPLATE_FIELD_OWNER_ID"] = "Organisateur";
$MESS["CRM_MAIL_TEMPLATE_SCOPE_COMMON"] = "tous";
$MESS["CRM_MAIL_TEMPLATE_FIELD_TITLE"] = "Dénomination";
$MESS["CRM_MAIL_TEMPLATE_ERROR_NOT_EXISTS"] = "La tentative de trouver l'élément avec ID# #ID# a échoué.";
$MESS["CRM_MAIL_TEMPLATE_ERROR_FIELD_NOT_SPECIFIED"] = "Veuillez renseigner une valeur pour le champ '#FIELD#'.";
$MESS["CRM_MAIL_TEMPLATE_FIELD_ENTITY_TYPE_ID"] = "Bloc Highload";
$MESS["CRM_MAIL_TEMPLATE_SCOPE_PERSONAL"] = "seulement pour moi";
?>