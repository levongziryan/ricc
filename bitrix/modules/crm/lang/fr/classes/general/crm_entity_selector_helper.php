<?
$MESS["CRM_FF_OK"] = "Choisir";
$MESS["CRM_FF_CANCEL"] = "Annuler";
$MESS["CRM_FF_CLOSE"] = "Fermer";
$MESS["CRM_FF_WAIT"] = "Recherche...";
$MESS["CRM_FF_NO_RESULT"] = "Malheureusement, il n'y a pas d'éléments retrouvés d'après votre demande de recherche.";
$MESS["CRM_FF_CHOISE"] = "Ajouter le produit";
$MESS["CRM_FF_CHANGE"] = "Editer";
$MESS["CRM_FF_LAST"] = "Dernier";
$MESS["CRM_FF_SEARCH"] = "Recherche";
$MESS["CRM_FF_LEAD"] = "Prospects";
$MESS["CRM_FF_CONTACT"] = "Contacts";
$MESS["CRM_FF_COMPANY"] = "Entreprises";
$MESS["CRM_FF_DEAL"] = "Transactions";
$MESS["CRM_FF_QUOTE"] = "Liste d'offres";
$MESS["CRM_ENT_SEL_HLP_PREF_CONTACT_TYPE"] = "Entité";
$MESS["CRM_ENT_SEL_HLP_PREF_PHONE"] = "Numéro de téléphone";
$MESS["CRM_ENT_SEL_HLP_PREF_EMAIL"] = "Courrier électronique";
$MESS["CRM_ENT_SEL_HLP_HIDDEN_CONTACT"] = "Contact masqué";
$MESS["CRM_ENT_SEL_HLP_HIDDEN_COMPANY"] = "Entreprise masquée";
$MESS["CRM_ENT_SEL_HLP_HIDDEN_LEAD"] = "Client potentiel masqué";
$MESS["CRM_ENT_SEL_HLP_HIDDEN_DEAL"] = "Affaire masquée";
$MESS["CRM_ENT_SEL_HLP_HIDDEN_QUOTE"] = "Devis masqué";
?>