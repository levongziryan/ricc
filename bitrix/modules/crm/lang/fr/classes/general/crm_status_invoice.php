<?
$MESS["CRM_INVOICE_STATUS_F"] = "La facture est réglée";
$MESS["CRM_INVOICE_STATUS_P"] = "Achevé(e)s";
$MESS["CRM_INVOICE_STATUS_D"] = "Refusé";
$MESS["CRM_STATUS_TYPE_INVOICE_STATUS"] = "Statuts des rapports";
$MESS["CRM_INVOICE_STATUS_N"] = "Brouillon";
$MESS["CRM_INVOICE_STATUSN_F"] = "Payé";
$MESS["CRM_INVOICE_STATUSN_D"] = "Non payé";
$MESS["CRM_INVOICE_STATUSN_N"] = "Nouveau";
$MESS["CRM_INVOICE_STATUSN_P"] = "Payé";
?>