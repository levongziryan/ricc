<?
$MESS["CRM_VCARD_ERR_VERSION"] = "Impossible de déterminer la version de vCard";
$MESS["CRM_VCARD_ERR_FORMAT"] = "Erreur dans le format du fichier vCard";
$MESS["CRM_VCARD_ERR_TMP_FILE"] = "Erreur de création de fichier temporaire vCard";
$MESS["CRM_VCARD_ERR_READ"] = "Erreur de lecture vCard";
?>