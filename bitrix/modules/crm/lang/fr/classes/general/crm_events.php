<?
$MESS["CRM_EVENT_ERR_ENTITY_TYPE"] = "Identifiant de l'entité inconnu.";
$MESS["CRM_EVENT_ERR_ENTITY_ID"] = "Identificateur d'un élément d'entité n'est pas indiqué.";
$MESS["CRM_EVENT_ERR_ENTITY_NAME"] = "Le type de l'événement n'est pas indiqué.";
?>