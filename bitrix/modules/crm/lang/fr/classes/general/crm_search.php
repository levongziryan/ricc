<?
$MESS["CRM_LEAD"] = "Prospect";
$MESS["CRM_CONTACT"] = "Client";
$MESS["CRM_COMPANY"] = "Entreprise";
$MESS["CRM_DEAL"] = "Affaire";
$MESS["CRM_INVOICE"] = "Facture";
$MESS["CRM_QUOTE"] = "Offre";
$MESS["CRM_EMAILS"] = "Courrier électronique";
$MESS["CRM_PHONES"] = "Téléphones";
?>