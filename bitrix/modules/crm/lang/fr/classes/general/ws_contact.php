<?
$MESS["CRM_FIELD_EMAIL"] = "Courrier électronique";
$MESS["CRM_FIELD_ADDRESS"] = "Adresse";
$MESS["CRM_FIELD_OPPORTUNITY"] = "Montant possible";
$MESS["CRM_FIELD_POST"] = "Emplacement";
$MESS["CRM_FIELD_NAME"] = "Prénom";
$MESS["CRM_FIELD_SOURCE_ID"] = "Chemin";
$MESS["CRM_FIELD_COMMENTS"] = "Commentaire";
$MESS["CRM_FIELD_COMPANY_ID"] = "Entreprise";
$MESS["CRM_FIELD_MESSENGER"] = "Messagerie";
$MESS["CRM_FIELD_FIND"] = "Recherche";
$MESS["CRM_ERROR_FIELD_IS_MISSING"] = "Le champ obligatoire '%FIELD_NAME%' n'a pas été renseigné.";
$MESS["CRM_ERROR_FIELD_INCORRECT"] = "Champ '%FIELD_NAME%'mal renseigné.";
$MESS["CRM_FIELD_STATUS_DESCRIPTION"] = "Description";
$MESS["CRM_FIELD_SOURCE_DESCRIPTION"] = "Description";
$MESS["CRM_FIELD_SECOND_NAME"] = "Deuxième prénom";
$MESS["CRM_FIELD_WEB"] = "Site";
$MESS["CRM_FIELD_STATUS_ID"] = "Statut";
$MESS["CRM_FIELD_PHONE"] = "Numéro de téléphone";
$MESS["CRM_FIELD_TYPE_ID"] = "Type de contact";
$MESS["CRM_FIELD_LAST_NAME"] = "Nom";
$MESS["CRM_FIELD_PHOTO"] = "Photo";
?>