<?
$MESS["CRM_PRODUCT_SECTION_ERR_SECTION_NAME_EMPTY"] = "Nom de la section non renseignée.";
$MESS["CRM_PRODUCT_SECTION_ERR_SECTION_INCLUDES_USED_PRODUCTS"] = "Vous ne pouvez pas supprimer la section, car il contient des produits visés par un accord, un plomb, une offre ou une facture.";
$MESS["CRM_PRODUCT_SECTION_ERR_SECTION_NOT_FOUND"] = "Section n'a pas été trouvé.";
?>