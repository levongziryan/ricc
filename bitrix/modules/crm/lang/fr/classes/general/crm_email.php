<?
$MESS["CRM_ADD_MESSAGE"] = "ajout d'un événement au système CRM";
$MESS["CRM_EMAIL_GET_EMAIL"] = "Envoyer&Sauvagarder la messagerie";
$MESS["CRM_EMAIL_SUBJECT"] = "Titre";
$MESS["CRM_EMAIL_EMAILS"] = "E-mail";
$MESS["CRM_EMAIL_FROM"] = "From";
$MESS["CRM_EMAIL_TO"] = "Dans";
$MESS["CRM_MAIL_COMPANY_NAME"] = "Nom de l'entreprise: %TITLE%";
$MESS["CRM_MAIL_LEAD_FROM_EMAIL_TITLE"] = "Prospect d'une lettre de %SENDER%";
$MESS["CRM_MAIL_LEAD_FROM_EMAIL_SOURCE"] = "créé à partir de lettre de % SENDER% qu'on n'a pas pu attribuer à aucun prospect existant, contact ou entreprise.";
$MESS["CRM_EMAIL_CODE_ALLOCATION_BODY"] = "Inclure au message";
$MESS["CRM_EMAIL_CODE_ALLOCATION_SUBJECT"] = "Placer dans le thème";
$MESS["CRM_EMAIL_CODE_ALLOCATION_NONE"] = "Ne pas utiliser";
$MESS["CRM_EMAIL_BANNENED_ATTACHMENTS"] = "Mon disque bloqués (la taille maximale a été dépassée: %MAX_SIZE% MB)";
$MESS["CRM_EMAIL_BANNENED_ATTACHMENT_INFO"] = "%NAME% (%SIZE% MB)";
$MESS["CRM_MAIL_LEAD_FROM_USER_EMAIL_TITLE"] = "Le prospect de la lettre redirigée de %SENDER%";
$MESS["CRM_EMAIL_DEFAULT_SUBJECT"] = "(sans objet)";
$MESS["CRM_EMAIL_BAD_RESP_QUEUE"] = "Un employé congédié existe dans la file d'attente de distribution des e-mails pour la boîte de réception \"#EMAIL#\". Il ne recevra pas les e-mails d'autres sources, mais tous les e-mails existants seront toujours transférés vers cet employé. <a href=\"#CONFIG_URL#\">Configurer les paramètres de la file d'attente</a>.";
?>