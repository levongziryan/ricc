<?
$MESS["CRM_INVOICE_EVENT_INFO_COMMENTED"] = "#COMMENTS#";
$MESS["CRM_INVOICE_EVENT_NAME_STATUS_CHANGED"] = "Modifier le statut de la facture";
$MESS["CRM_INVOICE_EVENT_INFO_STATUS_CHANGED"] = "Le statut est remplacé par: #STATUS_ID#";
$MESS["CRM_INVOICE_EVENT_NAME_PAYMENT_SYSTEM_CHANGED"] = "Modification de la méthode de paiement";
$MESS["CRM_INVOICE_EVENT_INFO_PAYMENT_SYSTEM_CHANGED"] = "A été modifiée au: #PAY_SYSTEM_ID#";
$MESS["CRM_INVOICE_EVENT_NAME_PAYMENT_VOUCHER_CHANGED"] = "Modification du document de paiement";
$MESS["CRM_INVOICE_EVENT_NAME_PERSON_TYPE_CHANGED"] = "Changement du type du payeur";
$MESS["CRM_INVOICE_EVENT_INFO_PERSON_TYPE_CHANGED"] = "Changé contre: #PERSON_TYPE_ID#";
$MESS["CRM_INVOICE_EVENT_NAME_USER_DESCRIPTION_CHANGED"] = "Modification du commentaire (affiché sur la facture)";
$MESS["CRM_INVOICE_EVENT_INFO_USER_DESCRIPTION_CHANGED"] = "#USER_DESCRIPTION#";
$MESS["CRM_INVOICE_EVENT_NAME_PRICE_CHANGED"] = "Modification de la somme de la facture";
$MESS["CRM_INVOICE_EVENT_INFO_PRICE_CHANGED"] = "Montant du Facture #AMOUNT#";
$MESS["CRM_INVOICE_EVENT_NAME_ADDED"] = "Facture créée";
$MESS["CRM_INVOICE_EVENT_NAME_PRODUCT_ADDED"] = "Ajout d'une marchandise";
$MESS["CRM_INVOICE_EVENT_NAME_PRODUCT_REMOVED"] = "Produit a été supprimé";
$MESS["CRM_INVOICE_EVENT_NAME_PRODUCT_QUANTITY_CHANGED"] = "Changement de la dénomination de la marchandise";
$MESS["CRM_INVOICE_EVENT_NAME_PRODUCT_PRICE_CHANGED"] = "Changement du prix du produit";
$MESS["CRM_PERSON_TYPE_CONTACT"] = "Client";
$MESS["CRM_PERSON_TYPE_COMPANY"] = "Entreprise";
$MESS["CRM_INVOICE_EVENT_NAME_COMMENTED"] = "Rédaction du commentaire du manager";
?>