<?
$MESS["CRM_NOTIFY_SCHEME_ACTIVITY_EMAIL_INCOMING"] = "Nouveau message reçu";
$MESS["CRM_NOTIFY_SCHEME_LIVEFEED_MENTION"] = "Vous avez été mentionné dans un message";
$MESS["CRM_NOTIFY_SCHEME_LIVEFEED_POST"] = "Un message vous est specifiquement adressé";
$MESS["CRM_NOTIFY_SCHEME_WEBFORM"] = "Formulaire du CRM envoyé";
$MESS["CRM_NOTIFY_SCHEME_CALLBACK"] = "Rappel demandé";
?>