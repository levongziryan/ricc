<?
$MESS["CRM_EVENT_ENTITY_ID"] = "ID";
$MESS["CRM_EVENT_ENTITY_DATE_CREATE"] = "Date de l'événement";
$MESS["CRM_EVENT_ENTITY_EVENT_NAME"] = "Titre";
$MESS["CRM_EVENT_RELATIONS_ENTITY_ASSIGNED_BY_ID"] = "Identificateur du responsable";
$MESS["CRM_EVENT_RELATIONS_ENTITY_EVENT_ID"] = "Identificateur de l'événement";
$MESS["CRM_EVENT_ENTITY_CREATED_BY_ID"] = "Identificateur du créateur";
$MESS["CRM_EVENT_ENTITY_ENTITY_ID"] = "ID de l'élément de l'entité";
$MESS["CRM_EVENT_RELATIONS_ENTITY_ENTITY_ID"] = "ID de l'élément de l'entité";
$MESS["CRM_EVENT_RELATIONS_ENTITY_ASSIGNED_BY"] = "Responsable";
$MESS["CRM_EVENT_RELATIONS_ENTITY_EVENT_BY"] = "Evènement";
$MESS["CRM_EVENT_ENTITY_CREATED_BY"] = "Créé par";
$MESS["CRM_EVENT_ENTITY_EVENT_BY"] = "Type d'événement";
$MESS["CRM_EVENT_RELATIONS_ENTITY_ENTITY_TYPE"] = "Type d'entité";
?>