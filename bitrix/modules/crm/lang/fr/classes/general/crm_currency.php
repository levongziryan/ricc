<?
$MESS["CRM_CURRERCY_MODULE_WARNING"] = "Attention! Pour le fonctionnement correct l'installation du module 'Devises' est requise.";
$MESS["CRM_CURRERCY_MODULE_IS_NOT_INSTALLED"] = "Le module ' Devises' est introuvable ! Installez-le s'il vous plaît.";
$MESS["CRM_CURRERCY_ERR_DELETION_OF_BASE_CURRENCY"] = "On ne doit pas Supprimer la devise de base.";
$MESS["CRM_CURRERCY_ERR_DELETION_OF_ACCOUNTING_CURRENCY"] = "Vous ne pouvez pas Supprimer la devise des rapports.";
?>