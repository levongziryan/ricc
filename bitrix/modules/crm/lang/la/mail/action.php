<?
$MESS["CRM_ENTITY_REGEXP"] = "Resolver la Entidad usando La Expresión";
$MESS["CRM_ENTITY_REGEXP_NOTES"] = "(regular la expresión; los primeros paréntesis del grupo deben contener el ID de la entidad)";
$MESS["CRM_MAIL_ENTITY"] = "Entidad:";
$MESS["CRM_MAIL_ENTITY_ALL"] = "Cualquiera";
$MESS["CRM_MAIL_ENTITY_LEAD"] = "Prospecto";
$MESS["CRM_MAIL_ENTITY_CONTACT"] = "Contacto";
$MESS["CRM_MAIL_ENTITY_COMPANY"] = "Compañía";
$MESS["CRM_MAIL_ENTITY_DEAL"] = "Negociación";
$MESS["CRM_MAIL_MAIL"] = "Dirección de e-mail del CRM";
?>