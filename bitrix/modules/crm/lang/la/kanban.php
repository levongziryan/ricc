<?
$MESS["CRM_KANBAN_NOTIFY_TITLE"] = "Permisos insuficientes.";
$MESS["CRM_KANBAN_NOTIFY_HEADER"] = "Permisos insuficientes para editar etapas.";
$MESS["CRM_KANBAN_NOTIFY_TEXT"] = "Solo el administrador de Bitrix24 puede crear una nueva etapa. Por favor, envíe un mensaje o hable con ellos para agregar las etapas de la tarea que necesita.";
$MESS["CRM_KANBAN_NOTIFY_BUTTON"] = "Enviar notificación";
$MESS["CRM_KANBAN_ACTIVITY_MY"] = "Actividades";
$MESS["CRM_KANBAN_ACTIVITY_PLAN"] = "Plan";
$MESS["CRM_KANBAN_ACTIVITY_PLAN_CALL"] = "Llamada";
$MESS["CRM_KANBAN_ACTIVITY_PLAN_MEETING"] = "Reunión";
$MESS["CRM_KANBAN_ACTIVITY_PLAN_VISIT"] = "Visita";
$MESS["CRM_KANBAN_ACTIVITY_PLAN_TASK"] = "Tarea";
$MESS["CRM_KANBAN_ACTIVITY_MORE"] = "Más";
$MESS["CRM_KANBAN_ACTIVITY_LETSGO_LEAD"] = "Planifique actividades para adelantar el prospecto.";
$MESS["CRM_KANBAN_ACTIVITY_LETSGO_DEAL"] = "Planifique actividades para adelantar la negociación.";
$MESS["CRM_KANBAN_NO_EMAIL"] = "No hay e-mail";
$MESS["CRM_KANBAN_NO_PHONE"] = "No hay teléfono";
$MESS["CRM_KANBAN_NO_IM"] = "No hay comunicación de chat";
$MESS["MAIN_KANBAN_COLUMN_TITLE_PLACEHOLDER"] = "Nombre";
$MESS["MAIN_KANBAN_DROPZONE_CANCEL"] = "Cancel";
$MESS["CRM_KANBAN_ACTIVITY_TO_PLAN"] = "Programar";
$MESS["CRM_KANBAN_ACTIVITY_CHANGE_LEAD"] = "Usted no tiene ninguna actividad programada. Mueva el estado del prospecto, <span class=\"crm-kanban-item-activity-link\">planificar una actividad</span> o entablar una espera.";
$MESS["CRM_KANBAN_ACTIVITY_CHANGE_DEAL"] = "Usted no tiene ninguna actividad programada. Mueva el estado de la negociación, <span class=\"crm-kanban-item-activity-link\">planificar una actividad</span>  entablar una espera.";
?>