<?
$MESS["CRM_FF_OK"] = "Seleccione";
$MESS["CRM_FF_CANCEL"] = "Cancelar";
$MESS["CRM_FF_CLOSE"] = "Cerrar";
$MESS["CRM_FF_WAIT"] = "Búsqueda...";
$MESS["CRM_FF_NO_RESULT"] = "Lamentablemente su solicitud de búsqueda no produjo resultados.";
$MESS["CRM_FF_CHOISE"] = "Agregar producto";
$MESS["CRM_FF_CHANGE"] = "Editar";
$MESS["CRM_FF_LAST"] = "Último";
$MESS["CRM_FF_SEARCH"] = "Búsqueda";
$MESS["CRM_FF_LEAD"] = "Prospecto";
$MESS["CRM_FF_CONTACT"] = "Contactos";
$MESS["CRM_FF_COMPANY"] = "Compañías";
$MESS["CRM_FF_DEAL"] = "Negociaciones";
$MESS["CRM_FF_QUOTE"] = "Cotizaciones";
$MESS["CRM_ENT_SEL_HLP_PREF_CONTACT_TYPE"] = "Tipo";
$MESS["CRM_ENT_SEL_HLP_PREF_PHONE"] = "Teléfono";
$MESS["CRM_ENT_SEL_HLP_PREF_EMAIL"] = "Correo Electrónico";
$MESS["CRM_ENT_SEL_HLP_HIDDEN_CONTACT"] = "Contacto oculto";
$MESS["CRM_ENT_SEL_HLP_HIDDEN_COMPANY"] = "Compañía oculta";
$MESS["CRM_ENT_SEL_HLP_HIDDEN_LEAD"] = "Prospecto oculto";
$MESS["CRM_ENT_SEL_HLP_HIDDEN_DEAL"] = "Negociación oculta";
$MESS["CRM_ENT_SEL_HLP_HIDDEN_QUOTE"] = "Cotización oculta";
?>