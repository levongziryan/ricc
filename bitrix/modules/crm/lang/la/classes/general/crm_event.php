<?
$MESS["CRM_EVENT_ERR_ENTITY_TYPE"] = "El ID de la entidad no está especificado.";
$MESS["CRM_EVENT_ERR_ENTITY_ID"] = "El ID del elemento de la entidad no está especificado.";
$MESS["CRM_EVENT_ERR_ENTITY_NAME"] = "El tipo de evento no está espceificado.";
$MESS["CRM_EVENT_ERR_ENTITY_DATE_NOT_VALID"] = "La fecha del evento no es válida.";
$MESS["CRM_EVENT_TYPE_USER"] = "Eventos personalizados";
$MESS["CRM_EVENT_TYPE_CHANGE"] = "Cambios";
$MESS["CRM_EVENT_TYPE_SNS"] = "Correo electrónico entrante";
$MESS["CRM_EVENT_TYPE_VIEW"] = "Ver";
$MESS["CRM_EVENT_TYPE_EXPORT"] = "Exportar";
$MESS["CRM_EVENT_TYPE_DELETE"] = "Eliminar";
?>