<?
$MESS["CRM_EXT_SALE_IMPORT_EMPTY_ANSW"] = "El servidor ha devuelto una respuesta vacía.";
$MESS["CRM_EXT_SALE_IMPORT_UNKNOWN_ANSW"] = "Respuesta del servidor no identificado. Primeros 100 caracteres:";
$MESS["CRM_EXT_SALE_IMPORT_ERROR_XML"] = "Error al analizar la respuesta del servidor.";
$MESS["CRM_GCES_NOTIFY_TITLE"] = "Sincronización con \"#NAME#\"";
$MESS["CRM_GCES_NOTIFY_MESSAGE"] = "Sincronización logrado, recibido:<br />#TOTALDEALS# <a href=\"#DEAL_URL#\">deals</a> (#CREATEDDEALS# new)<br />#TOTALCONTACTS# <a href=\"#CONTACT_URL#\">contacts</a> (#CREATEDCONTACTS# new)<br />#TOTALCOMPANIES# <a href=\"#COMPANY_URL#\">companies</a> (#CREATEDCOMPANIES# new)";
$MESS["CRM_EXT_SALE_TITLE_SETTINGS"] = "Resultado de la importación";
$MESS["CRM_GCES_NOTIFY_ERROR_TITLE"] = "Sincronización con \"#NAME#\"";
$MESS["CRM_GCES_NOTIFY_ERROR_MESSAGE"] = "Error de sincronización: <br />intentos de conexión: 10 últimos: #DATE#<br /> la conexión fue desactivado.<br /><br />Para obtener más información, por favor abra el formulario de conexión de la tienda web(<a href='#URL#'>CRM &gt; Configuraciones &gt; Conectividad de tienda Web</a>).";
$MESS["CRM_EXT_SALE_TITLE_ERROR_SETTINGS"] = "Error al importar";
$MESS["CRM_EXT_SALE_IMPORT_UNKNOWN_ANSW_PERMS"] = "Permisos insuficientes. Por favor, compruebe que el usuario tiene permiso de acceso para exportación.";
$MESS["CRM_EXT_SALE_IMPORT_UNKNOWN_ANSW_F"] = "Error de intercambio de datos en el almacén de Web.";
$MESS["CRM_EXT_SALE_IMPORT_UNKNOWN_ANSW_PERMS1"] = "Permisos insuficientes. Por favor, compruebe que el usuario tiene permiso de acceso para exportación.";
$MESS["CRM_EXT_SALE_IM_GROUP"] = "Grupo";
?>