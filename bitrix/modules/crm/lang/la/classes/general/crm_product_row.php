<?
$MESS["CRM_EVENT_PROD_ROW_ADD"] = "Agregar producto";
$MESS["CRM_EVENT_PROD_ROW_UPD"] = "Modificar producto";
$MESS["CRM_EVENT_PROD_ROW_PRICE_UPD"] = "Precio de '#NAME#' actualizado";
$MESS["CRM_EVENT_PROD_ROW_QTY_UPD"] = "Cantidad de '#NAME#' actualizado";
$MESS["CRM_EVENT_PROD_ROW_REM"] = "Producto eliminado";
$MESS["CRM_EVENT_PROD_ROW_NAME_UPD"] = "Nombre del producto ha cambiado";
$MESS["CRM_EVENT_PROD_ROW_DISCOUNT_UPD"] = "Descuento para \"#NAME#\" ha cambiado";
$MESS["CRM_EVENT_PROD_ROW_TAX_UPD"] = "Impuesto de \"#NAME#\" ha cambiado";
$MESS["CRM_EVENT_PROD_ROW_MEASURE_UPD"] = "Unidad de medida para \"#NAME#\" ha cambiado";
?>