<?
$MESS["CRM_COMPANY_ENTITY_ID"] = "ID";
$MESS["CRM_COMPANY_ENTITY_TITLE"] = "Nombre de la compañía";
$MESS["CRM_COMPANY_ENTITY_PHONE"] = "Teléfono";
$MESS["CRM_COMPANY_ENTITY_EMAIL"] = "Correo electrónico";
$MESS["CRM_COMPANY_ENTITY_WEB"] = "Sitio Web";
$MESS["CRM_COMPANY_ENTITY_MESSENGER"] = "Messenger";
$MESS["CRM_COMPANY_ENTITY_ADDRESS"] = "Dirección";
$MESS["CRM_COMPANY_ENTITY_ADDRESS_LEGAL"] = "Dirección Legal";
$MESS["CRM_COMPANY_ENTITY_BANKING_DETAILS"] = "Detalles de pago";
$MESS["CRM_COMPANY_ENTITY_COMMENTS"] = "Comentario";
$MESS["CRM_COMPANY_ENTITY_COMPANY_TYPE_BY"] = "Tipo de compañía";
$MESS["CRM_COMPANY_ENTITY_INDUSTRY_BY"] = "Industria";
$MESS["CRM_COMPANY_ENTITY_REVENUE_BY"] = "Ingresos anuales";
$MESS["CRM_COMPANY_ENTITY_CURRENCY_BY"] = "Tipo de cambio";
$MESS["CRM_COMPANY_ENTITY_EMPLOYEES_BY"] = "Empleados";
$MESS["CRM_COMPANY_ENTITY_CREATED_BY"] = "Creado por";
$MESS["CRM_COMPANY_ENTITY_CREATED_BY_ID"] = "Creado por";
$MESS["CRM_COMPANY_ENTITY_MODIFY_BY"] = "Modificado por";
$MESS["CRM_COMPANY_ENTITY_MODIFY_BY_ID"] = "Modificado por";
$MESS["CRM_COMPANY_ENTITY_DATE_CREATE"] = "Creado en ";
$MESS["CRM_COMPANY_ENTITY_DATE_MODIFY"] = "Creado en ";
$MESS["CRM_COMPANY_ENTITY_EVENT_RELATION"] = "Eventos";
$MESS["CRM_COMPANY_ENTITY_REVENUE"] = "Ingresos anuales";
$MESS["CRM_COMPANY_ENTITY_CURRENCY_ID"] = "Tipo de cambio";
?>