<?
$MESS["CRM_FIELD_FIND"] = "Búsqueda";
$MESS["CRM_FIELD_REST_NAME"] = "Primer nombre";
$MESS["CRM_FIELD_LAST_NAME"] = "Apellido";
$MESS["CRM_FIELD_SECOND_NAME"] = "Segundo nombre";
$MESS["CRM_FIELD_TITLE"] = "Título";
$MESS["CRM_FIELD_PHONE"] = "Teléfono";
$MESS["CRM_FIELD_EMAIL"] = "Correo electrónico";
$MESS["CRM_FIELD_WEB"] = "Sitio";
$MESS["CRM_FIELD_MESSENGER"] = "Messenger";
$MESS["CRM_FIELD_POST"] = "Ubicación";
$MESS["CRM_FIELD_ADDRESS"] = "Dirección";
$MESS["CRM_FIELD_COMMENTS"] = "Comentario";
$MESS["CRM_FIELD_STATUS_ID"] = "Estado";
$MESS["CRM_FIELD_COMPANY_ID"] = "Compañía";
$MESS["CRM_FIELD_SOURCE_ID"] = "Origen";
$MESS["CRM_FIELD_PRODUCT_ID"] = "Producto";
$MESS["CRM_FIELD_COMPANY_TITLE"] = "Nombre de la compañía";
$MESS["CRM_ERROR_FIELD_IS_MISSING"] = "El campo \"%FIELD_NAME%\" es requerido.";
$MESS["CRM_ERROR_FIELD_INCORRECT"] = "El campo \"%FIELD_NAME%\" es incorrecto.";
$MESS["CRM_FIELD_STATUS_DESCRIPTION"] = "Descripción del estado";
$MESS["CRM_FIELD_OPPORTUNITY"] = "Cantidad posible de negociaciones";
$MESS["CRM_FIELD_SOURCE_DESCRIPTION"] = "Descripción del origen";
$MESS["CRM_FIELD_CURRENCY_ID"] = "Tipo de cambio";
?>