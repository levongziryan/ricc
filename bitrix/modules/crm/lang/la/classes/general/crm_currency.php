<?
$MESS["CRM_CURRERCY_MODULE_IS_NOT_INSTALLED"] = "El módulo Currency Adjustment no está instalado.";
$MESS["CRM_CURRERCY_ERR_DELETION_OF_BASE_CURRENCY"] = "No se puede eliminar el tipo de cambio base.";
$MESS["CRM_CURRERCY_ERR_DELETION_OF_ACCOUNTING_CURRENCY"] = "En reporte de tipo de cambio no se puede eliminar.";
$MESS["CRM_CURRERCY_MODULE_WARNING"] = "Atención! El módulo \"Tipo de Cambio\" es necesario para su correcto funcionamiento.";
?>