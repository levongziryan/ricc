<?
$MESS["CRM_PRODUCT_SECTION_ERR_SECTION_NAME_EMPTY"] = "No se especifica nombre de la sección.";
$MESS["CRM_PRODUCT_SECTION_ERR_SECTION_NOT_FOUND"] = "No se encontró sección.";
$MESS["CRM_PRODUCT_SECTION_ERR_SECTION_INCLUDES_USED_PRODUCTS"] = "No se puede eliminar la sección, ya que contiene los productos mencionados por una negociación, un prospecto, una oferta o una factura.";
?>