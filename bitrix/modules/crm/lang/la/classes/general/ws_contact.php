<?
$MESS["CRM_ERROR_FIELD_IS_MISSING"] = "El campo \"%FIELD_NAME%\" es obligatorio.";
$MESS["CRM_ERROR_FIELD_INCORRECT"] = "El campo \"%FIELD_NAME%\" es incorrecto.";
$MESS["CRM_FIELD_FIND"] = "Buscar";
$MESS["CRM_FIELD_NAME"] = "Nombre";
$MESS["CRM_FIELD_LAST_NAME"] = "Apellido";
$MESS["CRM_FIELD_SECOND_NAME"] = "Segundo Nombre";
$MESS["CRM_FIELD_TYPE_ID"] = "Tipo de Contacto";
$MESS["CRM_FIELD_PHONE"] = "Teléfono";
$MESS["CRM_FIELD_EMAIL"] = "E-mail";
$MESS["CRM_FIELD_WEB"] = "Sitio";
$MESS["CRM_FIELD_MESSENGER"] = "Messenger";
$MESS["CRM_FIELD_POST"] = "Cargo";
$MESS["CRM_FIELD_ADDRESS"] = "Dirección";
$MESS["CRM_FIELD_COMMENTS"] = "Comentario";
$MESS["CRM_FIELD_STATUS_ID"] = "Estado";
$MESS["CRM_FIELD_STATUS_DESCRIPTION"] = "Descripción";
$MESS["CRM_FIELD_OPPORTUNITY"] = "Oportunidad";
$MESS["CRM_FIELD_COMPANY_ID"] = "Compañía";
$MESS["CRM_FIELD_SOURCE_ID"] = "Origen";
$MESS["CRM_FIELD_SOURCE_DESCRIPTION"] = "Descripción";
$MESS["CRM_FIELD_PHOTO"] = "Foto";
?>