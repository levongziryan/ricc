<?
$MESS["CRM_PS_USER_ID"] = "ID del usuario";
$MESS["CRM_PS_USER_LOGIN"] = "Login";
$MESS["CRM_PS_USER_NAME"] = "Primer nombre";
$MESS["CRM_PS_USER_SECOND_NAME"] = "Segundo nombre";
$MESS["CRM_PS_USER_LAST_NAME"] = "Apellido";
$MESS["CRM_PS_USER_SITE"] = "Registrado en";
$MESS["CRM_PS_USER_PROF"] = "Cargo";
$MESS["CRM_PS_USER_WEB"] = "Sitio web personal";
$MESS["CRM_PS_USER_ICQ"] = "ICQ";
$MESS["CRM_PS_USER_SEX"] = "Sexo";
$MESS["CRM_PS_USER_FAX"] = "Fax";
$MESS["CRM_PS_USER_PHONE"] = "Teléfono";
$MESS["CRM_PS_USER_ADDRESS"] = "Dirección";
$MESS["CRM_PS_USER_POST"] = "P.O. Box";
$MESS["CRM_PS_USER_CITY"] = "Ciudad";
$MESS["CRM_PS_USER_STATE"] = "Estado";
$MESS["CRM_PS_USER_ZIP"] = "Código postal";
$MESS["CRM_PS_USER_COUNTRY"] = "País";
$MESS["CRM_PS_USER_COMPANY"] = "Compañía";
$MESS["CRM_PS_USER_DEPT"] = "Departamento";
$MESS["CRM_PS_USER_DOL"] = "Posición";
$MESS["CRM_PS_USER_COM_WEB"] = "Sitio Web de la compañía";
$MESS["CRM_PS_USER_COM_PHONE"] = "Teléfono del trabajo";
$MESS["CRM_PS_USER_COM_FAX"] = "Fax del trabajo";
$MESS["CRM_PS_USER_COM_ADDRESS"] = "Dirección de la compañía";
$MESS["CRM_PS_USER_COM_POST"] = "P.O. Box compañía ";
$MESS["CRM_PS_USER_COM_CITY"] = "Ciudad de la compañía";
$MESS["CRM_PS_USER_COM_STATE"] = "Estado de la compañía";
$MESS["CRM_PS_USER_COM_ZIP"] = "Código postal de la compañia";
$MESS["CRM_PS_USER_COM_COUNTRY"] = "País de la compañía";
$MESS["CRM_PS_ORDER_ID"] = "ID del pedido";
$MESS["CRM_PS_ORDER_DATETIME"] = "Fecha del pedido";
$MESS["CRM_PS_ORDER_DATE"] = "Fecha del pedido (sin tiempo)";
$MESS["CRM_PS_ORDER_DATE_PAY_BEFORE"] = "Pago adelantado";
$MESS["CRM_PS_ORDER_PRICE"] = "Importe del pedido";
$MESS["CRM_PS_ORDER_CURRENCY"] = "Moneda";
$MESS["CRM_PS_ORDER_SUM"] = "Total del pedido";
$MESS["CRM_PS_ORDER_SITE"] = "Sitio Web";
$MESS["CRM_PS_ORDER_PRICE_DELIV"] = "Costo de envió";
$MESS["CRM_PS_ORDER_DESCOUNT"] = "Descuento";
$MESS["CRM_PS_ORDER_USER_ID"] = "ID del cliente";
$MESS["CRM_PS_ORDER_PS"] = "ID del sistema de pago";
$MESS["CRM_PS_ORDER_DELIV"] = "ID del servicio de envió";
$MESS["CRM_PS_ORDER_TAX"] = "Importe del impuesto";
$MESS["CRM_PS_ORDER_USER_DESCRIPTION"] = "Comentario";
$MESS["CRM_ANY"] = "Todo";
$MESS["CRM_COMPANY_PT"] = "Compañía";
$MESS["CRM_CONTACT_PT"] = "Contacto";
$MESS["CRM_PS_ORDER_DATE_BILL_DATE"] = "Fecha de la factura";
$MESS["CRM_PS_ORDER_DATE_BILL"] = "Fecha y hora de la factura";
$MESS["CRM_PS_SELECT_NONE"] = "(no seleccionado)";
$MESS["CRM_PS_ORDER_ACCOUNT_NUMBER"] = "Orden #";
$MESS["CRM_PS_PAYMENT_ID"] = "ID de pago";
$MESS["CRM_PS_PAYMENT_ACCOUNT_NUMBER"] = "Pago #";
$MESS["CRM_PS_PAYMENT_DATE_BILL"] = "Factura creada el (fecha y hora)";
$MESS["CRM_PS_PAYMENT_DATE"] = "Factura creada el (única fecha)";
$MESS["CRM_PS_PAYMENT_PRICE"] = "Total de la orden";
$MESS["CRM_PS_PAYMENT_CURRENCY"] = "Moneda";
$MESS["CRM_PS_TYPES_PAYMENT"] = "Pago";
$MESS["CRM_PS_USER_FIELDS"] = "Campos personalizados";
?>