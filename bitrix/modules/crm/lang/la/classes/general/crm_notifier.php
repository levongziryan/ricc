<?
$MESS["CRM_NOTIFY_SCHEME_ACTIVITY_EMAIL_INCOMING"] = "Hay nuevos mensajes de correo electrónico";
$MESS["CRM_NOTIFY_SCHEME_LIVEFEED_MENTION"] = "Usted fue mencionado en el post";
$MESS["CRM_NOTIFY_SCHEME_LIVEFEED_POST"] = "Usted tiene que especificar el destinatario del post";
$MESS["CRM_NOTIFY_SCHEME_WEBFORM"] = "Formulario del CRM enviado";
$MESS["CRM_NOTIFY_SCHEME_CALLBACK"] = "Se ha solicitado devolución de llamada";
?>