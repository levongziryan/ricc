<?
$MESS["CRM_STATUS_A"] = "Confirmada";
$MESS["CRM_STATUS_A_DESCR"] = "Confirmada";
$MESS["CRM_STATUS_D"] = "Rechazada";
$MESS["CRM_STATUS_D_DESCR"] = "Rechazada";
$MESS["CRM_STATUS_P"] = "Completada";
$MESS["CRM_STATUS_P_DESCR"] = "Completada";
$MESS["CRM_STATUS_S"] = "Enviada a un cliente";
$MESS["CRM_STATUS_S_DESCR"] = "Enviada a un cliente";
$MESS["CRM_STATUS_N"] = "Borrador";
$MESS["CRM_STATUS_N_DESCR"] = "Borrador";
$MESS["CRM_STATUSN_D"] = "No pagado";
$MESS["CRM_STATUSN_D_DESCR"] = "No pagado";
$MESS["CRM_STATUSN_P"] = "Pagado";
$MESS["CRM_STATUSN_P_DESCR"] = "Pagado";
$MESS["CRM_STATUSN_S"] = "Enviado al cliente";
$MESS["CRM_STATUSN_S_DESCR"] = "Enviado al cliente";
$MESS["CRM_STATUSN_N"] = "Nuevo";
$MESS["CRM_STATUSN_N_DESCR"] = "Nuevo";
?>