<?
$MESS["CRM_CONTACT"] = "Contactos";
$MESS["CRM_COMPANY"] = "Compañías";
$MESS["CRM_DEAL"] = "Negociaciones";
$MESS["CRM_LEAD"] = "Prospectos";
$MESS["CRM_EVENT"] = "Eventos";
$MESS["CRM_CONFIGS"] = "Configuraciones";
$MESS["CRM_HELP"] = "Ayuda";
$MESS["CRM_REPORTS"] = "Reportes";
$MESS["CRM_PRODUCTS"] = "Productos";
?>