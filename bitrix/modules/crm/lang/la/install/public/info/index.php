<?
$MESS["CRM_PAGE_TITLE"] = "Ayuda";
$MESS["CRM_PAGE_CONTENT"] = "<h3>CRM En pocas palabras</h3>
 
<p><strong>Customer Relationship Management (CRM)</strong> es una estrategia de gesti&oacute;n de la interacci&oacute;n de una de una compañía con sus clientes y sus respectivas perspectivas de ventas. Se trata de utilizar la tecnolog&iacute;a para organizar, automatizar, y sincronizar los procesos de negocio, principalmente las actividades de ventas, marketing y servicio al cliente. CRM guarda informaci&oacute;n relacionada con el cliente para su posterior uso y an&aacute;lisis.</p>
<p>Bitrix CRM se basa en los siguientes conceptos.</p>
<ul>
  <li><strong>Contacto</strong> es un registro que contiene los datos de una persona con la que su compañía tendr&aacute; o ya tiene una relaci&oacute;n.</li>
  <li> <strong>Empresa</strong> es un registro que contiene informaci&oacute;n acerca de una organizaci&oacute;n con la que su compañía tiene o puede tener alguna relaci&oacute;n.</li>
  <li><strong> Lead (Prospecto)</strong> es un registro que contiene los datos de contacto (correo electr&oacute;nico, tel&eacute;fono) de compañías que pueden ser interesantes para nuestro negocio, pero con las que aun no interactuamos.</li>
  <li> <strong>Evento</strong> se refiere a cualquier suceso o al cambio del mismo,  referente a un contacto, clientes potenciales o compañías. Por ejemplo: a&ntilde;adir un nuevo n&uacute;mero de tel&eacute;fono.</li>
  <li><strong>Contrato</strong> es el registro de una transacci&oacute;n financiera con un cliente o compañía.</li>
</ul>
<h3>&iquest;C&oacute;mo funciona?</h3>
<p> El CRM se puede utilizar dependiendo de las necesidades de su negocio:</p>
 
<ol>
  <li>Como base de datos decontactos de una compañía;</li>
  <li>Cl&aacute;sico  sistema CRM.</li>
 </ol>
<h4>1.Uso de la CRM como una base de datos</h4>
<p> El CRM puede actuar como base de datos para el seguimiento de relaciones comerciales. En este caso, las entidades principales son los contactos, compañías, y los acontecimientos que se refieren a ellos, combin&aacute;ndolos para formar una descripci&oacute;n completa del historial relacionado a dicha compañía. En este modelo inclusive es posible crear eventos para clientes potenciales y aplicarlos de modo que se puedan convertir en negociaciones. </p>
<p><img height='432' border='0' width='900' src='/upload/crm/cim/01-en.png'  /></p>
 
<h4>2.Cl&aacute;sico sistema CRM</h4>
<p> Si desea utilizar el CRM como un verdadero sistema de gesti&oacute;n de relaciones con los clientes, el punto de partida ser&iacute;a crear un prospecto, el mismo que podr&iacute;a ser a&ntilde;adido manualmente o puede ser importado desde Bitrix Site Manager. Una vez creado el &quot;prospecto&quot;, este puede ser procesado y convertirse finalmente en un contacto, una compañía o quedarse como &quot;prospecto&quot;. Si un prospecto es convertido en compa&ntilde;&iacute;a o contacto, es adicionado como registro de la base de datos de contactos o compa&ntilde;&iacute;as, seg&uacute;n sea el caso. Si los &quot;prospectos&quot; se convierten en una negociación como consecuencia ser&aacute;n tratados por el canal de ventas indicado. </p>
 
<p><img height='454' border='0' width='900' src='/upload/crm/cim/03-en.png'  /></p>";
?>