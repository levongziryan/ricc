<?
$MESS["CRM_ACTIVITY_VISIT_TITLE"] = "Visitar";
$MESS["CRM_ACTIVITY_VISIT_BUTTON_FINISH"] = "Cerrar";
$MESS["CRM_ACTIVITY_VISIT_OWNER_SELECT"] = "Seleccionar";
$MESS["CRM_ACTIVITY_VISIT_OWNER_CHANGE"] = "Cambiar";
$MESS["CRM_ACTIVITY_VISIT_OWNER_TAKE_PICTURE"] = "Tomar una foto";
$MESS["CRM_ACTIVITY_VISIT_OWNER_RETAKE_PICTURE"] = "Hazlo otra vez";
$MESS["CRM_ACTIVITY_VISIT_ERROR_MIC_FAILURE"] = "No se puede acceder al micrófono. Error de descripción:";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_TITLE"] = "Términos de Uso";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_BODY_1"] = "¡IMPORTANTE! Esto registrará la conversación con su visitante.";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_BODY_2"] = "Asegúrese de seguir todas las leyes que regulan la grabación de conversaciones en su país.";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_AGREED"] = "Acepto los términos";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_CLOSE"] = "Cerrar";
$MESS["CRM_ACTIVITY_VISIT_DEFAULT_CAMERA"] = "Cámara predeterminada";
$MESS["CRM_ACTIVITY_VISIT_FACEID_SOCIAL_SEACH_TITLE"] = "Buscar en el perfil de VK";
$MESS["CRM_ACTIVITY_VISIT_FACEID_SOCIAL_SEACH_IN_PROCESS"] = "Buscando datos...";
$MESS["CRM_ACTIVITY_VISIT_FACEID_AGREEMENT"] = "<div class=\"tracker-agreement-popup-content\">
   <ol class=\"tracker-agreement-popup-list\">
    <li class=\"tracker-agreement-popup-list-item\">
 El Servicio FindFace (en lo sucesivo denominado \"Servicio FindFace\") es proporcionado por N-TECH.LAB LTD. 
 Los usuarios del Servicio FindFace están obligados a firmar un acuerdo con el Servicio FindFace y deben <a href=\"https://saas.findface.pro/files/docs/license_us.pdf\" class=\"tracker-link\">Siguientes términos y condiciones</a> Seleccionando \"Acepto\". 
 <br>
 Al seleccionar \"Acepto\", el Usuario acepta y garantiza que cumplirá los siguientes requisitos:
     <ul class=\"tracker-agreement-popup-inner-list\">
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Seguir estrictamente todas las leyes y regulaciones locales que rigen la privacidad y el uso de datos personales para el Usuario&#039;s país de residencia.
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Seguir estrictamente todas las leyes y reglamentos locales. Este requisito permanecerá vigente durante toda la duración de la utilización del Servicio FindFace. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Para obtener autorización por escrito, o según lo prescrito por las leyes locales de cada país en el que el Usuario esté operando. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       El Usuario está obligado a informar diligentemente a cada persona cuyos datos personales, incluidas las fotografías e imágenes del individuo que el Usuario intente procesar mediante la utilización del Servicio FindFace.
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Buscar asesoría legal ANTES de utilizar el Servicio FindFace. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
    Garantizar que cualquier información cargada por el Usuario ha sido autorizada por el individuo y está en conformidad con las leyes y reglamentos del Usuario&#039;s jurisdicción.
   </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
    Para confirmar que el Usuario está utilizando el Servicio de FindFace a su propio riesgo y sin garantía de ningún tipo. 
   </li>   
     </ul>
    </li>
    <li class=\"tracker-agreement-popup-list-item\">
     Bitrix, Inc. no es responsable del Servicio y no hace reclamaciones o garantías con respecto a la exactitud del Servicio o la disponibilidad, ni proporciona ningún soporte técnico o consultas sobre el Servicio. 
    </li>
   </ol>
   <div class=\"tracker-agreement-popup-description\">
    Bitrix, Inc. no es responsable del Servicio FindFace y no hace reclamaciones ni garantías para el Servicio FindFace.
  Al aceptar los términos y condiciones de este contrato de servicio, el Usuario acepta y confirma que el Servicio FindFace, ni Bitrix, Inc. recopila o procesa los datos cargados por el Usuario.
  El usuario confirma que el Servicio FindFace, ni Bitrix, Inc. tiene conocimiento de las circunstancias bajo las cuales el Usuario ha recopilado los datos, fotografías o imágenes.
  El usuario se compromete a mantener el servicio FindFace y Bitrix inofensivos por todas y cada una de las acciones e inacciones del usuario.
 <br><br>
 Resolución de conflictos; Arbitraje vinculante: Cualquier controversia, interpretación o reclamación que incluya reclamos por, pero no limitados al incumplimiento de contrato, cualquier forma de negligencia, fraude o tergiversación que surja de o se relacione con este acuerdo será sometido a arbitraje final y vinculante . 
   </div>
  </div>";
?>