<?
$MESS["CRM_INSTALL_NAME"] = "CRM";
$MESS["CRM_TOP_LINKS_ITEM_NAME"] = "CRM";
$MESS["CRM_INSTALL_DESCRIPTION"] = "Brinda soporte CRM.";
$MESS["CRM_INSTALL_TITLE"] = "Instalación del Módulo CRM";
$MESS["CRM_UNINSTALL_TITLE"] = "Desinstalar módulo CRM";
$MESS["CRM_PAGE_FUNNEL"] = "Embudo de ventas";
$MESS["CRM_GADGET_MY_LEAD_TITLE"] = "Mis dirigentes";
$MESS["CRM_GADGET_NEW_LEAD_TITLE"] = "Nuevos dirigentes";
$MESS["CRM_GADGET_CLOSED_DEAL_TITLE"] = "Negociaciones Cerradas";
$MESS["CRM_GADGET_LAST_EVENT_TITLE"] = "Eventos Recientes";
$MESS["CRM_GADGET_NEW_CONTACT_TITLE"] = "Nuevos Contactos";
$MESS["CRM_GADGET_NEW_COMPANY_TITLE"] = "Nuevas compañías";
$MESS["CRM_PERM_R"] = "Vista de páginas";
$MESS["CRM_PERM_W"] = "Crear y editar páginas";
$MESS["CRM_PERM_D"] = "Acceso denegado";
$MESS["CRM_PERM_X"] = "Crear y editar páginas en el editor visual";
$MESS["CRM_PERM_Y"] = "Borrar páginas y su historial";
$MESS["CRM_PERM_Z"] = "Fijar Permiso";
$MESS["CRM_CONTACT_TYPE_SHARE"] = "Contactos generales";
$MESS["CRM_CONTACT_TYPE_JOURNALIST"] = "Periodistas";
$MESS["CRM_CONTACT_TYPE_CLIENT"] = "Clientes";
$MESS["CRM_CONTACT_TYPE_SUPPLIER"] = "Proveedores";
$MESS["CRM_CONTACT_TYPE_PARTNER"] = "Socios";
$MESS["CRM_STATUS_TYPE_SOURCE_SELF"] = "Contacto Personal";
$MESS["CRM_STATUS_TYPE_SOURCE_PARTNER"] = "Cliente Existente";
$MESS["CRM_STATUS_TYPE_SOURCE_CALL"] = "Llamar";
$MESS["CRM_STATUS_TYPE_SOURCE_WEB"] = "Sitio Web";
$MESS["CRM_STATUS_TYPE_SOURCE_WEB_FORM"] = "Formulario Web";
$MESS["CRM_STATUS_TYPE_SOURCE_EMAIL"] = "E-Mail";
$MESS["CRM_STATUS_TYPE_SOURCE_CONFERENCE"] = "Conferencia";
$MESS["CRM_STATUS_TYPE_SOURCE_TRADE_SHOW"] = "Mostrar/Exhibición";
$MESS["CRM_STATUS_TYPE_SOURCE_EMPLOYEE"] = "Empleado";
$MESS["CRM_STATUS_TYPE_SOURCE_COMPANY"] = "Campaña";
$MESS["CRM_STATUS_TYPE_SOURCE_HR"] = "Departamento de Recursos Humanos";
$MESS["CRM_STATUS_TYPE_SOURCE_MAIL"] = "Mensaje de E-Mail ";
$MESS["CRM_STATUS_TYPE_SOURCE_OTHER"] = "Otro";
$MESS["CRM_COMPANY_TYPE_CUSTOMER"] = "Cliente";
$MESS["CRM_COMPANY_TYPE_PARTNER"] = "Socio";
$MESS["CRM_COMPANY_TYPE_RESELLER"] = "Distribuidor";
$MESS["CRM_COMPANY_TYPE_COMPETITOR"] = "Competencia";
$MESS["CRM_COMPANY_TYPE_INVESTOR"] = "Inversionista";
$MESS["CRM_COMPANY_TYPE_INTEGRATOR"] = "Integrante";
$MESS["CRM_COMPANY_TYPE_PROSPECT"] = "Prospecto";
$MESS["CRM_COMPANY_TYPE_PRESS"] = "Prensa";
$MESS["CRM_COMPANY_TYPE_OTHER"] = "Otro";
$MESS["CRM_INDUSTRY_IT"] = "Tecnología de la Información";
$MESS["CRM_INDUSTRY_TELECOM"] = "Telecomunicación";
$MESS["CRM_INDUSTRY_MANUFACTURING"] = "Manufacturación";
$MESS["CRM_INDUSTRY_BANKING"] = "Servicios Bancarios";
$MESS["CRM_INDUSTRY_CONSULTING"] = "Consultoría";
$MESS["CRM_INDUSTRY_FINANCE"] = "Finanzas";
$MESS["CRM_INDUSTRY_GOVERNMENT"] = "Régimen";
$MESS["CRM_INDUSTRY_DELIVERY"] = "Envío";
$MESS["CRM_INDUSTRY_ENTERTAINMENT"] = "Entretenimiento";
$MESS["CRM_INDUSTRY_NOTPROFIT"] = "Sin lucro";
$MESS["CRM_INDUSTRY_OTHER"] = "Otro";
$MESS["CRM_DEAL_TYPE_SALE"] = "Ventas";
$MESS["CRM_DEAL_TYPE_COMPLEX"] = "Ventas Integradas";
$MESS["CRM_DEAL_TYPE_GOODS"] = "Venta de Mercadería";
$MESS["CRM_DEAL_TYPE_SERVICES"] = "Servicios";
$MESS["CRM_DEAL_TYPE_SERVICE"] = "Servicio Post Venta";
$MESS["CRM_DEAL_STATE_PLANNED"] = "Planeado";
$MESS["CRM_DEAL_STATE_PROCESS"] = "En progreso";
$MESS["CRM_DEAL_STATE_COMPLETE"] = "Completado";
$MESS["CRM_DEAL_STATE_CANCELED"] = "Cancelado";
$MESS["CRM_EVENT_TYPE_INFO"] = "Información";
$MESS["CRM_EVENT_TYPE_MESSAGE"] = "Enviar Mail";
$MESS["CRM_EVENT_TYPE_PHONE"] = "Llamada telefónica";
$MESS["CRM_UF_NAME"] = "Elementos del CRM";
$MESS["CRM_UF_NAME_CAL"] = "Elementos CRM";
$MESS["CRM_UF_NAME_LF_TYPE"] = "Código de entidad en el CRM del flujo de actividad";
$MESS["CRM_UF_NAME_LF_ID"] = "ID de entidad del flujo de actividad del CRM";
$MESS["CRM_ROLE_ADMIN"] = "Acceso completo";
$MESS["CRM_ROLE_DIRECTOR"] = "Director";
$MESS["CRM_ROLE_CHIF"] = "Jefe del departamento";
$MESS["CRM_ROLE_MAN"] = "Administrador";
$MESS["CRM_SALE_STATUS_A"] = "Confirmado";
$MESS["CRM_SALE_STATUS_D"] = "Rechazado";
$MESS["CRM_SALE_STATUS_P"] = "Completado";
$MESS["CRM_SALE_STATUS_S"] = "Enviado al cliente";
$MESS["CRM_VAT_1"] = "Impuestos no incluidos";
$MESS["CRM_VAT_2"] = "Tasa de impuesto: 18%";
$MESS["CRM_ORD_PROP_2"] = "Ubicación";
$MESS["CRM_ORD_PROP_21"] = "Ciudad";
$MESS["CRM_ORD_PROP_4"] = "Código postal";
$MESS["CRM_ORD_PROP_5"] = "Dirección de envio";
$MESS["CRM_ORD_PROP_6"] = "Nombre completo";
$MESS["CRM_ORD_PROP_7"] = "Domicilio legal";
$MESS["CRM_ORD_PROP_8"] = "Nombre de la compañía";
$MESS["CRM_ORD_PROP_9"] = "Teléfono";
$MESS["CRM_ORD_PROP_10"] = "Persona de contacto";
$MESS["CRM_ORD_PROP_11"] = "Fax";
$MESS["CRM_ORD_PROP_12"] = "Dirección de envio";
$MESS["CRM_ORD_PROP_13"] = "ID del contribuyente";
$MESS["CRM_ORD_PROP_14"] = "Autor";
$MESS["CRM_ORD_PROP_GROUP_FIZ1"] = "Información personal";
$MESS["CRM_ORD_PROP_GROUP_FIZ2"] = "Información de envio";
$MESS["CRM_ORD_PROP_GROUP_UR1"] = "Información de la compañía";
$MESS["CRM_ORD_PROP_GROUP_UR2"] = "Información del contacto";
$MESS["CRM_EMAIL_CONFIRM_TYPE_NAME"] = "Confirmar la dirección de correo electrónico del remitente";
$MESS["CRM_EMAIL_CONFIRM_TYPE_DESC"] = "#EMAIL# - Dirección de correo electrónico por confirmar
#CONFIRM_CODE# - Código de confirmación";
$MESS["CRM_EMAIL_CONFIRM_EVENT_NAME"] = "Confirmar el correo";
$MESS["CRM_EMAIL_CONFIRM_EVENT_DESC"] = "<span style=\"font-size:16px;line-height:20px;\">
Ingrese este código de confirmación en su Bitrix24 para confirmar su dirección de correo electrónico.<br>

<span style=\"font-size:24px;line-height:70px;\"><b>#CONFIRM_CODE#</b></span><br>

<span style=\"color:#808080;\">
¿Por qué necesito verificar mi dirección de correo electrónico?<br><br>
<span style=\"font-size:14px;\">Le solicitamos que confirme su dirección de correo electrónico para evitar la suplantación de identidad y que se asegure de tener la dirección a la que se envían los correos electrónicos.</span>
</span>";
?>