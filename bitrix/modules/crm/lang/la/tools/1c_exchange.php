<?
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "El módulo Information Blocks no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CATALOG_MODULE_NOT_INSTALLED"] = "El módulo Commercial Catalog no está instalado.";
$MESS["SALE_MODULE_NOT_INSTALLED"] = "El módulo e-Store no está instalado.";
$MESS["CRM_EXCH1C_UNKNOWN_XML_ID"] = "El ID del catálogo no es válido.";
$MESS["CRM_EXCH1C_AUTH_ERROR"] = "Error de autenticación: nombre de usuario y/o contraseña son incorrectos.";
$MESS["CRM_EXCH1C_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_EXCH1C_UNKNOWN_COMMAND_TYPE"] = "Comando desconocido.";
$MESS["CRM_EXCH1C_NOT_ENABLED"] = "Intercambio de datos con 1C desactivado";
?>