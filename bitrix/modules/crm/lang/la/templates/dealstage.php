<?
$MESS["CRM_BP_DEAL_TITLE"] = "Etapa de Ejecución de transacciones";
$MESS["CRM_BP_DEAL_PROPERTIES_1"] = "Procesos de negocios impulsados ??por Estado";
$MESS["CRM_BP_DEAL_PROPERTIES_1_TITLE"] = "Estado: Nuevo (análisis)";
$MESS["CRM_BP_DEAL_PROPERTIES_2"] = "Descargar Evento ";
$MESS["CRM_BP_DEAL_PROPERTIES_2_TITLE"] = "Crear una oferta";
$MESS["CRM_BP_DEAL_PROPERTIES_3_TITLE"] = "Establecer el estado";
$MESS["CRM_BP_DEAL_PROPERTIES_4_TITLE"] = "Ingresar estado";
$MESS["CRM_BP_DEAL_PROPERTIES_5_TITLE"] = "Editar documento";
$MESS["CRM_BP_DEAL_PROPERTIES_6_TITLE"] = "Procesamiento";
$MESS["CRM_BP_DEAL_PROPERTIES_7_TITLE"] = "Estado: nueva oferta";
$MESS["CRM_BP_DEAL_PROPERTIES_8_TITLE"] = "Oferta";
$MESS["CRM_BP_DEAL_PROPERTIES_10_TITLE"] = "Error";
$MESS["CRM_BP_DEAL_PROPERTIES_13_TITLE"] = "Estado: error";
$MESS["CRM_BP_DEAL_PROPERTIES_14_TITLE"] = "Oferta";
$MESS["CRM_BP_DEAL_PROPERTIES_16_TITLE"] = "Exitoso";
$MESS["CRM_BP_DEAL_PROPERTIES_17_TITLE"] = "Estado: exitoso";
$MESS["CRM_BP_DEAL_PROPERTIES_18_TITLE"] = "Código PHP";
$MESS["CRM_BP_DEAL_PROPERTIES_19_TITLE"] = "Preventa";
$MESS["CRM_BP_DEAL_PROPERTIES_20_TITLE"] = "Hora de inicio de la tarea";
$MESS["CRM_BP_DEAL_TASK_NAME"] = "Preventa de la negociación: {=Document:TITLE}  (Conjunto de procesos de negocio)";
$MESS["CRM_BP_DEAL_TASK_TEXT"] = "Realizar preventa de la negociación: {=Document:TITLE}";
$MESS["CRM_BP_DEAL_PROPERTIES_11_TITLE"] = "Factura";
$MESS["CRM_BP_DEAL_PROPERTIES_15_TITLE"] = "Estado: Factura";
?>