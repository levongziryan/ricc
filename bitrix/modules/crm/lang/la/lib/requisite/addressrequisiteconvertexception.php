<?
$MESS["CRM_ADDR_CONV_EX_COMPANY_ACCESS_DENIED"] = "Acceso denegado a la compañía";
$MESS["CRM_ADDR_CONV_EX_CONTACT_ACCESS_DENIED"] = "Acceso denegado al contacto";
$MESS["CRM_ADDR_CONV_EX_COMPANY_CREATION_FAILED"] = "No se puede crear un elemento de detalles de la compañía.";
$MESS["CRM_ADDR_CONV_EX_CONTACT_CREATION_FAILED"] = "No se puede crear un elemento de detalles del contacto";
?>