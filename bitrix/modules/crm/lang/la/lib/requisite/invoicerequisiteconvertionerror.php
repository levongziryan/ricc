<?
$MESS["CRM_INV_RQ_CONV_ERROR_GENERAL"] = "Error general";
$MESS["CRM_INV_RQ_CONV_ERROR_PRESET_NOT_BOUND"] = "La plantilla que eligió no tiene campos para poder transferir los detalles antiguos de la compañía.";
$MESS["CRM_INV_RQ_CONV_ERROR_PERSON_TYPE_NOT_FOUND"] = "Tipo de pagador no pudo ser localizado.";
$MESS["CRM_INV_RQ_CONV_ERROR_PROPERTY_NOT_FOUND"] = "Se eliminaron los detalles antiguos de la compañía para facturas. La transferencia no se pudo completar.";
?>