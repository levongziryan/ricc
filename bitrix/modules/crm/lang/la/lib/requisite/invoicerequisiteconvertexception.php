<?
$MESS["CRM_INV_RQ_CONV_ERROR_GENERAL"] = "Error general.";
$MESS["CRM_INV_RQ_CONV_ERROR_PRESET_NOT_BOUND"] = "La plantilla seleccionada no tiene campos que pueden completarse con datos actuales.";
$MESS["CRM_INV_RQ_CONV_ERROR_PERSON_TYPE_NOT_FOUND"] = "No se puede encontrar tipo de pagador.";
$MESS["CRM_INV_RQ_CONV_ERROR_PROPERTY_NOT_FOUND"] = "No se pueden transferir datos porque se eliminaron los detalles de la factura existentes.";
?>