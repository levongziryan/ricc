<?
$MESS["CRM_COMMENT_IM_MENTION_POST"] = "Te mencionó en un comentario para \"#ENTITY_TITLE#\", comentario de texto: \"#COMMENT#\"";
$MESS["CRM_COMMENT_IM_MENTION_POST_M"] = "Te mencionó en un comentario para \"#ENTITY_TITLE#\", comentario de texto: \"#COMMENT#\"";
$MESS["CRM_COMMENT_IM_MENTION_POST_F"] = "Te mencionó en un comentario para \"#ENTITY_TITLE#\", comentario de texto: \"#COMMENT#\"";
$MESS["CRM_ENTITY_TITLE_DEAL"] = "negociación #ENTITY_NAME# ";
$MESS["CRM_ENTITY_TITLE_LEAD"] = "prospecto #ENTITY_NAME# ";
$MESS["CRM_ENTITY_TITLE_QUOTE"] = "cotización #ENTITY_NAME# ";
$MESS["CRM_ENTITY_TITLE_CONTACT"] = "contacto #ENTITY_NAME# ";
$MESS["CRM_ENTITY_TITLE_COMPANY"] = "compañia #ENTITY_NAME# ";
$MESS["CRM_ENTITY_TITLE_INVOICE"] = "factura #ENTITY_NAME# ";
?>