<?
$MESS["CRM_DEAL_RECURRING_NEXT_EXECUTION"] = "Siguiente fecha de creación";
$MESS["CRM_DEAL_RECURRING_NEXT_EXECUTION_CHANGED"] = "La siguiente fecha de creación cambió";
$MESS["CRM_DEAL_RECURRING_NOT_ACTIVE"] = "La negociación recurrente no está activa";
$MESS["CRM_DEAL_RECURRING_ACTIVE"] = "La negociación recurrente está activa";
$MESS["CRM_DEAL_CREATION"] = "Negociación agregada";
$MESS["CRM_DEAL_RECURRING_CREATION"] = "Plantilla de negociación creada";
$MESS["CRM_RECURRING_CREATION_BASED_ON"] = "Creado en base a";
$MESS["CRM_DEAL_BASE_CAPTION_BASED_ON_DEAL"] = "Basado en la negociación";
?>