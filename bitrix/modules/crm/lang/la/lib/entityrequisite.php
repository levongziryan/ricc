<?
$MESS["CRM_REQUISITE_ERR_INVALID_ENTITY_TYPE"] = "Datos incorrectos del tipo de entidad";
$MESS["CRM_REQUISITE_ERR_INVALID_ENTITY_ID"] = "Ningun dato de la entidad especificado para su uso.";
$MESS["CRM_REQUISITE_ERR_ON_DELETE"] = "Error al borrar datos.";
$MESS["CRM_REQUISITE_ERR_NOTHING_TO_DELETE"] = "No se han encontrado datos que desea eliminar.";
$MESS["CRM_REQUISITE_FIXED_PRESET_COMPANY"] = "Compañía";
$MESS["CRM_REQUISITE_FIXED_PRESET_LEGALENTITY"] = "Representante Legal";
$MESS["CRM_REQUISITE_FIXED_PRESET_INDIVIDUAL"] = "Trabajador independiente";
$MESS["CRM_REQUISITE_FIXED_PRESET_PERSON"] = "Persona Natural ";
$MESS["CRM_REQUISITE_FILTER_PREFIX"] = "Detalles";
$MESS["CRM_REQUISITE_ERR_COMPANY_NOT_EXISTS"] = "No se encontró la compañía ID: #ID#)";
$MESS["CRM_REQUISITE_ERR_CONTACT_NOT_EXISTS"] = "No se encontró el contacto (ID: #ID#)";
$MESS["CRM_REQUISITE_ERR_INVALID_IMP_PRESET_ID"] = "ID de plantilla de importación de detalles es inválido";
$MESS["CRM_REQUISITE_ERR_IMP_PRESET_NOT_EXISTS"] = "No se encontró la plantilla de importación de detalles (ID: #ID#)";
$MESS["CRM_REQUISITE_ERR_DEF_IMP_PRESET_NOT_DEFINED"] = "No se ha definido ninguna plantilla para importar detalles para entidades \"#ENTITY_TYPE#\"";
$MESS["CRM_REQUISITE_ERR_ACCESS_DENIED_COMPANY_UPDATE"] = "Acceso denegado para editar compañía (ID: #ID#)";
$MESS["CRM_REQUISITE_ERR_ACCESS_DENIED_CONTACT_UPDATE"] = "Acceso denegado para editar contacto (ID: #ID#)";
$MESS["CRM_REQUISITE_ERR_NO_ADDRESSES_TO_IMPORT"] = "No hay direcciones para importar";
$MESS["CRM_REQUISITE_ERR_IMP_PRESET_HAS_NO_ADDR_FIELD"] = "La plantilla de importación de detalles no incluye el campo de dirección (ID: #ID#)";
$MESS["CRM_REQUISITE_ERR_DUP_CTRL_MODE_SKIP"] = "Los detalles no se pueden importar en modo de omisión duplicado";
$MESS["CRM_REQUISITE_ERR_CREATE_REQUISITE"] = "Error al crear #ENTITY_TYPE_NAME_GENITIVE# (ID: #ID#)";
$MESS["CRM_REQUISITE_ERR_COMPANY_GENITIVE"] = "detalles de la compañía";
$MESS["CRM_REQUISITE_ERR_CONTACT_GENITIVE"] = "detalles de contacto";
?>