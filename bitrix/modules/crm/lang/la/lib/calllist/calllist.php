<?
$MESS["CRM_CALL_LIST_LIMIT_ERROR"] = "Máximo de elementos superado. Establezca el filtro para seleccionar no más de #LIMIT# elementos.";
$MESS["CRM_CALL_LIST_UPDATE"] = "Agregar a la lista de llamadas";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_HEADER"] = "Disponible en el plan Plus y superior";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_CONTENT"] = "Utilice listas de llamadas para crear una lista de clientes que esperan su llamada y cree una tarea respectiva para la persona responsable asignada.";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_ITEM_1"] = "Crear lista de llamadas en unos pocos clics";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_ITEM_2"] = "Llame a todos los clientes uno por uno en una sola ventana";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_ITEM_3"] = "Controle el progreso y los resultados de las llamadas";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_SHOW_MORE"] = "Leer más";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_FOOTER"] = "Las listas de llamadas y otras funciones útiles están disponibles iniciando con el plan Plus a solo \$39/mensual.";
$MESS["CRM_CALL_LIST_SUBJECT"] = "Lista de llamadas";
?>