<?
$MESS["CRM_RECUR_WRONG_ID"] = "ID de la cuenta inválido";
$MESS["CRM_RECUR_ACTIVATE_LIMIT_REPEAT"] = "Error al activar la orden porque se superó el recuento máximo de repetición. Puede que desee aumentar el valor máximo.";
$MESS["CRM_RECUR_ACTIVATE_LIMIT_DATA"] = "Error al activar la orden porque se alcanzó la última fecha de factura recurrente. Es posible que desee cambiar la fecha.";
?>