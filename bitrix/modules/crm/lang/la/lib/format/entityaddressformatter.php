<?
$MESS["CRM_ENTITY_ADDR_FRMT_EU"] = "Europa";
$MESS["CRM_ENTITY_ADDR_FRMT_UK"] = "Reino Unido";
$MESS["CRM_ENTITY_ADDR_FRMT_USA"] = "USA";
$MESS["CRM_ENTITY_ADDR_FRMT_RUS"] = "Rusia";
$MESS["CRM_ENTITY_ADDR_FRMT_SMPL_EU"] = "Musterstr. 321<br/>54321 Musterstadt<br/>Deutschland";
$MESS["CRM_ENTITY_ADDR_FRMT_SMPL_USA"] = "455 Larkspur Dr.<br/>California Springs CA 92926<br/>USA";
$MESS["CRM_ENTITY_ADDR_FRMT_SMPL_UK"] = "49 Featherstone Street<br/>LONDON<br/>EC1Y 8SY<br/>UNITED KINGDOM";
$MESS["CRM_ENTITY_ADDR_FRMT_SMPL_RUS"] = "Forest St. 5, Suite 176<br/>Moscow<br/>Russia<br/>125075";
$MESS["CRM_ENTITY_ADDR_FRMT_SMPL_RUS2"] = "125075<br/>Rusia<br/>Moscú<br/>Forest St. 5, Suite 176
";
$MESS["CRM_ENTITY_ADDR_FRMT_RUS2"] = "Rusia (variante 2)";
?>