<?
$MESS["CRM_INVOICE_SPEC_ENTITY_ID_FIELD"] = "ID";
$MESS["CRM_INVOICE_SPEC_ENTITY_DATE_INSERT_FIELD"] = "Creado el";
$MESS["CRM_INVOICE_SPEC_ENTITY_DATE_INS_FIELD"] = "Creado el";
$MESS["CRM_INVOICE_SPEC_ENTITY_DATE_UPDATE_FIELD"] = "Modificado el";
$MESS["CRM_INVOICE_SPEC_ENTITY_DATE_UPD_FIELD"] = "Modificado el";
$MESS["CRM_INVOICE_SPEC_ENTITY_PRODUCT_ID_FIELD"] = "ID del producto";
$MESS["CRM_INVOICE_SPEC_ENTITY_NAME_FIELD"] = "Nombre";
$MESS["CRM_INVOICE_SPEC_ENTITY_NAME_WITH_IDENT_FIELD"] = "[ID] Nombre";
$MESS["CRM_INVOICE_SPEC_ENTITY_ORDER_ID_FIELD"] = "ID de la factura";
$MESS["CRM_INVOICE_SPEC_ENTITY_INVOICE_FIELD"] = "Factura";
$MESS["CRM_INVOICE_SPEC_ENTITY_PRICE_FIELD"] = "Precio";
$MESS["CRM_INVOICE_SPEC_ENTITY_VAT_RATE_FIELD"] = "Tipo de impuesto ";
$MESS["CRM_INVOICE_SPEC_ENTITY_VAT_RATE_PRC_FIELD"] = "Tipo de impuesto (%)";
$MESS["CRM_INVOICE_SPEC_ENTITY_QUANTITY_FIELD"] = "Cantidad";
$MESS["CRM_INVOICE_SPEC_ENTITY_NOTES_FIELD"] = "Tipo de precio";
$MESS["CRM_INVOICE_SPEC_ENTITY_LID_FIELD"] = "Sitio Web";
$MESS["CRM_INVOICE_SPEC_ENTITY_SUMMARY_PRICE_FIELD"] = "Total";
?>