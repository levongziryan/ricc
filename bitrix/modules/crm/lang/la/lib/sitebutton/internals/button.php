<?
$MESS["CRM_BUTTON_LOCATION_TOP_LEFT"] = "Parte superior izquierda";
$MESS["CRM_BUTTON_LOCATION_TOP_MIDDLE"] = "Parte superior central";
$MESS["CRM_BUTTON_LOCATION_TOP_RIGHT"] = "Parte superior derecha";
$MESS["CRM_BUTTON_LOCATION_BOTTOM_RIGHT"] = "Parte inferior derecha";
$MESS["CRM_BUTTON_LOCATION_BOTTOM_MIDDLE"] = "Parte inferior central";
$MESS["CRM_BUTTON_LOCATION_BOTTOM_LEFT"] = "Parte inferior izquierda";
$MESS["CRM_BUTTON_FIELD_NAME_EMPTY"] = "Nombre del botón";
$MESS["CRM_BUTTON_FIELD_NAME_TITLE"] = "Nombre del widget";
?>