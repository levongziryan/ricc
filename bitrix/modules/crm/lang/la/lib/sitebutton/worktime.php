<?
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_MO"] = "Lu";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_TU"] = "Ma";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_WE"] = "Mié";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_TH"] = "Jue";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_FR"] = "Vie";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_SA"] = "Sáb";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_SU"] = "Dom";
$MESS["CRM_SITE_BUTTON_WORK_TIME_ACTIONS_HIDE"] = "Ocultar botón de devolución de llamada";
$MESS["CRM_SITE_BUTTON_WORK_TIME_ACTIONS_TEXT"] = "guardar la información del cliente y mostrar un mensaje";
$MESS["CRM_SITE_BUTTON_WORK_TIME_ACTIONS_TEXT_CALLBACK"] = "Desafortunadamente, no podemos volver a llamarle. Nos pondremos en contacto con usted lo antes posible en el horario comercial.";
?>