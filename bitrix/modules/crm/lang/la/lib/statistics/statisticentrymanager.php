<?
$MESS["CRM_STAT_MGR_REBUILD_STATISTICS"] = "Por Favor <a id=\"#ID#\" href=\"#URL#\"> actualizar estadísticas</a> para que losreportes muestren datos correctos.";
$MESS["CRM_STAT_MGR_REBUILD_STATISTICS_DLG_TITLE"] = "Actualizar estadísticas";
$MESS["CRM_STAT_MGR_REBUILD_STATISTICS_DLG_SUMMARY"] = "Esto actualizará los datos estadísticos. Esto puede tomar un tiempo largo.";
?>