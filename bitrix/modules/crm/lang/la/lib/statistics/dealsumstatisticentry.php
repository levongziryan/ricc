<?
$MESS["CRM_DEAL_SUM_STAT_ENTRY_SLOT_SUM_TOTAL"] = "Suma total";
$MESS["CRM_DEAL_SUM_STAT_ENTRY_TITLE"] = "Negociaciones";
$MESS["CRM_DEAL_SUM_STAT_ENTRY_REBUILD"] = "Los reportes requieren <a id=\"#ID#\" href=\"#URL#\">actualizar estadísticas de las negociaciones</a> para funcionar correctamente.";
$MESS["CRM_DEAL_SUM_STAT_ENTRY_REBUILD_DLG_TITLE"] = "Actualizar estadísticas de las negociaciones";
$MESS["CRM_DEAL_SUM_STAT_ENTRY_REBUILD_DLG_SUMMARY"] = "Esto actualizará las estadísticas de la negociaciones. Esto puede tomar un tiempo largo.";
?>