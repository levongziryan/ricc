<?
$MESS["CRM_INVOICE_SUM_STAT_ENTRY_SLOT_SUM_TOTAL"] = "Importe total";
$MESS["CRM_INVOICE_SUM_STAT_ENTRY_TITLE"] = "Facturas";
$MESS["CRM_INVOICE_SUM_STAT_ENTRY_REBUILD"] = "Por favor <a id=\"#ID#\" href=\"#URL#\">actualice las estadísticas de la factura</a> para obtener los reportes correctos.";
$MESS["CRM_INVOICE_SUM_STAT_ENTRY_REBUILD_DLG_TITLE"] = "Actualizar estadísticas de las facturas";
$MESS["CRM_INVOICE_SUM_STAT_ENTRY_REBUILD_DLG_SUMMARY"] = "Esto actualizará las estadísticas de la factura. Puede tardar un poco.";
?>