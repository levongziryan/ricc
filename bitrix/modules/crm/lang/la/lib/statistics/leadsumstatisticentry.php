<?
$MESS["CRM_LEAD_SUM_STAT_ENTRY_SLOT_SUM_TOTAL"] = "Importe total";
$MESS["CRM_LEAD_SUM_STAT_ENTRY_TITLE"] = "Prospectos";
$MESS["CRM_LEAD_SUM_STAT_ENTRY_REBUILD"] = "Por favor <a id=\"#ID#\" href=\"#URL#\">actualizar las estadísticas de los prospectos</a> para obtener reportes correctos.";
$MESS["CRM_LEAD_SUM_STAT_ENTRY_REBUILD_DLG_TITLE"] = "Actualizar estadísticas de los prospectos";
$MESS["CRM_LEAD_SUM_STAT_ENTRY_REBUILD_DLG_SUMMARY"] = "Esto actualizará las estadísticas de los prospectos. Puede tardar un poco.";
?>