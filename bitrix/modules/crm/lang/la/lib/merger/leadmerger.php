<?
$MESS["CRM_LEAD_MERGER_COLLISION_READ_PERMISSION"] = "#USER_NAME#  ha fusionado una empresa \"#SEED_TITLE#\" [#SEED_ID#] with \"#TARG_TITLE#\" [#TARG_ID#] que no puede ver debido a las preferencias de permiso de acceso.";
$MESS["CRM_LEAD_MERGER_COLLISION_UPDATE_PERMISSION"] = "#USER_NAME# ha fusionado una empresa \"#SEED_TITLE#\" [#SEED_ID#] with \"#TARG_TITLE#\" [#TARG_ID#] que no puede editar debido a las preferencias de permiso de acceso.";
$MESS["CRM_LEAD_MERGER_COLLISION_READ_UPDATE_PERMISSION"] = "#USER_NAME# ha fusionado una empresa \"#SEED_TITLE#\" [#SEED_ID#] with \"#TARG_TITLE#\" [#TARG_ID#] que no puede ver o editar debido a las preferencias de permiso de acceso.";
?>