<?
$MESS["CRM_IBLOCK_PROPERTY_BIND_CRM_ELEMENT"] = "Vincular a elementos del CRM";
$MESS["CRM_IBLOCK_PROPERTY_SETTINGS_LABLE_ENTITY"] = "Entidad disponible:";
$MESS["CRM_IBLOCK_PROPERTY_ENTITY_LEAD"] = "Prospecto";
$MESS["CRM_IBLOCK_PROPERTY_ENTITY_CONTACT"] = "Contacto";
$MESS["CRM_IBLOCK_PROPERTY_ENTITY_COMPANY"] = "Compañía";
$MESS["CRM_IBLOCK_PROPERTY_ENTITY_DEAL"] = "Negociación";
$MESS["CRM_IBLOCK_PROPERTY_FORMAT_ERROR"] = "Formato de campo no válido: \"Vincular a elementos del CRM\"";
$MESS["CRM_IBLOCK_PROPERTY_ENTITY_SAVE"] = "Guardar";
$MESS["CRM_IBLOCK_PROPERTY_SETTINGS_LABLE_VISIBLE"] = "Mostrar en el formulario de detalles del CRM:";
?>