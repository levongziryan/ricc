<?
$MESS["CRM_B24_EMAIL_FREE_LICENSE_SIGNATURE"] = "Enviado por Bitrix24: Kit de herramientas de negocio en línea 
[url=http://www.bitrix24.com]bitrix24.com[/url]";
$MESS["CRM_B24_EMAIL_PAID_LICENSE_SIGNATURE"] = "Enviado por Bitrix24";
$MESS["CRM_B24_EMAIL_SIGNATURE_ENABLED"] = "Activar (se puede desactivar sólo en planes comerciales Standard o Professional)";
$MESS["CRM_B24_EMAIL_SIGNATURE_DISABLED"] = "Desactivar";
?>