<?
$MESS["CRM_INTEGRATION_SMS_PROVIDER_TWILIO_NAME"] = "Twilio.com";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_TWILIO_CAN_USE_ERROR"] = "La integración de Twilio no está configurada";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_TWILIO_ERROR_ACCOUNT_INACTIVE"] = "La cuenta especificada está inactiva";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_TWILIO_MESSAGE_STATUS_ACCEPTED"] = "Aceptado";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_TWILIO_MESSAGE_STATUS_QUEUED"] = "En la cola";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_TWILIO_MESSAGE_STATUS_SENDING"] = "Enviando";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_TWILIO_MESSAGE_STATUS_SENT"] = "Enviado";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_TWILIO_MESSAGE_STATUS_DELIVERED"] = "Entregado";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_TWILIO_MESSAGE_STATUS_UNDELIVERED"] = "No entregado";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_TWILIO_MESSAGE_STATUS_FAILED"] = "Error";
?>