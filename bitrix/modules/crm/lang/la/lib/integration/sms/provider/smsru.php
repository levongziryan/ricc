<?
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_NAME"] = "Proveedor SMS.RU";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_CAN_USE_ERROR"] = "El proveedor de SMS.RU no está configurado";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_201"] = "Saldo insuficiente";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_202"] = "Destinatario incorrecto";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_203"] = "El texto del mensaje está vacío";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_204"] = "El nombre del remitente no está confirmado por la administración";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_205"] = "El mensaje es demasiado largo (más de 8 SMS)";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_206"] = "Límite de mensaje diario excedido o superado";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_207"] = "No puede enviar mensajes a este número o uno de los números, o la lista de destinatarios excede el límite de 100 números";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_209"] = "Has agregado este número o uno de los números para detener la lista";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_230"] = "Límite de mensaje diario excedido para este número";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_231"] = "Se superó el límite de mensajes idénticos por minuto para este número";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_232"] = "Límite diario de mensajes idénticos excedido para este número";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_301"] = "No se encontró la contraseña o el usuario es incorrecto";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_302"] = "La cuenta no se confirmó (el usuario no ingresó el código que se había enviado en el mensaje de confirmación SMS)";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_303"] = "El código de confirmación es incorrecto";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_304"] = "Demasiados códigos de confirmación enviados. Por favor, inténtelo de nuevo más tarde";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_305"] = "Demasiados intentos incorrectos. Por favor, inténtelo de nuevo más tarde";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_900"] = "El código de confirmación es incorrecto";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_901"] = "El número de teléfono es incorrecto o formato incorrecto";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_903"] = "No se puede registrar un número con este código en este momento";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_905"] = "El usuario no proporcionó el nombre (o el nombre es más corto que 2 caracteres)";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_906"] = "El usuario no proporcionó el apellido (o el apellido tiene menos de 2 caracteres)";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_OTHER"] = "Error del sistema. Inténtalo de nuevo";
?>