<?
$MESS["CRM_USER_CONSENT_DATA_PROVIDER_NAME"] = "Detalles de la compañía en el CRM";
$MESS["CRM_USER_CONSENT_PROVIDER_NAME"] = "Actividad del CRM";
$MESS["CRM_USER_CONSENT_PROVIDER_ITEM_NAME"] = "Actividad del CRM #%id%";
$MESS["CRM_USER_CONSENT_NOTIFY_TEXT_BTN"] = "Más";
$MESS["CRM_USER_CONSENT_DEF_NAME"] = "Muestra el consentimiento para el procesamiento de datos personales";
?>