<?
$MESS["WS_CRMINTEGRATION_APP_TITLE"] = "CRM";
$MESS["WS_CRMINTEGRATION_APP_DESC"] = "Cuando se trabaja con el CRM, usted puede configurar el intercambio de datos con otros programas. Elija una de las opciones de conexión y haga clic en la opción \"Obtener Contraseña\".";
$MESS["WS_CRMINTEGRATION_APP_OPTIONS_CAPTION"] = "Conecte CRM";
$MESS["WS_CRMINTEGRATION_APP_OPTIONS_TITLE_SALE"] = "Integración con e-store";
?>