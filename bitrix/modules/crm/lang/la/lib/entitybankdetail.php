<?
$MESS["CRM_BANKDETAIL_ERR_INVALID_ENTITY_TYPE"] = "Tipo de entidad bancaria incorrecta";
$MESS["CRM_BANKDETAIL_ERR_INVALID_ENTITY_ID"] = "No hay datos bancarios de la entidad especificada";
$MESS["CRM_BANKDETAIL_ERR_ON_DELETE"] = "Error al eliminar los datos bancarios";
$MESS["CRM_BANKDETAIL_ERR_NOTHING_TO_DELETE"] = "No se han encontrado detalles bancarios para eliminarse";
?>