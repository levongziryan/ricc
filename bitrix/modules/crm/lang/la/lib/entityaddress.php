<?
$MESS["CRM_ENTITY_ADDRESS_1"] = "Calle, edificio";
$MESS["CRM_ENTITY_ADDRESS_2"] = "Habitación / Apartamento";
$MESS["CRM_ENTITY_ADDRESS_CITY"] = "Ciudad";
$MESS["CRM_ENTITY_ADDRESS_POSTAL_CODE"] = "Código Postal";
$MESS["CRM_ENTITY_ADDRESS_REGION"] = "Región";
$MESS["CRM_ENTITY_ADDRESS_PROVINCE"] = "Estado / Provincia";
$MESS["CRM_ENTITY_ADDRESS_COUNTRY"] = "País";
$MESS["CRM_ENTITY_REG_ADDRESS_1"] = "Calle, edificio (facturación)";
$MESS["CRM_ENTITY_REG_ADDRESS_2"] = "Habitación / Apartamento (facturación)";
$MESS["CRM_ENTITY_REG_ADDRESS_CITY"] = "Ciudad (facturación)";
$MESS["CRM_ENTITY_REG_ADDRESS_POSTAL_CODE"] = "Código Postal (facturación)";
$MESS["CRM_ENTITY_REG_ADDRESS_REGION"] = "Región (facturación)";
$MESS["CRM_ENTITY_REG_ADDRESS_PROVINCE"] = "Estado / Provincia (facturación)";
$MESS["CRM_ENTITY_REG_ADDRESS_COUNTRY"] = "País (facturación)";
$MESS["CRM_ENTITY_SHORT_ADDRESS_1"] = "Calle";
$MESS["CRM_ENTITY_SHORT_ADDRESS_2"] = "Apartamento / Habitación";
$MESS["CRM_ENTITY_SHORT_ADDRESS_CITY"] = "Ciudad";
$MESS["CRM_ENTITY_SHORT_ADDRESS_POSTAL_CODE"] = "Código Postal";
$MESS["CRM_ENTITY_SHORT_ADDRESS_REGION"] = "Región";
$MESS["CRM_ENTITY_SHORT_ADDRESS_PROVINCE"] = "Estado / Provincia";
$MESS["CRM_ENTITY_SHORT_ADDRESS_COUNTRY"] = "País";
$MESS["CRM_ENTITY_SHORT_REG_ADDRESS_1"] = "Calle (facturación)";
$MESS["CRM_ENTITY_SHORT_REG_ADDRESS_2"] = "Apartamento / Habitación (facturación)";
$MESS["CRM_ENTITY_SHORT_REG_ADDRESS_CITY"] = "Ciudad (facturación)";
$MESS["CRM_ENTITY_SHORT_REG_ADDRESS_POSTAL_CODE"] = "Código Postal (facturación)";
$MESS["CRM_ENTITY_SHORT_REG_ADDRESS_REGION"] = "Región (facturación)";
$MESS["CRM_ENTITY_SHORT_REG_ADDRESS_PROVINCE"] = "(facturación)";
$MESS["CRM_ENTITY_SHORT_REG_ADDRESS_COUNTRY"] = "País (facturación)";
$MESS["CRM_ENTITY_FULL_ADDRESS"] = "Dirección";
$MESS["CRM_ENTITY_FULL_REG_ADDRESS"] = "Dirección De Facturación";
$MESS["CRM_ENTITY_ADDRESS_CHANGE_EVENT_EMPTY"] = "-Vacío-";
$MESS["CRM_ENTITY_ADDRESS_PRY"] = "Dirección Fisica";
$MESS["CRM_ENTITY_ADDRESS_REG"] = "Dirección Legal";
?>