<?
$MESS["CRM_WEBFORM_FORM_ERROR_SCHEME"] = "Esquema del documento no es válido.";
$MESS["CRM_WEBFORM_FORM_BUTTON_CAPTION_DEFAULT"] = "Enviar";
$MESS["CRM_WEBFORM_FORM_SCRIPT_BUTTON_TEXT"] = "Nombre del botón";
$MESS["CRM_WEBFORM_FORM_COPY_NAME_PREFIX"] = "Copiar";
$MESS["CRM_WEBFORM_FORM_ERROR_CAPTCHA_KEY"] = "Se requiere una clave y una clave secreta para usar CAPTCHA";
$MESS["CRM_WEBFORM_FORM_PRESET_FIELDS"] = "En los campos predeterminados";
?>