<?
$MESS["CRM_WEBFORM_PRESET_ITEM_CD_NAME"] = "Datos de contacto";
$MESS["CRM_WEBFORM_PRESET_ITEM_CD_CAPTION"] = "Datos de contacto";
$MESS["CRM_WEBFORM_PRESET_ITEM_CD_DESCRIPTION"] = "Por favor, deje sus datos de contacto y nos pondremos en contacto con usted";
$MESS["CRM_WEBFORM_PRESET_ITEM_FB_NAME"] = "Formulario de comentarios";
$MESS["CRM_WEBFORM_PRESET_ITEM_FB_CAPTION"] = "Comentarios";
$MESS["CRM_WEBFORM_PRESET_ITEM_FB_DESCRIPTION"] = "Por favor, comparta sus opiniones en el campo Comentario";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_RESULT_SUCCESS_TEXT"] = "Gracias! Su mensaje ha sido enviado.";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_RESULT_FAILURE_TEXT"] = "Desafortunadamente, el mensaje no ha sido enviado, por favor intente de nuevo más tarde.";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_FIELD_NAME"] = "Nombre";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_FIELD_LAST_NAME"] = "Apellido";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_FIELD_PHONE"] = "Teléfono";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_FIELD_EMAIL"] = "E-mail";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_FIELD_COMMENTS"] = "Comentario";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_NAME"] = "Volver a llamar desde \"#call_from#\"";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_CAPTION"] = "¿No encontro lo que estaba buscando?";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_DESCRIPTION"] = "¿Todavía tiene preguntas? Nosotros lo volveremos a llamar";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_BUTTON_CAPTION"] = "Volver a llamar";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_RESULT_SUCCESS_TEXT"] = "Nuestro representante se comunicará con usted en unos pocos segundos.";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_CALL_TEXT"] = "Una devolución de llamada ha sido solicitada; conectándolo con el cliente ahora.";
?>