<?
$MESS["CRM_WEBFORM_HELPER_TEMPLATE_LIGHT"] = "Claro";
$MESS["CRM_WEBFORM_HELPER_TEMPLATE_TRANSPARENT"] = "Transparente";
$MESS["CRM_WEBFORM_HELPER_TEMPLATE_COLORED"] = "De colores";
$MESS["CRM_WEBFORM_HELPER_STRING_TYPES_PHONE"] = "Teléfono";
$MESS["CRM_WEBFORM_HELPER_STRING_TYPES_EMAIL"] = "E-mail";
$MESS["CRM_WEBFORM_HELPER_STRING_TYPES_INT"] = "Número";
$MESS["CRM_WEBFORM_HELPER_EXTERNAL_ANALYTICS_CATEGORY"] = "Completar formulario";
$MESS["CRM_WEBFORM_HELPER_EXTERNAL_ANALYTICS_FIELD"] = "Completar formulario";
$MESS["CRM_WEBFORM_HELPER_EXTERNAL_ANALYTICS_VIEW"] = "Ver formulario";
$MESS["CRM_WEBFORM_HELPER_EXTERNAL_ANALYTICS_START"] = "Iniciar el llenado del formulario";
$MESS["CRM_WEBFORM_HELPER_EXTERNAL_ANALYTICS_END"] = "Finalizar el llenado del formulario";
?>