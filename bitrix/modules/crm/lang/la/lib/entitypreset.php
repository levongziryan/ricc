<?
$MESS["CRM_ENTITY_TYPE_REQUISITE"] = "Detalles";
$MESS["CRM_ENTITY_TYPE_REQUISITE_DESC"] = "Plantillas para el contacto o detalles de la empresa";
$MESS["CRM_ENTITY_PRESET_ERR_DELETE_PRESET_USED"] = "La plantilla no se puede eliminar porque existen detalles creados en él.";
$MESS["CRM_ENTITY_PRESET_ERR_PRESET_NOT_FOUND"] = "No se ha encontrado la plantilla.";
$MESS["CRM_ENTITY_PRESET_ERR_INVALID_ENTITY_TYPE"] = "Tipo de entidad no válido para el uso con plantilla.";
$MESS["CRM_ENTITY_PRESET_ERR_DELETE_PRESET_DEF_FOR_COMPANY"] = "No puede eliminar esta plantilla porque es una plantilla de compañía predeterminada.";
$MESS["CRM_ENTITY_PRESET_ERR_DELETE_PRESET_DEF_FOR_CONTACT"] = "No puede eliminar esta plantilla porque es una plantilla de contacto predeterminado.";
$MESS["CRM_ENTITY_PRESET_NAME_EMPTY"] = "Plantilla sin título";
?>