<?
$MESS["CRM_LEAD_SETTINGS_VIEW_WIDGET"] = "Análisis de prospectos";
$MESS["CRM_LEAD_SETTINGS_VIEW_LIST"] = "Lista de prospectos";
$MESS["CRM_TYPE_TITLE"] = "Elija la forma en que desea trabajar con su CRM";
$MESS["CRM_TYPE_SAVE"] = "Guardar";
$MESS["CRM_TYPE_CANCEL"] = "Cancelar";
$MESS["CRM_TYPE_TURN_ON"] = "Habilitar";
$MESS["CRM_ROBOTS_TITLE"] = "Modo CRM simple";
$MESS["CRM_ROBOTS_TEXT"] = "En el modo CRM simple, Bitrix24 convierte automáticamente los nuevos prospectos a negociaciones usando reglas de automatización<br/><br/>
Ya tiene reglas de automatización de un prospecto que están activas actualmente. Habilitar este modo eliminará estas reglas.<br/><br/>
¿Seguro que quiere habilitar el modo CRM simple?";
$MESS["CRM_LEAD_SETTINGS_VIEW_KANBAN"] = "Kanban de prospectos";
?>