<?
$MESS["CRM_RESTR_MGR_DUP_CTRL_MSG_CONTENT"] = "<div class=\"crm-duplicate-tab-content\">
<h3 class=\"crm-duplicate-tab-title\">Busqueda avanzado de duplicado</h3>
<div class=\"crm-duplicate-tab-text\">
Cuando se crea un nuevo contacto (o prospectos o compañía), Bitrix24 busca y muestra los posibles duplicados &mdash; detener preventivamente los duplicados que se están creando.
</div>
<div class=\"crm-duplicate-tab-text\">
En las opciones avanzadas de búsqueda de duplicados, el CRM puede también encontrar duplicados en los datos importados y en datos ya introducidos en la base de datos. Estos duplicados se pueden combinar entre sí.
</div>
<div class=\"crm-duplicate-tab-text\">
Agregar esta y otras funciones muy útiles a su Bitrix24! Telefonía Avanzada + CRM avanzada y otras características útiles están disponibles a partir del plan de Bitrix24 Plus por sólo \$ 39/mes.
<a target=\"_blank\" href=\"https://www.bitrix24.com/pro/crm.php\">Find out more</a>
</div>
<div class=\"crm-history-tab-buttons\">
<span class=\"webform-button webform-button-create\" onclick=\"window.location = '#LICENSE_LIST_URL#';\">Obtener un plan extendido</span>
<span class=\"webform-button webform-button-transparent\" onclick=\"window.location = '#DEMO_LICENSE_URL#';\">Obtenga una prueba gratuita de 30 días</span>
</div>
</div>
";
$MESS["CRM_RESTR_MGR_HX_VIEW_MSG_CONTENT"] = "<div class=\"crm-history-tab-content\">
	<h3 class=\"crm-history-tab-title\">Cambiar el historial del CRM está disponible sólo en el CRM avanzado</h3>
	<div class=\"crm-history-tab-text\">
		Bitrix24 mantiene un registro detallado de todos los cambios del CRM. Al actualizar el CRM avanzado podrás ver el historial de cambios (es decir, quién accede o cambia la entrada al CRM) y será capaz de restaurar los valores anteriores, si es necesario.
	</div>
	<div class=\"crm-history-tab-text\">This is what it looks like:</div>
	<img alt=\"Tab History\" class=\"crm-history-tab-img\" src=\"/bitrix/js/crm/images/history-en.png\"/>
	<div class=\"crm-history-tab-text\">
		Agregar esta y otras funciones muy útiles a su Bitrix24! Telefonía Avanzada + CRM avanzada y otras características útiles están disponibles a partir del plan de Bitrix24 Plus por sólo \$ 39/mes.
	<a target=\"_blank\" href=\"https://www.bitrix24.com/pro/crm.php\">Find out more</a>
	</div>
	<div class=\"crm-history-tab-buttons\">
		<span class=\"webform-button webform-button-create\" onclick=\"window.location = '#LICENSE_LIST_URL#';\">Ir al plan extendido</span>
		<span class=\"webform-button webform-button-transparent\" onclick=\"window.location = '#DEMO_LICENSE_URL#';\">Gratuito durante 30 días</span>
	</div>
</div>";
$MESS["CRM_RESTR_MGR_POPUP_TITLE"] = "CRM avanzado en Bitrix24";
$MESS["CRM_RESTR_MGR_POPUP_CONTENT"] = "Agregue lo siguiente a su CRM:
<ul class=\"hide-features-list\">
 <li class=\"hide-features-list-item>Conversión entre negociaciones, facturas, y cotizaciones</li>
<li class=\"hide-features-list-item\">Búsqueda extendida de duplicados</li>
<li class=\"hide-features-list-item\">Cambiar el historial de reversión y recuperación de CRM</li>
<li class=\"hide-features-list-item\">Registro de acceso al CRM</li>
<li class=\"hide-features-list-item\">Visualización de más de 5000 registros en el CRM</li>
<li class=\"hide-features-list-item\">List-driven calling<sup class=\"hide-features-soon\">coming soon</sup></li>
<li class=\"hide-features-list-item\">Bulk Emails to clients<sup class=\"hide-features-soon\">coming soon</sup></li>
<li class=\"hide-features-list-item\">Support for sales of services<sup class=\"hide-features-soon\">coming soon</sup>
<a target=\"_blank\" class=\"hide-features-more\" href=\"https://www.bitrix24.com/pro/crm.php\">Find out more</a>
</li>
</ul>
<strong>Telefonía Avanzada + CRM avanzada y otras características útiles están disponibles a partir del plan de Bitrix24 Plus por sólo \$ 39/mes.</strong>";
$MESS["CRM_RESTR_MGR_DEAL_CATEGORY_POPUP_TITLE"] = "Múltiples Pipelines del CRM";
$MESS["CRM_RESTR_MGR_DEAL_CATEGORY_POPUP_CONTENT"] = "<div class=\"crm-deal-category-tab-content\">
 <div class=\"crm-deal-category-tab-text\">
Planes de Bitrix24 gratuitos y Plus se limitan a un solo pipeline/embudo de ventas. Para agregar la segunda pipeline de la negociación, por favor, actualice al plan Bitrix24 Standard. Para desbloquear pipelines ilimitadas y embudo de ventas, actualice a Bitrix24 Professional.
 </div>
</div>";
?>