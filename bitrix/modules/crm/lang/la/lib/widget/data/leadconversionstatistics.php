<?
$MESS["CRM_LEAD_CONV_STAT_PRESET_OVERALL_COUNT"] = "Número de prospectos convertidos";
$MESS["CRM_LEAD_CONV_STAT_PRESET_CONTACT_COUNT"] = "Número de contactos obtenidos desde prospectos";
$MESS["CRM_LEAD_CONV_STAT_PRESET_COMPANY_COUNT"] = "Número de compañías obtenidos desde prospectos";
$MESS["CRM_LEAD_CONV_STAT_PRESET_DEAL_COUNT"] = "Número de negociaciones obtenidos desde prospectos";
$MESS["CRM_LEAD_CONV_STAT_PRESET_OVERALL_SUM"] = "Importe total de prospectos convertidos";
$MESS["CRM_LEAD_CONV_STAT_PRESET_OVERALL_COUNT_SHORT"] = "Número de prospectos";
$MESS["CRM_LEAD_CONV_STAT_PRESET_OVERALL_SUM_SHORT"] = "Importe total de prospectos";
$MESS["CRM_LEAD_CONV_CATEGORY"] = "Prospectos convertidos";
?>