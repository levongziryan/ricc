<?
$MESS["CRM_ACTIVITY_ACTIVITY_STATUS_STAT_TOTAL"] = "Número de solicitudes";
$MESS["CRM_ACTIVITY_ACTIVITY_STATUS_STAT_ANSWERED_QTY"] = "Número de solicitudes atendidas";
$MESS["CRM_ACTIVITY_ACTIVITY_STATUS_STAT_UNANSWERED_QTY"] = "Número de solicitudes no atendidas";
$MESS["CRM_ACTIVITY_ACTIVITY_STATUS_STAT_GROUP_BY_STATUS"] = "Estado de actividad";
?>