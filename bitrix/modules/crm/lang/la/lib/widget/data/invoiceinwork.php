<?
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_COUNT"] = "Número de facturas activas";
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_COUNT_SHORT"] = "Número de facturas";
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_SUM"] = "Importe total de facturas activas";
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_SUM_SHORT"] = "Importe total de facturas";
$MESS["CRM_INVOICE_IN_WORK_CATEGORY"] = "Facturas activas";
$MESS["CRM_INVOICE_OWED_CATEGORY"] = "Facturas pendientes de pago";
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_COUNT_OWED"] = "Número de facturas pendientes de pago";
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_SUM_OWED"] = "Importe total de facturas pendientes de pago";
?>