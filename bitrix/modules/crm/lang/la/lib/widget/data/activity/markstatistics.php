<?
$MESS["CRM_ACTIVITY_ACTIVITY_MARK_STAT_TOTAL"] = "Número de actividades";
$MESS["CRM_ACTIVITY_ACTIVITY_MARK_STAT_NONE_QTY"] = "Número de actividades no calificadas";
$MESS["CRM_ACTIVITY_ACTIVITY_MARK_STAT_NEGATIVE_QTY"] = "Número de actividades calificadas negativamente";
$MESS["CRM_ACTIVITY_ACTIVITY_MARK_STAT_POSITIVE_QTY"] = "Número de actividades calificadas positivamente";
$MESS["CRM_ACTIVITY_ACTIVITY_MARK_STAT_GROUP_BY_SOURCE"] = "Origen de la actividad";
$MESS["CRM_ACTIVITY_ACTIVITY_MARK_STAT_GROUP_BY_MARK"] = "Calificación de la actividad";
?>