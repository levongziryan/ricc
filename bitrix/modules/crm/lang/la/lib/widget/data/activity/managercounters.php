<?
$MESS["CRM_MANAGER_CNTR_SUCCEED"] = "Terminado";
$MESS["CRM_MANAGER_CNTR_FAILED"] = "Restante";
$MESS["CRM_MANAGER_CNTR_LEAD"] = "Prospectos";
$MESS["CRM_MANAGER_CNTR_CONTACT"] = "Contactos";
$MESS["CRM_MANAGER_CNTR_COMPANY"] = "Compañías";
$MESS["CRM_MANAGER_CNTR_DEAL"] = "Negociaciones";
$MESS["CRM_MANAGER_CNTR_TITLE"] = "Contadores del gerente";
$MESS["CRM_MANAGER_CNTR_VIDEO"] = "<div style=\"width:400px;height:100px;font-size:15px;\">Use su contador personal para medir su desempeño: el CRM realiza un seguimiento de sus tareas completadas y restantes para hoy. Tu objetivo es no tener tareas pendientes al final del día.</div>";
?>