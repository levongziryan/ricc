<?
$MESS["CRM_ACTIVITY_DYNAMIC_CHANNELS_ARE_NOT_FOUND"] = "No hay canales disponibles.";
$MESS["CRM_ACTIVITY_DYNAMIC_CONNECTED_CHANNELS"] = "Canales conectados";
$MESS["CRM_ACTIVITY_DYNAMIC_CONNECTED_CHANNELS_IN"] = "Entrante";
$MESS["CRM_ACTIVITY_DYNAMIC_CONNECTED_CHANNELS_OUT"] = "Saliente";
$MESS["CRM_ACTIVITY_DYNAMIC_UNCONNECTED_1"] = "y otros";
$MESS["CRM_ACTIVITY_DYNAMIC_UNCONNECTED_3"] = "Conecta más canales:";
$MESS["CRM_ACTIVITY_DYNAMIC_CONNECTED"] = "Conectado:";
$MESS["CRM_ACTIVITY_DYNAMIC_CONNECT"] = "Conectar:";
$MESS["CRM_CH_TRACKER_START_VIDEO"] = "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/GipsvUe1gB0?rel=0&autoplay=1#VOLUME#\" frameborder=\"0\" allowfullscreen></iframe>";
$MESS["CRM_CH_TRACKER_START_VIDEO_TITLE"] = "Puedes ver el video en cualquier momento";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_IMOPENLINE"] = "Messengers y redes sociales";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_IMOPENLINE_1"] = "Primer Canal Abierto";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_WEBFORM"] = "Formulario CRM";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_LIVECHAT"] = "Chat en vivo";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_VKGROUP"] = "VK";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_WEBFORM_1"] = "Información del contacto";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_WEBFORM_2"] = "Formulario de comentarios";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_WEBFORM_4"] = "Devolución de llamada desde \"Número principal de salida\"";
$MESS["CRM_ACTIVITY_DYNAMIC_TITLE"] = "Visión general de comunicación";
?>