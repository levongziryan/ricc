<?
$MESS["CRM_INVOICE_OVERDUE_PRESET_OVERALL_COUNT"] = "Número de facturas vencidas";
$MESS["CRM_INVOICE_OVERDUE_PRESET_OVERALL_SUM"] = "Importe total de las facturas vencidas";
$MESS["CRM_INVOICE_OVERDUE_PRESET_OVERALL_COUNT_SHORT"] = "Número de facturas";
$MESS["CRM_INVOICE_OVERDUE_PRESET_OVERALL_SUM_SHORT"] = "Importe total de las facturas";
$MESS["CRM_INVOICE_OVERDUE_CATEGORY"] = "Facturas vencidas";
?>