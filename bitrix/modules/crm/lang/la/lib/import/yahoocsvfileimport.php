<?
$MESS["CRM_IMPORT_YAHOO_ERROR_FIELDS_NOT_FOUND"] = "Ninguno de los siguientes campos se encontraron:";
$MESS["CRM_IMPORT_YAHOO_REQUIREMENTS"] = "Un archivo debe cumplir los siguientes requisitos para ser importados con éxito: encoding: UTF-16; Campo idioma: Inglés; separador de campo: coma.";
?>