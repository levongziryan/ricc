<?
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_FIRST_NAME"] = "Primer Nombre";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_MIDDLE_NAME"] = "Segundo Nombre";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_LAST_NAME"] = "Apelldio";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_COMPANY"] = "Compañía";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_JOB_TITLE"] = "Compañía";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_STREET"] = "Dirección de la compañía";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_CITY"] = "Ciudad de la compañía";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_STATE"] = "Estado de la compañía";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_POSTAL_CODE"] = "Código Postal dela Compañía";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_POSTAL_COUNTRY"] = "Compañía País/Región";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_STREET"] = "Dirección Domiciliaria";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_CITY"] = "Ciudad del Domicilio";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_STATE"] = "Estado del Domicilio";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_POSTAL_CODE"] = "Código Postal del Domiciliao";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_COUNTRY"] = "Domicilio País/Región";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_STREET"] = "Otra Dirección ";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_CITY"] = "Otra Ciudad";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_STATE"] = "Otro Estado";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_POSTAL_CODE"] = "Otro Código Postal";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_COUNTRY"] = "Otra País/Región";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_FAX"] = "Fax de la Compañía";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_PHONE"] = "Teléfono de la Compañía";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BUSINESS_PHONE_2"] = "Teléfono 2 de la Compañía";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_FAX"] = "Fax del Domicilio";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_PHONE"] = "Teléfono del Domicilio";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_HOME_PHONE_2"] = "Teléfono 2 del Domicilio";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_PRIMARY_PHONE"] = "Teléfono Principal";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_COMPANY_MAIN_PHONE"] = "Principal Teléfono de la Compañía";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_MOBILE_PHONE"] = "Teléfono Móvil";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_RADIO_PHONE"] = "Radio de Teléfono";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_CAR_PHONE"] = "Teléfono del carro";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_FAX"] = "Otro Fax";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_OTHER_PHONE"] = "Otro teléfono";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_PAGER"] = "Paginador";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_BIRTHDAY"] = "Cumpleños";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_E_MAIL_ADDRESS"] = "Dirección de e-mail";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_E_MAIL_2_ADDRESS"] = "Dirección 2 de e-mail";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_E_MAIL_3_ADDRESS"] = "Dirección 3 de e-mail";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_NOTES"] = "Notas";
$MESS["CRM_IMPORT_OUTLOOK_ALIAS_WEB_PAGE"] = "Página web";
$MESS["CRM_IMPORT_OUTLOOK_ERROR_FIELDS_NOT_FOUND"] = "Ninguno de los siguientes campos se encontraron: #FIELD_LIST#";
$MESS["CRM_IMPORT_OUTLOOK_REQUIREMENTS"] = "Un archivo de importación debe cumplir con los siguientes requisitos para importar correctamente. Codificación de archivo:  #FILE_ENCODING#.  Idioma para los encabezados de columna: # FILE_LANG #. Separador de campo: coma.";
?>