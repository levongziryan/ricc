<?
$MESS["CRM_IMPORT_GMAIL_ERROR_FIELDS_NOT_FOUND"] = "Ninguno de los siguientes campos se encontraron: #FIELD_LIST#";
$MESS["CRM_IMPORT_GMAIL_REQUIREMENTS"] = "Un archivo debe cumplir los siguientes requisitos para ser importados con éxito: encoding: UTF-16; Campo idioma: Inglés; separador de campo: coma.";
?>