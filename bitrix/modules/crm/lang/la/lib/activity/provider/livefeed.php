<?
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_NAME"] = "Flujo de Actividad";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TYPE_LOG_ENTRY"] = "Mensaje del Flujo de Actividad";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TYPE_LOG_COMMENT"] = "Comentario del mensaje del Flujo de Actividad";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TYPE_LOG_COMMENT_OUT"] = "Comentario del mensaje del Flujo de Actividad (saliente)";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TYPE_LOG_COMMENT_IN"] = "Comentario del mensaje del Flujo de Actividad (entrante)";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TITLE_ENTRY"] = "Mensaje del Flujo de Actividad";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TITLE_COMMENT_IN"] = "Comentario entrante al mensaje del Flujo de Actividad";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TITLE_COMMENT_OUT"] = "Comentario saliente al mensaje del Flujo de Actividad";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_LINK_ENTRY"] = "Ir al mensaje";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_LINK_COMMENT"] = "Ir al comentario";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_AUTHOR_ACTION"] = "#USER_NAME# escribir:";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_AUTHOR_ACTIONM"] = "#USER_NAME# escribir:";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_AUTHOR_ACTIONF"] = "#USER_NAME# escribir:";
?>