<?
$MESS["CRM_REST_EXTERNAL_CHANNEL_ACTIVITY"] = "Canal externo. Documentos";
$MESS["CRM_REST_EXTERNAL_CHANNEL_ACTIVITY_IN"] = "Canal externo. Documentos (entrante)";
$MESS["CRM_REST_EXTERNAL_CHANNEL_IMPORT_AGENT"] = "Canal externo. Importar compañías o contactos";
$MESS["CRM_REST_EXTERNAL_CHANNEL_IMPORT_AGENT_IN"] = "Canal externo. Importar compañías o contactos (entrante)";
$MESS["CRM_REST_EXTERNAL_CHANNEL_IMPORT_AGENT_LABEL_COMPANY_TITLE"] = " Importar compañía";
$MESS["CRM_REST_EXTERNAL_CHANNEL_IMPORT_AGENT_LABEL_CONTACT_TITLE"] = " Importar contacto";
$MESS["CRM_REST_EXTERNAL_CHANNEL_IMPORT_AGENT_LABEL_URL"] = "Enlace externo";
$MESS["CRM_REST_EXTERNAL_CHANNEL_ACTIVITY_LABEL_TITLE"] = "#SUBJECT# ##NUMBER#";
$MESS["CRM_REST_EXTERNAL_CHANNEL_ACTIVITY_LABEL_TEXT"] = "para #START_TIME# for #RESULT_SUM_CURRENCY#";
$MESS["CRM_REST_EXTERNAL_CHANNEL_ACTIVITY_LABEL_URL"] = "Enlace a documento";
$MESS["CRM_REST_EXTERNAL_CHANNEL_ACTIVITY_LABEL_RESPONSIBLE"] = "Persona responsable:";
$MESS["CRM_REST_EXTERNAL_CHANNEL_ACTIVITY_LABEL_IN"] = "Información de entrada:";
$MESS["CRM_REST_EXTERNAL_CHANNEL_PORTRAIT_ACTIVE"] = "Conectado";
$MESS["CRM_REST_EXTERNAL_CHANNEL_PORTRAIT_INACTIVE"] = "Conectar";
$MESS["CRM_REST_EXTERNAL_CHANNEL"] = "Canal externo";
$MESS["CRM_REST_EXTERNAL_CHANNEL_ACTIVITY_FACE_CARD_EVENT"] = "desde";
$MESS["CRM_REST_EXTERNAL_CHANNEL_ACTIVITY_FACE_CARD_RESULT_PERCENT"] = "Porcentaje de descuento 1C:";
$MESS["CRM_REST_EXTERNAL_CHANNEL_ACTIVITY_FACE_CARD_RESULT_SUM"] = "1C total menos descuento:";
$MESS["CRM_REST_EXTERNAL_CHANNEL_ACTIVITY_FACE_CARD_MANGER"] = "1C persona responsable:";
?>