<?
$MESS["CRM_ACTIVITY_PROVIDER_CALL_LIST_TITLE"] = "Lista de llamadas";
$MESS["CRM_CALL_LIST_NOT_CREATED_ERROR"] = "La lista de llamadas no existe. Para crear la lista, haga clic en el enlace en el área \"Lista de llamadas\".";
$MESS["CRM_CALL_LIST_SUBJECT_EMPTY"] = "Ningún tema de actividad especificado";
$MESS["CRM_CALL_LIST_RESPONSIBLE_IM_NOTIFY"] = "Ahora eres el responsable de la lista de llamadas \"#title#\"";
$MESS["CRM_CALL_LIST_USE_WEBFORM"] = "Usar el formulario";
?>