<?
$MESS["CRM_ENTITY_REGEXP"] = "Resolver entidade usando expressão";
$MESS["CRM_ENTITY_REGEXP_NOTES"] = "(expressão regular; o primeiro grupo de classes deve conter a ID da entidade)";
$MESS["CRM_MAIL_ENTITY"] = "Entidade:";
$MESS["CRM_MAIL_ENTITY_ALL"] = "Qualquer";
$MESS["CRM_MAIL_ENTITY_LEAD"] = "Lead";
$MESS["CRM_MAIL_ENTITY_CONTACT"] = "Contato";
$MESS["CRM_MAIL_ENTITY_COMPANY"] = "Empresa";
$MESS["CRM_MAIL_ENTITY_DEAL"] = "Negócio";
$MESS["CRM_MAIL_MAIL"] = "Endereço de e-mail do CRM";
?>