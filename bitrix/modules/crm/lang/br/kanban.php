<?
$MESS["CRM_KANBAN_ACTIVITY_MY"] = "Atividades";
$MESS["CRM_KANBAN_ACTIVITY_PLAN_CALL"] = "Ligar";
$MESS["CRM_KANBAN_ACTIVITY_PLAN_MEETING"] = "Reunião";
$MESS["CRM_KANBAN_ACTIVITY_PLAN_TASK"] = "Tarefa";
$MESS["CRM_KANBAN_ACTIVITY_PLAN_VISIT"] = "Visita";
$MESS["CRM_KANBAN_NOTIFY_TITLE"] = "Permissões Insuficientes.";
$MESS["CRM_KANBAN_NOTIFY_HEADER"] = "Permissões insuficientes para editar etapas.";
$MESS["CRM_KANBAN_NOTIFY_TEXT"] = "Somente o administrador do seu Bitrix24 pode criar uma nova etapa. Envie uma mensagem ou fale com ele para adicionar as etapas da tarefa que você precisa.";
$MESS["CRM_KANBAN_NOTIFY_BUTTON"] = "Enviar notificação";
$MESS["CRM_KANBAN_ACTIVITY_PLAN"] = "Plano";
$MESS["CRM_KANBAN_ACTIVITY_MORE"] = "Mais";
$MESS["CRM_KANBAN_ACTIVITY_LETSGO_LEAD"] = "Plano de atividades para avançar com o cliente potencial.";
$MESS["CRM_KANBAN_ACTIVITY_LETSGO_DEAL"] = "Plano de atividades para avançar com a negociação.";
$MESS["CRM_KANBAN_NO_EMAIL"] = "Nenhum e-mail";
$MESS["CRM_KANBAN_NO_PHONE"] = "Nenhum telefone";
$MESS["CRM_KANBAN_NO_IM"] = "Nenhuma comunicação de bate-papo";
$MESS["MAIN_KANBAN_COLUMN_TITLE_PLACEHOLDER"] = "Nome";
$MESS["MAIN_KANBAN_DROPZONE_CANCEL"] = "Cancelar";
?>