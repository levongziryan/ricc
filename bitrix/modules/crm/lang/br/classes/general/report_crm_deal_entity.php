<?
$MESS["CRM_DEAL_ENTITY_ID"] = "ID";
$MESS["CRM_DEAL_ENTITY_TITLE"] = "Nome";
$MESS["CRM_DEAL_ENTITY_OPENED"] = "Disponível para todos";
$MESS["CRM_DEAL_ENTITY_PRODUCT_ID"] = "Produto";
$MESS["CRM_DEAL_ENTITY_PRODUCT_BY"] = "Produto";
$MESS["CRM_DEAL_ENTITY_COMMENTS"] = "Comentário";
$MESS["CRM_DEAL_ENTITY_OPPORTUNITY"] = "Total";
$MESS["CRM_DEAL_ENTITY_OPPORTUNITY_ACCOUNT"] = "Montante esperada";
$MESS["CRM_DEAL_ENTITY_RECEIVED_AMOUNT"] = "Montante recebido";
$MESS["CRM_DEAL_ENTITY_LOST_AMOUNT"] = "Montante perdido";
$MESS["CRM_DEAL_ENTITY_COMPANY_BY"] = "Empresa";
$MESS["CRM_DEAL_ENTITY_CONTACT_BY"] = "Contato";
$MESS["CRM_DEAL_ENTITY_LEAD_BY"] = "Lead";
$MESS["CRM_DEAL_ENTITY_STAGE_ID"] = "Fase do negócio";
$MESS["CRM_DEAL_ENTITY_STAGE_BY"] = "Fase do negócio";
$MESS["CRM_DEAL_ENTITY_STATE_ID"] = "Estado";
$MESS["CRM_DEAL_ENTITY_CLOSED"] = "Fechado";
$MESS["CRM_DEAL_ENTITY_CURRENCY_ID"] = "Moeda";
$MESS["CRM_DEAL_ENTITY_CURRENCY_BY"] = "Moeda";
$MESS["CRM_DEAL_ENTITY_ACCOUNT_CURRENCY_ID"] = "ID da moeda";
$MESS["CRM_DEAL_ENTITY_PROBABILITY"] = "Probabilidade, %";
$MESS["CRM_DEAL_ENTITY_TYPE_ID"] = "Tipo";
$MESS["CRM_DEAL_ENTITY_TYPE_BY"] = "Tipo";
$MESS["CRM_DEAL_ENTITY_EVENT_RELATION"] = "Eventos";
$MESS["CRM_DEAL_ENTITY_CLOSEDATE"] = "Suposta data de fechamento";
$MESS["CRM_DEAL_ENTITY_BEGINDATE"] = "Data de início";
$MESS["CRM_DEAL_ENTITY_ASSIGNED_BY_ID"] = "Pessoa responsável";
$MESS["CRM_DEAL_ENTITY_ASSIGNED_BY"] = "Pessoa responsável";
$MESS["CRM_DEAL_ENTITY_DEAL_EVENT"] = "Eventos do negócio";
$MESS["CRM_DEAL_ENTITY_EVENT_DATE"] = "Data do Evento";
$MESS["CRM_DEAL_ENTITY_EVENT_ID"] = "Tipo de evento";
$MESS["CRM_DEAL_ENTITY_EVENT_BY"] = "Tipo de evento";
$MESS["CRM_DEAL_ENTITY_EVENT_DESCRIPTION"] = "Descrição do Evento";
$MESS["CRM_DEAL_ENTITY_CREATED_BY"] = "Criado por";
$MESS["CRM_DEAL_ENTITY_CREATED_BY_ID"] = "Criado por";
$MESS["CRM_DEAL_ENTITY_MODIFY_BY"] = "Modificado por";
$MESS["CRM_DEAL_ENTITY_MODIFY_BY_ID"] = "Modificado por";
$MESS["CRM_DEAL_ENTITY_DATE_CREATE"] = "Criado em";
$MESS["CRM_DEAL_ENTITY_DATE_MODIFY"] = "Modificada em";
$MESS["CRM_DEAL_ENTITY_IS_WON"] = "Ganhos";
$MESS["CRM_DEAL_ENTITY_IS_LOSE"] = "Perdido";
$MESS["CRM_DEAL_ENTITY_IS_WORK"] = "Em andamento";
$MESS["CRM_DEAL_ENTITY_HAS_PRODUCTS"] = "Contém produtos";
$MESS["CRM_DEAL_ENTITY_ORIGINATOR_BY"] = "Vínculo";
?>