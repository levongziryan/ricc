<?
$MESS["CRM_CURRERCY_ERR_DELETION_OF_BASE_CURRENCY"] = "A moeda base não pode ser excluída.";
$MESS["CRM_CURRERCY_ERR_DELETION_OF_ACCOUNTING_CURRENCY"] = "A moeda de referência não pode ser excluída.";
$MESS["CRM_CURRERCY_MODULE_IS_NOT_INSTALLED"] = "O módulo de Moedas não está instalado.";
$MESS["CRM_CURRERCY_MODULE_WARNING"] = "Atenção! O módulo \"Moeda\" é necessário para operação adequada.";
?>