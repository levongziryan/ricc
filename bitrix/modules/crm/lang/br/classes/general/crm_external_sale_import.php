<?
$MESS["CRM_EXT_SALE_IMPORT_EMPTY_ANSW"] = "O servidor retornou uma resposta vazia.";
$MESS["CRM_EXT_SALE_IMPORT_UNKNOWN_ANSW"] = "Resposta do servidor não identificada. Primeiros 100 caracteres:";
$MESS["CRM_EXT_SALE_IMPORT_ERROR_XML"] = "Erro ao analizar a resposta do servidor.";
$MESS["CRM_GCES_NOTIFY_TITLE"] = "Sincronização com \"#NAME#\"";
$MESS["CRM_GCES_NOTIFY_MESSAGE"] = "Sincronização realizada com sucesso, recebeu:<br />#TOTALDEALS# <a href=\"#DEAL_URL#\">negócios</a> (#CREATEDDEALS# novos)<br />#TOTALCONTACTS# <a href=\"#CONTACT_URL#\">contatos</a> (#CREATEDCONTACTS# novos)<br />#TOTALCOMPANIES# <a href=\"#COMPANY_URL#\">empresas</a> (#CREATEDCOMPANIES# novas)";
$MESS["CRM_EXT_SALE_TITLE_SETTINGS"] = "Resultado da importação";
$MESS["CRM_GCES_NOTIFY_ERROR_TITLE"] = "Sincronização com \"#NAME#\"";
$MESS["CRM_GCES_NOTIFY_ERROR_MESSAGE"] = "Erro de sincronização:<br />tentativas de conexão: 10, último: #DATA#<br />conexão foi desativada.<br /><br />Para mais informações, por favor, abra o formulário de conexão com a loja virtual (<a href='#URL#'>CRM &gt; Configurações &gt; conectividade com a loja virtual</a>).";
$MESS["CRM_EXT_SALE_TITLE_ERROR_SETTINGS"] = "Erro de importação";
$MESS["CRM_EXT_SALE_IMPORT_UNKNOWN_ANSW_PERMS"] = "Permissões insuficientes. Por favor, verifique se o usuário tem permissão para exportar o pedido.";
$MESS["CRM_EXT_SALE_IMPORT_UNKNOWN_ANSW_F"] = "Erro ao enviar dados para a loja virtual.";
$MESS["CRM_EXT_SALE_IMPORT_UNKNOWN_ANSW_PERMS1"] = "Permissões insuficientes. Por favor, verifique se o usuário tem permissão para exportar o pedido.";
$MESS["CRM_EXT_SALE_IM_GROUP"] = "grupo";
?>