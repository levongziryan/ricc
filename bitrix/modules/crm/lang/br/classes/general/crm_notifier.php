<?
$MESS["CRM_NOTIFY_SCHEME_ACTIVITY_EMAIL_INCOMING"] = "Há novos e-mails";
$MESS["CRM_NOTIFY_SCHEME_LIVEFEED_MENTION"] = "Você foi mencionado numa publicação";
$MESS["CRM_NOTIFY_SCHEME_LIVEFEED_POST"] = "Você foi especificado como destinatário de uma publicação";
$MESS["CRM_NOTIFY_SCHEME_WEBFORM"] = "Formulário de CRM enviado";
$MESS["CRM_NOTIFY_SCHEME_CALLBACK"] = "Retorno de Chamada solicitado";
?>