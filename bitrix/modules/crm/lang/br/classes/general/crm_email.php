<?
$MESS["CRM_ADD_MESSAGE"] = "Nova mensagem CRM";
$MESS["CRM_EMAIL_GET_EMAIL"] = "Enviar e Salvar Mensagem";
$MESS["CRM_EMAIL_SUBJECT"] = "Título";
$MESS["CRM_EMAIL_EMAILS"] = "E-Mail";
$MESS["CRM_EMAIL_FROM"] = "De";
$MESS["CRM_EMAIL_TO"] = "Para";
$MESS["CRM_MAIL_COMPANY_NAME"] = "Nome da empresa: %TITLE%";
$MESS["CRM_MAIL_LEAD_FROM_EMAIL_TITLE"] = "Lead da mensagem de %SENDER%";
$MESS["CRM_MAIL_LEAD_FROM_EMAIL_SOURCE"] = "criado a partir da mensagem de %SENDER% que não pôde ser relacionada a qualquer lead, contato ou empresa existente.";
$MESS["CRM_EMAIL_CODE_ALLOCATION_BODY"] = "Adicionar ao corpo da mensagem";
$MESS["CRM_EMAIL_CODE_ALLOCATION_SUBJECT"] = "Adicionar ao assunto da mensagem";
$MESS["CRM_EMAIL_CODE_ALLOCATION_NONE"] = "Não adicionar";
$MESS["CRM_EMAIL_BANNENED_ATTACHMENTS"] = "Arquivos não disponíveis (tamanho máximo excedido: %MAX_SIZE% MB)";
$MESS["CRM_EMAIL_BANNENED_ATTACHMENT_INFO"] = "%NAME% (%SIZE% MB)";
$MESS["CRM_MAIL_LEAD_FROM_USER_EMAIL_TITLE"] = "Lead da mensagem encaminhada de %SENDER%";
?>