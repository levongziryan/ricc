<?
$MESS["CRM_MODULE_IS_NOT_INSTALLED"] = "O módulo de CRM não foi encontrado. Por favor, instale este módulo.";
$MESS["CRM_SALE_MODULE_IS_NOT_INSTALLED"] = "O módulo de e-Store não foi encontrado. Por favor, instale este módulo.";
$MESS["CRM_CATALOG_MODULE_IS_NOT_INSTALLED"] = "O módulo de Catálogo Comercial não foi encontrado. Por favor, instale este módulo. ";
$MESS["CRM_VAT_NOT_SELECTED"] = "[não selecionado]";
?>