<?
$MESS["CRM_EVENT_ERR_ENTITY_TYPE"] = "O ID da entidade não está especificado.";
$MESS["CRM_EVENT_ERR_ENTITY_ID"] = "O ID de elemento da entidade não está especificado.";
$MESS["CRM_EVENT_ERR_ENTITY_NAME"] = "O tipo de evento não está especificado.";
$MESS["CRM_EVENT_ERR_ENTITY_DATE_NOT_VALID"] = "A data do evento é inválido.";
$MESS["CRM_EVENT_TYPE_USER"] = "Eventos personalizados";
$MESS["CRM_EVENT_TYPE_CHANGE"] = "Alterações";
$MESS["CRM_EVENT_TYPE_SNS"] = "E-mail recebido";
$MESS["CRM_EVENT_TYPE_VIEW"] = "Visualizar";
$MESS["CRM_EVENT_TYPE_EXPORT"] = "Exportar";
$MESS["CRM_EVENT_TYPE_DELETE"] = "Excluir";
?>