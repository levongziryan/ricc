<?
$MESS["CRM_PRODUCT_CATALOG_TYPE_TITLE"] = "Catálogos CRM";
$MESS["CRM_PRODUCT_CATALOG_TITLE"] = "Catálogo de Produtos do CRM";
$MESS["CRM_PRODUCT_CATALOG_SECTION_NAME"] = "Categoria";
$MESS["CRM_PRODUCT_CATALOG_PRODUCT_NAME"] = "Produto";
$MESS["CRM_ERR_REGISTER_CATALOG"] = "Ocorreu um erro ao registrar o Catálogo Comercial. ";
?>