<?
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_ERROR_FIELD_IS_MISSING"] = "O campo \"%FIELD_NAME%\" é obrigatório.";
$MESS["CRM_ERROR_FIELD_INCORRECT"] = "O campo \"%FIELD_NAME%\" está incorreto.";
$MESS["CRM_FIELD_TITLE"] = "Título";
$MESS["CRM_FIELD_OPPORTUNITY"] = "Renda";
$MESS["CRM_FIELD_EVENT_DATE"] = "Data do Evento";
$MESS["CRM_FIELD_BEGINDATE"] = "Data de início";
$MESS["CRM_FIELD_COMPARE_TITLE"] = "Campo \"Título\" alterado";
$MESS["CRM_FIELD_COMPARE_PRODUCT"] = "O campo \"Produto\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_COMPANY_ID"] = "O campo \"Empresa\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_CONTACT_ID"] = "O campo \"Contato\" alterado.";
$MESS["CRM_FIELD_COMPARE_DEAL_STAGE"] = "O campo \"Fase\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_DEAL_STATE"] = "O campo \"Status\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_DEAL_TYPE"] = "O campo \"Tipo\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_COMMENTS"] = "O campo \"Comentário\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_OPPORTUNITY"] = "O campo \"Rendimento\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_PROBABILITY"] = "O campo \"Probabilidade\"foi alterado.";
$MESS["CRM_FIELD_COMPARE_BEGINDATE"] = "O campo \"Data de Início\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_CLOSEDATE"] = "O campo \"Data de Fechamento\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_EVENT_DATE"] = "O campo \"Data do evento\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_EVENT_ID"] = "O campo \"Tipo de evento\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_EVENT_DESCRIPTION"] = "O campo \"Descrição do evento\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_ASSIGNED_BY_ID"] = "O campo \"Pessoa Responsável\" foi alterado.";
$MESS["CRM_FIELD_COMPARE_EMPTY"] = "-vazio-";
$MESS["CRM_FIELD_COMPARE_CLOSED"] = "O campo \"Fechado\" foi modificado.";
$MESS["CRM_DEAL_FIELD_OPPORTUNITY_INVALID"] = "O campo \"Lucro\" contém caracteres inválidos.";
$MESS["CRM_DEAL_EVENT_ADD"] = "Reparte adicionado";
$MESS["CRM_DEAL_EVENT_UPDATE_STATUS"] = "Status atualizado";
$MESS["CRM_DEAL_EVENT_UPDATE_ASSIGNED_BY"] = "Responsável mudou";
$MESS["CRM_DEAL_EVENT_UPDATE_TITLE"] = "O nome foi alterado";
$MESS["CRM_DEAL_EVENT_UPDATE_CLIENT"] = "Cliente mudou";
$MESS["CRM_DEAL_RESPONSIBLE_IM_NOTIFY"] = "Você agora é responsável pelo negócio \"#title#\"";
$MESS["CRM_DEAL_NOT_RESPONSIBLE_IM_NOTIFY"] = "Você não é mais responsável para o negócio: \"#title#\"";
$MESS["CRM_DEAL_PROGRESS_IM_NOTIFY"] = "Acordo \"#title#\" mudou o status de \"#start_status_title#\" para \"#final_status_title#\"";
$MESS["CRM_DEAL_CREATION_CANCELED"] = "Acordo não foi criado porque a operação foi cancelada pelo manipulador de eventos: \"#NAME#\"";
$MESS["CRM_DEAL_UPDATE_CANCELED"] = "Acordo não foi criado porque a operação foi cancelada pelo manipulador de eventos: \"#NAME#\"";
$MESS["CRM_FIELD_COMPARE_LOCATION_ID"] = "Campo \"Localização\" alterado";
$MESS["CRM_FIELD_CLOSEDATE"] = "Data Planejada para o Fechamento";
$MESS["CRM_DEAL_FIELD_ID"] = "ID";
$MESS["CRM_DEAL_FIELD_TITLE"] = "Nome";
$MESS["CRM_DEAL_FIELD_STAGE_ID"] = "Fase da negociação";
$MESS["CRM_DEAL_FIELD_CURRENCY_ID"] = "Moeda";
$MESS["CRM_DEAL_FIELD_OPPORTUNITY"] = "Total";
$MESS["CRM_DEAL_FIELD_TYPE_ID"] = "Tipo";
$MESS["CRM_DEAL_FIELD_PROBABILITY"] = "Probabilidade";
$MESS["CRM_DEAL_FIELD_BEGINDATE"] = "Data de início";
$MESS["CRM_DEAL_FIELD_CLOSEDATE"] = "Data de término";
$MESS["CRM_DEAL_FIELD_OPENED"] = "Disponível para todos";
$MESS["CRM_DEAL_FIELD_CLOSED"] = "Fechado";
$MESS["CRM_DEAL_FIELD_CONTACT_ID"] = "Contato";
$MESS["CRM_DEAL_FIELD_COMPANY_ID"] = "Empresa";
$MESS["CRM_DEAL_FIELD_ASSIGNED_BY_ID"] = "Pessoa responsável";
$MESS["CRM_DEAL_FIELD_COMMENTS"] = "Observação";
$MESS["CRM_DEAL_FIELD_PRODUCT_ROWS"] = "Produtos";
$MESS["CRM_DEAL_FIELD_QUOTE_ID"] = "Orçamento";
$MESS["CRM_FIELD_COMPARE_CURRENCY"] = "Campo \"Moeda\" alterado";
$MESS["CRM_FIELD_COMPARE_CONTACTS_REMOVED"] = "Excluído link para contatos";
$MESS["CRM_FIELD_COMPARE_CONTACTS_ADDED"] = "Criado link para contatos";
$MESS["CRM_DEAL_FIELD_OPPORTUNITY_INVALID_2"] = "O valor total contém caracteres inválidos. ";
$MESS["CRM_FIELD_COMPARE_TAX_VALUE"] = "Campo \"Imposto\" alterado";
$MESS["CRM_FIELD_COMPARE_DEAL_CATEGORY"] = "Canal alterado";
?>