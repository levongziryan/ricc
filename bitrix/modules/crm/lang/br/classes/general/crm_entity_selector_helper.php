<?
$MESS["CRM_FF_OK"] = "Selecionar";
$MESS["CRM_FF_CANCEL"] = "Cancelar";
$MESS["CRM_FF_CLOSE"] = "Fechar";
$MESS["CRM_FF_WAIT"] = "Pesquisa...";
$MESS["CRM_FF_NO_RESULT"] = "Infelizmente, seu pedido de busca não retornou resultados.";
$MESS["CRM_FF_CHOISE"] = "Adicionar produto";
$MESS["CRM_FF_CHANGE"] = "Editar";
$MESS["CRM_FF_LAST"] = "Recentes";
$MESS["CRM_FF_SEARCH"] = "Pesquisar";
$MESS["CRM_FF_LEAD"] = "Leads";
$MESS["CRM_FF_CONTACT"] = "Contatos";
$MESS["CRM_FF_COMPANY"] = "Empresas";
$MESS["CRM_FF_DEAL"] = "Negócios";
$MESS["CRM_FF_QUOTE"] = "Orçamentos";
$MESS["CRM_ENT_SEL_HLP_PREF_CONTACT_TYPE"] = "Tipo";
$MESS["CRM_ENT_SEL_HLP_PREF_PHONE"] = "Telefone";
$MESS["CRM_ENT_SEL_HLP_PREF_EMAIL"] = "E-mail";
$MESS["CRM_ENT_SEL_HLP_HIDDEN_CONTACT"] = "Contato oculto";
$MESS["CRM_ENT_SEL_HLP_HIDDEN_COMPANY"] = "Empresa oculta";
$MESS["CRM_ENT_SEL_HLP_HIDDEN_LEAD"] = "Cliente potencial oculto";
$MESS["CRM_ENT_SEL_HLP_HIDDEN_DEAL"] = "Negociação oculta";
$MESS["CRM_ENT_SEL_HLP_HIDDEN_QUOTE"] = "Cotação oculta";
?>