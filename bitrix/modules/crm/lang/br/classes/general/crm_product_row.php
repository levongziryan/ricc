<?
$MESS["CRM_EVENT_PROD_ROW_ADD"] = "Produto adicionado";
$MESS["CRM_EVENT_PROD_ROW_UPD"] = "Produto modificado";
$MESS["CRM_EVENT_PROD_ROW_PRICE_UPD"] = "Preço do '#NAME#' atualizado";
$MESS["CRM_EVENT_PROD_ROW_QTY_UPD"] = "Quantidade de #NAME# atualizado";
$MESS["CRM_EVENT_PROD_ROW_REM"] = "Produto excluído";
$MESS["CRM_EVENT_PROD_ROW_NAME_UPD"] = "Nome do produto alterado";
$MESS["CRM_EVENT_PROD_ROW_DISCOUNT_UPD"] = "Desconto para \"#NAME#\" alterado";
$MESS["CRM_EVENT_PROD_ROW_TAX_UPD"] = "Imposto para \"#NAME#\" alterado";
$MESS["CRM_EVENT_PROD_ROW_MEASURE_UPD"] = "Unidade de medida para \"#NAME#\" alterada";
?>