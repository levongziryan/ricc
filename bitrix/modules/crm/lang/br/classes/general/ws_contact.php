<?
$MESS["CRM_ERROR_FIELD_IS_MISSING"] = "O campo \"%FIELD_NAME%\" é obrigatório.";
$MESS["CRM_ERROR_FIELD_INCORRECT"] = "O campo \"%FIELD_NAME%\" está incorreto.";
$MESS["CRM_FIELD_FIND"] = "Pesquisar";
$MESS["CRM_FIELD_NAME"] = "Primeiro nome";
$MESS["CRM_FIELD_LAST_NAME"] = "Sobrenome";
$MESS["CRM_FIELD_SECOND_NAME"] = "Segundo nome";
$MESS["CRM_FIELD_TYPE_ID"] = "Tipo de Contato";
$MESS["CRM_FIELD_PHONE"] = "Telefone";
$MESS["CRM_FIELD_EMAIL"] = "E-mail";
$MESS["CRM_FIELD_WEB"] = "Site";
$MESS["CRM_FIELD_MESSENGER"] = "Messenger";
$MESS["CRM_FIELD_POST"] = "Posição";
$MESS["CRM_FIELD_ADDRESS"] = "Endereço";
$MESS["CRM_FIELD_COMMENTS"] = "Comentário";
$MESS["CRM_FIELD_STATUS_ID"] = "Status";
$MESS["CRM_FIELD_STATUS_DESCRIPTION"] = "Descrição";
$MESS["CRM_FIELD_OPPORTUNITY"] = "Oportunidade";
$MESS["CRM_FIELD_COMPANY_ID"] = "Empresa";
$MESS["CRM_FIELD_SOURCE_ID"] = "Fonte";
$MESS["CRM_FIELD_SOURCE_DESCRIPTION"] = "Descrição";
$MESS["CRM_FIELD_PHOTO"] = "Foto";
?>