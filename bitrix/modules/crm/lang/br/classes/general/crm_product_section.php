<?
$MESS["CRM_PRODUCT_SECTION_ERR_SECTION_NAME_EMPTY"] = "O nome da seção não é especificado.";
$MESS["CRM_PRODUCT_SECTION_ERR_SECTION_NOT_FOUND"] = "A seção não foi encontrada.";
$MESS["CRM_PRODUCT_SECTION_ERR_SECTION_INCLUDES_USED_PRODUCTS"] = "Não foi possível excluir a seção porque ela contém produtos referidos por uma negociação, cliente potencial, oferta ou nota fiscal.";
?>