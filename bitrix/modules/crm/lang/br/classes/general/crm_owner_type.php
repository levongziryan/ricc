<?
$MESS["CRM_OWNER_TYPE_LEAD"] = "Lead";
$MESS["CRM_OWNER_TYPE_DEAL"] = "Negócio";
$MESS["CRM_OWNER_TYPE_CONTACT"] = "Contato";
$MESS["CRM_OWNER_TYPE_COMPANY"] = "Empresa";
$MESS["CRM_OWNER_TYPE_INVOICE"] = "Fatura";
$MESS["CRM_OWNER_TYPE_LEAD_CATEGORY"] = "Leads";
$MESS["CRM_OWNER_TYPE_DEAL_CATEGORY"] = "Negócios";
$MESS["CRM_OWNER_TYPE_CONTACT_CATEGORY"] = "Contatos";
$MESS["CRM_OWNER_TYPE_COMPANY_CATEGORY"] = "Empresas";
$MESS["CRM_OWNER_TYPE_INVOICE_CATEGORY"] = "Faturas";
$MESS["CRM_OWNER_TYPE_QUOTE"] = "Orçamento";
$MESS["CRM_OWNER_TYPE_QUOTE_CATEGORY"] = "Orçamentos";
$MESS["CRM_OWNER_TYPE_REQUISITE"] = "Detalhes";
$MESS["CRM_OWNER_TYPE_REQUISITE_CATEGORY"] = "Detalhes";
$MESS["CRM_OWNER_TYPE_SYSTEM"] = "Sistema";
$MESS["CRM_OWNER_TYPE_DEAL_CATEGORY_CATEGORY"] = "Pipelines de negociação";
$MESS["CRM_OWNER_TYPE_ACTIVITY"] = "Atividade";
$MESS["CRM_OWNER_TYPE_CUSTOM_ACTIVITY_TYPE"] = "Tipo de atividade personalizada";
$MESS["CRM_OWNER_TYPE_CUSTOM_ACTIVITY_TYPE_CATEGORY"] = "Tipos de atividade personalizada";
?>