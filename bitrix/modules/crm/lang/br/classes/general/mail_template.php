<?
$MESS["CRM_MAIL_TEMPLATE_SCOPE_PERSONAL"] = "apenas para mim";
$MESS["CRM_MAIL_TEMPLATE_SCOPE_COMMON"] = "para qualquer pessoa";
$MESS["CRM_MAIL_TEMPLATE_FIELD_OWNER_ID"] = "Dono";
$MESS["CRM_MAIL_TEMPLATE_FIELD_ENTITY_TYPE_ID"] = "Entidade:";
$MESS["CRM_MAIL_TEMPLATE_FIELD_TITLE"] = "Nome";
$MESS["CRM_MAIL_TEMPLATE_ERROR_NOT_EXISTS"] = "Não foi possível encontrar o item com ID# #ID#";
$MESS["CRM_MAIL_TEMPLATE_ERROR_FIELD_NOT_SPECIFIED"] = "O campo \"#FIELD#\" não pode ser vazio.";
?>