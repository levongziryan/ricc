<?
$MESS["CRM_INSTALL_NAME"] = "CRM";
$MESS["CRM_INSTALL_DESCRIPTION"] = "Fornece suporte CRM.";
$MESS["CRM_INSTALL_TITLE"] = "Instalação do módulo CRM";
$MESS["CRM_UNINSTALL_TITLE"] = "Desinstalação do módulo CRM";
$MESS["CRM_SALE_MODULE_NOT_INSTALL"] = "Por favor, instale o módulo de e-Store antes de instalar o CRM. ";
?>