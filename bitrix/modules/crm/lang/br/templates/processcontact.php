<?
$MESS["CRM_BP_TITLE"] = "Processando contato";
$MESS["CRM_BP_TYPE"] = "Processo de negócio sequencial";
$MESS["CRM_BP_TASK_NAME"] = "Processando contato: {=Document:NAME} {=Document:LAST_NAME} (Fornecido pelo processo de negócio)";
$MESS["CRM_BP_TASK_TEXT"] = "Processar o contato: {=Document:NAME} {=Document:LAST_NAME}";
$MESS["CRM_BP_CONTACT_TITLE"] = "Processando contato";
$MESS["CRM_BP_CONTACT_TYPE"] = "Processo comercial sequencial";
$MESS["CRM_BP_CONTACT_TASK_NAME"] = "Processando contato: {=Document:NAME} {=Document:LAST_NAME} (Criado pelo processo comercial)";
$MESS["CRM_BP_CONTACT_TASK_TEXT"] = "Processar o contato: {=Document:NAME} {=Document:LAST_NAME}";
?>