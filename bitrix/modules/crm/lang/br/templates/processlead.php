<?
$MESS["CRM_BP_TITLE"] = "Processar lead";
$MESS["CRM_BP_TYPE"] = "Processo de negócio sequencial";
$MESS["CRM_BP_TASK_NAME"] = "Processar lead: {=Document:TITLE}";
$MESS["CRM_BP_TASK_TEXT"] = "Processar lead: {=Document:TITLE}";
$MESS["CRM_BP_LEAD_TITLE"] = "Processar cliente potencial";
$MESS["CRM_BP_LEAD_TYPE"] = "Processo comercial sequencial";
$MESS["CRM_BP_LEAD_TASK_NAME"] = "Processar cliente potencial: {=Document:TITLE} (definido pelo processo comercial)";
$MESS["CRM_BP_LEAD_TASK_TEXT"] = "Processar cliente potencial: {=Document:TITLE}";
$MESS["CRM_BP_LEAD_LEADTASK_NAME"] = "Processar cliente potencial: {=Document:TITLE} (definido pelo processo comercial)";
?>