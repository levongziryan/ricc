<?
$MESS["CRM_PAGE_TITLE"] = "Ajuda";
$MESS["CRM_PAGE_CONTENT"] = "<h3>CRM em Poucas Palavras</h3>
 
<p><b>O Customer relationship management (CRM)</b>é uma estratégia para gerenciar interações de uma empresa com consumidores, clientes e porspectos de venda. Ela envolve o uso de tecnologia para organizar, automatizar e sincronizar processos de negócio - principamente atividades de venda, mas também para marketing e atendimento ao cliente. O CRM armazena informações de relacionamento com o cliente para análise adicional.</p>
 
<p>O CRM da Bitrix é contruído sob os seguintes conceitos.</p>
 
<ul> 
  <li><b>Contat0</b> é um registro contendo dados de uma pessoa com o qual sua empresa já possui ou possuirá um relacionamento.</li>
 
  <li><b>Empresa</b> é um registro contendo informações sobre a organização com o qual sua empresa já possui ou poderá ter um relacioinamento. </li>
 
  <li><b>Lead</b> é um registro contendo dados de um possível contato para o qual algumas informações (e-mail, telefone) está disponível, mas com quem ainda não houve interação formal.  </li>
 
  <li><b>Evento</b> é qualquer mudança em contatos, leads ou empresas. Por exemplo: adicionar um novo número telefônico. </li>
 
  <li><b>negócio</b> é um registro de uma transação financeira com um cliente ou empresa. </li>
 </ul>
 
<h3>Como funciona?</h3>
 
<p>Dependendo dos seus requisitos de negócios, o <b>CRM</b> pode ser usado:</p>
 
<ol> 
  <li>como um banco de dados para um contato e empresa;</li>
 
  <li>como um sistema clássico de <b>CRM</b>.</li>
 </ol>
 
<h4>1. Usando o CRM como um banco de dados</h4>
 
<p>O sistema CRM pode agir como base para acompanhar relações de negócios. Neste caso, as principais entidades são os Contatos e as Empresas e os Eventos nos quais os relaciona, combinando para formar uma descrção completa do histórico do relacionamento&#65533;. Nenste regime, é possível criar Leads e Eventos que se aplicam a eles para que eles sejam convertidos em Negociações.</p>
 
<p><img height='432' border='0' width='900' src='/upload/crm/cim/01-en.png'  /></p>
 
<h4>2. Sistema clássico de CRM</h4>
 
<p>Se você deseja usar o CRM como um verdadeiro sistema de gerenciamento de relações com o cliente, o ponto inicial seria a criação de um Lead que pode ser adicionado manualmente ou importado do Gerenciador de Site Bitrix. Uma vez criado, o Lead pode ser processado e eventualmente convertido para um contato, uma empresa ou uma negócio. Se o Lead convertido permanece um contato ou uma empresa, ele se torna um registro no banco de dados do tipo do Contato ou da Empresa. Se o Lead se torna uma negócio, então sua passagem pelo funil de vendas é indicado de acordo. </p>
 
<p><img height='454' border='0' width='900' src='/upload/crm/cim/03-en.png'  /></p>";
?>