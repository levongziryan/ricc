<?
$MESS["CRM_ACTIVITY_VISIT_TITLE"] = "Visitar";
$MESS["CRM_ACTIVITY_VISIT_BUTTON_FINISH"] = "Fechar";
$MESS["CRM_ACTIVITY_VISIT_OWNER_SELECT"] = "Selecionar";
$MESS["CRM_ACTIVITY_VISIT_OWNER_CHANGE"] = "Alterar";
$MESS["CRM_ACTIVITY_VISIT_OWNER_TAKE_PICTURE"] = "Tirar uma foto";
$MESS["CRM_ACTIVITY_VISIT_OWNER_RETAKE_PICTURE"] = "Tirar novamente";
$MESS["CRM_ACTIVITY_VISIT_ERROR_MIC_FAILURE"] = "Não é possível acessar o microfone. Descrição do erro:";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_TITLE"] = "Termos de Uso";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_BODY_1"] = "IMPORTANTE! Isso gravará a conversa com seu visitante.";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_BODY_2"] = "Certifique-se de seguir todas as leis que regulamentam a gravação de conversa no seu país.";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_AGREED"] = "Eu aceito os Termos";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_CLOSE"] = "Fechar";
$MESS["CRM_ACTIVITY_VISIT_DEFAULT_CAMERA"] = "Câmera padrão";
$MESS["CRM_ACTIVITY_VISIT_FACEID_AGREEMENT"] = "<div class=\"tracker-agreement-popup-content\">
   <ol class=\"tracker-agreement-popup-list\">
    <li class=\"tracker-agreement-popup-list-item\">
 O FindFace Service (aqui referido como \"FindFace Service\") é fornecido pelo N-TECH.LAB LTD. 
 Os usuários do FindFace Service precisam entrar num acordo com o FindFace Service e devem concordar com os <a href=\"Https://saas.findface.pro/files/docs/license_us.pdf\" class=\"tracker-link\">seguintes termos e condições</a> selecionando \"Concordo\". 
 <br>
 Ao selecionar \"Concordo\", o Usuário está concordando e garantindo que irá cumprir os seguintes requisitos: 
     <ul class=\"tracker-agreement-popup-inner-list\">
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Seguir rigorosamente todas as leis e normas locais que regem o uso da privacidade e dos dados pessoais do país de residência do Usuário. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Seguir rigorosamente todas as leis e normas locais. Esta exigência permanecerá em vigor durante o período total de vigência da utilização do FindFace Service. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
        Obter autorização por escrito ou de outra forma, de acordo com o prescrito pela legislação local de cada país a partir do qual o Usuário está operando. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       O Usuário deve informar assiduamente cada pessoa cujos dados pessoais, incluindo fotos e imagens pessoais que o Usuário pretende processar usando o FindFace Service.
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Procurar aconselhamento jurídico ANTES de utilizar o FindFace Service. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
    Garantir que quaisquer informações carregadas pelo Usuário foram autorizadas pela pessoa e estão em conformidade com as leis e normas da jurisdição do Usuário.
   </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
    Confirmar que o Usuário está utilizando o FindFace Service por sua própria conta e risco e sem garantia de qualquer espécie. 
   </li>   
     </ul>
    </li>
    <li class=\"tracker-agreement-popup-list-item\">
     A Bitrix, Inc, não é de forma alguma responsável pelo Serviço e não apresenta reclamações ou garantias no que diz respeito à exatidão ou à disponibilidade do Serviço, nem fornece qualquer suporte técnico ou consultas sobre o Serviço. 
    </li>
   </ol>
   <div class=\"tracker-agreement-popup-description\">
     A Bitrix, Inc não é de forma alguma responsável pelo FindFace Service e não apresenta reclamações ou garantias do Serviço FindFace.
 Ao aceitar os termos e condições deste contrato de serviço, o Usuário concorda e confirma que nem o FindFace Service, nem a Bitrix, Inc. coleta ou processa quaisquer dados carregados pelo Usuário.
 O Usuário confirma que nem o FindFace Service, nem a Bitrix, Inc. têm qualquer conhecimento das circunstâncias em que o Usuário coletou os dados, fotos ou imagens. 
 O Usuário concorda em manter o FindFace Service e o Bitrix inofensivos para todas e quaisquer ações e omissões do Usuário. 
 <br><br>
 Resolução de Litígios; Arbitragem Vinculativa: Quaisquer litígios, controvérsias, interpretações ou reclamações incluindo reclamações sobre, mas não se limitando a, violação do contrato, qualquer forma de negligência, fraude ou declarações falsas decorrentes de, ou a partir de, ou relacionadas a este contrato devem ser apresentados para a arbitragem final vinculativa. 
   </div>
  </div>";
$MESS["CRM_ACTIVITY_VISIT_FACEID_SOCIAL_SEACH_TITLE"] = "Pesquisar perfil VK";
$MESS["CRM_ACTIVITY_VISIT_FACEID_SOCIAL_SEACH_IN_PROCESS"] = "Pesquisando dados...";
?>