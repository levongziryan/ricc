<?
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "O módulo Blocos de Informação não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CATALOG_MODULE_NOT_INSTALLED"] = "O módulo Catálogo Comercial não está instalado.";
$MESS["SALE_MODULE_NOT_INSTALLED"] = "O módulo e-Store não está instalado.";
$MESS["CRM_EXCH1C_UNKNOWN_XML_ID"] = "O ID catálogo está inválido.";
$MESS["CRM_EXCH1C_AUTH_ERROR"] = "Erro de autenticação: login e/ou senha estão incorretos.";
$MESS["CRM_EXCH1C_PERMISSION_DENIED"] = "Acesso negado.";
$MESS["CRM_EXCH1C_UNKNOWN_COMMAND_TYPE"] = "Comando desconhecido.";
$MESS["CRM_EXCH1C_NOT_ENABLED"] = "Troca de dados com 1C desativado";
?>