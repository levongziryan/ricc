<?
$MESS["CRM_STAT_MGR_REBUILD_STATISTICS"] = "<a id=\"#ID#\" href=\"#URL#\">Atualizar estatísticas</a> para os relatórios mostrarem dados corretos.";
$MESS["CRM_STAT_MGR_REBUILD_STATISTICS_DLG_TITLE"] = "Atualizar estatísticas";
$MESS["CRM_STAT_MGR_REBUILD_STATISTICS_DLG_SUMMARY"] = "Isso irá atualizar os dados estatísticos. Isso pode levar um longo tempo.";
?>