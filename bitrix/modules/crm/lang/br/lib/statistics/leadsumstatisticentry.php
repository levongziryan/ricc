<?
$MESS["CRM_LEAD_SUM_STAT_ENTRY_SLOT_SUM_TOTAL"] = "Valor total";
$MESS["CRM_LEAD_SUM_STAT_ENTRY_TITLE"] = "Clientes Potenciais";
$MESS["CRM_LEAD_SUM_STAT_ENTRY_REBUILD"] = "<a id=\"#ID#\" href=\"#URL#\">Atualize as estatísticas de clientes potenciais</a> para obter relatórios corretos.";
$MESS["CRM_LEAD_SUM_STAT_ENTRY_REBUILD_DLG_TITLE"] = "Atualizar estatísticas de clientes potenciais";
$MESS["CRM_LEAD_SUM_STAT_ENTRY_REBUILD_DLG_SUMMARY"] = "Isso irá atualizar as estatísticas de clientes potenciais. Pode demorar algum tempo.";
?>