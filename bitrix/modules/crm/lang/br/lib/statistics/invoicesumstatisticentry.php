<?
$MESS["CRM_INVOICE_SUM_STAT_ENTRY_SLOT_SUM_TOTAL"] = "Valor total";
$MESS["CRM_INVOICE_SUM_STAT_ENTRY_TITLE"] = "Faturas";
$MESS["CRM_INVOICE_SUM_STAT_ENTRY_REBUILD"] = "<a id=\"#ID#\" href=\"#URL#\">Atualize as estatísticas de faturas</a> para obter relatórios corretos.";
$MESS["CRM_INVOICE_SUM_STAT_ENTRY_REBUILD_DLG_TITLE"] = "Atualizar as estatísticas da faturas";
$MESS["CRM_INVOICE_SUM_STAT_ENTRY_REBUILD_DLG_SUMMARY"] = "Isso irá atualizar as estatísticas de fatura. Pode demorar algum tempo.";
?>