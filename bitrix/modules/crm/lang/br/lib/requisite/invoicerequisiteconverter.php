<?
$MESS["CRM_DEFAULT_TITLE"] = "Sem Título";
$MESS["CRM_INV_RQ_CONV_INTRO"] = "Você não precisa mais inserir os dados da empresa nas suas faturas cada vez que você as cria. Adicione os dados da empresa para seus clientes no CRM uma vez e você poderá utilizá-los quando você trabalhar com vários registros de CRM. Se você inseriu os dados da empresa antes, você pode transferi-los automaticamente agora.  <a id=\"#ID#\" href=\"#URL#\">Transferir</a>.";
?>