<?
$MESS["CRM_CALL_LIST_LIMIT_ERROR"] = "Itens máximos excedidos. Defina o filtro para selecionar não mais do que #LIMIT# itens.";
$MESS["CRM_CALL_LIST_UPDATE"] = "Adicionar à lista de chamada";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_HEADER"] = "Disponível no plano Plus e acima";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_CONTENT"] = "Utilizar listas de chamadas para fazer uma lista de clientes que esperam sua chamada e criar uma tarefa respectiva para a pessoa responsável designada.";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_ITEM_1"] = "Criar lista de chamada em poucos cliques";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_ITEM_2"] = "Ligar para todos os clientes, um a um, numa única janela";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_ITEM_3"] = "Controlar o progresso e os resultados da chamada";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_SHOW_MORE"] = "Saiba mais";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_FOOTER"] = "Listas de chamadas e outros recursos úteis estão disponíveis a partir do plano Plus por apenas \$39/mês.";
$MESS["CRM_CALL_LIST_SUBJECT"] = "Lista de chamadas";
?>