<?
$MESS["CRM_BANKDETAIL_ERR_INVALID_ENTITY_TYPE"] = "Tipo de entidade de dados bancários incorreto";
$MESS["CRM_BANKDETAIL_ERR_INVALID_ENTITY_ID"] = "Nenhuma entidade de dados bancários especificada";
$MESS["CRM_BANKDETAIL_ERR_ON_DELETE"] = "Erro ao excluir dados bancários";
$MESS["CRM_BANKDETAIL_ERR_NOTHING_TO_DELETE"] = "Nenhum dado bancário encontrado para ser excluído";
?>