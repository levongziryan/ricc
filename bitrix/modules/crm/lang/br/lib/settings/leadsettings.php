<?
$MESS["CRM_LEAD_SETTINGS_VIEW_WIDGET"] = "Análise de clientes potenciais";
$MESS["CRM_LEAD_SETTINGS_VIEW_LIST"] = "Lista de clientes potenciais";
$MESS["CRM_TYPE_TITLE"] = "Escolha a maneira que você deseja trabalhar com o seu CRM";
$MESS["CRM_TYPE_SAVE"] = "Salvar";
$MESS["CRM_TYPE_CANCEL"] = "Cancelar";
$MESS["CRM_TYPE_TURN_ON"] = "Ativar";
$MESS["CRM_ROBOTS_TITLE"] = "Modo simples de CRM";
$MESS["CRM_ROBOTS_TEXT"] = "No modo CRM Simples, o Bitrix24 converte automaticamente novos clientes potenciais em vendas usando regras de automação.<br/><br/>
Você já tem regras de automação de clientes potenciais que estão ativas no momento. Ativar este modo irá excluir essas regras.<br/><br/>
Você tem certeza que deseja ativar o modo CRM simples?";
$MESS["CRM_LEAD_SETTINGS_VIEW_KANBAN"] = "Kanban de cliente potencial";
?>