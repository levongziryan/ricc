<?
$MESS["CRM_RECUR_WRONG_ID"] = "ID de conta inválido";
$MESS["CRM_RECUR_ACTIVATE_LIMIT_REPEAT"] = "Erro ao ativar o pedido porque a contagem de repetição máxima foi excedida. Você pode querer aumentar o valor máximo.";
$MESS["CRM_RECUR_ACTIVATE_LIMIT_DATA"] = "Erro ao ativar o pedido porque a última data de fatura recorrente foi alcançada. Você pode querer alterar a data.";
?>