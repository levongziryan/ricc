<?
$MESS["CRM_WEBFORM_ENTITY_FIELD_NAME_TEMPLATE"] = "Preencher formulário de CRM #";
$MESS["CRM_WEBFORM_ENTITY_FIELD_NAME_COMPANY_TEMPLATE"] = "empresa";
$MESS["CRM_WEBFORM_ENTITY_FIELD_NAME_CONTACT_TEMPLATE"] = "indivíduo";
$MESS["CRM_WEBFORM_ENTITY_SCHEME_LEED"] = "Cliente Potencial";
$MESS["CRM_WEBFORM_ENTITY_SCHEME_LEED_DESC"] = "cliente potencial";
$MESS["CRM_WEBFORM_ENTITY_SCHEME_LEED_INVOICE_DESC"] = "cliente potencial, contato ou empresa, fatura";
$MESS["CRM_WEBFORM_ENTITY_SCHEME_DEAL"] = "Negociação";
$MESS["CRM_WEBFORM_ENTITY_SCHEME_DEAL_DESC"] = "negociação, contato ou empresa";
$MESS["CRM_WEBFORM_ENTITY_SCHEME_DEAL_INVOICE_DESC"] = "negociação, contato ou empresa, fatura";
$MESS["CRM_WEBFORM_ENTITY_SCHEME_CLIENT"] = "Cliente";
$MESS["CRM_WEBFORM_ENTITY_SCHEME_CLIENT_DESC"] = "contato ou empresa";
$MESS["CRM_WEBFORM_ENTITY_SCHEME_CLIENT_INVOICE_DESC"] = "contato ou empresa, fatura";
$MESS["CRM_WEBFORM_ENTITY_SCHEME_QUOTE"] = "Cotação";
$MESS["CRM_WEBFORM_ENTITY_SCHEME_QUOTE_DESC"] = "cotação, contato ou empresa";
$MESS["CRM_WEBFORM_ENTITY_SCHEME_QUOTE_INVOICE_DESC"] = "cotação, contato ou empresa, fatura";
?>