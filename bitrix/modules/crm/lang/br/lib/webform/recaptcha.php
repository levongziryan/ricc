<?
$MESS["CRM_WEBFORM_RECAPTCHA_ERROR_MISSING_INPUT_SECRET"] = "reCAPTCHA: a chave secreta não está especificada.";
$MESS["CRM_WEBFORM_RECAPTCHA_ERROR_INVALID_INPUT_SECRET"] = "reCAPTCHA: a chave secreta está incorreta.";
$MESS["CRM_WEBFORM_RECAPTCHA_ERROR_MISSING_INPUT_RESPONSE"] = "reCAPTCHA: não é fornecida resposta.
";
$MESS["CRM_WEBFORM_RECAPTCHA_ERROR_INVALID_INPUT_RESPONSE"] = "A verificação de spam falhou.";
$MESS["CRM_WEBFORM_RECAPTCHA_ERROR_UNKNOWN"] = "Erro desconhecido..";
?>