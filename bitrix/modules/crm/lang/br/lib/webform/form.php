<?
$MESS["CRM_WEBFORM_FORM_ERROR_SCHEME"] = "Esquema de documento inválido.";
$MESS["CRM_WEBFORM_FORM_BUTTON_CAPTION_DEFAULT"] = "Enviar";
$MESS["CRM_WEBFORM_FORM_SCRIPT_BUTTON_TEXT"] = "Nome do botão";
$MESS["CRM_WEBFORM_FORM_COPY_NAME_PREFIX"] = "Copiar";
$MESS["CRM_WEBFORM_FORM_ERROR_CAPTCHA_KEY"] = "Uma chave e uma chave secreta são necessárias para usar reCAPTCHA";
$MESS["CRM_WEBFORM_FORM_PRESET_FIELDS"] = "Nos campos padrão";
?>