<?
$MESS["CRM_WEBFORM_PRESET_ITEM_CD_NAME"] = "Informações de contato";
$MESS["CRM_WEBFORM_PRESET_ITEM_CD_CAPTION"] = "Informações de contato";
$MESS["CRM_WEBFORM_PRESET_ITEM_CD_DESCRIPTION"] = "Deixe suas informações de contato e entraremos em contato com você";
$MESS["CRM_WEBFORM_PRESET_ITEM_FB_NAME"] = "Formulário de feedback";
$MESS["CRM_WEBFORM_PRESET_ITEM_FB_CAPTION"] = "Feedback";
$MESS["CRM_WEBFORM_PRESET_ITEM_FB_DESCRIPTION"] = "Compartilhe seu feedback no campo de comentário";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_RESULT_SUCCESS_TEXT"] = "Obrigado, sua mensagem foi enviada.";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_RESULT_FAILURE_TEXT"] = "Infelizmente, a mensagem não foi enviada, tente novamente mais tarde.";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_FIELD_NAME"] = "Nome";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_FIELD_LAST_NAME"] = "Sobrenome";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_FIELD_PHONE"] = "Telefone";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_FIELD_EMAIL"] = "E-mail";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_FIELD_COMMENTS"] = "Comentário";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_NAME"] = "Fazer chamada de retorno de \"#call_from#\" ";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_CAPTION"] = "Não encontrou o que estava procurando?";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_DESCRIPTION"] = "Ainda tem dúvidas? Ligaremos de volta para você!";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_BUTTON_CAPTION"] = "Ligue-me de volta";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_RESULT_SUCCESS_TEXT"] = "Nosso representante entrará em contato com você em poucos segundos.";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_CALL_TEXT"] = "Foi solicitada uma chamada de retorno; conectando você com o cliente agora.";
?>