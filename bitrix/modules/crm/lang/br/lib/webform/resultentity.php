<?
$MESS["CRM_WEBFORM_RESULT_ENTITY_DC_NONE"] = "Permitir duplicatas";
$MESS["CRM_WEBFORM_RESULT_ENTITY_DC_REPLACE"] = "Substituir";
$MESS["CRM_WEBFORM_RESULT_ENTITY_DC_MERGE"] = "Mesclar";
$MESS["CRM_WEBFORM_RESULT_ENTITY_ACTIVITY_SUBJECT"] = "Preencher formulário de CRM #";
$MESS["CRM_WEBFORM_RESULT_ENTITY_ACTIVITY_RESPONSIBLE_IM_NOTIFY"] = "A atividade \"#title#\" foi atribuída a você";
$MESS["CRM_WEBFORM_RESULT_ENTITY_NOTIFY_SUBJECT"] = "Formulário CRM \"%title%\" enviado";
$MESS["CRM_WEBFORM_RESULT_ENTITY_NOTIFY_SUBJECT_CALL"] = "Retorno de chamada solicitado de \"%phone%\"";
?>