<?
$MESS["CRM_CONTACT_MERGER_COLLISION_READ_PERMISSION"] = "#USER_NAME# combinou com um contato \"#SEED_TITLE#\" [#SEED_ID#] com \"#TARG_TITLE#\" [#TARG_ID#] que você não pode ver devido às preferências de permissão de acesso.";
$MESS["CRM_CONTACT_MERGER_COLLISION_UPDATE_PERMISSION"] = "#USER_NAME# combinou com um contato \"#SEED_TITLE#\" [#SEED_ID#] com \"#TARG_TITLE#\" [#TARG_ID#] que você não pode editar devido às preferências de permissão de acesso.";
$MESS["CRM_CONTACT_MERGER_COLLISION_READ_UPDATE_PERMISSION"] = "#USER_NAME# combinou com um contato \"#SEED_TITLE#\" [#SEED_ID#] com \"#TARG_TITLE#\" [#TARG_ID#] que você não pode ver ou editar devido às preferências de permissão de acesso.";
?>