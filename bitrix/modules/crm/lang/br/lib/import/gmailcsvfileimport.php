<?
$MESS["CRM_IMPORT_GMAIL_ERROR_FIELDS_NOT_FOUND"] = "Nenhum dos seguintes campos foram encontrados: #FIELD_LIST#";
$MESS["CRM_IMPORT_GMAIL_REQUIREMENTS"] = "Um arquivo deve seguir estes requisitos para serem importados com sucesso: Condificação: UTF-16; linguagem: Inglês; separador de campo: vírgula.";
?>