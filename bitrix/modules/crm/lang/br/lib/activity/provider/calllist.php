<?
$MESS["CRM_ACTIVITY_PROVIDER_CALL_LIST_TITLE"] = "Lista de Chamadas";
$MESS["CRM_CALL_LIST_NOT_CREATED_ERROR"] = "A lista de chamadas não existe. Para criar a lista, clique no link na área \"Lista de chamada\".";
$MESS["CRM_CALL_LIST_SUBJECT_EMPTY"] = "Nenhum assunto de atividade especificado";
$MESS["CRM_CALL_LIST_RESPONSIBLE_IM_NOTIFY"] = "Você é, agora, responsável pela lista de chamada \"#title#\"";
$MESS["CRM_CALL_LIST_USE_WEBFORM"] = "Utilizar formulário";
?>