<?
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_MO"] = "Seg";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_TU"] = "Ter";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_WE"] = "Qua";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_TH"] = "Qui";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_FR"] = "Sex";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_SA"] = "Sáb";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_SU"] = "Dom";
$MESS["CRM_SITE_BUTTON_WORK_TIME_ACTIONS_HIDE"] = "ocultar botão de retorno de chamada";
$MESS["CRM_SITE_BUTTON_WORK_TIME_ACTIONS_TEXT"] = "salvar informações do cliente e mostrar uma mensagem";
$MESS["CRM_SITE_BUTTON_WORK_TIME_ACTIONS_TEXT_CALLBACK"] = "Infelizmente, no momento, não podemos retornar sua ligação. Entraremos em contato logo que possível, dentro do horário comercial.";
?>