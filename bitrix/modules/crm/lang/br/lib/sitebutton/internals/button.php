<?
$MESS["CRM_BUTTON_LOCATION_TOP_LEFT"] = "Superior esquerdo";
$MESS["CRM_BUTTON_LOCATION_TOP_MIDDLE"] = "Centro superior";
$MESS["CRM_BUTTON_LOCATION_TOP_RIGHT"] = "Superior direito";
$MESS["CRM_BUTTON_LOCATION_BOTTOM_RIGHT"] = "Inferior direito";
$MESS["CRM_BUTTON_LOCATION_BOTTOM_MIDDLE"] = "Centro inferior";
$MESS["CRM_BUTTON_LOCATION_BOTTOM_LEFT"] = "Inferior esquerdo";
$MESS["CRM_BUTTON_FIELD_NAME_EMPTY"] = "Nome do botão";
$MESS["CRM_BUTTON_FIELD_NAME_TITLE"] = "Nome do widget";
?>