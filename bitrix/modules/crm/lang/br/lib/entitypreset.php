<?
$MESS["CRM_ENTITY_TYPE_REQUISITE"] = "Dados";
$MESS["CRM_ENTITY_TYPE_REQUISITE_DESC"] = "Modelos para detalhes de contato ou empresa";
$MESS["CRM_ENTITY_PRESET_ERR_DELETE_PRESET_USED"] = "O modelo não pode ser excluído porque há dados criados nele.";
$MESS["CRM_ENTITY_PRESET_ERR_PRESET_NOT_FOUND"] = "O modelo não foi encontrado.";
$MESS["CRM_ENTITY_PRESET_ERR_INVALID_ENTITY_TYPE"] = "Tipo de entidade inválido para uso com o modelo.";
$MESS["CRM_ENTITY_PRESET_ERR_DELETE_PRESET_DEF_FOR_COMPANY"] = "Você não pode excluir este modelo porque é um modelo padrão da empresa.";
$MESS["CRM_ENTITY_PRESET_ERR_DELETE_PRESET_DEF_FOR_CONTACT"] = "Você não pode excluir este modelo porque é um modelo padrão de contato.";
$MESS["CRM_ENTITY_PRESET_NAME_EMPTY"] = "Modelo sem título";
?>