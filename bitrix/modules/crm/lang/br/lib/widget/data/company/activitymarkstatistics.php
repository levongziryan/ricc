<?
$MESS["CRM_COMPANY_ACTIVITY_MARK_STAT_NONE_QTY"] = "Número de atividades não avaliadas";
$MESS["CRM_COMPANY_ACTIVITY_MARK_STAT_NEGATIVE_QTY"] = "Número de atividades avaliadas negativamente";
$MESS["CRM_COMPANY_ACTIVITY_MARK_STAT_POSITIVE_QTY"] = "Número de atividades avaliadas positivamente";
$MESS["CRM_COMPANY_ACTIVITY_MARK_STAT_GROUP_BY_SOURCE"] = "Fonte de atividade";
$MESS["CRM_COMPANY_ACTIVITY_MARK_STAT_GROUP_BY_MARK"] = "Avaliação de atividade";
$MESS["CRM_COMPANY_ACTIVITY_MARK_STAT_TOTAL"] = "Número de atividades (por avaliações)";
?>