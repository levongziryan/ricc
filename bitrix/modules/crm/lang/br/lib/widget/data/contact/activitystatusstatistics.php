<?
$MESS["CRM_CONTACT_ACTIVITY_STATUS_STAT_TOTAL"] = "Número de solicitações";
$MESS["CRM_CONTACT_ACTIVITY_STATUS_STAT_ANSWERED_QTY"] = "Número de solicitações atendidas";
$MESS["CRM_CONTACT_ACTIVITY_STATUS_STAT_UNANSWERED_QTY"] = "Número de solicitações não atendidas";
$MESS["CRM_CONTACT_ACTIVITY_STATUS_STAT_GROUP_BY_STATUS"] = "Status da atividade";
?>