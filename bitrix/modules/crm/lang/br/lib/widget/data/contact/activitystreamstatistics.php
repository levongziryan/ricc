<?
$MESS["CRM_CONTACT_ACTIVITY_STREAM_STAT_TOTAL"] = "Número de chamadas (por fluxo)";
$MESS["CRM_CONTACT_ACTIVITY_STREAM_STAT_INCOMING_QTY"] = "Número de chamadas recebidas";
$MESS["CRM_CONTACT_ACTIVITY_STREAM_STAT_OUTGOING_QTY"] = "Número de chamadas feitas";
$MESS["CRM_CONTACT_ACTIVITY_STREAM_STAT_REVERSING_QTY"] = "Número de chamadas de retorno";
$MESS["CRM_CONTACT_ACTIVITY_STREAM_STAT_MISSING_QTY"] = "Número de chamadas perdidas";
$MESS["CRM_CONTACT_ACTIVITY_STREAM_STAT_GROUP_BY_STREAM"] = "Fluxo de pedido";
?>