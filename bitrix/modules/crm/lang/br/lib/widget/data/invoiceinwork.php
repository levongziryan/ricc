<?
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_COUNT"] = "Número de faturas ativas";
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_COUNT_SHORT"] = "Número de faturas";
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_SUM"] = "Valor total das faturas ativas";
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_SUM_SHORT"] = "Valor total das faturas";
$MESS["CRM_INVOICE_IN_WORK_CATEGORY"] = "Faturas Ativas";
$MESS["CRM_INVOICE_OWED_CATEGORY"] = "Faturas Não Pagas";
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_COUNT_OWED"] = "Número de faturas não pagas";
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_SUM_OWED"] = "Valor total de faturas não pagas";
?>