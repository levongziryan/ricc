<?
$MESS["CRM_MANAGER_CNTR_SUCCEED"] = "Concluída";
$MESS["CRM_MANAGER_CNTR_FAILED"] = "Restantes";
$MESS["CRM_MANAGER_CNTR_LEAD"] = "Clientes potenciais";
$MESS["CRM_MANAGER_CNTR_CONTACT"] = "Contatos";
$MESS["CRM_MANAGER_CNTR_COMPANY"] = "Empresas";
$MESS["CRM_MANAGER_CNTR_DEAL"] = "Negociações";
$MESS["CRM_MANAGER_CNTR_TITLE"] = "Contadores do gerente";
$MESS["CRM_MANAGER_CNTR_VIDEO"] = "<div style=\"width:400px;height:100px;font-size:15px;\">Use seu contador pessoal para avaliar seu desempenho: o CRM mantém controle sobre suas tarefas concluídas e restantes para hoje. Seu objetivo é não ter nenhuma tarefa inacabada no final do dia.</div>";
?>