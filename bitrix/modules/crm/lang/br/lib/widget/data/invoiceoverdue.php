<?
$MESS["CRM_INVOICE_OVERDUE_PRESET_OVERALL_COUNT"] = "Número de faturas vencidas";
$MESS["CRM_INVOICE_OVERDUE_PRESET_OVERALL_SUM"] = "Valor total das faturas vencidas";
$MESS["CRM_INVOICE_OVERDUE_PRESET_OVERALL_COUNT_SHORT"] = "Número de faturas";
$MESS["CRM_INVOICE_OVERDUE_PRESET_OVERALL_SUM_SHORT"] = "Valor total das faturas";
$MESS["CRM_INVOICE_OVERDUE_CATEGORY"] = "Faturas vencidas";
?>