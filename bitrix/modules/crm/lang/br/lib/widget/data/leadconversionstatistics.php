<?
$MESS["CRM_LEAD_CONV_STAT_PRESET_OVERALL_COUNT"] = "Número de clientes potenciais convertidos";
$MESS["CRM_LEAD_CONV_STAT_PRESET_CONTACT_COUNT"] = "Número de contatos obtidos a partir de clientes potenciais";
$MESS["CRM_LEAD_CONV_STAT_PRESET_COMPANY_COUNT"] = "Número de empresas obtidas a partir de clientes potenciais";
$MESS["CRM_LEAD_CONV_STAT_PRESET_DEAL_COUNT"] = "Número de vendas obtidas a partir de clientes potenciais";
$MESS["CRM_LEAD_CONV_STAT_PRESET_OVERALL_SUM"] = "Valor total de clientes potenciais convertidos";
$MESS["CRM_LEAD_CONV_STAT_PRESET_OVERALL_COUNT_SHORT"] = "Número de clientes potenciais";
$MESS["CRM_LEAD_CONV_STAT_PRESET_OVERALL_SUM_SHORT"] = "Valor total de clientes potenciais";
$MESS["CRM_LEAD_CONV_CATEGORY"] = "Clientes potenciais convertidos";
?>