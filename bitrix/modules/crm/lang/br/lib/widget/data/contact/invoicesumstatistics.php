<?
$MESS["CRM_CONTACT_INVOICE_SUM_STAT_PRESET_PROCESS_COUNT"] = "Faturas em andamento";
$MESS["CRM_CONTACT_INVOICE_SUM_STAT_PRESET_SUCCESS_COUNT"] = "Faturas pagas";
$MESS["CRM_CONTACT_INVOICE_SUM_STAT_PRESET_PAID_INTIME_COUNT"] = "Faturas pagas no prazo";
$MESS["CRM_CONTACT_INVOICE_SUM_STAT_PRESET_PROCESS_SUM"] = "Total de faturas em andamento";
$MESS["CRM_CONTACT_INVOICE_SUM_STAT_PRESET_SUCCESS_SUM"] = "Total de faturas pagas";
$MESS["CRM_CONTACT_INVOICE_SUM_STAT_PRESET_OVERALL_SUM"] = "Total de faturas ativas";
$MESS["CRM_CONTACT_INVOICE_SUM_STAT_PRESET_OVERALL_COUNT"] = "Faturas ativas";
?>