<?
$MESS["CRM_RESTR_MGR_DUP_CTRL_MSG_CONTENT"] = "<div class=\"crm-duplicate-tab-content\">
<h3 class=\"crm-duplicate-tab-title\">Pesquisa avançada de duplicatas</h3>
<div class=\"crm-duplicate-tab-text\">
Quando um novo contato é criado (ou cliente potencial ou empresa), o Bitrix24 localiza e mostra possíveis duplicatas &mdash; impedindo antecipadamente a criação de duplicatas.
</div>
<div class=\"crm-duplicate-tab-text\">
Na pesquisa avançada de duplicatas, o CRM também pode encontrar duplicatas em dados importados e em dados já inseridos no banco de dados. Essas duplicatas podem ser associadas. 
</div>
<div class=\"crm-duplicate-tab-text\">
Adicione estes e outros excelentes recursos ao seu Bitrix24! Telefonia Avançada + CRM Avançado e outros recursos úteis estão disponíveis a partir do plano Bitrix24 Plus por apenas R\$ 99/mês. 
<a target=\"_blank\" href=\"https://www.bitrix24.com/pro/crm.php\">Saiba mais</a>
</div>
<div class=\"crm-history-tab-buttons\">
<span class=\"webform-buttom webform-button-create\" onclick=\"#LICENSE_LIST_SCRIPT#\">Obtenha um plano estendido</span>
<span class=\"webform-button webform-button-transparent\" onclick=\"#DEMO_lICENSE_SCRIPT#\">Obtenha um teste gratuito de 30 dias</span>
</div>
</div>
";
$MESS["CRM_RESTR_MGR_HX_VIEW_MSG_CONTENT"] = "<div class=\"crm-history-tab-content\">
	 <h3 class=\"crm-history-tab-title\">A mudança de histórico de CRM está disponível apenas no CRM avançado</h3>
	 <div class=\"crm-history-tab-text\">
		 O Bitrix24 mantém um registro detalhado de todas as mudanças de CRM. Quando você faz upgrade para o CRM avançado, você verá o histórico de mudanças (ou seja, quem acessou ou alterou a entrada de CRM) e poderá restabelecer valores anteriores, se necessário.
	</div>
	 <div class=\"crm-history-tab-text\">Este é o seu aspecto:</div>
	 <img alt=\"Tab History\" class=\"crm-history-tab-img\" src=\"/bitrix/js/crm/images/history-en.png\"/>
	 <div class=\"crm-history-tab-text\">
		 Adicione estes e outros excelentes recursos ao seu Bitrix24! Telefonia Avançada + CRM Avançado e outros recursos úteis estão disponíveis a partir do plano Bitrix24 Plus por apenas R\$ 99/mês. 
	<a target=\"_blank\" href=\"https://www.bitrix24.com/pro/crm.php\">Saiba mais</a>
	 </div>
	 <div class=\"crm-history-tab-buttons\">
		 <span class=\"webform-button webform-button-create\" onclick=\"#LICENSE_LIST_SCRIPT#\">Ir para o plano estendido</span>
		 <span class=\"webform-button webform-button-transparent\" onclick=\"#DEMO_lICENSE_SCRIPT#\">Grátis por 30 dias</span>
	 </div>
 </div>";
$MESS["CRM_RESTR_MGR_POPUP_TITLE"] = "CRM Avançado no Bitrix24";
$MESS["CRM_RESTR_MGR_POPUP_CONTENT"] = "Adicione o seguinte ao seu CRM:
<ul class=\"hide-features-list\">
 <li class=\"hide-features-list-item>Conversão entre ofertas, faturas e cotações</li>
 <li class=\"hide-features-list-item\">Pesquisa expandida de duplicatas</li>
 <li class=\"hide-features-list-item\">Mude o histórico para reversão e recuperação de CRM</li>
 <li class=\"hide-features-list-item\">Log de acesso de CRM</li>
 <li class=\"hide-features-list-item\">Visualização de mais de 5000 registros no CRM</li>
 <li class=\"hide-features-list-item\">Chamada acionada pela lista<sup class=\"hide-features-soon\">em breve</sup></li>
 <li class=\"hide-features-list-item\">Volume de e-mails para clientes<sup class=\"hide-features-soon\">em breve</sup></li>
 <li class=\"hide-features-list-item\">Suporte para vendas de serviços<sup class=\"hide-features-soon\">em breve</sup>
 <a target=\"_blank\" class=\"hide-features-more\" href=\"https://www.bitrix24.com/pro/crm.php\">Saiba mais</a>
 </li>
 </ul>
 <strong>Telefonia avançada + CRM Avançado e outros recursos úteis estão disponíveis a partir do plano Bitrix24 Plus por apenas R\$ 99/mês.</strong>

";
$MESS["CRM_RESTR_MGR_DEAL_CATEGORY_POPUP_TITLE"] = "Pipelines Múltiplos de CRM";
$MESS["CRM_RESTR_MGR_DEAL_CATEGORY_POPUP_CONTENT"] = "<div class=\"crm-deal-category-tab-content\">
 <div class=\"crm-deal-category-tab-text\">
  Os planos Bitrix24 Gratuito e Plus estão limitados a um único pipeline/canal de vendas. Para adicionar o segundo pipeline de negociação, faça o upgrade para o plano Bitrix24 Standard. Para desbloquear pipelines e canais de vendas ilimitados, faça o upgrade para o plano Bitrix24 Professional.
 </div>
</div>";
?>