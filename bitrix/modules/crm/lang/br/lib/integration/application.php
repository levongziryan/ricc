<?
$MESS["WS_CRMINTEGRATION_APP_TITLE"] = "CRM";
$MESS["WS_CRMINTEGRATION_APP_DESC"] = "Quando trabalhar com o CRM, você pode configurar troca de dados com outros programas. Escolha uma das opções de conexão e clique em \"Obter Senha\". ";
$MESS["WS_CRMINTEGRATION_APP_OPTIONS_CAPTION"] = "Conectar o CRM";
$MESS["WS_CRMINTEGRATION_APP_OPTIONS_TITLE_SALE"] = "Integração com e-store";
?>