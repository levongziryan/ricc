<?
$MESS["CRM_INSTALL_NAME"] = "CRM";
$MESS["CRM_TOP_LINKS_ITEM_NAME"] = "CRM";
$MESS["CRM_INSTALL_DESCRIPTION"] = "Bietet CRM-Unterstützung.";
$MESS["CRM_INSTALL_TITLE"] = "Installation des CRM-Moduls";
$MESS["CRM_UNINSTALL_TITLE"] = "Deinstallation des CRM-Moduls";
$MESS["CRM_PAGE_FUNNEL"] = "Sales Funnel";
$MESS["CRM_GADGET_MY_LEAD_TITLE"] = "Meine Leads";
$MESS["CRM_GADGET_NEW_LEAD_TITLE"] = "Neue Leads";
$MESS["CRM_GADGET_CLOSED_DEAL_TITLE"] = "Geschlossene Aufträge";
$MESS["CRM_GADGET_LAST_EVENT_TITLE"] = "Jüngste Aktivitäten";
$MESS["CRM_GADGET_NEW_CONTACT_TITLE"] = "Neue Kontakte";
$MESS["CRM_GADGET_NEW_COMPANY_TITLE"] = "Neue Unternehmen";
$MESS["CRM_PERM_R"] = "Seitenansicht";
$MESS["CRM_PERM_W"] = "Seiten erstellen und bearbeiten";
$MESS["CRM_PERM_D"] = "Zugriff verweigern";
$MESS["CRM_PERM_X"] = "Seiten im visuellen Editor erstellen und bearbeiten";
$MESS["CRM_PERM_Y"] = "Seiten und History löschen";
$MESS["CRM_PERM_Z"] = "Zugriffsrechte einstellen";
$MESS["CRM_CONTACT_TYPE_SHARE"] = "Allgemeine Kontakte";
$MESS["CRM_CONTACT_TYPE_JOURNALIST"] = "Journalisten";
$MESS["CRM_CONTACT_TYPE_CLIENT"] = "Kunden";
$MESS["CRM_CONTACT_TYPE_SUPPLIER"] = "Lieferanten";
$MESS["CRM_CONTACT_TYPE_PARTNER"] = "Partner";
$MESS["CRM_STATUS_TYPE_SOURCE_SELF"] = "Persönlicher Kontakt";
$MESS["CRM_STATUS_TYPE_SOURCE_PARTNER"] = "Kunde";
$MESS["CRM_STATUS_TYPE_SOURCE_CALL"] = "Anruf";
$MESS["CRM_STATUS_TYPE_SOURCE_WEB"] = "Website";
$MESS["CRM_STATUS_TYPE_SOURCE_WEB_FORM"] = "Web-Formular";
$MESS["CRM_STATUS_TYPE_SOURCE_EMAIL"] = "E-Mail";
$MESS["CRM_STATUS_TYPE_SOURCE_CONFERENCE"] = "Konferenz";
$MESS["CRM_STATUS_TYPE_SOURCE_TRADE_SHOW"] = "Messe/Ausstellung";
$MESS["CRM_STATUS_TYPE_SOURCE_EMPLOYEE"] = "Mitarbeiter";
$MESS["CRM_STATUS_TYPE_SOURCE_COMPANY"] = "Kampagne";
$MESS["CRM_STATUS_TYPE_SOURCE_HR"] = "Personalabteilung";
$MESS["CRM_STATUS_TYPE_SOURCE_MAIL"] = "E-Mail-Nachricht";
$MESS["CRM_STATUS_TYPE_SOURCE_OTHER"] = "Sonstiges";
$MESS["CRM_COMPANY_TYPE_CUSTOMER"] = "Kunde";
$MESS["CRM_COMPANY_TYPE_PARTNER"] = "Partner";
$MESS["CRM_COMPANY_TYPE_RESELLER"] = "Reseller";
$MESS["CRM_COMPANY_TYPE_COMPETITOR"] = "Konkurrent";
$MESS["CRM_COMPANY_TYPE_INVESTOR"] = "Investor";
$MESS["CRM_COMPANY_TYPE_INTEGRATOR"] = "Integrator";
$MESS["CRM_COMPANY_TYPE_PROSPECT"] = "Interessent";
$MESS["CRM_COMPANY_TYPE_PRESS"] = "Medien";
$MESS["CRM_COMPANY_TYPE_OTHER"] = "Sonstiges";
$MESS["CRM_INDUSTRY_IT"] = "Informationstechnologie";
$MESS["CRM_INDUSTRY_TELECOM"] = "Telekommunikation";
$MESS["CRM_INDUSTRY_MANUFACTURING"] = "Produktion";
$MESS["CRM_INDUSTRY_BANKING"] = "Bankdienstleistungen";
$MESS["CRM_INDUSTRY_CONSULTING"] = "Consulting";
$MESS["CRM_INDUSTRY_FINANCE"] = "Finanzen";
$MESS["CRM_INDUSTRY_GOVERNMENT"] = "Regierung";
$MESS["CRM_INDUSTRY_DELIVERY"] = "Lieferdienst";
$MESS["CRM_INDUSTRY_ENTERTAINMENT"] = "Unterhaltung";
$MESS["CRM_INDUSTRY_NOTPROFIT"] = "Gemeinnützig";
$MESS["CRM_INDUSTRY_OTHER"] = "Sonstiges";
$MESS["CRM_DEAL_TYPE_SALE"] = "Vertrieb";
$MESS["CRM_DEAL_TYPE_COMPLEX"] = "Integrierter Vertrieb";
$MESS["CRM_DEAL_TYPE_GOODS"] = "Warenverkauf";
$MESS["CRM_DEAL_TYPE_SERVICES"] = "Dienstleistungen";
$MESS["CRM_DEAL_TYPE_SERVICE"] = "Kundendienst";
$MESS["CRM_DEAL_STATE_PLANNED"] = "Geplant";
$MESS["CRM_DEAL_STATE_PROCESS"] = "In Arbeit";
$MESS["CRM_DEAL_STATE_COMPLETE"] = "Abgeschlossen";
$MESS["CRM_DEAL_STATE_CANCELED"] = "Storniert";
$MESS["CRM_EVENT_TYPE_INFO"] = "Information";
$MESS["CRM_EVENT_TYPE_MESSAGE"] = "E-Mail wurde gesendet";
$MESS["CRM_EVENT_TYPE_PHONE"] = "Telefonanruf";
$MESS["CRM_UF_NAME"] = "CRM-Elemente";
$MESS["CRM_UF_NAME_CAL"] = "CRM-Elemente";
$MESS["CRM_UF_NAME_LF_TYPE"] = "Code des CRM-Elements für Activity Stream";
$MESS["CRM_UF_NAME_LF_ID"] = "ID des CRM-Elements für Activity Stream";
$MESS["CRM_ROLE_ADMIN"] = "Voller Zugriff";
$MESS["CRM_ROLE_DIRECTOR"] = "Geschäftsführer";
$MESS["CRM_ROLE_CHIF"] = "Abteilungsleiter";
$MESS["CRM_ROLE_MAN"] = "Manager";
$MESS["CRM_SALE_STATUS_A"] = "Genehmigt";
$MESS["CRM_SALE_STATUS_D"] = "Abgelehnt";
$MESS["CRM_SALE_STATUS_P"] = "Abgeschlossen";
$MESS["CRM_SALE_STATUS_S"] = "An Kunden gesendet";
$MESS["CRM_VAT_1"] = "Ohne MwSt.";
$MESS["CRM_VAT_2"] = "MwSt. 19%";
$MESS["CRM_ORD_PROP_2"] = "Standort";
$MESS["CRM_ORD_PROP_21"] = "Stadt";
$MESS["CRM_ORD_PROP_4"] = "PLZ";
$MESS["CRM_ORD_PROP_5"] = "Lieferungsadresse";
$MESS["CRM_ORD_PROP_6"] = "Vor- und Nachname";
$MESS["CRM_ORD_PROP_7"] = "Geschäftsadresse";
$MESS["CRM_ORD_PROP_8"] = "Unternehmensname";
$MESS["CRM_ORD_PROP_9"] = "Telefon";
$MESS["CRM_ORD_PROP_10"] = "Ansprechpartner";
$MESS["CRM_ORD_PROP_11"] = "Fax";
$MESS["CRM_ORD_PROP_12"] = "Lieferungsadresse";
$MESS["CRM_ORD_PROP_13"] = "Steuerzahler-ID";
$MESS["CRM_ORD_PROP_14"] = "Zusätzlich";
$MESS["CRM_ORD_PROP_GROUP_FIZ1"] = "Persönliche Daten";
$MESS["CRM_ORD_PROP_GROUP_FIZ2"] = "Daten für die Lieferung";
$MESS["CRM_ORD_PROP_GROUP_UR1"] = "Unternehmensdaten";
$MESS["CRM_ORD_PROP_GROUP_UR2"] = "Kontaktinformation";
$MESS["CRM_EMAIL_CONFIRM_TYPE_NAME"] = "E-Mail-Adresse des Absenders bestätigen";
$MESS["CRM_EMAIL_CONFIRM_TYPE_DESC"] = "

#EMAIL# - E-Mail-Adresse, die bestätigt werden soll
#CONFIRM_CODE# - Bestätigungscode
";
$MESS["CRM_EMAIL_CONFIRM_EVENT_NAME"] = "E-Mail-Adresse bestätigen";
$MESS["CRM_EMAIL_CONFIRM_EVENT_DESC"] = "<span style=\"font-size:16px;line-height:20px;\">
Geben Sie diesen Bestätigungscode in Ihrem Bitrix24 ein, um Ihre E-Mail-Adresse zu bestätigen.<br>

<span style=\"font-size:24px;line-height:70px;\"><b>#CONFIRM_CODE#</b></span><br>

<span style=\"color:#808080;\">
Warum soll ich meine E-Mail-Adresse verifizieren?<br><br>
<span style=\"font-size:14px;\">Es ist erforderlich, dass Sie Ihre E-Mail-Adresse bestätigen, damit jegliche Manipulationen ausgeschlossen bleiben und damit sichergestellt wird, dass Sie die E-Mail-Adresse, vor der Mails gesendet werden, tatsächlich besitzen.</span>
</span>";
?>
