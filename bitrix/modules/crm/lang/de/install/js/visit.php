<?
$MESS["CRM_ACTIVITY_VISIT_TITLE"] = "Besuch";
$MESS["CRM_ACTIVITY_VISIT_BUTTON_FINISH"] = "Abschließen";
$MESS["CRM_ACTIVITY_VISIT_OWNER_SELECT"] = "Auswählen";
$MESS["CRM_ACTIVITY_VISIT_OWNER_CHANGE"] = "Ändern";
$MESS["CRM_ACTIVITY_VISIT_OWNER_TAKE_PICTURE"] = "Ein Foto machen";
$MESS["CRM_ACTIVITY_VISIT_OWNER_RETAKE_PICTURE"] = "Erneut machen";
$MESS["CRM_ACTIVITY_VISIT_ERROR_MIC_FAILURE"] = "Kein Zugriff auf Mikrofon. Fehlerbeschreibung:";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_TITLE"] = "Nutzungsbedingungen";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_BODY_1"] = "WICHTIG! Die Konversation mit Ihrem Besucher wird aufgezeichnet.";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_BODY_2"] = "Stellen Sie sicher, dass dabei alle rechtlichen Bestimmungen Ihres Landes erfüllt werden.";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_AGREED"] = "Ich akzeptiere Nutzungsbedingungen";
$MESS["CRM_ACTIVITY_VISIT_CONSENT_CLOSE"] = "Schließen";
$MESS["CRM_ACTIVITY_VISIT_DEFAULT_CAMERA"] = "Standardkamera";
$MESS["CRM_ACTIVITY_VISIT_FACEID_SOCIAL_SEACH_TITLE"] = "VK Profile durchsuchen";
$MESS["CRM_ACTIVITY_VISIT_FACEID_SOCIAL_SEACH_IN_PROCESS"] = "Nach Daten wird gesucht...";
$MESS["CRM_ACTIVITY_VISIT_FACEID_AGREEMENT"] = "<div class=\"tracker-agreement-popup-content\">
   <ol class=\"tracker-agreement-popup-list\">
    <li class=\"tracker-agreement-popup-list-item\">
 Der Service FindFace (weiterhin \"FindFace Service\") wird von N-TECH.LAB LTD bereitgestellt. 
 Nutzer des FindFace Service müssen eine Vereinbarung mit dem FindFace Service treffen und <a href=\"https://saas.findface.pro/files/docs/license_us.pdf\" class=\"tracker-link\">folgende Bedingungen</a> akzeptieren, indem sie \"Ich akzeptiere\" auswählen. 
 <br>
 Mit der Auswahl  von \"Ich akzeptiere\" erklärt sich der Nutzer bereit, folgende Anforderungen zu erfüllen: 
     <ul class=\"tracker-agreement-popup-inner-list\">
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Alle lokalen Gesetze und Regelungen einzuhalten, die den Datenschutz im Land des Nutzers bestimmen. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Alle lokalen Gesetze und Regelungen einzuhalten. Diese Anforderung gilt innerhalb der ganzen Zeit der Nutzung vom FindFace Service. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Eine schriftliche Erlaubnis bzw. Ein anderes schriftliches Dokument zu erhalten, welches lokaler Gesetzgebung des Nutzerlandes entspricht. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Der Nutzer muss jede Person informieren, deren persönliche Daten inkl. Fotos oder Bilder der Nutzer durch Nutzung vom FindFace Service bearbeiten wird.
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Sich rechtlich beraten zu lassen, BEVOR der FindFace Service genutzt wird.
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
    Zu garantieren, dass alle Informationen, die der Nutzer hochladen wird, von Personen, auf die sie sich beziehen, erlaubt sind und lokaler Gesetzgebung des Nutzerlandes entsprechen.
   </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
    Zu garantieren, dass der Nutzer den FindFace Service auf eigene Gefahr nutzt. 
   </li>   
     </ul>
    </li>
    <li class=\"tracker-agreement-popup-list-item\">
     Bitrix, Inc. ist für den Service nicht verantwortlich, er übernimmt weder die Beantwortung von etwaigen Beschwerden noch den technischen Support oder Beratung bezüglich der Arbeit des Services. 
    </li>
   </ol>
   <div class=\"tracker-agreement-popup-description\">
    Bitrix, Inc. ist für den Service nicht verantwortlich und garantiert dessen Arbeit nicht.
 Mit dem Akzeptieren dieser Bedingungen erklärt sich der Nutzer darüber informiert zu sein, dass der FindFace Service, nicht Bitrix, Inc. die Daten, welche der Nutzer hochlädt, sammelt und bearbeitet.
 Der Nutzer bestätigt und akzeptiert, dass der FindFace Service, nicht Bitrix, Inc. darüber weiß, unter welchen Umständen der Nutzer die Daten, Fotos oder Bilder gesammelt hat. 
 Der Nutzer erklärt sich bereit, den FindFace Service und Bitrix für alle Handlungen und Nichthandlungen des Nutzer schadlos zu halten. 
 <br><br>
 Streitbeilegung; Bindendes Schiedsverfahren: Alle Streitigkeiten, Meinungsverschiedenheiten oder Ansprüche aus oder im Zusammenhang mit diesem Vertrag, beliebige Fahrlässigkeit, Betrug oder Falschdarstellung im Zusammenhang mit dieser Vereinbarung müssen in einem verbindlichen Schiedsgericht geregelt. 
   </div>
  </div>
";
?>