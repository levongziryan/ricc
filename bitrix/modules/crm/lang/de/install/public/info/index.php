<?
$MESS["CRM_PAGE_TITLE"] = "Hilfe";
$MESS["CRM_PAGE_CONTENT"] = "<h3>Was ist CRM?</h3>
 
<p><b>Customer-Relationship-Management (dt. Kundenbeziehungsmanagement) </b> (oder <b>CRM</b>) bezeichnet die konsequente Ausrichtung einer Unternehmung auf ihre Kunden und die systematische Gestaltung der Kundenbeziehungs-Prozesse.  </p>
 
<p>Folgende Begriffe sind für das CRM wichtig:</p>
 
<ul> 
  <li><b>Kontakt</b> ist ein Eintrag über die Person, mit der ein erster Kontakt geknüpft wurde. </li>
 
  <li><b>Unternehmen</b> ist ein Eintrag über das Unternehmen, mit dem die Zusammenarbeit besteht oder bestehen kann. </li>
 
  <li><b>Lead</b> ist ein Eintrag über die erfolgreiche Kontaktanbahnung zu einem potenziellen Interessenten. </li>
 
  <li><b>Aktivität</b> ist eine beliebige Änderung in den Einträgen zum Kontakt, Lead oder Unternehmen. Es kann beispielsweise eine neue E-Mail-Adresse sein. </li>
 
  <li><b>Auftrag</b> ist ein Eintrag über die Arbeit mit dem Kunden oder Unternehmen bezüglich des Produktverkaufs. </li>
 </ul>
 
<h3>Wie funktioniert das CRM?</h3>
 
<p><b>CRM</b> kann entweder</p>
 
<ol> 
  <li>als <b>eine Datenbank</b> für Kontakte und Unternehmen</li>
 
  <li>oder als eigentliches <b>CRM</b> benutzt werden</li>
 </ol>
 
<h4>1. CRM als Datenbank</h4>
 
<p>Das CRM als eine Datenbank für Kontakte und Unternehmen ermöglicht es, eine komplette Kundenhistory zu führen. Die wichtigsten Einheiten sind dann Kontakt und Unternehmen, in denen mithilfe von Aktivitäten die Kundenhistory geführt und nachverfolgt wird. Auch bei einer solchen Nutzung des CRM ist es möglich, dass manche Aktivitäten zur Erstellung von Leads führen, welche wiederum zu einem Auftrag konvertiert werden können.</p>
 
<p><img height='432' border='0' width='900' src='/upload/crm/cim/01-de.png'  /></p>
 
<h4>2. Eigentliches CRM</h4>
 
<p>Die wichtigste Einheit im eigentlichen CRM ist ein Lead, welcher zum System entweder manuell oder automatisch aus dem &quot;Bitrix Site Manager&quot; hinzugefügt wird. Nachdem der Lead bearbeitet wird, kann er zu einem Kontakt, zu einem Unternehmen oder auch zu einem Auftrag konvertiert werden. In den zwei ersten Fällen wird der Lead zu einem Datenbankbestandteil. Im letzteren Fall, wenn alle Phasen des Sales Funnels abgearbeitet werden, kann der Lead zu einem wirklichen Verkauf werden.</p>
 
<p><img height='454' border='0' width='900' src='/upload/crm/cim/03-de.png'  /></p>
";
?>