<?
$MESS["CRM_KANBAN_NOTIFY_TITLE"] = "Sie haben nicht genügend Rechte.";
$MESS["CRM_KANBAN_NOTIFY_HEADER"] = "Sie haben nicht genügend Rechte, um Phasen zu bearbeiten.";
$MESS["CRM_KANBAN_NOTIFY_TEXT"] = "Nur Administratoren Ihres Bitrix24 können eine neue Phase erstellen. Senden Sie ihnen eine Nachricht oder sprechen Sie sie an, damit sie die erforderlichen Phasen hinzufügen.";
$MESS["CRM_KANBAN_NOTIFY_BUTTON"] = "Nachricht senden";
$MESS["CRM_KANBAN_ACTIVITY_MY"] = "Aktivitäten";
$MESS["CRM_KANBAN_ACTIVITY_PLAN"] = "Planen";
$MESS["CRM_KANBAN_ACTIVITY_PLAN_CALL"] = "Anruf";
$MESS["CRM_KANBAN_ACTIVITY_PLAN_MEETING"] = "Besprechung";
$MESS["CRM_KANBAN_ACTIVITY_PLAN_VISIT"] = "Besuch";
$MESS["CRM_KANBAN_ACTIVITY_PLAN_TASK"] = "Aufgabe";
$MESS["CRM_KANBAN_ACTIVITY_MORE"] = "Mehr";
$MESS["CRM_KANBAN_ACTIVITY_LETSGO_LEAD"] = "Aktivitäten planen, um den Lead nach vorne zu verschieben.";
$MESS["CRM_KANBAN_ACTIVITY_LETSGO_DEAL"] = "Aktivitäten planen, um den Auftrag nach vorne zu verschieben.";
$MESS["CRM_KANBAN_NO_EMAIL"] = "Keine E-Mail";
$MESS["CRM_KANBAN_NO_PHONE"] = "Kein Telefon";
$MESS["CRM_KANBAN_NO_IM"] = "Keine Kommunikation im Chat";
$MESS["MAIN_KANBAN_COLUMN_TITLE_PLACEHOLDER"] = "Name";
$MESS["MAIN_KANBAN_DROPZONE_CANCEL"] = "Abbrechen";
$MESS["CRM_KANBAN_ACTIVITY_TO_PLAN"] = "Planen";
$MESS["CRM_KANBAN_ACTIVITY_CHANGE_LEAD"] = "Sie haben keine geplanten Aktivitäten. Verschieben Sie einen Leadstatus, <span class=\"crm-kanban-item-activity-link\">planen Sie eine Aktivität</span> oder initiieren Sie ein Warten.";
$MESS["CRM_KANBAN_ACTIVITY_CHANGE_DEAL"] = "Sie haben keine geplanten Aktivitäten. Verschieben Sie eine Auftragsphase, <span class=\"crm-kanban-item-activity-link\">planen Sie eine Aktivitäty</span> oder initiieren Sie ein Warten.";
?>