<?
$MESS["CRM_ENTITY_REGEXP"] = "Die Einheit mit Hilfe des Ausdrucks beschreiben";
$MESS["CRM_ENTITY_REGEXP_NOTES"] = "(reguläre Ausdrücke; in den ersten Klammern muss die Einheit-ID stehen)";
$MESS["CRM_MAIL_ENTITY"] = "Einheit:";
$MESS["CRM_MAIL_ENTITY_ALL"] = "Beliebige";
$MESS["CRM_MAIL_ENTITY_LEAD"] = "Lead";
$MESS["CRM_MAIL_ENTITY_CONTACT"] = "Kontakt";
$MESS["CRM_MAIL_ENTITY_COMPANY"] = "Unternehmen";
$MESS["CRM_MAIL_ENTITY_DEAL"] = "Auftrag";
$MESS["CRM_MAIL_MAIL"] = "E-Mail-Adresse des CRM";
?>