<?
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "Das Modul Informationsblöcke ist nicht installiert.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Das Modul CRM ist nicht installiert.";
$MESS["CATALOG_MODULE_NOT_INSTALLED"] = "Das Modul Kommerzieller Katalog ist nicht installiert.";
$MESS["SALE_MODULE_NOT_INSTALLED"] = "Das Modul Online-Shop ist nicht installiert.";
$MESS["CRM_EXCH1C_UNKNOWN_XML_ID"] = "Die Katalog-ID ist ungültig.";
$MESS["CRM_EXCH1C_AUTH_ERROR"] = "Autorisierungsfehler: Login und/oder Passwort sind nicht korrekt.";
$MESS["CRM_EXCH1C_PERMISSION_DENIED"] = "Zugriff verweigert";
$MESS["CRM_EXCH1C_UNKNOWN_COMMAND_TYPE"] = "Unbekannter Befehl";
$MESS["CRM_EXCH1C_NOT_ENABLED"] = "Datenaustausch mit 1C wurde deaktiviert.";
?>