<?
$MESS["CRM_BP_CONTACT_TITLE"] = "Kontakt wird verarbeitet";
$MESS["CRM_BP_CONTACT_TYPE"] = "Regelmäßiger Geschäftsprozess";
$MESS["CRM_BP_CONTACT_TASK_NAME"] = "Kontakt wird verarbeitet: {=Document:NAME} {=Document:LAST_NAME} (Aus dem Geschäftsprozess)";
$MESS["CRM_BP_CONTACT_TASK_TEXT"] = "Kontakt verarbeiten: {=Document:NAME} {=Document:LAST_NAME}";
?>