<?
$MESS["CRM_BP_SHOP_TITLE"] = "Auftrag im E-Shop";
$MESS["CRM_BP_SHOP_PBP"] = "Regelmäßiger Geschäftsprozess";
$MESS["CRM_BP_SHOP_US"] = "Bedingung";
$MESS["CRM_BP_SHOP_10000"] = "Im E-Shop wurde ein Auftrag erstellt. Innerhalb von der nächsten Stunde muss der Kontakt zum Kunden aufgenommen werden. Auftragsbetrag ist über 10.000 {=Document:ACCOUNT_CURRENCY_ID}.";
$MESS["CRM_BP_SHOP_SS"] = "Nachricht des sozialen Netzwerks";
$MESS["CRM_BP_SHOP_10000_1"] = "Im E-Shop wurde ein neuer Auftrag erstellt.";
$MESS["CRM_BP_SHOP_US_10000"] = "10000";
?>