<?
$MESS["CRM_BP_DEAL_TITLE"] = "Ausführung in Phasen";
$MESS["CRM_BP_DEAL_PROPERTIES_1"] = "Geschäftsprozesse mit Status";
$MESS["CRM_BP_DEAL_PROPERTIES_1_TITLE"] = "Status: Neu (Analyse)";
$MESS["CRM_BP_DEAL_PROPERTIES_2"] = "Eigenschaften";
$MESS["CRM_BP_DEAL_PROPERTIES_2_TITLE"] = "Angebot";
$MESS["CRM_BP_DEAL_PROPERTIES_3_TITLE"] = "Status definieren";
$MESS["CRM_BP_DEAL_PROPERTIES_4_TITLE"] = "Status bearbeiten";
$MESS["CRM_BP_DEAL_PROPERTIES_5_TITLE"] = "Dokument bearbeiten";
$MESS["CRM_BP_DEAL_PROPERTIES_6_TITLE"] = "Bearbeiten";
$MESS["CRM_BP_DEAL_PROPERTIES_7_TITLE"] = "Status: Angebot eingereicht";
$MESS["CRM_BP_DEAL_PROPERTIES_8_TITLE"] = "Angebot";
$MESS["CRM_BP_DEAL_PROPERTIES_10_TITLE"] = "Auftrag nicht erhalten";
$MESS["CRM_BP_DEAL_PROPERTIES_11_TITLE"] = "Rechnung";
$MESS["CRM_BP_DEAL_PROPERTIES_13_TITLE"] = "Status: Auftrag nicht erhalten";
$MESS["CRM_BP_DEAL_PROPERTIES_14_TITLE"] = "Angebot";
$MESS["CRM_BP_DEAL_PROPERTIES_15_TITLE"] = "Status: Rechnung";
$MESS["CRM_BP_DEAL_PROPERTIES_16_TITLE"] = "Erfolgreich abgeschlossen";
$MESS["CRM_BP_DEAL_PROPERTIES_17_TITLE"] = "Status: Erfolgreich abgeschlossen";
$MESS["CRM_BP_DEAL_PROPERTIES_18_TITLE"] = "PHP-Code";
$MESS["CRM_BP_DEAL_PROPERTIES_19_TITLE"] = "Verkauf";
$MESS["CRM_BP_DEAL_PROPERTIES_20_TITLE"] = "Zeitpunkt zum Starten der Aufgabe";
$MESS["CRM_BP_DEAL_TASK_NAME"] = "Produktverkauf gemäß Auftrag: {=Document:TITLE}  (Aus dem Geschäftsprozess)";
$MESS["CRM_BP_DEAL_TASK_TEXT"] = "Produktverkauf planen gemäß Auftrag: {=Document:TITLE}";
?>