<?
$MESS["CRM_VCARD_ERR_READ"] = "Ein Fehler beim Lesen von vCard";
$MESS["CRM_VCARD_ERR_FORMAT"] = "Ein Formatfehler in der vCard-Datei";
$MESS["CRM_VCARD_ERR_TMP_FILE"] = "Ein Fehler bei der Erstellung einer temporären vCard-Datei";
$MESS["CRM_VCARD_ERR_VERSION"] = "Die vCard-Version kann nicht identifiziert werden";
?>