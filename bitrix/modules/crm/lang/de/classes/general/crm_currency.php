<?
$MESS["CRM_CURRERCY_ERR_DELETION_OF_BASE_CURRENCY"] = "Die Basiswährung kann nicht gelöscht werden.";
$MESS["CRM_CURRERCY_ERR_DELETION_OF_ACCOUNTING_CURRENCY"] = "Die Währung im Bericht kann nicht gelöscht.";
$MESS["CRM_CURRERCY_MODULE_IS_NOT_INSTALLED"] = "Das Modul Währungen ist nicht installiert.";
$MESS["CRM_CURRERCY_MODULE_WARNING"] = "Wichtig: Für eine korrekte Funktionsweise muss das Modul \"Währung\" installiert werden.";
?>