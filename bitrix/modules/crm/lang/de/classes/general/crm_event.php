<?
$MESS["CRM_EVENT_ERR_ENTITY_TYPE"] = "Die Einheit-ID ist nicht angegeben.";
$MESS["CRM_EVENT_ERR_ENTITY_ID"] = "Die ID des Einheitselements ist nicht angegeben.";
$MESS["CRM_EVENT_ERR_ENTITY_NAME"] = "Der Aktivitätstyp ist nicht angegeben.";
$MESS["CRM_EVENT_ERR_ENTITY_DATE_NOT_VALID"] = "Das Ereignisdatum ist nicht korrekt.";
$MESS["CRM_EVENT_TYPE_USER"] = "Benutzerdefinierte Ereignisse";
$MESS["CRM_EVENT_TYPE_CHANGE"] = "Änderungen";
$MESS["CRM_EVENT_TYPE_SNS"] = "E-Mail";
$MESS["CRM_EVENT_TYPE_VIEW"] = "Ansicht";
$MESS["CRM_EVENT_TYPE_EXPORT"] = "Export";
$MESS["CRM_EVENT_TYPE_DELETE"] = "Löschen";
?>