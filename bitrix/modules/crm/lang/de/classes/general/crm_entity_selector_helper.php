<?
$MESS["CRM_FF_OK"] = "Auswählen";
$MESS["CRM_FF_CANCEL"] = "Abbrechen";
$MESS["CRM_FF_CLOSE"] = "Schließen";
$MESS["CRM_FF_WAIT"] = "Suchen...";
$MESS["CRM_FF_NO_RESULT"] = "Leider wurden keine Suchergebnisse auf Ihre Suchanfrage gefunden.";
$MESS["CRM_FF_CHOISE"] = "Auswählen";
$MESS["CRM_FF_CHANGE"] = "Bearbeiten";
$MESS["CRM_FF_LAST"] = "Letzte";
$MESS["CRM_FF_SEARCH"] = "Suchen";
$MESS["CRM_FF_LEAD"] = "Leads";
$MESS["CRM_FF_CONTACT"] = "Kontakte";
$MESS["CRM_FF_COMPANY"] = "Unternehmen";
$MESS["CRM_FF_DEAL"] = "Aufträge";
$MESS["CRM_FF_QUOTE"] = "Angebote";
$MESS["CRM_ENT_SEL_HLP_PREF_CONTACT_TYPE"] = "Typ";
$MESS["CRM_ENT_SEL_HLP_PREF_PHONE"] = "Telefon";
$MESS["CRM_ENT_SEL_HLP_PREF_EMAIL"] = "E-Mail";
$MESS["CRM_ENT_SEL_HLP_HIDDEN_CONTACT"] = "Ausgeblendeter Kontakt";
$MESS["CRM_ENT_SEL_HLP_HIDDEN_COMPANY"] = "Ausgeblendetes Unternehmen";
$MESS["CRM_ENT_SEL_HLP_HIDDEN_LEAD"] = "Ausgeblendeter Lead";
$MESS["CRM_ENT_SEL_HLP_HIDDEN_DEAL"] = "Ausgeblendeter Lead";
$MESS["CRM_ENT_SEL_HLP_HIDDEN_QUOTE"] = "Ausgeblendetes Angebot";
?>