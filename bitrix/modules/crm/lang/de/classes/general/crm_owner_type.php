<?
$MESS["CRM_OWNER_TYPE_LEAD"] = "Lead";
$MESS["CRM_OWNER_TYPE_DEAL"] = "Auftrag";
$MESS["CRM_OWNER_TYPE_CONTACT"] = "Kontakt";
$MESS["CRM_OWNER_TYPE_COMPANY"] = "Unternehmen";
$MESS["CRM_OWNER_TYPE_INVOICE"] = "Rechnung";
$MESS["CRM_OWNER_TYPE_LEAD_CATEGORY"] = "Leads";
$MESS["CRM_OWNER_TYPE_DEAL_CATEGORY"] = "Aufträge";
$MESS["CRM_OWNER_TYPE_CONTACT_CATEGORY"] = "Kontakte";
$MESS["CRM_OWNER_TYPE_COMPANY_CATEGORY"] = "Unternehmen";
$MESS["CRM_OWNER_TYPE_INVOICE_CATEGORY"] = "Rechnungen";
$MESS["CRM_OWNER_TYPE_QUOTE"] = "Angebot";
$MESS["CRM_OWNER_TYPE_QUOTE_CATEGORY"] = "Angebote";
$MESS["CRM_OWNER_TYPE_REQUISITE"] = "Informationen";
$MESS["CRM_OWNER_TYPE_REQUISITE_CATEGORY"] = "Informationen";
$MESS["CRM_OWNER_TYPE_SYSTEM"] = "System";
$MESS["CRM_OWNER_TYPE_DEAL_CATEGORY_CATEGORY"] = "Auftragspipeline";
$MESS["CRM_OWNER_TYPE_ACTIVITY"] = "Aktivität";
$MESS["CRM_OWNER_TYPE_CUSTOM_ACTIVITY_TYPE"] = "Benutzerdefinierter Aktivitätstyp";
$MESS["CRM_OWNER_TYPE_CUSTOM_ACTIVITY_TYPE_CATEGORY"] = "Benutzerdefinierte Aktivitätstypen";
$MESS["CRM_OWNER_TYPE_RECURRING_DEAL"] = "Auftragsvorlage";
?>