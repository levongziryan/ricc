<?
$MESS["CRM_ADD_MESSAGE"] = "Eine neue CRM-Nachricht";
$MESS["CRM_EMAIL_GET_EMAIL"] = "Send&Save Nachricht";
$MESS["CRM_EMAIL_SUBJECT"] = "Überschrift";
$MESS["CRM_EMAIL_EMAILS"] = "E-Mail";
$MESS["CRM_EMAIL_FROM"] = "Von";
$MESS["CRM_EMAIL_TO"] = "An";
$MESS["CRM_MAIL_COMPANY_NAME"] = "Unternehmens: %TITLE%";
$MESS["CRM_MAIL_LEAD_FROM_EMAIL_TITLE"] = "Lead aus einer Nachricht von %SENDER%";
$MESS["CRM_MAIL_LEAD_FROM_EMAIL_SOURCE"] = "erstellt aus einer Nachricht von %SENDER%, welche nicht zu irgendeinem existierenden Lead, Kontakt oder Unternehmen zugeordnet werden konnte.";
$MESS["CRM_EMAIL_CODE_ALLOCATION_BODY"] = "Zum Nachrichtentext hinzufügen";
$MESS["CRM_EMAIL_CODE_ALLOCATION_SUBJECT"] = "Zum Nachrichtenbetreff hinzufügen";
$MESS["CRM_EMAIL_CODE_ALLOCATION_NONE"] = "Nicht hinzufügen";
$MESS["CRM_EMAIL_BANNENED_ATTACHMENTS"] = "Nicht verfügbare Dateien (Größenlimit überschritten: %MAX_SIZE% MB)";
$MESS["CRM_EMAIL_BANNENED_ATTACHMENT_INFO"] = "%NAME% (%SIZE% MB)";
$MESS["CRM_MAIL_LEAD_FROM_USER_EMAIL_TITLE"] = "Lead aus der Nachricht wurde weitergeleitet von %SENDER%";
$MESS["CRM_EMAIL_DEFAULT_SUBJECT"] = "(ohne Betreff)";
$MESS["CRM_EMAIL_BAD_RESP_QUEUE"] = "In der Warteschlange zur Verteilung der E-Mails für das Mail-Konto \"#EMAIL#\" ist ein gekündigter Mitarbeiter angegeben. Er wird keine E-Mails aus neuen Quellen erhalten, aber alle existierenden E-Mails werden weiterhin an diesen Mitarbeiter weitergeleitet. <a href=\"#CONFIG_URL#\">Para meter der Warteschlange konfigurieren</a>.";
?>