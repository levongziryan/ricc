<?
$MESS["CRM_ERROR_FIELD_IS_MISSING"] = "Das Feld \"%FIELD_NAME%\" ist erforderlich.";
$MESS["CRM_ERROR_FIELD_INCORRECT"] = "Das Feld \"%FIELD_NAME%\" ist erforderlich.";
$MESS["CRM_FIELD_FIND"] = "Suchen";
$MESS["CRM_FIELD_REST_NAME"] = "Vorname";
$MESS["CRM_FIELD_LAST_NAME"] = "Nachname";
$MESS["CRM_FIELD_SECOND_NAME"] = "Zweiter Vorname";
$MESS["CRM_FIELD_TITLE"] = "Überschrift";
$MESS["CRM_FIELD_PHONE"] = "Telefon";
$MESS["CRM_FIELD_EMAIL"] = "E-Mail";
$MESS["CRM_FIELD_WEB"] = "Website";
$MESS["CRM_FIELD_MESSENGER"] = "Messenger";
$MESS["CRM_FIELD_POST"] = "Position";
$MESS["CRM_FIELD_ADDRESS"] = "Adresse";
$MESS["CRM_FIELD_COMMENTS"] = "Kommentar";
$MESS["CRM_FIELD_STATUS_ID"] = "Status";
$MESS["CRM_FIELD_STATUS_DESCRIPTION"] = "Statusbeschreibung";
$MESS["CRM_FIELD_OPPORTUNITY"] = "Möglicher Gewinn";
$MESS["CRM_FIELD_COMPANY_ID"] = "Unternehmen";
$MESS["CRM_FIELD_SOURCE_ID"] = "Quelle";
$MESS["CRM_FIELD_SOURCE_DESCRIPTION"] = "Quellenbeschreibung";
$MESS["CRM_FIELD_PRODUCT_ID"] = "Produkt";
$MESS["CRM_FIELD_COMPANY_TITLE"] = "Unternehmensname";
$MESS["CRM_FIELD_CURRENCY_ID"] = "Währung";
?>