<?
$MESS["CRM_PRODUCT_SECTION_ERR_SECTION_NAME_EMPTY"] = "Der Bereichsname ist nicht angegeben.";
$MESS["CRM_PRODUCT_SECTION_ERR_SECTION_NOT_FOUND"] = "Bereich wurde nicht gefunden.";
$MESS["CRM_PRODUCT_SECTION_ERR_SECTION_INCLUDES_USED_PRODUCTS"] = "Der Bereich kann nicht gelöscht werden, weil er Produkte enthält, welche in einem Auftrag, Lead, Unternehmen, Angebot oder einer Rechnung vorkommen.";
?>