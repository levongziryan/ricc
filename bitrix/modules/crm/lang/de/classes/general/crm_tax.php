<?
$MESS["CRM_MODULE_IS_NOT_INSTALLED"] = "Das Modul CRM wurde nicht gefunden. Installieren Sie bitte das Modul CRM.";
$MESS["CRM_SALE_MODULE_IS_NOT_INSTALLED"] = "Das Modul Onlineshop wurde nicht gefunden. Installieren Sie bitte das Modul Onlineshop.";
$MESS["CRM_CATALOG_MODULE_IS_NOT_INSTALLED"] = "Das Modul Kommerzieller Katalog wurde nicht gefunden. Installieren Sie bitte das Modul Kommerzieller Katalog.";
$MESS["CRM_VAT_NOT_SELECTED"] = "[nicht definiert]";
?>