<?
$MESS["CRM_PRODUCT_CATALOG_TYPE_TITLE"] = "CRM-Kataloge";
$MESS["CRM_PRODUCT_CATALOG_TITLE"] = "CRM-Produktkatalog";
$MESS["CRM_PRODUCT_CATALOG_SECTION_NAME"] = "Kategorie";
$MESS["CRM_PRODUCT_CATALOG_PRODUCT_NAME"] = "Produkt";
$MESS["CRM_ERR_REGISTER_CATALOG"] = "Bei Registrierung des kommerziellen Katalogs ist ein Fehler aufgetreten.";
?>