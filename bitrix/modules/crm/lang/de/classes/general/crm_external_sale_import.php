<?
$MESS["CRM_EXT_SALE_IMPORT_EMPTY_ANSW"] = "Der Proxy-Server hat eine leere Antwort gesendet";
$MESS["CRM_EXT_SALE_IMPORT_UNKNOWN_ANSW"] = "Die Antwort vom Server konnte nicht erkannt werden. Die ersten 100 Zeichen: ";
$MESS["CRM_EXT_SALE_IMPORT_ERROR_XML"] = "Fehler bei Verarbeitung der Antwort vom Server";
$MESS["CRM_GCES_NOTIFY_TITLE"] = "Synchronisierung mit \"#NAME#\"";
$MESS["CRM_GCES_NOTIFY_MESSAGE"] = "Synchronisierung wurde erfolgreich abgeschlossen. Erhalten wurden:<br /><a href=\"#DEAL_URL#\">Aufträge</a> #TOTALDEALS# (neue #CREATEDDEALS#)<br /><a href=\"#CONTACT_URL#\">Kontakte</a> #TOTALCONTACTS# (neue #CREATEDCONTACTS#)<br /><a href=\"#COMPANY_URL#\">Unternehmen</a> #TOTALCOMPANIES# (neue #CREATEDCOMPANIES#)";
$MESS["CRM_EXT_SALE_TITLE_SETTINGS"] = "Importergebnis";
$MESS["CRM_GCES_NOTIFY_ERROR_TITLE"] = "Synchronisierung mit \"#NAME#\"";
$MESS["CRM_GCES_NOTIFY_ERROR_MESSAGE"] = "Synchronisierung fehlgeschlagen:<br />Verbindungsversuche: 10, zuletzt am #DATE#<br />Verbindung wurde der Status \"nicht aktiv\" verliehen<br /><br />Details finden Sie auf der Seite der Verbindungseinstellungen für CRM und E-Shop (<a href='#URL#'>CRM &gt; Einstellungen &gt; Verbindung mit E-Shops</a>).";
$MESS["CRM_EXT_SALE_TITLE_ERROR_SETTINGS"] = "Importfehler";
$MESS["CRM_EXT_SALE_IMPORT_UNKNOWN_ANSW_PERMS"] = "Nicht genügend Rechte. Überprüfen Sie, ob der Nutzer für Export der E-Shop-Bestellungen berechtigt ist.";
$MESS["CRM_EXT_SALE_IMPORT_UNKNOWN_ANSW_F"] = "Fehler beim Datenaustausch mit dem E-Shop";
$MESS["CRM_EXT_SALE_IMPORT_UNKNOWN_ANSW_PERMS1"] = "Nicht genügend Rechte. Überprüfen Sie, ob der Nutzer für Export der E-Shop-Bestellungen berechtigt ist.";
$MESS["CRM_EXT_SALE_IM_GROUP"] = "Gruppe";
?>