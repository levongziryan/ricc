<?
$MESS["CRM_EVENT_PROD_ROW_ADD"] = "Produkt hinzugefügt";
$MESS["CRM_EVENT_PROD_ROW_UPD"] = "Produkt geändert";
$MESS["CRM_EVENT_PROD_ROW_PRICE_UPD"] = "Preis des Produktes '#NAME#' wurde geändert";
$MESS["CRM_EVENT_PROD_ROW_QTY_UPD"] = "Menge des Produktes '#NAME#' wurde geändert";
$MESS["CRM_EVENT_PROD_ROW_REM"] = "Produkt gelöscht";
$MESS["CRM_EVENT_PROD_ROW_NAME_UPD"] = "Produktname wurde geändert";
$MESS["CRM_EVENT_PROD_ROW_DISCOUNT_UPD"] = "Der Rabatt auf Produkt '#NAME#' wurde geändert";
$MESS["CRM_EVENT_PROD_ROW_TAX_UPD"] = "Die Steuer auf Produkt '#NAME#' wurde geändert";
$MESS["CRM_EVENT_PROD_ROW_MEASURE_UPD"] = "Die Maßeinheit für das Produkt '#NAME#' wurde geändert";
?>