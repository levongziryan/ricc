<?
$MESS["CRM_INV_RQ_CONV_ERROR_GENERAL"] = "Allgemeiner Fehler";
$MESS["CRM_INV_RQ_CONV_ERROR_PRESET_NOT_BOUND"] = "In der ausgewählten Vorlage fehlen die Felder, welche mit älteren Informationen zum Unternehmen/Kontakt ausgefüllt werden können.";
$MESS["CRM_INV_RQ_CONV_ERROR_PERSON_TYPE_NOT_FOUND"] = "Die Kundengruppe wurde nicht gefunden.";
$MESS["CRM_INV_RQ_CONV_ERROR_PROPERTY_NOT_FOUND"] = "Die früher ausgefüllten Rechnungsinformationen wurden leider gelöscht, Datenübertragung ist nicht möglich.";
?>