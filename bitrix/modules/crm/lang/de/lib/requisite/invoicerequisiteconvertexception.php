<?
$MESS["CRM_INV_RQ_CONV_ERROR_GENERAL"] = "Allgemeiner Fehler.";
$MESS["CRM_INV_RQ_CONV_ERROR_PRESET_NOT_BOUND"] = "Die ausgewählte Vorlage hat keine Felder, die mit vorhandenen Informationen ausgefüllt werden können.";
$MESS["CRM_INV_RQ_CONV_ERROR_PERSON_TYPE_NOT_FOUND"] = "Kundengruppe kann nicht gefunden werden.";
$MESS["CRM_INV_RQ_CONV_ERROR_PROPERTY_NOT_FOUND"] = "Daten können nicht übertragen werden, weil die vorhandenen Rechnungsinformationen gelöscht wurden.";
?>