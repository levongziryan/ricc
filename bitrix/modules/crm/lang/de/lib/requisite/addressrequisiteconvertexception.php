<?
$MESS["CRM_ADDR_CONV_EX_COMPANY_ACCESS_DENIED"] = "Zugriff auf Unternehmen wurde verweigert";
$MESS["CRM_ADDR_CONV_EX_CONTACT_ACCESS_DENIED"] = "Zugriff auf Kontakt wurde verweigert";
$MESS["CRM_ADDR_CONV_EX_COMPANY_CREATION_FAILED"] = "Der Block mit Informationen zum Unternehmen kann nicht erstellt werden";
$MESS["CRM_ADDR_CONV_EX_CONTACT_CREATION_FAILED"] = "Der Block mit Informationen zum Kontakt kann nicht erstellt werden";
?>