<?
$MESS["CRM_RECUR_WRONG_ID"] = "Ungültige Account-ID";
$MESS["CRM_RECUR_ACTIVATE_LIMIT_REPEAT"] = "Fehler der Aktivierung der Bestellung, weil maximale Anzahl der Wiederholungen überschritten wurde. Sie sollten den maximalen Wert erhöhen.";
$MESS["CRM_RECUR_ACTIVATE_LIMIT_DATA"] = "Fehler der Aktivierung der Bestellung, weil das Datum der letzten Rechnung nun erreicht ist. Sie sollten das Datum ändern.";
?>