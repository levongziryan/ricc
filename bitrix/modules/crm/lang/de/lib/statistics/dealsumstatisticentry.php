<?
$MESS["CRM_DEAL_SUM_STAT_ENTRY_SLOT_SUM_TOTAL"] = "Gesamtbetrag";
$MESS["CRM_DEAL_SUM_STAT_ENTRY_TITLE"] = "Aufträge";
$MESS["CRM_DEAL_SUM_STAT_ENTRY_REBUILD"] = "Damit Berichte korrekt funktionieren, müssen Sie <a id=\"#ID#\" href=\"#URL#\">Auftragsstatistik aktualisieren</a>.";
$MESS["CRM_DEAL_SUM_STAT_ENTRY_REBUILD_DLG_TITLE"] = "Auftragsstatistik aktualisieren";
$MESS["CRM_DEAL_SUM_STAT_ENTRY_REBUILD_DLG_SUMMARY"] = "Die Auftragsstatistik wird jetzt aktualisiert. Das kann viel Zeit in Anspruch nehmen.";
?>