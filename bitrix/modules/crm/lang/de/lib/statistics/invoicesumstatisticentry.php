<?
$MESS["CRM_INVOICE_SUM_STAT_ENTRY_SLOT_SUM_TOTAL"] = "Gesamtbetrag";
$MESS["CRM_INVOICE_SUM_STAT_ENTRY_TITLE"] = "Rechnungen";
$MESS["CRM_INVOICE_SUM_STAT_ENTRY_REBUILD"] = "Sie sollten <a id=\"#ID#\" href=\"#URL#\">Rechnungsstatistik aktualisieren</a>, damit Berichte korrekt funktionieren.";
$MESS["CRM_INVOICE_SUM_STAT_ENTRY_REBUILD_DLG_TITLE"] = "Rechnungsstatistik aktualisieren";
$MESS["CRM_INVOICE_SUM_STAT_ENTRY_REBUILD_DLG_SUMMARY"] = "Die Rechnungsstatistik wird jetzt aktualisiert. Das kann einige Zeit dauern.";
?>