<?
$MESS["CRM_STAT_MGR_REBUILD_STATISTICS"] = "Damit die Berichte korrekt funktionieren, müssen Sie <a id=\"#ID#\" href=\"#URL#\">Statistik aktualisieren</a>.";
$MESS["CRM_STAT_MGR_REBUILD_STATISTICS_DLG_TITLE"] = "Statistik aktualisieren";
$MESS["CRM_STAT_MGR_REBUILD_STATISTICS_DLG_SUMMARY"] = "Die Statistikdaten werden jetzt aktualisiert. Das kann viel Zeit in Anspruch nehmen.";
?>