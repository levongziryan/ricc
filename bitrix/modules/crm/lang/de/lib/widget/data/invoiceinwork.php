<?
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_COUNT"] = "Anzahl der aktiven Rechnungen";
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_COUNT_SHORT"] = "Anzahl der Rechnungen";
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_SUM"] = "Gesamtbetrag der aktiven Rechnungen";
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_SUM_SHORT"] = "Gesamtbetrag der Rechnungen";
$MESS["CRM_INVOICE_IN_WORK_CATEGORY"] = "Aktive Rechnungen";
$MESS["CRM_INVOICE_OWED_CATEGORY"] = "Nicht bezahlte Rechnungen";
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_COUNT_OWED"] = "Anzahl der nicht bezahlten Rechnungen";
$MESS["CRM_INVOICE_IN_WORK_PRESET_OVERALL_SUM_OWED"] = "Gesamtbetrag der nicht bezahlten Rechnungen";
?>