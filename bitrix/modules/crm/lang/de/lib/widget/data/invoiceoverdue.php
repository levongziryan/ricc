<?
$MESS["CRM_INVOICE_OVERDUE_PRESET_OVERALL_COUNT"] = "Anzahl der überfälligen Rechnungen";
$MESS["CRM_INVOICE_OVERDUE_PRESET_OVERALL_SUM"] = "Gesamtbetrag der überfälligen Rechnungen";
$MESS["CRM_INVOICE_OVERDUE_PRESET_OVERALL_COUNT_SHORT"] = "Anzahl der Rechnungen";
$MESS["CRM_INVOICE_OVERDUE_PRESET_OVERALL_SUM_SHORT"] = "Gesamtbetrag der Rechnungen";
$MESS["CRM_INVOICE_OVERDUE_CATEGORY"] = "Überfällige Rechnungen";
?>