<?
$MESS["CRM_LEAD_CONV_STAT_PRESET_OVERALL_COUNT"] = "Anzahl der konvertierten Leads";
$MESS["CRM_LEAD_CONV_STAT_PRESET_CONTACT_COUNT"] = "Anzahl der Kontakte, die aus Leads erstellt wurden";
$MESS["CRM_LEAD_CONV_STAT_PRESET_COMPANY_COUNT"] = "Anzahl der Unternehmen, die Leads erstellt wurden";
$MESS["CRM_LEAD_CONV_STAT_PRESET_DEAL_COUNT"] = "Anzahl der Aufträge, die aus Leads erstellt wurden";
$MESS["CRM_LEAD_CONV_STAT_PRESET_OVERALL_SUM"] = "Gesamtbetrag der konvertierten Leads";
$MESS["CRM_LEAD_CONV_STAT_PRESET_OVERALL_COUNT_SHORT"] = "Anzahl der Leads";
$MESS["CRM_LEAD_CONV_STAT_PRESET_OVERALL_SUM_SHORT"] = "Gesamtbetrag der Leads";
$MESS["CRM_LEAD_CONV_CATEGORY"] = "Konvertierte Leads";
?>