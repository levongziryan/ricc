<?
$MESS["CRM_ACTIVITY_ACTIVITY_MARK_STAT_NONE_QTY"] = "Anzahl der Aktivitäten ohne Bewertung";
$MESS["CRM_ACTIVITY_ACTIVITY_MARK_STAT_NEGATIVE_QTY"] = "Anzahl der Aktivitäten mit negativer Bewertung";
$MESS["CRM_ACTIVITY_ACTIVITY_MARK_STAT_POSITIVE_QTY"] = "Anzahl der Aktivitäten mit positiver Bewertung";
$MESS["CRM_ACTIVITY_ACTIVITY_MARK_STAT_GROUP_BY_SOURCE"] = "Aktivitätsquelle";
$MESS["CRM_ACTIVITY_ACTIVITY_MARK_STAT_GROUP_BY_MARK"] = "Aktivitätsbewertung";
$MESS["CRM_ACTIVITY_ACTIVITY_MARK_STAT_TOTAL"] = "Anzahl der Aktivitäten (nach Bewertung)";
?>