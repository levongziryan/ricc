<?
$MESS["CRM_ACTIVITY_DYNAMIC_CHANNELS_ARE_NOT_FOUND"] = "Es gibt keine verfügbaren Kanäle.";
$MESS["CRM_ACTIVITY_DYNAMIC_CONNECTED_CHANNELS"] = "Verbundene Kanäle";
$MESS["CRM_ACTIVITY_DYNAMIC_CONNECTED_CHANNELS_IN"] = "Eingehend";
$MESS["CRM_ACTIVITY_DYNAMIC_CONNECTED_CHANNELS_OUT"] = "Ausgehend";
$MESS["CRM_ACTIVITY_DYNAMIC_UNCONNECTED_1"] = "und andere";
$MESS["CRM_ACTIVITY_DYNAMIC_UNCONNECTED_3"] = "Weitere Kanäle verbinden: ";
$MESS["CRM_ACTIVITY_DYNAMIC_CONNECTED"] = "Verbunden: ";
$MESS["CRM_ACTIVITY_DYNAMIC_CONNECT"] = "Verbinden: ";
$MESS["CRM_CH_TRACKER_START_VIDEO"] = "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/PMPjxAnEE0w?rel=0&autoplay=1#VOLUME#\" frameborder=\"0\" allowfullscreen></iframe>";
$MESS["CRM_CH_TRACKER_START_VIDEO_TITLE"] = "Sie können sich das Video jederzeit anschauen";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_IMOPENLINE"] = "Messenger und Soziale Services";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_IMOPENLINE_1"] = "Erster Kommunikationskanal";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_WEBFORM"] = "CRM-Formular";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_LIVECHAT"] = "Onlinechat";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_VKGROUP"] = "VK";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_WEBFORM_1"] = "Kontaktinformationen";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_WEBFORM_2"] = "Feedback-Formular";
$MESS["CRM_ACTIVITY_DYNAMIC_DEMO_WEBFORM_4"] = "Rückruf von der \"ausgehenden Basisstation\"";
$MESS["CRM_ACTIVITY_DYNAMIC_TITLE"] = "Kommunikation: Überblick";
?>