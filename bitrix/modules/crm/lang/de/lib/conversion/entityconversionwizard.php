<?
$MESS["CRM_ENTITY_CONV_WIZ_FORM_LEGEND"] = "Daten sind nicht ausreichend, um erforderliche Felder auszufüllen. Geben Sie bitte alle erforderlichen Informationen an, dann klicken Sie auf \"Fortfahren\".";
$MESS["CRM_ENTITY_CONV_WIZ_CUSTOM_FORM_LEGEND"] = "#TEXT# Geben Sie bitte die erforderlichen Informationen an und klicken Sie auf \"Fortfahren\".";
?>