<?
$MESS["CRM_LEAD_SETTINGS_VIEW_WIDGET"] = "Leadstatistik";
$MESS["CRM_LEAD_SETTINGS_VIEW_LIST"] = "Liste der Leads";
$MESS["CRM_TYPE_TITLE"] = "Wählen Sie eine Option aus, wie Sie Ihr CRM nutzen möchten";
$MESS["CRM_TYPE_SAVE"] = "Speichern";
$MESS["CRM_TYPE_CANCEL"] = "Abbrechen";
$MESS["CRM_TYPE_TURN_ON"] = "Aktivieren";
$MESS["CRM_ROBOTS_TITLE"] = "Basis-CRM";
$MESS["CRM_ROBOTS_TEXT"] = "Im Basis-CRM in Bitrix24 werden neue Leads automatisch zu Aufträgen mithilfe von Automatisierungsregeln konvertiert.<br/><br/>
Sie haben bereits Automatisierungsregeln für Leads, die aktiv sind. Wenn Sie diesen Modus aktivieren, werden diese Regeln deaktiviert. <br/><br/>
Möchten Sie das Basis-CRM wirklich aktivieren?
";
$MESS["CRM_LEAD_SETTINGS_VIEW_KANBAN"] = "Kanban der Leads";
?>