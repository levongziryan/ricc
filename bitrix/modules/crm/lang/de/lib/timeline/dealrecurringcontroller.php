<?
$MESS["CRM_DEAL_RECURRING_NEXT_EXECUTION"] = "Datum nächster Erstellung";
$MESS["CRM_DEAL_RECURRING_NEXT_EXECUTION_CHANGED"] = "Datum nächster Erstellung wurde geändert";
$MESS["CRM_DEAL_RECURRING_NOT_ACTIVE"] = "Wiederkehrender Auftrag ist nicht aktiv";
$MESS["CRM_DEAL_RECURRING_ACTIVE"] = "Wiederkehrender Auftrag ist aktiv";
$MESS["CRM_DEAL_CREATION"] = "Auftrag hinzugefügt";
$MESS["CRM_DEAL_RECURRING_CREATION"] = "Auftragsvorlage erstellt";
$MESS["CRM_RECURRING_CREATION_BASED_ON"] = "Erstellt aus";
$MESS["CRM_DEAL_BASE_CAPTION_BASED_ON_DEAL"] = "Aus dem Auftrag";
?>