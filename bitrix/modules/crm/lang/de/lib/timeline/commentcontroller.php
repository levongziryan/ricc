<?
$MESS["CRM_COMMENT_IM_MENTION_POST"] = "Hat Sie im Kommentar zu \"#ENTITY_TITLE#\" erwähnt, Kommentartext: \"#COMMENT#\"";
$MESS["CRM_COMMENT_IM_MENTION_POST_M"] = "Hat Sie im Kommentar zu \"#ENTITY_TITLE#\" erwähnt, Kommentartext: \"#COMMENT#\"";
$MESS["CRM_COMMENT_IM_MENTION_POST_F"] = "Hat Sie im Kommentar zu \"#ENTITY_TITLE#\" erwähnt, Kommentartext: \"#COMMENT#\"";
$MESS["CRM_ENTITY_TITLE_DEAL"] = "Auftrag #ENTITY_NAME# ";
$MESS["CRM_ENTITY_TITLE_LEAD"] = "Lead #ENTITY_NAME# ";
$MESS["CRM_ENTITY_TITLE_QUOTE"] = "Angebot #ENTITY_NAME# ";
$MESS["CRM_ENTITY_TITLE_CONTACT"] = "Kontakt #ENTITY_NAME# ";
$MESS["CRM_ENTITY_TITLE_COMPANY"] = "Unternehmen #ENTITY_NAME# ";
$MESS["CRM_ENTITY_TITLE_INVOICE"] = "Rechnung #ENTITY_NAME# ";
?>