<?
$MESS["CRM_CALL_LIST_LIMIT_ERROR"] = "Maximale Anzahl der Elemente überschritten. Setzen Sie den Filter so, dass nicht mehr als #LIMIT# Elemente ausgewählt werden können.";
$MESS["CRM_CALL_LIST_UPDATE"] = "Zur Anrufliste hinzufügen";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_HEADER"] = "Verfügbar ab dem Tarif Plus";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_CONTENT"] = "Nutzen Sie Anruflisten, um eine Liste der Kunden zu erstellen, die angerufen werden sollen, und eine entsprechende Aufgabe dem verantwortlichen Mitarbeiter zuzuweisen.";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_ITEM_1"] = "Eine Anrufliste in wenigen Klicks erstellen";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_ITEM_2"] = "Alle Kunden einen nach dem anderen im selben Fenster anrufen";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_ITEM_3"] = "Anrufprozess und Ergebnisse kontrollieren";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_SHOW_MORE"] = "Mehr ";
$MESS["CRM_CALL_LIST_LICENSE_POPUP_FOOTER"] = "Anruflisten und andere nützliche Funktionen sind verfügbar für lediglich 39 monatlich ab dem Tarif Plus.";
$MESS["CRM_CALL_LIST_SUBJECT"] = "Anrufliste";
?>