<?
$MESS["CRM_B24_EMAIL_FREE_LICENSE_SIGNATURE"] = "Gesendet mit [url=http://www.bitrix24.de]Bitrix24[/url]
Das weltweit erste CRM mit unbegrenztem Speicherplatz";
$MESS["CRM_B24_EMAIL_PAID_LICENSE_SIGNATURE"] = "Gesendet mit Bitrix24";
$MESS["CRM_B24_EMAIL_SIGNATURE_ENABLED"] = "Aktiviert (die Option kann nur in den kostenpflichtigen Tarifen Plus, Standard und Professional deaktiviert werden)";
$MESS["CRM_B24_EMAIL_SIGNATURE_DISABLED"] = "Deaktiviert";
?>