<?
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_NAME"] = "Provider SMS.RU";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_CAN_USE_ERROR"] = "Provider SMS.RU ist nicht konfiguriert";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_201"] = "Guthaben nicht ausreichend";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_202"] = "Empfänger nicht korrekt";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_203"] = "Nachrichtentext ist leer";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_204"] = "Absendername wurde von der Administration nicht bestätigt";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_205"] = "Nachricht ist zu lang (über 8 SMS's)";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_206"] = "Tägliches Nachrichtenlimit wurde oder wird überschritten";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_207"] = "Nachrichten können an diese Nummer oder eine dieser Nummern nicht gesendet, oder die Liste der Empfänger überschreitet das Limit von 100 Nummern";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_209"] = "Sie haben diese Nummer oder eine dieser Nummern in die schwarze Liste eingetragen";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_230"] = "Tägliches Nachrichtenlimit wurde für diese Nummer überschritten";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_231"] = "Limit von identischen Nachrichten pro Minute wurde für diese Nummer überschritten";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_232"] = "Tägliches Limit von identischen Nachrichten wurde für diese Nummer überschritten";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_301"] = "Passowrt ist nicht korrekt oder Nutzer wurde nicht gefunden";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_302"] = "Account wurde nicht bestätigt (der Nutzer hat den Code nicht eingegeben, der in der Bestätigungsnachricht gesendet wurde)";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_303"] = "Bestätigungscode ist nicht korrekt";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_304"] = "Zu viele Bestätigungscodes gesendet. Versuchen Sie bitte später erneut";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_305"] = "Zu viele nicht korrekte Versuche. Versuchen Sie bitte später erneut";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_900"] = "Bestätigungscode ist nicht korrekt";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_901"] = "Telefonnummer ist nicht korrekt oder im falschen Format";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_903"] = "Eine Nummer kann derzeit mit diesem Code nicht registriert werden";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_905"] = "Nutzer hat den Namen nicht angegeben (oder der Name ist kürzer als 2 Zeichen)";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_906"] = "Nutzer hat den Nachnamen nicht angegeben (oder der Nachname ist kürzer als 2 Zeichen)";
$MESS["CRM_INTEGRATION_SMS_PROVIDER_SMSRU_ERROR_OTHER"] = "Systemfehler. Versuchen Sie bitte erneut";
?>