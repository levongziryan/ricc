<?
$MESS["CRM_USER_CONSENT_DATA_PROVIDER_NAME"] = "Informationen zu meinem Unternehmen im CRM";
$MESS["CRM_USER_CONSENT_PROVIDER_NAME"] = "CRM-Aktivität";
$MESS["CRM_USER_CONSENT_PROVIDER_ITEM_NAME"] = "CRM-Aktivität #%id%";
$MESS["CRM_USER_CONSENT_NOTIFY_TEXT_BTN"] = "Mehr";
$MESS["CRM_USER_CONSENT_DEF_NAME"] = "Muster der Zustimmung der Verarbeitung persönlicher Daten";
?>