<?
$MESS["CRM_WEBFORM_RECAPTCHA_ERROR_MISSING_INPUT_SECRET"] = "reCAPTCHA: Geheimschlüssel ist nicht angegeben.";
$MESS["CRM_WEBFORM_RECAPTCHA_ERROR_INVALID_INPUT_SECRET"] = "reCAPTCHA: Geheimschlüssel ist nicht korrekt.";
$MESS["CRM_WEBFORM_RECAPTCHA_ERROR_MISSING_INPUT_RESPONSE"] = "reCAPTCHA: Antwort ist nicht angegeben.";
$MESS["CRM_WEBFORM_RECAPTCHA_ERROR_INVALID_INPUT_RESPONSE"] = "Prüfung auf Spam ist fehlgeschlagen.";
$MESS["CRM_WEBFORM_RECAPTCHA_ERROR_UNKNOWN"] = "Unbekannter Fehler..";
?>