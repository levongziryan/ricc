<?
$MESS["CRM_WEBFORM_FORM_ERROR_SCHEME"] = "Ungültiges Dokumentschema.";
$MESS["CRM_WEBFORM_FORM_BUTTON_CAPTION_DEFAULT"] = "Senden";
$MESS["CRM_WEBFORM_FORM_SCRIPT_BUTTON_TEXT"] = "Name der Schaltfläche";
$MESS["CRM_WEBFORM_FORM_COPY_NAME_PREFIX"] = "Kopie";
$MESS["CRM_WEBFORM_FORM_ERROR_CAPTCHA_KEY"] = "Ein Schlüssel und ein Geheimschlüssel sind zur Nutzung von reCAPTCHA erforderlich";
$MESS["CRM_WEBFORM_FORM_PRESET_FIELDS"] = "In Standardfeldern";
?>