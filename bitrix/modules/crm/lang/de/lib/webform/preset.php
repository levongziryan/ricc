<?
$MESS["CRM_WEBFORM_PRESET_ITEM_CD_NAME"] = "Kontaktinformation";
$MESS["CRM_WEBFORM_PRESET_ITEM_CD_CAPTION"] = "Kontaktinformation";
$MESS["CRM_WEBFORM_PRESET_ITEM_CD_DESCRIPTION"] = "Geben Sie bitte Ihre Kontaktinformation an, und wir werden uns mit Ihnen in Verbindung setzen";
$MESS["CRM_WEBFORM_PRESET_ITEM_FB_NAME"] = "Feedbackformular";
$MESS["CRM_WEBFORM_PRESET_ITEM_FB_CAPTION"] = "Feedback";
$MESS["CRM_WEBFORM_PRESET_ITEM_FB_DESCRIPTION"] = "Geben Sie bitte Ihr Feedback im Kommentarfeld";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_RESULT_SUCCESS_TEXT"] = "Danke, Ihre Nachricht wurde gesendet.";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_RESULT_FAILURE_TEXT"] = "Die Nachricht konnte leider nicht gesendet werden, versuchen Sie bitte später erneut.";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_FIELD_NAME"] = "Vorname";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_FIELD_LAST_NAME"] = "Nachname";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_FIELD_PHONE"] = "Telefon";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_FIELD_EMAIL"] = "E-Mail";
$MESS["CRM_WEBFORM_PRESET_ITEM_DEF_FIELD_COMMENTS"] = "Kommentar";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_NAME"] = "Rückruf von \"#call_from#\"";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_CAPTION"] = "Haben Sie es nicht gefunden, was Sie gesucht haben?";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_DESCRIPTION"] = "Haben Sie noch Fragen? Wir rufen Sie zurück!";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_BUTTON_CAPTION"] = "Zurückrufen";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_RESULT_SUCCESS_TEXT"] = "Unsere Mitarbeiter werden Sie in wenigen Sekunden kontaktieren.";
$MESS["CRM_WEBFORM_PRESET_ITEM_CB_CALL_TEXT"] = "Ein Rückruf wurde angefordert. Sie werden jetzt mit dem Kunden verbunden.";
?>