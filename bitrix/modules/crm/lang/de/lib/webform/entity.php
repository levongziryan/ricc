<?
$MESS["CRM_WEBFORM_ENTITY_FIELD_NAME_TEMPLATE"] = "CRM-Formular \"#FORM_NAME#\" ausfüllen";
$MESS["CRM_WEBFORM_ENTITY_FIELD_NAME_COMPANY_TEMPLATE"] = "Unternehmen";
$MESS["CRM_WEBFORM_ENTITY_FIELD_NAME_CONTACT_TEMPLATE"] = "Person";
$MESS["CRM_WEBFORM_ENTITY_SCHEME_LEED"] = "Lead";
$MESS["CRM_WEBFORM_ENTITY_SCHEME_LEED_DESC"] = "Lead";
$MESS["CRM_WEBFORM_ENTITY_SCHEME_LEED_INVOICE_DESC"] = "Lead, Kontakt oder Unternehmen, Rechnung";
$MESS["CRM_WEBFORM_ENTITY_SCHEME_DEAL"] = "Auftrag";
$MESS["CRM_WEBFORM_ENTITY_SCHEME_DEAL_DESC"] = "Auftrag, Kontakt oder Unternehmen";
$MESS["CRM_WEBFORM_ENTITY_SCHEME_DEAL_INVOICE_DESC"] = "Auftrag, Kontakt oder Unternehmen, Rechnung";
$MESS["CRM_WEBFORM_ENTITY_SCHEME_CLIENT"] = "Kunde";
$MESS["CRM_WEBFORM_ENTITY_SCHEME_CLIENT_DESC"] = "Kontakt oder Unternehmen";
$MESS["CRM_WEBFORM_ENTITY_SCHEME_CLIENT_INVOICE_DESC"] = "Kontakt oder Unternehmen, Rechnung";
$MESS["CRM_WEBFORM_ENTITY_SCHEME_QUOTE"] = "Angebot";
$MESS["CRM_WEBFORM_ENTITY_SCHEME_QUOTE_DESC"] = "Angebot, Kontakt oder Unternehmen";
$MESS["CRM_WEBFORM_ENTITY_SCHEME_QUOTE_INVOICE_DESC"] = "Angebot, Kontakt oder Unternehmen, Rechnung";
?>