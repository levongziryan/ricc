<?
$MESS["CRM_AUTOMATION_DEMO_DEAL_1_NOTIFY_TITLE"] = "Benachrichtigung";
$MESS["CRM_AUTOMATION_DEMO_DEAL_1_1_MESSAGE"] = "Neuer Auftrag #URL_1#{=Document:TITLE}#URL_2# erstellt 

Überprüfen Sie bitte, ob Sie alle erforderlichen Informationen zum Kunden und zur Bestellung haben. Sie können fehlende Daten hinzufügen, Aktivitäten planen und Aufgaben für diesen Auftrag definieren.

Vergessen Sie nicht, den Auftrag in die nächste Phase zu verschieben.
";
$MESS["CRM_AUTOMATION_DEMO_DEAL_1_2_MESSAGE"] = "Der Auftrag  #URL_1#{=Document:TITLE}#URL_2#  ist immer noch in der ursprünglichen Phase. 

Überprüfen Sie bitte, ob am Auftrag gearbeitet wird, und verschieben ihn, wenn erforderlich, in die nächste Phase.
";
?>