<?
$MESS["CRM_IMPORT_YAHOO_ERROR_FIELDS_NOT_FOUND"] = "Diese Felder wurden nicht gefunden: #FIELD_LIST#";
$MESS["CRM_IMPORT_YAHOO_REQUIREMENTS"] = "Für einen erfolgreichen Import müssen folgende Bedingungen erfüllt werden. Datei-Codierung: UTF-8. Sprache der Feldnamen: Englisch. Feldtrennzeichen: Komma.";
?>