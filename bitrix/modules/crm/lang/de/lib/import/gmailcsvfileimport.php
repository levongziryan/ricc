<?
$MESS["CRM_IMPORT_GMAIL_ERROR_FIELDS_NOT_FOUND"] = "Diese Felder wurden nicht gefunden: #FIELD_LIST#";
$MESS["CRM_IMPORT_GMAIL_REQUIREMENTS"] = "Für einen erfolgreichen Import müssen folgende Bedingungen erfüllt werden. Datei-Codierung: UTF-16. Sprache der Feldnamen: Englisch. Feldtrennzeichen: Komma.";
?>