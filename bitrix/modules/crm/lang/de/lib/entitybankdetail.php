<?
$MESS["CRM_BANKDETAIL_ERR_INVALID_ENTITY_TYPE"] = "Elementtyp für die Bankverbindung ist nicht korrekt";
$MESS["CRM_BANKDETAIL_ERR_INVALID_ENTITY_ID"] = "Es wurde kein Element für die Bankverbindung angegeben";
$MESS["CRM_BANKDETAIL_ERR_ON_DELETE"] = "Fehler beim Löschen der Bankverbindung";
$MESS["CRM_BANKDETAIL_ERR_NOTHING_TO_DELETE"] = "Es sind keine Bankverbindungen verfügbar, die gelöscht werden können";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_ID"] = "ID";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_COUNTRY_ID"] = "ID des Landes";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_COUNTRY_NAME"] = "Land";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_NAME"] = "Name";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_ACTIVE"] = "Aktiv";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_SORT"] = "Sortierung";
$MESS["CRM_BANK_DETAIL_EXPORT_FIELD_COMMENTS"] = "Kommentar";
?>