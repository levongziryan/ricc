<?
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_MO"] = "Mo.";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_TU"] = "Di.";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_WE"] = "Mi.";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_TH"] = "Do.";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_FR"] = "Fr.";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_SA"] = "Sa.";
$MESS["CRM_SITE_BUTTON_WORK_TIME_DAY_SU"] = "So.";
$MESS["CRM_SITE_BUTTON_WORK_TIME_ACTIONS_HIDE"] = "Schaltfläche des Rückrufs ausblenden";
$MESS["CRM_SITE_BUTTON_WORK_TIME_ACTIONS_TEXT"] = "Kundeninformationen speichern und eine Nachricht anzeigen";
$MESS["CRM_SITE_BUTTON_WORK_TIME_ACTIONS_TEXT_CALLBACK"] = "Leider können wir Sie im Moment nicht zurückrufen. Wir werden Sie so schnell wie möglich innerhalb von unseren Bürozeiten kontaktieren.";
?>