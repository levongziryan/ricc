<?
$MESS["CRM_ENTITY_TYPE_REQUISITE"] = "Informationen";
$MESS["CRM_ENTITY_TYPE_REQUISITE_DESC"] = "Vorlagen der Informationen zum Kontakt/Unternehmen";
$MESS["CRM_ENTITY_PRESET_ERR_DELETE_PRESET_USED"] = "Die Vorlage kann nicht gelöscht werden, weil es Informationen gibt, die damit erstellt wurden.";
$MESS["CRM_ENTITY_PRESET_ERR_PRESET_NOT_FOUND"] = "Vorlage wurde nicht gefunden.";
$MESS["CRM_ENTITY_PRESET_ERR_INVALID_ENTITY_TYPE"] = "Ungültiger Elementtyp zur Nutzung mit einer Vorlage.";
$MESS["CRM_ENTITY_PRESET_ERR_DELETE_PRESET_DEF_FOR_COMPANY"] = "Sie können diese Vorlage nicht löschen, weil es eine Standardvorlage des Unternehmens ist.";
$MESS["CRM_ENTITY_PRESET_ERR_DELETE_PRESET_DEF_FOR_CONTACT"] = "Sie können diese Vorlage nicht löschen, weil es eine Standardvorlage des Kontakts ist.";
$MESS["CRM_ENTITY_PRESET_NAME_EMPTY"] = "Vorlage ohne Namen";
$MESS["CRM_ENTITY_PRESET_UF_TYPE_STRING"] = "Zeile";
$MESS["CRM_ENTITY_PRESET_UF_TYPE_DOUBLE"] = "Nummer";
$MESS["CRM_ENTITY_PRESET_UF_TYPE_BOOLEAN"] = "Ja/Nein";
$MESS["CRM_ENTITY_PRESET_UF_TYPE_DATETIME"] = "Datum";
?>