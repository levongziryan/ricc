<?
$MESS["CRM_LEAD_MERGER_COLLISION_READ_PERMISSION"] = "#USER_NAME# hat den Lead \"#SEED_TITLE#\" [#SEED_ID#] mit \"#TARG_TITLE#\" [#TARG_ID#] vereinigt, welchen Sie wegen unzureichender Zugriffsrechte nicht anzeigen können.";
$MESS["CRM_LEAD_MERGER_COLLISION_UPDATE_PERMISSION"] = "#USER_NAME# hat den Lead \"#SEED_TITLE#\" [#SEED_ID#] mit \"#TARG_TITLE#\" [#TARG_ID#] vereinigt, welchen Sie wegen unzureichender Zugriffsrechte nicht bearbeiten können.";
$MESS["CRM_LEAD_MERGER_COLLISION_READ_UPDATE_PERMISSION"] = "#USER_NAME# hat den Lead \"#SEED_TITLE#\" [#SEED_ID#] mit \"#TARG_TITLE#\" [#TARG_ID#] vereinigt, welchen Sie wegen unzureichender Zugriffsrechte weder anzeigen noch bearbeiten können.";
?>