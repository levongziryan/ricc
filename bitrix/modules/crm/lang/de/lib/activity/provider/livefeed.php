<?
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_NAME"] = "Activity Stream";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TYPE_LOG_ENTRY"] = "Nachricht im Activity Stream";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TYPE_LOG_COMMENT"] = "Kommentar zu einer Nachricht im Activity Stream";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TYPE_LOG_COMMENT_OUT"] = "Kommentar zu einer Nachricht im Activity Stream (ausgehender)";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TYPE_LOG_COMMENT_IN"] = "Kommentar zu einer Nachricht im Activity Stream (eingehender)";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TITLE_ENTRY"] = "Nachricht im Activity Stream";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TITLE_COMMENT_IN"] = "Eingehender Kommentar zu einer Nachricht im Activity Stream";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_TITLE_COMMENT_OUT"] = "Ausgehender Kommentar zu einer Nachricht im Activity Stream";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_LINK_ENTRY"] = "Zur Nachricht wechseln";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_LINK_COMMENT"] = "Zum Kommentar wechseln";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_AUTHOR_ACTION"] = "#USER_NAME# hat geschrieben:";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_AUTHOR_ACTIONM"] = "#USER_NAME# hat geschrieben:";
$MESS["CRM_ACTIVITY_PROVIDER_LIVEFEED_AUTHOR_ACTIONF"] = "#USER_NAME# hat geschrieben:";
?>