<?
$MESS["CRM_ACTIVITY_WEBFORM_NAME"] = "CRM-Formular";
$MESS["CRM_ACTIVITY_WEBFORM_NAME_IN"] = "Eingehende";
$MESS["CRM_ACTIVITY_WEBFORM_FIELDS_LINK"] = "Link zum Formular";
$MESS["CRM_ACTIVITY_WEBFORM_STATUS_ACT"] = "Verbunden";
$MESS["CRM_ACTIVITY_WEBFORM_STATUS_INACT"] = "Verbinden";
$MESS["CRM_ACTIVITY_WEBFORM_YES"] = "Ja";
$MESS["CRM_ACTIVITY_WEBFORM_NO"] = "Nein";
$MESS["CRM_ACTIVITY_WEBFORM_IP"] = "Gesendet von der IP-Adresse";
$MESS["CRM_ACTIVITY_WEBFORM_USER_CONSENT"] = "Die Vereinbarung ist sichtbar";
$MESS["CRM_ACTIVITY_WEBFORM_VISITED_PAGES"] = "Besuchte Seiten";
?>