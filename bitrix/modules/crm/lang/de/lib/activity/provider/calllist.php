<?
$MESS["CRM_ACTIVITY_PROVIDER_CALL_LIST_TITLE"] = "Anrufliste";
$MESS["CRM_CALL_LIST_NOT_CREATED_ERROR"] = "Anrufliste existiert nicht. Um die Liste zu erstellen, klicken Sie auf den Link im Bereich \"Liste der Anrufe\".";
$MESS["CRM_CALL_LIST_SUBJECT_EMPTY"] = "Aktivitätsbetreff nicht angegeben";
$MESS["CRM_CALL_LIST_RESPONSIBLE_IM_NOTIFY"] = "Sie sind jetzt verantwortlich für die Anrufliste \"#title#\"";
$MESS["CRM_CALL_LIST_USE_WEBFORM"] = "Formular nutzen";
?>