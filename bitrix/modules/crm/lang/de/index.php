<?
$MESS["CRM_INSTALL_NAME"] = "CRM";
$MESS["CRM_INSTALL_DESCRIPTION"] = "Bietet CRM-Unterstützung.";
$MESS["CRM_INSTALL_TITLE"] = "Installation des CRM-Moduls";
$MESS["CRM_UNINSTALL_TITLE"] = "Deinstallation des CRM-Moduls";
$MESS["CRM_SALE_MODULE_NOT_INSTALL"] = "Vor der Installation des Moduls CRM müssen Sie das Modul Onlineshop installieren.";
?>