<?
$MESS["BPCDSA_PD_DEAL"] = "ID de la negociación";
$MESS["BPCDSA_PD_STAGE"] = "Fase";
$MESS["BPCDSA_PD_STAGE_DESCR"] = "Seleccione una o más etapas de espera. El flujo de trabajo esperará hasta que la negociación se haya trasladado a la etapa seleccionada o final.";
?>