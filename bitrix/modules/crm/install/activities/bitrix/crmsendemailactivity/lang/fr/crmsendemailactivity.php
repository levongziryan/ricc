<?
$MESS["CRM_SEMA_EMPTY_PROP"] = "Le texte du message est requis";
$MESS["CRM_SEMA_MESSAGE_TEXT"] = "Texte du message";
$MESS["CRM_SEMA_DEFAULT_SUBJECT"] = "Nouvel e-mail (#DATE#)";
$MESS["CRM_SEMA_EMAIL_SUBJECT"] = "Objet";
$MESS["CRM_SEMA_EMAIL_FROM"] = "De";
$MESS["CRM_SEMA_EMAIL_TO"] = "À";
$MESS["CRM_SEMA_NO_ADDRESSER"] = "L'adresse e-mail du client n'est pas indiquée";
$MESS["CRM_SEMA_MESSAGE_TEXT_TYPE"] = "Type de texte";
$MESS["CRM_SEMA_ATTACHMENT_TYPE"] = "Type de pièce jointe";
$MESS["CRM_SEMA_ATTACHMENT"] = "Pièces jointes";
$MESS["CRM_SEMA_ATTACHMENT_FILE"] = "Fichiers du document";
$MESS["CRM_SEMA_ATTACHMENT_DISK"] = "Lecteur";
$MESS["CRM_SEMA_NO_FROM"] = "L'expéditeur du message est incorrect ou non spécifié";
$MESS["CRM_SEMA_DEFAULT_BODY"] = "[sans texte]";
?>