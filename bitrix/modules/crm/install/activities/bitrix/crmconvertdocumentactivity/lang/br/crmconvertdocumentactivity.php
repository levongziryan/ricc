<?
$MESS["CRM_CVTDA_WIZARD_NOT_FOUND"] = "Não é possível inicializar o Assistente de Conversão.";
$MESS["CRM_CVTDA_ITEMS"] = "Criar usando fonte";
$MESS["CRM_CVTDA_DEAL_CATEGORY_ID"] = "Pipeline da negociação";
$MESS["CRM_CVTDA_DEAL"] = "Negociação";
$MESS["CRM_CVTDA_CONTACT"] = "Contato";
$MESS["CRM_CVTDA_COMPANY"] = "Empresa";
$MESS["CRM_CVTDA_EMPTY_PROP"] = "As entidades a criar não estão especificadas";
$MESS["CRM_CVTDA_INVOICE"] = "Nota Fiscal";
$MESS["CRM_CVTDA_QUOTE"] = "Cotação";
$MESS["CRM_CVTDA_REQUEST_SUBJECT_LEAD"] = "O cliente potencial precisa ser convertido";
$MESS["CRM_CVTDA_REQUEST_SUBJECT_DEAL"] = "A negociação precisa ser convertida";
$MESS["CRM_CVTDA_REQUEST_DESCRIPTION_LEAD"] = "As seguintes entidades precisam ser criadas usando o Cliente Potencial: #ITEMS#";
$MESS["CRM_CVTDA_REQUEST_DESCRIPTION_DEAL"] = "As seguintes entidades precisam ser criadas usando a Negociação: #ITEMS#";
$MESS["CRM_CVTDA_INCORRECT_DOCUMENT"] = "Não é possível converter elementos desse tipo";
$MESS["CRM_CVTDA_DEFAULT_CONTACT_NAME"] = "Sem nome";
?>