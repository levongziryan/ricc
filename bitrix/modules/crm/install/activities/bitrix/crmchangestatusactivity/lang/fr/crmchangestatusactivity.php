<?
$MESS["CRM_CHANGE_STATUS_EMPTY_PROP"] = "Statut de la cible non spécifié";
$MESS["CRM_CHANGE_STATUS_STATUS"] = "Nouveau statut";
$MESS["CRM_CHANGE_STATUS_STAGE"] = "Nouvelle étape";
$MESS["CRM_CHANGE_STATUS_TERMINATED"] = "Terminé parce que le statut a été modifié";
?>