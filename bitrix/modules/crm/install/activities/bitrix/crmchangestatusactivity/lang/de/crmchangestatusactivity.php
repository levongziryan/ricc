<?
$MESS["CRM_CHANGE_STATUS_EMPTY_PROP"] = "Endstatus ist nicht angegeben";
$MESS["CRM_CHANGE_STATUS_STATUS"] = "Neuer Status";
$MESS["CRM_CHANGE_STATUS_STAGE"] = "Neue Phase";
$MESS["CRM_CHANGE_STATUS_TERMINATED"] = "Abgeschlossen, weil Status geändert";
$MESS["CRM_CHANGE_STATUS_RECURSION"] = "Fehler bei Ausf?hrung: evtl. unendliche Rekursion beim ?ndern von Status";
?>