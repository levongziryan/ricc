<?
$MESS["CRM_SSMSA_EMPTY_TEXT"] = "Le texte du message est vide.";
$MESS["CRM_SSMSA_EMPTY_PROVIDER"] = "Aucun fournisseur sélectionné.";
$MESS["CRM_SSMSA_MESSAGE_TEXT"] = "Texte du message";
$MESS["CRM_SSMSA_PROVIDER"] = "Fournisseur";
$MESS["CRM_SSMSA_EMPTY_PHONE_NUMBER"] = "Le numéro de téléphone n'est pas spécifié";
$MESS["CRM_SSMSA_NO_PROVIDER"] = "Le fournisseur de SMS n’est pas inscrit ou a été supprimé.";
$MESS["CRM_SSMSA_REST_SESSION_ERROR"] = "Session REST bloquée";
$MESS["CRM_SSMSA_PAYMENT_REQUIRED"] = "L'application n’a pas été payée";
$MESS["CRM_SSMSA_RECIPIENT_TYPE"] = "Type de destinataire";
$MESS["CRM_SSMSA_RECIPIENT_TYPE_ENTITY"] = "Client";
$MESS["CRM_SSMSA_RECIPIENT_TYPE_USER"] = "Employé";
$MESS["CRM_SSMSA_RECIPIENT_USER"] = "Employé";
$MESS["CRM_SSMSA_SEND_RESULT_TRUE"] = "Message SMS envoyé à #PHONE#";
$MESS["CRM_SSMSA_SEND_ACTIVITY_SUBJECT"] = "Message SMS sortant";
$MESS["CRM_SSMSA_NO_MESSAGESERVICE"] = "Le module \"Service de messagerie\" n'est pas installé.";
?>