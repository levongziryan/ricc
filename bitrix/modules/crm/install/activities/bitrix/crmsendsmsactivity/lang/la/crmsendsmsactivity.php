<?
$MESS["CRM_SSMSA_EMPTY_TEXT"] = "El texto del mensaje está vacío.";
$MESS["CRM_SSMSA_EMPTY_PROVIDER"] = "Ningún proveedor seleccionado.";
$MESS["CRM_SSMSA_MESSAGE_TEXT"] = "Mensaje de texto";
$MESS["CRM_SSMSA_PROVIDER"] = "Proveedor";
$MESS["CRM_SSMSA_EMPTY_PHONE_NUMBER"] = "El número de teléfono no está especificado";
$MESS["CRM_SSMSA_NO_PROVIDER"] = "El SMS del proveedor no está registrado o se ha eliminado.";
$MESS["CRM_SSMSA_REST_SESSION_ERROR"] = "Sesión REST bloqueada";
$MESS["CRM_SSMSA_PAYMENT_REQUIRED"] = "La aplicación no se pagó";
$MESS["CRM_SSMSA_RECIPIENT_TYPE"] = "Tipo de destinatario";
$MESS["CRM_SSMSA_RECIPIENT_TYPE_ENTITY"] = "Cliente";
$MESS["CRM_SSMSA_RECIPIENT_TYPE_USER"] = "Empleado";
$MESS["CRM_SSMSA_RECIPIENT_USER"] = "Empleado";
$MESS["CRM_SSMSA_SEND_RESULT_TRUE"] = "Mensaje SMS enviado al #PHONE#";
$MESS["CRM_SSMSA_SEND_ACTIVITY_SUBJECT"] = "Mensaje SMS saliente";
$MESS["CRM_SSMSA_NO_MESSAGESERVICE"] = "El módulo \"Servicio de mensajería\" no está instalado.";
$MESS["CRM_SSMSA_MESSAGE_FROM"] = "De";
$MESS["CRM_SSMSA_FROM_DEFAULT"] = "por defecto";
?>