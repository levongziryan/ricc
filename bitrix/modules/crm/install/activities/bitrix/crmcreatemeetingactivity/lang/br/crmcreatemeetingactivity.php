<?
$MESS["CRM_CREATE_MEETING_EMPTY_PROP"] = "O parâmetro obrigatório está vazio: #PROPERTY#";
$MESS["CRM_CREATE_MEETING_SUBJECT"] = "Assunto da reunião";
$MESS["CRM_CREATE_MEETING_START_TIME"] = "Data de início";
$MESS["CRM_CREATE_MEETING_END_TIME"] = "Data de término";
$MESS["CRM_CREATE_MEETING_IS_IMPORTANT"] = "Importante";
$MESS["CRM_CREATE_MEETING_DESCRIPTION"] = "Descrição";
$MESS["CRM_CREATE_MEETING_LOCATION"] = "Local";
$MESS["CRM_CREATE_MEETING_NOTIFY_VALUE"] = "Lembrar em";
$MESS["CRM_CREATE_MEETING_NOTIFY_TYPE"] = "Intervalo de lembrete";
$MESS["CRM_CREATE_MEETING_RESPONSIBLE_ID"] = "Pessoa responsável";
$MESS["CRM_CREATE_MEETING_AUTO_COMPLETE"] = "Executa automaticamente quando o fluxo de trabalho é concluído";
?>