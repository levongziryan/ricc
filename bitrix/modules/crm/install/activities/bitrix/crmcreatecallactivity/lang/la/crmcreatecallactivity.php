<?
$MESS["CRM_CREATE_CALL_EMPTY_PROP"] = "El parámetro requerido está vacío: #PROPERTY#";
$MESS["CRM_CREATE_CALL_SUBJECT"] = "Asunto";
$MESS["CRM_CREATE_CALL_START_TIME"] = "Fecha de inicio";
$MESS["CRM_CREATE_CALL_END_TIME"] = "Fecha de finalización";
$MESS["CRM_CREATE_CALL_IS_IMPORTANT"] = "Importante";
$MESS["CRM_CREATE_CALL_DESCRIPTION"] = "Descripción";
$MESS["CRM_CREATE_CALL_NOTIFY_VALUE"] = "Recordar en";
$MESS["CRM_CREATE_CALL_NOTIFY_TYPE"] = "Intervalo de recordatorio";
$MESS["CRM_CREATE_CALL_RESPONSIBLE_ID"] = "Persona responsable";
$MESS["CRM_CREATE_CALL_AUTO_COMPLETE"] = "Ejecutar automáticamente cuando el flujo de trabajo termina";
?>