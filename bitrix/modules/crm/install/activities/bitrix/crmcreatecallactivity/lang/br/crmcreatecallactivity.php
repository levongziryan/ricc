<?
$MESS["CRM_CREATE_CALL_EMPTY_PROP"] = "O parâmetro obrigatório está vazio: #PROPERTY#";
$MESS["CRM_CREATE_CALL_SUBJECT"] = "Assunto";
$MESS["CRM_CREATE_CALL_START_TIME"] = "Data de início";
$MESS["CRM_CREATE_CALL_END_TIME"] = "Data de término";
$MESS["CRM_CREATE_CALL_IS_IMPORTANT"] = "Importante";
$MESS["CRM_CREATE_CALL_DESCRIPTION"] = "Descrição";
$MESS["CRM_CREATE_CALL_NOTIFY_VALUE"] = "Lembrar em";
$MESS["CRM_CREATE_CALL_NOTIFY_TYPE"] = "Intervalo de lembrete";
$MESS["CRM_CREATE_CALL_RESPONSIBLE_ID"] = "Pessoa responsável";
$MESS["CRM_CREATE_CALL_AUTO_COMPLETE"] = "Executa automaticamente quando o fluxo de trabalho é concluído";
?>