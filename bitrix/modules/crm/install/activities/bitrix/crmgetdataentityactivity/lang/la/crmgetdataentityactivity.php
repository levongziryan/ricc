<?
$MESS["CRM_ACTIVITY_LABLE_SELECT_FIELDS"] = "Seleccionar campos:";
$MESS["CRM_ACTIVITY_LABLE_PRINTABLE_VERSION"] = "Versión impresa:";
$MESS["CRM_ACTIVITY_ERROR_DT"] = "Tipo de documento incorrecto.";
$MESS["CRM_ACTIVITY_ERROR_FIELD_REQUIED"] = "El campo '#FIELD#' es requerido";
$MESS["CRM_ACTIVITY_ERROR_FIELD_TYPE"] = "El campo '#FIELD#' tipo especificado es incorrecto.";
$MESS["CRM_ACTIVITY_ERROR_ENTITY_ID"] = "ID de la entidad";
$MESS["CRM_ACTIVITY_ERROR_ENTITY_TYPE"] = "Tipo de entidad";
$MESS["CRM_ACTIVITY_ERROR_ENTITY_LIST_FIELDS"] = "Seleccione el campo entidad.";
$MESS["CRM_ACTIVITY_FIELD_MAIN_YES"] = "Si";
$MESS["CRM_ACTIVITY_FIELD_MAIN_NO"] = "No";
$MESS["CRM_ACTIVITY_FIELD_USER_TITLE"] = "(Usuario)";
?>