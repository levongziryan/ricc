<?
$MESS["CRM_TAB_1"] = "Lead";
$MESS["CRM_TAB_1_TITLE"] = "Propriedades do lead";
$MESS["CRM_TAB_2"] = "Contatos";
$MESS["CRM_TAB_2_TITLE"] = "Contatos do lead";
$MESS["CRM_TAB_3"] = "Empresas";
$MESS["CRM_TAB_3_TITLE"] = "Empresas do lead";
$MESS["CRM_TAB_4"] = "Negócios";
$MESS["CRM_TAB_4_TITLE"] = "Negócios do lead";
$MESS["CRM_TAB_5"] = "Registro";
$MESS["CRM_TAB_5_TITLE"] = "Registro do lead";
$MESS["CRM_TAB_6"] = "Atividade";
$MESS["CRM_TAB_6_TITLE"] = "Atividade do Lead";
$MESS["CRM_TAB_7"] = "Processo de Negócio";
$MESS["CRM_TAB_7_TITLE"] = "Processos de negócio do lead";
$MESS["CRM_TAB_8"] = "Cotações";
$MESS["CRM_TAB_8_TITLE"] = "Cotações de clientes potenciais";
$MESS["CRM_EDIT_BTN_TTL"] = "Clique para editar";
$MESS["CRM_LOCK_BTN_TTL"] = "Não é possível editar este item";
$MESS["CRM_LEAD_SIDEBAR_TITLE"] = "Informações do lead";
$MESS["CRM_LEAD_SIDEBAR_STATUS"] = "Status";
$MESS["CRM_LEAD_STATUS_UNDEF"] = "[não definido]";
$MESS["CRM_LEAD_SOURCE_UNDEF"] = "[indefinido]";
$MESS["CRM_LEAD_OPENED"] = "Esta lead pode ser visto por todos.";
$MESS["CRM_LEAD_NOT_OPENED"] = "Este lead não é público.";
$MESS["CRM_LEAD_RESPONSIBLE"] = "Pessoa responsável";
$MESS["CRM_LEAD_RESPONSIBLE_CHANGE"] = "alterar";
$MESS["CRM_LEAD_OPPORTUNITY"] = "Montante da oportunidade";
$MESS["CRM_LEAD_SOURCE"] = "Fonte";
$MESS["CRM_LEAD_SOURCE_DESCRIPTION"] = "Fonte de informação";
$MESS["CRM_LEAD_DATE_CREATE"] = "Criado em";
$MESS["CRM_LEAD_DATE_MODIFY"] = "Modificada em";
$MESS["CRM_LEAD_CREATOR"] = "Criado por";
$MESS["CRM_LEAD_MODIFIER"] = "Editado por";
$MESS["CRM_LEAD_BIZPROC_LIST"] = "Processo de Negócio";
$MESS["CRM_LEAD_EVENT_LIST"] = "Alterações recentes";
$MESS["CRM_LEAD_SHOW_TITLE"] = "Lead ##ID# &mdash; #TITLE#";
$MESS["CRM_LEAD_SHOW_LEGEND_EXTERNAL"] = "Leads da loja virtual";
$MESS["CRM_LEAD_SHOW_NAVIGATION_PREV"] = "Anterior";
$MESS["CRM_LEAD_SHOW_NAVIGATION_NEXT"] = "Próximo";
$MESS["CRM_LEAD_GET_USER_INFO_GENERAL_ERROR"] = "Não é possível recuperar informação do usuário.";
$MESS["CRM_SECTION_LEAD"] = "Informações do lead";
$MESS["CRM_TAB_EVENT"] = "Alterar registro";
$MESS["CRM_TAB_EVENT_TITLE"] = "Registro de mudanças do lead";
$MESS["CRM_TAB_HISTORY"] = "Histórico";
$MESS["CRM_TAB_HISTORY_TITLE"] = "Registro de mudanças do lead";
$MESS["CRM_TAB_DETAILS"] = "Detalhes";
$MESS["CRM_TAB_DETAILS_TITLE"] = "Detalhes";
$MESS["CRM_TAB_PRODUCT_ROWS"] = "Produtos";
$MESS["CRM_TAB_PRODUCT_ROWS_TITLE"] = "Produtos do lead";
$MESS["CRM_LEAD_COMMENT"] = "Comentário";
$MESS["CRM_LEAD_POST_COMPANY"] = "#COMPANY# (#POST#)";
$MESS["CRM_LEAD_SHOW_LEGEND"] = "lead ##ID#";
$MESS["CRM_LEAD_OPPORTUNITY_SHORT"] = "total";
$MESS["CRM_LEAD_CLIENT"] = "Cliente";
$MESS["CRM_LEAD_PHONE"] = "Telefone";
$MESS["CRM_LEAD_EMAIL"] = "Email";
$MESS["CRM_LEAD_IM"] = "Messenger";
$MESS["CRM_LEAD_CLIENT_NOT_ASSIGNED"] = "[não atribuído]";
$MESS["CRM_TAB_LIVE_FEED"] = "Fluxo";
$MESS["CRM_TAB_LIVE_FEED_TITLE"] = "Fluxo";
$MESS["CRM_LEAD_CONV_ACCESS_DENIED"] = "Você precisa de permissão de criação de contato, empresa e venda para converter um cliente potencial.";
$MESS["CRM_LEAD_CONV_GENERAL_ERROR"] = "Erro de conversão genérico.";
$MESS["CRM_LEAD_CONV_DIALOG_TITLE"] = "Converter Cliente Potencial";
$MESS["CRM_LEAD_CONV_DIALOG_CONTINUE_BTN"] = "Continuar";
$MESS["CRM_LEAD_CONV_DIALOG_CANCEL_BTN"] = "Cancelar";
$MESS["CRM_LEAD_CONV_DIALOG_SYNC_LEGEND"] = "As entidades selecionadas não têm campos para os quais os dados de cliente potencial podem ser passados. Escolha entidades nas quais campos adicionais serão criados para salvar todas as informações disponíveis. ";
$MESS["CRM_LEAD_CONV_DIALOG_SYNC_FILED_LIST_TITLE"] = "Quais campos serão criados";
$MESS["CRM_LEAD_CONV_DIALOG_SYNC_ENTITY_LIST_TITLE"] = "Escolha as entidades em que os campos adicionais serão criados";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_CONTACT"] = "Contatos";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_COMPANY"] = "Empresas";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_BTN"] = "Selecionar";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_SEARCH"] = "Pesquisar";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_SEARCH_NO_RESULT"] = "Não foram encontradas entradas.";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_LAST"] = "Última";
$MESS["CRM_LEAD_CONV_OPEN_ENTITY_SEL"] = "Selecionar da lista...";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_TITLE"] = "Selecionar contato e empresa";
$MESS["CRM_LEAD_SHOW_CONV_DEAL_CATEGORY_DLG_TITLE"] = "Preferências da negociação";
$MESS["CRM_LEAD_SHOW_CONV_DEAL_CATEGORY_DLG_FIELD"] = "Pipeline";
$MESS["CRM_LEAD_SHOW_BUTTON_SAVE"] = "Salvar";
$MESS["CRM_LEAD_SHOW_BUTTON_CANCEL"] = "Cancelar";
$MESS["CRM_TAB_TREE"] = "Dependências";
$MESS["CRM_TAB_TREE_TITLE"] = "Links para outras entidades e itens";
$MESS["CRM_TAB_AUTOMATION"] = "Automação";
$MESS["CRM_TAB_AUTOMATION_TITLE"] = "Automação. Regras e gatilhos";
?>