<?
$MESS["CRM_COLUMN_NAME"] = "Prénom";
$MESS["CRM_COLUMN_LAST_NAME"] = "Nom";
$MESS["CRM_COLUMN_SECOND_NAME"] = "Deuxième prénom";
$MESS["CRM_COLUMN_PHONE"] = "Numéro de téléphone";
$MESS["CRM_COLUMN_EMAIL"] = "Courrier électronique";
$MESS["CRM_COLUMN_STATUS"] = "Statut";
$MESS["CRM_COLUMN_DATE_MODIFY"] = "Date de modification";
$MESS["CRM_SECTION_CONTACT_INFO"] = "Information de contact";
$MESS["CRM_OPER_SHOW"] = "Affichage";
$MESS["CRM_OPER_EDIT"] = "Editer";
$MESS["CRM_COLUMN_PRODUCTS"] = "Produits";
$MESS["CRM_SIP_MGR_UNKNOWN_RECIPIENT"] = "Appelant inconnu";
$MESS["CRM_SIP_MGR_MAKE_CALL"] = "Appel";
?>