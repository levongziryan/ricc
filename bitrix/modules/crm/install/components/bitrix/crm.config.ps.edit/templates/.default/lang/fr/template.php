<?
$MESS["CRM_PS_TYPES_OTHER"] = "Autre";
$MESS["CRM_PS_TYPES_ORDER"] = "Paramètre de la commande";
$MESS["CRM_PS_TYPES_USER"] = "Paramètre de l'utilisateur";
$MESS["CRM_TAB_1"] = "Mode de paiement";
$MESS["CRM_PS_SHOW_FIELDS"] = "Montrer tous les champs";
$MESS["CRM_TAB_1_TITLE"] = "Propriétés du système de paiement";
$MESS["CRM_PS_TYPES_PROPERTY"] = "Propriété de la commande";
$MESS["CRM_PS_HIDE_FIELDS"] = "Cacher les champs superflus";
$MESS["CRM_PS_TYPES_REQUISITE"] = "Données de contact ou de société";
$MESS["CRM_PS_TYPES_PAYMENT"] = "Paiement";
$MESS["CRM_PS_TYPES_BANK_DETAIL"] = "Informations bancaires";
$MESS["CRM_PS_TYPES_CRM_COMPANY"] = "Entreprise";
$MESS["CRM_PS_TYPES_CRM_CONTACT"] = "Contact";
$MESS["CRM_PS_TYPES_MC_REQUISITE"] = "Informations du vendeur";
$MESS["CRM_PS_TYPES_MC_BANK_DETAIL"] = "Informations bancaires du vendeur";
$MESS["CRM_PS_TYPES_CRM_MYCOMPANY"] = "Entreprise";
$MESS["CRM_PS_FIELD_NAME"] = "Prénom";
$MESS["CRM_PS_FIELD_ACTIVE"] = "Actif";
$MESS["CRM_PS_FIELD_SORT"] = "Trier";
$MESS["CRM_PS_FIELD_NEW_WINDOW"] = "Ouvrir dans une nouvelle fenêtre";
$MESS["CRM_PS_FIELD_ENCODING"] = "Jeu de caractères";
$MESS["CRM_PS_FIELD_PERSON_TYPE_ID"] = "Type de client";
$MESS["CRM_PS_FIELD_DESCRIPTION"] = "Description";
$MESS["CRM_PS_FIELD_ACTION_FILE"] = "Handler";
$MESS["CRM_PS_FIELD_LOGOTIP"] = "Logo du système de paiement";
$MESS["CRM_BUTTON_FORM_SAVE"] = "Enregistrer";
$MESS["CRM_BUTTON_FORM_SAVE_TITLE"] = "Enregistrer et revenir";
$MESS["CRM_BUTTON_FORM_APPLY"] = "Appliquer";
$MESS["CRM_BUTTON_FORM_APPLY_TITLE"] = "Enregistrer et rester ici";
$MESS["CRM_BUTTON_FORM_CANCEL"] = "Annuler";
$MESS["CRM_BUTTON_FORM_CANCEL_TITLE"] = "Ne pas enregistrer et revenir";
$MESS["CRM_COLUMN_NAME"] = "Titre de colonne";
$MESS["CRM_COLUMN_SORT"] = "Trier";
$MESS["CRM_COLUMN_ACTIVE"] = "Actif";
$MESS["CRM_PROP_ALREADY_EXIST"] = "La propriété existe déjà";
$MESS["CRM_ADD_USER_PROP"] = "Ajouter une propriété";
$MESS["CRM_PS_MODE_LIST"] = "Types de système de paiement";
$MESS["CRM_PS_GENERATE_SUCCESS"] = "La clé a été générée.";
$MESS["CRM_PS_FIELD_PS_MODE"] = "Types de système de paiement";
?>