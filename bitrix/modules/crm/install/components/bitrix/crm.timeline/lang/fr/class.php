<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n’est pas installé.";
$MESS["CRM_TIMELINE_ENTITY_TYPE_NOT_ASSIGNED"] = "Le type d'entité n'est pas spécifié..";
$MESS["CRM_TIMELINE_ENTITY_NOT_ASSIGNED"] = "L'ID de l'entité n'est pas spécifié.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
?>