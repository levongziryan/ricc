<?
$MESS["CRM_PERMISSION_DENIED"] = "Доступ заборонено";
$MESS["CRM_WAIT_ACTION_INVALID_REQUEST_DATA"] = "Запит містить некоректні параметри.";
$MESS["CRM_WAIT_ACTION_ITEM_NOT_FOUND"] = "Елемент не знайдений";
$MESS["CRM_WAIT_ACTION_INVALID_BEFORE_PARAMS"] = "Дата завершення очікування повинна бути більше поточної дати. Можливо обрана дата вже пройшла або кількість днів до неї занадто велика.";
?>