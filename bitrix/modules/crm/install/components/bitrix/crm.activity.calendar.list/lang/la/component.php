<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CALENDAR_MODULE_NOT_INSTALLED"] = "El módulo Calendarios no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_NAME"] = "Nombre";
$MESS["CRM_COLUMN_ENTITY_TYPE"] = "Tipo";
$MESS["CRM_COLUMN_ENTITY_TITLE"] = "Título";
$MESS["CRM_COLUMN_ASSIGNED_BY"] = "Persona responsable";
$MESS["CRM_COLUMN_DATE_START"] = "Fecha de Inicio";
$MESS["CRM_COLUMN_DATE_END"] = "Fecha de finalización";
$MESS["CRM_COLUMN_DESCRIPTION"] = "Descripción";
$MESS["CRM_COLUMN_UF_CRM_CAL_EVENT"] = "Elemento CRM";
$MESS["CRM_PRESET_NEW"] = "Nuevas llamadas";
$MESS["CRM_PRESET_MY"] = "Mis llamadas";
$MESS["CRM_ENTITY_TYPE_LEAD"] = "Prospecto";
$MESS["CRM_ENTITY_TYPE_CONTACT"] = "Contacto";
$MESS["CRM_ENTITY_TYPE_COMPANY"] = "Compañía";
$MESS["CRM_ENTITY_TYPE_DEAL"] = "Negociación";
?>