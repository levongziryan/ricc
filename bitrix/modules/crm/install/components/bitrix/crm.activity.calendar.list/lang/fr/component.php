<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CALENDAR_MODULE_NOT_INSTALLED"] = "Module des calendriers non installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_NAME"] = "Dénomination";
$MESS["CRM_COLUMN_ENTITY_TYPE"] = "Entité";
$MESS["CRM_COLUMN_ENTITY_TITLE"] = "Titre";
$MESS["CRM_COLUMN_ASSIGNED_BY"] = "Responsable";
$MESS["CRM_COLUMN_DATE_START"] = "A partir de la date";
$MESS["CRM_COLUMN_DATE_END"] = "Date de clôture";
$MESS["CRM_COLUMN_DESCRIPTION"] = "Description";
$MESS["CRM_COLUMN_UF_CRM_CAL_EVENT"] = "Elément CRM";
$MESS["CRM_PRESET_NEW"] = "Nouveaux appels";
$MESS["CRM_PRESET_MY"] = "Mes appels";
$MESS["CRM_ENTITY_TYPE_LEAD"] = "Prospect";
$MESS["CRM_ENTITY_TYPE_CONTACT"] = "Client";
$MESS["CRM_ENTITY_TYPE_COMPANY"] = "Entreprise";
$MESS["CRM_ENTITY_TYPE_DEAL"] = "Affaire";
?>