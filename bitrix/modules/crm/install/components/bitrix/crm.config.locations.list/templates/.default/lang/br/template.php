<?
$MESS["CRM_LOC_EDIT_TITLE"] = "Abrir esta localização para edição";
$MESS["CRM_LOC_EDIT"] = "Editar localização";
$MESS["CRM_LOC_DELETE_TITLE"] = "Deletar esta localização";
$MESS["CRM_LOC_DELETE"] = "Deletar localização";
$MESS["CRM_LOC_DELETE_CONFIRM"] = "Você tem certeza que deseja deletar as '%s'?";
$MESS["CRM_ALL"] = "Total";
?>