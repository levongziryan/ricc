<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_LOC_DELETE_CONFIRM"] = "Tes-vous sûr de vouloir supprimer '%s'?";
$MESS["CRM_LOC_EDIT_TITLE"] = "Passer sur la page d'édition de cet emplacement";
$MESS["CRM_LOC_EDIT"] = "Editer la localisation";
$MESS["CRM_LOC_DELETE"] = "Annuler la localisation";
$MESS["CRM_LOC_DELETE_TITLE"] = "Supprimer ce lieu";
?>