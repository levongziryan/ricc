<?
$MESS["CRM_COLUMN_DATE"] = "Date de modification";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_CODE"] = "Code mnémonique";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "Le module 'Boutique en ligne' n'a pas été installé.";
$MESS["CRM_COLUMN_NAME"] = "Nom / description";
$MESS["CRM_TAX_UPDATE_GENERAL_ERROR"] = "Lors de la mise à jour du montant de l'impôt une erreur s'est produite.";
$MESS["CRM_TAX_DELETION_GENERAL_ERROR"] = "Erreur au cours de la suppression d'un impôt";
$MESS["CRM_COLUMN_SITE"] = "Site web";
$MESS["CRM_COLUMN_RATES"] = "Liste des taux d'impôt";
?>