<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "El módulo e-Store no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_DATE"] = "Modificado el ";
$MESS["CRM_COLUMN_SITE"] = "Sitio web";
$MESS["CRM_COLUMN_NAME"] = "Nombre/Descripción";
$MESS["CRM_COLUMN_CODE"] = "código mnemotécnico";
$MESS["CRM_COLUMN_RATES"] = "Tasas de impuestos";
$MESS["CRM_TAX_DELETION_GENERAL_ERROR"] = "Error al eliminar el impuesto.";
$MESS["CRM_TAX_UPDATE_GENERAL_ERROR"] = "Error al actualizar el impuesto.";
?>