<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_TAX_DELETE_CONFIRM"] = "Tes-vous sûr de vouloir supprimer '%s'?";
$MESS["CRM_TAX_LOCATIONS_IMPORT"] = "Charger";
$MESS["CRM_TAX_LOCATIONS"] = "Emplacements";
$MESS["CRM_TAX_LOCATIONS_CONTENT"] = "S'il vous plaît créer ou importer endroits avant la création de taxes.";
$MESS["CRM_TAX_SHOW_TITLE"] = "Passer à la page de consultation de ce taux de TVA";
$MESS["CRM_TAX_EDIT_TITLE"] = "Accéder à la page de l'édition de cet impôt";
$MESS["CRM_TAX_SHOW"] = "Passage à l'examen de la taxe";
$MESS["CRM_TAX_EDIT"] = "Modifier l'impôt";
$MESS["CRM_TAX_LOCATIONS_CREATE"] = "Ajouter";
$MESS["CRM_TAX_DELETE"] = "Eliminer l'impôt";
$MESS["CRM_TAX_DELETE_TITLE"] = "Supprimer cet impôt";
$MESS["CRM_TAX_LOCATIONS_REDIRECT"] = "Aller";
?>