<?
$MESS["CRM_WEBFORM_SCRIPT_BTN_COPY"] = "copier";
$MESS["CRM_WEBFORM_SCRIPT_BTN_EDIT"] = "configurer";
$MESS["CRM_WEBFORM_SCRIPT_TAB_ONPAGE"] = "Sur la page";
$MESS["CRM_WEBFORM_SCRIPT_TAB_WINDOW"] = "Sur la fenêtre";
$MESS["CRM_WEBFORM_SCRIPT_LINK"] = "Lien";
$MESS["CRM_WEBFORM_SCRIPT_INLINE"] = "Sur la page";
$MESS["CRM_WEBFORM_SCRIPT_WINDOW_BUTTON"] = "Fenêtre contextuelle - clic de bouton";
$MESS["CRM_WEBFORM_SCRIPT_WINDOW_LINK"] = "Fenêtre contextuelle - lien";
$MESS["CRM_WEBFORM_SCRIPT_WINDOW_DELAY"] = "Fenêtre contextuelle - afficher après délai";
$MESS["CRM_WEBFORM_SCRIPT_SEND_TO_EMAIL"] = "envoyer le code sur l'e-mail";
$MESS["CRM_WEBFORM_SCRIPT_WINDOW_INLINE"] = "Le formulaire sera incorporé dans votre page";
$MESS["CRM_WEBFORM_SCRIPT_TAB_SCRIPT_INLINE"] = "Embarqué";
$MESS["CRM_WEBFORM_SCRIPT_TAB_SCRIPT_BUTTON"] = "Bouton";
$MESS["CRM_WEBFORM_SCRIPT_TAB_SCRIPT_LINK"] = "Lien";
$MESS["CRM_WEBFORM_SCRIPT_TAB_SCRIPT_DELAY"] = "Retardé";
$MESS["CRM_WEBFORM_SCRIPT_COPY_ONE_LINE"] = "une ligne";
$MESS["CRM_WEBFORM_SCRIPT_SCRIPT_ON_SITE"] = "Code embarqué";
?>