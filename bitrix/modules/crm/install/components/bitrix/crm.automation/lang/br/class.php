<?
$MESS["CRM_AUTOMATION_TITLE"] = "Automação";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "O módulo Fluxos de Trabalho não está instalado.";
$MESS["CRM_AUTOMATION_NOT_SUPPORTED"] = "O componente não suporta esta entidade CRM.";
$MESS["CRM_AUTOMATION_ACCESS_DENIED"] = "O acesso à entidade foi negado.";
$MESS["CRM_AUTOMATION_NOT_AVAILABLE"] = "A automação não está disponível";
$MESS["CRM_AUTOMATION_TO_HEAD"] = "Para supervisor";
$MESS["CRM_AUTOMATION_NOT_AVAILABLE_SIMPLE_CRM"] = "Automação não está disponível no modo CRM Simples";
?>