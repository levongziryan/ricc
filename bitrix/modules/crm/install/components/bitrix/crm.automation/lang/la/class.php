<?
$MESS["CRM_AUTOMATION_TITLE"] = "Automatización";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "El módulo Workflows no está instalado.";
$MESS["CRM_AUTOMATION_NOT_SUPPORTED"] = "El componente no admite esta entidad en el CRM.";
$MESS["CRM_AUTOMATION_ACCESS_DENIED"] = "Se denegó el acceso a la entidad.";
$MESS["CRM_AUTOMATION_NOT_AVAILABLE"] = "La automatización no está disponible";
$MESS["CRM_AUTOMATION_TO_HEAD"] = "Al supervisor";
$MESS["CRM_AUTOMATION_NOT_AVAILABLE_SIMPLE_CRM"] = "La automatización no está disponible en el modo CRM simple";
?>