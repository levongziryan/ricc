<?
$MESS["CRM_AUTOMATION_TITLE"] = "Automatisation";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "Le module Flux de travail n'est pas installé.";
$MESS["CRM_AUTOMATION_NOT_SUPPORTED"] = "Le composant ne supporte pas cette entité CRM.";
$MESS["CRM_AUTOMATION_ACCESS_DENIED"] = "L'accès à l'entité a été refusé.";
$MESS["CRM_AUTOMATION_NOT_AVAILABLE"] = "L'automatisation n'est pas disponible";
$MESS["CRM_AUTOMATION_TO_HEAD"] = "Pour le superviseur";
$MESS["CRM_AUTOMATION_NOT_AVAILABLE_SIMPLE_CRM"] = "L'automatisation n'est pas disponible dans le mode CRM simple";
?>