<?
$MESS["CRM_AUTOMATION_CMP_TRIGGER_LIST"] = "Disparador";
$MESS["CRM_AUTOMATION_CMP_ROBOT_LIST"] = "Reglas de automatización";
$MESS["CRM_AUTOMATION_CMP_SAVE"] = "Guardar";
$MESS["CRM_AUTOMATION_CMP_CANCEL"] = "Cancelar";
$MESS["CRM_AUTOMATION_CMP_VIEW"] = "Ver";
$MESS["CRM_AUTOMATION_CMP_AUTOMATION_EDIT"] = "Configurar reglas de automatización";
$MESS["CRM_AUTOMATION_CMP_ADD"] = "agregar";
$MESS["CRM_AUTOMATION_CMP_EXTERNAL_EDIT"] = "Editar en el Diseñador del Flujo de Trabajo";
$MESS["CRM_AUTOMATION_CMP_TO_EXECUTE"] = "Ejecutar";
$MESS["CRM_AUTOMATION_CMP_AT_ONCE"] = "inmediatamente";
$MESS["CRM_AUTOMATION_CMP_TO"] = "para";
$MESS["CRM_AUTOMATION_CMP_AUTOMATICALLY"] = "Automáticamente";
$MESS["CRM_AUTOMATION_CMP_RESPONSIBLE"] = "Persona responsable";
$MESS["CRM_AUTOMATION_CMP_THROUGH"] = "en";
$MESS["CRM_AUTOMATION_CMP_AFTER_PREVIOUS"] = "después del anterior";
$MESS["CRM_AUTOMATION_CMP_AFTER_PREVIOUS_WIDE"] = "después de la siguiente regla de automatización";
$MESS["CRM_AUTOMATION_CMP_EDIT"] = "cambiar";
$MESS["CRM_AUTOMATION_CMP_CHOOSE_TIME"] = "fijar tiempo";
$MESS["CRM_AUTOMATION_CMP_INTERVAL_M"] = "minutos";
$MESS["CRM_AUTOMATION_CMP_INTERVAL_H"] = "horas";
$MESS["CRM_AUTOMATION_CMP_INTERVAL_D"] = "días";
$MESS["CRM_AUTOMATION_CMP_BEFORE_DATE"] = "anterior";
$MESS["CRM_AUTOMATION_CMP_BEFORE"] = "hasta";
$MESS["CRM_AUTOMATION_CMP_CHOOSE_DATE_FIELD"] = "seleccione el campo de fecha";
$MESS["CRM_AUTOMATION_CMP_FOR_TIME"] = "antes de";
$MESS["CRM_AUTOMATION_CMP_CHOOSE"] = "seleccionar";
$MESS["CRM_AUTOMATION_CMP_EXTERNAL_EDIT_TEXT"] = "Las reglas de automatización sólo se pueden configurar en Workflow Designer.";
$MESS["CRM_AUTOMATION_CMP_NEED_SAVE"] = "Los cambios no se han guardado.";
$MESS["CRM_AUTOMATION_CMP_DAY1"] = "día";
$MESS["CRM_AUTOMATION_CMP_DAY2"] = "días";
$MESS["CRM_AUTOMATION_CMP_DAY3"] = "días";
$MESS["CRM_AUTOMATION_CMP_HOUR1"] = "hora";
$MESS["CRM_AUTOMATION_CMP_HOUR2"] = "horas";
$MESS["CRM_AUTOMATION_CMP_HOUR3"] = "horas";
$MESS["CRM_AUTOMATION_CMP_MIN1"] = "minuto";
$MESS["CRM_AUTOMATION_CMP_MIN2"] = "minutos";
$MESS["CRM_AUTOMATION_CMP_MIN3"] = "minutos";
$MESS["CRM_AUTOMATION_CMP_TRIGGER_NAME"] = "Nombre del disparador";
$MESS["CRM_AUTOMATION_CMP_USER_SELECTOR_TAB"] = "Documento";
$MESS["CRM_AUTOMATION_CMP_WEBHOOK_ID"] = "{{ID}} es el ID de la entidad";
$MESS["CRM_AUTOMATION_CMP_TRIGGER_HELP"] = "Acción del lado del cliente que activará el procesamiento del estado. Cuando se establece un disparador, el documento se llevará a un estado requerido, lo que a su vez activará las reglas de automatización apropiadas.

Los desencadenadores no respetan los estados anteriores a los que podría haber estado una entidad de CRM. Mover una entidad a un estado superior no activará activadores conectados a estados inferiores aunque nunca se hayan activado para una entidad actual.";
$MESS["CRM_AUTOMATION_CMP_ROBOT_HELP"] = "Utilice reglas de automatización para agilizar el proceso de administración del cliente y ayudar a los empleados a adherirse al algoritmo de procesamiento del prospecto. Las reglas de automatización llevarán a cabo acciones de forma desatendida: enviar correos electrónicos, hacer llamadas, etc.

Una regla de automatización está vinculada a un estado específico. Tan pronto como una entidad CRM pasa a un nuevo estado, se activa una regla de automatización especificada para ese estado. Las reglas de automatización pueden especificar el tiempo de activación y una acción: tarea, notificación, correo electrónico, llamada u otra acción de su elección.";
$MESS["CRM_AUTOMATION_CMP_DELAY_AFTER_HELP"] = "Intervalo de retraso antes de que la regla se aplique, después de que una entidad del CRM haya cambiado de estado.";
$MESS["CRM_AUTOMATION_CMP_DELAY_BEFORE_HELP"] = "La regla se aplicará en el período de tiempo especificado antes de la fecha especificada en la entidad CRM. Por ejemplo, puede enviar un recordatorio de pago un día antes de la fecha de vencimiento.";
$MESS["CRM_AUTOMATION_CMP_IS_DEMO"] = "(pronto)";
$MESS["CRM_AUTOMATION_CMP_TITLE_LEAD_VIEW"] = "Etapa actual del prospecto: \"#TITLE#\"";
$MESS["CRM_AUTOMATION_CMP_TITLE_LEAD_EDIT"] = "Establecer reglas de automatización para todos los prospectos";
$MESS["CRM_AUTOMATION_CMP_TITLE_DEAL_VIEW"] = "Etapa actual de la negociación: \"#TITLE#\"";
$MESS["CRM_AUTOMATION_CMP_TITLE_DEAL_EDIT"] = "Establecer reglas de automatización para todas las negociaciones en el pipeline";
$MESS["CRM_AUTOMATION_TRIGGER_WEBFORM_LABEL"] = "Seleccionar formulario";
$MESS["CRM_AUTOMATION_TRIGGER_OPENLINE_LABEL"] = "Seleccione Canales Abiertos";
$MESS["CRM_AUTOMATION_TRIGGER_WEBFORM_ANY"] = "todo";
$MESS["CRM_AUTOMATION_ROBOT_CATEGORY_EMPLOYEE"] = "Para empleado";
$MESS["CRM_AUTOMATION_ROBOT_CATEGORY_CLIENT"] = "Para comunicación con el cliente";
$MESS["CRM_AUTOMATION_ROBOT_CATEGORY_ADS"] = "Publicidad";
$MESS["CRM_AUTOMATION_ROBOT_CATEGORY_OTHER"] = "Reglas de automatización personalizadas";
$MESS["CRM_AUTOMATION_ROBOT_CATEGORY_OTHER_MARKETPLACE"] = "Aplicaciones24";
$MESS["CRM_AUTOMATION_ROBOT_CONDITION"] = "Condición";
$MESS["CRM_AUTOMATION_ROBOT_CONDITION_EMPTY"] = "sin condición";
$MESS["CRM_AUTOMATION_ROBOT_CONDITION_EQ"] = "iguales";
$MESS["CRM_AUTOMATION_ROBOT_CONDITION_GT"] = "más que";
$MESS["CRM_AUTOMATION_ROBOT_CONDITION_GTE"] = "no menor que";
$MESS["CRM_AUTOMATION_ROBOT_CONDITION_LT"] = "menos que";
$MESS["CRM_AUTOMATION_ROBOT_CONDITION_LTE"] = "no más que";
$MESS["CRM_AUTOMATION_ROBOT_CONDITION_NE"] = "no igual a";
$MESS["CRM_AUTOMATION_ROBOT_CONDITION_IN"] = "es en";
$MESS["CRM_AUTOMATION_ROBOT_CONDITION_CONTAIN"] = "contiene";
$MESS["CRM_AUTOMATION_CMP_BY_CONDITION"] = "por condición";
$MESS["CRM_AUTOMATION_CMP_DISK_ATTACH_FILE"] = "Adjuntar archivos";
$MESS["CRM_AUTOMATION_CMP_DISK_ATTACHED_FILES"] = "Archivos adjuntos";
$MESS["CRM_AUTOMATION_CMP_DISK_SELECT_FILE"] = "Buscar Bitrix24";
$MESS["CRM_AUTOMATION_CMP_DISK_SELECT_FILE_LEGEND"] = "Abra la ventana Bitrix24.Drive";
$MESS["CRM_AUTOMATION_CMP_DISK_UPLOAD_FILE"] = "Cargar archivo";
$MESS["CRM_AUTOMATION_CMP_DISK_UPLOAD_FILE_LEGEND"] = "Agregar usando arrastrar y soltar";
$MESS["CRM_AUTOMATION_CMP_EXTERNAL_EDIT_LOCKED"] = "Las reglas de automatización personalizadas se pueden crear y editar en el Diseñador del Flujo de Trabajo solo en el plan Profesional. <br><br>
Actualícese a este plan para disfrutar de otras herramientas comerciales útiles:<br>
<ul class=\"hide-features-list\">
	<li class=\"hide-features-list-item\">Tarjeta de negocios ilimitada OCR, formularios del CRM</li>
	<li class=\"hide-features-list-item\">Gestión de tiempo de trabajo y reportes de trabajo</li>
	<li class=\"hide-features-list-item\">Sitios de aterrizaje ilimitados para ampliar su base de clientes</li>
</ul>
Consulte la tabla de comparación de planes y la descripción completa de las funciones <a href=\"https://www.bitrix24.com/prices/index.php\">aquí</a>.";
?>