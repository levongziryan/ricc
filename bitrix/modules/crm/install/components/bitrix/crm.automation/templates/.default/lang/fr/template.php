<?
$MESS["CRM_AUTOMATION_CMP_TRIGGER_LIST"] = "Déclencheurs";
$MESS["CRM_AUTOMATION_CMP_ROBOT_LIST"] = "Règles d'automatisation";
$MESS["CRM_AUTOMATION_CMP_SAVE"] = "Enregistrer";
$MESS["CRM_AUTOMATION_CMP_CANCEL"] = "Annuler";
$MESS["CRM_AUTOMATION_CMP_VIEW"] = "Afficher";
$MESS["CRM_AUTOMATION_CMP_AUTOMATION_EDIT"] = "Configurer les règles d'automatisation";
$MESS["CRM_AUTOMATION_CMP_ADD"] = "ajouter";
$MESS["CRM_AUTOMATION_CMP_EXTERNAL_EDIT"] = "Modifier dans le Créateur de Flux de travail";
$MESS["CRM_AUTOMATION_CMP_TO_EXECUTE"] = "Exécuter";
$MESS["CRM_AUTOMATION_CMP_AT_ONCE"] = "immédiatement";
$MESS["CRM_AUTOMATION_CMP_TO"] = "à";
$MESS["CRM_AUTOMATION_CMP_AUTOMATICALLY"] = "Automatiquement";
$MESS["CRM_AUTOMATION_CMP_RESPONSIBLE"] = "Personne responsable";
$MESS["CRM_AUTOMATION_CMP_THROUGH"] = "dans";
$MESS["CRM_AUTOMATION_CMP_AFTER_PREVIOUS"] = "après le précédent";
$MESS["CRM_AUTOMATION_CMP_EDIT"] = "modifier";
$MESS["CRM_AUTOMATION_CMP_CHOOSE_TIME"] = "définir l'heure";
$MESS["CRM_AUTOMATION_CMP_INTERVAL_M"] = "minutes";
$MESS["CRM_AUTOMATION_CMP_INTERVAL_H"] = "heures";
$MESS["CRM_AUTOMATION_CMP_INTERVAL_D"] = "jours";
$MESS["CRM_AUTOMATION_CMP_BEFORE_DATE"] = "précédent";
$MESS["CRM_AUTOMATION_CMP_BEFORE"] = "jusqu'à";
$MESS["CRM_AUTOMATION_CMP_CHOOSE_DATE_FIELD"] = "sélectionner le champ de date";
$MESS["CRM_AUTOMATION_CMP_FOR_TIME"] = "avant";
$MESS["CRM_AUTOMATION_CMP_CHOOSE"] = "sélectionner";
$MESS["CRM_AUTOMATION_CMP_EXTERNAL_EDIT_TEXT"] = "Les règles d'automatisation peuvent être configurées dans le Créateur de Flux de travail uniquement.";
$MESS["CRM_AUTOMATION_CMP_NEED_SAVE"] = "Les modifications n'ont pas été sauvegardées.";
$MESS["CRM_AUTOMATION_CMP_DAY1"] = "jour";
$MESS["CRM_AUTOMATION_CMP_DAY2"] = "jours";
$MESS["CRM_AUTOMATION_CMP_DAY3"] = "jours";
$MESS["CRM_AUTOMATION_CMP_HOUR1"] = "heure";
$MESS["CRM_AUTOMATION_CMP_HOUR2"] = "heures";
$MESS["CRM_AUTOMATION_CMP_HOUR3"] = "heures";
$MESS["CRM_AUTOMATION_CMP_MIN1"] = "minute";
$MESS["CRM_AUTOMATION_CMP_MIN2"] = "minutes";
$MESS["CRM_AUTOMATION_CMP_MIN3"] = "minutes";
$MESS["CRM_AUTOMATION_CMP_TRIGGER_NAME"] = "Nom du déclencheur";
$MESS["CRM_AUTOMATION_CMP_USER_SELECTOR_TAB"] = "Document";
$MESS["CRM_AUTOMATION_CMP_WEBHOOK_ID"] = "{{ID}} est l'ID de l'entité";
$MESS["CRM_AUTOMATION_CMP_TRIGGER_HELP"] = "Action du côté client qui enclenchera le traitement du statut.
Quand un déclencheur est activé, le document obtiendra un statut requis, ce qui à son tour activera les règles d'automatisation adéquates.
Les déclencheurs ne respectent pas les statuts qu'une entité CRM a pu avoir auparavant.
Déplacer une entité CRM vers un statut supérieur n'activera pas les déclencheurs liés aux statuts inférieurs, même s'ils n'ont jamais été activés pour une entité actuelle.";
$MESS["CRM_AUTOMATION_CMP_ROBOT_HELP"] = "Utilisez les règles d'automatisation afin de faciliter le processus de gestion des clients et d'aider les employés à adhérer à l'algorithme de traitement des clients potentiels.
Les règles d'automatisation effectueront des actions de manière automatisée: envoi d'e-mails, réalisation d'appels, etc.
Une règle d'automatisation est liée à un statut spécifique.
Dès qu'une entrée CRM obtient un nouveau statut, une règle d'automatisation spécifique à ce statut est activée.
Les règles d'automatisation peuvent préciser le temps d'activation et l'action: tâche, notification, e-mail, appel ou toute autre action de votre choix.";
$MESS["CRM_AUTOMATION_CMP_DELAY_AFTER_HELP"] = "Un intervalle de délai avant la règle est appliqué après qu'une entité CRM ait changé de statut.";
$MESS["CRM_AUTOMATION_CMP_DELAY_BEFORE_HELP"] = "La règle sera appliquée à la période de temps spécifiée avant la date spécifiée dans l'entité CRM.
Par exemple, elle peut envoyer un rappel de paiement un jour avant la date d'échéance.";
$MESS["CRM_AUTOMATION_CMP_IS_DEMO"] = "(prochainement)";
$MESS["CRM_AUTOMATION_CMP_TITLE_LEAD_VIEW"] = "Statut actuel du client potentiel: \"#TITLE#\"";
$MESS["CRM_AUTOMATION_CMP_TITLE_LEAD_EDIT"] = "Définir des règles d'automatisation pour tous les clients potentiels";
$MESS["CRM_AUTOMATION_CMP_TITLE_DEAL_VIEW"] = "Étape actuelle de l’affaire: \"#TITLE#\"";
$MESS["CRM_AUTOMATION_CMP_TITLE_DEAL_EDIT"] = "Définir des règles d'automatisation pour toutes les affaires dans ce pipeline";
$MESS["CRM_AUTOMATION_TRIGGER_WEBFORM_LABEL"] = "Sélectionner le formulaire";
$MESS["CRM_AUTOMATION_TRIGGER_WEBFORM_ANY"] = "tout";
$MESS["CRM_AUTOMATION_ROBOT_CATEGORY_EMPLOYEE"] = "Pour l'employé";
$MESS["CRM_AUTOMATION_ROBOT_CATEGORY_CLIENT"] = "Pour la communication au client";
$MESS["CRM_AUTOMATION_ROBOT_CATEGORY_ADS"] = "Publicité";
$MESS["CRM_AUTOMATION_ROBOT_CATEGORY_OTHER"] = "Règles d’automatisation personnalisées";
$MESS["CRM_AUTOMATION_ROBOT_CATEGORY_OTHER_MARKETPLACE"] = "Applications24";
$MESS["CRM_AUTOMATION_TRIGGER_OPENLINE_LABEL"] = "Sélectionner le Canal ouvert";
$MESS["CRM_AUTOMATION_CMP_AFTER_PREVIOUS_WIDE"] = "après la prochaine règle d'automatisation";
$MESS["CRM_AUTOMATION_ROBOT_CONDITION"] = "Condition";
$MESS["CRM_AUTOMATION_ROBOT_CONDITION_EMPTY"] = "aucune condition";
$MESS["CRM_AUTOMATION_ROBOT_CONDITION_EQ"] = "est égal à";
$MESS["CRM_AUTOMATION_ROBOT_CONDITION_GT"] = "supérieur à";
$MESS["CRM_AUTOMATION_ROBOT_CONDITION_GTE"] = "pas inférieur à";
$MESS["CRM_AUTOMATION_ROBOT_CONDITION_LT"] = "inférieur à";
$MESS["CRM_AUTOMATION_ROBOT_CONDITION_LTE"] = "pas supérieur à";
$MESS["CRM_AUTOMATION_ROBOT_CONDITION_NE"] = "pas égal à";
$MESS["CRM_AUTOMATION_ROBOT_CONDITION_IN"] = "est dans";
$MESS["CRM_AUTOMATION_ROBOT_CONDITION_CONTAIN"] = "contient";
$MESS["CRM_AUTOMATION_CMP_BY_CONDITION"] = "par condition";
$MESS["CRM_AUTOMATION_CMP_SELECT_DISK_FILE"] = "Rechercher dans Bitrix24";
$MESS["CRM_AUTOMATION_CMP_DISK_ATTACH_FILE"] = "Joindre des fichiers";
$MESS["CRM_AUTOMATION_CMP_DISK_ATTACHED_FILES"] = "Pièces jointes";
$MESS["CRM_AUTOMATION_CMP_DISK_SELECT_FILE"] = "Rechercher dans Bitrix24";
$MESS["CRM_AUTOMATION_CMP_DISK_SELECT_FILE_LEGEND"] = "Ouvrir la fenêtre Bitrix24.Drive";
$MESS["CRM_AUTOMATION_CMP_DISK_UPLOAD_FILE"] = "Téléverser un fichier";
$MESS["CRM_AUTOMATION_CMP_DISK_UPLOAD_FILE_LEGEND"] = "Faites glisser pour ajouter";
?>