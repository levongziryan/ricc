<?
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_ACCOUNT_NUMBER_PREFIX_WARNING"] = "Valeur incorrecte du préfixe '#PREFIX#' pour la numérotation de commande.";
$MESS["CRM_ACCOUNT_NUMBER_NUMBER_WARNING"] = "Nombre initial '#NOMBRE#'incorrect pour la numérotation.";
?>