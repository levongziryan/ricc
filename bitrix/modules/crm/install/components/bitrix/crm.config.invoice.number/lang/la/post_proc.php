<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo de CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado.";
$MESS["CRM_ACCOUNT_NUMBER_PREFIX_WARNING"] = "El prefijode la orden \"#PREFIX#\" no es válido.";
$MESS["CRM_ACCOUNT_NUMBER_NUMBER_WARNING"] = "El número inicial de la orden \"#NUMBER#\" no es válido.";
?>