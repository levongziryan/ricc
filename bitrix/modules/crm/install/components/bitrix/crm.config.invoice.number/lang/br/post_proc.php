<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado. ";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado.";
$MESS["CRM_ACCOUNT_NUMBER_PREFIX_WARNING"] = "O prefixo do pedido \"#PREFIX#\" é inválido.";
$MESS["CRM_ACCOUNT_NUMBER_NUMBER_WARNING"] = "O número inicial do pedido \"#NUMBER#\" é inválido. ";
?>