<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["UNKNOWN_ERROR"] = "Error desconocido.";
$MESS["CRM_LEAD_NAV_TITLE_EDIT"] = "Convertir Prospecto: #NAME#";
$MESS["CRM_LEAD_NAV_TITLE_LIST"] = "Prospectos";
$MESS["CRM_FIELD_LEAD_DEAL"] = "Convertir Prospecto";
$MESS["CRM_FIELD_LEAD_COMPANY"] = "Convertir compañía";
$MESS["CRM_FIELD_LEAD_CONTACT"] = "Convertir contacto";
$MESS["CRM_COMPANY_ERROR"] = "Por favor seleccione una compañía.";
$MESS["CRM_FIELD_TITLE"] = "Nombre del Prospecto";
$MESS["CRM_CONTACT_ERROR"] = "Por favor seleccione un contacto.";
$MESS["CRM_DEAL_ERROR"] = "Por favor llene en los campos de los prospectos.";
?>