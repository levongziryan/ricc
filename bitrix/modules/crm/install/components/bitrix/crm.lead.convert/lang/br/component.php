<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["UNKNOWN_ERROR"] = "Erro desconhecido.";
$MESS["CRM_LEAD_NAV_TITLE_EDIT"] = "Converter Lead: #NAME#";
$MESS["CRM_LEAD_NAV_TITLE_LIST"] = "Leads";
$MESS["CRM_FIELD_LEAD_DEAL"] = "Converter Negócio";
$MESS["CRM_FIELD_LEAD_COMPANY"] = "Converter Empresa";
$MESS["CRM_FIELD_LEAD_CONTACT"] = "Converter Contato";
$MESS["CRM_COMPANY_ERROR"] = "Por favor selecione uma empresa.";
$MESS["CRM_FIELD_TITLE"] = "Nome do lead";
$MESS["CRM_CONTACT_ERROR"] = "Por favor selecione um contato.";
$MESS["CRM_DEAL_ERROR"] = "Por favor, preencha os campos negócio.";
?>