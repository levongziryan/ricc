<?
$MESS["CRM_DEAL_LIST_REBUILD_STATISTICS_NOT_REQUIRED_SUMMARY"] = "Nenhuma estatística de negócio precisa ser atualizada.";
$MESS["CRM_DEAL_LIST_REBUILD_STATISTICS_PROGRESS_SUMMARY"] = "Negócios processados: #PROCESSED_ITEMS# de #TOTAL_ITEMS#.";
$MESS["CRM_DEAL_LIST_REBUILD_STATISTICS_COMPLETED_SUMMARY"] = "As estatísticas de negócio foram atualizadas. Negócios processados: #PROCESSED_ITEMS#.";
$MESS["CRM_DEAL_LIST_ROW_COUNT"] = "Total: #ROW_COUNT#";
$MESS["CRM_DEAL_LIST_REBUILD_SEMANTICS_NOT_REQUIRED_SUMMARY"] = "Os campos de negociação de serviço não precisam de atualização.";
$MESS["CRM_DEAL_LIST_REBUILD_SEMANTICS_PROGRESS_SUMMARY"] = "Negociações processadas: #PROCESSED_ITEMS# de #TOTAL_ITEMS#.";
$MESS["CRM_DEAL_LIST_REBUILD_SEMANTICS_COMPLETED_SUMMARY"] = "Os campos de negociação de serviço foram atualizados. Negociações processadas: #PROCESSED_ITEMS#.";
?>