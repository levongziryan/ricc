<?
$MESS["CRM_DEAL_LIST_REBUILD_STATISTICS_NOT_REQUIRED_SUMMARY"] = "Aucune statistique d'affaire doivent être mis à jour.";
$MESS["CRM_DEAL_LIST_REBUILD_STATISTICS_PROGRESS_SUMMARY"] = "Offres traitées: #PROCESSED_ITEMS# du #TOTAL_ITEMS#.";
$MESS["CRM_DEAL_LIST_REBUILD_STATISTICS_COMPLETED_SUMMARY"] = "Deal statistiques ont été mises à jour. Offres traitées: #PROCESSED_ITEMS#.";
$MESS["CRM_DEAL_LIST_ROW_COUNT"] = "Total : #ROW_COUNT#";
$MESS["CRM_DEAL_LIST_REBUILD_SEMANTICS_NOT_REQUIRED_SUMMARY"] = "Les champs d'affaires du service ne nécessitent aucune mise à jour.";
$MESS["CRM_DEAL_LIST_REBUILD_SEMANTICS_PROGRESS_SUMMARY"] = "Affaires traitées: #PROCESSED_ITEMS# de #TOTAL_ITEMS#.";
$MESS["CRM_DEAL_LIST_REBUILD_SEMANTICS_COMPLETED_SUMMARY"] = "Les champs d'affaires du service ont été mis à jour. Affaires traitées: #PROCESSED_ITEMS#.";
$MESS["CRM_DEAL_LIST_REBUILD_SEARCH_CONTENT_NOT_REQUIRED_SUMMARY"] = "L'index de recherche des transactions n'a pas besoin d'être recréé.";
$MESS["CRM_DEAL_LIST_REBUILD_SEARCH_CONTENT_PROGRESS_SUMMARY"] = "Transactions traitées : #PROCESSED_ITEMS# sur #TOTAL_ITEMS#.";
$MESS["CRM_DEAL_LIST_REBUILD_SEARCH_CONTENT_COMPLETED_SUMMARY"] = "L'index de recherche des transactions a été recréé. Transactions traitées : #PROCESSED_ITEMS#.";
?>