<?
$MESS["CRM_PRODUCT_SECTION_CRUMBS_AJAX_ERROR_REQUIRED_PARAMETER"] = "Le paramètre obligatoire #PARAM# n'a pas été renseigné.";
$MESS["CRM_PRODUCT_SECTION_CRUMBS_AJAX_ERROR_EMPTY_ACTION"] = "La réaction active n'a pas été sélectionnée.";
$MESS["CRM_PRODUCT_SECTION_CRUMBS_AJAX_ERROR_UNKNOWN_ACTION"] = "Action Unknown: #ACTION#.";
?>