<?
$MESS["CRM_TAB_1"] = "Configuración de importación";
$MESS["CRM_TAB_1_TITLE"] = "Editar la configuración de importación";
$MESS["CRM_IMPORT_SNS"] = "A parte de la importación clásica, usted puede importar usando la vCard.<br>  Para hacer esto en <b>MS Outlook</b>, ir a contactos y seleccionar loa cards de negocio para ser enviadas. Luego que accione, elegir <b> y especificar la dirección de email del destinatario <b>%EMAIL%</b>";
$MESS["CRM_TAB_2"] = "Campos";
$MESS["CRM_TAB_2_TITLE"] = "Configurar mapeo del campo";
$MESS["CRM_TAB_3"] = "Importar";
$MESS["CRM_TAB_3_TITLE"] = "Importar resultado";
$MESS["CRM_IMPORT_NEXT_STEP"] = "Siguiente >>";
$MESS["CRM_IMPORT_NEXT_STEP_TITLE"] = "Ir al siguiente paso";
$MESS["CRM_IMPORT_PREVIOUS_STEP"] = "<< Volver";
$MESS["CRM_IMPORT_PREVIOUS_STEP_TITLE"] = "Ir al paso anterior";
$MESS["CRM_IMPORT_DONE"] = "Listo";
$MESS["CRM_IMPORT_DONE_TITLE"] = "Ver contactos";
$MESS["CRM_IMPORT_CANCEL"] = "Cancelar ";
$MESS["CRM_IMPORT_CANCEL_TITLE"] = "Abandonar y regresar a la lista de contactos";
$MESS["CRM_IMPORT_AGAIN"] = "Importar otro archivo";
$MESS["CRM_IMPORT_AGAIN_TITLE"] = "Oprimir para importar otro archivo";
$MESS["CRM_TAB_DUP_CONTROL"] = "Control de Duplicados";
$MESS["CRM_TAB_DUP_CONTROL_TITLE"] = "Configurar control de duplicados";
?>