<?
$MESS["CRM_TAB_1"] = "Paramètres d'importation";
$MESS["CRM_TAB_1_TITLE"] = "Réglage des paramètres de l'importation";
$MESS["CRM_TAB_3"] = "Charger";
$MESS["CRM_TAB_3_TITLE"] = "Résultat de l'importation";
$MESS["CRM_IMPORT_NEXT_STEP"] = "Continuer >>";
$MESS["CRM_IMPORT_NEXT_STEP_TITLE"] = "Passer à l'étape suivante";
$MESS["CRM_IMPORT_PREVIOUS_STEP"] = "<< Retour";
$MESS["CRM_IMPORT_PREVIOUS_STEP_TITLE"] = "Revenir à l'étape précédente";
$MESS["CRM_IMPORT_DONE"] = "Prêt";
$MESS["CRM_IMPORT_DONE_TITLE"] = "Accéder à la liste des contacts";
$MESS["CRM_IMPORT_AGAIN"] = "Importer un autre fichier";
$MESS["CRM_IMPORT_AGAIN_TITLE"] = "Accéder à l'importation de nouvelles données";
$MESS["CRM_IMPORT_CANCEL"] = "Annuler";
$MESS["CRM_IMPORT_CANCEL_TITLE"] = "Ne pas continuer et retourner à la liste des contacts";
$MESS["CRM_TAB_2"] = "Le contrôle des doublons";
$MESS["CRM_TAB_2_TITLE"] = "Configurer le contrôle des doublons";
?>