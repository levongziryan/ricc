<?
$MESS["CRM_PERMISSION_DENIED"] = "Você não tem permissão para acessar este formulário.";
$MESS["BPWC_WLC_WRONG_BP"] = "O registro não foi encontrado.";
$MESS["BPWC_WNC_EMPTY_LOGIN"] = "O campo 'Login' é obrigatório.";
$MESS["BPWC_WNC_EMPTY_URL"] = "O campo 'URL' é obrigatório.";
$MESS["BPWC_WNC_EMPTY_PASSWORD"] = "O campo 'Senha' é obrigatório.";
$MESS["BPABL_PAGE_TITLE"] = "Conectividade da loja virtual";
$MESS["BPWC_WNC_MAX_SHOPS"] = "Sua edição não permite adicionar novas lojas virtuais.";
$MESS["BPWC_WNC_EMPTY_SESSID"] = "Erro ao conectar o CRM à loja online. É provável que o módulo \"e-Store\" precise ser atualizado.";
?>