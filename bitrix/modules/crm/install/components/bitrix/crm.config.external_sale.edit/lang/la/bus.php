<?
$MESS["CRM_PERMISSION_DENIED"] = "No tiene permiso para acceder a este formulario.";
$MESS["BPWC_WLC_WRONG_BP"] = "No se encontró el registro.";
$MESS["BPWC_WNC_EMPTY_LOGIN"] = "Se requiere el campo 'Iniciar sesión'.";
$MESS["BPWC_WNC_EMPTY_URL"] = "Se requiere el campo 'URL'.";
$MESS["BPWC_WNC_EMPTY_PASSWORD"] = "Se requiere el campo 'URL'.";
$MESS["BPABL_PAGE_TITLE"] = "Conectividad de tienda Web";
$MESS["BPWC_WNC_MAX_SHOPS"] = "Su edición no permite agregar nuevas tiendas web.";
$MESS["BPWC_WNC_EMPTY_SESSID"] = "Error de conexión CRM a la tienda en línea. Es probable que el módulo de \"e-Store\" necesita ser actualizada.";
?>