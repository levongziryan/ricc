<?
$MESS["CRM_PERMISSION_DENIED"] = "No tienes permiso para acceder a este formulario.";
$MESS["BPWC_WLC_WRONG_BP"] = "No se encontró el registro.";
$MESS["BPWC_WNC_EMPTY_LOGIN"] = "Se requiere el campo 'Iniciar sesión'.";
$MESS["BPWC_WNC_EMPTY_URL"] = "Se requiere el campo 'URL'.";
$MESS["BPWC_WNC_EMPTY_PASSWORD"] = "Se requiere el campo 'Password'.";
$MESS["BPABL_PAGE_TITLE"] = "Contactos de tienda";
$MESS["BPWC_WNC_MAX_SHOPS"] = "Su edición no permite agregar nuevas tiendas web.";
?>