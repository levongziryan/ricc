<?
$MESS["BPWC_WNC_MAX_SHOPS"] = "Vous ne pouvez pas ajouter de nouveaux e-commerce dans votre édition.";
$MESS["BPWC_WLC_WRONG_BP"] = "Enregistrement n'est pas trouvé.";
$MESS["BPWC_WNC_EMPTY_URL"] = "Champ 'Adresse' non renseigné.";
$MESS["BPWC_WNC_EMPTY_LOGIN"] = "Champ 'Identifiant' non renseigné.";
$MESS["BPWC_WNC_EMPTY_PASSWORD"] = "Champ 'Mot de passe' non renseigné.";
$MESS["BPABL_PAGE_TITLE"] = "Connexion avec les boutiques en ligne";
$MESS["CRM_PERMISSION_DENIED"] = "Vous n'avez pas de droits d'accès à cette forme.";
?>