<?
$MESS["CRM_ACTIVITY_VISIT_OWNER"] = "Según:";
$MESS["CRM_ACTIVITY_VISIT_FINISH"] = "Completar";
$MESS["CRM_ACTIVITY_VISIT_RECORDING"] = "Grabación en curso";
$MESS["CRM_ACTIVITY_VISIT_MINUTES"] = "min";
$MESS["CRM_ACTIVITY_VISIT_CREATE"] = "Crear:";
$MESS["CRM_ACTIVITY_VISIT_LEAD"] = "Prospecto";
$MESS["CRM_ACTIVITY_VISIT_CONTACT"] = "Contacto";
$MESS["CRM_ACTIVITY_VISIT_SELECT"] = "Seleccionar:";
$MESS["CRM_ACTIVITY_VISIT_CONTACT_OR_COMPANY"] = "Contacto o compañía";
$MESS["CRM_ACTIVITY_VISIT_ACTIVITY"] = "Actividad";
$MESS["CRM_ACTIVITY_VISIT_DEAL"] = "Negociación";
$MESS["CRM_ACTIVITY_VISIT_INVOICE"] = "Factura";
$MESS["CRM_ACTIVITY_VISIT_CAMERA"] = "Cámara:";
$MESS["CRM_ACTIVITY_VISIT_VK"] = "VK";
$MESS["CRM_ACTIVITY_BROWSER_ERROR"] = "Desafortunadamente, su navegador no admite la grabación de audio. <br> Se recomiendan los siguientes navegadores: Mozilla Firefox y Google Chrome.";
$MESS["CRM_ACTIVITY_VISIT_SEARCH_VK"] = "Encuentra el perfil de VK";
$MESS["CRM_ACTIVITY_VISIT_TAB_VISIT"] = "Grabación en vivo";
$MESS["CRM_ACTIVITY_VISIT_TAB_SAVE_PHOTO"] = "Foto de perfil";
$MESS["CRM_ACTIVITY_VISIT_SEARCH_IN_PROGRESS"] = "Buscando...";
?>