<?
$MESS["CRM_PRESET_TOOLBAR_ADD"] = "Agregar plantilla";
$MESS["CRM_PRESET_TOOLBAR_ADD_TITLE"] = "Agregar nueva plantilla";
$MESS["CRM_PRESET_TOOLBAR_EDIT"] = "Editar plantilla";
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_PRESET_LIST_ACTION_MENU_DELETE"] = "Eliminar";
$MESS["CRM_PRESET_LIST_ACTION_MENU_DELETE_CONF"] = "¿Está seguro de que quiere eliminar esta plantilla?";
$MESS["CRM_PRESET_LIST_ACTION_MENU_FIELD_LIST"] = "Campos";
$MESS["CRM_PRESET_LIST_ACTION_MENU_EDIT"] = "Editar";
$MESS["CRM_ADD_PRESET_DIALOG_ERR_EMPTY_NAME"] = "Por favor, introduzca el nombre de la plantilla";
$MESS["CRM_ADD_PRESET_DIALOG_ERR_LONG_NAME"] = "El nombre de la plantilla es demasiado largo.";
$MESS["CRM_ADD_PRESET_DIALOG_CREATE_NEW_TITLE"] = "Crear nuevo";
$MESS["CRM_ADD_PRESET_DIALOG_FIXED_PRESET_TITLE"] = "Seleccione muestra";
$MESS["CRM_ADD_PRESET_DIALOG_CREATE_SELECTED_TITLE"] = "Seleccionar de la lista";
$MESS["CRM_ADD_PRESET_DIALOG_NAME_TITLE"] = "Nonbre";
$MESS["CRM_ADD_PRESET_DIALOG_ACTIVE_TITLE"] = "Activo";
$MESS["CRM_ADD_PRESET_DIALOG_SORT_TITLE"] = "Clasificar";
$MESS["CRM_ADD_PRESET_DIALOG_BTN_ADD_TEXT"] = "Agregar";
$MESS["CRM_ADD_PRESET_DIALOG_BTN_EDIT_TEXT"] = "Aplicar";
$MESS["CRM_ADD_PRESET_DIALOG_BTN_CANCEL_TEXT"] = "Cancelar";
$MESS["CRM_PRESET_LIST_ACTION_MENU_SET_DEF_FOR_COMPANY"] = "Marcar como principal para empresas";
$MESS["CRM_PRESET_LIST_ACTION_MENU_SET_DEF_FOR_CONTACT"] = "Marcar como principal para contactos";
?>