<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_PRESET_LIST_TITLE_EDIT"] = "Plantillas: #NAME#";
$MESS["CRM_PRESET_ENTITY_TYPE_INVALID"] = "Tipo de plantilla incorrecta";
$MESS["CRM_PRESET_COUNTRY_EMPTY"] = "(no seleccionado)";
$MESS["CRM_PRESET_ERR_DELETE"] = "Error al eliminar la plantilla (ID = #ID#)";
$MESS["CRM_PRESET_NOT_SELECTED"] = "(no seleccionado)";
$MESS["CRM_PRESET_DEF_FOR_REQUISITE_OF_COMPANY"] = "Principal para empresas";
$MESS["CRM_PRESET_DEF_FOR_REQUISITE_OF_CONTACT"] = "Principal para contactos";
?>