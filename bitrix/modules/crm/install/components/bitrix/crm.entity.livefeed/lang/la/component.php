<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado";
$MESS["SONET_MODULE_NOT_INSTALLED"] = "El módulo Social Network no está instalado";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado.";
$MESS["CRM_SL_EVENT_EDIT_ENTITY_TYPE_NOT_DEFINED"] = "Tipo de entidad no está especificado en el CRM.";
$MESS["CRM_SL_EVENT_EDIT_ENTITY_ID_NOT_DEFINED"] = "ID del tipo de entidad no está especificado en el CRM.";
$MESS["CRM_SL_EVENT_NOT_AVAIBLE"] = "Los eventos no están disponibles para el usuario actual.";
$MESS["CRM_SL_EVENT_EDIT_HIDDEN_GROUP"] = "Grupo oculto";
?>