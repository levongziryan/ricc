<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["CRM_CHANNEL_1C_CAPTION"] = "Conector 1C";
$MESS["CRM_CHANNEL_1C_LEGEND"] = "Exportar todo o histórico de vendas e clientes para o CRM. Vincular vendas offline ao CRM online.";
$MESS["CRM_CHANNEL_WEB_FORM_CAPTION"] = "Formulários de CRM";
$MESS["CRM_CHANNEL_WEB_FORM_LEGEND"] = "Pedidos, formulários, formulários de cadastro são adicionados diretamente ao CRM. É fácil criar um formulário de CRM, mesmo se você não tiver um site.";
$MESS["CRM_CHANNEL_TELEPHONY_CAPTION"] = "Telefonia";
$MESS["CRM_CHANNEL_TELEPHONY_LEGEND"] = "Todas as chamadas são salvas no CRM. Um novo cliente potencial é sempre criado mesmo se uma chamada foi perdida.";
$MESS["CRM_CHANNEL_CHAT_CAPTION"] = "Bate-papo ao vivo";
$MESS["CRM_CHANNEL_CHAT_LEGEND"] = "Receba um widget de bate-papo gratuito. Todas as solicitações do cliente são enviadas para Bitrix24; todas as conversas são sempre salvas no CRM.";
$MESS["CRM_CHANNEL_CALLBACK_CAPTION"] = "Retorno de chamada";
$MESS["CRM_CHANNEL_CALLBACK_LEGEND"] = "Receba um widget de retorno de chamada gratuito. Um cliente pede um retorno de chamada, a nova chamada é feita pelo Bitrix24 e registrada no CRM.";
$MESS["CRM_CHANNEL_OPEN_LINE_CAPTION"] = "Canais Abertos";
$MESS["CRM_CHANNEL_OPEN_LINE_LEGEND"] = "Migrar conversas com clientes dos serviços em redes sociais para o bate-papo Bitrix24. Todas as conversas são salvas no CRM.";
$MESS["CRM_CHANNEL_EMAIL_CAPTION"] = "Contas de e-mail do CRM";
$MESS["CRM_CHANNEL_EMAIL_LEGEND"] = "Um novo e-mail cria um novo cliente potencial. A mensagem de e-mail é adicionada ao contato e é criada uma nova atividade.";
?>