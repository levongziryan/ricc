<?
$MESS["CRM_CHANNEL_PANEL_CONNECT_BUTTON"] = "Connecter";
$MESS["CRM_CHANNEL_PANEL_CLOSE_TITLE"] = "Masquer la description";
$MESS["CRM_CHANNEL_PANEL_CLOSE_CONFIRM"] = "Voulez-vous vraiment masquer la description des canaux disponibles dans votre CRM?";
$MESS["CRM_CHANNEL_PANEL_CLOSE"] = "Masquer";
?>