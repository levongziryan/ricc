<?
$MESS["CRM_DEAL_CONVERSION_NOT_FOUND"] = "Transaction introuvable.";
$MESS["CRM_DEAL_CONVERSION_ID_NOT_DEFINED"] = "ID de transaction introuvable.";
$MESS["CRM_DEAL_CONVERSION_ACCESS_DENIED"] = "Accès refusé";
$MESS["CRM_DEAL_PRODUCT_ROWS_SAVING_ERROR"] = "Une erreur est survenue pendant l'enregistrement des produits.";
$MESS["CRM_DEAL_MOVE_TO_CATEGORY_ERROR"] = "Migration de la transaction vers le nouveau pipeline impossible. Une transaction ne peut être être migrée qu'une fois les flux de travail actif arrêtés. Vous pouvez arrêter ou finaliser les flux de travail à partir de l'onglet Processus d'entreprise. Assurez-vous également que la migration que vous voulez migrer ait un responsable assigné et que le stade de transaction soit défini.";
$MESS["CRM_DEAL_DELETION_ERROR"] = "Erreur lors de la suppression de la transaction.";
?>