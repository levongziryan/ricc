<?
$MESS["CRM_DEAL_DETAIL_HISTORY_STUB"] = "Vous ajoutez maintenant une transaction...";
$MESS["CRM_DEAL_CONV_ACCESS_DENIED"] = "Vous avez besoin de permissions pour créer des contacts, sociétés et transactions afin de finaliser l'opération.";
$MESS["CRM_DEAL_CONV_GENERAL_ERROR"] = "Erreur de conversion générique.";
$MESS["CRM_DEAL_CONV_DIALOG_TITLE"] = "Générer une entité sur base de la transaction";
$MESS["CRM_DEAL_CONV_DIALOG_CONTINUE_BTN"] = "Continuer";
$MESS["CRM_DEAL_CONV_DIALOG_CANCEL_BTN"] = "Annuler";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_LEGEND"] = "Les entités sélectionnées n'ont pas de champs pouvant stocker des données de transactions. Choisissez les entités dans lesquelles les champs manquants seront créés pour accueillir toutes les informations disponibles.";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_FILED_LIST_TITLE"] = "Ces champs seront créés";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_ENTITY_LIST_TITLE"] = "Sélectionnez les entités dans lesquelles les champs manquants seront créés";
?>