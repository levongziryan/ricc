<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
$MESS["CRM_PRESET_EDIT_TITLE"] = "Champs des modèles : #NAME# (#ENTITY_TYPE_NAME#)";
$MESS["CRM_PRESET_ENTITY_TYPE_INVALID"] = "Type de modèle incorrect";
$MESS["CRM_PRESET_NOT_FOUND"] = "Le modèle n'a pas été trouvé";
$MESS["CRM_PRESET_FIELD_ID"] = "ID";
$MESS["CRM_PRESET_FIELD_FIELD_NAME"] = "Nom";
$MESS["CRM_PRESET_FIELD_FIELD_ETITLE"] = "Nom";
$MESS["CRM_PRESET_FIELD_FIELD_TITLE"] = "Nom dans le modèle";
$MESS["CRM_PRESET_FIELD_SORT"] = "Trier";
$MESS["CRM_PRESET_FIELD_IN_SHORT_LIST"] = "Afficher dans le résumer";
$MESS["CRM_PRESET_EDIT_SELECT_FIELDS_NONE"] = "(Non sélectionné)";
$MESS["CRM_PRESET_EDIT_SELECT_OTHER_FIELDS"] = "(Champs déliés)";
$MESS["CRM_REQUISITE_PRESET_NAME_EMPTY"] = "Template sans titre";
?>