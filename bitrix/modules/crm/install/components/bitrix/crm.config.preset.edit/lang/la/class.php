<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_PRESET_EDIT_TITLE"] = "Campos de la plantilla: #NAME# (#ENTITY_TYPE_NAME#)";
$MESS["CRM_PRESET_ENTITY_TYPE_INVALID"] = "Tipo de plantilla incorrecta";
$MESS["CRM_PRESET_NOT_FOUND"] = "No se encontró plantilla";
$MESS["CRM_PRESET_FIELD_ID"] = "ID";
$MESS["CRM_PRESET_FIELD_FIELD_NAME"] = "Nombre";
$MESS["CRM_PRESET_FIELD_FIELD_ETITLE"] = "Nombre";
$MESS["CRM_PRESET_FIELD_FIELD_TITLE"] = "Nombre de la plantilla";
$MESS["CRM_PRESET_FIELD_SORT"] = "Clasificar";
$MESS["CRM_PRESET_FIELD_IN_SHORT_LIST"] = "Mostrar en el resumen";
$MESS["CRM_PRESET_EDIT_SELECT_FIELDS_NONE"] = "(No seleccionado)";
$MESS["CRM_PRESET_EDIT_SELECT_OTHER_FIELDS"] = "(Desvincular campos)";
$MESS["CRM_REQUISITE_PRESET_NAME_EMPTY"] = "Plantilla sin título";
?>