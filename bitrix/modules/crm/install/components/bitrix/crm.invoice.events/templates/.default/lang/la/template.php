<?
$MESS["CRM_EVENT_TABLE_EMPTY"] = "- No hay datos -";
$MESS["CRM_EVENT_TABLE_FILES"] = "Archivos";
$MESS["CRM_EVENT_ENTITY_TYPE_DEAL"] = "Negociación";
$MESS["CRM_EVENT_ENTITY_TYPE_COMPANY"] = "Compañía";
$MESS["CRM_EVENT_ENTITY_TYPE_CONTACT"] = "Contacto";
$MESS["CRM_EVENT_ENTITY_TYPE_LEAD"] = "Prospecto";
$MESS["CRM_EVENT_DELETE"] = "Eliminar";
$MESS["CRM_EVENT_DELETE_TITLE"] = "Eliminar evento";
$MESS["CRM_EVENT_DELETE_CONFIRM"] = "Está seguro de que desea eliminarlo?";
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_EVENT_VIEW_ADD_SHORT"] = "Evento";
$MESS["CRM_EVENT_VIEW_ADD"] = "Agregar evento";
$MESS["CRM_EVENT_VIEW_SHOW_FILTER_SHORT"] = "Filtro";
$MESS["CRM_EVENT_VIEW_SHOW_FILTER"] = "Mostrar/ocultar filtro";
$MESS["CRM_IMPORT_EVENT"] = "Si el contacto de correo electrónico está especificado en el registro del CRM puede guardar automáticamente todos los mensajes de correo electrónico como un registro de eventos. Es necesario enviar mensaje recibido a la dirección de correo electrónico del sistema b>%EMAIL%</b> y todo el texto y archivos adjuntos se agregan como eventos para este contacto.";
?>