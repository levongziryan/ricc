<?
$MESS["CRM_COLUMN_DATE_CREATE"] = "Data";
$MESS["CRM_COLUMN_ENTITY_TYPE"] = "Tipo";
$MESS["CRM_COLUMN_ENTITY_TITLE"] = "Título";
$MESS["CRM_COLUMN_CREATED_BY"] = "Criado por";
$MESS["CRM_COLUMN_EVENT_NAME"] = "Tipo de Evento";
$MESS["CRM_COLUMN_EVENT_TYPE"] = "Tipo";
$MESS["CRM_COLUMN_CREATED_BY_ID"] = "Criado por";
$MESS["CRM_COLUMN_ASSIGNED_BY_ID"] = "Responsável";
$MESS["CRM_COLUMN_EVENT_DESC"] = "Descrição";
$MESS["CRM_ENTITY_TYPE_LEAD"] = "Lead";
$MESS["CRM_ENTITY_TYPE_CONTACT"] = "Contato";
$MESS["CRM_ENTITY_TYPE_COMPANY"] = "Empresa";
$MESS["CRM_ENTITY_TYPE_DEAL"] = "Negócio";
$MESS["CRM_EVENT_TYPE_SNS"] = "Mensagem de E-mail";
$MESS["CRM_EVENT_TYPE_USER"] = "Personalizado";
$MESS["CRM_EVENT_TYPE_CHANGE"] = "Mudanças";
$MESS["CRM_EVENT_DESC_MORE"] = "leia mais";
$MESS["CRM_EVENT_DESC_AFTER"] = "Depois";
$MESS["CRM_EVENT_DESC_BEFORE"] = "Antes";
$MESS["CRM_PRESET_CREATE_TODAY"] = "Criado hoje";
$MESS["CRM_PRESET_CREATE_YESTERDAY"] = "Criado Ontem";
$MESS["CRM_PRESET_CREATE_MY"] = "Criado por mim";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_COLUMN_ENTITY"] = "Item CRM";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "O módulo e-Store não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "O módulo Catálogo Comercial não está instalado.";
?>