<?
$MESS["CRM_COLUMN_DATE_CREATE"] = "Fecha";
$MESS["CRM_COLUMN_ENTITY_TYPE"] = "Tipo";
$MESS["CRM_COLUMN_ENTITY_TITLE"] = "Titúlo";
$MESS["CRM_COLUMN_CREATED_BY"] = "Creado por";
$MESS["CRM_COLUMN_EVENT_NAME"] = "Tipo de evento";
$MESS["CRM_COLUMN_CREATED_BY_ID"] = "Creado por";
$MESS["CRM_COLUMN_ASSIGNED_BY_ID"] = "Persona responsable";
$MESS["CRM_COLUMN_EVENT_DESC"] = "Descripción";
$MESS["CRM_ENTITY_TYPE_LEAD"] = "Prospecto";
$MESS["CRM_ENTITY_TYPE_CONTACT"] = "Contacto";
$MESS["CRM_ENTITY_TYPE_COMPANY"] = "Compañía";
$MESS["CRM_ENTITY_TYPE_DEAL"] = "Negociación";
$MESS["CRM_EVENT_DESC_MORE"] = "leer más";
$MESS["CRM_PRESET_CREATE_TODAY"] = "Creado Hoy";
$MESS["CRM_PRESET_CREATE_YESTERDAY"] = "Creado ayer";
$MESS["CRM_PRESET_CREATE_MY"] = "Creado por mí";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_EVENT_TYPE_SNS"] = "Mensaje en el correo electrónico";
$MESS["CRM_EVENT_DESC_AFTER"] = "Después de ";
$MESS["CRM_EVENT_DESC_BEFORE"] = "Antes de ";
$MESS["CRM_COLUMN_EVENT_TYPE"] = "Tipo";
$MESS["CRM_EVENT_TYPE_USER"] = "Personalización";
$MESS["CRM_EVENT_TYPE_CHANGE"] = "Cambio";
$MESS["CRM_COLUMN_ENTITY"] = "Elementos del CRM";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "El módulo e-Store no está instalado";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "El módulo Commercial Catalog no está instalado";
?>