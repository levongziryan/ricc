<?
$MESS["CRM_PRODUCT_FILE_CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_PRODUCT_FILE_IBLOCK_MODULE_NOT_INSTALLED"] = "Le module des blocs d'informations n'a pas été installé.";
$MESS["CRM_PRODUCT_FILE_WRONG_FILE"] = "Fichier introuvable";
$MESS["CRM_PRODUCT_FILE_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_PRODUCT_FILE_UNKNOWN_ERROR"] = "Erreur inconnue.";
?>