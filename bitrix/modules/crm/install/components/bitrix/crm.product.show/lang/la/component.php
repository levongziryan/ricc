<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_PRODUCT_NOT_FOUND"] = "No se encontró el producto.";
$MESS["CRM_SECTION_PRODUCT_INFO"] = "Información del producto";
$MESS["CRM_FIELD_DESCRIPTION"] = "Descripción";
$MESS["CRM_FIELD_ACTIVE"] = "Activo";
$MESS["CRM_FIELD_PRICE"] = "Precio";
$MESS["CRM_FIELD_SORT"] = "clasificar";
$MESS["CRM_PRODUCT_NAV_TITLE_EDIT"] = "Producto: #NAME#";
$MESS["CRM_PRODUCT_NAV_TITLE_ADD"] = "Nuevo producto";
$MESS["CRM_PRODUCT_NAV_TITLE_LIST"] = "Producto";
$MESS["CRM_FIELD_SECTION"] = "Sección";
$MESS["CRM_FIELD_VAT_ID"] = "Tasa del IVA";
$MESS["CRM_FIELD_VAT_INCLUDED"] = "IVA incluido";
$MESS["CRM_FIELD_MEASURE"] = "Unidad de medida";
$MESS["CRM_MEASURE_NOT_SELECTED"] = "[No seleccionado]";
$MESS["CRM_PRODUCT_FIELD_PREVIEW_PICTURE"] = "Vista previa de la imagen";
$MESS["CRM_PRODUCT_FIELD_DETAIL_PICTURE"] = "Imagen completa";
$MESS["CRM_PRODUCT_PROP_ENLARGE"] = "Ampliar";
$MESS["CRM_PRODUCT_PROP_DOWNLOAD"] = "Descargar";
$MESS["CRM_PRODUCT_FIELD_NAME"] = "Nombre";
$MESS["CRM_SECTION_NOT_SELECTED"] = "[No seleccionado]";
?>