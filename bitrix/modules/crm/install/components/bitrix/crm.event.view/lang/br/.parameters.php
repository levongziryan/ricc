<?
$MESS["CRM_ENTITY_TYPE"] = "Tipo da entidade";
$MESS["CRM_ENTITY_ID"] = "ID da Entidade";
$MESS["CRM_EVENT_COUNT"] = "Eventos por página";
$MESS["CRM_EVENT_ENTITY_LINK"] = "Mostrar Título de Entidade";
$MESS["CRM_ENTITY_TYPE_LEAD"] = "Lead";
$MESS["CRM_ENTITY_TYPE_CONTACT"] = "Contato";
$MESS["CRM_ENTITY_TYPE_COMPANY"] = "Empresa";
$MESS["CRM_ENTITY_TYPE_DEAL"] = "Negócio ";
$MESS["CRM_NAME_TEMPLATE"] = "Formato do nome";
?>