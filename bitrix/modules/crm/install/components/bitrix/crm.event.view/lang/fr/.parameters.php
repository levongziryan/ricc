<?
$MESS["CRM_ENTITY_TYPE"] = "Le type de substance n'est pas spécifié";
$MESS["CRM_ENTITY_ID"] = "ID de l'élément de l'entité";
$MESS["CRM_EVENT_COUNT"] = "Nombre d'événements sur la page";
$MESS["CRM_EVENT_ENTITY_LINK"] = "Afficher le titre d'essence";
$MESS["CRM_ENTITY_TYPE_LEAD"] = "Prospect";
$MESS["CRM_ENTITY_TYPE_CONTACT"] = "Client";
$MESS["CRM_ENTITY_TYPE_COMPANY"] = "Entreprise";
$MESS["CRM_ENTITY_TYPE_DEAL"] = "Affaire";
$MESS["CRM_NAME_TEMPLATE"] = "Format du nom";
$MESS["CRM_ENTITY_TYPE_QUOTE"] = "Offre";
?>