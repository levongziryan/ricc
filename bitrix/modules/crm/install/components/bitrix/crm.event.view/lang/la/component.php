<?
$MESS["CRM_COLUMN_DATE_CREATE"] = "Fecha";
$MESS["CRM_COLUMN_ENTITY_TYPE"] = "Tipo";
$MESS["CRM_COLUMN_ENTITY_TITLE"] = "Título";
$MESS["CRM_COLUMN_CREATED_BY"] = "Creado Por";
$MESS["CRM_COLUMN_EVENT_NAME"] = "Tipo de Evento";
$MESS["CRM_COLUMN_CREATED_BY_ID"] = "Creado Por";
$MESS["CRM_COLUMN_ASSIGNED_BY_ID"] = "Persona Responsable";
$MESS["CRM_COLUMN_EVENT_DESC"] = "Descripción";
$MESS["CRM_ENTITY_TYPE_CONTACT"] = "Contacto";
$MESS["CRM_ENTITY_TYPE_COMPANY"] = "Compañía";
$MESS["CRM_ENTITY_TYPE_DEAL"] = "Negociación";
$MESS["CRM_EVENT_DESC_MORE"] = "leer más";
$MESS["CRM_PRESET_CREATE_TODAY"] = "Creado Hoy";
$MESS["CRM_PRESET_CREATE_YESTERDAY"] = "Creado Ayer";
$MESS["CRM_PRESET_CREATE_MY"] = "Creado por mi";
$MESS["CRM_ENTITY_TYPE_LEAD"] = "Prospecto";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_EVENT_TYPE_SNS"] = "Mensaje de correo electrónico";
$MESS["CRM_EVENT_DESC_AFTER"] = "Despúes";
$MESS["CRM_EVENT_DESC_BEFORE"] = "Antes";
$MESS["CRM_COLUMN_EVENT_TYPE"] = "Tipo";
$MESS["CRM_EVENT_TYPE_USER"] = "Cliente";
$MESS["CRM_EVENT_TYPE_CHANGE"] = "Cambios";
$MESS["CRM_COLUMN_ENTITY"] = "Artículo del CRM";
$MESS["CRM_ENTITY_TYPE_QUOTE"] = "Cotización";
?>