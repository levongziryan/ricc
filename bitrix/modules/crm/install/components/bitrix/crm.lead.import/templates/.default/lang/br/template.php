<?
$MESS["CRM_TAB_1"] = "Configurações de importação";
$MESS["CRM_TAB_1_TITLE"] = "Editar configurações de importação";
$MESS["CRM_TAB_2"] = "Campos";
$MESS["CRM_TAB_2_TITLE"] = "Configurar mapeamento de campo";
$MESS["CRM_TAB_3"] = "Importar";
$MESS["CRM_TAB_3_TITLE"] = "Resultado de importação";
$MESS["CRM_IMPORT_NEXT_STEP"] = "Próximo >>";
$MESS["CRM_IMPORT_NEXT_STEP_TITLE"] = "Ir para próximo passo";
$MESS["CRM_IMPORT_PREVIOUS_STEP"] = "<< Voltar";
$MESS["CRM_IMPORT_PREVIOUS_STEP_TITLE"] = "Ir para passo anterior";
$MESS["CRM_IMPORT_DONE"] = "Concluído";
$MESS["CRM_IMPORT_DONE_TITLE"] = "Visualizar Leads";
$MESS["CRM_IMPORT_CANCEL"] = "Cancelar";
$MESS["CRM_IMPORT_CANCEL_TITLE"] = "Abortar e voltar para a lista de leads";
$MESS["CRM_IMPORT_AGAIN"] = "Importar outro arquivo";
$MESS["CRM_IMPORT_AGAIN_TITLE"] = "Clique para importar outro arquivo";
$MESS["CRM_TAB_DUP_CONTROL"] = "Controle de duplicados";
$MESS["CRM_TAB_DUP_CONTROL_TITLE"] = "Configurar controle de duplicados";
?>