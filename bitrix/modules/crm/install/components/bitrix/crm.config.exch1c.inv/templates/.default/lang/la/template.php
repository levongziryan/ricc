<?
$MESS["CRM_TAB_INVOICE_EXPORT"] = "Facturas: exportación";
$MESS["CRM_TAB_INVOICE_EXPORT_TITLE"] = "Configurar 1C: exportación de facturas empresariales";
$MESS["CRM_TAB_INVOICE_PROF_COM"] = "Facturas: perfil de la empresa";
$MESS["CRM_TAB_INVOICE_PROF_COM_TITLE"] = "Configurar 1C: exportación de campo de empresa";
$MESS["CRM_TAB_INVOICE_PROF_CON"] = "Facturas: perfil de contacto";
$MESS["CRM_TAB_INVOICE_PROF_CON_TITLE"] = "Configurar 1C: exportación de campo de empresa";
$MESS["CRM_BUTTON_SAVE"] = "Guardar";
$MESS["CRM_BUTTON_SAVE_TITLE"] = "Guardar preferencias de integración";
$MESS["CRM_BUTTON_CANCEL"] = "Cancelar";
$MESS["CRM_BUTTON_CANCEL_TITLE"] = "No guardar los parámetros actuales";
$MESS["CRM_CONFIGS_EXCH1C_LINK_TEXT"] = "Ver configuración de integración 1C";
$MESS["CRM_CONFIGS_EXCH1C_LINK_TITLE"] = "Configuración de integración 1C";
?>