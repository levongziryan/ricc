<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_IBLOCK"] = "El módulo Information Blocks no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "El módulo Currency no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "El módulo e-Store no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "El módulo Commercial Catalog no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_EXCH1C_LIST"] = "1C: Integración Empresarial";
$MESS["CRM_EXCH1C_REKV_TITLE"] = "Parámetros adicionales (pasados a 1C como propiedades de la orden)";
$MESS["SALE_1C_SALE_ACCOUNT_NUMBER_SHOP_PREFIX"] = "# Factura, prefijo para exportación";
$MESS["SALE_1C_EXPORT_FINAL_ORDERS"] = "Exportar facturas con este estado y más";
$MESS["SALE_1C_FINAL_STATUS_ON_DELIVERY"] = "Cambiar el estado de la factura al recibir el envío de 1C: Enterprise a";
$MESS["SALE_1C_REPLACE_CURRENCY"] = "Cambiar moneda a";
$MESS["SALE_1C_RUB"] = "RUB";
$MESS["SALE_1C_NO"] = "<No seleccionado>";
$MESS["SPSG_OTHER"] = "Otro valor";
$MESS["SPSG_FROM_USER"] = "Parámetro de usuario";
$MESS["SPSG_FROM_ORDER"] = "Parámetro de factura";
$MESS["SPSG_FROM_PROPS"] = "Propiedad de la factura";
$MESS["SOG_SURNAME"] = "Apellido";
$MESS["SOG_NAME"] = "Nombre";
$MESS["SOG_SECOND_NAME"] = "Segundo nombre";
$MESS["SOG_BIRTHDAY"] = "Fecha de nacimiento";
$MESS["SOG_MALE"] = "Sexo";
$MESS["SOG_INN"] = "OD de identificación fiscal";
$MESS["SOG_KPP"] = "KPP";
$MESS["SOG_ADDRESS_FULL"] = "Dirección completa";
$MESS["SOG_INDEX"] = "Código postal";
$MESS["SOG_COUNTRY"] = "País";
$MESS["SOG_REGION"] = "Región";
$MESS["SOG_STATE"] = "Distrito";
$MESS["SOG_TOWN"] = "Ciudad";
$MESS["SOG_CITY"] = "Ciudad";
$MESS["SOG_STREET"] = "Calle";
$MESS["SOG_BUILDING"] = "Edificio";
$MESS["SOG_HOUSE"] = "Casa";
$MESS["SOG_FLAT"] = "Dpto.";
$MESS["SOG_OKVED"] = "OKVED";
$MESS["SOG_OKDP"] = "OKDP";
$MESS["SOG_OKPO"] = "OKPO";
$MESS["SOG_ACCOUNT_NUMBER"] = "Cuenta bancaria";
$MESS["SOG_B_NAME"] = "Nombre del banco";
$MESS["SOG_B_BIK"] = "Código bancario";
$MESS["SOG_B_ADDRESS_FULL"] = "Dirección del banco";
$MESS["SOG_B_INDEX"] = "Código postal del banco";
$MESS["SOG_B_COUNTRY"] = "País del banco";
$MESS["SOG_B_REGION"] = "Región del banco";
$MESS["SOG_B_STATE"] = "Distrito bancario";
$MESS["SOG_B_TOWN"] = "Localidad del banco";
$MESS["SOG_B_CITY"] = "Ciudad del banco";
$MESS["SOG_B_STREET"] = "Calle del banco";
$MESS["SOG_B_BUILDING"] = "Edificio Nº del Banco ";
$MESS["SOG_AGENT_NAME"] = "Nombre";
$MESS["SOG_FULL_NAME"] = "Nombre completo";
$MESS["SOG_PHONE"] = "Teléfono de contacto";
$MESS["SOG_EMAIL"] = "Email de contacto";
$MESS["SOG_CONTACT_PERSON"] = "Persona de contacto";
$MESS["SOG_F_ADDRESS_FULL"] = "Dirección completa (dirección física)";
$MESS["SOG_F_INDEX"] = "Código postal (dirección física)";
$MESS["SOG_F_COUNTRY"] = "País (dirección física)";
$MESS["SOG_F_REGION"] = "Región (dirección física)";
$MESS["SOG_F_STATE"] = "Distrito (dirección física)";
$MESS["SOG_F_TOWN"] = "Localidad (dirección física)";
$MESS["SOG_F_CITY"] = "Ciudad (dirección física)";
$MESS["SOG_F_STREET"] = "Calle (dirección física)";
$MESS["SOG_F_BUILDING"] = "Edificio (dirección física)";
$MESS["SOG_F_HOUSE"] = "Casa (dirección física)";
$MESS["SOG_F_FLAT"] = "Dpto. (dirección física)";
$MESS["SPS_ORDER_ID"] = "ID d ela factura";
$MESS["SPS_ORDER_DATETIME"] = "Fecha de la factura";
$MESS["SPS_ORDER_DATE"] = "Fecha de la factura (sin tiempo)";
$MESS["SPS_ORDER_PRICE"] = "Total de la orden";
$MESS["SPS_ORDER_CURRENCY"] = "Moneda";
$MESS["SPS_ORDER_SUM"] = "Importe de la factura";
$MESS["SPS_ORDER_SITE"] = "Sitio Web";
$MESS["SPS_ORDER_PRICE_DELIV"] = "Precio de envio";
$MESS["SPS_ORDER_DESCOUNT"] = "Valor de descuento";
$MESS["SPS_ORDER_USER_ID"] = "ID del cliente";
$MESS["SPS_ORDER_PS"] = "ID del sistema de pago";
$MESS["SPS_ORDER_DELIV"] = "ID del servicio de envio";
$MESS["SPS_ORDER_TAX"] = "Importe del impuesto";
$MESS["SPS_JCOUNTRY"] = "país";
$MESS["SPS_JCITY"] = "ciudad";
$MESS["SPSG_ADD"] = "Más";
$MESS["CRM_EXCH1C_BTN_SHOW_ALL_TITLE"] = "Mostrar todos los campos";
$MESS["CRM_ACCOUNT_NUMBER_WARNING_TITLE"] = "Tenga en cuenta que el # de factura no debe superar los 11 caracteres.";
$MESS["SOG_EGRPO"] = "Principiante";
$MESS["SOG_OKOPF"] = "Principiante";
$MESS["SOG_OKFC"] = "Principiante";
$MESS["SOG_B_HOUSE"] = "No casa de banco.";
?>