<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Module CRM non installé";
$MESS["CRM_MODULE_NOT_INSTALLED_IBLOCK"] = "Module iblock non installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Module SALE non installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "Le module catalog n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_EXCH1C_LIST"] = "Intégration au programme '1C: Entreprise'";
$MESS["CRM_EXCH1C_REKV_TITLE"] = "Paramètres supplémentaires (sont transmis aux références de la commande en 1C)";
$MESS["SALE_1C_SALE_ACCOUNT_NUMBER_SHOP_PREFIX"] = "Préfixe du numéro de compte lors du déchargement";
$MESS["SALE_1C_EXPORT_FINAL_ORDERS"] = "Décharger les factures à commencer par le statut";
$MESS["SALE_1C_FINAL_STATUS_ON_DELIVERY"] = "Statut où il faut transférer le compte à la réception de l'envoi de '1C: Enterprise'";
$MESS["SALE_1C_REPLACE_CURRENCY"] = "Remplacer la devise pendant le déchargement dans '1C: Enterprise'";
$MESS["SALE_1C_RUB"] = "Rub.";
$MESS["SALE_1C_NO"] = "<Non indiqué>";
$MESS["SPSG_OTHER"] = "Autre valeur";
$MESS["SPSG_FROM_USER"] = "Paramètre de l'utilisateur";
$MESS["SPSG_FROM_ORDER"] = "Paramètre de la facture";
$MESS["SPSG_FROM_PROPS"] = "Propriété du compte";
$MESS["SOG_SURNAME"] = "Nom";
$MESS["SOG_NAME"] = "Dénomination";
$MESS["SOG_SECOND_NAME"] = "Patronyme";
$MESS["SOG_BIRTHDAY"] = "Date de naissance";
$MESS["SOG_MALE"] = "Sexe";
$MESS["SOG_INN"] = "Numéro de TVA intracommunautaire";
$MESS["SOG_KPP"] = "CMR";
$MESS["SOG_ADDRESS_FULL"] = "Adresse complète";
$MESS["SOG_INDEX"] = "Code Postal";
$MESS["SOG_COUNTRY"] = "Pays";
$MESS["SOG_REGION"] = "Région";
$MESS["SOG_STATE"] = "District";
$MESS["SOG_TOWN"] = "Cité";
$MESS["SOG_CITY"] = "Ville";
$MESS["SOG_STREET"] = "Rue";
$MESS["SOG_BUILDING"] = "Boîtier";
$MESS["SOG_HOUSE"] = "Maison";
$MESS["SOG_FLAT"] = "Appartement";
$MESS["SOG_EGRPO"] = "Registre général des entreprises et des organisations en Russie";
$MESS["SOG_OKVED"] = "Classification de codes de entreprise";
$MESS["SOG_OKDP"] = "Classificateur des aspects de l'activité économique en Russie";
$MESS["SOG_OKOPF"] = "Classification de entreprises";
$MESS["SOG_OKFC"] = "Classification de propriétés en Russie";
$MESS["SOG_OKPO"] = "Numéro didentification en classificateur panrusse des entreprises et institutions (OKPO)";
$MESS["SOG_ACCOUNT_NUMBER"] = "Numéro de compte bancaire";
$MESS["SOG_B_NAME"] = "Nom de la banque";
$MESS["SOG_B_BIK"] = "BIC (Bank Identification Code)";
$MESS["SOG_B_ADDRESS_FULL"] = "Adresse complète de la banque";
$MESS["SOG_B_INDEX"] = "Code postal de la banque";
$MESS["SOG_B_COUNTRY"] = "Pays de la banque";
$MESS["SOG_B_REGION"] = "Région de la banque";
$MESS["SOG_B_STATE"] = "Région de la banque";
$MESS["SOG_B_TOWN"] = "Localité de la banque";
$MESS["SOG_B_CITY"] = "Ville de la Banque";
$MESS["SOG_B_STREET"] = "Rue dans laquelle se trouve la banque";
$MESS["SOG_B_BUILDING"] = "Bâtiment de la banque";
$MESS["SOG_B_HOUSE"] = "Maison de la Banque";
$MESS["SOG_AGENT_NAME"] = "Nom";
$MESS["SOG_FULL_NAME"] = "Dénomination complète";
$MESS["SOG_PHONE"] = "Téléphone de contact";
$MESS["SOG_EMAIL"] = "E-mail de contact";
$MESS["SOG_CONTACT_PERSON"] = "Personne de contact";
$MESS["SOG_F_ADDRESS_FULL"] = "Adresse complète (Adresse réelle)";
$MESS["SOG_F_INDEX"] = "Code postal (Adresse réelle)";
$MESS["SOG_F_COUNTRY"] = "Pays (adresse actuelle)";
$MESS["SOG_F_REGION"] = "Région (Adresse réelle)";
$MESS["SOG_F_STATE"] = "Région (adresse réelle)";
$MESS["SOG_F_TOWN"] = "Lieu (adresse actuelle)";
$MESS["SOG_F_CITY"] = "Ville (Adresse réelle)";
$MESS["SOG_F_STREET"] = "Rue (adresse réelle)";
$MESS["SOG_F_BUILDING"] = "Bâtiment (l'adresse actuelle)";
$MESS["SOG_F_HOUSE"] = "Domicile (adresse réelle)";
$MESS["SOG_F_FLAT"] = "Appartement (adresse réelle)";
$MESS["SPS_ORDER_ID"] = "Code du compte (ID)";
$MESS["SPS_ORDER_DATETIME"] = "Date de la facture";
$MESS["SPS_ORDER_DATE"] = "Date de facture (pas de temps)";
$MESS["SPS_ORDER_PRICE"] = "Montant du compte";
$MESS["SPS_ORDER_CURRENCY"] = "Devises";
$MESS["SPS_ORDER_SUM"] = "Coût de la facture";
$MESS["SPS_ORDER_SITE"] = "Site";
$MESS["SPS_ORDER_PRICE_DELIV"] = "Coût de la livraison";
$MESS["SPS_ORDER_DESCOUNT"] = "Montant du rabais";
$MESS["SPS_ORDER_USER_ID"] = "Code de l'acheteur";
$MESS["SPS_ORDER_PS"] = "Code du système de paiement";
$MESS["SPS_ORDER_DELIV"] = "Code du service de livraison";
$MESS["SPS_ORDER_TAX"] = "Total des impôts";
$MESS["SPS_JCOUNTRY"] = "Pays";
$MESS["SPS_JCITY"] = "Ville";
$MESS["SPSG_ADD"] = "Encore";
$MESS["CRM_EXCH1C_BTN_SHOW_ALL_TITLE"] = "Montrer tous les champs";
$MESS["CRM_ACCOUNT_NUMBER_WARNING_TITLE"] = "Faites attention! La longueur totale du numéro de la facture avec un préfixe (pour un déchargement réussi en 1C) ne doit pas dépasser 11 symboles.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "Le module Devise n'est pas installé.";
?>