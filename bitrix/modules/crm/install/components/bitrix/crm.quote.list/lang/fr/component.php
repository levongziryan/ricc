<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_QUOTE_NUMBER"] = "Chiffre";
$MESS["CRM_COLUMN_TITLE"] = "En-tête";
$MESS["CRM_COLUMN_COMMENTS"] = "Commentaire du manager";
$MESS["CRM_COLUMN_OPPORTUNITY"] = "Total";
$MESS["CRM_COLUMN_COMPANY_ID"] = "Entreprise";
$MESS["CRM_COLUMN_COMPANY_TITLE"] = "Entreprise";
$MESS["CRM_COLUMN_DEAL_ID"] = "Affaire";
$MESS["CRM_COLUMN_DEAL_TITLE"] = "Affaire";
$MESS["CRM_COLUMN_CONTACT_ID"] = "Client";
$MESS["CRM_COLUMN_STATUS_ID"] = "Statut";
$MESS["CRM_COLUMN_CURRENCY_ID"] = "Devises";
$MESS["CRM_COLUMN_PRODUCT_ID"] = "Article";
$MESS["CRM_COLUMN_CLOSED"] = "Accompli";
$MESS["CRM_COLUMN_CLOSEDATE"] = "Achèvement";
$MESS["CRM_COLUMN_BEGINDATE"] = "Date d'établissement";
$MESS["CRM_COLUMN_MODIFY_BY"] = "Modifié(e)s par";
$MESS["CRM_COLUMN_ASSIGNED_BY"] = "Responsable";
$MESS["CRM_COLUMN_DATE_CREATE"] = "Créé le";
$MESS["CRM_COLUMN_DATE_MODIFY"] = "Date de modification";
$MESS["CRM_COLUMN_CONTACT_FULL_NAME"] = "Client";
$MESS["CRM_COLUMN_ENTITIES_LINKS"] = "Lié à";
$MESS["CRM_PRESET_NEW"] = "Nouvelles offres";
$MESS["CRM_PRESET_CHANGE_TODAY"] = "Modifiés aujourd'hui";
$MESS["CRM_PRESET_CHANGE_YESTERDAY"] = "Modifiés hier";
$MESS["CRM_PRESET_CHANGE_MY"] = "Modifiés par moi";
$MESS["CRM_PRESET_MY"] = "Mes offres";
$MESS["CRM_QUOTE_NAV_TITLE_LIST"] = "Voir tous les sites de référence";
$MESS["CRM_TASK_TITLE_PREFIX"] = "CRM:";
$MESS["CRM_TASK_TAG"] = "crm";
$MESS["CRM_BPLIST"] = "Processus d'affaires";
$MESS["CRM_BP_R_P"] = "De processus business";
$MESS["CRM_BP_R_P_TITLE"] = "Passer aux processus d'affaires relatifs à l'offre";
$MESS["CRM_TASKS"] = "Tâches";
$MESS["CRM_COLUMN_COMPANY_LIST"] = "Entreprises";
$MESS["CRM_COLUMN_DEAL_LIST"] = "Transactions";
$MESS["CRM_COLUMN_CONTACT_LIST"] = "Contacts";
$MESS["CRM_COLUMN_ALL"] = "(partout)";
$MESS["CRM_COLUMN_BINDING"] = "Objet cible";
$MESS["CRM_INTERNAL"] = "CRM";
$MESS["CRM_COLUMN_PRODUCT"] = "Article";
$MESS["CRM_COLUMN_CREATED_BY"] = "Créé par";
$MESS["CRM_COLUMN_QUOTE"] = "Offre";
$MESS["CRM_COLUMN_CLIENT"] = "Client";
$MESS["CRM_COLUMN_SUM"] = "Montant/Devise";
$MESS["CRM_COLUMN_LEAD_ID"] = "Prospect";
$MESS["CRM_COLUMN_LEAD_TITLE"] = "Prospect";
$MESS["CRM_COLUMN_LEAD_LIST"] = "Prospects";
$MESS["CRM_COLUMN_MYCOMPANY_LIST"] = "Liste des sociétés";
$MESS["CRM_COLUMN_MYCOMPANY_ID"] = "Détails de votre société";
$MESS["CRM_COLUMN_MYCOMPANY_TITLE"] = "Détails de votre société";
$MESS["CRM_COLUMN_WEBFORM"] = "Créé par formulaire CRM";
$MESS["CRM_QUOTE_NAV_TITLE_LIST_SHORT"] = "Devis";
$MESS["CRM_PRESET_MY_IN_WORK"] = "My (in progress)";
$MESS["CRM_COLUMN_MYCOMPANY_ID1"] = "Données de votre société";
$MESS["CRM_COLUMN_MYCOMPANY_TITLE1"] = "Données de votre société";
?>