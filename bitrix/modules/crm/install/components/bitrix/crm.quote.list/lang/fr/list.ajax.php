<?
$MESS["CRM_QUOTE_LIST_ROW_COUNT"] = "Total : #ROW_COUNT#";
$MESS["CRM_QUOTE_LIST_REBUILD_SEARCH_CONTENT_NOT_REQUIRED_SUMMARY"] = "L'index de recherche des devis n'a pas besoin d'être recréé.";
$MESS["CRM_QUOTE_LIST_REBUILD_SEARCH_CONTENT_PROGRESS_SUMMARY"] = "Devis traités : #PROCESSED_ITEMS# sur #TOTAL_ITEMS#.";
$MESS["CRM_QUOTE_LIST_REBUILD_SEARCH_CONTENT_COMPLETED_SUMMARY"] = "L'index de recherche des devis a été recréé. Devis traités : #PROCESSED_ITEMS#.";
?>