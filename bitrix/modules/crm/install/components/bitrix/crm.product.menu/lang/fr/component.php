<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_PRODUCT_ADD"] = "Ajouter le produit";
$MESS["CRM_PRODUCT_ADD_TITLE"] = "Le passage à la création d'un nouveau produit";
$MESS["CRM_PRODUCT_EDIT"] = "Editer";
$MESS["CRM_PRODUCT_EDIT_TITLE"] = "Allez à la rédaction du produit";
$MESS["CRM_PRODUCT_DELETE"] = "Suppression du produit";
$MESS["CRM_PRODUCT_DELETE_TITLE"] = "Suppression du produit";
$MESS["CRM_PRODUCT_DELETE_DLG_TITLE"] = "Suppression du produit";
$MESS["CRM_PRODUCT_DELETE_DLG_MESSAGE"] = "tes-vous sûr de vouloir supprimer cette marchandise?";
$MESS["CRM_PRODUCT_DELETE_DLG_BTNTITLE"] = "Suppression du produit";
$MESS["CRM_PRODUCT_SHOW"] = "Affichage";
$MESS["CRM_PRODUCT_SHOW_TITLE"] = "Voir le produit";
$MESS["CRM_PRODUCT_LIST_TITLE"] = "Passage à la liste des marchandises";
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "Le module des blocs d'informations n'a pas été installé.";
$MESS["CRM_MOVE_UP"] = "Déplacer vers le haut";
$MESS["CRM_MOVE_UP_TITLE"] = "Accéder à la section-parent";
$MESS["CRM_PRODUCT_SECTIONS"] = "Gestion des sections";
$MESS["CRM_PRODUCT_SECTIONS_TITLE"] = "Suppression et édition des sections";
$MESS["CRM_ADD_PRODUCT_SECTION"] = "Ajouter une section";
$MESS["CRM_ADD_PRODUCT_SECTION_TITLE"] = "Créer une nouvelle section à la section courante";
$MESS["CRM_PRODUCT_IMPORT"] = "L'importation de produit";
$MESS["CRM_PRODUCT_IMPORT_TITLE"] = "L'importation de produit";
$MESS["CRM_PRODUCT_LIST"] = "Produits";
?>