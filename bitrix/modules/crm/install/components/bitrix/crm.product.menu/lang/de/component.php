<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Das Modul CRM ist nicht installiert.";
$MESS["CRM_PERMISSION_DENIED"] = "Zugriff verweigert";
$MESS["CRM_PRODUCT_ADD"] = "Produkt hinzufügen";
$MESS["CRM_PRODUCT_ADD_TITLE"] = "Ein neues Produkt hinzufügen";
$MESS["CRM_PRODUCT_EDIT"] = "Bearbeiten";
$MESS["CRM_PRODUCT_EDIT_TITLE"] = "Produkt bearbeiten";
$MESS["CRM_PRODUCT_DELETE"] = "Produkt löschen";
$MESS["CRM_PRODUCT_DELETE_TITLE"] = "Dieses Produkt löschen";
$MESS["CRM_PRODUCT_DELETE_DLG_TITLE"] = "Produkt löschen";
$MESS["CRM_PRODUCT_DELETE_DLG_MESSAGE"] = "Möchten Sie dieses Produkt wirklich löschen?";
$MESS["CRM_PRODUCT_DELETE_DLG_BTNTITLE"] = "Produkt löschen";
$MESS["CRM_PRODUCT_SHOW"] = "Anzeigen";
$MESS["CRM_PRODUCT_SHOW_TITLE"] = "Produkt anzeigen";
$MESS["CRM_PRODUCT_LIST"] = "Produkte";
$MESS["CRM_PRODUCT_LIST_TITLE"] = "Produkte anzeigen";
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "Das Modul Informationsblöcke ist nicht installiert.";
$MESS["CRM_MOVE_UP"] = "Nach oben";
$MESS["CRM_MOVE_UP_TITLE"] = "In den übergeordneten Bereich wechseln";
$MESS["CRM_PRODUCT_SECTIONS"] = "Bereiche verwalten";
$MESS["CRM_PRODUCT_SECTIONS_TITLE"] = "Bereiche verwalten und bearbeiten";
$MESS["CRM_ADD_PRODUCT_SECTION"] = "Bereich hinzufügen";
$MESS["CRM_ADD_PRODUCT_SECTION_TITLE"] = "Zum aktuellen Bereich einen neuen Bereich hinzufügen";
$MESS["CRM_PRODUCT_IMPORT"] = "Produktimport";
$MESS["CRM_PRODUCT_IMPORT_TITLE"] = "Produktimport";
?>