<?
$MESS["CRM_MEASURE_FIELD_ID"] = "ID";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_SECTION_MEASURE_INFO"] = "Unité de mesure";
$MESS["CRM_MEASURE_FIELD_CODE"] = "Code";
$MESS["CRM_MEASURE_FIELD_SYMBOL_LETTER_INTL"] = "Notation littérale codée (internationale)";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CATALOG_MODULE_NOT_INSTALLED"] = "Le module 'Catalogue de marchandises' n'a pas été installé.";
$MESS["CRM_MEASURE_FIELD_MEASURE_TITLE"] = "Dénomination de l'unité de mesure";
$MESS["CRM_MEASURE_FIELD_IS_DEFAULT"] = "Par défaut";
$MESS["CRM_MEASURE_ERR_CODE_EMPTY"] = "Veuillez renseigner le code d'unité de mesure. Le code doit être un nombre entier positif.";
$MESS["CRM_MEASURE_ERR_TITLE_EMPTY"] = "Veuillez choisir le nom de l'unité de mesure.";
$MESS["CRM_MEASURE_ERR_UPDATE"] = "Une erreur inconnue est survenue pendant la mise à jour de l'unité de mesure.";
$MESS["CRM_MEASURE_ERR_CREATE"] = "Une erreur inconnue est survenue pendant la création de l'unité de mesure.";
$MESS["CRM_MEASURE_FIELD_SYMBOL_RUS"] = "Désignation conventionnelle";
$MESS["CRM_MEASURE_FIELD_SYMBOL_INTL"] = "Désignation conventionnelle (internationale)";
$MESS["CRM_MEASURE_ERR_CODE_INVALID"] = "L'ID de l'unité de mesure ne peut contenir que des chiffres.";
$MESS["CRM_MEASURE_ERR_ALREADY_EXISTS"] = "Il existe déjà une unité de mesure avec l'ID \"#CODE#\".";
?>