<?
$MESS["CRM_TAB_CONFIG"] = "Envio de e-mail";
$MESS["CRM_TAB_CONFIG_TITLE"] = "Configurar parâmetros de envio de e-mail";
$MESS["CRM_TAB_ACTIVITY_CONFIG"] = "Atividade";
$MESS["CRM_TAB_ACTIVITY_CONFIG_TITLE"] = "Parâmetros da atividade";
$MESS["CRM_TAB_FORMAT"] = "Formato";
$MESS["CRM_TAB_FORMAT_TITLE"] = "Parâmetros de formato";
$MESS["CRM_TAB_INVNUM"] = "Números da fatura";
$MESS["CRM_TAB_INVNUM_TITLE"] = "Template de números da fatura";
$MESS["CRM_TAB_QUOTENUM"] = "ID's dos Orçamentos";
$MESS["CRM_TAB_QUOTENUM_TITLE"] = "Formato de numeração do ID do orçamento";
$MESS["CRM_TAB_DUPLICATE_CONTROL"] = "Controle de duplicatas";
$MESS["CRM_TAB_DUPLICATE_CONTROL_TITLE"] = "Duplicar parâmetros de controle ";
$MESS["CRM_TAB_STATUS_CONFIG"] = "Dicionários";
$MESS["CRM_TAB_STATUS_CONFIG_TITLE"] = "Configurações de Dicionário";
$MESS["CRM_TAB_DEAL_CONFIG"] = "Negociações";
$MESS["CRM_TAB_DEAL_CONFIG_TITLE"] = "Parâmetros de negociação";
$MESS["CRM_TAB_GENERAL"] = "Geral";
$MESS["CRM_TAB_GENERAL_TITLE"] = "Parâmetros comuns";
$MESS["CRM_TAB_HISTORY"] = "Histórico";
$MESS["CRM_TAB_HISTORY_TITLE"] = "Configurações do histórico";
$MESS["CRM_TAB_LIVEFEED"] = "Fluxo de Atividade";
$MESS["CRM_TAB_LIVEFEED_TITLE"] = "Configurações do Fluxo de Atividade";
$MESS["CRM_TAB_REST"] = "Aplicativos";
$MESS["CRM_TAB_REST_TITLE"] = "Configurações de aplicativo";
$MESS["CRM_TAB_TAB_OUTGOING_EMAIL"] = "Mensagens de saída";
$MESS["CRM_TAB_TAB_OUTGOING_EMAIL_TITLE"] = "Parâmetros de saída de e-mail";
?>