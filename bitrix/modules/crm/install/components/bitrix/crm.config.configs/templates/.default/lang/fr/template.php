<?
$MESS["CRM_TAB_CONFIG"] = "Envoi de lettres";
$MESS["CRM_TAB_CONFIG_TITLE"] = "Paramètres de l'envoi des lettres";
$MESS["CRM_TAB_ACTIVITY_CONFIG"] = "Activité";
$MESS["CRM_TAB_ACTIVITY_CONFIG_TITLE"] = "Configuration des activités";
$MESS["CRM_TAB_FORMAT"] = "Affichage";
$MESS["CRM_TAB_FORMAT_TITLE"] = "Paramètres de mise en page";
$MESS["CRM_TAB_INVNUM"] = "Numéros des factures";
$MESS["CRM_TAB_INVNUM_TITLE"] = "Modèle de numéro de facture";
$MESS["CRM_TAB_QUOTENUM"] = "Numéros des offres";
$MESS["CRM_TAB_QUOTENUM_TITLE"] = "Modèle de génération de numéro d'offre";
$MESS["CRM_TAB_DUPLICATE_CONTROL"] = "Contrôle des duplicatas";
$MESS["CRM_TAB_DUPLICATE_CONTROL_TITLE"] = "Paramètres de contrôle de doubles";
$MESS["CRM_TAB_STATUS_CONFIG"] = "Indicatrices";
$MESS["CRM_TAB_STATUS_CONFIG_TITLE"] = "Réglages pour les listes de sélection";
$MESS["CRM_TAB_DEAL_CONFIG"] = "Transactions";
$MESS["CRM_TAB_DEAL_CONFIG_TITLE"] = "Paramètres Deal";
$MESS["CRM_TAB_GENERAL"] = "Général";
$MESS["CRM_TAB_GENERAL_TITLE"] = "Paramètres communs";
$MESS["CRM_TAB_HISTORY"] = "Historique";
$MESS["CRM_TAB_HISTORY_TITLE"] = "Paramètres des historiques";
$MESS["CRM_TAB_LIVEFEED"] = "Flux d'activités";
$MESS["CRM_TAB_LIVEFEED_TITLE"] = "Paramètres du Flux d'activités";
$MESS["CRM_TAB_REST"] = "Applications";
$MESS["CRM_TAB_REST_TITLE"] = "Paramètres d'application";
$MESS["CRM_TAB_TAB_OUTGOING_EMAIL"] = "Messages émis";
$MESS["CRM_TAB_TAB_OUTGOING_EMAIL_TITLE"] = "Paramètres du courrier électronique sortant";
?>