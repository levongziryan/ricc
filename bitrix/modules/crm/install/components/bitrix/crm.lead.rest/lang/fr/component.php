<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_REST_ERROR_BAD_REQUEST"] = "Erreur de demande";
$MESS["CRM_REST_ERROR_BAD_AUTH"] = "Erreur d'autorisation";
$MESS["CRM_REST_OK"] = "Un prospect a été ajouté";
$MESS["UNKNOWN_ERROR"] = "Erreur inconnue.";
?>