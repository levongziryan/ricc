<?
$MESS["CRM_LEAD_DETAIL_HISTORY_STUB"] = "Você está adicionando um cliente potencial agora...";
$MESS["CRM_LEAD_CONV_ACCESS_DENIED"] = "Você precisa de permissão de criação de contato, empresa e venda para converter um cliente potencial.";
$MESS["CRM_LEAD_CONV_GENERAL_ERROR"] = "Erro de conversão genérico.";
$MESS["CRM_LEAD_CONV_DIALOG_TITLE"] = "Converter Cliente Potencial";
$MESS["CRM_LEAD_CONV_DIALOG_SYNC_LEGEND"] = "As entidades selecionadas não têm campos que podem armazenar dados de cliente potencial. Selecione entidades em que campos faltantes serão criados para acomodar todas as informações disponíveis.";
$MESS["CRM_LEAD_CONV_DIALOG_SYNC_FILED_LIST_TITLE"] = "Estes campos serão criados";
$MESS["CRM_LEAD_CONV_DIALOG_SYNC_ENTITY_LIST_TITLE"] = "Selecionar as entidades nas quais os campos adicionais serão criados";
$MESS["CRM_LEAD_CONV_OPEN_ENTITY_SEL"] = "Selecionar da lista...";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_TITLE"] = "Selecionar contato e empresa";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_CONTACT"] = "Contatos";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_COMPANY"] = "Empresas";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_SEARCH"] = "Pesquisar";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_SEARCH_NO_RESULT"] = "Não foram encontradas entradas.";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_LAST"] = "Último";
$MESS["CRM_LEAD_CONV_DEAL_CATEGORY_DLG_TITLE"] = "Preferências de venda";
$MESS["CRM_LEAD_CONV_DEAL_CATEGORY_DLG_FIELD"] = "Pipeline";
$MESS["CRM_LEAD_DETAIL_CONTINUE_BTN"] = "Continuar";
$MESS["CRM_LEAD_DETAIL_CANCEL_BTN"] = "Cancelar";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_BTN"] = "Selecionar";
$MESS["CRM_LEAD_DETAIL_BUTTON_SAVE"] = "Salvar";
$MESS["CRM_LEAD_DETAIL_BUTTON_CANCEL"] = "Cancelar";
?>