<?
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_MEASURE_LIST"] = "Todas as unidades";
$MESS["CRM_MEASURE_LIST_TITLE"] = "Ver todas as unidades de medida";
$MESS["CRM_MEASURE_ADD"] = "Adicionar";
$MESS["CRM_MEASURE_ADD_TITLE"] = "Criar uma nova unidade de medida";
$MESS["CRM_MEASURE_EDIT"] = "Editar";
$MESS["CRM_MEASURE_EDIT_TITLE"] = "Editar unidade de medida";
$MESS["CRM_MEASURE_DELETE"] = "Excluir";
$MESS["CRM_MEASURE_DELETE_TITLE"] = "Excluir unidade de medida";
$MESS["CRM_MEASURE_DELETE_DLG_TITLE"] = "Excluir unidade de medida";
$MESS["CRM_MEASURE_DELETE_DLG_MESSAGE"] = "Tem certeza de que deseja excluir a unidade?";
$MESS["CRM_MEASURE_DELETE_DLG_BTNTITLE"] = "Excluir unidade";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["CATALOG_MODULE_NOT_INSTALLED"] = "O módulo de Catálogo Comercial não está instalado.";
?>