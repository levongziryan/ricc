<?
$MESS["CRM_MEASURE_DELETE_DLG_MESSAGE"] = "tes-vous sûr de vouloir supprimer cette unité de mesure?";
$MESS["CRM_MEASURE_ADD"] = "Ajouter";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CATALOG_MODULE_NOT_INSTALLED"] = "Le module 'Catalogue de marchandises' n'a pas été installé.";
$MESS["CRM_MEASURE_EDIT_TITLE"] = "Passer en mode édition de l'unité de mesure";
$MESS["CRM_MEASURE_ADD_TITLE"] = "Accéder à la création de la nouvelle unité de mesure";
$MESS["CRM_MEASURE_LIST_TITLE"] = "Accéder à la liste des unités de mesure";
$MESS["CRM_MEASURE_EDIT"] = "Editer";
$MESS["CRM_MEASURE_LIST"] = "Liste";
$MESS["CRM_MEASURE_DELETE_DLG_TITLE"] = "Suppression de l'unité de mesure";
$MESS["CRM_MEASURE_DELETE"] = "Supprimer";
$MESS["CRM_MEASURE_DELETE_TITLE"] = "Suppression de l'unité de mesure";
$MESS["CRM_MEASURE_DELETE_DLG_BTNTITLE"] = "Supprimer l'unité de mesure";
?>