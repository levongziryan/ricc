<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "Le module Processus opérationnels n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "Le module Devise n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Le module e-Store n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "Le module Catalogue commercial n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
$MESS["CRM_BUTTON_LIST_TITLE"] = "Widgets";
?>