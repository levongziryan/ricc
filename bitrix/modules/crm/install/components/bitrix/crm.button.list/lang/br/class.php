<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "O módulo Processos de Negócio não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "O módulo Moeda não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "O módulo e-Store não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "O módulo Catálogo Comercial não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_BUTTON_LIST_TITLE"] = "Widgets";
?>