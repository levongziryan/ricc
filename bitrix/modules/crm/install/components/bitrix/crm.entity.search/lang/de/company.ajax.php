<?
$MESS["CRM_ES_WINDOW_TITLE"] = "Unternehmen auswählen";
$MESS["CRM_ES_SUBMIT_TITLE"] = "Wählen Sie ein Unternehmen aus";
$MESS["CRM_ES_CANCEL_TITLE"] = "Auswahl abbrechen";
$MESS["CRM_ES_SEARCH"] = "Unternehmen suchen";
$MESS["CRM_ES_LIST"] = "Unternehmen";
$MESS["CRM_ES_WINDOW_CLOSE"] = "Schließen";
$MESS["CRM_ES_WAIT"] = "Warten Sie bitte, die Liste wird geladen";
$MESS["CRM_ES_SUBMIT"] = "Auswählen";
$MESS["CRM_ES_CANCEL"] = "Abbrechen";
$MESS["CRM_ES_LAST"] = "Zuletzt ausgewählte Elemente";
$MESS["CRM_ES_NOTHING_FOUND"] = "Die Suche brachte keine Ergebnisse";
?>