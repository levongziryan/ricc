<?
$MESS["CRM_ES_WINDOW_TITLE"] = "Выбор контакта";
$MESS["CRM_ES_SUBMIT_TITLE"] = "Выбрать выделенный контакт";
$MESS["CRM_ES_CANCEL_TITLE"] = "Отменить выбор контакта";
$MESS["CRM_ES_SEARCH"] = "поиск контакта";
$MESS["CRM_ES_LIST"] = "Список контактов";
$MESS["CRM_ES_COMPANY_NONE"] = "Компания не указана";
$MESS["CRM_ES_WINDOW_CLOSE"] = "Закрыть";
$MESS["CRM_ES_WAIT"] = "Подождите, идет загрузка списка...";
$MESS["CRM_ES_SUBMIT"] = "Выбрать";
$MESS["CRM_ES_CANCEL"] = "Отменить";
$MESS["CRM_ES_LAST"] = "Последние выбранные";
$MESS["CRM_ES_NOTHING_FOUND"] = "ничего не найдено";
?>