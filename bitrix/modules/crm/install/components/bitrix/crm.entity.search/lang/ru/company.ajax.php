<?
$MESS["CRM_ES_WINDOW_TITLE"] = "Выбор компании";
$MESS["CRM_ES_SUBMIT_TITLE"] = "Выбрать выделенную компанию";
$MESS["CRM_ES_CANCEL_TITLE"] = "Отменить выбор компании";
$MESS["CRM_ES_SEARCH"] = "поиск компании";
$MESS["CRM_ES_LIST"] = "Список компаний";
$MESS["CRM_ES_WINDOW_CLOSE"] = "Закрыть";
$MESS["CRM_ES_WAIT"] = "Подождите, идет загрузка списка...";
$MESS["CRM_ES_SUBMIT"] = "Выбрать";
$MESS["CRM_ES_CANCEL"] = "Отменить";
$MESS["CRM_ES_LAST"] = "Последние выбранные";
$MESS["CRM_ES_NOTHING_FOUND"] = "ничего не найдено";
?>