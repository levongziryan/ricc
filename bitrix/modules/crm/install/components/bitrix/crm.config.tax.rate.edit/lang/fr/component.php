<?
$MESS["CRM_EVENT_DEFAULT_TITLE"] = "Pas de données.";
$MESS["CRM_ERROR_NO_LOCATION"] = "Erreur: localisation introuvable.";
$MESS["CRM_ERROR_ADD_TAX_RATE"] = "Une erreur a eu lieu lors de l'ajout du taux de l'impôt.";
$MESS["CRM_ERROR_EDIT_TAX_RATE"] = "Erreur au cours du rafraîchissement du taux de l'impôt.";
?>