<?
$MESS["CRM_EVENT_DEFAULT_TITLE"] = "No hay datos.";
$MESS["CRM_ERROR_EDIT_TAX_RATE"] = "Un error al actualizar la tasa de impuestos.";
$MESS["CRM_ERROR_ADD_TAX_RATE"] = "Error al agregar la tasa de impuestos.";
$MESS["CRM_ERROR_NO_LOCATION"] = "Error: no existen localidades.";
?>