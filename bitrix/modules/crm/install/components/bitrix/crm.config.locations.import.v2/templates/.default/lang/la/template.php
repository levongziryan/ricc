<?
$MESS["SALE_SLI_TAB_IMPORT_TITLE"] = "Importar ubicación";
$MESS["SALE_SLI_TAB_CLEANUP_TITLE"] = "Quite todas las ubicaciones";
$MESS["SALE_SLI_PROC_NEUTRAL_ERROR"] = "Hubo errores de importación.";
$MESS["SALE_SLI_NEUTRAL_ERROR"] = "Se ha producido un error fatal. La importación no puede completarse.";
$MESS["SALE_SLI_START"] = "Inicio de importación";
$MESS["SALE_SLI_STOP"] = "Detener importación";
$MESS["SALE_SLI_STOPPING"] = "Detener ahora...";
$MESS["SALE_SLI_ERROR"] = "Error";
$MESS["SALE_SLI_STAGE_INITIAL"] = "a partir";
$MESS["SALE_SLI_STAGE_DOWNLOAD_FILES"] = "carga de archivos";
$MESS["SALE_SLI_STAGE_DELETE_ALL"] = "Acabar con la actual estructura de ubicación";
$MESS["SALE_SLI_STAGE_PROCESS_FILES"] = "leer los archivos y los datos de localización de carga";
$MESS["SALE_SLI_STAGE_COMPLETE"] = "Importación completa";
$MESS["SALE_SLI_STAGE_INTERRUPTED"] = "Cancelar importación";
$MESS["SALE_SLI_STAGE_INTERRUPTING"] = "cancelar ahora";
$MESS["SALE_SLI_STAGE_CLEANUP_TEMP_TABLE"] = "eliminación de datos temporales";
$MESS["SALE_SLI_STAGE_RESTORE_INDEXES"] = "restauración de los datos de la base de datos";
$MESS["SALE_SLI_STAGE_INTEGRITY_PRESERVE"] = "Verificar integridad de la estructura";
$MESS["SALE_SLI_DROP_INDEXES"] = "eliminar índice de la base de datos";
$MESS["SALE_SLI_LOCATION_SOURCE"] = "Origen de ubicación";
$MESS["SALE_SLI_SOURCE_REMOTE"] = "Servidor remoto";
$MESS["SALE_SLI_SOURCE_FILE"] = "Archivo";
$MESS["SALE_SLI_FILE_UPLOADED"] = "Archivo cargado";
$MESS["SALE_SLI_RELOAD_FILE"] = "recargar";
$MESS["SALE_SLI_FILE_UPLOAD_ERROR"] = "Error de carga de archivo.";
$MESS["SALE_SLI_RETRY_FILE_UPLOAD"] = "inténtelo de nuevo";
$MESS["SALE_SLI_FILE_IS_BEING_UPLOADED"] = "Carga de archivos";
$MESS["SALE_SLI_SOURCE_FILE_NOTES"] = "Al utilizar esta fuente de datos, usted tiene que crear #ANCHOR_LOCTYPES#tipos de ubicación#ANCHOR_END# y #ANCHOR_EXT_SERVS#servicios externos#ANCHOR_END# manualmente.";
$MESS["SALE_SLI_DONT_LIMIT_LOCATION_DEPTH"] = "Todo";
$MESS["SALE_SLI_EXTRA_DATA"] = "Seleccionar datos adicionales";
$MESS["SALE_SLI_EXTRA_EXTERNAL_ZIP"] = "Códigos postales";
$MESS["SALE_SLI_EXTRA_GEOCOORDS"] = "Coordenadas de ubicación geográfica";
$MESS["SALE_SLI_ADDITIONAL_PARAMS"] = "Seleccione la ubicación parámetros de importación";
$MESS["SALE_SLI_AP_DROP_STRUCTURE"] = "Eliminar todas las ubicaciones existentes";
$MESS["SALE_SLI_AP_TIMELIMIT"] = "Duración de paso, sec";
$MESS["SALE_SLI_AP_PRESERVE_INTEGRITY"] = "Preservar integridad de la estructura";
$MESS["SALE_SLI_SOURCE"] = "Seleccione el origen de ubicación";
$MESS["SALE_SLI_DELETE_ALL"] = "Elimina todas las ubicaciones";
$MESS["SALE_SLI_DELETE_ALL_CONFIRM"] = "¿Seguro que quieres eliminar todas las ubicaciones?";
$MESS["SALE_SLI_DELETE_ALL_CONFIRM_RELIC"] = "Al eliminar las antiguas ubicaciones se eliminará las órdenes creadas; un ID será visto en lugar de un ubicación. Puede importar nuevas ubicaciones ahora o hacerlo más tarde, después de haber actualizado las ordenes afectadas en primer lugar.
¿Desea retrasar la actual importación y guardar ubicaciones existentes?";
$MESS["SALE_SLI_STAT_TITLE"] = "Estadísticas de ubicación de la base de data";
$MESS["SALE_SLI_STAT_TOTAL"] = "Total de ubicaciones";
$MESS["SALE_SLI_STAT_TOTAL_GROUPS"] = "Total de grupos de ubicación";
$MESS["SALE_SLI_STAT_REFRESH"] = "Actualizar";
$MESS["SALE_SLI_CHECK_ITEMS_AND_PROCEED"] = "Por favor, seleccione las ubicaciones que desea importar y haga clic \"#START#\"";
$MESS["SALE_SLI_UPLOAD_FILE_AND_PROCEED"] = "Por favor, sube un archivo de ubicaciones y haga clic en \"#START#\"";
$MESS["SALE_SLI_STATUS"] = "Estado";
$MESS["SALE_SLI_REMOVE_ALL"] = "Quite todas las ubicaciones";
$MESS["SALE_SLI_COMPLETE_REMOVE_ALL"] = "ubicación de purga de base de datos";
$MESS["SALE_SLI_HEAVY_DUTY_NOTICE"] = "Importar las ubicaciones puntuales de una calle en que le le tomará al menos media hora, o hasta varias horas para llegar al lugar (dependiendo del hardware preferencias y servidor).";
$MESS["SALE_SLI_LOCATION_PACK"] = "Pack de ubicaciones";
$MESS["SALE_SLI_LOCATION_PACK_STANDARD"] = "Estándar";
$MESS["SALE_SLI_LOCATION_PACK_EXTENDED"] = "Extendido";
$MESS["SALE_SLI_LOAD_LOCATIONS_TILL_INCLUSIVELY"] = "Lugares de importación a (incl.)";
$MESS["SALE_SLI_HEAVY_DUTY_HOST_NOTICE"] = "la importación de todas las ubicaciones pueden aumentar brevemente la base de datos y el servidor web de carga. Le recomendamos que consulte al servicio de helpdesk  de su servidor antes de importar grandes conjuntos de ubicación.";
$MESS["SALE_SLI_STAGE_REBALANCE"] = "Reajuste de datos en la vista de árbol";
$MESS["SALE_SLI_EXCLUDE_AREAS"] = "No cargue las regiones";
?>