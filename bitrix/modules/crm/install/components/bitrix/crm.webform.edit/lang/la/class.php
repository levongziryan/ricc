<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "El módulo Business Processes no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "El módulo Currency no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "El módulo e-Store no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "El módulo Commercial Catalog no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_REST"] = "El módulo Rest no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_WEBFORM_EDIT_TITLE"] = "Editar formulario del CRM";
$MESS["CRM_WEBFORM_EDIT_"] = "Buscar";
$MESS["CRM_WEBFORM_EDIT_TITLE_ADD"] = "Crear formulario en el CRM";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_DOMAIN"] = "Nombre del dominio";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_DOMAIN_DESC"] = "Nombre del dominio desde el que se envía el formulario";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_URL"] = "Dirección de la página";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_URL_DESC"] = "Dirección de la página desde la que se envía el formulario";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_PARAM"] = "Parámetro";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_PARAM_DESC"] = "URL del nombre del parámetro que utilizará para reconocer el formulario";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_RESULT_ID"] = "ID del resultado";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_RESULT_ID_DESC"] = "Especifica el número asignado al resultado del formulario.";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_FORM_ID"] = "ID del formulario";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_FORM_ID_DESC"] = "ID del formulario";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_FORM_NAME"] = "Nombre del formulario";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_FORM_NAME_DESC"] = "Nombre del formulario";
$MESS["CRM_WEBFORM_EDIT_SECOND_SHORT"] = "seg";
$MESS["CRM_WEBFORM_EDIT_TITLE_VIEW"] = "Ver el formulario del CRM";
?>