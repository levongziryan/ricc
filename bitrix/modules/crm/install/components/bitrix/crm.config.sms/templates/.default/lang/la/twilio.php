<?
$MESS["CRM_CONFIG_SMS_TITLE"] = "Notificación simple y robusta de SMS";
$MESS["CRM_CONFIG_SMS_TITLE_2"] = "Desarrollado por TWILIO.COM";
$MESS["CRM_CONFIG_SMS_FEATURES_TITLE"] = "Explora posibilidades de marketing SMS";
$MESS["CRM_CONFIG_SMS_FEATURES_LIST_DESCRIPTION"] = "Bitrix24 proporciona mensajes de texto SMS gratuitos y sencillos";
$MESS["CRM_CONFIG_SMS_FEATURES_LIST_1"] = "Envía mensajes directamente desde el prospecto, negociación, cliente o cotización.";
$MESS["CRM_CONFIG_SMS_FEATURES_LIST_2"] = "Configure reglas para enviar mensajes SMS a un cliente o empleado a una hora específica o cuando se complete una tarea específica.";
$MESS["CRM_CONFIG_SMS_FEATURES_LIST_3"] = "Envío masivo de mensajes SMS desde una lista de clientes, prospecto, negociación u otras entidades del CRM.";
$MESS["CRM_CONFIG_SMS_RULES_LIST_TITLE"] = "Configura los parámetros de twilio.com y comienza a usar la notificación de SMS con Bitrix24";
$MESS["CRM_CONFIG_SMS_RULES_LIST_1"] = "Inicie sesión o cree una cuenta nueva con #A1#twilio.com#A2#";
$MESS["CRM_CONFIG_SMS_RULES_LIST_2"] = "Registrar y configurar un número de teléfono del remitente de SMS";
$MESS["CRM_CONFIG_SMS_RULES_LIST_3"] = "Copie el token y el SID en #A1#twilio.com#A2# su página de perfil";
$MESS["CRM_CONFIG_SMS_RULES_LIST_4"] = "Pegue el Token y el SID en el campo a continuación";
$MESS["CRM_CONFIG_SMS_LABEL_ACCOUNT_SID"] = "SID";
$MESS["CRM_CONFIG_SMS_LABEL_ACCOUNT_TOKEN"] = "Token";
$MESS["CRM_CONFIG_SMS_ACTION_REGISTRATION"] = "Conectar";
$MESS["CRM_CONFIG_SMS_REGISTRATION_INFO"] = "Usted tiene su información de registro en twilio.com";
$MESS["CRM_CONFIG_SMS_LABEL_ACCOUNT_FRIENDLY_NAME"] = "Nombre de la cuenta";
$MESS["CRM_CONFIG_SMS_CLEAR_OPTIONS"] = "Reiniciar ajustes";
$MESS["CRM_CONFIG_SMS_CLEAR_CONFIRMATION"] = "¿Está seguro que quiere restablecer la configuración?";
$MESS["CRM_CONFIG_SMS_CABINET_TITLE"] = "¡El servicio de Twilio.com ya está listo!";
$MESS["CRM_CONFIG_SMS_DEMO_IS_DISABLED"] = "Abre tu #A1#twilio.com#A2# perfil.";
$MESS["CRM_CONFIG_SMS_REGISTRATION_ERROR"] = "Los campos obligatorios son incorrectos";
$MESS["CRM_CONFIG_SMS_RING_1"] = "Fácil conectividad";
$MESS["CRM_CONFIG_SMS_RING_2"] = "Entrega inmediata";
$MESS["CRM_CONFIG_SMS_RING_3"] = "Pague solo por mensajes entregados.";
?>