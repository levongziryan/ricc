<?
$MESS["CRM_CONFIG_SMS_TITLE"] = "Les notifications par SMS sont simples";
$MESS["CRM_CONFIG_SMS_TITLE_2"] = "Fourni par SMS.RU";
$MESS["CRM_CONFIG_SMS_FEATURES_TITLE"] = "Explorez les possibilités du marketing par SMS";
$MESS["CRM_CONFIG_SMS_FEATURES_LIST_DESCRIPTION"] = "Bitrix24 fournit une messagerie SMS facile et sans tracas";
$MESS["CRM_CONFIG_SMS_FEATURES_LIST_1"] = "Envoyez des messages directement à partir d'une piste, d'une transaction, d'un client ou d'un devis.";
$MESS["CRM_CONFIG_SMS_FEATURES_LIST_2"] = "Configurez les règles pour envoyer des SMS à un client ou un employé à une heure précise ou quand une tâche spécifiée est accomplie.";
$MESS["CRM_CONFIG_SMS_FEATURES_LIST_3"] = "Envoyez plusieurs SMS à partir d'une liste de clients, de transactions, de pistes ou d'autres entités GRC.";
$MESS["CRM_CONFIG_SMS_RULES_LIST_TITLE"] = "Configurez les paramètres de twilio.com et commencez à utiliser les notifications par SMS avec Bitrix24";
$MESS["CRM_CONFIG_SMS_RULES_LIST_1"] = "Connectez-vous et créez un nouveau compte avec #A1#twilio.com#A2#";
$MESS["CRM_CONFIG_SMS_RULES_LIST_2"] = "Inscrivez et configurez un numéro de téléphone d'expéditeur de SMS";
$MESS["CRM_CONFIG_SMS_RULES_LIST_3"] = "Copiez le jeton et le SID sur votre page de profil #A1#twilio.come#A2#";
$MESS["CRM_CONFIG_SMS_RULES_LIST_4"] = "Collez le jeton et le SID dans le champ ci-dessous";
$MESS["CRM_CONFIG_SMS_LABEL_ACCOUNT_SID"] = "SID";
$MESS["CRM_CONFIG_SMS_LABEL_ACCOUNT_TOKEN"] = "Jeton";
$MESS["CRM_CONFIG_SMS_ACTION_REGISTRATION"] = "Inscription";
$MESS["CRM_CONFIG_SMS_REGISTRATION_INFO"] = "Vos informations d'inscription à SMS.RU";
$MESS["CRM_CONFIG_SMS_LABEL_ACCOUNT_FRIENDLY_NAME"] = "Nom du compte";
$MESS["CRM_CONFIG_SMS_CLEAR_OPTIONS"] = "Réinitialiser la configuration";
$MESS["CRM_CONFIG_SMS_CLEAR_CONFIRMATION"] = "Voulez-vous vraiment réinitialiser la configuration ?";
$MESS["CRM_CONFIG_SMS_CABINET_TITLE"] = "Finalisez la configuration de SMS.RU et commencez à utiliser la messagerie SMS";
$MESS["CRM_CONFIG_SMS_DEMO_IS_DISABLED"] = "La phase de test a été finalisée. Vous pouvez à présent profiter d'un service de message SMS entièrement fonctionnel. #A1#Ouvrez votre compte sms.ru#A2#";
$MESS["CRM_CONFIG_SMS_REGISTRATION_ERROR"] = "Les champs requis sont incorrects";
$MESS["CRM_CONFIG_SMS_RING_1"] = "Connectivité facile";
$MESS["CRM_CONFIG_SMS_RING_2"] = "Livraison instantanée";
$MESS["CRM_CONFIG_SMS_RING_3"] = "Payez uniquement pour les messages remis";
?>