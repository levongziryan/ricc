<?
$MESS["CRM_CONFIG_SMS_TITLE"] = "SMS інформування & ndash; це просто";
$MESS["CRM_CONFIG_SMS_TITLE_2"] = "Сервіс наданий компанією TWILIO.COM";
$MESS["CRM_CONFIG_SMS_FEATURES_TITLE"] = "Використовуйте всі можливості мобільного SMS маркетингу";
$MESS["CRM_CONFIG_SMS_FEATURES_LIST_DESCRIPTION"] = "Відправлення SMS легко налаштувати і використовувати в CRM Бітрікс24.";
$MESS["CRM_CONFIG_SMS_FEATURES_LIST_1"] = "Надсилайте повідомлення прямо з картки угоди, Ліда, клієнта, рахунки або пропозиції.";
$MESS["CRM_CONFIG_SMS_FEATURES_LIST_2"] = "Налаштуйте робота, який буде автоматично відправляти sms клієнту або менеджеру в потрібний час або після виконання конкретної дії.";
$MESS["CRM_CONFIG_SMS_FEATURES_LIST_3"] = "Робіть групове розсилання sms зі списку клієнтів, угод, Лідов та інших документів в CRM.";
$MESS["CRM_CONFIG_SMS_RULES_LIST_TITLE"] = "Налаштуйте сервіс twilio.com і почніть повноцінне використання sms повідомлень в Бітрікс24";
$MESS["CRM_CONFIG_SMS_RULES_LIST_1"] = "Авторизуйтесь або створіть новий акаунт в своєму #A1#twilio.com#A2#";
$MESS["CRM_CONFIG_SMS_RULES_LIST_2"] = "Зареєструйте та налаштуйте номер, який буде вказано при відправленні sms";
$MESS["CRM_CONFIG_SMS_RULES_LIST_3"] = "Скопіюйте Token та SID з головної сторінки вашого аккаунта #A1#twilio.com#A2#";
$MESS["CRM_CONFIG_SMS_RULES_LIST_4"] = "Зазначте Token та SID в поле нижче";
$MESS["CRM_CONFIG_SMS_LABEL_ACCOUNT_SID"] = "SID";
$MESS["CRM_CONFIG_SMS_LABEL_ACCOUNT_TOKEN"] = "Token";
$MESS["CRM_CONFIG_SMS_ACTION_REGISTRATION"] = "Підключити";
$MESS["CRM_CONFIG_SMS_REGISTRATION_INFO"] = "Ваші реєстраційні дані в twilio.com";
$MESS["CRM_CONFIG_SMS_LABEL_ACCOUNT_FRIENDLY_NAME"] = "Назва аккаунта";
$MESS["CRM_CONFIG_SMS_CLEAR_OPTIONS"] = "Скинути налаштування";
$MESS["CRM_CONFIG_SMS_CLEAR_CONFIRMATION"] = "Ви дійсно бажаєте скинути налаштування?";
$MESS["CRM_CONFIG_SMS_CABINET_TITLE"] = "Підключення сервісу Twilio.com завершено!";
$MESS["CRM_CONFIG_SMS_DEMO_IS_DISABLED"] = "Перейти в особистий кабінет #A1#twilio.com#A2#.";
$MESS["CRM_CONFIG_SMS_REGISTRATION_ERROR"] = "Некоректно заповнені обов'язкові поля форми";
$MESS["CRM_CONFIG_SMS_RING_1"] = "Просте підключення";
$MESS["CRM_CONFIG_SMS_RING_2"] = "Миттєва доставка";
$MESS["CRM_CONFIG_SMS_RING_3"] = "Оплата тільки за доставлені";
?>