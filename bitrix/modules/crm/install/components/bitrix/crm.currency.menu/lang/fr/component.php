<?
$MESS["CRM_CURRENCY_DELETE_DLG_MESSAGE"] = "tes-vous sûr de supprimer cette devise?";
$MESS["CRM_CURRENCY_ADD"] = "Ajouter la devise";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_CURRENCY_SHOW_TITLE"] = "Aller voir les devises";
$MESS["CRM_CURRENCY_EDIT_TITLE"] = "Passage à l'édition des devises";
$MESS["CRM_CURRENCY_ADD_TITLE"] = "Créer la nouvelle devise";
$MESS["CRM_CURRENCY_LIST_TITLE"] = "Aller à la liste des devises";
$MESS["CRM_CURRENCY_SHOW"] = "Affichage";
$MESS["CRM_CURRENCY_EDIT"] = "Editer";
$MESS["CRM_CURRENCY_LIST"] = "Liste des devises";
$MESS["CRM_CURRENCY_DELETE_TITLE"] = "Supprimer des devises";
$MESS["CRM_CURRENCY_DELETE_DLG_TITLE"] = "Suppression des devises";
$MESS["CRM_CURRENCY_DELETE"] = "Supprimer des devises";
$MESS["CRM_CURRENCY_DELETE_DLG_BTNTITLE"] = "Supprimer des devises";
?>