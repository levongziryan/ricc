<?
$MESS["CRM_CONFIG_PLG_TITLE"] = "Plug-ins CMS";
$MESS["CRM_CONFIG_PLG_DESC1"] = "Exportez dans Bitrix24 la base de données des clients de votre boutique en ligne et profitez de nombreux outils très utiles pour gérer vos clients.";
$MESS["CRM_CONFIG_PLG_DESC2"] = "Utilisez Bitrix24 pour augmenter votre taux de conversion et gérer vos clients !";
$MESS["CRM_CONFIG_PLG_DESC3"] = "Ressuscitez les commandes abandonnées en utilisant les outils d'automatisation de Bitrix24.";
$MESS["CRM_CONFIG_PLG_DESC4"] = "Un client est venu sur votre site mais n'a pas passé de commande ? Bitrix24 va capturer cet événement, créer une piste et appeler automatiquement le client par téléphone.";
$MESS["CRM_CONFIG_PLG_SELECT_CMS"] = "Sélectionnez la plateforme de votre boutique en ligne pour vous connecter à Bitrix24";
$MESS["CRM_CONFIG_PLG_SOON"] = "à venir !";
?>