<?
$MESS["CRM_WEBFORM_LIST_DESC1"] = "Utiliser les formulaires CRM pour améliorer la productivité. Toutes les données des formulaires seront enregistrées dans le système CRM. Les responsables n'auront à traiter que les informations obtenues dans le système CRM.";
$MESS["CRM_WEBFORM_LIST_DESC2"] = "Utilisez les formulaires du chat, sur les pages de votre site ou envoyez un lien vers une page publique pour que vos clients puissent remplir le formulaire quand ils veulent.";
$MESS["CRM_WEBFORM_LIST_ADD_CAPTION"] = "Ajouter un nouveau formulaire";
$MESS["CRM_WEBFORM_LIST_ITEM_FILL_START"] = "tentatives totales";
$MESS["CRM_WEBFORM_LIST_ITEM_FILL_END"] = "bien rempli";
$MESS["CRM_WEBFORM_LIST_ITEM_DATE_CREATE"] = "Date de création";
$MESS["CRM_WEBFORM_LIST_ITEM_PUBLIC_LINK"] = "Lien public";
$MESS["CRM_WEBFORM_LIST_ITEM_PUBLIC_LINK_COPY"] = "Copier dans le presse-papiers";
$MESS["CRM_WEBFORM_LIST_ITEM_START_EDIT_BUT_STOPPED"] = "Non terminé";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_ON"] = "activé";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_OFF"] = "désactivé";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_ON_STATUS"] = "Le formulaire est actif";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_OFF_STATUS"] = "Le formulaire est inactif";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_ON_NOW"] = "activé";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_OFF_NOW"] = "désactivé à l'instant";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_ACTIVATED"] = "activé ";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_DEACTIVATED"] = "désactivé";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_ACT_ON"] = "activé";
$MESS["CRM_WEBFORM_LIST_ERROR_ACTION"] = "L'action a été annulée à cause d'une erreur.";
$MESS["CRM_WEBFORM_LIST_DELETE_CONFIRM"] = "Voulez-vous supprimer ce formulaire ?";
$MESS["CRM_WEBFORM_LIST_CLOSE"] = "Fermer";
$MESS["CRM_WEBFORM_LIST_APPLY"] = "Exécuter";
$MESS["CRM_WEBFORM_LIST_CANCEL"] = "Annuler";
$MESS["CRM_WEBFORM_LIST_POPUP_LIMITED_TITLE"] = "Formulaires CRM étendus";
$MESS["CRM_WEBFORM_LIST_POPUP_LIMITED_TEXT"] = "Votre offre actuelle limite le nombre de formulaire CRM que vous pouvez avoir. Pour pouvoir ajouter plus de formulaire, veuillez changer d'offre. 
<br><br>
ASTUCE: Bitrix24 Professional propose un nombre illimité de formulaires CRM.
<br><br>
Essayez Bitrix24 Professional gratuitement pendant 30 jours.";
$MESS["CRM_WEBFORM_LIST_ADD_DESC1"] = "Cliquez ici pour créer un formulaire.";
$MESS["CRM_WEBFORM_LIST_ADD_DESC"] = "Cliquez sur la boîte pour créer un nouveau formulaire";
$MESS["CRM_WEBFORM_LIST_INFO_TITLE"] = "Utiliser des formulaires CRM pour augmenter la productivité.";
$MESS["CRM_WEBFORM_LIST_HIDE_DESC"] = "Masquer la description";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_BTN_ON"] = "activer";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_BTN_OFF"] = "désactiver";
$MESS["CRM_WEBFORM_LIST_ITEM_BTN_GET_SCRIPT"] = "Code embarqué";
$MESS["CRM_WEBFORM_LIST_PLUGIN_TITLE"] = "Plug-ins CRM";
$MESS["CRM_WEBFORM_LIST_PLUGIN_BTN_MORE"] = "plus";
$MESS["CRM_WEBFORM_LIST_PLUGIN_DESC"] = "Installez le widget sur votre site et communiquez avec vos clients dans votre Bitrix24. Vous obtiendrez une boîte à outils de communication prête à l’emploi: Chat live, rappels et formulaires CRM. Installer le widget est très facile: il vous suffit de sélectionner la plateforme de votre site et nous vous indiquerons la marche à suivre.";
$MESS["CRM_WEBFORM_LIST_FILTER_SHOW"] = "Afficher les formulaires";
$MESS["CRM_WEBFORM_LIST_SUCCESS_ACTION"] = "Action réussie.";
$MESS["CRM_WEBFORM_LIST_ADS_FORM_STATUS_ON_FACEBOOK"] = "Lié à Facebook";
$MESS["CRM_WEBFORM_LIST_ADS_FORM_DESC_HINT"] = "Ce formulaire est lié à Facebook.<br>Des formulaires de publicités Facebook sont maintenant envoyés au CRM.<br>Vous ne pouvez pas modifier le formulaire tant qu'il est lié à Facebook.";
$MESS["CRM_WEBFORM_LIST_SUCCESS_ACTION_CACHE_CLEANED"] = "Consentement appliqué ; formulaire mis à jour.";
?>