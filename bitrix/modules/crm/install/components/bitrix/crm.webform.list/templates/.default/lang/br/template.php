<?
$MESS["CRM_WEBFORM_LIST_DESC1"] = "Usar formulários de CRM para aumentar a produtividade. Todos os dados do formulário serão salvos no sistema de CRM; os gerenciadores terão apenas que processar as informações obtidas dentro do sistema de CRM.";
$MESS["CRM_WEBFORM_LIST_DESC2"] = "Utilize formulários no bate-papo, nas páginas do seu site ou envie um link para uma página pública para que seus clientes possam preencher o formulário quando escolherem.";
$MESS["CRM_WEBFORM_LIST_ADD_CAPTION"] = "Adicionar novo formulário";
$MESS["CRM_WEBFORM_LIST_ITEM_FILL_START"] = "Total de tentativas";
$MESS["CRM_WEBFORM_LIST_ITEM_FILL_END"] = "preenchidos com sucesso";
$MESS["CRM_WEBFORM_LIST_ITEM_DATE_CREATE"] = "Criado em";
$MESS["CRM_WEBFORM_LIST_ITEM_PUBLIC_LINK"] = "Link público";
$MESS["CRM_WEBFORM_LIST_ITEM_PUBLIC_LINK_COPY"] = "Copiar para Transferência";
$MESS["CRM_WEBFORM_LIST_ITEM_START_EDIT_BUT_STOPPED"] = "Não terminou";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_ON"] = "ligado";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_OFF"] = "desligado";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_ON_STATUS"] = "O formulário está ativo";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_OFF_STATUS"] = "O formulário está inativo";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_ON_NOW"] = "ativado";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_OFF_NOW"] = "desativado agora";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_ACTIVATED"] = "ativado ";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_DEACTIVATED"] = "desativado";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_ACT_ON"] = "ligado";
$MESS["CRM_WEBFORM_LIST_ERROR_ACTION"] = "A ação foi cancelada porque ocorreu um erro.";
$MESS["CRM_WEBFORM_LIST_DELETE_CONFIRM"] = "Você deseja excluir este formulário?";
$MESS["CRM_WEBFORM_LIST_CLOSE"] = "Fechar";
$MESS["CRM_WEBFORM_LIST_APPLY"] = "Executar";
$MESS["CRM_WEBFORM_LIST_CANCEL"] = "Cancelar";
$MESS["CRM_WEBFORM_LIST_POPUP_LIMITED_TITLE"] = "Formulários de CRM estendidos";
$MESS["CRM_WEBFORM_LIST_POPUP_LIMITED_TEXT"] = "Seu plano atual limita o número de formulários de CRM que você pode ter. Para adicionar novos formulários, faça um upgrade do seu plano. 
<br><br>
DICA: O Bitrix24 Professional vem com formulários de CRM ilimitados.
<br><br>
Experimente o Bitrix24 Professional gratuitamente por 30 dias.";
$MESS["CRM_WEBFORM_LIST_ADD_DESC1"] = "Clique aqui para criar um formulário";
$MESS["CRM_WEBFORM_LIST_ADD_DESC"] = "Clique na caixa para criar um novo formulário";
$MESS["CRM_WEBFORM_LIST_INFO_TITLE"] = "Usar formulários de CRM para aumentar a produtividade.";
$MESS["CRM_WEBFORM_LIST_HIDE_DESC"] = "Ocultar descrição";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_BTN_ON"] = "ativar";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_BTN_OFF"] = "desativar";
$MESS["CRM_WEBFORM_LIST_ITEM_BTN_GET_SCRIPT"] = "Código de inserção";
$MESS["CRM_WEBFORM_LIST_PLUGIN_TITLE"] = "Plug-ins do CMS";
$MESS["CRM_WEBFORM_LIST_PLUGIN_BTN_MORE"] = "mais";
$MESS["CRM_WEBFORM_LIST_PLUGIN_DESC"] = "Instale o widget no seu site e comunique-se com seus clientes dentro do seu Bitrix24. Você terá uma caixa de ferramentas de comunicação pronta para utilizar: Bate-papo ao vivo, chamadas de retorno e formulários de CRM. Instalar o widget é extremamente simples: basta selecionar a plataforma do seu site e diremos o que você deve fazer em seguida.";
$MESS["CRM_WEBFORM_LIST_FILTER_SHOW"] = "Exibir formulários";
$MESS["CRM_WEBFORM_LIST_SUCCESS_ACTION"] = "Ação concluída com sucesso.";
$MESS["CRM_WEBFORM_LIST_ADS_FORM_STATUS_ON_FACEBOOK"] = "Vinculado ao Facebook";
$MESS["CRM_WEBFORM_LIST_ADS_FORM_DESC_HINT"] = "O formulário está vinculado ao Facebook.<br>Formulários de anúncio do Facebook são agora enviados ao CRM.<br>Você não pode editar o formulário enquanto estiver vinculado ao Facebook.";
$MESS["CRM_WEBFORM_LIST_SUCCESS_ACTION_CACHE_CLEANED"] = "Consentimento aplicado; formulário atualizado.";
?>