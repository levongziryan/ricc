<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_BUTTON_EDIT_TITLE_EDIT"] = "Editar icono";
$MESS["CRM_BUTTON_EDIT_TITLE_ADD"] = "Crear icono";
$MESS["CRM_BUTTON_EDIT_UNIT_SECOND"] = "s";
$MESS["CRM_BUTTON_EDIT_UNIT_MINUTE"] = "min";
$MESS["CRM_BUTTON_EDIT_ERROR_FILE"] = "No se pudo generar el código widget. Por favor, guarde el formulario de nuevo.";
?>