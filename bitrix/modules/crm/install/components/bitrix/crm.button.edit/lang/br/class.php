<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_BUTTON_EDIT_TITLE_EDIT"] = "Editar widget";
$MESS["CRM_BUTTON_EDIT_TITLE_ADD"] = "Criar widget";
$MESS["CRM_BUTTON_EDIT_UNIT_SECOND"] = "seg";
$MESS["CRM_BUTTON_EDIT_UNIT_MINUTE"] = "min";
$MESS["CRM_BUTTON_EDIT_ERROR_FILE"] = "Não foi possível gerar o código do widget. Salve o formulário novamente.";
?>