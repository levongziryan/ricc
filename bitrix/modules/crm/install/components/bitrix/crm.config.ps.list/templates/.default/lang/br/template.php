<?
$MESS["CRM_PS_EDIT_TITLE"] = "Abrir sistema de pagamento para edição";
$MESS["CRM_PS_EDIT"] = "Editar";
$MESS["CRM_PS_DELETE_TITLE"] = "Deletar sistema de pagamento";
$MESS["CRM_PS_DELETE"] = "Deletar";
$MESS["CRM_PS_DELETE_CONFIRM"] = "Você tem certeza que deseja deletar as  '%s'?";
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_PS_LIST_TITLE"] = "Sistemas de Pagamento";
$MESS["CRM_PS_LIST_PS_CREATE"] = "Criar sistema de pagamento";
$MESS["CRM_PS_LIST_PS_CREATE_DESC"] = "Clique na caixa para criar um novo sistema de pagamento";
$MESS["CRM_PS_LIST_PS_ACTIVE_ON"] = "O sistema de pagamento está ativo";
$MESS["CRM_PS_LIST_PS_ACTIVE_OFF"] = "O sistema de pagamento está inativo";
$MESS["CRM_PS_LIST_PS_ACTIVE_BTN_ON"] = "ativo";
$MESS["CRM_PS_LIST_PS_ACTIVE_BTN_OFF"] = "inativo";
$MESS["CRM_PS_DELETE_CONFIRM_TITLE"] = "Tem certeza de que deseja excluir este sistema de pagamento?";
?>