<?
$MESS["CRM_PS_EDIT_TITLE"] = "Sistema de pago abierto para su edición";
$MESS["CRM_PS_EDIT"] = "Editar";
$MESS["CRM_PS_DELETE_TITLE"] = "Eliminar el sistema de pago";
$MESS["CRM_PS_DELETE"] = "Eliminar";
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_PS_LIST_TITLE"] = "Sistemas de pago";
$MESS["CRM_PS_LIST_PS_CREATE"] = "Crear sistema de pago";
$MESS["CRM_PS_LIST_PS_CREATE_DESC"] = "Haga clic en la casilla para crear un nuevo sistema de pago";
$MESS["CRM_PS_LIST_PS_ACTIVE_ON"] = "Sistema de pago activado";
$MESS["CRM_PS_LIST_PS_ACTIVE_OFF"] = "Sistema de pago desactivado";
$MESS["CRM_PS_LIST_PS_ACTIVE_BTN_ON"] = "on";
$MESS["CRM_PS_LIST_PS_ACTIVE_BTN_OFF"] = "off";
$MESS["CRM_PS_DELETE_CONFIRM_TITLE"] = "¿Usted está seguro que quiere eliminar este sistema de pago?";
?>