<?
$MESS["CRM_COLUMN_MAIL_TEMPLATE_ID"] = "ID";
$MESS["CRM_COLUMN_MAIL_TEMPLATE_IS_ACTIVE"] = "Actif(ve)";
$MESS["CRM_COLUMN_MAIL_TEMPLATE_OWNER"] = "Organisateur";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_COLUMN_MAIL_TEMPLATE_SCOPE"] = "Accessible";
$MESS["CRM_COLUMN_MAIL_TEMPLATE_LAST_UPDATED"] = "Date de modification";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_COLUMN_MAIL_TEMPLATE_TITLE"] = "Dénomination";
$MESS["CRM_MAIL_TEMPLATE_COMMON_TEMPLATE_NAME"] = "Modèle commun";
$MESS["CRM_MAIL_TEMPLATE_UPDATE_GENERAL_ERROR"] = "Une erreur est survenue lors de la mise à jour du modèle postal.";
$MESS["CRM_MAIL_TEMPLATE_DELETION_GENERAL_ERROR"] = "Une erreur est survenue pendant l'élimination du modèle postal '#TITLE#'.";
$MESS["CRM_COLUMN_MAIL_TEMPLATE_CREATED"] = "Créé le";
$MESS["CRM_COLUMN_MAIL_TEMPLATE_SORT"] = "Trier";
$MESS["CRM_COLUMN_MAIL_TEMPLATE_ENTITY_TYPE"] = "Bloc Highload";
?>