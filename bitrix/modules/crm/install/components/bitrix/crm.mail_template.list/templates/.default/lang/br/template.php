<?
$MESS["CRM_MAIL_TEMPLATE_DELETE_CONFIRM"] = "Tem certeza de que deseja excluir \"%\"s?";
$MESS["CRM_MAIL_TEMPLATE_DELETE"] = "Excluir Modelo de E-mail";
$MESS["CRM_MAIL_TEMPLATE_DELETE_TITLE"] = "Excluir este Modelo de E-mail";
$MESS["CRM_MAIL_TEMPLATE_EDIT"] = "Editar modelo de Email";
$MESS["CRM_MAIL_TEMPLATE_EDIT_TITLE"] = "Editar este modelo de Email";
$MESS["CRM_MAIL_TEMPLATE_NEED_FOR_CONVERTING"] = "Existem modelo de E-mail legados no sistema.Você pode <a href=\"#URL_EXECUTE_CONVERTING#\">adiciona-los a sua lista</a> ou <a href=\"#URL_SKIP_CONVERTING#\">pular</a>.";
$MESS["CRM_ALL"] = "total";
?>