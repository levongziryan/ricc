<?
$MESS["CRM_MAIL_TEMPLATE_EDIT_TITLE"] = "Editar esta plantilla de correo electrónico";
$MESS["CRM_MAIL_TEMPLATE_EDIT"] = "Editar plantilla de correo electrónico";
$MESS["CRM_MAIL_TEMPLATE_DELETE_TITLE"] = "Eliminar esta plantilla de correo electrónico";
$MESS["CRM_MAIL_TEMPLATE_DELETE"] = "Eliminar plantilla de correo electrónico";
$MESS["CRM_MAIL_TEMPLATE_DELETE_CONFIRM"] = "¿Está seguro de que desea eliminar \"% s\"?";
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_MAIL_TEMPLATE_NEED_FOR_CONVERTING"] = "Hay plantillas de correo electrónico antiguos en el sistema. Usted puede <a href=\"#URL_EXECUTE_CONVERTING#\">add them to your list</a> or <a href=\"#URL_SKIP_CONVERTING#\">skip</a>.
";
?>