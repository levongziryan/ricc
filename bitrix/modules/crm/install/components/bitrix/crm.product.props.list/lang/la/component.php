<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "El módulo e-Store no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_INTS_TASKS_NAV"] = "Registros";
$MESS["CRM_PRODUCT_PROP_PL_ID"] = "ID";
$MESS["CRM_PRODUCT_PROP_PL_NAME"] = "Nombre";
$MESS["CRM_PRODUCT_PROP_PL_CODE"] = "Código simbólico";
$MESS["CRM_PRODUCT_PROP_PL_SORT"] = "Clasificar";
$MESS["CRM_PRODUCT_PROP_PL_ACTIVE"] = "Activo";
$MESS["CRM_PRODUCT_PROP_PL_PROPERTY_TYPE"] = "Tipo";
$MESS["CRM_PRODUCT_PROP_PL_MULTIPLE"] = "Múltiple";
$MESS["CRM_PRODUCT_PROP_PL_XML_ID"] = "ID externo";
$MESS["CRM_PRODUCT_PROP_PL_WITH_DESCRIPTION"] = "Tiene descripción";
$MESS["CRM_PRODUCT_PROP_PL_SEARCHABLE"] = "Búsqueda";
$MESS["CRM_PRODUCT_PROP_PL_FILTRABLE"] = "Mostrar en el filtro";
$MESS["CRM_PRODUCT_PROP_PL_IS_REQUIRED"] = "Requerido";
$MESS["CRM_PRODUCT_PROP_PL_HINT"] = "Indirecta";
$MESS["CRM_PRODUCT_PROP_PL_DELETE_ERROR"] = "Se produjo un error al eliminar un objeto.";
$MESS["CRM_PRODUCT_PROP_PL_SAVE_ERROR"] = "Guardar el registro de error ##ID#: #ERROR_TEXT#";
$MESS["CRM_PRODUCT_PROP_IBLOCK_PROP_S"] = "Cadena";
$MESS["CRM_PRODUCT_PROP_IBLOCK_PROP_N"] = "Número";
$MESS["CRM_PRODUCT_PROP_IBLOCK_PROP_L"] = "Lista";
$MESS["CRM_PRODUCT_PROP_IBLOCK_PROP_F"] = "Archivo";
$MESS["CRM_PRODUCT_PROP_IBLOCK_PROP_G"] = "Se unen a las secciones";
$MESS["CRM_PRODUCT_PROP_IBLOCK_PROP_E"] = "Se unen a los elementos";
$MESS["CRM_PRODUCT_PROP_IBLOCK_ALL"] = "(cualquier)";
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "El módulo Information Blocks no está instalado.";
?>