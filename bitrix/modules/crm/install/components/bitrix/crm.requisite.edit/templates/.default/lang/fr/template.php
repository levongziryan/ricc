<?
$MESS["CRM_REQUISITE_CUSTOM_SAVE_BUTTON_TITLE"] = "Enregistrer et revenir";
$MESS["CRM_TAB_1_CONTACT"] = "Données de contact";
$MESS["CRM_TAB_1_COMPANY"] = "Données de la société";
$MESS["CRM_TAB_1_TITLE_CONTACT"] = "Informations sur les données de contact";
$MESS["CRM_TAB_1_TITLE_COMPANY"] = "Informations sur les données de la société";
$MESS["CRM_REQUISITE_SHOW_TITLE_COMPANY"] = "Données de la société";
$MESS["CRM_REQUISITE_SHOW_TITLE_CONTACT"] = "Données de contact";
$MESS["CRM_REQUISITE_SHOW_NEW_TITLE_COMPANY"] = "Nouvelles données de la société";
$MESS["CRM_REQUISITE_SHOW_NEW_TITLE_CONTACT"] = "Nouvelles données de contact";
$MESS["CRM_REQUISITE_SERCH_RESULT_NOT_FOUND"] = "Votre recherche n'a donné aucun résultat.";
$MESS["CRM_REQUISITE_EDIT_DUP_CTRL_SHORT_SUMMARY_TITLE"] = "trouvé";
$MESS["CRM_REQUISITE_EDIT_DUP_CTRL_WARNING_DLG_TITLE"] = "Duplicatas suspectés";
$MESS["CRM_REQUISITE_EDIT_DUP_CTRL_WARNING_ACCEPT_BTN_TITLE"] = "Ignorer et enregistrer";
$MESS["CRM_REQUISITE_EDIT_DUP_CTRL_WARNING_CANCEL_BTN_TITLE"] = "Annuler";
$MESS["CRM_REQUISITE_EDIT_DUP_CTRL_REQUISITE_SUMMARY_TITLE"] = "par données de contact/de la société";
$MESS["CRM_REQUISITE_EDIT_DUP_CTRL_BANK_DETAIL_SUMMARY_TITLE"] = "par informations bancaires";
?>