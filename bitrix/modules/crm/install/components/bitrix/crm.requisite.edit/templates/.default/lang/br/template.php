<?
$MESS["CRM_REQUISITE_CUSTOM_SAVE_BUTTON_TITLE"] = "Salvar e Retornar";
$MESS["CRM_TAB_1_CONTACT"] = "Informações de contato";
$MESS["CRM_TAB_1_COMPANY"] = "Informações da empresa";
$MESS["CRM_TAB_1_TITLE_CONTACT"] = "Informações de detalhes de contato";
$MESS["CRM_TAB_1_TITLE_COMPANY"] = "Informações de detalhes da empresa";
$MESS["CRM_REQUISITE_SHOW_TITLE_COMPANY"] = "Informações da empresa";
$MESS["CRM_REQUISITE_SHOW_TITLE_CONTACT"] = "Informações de contato";
$MESS["CRM_REQUISITE_SHOW_NEW_TITLE_COMPANY"] = "Informações de empresa nova";
$MESS["CRM_REQUISITE_SHOW_NEW_TITLE_CONTACT"] = "Informações de contato novo";
$MESS["CRM_REQUISITE_SERCH_RESULT_NOT_FOUND"] = "Sua solicitação de pesquisa não deu resultados.";
$MESS["CRM_REQUISITE_EDIT_DUP_CTRL_SHORT_SUMMARY_TITLE"] = "encontrado";
$MESS["CRM_REQUISITE_EDIT_DUP_CTRL_WARNING_DLG_TITLE"] = "Possíveis clones";
$MESS["CRM_REQUISITE_EDIT_DUP_CTRL_WARNING_ACCEPT_BTN_TITLE"] = "Ignorar e salvar";
$MESS["CRM_REQUISITE_EDIT_DUP_CTRL_WARNING_CANCEL_BTN_TITLE"] = "Cancelar";
$MESS["CRM_REQUISITE_EDIT_DUP_CTRL_REQUISITE_SUMMARY_TITLE"] = "por informações do contato/empresa";
$MESS["CRM_REQUISITE_EDIT_DUP_CTRL_BANK_DETAIL_SUMMARY_TITLE"] = "por dados bancários";
?>