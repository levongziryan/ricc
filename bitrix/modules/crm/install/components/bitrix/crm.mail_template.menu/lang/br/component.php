<?
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_MAIL_TEMPLATE_ADD"] = "Adicionar modelo de E-mail";
$MESS["CRM_MAIL_TEMPLATE_DELETE_DLG_MESSAGE"] = "Tem certeza de que deseja excluir esse modelo de e-mail?";
$MESS["CRM_MAIL_TEMPLATE_ADD_TITLE"] = "Criar novo Modelo de E-mail";
$MESS["CRM_MAIL_TEMPLATE_DELETE"] = "Excluir Modelo de E-mail";
$MESS["CRM_MAIL_TEMPLATE_DELETE_TITLE"] = "Excluir Modelo de E-mail";
$MESS["CRM_MAIL_TEMPLATE_DELETE_DLG_TITLE"] = "Excluir Modelo de E-mail";
$MESS["CRM_MAIL_TEMPLATE_DELETE_DLG_BTNTITLE"] = "Excluir Modelo de E-mail";
$MESS["CRM_MAIL_TEMPLATE_LIST"] = "Modelos de Email";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["CRM_MAIL_TEMPLATE_LIST_TITLE"] = "Ver todos os modelos de e-mail";
?>