<?
$MESS["CRM_MAIL_TEMPLATE_DELETE_DLG_MESSAGE"] = "tes-vous sûr de vouloir supprimer ce modèle de courrier?";
$MESS["CRM_MAIL_TEMPLATE_ADD"] = "Ajouter un modèle de messagerie";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_MAIL_TEMPLATE_ADD_TITLE"] = "Passage à la création d'un nouveeau modèle du courrier";
$MESS["CRM_MAIL_TEMPLATE_LIST_TITLE"] = "Passage vers la liste de modèles courriers";
$MESS["CRM_MAIL_TEMPLATE_LIST"] = "Liste des modèles postaux";
$MESS["CRM_MAIL_TEMPLATE_DELETE_TITLE"] = "Suppression du modèle postal";
$MESS["CRM_MAIL_TEMPLATE_DELETE_DLG_TITLE"] = "Suppression du modèle postal";
$MESS["CRM_MAIL_TEMPLATE_DELETE"] = "Suppression du modèle postal";
$MESS["CRM_MAIL_TEMPLATE_DELETE_DLG_BTNTITLE"] = "Suppression du modèle postal";
?>