<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no esta instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_MAIL_TEMPLATE_LIST"] = "Plantilla de correo electrónico";
$MESS["CRM_MAIL_TEMPLATE_LIST_TITLE"] = "Ver todas las plantillas de correos electrónico";
$MESS["CRM_MAIL_TEMPLATE_ADD"] = "Agregar plantilla de correo electrónico";
$MESS["CRM_MAIL_TEMPLATE_ADD_TITLE"] = "Crear una nueva plantilla de correo electrónico";
$MESS["CRM_MAIL_TEMPLATE_DELETE"] = "Eliminar plantilla de correo electrónico";
$MESS["CRM_MAIL_TEMPLATE_DELETE_TITLE"] = "Eliminar plantilla de correo electrónico";
$MESS["CRM_MAIL_TEMPLATE_DELETE_DLG_TITLE"] = "Eliminar plantilla de correo electrónico";
$MESS["CRM_MAIL_TEMPLATE_DELETE_DLG_MESSAGE"] = "¿Está seguro de que desea eliminar esta plantilla de correo electrónico?";
$MESS["CRM_MAIL_TEMPLATE_DELETE_DLG_BTNTITLE"] = "Eliminar plantilla de correo electrónico";
?>