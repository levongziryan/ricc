<?
$MESS["CRM_FIELD_ACTIVE"] = "Actif(ve)";
$MESS["CRM_FIELD_CURRENCY"] = "Devises";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit.";
$MESS["CRM_UNSUPPORTED_OWNER_TYPE"] = "Le type du propriétaire spécifié: '#OWNER_TYPE#' n'est pas soutenu.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "Le module 'Catalogue de marchandises' n'a pas été installé.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Le module 'Boutique en ligne' n'a pas été installé.";
$MESS["CRM_FIELD_NAME"] = "Dénomination";
$MESS["CRM_FIELD_VAT_INCLUDED"] = "La TVA est incluse dans le prix";
$MESS["CRM_CURRENCY_IS_NOT_FOUND"] = "Impossible de trouver la devise avec ID = #CURRENCY_ID#.";
$MESS["CRM_FIELD_DESCRIPTION"] = "Description";
$MESS["CRM_BUTTON_CANCEL_TITLE"] = "Annuler";
$MESS["CRM_PRODUCT_CREATE_AJAX_ERR"] = "Erreur de traitement de la demande pour la création de la marchandise<br>";
$MESS["CRM_FIELD_SECTION"] = "Créer une section";
$MESS["CRM_PRODUCT_CREATE"] = "Création d'une nouvelle marchandise";
$MESS["CRM_PRODUCT_CREATE_WAIT"] = "Création d'une marchandise...";
$MESS["CRM_BUTTON_CREATE_TITLE"] = "Ajouter";
$MESS["CRM_FIELD_SORT"] = "Trier";
$MESS["CRM_FIELD_VAT_ID"] = "Taux de TVA";
$MESS["CRM_FIELD_PRICE"] = "Prix";
?>