<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_IBLOCK_MODULE_NOT_INSTALLED"] = "Le module des blocs d'informations n'a pas été installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CT_BMTS_WINDOW_CLOSE"] = "Fermer";
$MESS["CT_BMTS_CANCEL"] = "Annuler";
$MESS["CT_BMTS_WAIT"] = "En cours de chargement...";
$MESS["CT_BMTS_SUBMIT"] = "Sélectionner";
?>