<?
$MESS["CRM_PRODUCT_PROP_START_TEXT"] = "(saisirle nom)";
$MESS["CRM_PRODUCT_PROP_NO_VALUE"] = "(non installé)";
$MESS["CRM_PRODUCT_PROP_ADD_BUTTON"] = "Ajouter";
$MESS["CRM_PRODUCT_PROP_NO_SEARCH_RESULT_TEXT"] = "rien n'a été trouvé";
$MESS["CRM_TAB_1_TITLE"] = "Caractéristiques de marchandises";
$MESS["CRM_TAB_1"] = "Article";
$MESS["CRM_PRODUCT_PROP_DOWNLOAD"] = "Télécharger";
$MESS["CRM_PRODUCT_PROP_ENLARGE"] = "Faire agrandir";
$MESS["CRM_PRODUCT_PROP_CHOOSE_ELEMENT"] = "Sélectionner dans la liste";
?>