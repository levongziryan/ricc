<?
$MESS["CRM_CATALOG_ID"] = "Catálogo Comercial";
$MESS["CRM_PRODUCT_ID"] = "ID do produto";
$MESS["CRM_PRODUCT_ID_PARAM"] = "Nome variável do ID do produto";
$MESS["CRM_PATH_TO_PRODUCT_LIST"] = "Modelo de Caminho da Página de Produtos";
$MESS["CRM_PATH_TO_PRODUCT_EDIT"] = "Modelo de Caminho da Página para Editar Produto";
$MESS["CRM_PATH_TO_PRODUCT_SHOW"] = "Modelo de Caminho da Página de Visualização do Produto";
$MESS["CRM_CATALOG_NOT_SELECTED"] = "[não selecionado]";
?>