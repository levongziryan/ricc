<?
$MESS["CRM_WGT_SLST_LRP_DLG_BTN_START"] = "Executar";
$MESS["CRM_WGT_SLST_LRP_DLG_BTN_STOP"] = "Parar";
$MESS["CRM_WGT_SLST_LRP_DLG_BTN_CLOSE"] = "Fechar";
$MESS["CRM_WGT_SLST_DLG_WAIT"] = "Por favor, aguarde...";
$MESS["CRM_WGT_SLST_LRP_DLG_REQUEST_ERR"] = "Erro ao processar a solicitação.";
$MESS["CRM_WGT_SLST_GENERAL_INTRO"] = "Selecione nesta página seus campos personalizados para incluir nos relatórios analíticos de CRM.";
$MESS["CRM_WGT_SLST_LIMITS_TOTALS"] = "Selecionados: #TOTAL# de #OVERALL#";
$MESS["CRM_WGT_SLST_NOT_SELECTED"] = "Não selecionado";
$MESS["CRM_WGT_SLST_BY_DEFALT"] = "Padrão";
$MESS["CRM_WGT_SLST_TOTAL_SUM"] = "Soma total";
$MESS["CRM_WGT_SLST_USER_FIELDS"] = "Campos personalizados";
$MESS["CRM_WGT_SLST_ADD_PRODUCT_SUM"] = "Adicionar quantidade de produto";
$MESS["CRM_WGT_SLST_ADD"] = "Adicionar";
$MESS["CRM_WGT_SLST_EDIT"] = "Editar";
$MESS["CRM_WGT_SLST_REMOVE"] = "Excluir";
$MESS["CRM_WGT_SLST_SAVE"] = "Salvar";
$MESS["CRM_WGT_SLST_CALCEL"] = "Cancelar";
$MESS["CRM_WGT_SLST_RESET"] = "Reiniciar";
$MESS["CRM_WGT_SLST_ERR_SELECT_FIELD"] = "Selecione um campo para processar.";
$MESS["CRM_WGT_SLST_ERR_FIELD_ALREADY_EXISTS"] = "Selecione outro campo. O campo \"#FIELD#\" já está em uso.";
$MESS["CRM_WGT_SLST_ERR_FIELD_LIMT_EXCEEDED"] = "Não é possível adicionar um novo campo porque o máximo possível de campos está adicionado.";
$MESS["CRM_WGT_SLST_ERR_NO_FREE_SLOTS"] = "Não é possível adicionar um novo campo porque não há slots livres.";
$MESS["CRM_WGT_SLST_NODE_TOOLTIP"] = "Se você usar um campo personalizado para o valor total da venda, especificar aqui. O total de produtos da venda pode ser adicionado a este campo. Por padrão, os relatórios são criados usando o campo \"Total\" da venda.";
$MESS["CRM_WGT_SLST_LIMITS_INTRO"] = "Até dez campos podem ser selecionados; até cinco do mesmo tipo.";
?>