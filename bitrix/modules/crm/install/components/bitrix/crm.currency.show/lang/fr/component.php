<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_CURRENCY_NOT_FOUND"] = "La devise  n'a pas été trouvée.";
$MESS["CRM_CURRENCY_SECTION_MAIN"] = "Devises";
$MESS["CRM_CURRENCY_FIELD_ID"] = "Devises";
$MESS["CRM_CURRENCY_FIELD_DEFAULT_EXCH_RATE"] = "Taux de change (par défaut)";
$MESS["CRM_CURRENCY_FIELD_SORT"] = "Classification";
$MESS["CRM_CURRENCY_FULL_NAME"] = "Dénomination";
$MESS["CRM_CURRENCY_FORMAT_STRING"] = "Format d'affichage de devise";
$MESS["CRM_CURRENCY_DEC_POINT"] = "Point décimal à la sortie";
$MESS["CRM_CURRENCY_THOUSANDS_VARIANT"] = "Séparateur des milliers lors de l'affichage";
$MESS["CRM_CURRENCY_THOUSANDS_VARIANT_N"] = "Absent";
$MESS["CRM_CURRENCY_THOUSANDS_VARIANT_D"] = "Délais";
$MESS["CRM_CURRENCY_THOUSANDS_VARIANT_C"] = "Virgule";
$MESS["CRM_CURRENCY_THOUSANDS_VARIANT_S"] = "Blanc";
$MESS["CRM_CURRENCY_THOUSANDS_VARIANT_B"] = "Espace insécable";
$MESS["CRM_CURRENCY_THOUSANDS_VARIANT_ANOTHER"] = "Autre";
$MESS["CRM_CURRENCY_THOUSANDS_SEP"] = "Propre séparateur des milliers à l'affichage";
$MESS["CRM_CURRENCY_INVOICES_DEFAULT"] = "Devise de facturation par défaut";
$MESS["CRM_CURRENCY_FIELD_AMOUNT_CNT"] = "Valeur nominale";
$MESS["CRM_CURRENCY_SHOW_BASE"] = "Devise de base";
?>