<?
$MESS["CRM_CONFIG_PERMS_REBUILD_COMPANY_ATTRS"] = "Por favor <a id=\"#ID#\" href=\"#URL#\">update</a> condiciones de acceso para las compañías existentes antes de utilizar permisos de acceso extendidos.";
$MESS["CRM_CONFIG_PERMS_LRP_DLG_BTN_START"] = "Iniciar";
$MESS["CRM_CONFIG_PERMS_LRP_DLG_BTN_STOP"] = "Stop";
$MESS["CRM_CONFIG_PERMS_LRP_DLG_BTN_CLOSE"] = "Cerrar";
$MESS["CRM_CONFIG_PERMS_LRP_DLG_WAIT"] = "Por favor, espere...";
$MESS["CRM_CONFIG_PERMS_LRP_DLG_REQUEST_ERR"] = "Error al procesar la solicitud.";
$MESS["CRM_CONFIG_PERMS_REBUILD_COMPANY_ATTR_DLG_TITLE"] = "Actualizar condiciones de acceso de la compañía";
$MESS["CRM_CONFIG_PERMS_REBUILD_COMPANY_ATTR_DLG_SUMMARY"] = "Condiciones de acceso de la compañía se volvera a crear. Esta operación puede tardar algún tiempo.";
$MESS["CRM_CONFIG_PERMS_REBUILD_CONTACT_ATTRS"] = "Las actualizaciones de permisos de acceso de los contactos requieren que usted <a id=\"#ID#\" href=\"#URL#\">actualice</a>atributos de acceso creado previamente para contactos.";
$MESS["CRM_CONFIG_PERMS_REBUILD_CONTACT_ATTR_DLG_TITLE"] = "Actualizar Atributos de Acceso del Contacto";
$MESS["CRM_CONFIG_PERMS_REBUILD_CONTACT_ATTR_DLG_SUMMARY"] = "Atributos de acceso será reconstruido para todos los contactos. Este procedimiento puede tardar algún tiempo.";
$MESS["CRM_CONFIG_PERMS_REBUILD_DEAL_ATTRS"] = "Los permisos de acceso de las negociaciones requieren que usted <a id=\"#ID#\" href=\"#URL#\">actualice</a> atributos de acceso creado previamente para las negociaciones.";
$MESS["CRM_CONFIG_PERMS_REBUILD_DEAL_ATTR_DLG_TITLE"] = "Actualizar Atributos de Acceso de la Negociación";
$MESS["CRM_CONFIG_PERMS_REBUILD_DEAL_ATTR_DLG_SUMMARY"] = "Atributos de acceso ahora serán reconstruidos para todas las negociaciones. Este procedimiento puede tardar algún tiempo.";
$MESS["CRM_CONFIG_PERMS_REBUILD_LEAD_ATTRS"] = "Las actualizaciones de permisos de acceso de los prospectos requieren que usted <a id=\"#ID#\" href=\"#URL#\">actualice</a> atributos de acceso creado previamente para los prospectos.";
$MESS["CRM_CONFIG_PERMS_REBUILD_LEAD_ATTR_DLG_TITLE"] = "Actualizar Atributos de Acceso de los Prospectos";
$MESS["CRM_CONFIG_PERMS_REBUILD_LEAD_ATTR_DLG_SUMMARY"] = "Atributos de acceso ahora serán reconstruidos para todas los prospectos. Este procedimiento puede tardar algún tiempo.";
$MESS["CRM_CONFIG_PERMS_REBUILD_QUOTE_ATTRS"] = "Las actualizaciones de permisos de acceso de las cotizaciones requieren que usted <a id=\"#ID#\" href=\"#URL#\">actualice</a> atributos de acceso creado previamente para las cotizaciones.";
$MESS["CRM_CONFIG_PERMS_REBUILD_QUOTE_ATTR_DLG_TITLE"] = "Actualizar Atributos de Acceso de las Cotizaciones";
$MESS["CRM_CONFIG_PERMS_REBUILD_QUOTE_ATTR_DLG_SUMMARY"] = "Atributos de acceso ahora serán reconstruidos para todas las cotizaciones. Este procedimiento puede tardar algún tiempo.";
$MESS["CRM_CONFIG_PERMS_REBUILD_INVOICE_ATTRS"] = "Las actualizaciones de permisos de acceso de las facturas requieren que usted <a id=\"#ID#\" href=\"#URL#\">actualice</a> atributos de acceso creado previamente para las facturas.";
$MESS["CRM_CONFIG_PERMS_REBUILD_INVOICE_ATTR_DLG_TITLE"] = "Actualizar Atributos de Acceso de las Facturas";
$MESS["CRM_CONFIG_PERMS_REBUILD_INVOICE_ATTR_DLG_SUMMARY"] = "Atributos de acceso ahora serán reconstruidos para todas las facturas. Este procedimiento puede tardar algún tiempo.";
?>