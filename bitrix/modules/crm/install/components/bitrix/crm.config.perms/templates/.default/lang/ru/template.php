<?
$MESS["CRM_PERMS_ALERT"] = "После изменения прав доступа необходимо выполнить полную переиндексацию";
$MESS["CRM_PERMS_LEAD"] = "Лиды";
$MESS["CRM_PERMS_CONTACT"] = "Контакты";
$MESS["CRM_PERMS_COMPANY"] = "Компании";
$MESS["CRM_PERMS_DEAL"] = "Сделки";
$MESS["CRM_PERMS_EVENT"] = "События";
$MESS["CRM_PERMS_CONFIG"] = "Настройки";
$MESS["CRM_PERMS_TYPE_ALL"] = "Все";
$MESS["CRM_PERMS_TYPE_SELF"] = "Свои";
$MESS["CRM_PERMS_TYPE_NONE"] = "Нет";
$MESS["CRM_PERMS_TYPE_EDIT"] = "Изменение";
$MESS["CRM_PERMS_LIST_EMPTY"] = "Права доступа не заданы";
$MESS["CRM_PERMS_ADD_LINK"] = "Добавить";
$MESS["CRM_PERMS_BUTTONS_SAVE"] = "Сохранить";
$MESS["CRM_PERMS_BUTTONS_CANCEL"] = "Отменить";
?>