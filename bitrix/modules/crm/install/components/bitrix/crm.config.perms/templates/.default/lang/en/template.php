<?
$MESS["CRM_PERMS_LEAD"] = "Leads";
$MESS["CRM_PERMS_CONTACT"] = "Contacts";
$MESS["CRM_PERMS_COMPANY"] = "Companies";
$MESS["CRM_PERMS_DEAL"] = "Deals";
$MESS["CRM_PERMS_EVENT"] = "Events";
$MESS["CRM_PERMS_CONFIG"] = "Settings";
$MESS["CRM_PERMS_TYPE_ALL"] = "All";
$MESS["CRM_PERMS_TYPE_SELF"] = "My";
$MESS["CRM_PERMS_TYPE_NONE"] = "None";
$MESS["CRM_PERMS_TYPE_EDIT"] = "Edit";
$MESS["CRM_PERMS_LIST_EMPTY"] = "No access permissions specified.";
$MESS["CRM_PERMS_ADD_LINK"] = "Add";
$MESS["CRM_PERMS_BUTTONS_SAVE"] = "Save";
$MESS["CRM_PERMS_BUTTONS_CANCEL"] = "Cancel";
?>