<?
$MESS["CRM_QUOTE_EDIT_EVENT_SUCCESSFULLY_CREATED"] = "La cotización <a href='#URL#'>#TITLE#</a> ha sido creada. Redirigir a la página anterior ahora. Si esta página aún se muestra, ciérrela manualmente.";
$MESS["CRM_QUOTE_EDIT_EVENT_CANCELED"] = "La acción ha sido cancelada. Ahora estás siendo redirigido a la página anterior. Si la página actual todavía se muestra, ciérrela manualmente.";
?>