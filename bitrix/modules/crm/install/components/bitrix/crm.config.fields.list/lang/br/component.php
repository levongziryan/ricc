<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_FIELDS_LIST_TITLE_EDIT"] = "Campos: #NAME#";
$MESS["CRM_FIELDS_LIST_ACTION_MENU_EDIT"] = "Editar";
$MESS["CRM_FIELDS_LIST_ACTION_MENU_DELETE"] = "Excluir";
$MESS["CRM_FIELDS_LIST_ACTION_MENU_DELETE_CONF"] = "Tem certeza de que deseja excluir este campo?";
$MESS["CRM_FIELDS_ENTITY_LIST"] = "Tipos";
?>