<?
$MESS["CRM_FIELDS_TOOLBAR_ADD"] = "Agregar Campo";
$MESS["CRM_FIELDS_TOOLBAR_ADD_TITLE"] = "Agregar Nuevo Campo";
$MESS["CRM_FIELDS_TOOLBAR_TYPES"] = "Tipos";
$MESS["CRM_FIELDS_TOOLBAR_TYPES_TITLE"] = "Ver tipos disponibles";
$MESS["CRM_FIELDS_LIST_SORT"] = "Clasificación";
$MESS["CRM_FIELDS_LIST_NAME"] = "Nombre";
$MESS["CRM_FIELDS_LIST_TYPE"] = "Tipo";
$MESS["CRM_FIELDS_LIST_IS_REQUIRED"] = "Requerido";
$MESS["CRM_FIELDS_LIST_MULTIPLE"] = "Múltiple";
$MESS["CRM_FIELDS_LIST_SHOW_IN_LIST"] = "Mostrar en la lista";
?>