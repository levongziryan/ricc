<?
$MESS["CRM_FIELDS_TOOLBAR_ADD"] = "Adicionar campo";
$MESS["CRM_FIELDS_TOOLBAR_ADD_TITLE"] = "Adicionar novo campo";
$MESS["CRM_FIELDS_TOOLBAR_TYPES"] = "Tipos";
$MESS["CRM_FIELDS_TOOLBAR_TYPES_TITLE"] = "Visualizar tipos disponíveis";
$MESS["CRM_FIELDS_LIST_SORT"] = "Ordenando";
$MESS["CRM_FIELDS_LIST_NAME"] = "Nome";
$MESS["CRM_FIELDS_LIST_TYPE"] = "Tipo";
$MESS["CRM_FIELDS_LIST_IS_REQUIRED"] = "Obrigatório";
$MESS["CRM_FIELDS_LIST_MULTIPLE"] = "Múltiplo";
$MESS["CRM_FIELDS_LIST_SHOW_IN_LIST"] = "Mostrar na lista";
?>