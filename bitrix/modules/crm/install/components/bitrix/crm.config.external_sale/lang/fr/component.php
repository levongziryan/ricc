<?
$MESS["CRM_EXT_SALE_DEJ_TITLE1"] = "Assistant de l'intégration";
$MESS["BPWC_NO_CRM_MODULE"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_IBLOCK"] = "Le module Blocs d'Informations n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "Le module Devise n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Le module Vente n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "Le module Catalogue n'est pas installé.";
?>