<?
$MESS["BPWC_NO_CRM_MODULE"] = "El módulo CRM no está instalado.";
$MESS["CRM_EXT_SALE_DEJ_TITLE1"] = "Asistente de integración";
$MESS["CRM_MODULE_NOT_INSTALLED_IBLOCK"] = "El módulo Information Blocks no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "El módulo Currency no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "El módulo Sale no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "El módulo Catalog no está instalado.";
?>