<?
$MESS["CRM_ENTITY_LF_ACTIVITY_CLIENT_INFO"] = "con #CLIENT#";
$MESS["CRM_ENTITY_LF_ACTIVITY_REFERENCE_INFO"] = "acerca #REFERENCE#";
$MESS["CRM_ENTITY_LF_ACTIVITY_TYPE_MEETING"] = "Reunión con un cliente";
$MESS["CRM_ENTITY_LF_ACTIVITY_TYPE_CALL_OUTGOING"] = "Llamada telefónica a un cliente";
$MESS["CRM_ENTITY_LF_ACTIVITY_TYPE_CALL_INCOMING"] = "Llamada telefónica de un cliente";
$MESS["CRM_ENTITY_LF_ACTIVITY_TYPE_EMAIL_OUTGOING"] = "Correo electrónico de un cliente";
$MESS["CRM_ENTITY_LF_ACTIVITY_TYPE_EMAIL_INCOMING"] = "Correo electrónico para un cliente";
$MESS["CRM_ENTITY_LF_ACTIVITY_TYPE_TASK"] = "Tarea";
$MESS["CRM_ENTITY_LF_ACTIVITY_LIST_TITLE"] = "Actividades actuales";
?>