<?
$MESS["CRM_ADS_LEADADS_ERROR_ACTION"] = "Se canceló la acción porque ocurrió un error.";
$MESS["CRM_ADS_LEADADS_CLOSE"] = "Cerrar";
$MESS["CRM_ADS_LEADADS_APPLY"] = "Ejecutar";
$MESS["CRM_ADS_LEADADS_CANCEL"] = "Cancelar";
$MESS["CRM_ADS_LEADADS_LOGIN"] = "Conectar";
$MESS["CRM_ADS_LEADADS_LOGOUT"] = "Descuento";
$MESS["CRM_ADS_LEADADS_REFRESH"] = "Refrescar";
$MESS["CRM_ADS_LEADADS_REFRESH_TEXT"] = "Actualice las preferencias disponibles.";
$MESS["CRM_ADS_LEADADS_CABINET_FACEBOOK"] = "Páginas de Facebook";
$MESS["CRM_ADS_LEADADS_TITLE"] = "Los anuncios de Facebook pueden mostrar los formularios que agregue en Bitrix24.";
$MESS["CRM_ADS_LEADADS_SELECT_ACCOUNT"] = "Agregar un formulario para páginas de Facebook";
$MESS["CRM_ADS_LEADADS_ERROR_NO_ACCOUNTS"] = "No se encontraron páginas de Facebook. Por favor proceda a % name% y cree una página.";
$MESS["CRM_ADS_LEADADS_BUTTON_EXPORT_FACEBOOK"] = "Enlace a Facebook";
$MESS["CRM_ADS_LEADADS_BUTTON_UNLINK_FACEBOOK"] = "Desvincular de Facebook";
$MESS["CRM_ADS_LEADADS_BUTTON_EXPORTED_SUCCESS"] = "Éxito";
$MESS["CRM_ADS_LEADADS_LINKS_ITEM_FACEBOOK"] = "para la página \"%account%\" como \"%name%\"";
$MESS["CRM_ADS_LEADADS_LINKS_TITLE"] = "El formulario \"%name%\" ya ha sido agregado";
$MESS["CRM_ADS_LEADADS_FORM_NAME"] = "Nombre de formulario de Facebook";
$MESS["CRM_ADS_LEADADS_FORM_SUCCESS_URL"] = "URL para redirigir después de la presentación del formulario";
$MESS["CRM_ADS_LEADADS_AFTER_ENABLE_FACEBOOK"] = "Una vez que el formulario está vinculado a Facebook, no se puede editar; los datos del formulario se enviarán al CRM.";
$MESS["CRM_ADS_LEADADS_AFTER_DISABLE_FACEBOOK"] = "Una vez que el formulario está desvinculado de Facebook, estará disponible para su edición; los datos del formulario no se enviarán al CRM.";
$MESS["CRM_ADS_LEADADS_NOW"] = "vinculado ahora";
$MESS["CRM_ADS_LEADADS_IS_LINKED"] = "vinculado";
$MESS["CRM_ADS_LEADADS_MORE"] = "Detalles";
$MESS["CRM_ADS_LEADADS_LIST"] = "Lista";
?>