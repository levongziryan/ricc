<?
$MESS["CRM_ADS_LEADADS_ERROR_ACTION"] = "Виникла помилка. Дія відмінена.";
$MESS["CRM_ADS_LEADADS_CLOSE"] = "Закрити";
$MESS["CRM_ADS_LEADADS_APPLY"] = "Виконати";
$MESS["CRM_ADS_LEADADS_CANCEL"] = "Скасувати";
$MESS["CRM_ADS_LEADADS_LOGIN"] = "Підключити";
$MESS["CRM_ADS_LEADADS_LOGOUT"] = "Відключити";
$MESS["CRM_ADS_LEADADS_REFRESH"] = "Оновити";
$MESS["CRM_ADS_LEADADS_REFRESH_TEXT"] = "Обновіть доступні налаштування.";
$MESS["CRM_ADS_LEADADS_CABINET_FACEBOOK"] = "Перелік сторінок в Facebook";
$MESS["CRM_ADS_LEADADS_TITLE"] = "У рекламі Facebook можна виводити форму, додану з Бітрікс24.";
$MESS["CRM_ADS_LEADADS_SELECT_ACCOUNT"] = "Додати форму для сторінки Facebook";
$MESS["CRM_ADS_LEADADS_ERROR_NO_ACCOUNTS"] = "Сторінки Facebook не знайдені, перейдіть в %name% та створіть сторінку.";
$MESS["CRM_ADS_LEADADS_BUTTON_EXPORT_FACEBOOK"] = "Зв'язати з Facebook";
$MESS["CRM_ADS_LEADADS_BUTTON_UNLINK_FACEBOOK"] = "Відв'язати від Facebook";
$MESS["CRM_ADS_LEADADS_BUTTON_EXPORTED_SUCCESS"] = "Успішно виконане";
$MESS["CRM_ADS_LEADADS_LINKS_ITEM_FACEBOOK"] = "для сторінки \"%account%\" як \"%name%\"";
$MESS["CRM_ADS_LEADADS_LINKS_TITLE"] = "Форма \"%name%\" була вже додана";
$MESS["CRM_ADS_LEADADS_FORM_NAME"] = "Назва форми для показу в Facebook";
$MESS["CRM_ADS_LEADADS_FORM_SUCCESS_URL"] = "Адреса для перенаправлення користувача після заповнення форми";
$MESS["CRM_ADS_LEADADS_AFTER_ENABLE_FACEBOOK"] = "Після зв'язку з Facebook, форма буде заблокована для редагування. А дані будуть передаватися в CRM.";
$MESS["CRM_ADS_LEADADS_AFTER_DISABLE_FACEBOOK"] = "Після відв'язування від Facebook, форма буде доступна для редагування. А дані перестануть передаватися в CRM.";
$MESS["CRM_ADS_LEADADS_NOW"] = "пов'язана зараз";
$MESS["CRM_ADS_LEADADS_IS_LINKED"] = "пов'язана";
$MESS["CRM_ADS_LEADADS_MORE"] = "Детальніше";
$MESS["CRM_ADS_LEADADS_LIST"] = "перелік";
?>