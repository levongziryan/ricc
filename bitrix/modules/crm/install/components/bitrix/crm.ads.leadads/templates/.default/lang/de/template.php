<?
$MESS["CRM_ADS_LEADADS_ERROR_ACTION"] = "Aktion wurde abgebrochen, weil ein Fehler aufgetreten ist.";
$MESS["CRM_ADS_LEADADS_CLOSE"] = "Schließen";
$MESS["CRM_ADS_LEADADS_APPLY"] = "Starten";
$MESS["CRM_ADS_LEADADS_CANCEL"] = "Abbrechen";
$MESS["CRM_ADS_LEADADS_LOGIN"] = "Verbinden";
$MESS["CRM_ADS_LEADADS_LOGOUT"] = "Verbindung trennen";
$MESS["CRM_ADS_LEADADS_REFRESH"] = "Aktualisieren";
$MESS["CRM_ADS_LEADADS_REFRESH_TEXT"] = "Aktualisieren Sie verfügbare Einstellungen.";
$MESS["CRM_ADS_LEADADS_CABINET_FACEBOOK"] = "Facebook Seiten";
$MESS["CRM_ADS_LEADADS_TITLE"] = "Facebook-Anzeigen können Formulare anzeigen, die Sie in BItrix24 hinzugefügt haben.";
$MESS["CRM_ADS_LEADADS_SELECT_ACCOUNT"] = "Fügen Sie ein Formular zu Facebook-Seiten hinzu";
$MESS["CRM_ADS_LEADADS_ERROR_NO_ACCOUNTS"] = "Es wurden keine Facebook-Seiten gefunden. Wechseln Sie bitte auf %name% und erstellen Sie eine Seite.";
$MESS["CRM_ADS_LEADADS_BUTTON_EXPORT_FACEBOOK"] = "Mit Facebook verknüpfen";
$MESS["CRM_ADS_LEADADS_BUTTON_UNLINK_FACEBOOK"] = "Verknüpfung mit Facebook trennen";
$MESS["CRM_ADS_LEADADS_BUTTON_EXPORTED_SUCCESS"] = "Erfolgreich";
$MESS["CRM_ADS_LEADADS_LINKS_ITEM_FACEBOOK"] = "für Seite \"%account%\" als \"%name%\"";
$MESS["CRM_ADS_LEADADS_LINKS_TITLE"] = "Das Formular \"%name%\" wurde bereits hinzugefügt";
$MESS["CRM_ADS_LEADADS_FORM_NAME"] = "Name des Facebook-Formulars";
$MESS["CRM_ADS_LEADADS_FORM_SUCCESS_URL"] = "URL zur Weiterleitung nach der Ausfüllung des Formulars";
$MESS["CRM_ADS_LEADADS_AFTER_ENABLE_FACEBOOK"] = "Nachdem das Formular mit Facebook verknüpft wird, kann es nicht mehr bearbeitet werden. Die Formulardaten werden ans CRM gesendet.";
$MESS["CRM_ADS_LEADADS_AFTER_DISABLE_FACEBOOK"] = "Nachdem das Formular von Facebook getrennt wird, kann es wieder bearbeitet werden. Die Formulardaten werden ans CRM nicht gesendet.";
$MESS["CRM_ADS_LEADADS_NOW"] = "jetzt verknüpft";
$MESS["CRM_ADS_LEADADS_IS_LINKED"] = "verknüpft";
$MESS["CRM_ADS_LEADADS_MORE"] = "Details";
$MESS["CRM_ADS_LEADADS_LIST"] = "Liste";
?>