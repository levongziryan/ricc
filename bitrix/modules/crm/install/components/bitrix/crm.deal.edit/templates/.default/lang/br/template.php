<?
$MESS["CRM_TAB_1"] = "Negócio";
$MESS["CRM_TAB_1_TITLE"] = "Propriedades do negócio";
$MESS["CRM_TAB_2"] = "Registro";
$MESS["CRM_TAB_2_TITLE"] = "Rergistro do negócio";
$MESS["CRM_TAB_3"] = "Processo de Negócio";
$MESS["CRM_TAB_3_TITLE"] = "Processos de negócios do negócio";
$MESS["CRM_DEAL_EDIT_TITLE"] = "Negociação ##ID# &mdash; #TITLE#";
$MESS["CRM_DEAL_CREATE_TITLE"] = "Nova negociação";
$MESS["CRM_DEAL_CONV_ACCESS_DENIED"] = "Permissão insuficiente para criar faturas e orçamentos.";
$MESS["CRM_DEAL_CONV_GENERAL_ERROR"] = "Erro genérico.";
$MESS["CRM_DEAL_CONV_DIALOG_TITLE"] = "Criar Entidade na Venda";
$MESS["CRM_DEAL_CONV_DIALOG_CONTINUE_BTN"] = "Continuar";
$MESS["CRM_DEAL_CONV_DIALOG_CANCEL_BTN"] = "Cancelar";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_LEGEND"] = "As entidades selecionadas não têm campos para os quais os dados de uma venda podem ser passados. Escolha entidades em que campos adicionais serão criados para salvar todas as informações disponíveis. ";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_FILED_LIST_TITLE"] = "Quais campos serão criados";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_ENTITY_LIST_TITLE"] = "Escolha as entidades em que os campos adicionais serão criados";
$MESS["CRM_DEAL_RECUR_SHOW_TITLE"] = "Modelo de venda ##ID# &mdash; #TITLE#";
?>