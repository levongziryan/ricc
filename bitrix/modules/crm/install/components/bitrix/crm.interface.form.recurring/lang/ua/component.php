<?
$MESS["CRM_RECURRING_FILTER_DAILY"] = "день";
$MESS["CRM_RECURRING_FILTER_WEEKLY"] = "тиждень";
$MESS["CRM_RECURRING_FILTER_MONTHLY"] = "місяць";
$MESS["CRM_RECURRING_FILTER_YEARLY"] = "рік";
$MESS["CRM_RECURRING_FILTER_END_CONSTRAINT_TIMES_PLURAL_0"] = "повторення";
$MESS["CRM_RECURRING_FILTER_END_CONSTRAINT_TIMES_PLURAL_1"] = "повторень";
$MESS["CRM_RECURRING_FILTER_END_CONSTRAINT_TIMES_PLURAL_2"] = "повторень";
$MESS["CRM_RECURRING_FILTER_DAILY_WORK"] = "робочий";
$MESS["CRM_RECURRING_FILTER_DAILY_ANY"] = "календарний";
$MESS["CRM_RECURRING_FILTER_EACH_M"] = "кожен";
$MESS["CRM_RECURRING_FILTER_EACH_M_ALT"] = "кожного";
$MESS["CRM_RECURRING_FILTER_EACH_F"] = "кожну";
$MESS["CRM_RECURRING_FILTER_EACH"] = "кожне";
$MESS["CRM_RECURRING_FILTER_REPEAT_TYPE"] = "повторюваність";
$MESS["CRM_RECURRING_FILTER_DAY_INTERVAL"] = "день";
$MESS["CRM_RECURRING_FILTER_MONTH_SHORT"] = "міс";
$MESS["CRM_RECURRING_FILTER_WEEK_ALT"] = "тиждень";
$MESS["CRM_RECURRING_FILTER_MONTH_ALT"] = "місяця";
$MESS["CRM_RECURRING_FILTER_NUMBER_OF_EACH_M_ALT"] = "день кожного";
$MESS["CRM_RECURRING_FILTER_DAY_OF_MONTH"] = "день місяця";
$MESS["CRM_RECURRING_FILTER_REPEAT_START"] = "Починати повторення";
$MESS["CRM_RECURRING_FILTER_REPEAT_END"] = "Повторювати до";
$MESS["CRM_RECURRING_FILTER_REPEAT_END_C_DATE"] = "дата закінчення";
$MESS["CRM_RECURRING_FILTER_REPEAT_END_C_NONE"] = "немає дати закінчення";
$MESS["CRM_RECURRING_FILTER_REPEAT_END_C_TIMES"] = "завершити після";
$MESS["CRM_RECURRING_FILTER_REPEATS"] = "повторень";
$MESS["CRM_RECURRING_FILTER_WD_SH_1"] = "пн";
$MESS["CRM_RECURRING_FILTER_WD_SH_2"] = "вт";
$MESS["CRM_RECURRING_FILTER_WD_SH_3"] = "ср";
$MESS["CRM_RECURRING_FILTER_WD_SH_4"] = "чт";
$MESS["CRM_RECURRING_FILTER_WD_SH_5"] = "пт";
$MESS["CRM_RECURRING_FILTER_WD_SH_6"] = "сб";
$MESS["CRM_RECURRING_FILTER_WD_SH_7"] = "нд";
$MESS["CRM_RECURRING_FILTER_MONTH_1"] = "січень";
$MESS["CRM_RECURRING_FILTER_MONTH_2"] = "лютий";
$MESS["CRM_RECURRING_FILTER_MONTH_3"] = "березень";
$MESS["CRM_RECURRING_FILTER_MONTH_4"] = "квітень";
$MESS["CRM_RECURRING_FILTER_MONTH_5"] = "травень";
$MESS["CRM_RECURRING_FILTER_MONTH_6"] = "червень";
$MESS["CRM_RECURRING_FILTER_MONTH_7"] = "липень";
$MESS["CRM_RECURRING_FILTER_MONTH_8"] = "серпень";
$MESS["CRM_RECURRING_FILTER_MONTH_9"] = "вересень";
$MESS["CRM_RECURRING_FILTER_MONTH_10"] = "жовтень";
$MESS["CRM_RECURRING_FILTER_MONTH_11"] = "листопад";
$MESS["CRM_RECURRING_FILTER_MONTH_12"] = "грудень";
$MESS["CRM_RECURRING_FILTER_WD_1"] = "понеділок";
$MESS["CRM_RECURRING_FILTER_WD_2"] = "вівторок";
$MESS["CRM_RECURRING_FILTER_WD_3"] = "середа";
$MESS["CRM_RECURRING_FILTER_WD_4"] = "четвер";
$MESS["CRM_RECURRING_FILTER_WD_5"] = "п'ятниця";
$MESS["CRM_RECURRING_FILTER_WD_6"] = "субота";
$MESS["CRM_RECURRING_FILTER_WD_7"] = "неділя";
$MESS["CRM_RECURRING_FILTER_NUMBER_0"] = "перше";
$MESS["CRM_RECURRING_FILTER_NUMBER_1"] = "друге";
$MESS["CRM_RECURRING_FILTER_NUMBER_2"] = "третє";
$MESS["CRM_RECURRING_FILTER_NUMBER_3"] = "четверте";
$MESS["CRM_RECURRING_FILTER_NUMBER_4"] = "останнє";
$MESS["CRM_RECURRING_FILTER_NUMBER_0_M"] = "перший";
$MESS["CRM_RECURRING_FILTER_NUMBER_0_F"] = "першу";
$MESS["CRM_RECURRING_FILTER_NUMBER_1_M"] = "другий";
$MESS["CRM_RECURRING_FILTER_NUMBER_1_F"] = "другу";
$MESS["CRM_RECURRING_FILTER_NUMBER_2_M"] = "третій";
$MESS["CRM_RECURRING_FILTER_NUMBER_2_F"] = "третю";
$MESS["CRM_RECURRING_FILTER_NUMBER_3_M"] = "четвертий";
$MESS["CRM_RECURRING_FILTER_NUMBER_3_F"] = "четверту";
$MESS["CRM_RECURRING_FILTER_NUMBER_4_M"] = "останній";
$MESS["CRM_RECURRING_FILTER_NUMBER_4_F"] = "останню";
$MESS["CRM_RECURRING_TEMPLATE_WILL_BE_CREATED"] = "Увага! На підставі рахунку буде створено новий шаблон, з вказаними параметрами повторення.";
$MESS["CRM_RECURRING_SWITCHER_BLOCK"] = "Зробити рахунок регулярним ";
$MESS["CRM_RECURRING_EMAIL_LABEL"] = "автоматично відправляти рахунок клієнту на";
$MESS["CRM_RECURRING_EMAIL_TEMPLATE"] = "шаблон листа";
$MESS["CRM_RECURRING_CREATE_NEW"] = "створити новий";
$MESS["CRM_RECURRING_HINT_BASE"] = "Рахунок виставляється #ELEMENT##START##END#.";
$MESS["CRM_RECURRING_HINT_START_DATE"] = ", починаючи з <nobr>#DATETIME#</nobr>";
$MESS["CRM_RECURRING_HINT_START_EMPTY"] = "з моменту збереження";
$MESS["CRM_RECURRING_HINT_END"] = ", із закінченням <nobr>#DATETIME#</nobr>";
$MESS["CRM_RECURRING_HINT_END_NONE"] = ", без дати закінчення";
$MESS["CRM_RECURRING_HINT_END_TIMES"] = ", із закінченням після <nobr>#TIMES# #TIMES_PLURAL#</nobr>";
$MESS["CRM_RECURRING_HINT_END_CONSTRAINT_TIMES_PLURAL_0"] = "повторення";
$MESS["CRM_RECURRING_HINT_END_CONSTRAINT_TIMES_PLURAL_1"] = "повторень";
$MESS["CRM_RECURRING_HINT_END_CONSTRAINT_TIMES_PLURAL_2"] = "повторень";
$MESS["CRM_RECURRING_HINT_ELEMENT_DAY_MASK"] = "кожен #DAY_NUMBER#день";
$MESS["CRM_RECURRING_HINT_ELEMENT_DAY_MASK_WORK"] = "кожен #DAY_NUMBER#робочий день";
$MESS["CRM_RECURRING_HINT_ELEMENT_WEEKDAY_MASK"] = "кожен #WEEK_NUMBER#тиждень(#LIST_WEEKDAY_NAMES#)";
$MESS["CRM_RECURRING_HINT_WEEKDAY_EVERY_DAY"] = "щодня";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_1"] = "понеділок";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_2"] = "вівторок";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_3"] = "середа";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_4"] = "четвер";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_5"] = "п'ятниця";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_6"] = "субота";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_7"] = "неділя";
$MESS["CRM_RECURRING_HINT_ELEMENT_MONTH_MASK_1"] = "кожне #DAY_NUMBER# число кожного #MONTH_NUMBER#місяця";
$MESS["CRM_RECURRING_HINT_ELEMENT_MONTH_MASK_1_WORK"] = "кожен #DAY_NUMBER# кожного робочого дня #MONTH_NUMBER#місяця";
$MESS["CRM_RECURRING_HINT_MONTHLY_EXT_TYPE_2"] = "#EACH# #WEEKDAY_NUMBER# #WEEKDAY_NAME# кожного #MONTH_NUMBER#місяця";
$MESS["CRM_RECURRING_HINT_YEARLY_EXT_TYPE_1"] = "кожне #DAY_NUMBER# число місяця #MONTH_NAME#";
$MESS["CRM_RECURRING_HINT_YEARLY_EXT_TYPE_1_WORKDAY"] = "кожен #DAY_NUMBER# робочий день місяця #MONTH_NAME#";
$MESS["CRM_RECURRING_HINT_YEARLY_EXT_TYPE_2"] = "#EACH# #WEEKDAY_NUMBER# #WEEKDAY_NAME# місяця #MONTH_NAME#";
$MESS["CRM_RECURRING_HINT_EACH"] = "кожне";
$MESS["CRM_RECURRING_HINT_EACH_M"] = "кожен";
$MESS["CRM_RECURRING_HINT_EACH_M_ALT"] = "кожного";
$MESS["CRM_RECURRING_HINT_EACH_F"] = "кожну";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_0"] = "перше";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_1"] = "друге";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_2"] = "третє";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_3"] = "четверте";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_4"] = "останнє";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_0_M"] = "перший";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_0_F"] = "першу";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_1_M"] = "другий";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_1_F"] = "другу";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_2_M"] = "третій";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_2_F"] = "третю";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_3_M"] = "четвертий";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_3_F"] = "четверту";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_4_M"] = "останні";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_4_F"] = "останню";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_3_ALT"] = "середу";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_5_ALT"] = "п'ятницю";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_6_ALT"] = "суботу";
$MESS["CRM_RECURRING_HINT_MONTH_1"] = "січень";
$MESS["CRM_RECURRING_HINT_MONTH_2"] = "лютий";
$MESS["CRM_RECURRING_HINT_MONTH_3"] = "березень";
$MESS["CRM_RECURRING_HINT_MONTH_4"] = "квітень";
$MESS["CRM_RECURRING_HINT_MONTH_5"] = "травень";
$MESS["CRM_RECURRING_HINT_MONTH_6"] = "червень";
$MESS["CRM_RECURRING_HINT_MONTH_7"] = "липень";
$MESS["CRM_RECURRING_HINT_MONTH_8"] = "серпень";
$MESS["CRM_RECURRING_HINT_MONTH_9"] = "вересень";
$MESS["CRM_RECURRING_HINT_MONTH_10"] = "жовтень";
$MESS["CRM_RECURRING_HINT_MONTH_11"] = "листопад";
$MESS["CRM_RECURRING_HINT_MONTH_12"] = "грудень";
$MESS["CRM_RECURRING_B24_BLOCK_TEXT"] = "Кожний рахунок у Бітрікс24 можна повторювати автоматично. 
Якщо ви працюєте з послугами, регулярно виставляєте рахунки своїм клієнтам, наприклад, раз в місяць, регулярні рахунки значно заощадять ваш час. 
Виставте рахунок клієнту і вкажіть регулярність його повторення. В потрібний час CRM автоматично створить рахунок і відправить його на e-mail клієнта.
<ul class=\"hide-features-list\">
<li class=\"hide-features-list-item\">Автоматичне створення рахунку</li>
<li class=\"hide-features-list-item\">Швидкий доступ до всіх шаблонів регулярних рахунків</li>
 <li class=\"hide-features-list-item\">Відправлення рахунку для оплати на e-mail клієнта <a href=\"#LINK#\" target=\"_blank\" class=\"hide-features-more\">Дізнатися докладніше</a></li>
</ul> 
<strong>Регулярні рахунки доступні в тарифах \"Команда\" і \"Компанія\"</strong>";
$MESS["CRM_RECURRING_B24_BLOCK_TITLE"] = "Регулярні рахунки доступні в тарифах \"Команда\" і \"Компанія\"";
$MESS["CRM_RECURRING_EMPTY_OWNER_EMAIL"] = "Не заповнено E-mail в реквізитах вашої компанії";
$MESS["CRM_RECURRING_EMPTY_OWNER_EMAIL1"] = "Не заповнено E-mail в реквізитах вашої компанії";
?>