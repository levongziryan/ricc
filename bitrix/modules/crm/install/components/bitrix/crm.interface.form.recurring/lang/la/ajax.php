<?
$MESS["NEXT_EXECUTION_DEAL_HINT"] = "La próxima negociación se creará en #DATE_EXECUTION#";
$MESS["NEXT_DEAL_EMPTY"] = "Nada seleccionado";
$MESS["NEXT_BASED_ON_DEAL_ONCE"] = "La plantilla ha sido creada para una negociación recurrente.##ID# ";
$MESS["NEXT_BASED_ON_DEAL_MULTY"] = "La plantilla ha sido creada para negociaciones recurrentes: #ID_LINE# ";
$MESS["SIGN_NUM_WITH_DEAL_ID"] = "##DEAL_ID#";
?>