<?
$MESS["CRM_FIELD_EDIT_URL"] = "URL de rédaction d'un champ";
$MESS["CRM_BLF_LIST_URL"] = "URL de la liste";
$MESS["CRM_FIELDS_LIST_URL"] = "Les champs à afficher dans un formulaire";
$MESS["CRM_ENTITY_LIST_URL"] = "URL de la liste de types";
$MESS["CRM_FIELS_EDIT_FIELD_ID"] = "Champ supplémentaire";
$MESS["CRM_FIELS_EDIT_ENTITY_ID"] = "Type de champ";
?>