<?
$MESS["CRM_PERMS_PERM_ADD"] = "El usuario puede editar configuración";
$MESS["CRM_PERMS_BUTTONS_SAVE"] = "Guardar";
$MESS["CRM_PERMS_BUTTONS_APPLY"] = "Aplicar";
$MESS["CRM_PERMS_HEAD_ENTITY"] = "Entidad";
$MESS["CRM_PERMS_HEAD_ADD"] = "Agregar";
$MESS["CRM_PERMS_HEAD_WRITE"] = "Actualización";
$MESS["CRM_PERMS_HEAD_READ"] = "Leer";
$MESS["CRM_PERMS_HEAD_DELETE"] = "Eliminar";
$MESS["CRM_PERMS_FILED_NAME"] = "Rol";
$MESS["CRM_PERMS_DLG_MESSAGE"] = "¿Usted está seguro que desea eliminarlo?";
$MESS["CRM_PERMS_DLG_TITLE"] = "Eliminar rol";
$MESS["CRM_PERMS_DLG_BTN"] = "Eliminar";
$MESS["CRM_PERMS_ROLE_DELETE"] = "Eliminar rol";
$MESS["CRM_PERMS_PERM_INHERIT"] = "Heredado";
$MESS["CRM_PERMS_HEAD_EXPORT"] = "Exportar";
$MESS["CRM_PERMS_HEAD_IMPORT"] = "Importar";
?>