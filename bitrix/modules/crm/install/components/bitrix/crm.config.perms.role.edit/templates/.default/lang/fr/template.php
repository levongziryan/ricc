<?
$MESS["CRM_PERMS_DLG_MESSAGE"] = "Etes-vous sûr de vouloir supprimer?";
$MESS["CRM_PERMS_HEAD_ADD"] = "Ajouter";
$MESS["CRM_PERMS_HEAD_WRITE"] = "Mettre à jour";
$MESS["CRM_PERMS_HEAD_IMPORT"] = "Charger";
$MESS["CRM_PERMS_PERM_INHERIT"] = "Hériter";
$MESS["CRM_PERMS_BUTTONS_APPLY"] = "Appliquer";
$MESS["CRM_PERMS_PERM_ADD"] = "Laisser modifier les paramètres";
$MESS["CRM_PERMS_FILED_NAME"] = "Rôle";
$MESS["CRM_PERMS_BUTTONS_SAVE"] = "Sauvegarder";
$MESS["CRM_PERMS_HEAD_ENTITY"] = "Bloc Highload";
$MESS["CRM_PERMS_HEAD_DELETE"] = "Supprimer";
$MESS["CRM_PERMS_DLG_TITLE"] = "Suppression du rôle";
$MESS["CRM_PERMS_DLG_BTN"] = "Supprimer";
$MESS["CRM_PERMS_ROLE_DELETE"] = "Suppression du rôle";
$MESS["CRM_PERMS_HEAD_READ"] = "Lecture";
$MESS["CRM_PERMS_HEAD_EXPORT"] = "Décharger";
?>