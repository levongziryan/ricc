<?
$MESS["CRM_PERMS_PERM_ADD"] = "O usuário pode editar as configurações";
$MESS["CRM_PERMS_BUTTONS_SAVE"] = "Salvar";
$MESS["CRM_PERMS_BUTTONS_APPLY"] = "Aplicar";
$MESS["CRM_PERMS_HEAD_ENTITY"] = "Entidade";
$MESS["CRM_PERMS_HEAD_ADD"] = "Adicionar";
$MESS["CRM_PERMS_HEAD_WRITE"] = "Atualizar";
$MESS["CRM_PERMS_HEAD_READ"] = "Ler";
$MESS["CRM_PERMS_HEAD_DELETE"] = "Excluir";
$MESS["CRM_PERMS_HEAD_EXPORT"] = "Exportar";
$MESS["CRM_PERMS_HEAD_IMPORT"] = "Importar";
$MESS["CRM_PERMS_DLG_MESSAGE"] = "Tem certeza de que deseja excluí-lo?";
$MESS["CRM_PERMS_DLG_BTN"] = "Excluir";
$MESS["CRM_PERMS_PERM_INHERIT"] = "Herdar";
$MESS["CRM_PERMS_FILED_NAME"] = "Função";
$MESS["CRM_PERMS_DLG_TITLE"] = "Excluir função";
$MESS["CRM_PERMS_ROLE_DELETE"] = "Excluir função";
?>