<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_ENTITY_TYPE_LEAD"] = "Prospecto";
$MESS["CRM_ENTITY_TYPE_CONTACT"] = "Contacto";
$MESS["CRM_ENTITY_TYPE_COMPANY"] = "Compañía";
$MESS["CRM_ENTITY_TYPE_DEAL"] = "Negociación";
$MESS["CRM_PERMS_TYPE_"] = "Acceso denegado.";
$MESS["CRM_PERMS_TYPE_X"] = "Todo";
$MESS["CRM_PERMS_TYPE_A"] = "Personal";
$MESS["CRM_PERMS_TYPE_D"] = "Personal y departamento";
$MESS["CRM_PERMS_TYPE_F"] = "Personal, departamento y subdepartamentos";
$MESS["CRM_PERMS_TYPE_O"] = "Todo abierto";
$MESS["CRM_PERMS_ENTITY_LIST"] = "Permisos de acceso";
$MESS["CRM_PERMS_ROLE_EDIT"] = "Rol del administrador";
$MESS["CRM_ENTITY_TYPE_INVOICE"] = "Factura";
$MESS["CRM_ENTITY_TYPE_QUOTE"] = "Cotización";
$MESS["CRM_ENTITY_TYPE_WEBFORM"] = "Formulario del CRM";
$MESS["CRM_ENTITY_TYPE_BUTTON"] = "Icono del sitio web";
$MESS["CRM_ENTITY_TYPE_SALETARGET"] = "Objetivo de Ventas";
?>