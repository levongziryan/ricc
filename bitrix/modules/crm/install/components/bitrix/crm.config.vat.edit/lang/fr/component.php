<?
$MESS["CRM_VAT_FIELD_ACTIVE"] = "Actif(ve)";
$MESS["CRM_VAT_UPDATE_UNKNOWN_ERROR"] = "Une erreur s'est produite au cours de la mise à jour du taux de TVA.";
$MESS["CRM_VAT_ADD_UNKNOWN_ERROR"] = "Une erreur s'est produite pendant la création d'un taux TVA.";
$MESS["CRM_VAT_DELETE_UNKNOWN_ERROR"] = "Pendant l'élimination du taux de TVA une erreur a eu lieu.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_VAT_FIELD_ID"] = "ID";
$MESS["CRM_CATALOG_MODULE_NOT_INSTALLED"] = "Le module 'Catalogue de marchandises' n'a pas été installé.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_VAT_FIELD_NAME"] = "Dénomination";
$MESS["CRM_VAT_FIELD_C_SORT"] = "Trier";
$MESS["CRM_VAT_FIELD_RATE"] = "Coefficient";
$MESS["CRM_VAT_SECTION_MAIN"] = "Taux de TVA";
$MESS["CRM_VAT_NOT_FOUND"] = "La TVA n'est pas trouvée!";
?>