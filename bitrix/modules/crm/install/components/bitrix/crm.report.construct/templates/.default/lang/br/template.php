<?
$MESS["REPORT_POPUP_COLUMN_TITLE"] = "Negócio";
$MESS["REPORT_CHOOSE"] = "Selecionar";
$MESS["CRM_FF_LEAD"] = "Leads";
$MESS["CRM_FF_CONTACT"] = "Contatos";
$MESS["CRM_FF_COMPANY"] = "Empresas";
$MESS["CRM_FF_DEAL"] = "Negócios";
$MESS["CRM_FF_OK"] = "Selecionar";
$MESS["CRM_FF_CANCEL"] = "Cancelar";
$MESS["CRM_FF_CLOSE"] = "Fechar";
$MESS["CRM_FF_SEARCH"] = "Pesquisar";
$MESS["CRM_FF_NO_RESULT"] = "Infelizmente, seu pedido de busca não retornou resultados.";
$MESS["CRM_FF_CHOISE"] = "Selecionar";
$MESS["CRM_FF_CHANGE"] = "Editar";
$MESS["CRM_FF_LAST"] = "ltimo";
$MESS["REPORT_IGNORE_FILTER_VALUE"] = "Ignorar";
$MESS["CRM_REPORT_INCLUDE_ALL"] = "Todos";
$MESS["REPORT_POPUP_COLUMN_TITLE_CRM"] = "Negócio";
$MESS["REPORT_POPUP_COLUMN_TITLE_CRM_PRODUCT_ROW"] = "Produto";
$MESS["CRM_REPORT_SELECT_OWNER"] = "Selecione os campos para o novo relatório";
$MESS["CRM_REPORT_CONSTRUCT_BUTTON_CANCEL"] = "Cancelar";
$MESS["CRM_REPORT_CONSTRUCT_BUTTON_CONTINUE"] = "Próximo...";
?>