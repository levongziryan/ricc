<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_FIELDS_ENTITY_LIST"] = "Tipos";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "El módulo Currency no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "El módulo Sale no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "El módulo Catalog no está instalado.";
$MESS["CRM_MODULE_ERROR_REMOVE_FIELD"] = "No se puede eliminar el campo #field# porque se utiliza en actuales entidades.";
$MESS["CRM_MODULE_ERROR_REMOVE_FIELD_MANY"] = "No se puede eliminar el campo #field# porque se utiliza en actuales entidades.";
$MESS["CRM_STATUS_ADD_DEAL_STAGE"] = "Agregar fase";
$MESS["CRM_STATUS_ADD_STATUS"] = "Agregar estado";
$MESS["CRM_STATUS_ADD_QUOTE_STATUS"] = "Agregar estado";
$MESS["CRM_STATUS_ADD_INVOICE_STATUS"] = "Agregar estado";
$MESS["CRM_STATUS_DEFAULT_NAME_DEAL_STAGE"] = "Nueva fase";
$MESS["CRM_STATUS_DEFAULT_NAME_STATUS"] = "Nuevo estado";
$MESS["CRM_STATUS_DEFAULT_NAME_QUOTE_STATUS"] = "Nuevo estado";
$MESS["CRM_STATUS_DEFAULT_NAME_INVOICE_STATUS"] = "Nuevo estado";
$MESS["CRM_STATUS_DELETION_CONFIRMATION_DEAL_STAGE"] = "¿Está seguro que quiere eliminar la fase? Puede estar en uso por algunas de las entidades activas.";
$MESS["CRM_STATUS_DELETION_CONFIRMATION_STATUS"] = "¿Está seguro que quiere eliminar el estado? Puede estar en uso por algunas de las entidades activas.";
$MESS["CRM_STATUS_DELETION_CONFIRMATION_QUOTE_STATUS"] = "¿Está seguro que quiere eliminar el estado? Puede estar en uso por algunas de las entidades activas.";
$MESS["CRM_STATUS_DELETION_CONFIRMATION_INVOICE_STATUS"] = "¿Está seguro que quiere eliminar el estado? Puede estar en uso por algunas de las entidades activas.";
?>