<?
$MESS["CRM_LEAD_EDIT_EVENT_SUCCESSFULLY_CREATED"] = "Prospecto <a href='#URL#'>#TITLE#</a> ha sido creado. Ahora está siendo redirigido a la página anterior. Si la página actual sigue mostrándose, ciérrela manualmente.";
$MESS["CRM_LEAD_EDIT_EVENT_CANCELED"] = "Se ha cancelado la acción. Ahora estás siendo redirigido a la página anterior. Si la página actual sigue mostrándose, ciérrela manualmente.";
?>