<?
$MESS["CRM_ACT_CUST_TYPE_EDIT_TITLE"] = "Editar tipo de actividad";
$MESS["CRM_ACT_CUST_TYPE_EDIT"] = "Editar";
$MESS["CRM_ACT_CUST_TYPE_DELETE_TITLE"] = "Eliminar pipeline";
$MESS["CRM_ACT_CUST_TYPE_DELETE"] = "Eliminar";
$MESS["CRM_ACT_CUST_TYPE_DELETE_CONFIRM"] = "¿Está seguro de que desea eliminar? \"#NAME#\"?";
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_ACT_CUST_TYPE_FIELD_NAME"] = "Nombre";
$MESS["CRM_ACT_CUST_TYPE_FIELD_SORT"] = "Clasificar";
$MESS["CRM_ACT_CUST_TYPE_DEFAULT_NAME"] = "Nuevo tipo de actividad";
$MESS["CRM_ACT_CUST_TYPE_TITLE_CREATE"] = "Crear un nuevo tipo de actividad";
$MESS["CRM_ACT_CUST_TYPE_TITLE_EDIT"] = "Editar tipo de actividad";
$MESS["CRM_ACT_CUST_TYPE_BUTTON_SAVE"] = "Guardar";
$MESS["CRM_ACT_CUST_TYPE_BUTTON_CANCEL"] = "Cancelar";
$MESS["CRM_ACT_CUST_TYPE_ERROR_TITLE"] = "Guardar error";
$MESS["CRM_ACT_CUST_TYPE_FIELD_NAME_NOT_ASSIGNED_ERROR"] = "Falta el valor de campo \"Nombre\".";
$MESS["CRM_ACT_CUST_TYPE_USER_FIELD_EDIT"] = "Editar campos personalizados";
$MESS["CRM_ACT_CUST_TYPE_USER_FIELD_EDIT_TITLE"] = "Editar campos personalizados para este tipo de actividad";
?>