<?
$MESS["CRM_ACT_CUST_TYPE_EDIT_TITLE"] = "Éditer le type d'activité";
$MESS["CRM_ACT_CUST_TYPE_EDIT"] = "Editer";
$MESS["CRM_ACT_CUST_TYPE_DELETE_TITLE"] = "Supprimer le pipeline";
$MESS["CRM_ACT_CUST_TYPE_DELETE"] = "Supprimer";
$MESS["CRM_ACT_CUST_TYPE_DELETE_CONFIRM"] = "Voulez-vous vraiment supprimer \"#NAME#\" ?";
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_ACT_CUST_TYPE_FIELD_NAME"] = "Nom";
$MESS["CRM_ACT_CUST_TYPE_FIELD_SORT"] = "Trier";
$MESS["CRM_ACT_CUST_TYPE_DEFAULT_NAME"] = "Nouveau type d'activité";
$MESS["CRM_ACT_CUST_TYPE_TITLE_CREATE"] = "Créer un nouveau type d'activité";
$MESS["CRM_ACT_CUST_TYPE_TITLE_EDIT"] = "Éditer le type d'activité";
$MESS["CRM_ACT_CUST_TYPE_BUTTON_SAVE"] = "Enregistrer";
$MESS["CRM_ACT_CUST_TYPE_BUTTON_CANCEL"] = "Annuler";
$MESS["CRM_ACT_CUST_TYPE_ERROR_TITLE"] = "Erreur de sauvegarde";
$MESS["CRM_ACT_CUST_TYPE_FIELD_NAME_NOT_ASSIGNED_ERROR"] = "La valeur du champ \"Nom\" est manquante.";
$MESS["CRM_ACT_CUST_TYPE_USER_FIELD_EDIT"] = "Éditer les champs personnalisés";
$MESS["CRM_ACT_CUST_TYPE_USER_FIELD_EDIT_TITLE"] = "Éditer les champs personnalisés pour ce type d'activité";
?>