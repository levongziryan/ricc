<?
$MESS["CRM_CLL2_CRM_MODULE_NOT_INSTALL"] = "O módulo de CRM não está instalado.";
$MESS["CRM_CLL2_SALE_MODULE_NOT_INSTALL"] = "A módulo e-Store não está instalado.";
$MESS["CRM_CLL2_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_CLL2_LOC_DELETION_GENERAL_ERROR"] = "Erro ao excluir a localização.";
$MESS["CRM_CLL2_LOC_UPDATE_GENERAL_ERROR"] = "Erro ao atualizar a localização.";
$MESS["CRM_CLL2_INTS_TASKS_NAV"] = "Registros";
?>