<?
$MESS["CRM_CLL2_CRM_MODULE_NOT_INSTALL"] = "El módulo CRM no está instalado.";
$MESS["CRM_CLL2_SALE_MODULE_NOT_INSTALL"] = "El módulo e-Store no está instalado.";
$MESS["CRM_CLL2_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_CLL2_LOC_DELETION_GENERAL_ERROR"] = "Error al eliminar la ubicación.";
$MESS["CRM_CLL2_LOC_UPDATE_GENERAL_ERROR"] = "Error al actualizar la ubicación.";
$MESS["CRM_CLL2_INTS_TASKS_NAV"] = "Registros";
?>