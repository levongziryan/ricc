<?
$MESS["CRM_CLL2_CRM_MODULE_NOT_INSTALL"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_CLL2_SALE_MODULE_NOT_INSTALL"] = "Le module 'Boutique en ligne' n'a pas été installé.";
$MESS["CRM_CLL2_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_CLL2_LOC_DELETION_GENERAL_ERROR"] = "Erreur de suppression de l'ordre.";
$MESS["CRM_CLL2_LOC_UPDATE_GENERAL_ERROR"] = "Erreur de la mise à jour de l'emplacement.";
$MESS["CRM_CLL2_INTS_TASKS_NAV"] = "Inscriptions";
?>