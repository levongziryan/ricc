<?
$MESS["CRM_CLL2_NOT_SELECTED"] = "No seleccionado";
$MESS["CRM_CLL2_EDIT_TITLE"] = "Abrir esta ubicación para la edición";
$MESS["CRM_CLL2_EDIT"] = "Editar ubicación";
$MESS["CRM_CLL2_DELETE_TITLE"] = "Eliminar esta ubicación";
$MESS["CRM_CLL2_DELETE"] = "Eliminar ubicación";
$MESS["CRM_CLL2_DELETE_CONFIRM"] = "¿Seguro que quieres eliminar '% s'?";
$MESS["CRM_CLL2_ALL"] = "Total";
$MESS["CRM_CLL2_VIEW_SUBTREE_TITLE"] = "Ver ubicaciones menores";
$MESS["CRM_CLL2_VIEW_SUBTREE"] = "Ver ubicaciones menores";
?>