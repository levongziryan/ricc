<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado. ";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_VAT_ADD"] = "Adicionar taxa de IVA";
$MESS["CRM_VAT_ADD_TITLE"] = "Criar uma nova taxa de IVA";
$MESS["CRM_VAT_EDIT"] = "Editar taxa de imposto";
$MESS["CRM_VAT_EDIT_TITLE"] = "Abrir taxa de IVA para edição";
$MESS["CRM_VAT_DELETE"] = "Deletar taxa de imposto";
$MESS["CRM_VAT_DELETE_TITLE"] = "Deletar taxa de IVA";
$MESS["CRM_VAT_DELETE_DLG_TITLE"] = "Deletar taxa de IVA";
$MESS["CRM_VAT_DELETE_DLG_MESSAGE"] = "Você tem certeza que deseja deletar esta taxa de IVA?";
$MESS["CRM_VAT_DELETE_DLG_BTNTITLE"] = "Deletar taxa de IVA";
$MESS["CRM_VAT_SHOW"] = "Visualizar taxa";
$MESS["CRM_VAT_SHOW_TITLE"] = "Visualizar taxa de IVA";
$MESS["CRM_VAT_LIST"] = "Taxas de IVA";
$MESS["CRM_VAT_LIST_TITLE"] = "Ver todas as taxas de IVA";
$MESS["CRM_VAT_SETTINGS"] = "Configurações";
$MESS["CRM_VAT_SETTINGS_TITLE"] = "Configurar sistema de impostos";
?>