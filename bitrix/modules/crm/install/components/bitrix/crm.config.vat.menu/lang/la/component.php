<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_VAT_ADD"] = "Agregar tasa de IVA";
$MESS["CRM_VAT_ADD_TITLE"] = "Crear un nuevo tipo de IVA";
$MESS["CRM_VAT_EDIT"] = "Editar tasa de impuesto";
$MESS["CRM_VAT_EDIT_TITLE"] = "Tipo del IVA abierto para su edición";
$MESS["CRM_VAT_DELETE"] = "Eliminar tasa de impuesto";
$MESS["CRM_VAT_DELETE_TITLE"] = "Eliminar tasa del IVA";
$MESS["CRM_VAT_DELETE_DLG_TITLE"] = "Eliminar tasa del IVA";
$MESS["CRM_VAT_DELETE_DLG_MESSAGE"] = "Está seguro de que desea eliminar esta tasa?";
$MESS["CRM_VAT_DELETE_DLG_BTNTITLE"] = "Eliminar tasa del IVA";
$MESS["CRM_VAT_SHOW"] = "Ver tasa";
$MESS["CRM_VAT_SHOW_TITLE"] = "Ver el tipo de IVA";
$MESS["CRM_VAT_LIST"] = "Tipos de IVA";
$MESS["CRM_VAT_LIST_TITLE"] = "Ver todos los tipos del IVA";
$MESS["CRM_VAT_SETTINGS"] = "Configuración";
$MESS["CRM_VAT_SETTINGS_TITLE"] = "Configurar sistema de impuestos";
?>