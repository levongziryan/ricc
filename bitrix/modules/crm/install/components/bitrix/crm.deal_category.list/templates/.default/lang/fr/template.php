<?
$MESS["CRM_DEAL_CATEGORY_EDIT_TITLE"] = "Modifier le pipeline";
$MESS["CRM_DEAL_CATEGORY_EDIT"] = "Modifier";
$MESS["CRM_DEAL_CATEGORY_STATUS_EDIT"] = "Modifier les étapes";
$MESS["CRM_DEAL_CATEGORY_STATUS_EDIT_TITLE"] = "Modifier les étapes du pipeline";
$MESS["CRM_DEAL_CATEGORY_DELETE_TITLE"] = "Supprimer le pipeline";
$MESS["CRM_DEAL_CATEGORY_DELETE"] = "Supprimer";
$MESS["CRM_DEAL_CATEGORY_DELETE_CONFIRM"] = "Voulez-vous vraiment supprimer \"#NAME#\"?";
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_DEAL_CATEGORY_FIELD_NAME"] = "Nom";
$MESS["CRM_DEAL_CATEGORY_FIELD_SORT"] = "Trier";
$MESS["CRM_DEAL_CATEGORY_TITLE_CREATE"] = "Créer un pipeline";
$MESS["CRM_DEAL_CATEGORY_TITLE_EDIT"] = "Modifier le pipeline";
$MESS["CRM_DEAL_CATEGORY_BUTTON_SAVE"] = "Enregistrer";
$MESS["CRM_DEAL_CATEGORY_BUTTON_CANCEL"] = "Annuler";
$MESS["CRM_DEAL_CATEGORY_ERROR_TITLE"] = "Erreur d'enregistrement";
$MESS["CRM_DEAL_CATEGORY_FIELD_NAME_NOT_ASSIGNED_ERROR"] = "La valeur du champ \"Nom\" est manquante.";
?>