<?
$MESS["CRM_DEAL_CATEGORY_EDIT_TITLE"] = "Editar pipeline";
$MESS["CRM_DEAL_CATEGORY_EDIT"] = "Editar";
$MESS["CRM_DEAL_CATEGORY_STATUS_EDIT"] = "Editar fases";
$MESS["CRM_DEAL_CATEGORY_DELETE"] = "Eliminar";
$MESS["CRM_DEAL_CATEGORY_DELETE_CONFIRM"] = "Usted está seguro que quieres eliminar \"#NAME#\"?";
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_DEAL_CATEGORY_FIELD_NAME"] = "Nombre";
$MESS["CRM_DEAL_CATEGORY_FIELD_SORT"] = "Clasificar";
$MESS["CRM_DEAL_CATEGORY_BUTTON_SAVE"] = "Guardar";
$MESS["CRM_DEAL_CATEGORY_BUTTON_CANCEL"] = "Cancelar";
$MESS["CRM_DEAL_CATEGORY_ERROR_TITLE"] = "Guardar error";
$MESS["CRM_DEAL_CATEGORY_FIELD_NAME_NOT_ASSIGNED_ERROR"] = "El valor del campo \"Nombre\" se ha omitido.";
$MESS["CRM_DEAL_CATEGORY_STATUS_EDIT_TITLE"] = "Editar fase de pipelines";
$MESS["CRM_DEAL_CATEGORY_DELETE_TITLE"] = "Eliminar pipeline";
$MESS["CRM_DEAL_CATEGORY_TITLE_CREATE"] = "Crear pipeline";
$MESS["CRM_DEAL_CATEGORY_TITLE_EDIT"] = "Editar pipelines";
?>