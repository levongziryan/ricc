<?
$MESS["CRM_ELEMENT_ID"] = "ID du contact";
$MESS["CRM_CONTACT_VAR"] = "Le nom d'une variable d'identifiant du contact";
$MESS["CRM_NAME_TEMPLATE"] = "Format du nom";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "Modèle de chemin d'accès à la page principale";
$MESS["CRM_SEF_PATH_TO_SERVICE"] = "Modèle de chemin d'accès à la page du service WEB";
$MESS["CRM_SEF_PATH_TO_IMPORT"] = "Importer la page Chemin Template";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "Modèle de Chemin d'accès à la Page de visualisation du Contact";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "Modèle de chemin d'accès à la page d'édition d'un contact";
$MESS["CRM_SEF_PATH_TO_LIST"] = "Modèle de chemin d'accès à la page de la liste de contacts";
$MESS["CRM_SEF_PATH_TO_EXPORT"] = "Modèle de chemin d'accès à la page d'exportation";
?>