<?
$MESS["CRM_CONTACT_WGT_PAGE_TITLE"] = "Contatos: Relatório resumo";
$MESS["CRM_CONTACT_WGT_DEMO_TITLE"] = "Esta é uma visualização de demonstração. Feche-a para obter análises para seus contatos.";
$MESS["CRM_CONTACT_WGT_DEMO_CONTENT"] = "Se você ainda não tem nenhum contato, <a href=\"#URL#\" class=\"#CLASS_NAME#\">crie um</a> agora!";
?>