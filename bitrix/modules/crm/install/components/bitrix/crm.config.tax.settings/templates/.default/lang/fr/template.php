<?
$MESS["CRM_TAX_VAT_HINT1"] = "Le taux d'impôt est calculé pour chaque produit du document séparément.";
$MESS["CRM_TAX_VAT_HINT"] = "Le montant de la taxe est calculé pour chaque marchandise de la facture individuellement.";
$MESS["CRM_TAX_TAX_HINT1"] = "La taxe est calculé à partir du montant du document. Le taux d'imposition dépend de l'emplacement du client.";
$MESS["CRM_TAX_TAX_HINT"] = "La valeur de la taxe est calculée sur la base de la somme de la facture. Le taux de taxe dépend de l'emplacement du client.";
$MESS["CRM_TAX_SETTINGS_CHOOSE"] = "Choisissez le type des impôts";
$MESS["CRM_TAX_TAX1"] = "Avis d'imposition";
$MESS["CRM_TAX_TAX"] = "Emplacements (AJAX)";
$MESS["CRM_TAX_SETTINGS_TITLE"] = "Réglages d'impôts";
$MESS["CRM_TAX_VAT"] = "TVA";
$MESS["CRM_TAX_SETTINGS_SAVE_BUTTON"] = "Sauvegarder";
?>