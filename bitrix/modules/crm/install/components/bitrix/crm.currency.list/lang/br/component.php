<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_NAME"] = "Nome";
$MESS["CRM_COLUMN_EXCH_RATE"] = "Taxa de câmbio";
$MESS["CRM_COLUMN_ACCOUNTING"] = "Relatando moeda";
$MESS["CRM_COLUMN_SORT"] = "Ordenar";
$MESS["CRM_CURRENCY_DELETION_GENERAL_ERROR"] = "Erro ao excluir a moeda.";
$MESS["CRM_CURRENCY_UPDATE_GENERAL_ERROR"] = "Erro ao atualizar a moeda.";
$MESS["CRM_COLUMN_INVOICE_DEF"] = "Moeda padrão das faturas";
$MESS["CRM_COLUMN_AMOUNT_CNT"] = "Valor nominal";
$MESS["CRM_COLUMN_CURRENCY_LIST_BASE"] = "Base";
$MESS["CRM_CURRENCY_MARK_AS_BASE_GENERAL_ERROR"] = "Erro ao configurar a moeda base.";
?>