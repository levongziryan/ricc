<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_CURRENCY_DELETE_CONFIRM"] = "Tes-vous sûr de vouloir supprimer '%s'?";
$MESS["CRM_CURRENCY_SHOW_TITLE"] = "Allez à la page d'affichage de cette devise";
$MESS["CRM_CURRENCY_EDIT_TITLE"] = "Aller à la page d'édition de cette devise";
$MESS["CRM_CURRENCY_SHOW"] = "Aller voir les devises";
$MESS["CRM_CURRENCY_EDIT"] = "Modifier la devise";
$MESS["CRM_CURRENCY_DELETE"] = "Supprimer des devises";
$MESS["CRM_CURRENCY_DELETE_TITLE"] = "Supprimer cette devise";
$MESS["CRM_CURRENCY_SET_AS_BASE_TITLE"] = "Assurez-monnaie de base";
$MESS["CRM_CURRENCY_SET_AS_BASE"] = "Assurez-monnaie de base";
?>