<?
$MESS["CRM_CURRENCY_SHOW_TITLE"] = "Mostrar tipo de cambio";
$MESS["CRM_CURRENCY_SHOW"] = "Ver tipo de cambio";
$MESS["CRM_CURRENCY_EDIT_TITLE"] = "Editar tipo de moneda";
$MESS["CRM_CURRENCY_EDIT"] = "Editar tipo de cambio";
$MESS["CRM_CURRENCY_DELETE_TITLE"] = "Eliminar tipo de moneda";
$MESS["CRM_CURRENCY_DELETE"] = "Eliminar moneda";
$MESS["CRM_CURRENCY_DELETE_CONFIRM"] = "¿Está seguro que desea eliminar '%s'?";
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_CURRENCY_SET_AS_BASE_TITLE"] = "Marcar moneda base";
$MESS["CRM_CURRENCY_SET_AS_BASE"] = "Marcar moneda base";
?>