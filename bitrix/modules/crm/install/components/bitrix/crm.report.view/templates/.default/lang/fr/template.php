<?
$MESS["CRM_FF_LEAD"] = "Prospects";
$MESS["CRM_FF_CONTACT"] = "Contacts";
$MESS["CRM_FF_COMPANY"] = "Entreprises";
$MESS["CRM_FF_DEAL"] = "Transactions";
$MESS["CRM_FF_OK"] = "Choisir";
$MESS["CRM_FF_CANCEL"] = "Annuler";
$MESS["CRM_FF_CLOSE"] = "Fermer";
$MESS["CRM_FF_SEARCH"] = "Recherche";
$MESS["CRM_FF_NO_RESULT"] = "Malheureusement, il n'y a pas d'éléments retrouvés d'après votre demande de recherche.";
$MESS["CRM_FF_CHOISE"] = "Choisir";
$MESS["CRM_FF_CHANGE"] = "Editer";
$MESS["CRM_FF_LAST"] = "Dernier";
$MESS["CRM_REPORT_CURRENCY_INFO"] = "Devise du rapport: #CURRENCY#";
$MESS["CRM_REPORT_INCLUDE_ALL"] = "Tous";
?>