<?
$MESS["CRM_BP_TOOLBAR_ADD"] = "Adicionar modelo";
$MESS["CRM_BP_TOOLBAR_ADD_TITLE"] = "Adicionar novo modelo";
$MESS["CRM_BP_TOOLBAR_TYPES"] = "Tipos";
$MESS["CRM_BP_TOOLBAR_TYPES_TITLE"] = "Tipos disponíveis";
$MESS["CRM_BP_LIST_NAME"] = "Nome";
$MESS["CRM_BP_LIST_DATE_MODIFY"] = "Modificada em";
$MESS["CRM_BP_LIST_MODIFIED_BY"] = "Modificado por";
$MESS["CRM_BP_LIST_AUTOSTART"] = "Auto executar";
?>