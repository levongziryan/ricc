<?
$MESS["CRM_TITLE_LEAD"] = "Prospect";
$MESS["CRM_FIELD_OPPORTUNITY"] = "Montant possible de la transaction";
$MESS["CRM_FIELD_STATUS"] = "Statut";
$MESS["CRM_FIELD_CONTACT_FULL_NAME"] = "Contact";
$MESS["CRM_CONTACT_INFO_PHONE"] = "Téléphone";
$MESS["CRM_CONTACT_INFO_EMAIL"] = "E-mail";
$MESS["CRM_CONTACT_INFO_IM"] = "Messagerie";
$MESS["CRM_CONTACT_INFO_WEB"] = "Site web";
$MESS["CRM_FIELD_ASSIGNED_BY"] = "Responsable";
?>