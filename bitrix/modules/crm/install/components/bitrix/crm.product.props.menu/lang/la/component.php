<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PRODUCTPROP_ADD"] = "Agregar";
$MESS["CRM_IBLOCK_MODULE_NOT_INSTALLED"] = "El módulo Information Blocks no está instalado.";
$MESS["CRM_PRODUCTPROP_ADD_TITLE"] = "Crear una nueva propiedad del producto";
$MESS["CRM_PRODUCTPROP_LIST"] = "Propiedades";
$MESS["CRM_PRODUCTPROP_LIST_TITLE"] = "Ver todas las propiedades del producto";
?>