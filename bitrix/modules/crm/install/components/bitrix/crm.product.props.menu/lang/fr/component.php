<?
$MESS["CRM_PRODUCTPROP_ADD"] = "Ajouter";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_IBLOCK_MODULE_NOT_INSTALLED"] = "Le module des blocs d'informations n'a pas été installé.";
$MESS["CRM_PRODUCTPROP_LIST"] = "Propriétés";
$MESS["CRM_PRODUCTPROP_ADD_TITLE"] = "Créer une nouvelle propriété du produit";
$MESS["CRM_PRODUCTPROP_LIST_TITLE"] = "Voir toutes les propriétés de ce produit";
?>