<?
$MESS["CRM_LOC_IMP_CHOOSE_FILE"] = "Sélectionner l'emplacement des fichiers";
$MESS["CRM_LOC_IMP_STEP_LENGTH"] = "Durée d'un pas (sec)";
$MESS["CRM_LOC_IMP_STEP_CHECK"] = "La durée d'un pas ne peut être qu'un nombre positif";
$MESS["CRM_LOC_IMP_LOAD_ZIP"] = "charger la base des codes ZIP";
$MESS["CRM_LOC_IMP_FILE_FFILE"] = "charger à partir du fichier";
$MESS["CRM_LOC_IMP_JS_FILE_PROCESS"] = "Chargement du fichier...";
$MESS["CRM_CLOSE_BUTTON"] = "Fermer";
$MESS["CRM_LOC_IMP_JS_IMPORT_PROCESS"] = "Importer les données...";
$MESS["CRM_LOC_IMP_TITLE"] = "Emplacements importation";
$MESS["CRM_LOC_IMP_JS_IMPORT_SUCESS"] = "Importation terminée avec succès.";
$MESS["CRM_IMPORT_BUTTON"] = "Charger";
$MESS["CRM_LOC_IMP_FILE_CNTR"] = "Monde (pays)";
$MESS["CRM_LOC_IMP_FILE_NONE"] = "aucun";
$MESS["CRM_CANCEL_BUTTON"] = "Annuler";
$MESS["CRM_LOC_IMP_JS_ERROR"] = "Erreur";
$MESS["CRM_LOC_IMP_SYNC_Y"] = "essayer de synchroniser les données existantes avec celles nouvelles";
$MESS["CRM_LOC_IMP_FILE_RS"] = "La Russie et la CEI (pays et villes)";
$MESS["CRM_LOC_IMP_SYNC"] = "Synchronisation";
$MESS["CRM_LOC_IMP_FILE_USA"] = "USA (villes)";
$MESS["CRM_LOC_IMP_SYNC_NO_ZIP"] = "Seulement pour la Russie. <br> La synchronisation de la base des ZIP-codes n'est pas assurée.";
$MESS["CRM_LOC_IMP_SYNC_N"] = "supprimer les données anciennes";
$MESS["CRM_LOC_IMP_STEP_LENGTH_HINT"] = "Indiquez une durée souhaitée de l'étape du traitement des données. Modifiez ce paramètre uniquement si vous êtes sûr que la page va réussir de traiter pendant le temps indiqué.";
?>