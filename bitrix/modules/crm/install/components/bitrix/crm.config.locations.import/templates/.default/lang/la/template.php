<?
$MESS["CRM_LOC_IMP_TITLE"] = "Importar ubicación";
$MESS["CRM_IMPORT_BUTTON"] = "Importar";
$MESS["CRM_LOC_IMP_CHOOSE_FILE"] = "Seleccione el archivo de definición de ubicación";
$MESS["CRM_LOC_IMP_FILE_RS"] = "Russia y ex-USSR (ciudad)";
$MESS["CRM_LOC_IMP_FILE_USA"] = "USA (ciudad)";
$MESS["CRM_LOC_IMP_FILE_CNTR"] = "Mundial (países)";
$MESS["CRM_LOC_IMP_FILE_FFILE"] = "cargar el archivo";
$MESS["CRM_LOC_IMP_FILE_NONE"] = "ninguno";
$MESS["CRM_LOC_IMP_LOAD_ZIP"] = "Cargar base de datos de códigos postales";
$MESS["CRM_LOC_IMP_SYNC"] = "Sincronización";
$MESS["CRM_LOC_IMP_SYNC_Y"] = "Intente sincronizar datos nuevos y existentes";
$MESS["CRM_LOC_IMP_SYNC_N"] = "eliminar datos antiguos";
$MESS["CRM_LOC_IMP_STEP_LENGTH"] = "La longitud del paso (seg)";
$MESS["CRM_LOC_IMP_STEP_LENGTH_HINT"] = "Especificar la longitud del paso, en cuestión de segundos. No cambie este parámetro si no está absolutamente seguro de que el valor que se va a introducir es suficiente y el script no se terminará antes de tiempo.";
$MESS["CRM_CLOSE_BUTTON"] = "Cerrar ";
$MESS["CRM_CANCEL_BUTTON"] = "Cancelar";
$MESS["CRM_LOC_IMP_STEP_CHECK"] = "La longitud del paso no puede ser cero o negativo.";
$MESS["CRM_LOC_IMP_JS_ERROR"] = "Error";
$MESS["CRM_LOC_IMP_JS_IMPORT_SUCESS"] = "La importación de datos se ha completado correctamente.";
$MESS["CRM_LOC_IMP_JS_IMPORT_PROCESS"] = "Importación de datos ...";
$MESS["CRM_LOC_IMP_JS_FILE_PROCESS"] = "Cargando archivo...";
$MESS["CRM_LOC_IMP_SYNC_NO_ZIP"] = "Sólo se sincronizarán los códigos postales de Rusia.";
?>