<?
$MESS["CRM_LOC_IMP_LOAD_ACCESS_DENIED"] = "Acceso denegado";
$MESS["CRM_LOC_IMP_GFILE_ERROR"] = "No carga el archivo seleccionado.";
$MESS["CRM_LOC_IMP_NO_LOC_FILE"] = "No se ha cargado la ubicación del archivo.";
$MESS["CRM_LOC_IMP_STEP_LENGTH_ERROR"] = "La longitud del paso es requerida.";
?>