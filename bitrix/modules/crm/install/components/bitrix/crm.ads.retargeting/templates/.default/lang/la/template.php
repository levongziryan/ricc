<?
$MESS["CRM_ADS_RTG_REFRESH"] = "Actualizar";
$MESS["CRM_ADS_RTG_REFRESH_TEXT"] = "Actualizar el público objetivo.";
$MESS["CRM_ADS_RTG_REFRESH_TEXT_YANDEX"] = "Actualizar segmentos de destino.";
$MESS["CRM_ADS_RTG_ERROR_ACTION"] = "Se ha cancelado la acción porque se ha producido un error.";
$MESS["CRM_ADS_RTG_CLOSE"] = "Cerrar";
$MESS["CRM_ADS_RTG_APPLY"] = "Ejecutar";
$MESS["CRM_ADS_RTG_CANCEL"] = "Cancelar";
$MESS["CRM_ADS_RTG_LOGIN"] = "Conectar";
$MESS["CRM_ADS_RTG_LOGOUT"] = "Desconectar";
$MESS["CRM_ADS_RTG_CABINET_FACEBOOK"] = "Cuenta de publicidad de Facebook";
$MESS["CRM_ADS_RTG_CABINET_YANDEX"] = "Cuenta de publicidad de Yandex.Audience";
$MESS["CRM_ADS_RTG_CABINET_VKONTAKTE"] = "Cuenta de publicidad de VK";
$MESS["CRM_ADS_RTG_CABINET_GOOGLE"] = "Cuenta de publicidad de Google AdWords";
$MESS["CRM_ADS_RTG_TITLE"] = "Configurar Público Objetivo";
$MESS["CRM_ADS_RTG_SELECT_ACCOUNT"] = "Seleccionar cuenta de publicidad";
$MESS["CRM_ADS_RTG_SELECT_AUDIENCE"] = "Agregar a audiencia";
$MESS["CRM_ADS_RTG_SELECT_CONTACT_DATA"] = "Agregar teléfono y correo electrónico a segmentos";
$MESS["CRM_ADS_RTG_SELECT_CONTACT_DATA_EMAIL"] = "e-mail";
$MESS["CRM_ADS_RTG_SELECT_CONTACT_DATA_PHONE"] = "teléfono";
$MESS["CRM_ADS_RTG_ERROR_NO_AUDIENCES"] = "No se ha encontrado público. Por favor, proceda a %name% crear una audiencia.";
$MESS["CRM_ADS_RTG_AUTO_REMOVE_TITLE"] = "eliminar de la audiencia en";
$MESS["CRM_ADS_RTG_AUTO_REMOVE_TITLE_YANDEX"] = "eliminar de segmentos en";
$MESS["CRM_ADS_RTG_AUTO_REMOVE_DAYS"] = "días";
$MESS["CRM_ADS_RTG_ADD_AUDIENCE"] = "Crear audiencia";
?>