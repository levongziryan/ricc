<?
$MESS["CRM_QUOTE_VAR"] = "ID de la cotización y nombre de la variable";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "Ruta a la plantilla de la página de index";
$MESS["CRM_SEF_PATH_TO_LIST"] = "Ruta a la plantilla de la página de las cotizaciones";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "Ruta a la plantilla de la página de edición de la cotización";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "Ruta a la plantilla de la página de visualización de la cotización";
$MESS["CRM_SEF_PATH_TO_IMPORT"] = "Ruta a la plantilla de la página importar ";
$MESS["CRM_ELEMENT_ID"] = "ID de la Cotización";
$MESS["CRM_NAME_TEMPLATE"] = "Formato de nombre";
?>