<?
$MESS["CRM_KANBAN_SUPERVISOR_Y"] = "Ativar o modo supervisor";
$MESS["CRM_KANBAN_SUPERVISOR_TITLE"] = "Use o modo supervisor para ver o trabalho de outros empregados em tempo real. O modo supervisor é desativado quando você sair.";
$MESS["CRM_KANBAN_SUPERVISOR_N"] = "Desativar o modo supervisor";
?>