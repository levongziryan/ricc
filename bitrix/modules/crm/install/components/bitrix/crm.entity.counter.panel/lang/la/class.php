<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "En módulo CRM no está instalado";
$MESS["CRM_COUNTER_ENTITY_TYPE_NOT_DEFINED"] = "El tipo de entidad no está especificado.";
$MESS["CRM_COUNTER_DEAL_CAPTION"] = "Negociaciones";
$MESS["CRM_COUNTER_LEAD_CAPTION"] = "Prospectos";
$MESS["CRM_COUNTER_CONTACT_CAPTION"] = "Contactos";
$MESS["CRM_COUNTER_COMPANY_CAPTION"] = "Compañías";
$MESS["CRM_COUNTER_DEAL_STUB"] = "No hay negociaciones que requieran atención inmediata.";
$MESS["CRM_COUNTER_LEAD_STUB"] = "No hay prospectos que requieran atención inmediata.";
$MESS["CRM_COUNTER_CONTACT_STUB"] = "No hay contactos que requieran atención inmediata.";
$MESS["CRM_COUNTER_COMPANY_STUB"] = "No hay compañías que requieran atención inmediata.";
?>