<?
$MESS["CRM_CONFIGS_LINK_TEXT"] = "Afficher les paramètres du CRM";
$MESS["CRM_CONFIGS_LINK_TITLE"] = "Ouvrir le formulaire des paramètres du CRM";
$MESS["CRM_TRACKER_SECTION_TITLE"] = "Préréglages";
$MESS["CRM_TRACKER_SECTION_NAME"] = "Configurer le tracker";
$MESS["CRM_BUTTON_SAVE_TITLE"] = "Enregistrer";
$MESS["CRM_BUTTON_CANCEL_TITLE"] = "Annuler";
$MESS["CRM_TRACKER_SETTINGS_COMPANY"] = "Pour entreprise";
$MESS["CRM_TRACKER_SETTINGS_CONTACT"] = "Pour contact";
?>