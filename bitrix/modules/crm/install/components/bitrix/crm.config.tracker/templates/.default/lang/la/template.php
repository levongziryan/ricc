<?
$MESS["CRM_CONFIGS_LINK_TEXT"] = "Ver configuración del CRM";
$MESS["CRM_CONFIGS_LINK_TITLE"] = "Abrir el formulario de configuración del CRM";
$MESS["CRM_TRACKER_SECTION_TITLE"] = "Preestablecer";
$MESS["CRM_TRACKER_SECTION_NAME"] = "Configurar seguimiento";
$MESS["CRM_BUTTON_SAVE_TITLE"] = "Guardar";
$MESS["CRM_BUTTON_CANCEL_TITLE"] = "Cancelar";
$MESS["CRM_TRACKER_SETTINGS_COMPANY"] = "Para la compañia";
$MESS["CRM_TRACKER_SETTINGS_CONTACT"] = "Para el contacto";
?>