<?
$MESS["CRM_FIELDS_EMPTY"] = "Estão faltando alguns dos campos requeridos.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "O módulo Moeda não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
?>