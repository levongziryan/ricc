<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_VAT_DELETE_CONFIRM"] = "Tes-vous sûr de vouloir supprimer '%s'?";
$MESS["CRM_PRODUCT_ROW_TAX_UNIFORM_ALERT"] = "Si nécessaire, vous pouvez désactiver cette option, en vous assurant que cela ne contredit pas à la législation de votre pays.";
$MESS["CRM_PRODUCT_ROW_TAX_UNIFORM_TITLE"] = "L'impôt au produit peut être appliqué pour tout le document (pour chaque produit du document) ou désactivé pour tous les produits. Dans un seul document, les produits avec impôt et sans impôt ne peuvent pas exister en même temps.";
$MESS["CRM_VAT_SHOW_TITLE"] = "Passer à la page de consultation de ce taux de TVA";
$MESS["CRM_VAT_EDIT_TITLE"] = "Accéder à la page de l'édition de cet impôt";
$MESS["CRM_VAT_SHOW"] = "Passage à l'examen de la taxe";
$MESS["CRM_VAT_EDIT"] = "Editer le taux TVA";
$MESS["CRM_VAT_DELETE"] = "Elimination du taux de TVA";
$MESS["CRM_VAT_DELETE_TITLE"] = "Supprimer ce pari";
?>