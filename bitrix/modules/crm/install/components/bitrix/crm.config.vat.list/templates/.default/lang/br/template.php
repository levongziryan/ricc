<?
$MESS["CRM_VAT_SHOW_TITLE"] = "Ver este imposto";
$MESS["CRM_VAT_SHOW"] = "Ver imposto";
$MESS["CRM_VAT_EDIT_TITLE"] = "Abrir este imposto para edição";
$MESS["CRM_VAT_EDIT"] = "Editar IVA";
$MESS["CRM_VAT_DELETE_TITLE"] = "Deletar esta taxa de imposto";
$MESS["CRM_VAT_DELETE"] = "Deletar taxa de IVA";
$MESS["CRM_VAT_DELETE_CONFIRM"] = "Você tem certeza que deseja deletar as '%s'?";
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_PRODUCT_ROW_TAX_UNIFORM_TITLE"] = "Imposto sobre bens pode ser aplicado a toda a lista, isto é, para cada item no documento, ou para nenhum deles. Dentro de um único documento, não pode haver itens sem esse imposto aplicado, bem como naqueles que impostos são aplicados.";
$MESS["CRM_PRODUCT_ROW_TAX_UNIFORM_ALERT"] = "Se necessário, você pode desativar esta opção. Tenha certeza que desativar essa opção não viola qualquer lei ou regulamento com o qual você é obrigado a cumprir.";
?>