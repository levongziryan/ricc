<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado. ";
$MESS["CRM_CATALOG_MODULE_NOT_INSTALLED"] = "O módulo de Catálogo Comercial não está instalado. ";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso Negado";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_C_SORT"] = "Classificar";
$MESS["CRM_COLUMN_ACTIVE"] = "Ativar";
$MESS["CRM_COLUMN_NAME"] = "Nome";
$MESS["CRM_COLUMN_RATE"] = "Taxa";
$MESS["CRM_VAT_DELETION_GENERAL_ERROR"] = "Erro ao deletar o imposto.";
$MESS["CRM_VAT_UPDATE_GENERAL_ERROR"] = "Erro ao atualizar o imposto.";
?>