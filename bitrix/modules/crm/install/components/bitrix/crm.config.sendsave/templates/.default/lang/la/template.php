<?
$MESS["CRM_TAB_CONFIG"] = "Integración Enviar&Guardar";
$MESS["CRM_TAB_CONFIG_TITLE"] = "Parámetros de Integración Enviar&Guardar";
$MESS["CRM_TAB_CONFIG_TITLE_EDIT"] = "Enviar&Guardar parámetros de integración de correo electrónico";
$MESS["CRM_TAB_CONFIG_TITLE_CREATE"] = "Nueva integración de correo electrónico Enviar&Guardar";
$MESS["CRM_BUTTON_SAVE"] = "Guardar";
$MESS["CRM_BUTTON_SAVE_TITLE"] = "Guardar parámetros de integración";
$MESS["CRM_BUTTON_CANCEL"] = "Cancelar";
$MESS["CRM_BUTTON_CANCEL_TITLE"] = "No guardar los parámetros actuales";
$MESS["CRM_BUTTON_DELETE"] = "Eliminar";
$MESS["CRM_BUTTON_DELETE_TITLE"] = "Eliminar los parámetros actuales de integración";
?>