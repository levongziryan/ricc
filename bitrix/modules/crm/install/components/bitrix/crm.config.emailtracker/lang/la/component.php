<?
$MESS["MAIL_MODULE_NOT_INSTALLED"] = "El módulo Mail no está instalado.";
$MESS["INTR_MAIL_MAX_AGE_ERROR"] = "Especifique el intervalo de sincronización.";
$MESS["INTR_MAIL_IMAP_DIRS"] = "Seleccionar carpetas para sincronizar";
$MESS["INTR_MAIL_CSRF"] = "Error de seguridad al enviar el formulario.";
$MESS["INTR_MAIL_AUTH"] = "Error de autenticación";
$MESS["INTR_MAIL_FORM_ERROR"] = "Error al procesar el formulario.";
$MESS["INTR_MAIL_AJAX_ERROR"] = "Error al procesar la solicitud.";
$MESS["INTR_MAIL_SAVE_ERROR"] = "Error al guardar datos de conexión.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado.";
$MESS["INTR_MAIL_INP_EMAIL_BAD"] = "Dirección de correo electrónico no válida";
$MESS["INTR_MAIL_CRM_ALREADY"] = "Cuentas de correo electrónico del CRM ya estan configuradas";
$MESS["INTR_MAIL_CHECK_INTERVAL_2M"] = "2 minutos";
$MESS["INTR_MAIL_CHECK_INTERVAL_5M"] = "5 minutos";
$MESS["INTR_MAIL_CHECK_INTERVAL_10M"] = "10 minutos";
$MESS["INTR_MAIL_CHECK_INTERVAL_1H"] = "1 hora";
$MESS["INTR_MAIL_CHECK_INTERVAL_3H"] = "3 hora";
$MESS["INTR_MAIL_CHECK_INTERVAL_12H"] = "12 horas";
$MESS["INTR_MAIL_IMAP_AUTH_ERR_EXT"] = "Error de autenticación. Compruebe que el inicio de sesión y la contraseña son las correctas.<br>Tenga en cuenta que si utiliza contraseñas de aplicaciones y la autenticación de dos pasos está habilitada, debe utilizar una contraseña de integración especial";
$MESS["INTR_MAIL_IMAP_OAUTH_ACC"] = "Error al obtener los datos del buzón";
?>