<?
$MESS["CRM_LOC_DELETE_DLG_MESSAGE"] = "tes-vous sûr de vouloir supprimer cet emplacement?";
$MESS["CRM_LOC_ADD"] = "Ajouter";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_LOCATIONS_IMPORT"] = "Charger";
$MESS["CRM_LOCATIONS_IMPORT_TITLE"] = "Importer les emplacements";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "Le module 'Boutique en ligne' n'a pas été installé.";
$MESS["CRM_LOC_EDIT_TITLE"] = "Passer à l'édition de l'emplacement";
$MESS["CRM_LOC_ADD_TITLE"] = "Accéder à la création d'un nouvel emplacement";
$MESS["CRM_LOC_LIST_TITLE"] = "Accéder à la liste des localisations";
$MESS["CRM_LOC_EDIT"] = "Editer";
$MESS["CRM_LOC_LIST"] = "Emplacements";
$MESS["CRM_LOC_DELETE_DLG_TITLE"] = "Annuler la localisation";
$MESS["CRM_LOC_DELETE"] = "Supprimer";
$MESS["CRM_LOC_DELETE_TITLE"] = "Annuler la localisation";
$MESS["CRM_LOC_DELETE_DLG_BTNTITLE"] = "Annuler la localisation";
$MESS["CRM_LOC_STEP_UP"] = "Haut";
$MESS["CRM_LOC_STEP_UP_TITLE"] = "Un niveau au-dessus";
?>