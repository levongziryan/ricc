<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_LOC_ADD"] = "Agregar";
$MESS["CRM_LOC_ADD_TITLE"] = "Crear una nueva ubicación";
$MESS["CRM_LOC_EDIT"] = "Editar";
$MESS["CRM_LOC_EDIT_TITLE"] = "Abrir ubicación para su edición";
$MESS["CRM_LOC_DELETE"] = "Eliminar";
$MESS["CRM_LOC_DELETE_TITLE"] = "Eliminar ubicación";
$MESS["CRM_LOC_DELETE_DLG_TITLE"] = "Eliminar ubicación";
$MESS["CRM_LOC_DELETE_DLG_MESSAGE"] = "Está seguro de que desea eliminar esta ubicación?";
$MESS["CRM_LOC_DELETE_DLG_BTNTITLE"] = "Eliminar ubicación";
$MESS["CRM_LOC_LIST"] = "Ubicación";
$MESS["CRM_LOC_LIST_TITLE"] = "Ver todas las ubicaciones";
$MESS["CRM_LOCATIONS_IMPORT"] = "Importar";
$MESS["CRM_LOCATIONS_IMPORT_TITLE"] = "Importar ubicación";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "El módulo e-Store no está instalado.";
$MESS["CRM_LOC_STEP_UP"] = "Arriba";
$MESS["CRM_LOC_STEP_UP_TITLE"] = "Un nivel más arriba";
?>