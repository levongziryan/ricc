<?
$MESS["CRM_NAME"] = "CRM";
$MESS["CRM_CONFIG_NAME"] = "Configuraciones";
$MESS["CRM_FIELDS_TYPES_NAME"] = "Tipos de campo personalizados";
$MESS["CRM_FIELDS_TYPES_DESCRIPTION"] = "Visualiza los tipos de campos personalizados usados por el módulo CRM.";
?>