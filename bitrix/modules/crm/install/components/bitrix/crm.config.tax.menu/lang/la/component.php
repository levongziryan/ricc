<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_TAX_ADD"] = "Agregar impuesto";
$MESS["CRM_TAX_EDIT"] = "Editar";
$MESS["CRM_TAX_EDIT_TITLE"] = "Editar Impuesto";
$MESS["CRM_TAX_DELETE"] = "Eliminar impuesto";
$MESS["CRM_TAX_DELETE_TITLE"] = "Eliminar impuesto";
$MESS["CRM_TAX_DELETE_DLG_TITLE"] = "Eliminar impuesto";
$MESS["CRM_TAX_DELETE_DLG_BTNTITLE"] = "Eliminar impuesto";
$MESS["CRM_TAX_SHOW"] = "Ver";
$MESS["CRM_TAX_SHOW_TITLE"] = "Ver impuesto";
$MESS["CRM_TAX_LIST"] = "Impuestos";
$MESS["CRM_TAX_LIST_TITLE"] = "Mostrar todos los impuestos";
$MESS["CRM_TAX_SETTINGS"] = "Configuración";
$MESS["CRM_TAX_SETTINGS_TITLE"] = "Configurar sistema de impuestos";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "El módulo e-Store no está instalado.";
$MESS["CRM_TAX_ADD_TITLE"] = "Crear un nuevo impuesto";
$MESS["CRM_TAX_DELETE_DLG_MESSAGE"] = "Está seguro que desea eliminar este impuesto?";
?>