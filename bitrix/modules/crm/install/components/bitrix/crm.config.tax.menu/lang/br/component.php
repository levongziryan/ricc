<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado. ";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_TAX_ADD"] = "Adicionar imposto";
$MESS["CRM_TAX_ADD_TITLE"] = "Criar um novo imposto";
$MESS["CRM_TAX_EDIT"] = "Editar";
$MESS["CRM_TAX_EDIT_TITLE"] = "Editar imposto";
$MESS["CRM_TAX_DELETE"] = "Deletar imposto";
$MESS["CRM_TAX_DELETE_TITLE"] = "Deletar imposto";
$MESS["CRM_TAX_DELETE_DLG_TITLE"] = "Deletar imposto";
$MESS["CRM_TAX_DELETE_DLG_MESSAGE"] = "Você tem certeza de que deseja deletar este imposto?";
$MESS["CRM_TAX_DELETE_DLG_BTNTITLE"] = "Deletar imposto";
$MESS["CRM_TAX_SHOW"] = "Ver";
$MESS["CRM_TAX_SHOW_TITLE"] = "Ver imposto";
$MESS["CRM_TAX_LIST"] = "Impostos";
$MESS["CRM_TAX_LIST_TITLE"] = "Ver todos os impostos";
$MESS["CRM_TAX_SETTINGS"] = "Configurações";
$MESS["CRM_TAX_SETTINGS_TITLE"] = "Configurar sistema de impostos";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "O módulo de e-Store não está instalado. ";
?>