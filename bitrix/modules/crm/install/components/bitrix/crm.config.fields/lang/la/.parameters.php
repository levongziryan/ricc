<?
$MESS["CRM_FIELD_CX_MODE"] = "ID del módulo";
$MESS["CRM_FIELD_CX_ENTITY_ID"] = "ID del tipo";
$MESS["CRM_FIELD_CX_FIELD_ID"] = "ID del campo";
$MESS["CRM_FIELD_CX_ENTITY_LIST"] = "URL de los tipos de lista";
$MESS["CRM_FIELD_CX_FIELDS_LIST"] = "URL de las listas de campos";
$MESS["CRM_FIELD_CX_FIELD_EDIT"] = "URL del editor del campo";
?>