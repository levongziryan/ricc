<?
$MESS["CRM_TAB_1"] = "Importar Configuraciones";
$MESS["CRM_TAB_1_TITLE"] = "Editar configuraciones de la edición";
$MESS["CRM_TAB_2"] = "Campos";
$MESS["CRM_TAB_2_TITLE"] = "Configurar mapeo del campo";
$MESS["CRM_TAB_3"] = "Importar";
$MESS["CRM_TAB_3_TITLE"] = "Resultado de la importación";
$MESS["CRM_IMPORT_NEXT_STEP"] = "Siguiente >>";
$MESS["CRM_IMPORT_NEXT_STEP_TITLE"] = "Ir al siguiente paso";
$MESS["CRM_IMPORT_PREVIOUS_STEP"] = "<< Regresar";
$MESS["CRM_IMPORT_PREVIOUS_STEP_TITLE"] = "Ir al paso anterior";
$MESS["CRM_IMPORT_DONE"] = "Listo";
$MESS["CRM_IMPORT_DONE_TITLE"] = "Ver negociaciones";
$MESS["CRM_IMPORT_CANCEL"] = "Cancelar";
$MESS["CRM_IMPORT_CANCEL_TITLE"] = "Abandonar y regresar a la lista de negociaciones";
$MESS["CRM_IMPORT_AGAIN"] = "Importar oro archivo";
$MESS["CRM_IMPORT_AGAIN_TITLE"] = "Click para importar el archivo";
?>