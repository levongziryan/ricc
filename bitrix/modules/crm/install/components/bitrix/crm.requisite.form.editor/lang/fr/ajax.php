<?
$MESS["CRM_REQUISITE_FORM_EDITOR_AJAX_ERROR_REQUIRED_PARAMETER"] = "Le paramètre #PARAM# est requis mais manquant.";
$MESS["CRM_REQUISITE_FORM_EDITOR_AJAX_ERROR_UNKNOWN_ACTION"] = "Action inconnue : #ACTION#.";
$MESS["CRM_REQUISITE_FORM_EDITOR_AJAX_ERROR_EMPTY_ACTION"] = "Aucune action sélectionnée.";
$MESS["CRM_REQUISITE_FORM_EDITOR_AJAX_ERROR_REQUISITE_NOT_FOUND"] = "Données de contact ou de la société introuvables (ID = #ID#)";
$MESS["CRM_REQUISITE_FORM_EDITOR_AJAX_ERROR_REQUISITE_DELETE"] = "Impossible de supprimer les données de contact ou de la société (ID = #ID#)";
?>