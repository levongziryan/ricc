<?
$MESS["CRM_ENT_DETAIL_MAIN_TAB"] = "Geral";
$MESS["CRM_ENT_DETAIL_COMPANY_URL_COPIED"] = "O link da empresa foi copiado para a Área de Transferência";
$MESS["CRM_ENT_DETAIL_CONTACT_URL_COPIED"] = "O link do contato foi copiado para a Área de Transferência";
$MESS["CRM_ENT_DETAIL_COPY_COMPANY_URL"] = "Copiar link da empresa na Área de Transferência";
$MESS["CRM_ENT_DETAIL_COPY_CONTACT_URL"] = "Copiar link do contato na Área de Transferência";
$MESS["CRM_ENT_DETAIL_COPY_DEAL_URL"] = "Copiar link da venda na Área de Transferência";
$MESS["CRM_ENT_DETAIL_COPY_LEAD_URL"] = "Copiar link do cliente potencial na Área de Transferência";
$MESS["CRM_ENT_DETAIL_DEAL_URL_COPIED"] = "O link da venda foi copiado para a Área de Transferência";
$MESS["CRM_ENT_DETAIL_LEAD_URL_COPIED"] = "O link do cliente potencial foi copiado para a Área de Transferência";
?>