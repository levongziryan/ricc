<?
$MESS["CRM_ENT_DETAIL_MAIN_TAB"] = "Загальне";
$MESS["CRM_ENT_DETAIL_COMPANY_URL_COPIED"] = "Посилання на компанію скопійоване в буфер обміну";
$MESS["CRM_ENT_DETAIL_CONTACT_URL_COPIED"] = "Посилання на контакт скопійоване в буфер обміну";
$MESS["CRM_ENT_DETAIL_COPY_COMPANY_URL"] = "Скопіювати посилання на компанію в буфер обміну";
$MESS["CRM_ENT_DETAIL_COPY_CONTACT_URL"] = "Скопіювати посилання на контакт в буфер обміну";
$MESS["CRM_ENT_DETAIL_COPY_DEAL_URL"] = "Скопіювати посилання на угоду в буфер обміну";
$MESS["CRM_ENT_DETAIL_COPY_LEAD_URL"] = "Скопіювати посилання на лід в буфер обміну";
$MESS["CRM_ENT_DETAIL_DEAL_URL_COPIED"] = "Посилання на угоду скопійовано в буфер обміну";
$MESS["CRM_ENT_DETAIL_LEAD_URL_COPIED"] = "Посилання на лід скопійовано в буфер обміну";
$MESS["CRM_ENT_DETAIL_COPY_DEAL_RECURRING_URL"] = "Копіювати посилання на постійну угоду в буфер обміну";
$MESS["CRM_ENT_DETAIL_DEAL_RECURRING_URL_COPIED"] = "Посилання на постійну угоду скопійовано в буфер обміну";
$MESS["CRM_ENT_DETAIL_DEAL_DELETE_DIALOG_TITLE"] = "Видалення угоди";
$MESS["CRM_ENT_DETAIL_LEAD_DELETE_DIALOG_TITLE"] = "Видалення ліда";
$MESS["CRM_ENT_DETAIL_CONTACT_DELETE_DIALOG_TITLE"] = "Видалення контакту";
$MESS["CRM_ENT_DETAIL_COMPANY_DELETE_DIALOG_TITLE"] = "Видалення компанії";
$MESS["CRM_ENT_DETAIL_DEAL_DELETE_DIALOG_MESSAGE"] = "Ви впевнені, що бажаєте видалити цю угоду?";
$MESS["CRM_ENT_DETAIL_LEAD_DELETE_DIALOG_MESSAGE"] = "Ви впевнені, що бажаєте видалити цей лід?";
$MESS["CRM_ENT_DETAIL_CONTACT_DELETE_DIALOG_MESSAGE"] = "Ви впевнені, що бажаєте видалити цей контакт?";
$MESS["CRM_ENT_DETAIL_COMPANY_DELETE_DIALOG_MESSAGE"] = "Ви впевнені, що бажаєте видалити цю компанію?";
$MESS["CRM_ENT_DETAIL_REST_BUTTON"] = "Застосунки";
?>