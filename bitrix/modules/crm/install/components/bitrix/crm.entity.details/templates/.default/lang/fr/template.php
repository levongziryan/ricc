<?
$MESS["CRM_ENT_DETAIL_MAIN_TAB"] = "Général";
$MESS["CRM_ENT_DETAIL_COMPANY_URL_COPIED"] = "Le lien de la société a été copié dans le Presse-papiers";
$MESS["CRM_ENT_DETAIL_CONTACT_URL_COPIED"] = "Le lien du contact a été copié dans le Presse-papiers";
$MESS["CRM_ENT_DETAIL_COPY_COMPANY_URL"] = "Copier le lien de la société dans le Presse-papiers";
$MESS["CRM_ENT_DETAIL_COPY_CONTACT_URL"] = "Copier le lien du contact dans le Presse-papiers";
$MESS["CRM_ENT_DETAIL_COPY_DEAL_URL"] = "Copier le lien de la transaction dans le Presse-papiers";
$MESS["CRM_ENT_DETAIL_COPY_LEAD_URL"] = "Copier le lien du lead dans le Presse-papiers";
$MESS["CRM_ENT_DETAIL_DEAL_URL_COPIED"] = "Le lien de la transaction a été copié dans le Presse-papiers";
$MESS["CRM_ENT_DETAIL_LEAD_URL_COPIED"] = "Le lien du lead a été copié dans le Presse-papiers";
$MESS["CRM_ENT_DETAIL_COPY_DEAL_RECURRING_URL"] = "Copier l'URL de la transaction récurrente dans le presse-papiers";
$MESS["CRM_ENT_DETAIL_DEAL_RECURRING_URL_COPIED"] = "L'URL de la transaction récurrence a été copiée dans le presse-papiers";
$MESS["CRM_ENT_DETAIL_DEAL_DELETE_DIALOG_TITLE"] = "Supprimer la transaction";
$MESS["CRM_ENT_DETAIL_LEAD_DELETE_DIALOG_TITLE"] = "Supprimer la piste";
$MESS["CRM_ENT_DETAIL_CONTACT_DELETE_DIALOG_TITLE"] = "Supprimer le contact";
$MESS["CRM_ENT_DETAIL_COMPANY_DELETE_DIALOG_TITLE"] = "Supprimer la société";
$MESS["CRM_ENT_DETAIL_DEAL_DELETE_DIALOG_MESSAGE"] = "Voulez-vous vraiment supprimer cette transaction ?";
$MESS["CRM_ENT_DETAIL_LEAD_DELETE_DIALOG_MESSAGE"] = "Voulez-vous vraiment supprimer cette piste ?";
$MESS["CRM_ENT_DETAIL_CONTACT_DELETE_DIALOG_MESSAGE"] = "Voulez-vous vraiment supprimer ce contact ?";
$MESS["CRM_ENT_DETAIL_COMPANY_DELETE_DIALOG_MESSAGE"] = "Voulez-vous vraiment supprimer cette société ?";
?>