<?
$MESS["CRM_FIELD_OPPORTUNITY"] = "Importe";
$MESS["CRM_FIELD_STATUS"] = "Estado";
$MESS["CRM_FIELD_ASSIGNED_BY"] = "Persona responsable";
$MESS["CRM_FIELD_DATE_BILL"] = "Fecha de factura";
$MESS["DATE_PAY_BEFORE"] = "Pagar antes";
$MESS["CRM_FIELD_DEAL"] = "Negociación";
$MESS["CRM_FIELD_QUOTE"] = "Cotización";
$MESS["CRM_TITLE_INVOICE"] = "Factura";
?>