<?
$MESS["CRM_ACTIVITY_CALL_DOWNLOAD_RECORD"] = "descargar grabación de llamadas";
$MESS["CRM_ACTIVITY_CALL_DURATION"] = "Duración";
$MESS["CRM_ACTIVITY_CALL_VOTE"] = "Valoración";
$MESS["CRM_ACTIVITY_CALL_TYPE"] = "Tipo de llamada";
$MESS["CRM_ACTIVITY_CALL_FAILED"] = "Error en la llamada";
$MESS["CRM_ACTIVITY_CALL_DESCRIPTION"] = "Descripción";
$MESS["CRM_ACTIVITY_CALL_COMMENT"] = "Comentario";
?>