<?
$MESS["CRM_ACTIVITY_CALL_DOWNLOAD_RECORD"] = "télécharger l'enregistrement d'appel";
$MESS["CRM_ACTIVITY_CALL_DURATION"] = "Durée";
$MESS["CRM_ACTIVITY_CALL_VOTE"] = "Évaluation";
$MESS["CRM_ACTIVITY_CALL_TYPE"] = "Type d'appel";
$MESS["CRM_ACTIVITY_CALL_FAILED"] = "Appel échoué";
$MESS["CRM_ACTIVITY_CALL_DESCRIPTION"] = "Description";
$MESS["CRM_ACTIVITY_CALL_COMMENT"] = "Commentaire";
?>