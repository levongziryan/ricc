<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["VOXIMPLANT_MODULE_NOT_INSTALLED"] = "El módulo Telephony no está instalado.";
$MESS["CRM_ACTIVITY_CALL_VI_INCOMING_CALL"] = "Llamada entrante";
$MESS["CRM_ACTIVITY_CALL_VI_INCOMING_REDIRECT_CALL"] = "llamada entrante desviada ";
$MESS["CRM_ACTIVITY_CALL_VI_OUTGOING_CALL"] = "Llamada saliente";
$MESS["CRM_ACTIVITY_CALL_VI_CALLBACK_CALL"] = "Volver a llamar";
?>