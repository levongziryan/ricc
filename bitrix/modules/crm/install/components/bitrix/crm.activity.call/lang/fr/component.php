<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module GRC n'est pas installé.";
$MESS["VOXIMPLANT_MODULE_NOT_INSTALLED"] = "Le module Téléphonie n'est pas installé.";
$MESS["CRM_ACTIVITY_CALL_VI_INCOMING_CALL"] = "Appel entrant";
$MESS["CRM_ACTIVITY_CALL_VI_INCOMING_REDIRECT_CALL"] = "Appel transféré entrant";
$MESS["CRM_ACTIVITY_CALL_VI_OUTGOING_CALL"] = "Appel sortant";
$MESS["CRM_ACTIVITY_CALL_VI_CALLBACK_CALL"] = "Rappel";
?>