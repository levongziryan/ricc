<?
$MESS["CRM_LEAD_WGT_PAGE_TITLE"] = "Rapports analytiques sur les pistes";
$MESS["CRM_LEAD_WGT_FUNNEL"] = "Entonnoir de pistes";
$MESS["CRM_LEAD_WGT_QTY_LEAD_NEW"] = "Nombre de nouvelles pistes";
$MESS["CRM_LEAD_WGT_QTY_LEAD_IN_WORK"] = "Nombre de pistes actives";
$MESS["CRM_LEAD_WGT_QTY_LEAD_SUCCESSFUL"] = "Nombre de pistes converties";
$MESS["CRM_LEAD_WGT_QTY_LEAD_FAILED"] = "Nombre de mauvaises pistes";
$MESS["CRM_LEAD_WGT_QTY_LEAD_IDLE"] = "Nombre de pistes oisives";
$MESS["CRM_LEAD_WGT_EMPLOYEE_LEAD_PROC"] = "Efficacité du traitement des pistes par les employés";
$MESS["CRM_LEAD_WGT_QTY_ACTIVITY"] = "Nombre d'activités";
$MESS["CRM_LEAD_WGT_QTY_CALL"] = "Nombre d'appels";
$MESS["CRM_LEAD_WGT_DATE_NEW_LEAD"] = "Dynamiques de traitement des nouvelles pistes";
$MESS["CRM_LEAD_WGT_RATING"] = "Classement des conversions des pistes";
$MESS["CRM_LEAD_WGT_SOURCE"] = "Sources des pistes";
$MESS["CRM_LEAD_WGT_CONVERSION_SUCCESS"] = "Conversion";
$MESS["CRM_LEAD_WGT_CONVERSION_FAIL"] = "Échec";
$MESS["CRM_LEAD_WGT_DEMO_TITLE"] = "Ceci est un affichage démo. Masquez-le pour accéder à l'analytique de vos pistes.";
$MESS["CRM_LEAD_WGT_DEMO_CONTENT"] = "Vous n'avez pas encore de piste. <a href=\"#URL#\" class=\"#CLASS_NAME#\">Créez-en une</a> maintenant !";
?>