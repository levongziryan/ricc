<?
$MESS["CRM_LEAD_WGT_PAGE_TITLE"] = "Reportes analíticos de los prospectos";
$MESS["CRM_LEAD_WGT_QTY_LEAD_NEW"] = "Número de nuevos prospectos";
$MESS["CRM_LEAD_WGT_QTY_LEAD_IN_WORK"] = "Número de prospectos activos";
$MESS["CRM_LEAD_WGT_QTY_LEAD_SUCCESSFUL"] = "Número de prospectos convertidos";
$MESS["CRM_LEAD_WGT_QTY_LEAD_FAILED"] = "Número de prospectos no utiles";
$MESS["CRM_LEAD_WGT_QTY_LEAD_IDLE"] = "Número de prospectos inactivos";
$MESS["CRM_LEAD_WGT_QTY_ACTIVITY"] = "Número de actividades";
$MESS["CRM_LEAD_WGT_QTY_CALL"] = "Número de llamadas";
$MESS["CRM_LEAD_WGT_SOURCE"] = "Origen del prospecto";
$MESS["CRM_LEAD_WGT_CONVERSION_SUCCESS"] = "Conversión";
$MESS["CRM_LEAD_WGT_FUNNEL"] = "Embudo de prospectos";
$MESS["CRM_LEAD_WGT_EMPLOYEE_LEAD_PROC"] = "Eficiencia del procedimientos de los prospectos de los empleados";
$MESS["CRM_LEAD_WGT_DATE_NEW_LEAD"] = "Nueva dinámica de procesamiento de los prospectos";
$MESS["CRM_LEAD_WGT_RATING"] = "Tabla de clasificación de conversión de los prospectos";
$MESS["CRM_LEAD_WGT_CONVERSION_FAIL"] = "Perdido";
$MESS["CRM_LEAD_WGT_DEMO_TITLE"] = "Esta es una vista de demo. Ocultar para acceder al análisis de sus prospectos.";
$MESS["CRM_LEAD_WGT_DEMO_CONTENT"] = "Si todavía no tiene prospectos, <a href=\"#URL#\" class=\"#CLASS_NAME#\">crear uno</a> ahora mismo!";
?>