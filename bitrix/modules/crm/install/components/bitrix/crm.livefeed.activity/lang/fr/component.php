<?
$MESS["C_CRM_LFA_TASKS_TITLE_COMPLETE_M"] = "#USER_NAME# a terminé la tâche #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_COMPLETE"] = "#USER_NAME# a terminé la tâche #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_COMPLETE_F"] = "#USER_NAME# a terminé la tâche #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_CREATE_M"] = "#USER_NAME# a créé la tâche #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_CREATE"] = "#USER_NAME# a créé la tâche #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_CREATE_F"] = "#USER_NAME# a créé la tâche #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_MODIFY_M"] = "#USER_NAME# a modifié la tâche #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_MODIFY_F"] = "#USER_NAME# a modifié la tâche #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_MODIFY"] = "#USER_NAME# a modifié la tâche #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_STATUS_M"] = "#USER_NAME# a changé le statut de #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_STATUS_F"] = "#USER_NAME# a changé le statut de #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_STATUS"] = "#USER_NAME# a changé le statut de #TITLE#";
$MESS["C_CRM_LFA_TASKS_CHANGED_MESSAGE_24_2"] = "Date de modification";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_1_24"] = "La tâche a été reprise";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_2_24"] = "La tâche est en attente";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_3_24"] = "La tâche en cours de réalisation";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_4_24"] = "La tâche a été fermée";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_4_24_2"] = "Raison";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_4_24_CHANGES"] = "nécessite le contrôle de l'auteur";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_5_24"] = "La tâche a été fermée";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_6_24"] = "La tâche est reportée";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_7_24"] = "La tache est refusée";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_7_24_2"] = "Raison";
?>