<?
$MESS["CRM_ACTION_TYPE_MEETING"] = "Rendez-vous avec client";
$MESS["CRM_ACTION_TYPE_CALL_OUTGOING"] = "Appel au client";
$MESS["CRM_ACTION_TYPE_CALL_INCOMING"] = "Appel du client";
$MESS["CRM_ACTION_TYPE_EMAIL_OUTGOING"] = "Lettre au client";
$MESS["CRM_ACTION_TYPE_EMAIL_INCOMING"] = "Lettre du client";
$MESS["CRM_ACTION_TYPE_TASK"] = "La tâche";
$MESS["CRM_ACTION_END_TIME"] = "Date limite";
$MESS["CRM_ACTION_REFERENCE_LEAD"] = "Prospect";
$MESS["CRM_ACTION_REFERENCE_DEAL"] = "Affaire";
$MESS["CRM_ACTION_CUSTOMER"] = "Client";
$MESS["CRM_ACTION_EXPIRED"] = "périmé(e)s";
$MESS["CRM_ACTION_IMPORTANT"] = "Affaire importante";
$MESS["CRM_ACTION_GO_TO_FULL_VIEW"] = "Accéder à liste complète";
$MESS["CRM_ACTION_COMPLETED"] = "c'est achevé";
?>