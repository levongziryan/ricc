<?
$MESS["CRM_TITLE_EMAIL_SUBJECT"] = "Asunto";
$MESS["CRM_TITLE_EMAIL_FROM"] = "De";
$MESS["CRM_TITLE_EMAIL_TO"] = "Para";
$MESS["CRM_COMMUNICATION_TAB_LEAD"] = "Prospecto";
$MESS["CRM_COMMUNICATION_TAB_DEAL"] = "Negociación";
$MESS["CRM_COMMUNICATION_TAB_COMPANY"] = "Compañía";
$MESS["CRM_COMMUNICATION_TAB_CONTACT"] = "Contacto";
$MESS["CRM_ACTION_DEFAULT_SUBJECT"] = "Nueva actividad (#DATE#)";
$MESS["CRM_CALL_ACTION_DEFAULT_SUBJECT"] = "Nueva llamada telefonica (#DATE#)";
$MESS["CRM_MEETING_ACTION_DEFAULT_SUBJECT"] = "Nueva reunión (#DATE#)";
$MESS["CRM_EMAIL_ACTION_DEFAULT_SUBJECT"] = "Nuevo correo electrónico (#DATE#)";
$MESS["CRM_ACTIVITY_EMAIL_EMPTY_FROM_FIELD"] = "Por favor especificar el remitente del mensaje.";
$MESS["CRM_ACTIVITY_EMAIL_EMPTY_TO_FIELD"] = "Por favor especificar el destinatarios del mensaje.";
$MESS["CRM_ACTIVITY_INVALID_EMAIL"] = "'#VALUE#' no es una dirección de correo electrónico válida.";
$MESS["CRM_ACTIVITY_RESPONSIBLE_NOT_FOUND"] = "No se puede encontrar un usuario responsable de esta actividad.";
?>