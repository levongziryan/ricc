<?
$MESS["CRM_ACTIVITY_INVALID_EMAIL"] = "#VALUE#' n'est pas adresseemail correcte.";
$MESS["CRM_COMMUNICATION_TAB_COMPANY"] = "Entreprise";
$MESS["CRM_TITLE_EMAIL_TO"] = "Dans";
$MESS["CRM_COMMUNICATION_TAB_CONTACT"] = "Client";
$MESS["CRM_COMMUNICATION_TAB_LEAD"] = "Prospect";
$MESS["CRM_ACTIVITY_RESPONSIBLE_NOT_FOUND"] = "Impossible de trouver l'utilisateur responsable de l'action.";
$MESS["CRM_MEETING_ACTION_DEFAULT_SUBJECT"] = "Nouveau rendez-vous (#DATE#)";
$MESS["CRM_ACTION_DEFAULT_SUBJECT"] = "Nouvelle action (#DATE#)";
$MESS["CRM_EMAIL_ACTION_DEFAULT_SUBJECT"] = "Une nouvelle lettre (#DATE#)";
$MESS["CRM_CALL_ACTION_DEFAULT_SUBJECT"] = "Nouvel appel (#DATE#)";
$MESS["CRM_TITLE_EMAIL_FROM"] = "De qui";
$MESS["CRM_ACTIVITY_EMAIL_EMPTY_FROM_FIELD"] = "Veuillez indiquer l'expéditeur dans le champ 'de'.";
$MESS["CRM_ACTIVITY_EMAIL_EMPTY_TO_FIELD"] = "Veuillez indiquer le destinataire dans le champ 'à qui'.";
$MESS["CRM_COMMUNICATION_TAB_DEAL"] = "Affaire";
$MESS["CRM_TITLE_EMAIL_SUBJECT"] = "En-tête";
?>