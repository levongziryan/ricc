<?
$MESS["CRM_ENTITY_QPV_NOT_SELECTED"] = "non choisi";
$MESS["CRM_ENTITY_QPV_DELETE_CONTEXT_MENU_ITEM"] = "Supprimer";
$MESS["CRM_ENTITY_QPV_SIP_MGR_ENABLE_CALL_RECORDING"] = "Enregistrer la conversation";
$MESS["CRM_ENTITY_QPV_COMPANY_HEADER"] = "Entreprise";
$MESS["CRM_ENTITY_QPV_CONTACT_HEADER"] = "Client";
$MESS["CRM_ENTITY_QPV__SIP_MGR_MAKE_CALL"] = "Appel";
$MESS["CRM_ENTITY_QPV_INFO_DLG_BTN_CONTINUE"] = "Continuer";
$MESS["CRM_ENTITY_QPV_RESPONSIBLE_CHANGE"] = "changer";
$MESS["CRM_ENTITY_QPV_SAVE_FOR_ALL_MENU_ITEM"] = "Sauvegarder les réglages pour tous les utilisateurs";
$MESS["CRM_ENTITY_QPV_SUM_HEADER"] = "Total";
$MESS["CRM_ENTITY_QPV_DELETION_CONFIRMATION"] = "tes-vous sûr que vous voulez cacher ce domaine?";
$MESS["CRM_ENTITY_QPV_CONTROL_FIELD_DATA_NOT_SAVED"] = "La valeur de '#FIELD#' n'a pas été enregistré.";
$MESS["CRM_ENTITY_QPV_SIP_MGR_UNKNOWN_RECIPIENT"] = "Numéro non reconnu";
$MESS["CRM_ENTITY_QPV_DRAG_DROP_ERROR_TITLE"] = "Erreur de domaine en évolution";
$MESS["CRM_ENTITY_QPV_DRAG_DROP_ERROR_FIELD_NOT_SUPPORTED"] = "Désolé, déplacer ce domaine au panneau supérieur est pas encore supporté.";
$MESS["CRM_ENTITY_QPV_DRAG_DROP_ERROR_FIELD_ALREADY_EXISTS"] = "Ce champ est déjà contenue dans une colonne.";
$MESS["CRM_ENTITY_QPV_EDIT_CONTEXT_MENU_ITEM"] = "Editer";
$MESS["CRM_ENTITY_QPV_RESET_MENU_ITEM"] = "Effacer les paramètres";
$MESS["CRM_ENTITY_QPV_RESET_FOR_ALL_MENU_ITEM"] = "Réinitialiser les préférences pour tous les utilisateurs";
$MESS["CRM_ENTITY_QPV_DD_BIN_PROMPTING"] = "Pour supprimer un champ, faites-le glisser pour  <a id='#DEMO_BTN_ID#' href='#'>la corbeille sur le droit de la forme</a> ou cliquez sur une icône à côté du nom de domaine pour ouvrir un menu contextuel. <a id='#CLOSE_BTN_ID#' href='#'>Fermer</a>";
$MESS["CRM_ENTITY_QPV_CONTACT_NOT_SELECTED"] = "Aucun contact sélectionné";
$MESS["CRM_ENTITY_QPV_COMPANY_NOT_SELECTED"] = "Aucune société sélectionnée";
?>