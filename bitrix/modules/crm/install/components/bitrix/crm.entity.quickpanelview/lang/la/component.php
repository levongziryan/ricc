<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_ENTITY_QPV_ENTITY_TYPE_NAME_NOT_DEFINED"] = "No se especifica el tipo de entidad.";
$MESS["CRM_ENTITY_QPV_ENTITY_TYPE_NAME_NOT_SUPPORTED"] = "Tipo de entidad especificado no es compatible.";
$MESS["CRM_ENTITY_QPV_ENTITY_ID_NOT_DEFINED"] = "No se especifica el ID de entidad.";
$MESS["CRM_ENTITY_QPV_ENTITY_FIELDS_NOT_FOUND"] = "No puede encontrar entidad.";
$MESS["CRM_ENTITY_QPV_NOT_AUTHORIZED"] = "Usuario no conectado";
$MESS["CRM_ENTITY_QPV_NOT_SELECTED"] = "valor indefinido";
$MESS["CRM_ENTITY_QPV_DEAL_NOT_ASSIGNED"] = "sin asignar";
$MESS["CRM_ENTITY_QPV_QUOTE_NOT_ASSIGNED"] = "sin asignar";
$MESS["CRM_ENTITY_QPV_CLIENT_NOT_ASSIGNED"] = "sin asignar";
$MESS["CRM_ENTITY_QPV_LOCATION_NOT_ASSIGNED"] = "sin asignar";
$MESS["CRM_ENTITY_QPV_PAY_SYSTEM_NOT_ASSIGNED"] = "sin asignar";
$MESS["CRM_ENTITY_QPV_MULTI_FIELD_NOT_ASSIGNED"] = "no especificado";
$MESS["CRM_ENTITY_QPV_ACCESS_DENIED"] = "Acceso denegado";
$MESS["CRM_ENTITY_QPV_HIDDEN_CONTACT"] = "Ocultar contactos";
$MESS["CRM_ENTITY_QPV_HIDDEN_COMPANY"] = "Ocultar compañía";
?>