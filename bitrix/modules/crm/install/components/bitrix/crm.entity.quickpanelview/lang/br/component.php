<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["CRM_ENTITY_QPV_ENTITY_TYPE_NAME_NOT_DEFINED"] = "O tipo de entidade não está especificado.";
$MESS["CRM_ENTITY_QPV_ENTITY_TYPE_NAME_NOT_SUPPORTED"] = "Não há suporte para este tipo de entidade especificada.";
$MESS["CRM_ENTITY_QPV_ENTITY_ID_NOT_DEFINED"] = "O ID da entidade não está especificado.";
$MESS["CRM_ENTITY_QPV_ENTITY_FIELDS_NOT_FOUND"] = "Não foi possível encontrar a entidade.";
$MESS["CRM_ENTITY_QPV_NOT_AUTHORIZED"] = "O usuário não está conectado";
$MESS["CRM_ENTITY_QPV_NOT_SELECTED"] = "valor indefinido";
$MESS["CRM_ENTITY_QPV_DEAL_NOT_ASSIGNED"] = "não atribuido";
$MESS["CRM_ENTITY_QPV_QUOTE_NOT_ASSIGNED"] = "não atribuido";
$MESS["CRM_ENTITY_QPV_CLIENT_NOT_ASSIGNED"] = "não atribuido";
$MESS["CRM_ENTITY_QPV_LOCATION_NOT_ASSIGNED"] = "não atribuido";
$MESS["CRM_ENTITY_QPV_PAY_SYSTEM_NOT_ASSIGNED"] = "não atribuido";
$MESS["CRM_ENTITY_QPV_MULTI_FIELD_NOT_ASSIGNED"] = "não especificado";
$MESS["CRM_ENTITY_QPV_ACCESS_DENIED"] = "Acesso negado";
$MESS["CRM_ENTITY_QPV_HIDDEN_CONTACT"] = "Contato oculto";
$MESS["CRM_ENTITY_QPV_HIDDEN_COMPANY"] = "Empresa oculta";
?>