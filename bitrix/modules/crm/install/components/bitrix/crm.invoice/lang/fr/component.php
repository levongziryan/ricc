<?
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "Le module 'Catalogue de marchandises' n'a pas été installé.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Le module 'Boutique en ligne' n'a pas été installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "Le module ' Devises' est introuvable ! Installez-le s'il vous plaît.";
?>