<?
$MESS["CRM_INVOICE_WGT_PAGE_TITLE"] = "Rapports analytiques de facture";
$MESS["CRM_INVOICE_WGT_FUNNEL"] = "Entonnoir de facture";
$MESS["CRM_INVOICE_WGT_SUM_INVOICE_IN_WORK"] = "Montant total des factures actives";
$MESS["CRM_INVOICE_WGT_SUM_INVOICE_SUCCESSFUL"] = "Montant total des factures payées";
$MESS["CRM_INVOICE_WGT_SUM_INVOICE_OWED"] = "Montant total des factures en attente";
$MESS["CRM_INVOICE_WGT_QTY_INVOICE_OVERDUE"] = "Nombre de factures en retard";
$MESS["CRM_INVOICE_WGT_SUM_INVOICE_OVERDUE"] = "Montant total des factures en retard";
$MESS["CRM_INVOICE_WGT_DEAL_PAYMENT_CONTROL"] = "Contrôle des paiements des transactions gagnées";
$MESS["CRM_INVOICE_WGT_SUM_DEAL_INVOICE_OVERALL"] = "Montant des factures créées";
$MESS["CRM_INVOICE_WGT_SUM_DEAL_INVOICE_OWED"] = "Montant des factures non créées";
$MESS["CRM_INVOICE_WGT_INVOCE_PAYMENT"] = "Dettes et paiements";
$MESS["CRM_INVOICE_WGT_RATING"] = "Classement des paiements des factures";
$MESS["CRM_INVOICE_WGT_INVOCE_MANAGER"] = "Efficacité de la gestion des factures par les employés";
$MESS["CRM_INVOICE_WGT_DEMO_TITLE"] = "Ceci est un rapport de démonstration Masquez-le pour accéder à l'analytique de vos factures.";
$MESS["CRM_INVOICE_WGT_DEMO_CONTENT"] = "Vous n'avez pas encore de facture. <a href=\"#URL#\" class=\"#CLASS_NAME#\">Créez-en une</a> maintenant !";
$MESS["CRM_INVOICE_WGT_PAGE_TITLE_SHORT"] = "Rapport de facture";
?>