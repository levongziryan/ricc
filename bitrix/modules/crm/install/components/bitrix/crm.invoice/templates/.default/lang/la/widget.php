<?
$MESS["CRM_INVOICE_WGT_PAGE_TITLE"] = "Reportes analíticos de facturas";
$MESS["CRM_INVOICE_WGT_FUNNEL"] = "Embudo de factura";
$MESS["CRM_INVOICE_WGT_SUM_INVOICE_IN_WORK"] = "Importe total de facturas activas";
$MESS["CRM_INVOICE_WGT_SUM_INVOICE_SUCCESSFUL"] = "Importe total de facturas pagadas";
$MESS["CRM_INVOICE_WGT_SUM_INVOICE_OWED"] = "Importe total de facturas pendientes";
$MESS["CRM_INVOICE_WGT_QTY_INVOICE_OVERDUE"] = "Número de facturas vencidas";
$MESS["CRM_INVOICE_WGT_SUM_INVOICE_OVERDUE"] = "Importe total de las facturas vencidas";
$MESS["CRM_INVOICE_WGT_DEAL_PAYMENT_CONTROL"] = "Control de pagos de negociaciones ganadas";
$MESS["CRM_INVOICE_WGT_SUM_DEAL_INVOICE_OVERALL"] = "Importe de facturas creadas";
$MESS["CRM_INVOICE_WGT_SUM_DEAL_INVOICE_OWED"] = "Importe de facturas no creadas";
$MESS["CRM_INVOICE_WGT_INVOCE_PAYMENT"] = "Deudas y pagos";
$MESS["CRM_INVOICE_WGT_RATING"] = "Clasificación de facturas pagadas";
$MESS["CRM_INVOICE_WGT_INVOCE_MANAGER"] = "Eficiencia en la gestión de facturas de los empleados";
$MESS["CRM_INVOICE_WGT_DEMO_TITLE"] = "Este es un ireporte de demo. Ocultar para acceder a análisis de sus facturas.";
$MESS["CRM_INVOICE_WGT_DEMO_CONTENT"] = "Si aún no tiene facturas, <a href=\"#URL#\" class=\"#CLASS_NAME#\">cree una</a> ahora mismo!";
$MESS["CRM_INVOICE_WGT_PAGE_TITLE_SHORT"] = "Reporte de facturas";
?>