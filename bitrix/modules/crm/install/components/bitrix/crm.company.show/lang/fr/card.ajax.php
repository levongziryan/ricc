<?
$MESS["CRM_COLUMN_EMAIL"] = "Courrier électronique";
$MESS["CRM_COLUMN_DATE_MODIFY"] = "Date de modification";
$MESS["CRM_SECTION_COMPANY_INFO"] = "Informations sur la compagnie";
$MESS["CRM_COLUMN_EMPLOYEES"] = "Employés";
$MESS["CRM_SECTION_CONTACT_INFO"] = "Information de contact";
$MESS["CRM_OPER_SHOW"] = "Affichage";
$MESS["CRM_OPER_EDIT"] = "Editer";
$MESS["CRM_COLUMN_WEB"] = "Site";
$MESS["CRM_COLUMN_PHONE"] = "Numéro de téléphone";
$MESS["CRM_COLUMN_COMPANY_TYPE"] = "Type de l'Entreprise";
$MESS["CRM_SIP_MGR_UNKNOWN_RECIPIENT"] = "Appelant inconnu";
$MESS["CRM_SIP_MGR_MAKE_CALL"] = "Appel";
?>