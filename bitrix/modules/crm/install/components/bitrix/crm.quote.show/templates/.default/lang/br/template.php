<?
$MESS["CRM_QUOTE_SHOW_TAB_1"] = "Cotação";
$MESS["CRM_QUOTE_SHOW_TAB_1_TITLE"] = "Propriedades da Estimativa";
$MESS["CRM_TAB_2"] = "Contato";
$MESS["CRM_TAB_2_TITLE"] = "Contato do orçamento ";
$MESS["CRM_TAB_3"] = "Empresa";
$MESS["CRM_TAB_3_TITLE"] = "Orçamento da empresa";
$MESS["CRM_TAB_4"] = "Lead";
$MESS["CRM_TAB_4_TITLE"] = "Lead do orçamento";
$MESS["CRM_TAB_6"] = "Atividades ";
$MESS["CRM_TAB_6_TITLE"] = "Atividades do orçamento";
$MESS["CRM_TAB_7"] = "Processos de negócios";
$MESS["CRM_TAB_7_TITLE"] = "Processos de negócios do orçamento";
$MESS["CRM_TAB_8"] = "Fatura";
$MESS["CRM_TAB_8_TITLE"] = "Faturas do orçamento";
$MESS["CRM_EDIT_BTN_TTL"] = "Clique para editar";
$MESS["CRM_LOCK_BTN_TTL"] = "Não é possível editar este item";
$MESS["CRM_QUOTE_SHOW_TITLE"] = "Orçamento ##QUOTE_NUMBER# de #BEGINDATE#";
$MESS["CRM_QUOTE_SHOW_LEGEND"] = "Orçamento ##QUOTE_NUMBER#";
$MESS["CRM_QUOTE_SIDEBAR_STATUS"] = "Status";
$MESS["CRM_QUOTE_OPPORTUNITY_SHORT"] = "Valor";
$MESS["CRM_QUOTE_OPENED"] = "Este orçamento pode ser visto por qualquer um.";
$MESS["CRM_QUOTE_NOT_OPENED"] = "Este orçamento não é público.";
$MESS["CRM_QUOTE_DATE_CREATE"] = "Criado em";
$MESS["CRM_QUOTE_DATE_MODIFY"] = "Modificado em";
$MESS["CRM_TAB_HISTORY"] = "Histórico";
$MESS["CRM_TAB_HISTORY_TITLE"] = "Histórico de atualizações do orçamento";
$MESS["CRM_TAB_DETAILS"] = "Detalhes";
$MESS["CRM_TAB_DETAILS_TITLE"] = "Detalhes";
$MESS["CRM_TAB_PRODUCT_ROWS"] = "Produtos";
$MESS["CRM_TAB_PRODUCT_ROWS_TITLE"] = "Produtos no orçamento";
$MESS["CRM_QUOTE_FILES"] = "Arquivos";
$MESS["CRM_QUOTE_COMMENT"] = "Comentário";
$MESS["CRM_QUOTE_DURATION"] = "Data de validade";
$MESS["CRM_QUOTE_CLIENT"] = "Cliente";
$MESS["CRM_QUOTE_CLIENT_PHONE"] = "Telefone";
$MESS["CRM_QUOTE_CLIENT_EMAIL"] = "E-mail";
$MESS["CRM_QUOTE_CLIENT_INFO"] = "Informações de contato";
$MESS["CRM_DEAL_LINK"] = "Negócio";
$MESS["CRM_QUOTE_CLIENT_NOT_ASSIGNED"] = "[não atribuído]";
$MESS["CRM_TAB_LIVE_FEED"] = "Feed";
$MESS["CRM_TAB_LIVE_FEED_TITLE"] = "Feed do negócio ";
$MESS["CRM_QUOTE_WEBDAV_FILE_LOADING"] = "Carregando";
$MESS["CRM_QUOTE_WEBDAV_FILE_ALREADY_EXISTS"] = "Já existe um arquivo com este nome. Você ainda pode usar a pasta atual. Neste caso, a versão existente do documento será salva no histórico.";
$MESS["CRM_QUOTE_WEBDAV_FILE_ACCESS_DENIED"] = "Acesso negado.";
$MESS["CRM_QUOTE_WEBDAV_ATTACH_FILE"] = "Anexar arquivo";
$MESS["CRM_QUOTE_WEBDAV_TITLE"] = "Arquivos";
$MESS["CRM_QUOTE_WEBDAV_DRAG_FILE"] = "Arraste e solte um ou mais arquivos aqui";
$MESS["CRM_QUOTE_WEBDAV_SELECT_FILE"] = "ou selecione um arquivo do seu computador";
$MESS["CRM_QUOTE_WEBDAV_SELECT_FROM_LIB"] = "Selecione a partir de biblioteca de arquivos";
$MESS["CRM_QUOTE_WEBDAV_LOAD_FILES"] = "Enviar arquivos";
$MESS["CRM_QUOTE_PRINT_BTN_TTL"] = "Imprimir";
$MESS["CRM_QUOTE_CANCEL_BTN_TTL"] = "Cancelar";
$MESS["CRM_QUOTE_PRINT_DLG_TTL"] = "Imprimir Orçamentos";
$MESS["CRM_QUOTE_PRINT_TEMPLATE_FIELD"] = "Modelo";
$MESS["CRM_QUOTE_NO_PRINT_TEMPLATES_ERROR"] = "Nenhum modelo de impressão definido.";
$MESS["CRM_QUOTE_NO_PRINT_URL_ERROR"] = "URL de impressão não foi encontrada.";
$MESS["CRM_QUOTE_DISK_ATTACHED_FILES"] = "Anexos";
$MESS["CRM_TAB_1"] = "Negócio";
$MESS["CRM_TAB_1_TITLE"] = "Propriedades do negócio";
$MESS["CRM_QUOTE_CONV_ACCESS_DENIED"] = "Você precisa de permissão de criação de negociação e fatura para prosseguir.";
$MESS["CRM_QUOTE_CONV_GENERAL_ERROR"] = "Erro genérico.";
$MESS["CRM_QUOTE_CONV_DIALOG_TITLE"] = "Crie entidade na cotação";
$MESS["CRM_QUOTE_CONV_DIALOG_CONTINUE_BTN"] = "Continuar";
$MESS["CRM_QUOTE_CONV_DIALOG_CANCEL_BTN"] = "Cancelar";
$MESS["CRM_QUOTE_CONV_DIALOG_SYNC_LEGEND"] = "As entidades selecionadas não têm campos para os quais os dados da cotação possam ser passados. Escolha as entidades em que os campos adicionais serão criados para salvar todas as informações disponíveis. ";
$MESS["CRM_QUOTE_CONV_DIALOG_SYNC_FILED_LIST_TITLE"] = "Quais campos serão criados";
$MESS["CRM_QUOTE_CONV_DIALOG_SYNC_ENTITY_LIST_TITLE"] = "Escolha as entidades nas quais os campos adicionais serão criados";
$MESS["CRM_TAB_DEAL"] = "Negociações";
$MESS["CRM_TAB_DEAL_TITLE"] = "Negociações de cotação";
$MESS["CRM_QUOTE_SHOW_CONV_DEAL_CATEGORY_DLG_TITLE"] = "Preferências da negociação";
$MESS["CRM_QUOTE_SHOW_CONV_DEAL_CATEGORY_DLG_FIELD"] = "Pipeline";
$MESS["CRM_QUOTE_SHOW_BUTTON_SAVE"] = "Salvar";
$MESS["CRM_QUOTE_SHOW_BUTTON_CANCEL"] = "Cancelar";
$MESS["CRM_TAB_TREE"] = "Dependências";
$MESS["CRM_TAB_TREE_TITLE"] = "Links para outras entidades e itens";
?>