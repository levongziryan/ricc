<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "O módulo de e-loja não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "O módulo de catálogo comercial não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "O Módulo Moeda não está instalado.";
?>