<?
$MESS["CRM_FIELD_OPPORTUNITY"] = "Importe";
$MESS["CRM_FIELD_STATUS"] = "Estados";
$MESS["CRM_FIELD_ASSIGNED_BY"] = "Persona responsable";
$MESS["CRM_FIELD_BEGINDATE"] = "Fecha";
$MESS["CRM_FIELD_CLOSEDATE"] = "Fecha límite";
$MESS["CRM_FIELD_LEAD"] = "Prospectos";
$MESS["CRM_FIELD_DEAL"] = "Negociación";
$MESS["CRM_TITLE_QUOTE"] = "Cotización";
$MESS["CRM_NO_DATE"] = "ninguna";
?>