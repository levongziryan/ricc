<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_LEAD_SHOW_TITLE"] = "Visualizar Lead";
$MESS["CRM_LEAD_SHOW"] = "Visualizar";
$MESS["CRM_LEAD_EDIT_TITLE"] = "Editar Lead";
$MESS["CRM_LEAD_EDIT"] = "Editar";
$MESS["CRM_LEAD_COPY_TITLE"] = "Copiar Lead";
$MESS["CRM_LEAD_COPY"] = "Copiar Lead";
$MESS["CRM_LEAD_DELETE_TITLE"] = "Excluir Lead";
$MESS["CRM_LEAD_DELETE"] = "Excluir";
$MESS["CRM_LEAD_EVENT_TITLE"] = "Adicionar novo evento";
$MESS["CRM_LEAD_EVENT"] = "Adicionar Evento";
$MESS["CRM_LEAD_TASK_TITLE"] = "Nova tarefa";
$MESS["CRM_LEAD_TASK"] = "Nova Tarefa";
$MESS["CRM_LEAD_BIZPROC_TITLE"] = "Processo de Negócio";
$MESS["CRM_LEAD_BIZPROC"] = "Processo de Negócio";
$MESS["CRM_LEAD_BIZPROC_LIST_TITLE"] = "Executar novo processo de negócio";
$MESS["CRM_LEAD_BIZPROC_LIST"] = "Novo processo de negócio";
$MESS["CRM_LEAD_SUBSCRIBE_TITLE"] = "Enviar mensagem";
$MESS["CRM_LEAD_SUBSCRIBE"] = "Enviar mensagem";
$MESS["CRM_LEAD_DELETE_CONFIRM"] = "Tem certeza de que deseja excluí-lo?";
$MESS["CRM_LEAD_CONVERT_TITLE"] = "Converter Lead";
$MESS["CRM_LEAD_CONVERT"] = "Converter";
$MESS["CRM_STATUS_INIT"] = "- Status -";
$MESS["CRM_LEAD_CALENDAR_TITLE"] = "Adicionar novo registro ao calendário";
$MESS["CRM_LEAD_CALENDAR"] = "Adicionar chamada ou evento";
$MESS["CRM_LEAD_SET_STATUS"] = "Definir status";
$MESS["CRM_LEAD_ASSIGN_TO"] = "Atribuir pessoa responsável";
$MESS["CRM_LEAD_ADD_EMAIL_TITLE"] = "Criar novo e-mail";
$MESS["CRM_LEAD_ADD_EMAIL"] = "Novo e-mail";
$MESS["CRM_LEAD_ADD_CALL_TITLE"] = "Criar uma nova chamada";
$MESS["CRM_LEAD_ADD_CALL"] = "Nova chamada";
$MESS["CRM_LEAD_ADD_MEETING_TITLE"] = "Criar uma nova reunião";
$MESS["CRM_LEAD_ADD_MEETING"] = "Nova reunião";
$MESS["CRM_LEAD_ADD_QUOTE_TITLE"] = "Nova cotação";
$MESS["CRM_LEAD_ADD_QUOTE"] = "Nova cotação";
$MESS["CRM_SIP_MGR_UNKNOWN_RECIPIENT"] = "Destinatário desconhecido";
$MESS["CRM_SIP_MGR_MAKE_CALL"] = "Chamada";
$MESS["CRM_LEAD_REBUILD_DUP_INDEX"] = "Duplicar o controle requer que você <a id=\"#ID#\" href=\"#URL#\">criar um índice</a> dos condutores no banco de dados.";
$MESS["CRM_LEAD_LRP_DLG_BTN_START"] = "Executar";
$MESS["CRM_LEAD_LRP_DLG_BTN_STOP"] = "Parar";
$MESS["CRM_LEAD_LRP_DLG_BTN_CLOSE"] = "Fechar";
$MESS["CRM_LEAD_LRP_DLG_WAIT"] = "Por favor espere...";
$MESS["CRM_LEAD_LRP_DLG_REQUEST_ERR"] = "Erro ao processar o pedido.";
$MESS["CRM_LEAD_REBUILD_DUP_INDEX_DLG_TITLE"] = "Reconstruir lead duplicado index";
$MESS["CRM_LEAD_REBUILD_DUP_INDEX_DLG_SUMMARY"] = "Lead duplicadas vai agora voltar a ser indexado. Esta operação pode levar algum tempo.";
$MESS["CRM_LEAD_MARK_AS_OPENED"] = "Tornar publico";
$MESS["CRM_LEAD_MARK_AS_OPENED_YES"] = "Sim";
$MESS["CRM_LEAD_MARK_AS_OPENED_NO"] = "Não";
$MESS["CRM_LEAD_REBUILD_ACCESS_ATTRS"] = "As permissões de acesso atualizadas exigem que você atualize os atuais atributos de acesso usando a <a id=\"#ID#\" target=\"_blank\" href= \"#URL#\">página de gerenciamento de permissões</a>.";
$MESS["CRM_LEAD_CONV_ACCESS_DENIED"] = "Você precisa de permissão de criação de contato, empresa e venda para converter um cliente potencial .";
$MESS["CRM_LEAD_CONV_GENERAL_ERROR"] = "Erro de conversão genérico.";
$MESS["CRM_LEAD_CONV_DIALOG_TITLE"] = "Converter Cliente Potencial";
$MESS["CRM_LEAD_CONV_DIALOG_CONTINUE_BTN"] = "Continuar";
$MESS["CRM_LEAD_CONV_DIALOG_CANCEL_BTN"] = "Cancelar";
$MESS["CRM_LEAD_CREATE_ON_BASIS"] = "Gerar";
$MESS["CRM_LEAD_CREATE_ON_BASIS_TITLE"] = "Criar contato, empresa e venda usando cliente potencial ";
$MESS["CRM_SHOW_ROW_COUNT"] = "Mostrar quantidade";
$MESS["CRM_LEAD_CONV_DIALOG_SYNC_LEGEND"] = "As entidades selecionadas não têm campos para os quais os dados de cliente potencial podem ser passados. Escolha entidades nas quais campos adicionais serão criados para salvar todas as informações disponíveis. ";
$MESS["CRM_LEAD_CONV_DIALOG_SYNC_FILED_LIST_TITLE"] = "Quais campos serão criados";
$MESS["CRM_LEAD_CONV_DIALOG_SYNC_ENTITY_LIST_TITLE"] = "Escolha as entidades em que os campos adicionais serão criados";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_CONTACT"] = "Contatos";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_COMPANY"] = "Empresas";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_BTN"] = "Selecionar";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_SEARCH"] = "Pesquisar";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_SEARCH_NO_RESULT"] = "Não foram encontradas entradas.";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_LAST"] = "Última";
$MESS["CRM_LEAD_CONV_OPEN_ENTITY_SEL"] = "Selecionar da lista...";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_TITLE"] = "Selecionar contato e empresa";
$MESS["CRM_LEAD_REBUILD_SEMANTICS"] = "Os relatórios requerem que você <a id=\"#ID#\" href=\"#URL#\">atualize os campos de serviço de cliente potencial</a> (isto não afetará nenhum dado do usuário).";
$MESS["CRM_LEAD_REBUILD_SEMANTICS_DLG_TITLE"] = "Atualizar campos de serviço de cliente potencial";
$MESS["CRM_LEAD_REBUILD_SEMANTICS_DLG_SUMMARY"] = "Os campos de serviço de cliente potencial serão atualizados agora. Isso pode demorar algum tempo.";
$MESS["CRM_LEAD_LIST_CONV_DEAL_CATEGORY_DLG_TITLE"] = "Preferências da negociação";
$MESS["CRM_LEAD_LIST_CONV_DEAL_CATEGORY_DLG_FIELD"] = "Pipeline";
$MESS["CRM_LEAD_LIST_BUTTON_SAVE"] = "Salvar";
$MESS["CRM_LEAD_LIST_BUTTON_CANCEL"] = "Cancelar";
$MESS["CRM_LEAD_LIST_DEL_PROC_DLG_TITLE"] = "Excluir clientes potenciais";
$MESS["CRM_LEAD_LIST_DEL_PROC_DLG_SUMMARY"] = "Isso irá excluir os clientes potenciais selecionados. Esta operação pode demorar um pouco.";
$MESS["CRM_LEAD_LIST_FILTER_NAV_BUTTON_LIST"] = "Lista";
$MESS["CRM_LEAD_LIST_FILTER_NAV_BUTTON_WIDGET"] = "Relatórios";
$MESS["CRM_LEAD_LIST_FILTER_NAV_BUTTON_KANBAN"] = "Kanban";
$MESS["CRM_LEAD_LIST_CHOOSE_ACTION"] = "Selecionar ação";
$MESS["CRM_LEAD_LIST_APPLY_BUTTON"] = "Aplicar";
$MESS["CRM_LEAD_REBUILD_SEARCH_CONTENT"] = "Para utilizar Pesquisa Rápida, você tem que <a id=\"#ID#\" href=\"#URL#\"> recriar o índice de pesquisa de cliente potencial</a>.";
$MESS["CRM_LEAD_REBUILD_SEARCH_CONTENT_DLG_TITLE"] = "Recriação do Índice de Pesquisa de Cliente Potencial";
$MESS["CRM_LEAD_REBUILD_SEARCH_CONTENT_DLG_SUMMARY"] = "Isto irá recriar o índice de pesquisa de cliente potencial. Esta operação pode demorar um pouco.";
$MESS["CRM_LEAD_START_CALL_LIST"] = "Começar a discagem";
$MESS["CRM_LEAD_CREATE_CALL_LIST"] = "Criar lista de chamada";
$MESS["CRM_LEAD_UPDATE_CALL_LIST"] = "Adicionar à lista de chamada";
$MESS["CRM_LEAD_ADD_ACTIVITY"] = "Plano";
$MESS["CRM_LEAD_ADD_ACTIVITY_TITLE"] = "Planejar atividade";
$MESS["CRM_LEAD_EVENT_SHORT"] = "Evento";
$MESS["CRM_LEAD_TASK_SHORT"] = "Tarefa";
$MESS["CRM_LEAD_ADD_CALL_SHORT"] = "Ligação";
$MESS["CRM_LEAD_ADD_MEETING_SHORT"] = "Reunião";
$MESS["CRM_REBUILD_SEARCH_CONTENT_STATE"] = "#processed# de #total#";
$MESS["CRM_SIP_MGR_ENABLE_CALL_RECORDING"] = "Gravar conversa";
$MESS["CRM_LEAD_BUILD_TIMELINE_DLG_TITLE"] = "Preparando o histórico do cliente potencial";
$MESS["CRM_LEAD_BUILD_TIMELINE_STATE"] = "#processed# de #total#";
?>