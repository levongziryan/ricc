<?
$MESS["CRM_LEAD_LIST_REBUILD_DUPLICATE_INDEX_COMPLETED_SUMMARY"] = "La réorganisation de l'index des doubles pour les prospects est terminée. Prospects traités: #PROCESSED_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_DUPLICATE_INDEX_PROGRESS_SUMMARY"] = "Prospects transformés: #PROCESSED_ITEMS# de #TOTAL_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_DUPLICATE_INDEX_NOT_REQUIRED_SUMMARY"] = "La restructuration de l'index des doubles des prospects n'est pas requise.";
$MESS["CRM_LEAD_LIST_ROW_COUNT"] = "Total: #ROW_COUNT#";
$MESS["CRM_LEAD_LIST_REBUILD_STATISTICS_NOT_REQUIRED_SUMMARY"] = "Les données statistiques des prospects sont à jour.";
$MESS["CRM_LEAD_LIST_REBUILD_STATISTICS_PROGRESS_SUMMARY"] = "Prospects traitées: #PROCESSED_ITEMS# sur #TOTAL_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_STATISTICS_COMPLETED_SUMMARY"] = "Le traitement des données statistiques des prospects est terminé. Prospects traitées: #PROCESSED_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_SEMANTICS_NOT_REQUIRED_SUMMARY"] = "Les champs du service de pistes ne nécessitent pas de mise à jour.";
$MESS["CRM_LEAD_LIST_REBUILD_SEMANTICS_PROGRESS_SUMMARY"] = "Pistes traitées : #PROCESSED_ITEMS# sur #TOTAL_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_SEMANTICS_COMPLETED_SUMMARY"] = "Mise à jour des champs du service de pistes terminée. Pistes traitées : #PROCESSED_ITEMS#.";
$MESS["CRM_LEAD_LIST_DELETION_COMPLETED_SUMMARY"] = "Les pistes ont été supprimées. Pistes traitées : #PROCESSED_ITEMS#.";
$MESS["CRM_LEAD_LIST_DELETION_PROGRESS_SUMMARY"] = "Pistes supprimées : #PROCESSED_ITEMS# sur #TOTAL_ITEMS#.";
$MESS["CRM_LEAD_LIST_DELETION_PARAM_ERROR"] = "L'opération de suppression a été abandonnée pour cause de paramètres non valides.";
$MESS["CRM_LEAD_LIST_DELETION_ACCESS_ERROR"] = "L'opération de suppression a été abandonnée pour cause de permissions insuffisantes.";
$MESS["CRM_LEAD_LIST_DELETION_FILTER_NOT_FOUND_ERROR"] = "L'opération de suppression a été abandonnée pour cause de préférences de filtre introuvables. Veuillez actualiser la page et réessayer.";
$MESS["CRM_LEAD_LIST_DELETION_FILTER_OUTDATED_ERROR"] = "L'opération de suppression a été abandonnée pour cause de préférences de filtre dépassées. Veuillez actualiser la page et réessayer.";
$MESS["CRM_LEAD_LIST_REBUILD_SEARCH_CONTENT_NOT_REQUIRED_SUMMARY"] = "L'index de recherche des pistes n'a pas besoin d'être recréé.";
$MESS["CRM_LEAD_LIST_REBUILD_SEARCH_CONTENT_PROGRESS_SUMMARY"] = "Pistes traitées : #PROCESSED_ITEMS# sur #TOTAL_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_SEARCH_CONTENT_COMPLETED_SUMMARY"] = "L'index de recherche des pistes a été recréé. Pistes traitées : #PROCESSED_ITEMS#.";
?>