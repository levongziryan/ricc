<?
$MESS["CRM_CLE2_NEW_ITEM"] = "Créer";
$MESS["CRM_CLE2_TAB_PARAMS_TITLE"] = "Paramètres de l'emplacement";
$MESS["CRM_CLE2_TAB_EXTERNAL"] = "Données externes";
$MESS["CRM_CLE2_REMOVE"] = "Supprimer";
$MESS["CRM_CLE2_TAB_EXTERNAL_MORE"] = "Plus";
$MESS["CRM_CLE2_TAB_PARAMS"] = "Paramètres";
$MESS["CRM_CLE2_TAB_EXTERNAL_TITLE"] = "Situation de données externes";
$MESS["CRM_CLE2_HEADING_GEO"] = "Données de géo";
$MESS["CRM_CLE2_HEADING_NAMES"] = "titres dépendant de la langue";
$MESS["CRM_CLE2_NOT_SELECTED"] = "Pas sélectionné";
?>