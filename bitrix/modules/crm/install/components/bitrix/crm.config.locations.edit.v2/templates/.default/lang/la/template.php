<?
$MESS["CRM_CLE2_NOT_SELECTED"] = "No seleccionado";
$MESS["CRM_CLE2_NEW_ITEM"] = "Nuevo";
$MESS["CRM_CLE2_TAB_PARAMS"] = "Parámetros";
$MESS["CRM_CLE2_TAB_PARAMS_TITLE"] = "Parámetros de ubicación";
$MESS["CRM_CLE2_TAB_EXTERNAL"] = "Datos externos";
$MESS["CRM_CLE2_TAB_EXTERNAL_TITLE"] = "Ubicación de datos externos";
$MESS["CRM_CLE2_REMOVE"] = "Eliminar";
$MESS["CRM_CLE2_TAB_EXTERNAL_MORE"] = "Más...";
$MESS["CRM_CLE2_HEADING_GEO"] = "Datos geográficos";
$MESS["CRM_CLE2_HEADING_NAMES"] = "Títulos dependientes del idioma";
?>