<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "O módulo de vendas não está instalado. ";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "O módulo de Catálogo não está instalado. ";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "O módulo de Moeda não está instalado.";
?>