<?
$MESS["CRM_CONFIGS_TAB_WHERE_TO_BEGIN"] = "Point de départ";
$MESS["CRM_CONFIGS_TAB_SETTINGS_FORMS_AND_REPORTS"] = "Paramètres des formulaires et des rapports";
$MESS["CRM_CONFIGS_TAB_CREATION_ON_THE_BASIS"] = "Générer";
$MESS["CRM_CONFIGS_TAB_PRINTED_FORMS_OF_DOCUMENTS"] = "Détails de paiement";
$MESS["CRM_CONFIGS_TAB_PS_LIST"] = "Options de paiement";
$MESS["CRM_CONFIGS_TAB_RIGHTS"] = "Permissions";
$MESS["CRM_CONFIGS_TAB_AUTOMATION"] = "Automatisation";
$MESS["CRM_CONFIGS_TAB_WORK_WITH_MAIL"] = "E-mail";
$MESS["CRM_CONFIGS_TAB_INTEGRATION"] = "Intégration";
$MESS["CRM_CONFIGS_TAB_OTHER"] = "Autre";
$MESS["CRM_CONFIGS_STATUS"] = "États et déroulants";
$MESS["CRM_CONFIGS_CURRENCY"] = "Devise";
$MESS["CRM_CONFIGS_TAX"] = "Taxes";
$MESS["CRM_CONFIGS_PS"] = "Systèmes de paiement";
$MESS["CRM_CONFIGS_LOCATIONS"] = "Localisations";
$MESS["CRM_CONFIGS_PERMS"] = "Permissions d'accès";
$MESS["CRM_CONFIGS_BP"] = "Processus d'entreprise";
$MESS["CRM_CONFIGS_AUTOMATION_LEAD"] = "Client potentiels";
$MESS["CRM_CONFIGS_AUTOMATION_DEAL"] = "Affaires";
$MESS["CRM_CONFIGS_FIELDS"] = "Champs personnalisés";
$MESS["CRM_CONFIGS_CONFIG"] = "Autres paramètres";
$MESS["CRM_CONFIGS_FACE_TRACER"] = "Face tracker";
$MESS["CRM_CONFIGS_SENDSAVE"] = "Intégration d'e-mail";
$MESS["CRM_CONFIGS_MEASURE"] = "Unités de mesure";
$MESS["CRM_CONFIGS_EXTERNAL_SALE"] = "Boutique en ligne";
$MESS["CRM_CONFIGS_EXTERNAL_SALE_BX"] = "1C-Bitrix";
$MESS["CRM_CONFIGS_SMS"] = "Notifications SMS pour les clients et les employés";
$MESS["CRM_CONFIGS_SMS_PROVIDER"] = "Notifications SMS via #PROVIDER#";
$MESS["CRM_CONFIGS_SMS_MARKETPLACE"] = "Applications SMS";
$MESS["CRM_CONFIGS_EXCH1C"] = "1C:Intégration d'entreprise";
$MESS["CRM_CONFIGS_MAIL_TRACKER"] = "Comptes e-mail du CRM";
$MESS["CRM_CONFIGS_MAIL_TRACKER_SH"] = "Connecter l'e-mail de l'entreprise";
$MESS["CRM_CONFIGS_MAIL_TEMPLATES"] = "Modèles d'e-mails";
$MESS["CRM_CONFIGS_PRODUCT_PROPS"] = "Propriétés des produits";
$MESS["CRM_CONFIGS_PRESET"] = "Modèles de données de contact ou d'entreprise";
$MESS["CRM_CONFIGS_MYCOMPANY"] = "Données de la société";
$MESS["CRM_CONFIGS_MYCOMPANY1"] = "Données de votre société";
$MESS["CRM_CONFIGS_SLOT"] = "Rapports analytiques";
$MESS["CRM_CONFIGS_REFERENCE"] = "Référence";
$MESS["CRM_CONFIGS_LEAD"] = "Prospect";
$MESS["CRM_CONFIGS_DEAL"] = "Transaction";
$MESS["CRM_CONFIGS_QOUTE"] = "Devis";
$MESS["CRM_CONFIGS_CONTACT"] = "Contact";
$MESS["CRM_CONFIGS_COMPANY"] = "Entreprise";
$MESS["CRM_CONFIGS_INVOICE"] = "Facture";
$MESS["CRM_CONFIGS_DESCRIPTION_WHERE_TO_BEGIN"] = "Fournir une configuration initiale aidera à ajuster votre CRM à votre entreprise. Créez vos dictionnaires, unités de mesure pour vos produits, taxes, devises, et définissez les autres préférences.";
$MESS["CRM_CONFIGS_DESCRIPTION_SETTINGS_FORMS_AND_REPORTS"] = "Créez vos propres champs personnalisés pour n'importe quelle entité CRM. Sélectionnez les champs à inclure dans les rapports analytiques.";
$MESS["CRM_CONFIGS_DESCRIPTION_PRINTED_FORMS_OF_DOCUMENTS"] = "Utilisez la page Méthodes de paiement pour spécifier les données de votre société, le logo, le timbre et la signature.";
$MESS["CRM_CONFIGS_DESCRIPTION_RIGHTS"] = "Avant de commencer : assurez-vous que les responsables qui travaillent directement avec les clients aient leurs droits d'accès correctement configurés pour leur permettre d'accéder aux fichiers clients.";
$MESS["CRM_CONFIGS_DESCRIPTION_AUTOMATION"] = "Automatisez les tâches liées aux clients : utilisez les flux de travail pour permettre au système de traiter les nouvelles prospects, contacts et transactions, et d'assigner des tâches.";
$MESS["CRM_CONFIGS_DESCRIPTION_WORK_WITH_MAIL"] = "Pour traiter les e-mails, connectez la boîte de réception de votre société au CRM. Tous les messages entrants seront distribués aux bons contacts, sociétés et prospects. Les tâches seront automatiquement crées et assignées aux responsables des ventes.";
$MESS["CRM_CONFIGS_DESCRIPTION_INTEGRATION"] = "Si vous disposez d'une boutique en ligne développée par Bitrix, vous pouvez configurer des échanges de données de commandes. Toutes les nouvelles commandes seront automatiquement exportées vers Bitrix sous la forme de transactions.";
$MESS["CRM_CONFIGS_DESCRIPTION_CREATION_ON_THE_BASIS"] = "Utilisez la création automatique de nouvelles entités à partir de celles déjà existantes tout en gardant toutes les informations essentielles. Utilisez la cartographie des champs pour profiter d'une meilleure flexibilité quand vous créez de nouvelles entités.";
$MESS["CRM_CONFIGS_DESCRIPTION_OTHER"] = "Ouvrez cette page pour définir d'autres préférences CRM.";
$MESS["CRM_CONFIGS_TAB_APPS"] = "Applications";
$MESS["CRM_CONFIGS_CRM_APPLICATION"] = "Applications CRM";
$MESS["CRM_CONFIGS_MIGRATION_OTHER_CRM"] = "Migrer depuis une autre CRM";
$MESS["CRM_CONFIGS_DESCRIPTION_APP"] = "<b>Migrez les données d'autres CRM</b><br>
Vous utilisez d'autres CRM? Gardez vos informations en sécurité en les migrant avec Bitrix24 : laissez les applications le faire pour vous.<br><br>
<b>Applications CRM</b><br>
Étendez les capacités de votre Bitrix24 grâce à des outils bien utiles pour vous aider à vendre plus. Ajoutez des services pour gérer les lettres d'information, les pages de renvoi, les chats, les appels. Calculez l'ICP et plus encore. Le nombre d'application grossit tous les jours.";
$MESS["CRM_CONFIGS_NO_ACCESS_ERROR"] = "Accès refusé.";
$MESS["CRM_CONFIGS_DEAL_CATEGORY"] = "Pipelines d'affaires";
$MESS["CRM_CONFIGS_TRACKER"] = "Paramètres de suivi";
?>