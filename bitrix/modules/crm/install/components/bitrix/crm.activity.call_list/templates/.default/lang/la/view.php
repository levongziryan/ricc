<?
$MESS["CRM_CALL_LIST_SUBJECT"] = "Asunto";
$MESS["CRM_CALL_LIST_DESCRIPTION"] = "Descripción";
$MESS["CRM_CALL_LIST_TITLE"] = "Lista de llamadas";
$MESS["CRM_CALL_LIST_ENTITY_LEADS"] = "Prospectos";
$MESS["CRM_CALL_LIST_ENTITY_CONTACTS"] = "Contactos";
$MESS["CRM_CALL_LIST_ENTITY_COMPANIES"] = "Compañías";
$MESS["CRM_CALL_LIST_SELECTION_PARAMS"] = "Parámetros de selección";
$MESS["CRM_CALL_LIST_STATISTICS"] = "Estadística";
$MESS["CRM_CALL_LIST_BUTTON_START"] = "Empezar a llamar";
$MESS["CRM_CALL_LIST_BUTTON_CONTINUE"] = "Continuar";
$MESS["CRM_CALL_LIST_COMPLETE"] = "completado #COMPLETE# of #TOTAL#";
$MESS["CRM_CALL_LIST_HAND_PICKED"] = "seleccionado manualmente";
$MESS["CRM_CALL_LIST_PICKED_BY_FILTER"] = "seleccionado de acuerdo con";
$MESS["CRM_CALL_LIST_FILTER"] = "filtrar";
$MESS["CRM_CALL_LIST_USE_FORM"] = "Usando el formulario";
?>