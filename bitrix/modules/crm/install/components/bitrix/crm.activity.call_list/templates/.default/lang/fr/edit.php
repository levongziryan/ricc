<?
$MESS["CRM_ACTIVITY_CALL_LIST_LABEL"] = "Liste des appels";
$MESS["CRM_ACTIVITY_CALL_LIST_SUBJECT"] = "Objet";
$MESS["CRM_ACTIVITY_CALL_LIST_SUBJECT_PLACEHOLDER"] = "Titre de l'activité";
$MESS["CRM_ACTIVITY_CALL_LIST_DESCRIPTION"] = "Description";
$MESS["CRM_ACTIVITY_CALL_LIST_DESCRIPTION_PLACEHOLDER"] = "Fournissez ici les détails de l'activité";
$MESS["CRM_ACTIVITY_CALL_LIST_CREATE_FROM"] = "Créer une liste à partir de";
$MESS["CRM_ACTIVITY_CALL_LIST_CREATE_FROM_LEADS"] = "Pistes";
$MESS["CRM_ACTIVITY_CALL_LIST_CREATE_FROM_CONTACTS"] = "Contacts";
$MESS["CRM_ACTIVITY_CALL_LIST_CREATE_FROM_COMPANIES"] = "Sociétés";
$MESS["CRM_ACTIVITY_CALL_LIST_CREATE_TAB_FILTER_PARAMS"] = "Paramètres de sélection";
$MESS["CRM_ACTIVITY_CALL_LIST_ACTIVITY_CREATED"] = "Activité de la liste des appels créée";
$MESS["CRM_ACTIVITY_CALL_LIST_ACTIVITY_CREATED_TEXT"] = "Vous pouvez maintenant éditer l'activité";
$MESS["CRM_ACTIVITY_CALL_LIST_ACTIVITY_GOTO"] = "Ouvrir l'activité";
$MESS["CRM_CALL_LIST_FILTER"] = "filtre";
?>