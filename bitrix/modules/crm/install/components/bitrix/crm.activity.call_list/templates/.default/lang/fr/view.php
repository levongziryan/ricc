<?
$MESS["CRM_CALL_LIST_SUBJECT"] = "Objet";
$MESS["CRM_CALL_LIST_DESCRIPTION"] = "Description";
$MESS["CRM_CALL_LIST_TITLE"] = "Liste des appels";
$MESS["CRM_CALL_LIST_ENTITY_LEADS"] = "Pistes";
$MESS["CRM_CALL_LIST_ENTITY_CONTACTS"] = "Contacts";
$MESS["CRM_CALL_LIST_ENTITY_COMPANIES"] = "Sociétés";
$MESS["CRM_CALL_LIST_SELECTION_PARAMS"] = "Paramètres de sélection";
$MESS["CRM_CALL_LIST_STATISTICS"] = "Statistiques";
$MESS["CRM_CALL_LIST_BUTTON_START"] = "Commencer l'appel";
$MESS["CRM_CALL_LIST_BUTTON_CONTINUE"] = "Continuer";
$MESS["CRM_CALL_LIST_COMPLETE"] = "#COMPLETE# sur #TOTAL# terminés";
$MESS["CRM_CALL_LIST_HAND_PICKED"] = "#ENTITIES# ont été manuellement sélectionnées";
$MESS["CRM_CALL_LIST_PICKED_BY_FILTER"] = "sélectionné selon ";
$MESS["CRM_CALL_LIST_FILTER"] = "filtre";
$MESS["CRM_CALL_LIST_USE_FORM"] = "Utilisation du formulaire";
?>