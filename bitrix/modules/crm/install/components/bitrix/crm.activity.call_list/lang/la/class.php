<?
$MESS["CRM_CALL_LIST_ENTITY_NAME"] = "Nombre";
$MESS["CRM_CALL_LIST_ITEM_STATUS"] = "Estado";
$MESS["CRM_CALL_LIST_ITEM_CALL_RECORD"] = "Grabar";
$MESS["CRM_CALL_LIST_ITEM_WEBFORM_ACTIVITY"] = "Actividad del formulario del CRM";
$MESS["CRM_CALL_LIST_ITEM_CREATED"] = "Creado el";
$MESS["CRM_CALL_LIST_ERROR_WRONG_ITEM_TYPE"] = "La lista de llamadas no puede incluir entidades de diferentes tipos";
$MESS["CRM_CALL_LIST_ENTITIES_ADDED"] = "#ENTITIES# agregar a la lista de llamadas";
$MESS["CRM_CALL_LIST_ITEM_FORM"] = "Formulario ##ID#";
$MESS["VOXIMPLANT_MODULE_NOT_INSTALLED"] = "El módulo Telephony no está instalado.";
$MESS["CRM_CALL_LIST_HAND_PICKED"] = "#ENTITIES# fueron seleccionados manualmente";
$MESS["CRM_CALL_LIST_FILTERED"] = "#ENTITIES# fueron seleccionados de acuerdo a";
$MESS["CRM_CALL_LIST_DELETE"] = "Eliminar";
$MESS["CRM_CALL_LIST_ADD_MORE"] = "Agregar más";
?>