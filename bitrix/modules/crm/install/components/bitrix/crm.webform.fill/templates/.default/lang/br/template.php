<?
$MESS["CRM_WEBFORM_FILL_ERROR_FIELD_EMPTY"] = "Preencher todos os campos obrigatórios";
$MESS["CRM_WEBFORM_FILL_ERROR_TITLE"] = "Atenção!";
$MESS["CRM_WEBFORM_FILL_NOT_SELECTED"] = "Não selecionado";
$MESS["CRM_WEBFORM_FILL_FILE_SELECT"] = "Selecionar";
$MESS["CRM_WEBFORM_FILL_FILE_NOT_SELECTED"] = "Nenhum arquivo selecionado";
$MESS["CRM_WEBFORM_FILL_FIELD_ADD_OTHER"] = "Adicionar mais campos";
$MESS["CRM_WEBFORM_FILL_PRODUCT_TITLE"] = "Produtos selecionados";
$MESS["CRM_WEBFORM_FILL_PRODUCT_SUMMARY"] = "Total";
$MESS["CRM_WEBFORM_FILL_COPYRIGHT_CHARGED_BY"] = "Fornecido por";
$MESS["CRM_WEBFORM_FILL_FILL_AGAIN"] = "Preencha o formulário novamente";
$MESS["CRM_WEBFORM_FILL_LICENCE_PROMPT"] = "Termos do Contrato de Licença:";
$MESS["CRM_WEBFORM_FILL_LICENCE_ACCEPT"] = "Eu aceito";
$MESS["CRM_WEBFORM_FILL_LICENCE_DECLINE"] = "Eu recuso";
$MESS["CRM_WEBFORM_FILL_REDIRECT_DESC"] = "Você será redirecionado em";
$MESS["CRM_WEBFORM_FILL_REDIRECT_SECONDS"] = "seg";
$MESS["CRM_WEBFORM_FILL_REDIRECT_GO_NOW"] = "Abrir agora";
$MESS["CRM_WEBFORM_FILL_RESULT_SENT"] = "O formulário foi preenchido com sucesso.";
$MESS["CRM_WEBFORM_FILL_RESULT_ERROR"] = "Não foi possível enviar o formulário.";
$MESS["CRM_WEBFORM_FILL_BUTTON_DEFAULT"] = "Enviar";
$MESS["CRM_WEBFORM_FILL_CALLBACK_FREE"] = "Ligação gratuita";
$MESS["CRM_WEBFORM_FILL_COPYRIGHT_BITRIX"] = "Bitrix";
$MESS["CRM_WEBFORM_FILL_LICENCE_PROMPT1"] = "Termos do contrato:";
?>