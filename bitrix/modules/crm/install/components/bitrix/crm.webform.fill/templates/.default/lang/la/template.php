<?
$MESS["CRM_WEBFORM_FILL_ERROR_FIELD_EMPTY"] = "Complete todos los campos requeridos";
$MESS["CRM_WEBFORM_FILL_ERROR_TITLE"] = "Atención!";
$MESS["CRM_WEBFORM_FILL_NOT_SELECTED"] = "No seleccionado";
$MESS["CRM_WEBFORM_FILL_FILE_SELECT"] = "Seleccionar";
$MESS["CRM_WEBFORM_FILL_FILE_NOT_SELECTED"] = "No se ha seleccionado ningún archivo";
$MESS["CRM_WEBFORM_FILL_FIELD_ADD_OTHER"] = "Agregar más campos";
$MESS["CRM_WEBFORM_FILL_PRODUCT_TITLE"] = "Productos seleccionados";
$MESS["CRM_WEBFORM_FILL_PRODUCT_SUMMARY"] = "Total";
$MESS["CRM_WEBFORM_FILL_COPYRIGHT_CHARGED_BY"] = "Desarrollado por";
$MESS["CRM_WEBFORM_FILL_FILL_AGAIN"] = "Completar el formulario nuevamente";
$MESS["CRM_WEBFORM_FILL_LICENCE_PROMPT"] = "Términos del acuerdo de licencia:";
$MESS["CRM_WEBFORM_FILL_LICENCE_ACCEPT"] = "Aceptar";
$MESS["CRM_WEBFORM_FILL_LICENCE_DECLINE"] = "Declinar";
$MESS["CRM_WEBFORM_FILL_REDIRECT_DESC"] = "Usted será redireccionado en";
$MESS["CRM_WEBFORM_FILL_REDIRECT_SECONDS"] = "seg";
$MESS["CRM_WEBFORM_FILL_REDIRECT_GO_NOW"] = "Abrir ahora";
$MESS["CRM_WEBFORM_FILL_RESULT_SENT"] = "El formulario ha sido completado con éxito.";
$MESS["CRM_WEBFORM_FILL_RESULT_ERROR"] = "No se pudo enviar el formulario.";
$MESS["CRM_WEBFORM_FILL_BUTTON_DEFAULT"] = "Enviar";
$MESS["CRM_WEBFORM_FILL_CALLBACK_FREE"] = "Llamada gratuita";
$MESS["CRM_WEBFORM_FILL_COPYRIGHT_BITRIX"] = "Bitrix";
$MESS["CRM_WEBFORM_FILL_LICENCE_PROMPT1"] = "Términos del acuerdo:";
?>