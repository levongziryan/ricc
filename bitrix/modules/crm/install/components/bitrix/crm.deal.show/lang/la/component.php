<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "El módulo Business Process no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso Denegado";
$MESS["CRM_FIELD_TITLE_DEAL"] = "Nombre";
$MESS["CRM_FIELD_OPENED"] = "Disponible para todos";
$MESS["CRM_FIELD_COMMENTS"] = "Comentario";
$MESS["CRM_FIELD_OPPORTUNITY"] = "Ingresos";
$MESS["CRM_FIELD_COMPANY_ID"] = "Compañía";
$MESS["CRM_FIELD_COMPANY_TITLE"] = "Compañía";
$MESS["CRM_FIELD_CONTACT_ID"] = "Contacto";
$MESS["CRM_FIELD_CONTACT_TITLE"] = "Contacto";
$MESS["CRM_FIELD_STAGE_ID"] = "Fase";
$MESS["CRM_COLUMN_PRODUCTS"] = "Productos";
$MESS["CRM_FIELD_STATE_ID"] = "Estado";
$MESS["CRM_FIELD_CLOSED"] = "Cerrado";
$MESS["CRM_FIELD_CURRENCY_ID"] = "Moneda";
$MESS["CRM_FIELD_PROBABILITY"] = "Probabilidad, %";
$MESS["CRM_FIELD_TYPE_ID"] = "Tipo";
$MESS["CRM_FIELD_BEGINDATE"] = "Fecha de inicio";
$MESS["CRM_FIELD_ASSIGNED_BY_ID"] = "Responsable";
$MESS["CRM_FIELD_CREATED_BY_ID"] = "Creado por";
$MESS["CRM_FIELD_MODIFY_BY_ID"] = "Modificado por";
$MESS["CRM_FIELD_DATE_CREATE"] = "Creado";
$MESS["CRM_FIELD_DATE_MODIFY"] = "Modificado";
$MESS["CRM_SECTION_CONTACT_INFO"] = "Información de Contacto";
$MESS["CRM_SECTION_CLIENT_INFO"] = "Información del cliente";
$MESS["CRM_SECTION_ADDITIONAL"] = "Más";
$MESS["CRM_SECTION_PROFIT"] = "Ingresos";
$MESS["CRM_SECTION_DATE"] = "Período";
$MESS["CRM_SECTION_PLANNED_EVENT"] = "Evento Planeado";
$MESS["CRM_SECTION_ACTIVITY_TASK"] = "Tareas";
$MESS["CRM_SECTION_BIZPROC"] = "Business Process";
$MESS["CRM_FIELD_EVENT_DATE"] = "Fecha del Evento";
$MESS["CRM_FIELD_EVENT_ID"] = "Tipo de Evento";
$MESS["CRM_FIELD_EVENT_DESCRIPTION"] = "Descripción del Evento";
$MESS["CRM_FIELD_DEAL_CONTACTS"] = "Contactos de la negociación";
$MESS["CRM_FIELD_DEAL_COMPANY"] = "Compañías de la negociación";
$MESS["CRM_FIELD_DEAL_LEAD"] = "Prospectos y Negociaciones";
$MESS["CRM_FIELD_DEAL_EVENT"] = "Eventos de la negociación";
$MESS["CRM_FIELD_DEAL_ACTIVITY"] = "Tareas";
$MESS["CRM_FIELD_DEAL_BIZPROC"] = "Business Process";
$MESS["CRM_DEAL_NAV_TITLE_EDIT"] = "Negociación : #NAME#";
$MESS["CRM_DEAL_NAV_TITLE_ADD"] = "Agregar negociación";
$MESS["CRM_DEAL_NAV_TITLE_LIST"] = "Negociaciones";
$MESS["CRM_SECTION_ACTIVITY_CALENDAR"] = "Eventos y llamadas";
$MESS["CRM_FIELD_DEAL_CALENDAR"] = "Registros de calendario";
$MESS["CRM_SECTION_PRODUCT_ROWS"] = "Productos";
$MESS["CRM_SECTION_ACTIVITIES"] = "Acciones";
$MESS["CRM_FIELD_PRODUCT_ROWS"] = "Productos de la negociación";
$MESS["CRM_FIELD_ACTIVITY_LIST"] = "Actividad de la negociación";
$MESS["CRM_FIELD_EXCH_RATE"] = "Tasa de cambio";
$MESS["CRM_FIELD_ADDITIONAL_INFO"] = "Datos del pedido";
$MESS["CRM_SALE_PAYMENTSYSTEM"] = "Sistema de pago";
$MESS["CRM_SALE_DELIVERYSERVICE"] = "Servicio de entrega";
$MESS["CRM_SALE_ORDERPAID"] = "Pedido pagado";
$MESS["CRM_SALE_DELIVERYALLOWED"] = "Entrega permitida";
$MESS["CRM_SALE_CANCELED"] = "Pedido cancelado";
$MESS["CRM_SALE_FINALSTATUS"] = "Orden terminada";
$MESS["CRM_SALE_ORDERSTATUS"] = "Estado del pedido";
$MESS["CRM_SALE_STATUSCHANGEDATE"] = "Ha cambiado el estado de su pedido";
$MESS["CRM_SALE_SITE"] = "Sitio Web";
$MESS["CRM_SALE_NO"] = "No";
$MESS["CRM_SALE_YES"] = "Si";
$MESS["CRM_EXT_SALE_CD_EDIT"] = "Editar";
$MESS["CRM_EXT_SALE_CD_VIEW"] = "Ver";
$MESS["CRM_EXT_SALE_CD_PRINT"] = "Imprimir";
$MESS["CRM_EXT_SALE_CD_CREATE"] = "Crear";
$MESS["CRM_EXT_SALE_DEJ_PRINT"] = "Imprimir";
$MESS["CRM_EXT_SALE_DEJ_SAVE"] = "Guardar";
$MESS["CRM_EXT_SALE_DEJ_ORDER"] = "Orden";
$MESS["CRM_EXT_SALE_DEJ_CLOSE"] = "Cerrar";
$MESS["CRM_SALE_PAYMENTDATE"] = "Fecha de pago";
$MESS["CRM_SALE_BILLNUMBER"] = "Pago  #";
$MESS["CRM_SALE_CUSTOMERCOMMENT"] = "Comentario de cliente";
$MESS["CRM_FIELD_SALE_ORDER1"] = "Pedidos de la tienda Web";
$MESS["CRM_SALE_CONTACT_FULL_NAME"] = "Cliente";
$MESS["CRM_SALE_COMPANY_FULL_NAME"] = "Cliente";
$MESS["CRM_FIELD_CONTACT_PHONE"] = "Teléfonos de contactos";
$MESS["CRM_FIELD_CONTACT_EMAIL"] = "Correos electrónicos de los contactos";
$MESS["CRM_FIELD_CONTACT_POST"] = "Posición";
$MESS["CRM_FIELD_CONTACT_ADDRESS"] = "Dirección";
$MESS["CRM_FIELD_CONTACT_IM"] = "Mis contactos";
$MESS["CRM_FIELD_CONTACT_SOURCE"] = "Origen de contacto";
$MESS["CRM_FIELD_CONTACT_TYPE"] = "Tipo de contacto";
$MESS["CRM_FIELD_COMPANY_INDUSTRY"] = "Industria";
$MESS["CRM_FIELD_COMPANY_PHONE"] = "Teléfonos de la compañía";
$MESS["CRM_FIELD_COMPANY_EMAIL"] = "Correos electrónicos de la compañía";
$MESS["CRM_FIELD_COMPANY_EMPLOYEES"] = "Empleados";
$MESS["CRM_FIELD_COMPANY_REVENUE"] = "Ingresos anuales";
$MESS["CRM_FIELD_COMPANY_TYPE"] = "Tipo de compañía";
$MESS["CRM_FIELD_COMPANY_WEB"] = "Sitios Web";
$MESS["CRM_FIELD_COMPANY_ADDRESS"] = "Dirección física";
$MESS["CRM_FIELD_COMPANY_ADDRESS_LEGAL"] = "Domicilio legal";
$MESS["CRM_FIELD_COMPANY_BANKING_DETAILS"] = "Detalles de pago";
$MESS["CRM_SECTION_DEAL"] = "Información de la negociación";
$MESS["CRM_SECTION_WEB_STORE"] = "Tienda Web";
$MESS["CRM_DEAL_EVENT_INFO"] = "#EVENT_NAME# &rarr; #NEW_VALUE#";
$MESS["CRM_SECTION_ACTIVITY_MAIN"] = "Actividades de la negociación";
$MESS["CRM_SECTION_BIZPROC_MAIN"] = "Business Process de negociaciones";
$MESS["CRM_SECTION_EVENT_MAIN"] = "Registro de la negociación";
$MESS["CRM_SECTION_DETAILS"] = "Detalles";
$MESS["RESPONSIBLE_NOT_ASSIGNED"] = "[sin asignar]";
$MESS["CRM_SECTION_MENU_ACTIVITY_FILTER_NOT_COMPLETED"] = "Actual";
$MESS["CRM_SECTION_MENU_ACTIVITY_FILTER_COMPLETED"] = "Completado";
$MESS["CRM_FIELD_DEAL_INVOICE"] = "Negociación de la factura";
$MESS["CRM_SECTION_LIVE_FEED"] = "Flujo";
$MESS["CRM_FIELD_LIVE_FEED"] = "Flujo";
$MESS["CRM_FIELD_DEAL_QUOTE"] = "Cotización de la negociación";
$MESS["CRM_DEAL_ADD_INVOICE_TITLE"] = "Crear factura de esta negociación ";
$MESS["CRM_FIELD_CLOSEDATE"] = "Fecha de cierre";
$MESS["CRM_FIELD_DEAL_QUOTE_LIST"] = "Negociación de la cotización";
$MESS["CRM_DEAL_QUOTE_LINK"] = "Crear negociaciones en base a la cotización <a href='#URL#'>#TITLE#</a>";
$MESS["CRM_DEAL_SHOW_FIELD_CLIENT"] = "Cliente";
$MESS["CRM_DEAL_SHOW_CONTACT_SELECTOR_HEADER"] = "Contactos que participan en la negociación";
$MESS["CRM_FIELD_UTM"] = "Parámetros UTM";
$MESS["CRM_FIELD_ENTITY_TREE"] = "Dependencias";
$MESS["CRM_DEAL_NAV_TITLE_LIST_SHORT"] = "Negociaciones";
$MESS["CRM_SECTION_RECURRING_ROWS"] = "Negociaciones recurrentes";
$MESS["CRM_FIELD_RECURRING_ACTIVE"] = "Activo";
$MESS["CRM_FIELD_RECURRING_NEXT_EXECUTION"] = "La siguiente negociación se creará el";
$MESS["CRM_FIELD_RECURRING_LAST_EXECUTION"] = "La última negociación fue creada el ";
$MESS["CRM_FIELD_RECURRING_COUNTER_REPEAT"] = "Negociaciones creadas";
$MESS["CRM_EXTERNAL_SALE_NOT_FOUND"] = "Conexión con la tienda web no fue encontrada.";
?>