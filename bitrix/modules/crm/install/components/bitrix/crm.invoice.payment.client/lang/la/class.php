<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "El módulo e-Store no está instalado.";
$MESS["CIPC_EMPTY_PAY_SYSTEM"] = "No hay ningún sistema de pago seleccionado";
$MESS["CIPC_WRONG_ACCOUNT_NUMBER"] = "Factura con el número especificado no fue encontrado";
$MESS["CIPC_WRONG_LINK"] = "No se han encontrado entradas.";
$MESS["CIPC_ERROR_PAYMENT_EXECUTION"] = "Error al ejecutar el pago";
$MESS["CIPC_TITLE_COMPONENT"] = "Factura pagada";
?>