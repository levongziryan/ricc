<?
$MESS["CIPC_TPL_BUTTON_LOAD"] = "Descargar";
$MESS["CIPC_TPL_BUTTON_PRINT"] = "Imprimir";
$MESS["CIPC_TPL_PAY_SYSTEM_BLOCK_HEADER"] = "Mostrar sistemas de pago";
$MESS["CIPC_TPL_SUM_PAYMENT"] = "Importe a pagar";
$MESS["CIPC_TPL_PAY_FOR"] = "Pagar con";
$MESS["CIPC_TPL_BANK_PROPS"] = "Detalles del pago";
$MESS["CIPC_TPL_RETURN_LIST"] = "Volver a las opciones de pago";
$MESS["CIPC_TPL_BITRIX_SIGN"] = "Desarrollado por #LOGO#, CRM gratis";
$MESS["CIPC_TPL_PAID"] = "Pagado";
$MESS["CIPC_TPL_PAID_TITLE"] = "Factura ##INVOICE_ID# of #DATE_BILL#";
$MESS["CIPC_TPL_PAID_SYSTEM"] = "Pagado con";
$MESS["CIPC_TPL_PAID_SUM"] = "Cantidad pagada";
$MESS["CIPC_TPL_PAID_DATE"] = "Fecha de pago";
?>