<?
$MESS["CIPC_TPL_BUTTON_LOAD"] = "Télécharger";
$MESS["CIPC_TPL_BUTTON_PRINT"] = "Imprimer";
$MESS["CIPC_TPL_PAY_SYSTEM_BLOCK_HEADER"] = "Afficher les systèmes de paiement";
$MESS["CIPC_TPL_SUM_PAYMENT"] = "Montant payable";
$MESS["CIPC_TPL_PAY_FOR"] = "Payer via";
$MESS["CIPC_TPL_BANK_PROPS"] = "Détails du paiement";
$MESS["CIPC_TPL_RETURN_LIST"] = "Retour aux options de paiement";
$MESS["CIPC_TPL_BITRIX_SIGN"] = "Proposé par #LOGO#, la GRC gratuite";
$MESS["CIPC_TPL_PAID"] = "Payée";
$MESS["CIPC_TPL_PAID_TITLE"] = "Facture ##INVOICE_ID# du #DATE_BILL#";
$MESS["CIPC_TPL_PAID_SYSTEM"] = "Payée avec";
$MESS["CIPC_TPL_PAID_SUM"] = "Montant payé";
$MESS["CIPC_TPL_PAID_DATE"] = "Date du paiement";
?>