<?
$MESS["CRM_1C_START_EXCHANGE_NAME"] = "CRM-Integration";
$MESS["CRM_1C_START_EXCHANGE_ADV_TITLE"] = "Nutzen Sie 1C, damit die offline Verkaufsdaten in Ihrem Bitrix24 verfügbar werden";
$MESS["CRM_1C_START_EXCHANGE_ADV_1"] = "Einfache Verbindung";
$MESS["CRM_1C_START_EXCHANGE_ADV_2"] = "Sofortiger Datenaustausch";
$MESS["CRM_1C_START_EXCHANGE_ADV_3"] = "Zahlungskontrolle";
$MESS["CRM_1C_START_EXCHANGE_INFO_TITLE"] = "Zwei Optionen für Integration von Bitrix24 und 1C: Rechnungen und Kommerzieller Katalog";
$MESS["CRM_1C_START_EXCHANGE_INFO_TEXT"] = "Integration mit 1C stellt sicher, dass der Produktkatalog in Ihrem CRM immer aktuell ist. Ihre Mitarbeiter werden immer wissen, welchen Status Ihre Rechnungen haben.";
$MESS["CRM_1C_START_EXCHANGE_INFO_1"] = "Im CRM erstellte Rechnungen";
$MESS["CRM_1C_START_EXCHANGE_INFO_2"] = "Rechnungen an 1C senden";
$MESS["CRM_1C_START_EXCHANGE_INFO_3"] = "Nutzen Sie 1C, um Rechnungen zu bezahlen, Bestellungen auszuliefern, Produkte und deren Parameter zu bearbeiten. Alle diese Daten werden mit dem CRM synchronisiert.";
$MESS["CRM_1C_START_EXCHANGE_INFO_4"] = "Werden die Daten im CRM aktualisiert, werden alle Änderungen auch mit 1C synchronisiert.";
$MESS["CRM_1C_START_EXCHANGE_NOTICE"] = "Produktaustausch funktioniert in einer Richtung: alle Änderungen. Die an Produkten in 1C vorgenommen wurden, werden im CRM wiederspiegelt.";
$MESS["CRM_1C_START_EXCHANGE_DO_START"] = "1C:Enterprise Datenaustausch aktivieren";
?>