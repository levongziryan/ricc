<?
$MESS["CRM_1C_START_REPORT_NAME"] = "1C Berichte";
$MESS["CRM_1C_START_REPORT_ADV_TITLE"] = "Zeigt 1C Berichte im Activity Stream an";
$MESS["CRM_1C_START_REPORT_ADV_1"] = "Einfache Verbindung";
$MESS["CRM_1C_START_REPORT_ADV_2"] = "Praktisch für Diskussionen";
$MESS["CRM_1C_START_REPORT_ADV_3"] = "Kontrolle über offline Verkaufsprozesse";
$MESS["CRM_1C_START_REPORT_INFO_TITLE"] = "Nutzen Sie diese App, um Bitrix24 Berichte im Bitrix24 zu veröffentlichen.";
$MESS["CRM_1C_START_REPORT_INFO_TEXT"] = "Veröffentlichen Sie Berichte im Activity Stream manuell oder richten Sie einen wiederkehrenden Export ein, und Sie erhalten täglich einen benötigten Bericht.";
$MESS["CRM_1C_START_REPORT_INFO_1"] = "Kontrolle über offline Verkaufsprozesse";
$MESS["CRM_1C_START_REPORT_INFO_2"] = "1C Berichte und Dokumente anzeigen";
$MESS["CRM_1C_START_REPORT_INFO_3"] = "Besprechen Sie Ihre Berichte";
$MESS["CRM_1C_START_REPORT_INFO_4"] = "Automatisieren Sie die Veröffentlichung der 1C Berichte im Activity Stream";
$MESS["CRM_1C_START_REPORT_INFO_5"] = "Zeigen Sie 1C Berichte mithilfe der Bitrix24 App auf Ihrem mobilen Gerät an";
$MESS["CRM_1C_START_REPORT_DO_START"] = "Verbinden";
?>