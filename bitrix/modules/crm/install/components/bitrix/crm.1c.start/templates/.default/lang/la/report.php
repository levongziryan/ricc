<?
$MESS["CRM_1C_START_REPORT_NAME"] = "1C Reportes";
$MESS["CRM_1C_START_REPORT_ADV_TITLE"] = "Muestra los reportes de 1C en el flujo de actividad";
$MESS["CRM_1C_START_REPORT_ADV_1"] = "Fácil conectividad";
$MESS["CRM_1C_START_REPORT_ADV_3"] = "Control de ventas sin conexión";
$MESS["CRM_1C_START_REPORT_INFO_TITLE"] = "Utilice esta aplicación para publicar informes 1C en Bitrix24.";
$MESS["CRM_1C_START_REPORT_INFO_TEXT"] = "Publique los informes en el flujo de actividades manualmente o configure una exportación periódica y obtenga el inventario diario o cualquier otro informe.";
$MESS["CRM_1C_START_REPORT_INFO_1"] = "Control de ventas sin conexión";
$MESS["CRM_1C_START_REPORT_INFO_2"] = "Ver informes y documentos de 1C";
$MESS["CRM_1C_START_REPORT_INFO_3"] = "Debatir informes";
$MESS["CRM_1C_START_REPORT_INFO_4"] = "Automatice la publicación de los informes 1C en Flujo de Actividad";
$MESS["CRM_1C_START_REPORT_INFO_5"] = "Ver informes 1C en tu teléfono con la aplicación Bitrix24";
$MESS["CRM_1C_START_REPORT_DO_START"] = "Conectar";
$MESS["CRM_1C_START_REPORT_ADV_2"] = "Debate sobre oportunidades";
?>