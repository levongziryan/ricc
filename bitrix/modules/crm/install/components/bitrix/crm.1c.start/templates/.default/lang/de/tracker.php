<?
$MESS["CRM_1C_START_TRACKER_NAME"] = "1C-Tracker";
$MESS["CRM_1C_START_TRACKER_ADV_TITLE"] = "Speichern Sie die Daten in Ihrem CRM ab";
$MESS["CRM_1C_START_TRACKER_ADV_1"] = "Einfache Verbindung";
$MESS["CRM_1C_START_TRACKER_ADV_2"] = "Sofortiger Datenaustausch";
$MESS["CRM_1C_START_TRACKER_ADV_3"] = "Vollständiges Kundenprofil";
$MESS["CRM_1C_START_TRACKER_INFO_TITLE"] = "Nutzen Sie 1C-Tracker, um die Daten Ihrer Kunden zu verwalten";
$MESS["CRM_1C_START_TRACKER_INFO_TEXT"] = "Speichern Sie die Daten aus 1C Tracker direkt in Ihrem CRM ab.";
$MESS["CRM_1C_START_TRACKER_INFO_1"] = "Speichern Sie alle Verkaufsinformationen im CRM ab";
$MESS["CRM_1C_START_TRACKER_INFO_2"] = "Kundenprofil wird automatisch vervollständigt";
$MESS["CRM_1C_START_TRACKER_INFO_3"] = "Sofortiger Datenaustausch zwischen 1C und Bitrix24";
$MESS["CRM_1C_START_TRACKER_INFO_4"] = "Einfache Verbindung";
$MESS["CRM_1C_START_TRACKER_INFO_5"] = "Verkaufshistory aus 1C exportieren";
$MESS["CRM_1C_START_TRACKER_INFO_6"] = "Dokumente aus 1C: Buchhaltung und anderen 1C Systemen exportieren";
$MESS["CRM_1C_START_TRACKER_INFO_7"] = "Identifizieren Sie eine Kunden und finden Sie die Dubletten nach Namen, Telefon und E-Mail";
$MESS["CRM_1C_START_TRACKER_INFO_8"] = "Verbinden Sie Bitrix24 mit so vielen 1C wie erforderlich";
$MESS["CRM_1C_START_TRACKER_DO_START"] = "Verbinden";
?>