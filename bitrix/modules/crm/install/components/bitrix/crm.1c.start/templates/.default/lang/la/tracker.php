<?
$MESS["CRM_1C_START_TRACKER_NAME"] = "1C Tracker";
$MESS["CRM_1C_START_TRACKER_ADV_TITLE"] = "Guardar datos en su CRM";
$MESS["CRM_1C_START_TRACKER_ADV_1"] = "Conectividad fácil";
$MESS["CRM_1C_START_TRACKER_ADV_2"] = "Intercambio instantáneo de datos";
$MESS["CRM_1C_START_TRACKER_ADV_3"] = "Perfil completo del cliente";
$MESS["CRM_1C_START_TRACKER_INFO_TITLE"] = "Utilice 1C Tracker para completar los datos de pool de clientes";
$MESS["CRM_1C_START_TRACKER_INFO_TEXT"] = "Guarde los datos en su CRM desde 1C Tracker en tiempo real.";
$MESS["CRM_1C_START_TRACKER_INFO_1"] = "Almacene toda la información de compra del cliente en el CRM";
$MESS["CRM_1C_START_TRACKER_INFO_2"] = "Auto agrega más rasgos al perfil del cliente";
$MESS["CRM_1C_START_TRACKER_INFO_3"] = "Intercambio de datos Instant 1C a Bitrix24";
$MESS["CRM_1C_START_TRACKER_INFO_4"] = "Fácil conexión";
$MESS["CRM_1C_START_TRACKER_INFO_5"] = "Historial de ventas de exportación desde 1C";
$MESS["CRM_1C_START_TRACKER_INFO_6"] = "Exportar documentos de 1C: Contabilidad y otros sistemas 1C";
$MESS["CRM_1C_START_TRACKER_INFO_7"] = "Identificar un cliente y buscar duplicados por nombre, teléfono, correo electrónico";
$MESS["CRM_1C_START_TRACKER_INFO_8"] = "Conecte tantos 1C a uno Bitrix24 como sea necesario";
$MESS["CRM_1C_START_TRACKER_DO_START"] = "Conectar";
?>