<?
$MESS["CRM_CONFIGS_LINK_TEXT"] = "Pour les réglages de la CRM";
$MESS["CRM_CONFIGS_LINK_TITLE"] = "Passage aux réglages CRM";
$MESS["CRM_EXCH1C_CONNECT_SECTION_TITLE"] = "Connexion à '1C: Enterprise'";
$MESS["CRM_EXCH1C_SYNCSERV_SECTION_TITLE"] = "Services de synchronisation";
$MESS["CRM_EXCH1C_CONNECT_COMMENT_P1"] = "Vous pouvez configurer l'intégration avec '1C: Enterprise' dans les éditions suivantes: '1C: Gestion du commerce', '1C: Gestion d'une petite entreprise', '1C: Automatisation complexe' etc.";
$MESS["CRM_EXCH1C_CONNECT_COMMENT_P2"] = "Dans un dialogue de configuration du profil d'échange dans '1C:Entreprise' il faut indiquer #URL# pour l'adresse du site et dans un champ 'Login' il faut indiquer un login de l'utilisateur CRM ayant le droit pour 'Ajuster le CRM' (par exemple, votre login et votre mot de passe).";
$MESS["CRM_EXCH1C_INV_SETTINGS_LINK_TITLE"] = "Régler la synchronisation les factures avec 1C";
$MESS["CRM_EXCH1C_CAT_SETTINGS_LINK_TITLE"] = "Régler la synchronisation du catalogue des marchandises avec 1C";
$MESS["CRM_EXCH1C_ENABLED_TITLE"] = "Permettre l'échange de données avec '1?:Entreprise'";
$MESS["CRM_EXCH1C_CONNECT_COMMENT_P3"] = "Pour recevoir l'information complète sur le réglage et l'utilisation de l'intégration avec '1C:Enterprise' vous pouvez vous adresser à";
$MESS["CRM_EXCH1C_CONNECT_COMMENT_P3_1"] = "Dans notre blog.";
?>