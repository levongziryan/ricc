<?
$MESS["CRM_EXCH1C_COMMENT_FREE_P2"] = "Grâce à l'intégration votre CRM affiche toujours le catalogue des marchandises le plus récent, liste de prix 'frais' et données correctes sur les résidus de produits en stock. Vous n'avez pas besoin de demander les données et de les dupliquer dans de différents systèmes. En outre, toutes les factures en CRM seront automatiquement envoyées à '1C'. Le fait de leur paiement sera reflété dans CRM et les gestionnaires seront toujours au courant de la facturation. En savoir plus";
$MESS["CRM_EXCH1C_COMMENT_FREE_P1"] = "Dans le CRM 'Bitrix24'un échange de données bidirectionnel avec '1C' est réalisé automatiquement. Les fonctionnalités de facturation du CRM et du catalogue de produits sont intégrées dans les produits '1C'.";
$MESS["CRM_EXCH1C_BITRIX24_LINK2"] = "Choisir un plan tarifaire convenable";
$MESS["CRM_EXCH1C_COMMENT_FREE_P3"] = "Pour faire cette intégration vous avez besoin d'avoir le logiciel '1C Entreprise' dans ses rédactions 'Gestion de commerce', 'Gestion d'une petite entreprise'.";
$MESS["CRM_EXCH1C_BITRIX24_LINK1"] = "Ici";
$MESS["CRM_CONFIGS_LINK_TEXT"] = "Pour les réglages de la CRM";
$MESS["CRM_CONFIGS_LINK_TITLE"] = "Passage aux réglages CRM";
$MESS["CRM_EXCH1C_CONNECT_SECTION_TITLE"] = "Connexion à '1C: Enterprise'";
$MESS["CRM_EXCH1C_COMMENT_FREE_P4"] = "Pour connecter l'intégration de CRM et '1C', vous devez passer à n'importe quel plan tarifaire 'Bitrix24' à commencer par 'Instructions'.";
?>