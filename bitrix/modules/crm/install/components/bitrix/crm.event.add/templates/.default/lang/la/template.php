<?
$MESS["CRM_EVENT_ADD_TITLE"] = "Agregar nuevo evento";
$MESS["CRM_EVENT_ADD_BUTTON"] = "Agregar";
$MESS["CRM_EVENT_DESC_TITLE"] = "Descripción del evento";
$MESS["CRM_EVENT_ADD_ID"] = "Tipo de evento";
$MESS["CRM_EVENT_ADD_FILE"] = "Adjuntar Archivo";
$MESS["CRM_EVENT_TITLE_COMPANY"] = "Compañía";
$MESS["CRM_EVENT_TITLE_DEAL"] = "Negociación";
$MESS["CRM_EVENT_TITLE_CONTACT"] = "Contacto";
$MESS["CRM_EVENT_TITLE_LEAD"] = "Prospecto";
$MESS["CRM_EVENT_STAGE_ID"] = "Etapa de la Negociación";
$MESS["CRM_EVENT_STATUS_ID"] = "Estado del Prospecto";
$MESS["CRM_EVENT_DATE"] = "Fecha del Evento";
$MESS["CRM_EVENT_PHONE_OBSOLETE"] = "La opción \"Llamada telefónica\" está en desuso, sólo existe para la compatibilidad con versiones anteriores. Por favor, utilice el botón \"Nueva llamada\".";
$MESS["CRM_EVENT_MESSAGE_OBSOLETE"] = "La opción \"E-mail ha sido enviado\" está en desuso, sólo existe para la compatibilidad con versiones anteriores. Por favor, utilice el botón \"Enviar mensaje\".";
$MESS["CRM_EVENT_TITLE_QUOTE"] = "Cotización";
$MESS["CRM_EVENT_QUOTE_STATUS_ID"] = "Estados de la cotización";
?>