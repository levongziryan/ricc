<?
$MESS["CRM_EVENT_ADD_TITLE"] = "Créer un nouvel événement";
$MESS["CRM_EVENT_ADD_BUTTON"] = "Ajouter";
$MESS["CRM_EVENT_DESC_TITLE"] = "Veuillez saisir la description de l'événement";
$MESS["CRM_EVENT_ADD_ID"] = "Type d'événement";
$MESS["CRM_EVENT_ADD_FILE"] = "Attacher le fichier";
$MESS["CRM_EVENT_TITLE_COMPANY"] = "Entreprise";
$MESS["CRM_EVENT_TITLE_DEAL"] = "Affaire";
$MESS["CRM_EVENT_TITLE_CONTACT"] = "Client";
$MESS["CRM_EVENT_TITLE_LEAD"] = "Prospect";
$MESS["CRM_EVENT_STAGE_ID"] = "Stade du marché";
$MESS["CRM_EVENT_STATUS_ID"] = "Statut du prospect";
$MESS["CRM_EVENT_DATE"] = "Date de l'événement";
$MESS["CRM_EVENT_PHONE_OBSOLETE"] = "Le type 'Appel téléphonique' est périmé et laissé seulement pour assurer la rétrocompatibilité. Veuille utiliser l'instruction 'Ajouter un appel'.";
$MESS["CRM_EVENT_MESSAGE_OBSOLETE"] = "Le type 'Adresse email envoyé' est périmé, et il ne sert qu'à assurer une compatibilité inverse. Veuillez utiliser l'instruction 'Écrire une lettre'.";
$MESS["CRM_EVENT_TITLE_QUOTE"] = "Offre";
$MESS["CRM_EVENT_QUOTE_STATUS_ID"] = "Statut de l'offre";
?>