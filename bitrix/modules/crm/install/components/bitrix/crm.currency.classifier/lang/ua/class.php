<?
$MESS["CRM_CURRENCY_CLASSIFIER_MODULE_NOT_INSTALLED_CRM"] = "Модуль CRM не встановлено.";
$MESS["CRM_CURRENCY_CLASSIFIER_MODULE_NOT_INSTALLED_CURRENCY"] = "Модуль Валюти не встановлено.";
$MESS["CRM_CURRENCY_CLASSIFIER_CURRENCY_NOT_FOUND"] = "Валюта #currency# не знайдена";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_INCORRECT_VALUE_ERROR"] = "Значення некоректно або занадто велике";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_MAX_LENGTH_ERROR"] = "Значення не повинно перевищувати #max_length# символів";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_SYM_CODE_ERROR"] = "Символьний код повинен складатися з 3-х символів латинського алфавіту";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_FULL_NAME_ERROR"] = "Назва валюти не повинна бути порожньою  або перевищувати 50 символів";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_THOUSANDS_VARIANT_OWN"] = "Інше значення";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_SIGN_POSITION_BEFORE"] = "Перед сумою";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_SIGN_POSITION_AFTER"] = "Після суми";
$MESS["CRM_CURRENCY_CLASSIFIER_ADD_UNKNOWN_ERROR"] = "Під час створення валюти сталася невідома помилка";
$MESS["CRM_CURRENCY_CLASSIFIER_UPDATE_UNKNOWN_ERROR"] = "Під час оновлення валюти сталася невідома помилка";
?>