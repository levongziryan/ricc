<?
$MESS["CRM_CURRENCY_CLASSIFIER_FORM_SWITCHER_CLASSIFIER"] = "Agregar desde el registro";
$MESS["CRM_CURRENCY_CLASSIFIER_SECTION_SEARCH"] = "Encontrar moneda";
$MESS["CRM_CURRENCY_CLASSIFIER_SECTION_PROPERTIES"] = "Propiedades de la moneda";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_SEARCH"] = "Encontrar moneda";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_SEARCH_PLACEHOLDER"] = "Comience a escribir...";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_SELECT"] = "Seleccione el tipo de moneda";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_NUM_CODE"] = "Código numérico";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_SYM_CODE"] = "Código simbólico";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_EXCHANGE_RATE"] = "Tasa de cambio (predeterminado)";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_SORT_INDEX"] = "Índice de clasificación";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_BASE_FOR_REPORTS"] = "Moneda de referencia";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_BASE_FOR_COUNT"] = "Moneda de facturación predeterminada";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_FULL_NAME"] = "Nombre";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_FORMAT_TEMPLATE"] = "Plantilla de formato";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_SIGN"] = "Símbolo de moneda";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_SIGN_POSITION"] = "Posición del símbolo";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_ALL_PARAMETERS_SHOW"] = "Mostrar parámetros adicionales";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_ALL_PARAMETERS_HIDE"] = "Ocultar parámetros adicionales";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_THOUSANDS_VARIANT"] = "Separador de miles";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_DEC_POINT"] = "Punto decimal";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_DECIMALS"] = "Cantidad de dígitos decimales";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_EXAMPLE"] = "Ejemplo de formato de moneda";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_HIDE_ZERO"] = "Ocultar ceros no significativos";
$MESS["CRM_CURRENCY_CLASSIFIER_FORM_BUTTONS_SAVE"] = "Guardar";
$MESS["CRM_CURRENCY_CLASSIFIER_FORM_BUTTONS_APPLY"] = "Aplicar";
$MESS["CRM_CURRENCY_CLASSIFIER_FORM_BUTTONS_CANCEL"] = "Cancelar";
$MESS["CRM_CURRENCY_CLASSIFIER_FORM_POPUP_WINDOW_TITLE"] = "Atención!";
$MESS["CRM_CURRENCY_CLASSIFIER_FORM_POPUP_WINDOW_MESSAGE"] = "Está a punto de modificar la moneda existente. Está seguro que quiere continuar?";
$MESS["CRM_CURRENCY_CLASSIFIER_FORM_POPUP_WINDOW_YES"] = "Si";
$MESS["CRM_CURRENCY_CLASSIFIER_FORM_POPUP_WINDOW_NO"] = "No";
$MESS["CRM_CURRENCY_CLASSIFIER_FORM_SWITCHER_MANUALLY"] = "Agregar manualmente";
?>