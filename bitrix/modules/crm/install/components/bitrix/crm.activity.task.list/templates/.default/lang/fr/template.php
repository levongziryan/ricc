<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_TASK_DELETE_CONFIRM"] = "Etes-vous sûr de vouloir supprimer?";
$MESS["CRM_TASK_SHOW_TITLE"] = "Affichage de la tâche";
$MESS["CRM_TASK_SHOW"] = "Affichage de la tâche";
$MESS["CRM_TASK_EDIT_TITLE"] = "Changer la tâche";
$MESS["CRM_TASK_EDIT"] = "Changer la tâche";
$MESS["CRM_TASK_DELETE_TITLE"] = "Supprimer la Tâche";
$MESS["CRM_TASK_DELETE"] = "Supprimer la Tâche";
?>