<?
$MESS["CRM_COLUMN_EMAIL"] = "E-mail";
$MESS["CRM_COLUMN_ADDRESS"] = "Адреса";
$MESS["CRM_COLUMN_DATE_MODIFY"] = "Дата змінення";
$MESS["CRM_COLUMN_DATE_CREATE"] = "Дата створення";
$MESS["CRM_COLUMN_BIRTHDATE"] = "День народження";
$MESS["CRM_COLUMN_POST"] = "Посада";
$MESS["CRM_PERMISSION_DENIED"] = "Доступ заборонений";
$MESS["CRM_COLUMN_TITLE"] = "Заголовок";
$MESS["CRM_COLUMN_NAME"] = "Ім'я";
$MESS["CRM_COLUMN_SOURCE"] = "Джерело";
$MESS["CRM_COLUMN_COMMENTS"] = "Коментар";
$MESS["CRM_COLUMN_MESSENGER"] = "Месенджер";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Модуль CRM не встановлений.";
$MESS["CRM_COLUMN_SOURCE_DESCRIPTION"] = "Опис";
$MESS["CRM_COLUMN_ASSIGNED_BY"] = "Відповідальний";
$MESS["CRM_COLUMN_SECOND_NAME"] = "По батькові";
$MESS["CRM_COLUMN_WEB"] = "Сайт";
$MESS["CRM_COLUMN_PHONE"] = "Телефон";
$MESS["CRM_COLUMN_TYPE"] = "Тип контакту";
$MESS["CRM_COLUMN_LAST_NAME"] = "Прізвище";
$MESS["CRM_COLUMN_COMMENT"] = "Коментар";
?>