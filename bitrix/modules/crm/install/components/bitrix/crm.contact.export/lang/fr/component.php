<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'a pas été installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
$MESS["CRM_COLUMN_NAME"] = "Prénom";
$MESS["CRM_COLUMN_LAST_NAME"] = "Nom";
$MESS["CRM_COLUMN_SECOND_NAME"] = "Nom de jeune fille";
$MESS["CRM_COLUMN_BIRTHDATE"] = "Date de naissance";
$MESS["CRM_COLUMN_TITLE"] = "Titre";
$MESS["CRM_COLUMN_PHONE"] = "Téléphone";
$MESS["CRM_COLUMN_WEB"] = "Site";
$MESS["CRM_COLUMN_MESSENGER"] = "Messagerie";
$MESS["CRM_COLUMN_POST"] = "Position";
$MESS["CRM_COLUMN_ADDRESS"] = "Adresse";
$MESS["CRM_COLUMN_COMMENT"] = "Commentaire";
$MESS["CRM_COLUMN_TYPE"] = "Type de contact";
$MESS["CRM_COLUMN_SOURCE"] = "Source";
$MESS["CRM_COLUMN_ASSIGNED_BY"] = "Responsable";
$MESS["CRM_COLUMN_DATE_CREATE"] = "Créé le";
$MESS["CRM_COLUMN_DATE_MODIFY"] = "Modifié le";
?>