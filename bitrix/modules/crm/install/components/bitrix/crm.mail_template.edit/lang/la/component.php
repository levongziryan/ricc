<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_MAIL_TEMPLATE_NOT_FOUND"] = "No se ha encontrado la plantilla de correo electrónico.";
$MESS["CRM_MAIL_TEMPLATE_IS_ACTIVE"] = "Activo";
$MESS["CRM_MAIL_TEMPLATE_EMAIL_FROM"] = "De";
$MESS["CRM_MAIL_TEMPLATE_TITLE"] = "Nombre";
$MESS["CRM_MAIL_TEMPLATE_SCOPE"] = "Disponible";
$MESS["CRM_MAIL_TEMPLATE_SUBJECT"] = "Asunto";
$MESS["CRM_MAIL_TEMPLATE_BODY"] = "Plantilla";
$MESS["CRM_MAIL_ENTITY_TYPE"] = "Entidad";
$MESS["CRM_MAIL_TEMPLATE_SORT"] = "Ordenamiento";
$MESS["CRM_MAIL_TEMPLATE_ADD_UNKNOWN_ERROR"] = "Error al crear la plantilla de correo electrónico.";
$MESS["CRM_MAIL_TEMPLATE_UPDATE_UNKNOWN_ERROR"] = "Error al actualizar la plantilla de correo electrónico.";
$MESS["CRM_MAIL_TEMPLATE_DELETE_UNKNOWN_ERROR"] = "Error al eliminar la plantilla de correo electrónico.";
?>