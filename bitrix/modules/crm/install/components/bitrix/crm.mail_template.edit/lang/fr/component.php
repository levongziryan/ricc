<?
$MESS["CRM_MAIL_TEMPLATE_IS_ACTIVE"] = "Actif(ve)";
$MESS["CRM_MAIL_TEMPLATE_UPDATE_UNKNOWN_ERROR"] = "Une erreur est survenue lors de la mise à jour du modèle postal.";
$MESS["CRM_MAIL_TEMPLATE_ADD_UNKNOWN_ERROR"] = "Lors de la création du modèle de messagerie une erreur s'est produite.";
$MESS["CRM_MAIL_TEMPLATE_DELETE_UNKNOWN_ERROR"] = "Une erreur a eu lieu lors de la suppression du modèle postal.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_MAIL_TEMPLATE_SCOPE"] = "Accessible";
$MESS["CRM_MAIL_TEMPLATE_SORT"] = "Classification";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_MAIL_TEMPLATE_TITLE"] = "Dénomination";
$MESS["CRM_MAIL_TEMPLATE_EMAIL_FROM"] = "De qui";
$MESS["CRM_MAIL_TEMPLATE_NOT_FOUND"] = "Modèle de courrier introuvable.";
$MESS["CRM_MAIL_ENTITY_TYPE"] = "Bloc Highload";
$MESS["CRM_MAIL_TEMPLATE_SUBJECT"] = "En-tête";
$MESS["CRM_MAIL_TEMPLATE_BODY"] = "Modèle";
?>