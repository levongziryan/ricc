<?
$MESS["CRM_TEMPLATE_EDIT_EVENT_SUCCESSFULLY_CREATED"] = "Le modèle de message <a href='#URL#'>#TITLE#</a> a été créé. Vous êtes maintenant redirigé vers la page précédente. Si la page actuelle est toujours affichée, veuillez la fermer manuellement.";
$MESS["CRM_TEMPLATE_EDIT_EVENT_CANCELED"] = "Une action a été annulée. Vous êtes maintenant redirigé vers la page précédente. Si la page actuelle est toujours affichée, veuillez la fermer manuellement.";
?>