<?
$MESS["CRM_BP_CX_BP_EDIT"] = "URL de l'édition de l'image";
$MESS["CRM_BP_CX_ENTITY_LIST"] = "URL de la liste de types";
$MESS["CRM_BP_CX_BP_LIST"] = "URL de la liste des modèles";
$MESS["CRM_BP_CX_MODE"] = "Identificateur du régime";
$MESS["CRM_BP_CX_ENTITY_ID"] = "Identificateur du type";
$MESS["CRM_BP_CX_BP_ID"] = "Identifiant du modèle";
?>