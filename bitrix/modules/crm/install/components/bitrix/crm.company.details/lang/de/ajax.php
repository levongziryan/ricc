<?
$MESS["CRM_COMPANY_NOT_FOUND"] = "Unternehmen wurde nicht gefunden.";
$MESS["CRM_COMPANY_ACCESS_DENIED"] = "Zugriff verweigert.";
$MESS["CRM_COMPANY_DELETION_ERROR"] = "Fehler beim Löschen des Unternehmens.";
?>