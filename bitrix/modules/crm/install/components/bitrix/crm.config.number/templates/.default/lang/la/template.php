<?
$MESS["CRM_NUMBER_TEMPLATE_EXAMPLE"] = "Ejemplo:";
$MESS["CRM_NUMBER_NUMBER"] = "Número inicial:";
$MESS["CRM_NUMBER_NUMBER_DESC"] = "1 a 7 caracteres. El nuevo valor debe ser mayor que la anterior.";
$MESS["CRM_NUMBER_PREFIX"] = "Prefijo:";
$MESS["CRM_NUMBER_DATE"] = "Período:";
$MESS["CRM_NUMBER_DATE_1"] = "Diario";
$MESS["CRM_NUMBER_DATE_2"] = "Mensual";
$MESS["CRM_NUMBER_DATE_3"] = "Anual";
$MESS["CRM_NUMBER_RANDOM"] = "Número de caracteres:";
$MESS["CRM_NUMBER_WARNING"] = "Prefijo:";
$MESS["CRM_NUMBER_TEMPL"] = "Plantilla:";
$MESS["CRM_NUMBER_PREFIX_DESC"] = "1 a 7 caracteres (use las letras latinas, números, signos de menos y guiones). Ejemplo con prefijo: TEST_1234";
?>