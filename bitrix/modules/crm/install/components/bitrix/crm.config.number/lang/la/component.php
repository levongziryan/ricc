<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado.";
$MESS["CRM_NUMBER_TEMPLATE_0"] = "No utilizado";
$MESS["CRM_NUMBER_TEMPLATE_1"] = "Iniciar numeración desde";
$MESS["CRM_NUMBER_TEMPLATE_2"] = "Utilizar el prefijo";
$MESS["CRM_NUMBER_TEMPLATE_3"] = "Número aleatorio";
$MESS["CRM_NUMBER_TEMPLATE_5"] = "Reiniciar numeración periódicamente";
$MESS["CRM_NUMBER_TEMPLATE_4"] = "ID y número de documento del usuario";
?>