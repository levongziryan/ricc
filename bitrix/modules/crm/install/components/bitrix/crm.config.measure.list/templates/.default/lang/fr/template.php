<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_MEASURE_DELETE_CONFIRM"] = "Vous voulez réellement supprimer '#MEASURE_TITLE#'?";
$MESS["CRM_MEASURE_SHOW_TITLE"] = "Accéder à la page de l'affichage de cette unité de mesure";
$MESS["CRM_MEASURE_EDIT_TITLE"] = "Accéder à la page de l'édition de cette unité de mesure";
$MESS["CRM_MEASURE_SHOW"] = "Examiner l'unité de mesure";
$MESS["CRM_MEASURE_EDIT"] = "Editer l'unité de mesure";
$MESS["CRM_MEASURE_DELETE"] = "Supprimer l'unité de mesure";
$MESS["CRM_MEASURE_DELETE_TITLE"] = "Supprimer cette unité de mesure";
?>