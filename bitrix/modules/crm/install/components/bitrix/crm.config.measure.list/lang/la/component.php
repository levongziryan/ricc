<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado";
$MESS["CATALOG_MODULE_NOT_INSTALLED"] = "El módulo Commercial Catalog no está instalado";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_CODE"] = "Código";
$MESS["CRM_COLUMN_MEASURE_TITLE"] = "Nombre de la unidad";
$MESS["CRM_COLUMN_SYMBOL_RUS"] = "Símbolo de la unidad";
$MESS["CRM_COLUMN_SYMBOL_INTL"] = "Símbolo de la unidad (internacional)";
$MESS["CRM_COLUMN_SYMBOL_LETTER_INTL"] = "Nombre de código (Intl.)";
$MESS["CRM_COLUMN_IS_DEFAULT"] = "Predeterminado";
?>