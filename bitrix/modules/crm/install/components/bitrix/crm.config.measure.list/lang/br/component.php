<?
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_CODE"] = "Código";
$MESS["CRM_COLUMN_MEASURE_TITLE"] = "Nome da unidade";
$MESS["CRM_COLUMN_SYMBOL_RUS"] = "Símbolo da unidade";
$MESS["CRM_COLUMN_SYMBOL_INTL"] = "Símbolo da unidade (internacional)";
$MESS["CRM_COLUMN_SYMBOL_LETTER_INTL"] = "Nome de código (Intl.)";
$MESS["CRM_COLUMN_IS_DEFAULT"] = "Padrão";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["CATALOG_MODULE_NOT_INSTALLED"] = "O módulo de Catálogo Comercial não está instalado.";
?>