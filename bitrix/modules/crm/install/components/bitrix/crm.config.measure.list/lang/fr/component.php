<?
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_COLUMN_CODE"] = "Code";
$MESS["CRM_COLUMN_SYMBOL_LETTER_INTL"] = "Notation littérale codée (internationale)";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CATALOG_MODULE_NOT_INSTALLED"] = "Le module 'Catalogue de marchandises' n'a pas été installé.";
$MESS["CRM_COLUMN_MEASURE_TITLE"] = "Dénomination de l'unité de mesure";
$MESS["CRM_COLUMN_IS_DEFAULT"] = "Par défaut";
$MESS["CRM_COLUMN_SYMBOL_RUS"] = "Désignation conventionnelle";
$MESS["CRM_COLUMN_SYMBOL_INTL"] = "Désignation conventionnelle (internationale)";
?>