<?
$MESS["CRM_TAX_FIELD_TIMESTAMP_X"] = "Date de modification";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_TAX_FIELD_ID"] = "ID";
$MESS["CRM_TAX_FIELD_CODE"] = "Code mnémonique";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_TAX_FIELD_NAME"] = "Dénomination";
$MESS["CRM_TAX_SECTION_MAIN"] = "Impôt";
$MESS["CRM_TAX_NOT_FOUND"] = "L'impôt n'est pas retrouvé!";
$MESS["CRM_TAX_FIELD_DESCRIPTION"] = "Description";
$MESS["CRM_TAX_FIELD_LID"] = "Site web";
$MESS["CRM_TAX_RATE_LIST"] = "Liste des taux d'impôt";
?>