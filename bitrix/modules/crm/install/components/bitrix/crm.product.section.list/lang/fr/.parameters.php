<?
$MESS["CRM_CATALOG_NOT_SELECTED"] = "[non indiqué]";
$MESS["CRM_SECTION_NOT_SELECTED"] = "[non indiqué]";
$MESS["CRM_SECTION_ID_PARAM"] = "Nom de la variable d'identificateur de la section";
$MESS["CRM_SECTION_COUNT"] = "Nombre de sections par défaut sur une page";
$MESS["CRM_PARENT_SECTION_ID"] = "Créer une section";
$MESS["CRM_CATALOG_ID"] = "Catalogue de marchandises";
$MESS["CRM_PATH_TO_SECTION_LIST"] = "Modèle de chemin d'accès à la page de la liste des rubriques";
?>