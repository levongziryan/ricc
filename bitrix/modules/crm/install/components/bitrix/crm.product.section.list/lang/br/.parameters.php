<?
$MESS["CRM_SECTION_COUNT"] = "Padrão de seções por página";
$MESS["CRM_CATALOG_ID"] = "Catálogo Comercial";
$MESS["CRM_PARENT_SECTION_ID"] = "Seção";
$MESS["CRM_PATH_TO_SECTION_LIST"] = "Modelo de caminho da página seções";
$MESS["CRM_SECTION_ID_PARAM"] = "Nome variável da ID da seção";
$MESS["CRM_CATALOG_NOT_SELECTED"] = "[não selecionado]";
$MESS["CRM_SECTION_NOT_SELECTED"] = "[não selecionado]";
?>