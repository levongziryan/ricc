<?
$MESS["CRM_SECTION_COUNT"] = "Secciones predeterminadas por página";
$MESS["CRM_CATALOG_ID"] = "Catálogo comercial";
$MESS["CRM_PARENT_SECTION_ID"] = "Sección";
$MESS["CRM_PATH_TO_SECTION_LIST"] = "Plantilla de ruta de la página de secciones";
$MESS["CRM_SECTION_ID_PARAM"] = "Nombre de la variable para el ID de sesión";
$MESS["CRM_CATALOG_NOT_SELECTED"] = "[no seleccionado]";
$MESS["CRM_SECTION_NOT_SELECTED"] = "[no seleccionado]";
?>