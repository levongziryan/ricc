<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado. ";
$MESS["CRM_REQUISITE_LIST_ERR_COMPANY_NOT_FOUND"] = "Datos de la lista de compañía no fue encontrada.";
$MESS["CRM_REQUISITE_LIST_ERR_CONTACT_NOT_FOUND"] = "Datos de la lista de contactos no fue encontrada.";
$MESS["CRM_REQUISITE_LIST_ERR_COMPANY_READ_DENIED"] = "Acceso denegado a las compañías.";
$MESS["CRM_REQUISITE_LIST_ERR_CONTACT_READ_DENIED"] = "Acceso denegado a los contactos.";
$MESS["CRM_REQUISITE_LIST_YES"] = "si";
$MESS["CRM_REQUISITE_LIST_NO"] = "no";
$MESS["CRM_REQUISITE_LIST_PRESET_NAME_FIELD"] = "Nombre de la plantilla";
$MESS["CRM_REQUISITE_LIST_ERR_ENTITY_TYPE_ID"] = "Tipo de entidad incorrecto para el contacto o datos de la compañía de la lista proporcionada.
";
$MESS["CRM_REQUISITE_PRESET_NAME_EMPTY"] = "Plantilla sin título";
?>