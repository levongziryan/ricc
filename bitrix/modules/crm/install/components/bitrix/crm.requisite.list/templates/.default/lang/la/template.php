<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_REQUISITE_EDIT"] = "Editar";
$MESS["CRM_REQUISITE_COPY"] = "Copiar";
$MESS["CRM_REQUISITE_DELETE"] = "Eliminar";
$MESS["CRM_REQUISITE_DELETE_CONFIRM"] = "¿Está seguro de que desea eliminar el elemento?";
$MESS["CRM_STATUS_INIT"] = "- Estado de SKU -";
$MESS["CRM_SHOW_ROW_COUNT"] = "Mostrar la cantidad";
$MESS["CRM_JS_STATUS_ACTION_SUCCESS"] = "Éxitoso";
$MESS["CRM_JS_STATUS_ACTION_ERROR"] = "Eso es un error.";
$MESS["CRM_REQUISITE_POPUP_SAVE_BUTTON_TITLE"] = "Guardar";
$MESS["CRM_REQUISITE_POPUP_CANCEL_BUTTON_TITLE"] = "Cerrar";
$MESS["CRM_REQUISITE_PRESET_SELECTOR_TEXT"] = "Agregar nuevo";
$MESS["CRM_REQUISITE_POPUP_TITLE_CONTACT"] = "Datos del contacto";
$MESS["CRM_REQUISITE_POPUP_TITLE_COMPANY"] = "Datos de la compañía";
$MESS["CRM_REQUISITE_EDIT_TITLE_COMPANY"] = "Editar datos de la compañía";
$MESS["CRM_REQUISITE_EDIT_TITLE_CONTACT"] = "Editar datos del contacto";
$MESS["CRM_REQUISITE_COPY_TITLE_COMPANY"] = "Copiar datos de la compañía";
$MESS["CRM_REQUISITE_COPY_TITLE_CONTACT"] = "Copiar datos del contacto";
$MESS["CRM_REQUISITE_DELETE_TITLE_COMPANY"] = "Eliminar datos de la compañía";
$MESS["CRM_REQUISITE_DELETE_TITLE_CONTACT"] = "Eliminar datos del contacto";
$MESS["CRM_REQUISITE_PRESET_SELECTOR_TITLE_COMPANY"] = "Agregar información de la compañía utilizando la plantilla";
$MESS["CRM_REQUISITE_PRESET_SELECTOR_TITLE_CONTACT"] = "Agregar información del contacto utilizando la plantilla";
$MESS["CRM_REQUISITE_POPUP_ERR_PRESET_NOT_SELECTED_COMPANY"] = "No hay plantillas existentes disponibles.";
$MESS["CRM_REQUISITE_POPUP_ERR_PRESET_NOT_SELECTED_CONTACT"] = "No hay plantillas existentes disponibles.";
?>