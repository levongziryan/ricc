<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_REQUISITE_EDIT"] = "Éditer";
$MESS["CRM_REQUISITE_COPY"] = "Copier";
$MESS["CRM_REQUISITE_DELETE"] = "Supprimer";
$MESS["CRM_REQUISITE_DELETE_CONFIRM"] = "Voulez-vous vraiment supprimer cet élément ?";
$MESS["CRM_STATUS_INIT"] = "- Statut UGS -";
$MESS["CRM_SHOW_ROW_COUNT"] = "Afficher la quantité";
$MESS["CRM_JS_STATUS_ACTION_SUCCESS"] = "Réussi";
$MESS["CRM_JS_STATUS_ACTION_ERROR"] = "C'est une erreur.";
$MESS["CRM_REQUISITE_POPUP_SAVE_BUTTON_TITLE"] = "Enregistrer";
$MESS["CRM_REQUISITE_POPUP_CANCEL_BUTTON_TITLE"] = "Fermer";
$MESS["CRM_REQUISITE_PRESET_SELECTOR_TEXT"] = "Ajouter un nouveau";
$MESS["CRM_REQUISITE_POPUP_TITLE_CONTACT"] = "Données de contact";
$MESS["CRM_REQUISITE_POPUP_TITLE_COMPANY"] = "Données de la société";
$MESS["CRM_REQUISITE_EDIT_TITLE_COMPANY"] = "Éditer les données de la société";
$MESS["CRM_REQUISITE_EDIT_TITLE_CONTACT"] = "Éditer les données de contact";
$MESS["CRM_REQUISITE_COPY_TITLE_COMPANY"] = "Copier les données de la société";
$MESS["CRM_REQUISITE_COPY_TITLE_CONTACT"] = "Copier les données de contact";
$MESS["CRM_REQUISITE_DELETE_TITLE_COMPANY"] = "Supprimer les données de la société";
$MESS["CRM_REQUISITE_DELETE_TITLE_CONTACT"] = "Supprimer les données de contact";
$MESS["CRM_REQUISITE_PRESET_SELECTOR_TITLE_COMPANY"] = "Ajouter des données de la société via un modèle";
$MESS["CRM_REQUISITE_PRESET_SELECTOR_TITLE_CONTACT"] = "Ajouter des données de contact via un modèle";
$MESS["CRM_REQUISITE_POPUP_ERR_PRESET_NOT_SELECTED_COMPANY"] = "Aucun modèle existant n'est disponible.";
$MESS["CRM_REQUISITE_POPUP_ERR_PRESET_NOT_SELECTED_CONTACT"] = "Aucun modèle existant n'est disponible.";
?>