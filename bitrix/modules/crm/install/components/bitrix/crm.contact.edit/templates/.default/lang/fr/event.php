<?
$MESS["CRM_CONTACT_EDIT_EVENT_SUCCESSFULLY_CREATED"] = "Le contact <a href='#URL#'>#TITLE#</a> a bien été créé. Vous allez maintenant être redirigé vers la page précédente. Si vous êtes toujours sur cette page, veuillez la fermer manuellement.";
$MESS["CRM_CONTACT_EDIT_EVENT_CANCELED"] = "L'action a été annulée. Vous allez maintenant être redirigé vers la page précédente. Si vous êtes toujours sur cette page, veuillez la fermer manuellement.";
?>