<?
$MESS["CRM_TAB_3"] = "De processus business";
$MESS["CRM_TAB_3_TITLE"] = "Processus d'affaires de la transaction";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_WARNING_ACCEPT_BTN_TITLE"] = "Ignorer et sauvegarder";
$MESS["CRM_TAB_1"] = "Client";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_SUMMARY_TITLE"] = "Coïncidences retrouvées";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_SHORT_SUMMARY_TITLE"] = "Transférer un message uniquement lors d'envoi aux utilisateurs autorisés";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_WARNING_CANCEL_BTN_TITLE"] = "Annuler";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_EMAIL_SUMMARY_TITLE"] = "par e-mail";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_PHONE_SUMMARY_TITLE"] = "par téléphoniquement";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_FULL_NAME_SUMMARY_TITLE"] = "par nom";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_WARNING_DLG_TITLE"] = "Duplicatas suspectés";
$MESS["CRM_IMPORT_SNS"] = "Vous pouvez également utiliser vCard pour ajouter des contacts <br>Ouverts 'Contacts' dans <b>MS Outlook</b> et sélectionner les contacts nécessaires. puis sélectionnez '<b>Actions %ARROW% Envoyer comme carte de visite</b>' sur la barre du haut et l'envoyer à <b>% EMAIL%</b>";
$MESS["CRM_TAB_1_TITLE"] = "Propriétés du contact";
$MESS["CRM_TAB_2"] = "evènements";
$MESS["CRM_TAB_2_TITLE"] = "Evènements du contact";
$MESS["CRM_CONTACT_CREATE_TITLE"] = "Création d'un contact";
$MESS["CRM_CONTACT_EDIT_TITLE"] = "Contactez ID ##ID# &mdash; #NAME#";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_REQUISITE_SUMMARY_TITLE"] = "par données de contact";
$MESS["CRM_CONTACT_EDIT_DUP_CTRL_BANK_DETAIL_SUMMARY_TITLE"] = "par informations bancaires";
?>