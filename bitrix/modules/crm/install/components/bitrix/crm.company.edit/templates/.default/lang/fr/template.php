<?
$MESS["CRM_TAB_1"] = "Entreprise";
$MESS["CRM_TAB_1_TITLE"] = "Attributs de l'entreprise";
$MESS["CRM_TAB_2"] = "evènements";
$MESS["CRM_TAB_2_TITLE"] = "Evènements de l'entreprise";
$MESS["CRM_TAB_3"] = "De processus business";
$MESS["CRM_TAB_3_TITLE"] = "Processus d'affaires de l'entreprise";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_SUMMARY_TITLE"] = "Coïncidences retrouvées";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_WARNING_DLG_TITLE"] = "Duplicatas suspectés";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_WARNING_ACCEPT_BTN_TITLE"] = "Ignorer et sauvegarder";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_WARNING_CANCEL_BTN_TITLE"] = "Annuler";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_TTL_SUMMARY_TITLE"] = "par dénomination";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_PHONE_SUMMARY_TITLE"] = "par téléphoniquement";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_EMAIL_SUMMARY_TITLE"] = "par e-mail";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_SHORT_SUMMARY_TITLE"] = "Transférer un message uniquement lors d'envoi aux utilisateurs autorisés";
$MESS["CRM_COMPANY_EDIT_TITLE"] = "Entreprise ##ID# &mdash; #TITLE#";
$MESS["CRM_COMPANY_CREATE_TITLE"] = "Nouvelle entreprise";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_REQUISITE_SUMMARY_TITLE"] = "par données de la société";
$MESS["CRM_COMPANY_EDIT_DUP_CTRL_BANK_DETAIL_SUMMARY_TITLE"] = "par informations bancaires";
?>