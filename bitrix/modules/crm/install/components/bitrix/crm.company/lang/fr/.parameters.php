<?
$MESS["CRM_ELEMENT_ID"] = "ID de l'entreprise";
$MESS["CRM_COMPANY_VAR"] = "Nom de la variable de l'ID de la entreprise";
$MESS["CRM_NAME_TEMPLATE"] = "Format du nom";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "Modèle de chemin d'accès à la page principale";
$MESS["CRM_SEF_PATH_TO_IMPORT"] = "Importer la page Chemin Template";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "Modèle de Chemin d'accès à la Page de visualisation de l'Entreprise";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "Modèle de chemin d'accès à Page d'Edition de l'entreprise";
$MESS["CRM_SEF_PATH_TO_LIST"] = "Modèle de chemin d'accès à la page de la liste des entreprises";
?>