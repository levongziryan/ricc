<?
$MESS["CRM_COMPANY_WGT_PAGE_TITLE"] = "Empresas: Relatório resumo";
$MESS["CRM_COMPANY_WGT_DEMO_TITLE"] = "Esta é uma visualização de demonstração. Feche-a para obter análises para suas empresas.";
$MESS["CRM_COMPANY_WGT_DEMO_CONTENT"] = "Se você ainda não tem qualquer empresa, <a href=\"#URL#\" class=\"#CLASS_NAME#\">crie uma</a> agora!";
?>