<?
$MESS["CRM_COMPANY_WGT_PAGE_TITLE"] = "Entreprises: Rapport sommaire";
$MESS["CRM_COMPANY_WGT_DEMO_TITLE"] = "Ceci est un affichage démo. Fermez-le pour obtenir les données analytiques pour vos entreprises.";
$MESS["CRM_COMPANY_WGT_DEMO_CONTENT"] = "Si vous n'avez toujours aucune entreprise, <a href=\"#URL#\" class=\"#CLASS_NAME#\">créez-en une</a> maintenant!";
?>