<?
$MESS["BX_CRM_CACA_ERR_TOPIC_ABSENT"] = "O tópico não está especificado.";
$MESS["BX_CRM_CACA_ERR_DATE_FROM_ABSENT"] = "A data de início não é especificada";
$MESS["BX_CRM_CACA_ERR_DATE_FROM_FORMAT"] = "Formato de data inválido final início";
$MESS["BX_CRM_CACA_ERR_DATE_TO_FORMAT"] = "Formato de data inválido final início";
$MESS["BX_CRM_CACA_ERR_DATE_COMPARE"] = "A data de início é posterior à data final";
$MESS["BX_CRM_CACA_ERR_ADD_FAIL"] = "Erro ao adicionar uma nova entrada no calendário";
?>