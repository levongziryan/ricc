<?
$MESS["BX_CRM_CACA_ERR_TOPIC_ABSENT"] = "No se ha especificado el tema.";
$MESS["BX_CRM_CACA_ERR_DATE_FROM_ABSENT"] = "No se ha especificado la fecha de inicio";
$MESS["BX_CRM_CACA_ERR_DATE_FROM_FORMAT"] = "Formato de fecha de inicio no válido";
$MESS["BX_CRM_CACA_ERR_DATE_TO_FORMAT"] = "Formato de fecha final no válido";
$MESS["BX_CRM_CACA_ERR_DATE_COMPARE"] = "La fecha de inicio es posterior a la fecha de finalización";
$MESS["BX_CRM_CACA_ERR_ADD_FAIL"] = "Error al agregar una nueva entrada de calendario";
?>