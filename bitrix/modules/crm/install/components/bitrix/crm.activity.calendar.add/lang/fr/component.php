<?
$MESS["BX_CRM_CACA_ERR_DATE_COMPARE"] = "Date de début est postérieure à la date de fin";
$MESS["BX_CRM_CACA_ERR_DATE_FROM_ABSENT"] = "La date de début n'est pas indiquée";
$MESS["BX_CRM_CACA_ERR_TOPIC_ABSENT"] = "Le sujet n'est pas indiqué.";
$MESS["BX_CRM_CACA_ERR_DATE_FROM_FORMAT"] = "Format incorrect de la date de début";
$MESS["BX_CRM_CACA_ERR_DATE_TO_FORMAT"] = "Mauvais format de la date de fin";
$MESS["BX_CRM_CACA_ERR_ADD_FAIL"] = "Une erreur s'est produite lors de l'ajout de la note dans le calendrier";
?>