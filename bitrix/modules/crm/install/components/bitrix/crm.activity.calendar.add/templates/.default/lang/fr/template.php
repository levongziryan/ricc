<?
$MESS["BX_CRM_CACA_PRIORITY"] = "Importance de l'événement";
$MESS["CRM_CALENDAR_DESC_TITLE"] = "Veuillez saisir votre commentaire";
$MESS["BX_CRM_CACA_PRIORITY_HIGH"] = "Augmenté";
$MESS["CRM_CALENDAR_DATE"] = "Date et temps";
$MESS["BX_CRM_CACA_REM_DAY"] = "jours";
$MESS["CRM_CALENDAR_ADD_BUTTON"] = "Ajouter";
$MESS["CRM_CALENDAR_ADD_TITLE"] = "Créer un appel/une rencontre au calendrier";
$MESS["CRM_CALENDAR_REMIND_FROM"] = "Dans";
$MESS["BX_CRM_CACA_REM_MIN"] = "minutes";
$MESS["CRM_CALENDAR_REMIND"] = "Rappeler de l'événement";
$MESS["BX_CRM_CACA_PRIORITY_LOW"] = "Bas";
$MESS["BX_CRM_CACA_PRIORITY_NORMAL"] = "Moyen";
$MESS["CRM_CALENDAR_DESC"] = "Description";
$MESS["CRM_CALENDAR_TOPIC"] = "En-tête";
$MESS["BX_CRM_CACA_REM_HOUR"] = "heures";
?>