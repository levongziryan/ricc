<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "El módulo e-Store no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_ACTIVE"] = "Activo";
$MESS["CRM_COLUMN_TIMESTAMP_X"] = "Modificado el ";
$MESS["CRM_COLUMN_NAME"] = "Nombre";
$MESS["CRM_COLUMN_PERSON_TYPE_ID"] = "Tipo de cliente";
$MESS["CRM_COLUMN_VALUE"] = "Tasa";
$MESS["CRM_COLUMN_IS_IN_PRICE"] = "Incluir impuesto en el precio";
$MESS["CRM_COLUMN_APPLY_ORDER"] = "Orden de aplicación";
$MESS["CRM_TAXRATE_DELETION_GENERAL_ERROR"] = "Error al eliminar la tasa de impuesto.";
$MESS["CRM_TAXRATE_UPDATE_GENERAL_ERROR"] = "Error al actualizar la tasa de impuesto.";
$MESS["CRM_COMPANY_PT"] = "Compañía";
$MESS["CRM_CONTACT_PT"] = "Contacto";
?>