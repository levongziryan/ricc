<?
$MESS["CRM_INVOICE_LIST_REBUILD_STATISTICS_NOT_REQUIRED_SUMMARY"] = "Les données statistiques des factures sont à jour.";
$MESS["CRM_INVOICE_LIST_REBUILD_STATISTICS_PROGRESS_SUMMARY"] = "Factures traitées : #PROCESSED_ITEMS# sur #TOTAL_ITEMS#.";
$MESS["CRM_INVOICE_LIST_REBUILD_STATISTICS_COMPLETED_SUMMARY"] = "Traitement des données statistiques des factures terminé. Factures traitées : #PROCESSED_ITEMS#.";
$MESS["CRM_INVOICE_LIST_ROW_COUNT"] = "Total : #ROW_COUNT#";
?>