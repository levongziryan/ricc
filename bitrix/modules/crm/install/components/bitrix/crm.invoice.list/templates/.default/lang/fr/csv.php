<?
$MESS["CRM_COLUMN_PRODUCT_NAME"] = "Article";
$MESS["CRM_COLUMN_PRODUCT_PRICE"] = "Prix";
$MESS["CRM_COLUMN_PRODUCT_QUANTITY"] = "Quantité";
$MESS["ERROR_INVOICE_IS_EMPTY_2"] = "Aucun élément à afficher.";
$MESS["CRM_COLUMN_DEAL"] = "Affaire";
$MESS["CRM_COLUMN_COMPANY"] = "Entreprise";
$MESS["CRM_COLUMN_CONTACT"] = "Client";
?>