<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_INVOICE_SHOW_TITLE"] = "Ver factura";
$MESS["CRM_INVOICE_SHOW"] = "Ver ";
$MESS["CRM_INVOICE_PAYMENT_HTML_TITLE"] = "Imprimir";
$MESS["CRM_INVOICE_PAYMENT_HTML"] = "Imprimir";
$MESS["CRM_INVOICE_PAYMENT_PDF_TITLE"] = "Ver factura en PDF";
$MESS["CRM_INVOICE_PAYMENT_PDF"] = "Ver PDF";
$MESS["CRM_INVOICE_EDIT_TITLE"] = "Editar factura";
$MESS["CRM_INVOICE_EDIT"] = "Editar";
$MESS["CRM_INVOICE_COPY_TITLE"] = "Copiar";
$MESS["CRM_INVOICE_COPY"] = "Copiar";
$MESS["CRM_INVOICE_DELETE_TITLE"] = "Eliminar factura";
$MESS["CRM_INVOICE_DELETE"] = "Eliminar";
$MESS["CRM_INVOICE_DELETE_CONFIRM"] = "¿Está seguro de que desea eliminarlo?";
$MESS["CRM_INVOICE_LIST_ADD_SHORT"] = "Crear factura";
$MESS["CRM_INVOICE_REBUILD_ACCESS_ATTRS"] = "Los permisos de acceso actualizados requieren que actualice los atributos actuales de acceso mediante el <a id=\"#ID#\" target=\"_blank\" href=\"#URL#\">la página de gestión de permisos</a>.
";
$MESS["CRM_PS_RQ_TX_PROC_DLG_TITLE"] = "Detalles de transferencia de las opciones de pago";
$MESS["CRM_PS_RQ_TX_PROC_DLG_DLG_SUMMARY"] = "Esto hará migrar los detalles del vendedor desde la opciones de pago a la compañía.";
$MESS["CRM_PS_RQ_TX_PROC_DLG_DLG_SUMMARY1"] = "Esto migrará los detalles del vendedor de las opciones de pago a los detalles de su compañía.";
$MESS["CRM_PSRQ_LRP_DLG_BTN_START"] = "Ejecutar";
$MESS["CRM_PSRQ_LRP_DLG_BTN_STOP"] = "Stop";
$MESS["CRM_PSRQ_LRP_DLG_BTN_CLOSE"] = "Cerrar";
$MESS["CRM_PSRQ_LRP_DLG_REQUEST_ERR"] = "Error al procesar su requerimiento.";
$MESS["CRM_INVOICE_LIST_FILTER_NAV_BUTTON_LIST"] = "Lista";
$MESS["CRM_INVOICE_LIST_FILTER_NAV_BUTTON_WIDGET"] = "Reportes";
$MESS["CRM_INVOICE_LIST_FILTER_NAV_BUTTON_KANBAN"] = "Kanban";
$MESS["CRM_INVOICE_LIST_CHOOSE_ACTION"] = "Seleccione la acción";
$MESS["CRM_INVOICE_LIST_APPLY_BUTTON"] = "Aplicar";
$MESS["CRM_SHOW_ROW_COUNT"] = "Mostrar cantidad";
$MESS["CRM_INVOICE_START_CALL_LIST"] = "Comenzar a marcar";
$MESS["CRM_INVOICE_CREATE_CALL_LIST"] = "Crear lista de llamadas";
$MESS["CRM_INVOICE_UPDATE_CALL_LIST"] = "Agregar a la lista de llamadas";
?>