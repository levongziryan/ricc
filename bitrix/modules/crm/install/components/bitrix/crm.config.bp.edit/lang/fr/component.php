<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "Le module bizproc n'a pas été installé.";
$MESS["BIZPROCDESIGNER_MODULE_NOT_INSTALLED"] = "Module du Concepteur des procédures d'entreprise non installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_BP_LIST_TITLE_EDIT"] = "Liste des modèles: #NAME#";
$MESS["CRM_BP_ENTITY_LIST"] = "Liste de modèles";
$MESS["CRM_BP_LEAD"] = "Prospect";
$MESS["CRM_BP_DEAL"] = "Affaire";
$MESS["CRM_BP_CONTACT"] = "Client";
$MESS["CRM_BP_COMPANY"] = "Entreprise";
?>