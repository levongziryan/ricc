<?
$MESS["CRM_CONFIGS_EXCH1C_LINK_TEXT"] = "Vers les paramètres de l'intégration avec 1C";
$MESS["CRM_TAB_CATALOG_IMPORT_TITLE"] = "Configuration des paramètres d'importation des produits depuis '1C: Entreprise'";
$MESS["CRM_TAB_CATALOG_EXPORT_TITLE"] = "Réglage des paramètres d'exportation de marchandises dans le programme '1C: Entreprise'";
$MESS["CRM_BUTTON_CANCEL_TITLE"] = "Ne pas enregistrer les paramètres courants de l'intégration avec '1C:Entreprise'";
$MESS["CRM_BUTTON_CANCEL"] = "Annuler";
$MESS["CRM_CONFIGS_EXCH1C_LINK_TITLE"] = "Passage aux réglages de l'intégration avec 1C";
$MESS["CRM_BUTTON_SAVE"] = "Sauvegarder";
$MESS["CRM_BUTTON_SAVE_TITLE"] = "Sauvegarder les réglages d'intégration avec '1C:Entreprise'";
$MESS["CRM_TAB_CATALOG_IMPORT"] = "Produits - importation";
$MESS["CRM_TAB_CATALOG_EXPORT"] = "Produits - exportation";
?>