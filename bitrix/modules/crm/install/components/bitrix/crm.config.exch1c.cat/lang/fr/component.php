<?
$MESS["CAT_1C_DEACTIVATE"] = "Désactiver";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_CATALOG_XML_ID"] = "Identificateur du catalogue pour l'échange";
$MESS["CAT_1C_DETAIL_RESIZE"] = "Modifier l'image détaillée";
$MESS["CRM_EXCH1C_LIST"] = "Intégration au programme '1C: Entreprise'";
$MESS["CAT_1C_INTERVAL"] = "Intervalle d'une étape en secondes (0 - effectuer le téléchargement en une seule étape)";
$MESS["CAT_1CE_INTERVAL"] = "Intervalle d'une étape en secondes (0 - effectuer le téléchargement en une seule étape)";
$MESS["CAT_1C_USE_CRC"] = "Utiliser des totaux de contrôle d'éléments pour l'optimisation de la mise à jour du catalogue";
$MESS["CAT_1C_PICTURE"] = "Image de l'annonce";
$MESS["CAT_1CE_ELEMENTS_PER_STEP"] = "Nombre d'éléments à extraire en une étape (0 - effectuer l'extraction en une étape)";
$MESS["CAT_1C_DETAIL_HEIGHT"] = "Hauteur maximale admissible de l'image détaillée";
$MESS["CAT_1C_DETAIL_WIDTH"] = "Largeur admissible maximale de l'image détaillée";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "Le module catalog n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Module CRM non installé";
$MESS["CRM_MODULE_NOT_INSTALLED_IBLOCK"] = "Module iblock non installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Module SALE non installé.";
$MESS["CAT_1C_NONE"] = "Rien";
$MESS["CAT_1C_FILE_SIZE_LIMIT"] = "Volume de la partie de fichier dont le chargement s'effectue en une seule fois dans le même temps (en bytes)";
$MESS["CAT_1C_GROUP_PERMISSIONS"] = "Permettre le chargement aux groupes des utilisateurs";
$MESS["CRM_SELECTED_CATALOG_GROUP_ID"] = "Type de prix utilisé dans CRM";
$MESS["CAT_1C_DELETE"] = "Supprimer";
$MESS["CAT_1C_SECTION_ACTION"] = "Que faire avec les rubriques absentes dans le fichier d'importation";
$MESS["CAT_1C_ELEMENT_ACTION"] = "Que faire des éléments qui ne figurent pas dans le fichier d'importation";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "Le module Devise n'est pas installé.";
?>