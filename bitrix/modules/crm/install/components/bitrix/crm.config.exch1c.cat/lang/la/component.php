<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_IBLOCK"] = "El módulo Information Blocks no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "El módulo Currency no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "El módulo e-Store no está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "El módulo Commercial Catalog no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_EXCH1C_LIST"] = "1C: Integración Empresarial";
$MESS["CAT_1C_NONE"] = "ninguno";
$MESS["CAT_1C_DEACTIVATE"] = "desactivar";
$MESS["CAT_1C_DELETE"] = "eliminar";
$MESS["CAT_1C_ELEMENT_ACTION"] = "Elementos que faltan en el archivo de importación";
$MESS["CAT_1C_SECTION_ACTION"] = "Secciones que faltan en el archivo de importación";
$MESS["CAT_1C_GROUP_PERMISSIONS"] = "Permitir la importación para grupos de usuarios";
$MESS["CAT_1C_INTERVAL"] = "Importar la duración del paso, seg. (0 - importar todo a la vez)";
$MESS["CAT_1C_FILE_SIZE_LIMIT"] = "Tamaño máximo del fragmento de archivo de importación (bytes)";
$MESS["CAT_1C_USE_CRC"] = "Utilice la suma de comprobación de elementos para optimizar la actualización del catálogo";
$MESS["CAT_1C_PICTURE"] = "Vista previa de la imagen";
$MESS["CAT_1C_DETAIL_RESIZE"] = "Cambiar el tamaño de la imagen detallada";
$MESS["CAT_1C_DETAIL_WIDTH"] = "Ancho máx. de la imagen detallada";
$MESS["CAT_1C_DETAIL_HEIGHT"] = "Altura máx. de la imagen detallada";
$MESS["CAT_1CE_INTERVAL"] = "Importar la duración del paso, seg. (0 - importar todo a la vez)";
$MESS["CAT_1CE_ELEMENTS_PER_STEP"] = "Elementos por paso (0 - importar todos a la vez)";
$MESS["CRM_CATALOG_XML_ID"] = "ID de intercambio de catálogo";
$MESS["CRM_SELECTED_CATALOG_GROUP_ID"] = "Tipo de precio en el CRM";
?>