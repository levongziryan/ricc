<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_COLUMN_FUNNEL"] = "Funil";
$MESS["CRM_COLUMN_TITLE"] = "Fase do negócio";
$MESS["CRM_COLUMN_OPPORTUNITY"] = "Montante";
$MESS["CRM_COLUMN_STAGE_ID"] = "Fase do negócio";
$MESS["CRM_COLUMN_STATE_ID"] = "Status";
$MESS["CRM_COLUMN_CURRENCY_ID"] = "Moeda";
$MESS["CRM_COLUMN_PROBABILITY"] = "Probabilidade";
$MESS["CRM_COLUMN_PROCENT"] = "%";
$MESS["CRM_COLUMN_COUNT"] = "Negócios";
$MESS["CRM_COLUMN_PRODUCT_ID"] = "Produto";
$MESS["CRM_COLUMN_COMPANY_LIST"] = "Empresas";
$MESS["CRM_COLUMN_CONTACT_LIST"] = "Contatos";
$MESS["CRM_COLUMN_TYPE_ID"] = "Tipo";
$MESS["CRM_COLUMN_CLOSED"] = "Fechado";
$MESS["CRM_COLUMN_BEGINDATE"] = "Data de início";
$MESS["CRM_COLUMN_MODIFY_BY"] = "Modificado por";
$MESS["CRM_COLUMN_ASSIGNED_BY"] = "Pessoa responsável";
$MESS["CRM_COLUMN_DATE_CREATE"] = "Criado";
$MESS["CRM_COLUMN_DATE_MODIFY"] = "Modificado";
$MESS["CRM_COLUMN_SUMM"] = "Montante,  #CURRENCY#";
$MESS["CRM_PRESET_WEEK"] = "Esta semana";
$MESS["CRM_PRESET_WEEK_PREV"] = "Semana passada";
$MESS["CRM_PRESET_MONTH"] = "Este mês";
$MESS["CRM_PRESET_MONTH_PREV"] = "Mês passado";
$MESS["CRM_PRESET_MY_WEEK"] = "Meus negócios desta semana";
$MESS["CRM_PRESET_MY_WEEK_AGO"] = "Meus negócios da semana passada";
$MESS["CRM_DEAL_NAV_TITLE_LIST"] = "Funil de Vendas";
$MESS["CRM_FUNNEL_TYPE_CLASSICAL"] = "padrão";
$MESS["CRM_FUNNEL_TYPE_CUMULATIVE"] = "com conversões";
$MESS["CRM_FUNNEL_TYPE_CUMULATIVE2"] = "com conversões";
$MESS["CRM_COLUMN_CLOSEDATE"] = "Data de fechamento";
?>