<?
$MESS["DEAL_STAGES_WON"] = "Atualmente ativo";
$MESS["DEAL_STAGES_LOSE"] = "Não Ativo";
$MESS["CRM_DEAL_FUNNEL_SHOW_FILTER_SHORT"] = "Filtro";
$MESS["CRM_DEAL_FUNNEL_TYPE_SELECTOR_TITLE"] = "Tipo de Funil";
$MESS["CRM_DEAL_FUNNEL_SHOW_FILTER"] = "Mostrar/ocultar filtro";
$MESS["FUNNEL_CHART_HIDE"] = "Ocultar gráfico";
$MESS["FUNNEL_CHART_SHOW"] = "Mostrar gráfico";
$MESS["FUNNEL_CHART_NO_DATA"] = "Não há dados para criar gráfico";
$MESS["CRM_DEAL_CATEGORY_SELECTOR_TITLE"] = "Pipeline";
?>