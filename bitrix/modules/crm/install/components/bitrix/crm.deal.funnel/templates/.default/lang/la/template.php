<?
$MESS["DEAL_STAGES_WON"] = "Actualmente Activo";
$MESS["DEAL_STAGES_LOSE"] = "No hay activación";
$MESS["CRM_DEAL_FUNNEL_TYPE_SELECTOR_TITLE"] = "Tipo de embudo";
$MESS["CRM_DEAL_FUNNEL_SHOW_FILTER_SHORT"] = "Filtro";
$MESS["CRM_DEAL_FUNNEL_SHOW_FILTER"] = "Mostrar/ocultar filtro";
$MESS["FUNNEL_CHART_HIDE"] = "Ocultar gráfico";
$MESS["FUNNEL_CHART_SHOW"] = "Mostrar gráfico";
$MESS["FUNNEL_CHART_NO_DATA"] = "No hay datos para crear gráficos";
$MESS["CRM_DEAL_CATEGORY_SELECTOR_TITLE"] = "Pipeline";
?>