<?
$MESS["GD_CRM_SORT_ASC"] = "Croissant";
$MESS["GD_CRM_COLUMN_DATE_MODIFY"] = "Modifié";
$MESS["GD_CRM_COLUMN_DATE_CREATE"] = "Créé";
$MESS["GD_CRM_CONTACT_LIST_CONTACT_COUNT"] = "Nombre de contacts par page";
$MESS["GD_CRM_SORT_BY"] = "Ordre";
$MESS["GD_CRM_SORT"] = "Classification";
$MESS["GD_CRM_COLUMN_TYPE"] = "Entité";
$MESS["GD_CRM_ONLY_MY"] = "Seulement les miens";
$MESS["GD_CRM_SORT_DESC"] = "Décroissant";
?>