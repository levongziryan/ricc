<?
$MESS["GD_CRM_TASK_ENTITY_TYPE"] = "Entités dans la liste";
$MESS["GD_CRM_TASK_TYPE_LEAD"] = "Prospect";
$MESS["GD_CRM_TASK_TYPE_CONTACT"] = "Client";
$MESS["GD_CRM_TASK_TYPE_COMPANY"] = "Entreprise";
$MESS["GD_CRM_TASK_TYPE_DEAL"] = "Affaire";
$MESS["GD_CRM_TASK_LIST_EVENT_COUNT"] = "Nombre de tâches par page";
$MESS["GD_CRM_ONLY_MY"] = "Seulement les miens";
$MESS["GD_CRM_SORT"] = "Classification";
$MESS["GD_CRM_SORT_BY"] = "Par";
$MESS["GD_CRM_SORT_ASC"] = "Croissant";
$MESS["GD_CRM_SORT_DESC"] = "Décroissant";
$MESS["GD_CRM_COLUMN_CREATED_DATE"] = "Date de création";
$MESS["GD_CRM_COLUMN_CHANGED_DATE"] = "Date de modification";
$MESS["GD_CRM_COLUMN_CLOSED_DATE"] = "Date de la clôture";
$MESS["GD_CRM_COLUMN_DATE_START"] = "Date du début";
?>