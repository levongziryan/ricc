<?
$MESS["GD_CRM_LEAD_LIST_LEAD_COUNT"] = "Nombre des prospects sur une page";
$MESS["GD_CRM_ONLY_MY"] = "Seulement les miens";
$MESS["GD_CRM_SORT"] = "Classification";
$MESS["GD_CRM_SORT_BY"] = "Ordre";
$MESS["GD_CRM_SORT_ASC"] = "Croissant";
$MESS["GD_CRM_SORT_DESC"] = "Décroissant";
$MESS["GD_CRM_COLUMN_STATUS"] = "Statut";
$MESS["GD_CRM_COLUMN_DATE_CREATE"] = "Créé";
$MESS["GD_CRM_COLUMN_DATE_MODIFY"] = "Modifié";
?>