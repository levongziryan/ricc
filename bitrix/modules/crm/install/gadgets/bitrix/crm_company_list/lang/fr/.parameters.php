<?
$MESS["GD_CRM_SORT_ASC"] = "Croissant";
$MESS["GD_CRM_COLUMN_DATE_MODIFY"] = "Modifié";
$MESS["GD_CRM_COLUMN_DATE_CREATE"] = "Créé";
$MESS["GD_CRM_COMPANY_LIST_COMPANY_COUNT"] = "Nombre de transactions sur une page";
$MESS["GD_CRM_SORT_BY"] = "Ordre";
$MESS["GD_CRM_SORT"] = "Classification";
$MESS["GD_CRM_COLUMN_COMPANY_TYPE"] = "Type de l'Entreprise";
$MESS["GD_CRM_ONLY_MY"] = "Seulement les miens";
$MESS["GD_CRM_SORT_DESC"] = "Décroissant";
?>