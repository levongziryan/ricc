<?
$MESS["CRM_AUTOMATION_DEMO_LEAD_2_NOTIFY_TITLE"] = "Notification";
$MESS["CRM_AUTOMATION_DEMO_LEAD_2_CONTROL_TITLE"] = "Contrôle";
$MESS["CRM_AUTOMATION_DEMO_LEAD_2_MESSAGE_1"] = "Nouvelle piste créée : {=Document:ID} {=Document:TITLE}

Traitez la nouvelle piste. Remplissez le formulaire et planifiez vos activités.

Passez la piste au statut suivant, ou convertissez-la en transaction.";
$MESS["CRM_AUTOMATION_DEMO_LEAD_2_MESSAGE_2"] = "Félicitations ! Vous travaillez à présent sur la transaction {=Document:ID} {=Document:TITLE}

N'oubliez pas : votre objectif est de convertir la piste. Faites progresser la transaction dans l'entonnoir en accomplissant les activités prévues.";
$MESS["CRM_AUTOMATION_DEMO_LEAD_2_MESSAGE_3"] = "Attention ! La piste {=Document:ID} {=Document:TITLE} a depuis trois jours le statut {=Document:STAGE_ID_PRINTABLE} 

Un responsable en a été informé, mais aucune mesure n'a été prise.

Responsable : {=Document:ASSIGNED_BY_PRINTABLE}";
?>