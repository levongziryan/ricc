<?
$MESS["CRM_AUTOMATION_DEMO_LEAD_1_NOTIFY_TITLE"] = "Notification";
$MESS["CRM_AUTOMATION_DEMO_LEAD_1_CONTROL_TITLE"] = "Contrôle";
$MESS["CRM_AUTOMATION_DEMO_LEAD_1_1_MESSAGE"] = "Une nouvelle piste #URL_1#{=Document:TITLE}#URL_2# a été créée

Veuillez vérifier la source de la piste. S'il s'agit d'un appel téléphonique, écoutez l'enregistrement de la conversation, saisissez les données nécessaires et prévoyez des activités. Si la source est un e-mail, lisez-le et envoyez votre réponse. Si cette piste provient d'un autre vecteur ou source, traitez les informations reçues et passez la piste au statut suivant, ou convertissez-la en transaction.";
$MESS["CRM_AUTOMATION_DEMO_LEAD_1_2_MESSAGE"] = "Cette #URL_1#piste#URL_2# est dans son statut d'origine depuis plus de 2 jours. Vous devez la traiter, changer son statut ou la convertir en transaction.";
$MESS["CRM_AUTOMATION_DEMO_LEAD_1_3_MESSAGE"] = "Important ! La piste #URL_1#{=Document:TITLE}#URL_2# a le statut {=Document:STATUS_ID} depuis plus de 3 jours.

Votre superviseur a été informé que le statut de cette piste doit être changé ou qu'elle doit être convertie.

Cette piste a été assignée à {=Document:ASSIGNED_BY_PRINTABLE}";
?>