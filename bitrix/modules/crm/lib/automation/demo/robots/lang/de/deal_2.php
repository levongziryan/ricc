<?
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_NOTIFY_TITLE"] = "Benachrichtigung";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_CONTROL_TITLE"] = "Kontrolle";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_CALL_TITLE"] = "Einen Anruf planen";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_1_MESSAGE"] = "Neuer Auftrag erstellt: {=Document:ID} {=Document:TITLE}

Lesen Sie hier das Wichtigste zum neuen Auftrag: bestellte Produkte; Frist; geplantes Anfangsdatum und andere wichtige Informationen.

Planen Sie nun entsprechende Aktivitäten, damit der Auftrag erfolgreich wird.
";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_2_MESSAGE"] = "Glückwunsch! Sie arbeiten jetzt an dem Auftrag {=Document:ID} {=Document:TITLE}

Nicht vergessen: Ihr Ziel ist es, den Auftrag erfolgreich abzuschließen. Dafür sollten Sie geplante Aktivitäten rechtzeitig abschließen und den Auftrag phasenweise aktualisieren.
";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_3_MESSAGE"] = "Achtung! Der Auftrag {=Document:ID} {=Document:TITLE} befindet sich schon seit drei Tagen in der Phase {=Document:STAGE_ID_PRINTABLE} 

Verantwortliche Person wurde benachrichtigt, es folgten jedoch keine Aktionen.

Verantwortlich: {=Document:ASSIGNED_BY_PRINTABLE}
";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_4_MESSAGE"] = "Der Auftrag {=Document:ID} {=Document:TITLE} für {=Document:OPPORTUNITY_ACCOUNT} ist gescheitert.

Verantwortlich: {=Document:ASSIGNED_BY_PRINTABLE}
";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_5_MESSAGE"] = "Glückwunsch! Sie arbeiten sehr aktiv am Auftrag {=Document:ID} {=Document:TITLE}

Nicht vergessen: Ihr Ziel ist es, den Auftrag erfolgreich abzuschließen. Dafür sollten Sie geplante Aktivitäten rechtzeitig abschließen und den Auftrag phasenweise aktualisieren.
";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_6_MESSAGE"] = "Sie müssen den Auftrag immer nach vorne verschieben, damit er ein Erfolg wird.

Der Auftrag {=Document:ID} {=Document:TITLE} befindet sich schon seit zwei Tagen in der Phase {=Document:STAGE_ID_PRINTABLE}. Sie sollten diese Phase abschließen und den Auftrag weiter verschieben.
";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_7_MESSAGE"] = "Achtung! Der Auftrag {=Document:ID} {=Document:TITLE} befindet sich schon seit drei Tagen in der Phase {=Document:STAGE_ID_PRINTABLE} 

Verantwortliche Person wurde benachrichtigt, es folgten jedoch keine Aktionen.

Verantwortlich: {=Document:ASSIGNED_BY_PRINTABLE}
";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_8_MESSAGE"] = "Glückwunsch! Sie arbeiten sehr aktiv am Auftrag {=Document:ID} {=Document:TITLE}.

In dieser Phase sollten Sie eins der möglichen Szenarien auswählen:

Ist für den Auftrag eine Vorauszahlung erforderlich, erstellen Sie eine Rechnung und senden Sie sie an den Kunden.

Ist keine Vorauszahlung erforderlich, können Sie den Auftrag in die nächste Phase verschieben.
";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_9_MESSAGE"] = "Erinnern Sie den Kunden daran, dass die Rechnung bezahlt werden soll, damit der Auftrag {=Document:ID} {=Document:TITLE} Fortschritt macht.

Ist die Rechnung bereits bezahlt, markieren Sie sie entsprechend und verschieben Sie den Auftrag in die nächste Phase.
";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_10_MESSAGE"] = "Achtung! Der Auftrag {=Document:ID} {=Document:TITLE} befindet sich schon seit fünf Tagen in der Phase {=Document:STAGE_ID_PRINTABLE} 

Verantwortliche Person wurde benachrichtigt, es folgten jedoch keine Aktionen.

Verantwortlich: {=Document:ASSIGNED_BY_PRINTABLE}
";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_11_MESSAGE"] = "Glückwunsch! Sie arbeiten sehr aktiv am Auftrag {=Document:ID} {=Document:TITLE}

Nicht vergessen: Ihr Ziel ist es, den Auftrag erfolgreich abzuschließen. Dafür sollten Sie geplante Aktivitäten rechtzeitig abschließen und den Auftrag phasenweise aktualisieren.
";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_12_MESSAGE"] = "Achtung! Der Auftrag {=Document:ID} {=Document:TITLE} befindet sich schon seit vier Tagen in der Phase {=Document:STAGE_ID_PRINTABLE} 

Verantwortliche Person wurde benachrichtigt, es folgten jedoch keine Aktionen.

Verantwortlich: {=Document:ASSIGNED_BY_PRINTABLE}
";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_13_MESSAGE"] = "Glückwunsch! Sie arbeiten sehr aktiv am Auftrag {=Document:ID} {=Document:TITLE} 

Sie müssen jetzt eine Zahlung erhalten, um den Auftrag abzuschließen.

Erstellen Sie eine Rechnung und senden Sie diese an den Kunden.
";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_14_MESSAGE"] = "Erinnern Sie den Kunden daran, die Rechnung für den Auftrag {=Document:ID} {=Document:TITLE} zu bezahlen.

Ist die Rechnung bereits bezahlt, markieren Sie sie entsprechend und verschieben Sie den Auftrag in die nächste Phase.
";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_15_MESSAGE"] = "Glückwunsch! Sie arbeiten sehr aktiv am Auftrag {=Document:ID} {=Document:TITLE}

Alle Aktivitäten erledigt, die Rechnung bezahlt? Dann verschieben Sie den Auftrag in die Abschlussphase, um ihn als Erfolg zu verzeichnen.
";
$MESS["CRM_AUTOMATION_DEMO_DEAL_2_CALL_SUBJECT"] = "Wegen Rechnung erinnern";
?>