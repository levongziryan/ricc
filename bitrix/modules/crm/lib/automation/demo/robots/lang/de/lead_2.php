<?
$MESS["CRM_AUTOMATION_DEMO_LEAD_2_NOTIFY_TITLE"] = "Benachrichtigung";
$MESS["CRM_AUTOMATION_DEMO_LEAD_2_CONTROL_TITLE"] = "Kontrolle";
$MESS["CRM_AUTOMATION_DEMO_LEAD_2_MESSAGE_1"] = "Neuer Lead erstellt: {=Document:ID} {=Document:TITLE}

Verarbeiten Sie den neuen Lead. Tragen Sie die notwendigen Informationen ein und planen Sie Ihre Aktivitäten.

Verschieben Sie den Lead in den nächsten Status  oder konvertieren Sie ihn zum Auftrag.
";
$MESS["CRM_AUTOMATION_DEMO_LEAD_2_MESSAGE_2"] = "Glückwunsch! Sie arbeiten jetzt am Auftrag {=Document:ID} {=Document:TITLE}

Nicht vergessen: Ihr Ziel ist es, den Lead zu konvertieren. Erledigen Sie die geplanten Aktivitäten und verschieben Sie den Lead weiter nach vorne.
";
$MESS["CRM_AUTOMATION_DEMO_LEAD_2_MESSAGE_3"] = "Achtung! Der Lead {=Document:ID} {=Document:TITLE} befindet sich schon seit drei Tagen im Status {=Document:STAGE_ID_PRINTABLE} 

Verantwortliche Person wurde benachrichtigt, es folgten jedoch keine Aktionen.

Verantwortlich: {=Document:ASSIGNED_BY_PRINTABLE}
";
?>