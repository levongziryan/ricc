<?
$MESS["CRM_AUTOMATION_DEMO_LEAD_1_NOTIFY_TITLE"] = "Benachrichtigung";
$MESS["CRM_AUTOMATION_DEMO_LEAD_1_CONTROL_TITLE"] = "Kontrolle";
$MESS["CRM_AUTOMATION_DEMO_LEAD_1_1_MESSAGE"] = "Neuer Lead #URL_1#{=Document:TITLE}#URL_2# erstellt 

Überprüfen Sie bitte die Leadquelle. Ist das ein Anruf, hören Sie sich die Telefonatsaufzeichnung an, Sie können dabei notwendige Daten eingeben und Aktivitäten planen. Ist die Quelle eine E-Mail, lesen und beantworten Sie diese. Kommt der Lead aus einer anderen Quelle, bearbeiten Sie eingehende Informationen, dann können Sie den Lead in den nächsten Status verschieben oder ihn zu einem Auftrag konvertieren.
";
$MESS["CRM_AUTOMATION_DEMO_LEAD_1_2_MESSAGE"] = "Dieser #URL_1#Lead#URL_2# ist schon seit zwei Tagen in seinem ursprünglichen Status. Sie müssen den Lead bearbeiten, seinen Status ändern oder ihn zu einem Auftrag konvertieren.";
$MESS["CRM_AUTOMATION_DEMO_LEAD_1_3_MESSAGE"] = "Wichtig! Der Lead #URL_1#{=Document:TITLE}#URL_2# ist schon seit 3 Tagen im Status {=Document:STATUS_ID}.

Ihre Vorgesetzter wurde darüber benachrichtigt, dass der Status dieses Leads geändert werden soll, oder der Lead selbst soll konvertiert werden.

Verantwortlicher für diesen Lead  {=Document:ASSIGNED_BY_PRINTABLE}
";
?>