<?
$MESS["SHOW_SCHEDULE"] = "Exibir horas de serviço";
$MESS["SHOW_PHONE"] = "Exibir telefone";
$MESS["CATALOG_SEF_INDEX"] = "Depósitos";
$MESS["CATALOG_SEF_DETAIL"] = "Detalhes de depósitos";
$MESS["USE_TITLE"] = "Definir título da página";
$MESS["TITLE"] = "Título da página";
$MESS["MAP_TYPE"] = "Tipo de mapa";
$MESS["DEFAULT_TITLE"] = "Lista e Detalhes de Depósitos";
?>