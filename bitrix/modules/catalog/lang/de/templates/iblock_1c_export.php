<?
$MESS["IB1C_ERROR_PROPERTY"] = "Beim Importieren der Eigenschaft ist ein Fehler aufgetreten";
$MESS["IB1C_ERROR_CATEGORY"] = "Beim Importieren der Gruppe ist ein Fehler aufgetreten";
$MESS["IB1C_ERROR_CATALOG"] = "Beim Importieren des Katalogs ist ein Fehler aufgetreten";
$MESS["IB1C_ERROR_PRODUCT"] = "Beim Importieren des Produkts ist ein Fehler aufgetreten";
$MESS["IB1C_ERROR_IBTYPE"] = "Der Katalog kann nicht importiert werden: es gibt keinen einzigen Informationsblocktyp.";
$MESS["IB1C_ERROR_DATA_LOAD"] = "Die Daten für den Import wurden nicht übergeben.";
$MESS["IB1C_ERROR_WRONG_DATA"] = "Ungültige Datei. Importieren ist nicht möglich.";
?>