<?
$MESS["SMAIL_FOOTER_BR"] = "Mit freundlichen Grüßen,";
$MESS["SMAIL_FOOTER_SHOP"] = "Administration.";
$MESS["SMAIL_UNSUBSCRIBE"] = "Onlineshop";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_LIST_CONFIRM_NAME"] = "Abbestellen";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_LIST_CONFIRM_DESC"] = "
#TOKEN# - Bestätigungscode
#TOKEN_URL# - Link mit dem Bestätigungscode
#LIST_SUBSCRIBES# - Verfügbare Abonnements
";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_LIST_CONFIRM_SUBJECT"] = "#SITE_NAME#: Bestätigungscode";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_LIST_CONFIRM_HTML_TITLE"] = "Benachrichtigung von #SITE_NAME#";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_LIST_CONFIRM_HTML_SUB_TITLE"] = "Lieber #USER_NAME#!";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_LIST_CONFIRM_HTML_TEXT"] = "
Sie erhalten diese Nachricht, weil von Ihrer E-Mail-Adresse eine Anfrage für einen Bestätigungscode gekommen ist, um auf Abonnements zugreifen zu können, welche auf #SERVER_NAME# verfügbar sind. <br><br> 
Ihr Bestätigungscode: #TOKEN# <br><br> 
Um auf die Abonnements zuzugreifen, folgen Sie bitte diesem Link: #TOKEN_URL# <br><br>
Sie können den Code auch manuell hier eingeben: #LIST_SUBSCRIBES# <br><br>
Diese Nachricht enthält Authentifizierungsinformationen.<br>
Nutzen Sie den Bestätigungscode, um auf Ihre Abonnements zuzugreifen.<br>
Diese Nachricht wurde automatisch erzeugt, antworten Sie nicht darauf.<br><br>
Danke, dass Sie sich für uns entschieden haben.<br>
";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_NOTIFY_NAME"] = "Benachrichtigung über Wiederverfügbarkeit";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_NOTIFY_DESC"] = "#USER_NAME# - Nutzername
#EMAIL_TO# - E-Mail des Nutzers 
#NAME# - Produktname
#PAGE_URL# - Detaillierte Seite des Produktes
#CHECKOUT_URL# - Fügt Produkt zum Warenkorb hinzu
#PRODUCT_ID# - Produkt-ID zur Nutzung in den Links
";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_NOTIFY_SUBJECT"] = "#SITE_NAME#: Produkt ist wieder verfügbar";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_NOTIFY_HTML_TITLE"] = "Produkt ist wieder verfügbar auf #SITE_NAME#";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_NOTIFY_HTML_SUB_TITLE"] = "Lieber #USER_NAME#!";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_NOTIFY_HTML_TEXT"] = "
\"#NAME#\" (#PAGE_URL#) ist wieder auf Lager verfügbar.<br><br>
Sie haben uns gebeten, Sie zu benachrichtigen, wenn das Produkt verfügbar ist.<br><br>
Es ist jetzt wieder verfügbar, und Sie können es sofort bestellen: (#CHECKOUT_URL#)<br><br>
Antworten Sie nicht auf diese Nachricht.<br><br>
Danke, dass Sie sich für uns entschieden haben.<br>
";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_NOTIFY_REPEATED_NAME"] = "Benachrichtigung über Wiederverfügbarkeit";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_NOTIFY_REPEATED_DESC"] = "#USER_NAME# - Nutzername
#EMAIL_TO# - E-Mail des Nutzers 
#NAME# - Produktname
#PAGE_URL# - Detaillierte Seite des Produktes
#PRODUCT_ID# - Produkt-ID zur Nutzung in den Links
";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_NOTIFY_REPEATED_SUBJECT"] = "Benachrichtigung über Wiederverfügbarkeit: #SITE_NAME#";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_NOTIFY_REPEATED_HTML_TITLE"] = "Benachrichtigung über Wiederverfügbarkeit: #SITE_NAME#";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_NOTIFY_REPEATED_HTML_SUB_TITLE"] = "Lieber #USER_NAME#!";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_NOTIFY_REPEATED_HTML_TEXT"] = "
Leider ist \"#NAME#\" (#PAGE_URL#) wieder ausverkauft.<br><br>
Wir werden Sie benachrichtigen, wenn es wieder verfügbar ist.<br><br>
Antworten Sie nicht auf diese Nachricht.<br><br>
Danke, dass Sie sich für uns entschieden haben.<br>
";
?>