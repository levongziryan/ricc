<?
$MESS["CATALOG_INSTALL_NAME"] = "Kommerzieller Katalog";
$MESS["CATALOG_INSTALL_DESCRIPTION"] = "Das Modul \"Kommerzieller Katalog\" erlaubt die Erstellung von Warenkatalogen und Dienstleistungen mit Preisen, Aufschlägen und dem Import/Export von Daten. Das Modul \"Kommerzieller Katalog\" kann nicht ohne das Modul \"Informationsblöcke\" funktionieren.";
$MESS["CATALOG_INSTALL_TITLE"] = "Installation des Moduls \"Katalog\"";
$MESS["CATALOG_INSTALL_PUBLIC_DIR"] = "Öffentlicher Ordner";
$MESS["CATALOG_INSTALL_SETUP"] = "Installieren";
$MESS["CATALOG_INSTALL_COMPLETE_OK"] = "Die Installation wurde abgeschlossen. Für weitere Informationen benutzen Sie Hilfe.";
$MESS["CATALOG_INSTALL_COMPLETE_ERROR"] = "Die Installation war fehlerhaft";
$MESS["CATALOG_INSTALL_ERROR"] = "Es sind Fehler beim Installieren aufgetreten";
$MESS["CATALOG_INSTALL_BACK"] = "Zurück zur Modulverwaltung";
$MESS["CATALOG_UNINSTALL_WARNING"] = "Achtung! Das Modul wird aus dem System gelöscht.";
$MESS["CATALOG_UNINSTALL_SAVEDATA"] = "Sie können Daten in der Datenbank speichern, wenn Sie das Flag &quot;Tabellen speichern&quot; aktivieren.";
$MESS["CATALOG_UNINSTALL_SAVECURRENCY"] = "Währungstabellen speichern (werden von den Modulen \"Katalog\" und \"Onlineshop\" verwendet).";
$MESS["CATALOG_UNINSTALL_SAVETABLE"] = "Tabellen speichern";
$MESS["CATALOG_UNINSTALL_DEL"] = "Deinstallieren";
$MESS["CATALOG_UNINSTALL_ERROR"] = "Es sind Fehler beim Deinstallieren aufgetreten:";
$MESS["CATALOG_UNINSTALL_COMPLETE"] = "Deinstallation wurde abgeschlossen.";
$MESS["CATALOG_INSTALL_PUBLIC_SETUP"] = "Installieren";
$MESS["CATALOG_UNINS_CURRENCY"] = "Damit  das Moduls \"Kommerzieller Katalog\" funktioniert,  müssen Sie zuerst das Modul \"Währung\" installieren.";
$MESS["CATALOG_UNINS_IBLOCK"] = "Für die Arbeit des Katalogmoduls muss das Modul der Informationsblöcke installiert werden.";
$MESS["CATALOG_INSTALL_DESCRIPTION2"] = "Mit dem Modul \"Kommerzieller Katalog\" können Produktkataloge, Preise, Preisaufschläge und Rabatte erstellt sowie Daten exportiert und importiert werden.";
$MESS["CATALOG_INSTALL_PROFILE_IRR2"] = "irr.ru";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_LIST_NAME"] = "Bestätigungscode";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_LIST_DESC"] = "
#TOKEN# - Bestätigungscode
#TOKEN_URL# - Link zum Bestätigungscode
#LIST_SUBSCRIBES# - Verfügbare Abonnements
";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_LIST_SUBJECT"] = "#SITE_NAME#: Bestätigungscode";
$MESS["CATALOG_PRODUCT_SUBSCRIBE_LIST_MESSAGE"] = "
Diese Nachricht wurde von #SITE_NAME# gesendet
------------------------------------------------

Hallo,

Sie erhalten diese Nachricht, weil Ihre E-Mail-Adresse angegeben wurde, um auf Abonnements auf #SERVER_NAME# zugreifen zu können.

Ihr Bestätigungscode für Abonnement: #TOKEN#

Klicken Sie auf diesen Link, um auf Ihre Abonnements zuzugreifen:
#TOKEN_URL#

Alternativ können Sie den Bestätigungscode hier manuell eingeben:
#LIST_SUBSCRIBES#

---------------------------------------------------------------------
Diese Nachricht enthält Authentifizierungsinformationen.
Benutzen Sie den Bestätigungscode, um auf Ihre Abonnements zuzugreifen.
---------------------------------------------------------------------

Diese Nachricht wurde automatisch erzeugt.
";
?>