<?
$MESS["DDCT_DEDUCTION_STORE_ERROR"] = "Es wurde kein Lager ausgewählt, um das Produkt #PRODUCT_NAME# (##PRODUCT_ID#) auszuliefern";
$MESS["DDCT_DEDUCTION_QUANTITY_STORE_ERROR"] = "Unzureichende Menge von #PRODUCT_NAME# (##PRODUCT_ID#) für eine Auslieferung vom Lager ##STORE_ID#.";
$MESS["SALE_PROVIDER_SHIPMENT_QUANTITY_NOT_ENOUGH"] = "Die zu reservierende Menge von \"#PRODUCT_NAME#\" überschreitet die Menge im Katalog";
$MESS["DDCT_DEDUCTION_QUANTITY_ERROR"] = "Unzureichende Menge von #PRODUCT_NAME# (##PRODUCT_ID#) für eine Auslieferung";
$MESS["DDCT_DEDUCTION_MULTI_BARCODE_EMPTY"] = "Strichcode für \"#PRODUCT_NAME#\" fehlt.";
$MESS["DDCT_DEDUCTION_BARCODE_ERROR"] = "Strichcode \"#BARCODE#\" für das Produkt \"#PRODUCT_NAME#\" (##PRODUCT_ID#) wurde nicht gefunden.";
$MESS["SALE_PROVIDER_PRODUCT_NOT_AVAILABLE"] = "#PRODUCT_NAME# (##PRODUCT_ID#) ist nicht am Lager.";
?>