<?
$MESS["BX_CATALOG_PRODUCT_BASKET_ERR_SEARCHER"] = "Ein Suchsystem kann nicht ein Käufer sein.";
$MESS["BX_CATALOG_PRODUCT_BASKET_ERR_NO_PRODUCT"] = "Das Produkt wurde nicht gefunden.";
$MESS["BX_CATALOG_PRODUCT_BASKET_ERR_NO_SALE"] = "Das Modul Online-Shop ist nicht installiert.";
$MESS["BX_CATALOG_PRODUCT_BASKET_ERR_NO_IBLOCK_ELEMENT"] = "Das Informationsblockelement wurde nicht gefunden.";
$MESS["BX_CATALOG_PRODUCT_BASKET_ERR_CANNOT_ADD_SKU"] = "Sie können nicht ein Produkt mit mehreren Produktvarianten zum Warenkorb hinzufügen. Nur eine ganz bestimmte Produktvariante kann zum Warenkorb hinzugefügt werden.";
$MESS["BX_CATALOG_PRODUCT_BASKET_ERR_PRODUCT_RUN_OUT"] = "Das Produkt ist nicht verfügbar.";
$MESS["BX_CATALOG_PRODUCT_BASKET_ERR_PRODUCT_BAD_TYPE"] = "Produkttyp ist nicht korrekt.";
$MESS["BX_CATALOG_PRODUCT_BASKET_ERR_NO_PRODUCT_SET"] = "Paketinhalte wurden nicht gefunden.";
$MESS["BX_CATALOG_PRODUCT_BASKET_ERR_NO_PRODUCT_SET_ITEMS"] = "Produkte im Paket wurden nicht gefunden.";
$MESS["BX_CATALOG_PRODUCT_BASKET_ERR_UNKNOWN"] = "Es gibt keine Informationen darüber, dass ein Produkt zum Warenkorb hinzugefügt wurde.";
$MESS["BX_CATALOG_PRODUCT_BASKET_ERR_EMPTY_QUANTITY"] = "Die Produktmenge, die zum Warenkorb hinzugefügt werden soll, ist nicht angegeben.";
?>