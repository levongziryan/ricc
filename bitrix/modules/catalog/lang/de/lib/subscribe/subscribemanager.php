<?
$MESS["ERROR_REQUIRED_PARAMATERS"] = "Der Parameter  \"#PARAM#\" ist erforderlich, fehlt jedoch.";
$MESS["ERROR_PRODUCT_NOT_FOUND"] = "Das Produkt, welches abonniert werden soll, kann nicht gefunden werden.";
$MESS["ERROR_SUBSCRIBE_DENIED"] = "Abonnement verweigert";
$MESS["ERROR_CONTACT_TYPE"] = "Ungültiger Kontakttyp für Abonnement";
$MESS["ERROR_VALIDATE_FIELDS"] = "Feldformat \"#FIELD#\" ist nicht gültig.";
$MESS["ERROR_SUBSCRIBE_ALREADY_EXISTS"] = "Sie haben das bereits abonniert";
$MESS["ERROR_SUBSCRIBE_ENTRY_CONFIRMATION_CODE"] = "Fehler beim Eingeben des Bestätigungscodes";
$MESS["ERROR_AUTHORIZATION_ACCESS_ROW_NOT_FOUND"] = "Fehler der Authentifizierung";
$MESS["ERROR_ACCESS_DENIDE_DELETE_SUBSCRIBE"] = "Sie haben nicht genügend Rechte, um Abonnement zu löschen";
?>