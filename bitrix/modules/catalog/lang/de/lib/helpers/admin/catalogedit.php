<?
$MESS["BX_CAT_HELPER_ADMIN_CATALOGEDIT_ERR_IBLOCK_ID_ABSENT"] = "ID des Informationsblocks fehlt.";
$MESS["BX_CAT_HELPER_ADMIN_CATALOGEDIT_ERR_IBLOCK_IS_NOT_EXISTS"] = "Der angegebene Informationsblock existiert nicht.";
$MESS["BX_CAT_HELPER_ADMIN_CATALOGEDIT_ERR_IBLOCK_SITELIST_IS_EMPTY"] = "Der Informationsblock hat keine Websites, die damit verknüpft wären.";
$MESS["BX_CAT_HELPER_ADMIN_CATALOGEDIT_ERR_EMPTY_DATA"] = "Keine Katalogdaten";
$MESS["BX_CAT_HELPER_ADMIN_CATALOGEDIT_ERR_BAD_DATA"] = "Das Format der Katalogdaten ist ungültig.";
$MESS["BX_CAT_HELPER_ADMIN_CATALOGEDIT_ERR_FIELD_CATALOG_IS_BAD"] = "Der Status \"Kommerzieller Katalog\" ist nicht definiert.";
$MESS["BX_CAT_HELPER_ADMIN_CATALOGEDIT_ERR_FIELD_USE_SKU_IS_ABSENT"] = "Der Status \"Produktvariante\" ist nicht definiert.";
$MESS["BX_CAT_HELPER_ADMIN_CATALOGEDIT_ERR_SKU_SELF"] = "Ein Informationsblock kann kein Informationsblock der Produktvariante für sich selbst sein.";
$MESS["BX_CAT_HELPER_ADMIN_CATALOGEDIT_ERR_SKU_BAD"] = "Ein Informationsblock der Produktvariante existiert nicht.";
$MESS["BX_CAT_HELPER_ADMIN_CATALOGEDIT_ERR_SKU_SITES_EMPTY"] = "Der Informationsblock der Produktvariante hat keine Websites, die damit verknüpft wären.";
$MESS["BX_CAT_HELPER_ADMIN_CATALOGEDIT_ERR_SKU_SITES_NOT_EQUAL"] = "Websites, die mit dem Master-Informationsblock verknüpft sind, stimmen nicht mit denen des Informationsblocks der Produktvariante überein.";
$MESS["BX_CAT_HELPER_ADMIN_CATALOGEDIT_ERR_SKU_WITH_SKU"] = "Ein Informationsblock der Produktvariante kann keine Produktvarianten haben.";
$MESS["BX_CAT_HELPER_ADMIN_CATALOGEDIT_ERR_SKU_FROM_OTHER_IBLOCK"] = "Der Informationsblock der Produktvariante ist bereits mit einem anderen Informationsblock verknüpft.";
$MESS["BX_CAT_HELPER_ADMIN_CATALOGEDIT_ERR_FIELD_SKU_IS_ABSENT"] = "Informationsblock der Produktvariante ist nicht ausgewählt.";
$MESS["BX_CAT_HELPER_ADMIN_CATALOGEDIT_ERR_FIELD_SUBSCRIPTION_IS_ABSENT"] = "Der Status \"Inhalte zum Verkauf\" ist nicht definiert.";
$MESS["BX_CAT_HELPER_ADMIN_CATALOGEDIT_ERR_OFFERS_ONLY_CATALOG"] = "Informationsblock der Produktvariante muss ein Kommerzieller Katalog sein.";
$MESS["BX_CAT_HELPER_ADMIN_CATALOGEDIT_ERR_PARENT_IBLOCK_WITH_SUBSCRIPTION"] = "Der Informationsblock der Produktvariante kann nicht zum Verkauf von Inhalten genutzt werden.";
$MESS["BX_CAT_HELPER_ADMIN_CATALOGEDIT_ERR_SKU_PARENT_IBLOCK_IS_ABSENT"] = "Für den Informationsblock der Produktvariante wurde kein Produktinformationsblock angegeben.";
$MESS["BX_CAT_HELPER_ADMIN_CATALOGEDIT_ERR_SKU_PARENT_IBLOCK_OTHER"] = "Der Muster-Produktinformationsblock ist ungültig.";
$MESS["BX_CAT_HELPER_ADMIN_CATALOGEDIT_ERR_VAT_ID_IS_ABSENT"] = "Die Steuer-ID ist nicht angegeben.";
$MESS["BX_CAT_HELPER_ADMIN_CATALOGEDIT_ERR_BAD_VAT_ID"] = "Die Steuer-ID ist nicht korrekt.";
$MESS["BX_CAT_HELPER_ADMIN_CATALOGEDIT_ERR_YANDEX_EXPORT_IS_ABSENT"] = "Der Status \"yandex export\" ist nicht definiert.";
$MESS["BX_CAT_HELPER_ADMIN_CATALOGEDIT_ERR_BAD_YANDEX_EXPORT"] = "Der Status \"yandex export\" ist ungültig.";
?>