<?
$MESS["IBLIST_CHPRICE_HEAD_TABLE_ACTION"] = "Aktion auswählen";
$MESS["IBLIST_CHPRICE_HEAD_TABLE_VALUE"] = "Wert eingeben";
$MESS["IBLIST_CHPRICE_HEAD_TABLE_UNITS"] = "Einheiten auswählen";
$MESS["IBLIST_CHPRICE_HEAD_TABLE_TYPE"] = "Preistyp auswählen";
$MESS["IBLIST_CHPRICE_TABLE_ACTION_TYPE_ADD"] = "Erhöhen";
$MESS["IBLIST_CHPRICE_TABLE_ACTION_TYPE_SUB"] = "Reduzieren";
$MESS["IBLIST_CHPRICE_TABLE_ACTION_RESULT_LABEL"] = "Abrunden/Aufrunden";
$MESS["IBLIST_CHPRICE_TABLE_ACTION_RESULT_FLOOR"] = "Immer zugunsten des Kunden (nach unten)";
$MESS["IBLIST_CHPRICE_TABLE_ACTION_RESULT_CEIL"] = "Immer zugunsten des Unternehmens (nach oben)";
$MESS["IBLIST_CHPRICE_TABLE_ACTION_ROUND_RESULT_ROUND"] = "Mathematische Regeln benutzen";
$MESS["IBLIST_CHPRICE_ALERT_NOT_NULL"] = "Nichtnull-Werte eingeben";
$MESS["IBLIST_CHPRICE_UNITS_NOTE_PERCENT"] = "Die Preisänderung wird auf alle ausgewählten Elemente angewendet.";
$MESS["IBLIST_CHPRICE_UNITS_NOTE_CURRENCY"] = "Mit Auswahl der Währung werden nur Werte aktualisiert, die dieser Währung entsprechen.";
$MESS["IBLIST_CHPRICE_TABLE_INITIAL_PRICE_TYPE"] = "Preis für Berechnung nutzen";
$MESS["IBLIST_CHPRICE_ALERT_ONE_PRICE_TYPE"] = "Die Funktion ist nicht verfügbar, weil es nur einen Preistyp gibt.";
$MESS["IBLIST_CHPRICE_TABLE_RESULT_MASK_LABEL"] = "Präzision beim Runden";
$MESS["IBLIST_CHPRICE_TABLE_MINUS_COUNT_LABEL"] = "Vom Preis abziehen";
$MESS["IBLIST_CHPRICE_EXAMPLE_LABEL"] = "Beispiel";
$MESS["IBLIST_CHPRICE_EXAMPLE_VALUE"] = "#VALUE_BEFORE# ab- aufrunden zu";
$MESS["IBLIST_CHPRICE_TABLE_UNIT_MULTYPLE"] = "mal";
$MESS["IBLIST_CHPRICE_PRICE_TYPE_EMPTY"] = "nicht ausgewählt";
$MESS["IBLIST_CHPRICE_ERR_BASE_PRICE_SELECTED"] = "Achtung! Sie sind gerade dabei, die Preise des Basispreistyps zu ändern. Alle davon abhängenden Preise werden neu kalkuliert. Fortfahren?";
$MESS["IBLIST_CHPRICE_ERR_EQUAL_PRICE_TYPES"] = "Der Typ des Quellpreises stimmt mit dem Preistyp überein, der geändert wird.";
$MESS["IBLIST_CHPRICE_ERR_DESTINATION_PRICE_EMPTY"] = "Es wurde kein Preistyp zur Änderung ausgewählt";
$MESS["IBLIST_CHPRICE_ERR_SOURCE_PRICE_EMPTY"] = "Es wurde kein Quellpreistyp ausgewählt";
?>