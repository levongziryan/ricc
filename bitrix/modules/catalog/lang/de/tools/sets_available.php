<?
$MESS["CAT_SETS_AVAILABLE_ERRORS_FATAL"] = "Operation kann nicht ausgeführt werden.";
$MESS["CAT_SETS_AVAILABLE_PAGE_TITLE"] = "Bestand und verfügbare Sätze aktualisieren";
$MESS["CAT_SETS_AVAILABLE_TAB"] = "Parameter";
$MESS["CAT_SETS_AVAILABLE_TAB_TITLE"] = "Parameter aktualisieren";
$MESS["CAT_SETS_AVAILABLE_UPDATE_BTN"] = "Start";
$MESS["CAT_SETS_AVAILABLE_STOP_BTN"] = "Stopp";
$MESS["CAT_SETS_AVAILABLE_MAX_EXECUTION_TIME"] = "Schrittlänge in Sek.";
$MESS["CAT_SETS_AVAILABLE_ERRORS_TITLE"] = "Fehler bei der Verarbeitung";
?>