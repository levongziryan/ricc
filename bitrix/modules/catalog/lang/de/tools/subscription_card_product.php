<?
$MESS["CSD_INCORRECT_SESSION"] = "Ihre Sitzung ist abgelaufen. Schließen Sie bitte das Dialogfenster und loggen sich ein, dann versuchen Sie erneut.";
$MESS["CSD_MODULE_NOT_INSTALLED"] = "Das Modul \"#NAME#\" ist nicht installiert.";
$MESS["CSD_ACCESS_DENIED"] = "Zugriff verweigert";
$MESS["CSD_NUMBER_SUBSCRIPTIONS"] = "Anzahl der Abonnements:";
$MESS["CSD_NUMBER_ACTIVE_SUBSCRIPTIONS"] = "Aktive Abonnements:";
$MESS["CSD_LIST_SUBSCRIPTIONS"] = "Verfügbare Abonnements:";
$MESS["CSD_LIST_SUBSCRIPTIONS_TEXT"] = "anzeigen";
?>