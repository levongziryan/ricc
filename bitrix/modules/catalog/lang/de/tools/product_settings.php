<?
$MESS["BX_CATALOG_PRODUCT_SETTINGS_ACCESS_DENIED"] = "Zugriff verweigert";
$MESS["BX_CATALOG_PRODUCT_SETTINGS_ERRORS_INCORRECT_SESSION"] = "Ihre Sitzung ist abgelaufen. Schließen Sie das Dialogfenster und melden Sie sich erneut an, danach wiederholen Sie bitte Ihre Aktion.";
$MESS["BX_CATALOG_PRODUCT_SETTINGS_REINDEX_ERRORS_MODULE_CATALOG_ABSENT"] = "Das Modul Kommerzieller Katalog konnte nicht angeschlossen werden";
$MESS["BX_CATALOG_PRODUCT_SETTINGS_PAGE_TITLE"] = "Standardmäßige Produktparameter";
$MESS["BX_CATALOG_PRODUCT_SETTINGS_TAB"] = "Parameter";
$MESS["BX_CATALOG_PRODUCT_SETTINGS_TAB_TITLE"] = "Produktparameter";
$MESS["BX_CATALOG_PRODUCT_SETTINGS_ENABLE_QUANTITY_TRACE"] = "Lagerverwaltung aktivieren";
$MESS["BX_CATALOG_PRODUCT_SETTINGS_ALLOW_CAN_BUY_ZERO"] = "Nicht verfügbare Produkte zum Kauf freigeben (dabei negative Produktmenge erlauben)";
$MESS["BX_CATALOG_PRODUCT_SETTINGS_PRODUCT_SUBSCRIBE"] = "Abonnement bei nicht verfügbaren Produkten erlauben";
$MESS["BX_CATALOG_PRODUCT_SETTINGS_MAX_EXECUTION_TIME"] = "Schrittdauer, in Sek.:";
$MESS["BX_CATALOG_PRODUCT_SETTINGS_UPDATE_BTN"] = "Speichern";
$MESS["BX_CATALOG_PRODUCT_SETTINGS_STOP_BTN"] = "Stopp";
$MESS["BX_CATALOG_PRODUCT_SETTINGS_STATUS_YES"] = "Ja";
$MESS["BX_CATALOG_PRODUCT_SETTINGS_STATUS_NO"] = "Nein";
?>