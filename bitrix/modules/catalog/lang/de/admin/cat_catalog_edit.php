<?
$MESS["CAT_CEDIT_IBLOCK_MODULE_IS_MISSING"] = "Fehler in Anwendung des Moduls Informationsblöcke.";
$MESS["CAT_CEDIT_CATALOG_MODULE_IS_MISSING"] = "Fehler in Anwendung des Moduls Kommerzieller Katalog.";
$MESS["CAT_CEDIT_BAD_IBLOCK"] = "Der Informationsblock wurde nicht gefunden bzw. der Zugriff wurde verweigert.";
$MESS["CAT_CEDIT_MAIN_TAB"] = "Katalog";
$MESS["CAT_CEDIT_MAIN_TAB_TITLE"] = "Katalogeinstellungen";
$MESS["CAT_CEDIT_PROPERTY_TAB"] = "Elementeigenschaften";
$MESS["CAT_CEDIT_PROPERTY_TAB_TITLE"] = "Parameter der Ansicht von Elementeigenschaften";
$MESS["CAT_CEDIT_EDIT_TITLE"] = "Einstellungen für: #IBLOCK_NAME#";
$MESS["CAT_CEDIT_SECTION_PROPERTY_FIELD"] = "Elementeigenschaften";
$MESS["CAT_CEDIT_PROP_TABLE_NAME"] = "Name";
$MESS["CAT_CEDIT_PROP_TABLE_TYPE"] = "Typ";
$MESS["CAT_CEDIT_PROP_TABLE_SMART_FILTER"] = "Im Smart-Filter anzeigen";
$MESS["CAT_CEDIT_PROP_TABLE_ACTION"] = "Aktionen";
$MESS["CAT_CEDIT_PROP_SELECT_CHOOSE"] = "<Auswählen>";
$MESS["CAT_CEDIT_PROP_SELECT_CREATE"] = "[Erstellen]";
$MESS["CAT_CEDIT_PROP_TABLE_ACTION_ADD"] = "Hinzufügen";
$MESS["CAT_CEDIT_PROP_TABLE_EMPTY"] = "-keine Daten-";
$MESS["CAT_CEDIT_PROP_SKU_SECTION"] = "Eigenschaften der Produktvariante";
$MESS["CAT_CEDIT_PROP_TABLE_DISPLAY_TYPE"] = "Ansicht im Smart-Filter";
$MESS["CAT_CEDIT_PROP_TABLE_ACTION_HIDE"] = "Ausblenden";
$MESS["CAT_CEDIT_PROP_TABLE_DISPLAY_EXPANDED"] = "Maximiert anzeigen";
$MESS["CAT_CEDIT_PROP_TABLE_FILTER_HINT"] = "Tipp für Nutzer";
?>