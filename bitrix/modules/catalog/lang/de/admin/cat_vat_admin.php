<?
$MESS["CVAT_ACTIVE"] = "Akt.";
$MESS["CVAT_YES"] = "aktive";
$MESS["CVAT_FILTER_ACTIVE"] = "Aktivität";
$MESS["CVAT_ALL"] = "alle";
$MESS["CVAT_EDIT_ALT"] = "Bearbeiten";
$MESS["ERROR_UPDATE_VAT"] = "Fehler beim Aktualisieren des MwSt.- Satzes ##ID# ";
$MESS["ERROR_DELETE_VAT"] = "Fehler beim Löschen des MwSt.- Satzes ##ID# ";
$MESS["CVAT_ADD_NEW"] = "Hinzufügen";
$MESS["CVAT_NO"] = "inaktiv";
$MESS["CVAT_DELETE_ALT"] = "Löschen";
$MESS["CVAT_NAV"] = "MwSt.-Sätze";
$MESS["CVAT_PAGE_TITLE"] = "MwSt.-Sätze";
$MESS["CVAT_ADD_NEW_ALT"] = "Neuen MwSt.-Satz hinzufügen";
$MESS["CVAT_RATE"] = "Satz";
$MESS["CVAT_FILTER_RATE"] = "Satz";
$MESS["CVAT_SORT"] = "Sort.";
$MESS["CVAT_NAME"] = "Name";
$MESS["CVAT_FILTER_NAME"] = "Name";
$MESS["CVAT_DELETE_CONF"] = "Sind Sie sicher, dass Sie  diesen Mehrwertsteuersatz  löschen wollen?";
?>