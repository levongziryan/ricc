<?
$MESS["CEEN_APPLY"] = "Anwenden";
$MESS["CEEN_ERROR_SAVING_EXTRA"] = "Fehler beim Speichern des Preisaufschlags ";
$MESS["CEEN_NAME"] = "Name";
$MESS["CEEN_NO_DISCOUNT"] = "Der Preisaufschlag ##ID# wurde nicht gefunden";
$MESS["CEEN_SAVE_ADD"] = "Hinzufügen";
$MESS["CEEN_TIMESTAMP"] = "Letztes Änderungdatum:";
$MESS["CEEN_NEW_DISCOUNT"] = "Neuen Preisaufschlag erstellen";
$MESS["CEEN_ADDING"] = "Neuen Preisaufschlag hinzufügen";
$MESS["CEEN_TAB_DISCOUNT"] = "Preisaufschlag";
$MESS["CEEN_DELETE_DISCOUNT"] = "Preisaufschlag löschen";
$MESS["CEEN_TO_LIST"] = "Preisaufschlagliste";
$MESS["CEEN_2FLIST"] = "Preisaufschlagliste";
$MESS["CEEN_TAB_DISCOUNT_DESCR"] = "Preisaufschlag-Parameter";
$MESS["CEEN_UPDATING"] = "Preisaufschlagparameter bearbeiten";
$MESS["CEEN_RECALC"] = "Preise neuberechnen";
$MESS["CEEN_NO_PERMS2ADD"] = "Sie haben keine Berechtigung, um einen neuen Preisaufschlag hinzuzufügen";
$MESS["CEEN_SORT"] = "Sortierindex";
$MESS["CEEN_SAVE"] = "Speichern";
$MESS["CEEN_PERCENTAGE"] = "Preisaufschlaghöhe";
$MESS["CEEN_DISCOUNT_VALUE"] = "Preisaufschlaghöhe";
$MESS["CEEN_DELETE_DISCOUNT_CONFIRM"] = "Sind Sie sicher, dass Sie  diesen Preisaufschlag  löschen wollen?";
$MESS["CEEN_CANCEL"] = "Abbrechen";
?>