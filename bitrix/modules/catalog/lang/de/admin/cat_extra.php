<?
$MESS["CEN_ERROR_UPDATE"] = "Fehler beim Aktualisieren des Preisaufschlagparameters ";
$MESS["EXTRA_DELETE_ERROR"] = "Fehler beim Löschen des Preisaufschlags ";
$MESS["EXTRA_NOTES"] = "Der Preis besteht aus dem Basispreis <b>plus</b>  angegebenes Prozent des Basispreises <br>[Preis] = [Basispreis] * (1 +[Prozent] / 100) = [Basispreis] + [Basispreis] * [Prozent] / 100 ";
$MESS["CEN_ADD_NEW_ALT"] = "Klicken Sie hier, um einen neuen Preisaufschlag hinzuzufügen";
$MESS["EXTRA_ACTIONS"] = "Löschen";
$MESS["CEN_ADD_NEW"] = "Neuer Preisaufschlag";
$MESS["CEN_DELETE_ALT"] = "Preisaufschlag löschen";
$MESS["cat_extra_nav"] = "Preisaufschläge";
$MESS["CEN_UPDATE_ALT"] = "Preisaufschlagparameter bearbeiten";
$MESS["EXTRA_NAME"] = "Preisaufschlagname";
$MESS["EXTRA_TITLE"] = "Preisaufschlagtypen";
$MESS["EXTRA_RECALCULATE"] = "Preise neuberechnen";
$MESS["EXTRA_PERCENTAGE"] = "Prozent";
$MESS["EXTRA_UPD"] = "Speichern";
$MESS["CEN_DELETE_CONF"] = "Sind  Sie sicher, dass Sie diesen Preisaufschlag  löschen wollen?";
?>