<?
$MESS["IB1C_ERROR_NO_PP"] = "Der Parameter \"PostPrices\" ist nicht vorhanden.";
$MESS["IB1C_ERROR_WRONG_GET"] = "Fehlerhafter String GET.";
$MESS["IB1C_ERROR_WRONG_AUTH"] = "Loginname oder Kennwort sind falsch.";
$MESS["IB1C_ERROR_NO_RIGHTS"] = "Sie haben keine Berechtigung für diese Funktion.";
?>