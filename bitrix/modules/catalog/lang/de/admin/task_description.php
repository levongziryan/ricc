<?
$MESS["TASK_DESC_CATALOG_DENIED"] = "Der Zugriff auf Modul \"Kommerzieller Katalog\" wurde verweigert";
$MESS["TASK_NAME_CATALOG_EXPORT_IMPORT"] = "Import-/Exportverwaltung";
$MESS["TASK_DESC_CATALOG_EXPORT_IMPORT"] = "Import-/Exportverwaltung der Produkte";
$MESS["TASK_NAME_CATALOG_READ"] = "Lesen";
$MESS["TASK_DESC_CATALOG_READ"] = "Lesezugriff auf die Parameter des Moduls \"Kommerzieller Katalog\"";
$MESS["TASK_DESC_CATALOG_PRICE_EDIT"] = "Verwaltung von Preisen, Preistypen, Preisaufschlägen, Rabatten und Steuern";
$MESS["TASK_NAME_CATALOG_PRICE_EDIT"] = "Preise bearbeiten";
$MESS["TASK_NAME_CATALOG_FULL_ACCESS"] = "Voller Zugriff";
$MESS["TASK_DESC_CATALOG_FULL_ACCESS"] = "Voller Zugriff auf Modul \"Katalog\"";
$MESS["TASK_NAME_CATALOG_DENIED"] = "Zugriff verweigert";
$MESS["TASK_NAME_CATALOG_STORE_EDIT"] = "Lagerverwaltung";
$MESS["TASK_DESC_CATALOG_STORE_EDIT"] = "Lagerbestände verwalten, Kaufpreise im kommerziellen Katalog anzeigen";
$MESS["TASK_NAME_CATALOG_VIEW"] = "Produkte anzeigen";
$MESS["TASK_DESC_CATALOG_VIEW"] = "Leserechte für Produktinformationen (ohne Zugriff auf Parameter des Kommerziellen Katalogs)";
?>