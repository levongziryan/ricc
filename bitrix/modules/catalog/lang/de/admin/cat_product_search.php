<?
$MESS["prod_search_cancel"] = "Abbrechen";
$MESS["SPS_ACT"] = "Akt.";
$MESS["SPS_ACTIVE"] = "Aktiv";
$MESS["prod_search_cancel_title"] = "Alle Einträge anzeigen";
$MESS["SPS_ID_FROM_TO"] = "Anfang und Ende";
$MESS["SPS_ANY"] = "beliebig";
$MESS["SPS_SECTION"] = "Bereich";
$MESS["SPS_DESCR"] = "Beschreibung";
$MESS["SPS_NAME"] = "Name";
$MESS["prod_search_find_title"] = "Einträge nach Suchkriterien finden";
$MESS["SPS_SET"] = "Einstellen";
$MESS["SPS_TIMESTAMP"] = "Änderungdatum";
$MESS["SPS_CHANGER"] = "Geändert von";
$MESS["SPS_YES"] = "Ja";
$MESS["SPS_CATALOG"] = "Katalog";
$MESS["SPS_NO"] = "Nein";
$MESS["SPS_TOP_LEVEL"] = "Obere Ebene";
$MESS["sale_prod_search_nav"] = "Produkte";
$MESS["SPS_SEARCH_TITLE"] = "Produkte wählen";
$MESS["SPS_CLOSE"] = "Schließen";
$MESS["SPS_NO_PERMS"] = "Sie haben keine Berechtigung, diesen Katalog durchzuschauen";
$MESS["SPS_STATUS"] = "Status";
$MESS["prod_search_find"] = "Suchen";
$MESS["SPS_INCLUDING_SUBS"] = "einschließlich Unterbereiche ";
$MESS["SPS_SELECT"] = "Auswählen";
$MESS["SPS_UNSET"] = "Zurücksetzen";
?>