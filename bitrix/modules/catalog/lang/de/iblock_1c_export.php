<?
$MESS["C_F_DATAFILE1_NOTE"] = "(Auswahl von der Festplatte)";
$MESS["C_F_DATAFILE"] = "Datendatei vom lokalen Computer:";
$MESS["C_DATA_LOADING"] = "Datenimport";
$MESS["C_F_TITLE"] = "Datenimport im CommerceXML Format";
$MESS["CATI_OF_DEACT"] = "deaktivieren";
$MESS["C_LOAD_TIME1"] = "Der Import benötigte";
$MESS["C_ERROR_NO_IBLOCKTYPE"] = "Der Informationsblocktyp wurde nicht ausgewählt. Importieren ist nicht möglich.";
$MESS["C_ERROR_NO_DATAFILE"] = "Die Datei mit den Daten wurde nicht importiert und nicht ausgewählt. Der Import ist nicht möglich.";
$MESS["C_LOAD_ERROR"] = "Falsch:";
$MESS["C_LOAD_CHANGED"] = "Geändert:";
$MESS["C_LOAD_PROPS"] = "Iimportierte Eigenschaften:";
$MESS["C_LOAD_GROUP"] = "Iimportierte Gruppen:";
$MESS["C_LOAD_CATALOG"] = "Iimportierte Kataloge:";
$MESS["C_LOAD_PRODUCT"] = "Iimportierte Produkte:";
$MESS["C_F_IBLOCK"] = "Informationsblocktyp:";
$MESS["C_F_LOAD"] = "Laden";
$MESS["CATI_OF_DEL"] = "löschen";
$MESS["C_LOAD_NEW"] = "neue:";
$MESS["C_F_DATAFILE1"] = "ODER Datendatei von der Website:";
$MESS["C_F_OUTFILEACTION"] = "Produkte, die es nicht in der Datei gab";
$MESS["C_LOAD_TIME2"] = "Sek";
$MESS["CATI_OF_KEEP"] = "so lassen";
$MESS["C_F_IBLOCK_SELECT"] = "wählen";
?>