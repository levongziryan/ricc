<?
$MESS["KGP_EMPTY_ID"] = "Die Produkt-ID wurde nicht angegeben";
$MESS["BT_MOD_CATALOG_PROD_ERR_PRODUCT_ID_ABSENT"] = "Die Produkt-ID fehlt oder ist nicht korrekt.";
$MESS["BT_MOD_CATALOG_PROD_ERR_QUANTITY_ABSENT"] = "Die Produktmenge fehlt oder ist nicht korrekt.";
$MESS["BT_MOD_CATALOG_PROD_ERR_ELEMENT_ID_NOT_FOUND"] = "Das Informationsblockelement Nr.#ID# wurde nicht gefunden.";
$MESS["BT_MOD_CATALOG_PROD_ERR_COST_CURRENCY"] = "Die Kaufwährung ist nicht angegeben.";
$MESS["BT_MOD_CATALOG_PROD_ERR_NO_RESULT_CURRENCY"] = "Währung für Berechnung ist nicht angegeben";
?>