<?
$MESS["BT_CAT_SET_ERR_ITEMS_IS_ABSENT"] = "Die Produkte, welche im Paket/Satz enthalten sind, sind nicht angegeben.";
$MESS["BT_CAT_SET_ERR_PRODUCT_ID_IS_BAD"] = "Die ID des Produktes, für welches ein Satz erstellt wird, ist nicht korrekt.";
$MESS["BT_CAT_SET_ERR_TYPE_IS_BAD"] = "Der Elementtyp ist nicht korrekt";
$MESS["BT_CAT_SET_ERR_ID_IS_BAD"] = "Die Element-ID ist nicht korrekt";
$MESS["BT_CAT_SET_ERR_EMPTY_VALID_ITEMS"] = "Es fehlen Produkte, die im Element enthalten sind";
$MESS["BT_CAT_PRODUCT_SET_ERR_ITEM_ID_IS_BAD"] = "Die Produkt-ID ist nicht korrekt";
$MESS["BT_CAT_PRODUCT_SET_ERR_ITEM_ID_DUBLICATE"] = "Das Produkt wiederholt sich im Paket/Satz.";
$MESS["BT_CAT_PRODUCT_SET_ERR_QUANTITY_IS_BAD"] = "Die Produktmenge im Paket ist nicht korrekt.";
$MESS["BT_CAT_PRODUCT_SET_ERR_QUANTITY_GROUP_IS_BAD"] = "Die Produktmenge für Produktpaket ist nicht korrekt";
$MESS["BT_CAT_PRODUCT_SET_ERR_DISCOUNT_PERCENT_IS_BAD"] = "Das Rabattprozent im Paket ist nicht korrekt.";
$MESS["BT_CAT_PRODUCT_SET_ERR_ALL_DISCOUNT_PERCENT_IS_BAD"] = "Der Gesamtwert des Rabattprozentes ist höher als 100.";
$MESS["BT_CAT_PRODUCT_SET_ERR_ITEM_ID_IS_ABSENT"] = "Die Produkt-ID ist nicht angegeben";
$MESS["BT_CAT_PRODUCT_SET_ERR_BAD_ITEMS_IN_SET"] = "Nur Produkte oder Produktvarianten können zu einem Paket hinzugefügt werden.";
$MESS["BT_CAT_PRODUCT_SET_ERR_BAD_ITEMS_IN_GROUP"] = "Nur Produkte, Produkt-Sets oder Produktvarianten können zu einem Paket hinzugefügt werden.";
?>