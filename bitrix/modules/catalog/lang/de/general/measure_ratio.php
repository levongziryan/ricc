<?
$MESS["CATALOG_MEASURE_RATIO_BAD_ACTION"] = "Nicht korrekte Aktion angegeben (nur erstellen oder aktualisieren)";
$MESS["CATALOG_MEASURE_RATIO_EMPTY_CLEAR_FIELDS"] = "Datenbereich ist nach Validierung des Schlüssels leer";
$MESS["CATALOG_MEASURE_RATIO_PRODUCT_ID_IS_ABSENT"] = "Die Produkt-ID ist nicht angegeben.";
$MESS["CATALOG_MEASURE_RATIO_BAD_PRODUCT_ID"] = "Produkt-ID ist nicht korrekt";
$MESS["CATALOG_MEASURE_RATIO_RATIO_ALREADY_EXIST"] = "Koeffizient #RATIO# existiert bereits für dieses Produkt";
?>