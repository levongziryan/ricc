<?
$MESS["BX_CATALOG_EXPORT_YANDEX_ERR_BAD_SITE"] = "No site with this ID was found, or it was deactivated";
$MESS["BX_CATALOG_EXPORT_YANDEX_ERR_BAD_SERVER_NAME"] = "Domainname ist nicht angegeben";
$MESS["BX_CATALOG_EXPORT_YANDEX_ERR_UNLINK_FILE"] = "Fehler beim Löschen der Datei #FILE#";
$MESS["BX_CATALOG_EXPORT_YANDEX_ERR_RENAME_FILE"] = "Die Exportdatei kann nicht umbenannt werden #FILE#";
?>