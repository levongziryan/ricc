<?
$MESS["BX_CATALOG_EXPORT_IBLOCK"] = "Wählen Sie einen Informationsblock zum Export aus:";
$MESS["BX_CATALOG_EXPORT_YANDEX_SITE"] = "Wählen Sie eine Website zum Export aus:";
$MESS["BX_CATALOG_EXPORT_YANDEX_COMPANY_NAME"] = "Unternehmensname:";
$MESS["BX_CATALOG_EXPORT_YANDEX_ERR_EMPTY_SITE"] = "ID der zu exportierenden Website ist nicht angegeben";
$MESS["BX_CATALOG_EXPORT_YANDEX_ERR_BAD_SITE"] = "Es wurde keine Website mit dieser ID gefunden, oder die Website wurde deaktiviert";
?>