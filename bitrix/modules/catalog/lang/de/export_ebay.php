<?
$MESS["EBAY_ERR_BAD_PRICE_TYPE"] = "Der Preistyp für Export ist nicht korrekt.";
$MESS["EBAY_ERR_FILE_OPEN_WRITING"] = "Die Datei #FILE# kann für einen Eintrag nicht geöffnet werden.";
$MESS["EBAY_ERR_SETUP_FILE_WRITE"] = "Eintrag in der Datei #FILE# kann nicht gemacht werden.";
?>