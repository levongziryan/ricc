<?
$MESS["JC_CORE_TREE_SELECT_CONTROL"] = "Bedingung auswählen";
$MESS["JC_CORE_TREE_ADD_CONTROL"] = "Bedingung hinzufügen";
$MESS["JC_CORE_TREE_DELETE_CONTROL"] = "Bedingung löschen";
$MESS["JC_CORE_TREE_CONTROL_DATETIME_ICON"] = "Klicken Sie, um Daten auszuwählen";
$MESS["JC_CORE_TREE_CONDITION_ERROR"] = "Fehler der Bedingung";
$MESS["JC_CORE_TREE_CONDITION_FATAL_ERROR"] = "Fataler Fehler der Bedingung. Es wird empfohlen, die Bedingung zu löschen.";
?>