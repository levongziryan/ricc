<?
$MESS["VIDEOPORT_ServerHost"] = "Адреса сервера";
$MESS["VIDEOPORT_ServerPort"] = "Порт сервера";
$MESS["VIDEOPORT_SecretCode"] = "Секретне слово";
$MESS["VIDEOPORT_window_main"] = "Показувати головне вікно відео";
$MESS["VIDEOPORT_window_chat"] = "Показувати вікно чату";
$MESS["VIDEOPORT_window_members"] = "Показувати вікно учасників конференції";
$MESS["VIDEOPORT_window_settings"] = "Показувати вікно налаштувань";
$MESS["VIDEOPORT_window_info"] = "Показувати вікно інформації";
$MESS["VIDEOPORT_window_self_camera"] = "Показувати вікно зі своїм зображенням";
$MESS["VIDEOPORT_window_show_invite"] = "Дозволяти запрошувати користувачів";
?>