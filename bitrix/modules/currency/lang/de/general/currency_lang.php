<?
$MESS["BT_CUR_LANG_SEP_VARIANT_EMPTY"] = "Ohne Trennzeichen";
$MESS["BT_CUR_LANG_SEP_VARIANT_DOT"] = "Punkt";
$MESS["BT_CUR_LANG_SEP_VARIANT_COMMA"] = "Komma";
$MESS["BT_CUR_LANG_SEP_VARIANT_SPACE"] = "Leerzeichen";
$MESS["BT_CUR_LANG_SEP_VARIANT_NBSPACE"] = "Geschütztes Leerzeichen";
$MESS["BT_CUR_LANG_CURRENCY_RUBLE"] = "RUB";
$MESS["BT_CUR_LANG_ERR_FORMAT_STRING_IS_EMPTY"] = "Das Feld \"Format\" für die Sprache #LANG# ist leer.";
$MESS["BT_CUR_LANG_ERR_THOUSANDS_SEP_IS_EMPTY"] = "Ein benutzerdefiniertes Tausendtrennzeichen ist für #LANG# nicht angegeben.";
$MESS["BT_CUR_LANG_ERR_THOUSANDS_SEP_IS_NOT_VALID"] = "Für #LANG# ist ein ungültiges benutzerdefiniertes Tausendtrennzeichen angegeben. Nur die Unicode-Zeichen und HTML-Elemente können als Trennzeichen genutzt werden.";
?>