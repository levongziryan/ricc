<?
$MESS["CIMP_INPUT_DESCRIPTION"] = "Geld";
$MESS["CIMP_INPUT_FORMAT_ERROR_1"] = "Nur Zahlen und Trennzeichen: [#separators#] (#example#)";
$MESS["CIMP_INPUT_FORMAT_ERROR_2"] = "Ungültiges Format. Ein Beispiel: (#example#)";
$MESS["CIMP_SEPARATOR_SPACE"] = "Leerzeichen";
$MESS["CIMP_FORMAT_ERROR"] = "Geldfeld hat ein ungültiges Format.";
$MESS["CIMP_INPUT_FORMAT_ERROR"] = "Das Format des Geldfeldes ist ungültig. Zum Beispiel: #example#";
?>