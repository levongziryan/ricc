<?
$MESS["FACEID_TAB_SETTINGS"] = "Paramètres";
$MESS["FACEID_TAB_SETTINGS_BUY"] = "Solde";
$MESS["FACEID_TAB_TITLE_SETTINGS_2"] = "Paramètres de connexion";
$MESS["FACEID_TAB_TITLE_SETTINGS_3"] = "Statistiques de reconnaissance";
$MESS["FACEID_ACCOUNT_ERROR_PUBLIC"] = "Adressespécifiée incorrecte.";
$MESS["FACEID_PUBLIC_URL"] = "Adresse du site";
$MESS["FACEID_WAIT_RESPONSE"] = "Autoriser un temps d'attente plus long";
$MESS["FACEID_WAIT_RESPONSE_DESC"] = "Cochez cette option si vous ne recevez pas de réponse du serveur à cause des paramètres environnementaux.";
$MESS["FACEID_ACCOUNT_DEBUG"] = "Mode de débogage";
$MESS["FACEID_ADM_STATS_BALANCE"] = "Il vous reste <span class=\"adm-table-content-text-bold\">#COUNT#</span> reconnaissances à utiliser avant le #DATE#";
$MESS["FACEID_ADM_STATS_USAGE"] = "Nombre de reconnaissances utilisées en 30 jours - <span class=\"adm-table-content-text-bold\">#COUNT #</span> - dont :";
$MESS["FACEID_ADM_STATS_BALANCE_EMPTY"] = "Les statistiques ne sont pas disponibles. Vous n'avez probablement pas encore démarré la reconnaissance faciale.";
$MESS["FACEID_ADM_STATS_USAGE_1C"] = "Carte faciale";
$MESS["FACEID_ADM_STATS_USAGE_FTRACKER"] = "Traqueur facial";
$MESS["FACEID_ADM_STATS_USAGE_VTRACKER"] = "Enregistrement en direct";
$MESS["FACEID_ADM_STATS_BY_1000"] = "Acheter 1000 crédits de reconnaissance";
?>