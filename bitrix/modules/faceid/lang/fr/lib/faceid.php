<?
$MESS["FACEID_TRACKER"] = "Tracker facial";
$MESS["FACEID_CLOUD_ERR_FAIL"] = "Erreur lors de d’obtention des données du cloud.";
$MESS["FACEID_CLOUD_ERR_FAIL_CONNECT"] = "Erreur de connexion au cloud.";
$MESS["FACEID_CLOUD_ERR_FAIL_RESPONSE"] = "Erreur lors de d’obtention d'une réponse du cloud.";
$MESS["FACEID_CLOUD_ERR_FAIL_TEMPORARY_UNAVAILABLE"] = "Le service cloud est temporairement indisponible, veuillez réessayer plus tard.";
$MESS["FACEID_CLOUD_ERR_FAIL_UNKNOWN_COMMAND"] = "Erreur lors de l'envoi de la demande au cloud.";
$MESS["FACEID_CLOUD_ERR_FAIL_UNKNOWN_SERVICE"] = "Le service exécutant la commande n’est pas spécifié.";
$MESS["FACEID_CLOUD_ERR_FAIL_GALLERY_CREATE"] = "Erreur lors de la création de la galerie du portail sur le cloud.";
$MESS["FACEID_CLOUD_ERR_FAIL_SAVE_LOCAL_PHOTO"] = "Erreur lors de l’enregistrement de la photo sur le portail. Veuillez réessayer plus tard.";
$MESS["FACEID_CLOUD_ERR_FAIL_PORTAL_REGISTER"] = "Erreur lors de l’inscription du portail sur le cloud.";
$MESS["FACEID_CLOUD_ERR_FAIL_NO_CREDITS"] = "Crédits insuffisants sur le solde du portail.";
$MESS["FACEID_CLOUD_ERR_FAIL_DETECT_FACE"] = "Le Face tracker n'a pas réussi à reconnaître le visage.";
$MESS["FACEID_CLOUD_ERR_FAIL_PROVIDER_RESPONSE"] = "Le Face tracker est temporairement indisponible.";
$MESS["FACEID_CLOUD_ERR_OK_UNKNOWN_PERSON"] = "Le visage n’est pas reconnu.";
$MESS["FACEID_CLOUD_ERR_OK_NOT_FOUND"] = "Le visage n'a pas été trouvé dans le cloud.";
?>