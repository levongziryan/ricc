<?
$MESS["FACEID_LICENSE_AGREEMENT_HTML_RICH"] = "<div class=\"tracker-agreement-popup-content\">
   <ol class=\"tracker-agreement-popup-list\">
    <li class=\"tracker-agreement-popup-list-item\">
	Le Service FindFace (ci-après dénommé \"Service FindFace\") est fourni par N-TECH.LAB LTD. 
	Les utilisateurs du Service FindFace sont tenus de souscrire un accord avec le Service FindFace et d'accepter les <a href=\"https://saas.findface.pro/files/docs/license_us.pdf\" class=\"tracker-link\">termes et conditions suivants</a> en sélectionnant \"J'accepte\". 
	<br>
	En sélectionnant \"J'accepte\", l'Utilisateur accepte et garantit qu'il se conformera aux exigences suivantes : 
     <ul class=\"tracker-agreement-popup-inner-list\">
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Respecter strictement toutes les dispositions légales et réglementaires locales en matière d'utilisation des données à caractère privé et personnel pour le pays de résidence de l'Utilisateur.
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Respecter strictement toutes les dispositions légales et réglementaires locales. Cette exigence demeurera en vigueur pour toute la durée d'utilisation du Service FindFace. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Obtenir une autorisation écrite ou toute autre manière prescrite par les dispositions locales de chaque pays depuis lequel l'Utilisateur exerce ses activités.
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       L'Utilisateur est tenu de faire part avec diligence de son intention de traiter à travers l'utilisation du  Service FindFace les données personnelles, y compris les photographies et les images, à tout individu visé par le Service.
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Solliciter des conseils juridiques AVANT d'utiliser le Service FindFace. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
	   Garantir que toute information téléchargée par l'Utilisateur a fait l'objet de l'autorisation de l'individu, et est conforme aux dispositions et réglementations de la juridiction de l'Utilisateur.
	  </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
	  Confirmer que l'Utilisateur utilise le Service FindFace à ses propres risques, et sans garantie d'aucune sorte. 
	  </li>	  
     </ul>
    </li>
    <li class=\"tracker-agreement-popup-list-item\">
     Bitrix, Inc, ne peut en aucun cas être tenu responsable du Service FindFace, et ne fait aucune allégation et n'offre aucune garantie concernant l'exactitude ou la disponibilité du Service, et ne fournit aucun appui technique ni consultation à propos du Service. 
    </li>
   </ol>
   <div class=\"tracker-agreement-popup-description\">
   Bitrix, Inc, ne peut en aucun cas être tenu responsable du Service FindFace, et ne fait aucune allégation et n'offre aucune garantie.
 En acceptant les termes et conditions du présent contrat de service, l'Utilisateur accepte et confirme que ni le Service FindFace, ni Bitrix, Inc, ne recueille ou ne traite les données téléchargées par l'Utilisateur.
L'Utilisateur confirme que ni le Service FindFace, ni Bitrix, Inc, n'ont aucune connaissance des circonstances dans lesquelles l'Utilisateur a collecté les données, les photographies ou les images.
L'Utilisateur décharge de tout dommage le Service FindFace et Bitrix de quelque action et inaction de l'Utilisateur que ce soit.
	<br><br>
Règlement de différends; Arbitrage exécutoire : tout différend, contestation, interprétation ou réclamation,  y compris, mais sans s'y limiter, les réclamations pour violation de contrat, toute forme de négligence, fraude ou fausse déclaration découlant de ou lié au présent contrat, doit être soumis à un arbitrage définitif et contraignant.
   </div>
  </div>";
$MESS["FACEID_LICENSE_AGREEMENT_HTML"] = "Le Service FindFace (ci-après dénommé \"Service FindFace\") est fourni par N-TECH.LAB LTD. Les utilisateurs du Service FindFace sont tenus de souscrire un accord avec le Service FindFace et d'accepter les <a href='https://saas.findface.pro/files/docs/license_us.pdf'>termes et conditions suivants</a> en sélectionnant \"J'accepte\". En sélectionnant \"J'accepte\", l'Utilisateur accepte et garantit qu'il se conformera aux exigences suivantes : 
* Respecter strictement toutes les dispositions légales et réglementaires locales en matière d'utilisation des données à caractère privé et personnel pour le pays de résidence de l'Utilisateur. 
* Respecter strictement toutes les dispositions légales et réglementaires locales. Cette exigence demeurera en vigueur pour toute la durée d'utilisation du Service FindFace. 
* Obtenir une autorisation écrite ou toute autre manière prescrite par les dispositions locales de chaque pays depuis lequel l'Utilisateur exerce ses activités.
* L'Utilisateur est tenu de faire part avec diligence de son intention de traiter à travers l'utilisation du Service FindFace les données personnelles, y compris les photographies et les images, à tout individu visé par le Service. 
* Solliciter des conseils juridiques AVANT d'utiliser le Service FindFace.
* Garantir que toute information téléchargée par l'Utilisateur a fait l'objet de l'autorisation de l'individu, et est conforme aux dispositions et réglementations de la juridiction de l'Utilisateur.
* Confirmer que l'Utilisateur utilise le Service FindFace à ses propres risques, et sans garantie d'aucune sorte. Bitrix, Inc ne peut en aucun cas être tenu responsable du Service FindFace, et ne fait aucune allégation et n'offre aucune garantie concernant l'exactitude ou la disponibilité du Service, et ne fournit aucun appui technique ni consultation à propos du Service.

Bitrix, Inc, ne peut en aucun cas être tenu responsable du Service FindFace, et ne fait aucune allégation et n'offre aucune garantie. En acceptant les termes et conditions du présent contrat de service, l'Utilisateur accepte et confirme que ni le Service FindFace, ni Bitrix, Inc, ne recueille ou ne traite les données téléchargées par l'Utilisateur.L'Utilisateur confirme que ni le Service FindFace, ni Bitrix, Inc, n'ont aucune connaissance des circonstances dans lesquelles l'Utilisateur a collecté les données, les photographies ou les images. L'Utilisateur décharge de tout dommage le Service FindFace et Bitrix de quelque action et inaction de l'Utilisateur que ce soit.

Règlement de différends; Arbitrage exécutoire : tout différend, contestation, interprétation ou réclamation, y compris, mais sans y limiter, les réclamations pour violation de contrat, toute forme de négligence, fraude ou fausse déclaration découlant de ou lié au présent contrat, doit être soumis à un arbitrage définitif et contraignant.";
$MESS["FACEID_LEAD_SOURCE_DEFAULT"] = "Face tracker";
?>