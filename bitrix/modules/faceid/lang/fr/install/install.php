<?
$MESS["FACEID_MODULE_NAME"] = "Reconnaissance faciale";
$MESS["FACEID_MODULE_DESCRIPTION"] = "Module de reconnaissance faciale pour une utilisation avec Bitrix24.";
$MESS["FACEID_INSTALL_TITLE"] = "Installation du module \"Reconnaissance faciale\"";
$MESS["FACEID_UNINSTALL_TITLE"] = "Désinstallation du module \"Reconnaissance faciale\"";
$MESS["FACEID_UNINSTALL_QUESTION"] = "Voulez-vous vraiment supprimer le module ?";
$MESS["FACEID_CHECK_PUBLIC_PATH"] = "Adresse e-mail spécifiée incorrecte.";
?>