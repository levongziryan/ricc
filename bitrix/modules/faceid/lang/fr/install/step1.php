<?
$MESS["FACEID_PUBLIC_PATH_DESC"] = "Le module nécessite une adresse de site public correcte pour fonctionner correctement.";
$MESS["FACEID_PUBLIC_PATH_DESC_2"] = "Si l'accès externe à votre réseau est restreint, activez-le seulement pour certaines pages. Veuillez consulter la #LINK_START#documentation#LINK_END# pour plus de détails.";
$MESS["FACEID_PUBLIC_PATH"] = "Adresse du site :";
?>