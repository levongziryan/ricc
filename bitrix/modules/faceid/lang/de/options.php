<?
$MESS["FACEID_TAB_SETTINGS"] = "Einstellungen";
$MESS["FACEID_TAB_SETTINGS_BUY"] = "Guthaben";
$MESS["FACEID_TAB_TITLE_SETTINGS_2"] = "Verbindungsparameter";
$MESS["FACEID_TAB_TITLE_SETTINGS_3"] = "Erkennungsstatistik";
$MESS["FACEID_ACCOUNT_ERROR_PUBLIC"] = "Es wurde eine nicht korrekte öffentliche Adresse angegeben.";
$MESS["FACEID_PUBLIC_URL"] = "Öffentliche Website-Adresse";
$MESS["FACEID_WAIT_RESPONSE"] = "Längere Wartezeit erlauben";
$MESS["FACEID_WAIT_RESPONSE_DESC"] = "Aktivieren Sie diese Option, wenn Sie keine Antwort vom Server aufgrund der Einstellungen Ihrer Umgebung erhalten.";
$MESS["FACEID_ACCOUNT_DEBUG"] = "Debug-Modus";
$MESS["FACEID_ADM_STATS_BALANCE"] = "Sie haben noch <span class=\"adm-table-content-text-bold\">#COUNT#</span> Erkennungen, die Sie bis #DATE# nutzen können";
$MESS["FACEID_ADM_STATS_BALANCE_0"] = "Sie haben noch <span class=\"adm-table-content-text-bold\">0</span> Erkennungen";
$MESS["FACEID_ADM_STATS_USAGE"] = "Anzahl der Erkennungen, die innerhalb von 30 Tagen genutzt wurden <span class=\"adm-table-content-text-bold\">#COUNT#</span>, inklusive:";
$MESS["FACEID_ADM_STATS_BALANCE_EMPTY"] = "Statistik ist nicht verfügbar. Wahrscheinlich haben Sie die Gesichtserkennung noch nie genutzt.";
$MESS["FACEID_ADM_STATS_USAGE_1C"] = "Face-Card";
$MESS["FACEID_ADM_STATS_USAGE_FTRACKER"] = "Face Tracker";
$MESS["FACEID_ADM_STATS_USAGE_VTRACKER"] = "Aufzeichnung";
$MESS["FACEID_ADM_STATS_BY_1000"] = "1000 Punkte für Erkennungen kaufen";
?>