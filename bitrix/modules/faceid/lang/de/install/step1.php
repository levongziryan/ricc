<?
$MESS["FACEID_PUBLIC_PATH_DESC"] = "Das Modul erfordert eine korrekte öffentliche Website-Adresse, damit sie korrekt funktioniert.";
$MESS["FACEID_PUBLIC_PATH_DESC_2"] = "Ist der externe Zugriff auf Ihr Netzwerk eingeschränkt, können Sie den Zugriff nur auf bestimmte Seiten gewähren. Details finden Sie in der #LINK_START#documentation#LINK_END#.";
$MESS["FACEID_PUBLIC_PATH"] = "Öffentliche Website-Adresse:";
?>