<?
$MESS["FACEID_MODULE_NAME"] = "Gesichtserkennung";
$MESS["FACEID_MODULE_DESCRIPTION"] = "Das Modul der Gesichtserkennung zur Nutzung mit Bitrix24.";
$MESS["FACEID_INSTALL_TITLE"] = "Das Modul \"Gesichtserkennung\" installieren";
$MESS["FACEID_UNINSTALL_TITLE"] = "Das Modul \"Gesichtserkennung\" deinstallieren";
$MESS["FACEID_UNINSTALL_QUESTION"] = "Möchten Sie dieses Modul wirklich löschen?";
$MESS["FACEID_CHECK_PUBLIC_PATH"] = "Es wurde eine nicht korrekte öffentliche Adresse angegeben.";
?>