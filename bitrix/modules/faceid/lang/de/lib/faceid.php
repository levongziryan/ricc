<?
$MESS["FACEID_TRACKER"] = "Face Tracker";
$MESS["FACEID_CLOUD_ERR_FAIL"] = "Fehler beim Anfordern von Daten aus der Cloud.";
$MESS["FACEID_CLOUD_ERR_FAIL_CONNECT"] = "Fehler bei der Verbindung mit der Cloud.";
$MESS["FACEID_CLOUD_ERR_FAIL_RESPONSE"] = "Fehler beim Erhalten der Antwort von der Cloud.";
$MESS["FACEID_CLOUD_ERR_FAIL_TEMPORARY_UNAVAILABLE"] = "Der Cloud-Service ist vorübergehend nicht verfügbar, versuchen Sie bitte später erneut.";
$MESS["FACEID_CLOUD_ERR_FAIL_UNKNOWN_COMMAND"] = "Fehler beim Senden einer Anfrage an die Cloud.";
$MESS["FACEID_CLOUD_ERR_FAIL_UNKNOWN_SERVICE"] = "Der Service zur Befehlsausführung ist nicht angegeben.";
$MESS["FACEID_CLOUD_ERR_FAIL_GALLERY_CREATE"] = "Fehler beim Erstellen einer Galerie in der Cloud.";
$MESS["FACEID_CLOUD_ERR_FAIL_SAVE_LOCAL_PHOTO"] = "Fehler beim Speichern des Fotos auf dem Portal. Versuchen Sie bitte erneut.";
$MESS["FACEID_CLOUD_ERR_FAIL_PORTAL_REGISTER"] = "Fehler beim Registrieren des Portals in der Cloud.";
$MESS["FACEID_CLOUD_ERR_FAIL_NO_CREDITS"] = "Unzureichende Mittel auf dem Portal-Guthaben.";
$MESS["FACEID_CLOUD_ERR_FAIL_DETECT_FACE"] = "Das Gesicht konnte nicht erkannt werden.";
$MESS["FACEID_CLOUD_ERR_FAIL_PROVIDER_RESPONSE"] = "Die Gesichtserkennung ist temporär nicht verfügbar.";
$MESS["FACEID_CLOUD_ERR_OK_UNKNOWN_PERSON"] = "Das Gesicht wurde nicht erkannt.";
$MESS["FACEID_CLOUD_ERR_OK_NOT_FOUND"] = "Das Gesicht wurde in der Cloud nicht gefunden.";
?>