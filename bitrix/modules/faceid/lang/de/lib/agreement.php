<?
$MESS["FACEID_LICENSE_AGREEMENT_HTML_RICH"] = "<div class=\"tracker-agreement-popup-content\">
   <ol class=\"tracker-agreement-popup-list\">
    <li class=\"tracker-agreement-popup-list-item\">
 Der Service FindFace (weiterhin \"FindFace Service\") wird von N-TECH.LAB LTD bereitgestellt. 
 Nutzer des FindFace Service müssen eine Vereinbarung mit dem FindFace Service treffen und <a href=\"https://saas.findface.pro/files/docs/license_us.pdf\" class=\"tracker-link\">folgende Bedingungen</a> akzeptieren, indem sie \"Ich akzeptiere\" auswählen. 
 <br>
 Mit der Auswahl  von \"Ich akzeptiere\" erklärt sich der Nutzer bereit, folgende Anforderungen zu erfüllen: 
     <ul class=\"tracker-agreement-popup-inner-list\">
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Alle lokalen Gesetze und Regelungen einzuhalten, die den Datenschutz im Land des Nutzers bestimmen. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Alle lokalen Gesetze und Regelungen einzuhalten. Diese Anforderung gilt innerhalb der ganzen Zeit der Nutzung vom FindFace Service. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Eine schriftliche Erlaubnis bzw. Ein anderes schriftliches Dokument zu erhalten, welches lokaler Gesetzgebung des Nutzerlandes entspricht. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Der Nutzer muss jede Person informieren, deren persönliche Daten inkl. Fotos oder Bilder der Nutzer durch Nutzung vom FindFace Service bearbeiten wird.
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Sich rechtlich beraten zu lassen, BEVOR der FindFace Service genutzt wird.
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
    Zu garantieren, dass alle Informationen, die der Nutzer hochladen wird, von Personen, auf die sie sich beziehen, erlaubt sind und lokaler Gesetzgebung des Nutzerlandes entsprechen.
   </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
    Zu garantieren, dass der Nutzer den FindFace Service auf eigene Gefahr nutzt. 
   </li>   
     </ul>
    </li>
    <li class=\"tracker-agreement-popup-list-item\">
     Bitrix, Inc. ist für den Service nicht verantwortlich, er übernimmt weder die Beantwortung von etwaigen Beschwerden noch den technischen Support oder Beratung bezüglich der Arbeit des Services. 
    </li>
   </ol>
   <div class=\"tracker-agreement-popup-description\">
    Bitrix, Inc. ist für den Service nicht verantwortlich und garantiert dessen Arbeit nicht.
 Mit dem Akzeptieren dieser Bedingungen erklärt sich der Nutzer darüber informiert zu sein, dass der FindFace Service, nicht Bitrix, Inc. die Daten, welche der Nutzer hochlädt, sammelt und bearbeitet.
 Der Nutzer bestätigt und akzeptiert, dass der FindFace Service, nicht Bitrix, Inc. darüber weiß, unter welchen Umständen der Nutzer die Daten, Fotos oder Bilder gesammelt hat. 
 Der Nutzer erklärt sich bereit, den FindFace Service und Bitrix für alle Handlungen und Nichthandlungen des Nutzer schadlos zu halten. 
 <br><br>
 Streitbeilegung; Bindendes Schiedsverfahren: Alle Streitigkeiten, Meinungsverschiedenheiten oder Ansprüche aus oder im Zusammenhang mit diesem Vertrag, beliebige Fahrlässigkeit, Betrug oder Falschdarstellung im Zusammenhang mit dieser Vereinbarung müssen in einem verbindlichen Schiedsgericht geregelt. 
   </div>
  </div>
";
$MESS["FACEID_LICENSE_AGREEMENT_HTML"] = "Der Service FindFace (weiterhin \"FindFace Service\") wird von N-TECH.LAB LTD bereitgestellt. 
 Nutzer des FindFace Service müssen eine Vereinbarung mit dem FindFace Service treffen und <a href=\"https://saas.findface.pro/files/docs/license_us.pdf\" class=\"tracker-link\">folgende Bedingungen</a> akzeptieren, indem sie \"Ich akzeptiere\" auswählen. 
 
 Mit der Auswahl  von \"Ich akzeptiere\" erklärt sich der Nutzer bereit, folgende Anforderungen zu erfüllen: 
     
* Alle lokalen Gesetze und Regelungen einzuhalten, die den Datenschutz im Land des Nutzers bestimmen. 
 
 * Alle lokalen Gesetze und Regelungen einzuhalten. Diese Anforderung gilt innerhalb der ganzen Zeit der Nutzung vom FindFace Service. 
      
 * Eine schriftliche Erlaubnis bzw. Ein anderes schriftliches Dokument zu erhalten, welches lokaler Gesetzgebung des Nutzerlandes entspricht. 
     
 * Der Nutzer muss jede Person informieren, deren persönliche Daten inkl. Fotos oder Bilder der Nutzer durch Nutzung vom FindFace Service bearbeiten wird.
      
 * Sich rechtlich beraten zu lassen, BEVOR der FindFace Service genutzt wird.
  
  * Zu garantieren, dass alle Informationen, die der Nutzer hochladen wird, von Personen, auf die sie sich beziehen, erlaubt sind und lokaler Gesetzgebung des Nutzerlandes entsprechen.
 
 * Zu garantieren, dass der Nutzer den FindFace Service auf eigene Gefahr nutzt. 
   
 Bitrix, Inc. ist für den Service nicht verantwortlich, er übernimmt weder die Beantwortung von etwaigen Beschwerden noch den technischen Support oder Beratung bezüglich der Arbeit des Services. 

 Bitrix, Inc. ist für den Service nicht verantwortlich und garantiert dessen Arbeit nicht.
 Mit dem Akzeptieren dieser Bedingungen erklärt sich der Nutzer darüber informiert zu sein, dass der FindFace Service, nicht Bitrix, Inc. die Daten, welche der Nutzer hochlädt, sammelt und bearbeitet.

Der Nutzer bestätigt und akzeptiert, dass der FindFace Service, nicht Bitrix, Inc. darüber weiß, unter welchen Umständen der Nutzer die Daten, Fotos oder Bilder gesammelt hat. 

Der Nutzer erklärt sich bereit, den FindFace Service und Bitrix für alle Handlungen und Nichthandlungen des Nutzer schadlos zu halten. 

Streitbeilegung; Bindendes Schiedsverfahren: Alle Streitigkeiten, Meinungsverschiedenheiten oder Ansprüche aus oder im Zusammenhang mit diesem Vertrag, beliebige Fahrlässigkeit, Betrug oder Falschdarstellung im Zusammenhang mit dieser Vereinbarung müssen in einem verbindlichen Schiedsgericht geregelt. 
";
$MESS["FACEID_LEAD_SOURCE_DEFAULT"] = "Face Tracker";
?>