<?
$MESS["FACEID_MODULE_NAME"] = "Face recognition";
$MESS["FACEID_MODULE_DESCRIPTION"] = "Módulo Face recognition para el uso con Bitrix24.";
$MESS["FACEID_INSTALL_TITLE"] = "Instalación del Módulo \"Face recognition\"";
$MESS["FACEID_UNINSTALL_TITLE"] = "Desinstalación del Módulo \"Face recognition\"";
$MESS["FACEID_UNINSTALL_QUESTION"] = "¿Está seguro de que desea eliminar el módulo?";
$MESS["FACEID_CHECK_PUBLIC_PATH"] = "Dirección especificada incorrecta.";
?>