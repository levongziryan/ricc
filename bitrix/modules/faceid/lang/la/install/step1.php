<?
$MESS["FACEID_PUBLIC_PATH_DESC"] = "El módulo requiere una dirección de sitio web pública correcta para funcionar adecuadamente.";
$MESS["FACEID_PUBLIC_PATH_DESC_2"] = "Si el acceso externo a su red está restringido, habilite el acceso sólo a ciertas páginas. Por favor consulte #LINK_START#documentation#LINK_END# para más detalles.";
$MESS["FACEID_PUBLIC_PATH"] = "Dirección web:";
?>