<?
$MESS["FACEID_TAB_SETTINGS"] = "Ajustes";
$MESS["FACEID_TAB_TITLE_SETTINGS_2"] = "Parámetros de conexión";
$MESS["FACEID_ACCOUNT_ERROR_PUBLIC"] = "Dirección especificada incorrecta.";
$MESS["FACEID_PUBLIC_URL"] = "Dirección web";
$MESS["FACEID_WAIT_RESPONSE"] = "Permitir un tiempo de espera más largo";
$MESS["FACEID_WAIT_RESPONSE_DESC"] = "Marque esta opción si no obtiene respuesta del servidor debido a la configuración del entorno.";
$MESS["FACEID_ACCOUNT_DEBUG"] = "Modo de depuración";
$MESS["FACEID_TAB_SETTINGS_BUY"] = "Balance";
$MESS["FACEID_TAB_TITLE_SETTINGS_3"] = "Estadísticas de reconocimiento";
$MESS["FACEID_ADM_STATS_BALANCE"] = "Tienes los reconocimientos de <span class = \"adm-table-content-text-bold\"> # COUNT # </ span> que debes utilizar antes de # DATE #";
$MESS["FACEID_ADM_STATS_USAGE"] = "Número de reconocimientos utilizados dentro de los 30 días <span class = \"adm-table-content-text-bold\"> # COUNT # </ span>, incluyendo:";
$MESS["FACEID_ADM_STATS_BALANCE_EMPTY"] = "Las estadísticas no están disponibles. Probablemente todavía no haya empezado a utilizar el reconocimiento de rostros.";
$MESS["FACEID_ADM_STATS_USAGE_1C"] = "Tarjeta facial";
$MESS["FACEID_ADM_STATS_USAGE_FTRACKER"] = "Face tracker";
$MESS["FACEID_ADM_STATS_USAGE_VTRACKER"] = "Grabación en vivo";
$MESS["FACEID_ADM_STATS_BY_1000"] = "Compre 1000 créditos de reconocimiento";
?>