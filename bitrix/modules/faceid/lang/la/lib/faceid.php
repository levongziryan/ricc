<?
$MESS["FACEID_TRACKER"] = "Face tracker";
$MESS["FACEID_CLOUD_ERR_FAIL"] = "Error al obtener datos de la nube.";
$MESS["FACEID_CLOUD_ERR_FAIL_CONNECT"] = "Error al conectar a la nube. ";
$MESS["FACEID_CLOUD_ERR_FAIL_RESPONSE"] = "Error al obtener una respuesta de la nube.";
$MESS["FACEID_CLOUD_ERR_FAIL_TEMPORARY_UNAVAILABLE"] = "El servicio de la nube no está temporalmente disponible, por favor intente otra vez luego.";
$MESS["FACEID_CLOUD_ERR_FAIL_UNKNOWN_COMMAND"] = "Error al enviar solicitud a la nube.";
$MESS["FACEID_CLOUD_ERR_FAIL_UNKNOWN_SERVICE"] = "No se especifica el servicio que ejecuta el comando.";
$MESS["FACEID_CLOUD_ERR_FAIL_GALLERY_CREATE"] = "Error al crear una galeria en la nube. ";
$MESS["FACEID_CLOUD_ERR_FAIL_SAVE_LOCAL_PHOTO"] = "Error al guardar una foto en el portal. Por favor intente otra vez.";
$MESS["FACEID_CLOUD_ERR_FAIL_PORTAL_REGISTER"] = "Error al registrar el portal en la nube. ";
$MESS["FACEID_CLOUD_ERR_FAIL_NO_CREDITS"] = "Créditos insuficientes en el balance del portal.";
$MESS["FACEID_CLOUD_ERR_FAIL_DETECT_FACE"] = "El Face Tracker no pudo reconocer el rostro.";
$MESS["FACEID_CLOUD_ERR_FAIL_PROVIDER_RESPONSE"] = "Face Tracker no está temporalmente disponible.";
$MESS["FACEID_CLOUD_ERR_OK_UNKNOWN_PERSON"] = "El rostro no fue reconocido.";
$MESS["FACEID_CLOUD_ERR_OK_NOT_FOUND"] = "El rostro no fue encontrado en la nube.";
?>