<?
$MESS["FACEID_LICENSE_AGREEMENT_HTML_RICH"] = "<div class=\"tracker-agreement-popup-content\">
   <ol class=\"tracker-agreement-popup-list\">
    <li class=\"tracker-agreement-popup-list-item\">
	El Servicio FindFace (denominado en lo sucesivo \"Servicio FindFace\") está N-TECH.LAB LTD. 
	Los usuarios del Servicio FindFace están obligados a firmar un acuerdo con el Servicio FindFace y deben aceptar los <a href=\"https://saas.findface.pro/files/docs/license_us.pdf\" class=\"tracker-link\">siguientes términos y condiciones</a> Seleccionando \"Estoy de acuerdo\". 
	<br>
	Al seleccionar \"Acepto\", el Usuario acepta y garantiza que cumplirá los siguientes requisitos: 
     <ul class=\"tracker-agreement-popup-inner-list\">
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Seguir estrictamente todas las leyes y reglamentos locales que rigen la privacidad y el uso de datos personales para el país de residencia del Usuario.
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Seguir estrictamente todas las leyes y reglamentos locales. Este requisito permanecerá vigente durante toda la duración de la utilización del Servicio FindFace. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Para obtener autorización por escrito, o según lo prescrito por las leyes locales de cada país en el que el Usuario esté operando.
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       El Usuario está obligado a informar diligentemente a todas las personas cuyos datos personales, incluidas las fotografías e imágenes que el Usuario pretende procesar mediante la utilización del Servicio FindFace.
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Buscar asesoría legal ANTES de utilizar el Servicio FindFace.
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
	   Garantizar que cualquier información cargada por el Usuario ha sido autorizada por el individuo y cumple con las leyes y reglamentos de la jurisdicción del Usuario.
	  </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
	   Para confirmar que el Usuario está utilizando el Servicio FindFace a su propio riesgo y sin garantía de ningún tipo.
	  </li>	  
     </ul>
    </li>
    <li class=\"tracker-agreement-popup-list-item\">
     Bitrix, Inc., no es responsable del Servicio y no hace reclamaciones ni garantías con respecto a la exactitud o disponibilidad del Servicio, ni proporciona ningún soporte técnico o consultas sobre el Servicio.
    </li>
   </ol>
   <div class=\"tracker-agreement-popup-description\">
    Bitrix, Inc, no es responsable del Servicio FindFace y no hace reclamaciones ni garantías para el Servicio FindFace.
	Al aceptar los términos y condiciones de este acuerdo de servicio, el Usuario acepta y confirma que el Servicio FindFace, ni Bitrix, Inc. recopila o procesa los datos cargados por el Usuario.
	El usuario confirma que el Servicio FindFace, ni Bitrix, Inc. tiene conocimiento de las circunstancias bajo las cuales el Usuario ha recopilado los datos, fotografías o imágenes. 
	El usuario se compromete a mantener el servicio FindFace y Bitrix inofensivos por todas y cada una de las acciones e inacciones del usuario.
	<br><br>
	Resolución de conflictos; Arbitraje Vinculante: Cualquier controversia, controversia, interpretación o reclamación incluyendo reclamaciones por, pero no limitado a incumplimiento de contrato, cualquier forma de negligencia, fraude o declaraciones falsas que surjan de, o relacionadas con este acuerdo serán sometidas a arbitraje final y vinculante . 
   </div>
  </div>";
$MESS["FACEID_LICENSE_AGREEMENT_HTML"] = "El Servicio FindFace (denominado en lo sucesivo \"Servicio FindFace\") es proporcionado por N-TECH.LAB LTD.
Los usuarios del Servicio FindFace están obligados a firmar un acuerdo con el Servicio FindFace y deben <a href='https://saas.findface.pro/files/docs/license_us.pdf'>siguientes términos y condiciones</a> Seleccionando \"Estoy de acuerdo\". 

Al seleccionar \"Acepto\", el Usuario acepta y garantiza que cumplirá los siguientes requisitos:

* Seguir estrictamente todas las leyes y reglamentos locales que rigen la privacidad y el uso de datos personales para el país de residencia del Usuario.

* Seguir estrictamente todas las leyes y reglamentos locales. Este requisito permanecerá vigente durante toda la duración de la utilización del Servicio FindFace.

* Para obtener autorización por escrito, o según lo prescrito por las leyes locales de cada país en el que el Usuario esté operando. 

* El Usuario está obligado a informar diligentemente a todas las personas cuyos datos personales, incluidas las fotografías e imágenes que el Usuario pretende procesar mediante la utilización del Servicio FindFace.

* Buscar asesoría legal ANTES de utilizar el Servicio FindFace. 

* Garantizar que cualquier información cargada por el Usuario ha sido autorizada por el individuo y cumple con las leyes y reglamentos de la jurisdicción del Usuario.

* Para confirmar que el Usuario está utilizando el Servicio de FindFace a su propio riesgo y sin garantía de ningún tipo.

Bitrix, Inc., no es responsable del Servicio y no hace reclamaciones ni garantías con respecto a la exactitud o disponibilidad del Servicio, ni proporciona ningún soporte técnico o consultas sobre el Servicio.

Bitrix, Inc, no es responsable del Servicio FindFace y no hace reclamaciones ni garantías para el Servicio FindFace.
Al aceptar los términos y condiciones de este acuerdo de servicio, el Usuario acepta y confirma que el Servicio FindFace, ni Bitrix, Inc. recopila o procesa los datos cargados por el Usuario.
El usuario confirma que el Servicio FindFace, ni Bitrix, Inc. tiene conocimiento de las circunstancias bajo las cuales el Usuario ha recopilado los datos, fotografías o imágenes.
El usuario se compromete a mantener el servicio FindFace y Bitrix inofensivos por todas y cada una de las acciones e inacciones del usuario.

Resolución de conflictos; Arbitraje Vinculante: Cualquier controversia, controversia, interpretación o reclamación incluyendo reclamaciones por, pero no limitado a incumplimiento de contrato, cualquier forma de negligencia, fraude o declaraciones falsas que surjan de, o relacionadas con este acuerdo serán sometidas a arbitraje final y vinculante .";
$MESS["FACEID_LEAD_SOURCE_DEFAULT"] = "Face Tracker";
?>