<?
$MESS["FACEID_TRACKER"] = "Face tracker";
$MESS["FACEID_CLOUD_ERR_FAIL"] = "Erro ao obter dados da nuvem.";
$MESS["FACEID_CLOUD_ERR_FAIL_CONNECT"] = "Erro ao conectar a nuvem.";
$MESS["FACEID_CLOUD_ERR_FAIL_RESPONSE"] = "Erro em obter resposta da nuvem.";
$MESS["FACEID_CLOUD_ERR_FAIL_TEMPORARY_UNAVAILABLE"] = "O serviço em nuvem está temporariamente indisponível, tente novamente mais tarde.";
$MESS["FACEID_CLOUD_ERR_FAIL_UNKNOWN_COMMAND"] = "Erro ao enviar solicitação para a nuvem.";
$MESS["FACEID_CLOUD_ERR_FAIL_UNKNOWN_SERVICE"] = "O serviço que está executando o comando não foi especificado.";
$MESS["FACEID_CLOUD_ERR_FAIL_GALLERY_CREATE"] = "Erro ao criar galeria do portal na nuvem.";
$MESS["FACEID_CLOUD_ERR_FAIL_SAVE_LOCAL_PHOTO"] = "Erro ao salvar a foto no portal. Tente novamente.";
$MESS["FACEID_CLOUD_ERR_FAIL_PORTAL_REGISTER"] = "Erro ao registrar o portal na nuvem.";
$MESS["FACEID_CLOUD_ERR_FAIL_NO_CREDITS"] = "Créditos insuficientes no saldo do portal.";
$MESS["FACEID_CLOUD_ERR_FAIL_DETECT_FACE"] = "O Face Tracker não conseguiu reconhecer o rosto.";
$MESS["FACEID_CLOUD_ERR_FAIL_PROVIDER_RESPONSE"] = "O Face Tracker está temporariamente indisponível.";
$MESS["FACEID_CLOUD_ERR_OK_UNKNOWN_PERSON"] = "O rosto não foi reconhecido.";
$MESS["FACEID_CLOUD_ERR_OK_NOT_FOUND"] = "O rosto não foi encontrado na nuvem.";
?>