<?
$MESS["FACEID_LICENSE_AGREEMENT_HTML_RICH"] = "<div class=\"tracker-agreement-popup-content\">
<ol class=\"tracker-agreement-popup-list\">
<li class=\"tracker-agreement-popup-list-item\">
O FindFace Service (adiante designado como \"FindFace Service\") é fornecido pela N-TECH. LAB LTD.
Os usuários do FindFace Service são obrigados a fazer um contrato com o FindFace Service e devem concordar com os <a href=\"https://saas.findface.pro/files/docs/license_us.pdf\" class=\"tracker-link\"> seguintes termos e condições</a> selecionando \"Eu concordo\".
<br>
Ao selecionar \"Eu concordo\", o Usuário está concordando e garantindo que adere aos seguintes requisitos:
<ul class=\"tracker-agreement-popup-inner-list\">
<li class=\"tracker-agreement-popup-inner-list-item\">
Seguir rigorosamente todas as leis locais e regulamentações que regem a privacidade e uso de dados pessoais para o país de residência do Usuário.
 </li>
<li class=\"tracker-agreement-popup-inner-list-item\"> 
Seguir rigorosamente todas as leis e regulamentos locais. Este requisito permanecerá em vigor por toda a duração da utilização do FindFace Service.
</li>
<li class=\"tracker-agreement-popup-inner-list-item\">
Obter autorização por escrito ou conforme prescrito pelas leis locais de cada país de onde o Usuário está operando.
</li>
<li class=\"tracker-agreement-popup-inner-list-item\">
O Usuário é obrigado a informar cuidadosamente cada indivíduo cujos dados pessoais, incluindo fotografias e imagens do indivíduo, o Usuário pretende processar por meio da utilização do FindFace Service.
</li>
<li class=\"tracker-agreement-popup-inner-list-item\">
Procurar aconselhamento jurídico antes de utilizar o FindFace Service.
</li>
<li class=\"tracker-agreement-popup-inner-list-item\">
Garantir que quaisquer informações carregadas pelo Usuário foram autorizadas pelo indivíduo e estão em conformidade com as leis e regulamentos da jurisdição do usuário.
</li>
<li class=\"tracker-agreement-popup-inner-list-item\">
Confirmar que o Usuário está utilizando o FindFace Service por seu próprio risco e sem garantia de qualquer tipo.
</li>
</ul>
</li>
<li class=\"tracker-agreement-popup-list-item\">
A Bitrix, Inc, não é responsável de nenhuma forma pelo Serviço e não oferece direito a reclamações ou garantias com relação à exatidão ou disponibilidade do Serviço, nem fornece qualquer suporte técnico ou consultas sobre o Serviço.
</li>
</ol>
<div class=\"tracker-agreement-popup-description\">
A Bitrix, Inc, não é responsável de nenhuma forma pelo FindFace Service e não oferece direitos a reclamações ou garantias pelo FindFace Service.
Ao aceitar os termos e condições deste contrato de serviço, o Usuário concorda e confirma que nem o FindFace Service, nem a Bitrix, Inc. recolhe ou processa quaisquer dados carregados pelo Usuário.
O Usuário confirma que nem o FindFace Service, nem a Bitrix, Inc. tem qualquer conhecimento das circunstâncias sob as quais o Usuário coletou os dados, fotografias ou imagens.
O Usuário concorda em isentar o FindFace Service e o Bitrix de todas e quaisquer ações e omissões do Usuário.
<br><br>
Resolução de Litígios; Arbitragem Vinculativa: Qualquer disputa, controvérsia, interpretação ou reivindicação, incluindo reclamações, mas não limitado à violação do contrato, qualquer forma de negligência, fraude ou deturpação decorrentes de ou do ou relacionados ao presente contrato deve ser apresentada para arbitragem final e vinculativa.
</div>
</div>";
$MESS["FACEID_LICENSE_AGREEMENT_HTML"] = "O FindFace Service (adiante designado \"FindFace Service\") é fornecido pela N-TECH. LAB LTD.
Os Usuários do FindFace Service são obrigados a fazer um contrato com o FindFace Service e devem concordar com os <a href='https://saas.findface.pro/files/docs/license_us.pdf'> seguintes termos e condições</a> selecionando \"Eu Concordo\".

Ao selecionar \"Eu concordo\" o usuário está concordando e garantindo que adere aos seguintes requisitos:

* Seguir rigorosamente todas as leis locais e regulamentos que regem a privacidade e uso de dados pessoais para o país de residência do Usuário.

* Seguir rigorosamente todas as leis e regulamentações locais. Este requisito permanecerá em vigor por toda a duração da utilização do FindFace Service.

* Obter autorização por escrito ou conforme prescrito pelas leis locais de cada país de onde o Usuário está operando.

* O Usuário é obrigado a informar cuidadosamente cada indivíduo cujos dados pessoais, incluindo fotografias e imagens do indivíduo, o Usuário pretende processar por meio da utilização do FindFace Service.
* Procurar aconselhamento jurídico antes de utilizar o FindFace Service. 

* Garantir que quaisquer informações carregadas pelo Usuário foram autorizadas pelo indivíduo e estão em conformidade com as leis e regulamentos da jurisdição do Usuário.

* Confirmar que o Usuário está utilizando o FindFace Service por seu próprio risco e sem garantia de qualquer tipo.

A Bitrix, Inc, não é responsável de nenhuma forma pelo Serviço e não oferece direitos de reclamação ou garantias com relação a exatidão ou disponibilidade do Serviço, nem fornece qualquer suporte técnico ou consultas sobre o Serviço.

A Bitrix, Inc, é não responsável de nenhuma forma pelo FindFace Service e não oferece direito a reclamações ou garantias para o FindFace Service.
Ao aceitar os termos e condições deste contrato de serviço, o Usuário concorda e confirma que nem o FindFace Service, nem a Bitrix, Inc. recolhem ou processam quaisquer dados carregados pelo Usuário.
O usuário confirma que nem o FindFace Service, nem a Bitrix, Inc. tem qualquer conhecimento das circunstâncias sob as quais o Usuário coletou os dados, fotografias ou imagens. 
O usuário concorda em isentar o FindFace Service e o Bitrix de todas e quaisquer ações e omissões do Usuário.

Resolução de Litígios; Arbitragem Vinculativa: Qualquer disputa, controvérsia, interpretação ou reivindicação, incluindo reclamações, mas não limitado à violação do contrato, qualquer forma de negligência, fraude ou deturpação decorrentes de ou do ou relacionados ao presente contrato deve ser apresentada para arbitragem final e vinculativa.";
$MESS["FACEID_LEAD_SOURCE_DEFAULT"] = "Face Tracker";
?>