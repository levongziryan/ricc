<?
$MESS["FACEID_MODULE_NAME"] = "Reconhecimento facial";
$MESS["FACEID_MODULE_DESCRIPTION"] = "Módulo de reconhecimento facial para uso com o Bitrix24.";
$MESS["FACEID_INSTALL_TITLE"] = "Instalação do Módulo de \"Reconhecimento Facial\"";
$MESS["FACEID_UNINSTALL_TITLE"] = "Desinstalação do Módulo de \"Reconhecimento Facial\"";
$MESS["FACEID_UNINSTALL_QUESTION"] = "Tem certeza de que deseja excluir o módulo?";
$MESS["FACEID_CHECK_PUBLIC_PATH"] = "Endereço incorreto especificado.";
?>