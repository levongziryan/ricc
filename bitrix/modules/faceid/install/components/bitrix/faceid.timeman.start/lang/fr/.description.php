<?
$MESS["FACEID_TMS_START_SERVICES_MAIN_SECTION"] = "Portail Bitrix24";
$MESS["FACEID_TMS_START_SERVICES_PARENT_SECTION"] = "Suivi des heures de travail";
$MESS["FACEID_TMS_START_SECTION_TEMPLATE_NAME"] = "Gestion du temps de travail avec Bitrix24.Time";
$MESS["FACEID_TMS_START_SECTION_TEMPLATE_DESCRIPTION"] = "Active Bitrix24.Time et indexe les photos de profil des employés.";
?>