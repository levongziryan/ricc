<?
$MESS["FACEID_TRACKER1C_CMP_JS_CAMERA_DEFAULT"] = "Cámara";
$MESS["FACEID_TRACKER1C_CMP_JS_CAMERA_NOT_FOUND"] = "La cámara no fue encontrada";
$MESS["FACEID_TRACKER1C_CMP_JS_CAMERA_NO_SUPPORT"] = "Su navegador no admite cámara";
$MESS["FACEID_TRACKER1C_CMP_JS_CAMERA_ERROR"] = "Su navegador no admite grabación de video o la cámara no está conectada.";
$MESS["FACEID_TRACKER1C_CMP_JS_AJAX_ERROR"] = "Error de solicitud AJAX";
$MESS["FACEID_TRACKER1C_CMP_CAMERA"] = "Cámara";
$MESS["FACEID_1C_PUBLIC_TITLE"] = "1C Rostro-Tarjeta";
?>