<?
$MESS["FACEID_TRACKER_CMP_TITLE"] = "Regardez les personnes qui visitent vos emplacements physiques.";
$MESS["FACEID_TRACKER_CMP_DESCR"] = "Le tracker facial va enregistrer et compter les visiteurs dans votre magasin ou bureau: total de visiteurs par jour; nouveaux visiteurs; visiteurs existants; visiteurs déjà présents dans votre CRM.";
$MESS["FACEID_TRACKER_CMP_DESCR_P1"] = "Recherche instantanée par photo parmi les personnes enregistrées dans votre CRM.";
$MESS["FACEID_TRACKER_CMP_DESCR_P1_NEW"] = "Le tracker facial utilise la technologie de reconnaissance faciale pour identifier vos clients.
Il peut compter l'ensemble des visiteurs de votre magasin ou bureau ainsi que le nombre de nouveaux visiteurs, de visiteurs existants et de visiteurs se trouvant déjà dans votre CRM.";
$MESS["FACEID_TRACKER_CMP_DESCR_P2"] = "Bitrix24 trouvera un profil social par photo et ajoutera le profil à votre CRM.";
$MESS["FACEID_TRACKER_CMP_DESCR_P2_NEW"] = "Le tracker facial peut identifier un profil social VK par photo prise et ajoutera le profil à votre CRM.";
$MESS["FACEID_TRACKER_CMP_AUTO"] = "Auto";
$MESS["FACEID_TRACKER_CMP_AUTO_PHOTO"] = "prendre une photo";
$MESS["FACEID_TRACKER_CMP_AUTO_LEAD"] = "créer un client potentiel";
$MESS["FACEID_TRACKER_CMP_CAMERA"] = "Caméra vidéo";
$MESS["FACEID_TRACKER_CMP_STATS"] = "Statistiques visiteurs";
$MESS["FACEID_TRACKER_CMP_STATS_NEW"] = "Nouveau";
$MESS["FACEID_TRACKER_CMP_STATS_OLD"] = "Existant";
$MESS["FACEID_TRACKER_CMP_STATS_ALL"] = "Total";
$MESS["FACEID_TRACKER_CMP_STATS_CRM"] = "Statistiques CRM";
$MESS["FACEID_TRACKER_CMP_STATS_CRM_SAVE"] = "Enregistré dans le CRM";
$MESS["FACEID_TRACKER_CMP_STATS_DETAILED"] = "Détails des statistiques";
$MESS["FACEID_TRACKER_CMP_HEAD_VISITORS"] = "Visiteurs";
$MESS["FACEID_TRACKER_CMP_HEAD_CREDITS"] = "Crédit de reconnaissance:";
$MESS["FACEID_TRACKER_CMP_HEAD_CREDITS_ADD"] = "Ajouter du crédit";
$MESS["FACEID_TRACKER_CMP_FIRST_TIME_VISIT"] = "Prenez une photo de votre client pour l'ajouter au système. Bitrix24 reconnaîtra le client lors de la prochaine rencontre et vous pourrez savoir qu'il s'agit d'un client existant.<br><br>";
$MESS["FACEID_TRACKER_CMP_FIRST_TIME_VISIT_NEW"] = "<div class=\"faceid-tracker-first-visit-desc\">
<div class=\"faceid-tracker-first-visit-desc-inner\">
<div class=\"faceid-tracker-first-visit-desc-title\">Comment commencer?</div>
<div class=\"faceid-tracker-first-visit-desc-block\">
<span class=\"faceid-tracker-first-visit-desc-item\">Prenez une première photo de votre client et le tracker facial pourra \"s'en rappeler\".
Lors de la prochaine visite de cette personne, le système   l'identifiera comme visiteur existant. </span>
</div>
</div>
<div class=\"faceid-tracker-first-visit-desc-inner\">
<div class=\"faceid-tracker-first-visit-desc-title\">Comment utiliser le tracker facial comme compteur de fréquentation</div>
<div class=\"faceid-tracker-first-visit-desc-block\">
<span class=\"faceid-tracker-first-visit-desc-item\">Installez la caméra près de l'entrée de votre bureau ou magasin</span>
<span class=\"faceid-tracker-first-visit-desc-item\">Connectez la caméra à n'importe quel ordinateur</span>
<span class=\"faceid-tracker-first-visit-desc-item\">Lancez le tracker facial sur cet ordinateur</span>
<span class=\"faceid-tracker-first-visit-desc-item\">Activez la prise de photo automatique</span>
</div>
</div>
</div>";
$MESS["FACEID_TRACKER_CMP_LIST_MORE"] = "Afficher plus";
$MESS["FACEID_TRACKER_CMP_VK_TITLE"] = "Rechercher un profil VK";
$MESS["FACEID_TRACKER_CMP_VK_PROGRESS"] = "Recherche...";
$MESS["FACEID_TRACKER_CMP_VK_FOUND_COUNT"] = "Personnes trouvées:";
$MESS["FACEID_TRACKER_CMP_AGR_TITLE"] = "Conditions d'utilisation";
$MESS["FACEID_TRACKER_CMP_AGR_TEXT"] = "<div class=\"tracker-agreement-popup-content\">
<ol class=\"tracker-agreement-popup-list\">
<li class=\"tracker-agreement-popup-list-item\">
Le Service FindFace (le Service) es fourni par N-TECH.LAB LTD conformément au suivant <a href=\"https://saas.findface.pro/files/docs/license_us.pdf\" class=\"tracker-link\">Accord</a>.
En appuyant sur \"J'accepte les Conditions\", vous reconnaissez et garantissez votre adhésion aux obligations suivantes:
<ul class=\"tracker-agreement-popup-inner-list\">
<li class=\"tracker-agreement-popup-inner-list-item\">
Respecter strictement l'ensemble des lois et règlementations locales relatives à la vie privée et à l'utilisation des données personnelles dans votre pays pour tout la durée d'utilisation du Service.
</li>
<li class=\"tracker-agreement-popup-inner-list-item\">
Obtenir l'autorisation nécessaire (écrite ou tel qu'autrement prévu par les lois locales de chaque pays dans lequel vous opérez) de chaque personne dont vous traiterez les données personnelles en utilisant le Service, y compris les photographies et images.
</li>
<li class=\"tracker-agreement-popup-inner-list-item\">
Solliciter un avis juridique AVANT d'avoir recours au Service, si vous n'êtes pas certain des permissions nécessaires à obtenir et sous quelle forme.
</li>
<li class=\"tracker-agreement-popup-inner-list-item\">
Garantir que toutes les informations obtenues que vous téléchargez sur le Service sont conformes aux lois et règlementations locales.
</li>
<li class=\"tracker-agreement-popup-inner-list-item\">
Confirmer que vous utilisez le Service à vos propres risques, sans garantie de quelque nature que ce soit.
</li>
</ul>
</li>
<li class=\"tracker-agreement-popup-list-item\">
Bitrix, Inc, n'est en aucun cas responsable du Service et ne formule aucune prétention ou garantie quant à l'exactitude ou la disponibilité du Service, et ne fournit aucune assistance technique ou consultation pour le Service.
</li>
</ol>
<div class=\"tracker-agreement-popup-description\">
En acceptant l'accord de service, vous convenez et confirmez que Bitrix, Inc. ne recueille ou ne traite aucune donnée que vous téléchargez sur le Service et que Bitrix, Inc. n'a aucune connaissance de la façon dont vous recueillez les données.
</div>
</div>";
$MESS["FACEID_TRACKER_CMP_AGR_BUTTON"] = "J'accepte les Conditions";
$MESS["FACEID_TRACKER_CMP_JS_CAMERA_DEFAULT"] = "Caméra";
$MESS["FACEID_TRACKER_CMP_JS_CAMERA_NOT_FOUND"] = "Caméra introuvable";
$MESS["FACEID_TRACKER_CMP_JS_CAMERA_NO_SUPPORT"] = "Votre navigateur ne supporte pas la caméra";
$MESS["FACEID_TRACKER_CMP_JS_CAMERA_ERROR"] = "Votre navigateur ne supporte pas l’enregistrement vidéo ou la caméra n’est pas connectée.";
$MESS["FACEID_TRACKER_CMP_JS_FACE_NOT_FOUND"] = "Malheureusement, nous n'avons pas réussi à reconnaître la personne. Veuillez essayer de prendre une autre photo.";
$MESS["FACEID_TRACKER_CMP_JS_FACE_ORIGINAL"] = "Exemple de photo";
$MESS["FACEID_TRACKER_CMP_JS_SAVE_CRM"] = "Enregistrer dans le CRM";
$MESS["FACEID_TRACKER_CMP_JS_SAVE_CRM_DONE"] = "Client potentiel enregistré";
$MESS["FACEID_TRACKER_CMP_JS_VK_LINK"] = "VK";
$MESS["FACEID_TRACKER_CMP_JS_VK_LINK_ACTION"] = "Trouver un profil VK";
$MESS["FACEID_TRACKER_CMP_JS_VK_FOUND_PEOPLE"] = "personnes";
$MESS["FACEID_TRACKER_CMP_JS_VK_SELECT"] = "Sélectionner";
$MESS["FACEID_TRACKER_CMP_AUTH_ONLY"] = "Ce composant nécessite l'authentification.";
$MESS["FACEID_TRACKER_CMP_AUTO_PHOTO_NEW"] = "prendre des photos automatiquement";
$MESS["FACEID_TRACKER_CMP_SETTINGS"] = "Paramètres avancés";
?>