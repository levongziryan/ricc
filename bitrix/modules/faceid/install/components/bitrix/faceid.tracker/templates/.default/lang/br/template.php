<?
$MESS["FACEID_TRACKER_CMP_TITLE"] = "Observe pessoas visitando seus locais tradicionais.";
$MESS["FACEID_TRACKER_CMP_DESCR"] = "O rastreador Face irá registrar e contar visitantes na sua loja ou escritório: total de visitantes no dia de hoje; novos visitantes; visitantes que retornaram; visitantes que já existem no seu CRM.";
$MESS["FACEID_TRACKER_CMP_DESCR_P1"] = "Pesquisa imediata por foto entre pessoas registradas no seu CRM.";
$MESS["FACEID_TRACKER_CMP_DESCR_P1_NEW"] = "O Face-Tracker utiliza tecnologia de reconhecimento facial para identificar seus clientes. Ele pode contar todos os visitantes da sua loja ou do seu escritório, contar quantos visitantes são novos, quantos estão retornando e quantos já estão no seu CRM.";
$MESS["FACEID_TRACKER_CMP_DESCR_P2"] = "O Bitrix24 irá encontrar um perfil social por foto e adicionar o perfil ao seu CRM.";
$MESS["FACEID_TRACKER_CMP_DESCR_P2_NEW"] = "O Face-tracker pode identificar o perfil social VK pela foto tirada e adicionar o perfil ao seu CRM.";
$MESS["FACEID_TRACKER_CMP_AUTO"] = "Automático";
$MESS["FACEID_TRACKER_CMP_AUTO_PHOTO"] = "tirar uma foto";
$MESS["FACEID_TRACKER_CMP_AUTO_LEAD"] = "criar um cliente potencial";
$MESS["FACEID_TRACKER_CMP_CAMERA"] = "Câmera de vídeo";
$MESS["FACEID_TRACKER_CMP_STATS"] = "Estatísticas de visitantes";
$MESS["FACEID_TRACKER_CMP_STATS_NEW"] = "Novo";
$MESS["FACEID_TRACKER_CMP_STATS_OLD"] = "Retornando";
$MESS["FACEID_TRACKER_CMP_STATS_ALL"] = "Total";
$MESS["FACEID_TRACKER_CMP_STATS_CRM"] = "Estatísticas de CRM";
$MESS["FACEID_TRACKER_CMP_STATS_CRM_SAVE"] = "Salvo no CRM";
$MESS["FACEID_TRACKER_CMP_STATS_DETAILED"] = "Informações de estatísticas";
$MESS["FACEID_TRACKER_CMP_HEAD_VISITORS"] = "Visitantes";
$MESS["FACEID_TRACKER_CMP_HEAD_CREDITS"] = "Crédito de reconhecimento:";
$MESS["FACEID_TRACKER_CMP_HEAD_CREDITS_ADD"] = "Adicionar crédito";
$MESS["FACEID_TRACKER_CMP_FIRST_TIME_VISIT"] = "Tire uma foto do seu cliente para adicioná-la ao sistema. O Bitrix24 reconhecerá o cliente no próximo encontro, assim você vai saber se eles são clientes que estão retornando.<br><br>Instale uma câmera na sua loja ou escritório para registrar seus visitantes e manter estatísticas.";
$MESS["FACEID_TRACKER_CMP_FIRST_TIME_VISIT_NEW"] = "<div class=\"faceid-tracker-first-visit-desc\">
	<div class=\"faceid-tracker-first-visit-desc-inner\">
		<div class=\"faceid-tracker-first-visit-desc-title\">Como posso começar?</div>
		<div class=\"faceid-tracker-first-visit-desc-block\">
			<span class=\"faceid-tracker-first-visit-desc-item\">Tire uma primeira foto do seu cliente e o face-tracker vai \"lembrar\" dela. Da próxima vez que esta pessoa retorna, o sistema irá identificá-la como um visitante repetido. </span>
		</div>
	</div>
	<div class=\"faceid-tracker-first-visit-desc-inner\">
		<div class=\"faceid-tracker-first-visit-desc-title\">Como usar o face tracker como contador de tráfego de varejo</div>
		<div class=\"faceid-tracker-first-visit-desc-block\">
			<span class=\"faceid-tracker-first-visit-desc-item\">Instale a câmera próximo da entrada de seu escritório ou loja</span>
			<span class=\"faceid-tracker-first-visit-desc-item\">Conecte e câmera a qualquer computador</span>
			<span class=\"faceid-tracker-first-visit-desc-item\">Inicie o face-tracker neste computador</span>
			<span class=\"faceid-tracker-first-visit-desc-item\">Habilite tirar foto automaticamente</span>
		</div>
	</div>
</div>
";
$MESS["FACEID_TRACKER_CMP_LIST_MORE"] = "Mostrar mais";
$MESS["FACEID_TRACKER_CMP_VK_TITLE"] = "Pesquisar perfil VK";
$MESS["FACEID_TRACKER_CMP_VK_PROGRESS"] = "Pesquisando...";
$MESS["FACEID_TRACKER_CMP_VK_FOUND_COUNT"] = "Pessoas encontradas:";
$MESS["FACEID_TRACKER_CMP_AGR_TITLE"] = "Termos de Uso";
$MESS["FACEID_TRACKER_CMP_AGR_TEXT"] = "<div class=\"tracker-agreement-popup-content\">
   <ol class=\"tracker-agreement-popup-list\">
    <li class=\"tracker-agreement-popup-list-item\">
     O FindFace Service (o Serviço) é fornecido pela N-TECH.LAB LTD em conformidade com o seguinte <a href=\"https://saas.findface.pro/files/docs/license_us.pdf\" class=\"tracker-link\">Acordo</a>. Apertando \"Aceito os termos\", você estará concordando e garantindo que irá atender os seguintes requisitos: 
     <ul class=\"tracker-agreement-popup-inner-list\">
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Seguir rigorosamente todas as leis e normas locais que regem o uso da privacidade e dos dados pessoais no seu país por toda a duração da utilização do serviço.
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Obter a permissão necessária (por escrito ou de outra forma, tal como exigido pelas leis locais de cada país em que estão operando) de cada pessoa cujos dados pessoais você vai estará processando utilizando o Serviço, incluindo suas fotos e imagens. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Procurar aconselhamento jurídico ANTES de começar a usar o Serviço, se você não tiver certeza, quais permissões você precisa obter e de que forma. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Garantir que quaisquer informações que você carregar no Serviço foram obtidas por você em plena conformidade com as leis e normas locais. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Confirmar que você está usando o Serviço por sua própria conta e risco sem qualquer tipo de garantia. 
      </li>
     </ul>
    </li>
    <li class=\"tracker-agreement-popup-list-item\">
     A Bitrix, Inc, não é de forma alguma responsável pelo Serviço e não apresenta reclamações ou garantias no que diz respeito à exatidão ou à disponibilidade do Serviço, nem fornece qualquer suporte técnico ou consultas sobre o Serviço. 
    </li>
   </ol>
   <div class=\"tracker-agreement-popup-description\">
    Ao aceitar o acordo de Serviço, você concorda e confirma que a Bitrix, Inc. não coleta ou processa quaisquer dados carregados por você no Serviço e que a Bitrix, Inc. não sabe e não deve saber as circunstâncias nas quais estes dados foram coletados. 
   </div>
  </div>";
$MESS["FACEID_TRACKER_CMP_AGR_BUTTON"] = "Eu aceito os Termos";
$MESS["FACEID_TRACKER_CMP_JS_CAMERA_DEFAULT"] = "Câmera";
$MESS["FACEID_TRACKER_CMP_JS_CAMERA_NOT_FOUND"] = "A câmera não foi encontrada";
$MESS["FACEID_TRACKER_CMP_JS_CAMERA_NO_SUPPORT"] = "Seu navegador não é compatível com a câmera";
$MESS["FACEID_TRACKER_CMP_JS_CAMERA_ERROR"] = "Seu navegador não é compatível com gravação de vídeo ou a câmera não está conectada.";
$MESS["FACEID_TRACKER_CMP_JS_FACE_NOT_FOUND"] = "Infelizmente não foi possível reconhecer a pessoa. Tente tirar outra foto.";
$MESS["FACEID_TRACKER_CMP_JS_FACE_ORIGINAL"] = "Foto de amostra";
$MESS["FACEID_TRACKER_CMP_JS_SAVE_CRM"] = "Salvar no CRM";
$MESS["FACEID_TRACKER_CMP_JS_SAVE_CRM_DONE"] = "O cliente potencial foi salvo";
$MESS["FACEID_TRACKER_CMP_JS_VK_LINK"] = "VK";
$MESS["FACEID_TRACKER_CMP_JS_VK_LINK_ACTION"] = "Encontrar perfil VK";
$MESS["FACEID_TRACKER_CMP_JS_VK_FOUND_PEOPLE"] = "pessoas";
$MESS["FACEID_TRACKER_CMP_JS_VK_SELECT"] = "Selecionar";
$MESS["FACEID_TRACKER_CMP_AUTH_ONLY"] = "Este componente requer autenticação.";
$MESS["FACEID_TRACKER_CMP_AUTO_PHOTO_NEW"] = "tirar fotos automaticamente";
$MESS["FACEID_TRACKER_CMP_SETTINGS"] = "Parâmetros ampliados";
?>