<?
$MESS["FACEID_TRACKERWD_CMP_TITLE"] = "Regardez les personnes qui visitent vos emplacements physiques.";
$MESS["FACEID_TRACKERWD_CMP_DESCR"] = "Le Traqueur facial va consigner et compter les visiteurs de votre boutique ou de vos bureaux : total des visiteurs de la journée ; nouveaux visiteurs ; visiteurs revenus ; visiteurs qui existent déjà dans votre GRC.";
$MESS["FACEID_TRACKERWD_CMP_DESCR_P1"] = "Une recherche instantanée par photo parmi les personnes enregistrées dans votre GRC.";
$MESS["FACEID_TRACKERWD_CMP_DESCR_P1_NEW"] = "Le Traqueur facial utilise la technologie de reconnaissance faciale pour identifier vos clients. Il compte tous les visiteurs de votre boutique ou de votre bureau, détermine combien sont nouveaux, combien reviennent et combien sont déjà dans votre GRC.";
$MESS["FACEID_TRACKERWD_CMP_DESCR_P2"] = "Birtrix24 trouvera un profil social à partir d'une photo et l'ajoutera à votre GRC.";
$MESS["FACEID_TRACKERWD_CMP_DESCR_P2_NEW"] = "Le Traqueur facial peut identifier un profil social VK à partir d'une photo prise et l'ajouter à votre GRC.";
$MESS["FACEID_TRACKERWD_CMP_AUTO"] = "Auto";
$MESS["FACEID_TRACKERWD_CMP_AUTO_PHOTO"] = "prendre une photo";
$MESS["FACEID_TRACKERWD_CMP_AUTO_LEAD"] = "créer une piste";
$MESS["FACEID_TRACKERWD_CMP_CAMERA"] = "Caméra vidéo";
$MESS["FACEID_TRACKERWD_CMP_STATS"] = "Statistiques des visiteurs";
$MESS["FACEID_TRACKERWD_CMP_STATS_NEW"] = "Nouveau";
$MESS["FACEID_TRACKERWD_CMP_STATS_OLD"] = "De retour";
$MESS["FACEID_TRACKERWD_CMP_STATS_ALL"] = "Total";
$MESS["FACEID_TRACKERWD_CMP_STATS_CRM"] = "Statistiques GRC";
$MESS["FACEID_TRACKERWD_CMP_STATS_CRM_SAVE"] = "Enregistré dans la GRC";
$MESS["FACEID_TRACKERWD_CMP_STATS_DETAILED"] = "Détails des statistiques";
$MESS["FACEID_TRACKERWD_CMP_HEAD_VISITORS"] = "Visiteurs";
$MESS["FACEID_TRACKERWD_CMP_HEAD_CREDITS"] = "Crédit de reconnaissance :";
$MESS["FACEID_TRACKERWD_CMP_HEAD_CREDITS_ADD"] = "Ajouter du crédit";
$MESS["FACEID_TRACKERWD_CMP_FIRST_TIME_VISIT"] = "Prenez une photo de votre client pour l'ajouter au système. Bitrix24 le reconnaîtra alors à la prochaine rencontre, ce qui vous permettra de savoir que c'est un client de retour.<br><br>Installez une caméra dans votre boutique ou bureau pour consigner vos visiteurs et conservez les statistiques.";
$MESS["FACEID_TRACKERWD_CMP_FIRST_TIME_VISIT_NEW"] = "<div class=\"faceid-tracker-first-visit-desc\">
	<div class=\"faceid-tracker-first-visit-desc-inner\">
		<div class=\"faceid-tracker-first-visit-desc-title\">Comment démarrer ?</div>
		<div class=\"faceid-tracker-first-visit-desc-block\">
			<span class=\"faceid-tracker-first-visit-desc-item\">Prenez une première photo de votre client et le Traqueur facial s'en 'souviendra'. La prochaine fois que cette personne reviendra, le système l'identifiera comme étant un visiteur revenu. </span>
		</div>
	</div>
	<div class=\"faceid-tracker-first-visit-desc-inner\">
		<div class=\"faceid-tracker-first-visit-desc-title\">Comment utiliser le traqueur facial comme compteur de visites de commerce</div>
		<div class=\"faceid-tracker-first-visit-desc-block\">
			<span class=\"faceid-tracker-first-visit-desc-item\">Installez la caméra à proximité de l'entrée de votre bureau ou boutique</span>
			<span class=\"faceid-tracker-first-visit-desc-item\">Connectez la caméra à n'importe quel ordinateur</span>
			<span class=\"faceid-tracker-first-visit-desc-item\">Lancez le traqueur facial sur cet ordinateur</span>
			<span class=\"faceid-tracker-first-visit-desc-item\">Activez la prise automatique de photo</span>
		</div>
	</div>
</div>
";
$MESS["FACEID_TRACKERWD_CMP_LIST_MORE"] = "Afficher plus";
$MESS["FACEID_TRACKERWD_CMP_VK_TITLE"] = "Recherche de profil VK";
$MESS["FACEID_TRACKERWD_CMP_VK_PROGRESS"] = "Recherche...";
$MESS["FACEID_TRACKERWD_CMP_VK_FOUND_COUNT"] = "Personnes trouvées :";
$MESS["FACEID_TRACKERWD_CMP_AGR_TITLE"] = "Conditions d'utilisation";
$MESS["FACEID_TRACKERWD_CMP_AGR_TEXT"] = "<div class=\"tracker-agreement-popup-content\">
   <ol class=\"tracker-agreement-popup-list\">
    <li class=\"tracker-agreement-popup-list-item\">
	Le service FindFace (ci-après appelé \"Service FindFace\") est fourni par N-TECH.LAB LTD. 
	Les utilisateurs du Service FindFace doivent conclure un accord avec le Service FindFace Service et accepter les <a href=\"https://saas.findface.pro/files/docs/license_us.pdf\" class=\"tracker-link\">termes et conditions suivantes</a> en sélectionnant \"J'accepte\". 
	<br>
	En sélectionnant \"J'accepte\", l'Utilisateur accepte et garantit qu'il adhère aux conditions suivantes : 
     <ul class=\"tracker-agreement-popup-inner-list\">
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Respecter strictement toutes les lois et régulations locales qui gouvernent la confidentialité et l'utilisation des données personnelles dans le pays de résidence de l'Utilisateur. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Respecter strictement toutes les lois et régulations locales. Cette condition reste effective pour toute la durée de l'utilisation du Service FindFace. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Obtenir une autorisation écrite, sauf si prescrit par les autorités locales de tous les pays dans lesquels l'Utilisateur opère. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       L'Utilisateur doit diligemment informer chaque individu dont les données personnelles, y compris les photos et les images prises par la caméra, seront traitées par l'utilisation du Service FindFace.
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Obtenir un avis juridique AVANT d'utiliser le Service FindFace. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
	   Garantir que les informations téléversées par l'Utilisateur sont autorisées par l'individu concerné et respectent les lois et les régulations de la juridiction de l'Utilisateur.
	  </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
	   Confirmer que l'Utilisateur utilise le Service FindFace à ses propres risques et sans aucune garantie d'aucune sorte. 
	  </li>	  
     </ul>
    </li>
    <li class=\"tracker-agreement-popup-list-item\">
     Bitrix, Inc, n'est aucunement responsable du Service et ne prétend ni ne garantit la précision ou la disponibilité du Service, ni ne fournit une quelconque assistance technique ou consultation sur le Service. 
    </li>
   </ol>
   <div class=\"tracker-agreement-popup-description\">
    Bitrix, Inc, n'est aucunement responsable du Service FindFace Service et ne prétend ni ne garantit le Service FindFace.
	En acceptant les termes et conditions de ce contrat d'utilisation, l'Utilisateur accepte et confirme que le Service FindFace, et non Bitrix, Inc. collecte et traite les données téléversées par l'Utilisateur.
	L'Utilisateur confirme que le Service FindFace Service, et non Bitrix, Inc. connaît les circonstances dans lesquelles l'Utilisateur a collecté les données, photographies ou images. 
	L'Utilisateur accepte de ne pas tenir le service FindFace et Bitrix responsables pour toute action et inaction de l'Utilisateur. 
	<br><br>
	Résolution des conflits ; arbitrage exécutoire : tout conflit, polémique, interprétation ou revendication, y compris les revendications de - mais pas limitées à - rupture de contrat, négligence de toute forme, fraude ou mauvaise représentation émanant, découlant ou liées à ce contrat doivent être soumis à un arbitrage définitif et exécutoire. 
   </div>
  </div>";
$MESS["FACEID_TRACKERWD_CMP_AGR_BUTTON"] = "J'accepte les Conditions";
$MESS["FACEID_TRACKERWD_CMP_JS_CAMERA_DEFAULT"] = "Caméra";
$MESS["FACEID_TRACKERWD_CMP_JS_CAMERA_NOT_FOUND"] = "Caméra introuvable";
$MESS["FACEID_TRACKERWD_CMP_JS_CAMERA_NO_SUPPORT"] = "Votre navigateur ne prend pas en charge la caméra";
$MESS["FACEID_TRACKERWD_CMP_JS_CAMERA_ERROR"] = "Votre navigateur ne prend pas en charge l'enregistrement vidéo ou la caméra n'est pas connectée.";
$MESS["FACEID_TRACKERWD_CMP_JS_FACE_NOT_FOUND"] = "Malheureusement, nous n'avons pas réussi à reconnaître la personne. Veuillez essayer de prendre une autre photo.";
$MESS["FACEID_TRACKERWD_CMP_JS_FACE_ORIGINAL"] = "Exemple de photo";
$MESS["FACEID_TRACKERWD_CMP_JS_SAVE_CRM"] = "Enregistrer dans la GRC";
$MESS["FACEID_TRACKERWD_CMP_JS_SAVE_CRM_DONE"] = "La piste a été enregistrée";
$MESS["FACEID_TRACKERWD_CMP_JS_VK_LINK"] = "VK";
$MESS["FACEID_TRACKERWD_CMP_JS_VK_LINK_ACTION"] = "Trouver un profil VK";
$MESS["FACEID_TRACKERWD_CMP_JS_VK_FOUND_PEOPLE"] = "personnes";
$MESS["FACEID_TRACKERWD_CMP_JS_VK_SELECT"] = "Sélectionner";
$MESS["FACEID_TRACKERWD_CMP_AUTH_ONLY"] = "Ce composant nécessite une authentification.";
$MESS["FACEID_TRACKERWD_CMP_STATUS_STARTED"] = "Pointage en arrivant";
$MESS["FACEID_TRACKERWD_CMP_STATUS_ENDED"] = "Pointage en partant";
$MESS["FACEID_TRACKERWD_CMP_ACTION_END"] = "pointage en partant";
?>