<?
$MESS["FIT_SECTION_TEMPLATE_NAME"] = "Besuch-Tracker";
$MESS["FIT_SECTION_TEMPLATE_DESCRIPTION"] = "Zeigt Besucher-Tracker und die dazu gehörige Statistik an";
$MESS["FITWD_SERVICES_MAIN_SECTION"] = "Intranet Portal";
$MESS["FITWD_SERVICES_PARENT_SECTION"] = "Arbeitszeitmanagement";
$MESS["FITWD_SECTION_TEMPLATE_NAME"] = "Gesichtserkennung für Arbeitszeitmanagement";
$MESS["FITWD_SECTION_TEMPLATE_DESCRIPTION"] = "Ermöglicht es den Mitarbeitern, den Arbeitstag mithilfe von der Gesichtserkennung zu starten und zu beenden.";
?>