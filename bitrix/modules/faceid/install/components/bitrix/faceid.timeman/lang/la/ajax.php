<?
$MESS["FACEID_TRACKERWD_CMP_ERROR_ANON"] = "Error de autenticación";
$MESS["FACEID_TRACKERWD_CMP_ERROR_TIMEMAN_ACCESS"] = "Se denegó el acceso a la edición del tiempo de trabajo de los empleados";
$MESS["FACEID_TRACKERWD_CMP_ERROR_AGREEMENT"] = "El acuerdo de licencia del Face Tracker no fue aceptado.";
$MESS["FACEID_TRACKERWD_CMP_ERROR_OPEN_DAY"] = "Error al marcar ingreso";
$MESS["FACEID_TRACKERWD_CMP_ERROR_PAUSE_DAY"] = "Error al registrar descanso";
$MESS["FACEID_TRACKERWD_CMP_ERROR_REOPEN_DAY"] = "Error al reanudar el día de trabajo";
$MESS["FACEID_TRACKERWD_CMP_ERROR_CLOSE_DAY"] = "Error al marcar salida";
$MESS["FACEID_TRACKERWD_CMP_ERROR_TIMEMAN_DISABLED"] = "La gestión del tiempo de trabajo está desactivada para este empleado";
$MESS["FACEID_TRACKERWD_CMP_ERROR_USER_NOT_FOUND_LOCAL"] = "El usuario no fue encontrado en la base de datos";
$MESS["FACEID_TRACKERWD_CMP_ERROR_NO_FACE_LOCAL"] = "El rostro no fue encontrado en la base de datos";
$MESS["FACEID_TRACKERWD_CMP_ERROR_INDEX_DISABLED"] = "Debe habilitar la indexación de fotos de avatar de usuario en la sección Tiempo y Reportes - Bitrix24.Tiempo.";
?>