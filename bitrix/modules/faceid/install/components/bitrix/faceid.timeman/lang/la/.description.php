<?
$MESS["FITWD_SERVICES_MAIN_SECTION"] = "Portal de la Intranet";
$MESS["FITWD_SERVICES_PARENT_SECTION"] = "Gestión del tiempo de trabajo";
$MESS["FITWD_SECTION_TEMPLATE_NAME"] = "Face Tracker para gestión del tiempo de trabajo ";
$MESS["FITWD_SECTION_TEMPLATE_DESCRIPTION"] = "Habilita a los empleados para ingresar y salir usando el reconocimiento facial.";
?>