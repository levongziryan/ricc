<?
$MESS["FACEID_TRACKERWD_CMP_ERROR_ANON"] = "Erreur d'authentification";
$MESS["FACEID_TRACKERWD_CMP_ERROR_TIMEMAN_ACCESS"] = "L'accès à l'édition du temps de travail des employés a été refusé";
$MESS["FACEID_TRACKERWD_CMP_ERROR_AGREEMENT"] = "Le Contrat de licence du Traqueur facial n'a pas été accepté";
$MESS["FACEID_TRACKERWD_CMP_ERROR_OPEN_DAY"] = "Erreur de pointage au moment d'arriver";
$MESS["FACEID_TRACKERWD_CMP_ERROR_PAUSE_DAY"] = "Erreur lors de la saisie de la pause";
$MESS["FACEID_TRACKERWD_CMP_ERROR_REOPEN_DAY"] = "Erreur lors de la reprise de jour de travail";
$MESS["FACEID_TRACKERWD_CMP_ERROR_CLOSE_DAY"] = "Erreur de pointage au moment de partir";
$MESS["FACEID_TRACKERWD_CMP_ERROR_TIMEMAN_DISABLED"] = "La gestion du temps de travail a été désactivée pour cet employé";
$MESS["FACEID_TRACKERWD_CMP_ERROR_USER_NOT_FOUND_LOCAL"] = "Utilisateur introuvable dans la base de données";
$MESS["FACEID_TRACKERWD_CMP_ERROR_NO_FACE_LOCAL"] = "Visage introuvable dans la base de données";
$MESS["FACEID_TRACKERWD_CMP_ERROR_INDEX_DISABLED"] = "Vous devez activer l'indexation des photos d'avatar d'utilisateur dans la section Temps et rapports de Bitrix24.Time.";
?>