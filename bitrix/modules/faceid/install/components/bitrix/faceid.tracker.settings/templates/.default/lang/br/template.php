<?
$MESS["FACEID_FTS_AUTO_LEAD_SETTING_A"] = "criar automaticamente novo cliente potencial";
$MESS["FACEID_FTS_LEAD_SOURCE_SETTING"] = "Nova fonte de cliente potencial";
$MESS["FACEID_FTS_CONFIG_EDIT_SAVE"] = "Salvar";
$MESS["FACEID_FTS_AUTO_LEAD_SETTING"] = "Se o visitante não foi encontrado no CRM";
$MESS["FACEID_FTS_AUTO_LEAD_SETTING_M"] = "criar manualmente";
$MESS["FACEID_FTS_SOCNET_ENABLED"] = "Ativar pesquisa de serviços sociais";
$MESS["FACEID_FTS_TO_FTRACKER"] = "Abrir Face Tracker";
$MESS["FACEID_FTS_DELETE_ALL"] = "Excluir todos os visitantes do Face Tracker";
$MESS["FACEID_FTS_DELETE_ALL_CONFIRM"] = "Você tem certeza de que deseja limpar o histórico e excluir todos os visitantes?";
?>