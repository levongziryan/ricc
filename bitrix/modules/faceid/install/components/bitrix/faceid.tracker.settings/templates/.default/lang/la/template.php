<?
$MESS["FACEID_FTS_AUTO_LEAD_SETTING_A"] = "creación automática de un nuevo prospecto";
$MESS["FACEID_FTS_LEAD_SOURCE_SETTING"] = "Nuevo origen del prospecto";
$MESS["FACEID_FTS_CONFIG_EDIT_SAVE"] = "Guardar";
$MESS["FACEID_FTS_AUTO_LEAD_SETTING"] = "Si el visitante no se encontró en CRM";
$MESS["FACEID_FTS_AUTO_LEAD_SETTING_M"] = "crear manualmente";
$MESS["FACEID_FTS_SOCNET_ENABLED"] = "Habilite la búsqueda de redes sociales";
$MESS["FACEID_FTS_TO_FTRACKER"] = "Abrir Face Tracker";
$MESS["FACEID_FTS_DELETE_ALL"] = "Eliminar todos los visitantes de Face Tracker";
$MESS["FACEID_FTS_DELETE_ALL_CONFIRM"] = "¿Estás seguro que quiere eliminar el historial y eliminar todos los visitantes?";
?>