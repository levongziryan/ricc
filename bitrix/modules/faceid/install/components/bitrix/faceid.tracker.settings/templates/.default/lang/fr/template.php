<?
$MESS["FACEID_FTS_AUTO_LEAD_SETTING_A"] = "créer automatiquement un nouveau lead";
$MESS["FACEID_FTS_LEAD_SOURCE_SETTING"] = "Nouvelle source de lead";
$MESS["FACEID_FTS_CONFIG_EDIT_SAVE"] = "Enregistrer";
$MESS["FACEID_FTS_AUTO_LEAD_SETTING"] = "Si le visiteur est introuvable dans le CRM";
$MESS["FACEID_FTS_AUTO_LEAD_SETTING_M"] = "créer manuellement";
$MESS["FACEID_FTS_SOCNET_ENABLED"] = "Activer la recherche de services sociaux";
$MESS["FACEID_FTS_TO_FTRACKER"] = "Ouvrir le Face tracker";
$MESS["FACEID_FTS_DELETE_ALL"] = "Supprimer tous les visiteurs du Face tracker";
$MESS["FACEID_FTS_DELETE_ALL_CONFIRM"] = "Voulez-vous vraiment effacer l'historique et supprimer tous les visiteurs ?";
?>