<?
$MESS["JS_CORE_PL_TASKS_ADD"] = "veuillez saisir le contenu de la tâche";
$MESS["JS_CORE_PL_TASKS_CHOOSE"] = "choisir dans la liste";
$MESS["JS_CORE_PL_TASKS_CREATE"] = "Ajouter une tâche";
$MESS["JS_CORE_PL_TASKS_FINISH"] = "Terminer la tâche et garder mon temps là-dessus";
$MESS["JS_CORE_PL_TASKS"] = "Tâches du jour";
$MESS["JS_CORE_PL_TASKS_START_TIMER"] = "Commencer le comptage de mon temps";
$MESS["JS_CORE_PL_TASKS_STOP_TIMER"] = "Suspendre le comptage de mon temps";
$MESS["JS_CORE_PL_TASKS_MENU_REMOVE_FROM_PLAN"] = "Exclure du plan du jour";
?>