<?
$MESS["TASKS_COMMON_CONFIRM_DELETE"] = "Voulez-vous vraiment supprimer #ENTITY_NAME# ?";
$MESS["TASKS_TASK_STATUS_1"] = "Nouveau";
$MESS["TASKS_TASK_STATUS_2"] = "En attente";
$MESS["TASKS_TASK_STATUS_3"] = "En cours";
$MESS["TASKS_TASK_STATUS_4"] = "En attente de contrôle";
$MESS["TASKS_TASK_STATUS_5"] = "Terminé";
$MESS["TASKS_TASK_STATUS_6"] = "Différé";
$MESS["TASKS_TASK_STATUS_7"] = "Rejeté";
$MESS["TASKS_TASK_STATUS_-1"] = "Dépassé";
$MESS["TASKS_COMMON_LOADING"] = "Chargement";
$MESS["TASKS_COMMON_TASK"] = "Tâche";
$MESS["TASKS_COMMON_TASKS"] = "Tâches";
$MESS["TASKS_COMMON_CANCEL_SELECT"] = "Annuler la sélection";
$MESS["TASKS_COMMON_NOT_SELECTED"] = "Non sélectionné";
$MESS["TASKS_COMMON_CHANGE"] = "Modifier";
$MESS["TASKS_COMMON_ADD"] = "Ajouter";
$MESS["TASKS_COMMON_ADD_MORE"] = "Ajouter plus";
$MESS["TASKS_COMMON_DONT_SHOW_AGAIN"] = "Ne plus afficher";
$MESS["TASKS_COMMON_NO"] = "Non";
$MESS["TASKS_COMMON_YES"] = "Oui";
$MESS["TASKS_COMMON_ALL"] = "Tout";
$MESS["TASKS_COMMON_ANY"] = "N'importe quel";
$MESS["TASKS_COMMON_CANCEL"] = "Annuler";
$MESS["TASKS_COMMON_SAVE"] = "Enregistrer";
$MESS["TASKS_COMMON_SAVE_CHANGES"] = "Enregistrer les modifications";
$MESS["TASKS_COMMON_DONT_ASK_AGAIN"] = "Ne plus demander";
$MESS["TASKS_COMMON_ACCESS_DENIED"] = "Accès refusé";
$MESS["TASKS_COMMON_ACTION_NOT_ALLOWED"] = "Action impossible";
$MESS["TASKS_COMMON_HIDE"] = "Masquer";
$MESS["TASKS_COMMON_DELETE"] = "Supprimer";
$MESS["TASKS_COMMON_CLOSE"] = "Fermer";
$MESS["TASKS_COMMON_SELECT"] = "Sélectionner";
$MESS["TASKS_COMMON_TASK_LC"] = "tâche";
$MESS["TASKS_COMMON_TEMPLATE"] = "Modèle";
$MESS["TASKS_COMMON_TEMPLATE_LC"] = "modèle";
$MESS["TASKS_COMMON_ERROR_OCCURRED"] = "Malheureusement, une erreur s'est produite";
$MESS["TASKS_COMMON_CHANGE_LCF"] = "modifier";
$MESS["TASKS_COMMON_ADD_LCF"] = "ajouter";
$MESS["TASKS_COMMON_MORE"] = "Plus";
$MESS["TASKS_COMMON_ANY_M"] = "N’importe quel";
$MESS["TASKS_COMMON_EDIT"] = "Modifier";
$MESS["TASKS_COMMON_NEW_USER"] = "Nouvel utilisateur";
$MESS["TASKS_COMMON_HOUR_F0"] = "heure";
$MESS["TASKS_COMMON_HOUR_F1"] = "heure";
$MESS["TASKS_COMMON_HOUR_F2"] = "heures";
$MESS["TASKS_COMMON_MINUTE_F0"] = "minute";
$MESS["TASKS_COMMON_MINUTE_F1"] = "minutes";
$MESS["TASKS_COMMON_MINUTE_F2"] = "minutes";
$MESS["TASKS_COMMON_DAY_F0"] = "jour";
$MESS["TASKS_COMMON_DAY_F1"] = "jours";
$MESS["TASKS_COMMON_DAY_F2"] = "jours";
$MESS["TASKS_COMMON_OP_CREATE"] = "créer";
$MESS["TASKS_COMMON_OP_READ"] = "lu";
$MESS["TASKS_COMMON_OP_UPDATE"] = "mettre à jour";
$MESS["TASKS_COMMON_OP_DELETE"] = "supprimer";
$MESS["TASKS_COMMON_TASK_ALT_A"] = "Tâche";
?>