<?
$MESS["TASKS_GANTT_EMPTY_DATE"] = "aucun";
$MESS["TASKS_GANTT_CHART_TITLE"] = "Tâches";
$MESS["TASKS_GANTT_DATE_START"] = "Date de début";
$MESS["TASKS_GANTT_DATE_END"] = "Date de fin";
$MESS["TASKS_GANTT_DEADLINE"] = "Date limite";
$MESS["TASKS_GANTT_START"] = "début";
$MESS["TASKS_GANTT_END"] = "fin";
$MESS["TASKS_GANTT_DELETE_DEPENDENCY"] = "Supprimer";
$MESS["TASKS_GANTT_EMPTY_END_DATE"] = "La date de fin est requise pour établir un lien";
$MESS["TASKS_GANTT_CIRCULAR_DEPENDENCY"] = "Les liens circulaires ne sont pas autorisés";
$MESS["TASKS_GANTT_PERMISSION_ERROR"] = "Vous ne disposez pas des permissions pour éditer les dates de cette tâche";
$MESS["TASKS_GANTT_DEPENDENCY_FROM"] = "De";
$MESS["TASKS_GANTT_DEPENDENCY_TO"] = "À";
$MESS["TASKS_GANTT_RELATION_ERROR"] = "Impossible de lier une tâche à sa tâche parente";
$MESS["TASKS_GANTT_INDENT_TASK"] = "Mettre la tâche en retrait";
$MESS["TASKS_GANTT_OUTDENT_TASK"] = "Mettre la tâche en retrait négatif";
?>