<?
$MESS["TASKS_PREVENT_WEBDAV_UNINSTALL"] = "Vous ne pouvez pas supprimer ce module, car il est nécessaire pour le module de tâches.";
$MESS["TASKS_PREVENT_FORUM_UNINSTALL"] = "Vous ne pouvez pas supprimer ce module, car il est nécessaire pour le module de tâches.";
$MESS["TASKS_PREVENT_INTRANET_UNINSTALL"] = "Vous ne pouvez pas supprimer ce module, car il est nécessaire pour le module de tâches.";
?>