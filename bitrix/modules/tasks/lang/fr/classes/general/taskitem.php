<?
$MESS["TASK_CANT_ADD_LINK"] = "Impossible de lier les tâches";
$MESS["TASK_CANT_DELETE_LINK"] = "Impossible de délier les tâches";
$MESS["TASKS_HAS_PARENT_RELATION"] = "Impossible de lier une tâche à sa tâche parente";
$MESS["TASKS_TRIAL_PERIOD_EXPIRED"] = "Votre période d'essai a expiré";
$MESS["TASK_CANT_UPDATE_LINK"] = "Impossible de mettre à jour le lien d'une tâche";
?>