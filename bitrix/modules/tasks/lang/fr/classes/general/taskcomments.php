<?
$MESS["TASKS_COMMENT_MESSAGE_ADD"] = "On a ajouté un commentaire à la tâche '#TASK_TITLE#' avec le texte suivant: '#TASK_COMMENT_TEXT#'";
$MESS["TASKS_COMMENT_MESSAGE_ADD_M"] = "On a ajouté un commentaire à la tâche '#TASK_TITLE#' avec le texte suivant: '#TASK_COMMENT_TEXT#'";
$MESS["TASKS_COMMENT_MESSAGE_ADD_F"] = "Commentaire ajouté à la tâche '#TASK_TITLE#' avec le texte suivant: '#TASK_COMMENT_TEXT#'";
$MESS["TASKS_COMMENT_MESSAGE_EDIT"] = "A remplacé le commentaire pour la tâche '#TASK_TITLE#' par le texte: '#TASK_COMMENT_TEXT#'";
$MESS["TASKS_COMMENT_MESSAGE_EDIT_M"] = "A remplacé le commentaire pour la tâche '#TASK_TITLE#' par le texte: '#TASK_COMMENT_TEXT#'";
$MESS["TASKS_COMMENT_MESSAGE_EDIT_F"] = "Il (elle) a remplacé le commentaire pour la tâche '#TASK_TITLE# par le texte: '#TASK_COMMENT_TEXT#'.";
$MESS["TASKS_COMMENT_SONET_NEW_TASK_MESSAGE"] = "La tâche est créée";
?>