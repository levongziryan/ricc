<?
$MESS["TASKS_NS_REMINDER"] = "Rappel de la tâche";
$MESS["TASKS_NS_COMMENT"] = "Nouveau commentaire de la tâche";
$MESS["TASKS_NS_MANAGE"] = "Créer / modifier une tâche";
$MESS["TASKS_NS_TASK_ASSIGNED"] = "Notification sur l'attribution d'une tâche";
?>