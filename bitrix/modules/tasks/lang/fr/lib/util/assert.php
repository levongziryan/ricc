<?
$MESS["TASKS_ASSERT_INTEGER_EXPECTED"] = "L'argument#ARG_NAME#doit être un entier.";
$MESS["TASKS_ASSERT_INTEGER_NOTNULL_EXPECTED"] = "L'argument#ARG_NAME#doit être un entier positif.";
$MESS["TASKS_ASSERT_ARRAY_NOT_EMPTY_EXPECTED"] = "L'argument#ARG_NAME#doit être un tableau non vide.";
$MESS["TASKS_ASSERT_ARRAY_EXPECTED"] = "L'argument#ARG_NAME#doit être un tableau.";
$MESS["TASKS_ASSERT_STRING_NOTNULL_EXPECTED"] = "L'argument#ARG_NAME#doit être une chaîne non vide.";
$MESS["TASKS_ASSERT_ARRAY_OF_INTEGER_NOT_NULL_EXPECTED"] = "L'argument#ARG_NAME#est pas un tableau d'entiers positifs.";
$MESS["TASKS_ASSERT_ARRAY_OF_STRING_NOT_NULL_EXPECTED"] = "L'argument#ARG_NAME#est pas un tableau de chaînes non-vides.";
$MESS["TASKS_ASSERT_EMPTY_ENUMERATION"] = "Une énumération vide passé à la méthode de vérification.";
$MESS["TASKS_ASSERT_ITEM_NOT_IN_ENUMERATION"] = "L'argument#ARG_NAME#est pas une des valeurs d'énumération.";
$MESS["TASKS_ASSERT_EMPTY_ARGUMENT"] = "L'argument vide passé.";
$MESS["TASKS_ASSERT_INTEGER_NONNEGATIVE_EXPECTED"] = "L'argument#ARG_NAME#doit être un entier non négatif.";
?>