<?
$MESS["TASKS_REPLICATOR_TASK_CREATED"] = "Tâche planifiée créée";
$MESS["TASKS_REPLICATOR_TASK_CREATED_WITH_ERRORS"] = "Tâche planifiée créée, mais des erreurs sont survenues";
$MESS["TASKS_REPLICATOR_TASK_WAS_NOT_CREATED"] = "La tâche planifiée n'a pas été créée";
$MESS["TASKS_REPLICATOR_TASK_POSSIBLY_WAS_NOT_CREATED"] = "Il est possible que la tâche planifiée n'ait pas été créée";
$MESS["TASKS_REPLICATOR_PROCESS_STOPPED"] = "Itération de la tâche arrêtée";
$MESS["TASKS_REPLICATOR_END_DATE_REACHED"] = "Date de fin atteinte";
$MESS["TASKS_REPLICATOR_LIMIT_REACHED"] = "Maximum d'itérations atteint";
$MESS["TASKS_REPLICATOR_ILLEGAL_NEXT_TIME"] = "Impossible de calculer l'heure de la prochaine exécution";
$MESS["TASKS_REPLICATOR_PROCESS_ERROR"] = "Erreur lors du calcul de l'heure de la prochaine exécution";
$MESS["TASKS_REPLICATOR_NEXT_TIME"] = "Prochaine exécution prévue à : #TIME# (dans #PERIOD# secondes)";
$MESS["TASKS_REPLICATOR_SUBTREE_LOOP"] = "Il est possible que certaines des sous-tâches soient auto-référencées. Les sous-tâches n'ont pas été créées.";
$MESS["TASKS_REPLICATOR_INTERNAL_ERROR"] = "Erreur interne. Veuillez contacter l'assistance technique.";
$MESS["TASKS_REPLICATOR_CANT_IDENTIFY_USER"] = "Impossible de définir un utilisateur qui servira à exécuter la tâche. Veuillez vérifier si le compte administrateur est actif.";
$MESS["TASKS_REPLICATOR_SECOND_0"] = "seconde";
$MESS["TASKS_REPLICATOR_SECOND_1"] = "secondes";
$MESS["TASKS_REPLICATOR_SECOND_2"] = "secondes";
$MESS["TASKS_REPLICATOR_CREATOR_INACTIVE"] = "Le compte créateur de tâche n'existe pas ou est inactif.";
?>