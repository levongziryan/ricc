<?
$MESS["DEPENDENCE_ENTITY_TASK_ID_FIELD"] = "Tâche";
$MESS["DEPENDENCE_ENTITY_DEPENDS_ON_ID_FIELD"] = "Tâche parente";
$MESS["DEPENDENCE_ENTITY_TYPE_FIELD"] = "Type du lien";
$MESS["DEPENDENCE_ENTITY_CANT_ADD_LINK_CREATED_DATE_NOT_SET"] = "La tâche à lier n'a pas de date de création";
$MESS["DEPENDENCE_ENTITY_CANT_ADD_LINK_CREATED_DATE_NOT_SET_PARENT_TASK"] = "La tâche parente n'a pas de date de création";
$MESS["DEPENDENCE_ENTITY_CANT_ADD_LINK_END_DATE_PLAN_NOT_SET"] = "La tâche à lier n'a pas de date de fin prévue";
$MESS["DEPENDENCE_ENTITY_CANT_ADD_LINK_END_DATE_PLAN_NOT_SET_PARENT_TASK"] = "La tâche parente n'a pas de date de fin prévue";
?>