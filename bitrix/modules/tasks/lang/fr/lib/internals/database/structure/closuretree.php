<?
$MESS["TASKS_CLOSURE_TREE_LINK_EXISTS"] = "Nœuds déjà liés";
$MESS["TASKS_CLOSURE_TREE_NODE_NOT_FOUND"] = "Le noeud spécifié est introuvable.";
$MESS["TASKS_CLOSURE_TREE_NODE_EXISTS_BUT_DECLARED_NEW"] = "Le noeud spécifié existe mais sera ajouté comme nouveau";
$MESS["TASKS_CLOSURE_TREE_CANT_ATTACH_TO_SELF"] = "Impossible de lier nœud à lui-même";
$MESS["TASKS_CLOSURE_TREE_ILLEGAL_NODE"] = "ID de noeud incorrect";
$MESS["TASKS_CLOSURE_TREE_CANT_ATTACH_TO_ANCESTOR"] = "Impossible de lier nœud à son prédécesseur";
?>