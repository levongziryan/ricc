<?
$MESS["STAGES_ERROR_ACCESS_DENIED_GROUP_TASK"] = "Vous ne pouvez afficher les tâches dans ce groupe";
$MESS["STAGES_ERROR_ACCESS_DENIED_STAGES"] = "Vous ne pouvez gérer les étapes";
$MESS["STAGES_ERROR_ACCESS_DENIED_MOVE"] = "Vous ne pouvez déplacer cette tâche";
$MESS["STAGES_ERROR_DIFFERENT_STAGES"] = "L'étape et la tâche appartiennent à des groupes différents";
$MESS["STAGES_ERROR_EMPTY_TITLE"] = "L'intitulé de l'étape est manquant";
$MESS["STAGES_ERROR_NOT_FOUND"] = "L'étape est introuvable";
$MESS["STAGES_ERROR_EMPTY_DATA"] = "Aucune donnée de mise à jour n'a été fournie";
$MESS["STAGES_ERROR_NO_EMPTY"] = "Il y a des tâches dans cette étape";
$MESS["STAGES_ERROR_IS_SYSTEM"] = "L'étape par défaut ne peut être supprimée";
$MESS["STAGES_ERROR_SOCIALNETWORK_IS_NOT_INSTALLED"] = "le module réseau social n'est pas installé.";
$MESS["STAGES_ERROR_TASK_NOT_FOUND"] = "La tâche est introuvable, ou son accès est refusé.";
?>