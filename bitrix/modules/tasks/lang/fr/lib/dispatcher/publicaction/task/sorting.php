<?
$MESS["TASKS_SORTING_AUTH_REQUIRED"] = "Vous devez être autorisé pour effectuer cette opération.";
$MESS["TASKS_SORTING_WRONG_SOURCE_TASK"] = "Tâche source introuvable";
$MESS["TASKS_SORTING_WRONG_TARGET_TASK"] = "Tâche adjacente introuvable";
$MESS["TASKS_SORTING_WRONG_GROUP_PERMISSIONS"] = "Vous ne pouvez pas trier les tâches dans ce groupe";
?>