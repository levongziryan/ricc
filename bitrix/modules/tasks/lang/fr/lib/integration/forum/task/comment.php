<?
$MESS["TASKS_COMMENT_MESSAGE_ADD_N"] = "A ajouté un commentaire à la tâche #TASK_URL_BEGIN##TASK_TITLE##URL_END#.";
$MESS["TASKS_COMMENT_MESSAGE_EDIT_N"] = "A changé un commentaire de #TASK_URL_BEGIN##TASK_TITLE##URL_END#.";
$MESS["TASKS_COMMENT_MESSAGE_ADD_F"] = "A ajouté un commentaire à la tâche #TASK_URL_BEGIN##TASK_TITLE##URL_END#.";
$MESS["TASKS_COMMENT_MESSAGE_ADD_WITH_TEXT"] = " Le texte du commentaire est : \"#TASK_COMMENT_TEXT#\"";
$MESS["TASKS_COMMENT_MESSAGE_EDIT_WITH_TEXT"] = " Le nouveau texte est : \"#TASK_COMMENT_TEXT#\"";
$MESS["TASKS_COMMENT_MESSAGE_EDIT_F"] = "A mis à jour un commentaire de #TASK_URL_BEGIN##TASK_TITLE##URL_END#.";
$MESS["TASKS_COMMENT_MESSAGE_ADD_M"] = "A ajouté un commentaire à la tâche #TASK_URL_BEGIN##TASK_TITLE##URL_END#.";
$MESS["TASKS_COMMENT_MESSAGE_EDIT_M"] = "A changé un commentaire de #TASK_URL_BEGIN##TASK_TITLE##URL_END#.";
$MESS["TASKS_COMMENT_MESSAGE_ADD_PUSH_M"] = "#USER_NAME# a commenté sur la tâche #TASK_TITLE#";
$MESS["TASKS_COMMENT_MESSAGE_ADD_PUSH_F"] = "#USER_NAME# a commenté sur la tâche #TASK_TITLE#";
?>