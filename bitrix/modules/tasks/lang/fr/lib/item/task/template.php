<?
$MESS["TASKS_ITEM_TASK_TEMPLATE_FIELD_SE_CHECKLIST"] = "Liste de contrôle";
$MESS["TASKS_ITEM_TASK_TEMPLATE_FIELD_SE_ACCESS"] = "Autorisations d'accès";
$MESS["TASKS_ITEM_TASK_TEMPLATE_FIELD_SE_TAG"] = "Tags";
$MESS["TASKS_ITEM_TASK_TEMPLATE_FIELD_BASE_TEMPLATE_ID"] = "Modèle général";
$MESS["TASKS_ITEM_TASK_TEMPLATE_CANT_SWITCH_TYPE_ERROR"] = "Impossible de changer le type d’un modèle existant";
$MESS["TASKS_ITEM_TASK_TEMPLATE_PARENT_ITEM_CONFLICT_ERROR"] = "Impossible de définir le modèle général et la tâche de base simultanément";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_MULTITASKING_ALLOWED_ERROR_BECAUSE_TYPE_1"] = "Impossible de créer un modèle de plusieurs personnes responsables pour le nouvel utilisateur.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_BASE_TEMPLATE_ALLOWED_ERROR_BECAUSE_TYPE_1"] = "Impossible d’assigner un modèle de base au modèle du nouvel utilisateur.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_BASE_TEMPLATE_ALLOWED_ERROR_BECAUSE_REPLICATION"] = "Impossible d’assigner le modèle de base car l’exécution récurrente est activée.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_BASE_TEMPLATE_ALLOWED_ERROR_BECAUSE_MULTITASK"] = "Impossible d’assigner le modèle de base si plusieurs personnes responsables sont indiquées.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_REPLICATION_ALLOWED_ERROR_BECAUSE_TYPE_1"] = "Impossible de définir les paramètres d’exécution récurrente lors de la création d'un modèle de nouvel utilisateur.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_REPLICATION_ALLOWED_ERROR_BASE_TEMPLATE_ID"] = "Impossible de définir les paramètres d’exécution récurrente lors de l'attribution d'un modèle de base.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_TPARAM_TYPE_1_ALLOWED_ERROR_BECAUSE_REPLICATION"] = "Impossible de créer le modèle de nouvel utilisateur car l’exécution récurrente est activée.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_TPARAM_TYPE_1_ALLOWED_ERROR_BASE_TEMPLATE_ID"] = "Impossible de créer le modèle de nouvel utilisateur lors de l'attribution du modèle de base.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_TPARAM_TYPE_1_ALLOWED_ERROR_BECAUSE_MULTITASK"] = "Impossible de créer le modèle de nouvel utilisateur car l’exécution récurrente est activée.";
?>