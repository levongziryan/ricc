<?
$MESS["TASKS_ASSET_QUERY_QUERY_FAILED"] = "Erreur lors du traitement de la requête..";
$MESS["TASKS_ASSET_QUERY_QUERY_FAILED_STATUS"] = "Erreur lors du traitement de la requête.. (HTTP #HTTP_STATUS#)";
$MESS["TASKS_ASSET_QUERY_ILLEGAL_RESPONSE"] = "Erreur d'analyse de la réponse du serveur.";
$MESS["TASKS_ASSET_QUERY_QUERY_FAILED_EXCEPTION"] = "Erreur d'analyse de la réponse du serveur.";
$MESS["TASKS_ASSET_QUERY_EMPTY_RESPONSE"] = "Réponse vide du serveur.";
?>