<?
$MESS["TASK_REMINDER_SUBJECT"] = "#SITE_NAME#: Rappel sur la tâche '#TASK_TITLE#'.";
$MESS["TASK_REMINDER_DESC"] = "#TASK_TITLE# - intitulé de la tâche
#PATH_TO_TASK# - lien pour accèder à la tâche";
$MESS["TASK_REMINDER_MESSAGE"] = "Message d'information du site #SITE_NAME#
------------------------------------------
Lien à l'affichage de la tâche:

#PATH_TO_TASK#

Ce message a été généré automatiquement.";
$MESS["TASK_REMINDER_NAME"] = "Rappel de la tâche";
$MESS["TASKS_ADD_TASK_NAME"] = "Nouvelle tâche ajoutée";
$MESS["TASKS_ADD_TASK_DESC"] = "#EMAIL_TO# - E-mail du destinataire
#TASK_ID# - ID de la tâche
#RECIPIENT_ID# - ID de l'utilisateur assigné
#URL_ID# - URL de la tâche 
";
$MESS["TASKS_UPDATE_TASK_NAME"] = "Statut de la tâche changé";
$MESS["TASKS_UPDATE_TASK_DESC"] = "#EMAIL_TO# - E-mail du destinataire
#TASK_ID# - ID de la tâche
#RECIPIENT_ID# - ID de l'utilisateur assigné
#URL_ID# - URL de la tâche 
";
$MESS["TASKS_ADD_COMMENT_NAME"] = "Nouveau commentaire ajouté à la tâche";
$MESS["TASKS_TASK_ADD_EMAIL_NAME"] = "Nouvelle tâche ajoutée (envoyer un e-mail à l'utilisateur)";
$MESS["TASKS_TASK_ADD_EMAIL_DESC"] = "#EMAIL_TO# - E-mail du destinataire
#TASK_ID# - ID de la tâche
#TASK_TITLE# - Titre de la tâche
#RECIPIENT_ID# - ID de l'utilisateur assigné
#USER_ID# - ID de l'utilisateur pour vérifier les permissions de la tâche
#URL# - URL de la tâche
#SUBJECT# - Titre du message
";
$MESS["TASKS_TASK_UPDATE_EMAIL_NAME"] = "Tâche modifiée (envoyer un e-mail à l'utilisateur)";
$MESS["TASKS_TASK_UPDATE_EMAIL_DESC"] = "#EMAIL_TO# - E-mail du destinataire
#TASK_ID# - ID de la tâche
#TASK_TITLE# - Titre de la tâche
#TASK_PREVIOUS_FIELDS# - Détails de la tâche avant modification
#RECIPIENT_ID# - ID de l'utilisateur assigné
#USER_ID# - ID de l'utilisateur pour vérifier les permissions de la tâche
#URL# - URL de la tâche
#SUBJECT# - Titre du message
";
$MESS["TASKS_TASK_COMMENT_ADD_EMAIL_NAME"] = "Nouveau commentaire ajouté à la tâche (envoyer un e-mail à l'utilisateur)";
$MESS["TASKS_TASK_COMMENT_ADD_EMAIL_DESC"] = "#EMAIL_TO# - E-mail du destinataire
#TASK_ID# - ID de la tâche
#TASK_TITLE# - Titre de la tâche
#COMMENT_ID# - ID du commentaire
#RECIPIENT_ID# - ID de l'utilisateur assigné
#USER_ID# - ID de l'utilisateur pour vérifier les permissions de la tâche
#URL# - URL de la tâche
#SUBJECT# - Titre du message";
?>