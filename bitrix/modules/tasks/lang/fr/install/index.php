<?
$MESS["TASKS_INSTALL_BACK"] = "Revenir à la commande des modules";
$MESS["TASKS_UNINSTALL_WARNING"] = "Attention ! Le module sera supprimé du système.";
$MESS["TASKS_UNINSTALL_SAVEDATA"] = "Vous pouvez stocker des données dans les tables de la base de données, si vous installez le drapeau &quot;Sauvegarder les tables&quot;";
$MESS["TASKS_UNINSTALL_TITLE"] = "Nom unique pour la liste de comparaison";
$MESS["TASKS_MODULE_NAME"] = "Tâches";
$MESS["TASKS_CONVERT_AND_USE_NEW"] = "Convertir les tâches et passer à l'utilisation de Tâches 2.0";
$MESS["TASKS_INSTALL_COPY_PUBLIC"] = "Copier les scripts de la section publique";
$MESS["TASKS_MODULE_DESC"] = "Module de gestion des tâches";
$MESS["TASKS_UNINSTALL_ERROR"] = "Erreurs lors de la suppression:";
$MESS["COPY_PUBLIC_FILES"] = "Copier les Fichiers Réservés au public Et le Modèle du Site";
$MESS["TASKS_UNINSTALL_SAVETABLE"] = "Enregistrer tableaux";
$MESS["TASKS_UNINSTALL_COMPLETE"] = "La suppression est terminée.";
$MESS["TASKS_UNINSTALL_DEL"] = "Supprimer";
$MESS["TASKS_INSTALL_PUBLIC_SETUP"] = "Installation";
$MESS["TASKS_INSTALL_TITLE"] = "Installez le module de gestion des tâches";
$MESS["TASKS_INSTALL_COMPLETE_ERROR"] = "L'installation est terminée avec erreurs";
$MESS["TASKS_INSTALL_COMPLETE_OK"] = "L'installation est terminée.";
?>