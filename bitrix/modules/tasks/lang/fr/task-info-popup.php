<?
$MESS["TASKS_DATE_START"] = "Date de début";
$MESS["TASKS_DATE_END"] = "Date de fin";
$MESS["TASKS_DATE_STARTED"] = "Démarré(e)s le";
$MESS["TASKS_DATE_COMPLETED"] = "Terminé le";
$MESS["TASKS_DATE_CREATED"] = "Créé le";
$MESS["TASKS_DATE_DEADLINE"] = "Date limite";
$MESS["TASKS_TASK_TITLE_LABEL"] = "Tâche No.";
$MESS["TASKS_STATUS"] = "Statut";
$MESS["TASKS_STATUS_OVERDUE"] = "En retard";
$MESS["TASKS_STATUS_NEW"] = "Nouveau";
$MESS["TASKS_STATUS_ACCEPTED"] = "En attente";
$MESS["TASKS_STATUS_IN_PROGRESS"] = "En cours";
$MESS["TASKS_STATUS_WAITING"] = "En attente de passage en revue";
$MESS["TASKS_STATUS_COMPLETED"] = "Terminée";
$MESS["TASKS_STATUS_DELAYED"] = "Repoussé";
$MESS["TASKS_STATUS_DECLINED"] = "Refusé";
$MESS["TASKS_PRIORITY"] = "Priorité";
$MESS["TASKS_PRIORITY_0"] = "Basse";
$MESS["TASKS_PRIORITY_1"] = "Normale";
$MESS["TASKS_PRIORITY_2"] = "Haute";
$MESS["TASKS_QUICK_INFO_DETAILS"] = "Détails";
$MESS["TASKS_QUICK_INFO_EMPTY_DATE"] = "non";
$MESS["TASKS_RESPONSIBLE"] = "Responsable";
$MESS["TASKS_DIRECTOR"] = "Créateur";
$MESS["TASKS_FILES"] = "Fichiers";
$MESS["TASKS_PRIORITY_V2"] = "Tâche importante";
?>