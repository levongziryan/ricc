<?
$MESS["TASKS_COUNTERS_NOTICE_TITLE"] = "Bitrix24 informa:";
$MESS["TASKS_COUNTERS_NOTICE_CONTENT_V2_PLURAL_0"] = "[B]#TASKS_COUNT#  de suas tarefas[/B] precisam da sua atenção ou foram comentadas.
[URL=#HREF#]Clique para ver os detalhes[/URL].";
$MESS["TASKS_COUNTERS_NOTICE_CONTENT_V2_PLURAL_1"] = "[B]#TASKS_COUNT# de suas tarefas[/B] precisam da sua atenção ou foram comentadas.
[URL=#HREF#]Clique para ver os detalhes[/URL].";
$MESS["TASKS_COUNTERS_NOTICE_CONTENT_V2_PLURAL_2"] = "[B]#TASKS_COUNT# de suas tarefa[/B] precisam da sua atenção ou foram comentadas.
[URL=#HREF#]Clique para ver os detalhes[/URL].";
$MESS["TASKS_COUNTERS_NOTICE_CONTENT_PLURAL_0"] = "#USERNAME#, [B]#TASKS_COUNT# de suas tarefas [/B] contém observações ou precisam de uma resposta.
[URL=#HREF#]Vá para Tarefas para visualizá-las[/URL].";
$MESS["TASKS_COUNTERS_NOTICE_CONTENT_PLURAL_1"] = "#USERNAME#, [B]#TASKS_COUNT# de suas tarefas [/B] contém observações ou precisam de uma resposta.
[URL=#HREF#]Vá para Tarefas para visualizá-las[/URL].";
$MESS["TASKS_COUNTERS_NOTICE_CONTENT_PLURAL_2"] = "#USERNAME#, [B]#TASKS_COUNT# de suas tarefas [/B] contém observações ou precisam de uma resposta.
[URL=#HREF#]Vá para Tarefas para visualizá-las[/URL].";
?>