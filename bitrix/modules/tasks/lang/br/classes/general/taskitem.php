<?
$MESS["TASK_CANT_ADD_LINK"] = "Não é possível vincular tarefas";
$MESS["TASK_CANT_DELETE_LINK"] = "Não é possível desvincular tarefas";
$MESS["TASKS_HAS_PARENT_RELATION"] = "Não é possível vincular uma tarefa à sua tarefa primária";
$MESS["TASKS_TRIAL_PERIOD_EXPIRED"] = "Seu período de teste expirou";
$MESS["TASK_CANT_UPDATE_LINK"] = "Não é possível atualizar o link da tarefa";
?>