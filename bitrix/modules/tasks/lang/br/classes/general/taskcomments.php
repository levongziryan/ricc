<?
$MESS["TASKS_COMMENT_MESSAGE_ADD"] = "Adicionou um comentário à tarefa \"#TASK_TITLE#\", texto do comentário: \"#TASK_COMMENT_TEXT#\"";
$MESS["TASKS_COMMENT_MESSAGE_EDIT"] = "Alterou um comentário para \"#TASK_TITLE#\". O novo texto é: \"#TASK_COMMENT_TEXT#\".";
$MESS["TASKS_COMMENT_MESSAGE_ADD_F"] = "Adicionou um comentário à tarefa \"#TASK_TITLE#\". O texto do comentário é: \"#TASK_COMMENT_TEXT#\".";
$MESS["TASKS_COMMENT_MESSAGE_EDIT_F"] = "Atualizou um comentário para \"#TASK_TITLE#\". O novo texto é: \"#TASK_COMMENT_TEXT#\".";
$MESS["TASKS_COMMENT_MESSAGE_ADD_M"] = "Adicionou um comentário à tarefa \"#TASK_TITLE#\", texto do comentário: \"#TASK_COMMENT_TEXT#\"";
$MESS["TASKS_COMMENT_MESSAGE_EDIT_M"] = "Alterou um comentário para \"#TASK_TITLE#\". O novo texto é: \"#TASK_COMMENT_TEXT#\".";
$MESS["TASKS_COMMENT_SONET_NEW_TASK_MESSAGE"] = "Tarefa criada";
?>