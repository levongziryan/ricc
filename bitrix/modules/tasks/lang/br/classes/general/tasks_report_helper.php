<?
$MESS["REPORT_TASKS_COLUMN_TREE_STATUS_SUB"] = "Status";
$MESS["REPORT_TASKS_DURATION_FOR_PERIOD"] = "Tempo gasto (Relatório de tempo)";
$MESS["TASKS_REPORT_DEFAULT_2"] = "Envolvimento em projetos";
$MESS["TASKS_REPORT_DEFAULT_3"] = "Tarefas desse mês";
$MESS["TASKS_REPORT_DEFAULT_4"] = "Relatório de eficiência";
$MESS["TASKS_REPORT_EFF_EMPLOYEE"] = "Colaborador";
$MESS["TASKS_REPORT_EFF_NEW"] = "Novo";
$MESS["TASKS_REPORT_EFF_OPEN"] = "Abrir";
$MESS["TASKS_REPORT_EFF_CLOSED"] = "Fechado";
$MESS["TASKS_REPORT_EFF_OVERDUE"] = "Atrasado";
$MESS["TASKS_REPORT_EFF_MARKED"] = "Avaliação";
$MESS["TASKS_REPORT_EFF_EFFICIENCY"] = "Eficiência";
$MESS["TASKS_REPORT_DEFAULT_5"] = "Tarefas do mês passado";
$MESS["TASKS_REPORT_DURATION_DAYS"] = "d.";
$MESS["TASKS_REPORT_DURATION_HOURS"] = "hr.";
$MESS["TASKS_REPORT_DEFAULT_6"] = "Rastreamento de recursos Tarefa ";
$MESS["TASKS_REPORT_DEFAULT_6_DESCR"] = "Este relatório mostra tarefas feitas por um projeto, departamento específico ou toda a empresa para o período de relatório eo tempo gasto. ";
$MESS["TASKS_REPORT_DEFAULT_7_DESCR"] = "Mostra o tempo que levou para cada funcionário para completar as tarefas de rastreamento de recursos habilitados. O relatório pode ser discriminado por projetos.";
$MESS["TASKS_REPORT_DEFAULT_7_ALIAS_4"] = "Número de tarefas ";
$MESS["TASKS_REPORT_DEFAULT_7_ALIAS_6"] = "total gasto ";
$MESS["TASKS_REPORT_DEFAULT_7_ALIAS_8"] = "Passou por período de relatório ";
$MESS["TASKS_REPORT_DEFAULT_7_ALIAS_10"] = "Custos de trabalho planejadas";
$MESS["TASKS_REPORT_DEFAULT_7"] = "Rastreamento de recursos empregado ";
$MESS["REPORT_TASKS_Tag:TASK.NAME"] = "Tags";
$MESS["TASKS_REPORT_UF_TASK_WEBDAV_FILES"] = "Documentos";
$MESS["REPORT_TASKS_Member:TASK_COWORKED.USER"] = "Participantes";
$MESS["REPORT_TASKS_TAGS"] = "Marcadores";
?>