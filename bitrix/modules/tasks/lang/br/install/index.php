<?
$MESS["TASKS_MODULE_NAME"] = "Tarefas";
$MESS["TASKS_MODULE_DESC"] = "Módulo de gerenciamento de tarefas";
$MESS["TASKS_INSTALL_COPY_PUBLIC"] = "Copiar Scripts de seção pública";
$MESS["TASKS_INSTALL_COMPLETE_OK"] = "A instalação foi concluída";
$MESS["TASKS_INSTALL_COMPLETE_ERROR"] = "A instalação foi concluída com erros";
$MESS["TASKS_INSTALL_PUBLIC_SETUP"] = "Instalar";
$MESS["TASKS_INSTALL_TITLE"] = "Instalar o módulo de gerenciamento de tarefas";
$MESS["TASKS_UNINSTALL_TITLE"] = "Desinstalar o módulo de gerenciamento de tarefas";
$MESS["TASKS_UNINSTALL_WARNING"] = "Atenção! Esse módulo será removido do sistema";
$MESS["TASKS_UNINSTALL_SAVEDATA"] = "Para salvar os dados armazenados nas tabelas de banco de dados,verifique as &quot;Tabelas&quot; box";
$MESS["TASKS_UNINSTALL_SAVETABLE"] = "Salvar Tabelas";
$MESS["TASKS_UNINSTALL_DEL"] = "Excluir";
$MESS["TASKS_UNINSTALL_ERROR"] = "Desinstalação de erros:";
$MESS["TASKS_UNINSTALL_COMPLETE"] = "A desinstalação foi concluída";
$MESS["TASKS_INSTALL_BACK"] = "Voltar para o gerenciador de módulos";
$MESS["COPY_PUBLIC_FILES"] = "Copiar arquivos públicos";
$MESS["TASKS_CONVERT_AND_USE_NEW"] = "Converter tarefas e começar a usar o Task 2.0";
?>