<?
$MESS["TASKS_DATE_START"] = "Data de início";
$MESS["TASKS_DATE_END"] = "Data de término";
$MESS["TASKS_DATE_STARTED"] = "Iniciada em";
$MESS["TASKS_DATE_COMPLETED"] = "Concluída em";
$MESS["TASKS_DATE_CREATED"] = "Criada em";
$MESS["TASKS_DATE_DEADLINE"] = "Prazo";
$MESS["TASKS_TASK_TITLE_LABEL"] = "Tarefa n°";
$MESS["TASKS_STATUS"] = "Status";
$MESS["TASKS_STATUS_OVERDUE"] = "Atrasada";
$MESS["TASKS_STATUS_NEW"] = "Nova";
$MESS["TASKS_STATUS_ACCEPTED"] = "Pendente";
$MESS["TASKS_STATUS_IN_PROGRESS"] = "Em andamento";
$MESS["TASKS_STATUS_WAITING"] = "Dependendo de revisão";
$MESS["TASKS_STATUS_COMPLETED"] = "Concluída";
$MESS["TASKS_STATUS_DELAYED"] = "Adiada";
$MESS["TASKS_STATUS_DECLINED"] = "Recusada";
$MESS["TASKS_PRIORITY"] = "Prioridade";
$MESS["TASKS_PRIORITY_0"] = "Baixa";
$MESS["TASKS_PRIORITY_1"] = "Normal";
$MESS["TASKS_PRIORITY_2"] = "Alta";
$MESS["TASKS_QUICK_INFO_DETAILS"] = "Detalhes";
$MESS["TASKS_QUICK_INFO_EMPTY_DATE"] = "não";
$MESS["TASKS_RESPONSIBLE"] = "Responsável";
$MESS["TASKS_DIRECTOR"] = "Criador";
$MESS["TASKS_FILES"] = "Arquivos";
$MESS["TASKS_PRIORITY_V2"] = "Tarefa importante";
?>