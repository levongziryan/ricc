<?
$MESS["TASKS_GANTT_EMPTY_DATE"] = "nenhuma";
$MESS["TASKS_GANTT_CHART_TITLE"] = "Tarefas";
$MESS["TASKS_GANTT_DATE_START"] = "Data de início";
$MESS["TASKS_GANTT_DATE_END"] = "Data de término";
$MESS["TASKS_GANTT_DEADLINE"] = "Prazo";
$MESS["TASKS_GANTT_START"] = "início";
$MESS["TASKS_GANTT_END"] = "fim";
$MESS["TASKS_GANTT_DELETE_DEPENDENCY"] = "Excluir";
$MESS["TASKS_GANTT_EMPTY_END_DATE"] = "A data de término é obrigatória para estabelecer um link";
$MESS["TASKS_GANTT_CIRCULAR_DEPENDENCY"] = "Links circulares não são permitidos";
$MESS["TASKS_GANTT_PERMISSION_ERROR"] = "Você não tem permissão para editar datas desta tarefa";
$MESS["TASKS_GANTT_DEPENDENCY_FROM"] = "De";
$MESS["TASKS_GANTT_DEPENDENCY_TO"] = "Para";
$MESS["TASKS_GANTT_RELATION_ERROR"] = "Não é possível vincular uma tarefa à sua tarefa principal";
$MESS["TASKS_GANTT_INDENT_TASK"] = "Tarefa Aumentada";
$MESS["TASKS_GANTT_OUTDENT_TASK"] = "Tarefa Recuada";
?>