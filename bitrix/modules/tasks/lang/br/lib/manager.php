<?
$MESS["TASKS_MANAGER_TASK_ITEM_NAME"] = "elemento dependente";
$MESS["TASKS_MANAGER_TASK_CANT_UPDATE"] = "Permissão insuficiente para editar #ITEM_NAME# ID=#ID#";
$MESS["TASKS_MANAGER_TASK_CANT_DELETE"] = "Permissão insuficiente para excluir #ITEM_NAME# ID=#ID#";
?>