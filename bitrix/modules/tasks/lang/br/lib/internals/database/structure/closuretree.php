<?
$MESS["TASKS_CLOSURE_TREE_LINK_EXISTS"] = "Nós já vinculados";
$MESS["TASKS_CLOSURE_TREE_NODE_NOT_FOUND"] = "O nó especificado não foi encontrado.";
$MESS["TASKS_CLOSURE_TREE_NODE_EXISTS_BUT_DECLARED_NEW"] = "O nó especificado existe, mas será adicionado como novo";
$MESS["TASKS_CLOSURE_TREE_CANT_ATTACH_TO_SELF"] = "Não é possível vincular o nó a si mesmo";
$MESS["TASKS_CLOSURE_TREE_ILLEGAL_NODE"] = "ID do nó incorreto";
$MESS["TASKS_CLOSURE_TREE_CANT_ATTACH_TO_ANCESTOR"] = "Não é possível vincular o nó ao seu ancestral";
?>