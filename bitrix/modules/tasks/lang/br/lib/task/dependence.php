<?
$MESS["DEPENDENCE_ENTITY_TASK_ID_FIELD"] = "Tarefa";
$MESS["DEPENDENCE_ENTITY_DEPENDS_ON_ID_FIELD"] = "Tarefa primária";
$MESS["DEPENDENCE_ENTITY_TYPE_FIELD"] = "Tipo de link";
$MESS["DEPENDENCE_ENTITY_CANT_ADD_LINK_CREATED_DATE_NOT_SET"] = "A tarefa a ser vinculada não tem data de criação";
$MESS["DEPENDENCE_ENTITY_CANT_ADD_LINK_CREATED_DATE_NOT_SET_PARENT_TASK"] = "A tarefa primária não tem data de criação";
$MESS["DEPENDENCE_ENTITY_CANT_ADD_LINK_END_DATE_PLAN_NOT_SET"] = "A tarefa a ser vinculada não tem data de término planejada";
$MESS["DEPENDENCE_ENTITY_CANT_ADD_LINK_END_DATE_PLAN_NOT_SET_PARENT_TASK"] = "A tarefa primária não tem data de término planejada";
?>