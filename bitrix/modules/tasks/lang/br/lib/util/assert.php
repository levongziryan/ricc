<?
$MESS["TASKS_ASSERT_INTEGER_EXPECTED"] = "O argumento#ARG_NAME#deve ser um número inteiro.";
$MESS["TASKS_ASSERT_INTEGER_NOTNULL_EXPECTED"] = "O argumento#ARG_NAME#deve ser um número inteiro positivo.";
$MESS["TASKS_ASSERT_ARRAY_NOT_EMPTY_EXPECTED"] = "O argumento#ARG_NAME#deve ser uma matriz não vazia.";
$MESS["TASKS_ASSERT_ARRAY_EXPECTED"] = "O argumento#ARG_NAME#deve ser uma matriz.";
$MESS["TASKS_ASSERT_STRING_NOTNULL_EXPECTED"] = "O argumento#ARG_NAME#deve ser uma cadeia não vazia.";
$MESS["TASKS_ASSERT_ARRAY_OF_INTEGER_NOT_NULL_EXPECTED"] = "O argumento#ARG_NAME#não é uma matriz de números inteiros positivos.";
$MESS["TASKS_ASSERT_ARRAY_OF_STRING_NOT_NULL_EXPECTED"] = "O argumento#ARG_NAME#não é uma matriz de cadeias não vazias.";
$MESS["TASKS_ASSERT_EMPTY_ENUMERATION"] = "Um enum vazio aprovado para o método de verificação.";
$MESS["TASKS_ASSERT_ITEM_NOT_IN_ENUMERATION"] = "O argumento#ARG_NAME#não é um dos valores enum.";
$MESS["TASKS_ASSERT_EMPTY_ARGUMENT"] = "Argumento vazio aprovado.";
$MESS["TASKS_ASSERT_INTEGER_NONNEGATIVE_EXPECTED"] = "O argumento#ARG_NAME#deve ser um inteiro não negativo.";
?>