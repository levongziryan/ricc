<?
$MESS["TASKS_ITEM_TASK_TEMPLATE_FIELD_SE_CHECKLIST"] = "Lista de verificação";
$MESS["TASKS_ITEM_TASK_TEMPLATE_FIELD_SE_ACCESS"] = "Permissões de acesso";
$MESS["TASKS_ITEM_TASK_TEMPLATE_FIELD_SE_TAG"] = "Marcadores";
$MESS["TASKS_ITEM_TASK_TEMPLATE_FIELD_BASE_TEMPLATE_ID"] = "Modelo geral";
$MESS["TASKS_ITEM_TASK_TEMPLATE_CANT_SWITCH_TYPE_ERROR"] = "Não é possível alterar o tipo de um modelo existente";
$MESS["TASKS_ITEM_TASK_TEMPLATE_PARENT_ITEM_CONFLICT_ERROR"] = "Não é possível definir o modelo geral e a tarefa básica simultaneamente";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_MULTITASKING_ALLOWED_ERROR_BECAUSE_TYPE_1"] = "Não é possível criar um modelo de várias pessoas responsáveis para o novo usuário.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_BASE_TEMPLATE_ALLOWED_ERROR_BECAUSE_TYPE_1"] = "Não é possível atribuir um modelo básico para o novo modelo de usuário.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_BASE_TEMPLATE_ALLOWED_ERROR_BECAUSE_REPLICATION"] = "Não é possível atribuir o modelo básico porque a execução recorrente está habilitada.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_BASE_TEMPLATE_ALLOWED_ERROR_BECAUSE_MULTITASK"] = "Não é possível atribuir o modelo básico se forem especificados vários responsáveis.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_REPLICATION_ALLOWED_ERROR_BECAUSE_TYPE_1"] = "Não é possível definir parâmetros de execução recorrentes ao criar um novo modelo de usuário.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_REPLICATION_ALLOWED_ERROR_BASE_TEMPLATE_ID"] = "Não é possível definir parâmetros de execução recorrentes ao atribuir o modelo básico.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_TPARAM_TYPE_1_ALLOWED_ERROR_BECAUSE_REPLICATION"] = "Não é possível criar novo modelo de usuário porque a execução recorrente está habilitada.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_TPARAM_TYPE_1_ALLOWED_ERROR_BASE_TEMPLATE_ID"] = "Não é possível criar novo modelo de usuário ao atribuir o modelo básico.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_TPARAM_TYPE_1_ALLOWED_ERROR_BECAUSE_MULTITASK"] = "Não é possível criar novo modelo de usuário porque a execução recorrente está habilitada.";
?>