<?
$MESS["TASKS_ASSET_QUERY_QUERY_FAILED"] = "Erro ao processar o pedido..";
$MESS["TASKS_ASSET_QUERY_QUERY_FAILED_STATUS"] = "Erro ao processar o pedido.. (HTTP #HTTP_STATUS#)";
$MESS["TASKS_ASSET_QUERY_ILLEGAL_RESPONSE"] = "Erro de análise de resposta do servidor.";
$MESS["TASKS_ASSET_QUERY_QUERY_FAILED_EXCEPTION"] = "Erro de análise de resposta do servidor.";
$MESS["TASKS_ASSET_QUERY_EMPTY_RESPONSE"] = "Resposta do servidor vazia.";
?>