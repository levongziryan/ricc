<?
$MESS["JS_CORE_PL_TASKS"] = "Tarefas de hoje";
$MESS["JS_CORE_PL_TASKS_CHOOSE"] = "Selecione da lista";
$MESS["JS_CORE_PL_TASKS_ADD"] = "digite aqui suas tarefas";
$MESS["JS_CORE_PL_TASKS_START_TIMER"] = "Iniciar contador";
$MESS["JS_CORE_PL_TASKS_STOP_TIMER"] = "Pausar contador";
$MESS["JS_CORE_PL_TASKS_FINISH"] = "Finalizar tarefa e concluir registro de tempo";
$MESS["JS_CORE_PL_TASKS_MENU_REMOVE_FROM_PLAN"] = "Remover do planejamento diário";
$MESS["JS_CORE_PL_TASKS_CREATE"] = "Nova tarefa";
?>