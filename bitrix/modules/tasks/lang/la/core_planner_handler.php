<?
$MESS["JS_CORE_PL_TASKS"] = "Tareas de hoy";
$MESS["JS_CORE_PL_TASKS_CHOOSE"] = "seleccionar de la lista";
$MESS["JS_CORE_PL_TASKS_ADD"] = "introducir una nueva tarea";
$MESS["JS_CORE_PL_TASKS_START_TIMER"] = "Iniciar control del tiempo";
$MESS["JS_CORE_PL_TASKS_STOP_TIMER"] = "Pausar el temporizador";
$MESS["JS_CORE_PL_TASKS_FINISH"] = "Terminar tarea y detener controlador del tiempo";
$MESS["JS_CORE_PL_TASKS_MENU_REMOVE_FROM_PLAN"] = "Eliminar del plan diario";
$MESS["JS_CORE_PL_TASKS_CREATE"] = "Nueva tarea";
?>