<?
$MESS["TASKS_DATE_START"] = "Fecha de inicio";
$MESS["TASKS_DATE_END"] = "Fecha de finalización";
$MESS["TASKS_DATE_STARTED"] = "Iniciada el";
$MESS["TASKS_DATE_COMPLETED"] = "Completada el";
$MESS["TASKS_DATE_CREATED"] = "Creada el ";
$MESS["TASKS_DATE_DEADLINE"] = "Fecha límite";
$MESS["TASKS_TASK_TITLE_LABEL"] = "Nro. de Tarea";
$MESS["TASKS_STATUS"] = "Estados";
$MESS["TASKS_STATUS_OVERDUE"] = "Atrasada";
$MESS["TASKS_STATUS_NEW"] = "Nueva";
$MESS["TASKS_STATUS_ACCEPTED"] = "Pendiente";
$MESS["TASKS_STATUS_IN_PROGRESS"] = "En progreso";
$MESS["TASKS_STATUS_WAITING"] = "En espera de control";
$MESS["TASKS_STATUS_COMPLETED"] = "Completada";
$MESS["TASKS_STATUS_DELAYED"] = "Diferida";
$MESS["TASKS_STATUS_DECLINED"] = "Declinada";
$MESS["TASKS_QUICK_INFO_DETAILS"] = "Detalles";
$MESS["TASKS_QUICK_INFO_EMPTY_DATE"] = "no";
$MESS["TASKS_RESPONSIBLE"] = "Responsable";
$MESS["TASKS_DIRECTOR"] = "Creador";
$MESS["TASKS_FILES"] = "Archivos";
$MESS["TASKS_PRIORITY_V2"] = "Tarea importante";
?>