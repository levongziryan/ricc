<?
$MESS["TASKS_KANBAN_STATUS_NEW"] = "Nuevo";
$MESS["TASKS_KANBAN_STATUS_PAUSE"] = "En pausa";
$MESS["TASKS_KANBAN_STATUS_PROGRESS"] = "En progreso";
$MESS["TASKS_KANBAN_STATUS_DEFERRED"] = "Diferido";
$MESS["TASKS_KANBAN_STATUS_COMPLETED"] = "Terminado";
$MESS["TASKS_KANBAN_STATUS_COMPLETED_SUPPOSEDLY"] = "Revisión pendiente";
$MESS["TASKS_KANBAN_STATUS_OVERDUE"] = "Atrasado";
$MESS["TASKS_KANBAN_TITLE_COMMENTS"] = "Comentarios (#count#)";
$MESS["TASKS_KANBAN_TITLE_CHECKLIST"] = "Checklist (listo #complete# of #all#)";
$MESS["TASKS_KANBAN_TITLE_FILES"] = "Archivos (#count#)";
$MESS["TASKS_KANBAN_TITLE_DEADLINE"] = "Fecha límite";
$MESS["TASKS_KANBAN_TITLE_DEADLINE_SET"] = "Establecer fecha límite";
$MESS["TASKS_KANBAN_MORE_USERS"] = "más";
$MESS["TASKS_KANBAN_TITLE_START"] = "Iniciar tarea";
$MESS["TASKS_KANBAN_TITLE_PAUSE"] = "Pausar tarea";
$MESS["TASKS_KANBAN_TITLE_COMPLETE"] = "Tarea completa";
$MESS["TASKS_KANBAN_WO_GROUP_LABEL"] = "Grupo (proyecto)";
$MESS["TASKS_KANBAN_WO_GROUP_VALUE"] = "ninguno";
$MESS["TASKS_TASK_CONFIRM_START_TIMER"] = "Ya está utilizando el seguimiento de tiempo para \"{{TITLE}}\". Esta tarea se detendrá. ¿Desea continuar?";
$MESS["TASKS_TASK_CONFIRM_START_TIMER_TITLE"] = "Ya está utilizando el seguimiento de tiempo para \"{{TITLE}}\". Esta tarea se detendrá. ¿Desea continuar?";
$MESS["TASKS_KANBAN_ME_DISABLE_DEADLINE"] = "Las fechas de las tareas no se pueden cambiar sin cambiar la fecha límite";
$MESS["TASKS_KANBAN_ME_DISABLE_DEADLINE_PART"] = "La fecha límite de la tarea restringe el posible rango de fechas
";
$MESS["TASKS_KANBAN_ME_DISABLE_FROM_OVERDUE"] = "Cambiar la fecha límite de la tarea o cerrarla antes de moverla";
$MESS["TASKS_KANBAN_ME_DISABLE_COMPLETE"] = "La tarea ha sido cerrada. Continúe para moverlo a otra columna.";
$MESS["TASKS_KANBAN_NOTIFY_TITLE"] = "Permisos insuficientes";
$MESS["TASKS_KANBAN_NOTIFY_HEADER"] = "Permisos insuficientes para editar etapas.";
$MESS["TASKS_KANBAN_NOTIFY_BUTTON"] = "Enviar mensaje";
$MESS["TASKS_KANBAN_DIABLE_SORT_TOOLTIP"] = "No tiene permisos para editar las preferencias del proyecto";
$MESS["MAIN_KANBAN_TITLE_PLACEHOLDER"] = "Nombre #tag";
$MESS["TASKS_KANBAN_NOTIFY_TEXT"] = "Sólo el administrador de Bitrix24's o administrador del  proyecto puede crear una nueva etapa. Por favor, envíe un mensaje o hable con ellos para agregar las etapas de la tarea que necesita.";
?>