<?
$MESS["TASKS_ITEM_TASK_TEMPLATE_FIELD_SE_CHECKLIST"] = "Lista de verificación";
$MESS["TASKS_ITEM_TASK_TEMPLATE_FIELD_SE_ACCESS"] = "Permisos de acceso";
$MESS["TASKS_ITEM_TASK_TEMPLATE_FIELD_SE_TAG"] = "Etiquetas";
$MESS["TASKS_ITEM_TASK_TEMPLATE_FIELD_BASE_TEMPLATE_ID"] = "Plantilla general";
$MESS["TASKS_ITEM_TASK_TEMPLATE_CANT_SWITCH_TYPE_ERROR"] = "No se puede cambiar el tipo de una plantilla existente";
$MESS["TASKS_ITEM_TASK_TEMPLATE_PARENT_ITEM_CONFLICT_ERROR"] = "No se puede establecer la plantilla general y la tarea base simultáneamente";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_MULTITASKING_ALLOWED_ERROR_BECAUSE_TYPE_1"] = "No se puede crear una plantilla de personas responsables múltiples para el nuevo usuario.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_BASE_TEMPLATE_ALLOWED_ERROR_BECAUSE_TYPE_1"] = "No se puede asignar una plantilla de base de la nueva plantilla de usuario.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_BASE_TEMPLATE_ALLOWED_ERROR_BECAUSE_REPLICATION"] = "No se puede asignar la plantilla base porque está habilitada la ejecución repetitiva.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_BASE_TEMPLATE_ALLOWED_ERROR_BECAUSE_MULTITASK"] = "No se puede asignar la plantilla base si se especifican varias personas responsables.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_REPLICATION_ALLOWED_ERROR_BECAUSE_TYPE_1"] = "No se pueden establecer parámetros de ejecución recurrente al crear una nueva plantilla de usuario.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_REPLICATION_ALLOWED_ERROR_BASE_TEMPLATE_ID"] = "No se pueden establecer los parámetros de ejecución recurrente al asignar la plantilla base.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_TPARAM_TYPE_1_ALLOWED_ERROR_BECAUSE_REPLICATION"] = "No se puede crear una plantilla de usuario porque está habilitada la ejecución repetitiva.
";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_TPARAM_TYPE_1_ALLOWED_ERROR_BASE_TEMPLATE_ID"] = "No se puede crear una nueva plantilla de usuario cuando se asigna la plantilla base.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_TPARAM_TYPE_1_ALLOWED_ERROR_BECAUSE_MULTITASK"] = "No se puede crear una plantilla de usuario porque está habilitada la ejecución repetitiva.
";
?>