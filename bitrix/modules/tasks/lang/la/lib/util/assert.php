<?
$MESS["TASKS_ASSERT_INTEGER_EXPECTED"] = "El argumento#ARG_NAME#debe ser un entero.";
$MESS["TASKS_ASSERT_INTEGER_NOTNULL_EXPECTED"] = "El argumento#ARG_NAME#debe ser un número entero positivo.";
$MESS["TASKS_ASSERT_ARRAY_NOT_EMPTY_EXPECTED"] = "El argumento#ARG_NAME#debe ser un array vacía.";
$MESS["TASKS_ASSERT_ARRAY_EXPECTED"] = "El argumento#ARG_NAME#debe ser un array.";
$MESS["TASKS_ASSERT_STRING_NOTNULL_EXPECTED"] = "El argumento#ARG_NAME#debe ser una cadena no vacía.";
$MESS["TASKS_ASSERT_ARRAY_OF_INTEGER_NOT_NULL_EXPECTED"] = "El argumento#ARG_NAME#no es un array de números enteros positivos.";
$MESS["TASKS_ASSERT_ARRAY_OF_STRING_NOT_NULL_EXPECTED"] = "El argumento#ARG_NAME#No es un array de cadenas vacías.";
$MESS["TASKS_ASSERT_EMPTY_ENUMERATION"] = "Una enumeración vacía pasa al método de verificación.";
$MESS["TASKS_ASSERT_ITEM_NOT_IN_ENUMERATION"] = "El argumento#ARG_NAME#no es uno de los valores de la enumeración.";
$MESS["TASKS_ASSERT_EMPTY_ARGUMENT"] = "Un argumento vacio pasó.";
$MESS["TASKS_ASSERT_INTEGER_NONNEGATIVE_EXPECTED"] = "El argumento#ARG_NAME#debe ser un entero no negativo.";
?>