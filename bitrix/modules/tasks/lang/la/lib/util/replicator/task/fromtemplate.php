<?
$MESS["TASKS_REPLICATOR_TASK_CREATED"] = "Se ha creado la tarea programada";
$MESS["TASKS_REPLICATOR_TASK_CREATED_WITH_ERRORS"] = "Se ha creado la tarea programada, hubo errores";
$MESS["TASKS_REPLICATOR_TASK_WAS_NOT_CREATED"] = "No se ha creado la tarea programada";
$MESS["TASKS_REPLICATOR_TASK_POSSIBLY_WAS_NOT_CREATED"] = "Es posible que no se haya creado la tarea programada";
$MESS["TASKS_REPLICATOR_PROCESS_STOPPED"] = "La interacción con la tarea se detuvo";
$MESS["TASKS_REPLICATOR_END_DATE_REACHED"] = "Fecha de finalización alcanzada";
$MESS["TASKS_REPLICATOR_LIMIT_REACHED"] = "Máximo de interacciones alcanzadas";
$MESS["TASKS_REPLICATOR_ILLEGAL_NEXT_TIME"] = "No se pudo calcular la hora de la próxima ejecución";
$MESS["TASKS_REPLICATOR_PROCESS_ERROR"] = "Error al calcular la hora de la próxima ejecución";
$MESS["TASKS_REPLICATOR_NEXT_TIME"] = "Siguiente ejecución programada en: #TIME# (en #PERIOD# segundos)";
$MESS["TASKS_REPLICATOR_SUBTREE_LOOP"] = "Es posible que algunas de las subplantillas sean referidas. No se han creado las subtareas.";
$MESS["TASKS_REPLICATOR_INTERNAL_ERROR"] = "Error interno. Póngase en contacto con Helpdesk.";
$MESS["TASKS_REPLICATOR_CANT_IDENTIFY_USER"] = "No se puede definir un usuario para ejecutar la tarea como. Compruebe si la cuenta de administrador está activa.";
$MESS["TASKS_REPLICATOR_SECOND_0"] = "segundo";
$MESS["TASKS_REPLICATOR_SECOND_1"] = "segundos";
$MESS["TASKS_REPLICATOR_SECOND_2"] = "segundos";
$MESS["TASKS_REPLICATOR_CREATOR_INACTIVE"] = "La cuenta del creador de tareas no existe o está inactiva.";
?>