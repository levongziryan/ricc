<?
$MESS["TASKS_SORTING_AUTH_REQUIRED"] = "Tiene que estar autorizado para realizar esta operación.";
$MESS["TASKS_SORTING_WRONG_SOURCE_TASK"] = "No se ha encontrado la tarea de origen";
$MESS["TASKS_SORTING_WRONG_TARGET_TASK"] = "No se ha encontrado tarea consecutiva";
$MESS["TASKS_SORTING_WRONG_GROUP_PERMISSIONS"] = "No se puede ordenar las tareas de este grupo";
?>