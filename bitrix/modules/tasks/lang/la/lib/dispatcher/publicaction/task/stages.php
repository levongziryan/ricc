<?
$MESS["STAGES_ERROR_ACCESS_DENIED_GROUP_TASK"] = "No puede ver las tareas de este grupo";
$MESS["STAGES_ERROR_ACCESS_DENIED_STAGES"] = "No se pueden gestionar etapas";
$MESS["STAGES_ERROR_ACCESS_DENIED_MOVE"] = "No se puede mover esta tarea";
$MESS["STAGES_ERROR_DIFFERENT_STAGES"] = "Etapa y tarea pertenecen a diferentes grupos";
$MESS["STAGES_ERROR_EMPTY_TITLE"] = "Falta el título de la etapa";
$MESS["STAGES_ERROR_NOT_FOUND"] = "No se encontró la etapa";
$MESS["STAGES_ERROR_EMPTY_DATA"] = "No se proporcionaron datos de actualización";
$MESS["STAGES_ERROR_NO_EMPTY"] = "Hay tareas en esta etapa";
$MESS["STAGES_ERROR_IS_SYSTEM"] = "No se puede eliminar la etapa predeterminada";
$MESS["STAGES_ERROR_SOCIALNETWORK_IS_NOT_INSTALLED"] = "el módulo social network no está instalado.";
$MESS["STAGES_ERROR_TASK_NOT_FOUND"] = "La tarea no se ha encontrado o se ha denegado el acceso.";
?>