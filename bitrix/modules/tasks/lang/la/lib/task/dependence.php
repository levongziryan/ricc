<?
$MESS["DEPENDENCE_ENTITY_TASK_ID_FIELD"] = "Tareas";
$MESS["DEPENDENCE_ENTITY_DEPENDS_ON_ID_FIELD"] = "Tarea principal";
$MESS["DEPENDENCE_ENTITY_TYPE_FIELD"] = "Tipo de enlace";
$MESS["DEPENDENCE_ENTITY_CANT_ADD_LINK_CREATED_DATE_NOT_SET"] = "La tarea a vincular no tiene fecha de creación";
$MESS["DEPENDENCE_ENTITY_CANT_ADD_LINK_CREATED_DATE_NOT_SET_PARENT_TASK"] = "La tarea principal no tiene fecha de creación";
$MESS["DEPENDENCE_ENTITY_CANT_ADD_LINK_END_DATE_PLAN_NOT_SET"] = "La tarea a vincular no tiene fecha de finalización planificada";
$MESS["DEPENDENCE_ENTITY_CANT_ADD_LINK_END_DATE_PLAN_NOT_SET_PARENT_TASK"] = "La tarea principal no tiene fecha de finalización planificada";
?>