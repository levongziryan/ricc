<?
$MESS["TASKS_CLOSURE_TREE_LINK_EXISTS"] = "Nodes ya vinculados";
$MESS["TASKS_CLOSURE_TREE_NODE_NOT_FOUND"] = "No se encontró el node especificado.";
$MESS["TASKS_CLOSURE_TREE_NODE_EXISTS_BUT_DECLARED_NEW"] = "El node especificado existe pero se agregará como nuevo";
$MESS["TASKS_CLOSURE_TREE_CANT_ATTACH_TO_SELF"] = "No se puede vincular el node a sí mismo";
$MESS["TASKS_CLOSURE_TREE_ILLEGAL_NODE"] = "ID de node incorrecto";
$MESS["TASKS_CLOSURE_TREE_CANT_ATTACH_TO_ANCESTOR"] = "No se puede vincular el node a su antecesor";
?>