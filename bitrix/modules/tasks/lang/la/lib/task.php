<?
$MESS["TASKS_TASK_ENTITY_ID_FIELD"] = "ID";
$MESS["TASKS_TASK_ENTITY_TITLE_FIELD"] = "Nombre";
$MESS["TASKS_TASK_ENTITY_PRIORITY_FIELD"] = "Prioridad";
$MESS["TASKS_TASK_ENTITY_RESPONSIBLE_FIELD"] = "Persona responsable";
$MESS["TASKS_TASK_ENTITY_START_DATE_PLAN_FIELD"] = "Fecha de inicio del proyecto";
$MESS["TASKS_TASK_ENTITY_END_DATE_PLAN_FIELD"] = "Fecha de fin del proyecto";
$MESS["TASKS_TASK_ENTITY_DATE_START_FIELD"] = "Fecha de inicio";
$MESS["TASKS_TASK_ENTITY_DEADLINE_FIELD"] = "Fecha límite";
$MESS["TASKS_TASK_ENTITY_CREATED_BY_USER_FIELD"] = "Creado por";
$MESS["TASKS_TASK_ENTITY_CREATED_DATE_FIELD"] = "Creado en";
$MESS["TASKS_TASK_ENTITY_CHANGED_BY_USER_FIELD"] = "La última modificación";
$MESS["TASKS_TASK_ENTITY_CHANGED_DATE_FIELD"] = "Modificada por última vez";
$MESS["TASKS_TASK_ENTITY_STATUS_CHANGED_BY_USER_FIELD"] = "Estado de la última modificación por";
$MESS["TASKS_TASK_ENTITY_STATUS_CHANGED_DATE_FIELD"] = "Estado de la última modificación en";
$MESS["TASKS_TASK_ENTITY_CLOSED_BY_USER_FIELD"] = "Completado por";
$MESS["TASKS_TASK_ENTITY_CLOSED_DATE_FIELD"] = "Completado en";
$MESS["TASKS_TASK_ENTITY_GROUP_FIELD"] = "Proyecto";
$MESS["TASKS_TASK_ENTITY_ADD_IN_REPORT_FIELD"] = "En el informe";
$MESS["TASKS_TASK_ENTITY_DURATION_FIELD"] = "Tiempo dedicado";
$MESS["TASKS_TASK_ENTITY_IS_OPEN_FIELD"] = "Abrir";
$MESS["TASKS_TASK_ENTITY_IS_NEW_FIELD"] = "Nuevo";
$MESS["TASKS_TASK_ENTITY_IS_OVERDUE_FIELD"] = "Vencidos";
$MESS["TASKS_TASK_ENTITY_IS_FINISHED_FIELD"] = "Completado";
$MESS["TASKS_TASK_ENTITY_IS_RUNNING_FIELD"] = "En progreso";
$MESS["TASKS_TASK_ENTITY_IS_MARKED_FIELD"] = "Evaluados";
$MESS["TASKS_TASK_ENTITY_IS_EFFECTIVE_FIELD"] = "Eficacia";
$MESS["TASKS_TASK_ENTITY_STATUS_FIELD_VALUE_1"] = "Nuevo (aún no aceptada)";
$MESS["TASKS_TASK_ENTITY_STATUS_FIELD_VALUE_2"] = "Aceptado";
$MESS["TASKS_TASK_ENTITY_STATUS_FIELD_VALUE_3"] = "En curso";
$MESS["TASKS_TASK_ENTITY_STATUS_FIELD_VALUE_4"] = "Supuestamente completado";
$MESS["TASKS_TASK_ENTITY_STATUS_FIELD_VALUE_5"] = "Completado";
$MESS["TASKS_TASK_ENTITY_STATUS_FIELD_VALUE_6"] = "Diferir";
$MESS["TASKS_TASK_ENTITY_STATUS_FIELD_VALUE_7"] = "Declinar";
$MESS["TASKS_TASK_ENTITY_PRIORITY_FIELD_VALUE_0"] = "Bajo";
$MESS["TASKS_TASK_ENTITY_PRIORITY_FIELD_VALUE_1"] = "Normal";
$MESS["TASKS_TASK_ENTITY_PRIORITY_FIELD_VALUE_2"] = "Alto";
$MESS["TASKS_TASK_ENTITY_MARK_FIELD"] = "Calificación";
$MESS["TASKS_TASK_ENTITY_MARK_FIELD_VALUE_P"] = "Positivo";
$MESS["TASKS_TASK_ENTITY_MARK_FIELD_VALUE_N"] = "Negativo";
$MESS["TASKS_TASK_ENTITY_MARK_FIELD_VALUE_NONE"] = "Ninguno";
$MESS["TASKS_TASK_ENTITY_STATUS_PSEUDO_FIELD"] = "Estado";
$MESS["TASKS_TASK_ENTITY_DURATION_PLAN_HOURS_FIELD"] = "Duración prevista";
$MESS["TASKS_TASK_ENTITY_STATUS_FIELD"] = "Estado (versión antigua)";
$MESS["TASKS_TASK_ENTITY_STATUS_PSEUDO_FIELD_VALUE_-1"] = "Atrasado";
$MESS["TASKS_TASK_ENTITY_STATUS_PSEUDO_FIELD_VALUE_1"] = "Nueva (todavía no aceptada)";
$MESS["TASKS_TASK_ENTITY_STATUS_PSEUDO_FIELD_VALUE_2"] = "Pendiente";
$MESS["TASKS_TASK_ENTITY_STATUS_PSEUDO_FIELD_VALUE_3"] = "En progreso";
$MESS["TASKS_TASK_ENTITY_STATUS_PSEUDO_FIELD_VALUE_4"] = "En espera de control";
$MESS["TASKS_TASK_ENTITY_STATUS_PSEUDO_FIELD_VALUE_5"] = "Completado";
$MESS["TASKS_TASK_ENTITY_STATUS_PSEUDO_FIELD_VALUE_6"] = "Diferir";
$MESS["TASKS_TASK_ENTITY_STATUS_PSEUDO_FIELD_VALUE_7"] = "Declinar";
$MESS["TASKS_TASK_ENTITY_DESCRIPTION_FIELD"] = "Descripción";
$MESS["TASKS_TASK_ENTITY_DESCRIPTION_TR_FIELD"] = "Descripción";
$MESS["TASKS_TASK_ENTITY_ALLOW_TIME_TRACKING_FIELD"] = "Control del tiempo empleado";
$MESS["TASKS_TASK_ENTITY_TIME_ESTIMATE_FIELD"] = "Tiempo estimado requerido";
?>