<?
$MESS["REPORT_TASKS_COLUMN_TREE_STATUS_SUB"] = "En estado";
$MESS["REPORT_TASKS_TAGS"] = "Etiquetas";
$MESS["TASKS_REPORT_DEFAULT_2"] = "Participación en proyectos";
$MESS["TASKS_REPORT_DEFAULT_3"] = "Tareas de este mes";
$MESS["TASKS_REPORT_DEFAULT_4"] = "Reportes de Eficiencia";
$MESS["TASKS_REPORT_EFF_EMPLOYEE"] = "Empleado";
$MESS["TASKS_REPORT_EFF_NEW"] = "Nuevo";
$MESS["TASKS_REPORT_EFF_OPEN"] = "Abierto";
$MESS["TASKS_REPORT_EFF_CLOSED"] = "Cerrado";
$MESS["TASKS_REPORT_EFF_OVERDUE"] = "Atrasados";
$MESS["TASKS_REPORT_EFF_MARKED"] = "Evaluación";
$MESS["TASKS_REPORT_EFF_EFFICIENCY"] = "Eficiencia";
$MESS["TASKS_REPORT_DEFAULT_5"] = "Tareas para el último mes";
$MESS["TASKS_REPORT_DURATION_DAYS"] = "d.";
$MESS["TASKS_REPORT_DURATION_HOURS"] = "hr.";
$MESS["TASKS_REPORT_DEFAULT_6"] = "Seguimiento de recurso de tareas";
$MESS["TASKS_REPORT_DEFAULT_6_DESCR"] = "Este reporte muestra las tareas realizadas por un proyecto, departamento o toda la compañía para el periodo de referencia y el tiempo empleado.";
$MESS["TASKS_REPORT_DEFAULT_7"] = "Seguimiento de recursos de empleados";
$MESS["TASKS_REPORT_DEFAULT_7_DESCR"] = "Muestra la cantidad de tiempo que se tomó cada empleado para completar las tareas activado el seguimiento de recursos. El reporte puede ser descompuesto por proyectos.";
$MESS["TASKS_REPORT_DEFAULT_7_ALIAS_4"] = "Número de tareas";
$MESS["TASKS_REPORT_DEFAULT_7_ALIAS_6"] = "Total invertido";
$MESS["TASKS_REPORT_DEFAULT_7_ALIAS_8"] = "Período dedicado en la presentación de reportes";
$MESS["TASKS_REPORT_DEFAULT_7_ALIAS_10"] = "Costos laborales planificados";
$MESS["TASKS_REPORT_UF_TASK_WEBDAV_FILES"] = "Documentos";
$MESS["REPORT_TASKS_DURATION_FOR_PERIOD"] = "Tiempo invertido (tiempo reportado)";
$MESS["REPORT_TASKS_Member:TASK_COWORKED.USER"] = "Participantes";
?>