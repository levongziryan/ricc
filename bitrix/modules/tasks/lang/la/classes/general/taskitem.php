<?
$MESS["TASK_CANT_ADD_LINK"] = "No se puede vincular tareas";
$MESS["TASK_CANT_DELETE_LINK"] = "No se puede desvincular las tareas";
$MESS["TASKS_HAS_PARENT_RELATION"] = "No se puede vincular una tarea a una tarea principal";
$MESS["TASKS_TRIAL_PERIOD_EXPIRED"] = "Su período de prueba ha caducado";
$MESS["TASK_CANT_UPDATE_LINK"] = "No puede actualizar vínculo de tarea";
?>