<?
$MESS["TASKS_PREVENT_WEBDAV_UNINSTALL"] = "Este módulo no se puede desinstalar ya que el módulo Tareas depende de él.";
$MESS["TASKS_PREVENT_FORUM_UNINSTALL"] = "Este módulo no se puede desinstalar ya que el módulo de Tareas depende de él.";
$MESS["TASKS_PREVENT_INTRANET_UNINSTALL"] = "Este módulo no se puede desinstalar ya que el módulo de Tareas depende de él.";
?>