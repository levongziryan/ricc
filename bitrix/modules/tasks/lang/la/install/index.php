<?
$MESS["TASKS_MODULE_NAME"] = "Tareas";
$MESS["TASKS_MODULE_DESC"] = "Módulo de la tarea administrativa";
$MESS["TASKS_INSTALL_COPY_PUBLIC"] = "Copiar los scripts de la sección pública";
$MESS["TASKS_INSTALL_COMPLETE_OK"] = "La instalación ha finalizado.";
$MESS["TASKS_INSTALL_COMPLETE_ERROR"] = "La instalación ha finalizado con errores.";
$MESS["TASKS_INSTALL_PUBLIC_SETUP"] = "Instalar";
$MESS["TASKS_INSTALL_TITLE"] = "Instalar el Módulo de Administración de la tarea";
$MESS["TASKS_UNINSTALL_TITLE"] = "Desinstalar el Módulo de administración de la tarea";
$MESS["TASKS_UNINSTALL_WARNING"] = "Advertencia! El módulo será removido del sistema.";
$MESS["TASKS_UNINSTALL_SAVEDATA"] = "Para guardar la data almacenada en las tablas de la base de datos, compruebe &quot;Save Tables&quot; box";
$MESS["TASKS_UNINSTALL_SAVETABLE"] = "Guardar Tablas";
$MESS["TASKS_UNINSTALL_DEL"] = "Eliminar";
$MESS["TASKS_UNINSTALL_ERROR"] = "Errores de desinstalación:";
$MESS["TASKS_UNINSTALL_COMPLETE"] = "La desinstalación ha finlizado.";
$MESS["TASKS_INSTALL_BACK"] = "Regresar al módulo de adminsitración";
$MESS["COPY_PUBLIC_FILES"] = "Copiar los archivos públicos y la plantilla del sitio";
$MESS["TASKS_CONVERT_AND_USE_NEW"] = "Convertir las tareas y empezar a usar las Tareas 2.0";
?>