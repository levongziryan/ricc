<?
$MESS["TASKS_ASSET_QUERY_QUERY_FAILED"] = "Error al procesar la solicitud.";
$MESS["TASKS_ASSET_QUERY_QUERY_FAILED_STATUS"] = "Error al procesar la solicitud.. (HTTP #HTTP_STATUS#)";
$MESS["TASKS_ASSET_QUERY_ILLEGAL_RESPONSE"] = "Error al analizar la respuesta del servidor.";
$MESS["TASKS_ASSET_QUERY_QUERY_FAILED_EXCEPTION"] = "Error al analizar la respuesta del servidor.";
$MESS["TASKS_ASSET_QUERY_EMPTY_RESPONSE"] = "Respuesta del servidor vacía.";
?>