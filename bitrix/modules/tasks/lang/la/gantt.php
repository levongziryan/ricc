<?
$MESS["TASKS_GANTT_EMPTY_DATE"] = "ninguno";
$MESS["TASKS_GANTT_CHART_TITLE"] = "Tareas";
$MESS["TASKS_GANTT_DATE_START"] = "Fecha de inicio";
$MESS["TASKS_GANTT_DATE_END"] = "Fecha de finalización";
$MESS["TASKS_GANTT_DEADLINE"] = "Fecha límite";
$MESS["TASKS_GANTT_START"] = "inicio";
$MESS["TASKS_GANTT_END"] = "final";
$MESS["TASKS_GANTT_DELETE_DEPENDENCY"] = "Eliminar";
$MESS["TASKS_GANTT_EMPTY_END_DATE"] = "Se requiere la fecha de finalización para establecer un link";
$MESS["TASKS_GANTT_CIRCULAR_DEPENDENCY"] = "No se permiten links circulares";
$MESS["TASKS_GANTT_PERMISSION_ERROR"] = "No tienes permiso para editar fechas de esta tarea";
$MESS["TASKS_GANTT_DEPENDENCY_FROM"] = "Hasta";
$MESS["TASKS_GANTT_DEPENDENCY_TO"] = "Desde";
$MESS["TASKS_GANTT_RELATION_ERROR"] = "No se puede unir una tarea a su tarea principal";
$MESS["TASKS_GANTT_INDENT_TASK"] = "Agregar sangría a Tarea";
$MESS["TASKS_GANTT_OUTDENT_TASK"] = "Quitar sangría a Tarea";
?>