<?php
$MESS['TASKS_STAGE_NEW'] = 'Новые';
$MESS['TASKS_STAGE_WORK'] = 'Выполняются';
$MESS['TASKS_STAGE_FINISH'] = 'Сделаны';
$MESS['TASKS_STAGE_MP_1'] = 'Не спланированы';
$MESS['TASKS_STAGE_MP_2'] = 'Сделаю на неделе';
$MESS['TASKS_STAGE_ERROR_CANT_DELETE_FIRST'] = 'Нельзя удалить первую стадию. Передвиньте стадию, чтобы удалить.';