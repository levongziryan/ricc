<?php
$MESS ['TASKS_NEW_TASK'] = "Новая задача";

$MESS ['TASKS_PLAN_DATE_END_OUT_OF_PROJECT_RANGE'] = "Планируемая дата завершения задачи стоит за рамками проекта";
$MESS ['TASKS_PLAN_DATE_START_OUT_OF_PROJECT_RANGE'] = "Планируемая дата начала задачи стоит за рамками проекта";
$MESS ['TASKS_DEADLINE_OUT_OF_PROJECT_RANGE'] = "Крайний срок задачи стоит за рамками проекта";

$MESS ['TASKS_MESSAGE_DESCRIPTION'] = "Описание";
$MESS ['TASKS_MESSAGE_ACCOMPLICES'] = "Соисполнители";
$MESS ['TASKS_MESSAGE_AUDITORS'] = "Наблюдатели";
$MESS ['TASKS_MESSAGE_DEADLINE'] = "Крайний срок";
$MESS ['TASKS_MESSAGE_RESPONSIBLE'] = "Ответственный";
$MESS ['TASKS_MESSAGE_TITLE'] = "Название";
$MESS ['TASKS_MESSAGE_PRIORITY'] = "Приоритет";
$MESS ['TASKS_MESSAGE_NO'] = "нет";
$MESS ['TASKS_NEW_TASK_MESSAGE'] = "Добавлена новая задача

Название: #TASK_TITLE#
Постановщик: #TASK_AUTHOR#
Ответственный: #TASK_RESPONSIBLE#
#TASK_EXTRA#
Просмотр задачи:
#PATH_TO_TASK#";
?>