<?
$MESS["TASK_REMINDER_NAME"] = "Aufgabenerinnerung";
$MESS["TASK_REMINDER_DESC"] = "#TASK_TITLE# - Aufgabenname
#PATH_TO_TASK# - Aufgaben-URL
";
$MESS["TASK_REMINDER_SUBJECT"] = "#SITE_NAME#: Das ist eine Erinnerung an die Aufgabe \"#TASK_TITLE#\".";
$MESS["TASK_REMINDER_MESSAGE"] = "Nachricht von der Website #SITE_NAME#
------------------------------------------
Klicken Sie auf diesen Link, um die Aufgabe anzuzeigen:

#PATH_TO_TASK#

Diese Nachricht wurde automatisch generiert.
";
$MESS["TASKS_ADD_TASK_NAME"] = "Eine neue Aufgabe wurde hinzugefügt";
$MESS["TASKS_ADD_TASK_DESC"] = "#EMAIL_TO# - E-Mail des Nachrichtenempfängers
#TASK_ID# - ID der Aufgabe
#RECIPIENT_ID# - ID des Empfängers
#URL_ID# - URL der Seite mit Aufgaben
";
$MESS["TASKS_UPDATE_TASK_NAME"] = "Aufgabenstatus wurde geändert";
$MESS["TASKS_UPDATE_TASK_DESC"] = "#EMAIL_TO# - E-Mail des Nachrichtenempfängers
#TASK_ID# - ID der Aufgabe
#RECIPIENT_ID# - ID des Empfängers
#URL_ID# - URL der Seite mit Aufgaben
";
$MESS["TASKS_ADD_COMMENT_NAME"] = "Ein neuer Kommentar wurde zur Aufgabe hinzugefügt";
$MESS["TASKS_TASK_ADD_EMAIL_NAME"] = "Neue Aufgabe wurde hinzugefügt (E-Mail-Nutzer)";
$MESS["TASKS_TASK_ADD_EMAIL_DESC"] = "#EMAIL_TO# - E-Mail des Nachrichtenempfängers
#TASK_ID# - ID der Aufgabe
#TASK_TITLE# - Aufgabenname
#RECIPIENT_ID# - ID des Empfängers
#USER_ID# - ID des Nutzers zur Überprüfung der Zugriffsrechte für Aufgabe
#URL# - URL der Seite mit Aufgaben
#SUBJECT# - Betreff der Nachricht
";
$MESS["TASKS_TASK_UPDATE_EMAIL_NAME"] = "Aufgabe wurde geändert (E-Mail-Nutzer)";
$MESS["TASKS_TASK_UPDATE_EMAIL_DESC"] = "#EMAIL_TO# - E-Mail des Nachrichtenempfängers
#TASK_ID# - ID der Aufgabe
#TASK_TITLE# - Aufgabenname
#TASK_PREVIOUS_FIELDS# - Aufgabendaten bis zum Zeitpunkt der Änderung
#RECIPIENT_ID# - ID des Empfängers
#USER_ID# - ID des Nutzers zur Überprüfung der Zugriffsrechte für Aufgabe
#URL# - URL der Seite mit Aufgaben
#SUBJECT# - Betreff der Nachricht
";
$MESS["TASKS_TASK_COMMENT_ADD_EMAIL_NAME"] = "Ein Kommentar wurde zur Aufgabe hinzugefügt (E-Mail-Nutzer)";
$MESS["TASKS_TASK_COMMENT_ADD_EMAIL_DESC"] = "#EMAIL_TO# - E-Mail des Nachrichtenempfängers
#TASK_ID# - ID der Aufgabe
#TASK_TITLE# - Aufgabenname
#COMMENT_ID# - ID des Kommentars
#RECIPIENT_ID# - ID des Empfängers
#USER_ID# - ID des Nutzers zur Überprüfung der Zugriffsrechte für Aufgabe
#URL# - URL der Seite mit Aufgaben
#SUBJECT# - Betreff der Nachricht
";
?>