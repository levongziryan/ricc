<?
$MESS["TASKS_MODULE_NAME"] = "Aufgaben";
$MESS["TASKS_MODULE_DESC"] = "Das Modul Aufgabenmanagement";
$MESS["TASKS_INSTALL_COPY_PUBLIC"] = "Skripte des Bereichs Ansicht kopieren";
$MESS["TASKS_INSTALL_COMPLETE_OK"] = "Installation wurde abgeschlossen.";
$MESS["TASKS_INSTALL_COMPLETE_ERROR"] = "Installation wurde mit Fehlern abgeschlossen.";
$MESS["TASKS_INSTALL_PUBLIC_SETUP"] = "Installieren";
$MESS["TASKS_INSTALL_TITLE"] = "Das Modul Aufgabenmanagement installieren";
$MESS["TASKS_UNINSTALL_TITLE"] = "Das Modul Aufgabenmanagement deinstallieren";
$MESS["TASKS_UNINSTALL_WARNING"] = "Achtung! Das Modul wird aus dem System entfernt.";
$MESS["TASKS_UNINSTALL_SAVEDATA"] = "Um die Daten in den Datenbanktabellen zu speichern, aktivieren Sie die Option &quot;Save Tables&quot;";
$MESS["TASKS_UNINSTALL_SAVETABLE"] = "Tabellen speichern";
$MESS["TASKS_UNINSTALL_DEL"] = "Löschen";
$MESS["TASKS_UNINSTALL_ERROR"] = "Fehler bei der Deinstallation:";
$MESS["TASKS_UNINSTALL_COMPLETE"] = "Deinstallation wurde abgeschlossen.";
$MESS["TASKS_INSTALL_BACK"] = "Zurück zur Modulverwaltung";
$MESS["COPY_PUBLIC_FILES"] = "Öffentliche Dateien und Website-Vorlagen kopieren";
$MESS["TASKS_CONVERT_AND_USE_NEW"] = "Aufgaben konvertieren und die Nutzung der Aufgaben 2.0 anfangen";
?>