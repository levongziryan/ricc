<?
$MESS["TASKS_DATE_START"] = "Voraussichtliches Anfangsdatum";
$MESS["TASKS_DATE_END"] = "Voraussichtliches Abschlussdatum";
$MESS["TASKS_DATE_STARTED"] = "Angefangen am";
$MESS["TASKS_DATE_COMPLETED"] = "Abgeschlossen am";
$MESS["TASKS_DATE_CREATED"] = "Erstellt am";
$MESS["TASKS_DATE_DEADLINE"] = "Frist";
$MESS["TASKS_TASK_TITLE_LABEL"] = "Aufgabe";
$MESS["TASKS_STATUS"] = "Status";
$MESS["TASKS_STATUS_OVERDUE"] = "Überfällige";
$MESS["TASKS_STATUS_NEW"] = "Neu";
$MESS["TASKS_STATUS_ACCEPTED"] = "Anstehend";
$MESS["TASKS_STATUS_IN_PROGRESS"] = "In Arbeit";
$MESS["TASKS_STATUS_WAITING"] = "Muss kontrolliert werden";
$MESS["TASKS_STATUS_COMPLETED"] = "Abgeschlossen";
$MESS["TASKS_STATUS_DELAYED"] = "Verschoben";
$MESS["TASKS_STATUS_DECLINED"] = "Abgelehnt";
$MESS["TASKS_PRIORITY_V2"] = "Wichtige Aufgabe";
$MESS["TASKS_QUICK_INFO_DETAILS"] = "Details";
$MESS["TASKS_QUICK_INFO_EMPTY_DATE"] = "Keine";
$MESS["TASKS_RESPONSIBLE"] = "Verantwortlich";
$MESS["TASKS_DIRECTOR"] = "Erstellt von";
$MESS["TASKS_FILES"] = "Dateien";
?>