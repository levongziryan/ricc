<?
$MESS["TASKS_ITEM_TASK_TEMPLATE_FIELD_SE_CHECKLIST"] = "Checkliste";
$MESS["TASKS_ITEM_TASK_TEMPLATE_FIELD_SE_ACCESS"] = "Zugriffsrechte";
$MESS["TASKS_ITEM_TASK_TEMPLATE_FIELD_SE_TAG"] = "Tags";
$MESS["TASKS_ITEM_TASK_TEMPLATE_FIELD_BASE_TEMPLATE_ID"] = "Basisvorlage";
$MESS["TASKS_ITEM_TASK_TEMPLATE_CANT_SWITCH_TYPE_ERROR"] = "Der Typ einer bereits existierenden Vorlage kann nicht geändert werden";
$MESS["TASKS_ITEM_TASK_TEMPLATE_PARENT_ITEM_CONFLICT_ERROR"] = "Basisvorlage und Basisaufgabe können nicht gleichzeitig definiert werden";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_MULTITASKING_ALLOWED_ERROR_BECAUSE_TYPE_1"] = "Eine Vorlage mit mehreren Verantwortlichen kann für den neuen Nutzer nicht erstellt werden.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_BASE_TEMPLATE_ALLOWED_ERROR_BECAUSE_TYPE_1"] = "Eine Basisvorlage kann der neuen Nutzervorlage nicht zugewiesen werden.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_BASE_TEMPLATE_ALLOWED_ERROR_BECAUSE_REPLICATION"] = "Eine Basisvorlage kann nicht zugewiesen werden, weil wiederholende Ausführung aktiviert ist.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_BASE_TEMPLATE_ALLOWED_ERROR_BECAUSE_MULTITASK"] = "Eine Basisvorlage kann nicht zugewiesen werden, wenn mehrere Verantwortliche angegeben sind.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_REPLICATION_ALLOWED_ERROR_BECAUSE_TYPE_1"] = "Parameter der Wiederholung können nicht definiert werden, wenn eine neue Nutzervorlage erstellt wird.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_REPLICATION_ALLOWED_ERROR_BASE_TEMPLATE_ID"] = "Parameter der Wiederholung können nicht definiert werden, wenn die Basisvorlage zugewiesen wird.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_TPARAM_TYPE_1_ALLOWED_ERROR_BECAUSE_REPLICATION"] = "Neue Nutzervorlage kann nicht erstellt werden, weil wiederholende Ausführung aktiviert ist.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_TPARAM_TYPE_1_ALLOWED_ERROR_BASE_TEMPLATE_ID"] = "Neue Nutzervorlage kann nicht erstellt werden, wenn die Basisvorlage zugewiesen wird.";
$MESS["TASKS_ITEM_TASK_TEMPLATE_NO_TPARAM_TYPE_1_ALLOWED_ERROR_BECAUSE_MULTITASK"] = "Neue Nutzervorlage kann nicht erstellt werden, weil wiederholende Ausführung aktiviert ist.";
?>