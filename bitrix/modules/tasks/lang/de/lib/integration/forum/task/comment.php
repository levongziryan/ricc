<?
$MESS["TASKS_COMMENT_MESSAGE_ADD_N"] = "Hat einen Kommentar zur Aufgabe #TASK_URL_BEGIN##TASK_TITLE##URL_END# hinzugefügt.";
$MESS["TASKS_COMMENT_MESSAGE_EDIT_N"] = "Hat einen Kommentar zur Aufgabe #TASK_URL_BEGIN##TASK_TITLE##URL_END# geändert.";
$MESS["TASKS_COMMENT_MESSAGE_ADD_F"] = "Hat einen Kommentar zur Aufgabe #TASK_URL_BEGIN##TASK_TITLE##URL_END# hinzugefügt.";
$MESS["TASKS_COMMENT_MESSAGE_ADD_WITH_TEXT"] = ' Der Kommentartext ist: "#TASK_COMMENT_TEXT#"';
$MESS["TASKS_COMMENT_MESSAGE_EDIT_WITH_TEXT"] = ' Der neue Text ist: "#TASK_COMMENT_TEXT#"';

$MESS["TASKS_COMMENT_MESSAGE_EDIT_F"] = "Hat einen Kommentar zur Aufgabe #TASK_URL_BEGIN##TASK_TITLE##URL_END# geändert.";
$MESS["TASKS_COMMENT_MESSAGE_ADD_M"] = "Hat einen Kommentar zur Aufgabe #TASK_URL_BEGIN##TASK_TITLE##URL_END# hinzugefügt.";
$MESS["TASKS_COMMENT_MESSAGE_EDIT_M"] = "Hat einen Kommentar zur Aufgabe #TASK_URL_BEGIN##TASK_TITLE##URL_END# geändert.";
$MESS["TASKS_COMMENT_MESSAGE_ADD_PUSH_M"] = "#USER_NAME# hat einen Kommentar zur Aufgabe #TASK_TITLE# hinzugefügt";
$MESS["TASKS_COMMENT_MESSAGE_ADD_PUSH_F"] = "#USER_NAME# hat einen Kommentar zur Aufgabe #TASK_TITLE# hinzugefügt";
?>