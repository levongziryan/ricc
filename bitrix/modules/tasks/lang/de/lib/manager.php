<?
$MESS["TASKS_MANAGER_TASK_ITEM_NAME"] = "des abhängigen Elements";
$MESS["TASKS_MANAGER_TASK_CANT_UPDATE"] = "Sie haben nicht genügend Rechte, um #ITEM_NAME# ID=#ID# zu bearbeiten";
$MESS["TASKS_MANAGER_TASK_CANT_DELETE"] = "Sie haben nicht genügend Rechte, um #ITEM_NAME# ID=#ID# zu löschen";
?>