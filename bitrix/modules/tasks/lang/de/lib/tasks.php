<?
$MESS["TASKS_ENTITY_ID"] = "ID";
$MESS["TASKS_ENTITY_TITLE"] = "Name";
$MESS["TASKS_ENTITY_PRIORITY"] = "Priorität";
$MESS["TASKS_ENTITY_STATUS"] = "Status";
$MESS["TASKS_ENTITY_RESPONSIBLE"] = "Verantwortlich";
$MESS["TASKS_ENTITY_START_DATE_PLAN"] = "Voraussichtliches Anfangsdatum";
$MESS["TASKS_ENTITY_END_DATE_PLAN"] = "Voraussichtliches Abschlussdatum";
$MESS["TASKS_ENTITY_DURATION_PLAN_MINUTES"] = "Voraussichtliche Dauer";
$MESS["TASKS_ENTITY_DATE_START"] = "Anfangsdatum";
$MESS["TASKS_ENTITY_DEADLINE"] = "Frist";
$MESS["TASKS_ENTITY_CREATED_BY_USER"] = "Erstellt von";
$MESS["TASKS_ENTITY_CREATED_DATE"] = "Erstellt am";
$MESS["TASKS_ENTITY_CHANGED_BY_USER"] = "Zuletzt geändert von";
$MESS["TASKS_ENTITY_CHANGED_DATE"] = "Zuletzt geändert am";
$MESS["TASKS_ENTITY_STATUS_CHANGED_BY_USER"] = "Status zuletzt geändert von";
$MESS["TASKS_ENTITY_STATUS_CHANGED_DATE"] = "Status zuletzt geändert am";
$MESS["TASKS_ENTITY_CLOSED_BY_USER"] = "Abgeschlossen von";
$MESS["TASKS_ENTITY_CLOSED_DATE"] = "Abgeschlossen am";
$MESS["TASKS_ENTITY_GROUP"] = "Projekt";
$MESS["TASKS_ENTITY_ADD_IN_REPORT"] = "Im Bericht";
$MESS["TASKS_ENTITY_DURATION"] = "Zeitaufwand";
$MESS["TASKS_ENTITY_IS_OPEN"] = "Offen";
$MESS["TASKS_ENTITY_IS_NEW"] = "Neu";
$MESS["TASKS_ENTITY_IS_OVERDUE"] = "Überfällig";
$MESS["TASKS_ENTITY_IS_FINISHED"] = "Abgeschlossen";
$MESS["TASKS_ENTITY_IS_RUNNING"] = "In Arbeit";
$MESS["TASKS_ENTITY_IS_MARKED"] = "Bewertet";
$MESS["TASKS_ENTITY_IS_EFFECTIVE"] = "Effektivität";
$MESS["TASKS_ENTITY_STATUS_VALUE_1"] = "Neu (noch nicht angenommen)";
$MESS["TASKS_ENTITY_STATUS_VALUE_2"] = "Angenommen";
$MESS["TASKS_ENTITY_STATUS_VALUE_3"] = "Wird ausgeführt";
$MESS["TASKS_ENTITY_STATUS_VALUE_4"] = "Bedingt abgeschlossen";
$MESS["TASKS_ENTITY_STATUS_VALUE_5"] = "Abgeschlossen";
$MESS["TASKS_ENTITY_STATUS_VALUE_6"] = "Verschoben";
$MESS["TASKS_ENTITY_STATUS_VALUE_7"] = "Abgelehnt";
$MESS["TASKS_ENTITY_PRIORITY_VALUE_0"] = "Niedrig";
$MESS["TASKS_ENTITY_PRIORITY_VALUE_1"] = "Normal";
$MESS["TASKS_ENTITY_PRIORITY_VALUE_2"] = "Hoch";
$MESS["TASKS_ENTITY_MARK"] = "Bewertung";
$MESS["TASKS_ENTITY_MARK_VALUE_P"] = "Positive";
$MESS["TASKS_ENTITY_MARK_VALUE_N"] = "Negative";
$MESS["TASKS_ENTITY_MARK_VALUE_NONE"] = "Keine";
?>