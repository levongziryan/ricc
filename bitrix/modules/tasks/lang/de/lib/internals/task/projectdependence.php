<?
$MESS["DEPENDENCE_ENTITY_TASK_ID_FIELD"] = "Aufgabe";
$MESS["DEPENDENCE_ENTITY_DEPENDS_ON_ID_FIELD"] = "Übergeordnete Aufgabe";
$MESS["DEPENDENCE_ENTITY_TYPE_FIELD"] = "Verknüpfungstyp";
$MESS["DEPENDENCE_ENTITY_CANT_ADD_LINK_CREATED_DATE_NOT_SET"] = "Die zu verknüpfende Aufgabe hat kein Erstelldatum";
$MESS["DEPENDENCE_ENTITY_CANT_ADD_LINK_CREATED_DATE_NOT_SET_PARENT_TASK"] = "Die übergeordnete Aufgabe hat kein Erstelldatum";
$MESS["DEPENDENCE_ENTITY_CANT_ADD_LINK_END_DATE_PLAN_NOT_SET"] = "Die zu verknüpfende Aufgabe kein geplantes Abschlussdatum";
$MESS["DEPENDENCE_ENTITY_CANT_ADD_LINK_END_DATE_PLAN_NOT_SET_PARENT_TASK"] = "Die übergeordnete Aufgabe hat kein geplantes Abschlussdatum";
?>