<?
$MESS["TASKS_CLOSURE_TREE_LINK_EXISTS"] = "Knoten sind bereits verknüpft";
$MESS["TASKS_CLOSURE_TREE_NODE_NOT_FOUND"] = "Angegebener Knoten wurde nicht gefunden.";
$MESS["TASKS_CLOSURE_TREE_NODE_EXISTS_BUT_DECLARED_NEW"] = "Angegebener Knoten existiert, wird aber als ein neuer hinzugefügt";
$MESS["TASKS_CLOSURE_TREE_CANT_ATTACH_TO_SELF"] = "Ein Knoten kann mit sich selbst nicht verknüpft werden";
$MESS["TASKS_CLOSURE_TREE_ILLEGAL_NODE"] = "Knoten-ID ist nicht korrekt";
$MESS["TASKS_CLOSURE_TREE_CANT_ATTACH_TO_ANCESTOR"] = "Knoten kann nicht mit seinem untergeordneten Element verknüpft werden";
?>