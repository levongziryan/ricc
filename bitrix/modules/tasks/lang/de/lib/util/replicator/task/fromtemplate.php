<?
$MESS["TASKS_REPLICATOR_TASK_CREATED"] = "Geplante Aufgabe wurde erstellt";
$MESS["TASKS_REPLICATOR_TASK_CREATED_WITH_ERRORS"] = "Geplante Aufgabe wurde erstellt, es gab allerdings Fehler";
$MESS["TASKS_REPLICATOR_TASK_WAS_NOT_CREATED"] = "Geplante Aufgabe wurde nicht erstellt";
$MESS["TASKS_REPLICATOR_TASK_POSSIBLY_WAS_NOT_CREATED"] = "Es kann sein, dass die geplante Aufgabe nicht erstellt wurde";
$MESS["TASKS_REPLICATOR_PROCESS_STOPPED"] = "Wiederholung der Aufgabe wurde gestoppt";
$MESS["TASKS_REPLICATOR_END_DATE_REACHED"] = "Abschlussdatum erreicht";
$MESS["TASKS_REPLICATOR_LIMIT_REACHED"] = "Maximale Wiederholungszahl erreicht";
$MESS["TASKS_REPLICATOR_ILLEGAL_NEXT_TIME"] = "Zeitpunkt der nächsten Wiederholung konnte nicht errechnet werden";
$MESS["TASKS_REPLICATOR_PROCESS_ERROR"] = "Fehler beim Errechnen des Zeitpunkts der nächsten Wiederholung";
$MESS["TASKS_REPLICATOR_NEXT_TIME"] = "Zeitpunkt der nächsten Wiederholung: #TIME# (in #PERIOD# Sekunden)";
$MESS["TASKS_REPLICATOR_SUBTREE_LOOP"] = "Es kann sein, dass einige der Teilvorlage sich auf sich selbst beziehen. Teilaufgaben wurden nicht erstellt.";
$MESS["TASKS_REPLICATOR_INTERNAL_ERROR"] = "Interner Fehler. Wenden Sie sich bitte an Technischen Support.";
$MESS["TASKS_REPLICATOR_CANT_IDENTIFY_USER"] = "Ein Nutzer, unter welchem die Aufgabe ausgeführt werden soll, kann nicht definiert werden. Überprüfen Sie bitte, ob der Administrator-Account aktiv ist.";
$MESS["TASKS_REPLICATOR_SECOND_0"] = "Sekunde";
$MESS["TASKS_REPLICATOR_SECOND_1"] = "Sekunden";
$MESS["TASKS_REPLICATOR_SECOND_2"] = "Sekunden";
$MESS["TASKS_REPLICATOR_CREATOR_INACTIVE"] = "Account des Aufgabenautors existiert nicht, oder ist nicht aktiv.";
?>