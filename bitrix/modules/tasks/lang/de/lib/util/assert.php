<?
$MESS["TASKS_ASSERT_INTEGER_EXPECTED"] = "Das Argument#ARG_NAME#muss eine ganze Zahl sein.";
$MESS["TASKS_ASSERT_INTEGER_NOTNULL_EXPECTED"] = "Das Argument#ARG_NAME#muss eine positive ganze Zahl sein.";
$MESS["TASKS_ASSERT_ARRAY_NOT_EMPTY_EXPECTED"] = "Das Argument#ARG_NAME#muss ein nicht leerer Bereich sein.";
$MESS["TASKS_ASSERT_ARRAY_EXPECTED"] = "Das Argument#ARG_NAME#muss ein Bereich sein.";
$MESS["TASKS_ASSERT_STRING_NOTNULL_EXPECTED"] = "Das Argument#ARG_NAME#muss eine nicht leere Zeile sein.";
$MESS["TASKS_ASSERT_ARRAY_OF_INTEGER_NOT_NULL_EXPECTED"] = "Das Argument#ARG_NAME#ist nicht ein Bereich von positiven ganzen Zahlen.";
$MESS["TASKS_ASSERT_ARRAY_OF_STRING_NOT_NULL_EXPECTED"] = "Das Argument#ARG_NAME#ist nicht ein Bereich von nicht leeren Zeilen.";
$MESS["TASKS_ASSERT_EMPTY_ENUMERATION"] = "An die Überprüfungsmethode wurde eine leere Enum weitergegeben.";
$MESS["TASKS_ASSERT_ITEM_NOT_IN_ENUMERATION"] = "Das Argument#ARG_NAME#ist nicht einer der Enum-Werte.";
$MESS["TASKS_ASSERT_EMPTY_ARGUMENT"] = "Es wurde ein leeres Argument weitergegeben.";
$MESS["TASKS_ASSERT_INTEGER_NONNEGATIVE_EXPECTED"] = "Das Argument#ARG_NAME#muss eine nicht negative ganze Zahl sein.";
?>