<?
$MESS["TASKS_SORTING_AUTH_REQUIRED"] = "Sie müssen autorisiert sein, um diese Operation ausführen zu können.";
$MESS["TASKS_SORTING_WRONG_SOURCE_TASK"] = "Quellaufgabe wurde nicht gefunden";
$MESS["TASKS_SORTING_WRONG_TARGET_TASK"] = "Angrenzende Aufgabe wurde nicht gefunden";
$MESS["TASKS_SORTING_WRONG_GROUP_PERMISSIONS"] = "Sie können Aufgaben in dieser Gruppe nicht sortieren";
?>