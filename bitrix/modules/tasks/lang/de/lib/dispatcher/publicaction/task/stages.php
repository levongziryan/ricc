<?
$MESS["STAGES_ERROR_ACCESS_DENIED_GROUP_TASK"] = "Sie können keine Aufgaben in dieser Gruppe anzeigen";
$MESS["STAGES_ERROR_ACCESS_DENIED_STAGES"] = "Sie können keine Phasen verwalten";
$MESS["STAGES_ERROR_ACCESS_DENIED_MOVE"] = "Sie können diese Aufgabe nicht verschieben";
$MESS["STAGES_ERROR_DIFFERENT_STAGES"] = "Phase und Aufgabe gehören zu jeweils verschiedenen Gruppen";
$MESS["STAGES_ERROR_EMPTY_TITLE"] = "Überschrift der Phase fehlt";
$MESS["STAGES_ERROR_NOT_FOUND"] = "Phase wurde nicht gefunden";
$MESS["STAGES_ERROR_EMPTY_DATA"] = "Es wurden keine Daten für Aktualisierung bereitgestellt";
$MESS["STAGES_ERROR_NO_EMPTY"] = "Es gibt Aufgaben in dieser Phase";
$MESS["STAGES_ERROR_IS_SYSTEM"] = "Standardphase kann nicht gelöscht werden";
$MESS["STAGES_ERROR_SOCIALNETWORK_IS_NOT_INSTALLED"] = "Das Modul Soziales Netzwerk ist nicht installiert.";
$MESS["STAGES_ERROR_TASK_NOT_FOUND"] = "Die Aufgabe wurde nicht gefunden, oder der Zugriff wurde verweigert.";
?>