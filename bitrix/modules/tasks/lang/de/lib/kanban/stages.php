<?
$MESS["TASKS_STAGE_NEW"] = "Neu";
$MESS["TASKS_STAGE_WORK"] = "In Arbeit";
$MESS["TASKS_STAGE_FINISH"] = "Erledigt";
$MESS["TASKS_STAGE_MP_1"] = "Nicht geplant";
$MESS["TASKS_STAGE_MP_2"] = "Diese Woche zu erledigen";
$MESS["TASKS_STAGE_ERROR_CANT_DELETE_FIRST"] = "Eine ursprüngliche Phase kann nicht gelöscht werden. Sie müssen die Phase verschieben, um sie lösche zu können.";
?>