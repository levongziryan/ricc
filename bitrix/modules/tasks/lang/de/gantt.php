<?
$MESS["TASKS_GANTT_EMPTY_DATE"] = "keins";
$MESS["TASKS_GANTT_CHART_TITLE"] = "Aufgaben";
$MESS["TASKS_GANTT_DATE_START"] = "Anfangsdatum";
$MESS["TASKS_GANTT_DATE_END"] = "Abschlussdatum";
$MESS["TASKS_GANTT_DEADLINE"] = "Frist";
$MESS["TASKS_GANTT_START"] = "Anfang";
$MESS["TASKS_GANTT_END"] = "Abschluss";
$MESS["TASKS_GANTT_DELETE_DEPENDENCY"] = "Löschen";
$MESS["TASKS_GANTT_EMPTY_END_DATE"] = "Abschlussdatum ist erforderlich, um eine Verknüpfung zu erstellen";
$MESS["TASKS_GANTT_CIRCULAR_DEPENDENCY"] = "Zirkelbezug ist nicht erlaubt";
$MESS["TASKS_GANTT_PERMISSION_ERROR"] = "Sie sind nicht berechtigt, Fristen dieser Aufgabe zu ändern";
$MESS["TASKS_GANTT_DEPENDENCY_FROM"] = "Von";
$MESS["TASKS_GANTT_DEPENDENCY_TO"] = "Bis";
$MESS["TASKS_GANTT_RELATION_ERROR"] = "Aufgabe kann nicht mit übergeordneter Aufgabe verknüpft werden";
$MESS["TASKS_GANTT_INDENT_TASK"] = "Vorgang herunterstufen";
$MESS["TASKS_GANTT_OUTDENT_TASK"] = "Vorgang heraufstufen";