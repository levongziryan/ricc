<?
$MESS["JS_CORE_PL_TASKS"] = "Aufgaben";
$MESS["JS_CORE_PL_TASKS_CHOOSE"] = "Aus der Liste auswählen";
$MESS["JS_CORE_PL_TASKS_ADD"] = "Neue Aufgabe eingeben";
$MESS["JS_CORE_PL_TASKS_START_TIMER"] = "Zeiterfassung starten";
$MESS["JS_CORE_PL_TASKS_STOP_TIMER"] = "Zeiterfassung pausieren";
$MESS["JS_CORE_PL_TASKS_FINISH"] = "Aufgabe fertigstellen und Zeiterfassung beenden";
$MESS["JS_CORE_PL_TASKS_MENU_REMOVE_FROM_PLAN"] = "Aus dem Tagesplan entfernen";
$MESS["JS_CORE_PL_TASKS_CREATE"] = "Neue Aufgabe";
?>