<?
$MESS["REPORT_TASKS_COLUMN_TREE_STATUS_SUB"] = "Mit Status";
$MESS["REPORT_TASKS_TAGS"] = "Tags";
$MESS["TASKS_REPORT_DEFAULT_2"] = "Teilnahme an Projekten";
$MESS["TASKS_REPORT_DEFAULT_3"] = "Aufgaben für den Monat";
$MESS["TASKS_REPORT_DEFAULT_4"] = "Leistungsbericht";
$MESS["TASKS_REPORT_EFF_EMPLOYEE"] = "Mitarbeiter";
$MESS["TASKS_REPORT_EFF_NEW"] = "Neue";
$MESS["TASKS_REPORT_EFF_OPEN"] = "Offene";
$MESS["TASKS_REPORT_EFF_CLOSED"] = "Abgeschlossen";
$MESS["TASKS_REPORT_EFF_OVERDUE"] = "Überfällig";
$MESS["TASKS_REPORT_EFF_MARKED"] = "Bewertet";
$MESS["TASKS_REPORT_EFF_EFFICIENCY"] = "Leistung";
$MESS["TASKS_REPORT_DEFAULT_5"] = "Aufgaben vom letzten Monat ";
$MESS["TASKS_REPORT_DURATION_DAYS"] = "T.";
$MESS["TASKS_REPORT_DURATION_HOURS"] = "St.";
$MESS["TASKS_REPORT_DEFAULT_6"] = "Ressourcenerfassung für Aufgaben";
$MESS["TASKS_REPORT_DEFAULT_6_DESCR"] = "Dieser Bericht zeigt Aufgaben an, die im Rahmen eines Projektes, einer Abteilung oder des ganzen Unternehmens innerhalb des angegebenen Zeitraums erledigt wurden, sowie für diese Aufgaben aufgewendete Zeit.";
$MESS["TASKS_REPORT_DEFAULT_7"] = "Ressourcenerfassung für Mitarbeiter";
$MESS["TASKS_REPORT_DEFAULT_7_DESCR"] = "Zeigt an, wie viel Zeit es bei jedem Mitarbeiter in Anspruch nimmt, Aufgaben zu erledigen, wenn Zeiterfassung aktiviert wurde. Dieser Bericht kann auch projektanhängig erstellt werden.";
$MESS["TASKS_REPORT_DEFAULT_7_ALIAS_4"] = "Anzahl der Aufgaben";
$MESS["TASKS_REPORT_DEFAULT_7_ALIAS_6"] = "Aufgewendete Zeit";
$MESS["TASKS_REPORT_DEFAULT_7_ALIAS_8"] = "Aufgewendet innerhalb des angegebenen Zeitraums";
$MESS["TASKS_REPORT_DEFAULT_7_ALIAS_10"] = "Geplante Arbeitszeit";
$MESS["TASKS_REPORT_UF_TASK_WEBDAV_FILES"] = "Dokumente";
$MESS["REPORT_TASKS_DURATION_FOR_PERIOD"] = "Zeitaufwand (Berichtszeitraum, veraltet)";
$MESS["REPORT_TASKS_Member:TASK_COWORKED.USER"] = "Mitwirkende";
?>