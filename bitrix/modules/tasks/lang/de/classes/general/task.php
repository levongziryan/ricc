<?
$MESS["TASKS_NEW_TASK"] = "Neue Aufgabe";
$MESS["TASKS_MESSAGE_DESCRIPTION"] = "Beschreibung";
$MESS["TASKS_MESSAGE_ACCOMPLICES"] = "Mitwirkende";
$MESS["TASKS_MESSAGE_AUDITORS"] = "Beobachter";
$MESS["TASKS_MESSAGE_DEADLINE"] = "Frist";
$MESS["TASKS_MESSAGE_RESPONSIBLE"] = "Verantwortliche Person";
$MESS["TASKS_MESSAGE_TITLE"] = "Name";
$MESS["TASKS_MESSAGE_PRIORITY"] = "Priorität";
$MESS["TASKS_MESSAGE_NO"] = "Keine";
$MESS["TASKS_NEW_TASK_MESSAGE"] = "Eine neue Aufgabe wurde hinzugefügt

Aufgabenname: #TASK_TITLE#
Erstellt von: #TASK_AUTHOR#
Verantwortliche Person: #TASK_RESPONSIBLE#
#TASK_EXTRA#
Die Aufgabe anzeigen:
#PATH_TO_TASK#";
$MESS["TASKS_PLAN_DATE_END_OUT_OF_PROJECT_RANGE"] = "Das geplante Abschlussdatum der Aufgabe liegt außerhalb des Projektzeitrahmens.";
$MESS["TASKS_PLAN_DATE_START_OUT_OF_PROJECT_RANGE"] = "Das geplante Anfangsdatum der Aufgabe liegt außerhalb des Projektzeitrahmens.";
$MESS["TASKS_DEADLINE_OUT_OF_PROJECT_RANGE"] = "Die Frist der Aufgabe liegt außerhalb des Projektzeitrahmens.";
?>