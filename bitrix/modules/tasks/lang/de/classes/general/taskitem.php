<?
$MESS["TASK_CANT_ADD_LINK"] = "Aufgaben können nicht verknüpft werden";
$MESS["TASK_CANT_DELETE_LINK"] = "Verknüpfung zwischen Aufgaben kann nicht gelöscht werden";
$MESS["TASKS_HAS_PARENT_RELATION"] = "Aufgabe kann nicht mit übergeordneter Aufgabe verknüpft werden";
$MESS["TASKS_TRIAL_PERIOD_EXPIRED"] = "Ihre Demo-Version ist abgelaufen";
$MESS["TASK_CANT_UPDATE_LINK"] = "Aufgabenverknüpfung kann nicht aktualisiert werden";
$MESS["TASKS_PLAN_DATE_END_OUT_OF_PROJECT_RANGE"] = "Das geplante Abschlussdatum der Aufgabe liegt außerhalb des Projektzeitrahmens.";
$MESS["TASKS_PLAN_DATE_START_OUT_OF_PROJECT_RANGE"] = "Das geplante Anfangsdatum der Aufgabe liegt außerhalb des Projektzeitrahmens.";
$MESS["TASKS_DEADLINE_OUT_OF_PROJECT_RANGE"] = "Die Frist der Aufgabe liegt außerhalb des Projektzeitrahmens.";
$MESS["TASKS_ACCESS_DENIED_TO_CREATOR_UPDATE"] = "Sie haben nicht genügend Rechte, um den Aufgabenautor zu ändern";
$MESS["TASKS_ACCESS_DENIED_TO_DEADLINE_UPDATE"] = "Sie haben nicht genügend Rechte, um die Frist zu ändern";
$MESS["TASKS_ACCESS_DENIED_TO_TASK_UPDATE"] = "Sie haben nicht genügend Rechte, um die Aufgabe zu bearbeiten";
?>