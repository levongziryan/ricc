<?
$MESS["TASKS_COUNTERS_NOTICE_TITLE"] = "Intranet-Assistent";
$MESS["TASKS_COUNTERS_NOTICE_CONTENT_V2_PLURAL_0"] = "[B]#TASKS_COUNT# Ihre Aufgabe[/B] verlangt Ihre Reaktion oder enthält Anmerkungen.
[URL=#HREF#]Zur Aufgabe wechseln, um Details anzuzeigen[/URL].
";
$MESS["TASKS_COUNTERS_NOTICE_CONTENT_V2_PLURAL_1"] = "[B]#TASKS_COUNT# Ihre Aufgaben[/B] verlangen Ihre Reaktion oder enthalten Anmerkungen.
[URL=#HREF#]Zu Aufgaben wechseln, um Details anzuzeigen[/URL].
";
$MESS["TASKS_COUNTERS_NOTICE_CONTENT_V2_PLURAL_2"] = "[B]#TASKS_COUNT# von Ihren Aufgaben[/B] verlangen Ihre Reaktion oder enthalten Anmerkungen.
[URL=#HREF#]Zu Aufgaben wechseln, um Details anzuzeigen[/URL].
";
?>