<?
$MESS["TASKS_ASSET_QUERY_QUERY_FAILED"] = "Fehler bei Verarbeitung der Anfrage..";
$MESS["TASKS_ASSET_QUERY_QUERY_FAILED_STATUS"] = "Fehler bei Verarbeitung der Anfrage.. (HTTP #HTTP_STATUS#)";
$MESS["TASKS_ASSET_QUERY_ILLEGAL_RESPONSE"] = "Fehler beim Parsing der Server-Antwort.";
$MESS["TASKS_ASSET_QUERY_QUERY_FAILED_EXCEPTION"] = "Fehler beim Parsing der Server-Antwort.";
$MESS["TASKS_ASSET_QUERY_EMPTY_RESPONSE"] = "Leere Antwort vom Server.";
?>