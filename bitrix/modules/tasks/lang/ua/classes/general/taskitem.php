<?
$MESS["TASK_CANT_ADD_LINK"] = "Неможливо додати зв'язок між завданнями";
$MESS["TASK_CANT_DELETE_LINK"] = "Неможливо видалити зв'язок між завданнями";
$MESS["TASKS_HAS_PARENT_RELATION"] = "Зв'язок між підзавданням і базовим завданням неможливий";
$MESS["TASKS_TRIAL_PERIOD_EXPIRED"] = "Пробний період завершено";
$MESS["TASK_CANT_UPDATE_LINK"] = "Неможливо оновити зв'язок між завданнями";
$MESS["TASKS_PLAN_DATE_END_OUT_OF_PROJECT_RANGE"] = "Запланована дата завершення завдання стоїть за рамками проекту";
$MESS["TASKS_PLAN_DATE_START_OUT_OF_PROJECT_RANGE"] = "Запланована дата початку завдання стоїть за рамками проекту";
$MESS["TASKS_DEADLINE_OUT_OF_PROJECT_RANGE"] = "Останній термін завдання стоїть за рамками проекту";
?>