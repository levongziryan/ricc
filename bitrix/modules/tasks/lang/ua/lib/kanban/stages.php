<?
$MESS["TASKS_STAGE_NEW"] = "Нові";
$MESS["TASKS_STAGE_WORK"] = "Виконуються";
$MESS["TASKS_STAGE_FINISH"] = "Зроблені";
$MESS["TASKS_STAGE_MP_1"] = "Не сплановані";
$MESS["TASKS_STAGE_MP_2"] = "Зроблю на тижні";
$MESS["TASKS_STAGE_ERROR_CANT_DELETE_FIRST"] = "Не можна видалити першу стадію. Пересуньте стадію, щоб видалити.";
?>