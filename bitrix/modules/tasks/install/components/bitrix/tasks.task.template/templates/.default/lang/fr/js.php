<?
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_MULTIPLE_RESPONSIBLE_NOTICE"] = "Une tâche séparée sera créée pour chaque personne responsable";
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_TPARAM_TYPE"] = "Modèle de tâche pour un nouvel employé";
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_NO_PARENT_TEMPLATE_NOTICE"] = "Le modèle de tâche ne peut pas avoir un modèle parent si l’option '#TPARAM_FOR_NEW_USER#' est cochée.";
?>