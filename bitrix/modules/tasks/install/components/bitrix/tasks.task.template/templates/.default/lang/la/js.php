<?
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_MULTIPLE_RESPONSIBLE_NOTICE"] = "Se creará una tarea separada para cada persona responsable";
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_TPARAM_TYPE"] = "Plantilla de tareas para un nuevo empleado";
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_NO_PARENT_TEMPLATE_NOTICE"] = "La plantilla de tarea no puede tener una plantilla principal si la opción '#TPARAM_FOR_NEW_USER#' está chequeada.";
?>