<?
$MESS["TASKS_REPORT_MY_TASKS_ONLY"] = "Minhas tarefas";
$MESS["TASKS_REPORT_MY_DEPTS_TASKS_ONLY"] = "Tarefas dos subordinados";
$MESS["TASKS_REPORT_MY_GROUPS_TASKS_ONLY"] = "Tarefas do Grupo";
$MESS["TASKS_REPORT_MY_TASKS_HINT"] = "Mostra todas as tarefas pelas quais você é o executante ou participante";
$MESS["TASKS_REPORT_MY_DEPTS_TASKS_ONLY_HINT"] = "Mostra as tarefas criadas por você ou atribuídas a seus subordinados";
$MESS["TASKS_REPORT_MY_GROUPS_TASKS_ONLY_HINT"] = "Mostra todas as tarefas atribuídas a seus grupos de trabalho";
$MESS["TASKS_REPORT_DURATION_DAYS"] = "d.";
$MESS["TASKS_REPORT_DURATION_HOURS"] = "hr.";
?>