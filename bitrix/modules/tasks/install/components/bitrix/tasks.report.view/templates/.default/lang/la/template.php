<?
$MESS["TASKS_REPORT_MY_TASKS_ONLY"] = "Mis tareas";
$MESS["TASKS_REPORT_MY_DEPTS_TASKS_ONLY"] = "Tareas de los subordinados";
$MESS["TASKS_REPORT_MY_GROUPS_TASKS_ONLY"] = "Tareas del Grupo";
$MESS["TASKS_REPORT_MY_TASKS_HINT"] = "Muestra todas las tareas para las que usted es responsable o participante";
$MESS["TASKS_REPORT_MY_DEPTS_TASKS_ONLY_HINT"] = "Muestra las tareas creadas por usted o asignados a sus subordinados";
$MESS["TASKS_REPORT_MY_GROUPS_TASKS_ONLY_HINT"] = "Muestra todas las tareas asignadas a sus grupos de trabajo";
$MESS["TASKS_REPORT_DURATION_DAYS"] = "d.";
$MESS["TASKS_REPORT_DURATION_HOURS"] = "hr.";
?>