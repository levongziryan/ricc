<?
$MESS["TASK_LIST_TASK_NOT_INSTALLED"] = "El módulo Tasks no está instalado.";
$MESS["TASK_LIST_SOCNET_NOT_INSTALLED"] = "El módulo Social Network no está instalado.";
$MESS["TASK_LIST_NOT_AVAILABLE_IN_THIS_EDITION"] = "El módulo Tareas no está disponible en esta edición.";
$MESS["TASK_LIST_ACCESS_DENIED"] = "No se puede ver la lista de tareas porque se denegó el acceso.";
$MESS["TASK_LIST_ACCESS_TO_GROUP_DENIED"] = "No puede ver la lista de tareas para este grupo porque se denegó el acceso.";
$MESS["TASK_LIST_USER_NOT_FOUND"] = "No se encontró el usuario.";
$MESS["TASK_LIST_GROUP_NOT_FOUND"] = "No se encontró el grupo de trabajo.";
$MESS["TASK_LIST_TASK_ACTION_DENIED"] = "No se puede ejecutar este comando porque se denegó el acceso.";
$MESS["TASK_LIST_TASK_CREATE_DENIED"] = "No se pueden crear tareas porque se denegó el acceso.";
$MESS["TASK_LIST_TITLE"] = "Tareas";
$MESS["TASK_LIST_TITLE_MY"] = "Mis tareas";
$MESS["TASK_LIST_TITLE_GROUP"] = "Tareas del grupo de trabajo";
$MESS["TASK_LIST_COLUMN_TITLE_EMPTY"] = "El nombre de la etapa no puede estar vacío.";
$MESS["TASK_LIST_UNKNOWN_ACTION"] = "Acción desconocida.";
$MESS["TASK_LIST_COLUMN_NOT_EMPTY"] = "Hay tareas en esta etapa. Muévalos antes de eliminar la etapa.";
$MESS["TASK_LIST_SESS_EXPIRED"] = "Su sesión ha caducado.";
$MESS["TASK_LIST_ERROR_CHANGE_DEADLINE"] = "La fecha límite de la tarea no se puede cambiar.";
$MESS["TASK_ACCESS_NOTIFY_MESSAGE"] = "Por favor <a href=\"#URL#\">configurar las etapas de la tarea del proyecto kanban</a> para mí, o concedame un permiso apropiado para que pueda hacerlo yo mismo.";
?>