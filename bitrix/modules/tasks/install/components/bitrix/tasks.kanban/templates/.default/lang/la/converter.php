<?
$MESS["TASKS_KANBAN_CONVERTER_DIALOG_TITLE"] = "Actualización rápida";
$MESS["TASKS_KANBAN_CONVERTER_DIALOG_TEXT"] = "Esta pequeña actualización es necesaria para continuar usando el Planner. No llevara mucho tiempo; ya hemos procesado #PROCESSED# sus tareas.";
?>