<?
$MESS["TASKS_FILTER_ID"] = "ID";
$MESS["TASKS_FILTER_ADV_IN_REPORT"] = "dans le rapport";
$MESS["TASKS_FILTER_SELECT"] = "choisir";
$MESS["TASKS_FILTER_PICK_DATE"] = "Choisir la date du calendrier";
$MESS["TASKS_FILTER_CLOSE_DATE"] = "Date de la clôture";
$MESS["TASKS_FILTER_CREAT_DATE"] = "Date de création";
$MESS["TASKS_FILTER_TITLE"] = "Mes Tâches";
$MESS["TASKS_FILTER_FIND"] = "Recherche";
$MESS["TASKS_FILTER_ACTIVE_DATE"] = "En cours de construction";
$MESS["TASKS_FILTER_COMMON"] = "Moyen";
$MESS["TASKS_CANCEL"] = "Annuler";
$MESS["TASKS_FILTER_MARKED"] = "valués";
$MESS["TASKS_FILTER_BY_TAG"] = "Par tags";
$MESS["TASKS_FILTER_SHOW_SUBORDINATE"] = "montrer les tâches des subordonnés";
$MESS["TASKS_FILTER_OVERDUED"] = "périmé(e)s";
$MESS["TASKS_FILTER_WORKGROUP"] = "Du groupe de travail";
$MESS["TASKS_FILTER_EXTENDED"] = "Elargi";
$MESS["TASKS_FILTER_STATUS"] = "Statut";
$MESS["TASKS_FILTER_STATUSES"] = "Statuts";
$MESS["TASKS_FILTER"] = "Filtre";
?>