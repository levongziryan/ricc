<?
$MESS["TASKS_FILTER"] = "Filtro";
$MESS["TASKS_FILTER_COMMON"] = "Normal";
$MESS["TASKS_FILTER_EXTENDED"] = "Extendido";
$MESS["TASKS_FILTER_STATUSES"] = "Estados";
$MESS["TASKS_FILTER_STATUS"] = "Estado";
$MESS["TASKS_FILTER_BY_TAG"] = "Por Etiqueta";
$MESS["TASKS_FILTER_SELECT"] = "seleccionar";
$MESS["TASKS_FILTER_CREAT_DATE"] = "Creado En";
$MESS["TASKS_FILTER_PICK_DATE"] = "Seleccionar Fecha en el Calendario";
$MESS["TASKS_FILTER_SHOW_SUBORDINATE"] = "mostrar tareas subordinadas";
$MESS["TASKS_FILTER_FIND"] = "Buscar";
$MESS["TASKS_FILTER_CLOSE_DATE"] = "Cerrado El";
$MESS["TASKS_FILTER_ACTIVE_DATE"] = "Fue activado";
$MESS["TASKS_FILTER_ADV_IN_REPORT"] = "en reporte";
$MESS["TASKS_CANCEL"] = "Cancelar";
$MESS["TASKS_FILTER_WORKGROUP"] = "Grupo de trabajo";
$MESS["TASKS_FILTER_ID"] = "ID";
$MESS["TASKS_FILTER_MARKED"] = "calificado";
$MESS["TASKS_FILTER_OVERDUED"] = "vencido";
?>