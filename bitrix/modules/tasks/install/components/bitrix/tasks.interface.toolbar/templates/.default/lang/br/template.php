<?
$MESS["TASKS_COUNTER_TOTAL_PLURAL_0"] = "tarefa";
$MESS["TASKS_COUNTER_TOTAL_PLURAL_1"] = "tarefas";
$MESS["TASKS_COUNTER_TOTAL_PLURAL_2"] = "tarefas";
$MESS["TASKS_COUNTER_EXPIRED_PLURAL_0"] = "atrasada";
$MESS["TASKS_COUNTER_EXPIRED_PLURAL_1"] = "atrasada";
$MESS["TASKS_COUNTER_EXPIRED_PLURAL_2"] = "atrasada";
$MESS["TASKS_COUNTER_WO_DEADLINE_PLURAL_0"] = "sem prazo";
$MESS["TASKS_COUNTER_WO_DEADLINE_PLURAL_1"] = "sem prazo";
$MESS["TASKS_COUNTER_WO_DEADLINE_PLURAL_2"] = "sem prazo";
$MESS["TASKS_COUNTER_EXPIRED_CANDIDATES_PLURAL_0"] = "quase atrasada";
$MESS["TASKS_COUNTER_EXPIRED_CANDIDATES_PLURAL_1"] = "quase atrasada";
$MESS["TASKS_COUNTER_EXPIRED_CANDIDATES_PLURAL_2"] = "quase atrasada";
$MESS["TASKS_COUNTER_NEW_PLURAL_0"] = "não visualizada";
$MESS["TASKS_COUNTER_NEW_PLURAL_1"] = "não visualizada";
$MESS["TASKS_COUNTER_NEW_PLURAL_2"] = "não visualizada";
$MESS["TASKS_COUNTER_WAIT_CTRL_PLURAL_0"] = "na dependência de revisão";
$MESS["TASKS_COUNTER_WAIT_CTRL_PLURAL_1"] = "na dependência de revisão";
$MESS["TASKS_COUNTER_WAIT_CTRL_PLURAL_2"] = "na dependência de revisão";
$MESS["TASKS_COUNTER_EMPTY"] = "Não há nenhuma tarefa que necessite de atenção imediata";
$MESS["TASKS_SWITCHER_NAME"] = "Visualizar";
$MESS["TASKS_SWITCHER_ITEM_LIST"] = "Lista";
$MESS["TASKS_SWITCHER_ITEM_GANTT"] = "Gantt";
$MESS["TASKS_SWITCHER_ITEM_KANBAN"] = "Kanban";
$MESS["TASKS_SWITCHER_ITEM_REPORTS"] = "Relatórios";
$MESS["TASKS_COUNTER_TOTAL"] = "Tarefas:";
$MESS["TASKS_GROUP_COUNTERS_SOON"] = "Em breve, contadores de tarefas do projeto";
?>