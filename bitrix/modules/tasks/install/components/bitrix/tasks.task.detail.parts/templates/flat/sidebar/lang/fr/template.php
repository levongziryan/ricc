<?
$MESS["TASKS_SIDEBAR_STATUS"] = "Statut";
$MESS["TASKS_SIDEBAR_START_DATE"] = "depuis";
$MESS["TASKS_SIDEBAR_START"] = "Démarrer";
$MESS["TASKS_SIDEBAR_FINISH"] = "Fin";
$MESS["TASKS_SIDEBAR_DURATION"] = "Durée";
$MESS["TASKS_SIDEBAR_IN_REPORT"] = "En rapport";
$MESS["TASKS_SIDEBAR_IN_REPORT_YES"] = "Oui";
$MESS["TASKS_SIDEBAR_IN_REPORT_NO"] = "Non";
$MESS["TASKS_SIDEBAR_REPEAT"] = "Récurrent";
$MESS["TASKS_SIDEBAR_TEMPLATE"] = "modèle";
$MESS["TASKS_SIDEBAR_CHANGE"] = "changer";
$MESS["TASKS_SIDEBAR_ACCOMPLICES"] = "Participants";
$MESS["TASKS_SIDEBAR_AUDITORS"] = "Observateurs";
$MESS["TASKS_SIDEBAR_ADD_ACCOMPLICES"] = "Ajouter des participants";
$MESS["TASKS_SIDEBAR_ADD_AUDITORS"] = "Ajouter des observateurs";
$MESS["TASKS_SIDEBAR_DEADLINE_NO"] = "Non";
$MESS["TASKS_SIDEBAR_STOP_WATCH_CONFIRM"] = "Voulez-vous vraiment ne plus être observateur sur cette tâche ?";
$MESS["TASKS_SIDEBAR_CREATED_DATE"] = "Créé le";
$MESS["TASKS_SIDEBAR_REMINDER"] = "Rappel";
$MESS["TASKS_SIDEBAR_TIME_SPENT_IN_LOGS"] = "Durée";
$MESS["TASKS_SIDEBAR_TIME_ESTIMATE"] = "Estimer";
$MESS["TASKS_SIDEBAR_REGULAR_TASK"] = "Tâche récurrente";
$MESS["TASKS_SIDEBAR_TASK_REPEATS"] = "La tâche se répète";
$MESS["TASKS_SIDEBAR_TASK_OVERDUE"] = "La tâche est en retard !";
$MESS["TASKS_SIDEBAR_TASK_CREATED_BY_TEMPLATE"] = "La tâche a été automatiquement créée grâce à un modèle";
$MESS["TASKS_SIDEBAR_TEMPLATE_NOT_ACCESSIBLE"] = "Impossible de voir le modèle d'une tâche récurrente";
$MESS["TASKS_TTDP_TEMPLATE_USER_VIEW_ORIGINATOR"] = "Créé par";
$MESS["TASKS_TTDP_TEMPLATE_USER_VIEW_AUDITORS"] = "Observateurs";
$MESS["TASKS_TTDP_TEMPLATE_USER_VIEW_ACCOMPLICES"] = "Participants";
$MESS["TASKS_TTDP_TEMPLATE_USER_VIEW_ENTER_AUDITOR"] = "observer";
$MESS["TASKS_TTDP_TEMPLATE_USER_VIEW_LEAVE_AUDITOR"] = "ne pas suivre";
$MESS["TASKS_TTDP_TEMPLATE_USER_VIEW_LEAVE_AUDITOR_CONFIRM"] = "Vous risquez de ne plus voir cette tâche si vous n'êtes plus un observateur. Voulez-vous supprimer votre rôle d'observateur ?";
$MESS["TASKS_TTDP_TEMPLATE_COPY_CURRENT_URL"] = "Copiez le lien de la tâche dans le Presse-papiers";
$MESS["TASKS_TTDP_TEMPLATE_USER_VIEW_RESPONSIBLE"] = "Personne responsable";
?>