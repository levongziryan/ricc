<?
$MESS["TASKS_TUP_TEMPLATE_PROMPT"] = "Hay tareas en su Bitrix24 que incluyen archivos cargados usando el módulo de actualización de documentos obsoletos. Las tareas pueden ahora utilizar sólo el nuevo Drive; usted tendrá que convertir los archivos antiguos para preservarlos.";
$MESS["TASKS_TUP_TEMPLATE_START"] = "Ejecutar la conversión";
$MESS["TASKS_TUP_TEMPLATE_SUCCESS"] = "Los archivos se han convertido con éxito";
$MESS["TASKS_TUP_TEMPLATE_CLOSE_NOTIFICATION"] = "No convertir";
?>