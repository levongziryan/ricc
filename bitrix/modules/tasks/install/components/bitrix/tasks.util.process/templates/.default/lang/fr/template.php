<?
$MESS["TASKS_TUP_TEMPLATE_PROMPT"] = "Il existe des tâches dans votre Bitrix24 qui comprennent des fichiers téléchargés en utilisant le module de gestion de documents obsolète. Les Tâches peuvent maintenant utiliser uniquement le nouveau Drive; vous devez convertir les anciens fichiers pour pouvoir les conserver.";
$MESS["TASKS_TUP_TEMPLATE_START"] = "Exécuter la conversion";
$MESS["TASKS_TUP_TEMPLATE_SUCCESS"] = "Les fichiers ont été convertis avec succès";
$MESS["TASKS_TUP_TEMPLATE_CLOSE_NOTIFICATION"] = "Ne pas convertir";
?>