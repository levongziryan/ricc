<?
$MESS["TASKS_TUP_TEMPLATE_PROMPT"] = "Existem tarefas em seu Bitrix24 que incluem arquivos carregados usando o módulo desatualizado de manipulação de documento. Agora, as Tarefas podem usar somente o novo Drive; você tem que converter os arquivos antigos para preservá-los.";
$MESS["TASKS_TUP_TEMPLATE_START"] = "Executar a conversão";
$MESS["TASKS_TUP_TEMPLATE_SUCCESS"] = "Os arquivos foram convertidos com sucesso";
$MESS["TASKS_TUP_TEMPLATE_CLOSE_NOTIFICATION"] = "Não converter";
?>