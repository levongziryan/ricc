<?
$MESS["TASKS_PROJECTS_TASK_ALL"] = "Total";
$MESS["TASKS_PROJECTS_SUMMARY"] = "Total de tâches des projets";
$MESS["TASKS_PROJECTS_TASK_COMPLETE"] = "Achevé(e)s";
$MESS["TASKS_PROJECTS_TASK_IN_WORK"] = "Tâches en cours";
$MESS["TASKS_PROJECTS_MEMBERS_PLURAL_0"] = "et #SPAN# encore #COUNT# participants #/SPAN#";
$MESS["TASKS_PROJECTS_MEMBERS_PLURAL_1"] = "et #SPAN# encore #COUNT# des participants #/SPAN#";
$MESS["TASKS_PROJECTS_MEMBERS_PLURAL_2"] = "et #SPAN# encore #COUNT# des participants #/SPAN#";
$MESS["TASKS_PROJECTS_OVERVIEW_NO_DATA"] = "Il n'y a aucunes tâches dans les projets";
$MESS["TASKS_PROJECTS_WITH_MY_MEMBERSHIP"] = "Projets avec ma participation";
$MESS["TASKS_PROJECTS_HEADS"] = "Les cadres";
$MESS["TASKS_PROJECTS_HEAD"] = "Dirigeant";
?>