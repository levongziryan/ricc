<?
$MESS["TASKS_PROJECTS_OVERVIEW_NO_DATA"] = "Actualmente no hay tareas de proyectos";
$MESS["TASKS_PROJECTS_WITH_MY_MEMBERSHIP"] = "Estoy en el proyecto";
$MESS["TASKS_PROJECTS_TASK_IN_WORK"] = "Tareas en progreso";
$MESS["TASKS_PROJECTS_TASK_COMPLETE"] = "Completado";
$MESS["TASKS_PROJECTS_TASK_ALL"] = "Total";
$MESS["TASKS_PROJECTS_SUMMARY"] = "Total de tareas por proyectos ";
$MESS["TASKS_PROJECTS_HEAD"] = "Supervisor";
$MESS["TASKS_PROJECTS_HEADS"] = "Supervisores";
$MESS["TASKS_PROJECTS_MEMBERS_PLURAL_0"] = "y #SPAN#más #COUNT# miembros#/SPAN#";
$MESS["TASKS_PROJECTS_MEMBERS_PLURAL_1"] = "y #SPAN#más #COUNT# miembros#/SPAN#";
$MESS["TASKS_PROJECTS_MEMBERS_PLURAL_2"] = "y #SPAN#más #COUNT# miembros#/SPAN#";
?>