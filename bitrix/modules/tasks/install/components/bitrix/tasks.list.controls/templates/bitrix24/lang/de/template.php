<?
$MESS["TASK_TOOLBAR_FILTER_TREE"] = "Mit Teilaufgaben";
$MESS["TASK_TOOLBAR_FILTER_LIST"] = "Flache Ansicht";
$MESS["TASK_TOOLBAR_FILTER_GANTT"] = "Gantt-Diagramm";
$MESS["TASK_TOOLBAR_FILTER_BUTTON"] = "Filter";
$MESS["TASK_TOOLBAR_FILTER_REPORTS"] = "Berichte";
$MESS["TASKS_PANEL_TAB_ALL"] = "Alle";
$MESS["TASKS_PANEL_TAB_REPORTS"] = "Berichte";
$MESS["TASKS_PANEL_TAB_MANAGE"] = "Überwache";
$MESS["TASKS_PANEL_TAB_PROJECTS"] = "Projekte";
$MESS["TASKS_PANEL_TAB_VIEW_SELECTOR_TITLE"] = "Anzeigen";
$MESS["TASKS_PANEL_TAB_BACK_TO_LIST"] = "Zur Liste";
$MESS["TASKS_PANEL_HUMAN_FILTER_STRING_MATRIX"] = "#SPAN_TEXT#Ich #ROLE#, Aufgaben werden angezeigt: #STATUS##SPAN_END#";
$MESS["TASKS_PANEL_HUMAN_FILTER_STRING_MATRIX_FOR_RESPONSIBLE"] = "#SPAN_TEXT#Ich bin #ROLE#, angezeigt werden Aufgaben #STATUS##SPAN_END##SPAN_TEXT# von #SPAN_END##ORIGINATOR#";
$MESS["TASKS_PANEL_HUMAN_FILTER_STRING_MATRIX_FOR_ORIGINATOR"] = "#SPAN_TEXT#Ich bin #ROLE#, angezeigt werden Aufgaben #STATUS##SPAN_END##SPAN_TEXT# für #SPAN_END##RESPONSIBLE#";
$MESS["TASKS_PANEL_HUMAN_FILTER_STRING_ANY_ORIGINATOR"] = "allen";
$MESS["TASKS_PANEL_HUMAN_FILTER_STRING_ANY_RESPONSIBLE"] = "alle";
$MESS["TASKS_PANEL_HUMAN_FILTER_STRING_RESPONSIBLE_IS_ME"] = "mich";
$MESS["TASKS_PANEL_EXPLANATION_PREFIX"] = "Aufgaben:";
$MESS["TASKS_PANEL_EXPLANATION_NEW_TASKS_SUFFIX_PLURAL_0"] = "nicht angezeigt";
$MESS["TASKS_PANEL_EXPLANATION_NEW_TASKS_SUFFIX_PLURAL_1"] = "nicht angezeigt";
$MESS["TASKS_PANEL_EXPLANATION_NEW_TASKS_SUFFIX_PLURAL_2"] = "nicht angezeigt";
$MESS["TASKS_PANEL_EXPLANATION_EXPIRED_TASKS_SUFFIX_PLURAL_0"] = "überfällig";
$MESS["TASKS_PANEL_EXPLANATION_EXPIRED_TASKS_SUFFIX_PLURAL_1"] = "überfällig";
$MESS["TASKS_PANEL_EXPLANATION_EXPIRED_TASKS_SUFFIX_PLURAL_2"] = "überfällig";
$MESS["TASKS_PANEL_EXPLANATION_EXPIRED_SOON_TASKS_SUFFIX_PLURAL_0"] = "fast überfällig";
$MESS["TASKS_PANEL_EXPLANATION_EXPIRED_SOON_TASKS_SUFFIX_PLURAL_1"] = "fast überfällig";
$MESS["TASKS_PANEL_EXPLANATION_EXPIRED_SOON_TASKS_SUFFIX_PLURAL_2"] = "fast überfällig";
$MESS["TASKS_PANEL_EXPLANATION_WAIT_CTRL_TASKS_SUFFIX_PLURAL_0"] = "müssen kontrolliert werden";
$MESS["TASKS_PANEL_EXPLANATION_WAIT_CTRL_TASKS_SUFFIX_PLURAL_1"] = "müssen kontrolliert werden";
$MESS["TASKS_PANEL_EXPLANATION_WAIT_CTRL_TASKS_SUFFIX_PLURAL_2"] = "müssen kontrolliert werden";
$MESS["TASKS_PANEL_EXPLANATION_WO_DEADLINE_TASKS_SUFFIX_PLURAL_0"] = "unbefristet";
$MESS["TASKS_PANEL_EXPLANATION_WO_DEADLINE_TASKS_SUFFIX_PLURAL_1"] = "unbefristet";
$MESS["TASKS_PANEL_EXPLANATION_WO_DEADLINE_TASKS_SUFFIX_PLURAL_2"] = "unbefristet";
$MESS["TASKS_PANEL_EXPLANATION_AND_WORD"] = "und";
$MESS["TASKS_PANEL_SWITCH_TO_ADVANCED_FILTER"] = "Erweiterten Filter benutzen";
$MESS["TASKS_PANEL_BTN_MORE"] = "Mehr";
$MESS["TASKS_PANEL_TAB_APPLICATIONS"] = "Anwendungen";
$MESS["TASKS_PANEL_HUMAN_FILTER_STRING_FOR"] = "für";
$MESS["TASKS_PANEL_HUMAN_FILTER_STRING_FROM"] = "von";
$MESS["TASKS_PANEL_HUMAN_FILTER_STRING_MATRIX_FOR_AUDITOR_ACCOMPLICE"] = "#SPAN_TEXT#Ich #ROLE#, angezeigt werden Aufgaben #SPAN_END# #STATUS##SPAN_TEXT#, #SPAN_END##TYPE##SPAN_TEXT# #SPAN_END##PERSON#";
$MESS["TASKS_PANEL_HUMAN_FILTER_STRING_MATRIX_FOR_SPECIAL_PRESET"] = "#SPAN_TEXT#Angezeigt werden Aufgaben#SPAN_END# #STATUS##SPAN_TEXT#, #SPAN_END##TYPE##SPAN_TEXT# #SPAN_END##PERSON#";
?>