<?
$MESS["TASK_TOOLBAR_FILTER_TREE"] = "Incluir subtareas";
$MESS["TASK_TOOLBAR_FILTER_LIST"] = "Vista plana";
$MESS["TASK_TOOLBAR_FILTER_GANTT"] = "Gráfico de Gantt";
$MESS["TASK_TOOLBAR_FILTER_BUTTON"] = "Filtro";
$MESS["TASK_TOOLBAR_FILTER_REPORTS"] = "Reportes";
$MESS["TASKS_PANEL_TAB_ALL"] = "Todas";
$MESS["TASKS_PANEL_TAB_REPORTS"] = "Reportes";
$MESS["TASKS_PANEL_TAB_MANAGE"] = "Supervisión";
$MESS["TASKS_PANEL_TAB_PROJECTS"] = "Proyectos";
$MESS["TASKS_PANEL_TAB_TEMPLATES"] = "Plantillas";
$MESS["TASKS_PANEL_TAB_VIEW_SELECTOR_TITLE"] = "Ver";
$MESS["TASKS_PANEL_TAB_BACK_TO_LIST"] = "Volver a la lista";
$MESS["TASKS_PANEL_TAB_EMPLOYEE_PLAN"] = "Participantes";
$MESS["TASKS_PANEL_HUMAN_FILTER_STRING_MATRIX"] = "#SPAN_TEXT#I #ROLE#, mostrar tareas #STATUS##SPAN_END#";
$MESS["TASKS_PANEL_HUMAN_FILTER_STRING_MATRIX_FOR_RESPONSIBLE"] = "#SPAN_TEXT#I'm #ROLE#, mostrar tareas #STATUS##SPAN_END##SPAN_TEXT# por #SPAN_END##ORIGINATOR#";
$MESS["TASKS_PANEL_HUMAN_FILTER_STRING_MATRIX_FOR_ORIGINATOR"] = "#SPAN_TEXT#I have #ROLE#, mostrar tareas #STATUS##SPAN_END##SPAN_TEXT# para #SPAN_END##RESPONSIBLE#";
$MESS["TASKS_PANEL_HUMAN_FILTER_STRING_ANY_ORIGINATOR"] = "todos";
$MESS["TASKS_PANEL_HUMAN_FILTER_STRING_ANY_RESPONSIBLE"] = "todos";
$MESS["TASKS_PANEL_HUMAN_FILTER_STRING_RESPONSIBLE_IS_ME"] = "mí";
$MESS["TASKS_PANEL_EXPLANATION_PREFIX"] = "Tareas:";
$MESS["TASKS_PANEL_EXPLANATION_NEW_TASKS_SUFFIX_PLURAL_0"] = "no vista";
$MESS["TASKS_PANEL_EXPLANATION_NEW_TASKS_SUFFIX_PLURAL_1"] = "no vistas";
$MESS["TASKS_PANEL_EXPLANATION_NEW_TASKS_SUFFIX_PLURAL_2"] = "no vistas";
$MESS["TASKS_PANEL_EXPLANATION_EXPIRED_TASKS_SUFFIX_PLURAL_0"] = "atrasada";
$MESS["TASKS_PANEL_EXPLANATION_EXPIRED_TASKS_SUFFIX_PLURAL_1"] = "atrasadas";
$MESS["TASKS_PANEL_EXPLANATION_EXPIRED_TASKS_SUFFIX_PLURAL_2"] = "atrasadas";
$MESS["TASKS_PANEL_EXPLANATION_EXPIRED_SOON_TASKS_SUFFIX_PLURAL_0"] = "muy pronto";
$MESS["TASKS_PANEL_EXPLANATION_EXPIRED_SOON_TASKS_SUFFIX_PLURAL_1"] = "muy pronto";
$MESS["TASKS_PANEL_EXPLANATION_EXPIRED_SOON_TASKS_SUFFIX_PLURAL_2"] = "muy pronto";
$MESS["TASKS_PANEL_EXPLANATION_WAIT_CTRL_TASKS_SUFFIX_PLURAL_0"] = "pendiente de aprobación";
$MESS["TASKS_PANEL_EXPLANATION_WAIT_CTRL_TASKS_SUFFIX_PLURAL_1"] = "pendientes de aprobación";
$MESS["TASKS_PANEL_EXPLANATION_WAIT_CTRL_TASKS_SUFFIX_PLURAL_2"] = "pendientes de aprobación";
$MESS["TASKS_PANEL_EXPLANATION_WO_DEADLINE_TASKS_SUFFIX_PLURAL_0"] = "sin fecha límite";
$MESS["TASKS_PANEL_EXPLANATION_WO_DEADLINE_TASKS_SUFFIX_PLURAL_1"] = "sin fecha límite";
$MESS["TASKS_PANEL_EXPLANATION_WO_DEADLINE_TASKS_SUFFIX_PLURAL_2"] = "sin fecha límite";
$MESS["TASKS_PANEL_EXPLANATION_WO_DEADLINE_TASKS_RESPONSIBLE"] = "(asignado a mí)";
$MESS["TASKS_PANEL_EXPLANATION_WO_DEADLINE_TASKS_ORIGINATOR"] = "(asignado por mí)";
$MESS["TASKS_PANEL_EXPLANATION_AND_WORD"] = "y";
$MESS["TASKS_PANEL_SWITCH_TO_ADVANCED_FILTER"] = "Utilizar filtro avanzado";
$MESS["TASKS_PANEL_BTN_MORE"] = "Más";
$MESS["TASKS_PANEL_TAB_APPLICATIONS"] = "Aplicaciones";
$MESS["TASKS_PANEL_HUMAN_FILTER_STRING_FOR"] = "para";
$MESS["TASKS_PANEL_HUMAN_FILTER_STRING_FROM"] = "por";
$MESS["TASKS_PANEL_HUMAN_FILTER_STRING_MATRIX_FOR_AUDITOR_ACCOMPLICE"] = "#SPAN_TEXT#I #ROLE#, mostrar tareas #SPAN_END# #STATUS##SPAN_TEXT#, #SPAN_END##TYPE##SPAN_TEXT# #SPAN_END##PERSON#";
$MESS["TASKS_PANEL_HUMAN_FILTER_STRING_MATRIX_FOR_SPECIAL_PRESET"] = "#SPAN_TEXT#Mostrar tareas#SPAN_END# #STATUS##SPAN_TEXT#, #SPAN_END##TYPE##SPAN_TEXT# #SPAN_END##PERSON#";
$MESS["TASKS_PANEL_SORTED_BY"] = "ordenar por";
$MESS["TASKS_PANEL_FILTER_STATUS_LABEL"] = "tareas";
$MESS["TASKS_PANEL_SORTING_DIRECTION_ASC"] = "Orden ascendente";
$MESS["TASKS_PANEL_SORTING_DIRECTION_DESC"] = "Orden descendente";
$MESS["TASKS_QUICK_FORM_DESC_PLACEHOLDER"] = "Descripción de la tarea";
$MESS["TASKS_QUICK_FORM_TITLE_PLACEHOLDER"] = "Nuevas tareas";
$MESS["TASKS_QUICK_FORM_AFTER_SAVE_MESSAGE"] = "La tarea &quot;#TASK_NAME#&quot; ha sido guardada.";
$MESS["TASKS_QUICK_FORM_OPEN_TASK"] = "Abrir tarea";
$MESS["TASKS_QUICK_FORM_HIGHLIGHT_TASK"] = "Mostar en la lista";
$MESS["TASKS_PANEL_VM_HINT_TITLE"] = "Alternar la vista";
$MESS["TASKS_PANEL_VM_HINT_BODY"] = "Este botón cambia a vista de lista normal";
$MESS["TASKS_PANEL_VM_HINT_DISABLE"] = "No volver a mostrar";
$MESS["TASKS_PANEL_GANTT_HINT_TITLE"] = "Establecer dependencias entre tareas para administrar fácilmente la línea de tiempo del proyecto.";
$MESS["TASKS_PANEL_GANTT_HINT_BODY"] = "Al cambiar de línea de tiempo para la tarea principal, todos las fechas de inicio/finalización de las tareas dependientes cambiarán automáticamente. Simplemente arrastre la flecha de una tarea a otra para crear dependencia.";
$MESS["TASKS_TASK_COMPONENT_TEMPLATE_TO_LIST"] = "Volver a la lista";
?>