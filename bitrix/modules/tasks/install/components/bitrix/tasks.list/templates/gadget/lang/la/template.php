<?
$MESS["INTASK_LIST_EMPTY"] = "No hay tareas asignadas.";
$MESS["INTASK_TASKPRIORITY"] = "Prioridad";
$MESS["INTASK_TASKSTATUS"] = "Estado";
$MESS["INTASK_TO_DATE_TLP"] = "hasta #DATE#";
$MESS["INTASK_FROM_DATE_TLP"] = "desde #DATE#";
$MESS["INTASK_NO_DATE_TLP"] = "fecha de vencimiento no asignada";
$MESS["INTASK_TASKASSIGNEDTO"] = "Asignado a";
$MESS["TASKS_PRIORITY_0"] = "Baja";
$MESS["TASKS_PRIORITY_1"] = "Normal";
$MESS["TASKS_PRIORITY_2"] = "Alta";
$MESS["TASKS_STATUS_1"] = "Nuevo";
$MESS["TASKS_STATUS_2"] = "Pendiente";
$MESS["TASKS_STATUS_3"] = "En Progreso";
$MESS["TASKS_STATUS_5"] = "Finalizada";
$MESS["TASKS_STATUS_6"] = "Diferida";
$MESS["TASKS_DEADLINE"] = "Fecha límite";
$MESS["TASKS_STATUS_4"] = "Supuestamente finalizado";
$MESS["TASKS_STATUS_7"] = "Declinada";
?>