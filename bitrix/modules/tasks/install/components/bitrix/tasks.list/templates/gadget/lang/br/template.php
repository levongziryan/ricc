<?
$MESS["INTASK_LIST_EMPTY"] = "Nenhuma tarefas atribuída.";
$MESS["INTASK_TASKPRIORITY"] = "Prioridade";
$MESS["INTASK_TASKSTATUS"] = "Status";
$MESS["INTASK_TO_DATE_TLP"] = "até #DATE#";
$MESS["INTASK_FROM_DATE_TLP"] = "de #DATE#";
$MESS["INTASK_NO_DATE_TLP"] = "A data de vencimento não foi atribuído";
$MESS["INTASK_TASKASSIGNEDTO"] = "designado para";
$MESS["TASKS_PRIORITY_0"] = "Baixo";
$MESS["TASKS_PRIORITY_1"] = "Normal";
$MESS["TASKS_PRIORITY_2"] = "Alto";
$MESS["TASKS_STATUS_1"] = "Novo";
$MESS["TASKS_STATUS_3"] = "Em andamento";
$MESS["TASKS_STATUS_4"] = "Supostamente concluída";
$MESS["TASKS_STATUS_5"] = "Concluído";
$MESS["TASKS_STATUS_6"] = "Deferido";
$MESS["TASKS_STATUS_7"] = "Recusado";
$MESS["TASKS_DEADLINE"] = "Prazo";
$MESS["TASKS_STATUS_2"] = "Pendente";
?>