<?
$MESS["INTASK_LIST_EMPTY"] = "Nenhuma tarefas atribuído.";
$MESS["INTASK_TASKPRIORITY"] = "Prioridade";
$MESS["INTASK_TASKSTATUS"] = "Status";
$MESS["INTASK_TO_DATE_TLP"] = "Até #DATE#";
$MESS["INTASK_FROM_DATE_TLP"] = "A partir de #DATE#";
$MESS["INTASK_NO_DATE_TLP"] = "data de vencimento não é atribuído";
$MESS["INTASK_TASKASSIGNEDTO"] = "designado para";
$MESS["TASKS_PRIORITY_0"] = "Baixo";
$MESS["TASKS_PRIORITY_1"] = "Normal Alto ";
$MESS["TASKS_PRIORITY_2"] = "Alto";
$MESS["TASKS_STATUS_1"] = "Novo";
$MESS["TASKS_STATUS_3"] = "Em progresso";
$MESS["TASKS_STATUS_4"] = "supostamente concluída";
$MESS["TASKS_STATUS_5"] = "Completo";
$MESS["TASKS_STATUS_6"] = "Diferido";
$MESS["TASKS_STATUS_7"] = "Recusado ";
$MESS["TASKS_DEADLINE"] = "prazo de entrega";
$MESS["TASKS_STATUS_2"] = "pendente";
?>