<?
$MESS["TASKS_MODULE_NOT_AVAILABLE_IN_THIS_EDITION"] = "El módulo Tasks está disponible en esta edición.";
$MESS["TASKS_MODULE_NOT_FOUND"] = "El módulo Tasks no está instalado.";
$MESS["SOCNET_MODULE_NOT_INSTALLED"] = "El módulo \"Social Network\" no está instalado.";
$MESS["FORUM_MODULE_NOT_INSTALLED"] = "El módulo Forum no está instalado.";
$MESS["TASKS_UNEXPECTED_ERROR"] = "Error no definido";
$MESS["TASKS_ACCESS_TO_GROUP_DENIED"] = "No se puede ver la lista de tareas para este grupo.";
$MESS["TASKS_TITLE_TASKS"] = "Tareas";
$MESS["TASKS_TITLE_GROUP_TASKS"] = "Tareas del Grupo";
$MESS["TASKS_TITLE_MY_TASKS"] = "Mis Tareas";
$MESS["TASKS_ACCESS_DENIED"] = "Usted no tiene permisos para ver esta lista de tareas";
?>