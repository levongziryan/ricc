<?
$MESS["TASKS_PRIORITY"] = "Prioridade";
$MESS["TASKS_PRIORITY_0"] = "Baixo";
$MESS["TASKS_PRIORITY_1"] = "Normal";
$MESS["TASKS_PRIORITY_2"] = "Alto";
$MESS["TASKS_MARK"] = "Contagem";
$MESS["TASKS_MARK_NONE"] = "Nenhum";
$MESS["TASKS_MARK_P"] = "Positivo";
$MESS["TASKS_MARK_N"] = "Negativo";
$MESS["TASKS_DOUBLE_CLICK"] = "Dê um Clique Duplo para ver";
?>