<?
$MESS["TASKS_LIST_ITEMS_YES"] = "Sim";
$MESS["TASKS_LIST_ITEMS_NO"] = "Não";
$MESS["TASKS_LIST_ITEMS_STATUS_1"] = "Nova";
$MESS["TASKS_LIST_ITEMS_STATUS_2"] = "Pendente";
$MESS["TASKS_LIST_ITEMS_STATUS_3"] = "Em andamento";
$MESS["TASKS_LIST_ITEMS_STATUS_4"] = "Aguardando controle";
$MESS["TASKS_LIST_ITEMS_STATUS_5"] = "Concluída";
$MESS["TASKS_LIST_ITEMS_STATUS_6"] = "Adiada";
$MESS["TASKS_LIST_ITEMS_STATUS_7"] = "Recusada";
$MESS["TASKS_LIST_ITEMS_PRIORITY_0"] = "Baixa";
$MESS["TASKS_LIST_ITEMS_PRIORITY_1"] = "Normal";
$MESS["TASKS_LIST_ITEMS_PRIORITY_2"] = "Alta";
$MESS["TASKS_LIST_CRM_TYPE_CO"] = "Empresa";
$MESS["TASKS_LIST_CRM_TYPE_C"] = "Contato";
$MESS["TASKS_LIST_CRM_TYPE_L"] = "Lead";
$MESS["TASKS_LIST_CRM_TYPE_D"] = "Negócio";
$MESS["TASKS_LIST_NOT_SET"] = "Novo empregado";
?>