<?
$MESS["TASKS_LIST_ITEMS_STATUS_3"] = "En cours d'exécution";
$MESS["TASKS_LIST_ITEMS_PRIORITY_2"] = "Augmenté";
$MESS["TASKS_LIST_ITEMS_YES"] = "Oui";
$MESS["TASKS_LIST_ITEMS_STATUS_2"] = "Attend l'exécution";
$MESS["TASKS_LIST_ITEMS_STATUS_4"] = "En attente de contrôle";
$MESS["TASKS_LIST_ITEMS_STATUS_5"] = "Achevé(e)s";
$MESS["TASKS_LIST_CRM_TYPE_CO"] = "Entreprise";
$MESS["TASKS_LIST_CRM_TYPE_C"] = "Client";
$MESS["TASKS_LIST_CRM_TYPE_L"] = "Lead";
$MESS["TASKS_LIST_ITEMS_NO"] = "Non";
$MESS["TASKS_LIST_ITEMS_PRIORITY_0"] = "Bas";
$MESS["TASKS_LIST_ITEMS_STATUS_1"] = "Créer";
$MESS["TASKS_LIST_ITEMS_PRIORITY_1"] = "Moyen";
$MESS["TASKS_LIST_ITEMS_STATUS_7"] = "Est rejetée";
$MESS["TASKS_LIST_ITEMS_STATUS_6"] = "Différé";
$MESS["TASKS_LIST_CRM_TYPE_D"] = "Affaire";
$MESS["TASKS_LIST_NOT_SET"] = "Nouvel employé";
?>