<?
$MESS["TASKS_SONET_LOG_DESCRIPTION"] = "Descripcion";
$MESS["TASKS_SONET_LOG_RESPONSIBLE_ID"] = "persona responsable";
$MESS["TASKS_SONET_LOG_LABEL_TITLE"] = "Tareas";
$MESS["TASKS_SONET_LOG_STATUS_CHANGED"] = "El estado de la tarea ha cambiado";
$MESS["TASKS_SONET_LOG_STATUS"] = "Estado";
$MESS["TASKS_SONET_LOG_TAGS"] = "Etiquetas:";
?>