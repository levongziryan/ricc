<?
$MESS["TASKS_BTN_ADD_TASK"] = "Nouvelle tâche";
$MESS["TASKS_BTN_ADD_TASK_BY_TEMPLATE"] = "Nouvelle tâche d'après le modèle";
$MESS["TASKS_BTN_LIST_TASK_TEMPLATE"] = "Tous les modèles";
$MESS["TASKS_AJAX_ERROR_LOAD_TEMPLATES"] = "Erreur lors du chargement de la liste des modèles";
$MESS["TASKS_AJAX_LOAD_TEMPLATES"] = "Chargement de la liste...";
$MESS["TASKS_AJAX_EMPTY_TEMPLATES"] = "Il n’y a aucun modèle de tâche";
$MESS["TASKS_BTN_GROUP_BY_SUBTASKS"] = "Grouper par sous-tâches";
$MESS["TASKS_BTN_EXPORT"] = "Exporter la liste";
$MESS["TASKS_BTN_EXPORT_OUTLOOK"] = "vers Microsoft Outlook";
$MESS["TASKS_BTN_EXPORT_EXCEL"] = "vers Microsoft Excel";
$MESS["TASKS_BTN_GROUP_WO"] = "Aucun projet";
$MESS["TASKS_BTN_GROUP_SELECT"] = "Sélectionner...";
$MESS["TASKS_ADD_QUICK_TASK"] = "Créer une tâche rapide";
$MESS["TASKS_USER_SORT"] = "Trier";
$MESS["TASKS_BTN_SORT_DIR_ASC"] = "Croissant";
$MESS["TASKS_BTN_SORT_DIR_DESC"] = "Décroissant";
$MESS["TASKS_BTN_SORT_ID"] = "ID";
$MESS["TASKS_BTN_SORT_TITLE"] = "Intitulé";
$MESS["TASKS_BTN_SORT_DEADLINE"] = "Date limite";
$MESS["TASKS_BTN_SORT_ORIGINATOR_NAME"] = "Créé par";
$MESS["TASKS_BTN_SORT_RESPONSIBLE_NAME"] = "Personne responsable";
$MESS["TASKS_BTN_SORT_PRIORITY"] = "Priorité";
$MESS["TASKS_BTN_SORT_MARK"] = "Évaluation";
$MESS["TASKS_BTN_SORT_TIME_ESTIMATE"] = "Temps nécessaire estimé";
$MESS["TASKS_BTN_SORT_ALLOW_TIME_TRACKING"] = "Suivre le temps écoulé";
$MESS["TASKS_BTN_SORT_CREATED_DATE"] = "Créé le";
$MESS["TASKS_BTN_SORT_CHANGED_DATE"] = "Modifié le";
$MESS["TASKS_BTN_SORT_CLOSED_DATE"] = "Fermé le";
$MESS["TASKS_BTN_SORT_SORTING"] = "Mon tri";
$MESS["TASKS_BTN_SYNC"] = "Synchroniser la liste";
$MESS["TASKS_BTN_SYNC_OUTLOOK"] = "avec Microsoft Outlook";
?>