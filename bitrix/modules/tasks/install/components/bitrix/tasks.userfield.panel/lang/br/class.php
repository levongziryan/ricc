<?
$MESS["TASKS_TUFE_EMPTY_LABEL"] = "O nome de campo não está especificado";
$MESS["TASKS_TUFE_UF_USAGE_RESTRICTED"] = "Você não pode usar campos personalizados no seu plano.";
$MESS["TASKS_TUFE_UF_MANAGING_RESTRICTED"] = "Você não pode gerenciar campos personalizados no seu plano.";
$MESS["TASKS_TUFE_UF_USAGE_RESTRICTED_MANDATORY"] = "Não é possível criar um campo obrigatório.";
?>