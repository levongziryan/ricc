<?
$MESS["TASKS_TUFE_EMPTY_LABEL"] = "No se especifica el nombre del campo";
$MESS["TASKS_TUFE_UF_USAGE_RESTRICTED"] = "No se puede utilizar campos personalizados en su plan.";
$MESS["TASKS_TUFE_UF_MANAGING_RESTRICTED"] = "No se puede administrar campos personalizados en su plan.";
$MESS["TASKS_TUFE_UF_USAGE_RESTRICTED_MANDATORY"] = "No se puede crear un campo obligatorio.";
?>