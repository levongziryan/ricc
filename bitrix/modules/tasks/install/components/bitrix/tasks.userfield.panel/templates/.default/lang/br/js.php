<?
$MESS["TASKS_TUFP_NEW_FIELD_STRING"] = "Novo campo de texto";
$MESS["TASKS_TUFP_NEW_FIELD_DOUBLE"] = "Novo campo de número";
$MESS["TASKS_TUFP_NEW_FIELD_DATETIME"] = "Novo campo de data";
$MESS["TASKS_TUFP_NEW_FIELD_BOOLEAN"] = "Novo campo booleano";
$MESS["TASKS_TUFP_FIELD_HIDE_CONFIRM"] = "Deseja ocultar o campo? Você pode mostrá-lo novamente clicando em \"Mostrar campo\" na parte inferior do formulário.";
$MESS["TASKS_TUFP_FIELD_HIDE_DELETE_CONFIRM"] = "Deseja excluir o campo ou apenas ocultá-lo para que não seja mostrado no formulário de editar tarefa?";
$MESS["TASKS_TUFP_SAVE_TO_ALL_CONFIRM"] = "A configuração do campo selecionado será atribuída a todos os usuários do portal. Continuar?";
$MESS["TASKS_TUFP_SAVE_SCHEME_TO_EVERYONE"] = "Configurar campo definido para todos os usuários";
$MESS["TASKS_TUFP_DRAG_N_DROP_ON"] = "Permitir arrastamento do campo";
$MESS["TASKS_TUFP_DRAG_N_DROP_OFF"] = "Desabilitar arrastamento do campo";
$MESS["TASKS_TUFP_FIELD_MANDATORY"] = "obrigatório";
$MESS["TASKS_TUFP_LICENSE_TITLE"] = "Adicionar seus campos a Tarefas";
$MESS["TASKS_TUFP_LICENSE_BODY"] = "
Os campos personalizados estão disponíveis nos planos \"Standard\" e \"Professional\".<br />
 <br />
Coloque mais campos em suas tarefas (ex: \"Orçamento do projeto\" ou \"Valor pré-pago\"). Eles estarão disponíveis para todos os usuários.";
?>