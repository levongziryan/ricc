<?
$MESS["TASKS_TUFP_NEW_FIELD_STRING"] = "Nouveau champ de texte";
$MESS["TASKS_TUFP_NEW_FIELD_DOUBLE"] = "Nouveau champ de numéro";
$MESS["TASKS_TUFP_NEW_FIELD_DATETIME"] = "Nouveau champ de date";
$MESS["TASKS_TUFP_NEW_FIELD_BOOLEAN"] = "Nouveau champ booléen";
$MESS["TASKS_TUFP_FIELD_HIDE_CONFIRM"] = "Voulez-vous masquer le champ? Vous pouvez l'afficher une nouvelle fois en cliquant sur \"Afficher le champ\" au bas du formulaire.";
$MESS["TASKS_TUFP_FIELD_HIDE_DELETE_CONFIRM"] = "Voulez-vous supprimer le champ ou simplement le masquer pour qu'il n'apparaisse pas dans le formulaire de modification de tâche?";
$MESS["TASKS_TUFP_SAVE_TO_ALL_CONFIRM"] = "La configuration de champ sélectionnée sera assignée à l'ensemble des utilisateurs du portail. Continuer?";
$MESS["TASKS_TUFP_SAVE_SCHEME_TO_EVERYONE"] = "Définir le réglage du champ pour tous les utilisateurs";
$MESS["TASKS_TUFP_DRAG_N_DROP_ON"] = "Activer le déplacement des champs";
$MESS["TASKS_TUFP_DRAG_N_DROP_OFF"] = "Désactiver le déplacement des champs";
$MESS["TASKS_TUFP_FIELD_MANDATORY"] = "requis";
$MESS["TASKS_TUFP_LICENSE_TITLE"] = "Ajouter des champs aux Tâches";
$MESS["TASKS_TUFP_LICENSE_BODY"] = "
Les champs personnalisés sont disponibles dans les abonnements \"Standard\" et \"Professional\".
<br />
Renforcez vos tâches avec plus de champs (par ex. \"Budget du projet\" ou \"Montant prépayé\"). Elles seront disponibles pour tous les utilisateurs.";
?>