<?
$MESS["TASKS_TT_TASKS_MODULE_NOT_INSTALLED"] = "El módulo Task no está instalado.";
$MESS["TASKS_TT_SOCIALNETWORK_MODULE_NOT_INSTALLED"] = "El módulo Social Network no está instalado.";
$MESS["TASKS_TT_FORUM_MODULE_NOT_INSTALLED"] = "El módulo Forum no está instalado.";
$MESS["TASKS_TT_NOT_FOUND_OR_NOT_ACCESSIBLE"] = "La tarea no se ha encontrado, o se ha denegado el acceso.";
$MESS["TASKS_TT_NOT_FOUND_OR_NOT_ACCESSIBLE_COPY"] = "Tarea a copiar no se ha encontrado o se ha denegado el acceso.";
$MESS["TASKS_TT_COPY_READ_ERROR"] = "Error al leer el objeto que se desea copiar";
?>