<?
$MESS["TASKS_TT_TASKS_MODULE_NOT_INSTALLED"] = "Le module Tasks n'est pas installé.";
$MESS["TASKS_TT_SOCIALNETWORK_MODULE_NOT_INSTALLED"] = "Le module Réseau social n'est pas installé.";
$MESS["TASKS_TT_FORUM_MODULE_NOT_INSTALLED"] = "Le module Forum n'est pas installé.";
$MESS["TASKS_TT_NOT_FOUND_OR_NOT_ACCESSIBLE"] = "La tâche est introuvable ou l'accès est refusé.";
$MESS["TASKS_TT_NOT_FOUND_OR_NOT_ACCESSIBLE_COPY"] = "La tâche à copier est introuvable ou l'accès est refusé.";
$MESS["TASKS_TT_COPY_READ_ERROR"] = "Erreur lors de la lecture de l'objet à copier";
?>