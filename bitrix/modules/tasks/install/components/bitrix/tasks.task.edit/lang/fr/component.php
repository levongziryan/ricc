<?
$MESS["TASKS_PRIORITY"] = "(niveau d'importance)";
$MESS["TASKS_STATUS_W"] = "Attend l'exécution";
$MESS["TASKS_STATUS_I"] = "En cours";
$MESS["TASKS_PRIORITY_H"] = "Augmenté";
$MESS["TASKS_DATE_MUST_BE_IN_FUTURE"] = "La date du rappel doit être dans le futur.";
$MESS["TASKS_STATUS_C"] = "Achevé(e)s";
$MESS["TASKS_TASK_CHANGED"] = "La tâche ##TASK_ID# est changée";
$MESS["TASKS_TASK_NOT_FOUND"] = "La tâche n'est pas trouvée et l'accès à lui est interdit.";
$MESS["SOCNET_MODULE_NOT_INSTALLED"] = "Module 'Réseau social' n'a pas été installé.";
$MESS["TASKS_MODULE_NOT_INSTALLED"] = "Le module 'Tâches' n'a pas été installé.";
$MESS["TASKS_STATUS_S"] = "N'a pas commencé";
$MESS["TASKS_STATUS_N"] = "Non accepté(e)";
$MESS["TASKS_PRIORITY_L"] = "Bas";
$MESS["TASKS_TITLE_CREATE_TASK"] = "Ajouter une tâche";
$MESS["TASKS_NEW_TASK"] = "Ajouter une tâche";
$MESS["TASKS_PRIORITY_N"] = "Moyen";
$MESS["TASKS_STATUS_D"] = "Différé";
$MESS["TASKS_TITLE_EDIT_TASK"] = "Edition de la tâche ##TASK_ID#";
$MESS["TASKS_COPY_CHILD_TEMPLATES"] = "Tâches Copie de l'enfant de modèle";
?>