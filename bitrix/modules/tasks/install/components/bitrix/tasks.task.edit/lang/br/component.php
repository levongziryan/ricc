<?
$MESS["TASKS_PRIORITY"] = "Prioridade";
$MESS["TASKS_PRIORITY_L"] = "Baixo";
$MESS["TASKS_PRIORITY_N"] = "Normal";
$MESS["TASKS_PRIORITY_H"] = "Alto";
$MESS["TASKS_STATUS_N"] = "Não Aceito";
$MESS["TASKS_STATUS_S"] = "Não Iniciado";
$MESS["TASKS_STATUS_I"] = "Em andamento";
$MESS["TASKS_STATUS_C"] = "Concluído";
$MESS["TASKS_STATUS_W"] = "Pendente";
$MESS["TASKS_STATUS_D"] = "Deferido";
$MESS["TASKS_MODULE_NOT_INSTALLED"] = "O módulo Tarefas não está instalado.";
$MESS["TASKS_TASK_NOT_FOUND"] = "A tarefa não foi encontrada, ou o acesso foi negado.";
$MESS["TASKS_TITLE_EDIT_TASK"] = "Editar Tarefa ##TASK_ID#";
$MESS["TASKS_TITLE_CREATE_TASK"] = "Nova tarefa";
$MESS["TASKS_TASK_CHANGED"] = "Tarefa #TASK_ID# foi alterada";
$MESS["TASKS_NEW_TASK"] = "Nova tarefa";
$MESS["SOCNET_MODULE_NOT_INSTALLED"] = "O módulo \"Rede Social\" não está instalado.";
$MESS["TASKS_DATE_MUST_BE_IN_FUTURE"] = "A data do lembrete deve ser posterior a data presente.";
$MESS["TASKS_COPY_CHILD_TEMPLATES"] = "Copiar tarefas secundárias a partir do modelo";
?>