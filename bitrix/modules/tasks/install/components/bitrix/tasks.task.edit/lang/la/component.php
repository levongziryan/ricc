<?
$MESS["TASKS_PRIORITY"] = "(prioridad)";
$MESS["TASKS_PRIORITY_L"] = "Bajo";
$MESS["TASKS_PRIORITY_N"] = "Normal";
$MESS["TASKS_PRIORITY_H"] = "Alto";
$MESS["TASKS_STATUS_N"] = "No ha sido aceptado";
$MESS["TASKS_STATUS_S"] = "No se ha Iniciado";
$MESS["TASKS_STATUS_I"] = "En Progreso";
$MESS["TASKS_STATUS_C"] = "Completado";
$MESS["TASKS_STATUS_W"] = "Pendiente";
$MESS["TASKS_STATUS_D"] = "Diferir";
$MESS["TASKS_MODULE_NOT_INSTALLED"] = "El módulo Tasks no está instalado.";
$MESS["SOCNET_MODULE_NOT_INSTALLED"] = "El módulo \"Social Network\" no está instalado.";
$MESS["TASKS_TASK_NOT_FOUND"] = "La tarea no fue encontrada,o el acceso está denegado.";
$MESS["TASKS_TITLE_EDIT_TASK"] = "Editar Tarea ##TASK_ID#";
$MESS["TASKS_TITLE_CREATE_TASK"] = "Agregar Tarea";
$MESS["TASKS_TASK_CHANGED"] = "Tarea ##TASK_ID# ha sido cambiado";
$MESS["TASKS_NEW_TASK"] = "Nueva tarea";
$MESS["TASKS_DATE_MUST_BE_IN_FUTURE"] = "La fecha de recordatorio debe ser posterior a la fecha actual.";
$MESS["TASKS_COPY_CHILD_TEMPLATES"] = "Copiar tareas secundarias desde la plantilla.";
?>