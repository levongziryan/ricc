<?
$MESS["TASKS_PRIORITY"] = "Prioridade";
$MESS["TASKS_PRIORITY_L"] = "Baixo";
$MESS["TASKS_PRIORITY_N"] = "Normal";
$MESS["TASKS_PRIORITY_H"] = "Alto";
$MESS["TASKS_STATUS_N"] = "Não Aceito";
$MESS["TASKS_STATUS_S"] = "Não Iniciado";
$MESS["TASKS_STATUS_I"] = "Em andamento";
$MESS["TASKS_STATUS_C"] = "Concluído";
$MESS["TASKS_STATUS_W"] = "Pendente";
$MESS["TASKS_STATUS_D"] = "Deferido";
$MESS["TASKS_MODULE_NOT_INSTALLED"] = "O módulo Tarefas não está instalado.";
$MESS["SOCNET_MODULE_NOT_INSTALLED"] = "O módulo \"Rede Social\" não está instalado.";
$MESS["TASKS_TEMPLATE_NOT_FOUND"] = "O modelo não foi encontrado ou o acesso foi negado.";
$MESS["TASKS_TITLE_EDIT_TEMPLATE"] = "Editar Modelo No. #TEMPLATE_ID#";
$MESS["TASKS_TITLE_CREATE_TEMPLATE"] = "Novo Modelo";
?>