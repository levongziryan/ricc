<?
$MESS["TASKS_PARAM_VARIABLE_ALIASES"] = "Aliases variáveis";
$MESS["TASKS_PARAM_USER_ID"] = "ID de usuário";
$MESS["TASKS_PARAM_GROUP_ID"] = "ID do Grupo";
$MESS["TASKS_PARAM_TASK_VAR"] = "Nome da Variável de ID da Tarefa";
$MESS["TASKS_PARAM_USER_VAR"] = "Nome da variável de ID de usuário";
$MESS["TASKS_PARAM_GROUP_VAR"] = "ID do Nome da variável do Grupo";
$MESS["TASKS_PARAM_ACTION_VAR"] = "Nome da variável de ID de ação";
$MESS["TASKS_PARAM_PAGE_VAR"] = "ID do Nome da Variável da Página";
$MESS["TASKS_PARAM_PATH_TO_USER_TASKS"] = "Caminho das tarefas do usuário";
$MESS["TASKS_PARAM_PATH_TO_USER_TASKS_TASK"] = "Caminho para uma tarefa de usuário";
$MESS["TASKS_PARAM_PATH_TO_GROUP_TASKS"] = "Modelo do caminho de Tarefas do Grupo";
$MESS["TASKS_PARAM_PATH_TO_GROUP_TASKS_TASK"] = "Caminho para uma Tarefa em Grupo";
$MESS["TASKS_PARAM_TASK_ID"] = "ID";
?>