<?
$MESS["TASKS_STATUS_1"] = "Nueva";
$MESS["TASKS_STATUS_2"] = "Pendiente";
$MESS["TASKS_STATUS_3"] = "En Progreso";
$MESS["TASKS_STATUS_4"] = "Supuestamente completada";
$MESS["TASKS_STATUS_5"] = "Completada";
$MESS["TASKS_STATUS_6"] = "Diferida";
$MESS["TASKS_STATUS_7"] = "Declinada";
$MESS["TASKS_PRIORITY"] = "Prioridad";
$MESS["TASKS_PRIORITY_0"] = "Baja";
$MESS["TASKS_PRIORITY_1"] = "Normal";
$MESS["TASKS_PRIORITY_2"] = "Alta";
$MESS["TASKS_MARK"] = "Calificación";
$MESS["TASKS_MARK_NONE"] = "Ninguno";
$MESS["TASKS_MARK_P"] = "Positivo";
$MESS["TASKS_MARK_N"] = "Negativo";
$MESS["TASKS_DOUBLE_CLICK"] = "Haga doble clic para ver";
$MESS["TASKS_APPLY"] = "Aplicar";
$MESS["TASKS_NO_TITLE"] = "No se ha especificado el nombre de la tarea.";
$MESS["TASKS_NO_RESPONSIBLE"] = "No se ha especificado persona responsable de la tarea.";
$MESS["TASKS_TITLE"] = "Tareas";
$MESS["TASKS_DEADLINE"] = "Fecha límite";
$MESS["TASKS_RESPONSIBLE"] = "Responsable";
$MESS["TASKS_CREATOR"] = "Creador";
$MESS["TASKS_QUICK_TITLE"] = "Nombre de la Tarea";
$MESS["TASKS_QUICK_DEADLINE"] = "Fecha límite";
$MESS["TASKS_QUICK_SAVE"] = "Cuardar";
$MESS["TASKS_QUICK_CANCEL"] = "Cancelar";
$MESS["TASKS_FILTER"] = "Fijar Filtros";
$MESS["TASKS_FILTER_COMMON"] = "Simple";
$MESS["TASKS_FILTER_EXTENDED"] = "Extendido";
$MESS["TASKS_FILTER_STATUSES"] = "Estados";
$MESS["TASKS_TEMPLATES"] = "Plantillas";
$MESS["TASKS_REPORTS"] = "Reportes";
$MESS["TASKS_EXPORT_EXCEL"] = "Exportar a Excel";
$MESS["TASKS_EXPORT_OUTLOOK"] = "Exportar a Outlook";
$MESS["TASKS_ADD_TASK"] = "Nueva Tarea";
$MESS["TASKS_ACCEPT_TASK"] = "Aceptar Tarea";
$MESS["TASKS_START_TASK"] = "Iniciar";
$MESS["TASKS_DECLINE_TASK"] = "Declinada";
$MESS["TASKS_RENEW_TASK"] = "Reanudar";
$MESS["TASKS_CLOSE_TASK"] = "Finalizar";
$MESS["TASKS_PAUSE_TASK"] = "Pausar";
$MESS["TASKS_DEFER_TASK"] = "Posponer";
$MESS["TASKS_APPROVE_TASK"] = "Aceptar como Completada";
$MESS["TASKS_REDO_TASK"] = "Devuelto para Completar";
$MESS["TASKS_DELETE_TASK"] = "Eliminar";
$MESS["TASKS_DELETE_CONFIRM"] = "¿Confirmar eliminación?";
$MESS["TASKS_DELETE_TASK_CONFIRM"] = "¿Confirmar eliminación?";
$MESS["TASKS_DELETE_FILE_CONFIRM"] = "¿Eliminar archivo?";
$MESS["TASKS_ADD_TEMPLATE_TASK"] = "Crear desde la plantilla de tareas";
$MESS["TASKS_TEMPLATES_LIST"] = "Todas las plantillas";
$MESS["TASKS_DURATION"] = "Horas Empleadas";
$MESS["TASKS_OK"] = "OK";
$MESS["TASKS_CANCEL"] = "Cancelar";
$MESS["TASKS_DECLINE_REASON"] = "Motivo de rechazo";
$MESS["TASKS_ADD_TEMPLATE_SUBTASK"] = "Plantillas de tareas (para la nueva subtarea)";
$MESS["TASKS_SELECT"] = "Seleccionar";
$MESS["TASKS_TASK_TITLE"] = "Nombre de la tarea y descripción";
$MESS["TASKS_REMIND"] = "recordar";
$MESS["TASKS_TASK_TAGS"] = "Etiquetas";
$MESS["TASKS_TASK_NO_TAGS"] = "ninguno";
$MESS["TASKS_TASK_FILES"] = "Archivos";
$MESS["TASKS_TASK_SUBTASKS"] = "Sub tareas";
$MESS["TASKS_TASK_PREVIOUS_TASKS"] = "Tareas Anidadas";
$MESS["TASKS_TASK_COMMENTS"] = "Comentarios";
$MESS["TASKS_TASK_LOG"] = "Registro de Cambios";
$MESS["TASKS_SIDEBAR_STATUS"] = "Estados";
$MESS["TASKS_SIDEBAR_START_DATE"] = "desde";
$MESS["TASKS_SIDEBAR_START"] = "Inicio";
$MESS["TASKS_SIDEBAR_FINISH"] = "Fin";
$MESS["TASKS_SIDEBAR_DURATION"] = "Duración";
$MESS["TASKS_SIDEBAR_IN_REPORT"] = "En el Reporte";
$MESS["TASKS_SIDEBAR_IN_REPORT_YES"] = "Si";
$MESS["TASKS_SIDEBAR_IN_REPORT_NO"] = "No";
$MESS["TASKS_SIDEBAR_REPEAT"] = "Recurrente";
$MESS["TASKS_SIDEBAR_TEMPLATE"] = "plantilla";
$MESS["TASKS_SIDEBAR_TEMPLATE_NOT_EXISTS"] = "La tarea no se inicia ya que la plantilla se ha eliminado.";
$MESS["TASKS_SIDEBAR_CHANGE"] = "cambio";
$MESS["TASKS_SIDEBAR_ACCOMPLICES"] = "Participantes";
$MESS["TASKS_SIDEBAR_AUDITORS"] = "Observadores";
$MESS["TASKS_SIDEBAR_ADD_ACCOMPLICES"] = "Agregar Participantes";
$MESS["TASKS_SIDEBAR_ADD_AUDITORS"] = "Agregar Observadores";
$MESS["TASKS_SIDEBAR_DEADLINE_NO"] = "No";
$MESS["TASKS_SIDEBAR_STOP_WATCH_CONFIRM"] = "¿Seguro que no quieres ser un observador de esta tarea?";
$MESS["TASKS_ADD_BACK_TO_TASKS_LIST"] = "Volver a la Tareas";
$MESS["TASKS_HOURS_N"] = "hora";
$MESS["TASKS_HOURS_G"] = "hora";
$MESS["TASKS_HOURS_P"] = "horas";
$MESS["TASKS_NO_TEMPLATES"] = "No hay plantillas disponibles";
$MESS["TASKS_PARENT_TASK"] = "Tarea Principal";
$MESS["TASKS_TASK_GROUP"] = "Esta tarea está en el grupo (proyecto)";
$MESS["TASKS_NO_SUBTASKS"] = "no hay subtareas";
$MESS["TASKS_LOG_NEW"] = "Tarea creada";
$MESS["TASKS_LOG_TITLE"] = "Nombre de la Tarea";
$MESS["TASKS_LOG_DESCRIPTION"] = "Descripción actualizada";
$MESS["TASKS_LOG_CREATED_BY"] = "Creador";
$MESS["TASKS_LOG_RESPONSIBLE_ID"] = "Persona Responsable";
$MESS["TASKS_LOG_DEADLINE"] = "Fecha límite";
$MESS["TASKS_LOG_ACCOMPLICES"] = "Participantes";
$MESS["TASKS_LOG_AUDITORS"] = "Observadores";
$MESS["TASKS_LOG_FILES"] = "Archivos";
$MESS["TASKS_LOG_TAGS"] = "Etiquetas";
$MESS["TASKS_LOG_PRIORITY"] = "Prioridad";
$MESS["TASKS_LOG_GROUP_ID"] = "Grupo";
$MESS["TASKS_LOG_PARENT_ID"] = "Tarea Principal";
$MESS["TASKS_LOG_DEPENDS_ON"] = "Tareas Anidadas";
$MESS["TASKS_LOG_STATUS"] = "Estados";
$MESS["TASKS_LOG_MARK"] = "Calificación";
$MESS["TASKS_LOG_ADD_IN_REPORT"] = "En el Reporte";
$MESS["TASKS_LOG_WHEN"] = "Fecha";
$MESS["TASKS_LOG_WHO"] = "Creado por";
$MESS["TASKS_LOG_WHERE"] = "Disposición de Actualización";
$MESS["TASKS_LOG_WHAT"] = "Actualización";
$MESS["TASKS_LOG_DELETED_FILES"] = "Archivos eliminados";
$MESS["TASKS_LOG_NEW_FILES"] = "Archivos agregados";
$MESS["TASKS_LOG_COMMENT"] = "Comentario creado";
$MESS["TASKS_LOG_COMMENT_EDIT"] = "Comentario cambiado";
$MESS["TASKS_LOG_COMMENT_REMOVE"] = "Comentario eliminado";
$MESS["TASKS_LOG_START_DATE_PLAN"] = "Fecha de Inicio del Proyecto";
$MESS["TASKS_LOG_END_DATE_PLAN"] = "Fecha de Finalización del Proyecto";
$MESS["TASKS_LOG_DURATION_PLAN"] = "Duración Planificada";
$MESS["TASKS_LOG_DURATION_PLAN_SECONDS"] = "Duración Planificada";
$MESS["TASKS_LOG_DURATION_FACT"] = "Duración Actual";
$MESS["TASKS_LOG_TIME_ESTIMATE"] = "Tiempo estimado";
$MESS["TASKS_LOG_TIME_SPENT_IN_LOGS"] = "Tiempo empleado";
$MESS["TASKS_LOG_CHECKLIST_ITEM_CREATE"] = "Elemento agregado a lista";
$MESS["TASKS_LOG_CHECKLIST_ITEM_REMOVE"] = "Elemento retirado de lista";
$MESS["TASKS_LOG_CHECKLIST_ITEM_RENAME"] = "El elemento de la lista ha cambiado de nombre.";
$MESS["TASKS_DELEGATE_TASK"] = "Delegar";
$MESS["TASKS_COPY_TASK"] = "Copiar";
$MESS["TASKS_ADD_TASK_TO_TIMEMAN"] = "Agregar al plan diario";
$MESS["TASKS_COPY_TASK_EX"] = "Duplicar Tarea";
$MESS["TASKS_ADD_TASK_TO_TIMEMAN_EX"] = "Agregar al Plan Diario";
$MESS["TASKS_ELAPSED_TIME"] = "Tiempo Empleado";
$MESS["TASKS_ELAPSED_TIME_SHORT"] = "Tiempo transcurrido";
$MESS["TASKS_ELAPSED_H"] = "h";
$MESS["TASKS_ELAPSED_M"] = "m";
$MESS["TASKS_ELAPSED_S"] = "s";
$MESS["TASKS_ELAPSED_AUTHOR"] = "Creado Por";
$MESS["TASKS_ELAPSED_DATE"] = "Fecha";
$MESS["TASKS_ELAPSED_COMMENT"] = "Comentario";
$MESS["TASKS_ELAPSED_ADD"] = "Agregar";
$MESS["TASKS_ELAPSED_SOURCE_UNDEFINED"] = "No se sabe si el registro se ha agregado o modificado manualmente";
$MESS["TASKS_ELAPSED_SOURCE_MANUAL"] = "Se agregó o editó manualmente este registro";
$MESS["TASKS_REMINDER_TITLE"] = "Recordar";
$MESS["TASKS_ABOUT_DEADLINE"] = "Antes de la fecha";
$MESS["TASKS_BY_DATE"] = "Por fecha";
$MESS["TASKS_REMIND_VIA_JABBER"] = "mensaje instantáneo";
$MESS["TASKS_REMIND_VIA_EMAIL"] = "por e-mail";
$MESS["TASKS_REMIND_VIA_JABBER_EX"] = "Enviar mensajes instantáneos";
$MESS["TASKS_REMIND_VIA_EMAIL_EX"] = "Enviar mensaje de e-mail";
$MESS["TASKS_REMIND_BEFORE"] = "en #NUM# día(s)";
$MESS["TASKS_REMINDER_OK"] = "ACEPTAR";
$MESS["TASKS_ADD_SUBTASK_2"] = "Crear subtarea";
$MESS["TASKS_QUICK_IN_GROUP"] = "tarea del proyecto";
$MESS["TASKS_QUICK_DESCRIPTION"] = "descripción";
$MESS["TASKS_DATE_CREATED"] = "Creada el";
$MESS["TASKS_DATE_START"] = "Fecha de inicio";
$MESS["TASKS_DATE_END"] = "Fecha de finalización";
$MESS["TASKS_DATE_STARTED"] = "Iniciado el";
$MESS["TASKS_DATE_COMPLETED"] = "Cmpletado el";
$MESS["TASKS_TASK_TITLE_LABEL"] = "Tarea";
$MESS["TASKS_STATUS"] = "Estados";
$MESS["TASKS_STATUS_OVERDUE"] = "Atrasada";
$MESS["TASKS_STATUS_NEW"] = "Nueva";
$MESS["TASKS_STATUS_ACCEPTED"] = "Pendiente";
$MESS["TASKS_STATUS_IN_PROGRESS"] = "En progreso";
$MESS["TASKS_STATUS_WAITING"] = "En espera de control";
$MESS["TASKS_STATUS_COMPLETED"] = "Completada";
$MESS["TASKS_STATUS_DELAYED"] = "Diferida";
$MESS["TASKS_STATUS_DECLINED"] = "Declinada";
$MESS["TASKS_QUICK_INFO_DETAILS"] = "Detalles";
$MESS["TASKS_QUICK_INFO_EMPTY_DATE"] = "no";
$MESS["TASKS_GROUP_ADD"] = "agregar";
$MESS["TASKS_GROUP_LOADING"] = "por favor espere...";
$MESS["TASKS_TASK_DURATION_HOURS"] = "horas";
$MESS["TASKS_TASK_DURATION_HOURS_PLURAL_0"] = "hora";
$MESS["TASKS_TASK_DURATION_HOURS_PLURAL_1"] = "hora";
$MESS["TASKS_TASK_DURATION_HOURS_PLURAL_2"] = "horas";
$MESS["TASKS_TASK_DURATION_DAYS"] = "días";
$MESS["TASKS_TASK_DURATION_DAYS_PLURAL_0"] = "día";
$MESS["TASKS_TASK_DURATION_DAYS_PLURAL_1"] = "días";
$MESS["TASKS_TASK_DURATION_DAYS_PLURAL_2"] = "días";
$MESS["TASKS_TASK_DURATION_MINUTES"] = "minutos";
$MESS["TASKS_TASK_DURATION_MINUTES_PLURAL_0"] = "minuto";
$MESS["TASKS_TASK_DURATION_MINUTES_PLURAL_1"] = "minutos";
$MESS["TASKS_TASK_DURATION_MINUTES_PLURAL_2"] = "minutos";
$MESS["TASKS_TASK_DURATION_SECONDS_PLURAL_0"] = "segundo";
$MESS["TASKS_TASK_DURATION_SECONDS_PLURAL_1"] = "segundos";
$MESS["TASKS_TASK_DURATION_SECONDS_PLURAL_2"] = "segundos";
$MESS["TASKS_DETAIL_CHECKLIST"] = "Lista de verificación";
$MESS["TASKS_DETAIL_CHECKLIST_ADD"] = "agregar";
$MESS["TASKS_BASE_TEMPLATE"] = "Plantilla de base";
$MESS["TASKS_TEMPLATE_CREATE_TASK"] = "Crear tarea en esta plantilla";
$MESS["TASKS_TEMPLATE_CREATE_SUB"] = "Agregar subplantilla";
$MESS["TASKS_LOG_CHECKLIST_ITEM_UNCHECK"] = "El elemento de la lista de verificación se convirtió en incompleto.";
$MESS["TASKS_LOG_CHECKLIST_ITEM_CHECK"] = "El elemento de la lista de verificación se ha completado.";
?>