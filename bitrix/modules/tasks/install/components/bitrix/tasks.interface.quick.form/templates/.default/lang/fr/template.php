<?
$MESS["TASKS_QUICK_FORM_DESC_PLACEHOLDER"] = "Description de la tâche";
$MESS["TASKS_QUICK_FORM_TITLE_PLACEHOLDER"] = "Nouvelle tâche";
$MESS["TASKS_QUICK_FORM_AFTER_SAVE_MESSAGE"] = "La tâche &quot;#TASK_NAME#&quot; a été enregistrée.";
$MESS["TASKS_QUICK_FORM_OPEN_TASK"] = "Tâche ouverte";
$MESS["TASKS_QUICK_FORM_HIGHLIGHT_TASK"] = "Afficher en liste";
$MESS["TASKS_QUICK_TITLE"] = "Nom de la tâche";
$MESS["TASKS_QUICK_DEADLINE"] = "Date limite";
$MESS["TASKS_QUICK_SAVE"] = "Enregistrer";
$MESS["TASKS_QUICK_CANCEL"] = "Annuler";
$MESS["TASKS_QUICK_RESPONSIBLE"] = "Responsable";
$MESS["TASKS_QUICK_IN_GROUP"] = "tâche dans le projet";
$MESS["TASKS_QUICK_DESCRIPTION"] = "description";
?>