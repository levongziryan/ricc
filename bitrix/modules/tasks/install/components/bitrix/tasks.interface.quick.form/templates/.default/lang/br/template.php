<?
$MESS["TASKS_QUICK_FORM_DESC_PLACEHOLDER"] = "Descrição da tarefa";
$MESS["TASKS_QUICK_FORM_TITLE_PLACEHOLDER"] = "Nova tarefa";
$MESS["TASKS_QUICK_FORM_AFTER_SAVE_MESSAGE"] = "A tarefa &quot;#TASK_NAME#&quot; foi salva.";
$MESS["TASKS_QUICK_FORM_OPEN_TASK"] = "Abrir tarefa";
$MESS["TASKS_QUICK_FORM_HIGHLIGHT_TASK"] = "Mostrar na lista";
$MESS["TASKS_QUICK_TITLE"] = "Nome da Tarefa";
$MESS["TASKS_QUICK_DEADLINE"] = "Prazo";
$MESS["TASKS_QUICK_SAVE"] = "Salvar";
$MESS["TASKS_QUICK_CANCEL"] = "Cancelar";
$MESS["TASKS_QUICK_RESPONSIBLE"] = "Responsável";
$MESS["TASKS_QUICK_IN_GROUP"] = "tarefa no projeto";
$MESS["TASKS_QUICK_DESCRIPTION"] = "descrição";
?>