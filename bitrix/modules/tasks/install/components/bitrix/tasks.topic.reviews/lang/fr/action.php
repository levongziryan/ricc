<?
$MESS["F_ERR_SESSION_TIME_IS_UP"] = "Votre session a expiré. S'il vous plaît republier votre message.";
$MESS["TASKS_COMMENT_MESSAGE_ADD"] = "On a ajouté un commentaire à la tâche '#TASK_TITLE#' avec le texte suivant: '#TASK_COMMENT_TEXT#'";
$MESS["TASKS_COMMENT_MESSAGE_ADD_M"] = "On a ajouté un commentaire à la tâche '#TASK_TITLE#' avec le texte suivant: '#TASK_COMMENT_TEXT#'";
$MESS["TASKS_COMMENT_MESSAGE_ADD_F"] = "Commentaire ajouté à la tâche '#TASK_TITLE#' avec le texte suivant: '#TASK_COMMENT_TEXT#'";
$MESS["TASKS_COMMENT_MESSAGE_EDIT"] = "A remplacé le commentaire pour la tâche '#TASK_TITLE#' par le texte: '#TASK_COMMENT_TEXT#'";
$MESS["TASKS_COMMENT_MESSAGE_EDIT_M"] = "A remplacé le commentaire pour la tâche '#TASK_TITLE#' par le texte: '#TASK_COMMENT_TEXT#'";
$MESS["TASKS_COMMENT_MESSAGE_EDIT_F"] = "Il (elle) a remplacé le commentaire pour la tâche '#TASK_TITLE# par le texte: '#TASK_COMMENT_TEXT#'.";
$MESS["F_FORUM_MESSAGE_CNT"] = "Commentaire";
$MESS["F_ERR_NOT_RIGHT_FOR_ADD"] = "Vous ne disposez pas des droits nécessaires pour commenter.";
$MESS["F_ERR_NO_REVIEW_TEXT"] = "Le texte de rappel n'est pas indiqué.";
$MESS["POSTM_CAPTCHA"] = "Le code de protection contre les messages automatiques est iinexactement indiqué.";
$MESS["COMM_COMMENT_OK"] = "Un avis a été ajouté avec succès.";
$MESS["F_ERR_ADD_MESSAGE"] = "Erreur d'ajout du message.";
$MESS["F_ERR_ADD_TOPIC"] = "Erreur d'ajout du thème.";
$MESS["F_ERR_REMOVE_COMMENT"] = "Une erreur s'est produite lors de la tentative de supprimer le commentaire.";
$MESS["TASKS_COMMENT_SONET_NEW_TASK_MESSAGE"] = "La tâche est créée";
$MESS["F_FORUM_TOPIC_ID"] = "Sujet du forum pour les commentaires";
?>