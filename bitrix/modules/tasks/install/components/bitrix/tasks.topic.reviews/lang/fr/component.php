<?
$MESS["F_ERR_TID_IS_NOT_EXIST"] = "La tâche #TASK_ID# n'a pas été trouvée.";
$MESS["F_NO_MODULE_TASKS"] = "Le module 'Tâches' n'a pas été installé.";
$MESS["F_NO_MODULE"] = "Le module 'Forum' n'a pas été installé";
$MESS["F_ERR_FID_EMPTY"] = "Le forum pour opinions n'est pas indiqué";
$MESS["F_ERR_TID_EMPTY"] = "Tâche non indiquée.";
$MESS["COMM_COMMENT_OK_AND_NOT_APPROVED"] = "Avis est rajouté avec succès et sera affiché après l'approbation du modérateur.";
$MESS["COMM_COMMENT_OK"] = "Un avis a été ajouté avec succès.";
$MESS["NAV_OPINIONS"] = "Avis";
$MESS["F_ERR_FORUM_NO_ACCESS"] = "Vous n'avez pas accès de droits pour l'affichage des commentaires.";
$MESS["F_ERR_FID_IS_NOT_EXIST"] = "Le forum des commentaires #FORUM# n'existe pas";
?>