<?
$MESS["F_TASK_ID"] = "ID de la tâche";
$MESS["F_FORUM_ID"] = "Forum ID";
$MESS["RATING_TYPE"] = "Vue des boutons de rating";
$MESS["SHOW_RATING"] = "Activer le classement";
$MESS["F_PREORDER"] = "Afficher les messages en ordre direct";
$MESS["F_DISPLAY_PANEL"] = "Ajouter les boutons au panneau admin. Pour ce composant";
$MESS["F_USE_CAPTCHA"] = "Utiliser CAPTCHA";
$MESS["F_MESSAGES_PER_PAGE"] = "Nombre de messages sur une page";
$MESS["RATING_TYPE_LIKE_GRAPHIC"] = "J'aime (graphique)";
$MESS["RATING_TYPE_LIKE_TEXT"] = "J'aime (textuel)";
$MESS["F_PAGE_NAVIGATION_TEMPLATE"] = "Dénomination du modèle";
$MESS["F_POST_FIRST_MESSAGE"] = "Commencer le thème par le texte de l'élément";
$MESS["RATING_TYPE_STANDART_GRAPHIC"] = "J'aime / Je n'aime pas (graphique)";
$MESS["RATING_TYPE_STANDART_TEXT"] = "J'aime / Je n'aime pas (textuel)";
$MESS["SHOW_RATING_CONFIG"] = "ordinaire";
$MESS["RATING_TYPE_CONFIG"] = "ordinaire";
$MESS["F_PATH_TO_SMILE"] = "Chemin par rapport à la racine du site vers le dossier avec des smileys";
$MESS["F_PROFILE_VIEW_TEMPLATE"] = "Page de profil";
$MESS["F_READ_TEMPLATE"] = "Page de lecture du message";
$MESS["F_DETAIL_TEMPLATE"] = "Page de l'élément du bloc d'information";
$MESS["F_NAME_TEMPLATE"] = "Format du nom";
$MESS["F_DATE_TIME_FORMAT"] = "Format d'affichage de la date et de l'heure";
$MESS["F_POST_FIRST_MESSAGE_TEMPLATE"] = "Modèle du texte pour la première communication du thème";
?>