<?
$MESS["F_ERR_SESSION_TIME_IS_UP"] = "Sua sessão expirou. Por favor, reposte sua mensagem.";
$MESS["F_ERR_NO_REVIEW_TEXT"] = "Por favor, digite os seus comentários.";
$MESS["COMM_COMMENT_OK"] = "O comentário foi salvo com sucesso";
$MESS["POSTM_CAPTCHA"] = "O código CAPTCHA está incorreto.";
$MESS["F_ERR_NOT_RIGHT_FOR_ADD"] = "Permissões Insuficientes para adicionar comentários.";
$MESS["F_ERR_ADD_TOPIC"] = "Erro ao criar tópico.";
$MESS["F_ERR_ADD_MESSAGE"] = "Erro ao criar post.";
$MESS["F_FORUM_TOPIC_ID"] = "Tópico do Fórum";
$MESS["F_FORUM_MESSAGE_CNT"] = "Comentários";
$MESS["TASKS_COMMENT_SONET_NEW_TASK_MESSAGE"] = "Tarefa criada";
$MESS["TASKS_COMMENT_MESSAGE_ADD"] = "comentou sobre \"#TASK_TITLE#\": \"#TASK_COMMENT_TEXT#\"";
$MESS["TASKS_COMMENT_MESSAGE_EDIT"] = "comentário modificado em \"#TASK_TITLE\", o novo texto: \"#TASK_COMMENT_TEXT#\"";
$MESS["F_ERR_REMOVE_COMMENT"] = "Erro ao tentar excluir comentários.";
$MESS["TASKS_COMMENT_MESSAGE_ADD_F"] = "Adicionou um comentário a \"#TASK_TITLE#\". O texto do comentário é: \"#TASK_COMMENT_TEXT#\".";
$MESS["TASKS_COMMENT_MESSAGE_EDIT_F"] = "Atualizou um comentário em\"#TASK_TITLE#\". O novo texto é: \"#TASK_COMMENT_TEXT#\".";
$MESS["TASKS_COMMENT_MESSAGE_ADD_M"] = "Adicionou um comentário na tarefa \"#TASK_TITLE#\", comentário:  \"#TASK_COMMENT_TEXT#\".";
$MESS["TASKS_COMMENT_MESSAGE_EDIT_M"] = "Modificou um comentário em \"#TASK_TITLE#\". O novo texto é: \"#TASK_COMMENT_TEXT#\".";
?>