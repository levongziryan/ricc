<?
$MESS["TASKS_TT_TASKS_MODULE_NOT_INSTALLED"] = "Модуль управління завданнями не встановлено";
$MESS["TASKS_TT_SOCIALNETWORK_MODULE_NOT_INSTALLED"] = "Модуль «Соціальна мережа» не встановлено.";
$MESS["TASKS_TT_FORUM_MODULE_NOT_INSTALLED"] = "Модуль форум не встановлено";
$MESS["TASKS_TL_SOCIALNETWORK_MODULE_NOT_INSTALLED"] = "Модуль \"Соціальна мережа\" не встановлено";
$MESS["TASKS_TL_FORUM_MODULE_NOT_INSTALLED"] = "Модуль \"Форум\" не встановлено";
$MESS["TASKS_TL_ACCESS_TO_GROUP_DENIED"] = "Ви не можете переглядати список завдань у цій групі";
$MESS["TASKS_TL_TITLE_TASKS"] = "Завдання";
$MESS["TASKS_GROUP_ACTION_DAYS_NUM_INVALID_TITLE"] = "Помилка введення даних";
$MESS["TASKS_GROUP_ACTION_DAYS_NUM_INVALID_TEXT"] = "Дане поле може містити тільки цифри";
?>