<?
$MESS["TASKS_TT_TASKS_MODULE_NOT_INSTALLED"] = "El módulo Task no está instalado.";
$MESS["TASKS_TT_SOCIALNETWORK_MODULE_NOT_INSTALLED"] = "El módulo Social Network no está instalado.";
$MESS["TASKS_TT_FORUM_MODULE_NOT_INSTALLED"] = "El módulo Forum no está instalado.";
$MESS["TASKS_TL_SOCIALNETWORK_MODULE_NOT_INSTALLED"] = "El módulo \"Social Network\" no está instalado.";
$MESS["TASKS_TL_FORUM_MODULE_NOT_INSTALLED"] = "El módulo \"Forum\" no está instalado.";
$MESS["TASKS_TL_ACCESS_TO_GROUP_DENIED"] = "No se puede ver la lista de tareas para este grupo.";
$MESS["TASKS_TL_TITLE_TASKS"] = "Tareas";
$MESS["TASKS_GROUP_ACTION_DAYS_NUM_INVALID_TITLE"] = "Entrada Inválida";
$MESS["TASKS_GROUP_ACTION_DAYS_NUM_INVALID_TEXT"] = "Este campo sólo puede contener números.";
?>