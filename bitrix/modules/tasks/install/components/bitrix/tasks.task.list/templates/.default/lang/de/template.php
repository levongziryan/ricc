<?
$MESS["TASKS_COLUMN_ID"] = "ID";
$MESS["TASKS_COLUMN_TITLE"] = "Name";
$MESS["TASKS_COLUMN_ORIGINATOR_NAME"] = "Erstellt von";
$MESS["TASKS_COLUMN_RESPONSIBLE_NAME"] = "Verantwortliche Person";
$MESS["TASKS_COLUMN_DEADLINE"] = "Frist";
$MESS["TASKS_COLUMN_PRIORITY"] = "Priorität";
$MESS["TASKS_COLUMN_STATUS"] = "Status";
$MESS["TASKS_COLUMN_GROUP_NAME"] = "Projekt";
$MESS["TASKS_COLUMN_CHANGED_DATE"] = "Geändert am";
$MESS["TASKS_COLUMN_TIME_ESTIMATE"] = "Geplante Zeit erforderlich";
$MESS["TASKS_COLUMN_CREATED_DATE"] = "Erstellt am";
$MESS["TASKS_COLUMN_CLOSED_DATE"] = "Geschlossen am";
$MESS["TASKS_COLUMN_ALLOW_TIME_TRACKING"] = "Zeitaufwand berücksichtigen";
$MESS["TASKS_COLUMN_ALLOW_CHANGE_DEADLINE"] = "Verantwortliche Person darf Frist ändern";
$MESS["TASKS_COLUMN_TIME_SPENT_IN_LOGS"] = "Zeitaufwand";
$MESS["TASKS_COLUMN_UF_CRM_TASK"] = "CRM";
$MESS["TASKS_COLUMN_FLAG_COMPLETE"] = "Abgeschlossene Aufgaben";
$MESS["TASKS_LIST_CRM_TYPE_CO"] = "Unternehmen";
$MESS["TASKS_LIST_CRM_TYPE_C"] = "Kontakt";
$MESS["TASKS_LIST_CRM_TYPE_L"] = "Lead";
$MESS["TASKS_LIST_CRM_TYPE_D"] = "Auftrag";
$MESS["TASKS_ALLOW_CHANGE_DEADLINE_Y"] = "Ja";
$MESS["TASKS_ALLOW_CHANGE_DEADLINE_N"] = "Nein";
$MESS["CONFIRM_APPLY_REMOVE_BUTTON_TEXT"] = "Löschen";
$MESS["TASKS_ALLOW_TIME_TRACKING_Y"] = "Ja";
$MESS["TASKS_ALLOW_TIME_TRACKING_N"] = "Nein";
$MESS["TASKS_COLUMN_MARK"] = "Bewertung";
$MESS["TASKS_MARK_NONE"] = "Keine";
$MESS["TASKS_MARK_P"] = "Positive";
$MESS["TASKS_MARK_N"] = "Negative";
$MESS["TASKS_STATUS"] = "Status";
$MESS["TASKS_STATUS_1"] = "Neu";
$MESS["TASKS_STATUS_2"] = "Anstehend";
$MESS["TASKS_STATUS_3"] = "In Arbeit";
$MESS["TASKS_STATUS_4"] = "Angeblich fertiggestellt";
$MESS["TASKS_STATUS_5"] = "Fertiggestellt";
$MESS["TASKS_STATUS_6"] = "Verschoben";
$MESS["TASKS_STATUS_7"] = "Abgelehnt";
$MESS["TASKS_PRIORITY"] = "Priorität";
$MESS["TASKS_PRIORITY_0"] = "Niedrig";
$MESS["TASKS_PRIORITY_1"] = "Normal";
$MESS["TASKS_PRIORITY_2"] = "Hoch";
$MESS["TASKS_TASK_COMMENTS"] = "Kommentare";
$MESS["TASKS_LOG_WHAT"] = "Was geändert";
$MESS["TASKS_ADD_TASK"] = "Neue Aufgabe";
$MESS["TASKS_ACCEPT_TASK"] = "Aufgabe annehmen";
$MESS["TASKS_START_TASK"] = "Ausführung beginnen";
$MESS["TASKS_DECLINE_TASK"] = "Ablehnen";
$MESS["TASKS_RENEW_TASK"] = "Wiederaufnehmen";
$MESS["TASKS_CLOSE_TASK"] = "Fertigstellen";
$MESS["TASKS_PAUSE_TASK"] = "Pause";
$MESS["TASKS_DEFER_TASK"] = "Verschieben";
$MESS["TASKS_APPROVE_TASK"] = "Akzeptieren und Abschließen";
$MESS["TASKS_REDO_TASK"] = "Zurück zur Nachbesserung";
$MESS["TASKS_EDIT_TASK"] = "Bearbeiten";
$MESS["TASKS_DELETE_TASK"] = "Löschen";
$MESS["TASKS_TASK_ADD_TO_FAVORITES"] = "Zu Lesezeichen hinzufügen";
$MESS["TASKS_TASK_REMOVE_FROM_FAVORITES"] = "Aus Lesezeichen entfernen";
$MESS["TASKS_LIST_CHOOSE_ACTION"] = "Aktion auswählen";
$MESS["TASKS_LIST_GROUP_ACTION_REMOVE"] = "Löschen";
$MESS["TASKS_LIST_GROUP_ACTION_COMPLETE"] = "Abschließen";
$MESS["TASKS_LIST_GROUP_ACTION_CHANGE_RESPONSIBLE"] = "Verantwortliche Person ändern";
$MESS["TASKS_LIST_GROUP_ACTION_CHANGE_ORIGINATOR"] = "Autor ändern";
$MESS["TASKS_LIST_GROUP_ACTION_ADD_AUDITOR"] = "Beobachter hinzufügen";
$MESS["TASKS_LIST_GROUP_ACTION_ADD_ACCOMPLICE"] = "Mitwirkende hinzufügen";
$MESS["TASKS_LIST_GROUP_ACTION_SET_DEADLINE"] = "Frist setzen";
$MESS["TASKS_LIST_GROUP_ACTION_SET_GROUP"] = "Gruppe (Projekt) definieren";
$MESS["TASKS_LIST_GROUP_ACTION_DAYS_PLURAL_0"] = "Tag";
$MESS["TASKS_LIST_GROUP_ACTION_DAYS_PLURAL_1"] = "Tage";
$MESS["TASKS_LIST_GROUP_ACTION_DAYS_PLURAL_2"] = "Tage";
$MESS["TASKS_LIST_GROUP_ACTION_WEEKS_PLURAL_0"] = "Woche";
$MESS["TASKS_LIST_GROUP_ACTION_WEEKS_PLURAL_1"] = "Wochen";
$MESS["TASKS_LIST_GROUP_ACTION_WEEKS_PLURAL_2"] = "Wochen";
$MESS["TASKS_LIST_GROUP_ACTION_MONTHES_PLURAL_0"] = "Monat";
$MESS["TASKS_LIST_GROUP_ACTION_MONTHES_PLURAL_1"] = "Monate";
$MESS["TASKS_LIST_GROUP_ACTION_MONTHES_PLURAL_2"] = "Monate";
$MESS["TASKS_LIST_GROUP_ACTION_PLEASE_WAIT"] = "Bitte warten...";
$MESS["TASKS_LIST_CONFIRM_REMOVE_FOR_SELECTED_ITEMS"] = "Möchten Sie die ausgewählten Aufgaben wirklich löschen?";
$MESS["TASKS_LIST_CONFIRM_REMOVE_FOR_ALL_ITEMS"] = "Möchten Sie alle Aufgaben zusammen mit Teilaufgaben (auch auf anderen Seiten) wirklich löschen?";
$MESS["TASKS_LIST_GROUP_ACTION_ADD_FAVORITE"] = "Zu Lesezeichen hinzufügen";
$MESS["TASKS_LIST_GROUP_ACTION_DELETE_FAVORITE"] = "Aus Lesezeichen entfernen";
$MESS["TASKS_LIST_GROUP_ACTION_MOVE_DEADLINE_RIGHT"] = "Frist nach vorne verschieben";
$MESS["TASKS_LIST_GROUP_ACTION_MOVE_DEADLINE_LEFT"] = "Frist nach hinten verschieben";
$MESS["TASKS_LIST_GROUP_ACTION_MOVE_DEADLINE_AT_DAY"] = "Tag";
$MESS["TASKS_LIST_GROUP_ACTION_MOVE_DEADLINE_AT_WEEK"] = "Woche";
$MESS["TASKS_LIST_GROUP_ACTION_MOVE_DEADLINE_AT_MONTH"] = "Monat";
$MESS["TASKS_TITLE_GROUP_TASKS"] = "Aufgaben der Projektgruppe";
$MESS["TASKS_TITLE"] = "Aufgaben";
$MESS["TASKS_TITLE_MY"] = "Meine Aufgaben";
$MESS["TASKS_CONFIRM_GROUP_ACTION"] = "Möchten Sie wirklich fortfahren?";
$MESS["TASKS_JS_MARK"] = "Bewertung";
$MESS["TASKS_JS_MARK_NONE"] = "Keine";
$MESS["TASKS_JS_MARK_N"] = "Negative";
$MESS["TASKS_JS_MARK_P"] = "Positive";
$MESS["TASKS_NOT_PRESENT"] = "nicht angegeben";
$MESS["TASKS_VIEW_TASK"] = "Ansicht";
$MESS["TASKS_ADD_SUB_TASK"] = "Teilaufgabe erstellen";
$MESS["TASKS_COPY_TASK"] = "Kopieren";
$MESS["TASKS_ADD_TASK_TO_TIMEMAN"] = "Zum Tagesplan hinzufügen";
$MESS["TASKS_ADD_TASK_TO_TIMEMAN_EX"] = "Zum Tagesplan hinzufügen";
$MESS["TASKS_MARK"] = "Bewertung";
$MESS["TASKS_DOUBLE_CLICK"] = "Mit dem Doppelklick anzeigen";
$MESS["TASKS_APPLY"] = "Anwenden";
$MESS["TASKS_NO_TITLE"] = "Der Aufgabenname ist nicht angegeben.";
$MESS["TASKS_NO_RESPONSIBLE"] = "Die verantwortliche Person ist nicht angegeben.";
$MESS["TASKS_DEADLINE"] = "Frist";
$MESS["TASKS_RESPONSIBLE"] = "Verantwortlich";
$MESS["TASKS_CREATOR"] = "Erstellt von";
$MESS["TASKS_QUICK_TITLE"] = "Aufgabenname";
$MESS["TASKS_QUICK_DEADLINE"] = "Frist";
$MESS["TASKS_QUICK_SAVE"] = "Speichern";
$MESS["TASKS_QUICK_CANCEL"] = "Abbrechen";
$MESS["TASKS_FILTER"] = "Filter einstellen";
$MESS["TASKS_FILTER_COMMON"] = "Normal";
$MESS["TASKS_FILTER_EXTENDED"] = "Erweitert";
$MESS["TASKS_FILTER_STATUSES"] = "Status";
$MESS["TASKS_TEMPLATES"] = "Vorlagen";
$MESS["TASKS_REPORTS"] = "Berichte";
$MESS["TASKS_EXPORT_EXCEL"] = "Nach Excel exportieren";
$MESS["TASKS_EXPORT_OUTLOOK"] = "Nach Outlook exportieren";
$MESS["TASKS_DELETE_CONFIRM"] = "Löschen bestätigen";
$MESS["TASKS_DELETE_TASK_CONFIRM"] = "Wirklich löschen?";
$MESS["TASKS_DELETE_FILE_CONFIRM"] = "Soll die Datei gelöscht werden?";
$MESS["TASKS_ADD_TEMPLATE_TASK"] = "Aufgabevorlagen";
$MESS["TASKS_TEMPLATES_LIST"] = "Alle Vorlagen";
$MESS["TASKS_DURATION"] = "Aufgewendet (Stunden)";
$MESS["TASKS_OK"] = "OK";
$MESS["TASKS_CANCEL"] = "Abbrechen";
$MESS["TASKS_DECLINE_REASON"] = "Grund für Aufgabenablehnung";
$MESS["TASKS_ADD_TEMPLATE_SUBTASK"] = "Aufgabevorlagen (für neue Teilaufgaben)";
$MESS["TASKS_SELECT"] = "Auswählen";
$MESS["TASKS_TASK_TITLE"] = "Wesen der Aufgabe";
$MESS["TASKS_REMIND"] = "Erinnern";
$MESS["TASKS_TASK_TAGS"] = "Tags";
$MESS["TASKS_TASK_NO_TAGS"] = "Keine";
$MESS["TASKS_TASK_FILES"] = "Dateien";
$MESS["TASKS_TASK_SUBTASKS"] = "Teilaufgaben";
$MESS["TASKS_TASK_PREVIOUS_TASKS"] = "Vorherige Aufgaben";
$MESS["TASKS_TASK_LOG"] = "Änderungshistory";
$MESS["TASKS_SIDEBAR_STATUS"] = "Status";
$MESS["TASKS_SIDEBAR_START_DATE"] = "Seit";
$MESS["TASKS_SIDEBAR_START"] = "Beginn";
$MESS["TASKS_SIDEBAR_FINISH"] = "Ende";
$MESS["TASKS_SIDEBAR_DURATION"] = "Dauer";
$MESS["TASKS_SIDEBAR_IN_REPORT"] = "Im Bericht";
$MESS["TASKS_SIDEBAR_IN_REPORT_YES"] = "Ja";
$MESS["TASKS_SIDEBAR_IN_REPORT_NO"] = "Nein";
$MESS["TASKS_SIDEBAR_REPEAT"] = "Wiederholt sich";
$MESS["TASKS_SIDEBAR_TEMPLATE"] = "Vorlage";
$MESS["TASKS_SIDEBAR_TEMPLATE_NOT_EXISTS"] = "Die Aufgabe kann nicht mehr gestartet werden, da die Vorlage gelöscht wurde.";
$MESS["TASKS_SIDEBAR_CHANGE"] = "Ändern";
$MESS["TASKS_SIDEBAR_ACCOMPLICES"] = "Mitwirkende";
$MESS["TASKS_SIDEBAR_AUDITORS"] = "Beobachter";
$MESS["TASKS_SIDEBAR_ADD_ACCOMPLICES"] = "Mitwirkende hinzufügen";
$MESS["TASKS_SIDEBAR_ADD_AUDITORS"] = "Beobachter hinzufügen";
$MESS["TASKS_SIDEBAR_DEADLINE_NO"] = "Kein";
$MESS["TASKS_SIDEBAR_STOP_WATCH_CONFIRM"] = "Möchten Sie diese Aufgabe wirklich nicht mehr beobachten?";
$MESS["TASKS_ADD_BACK_TO_TASKS_LIST"] = "Zurück zu Aufgaben";
$MESS["TASKS_HOURS_N"] = "Stunde";
$MESS["TASKS_HOURS_G"] = "Stunde";
$MESS["TASKS_HOURS_P"] = "Stunden";
$MESS["TASKS_NO_TEMPLATES"] = "Keine Vorlagen sind verfügbar";
$MESS["TASKS_PARENT_TASK"] = "Übergeordnete Aufgabe";
$MESS["TASKS_TASK_GROUP"] = "Aufgabe ist diesem Projekt zugeordnet";
$MESS["TASKS_NO_SUBTASKS"] = "Keine Teilaufgaben";
$MESS["TASKS_LOG_NEW"] = "Aufgabe wurde erstellt";
$MESS["TASKS_LOG_TITLE"] = "Aufgabenname";
$MESS["TASKS_LOG_DESCRIPTION"] = "Beschreibung wurde aktualisiert";
$MESS["TASKS_LOG_RESPONSIBLE_ID"] = "Verantwortliche Person";
$MESS["TASKS_LOG_DEADLINE"] = "Fällig am";
$MESS["TASKS_LOG_ACCOMPLICES"] = "Mitwirkende";
$MESS["TASKS_LOG_AUDITORS"] = "Beobachter";
$MESS["TASKS_LOG_FILES"] = "Dateien";
$MESS["TASKS_LOG_TAGS"] = "Tags";
$MESS["TASKS_LOG_PRIORITY"] = "Priorität";
$MESS["TASKS_LOG_GROUP_ID"] = "Gruppe";
$MESS["TASKS_LOG_PARENT_ID"] = "Übergeordnete Aufgabe";
$MESS["TASKS_LOG_DEPENDS_ON"] = "Vorherige Aufgaben";
$MESS["TASKS_LOG_STATUS"] = "Status";
$MESS["TASKS_LOG_MARK"] = "Bewertung";
$MESS["TASKS_LOG_ADD_IN_REPORT"] = "Im Bericht";
$MESS["TASKS_LOG_WHEN"] = "Datum";
$MESS["TASKS_LOG_WHO"] = "Erstellt von";
$MESS["TASKS_LOG_WHERE"] = "Wo geändert";
$MESS["TASKS_LOG_DELETED_FILES"] = "Dateien wurden gelöscht";
$MESS["TASKS_LOG_NEW_FILES"] = "Dateien wurden hinzugefügt";
$MESS["TASKS_LOG_COMMENT"] = "Kommentar wurde erstellt";
$MESS["TASKS_LOG_COMMENT_EDIT"] = "Kommentar geändert";
$MESS["TASKS_LOG_COMMENT_DEL"] = "Kommentar gelöscht";
$MESS["TASKS_LOG_START_DATE_PLAN"] = "Voraussichtliches Anfangsdatum";
$MESS["TASKS_LOG_END_DATE_PLAN"] = "Voraussichtliches Abschlussdatum";
$MESS["TASKS_LOG_DURATION_PLAN"] = "Voraussichtliche Dauer";
$MESS["TASKS_LOG_DURATION_FACT"] = "Aktuelle Dauer";
$MESS["TASKS_LOG_CHECKLIST_ITEM_CREATE"] = "Ein Element wurde zur Checkliste hinzugefügt";
$MESS["TASKS_LOG_CHECKLIST_ITEM_REMOVE"] = "Ein Element wurde aus der Checkliste entfernt";
$MESS["TASKS_LOG_CHECKLIST_ITEM_RENAME"] = "Das Element der Checkliste wurde umbenannt.";
$MESS["TASKS_DELEGATE_TASK"] = "Delegieren";
$MESS["TASKS_COPY_TASK_EX"] = "Aufgabe duplizieren";
$MESS["TASKS_ELAPSED_TIME"] = "Aufgewendete Zeit";
$MESS["TASKS_ELAPSED_TIME_SHORT"] = "Zeitaufwand";
$MESS["TASKS_ELAPSED_H"] = "h";
$MESS["TASKS_ELAPSED_M"] = "m";
$MESS["TASKS_ELAPSED_AUTHOR"] = "Erstellt von";
$MESS["TASKS_ELAPSED_DATE"] = "Datum";
$MESS["TASKS_ELAPSED_COMMENT"] = "Anmerkungen";
$MESS["TASKS_ELAPSED_ADD"] = "Hinzufügen";
$MESS["TASKS_REMINDER_TITLE"] = "Erinnern";
$MESS["TASKS_ABOUT_DEADLINE"] = "An Frist";
$MESS["TASKS_BY_DATE"] = "Nach Datum";
$MESS["TASKS_REMIND_VIA_JABBER"] = "Per Instant Message";
$MESS["TASKS_REMIND_VIA_EMAIL"] = "Per E-Mail";
$MESS["TASKS_REMIND_VIA_JABBER_EX"] = "Instant Message senden";
$MESS["TASKS_REMIND_VIA_EMAIL_EX"] = "Nachricht per E-Mail senden";
$MESS["TASKS_REMIND_BEFORE"] = "#NUM# Tag(e) davor";
$MESS["TASKS_REMINDER_OK"] = "OK";
$MESS["TASKS_ADD_SUBTASK_2"] = "Teilaufgabe erstellen";
$MESS["TASKS_QUICK_IN_GROUP"] = "Projektaufgabe";
$MESS["TASKS_QUICK_DESCRIPTION"] = "Beschreibung";
$MESS["TASKS_DATE_CREATED"] = "Erstellt am";
$MESS["TASKS_DATE_START"] = "Voraussichtliches Anfangsdatum";
$MESS["TASKS_DATE_END"] = "Voraussichtliches Abschlussdatum";
$MESS["TASKS_DATE_STARTED"] = "Angefangen am";
$MESS["TASKS_DATE_COMPLETED"] = "Abgeschlossen am";
$MESS["TASKS_TASK_TITLE_LABEL"] = "Aufgabe";
$MESS["TASKS_STATUS_OVERDUE"] = "Überfällige";
$MESS["TASKS_STATUS_NEW"] = "Neu";
$MESS["TASKS_STATUS_ACCEPTED"] = "Anstehend";
$MESS["TASKS_STATUS_IN_PROGRESS"] = "In Arbeit";
$MESS["TASKS_STATUS_WAITING"] = "Muss kontrolliert werden";
$MESS["TASKS_STATUS_COMPLETED"] = "Abgeschlossen";
$MESS["TASKS_STATUS_DELAYED"] = "Verschoben";
$MESS["TASKS_STATUS_DECLINED"] = "Abgelehnt";
$MESS["TASKS_QUICK_INFO_DETAILS"] = "Details";
$MESS["TASKS_QUICK_INFO_EMPTY_DATE"] = "Keine";
$MESS["TASKS_GROUP_ADD"] = "Hinzufügen";
$MESS["TASKS_TASK_DURATION_HOURS"] = "Stunden";
$MESS["TASKS_TASK_DURATION_DAYS"] = "Tage";
$MESS["TASKS_TASK_DURATION_MINUTES"] = "Minuten";
$MESS["TASKS_DETAIL_CHECKLIST"] = "Checkliste";
$MESS["TASKS_DETAIL_CHECKLIST_ADD"] = "hinzufügen";
$MESS["TASKS_LOG_CREATED_BY"] = "Erstellt von";
$MESS["TASKS_LOG_DURATION_PLAN_SECONDS"] = "Voraussichtliche Dauer";
$MESS["TASKS_LOG_TIME_ESTIMATE"] = "Geplante Zeit";
$MESS["TASKS_LOG_TIME_SPENT_IN_LOGS"] = "Aufgewendete Zeit";
$MESS["TASKS_TASK_DURATION_HOURS_PLURAL_0"] = "Stunde";
$MESS["TASKS_TASK_DURATION_HOURS_PLURAL_1"] = "Stunde";
$MESS["TASKS_TASK_DURATION_HOURS_PLURAL_2"] = "Stunden";
$MESS["TASKS_TASK_DURATION_DAYS_PLURAL_0"] = "Tag";
$MESS["TASKS_TASK_DURATION_DAYS_PLURAL_1"] = "Tage";
$MESS["TASKS_TASK_DURATION_DAYS_PLURAL_2"] = "Tage";
$MESS["TASKS_TASK_DURATION_MINUTES_PLURAL_0"] = "Minute";
$MESS["TASKS_TASK_DURATION_MINUTES_PLURAL_1"] = "Minute";
$MESS["TASKS_TASK_DURATION_MINUTES_PLURAL_2"] = "Minuten";
$MESS["TASKS_TASK_DURATION_SECONDS_PLURAL_0"] = "Sekunde";
$MESS["TASKS_TASK_DURATION_SECONDS_PLURAL_1"] = "Sekunden";
$MESS["TASKS_TASK_DURATION_SECONDS_PLURAL_2"] = "Sekunden";
$MESS["TASKS_ELAPSED_S"] = "s";
$MESS["TASKS_GROUP_LOADING"] = "Bitte warten...";
$MESS["TASKS_LOG_CHECKLIST_ITEM_UNCHECK"] = "Der Punkt der Checkliste wurde nicht erledigt.";
$MESS["TASKS_LOG_CHECKLIST_ITEM_CHECK"] = "Der Punkt der Checkliste wurde erledigt.";
$MESS["TASKS_ELAPSED_SOURCE_UNDEFINED"] = "Es ist unbekannt, ob dieser Eintrag hinzugefügt oder manuell geändert wurde";
$MESS["TASKS_ELAPSED_SOURCE_MANUAL"] = "Dieser Eintrag wurde manuell hinzugefügt oder geändert";
$MESS["TASKS_LIST_GROUP_ACTION_ADJUST_DEADLINE"] = "Frist nach vorne verschieben";
$MESS["TASKS_LIST_GROUP_ACTION_SUBSTRACT_DEADLINE"] = "Frist nach hinten verschieben";
$MESS["TASKS_COLUMN_TAG"] = "Tags";
$MESS["TASKS_CLOSE_PAGE_CONFIRM"] = "Die vorgenommenen Änderungen können evtl. verloren gehen.";
?>