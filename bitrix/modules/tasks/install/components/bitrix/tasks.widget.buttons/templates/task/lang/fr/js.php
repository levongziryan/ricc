<?
$MESS["TASKS_COPY_TASK"] = "Copier";
$MESS["TASKS_ADD_TASK_TO_TIMEMAN"] = "Add to working day plan";
$MESS["TASKS_COPY_TASK_EX"] = "Duplicate task";
$MESS["TASKS_ADD_TASK_TO_TIMEMAN_EX"] = "Add to working day plan";
$MESS["TASKS_ADD_SUBTASK"] = "Créer une sous-tâche";
$MESS["TASKS_DELETE_TASK"] = "Supprimer";
$MESS["TASKS_DELETE_CONFIRM"] = "Do you really want to delete it?";
$MESS["TASKS_DEFER_TASK"] = "Reporter";
$MESS["TASKS_RENEW_TASK"] = "Reprendre";
$MESS["TASKS_DELEGATE_TASK"] = "Déléguer";
$MESS["TASKS_TASK_CONFIRM_START_TIMER_TITLE"] = "Le tracker de temps est pour l'instant utilisé avec une autre tâche.";
$MESS["TASKS_TASK_CONFIRM_START_TIMER"] = "Vous utilisez déjà le tracker de temps pour \"{{TITLE}}\". Cette tâche va être suspendue. Continuer ?";
$MESS["TASKS_UNKNOWN"] = "Inconnu";
?>