<?
$MESS["INTL_OWNER_ID"] = "ID de propietario";
$MESS["INTL_PATH_TO_USER_PROFILE"] = "Ruta del perfil de usuario";
$MESS["INTL_PATH_TO_GROUP_TASKS"] = "Ruta a la página de tareas del grupo de trabajo";
$MESS["INTL_PATH_TO_GROUP_TASKS_TASK"] = "Ruta de tareas del grupo de trabajo";
$MESS["INTL_PATH_TO_USER_TASKS"] = "Ruta a la página de tareas del usuario";
$MESS["INTL_PATH_TO_USER_TASKS_TASK"] = "Ruta a la tarea del usuario";
$MESS["INTL_ITEM_COUNT"] = "Elementos por página";
$MESS["INTL_NAME_TEMPLATE"] = "Plantilla de visualización de nombres";
$MESS["INTL_USER_ID"] = "ID del usuario";
$MESS["INTL_GROUP_ID"] = "ID del grupo de tareas";
?>