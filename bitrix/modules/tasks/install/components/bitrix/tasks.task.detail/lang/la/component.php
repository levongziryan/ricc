<?
$MESS["TASKS_MODULE_NOT_INSTALLED"] = "El módulo Tasks no está instalado.";
$MESS["FORUM_MODULE_NOT_INSTALLED"] = "El módulo Forum no está instalado.";
$MESS["SOCNET_MODULE_NOT_INSTALLED"] = "El módulo Social Network no está instalado.  ";
$MESS["TASKS_BAD_TASK_ID"] = "El ID de la tarea es inválido.";
$MESS["TASKS_TASK_NOT_FOUND"] = "La tarea no fue encontrada, o su acceso se denegó.";
$MESS["TASKS_TASK_NUM"] = "Tarea ##TASK_NUM#";
$MESS["TASKS_DATE_MUST_BE_IN_FUTURE"] = "La fecha de record debe ser posterior a la fecha actual.";
$MESS["TASKS_TASK_UNABLE_TO_DELETE"] = "No se puede eliminar la tarea";
?>