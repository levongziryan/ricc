<?
$MESS["TASKS_MODULE_NOT_INSTALLED"] = "Le module Tasks n'est pas installé.";
$MESS["FORUM_MODULE_NOT_INSTALLED"] = "Le module Forum n'est pas installé.";
$MESS["TASKS_BAD_TASK_ID"] = "ID de tâche invalide.";
$MESS["TASKS_TASK_NOT_FOUND"] = "La tâche est introuvable ou l'accès est refusé.";
$MESS["TASKS_TASK_NUM"] = "Tâche ##TASK_NUM#";
$MESS["SOCNET_MODULE_NOT_INSTALLED"] = "Le module Réseau social n'est pas installé.";
$MESS["TASKS_DATE_MUST_BE_IN_FUTURE"] = "La date du rappel doit être postérieure à la date du jour.";
$MESS["TASKS_TASK_UNABLE_TO_DELETE"] = "Impossible de supprimer la tâche";
$MESS["TASKS_LOG_DEADLINE"] = "Date limite";
?>