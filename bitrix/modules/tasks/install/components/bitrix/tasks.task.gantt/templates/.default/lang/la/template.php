<?
$MESS["TASKS_GANTT_MONTH_JAN"] = "Enero";
$MESS["TASKS_GANTT_MONTH_FEB"] = "Febrero";
$MESS["TASKS_GANTT_MONTH_MAR"] = "Marzo";
$MESS["TASKS_GANTT_MONTH_APR"] = "Abril";
$MESS["TASKS_GANTT_MONTH_MAY"] = "Mayo";
$MESS["TASKS_GANTT_MONTH_JUN"] = "Junio";
$MESS["TASKS_GANTT_MONTH_JUL"] = "Julio";
$MESS["TASKS_GANTT_MONTH_AUG"] = "Agosto";
$MESS["TASKS_GANTT_MONTH_SEP"] = "Septiembre";
$MESS["TASKS_GANTT_MONTH_OCT"] = "Octubre";
$MESS["TASKS_GANTT_MONTH_NOV"] = "Noviembre";
$MESS["TASKS_GANTT_MONTH_DEC"] = "Diciembre";
$MESS["TASKS_CANNOT_ADD_DEPENDENCY"] = "No se pueden vincular estas tareas";
$MESS["TASKS_TITLE_GROUP_TASKS"] = "Tareas del grupo de trabajo";
$MESS["TASKS_TITLE"] = "Tareas";
$MESS["TASKS_TITLE_MY"] = "Mis tareas";
?>