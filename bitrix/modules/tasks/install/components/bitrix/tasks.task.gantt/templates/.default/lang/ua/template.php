<?
$MESS["TASKS_GANTT_MONTH_JAN"] = "Січень";
$MESS["TASKS_GANTT_MONTH_FEB"] = "Лютий";
$MESS["TASKS_GANTT_MONTH_MAR"] = "Березень";
$MESS["TASKS_GANTT_MONTH_APR"] = "Квітень";
$MESS["TASKS_GANTT_MONTH_MAY"] = "Травень";
$MESS["TASKS_GANTT_MONTH_JUN"] = "Червень";
$MESS["TASKS_GANTT_MONTH_JUL"] = "Липень";
$MESS["TASKS_GANTT_MONTH_AUG"] = "Серпень";
$MESS["TASKS_GANTT_MONTH_SEP"] = "Вересень";
$MESS["TASKS_GANTT_MONTH_OCT"] = "Жовтень";
$MESS["TASKS_GANTT_MONTH_NOV"] = "Листопад";
$MESS["TASKS_GANTT_MONTH_DEC"] = "Грудень";
$MESS["TASKS_CANNOT_ADD_DEPENDENCY"] = "Неможливо додати зв'язок між даними задачами";
$MESS["TASKS_TITLE_GROUP_TASKS"] = "Завдання групи";
$MESS["TASKS_TITLE"] = "Завдання";
$MESS["TASKS_TITLE_MY"] = "Мої завдання";
$MESS["TASKS_CLOSE_PAGE_CONFIRM"] = "Можливо, внесені зміни не будуть збережені.";
?>