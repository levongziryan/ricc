<?
$MESS["TASKS_GANTT_MONTH_JAN"] = "Janvier";
$MESS["TASKS_GANTT_MONTH_FEB"] = "Février";
$MESS["TASKS_GANTT_MONTH_MAR"] = "Mars";
$MESS["TASKS_GANTT_MONTH_APR"] = "Avril";
$MESS["TASKS_GANTT_MONTH_MAY"] = "Mai";
$MESS["TASKS_GANTT_MONTH_JUN"] = "Juin";
$MESS["TASKS_GANTT_MONTH_JUL"] = "Juillet";
$MESS["TASKS_GANTT_MONTH_AUG"] = "Août";
$MESS["TASKS_GANTT_MONTH_SEP"] = "Septembre";
$MESS["TASKS_GANTT_MONTH_OCT"] = "Octobre";
$MESS["TASKS_GANTT_MONTH_NOV"] = "Novembre";
$MESS["TASKS_GANTT_MONTH_DEC"] = "Décembre";
$MESS["TASKS_CANNOT_ADD_DEPENDENCY"] = "Impossible de lier ces tâches";
$MESS["TASKS_TITLE_GROUP_TASKS"] = "Tâches du groupe de travail";
$MESS["TASKS_TITLE"] = "Tâches";
$MESS["TASKS_TITLE_MY"] = "Mes tâches";
?>