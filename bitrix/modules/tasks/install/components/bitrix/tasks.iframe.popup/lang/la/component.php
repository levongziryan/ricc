<?
$MESS["TASKS_MODULE_NOT_FOUND"] = "El módulo de la Tarea no está instalado.";
$MESS["TASKS_TASK_CONFIRM_START_TIMER_TITLE"] = "El seguimiento de tiempo se está utilizando ahora con otra tarea.";
$MESS["TASKS_TASK_CONFIRM_START_TIMER"] = "Ya está utilizando el seguimiento de tiempo para \"{{TITLE}}\". Esta tarea se detendrá. ¿Desea continuar?";
?>