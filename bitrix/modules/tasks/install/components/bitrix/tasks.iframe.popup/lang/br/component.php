<?
$MESS["TASKS_MODULE_NOT_FOUND"] = "O módulo de tarefas não está instalado.";
$MESS["TASKS_TASK_CONFIRM_START_TIMER_TITLE"] = "O gerenciador de tempo está sendo usado agora com outra tarefa.";
$MESS["TASKS_TASK_CONFIRM_START_TIMER"] = "Você já está usando o gerenciador de tempo para \"{{TITLE}}\". Esta tarefa será pausada. Continuar?";
?>