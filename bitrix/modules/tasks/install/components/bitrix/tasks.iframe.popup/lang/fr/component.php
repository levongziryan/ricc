<?
$MESS["TASKS_MODULE_NOT_FOUND"] = "Le module de commande des tâches n'a pas été installé.";
$MESS["TASKS_TASK_CONFIRM_START_TIMER_TITLE"] = "Le tracker de temps est pour l'instant utilisé avec une autre tâche.";
$MESS["TASKS_TASK_CONFIRM_START_TIMER"] = "Vous utilisez déjà le tracker de temps pour \"{{TITLE}}\". Cette tâche va être suspendue. Continuer ?";
?>