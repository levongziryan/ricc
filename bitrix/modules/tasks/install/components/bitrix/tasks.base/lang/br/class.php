<?
$MESS["TASKS_TB_TASKS_MODULE_NOT_INSTALLED"] = "O módulo Tarefas não está instalado.";
$MESS["TASKS_TB_TASKS_MODULE_NOT_AVAILABLE"] = "O módulo Tarefas está disponível nesta edição.";
$MESS["TASKS_TB_USER_NOT_AUTHORIZED"] = "Você não tem permissão para visualizar tarefas";
?>