<?
$MESS["TASKS_CSL_T_MESSAGE_TYPE_1"] = "Notificação";
$MESS["TASKS_CSL_T_MESSAGE_TYPE_2"] = "Aviso";
$MESS["TASKS_CSL_T_MESSAGE_TYPE_3"] = "Erro";
$MESS["TASKS_CSL_T_NO_ERROR"] = "Não";
$MESS["TASKS_CSL_T_COL_TYPE"] = "Tipo";
$MESS["TASKS_CSL_T_COL_DATE"] = "Data";
$MESS["TASKS_CSL_T_COL_MESSAGE"] = "Mensagem";
$MESS["TASKS_CSL_T_COL_ERRORS"] = "Erros";
?>