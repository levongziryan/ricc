<?
$MESS["BPSNMA_EMPTY_TASKASSIGNEDTO"] = "No se especifica el valor del 'Responsable'.";
$MESS["BPSNMA_EMPTY_TASKNAME"] = "No se especifica el valor del nombre de la tarea.";
$MESS["TASK_EMPTY_GROUP"] = "Tarea personal";
$MESS["BPSA_TRACK_OK"] = "Crear una tarea con ID ##VAL#";
$MESS["BPTA1A_TASKGROUPID"] = "Grupo de redes sociales";
$MESS["BPTA1A_TASKCREATEDBY"] = "Crear la tarea como";
$MESS["BPTA1A_TASKASSIGNEDTO"] = "Responsable";
$MESS["BPTA1A_TASKACTIVEFROM"] = "Inicio";
$MESS["BPTA1A_TASKACTIVETO"] = "Final";
$MESS["BPTA1A_TASKDEADLINE"] = "Fecha límite";
$MESS["BPTA1A_TASKNAME"] = "Nombre de tarea";
$MESS["BPTA1A_TASKDETAILTEXT"] = "Descripcion de  tarea";
$MESS["BPTA1A_TASKTRACKERS"] = "Segumiento";
$MESS["BPTA1A_TASKACCOMPLICES"] = "Participantes";
$MESS["BPTA1A_TASKFORUM"] = "Foro de comentarios de tarea";
$MESS["BPTA1A_CHECK_RESULT"] = "Resultado de la tarea de control";
$MESS["BPTA1A_ADD_TO_REPORT"] = "Incluir tareas en informe de productividad";
$MESS["BPTA1A_CHANGE_DEADLINE"] = "Persona responsable puede cambiar fecha límite";
$MESS["BPCDA_FIELD_REQUIED"] = "El campo '#FIELD#' requerido";
$MESS["BPSA_TRACK_SUBSCR"] = "El proceso está esperando finalización de tareas";
$MESS["BPSA_TRACK_CLOSED"] = "Tarea cerrado en #DATE#";
$MESS["BPSA_TRACK_ERROR"] = "Cuando fue creada se produjo un error de la tarea.";
$MESS["BPTA1A_ALLOW_TIME_TRACKING"] = "Habilitar seguimiento de tiempos para tareas";
$MESS["BPTA1A_TASKPRIORITY_V2"] = "Prioridad";
?>