<?
$MESS["BC_PATH_TO_BLOG"] = "Ruta de plantilla de la página de blog";
$MESS["BC_BLOG_VAR"] = "Variable identificador de Blog";
$MESS["BC_PAGE_VAR"] = "Variable de página";
$MESS["BC_BLOG_URL"] = "URL de blog";
$MESS["BC_YEAR"] = "Filtro por año";
$MESS["BC_MONTH"] = "Filtro por mes";
$MESS["BC_DAY"] = "FIltro por dia";
$MESS["B_VARIABLE_ALIASES"] = "Alias para la variable";
?>