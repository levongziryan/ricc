<?
$MESS["BLOG_MES_HIDE_POST_CONFIRM"] = "tes-vous sûr de vouloir cacher ce message?";
$MESS["BLOG_MES_DELETE_POST_CONFIRM"] = "?tes-vous sûr de vouloir supprimer le message?";
$MESS["BLOG_BLOG_BLOG_COMMENTS"] = "Nombre de commentaires:";
$MESS["BLOG_BLOG_BLOG_VIEWS"] = "vues:";
$MESS["BLOG_BLOG_BLOG_EDIT"] = "Editer";
$MESS["BLOG_MES_HIDE"] = "Cacher";
$MESS["BLOG_BLOG_BLOG_NO_AVAIBLE_MES"] = "Message introuvable";
$MESS["BLOG_BLOG_BLOG_CATEGORY"] = "Tags:";
$MESS["BLOG_BLOG_BLOG_DELETE"] = "Supprimer";
$MESS["BLOG_PHOTO"] = "Photo:";
?>