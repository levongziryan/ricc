<?
$MESS["BLOG_MES_SHOW_POST_CONFIRM"] = "tes-vous sûr de vouloir publier ce message?";
$MESS["BLOG_MES_DELETE_POST_CONFIRM"] = "?tes-vous sûr de vouloir supprimer le message?";
$MESS["BLOG_BLOG_BLOG_COMMENTS"] = "Nombre de commentaires:";
$MESS["BLOG_MES_SHOW"] = "Publier";
$MESS["BLOG_BLOG_BLOG_VIEWS"] = "vues:";
$MESS["BLOG_MES_EDIT"] = "Editer";
$MESS["BLOG_BLOG_BLOG_CATEGORY"] = "Tags:";
$MESS["B_B_DRAFT_NO_MES"] = "Vous n'avez pas de messages inachevés dans ce blogue";
$MESS["BLOG_MES_DELETE"] = "Supprimer";
$MESS["BLOG_PHOTO"] = "Photo(s):";
?>