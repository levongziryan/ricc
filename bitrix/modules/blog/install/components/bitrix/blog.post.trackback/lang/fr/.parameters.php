<?
$MESS["BPH_ID"] = "Identifiant du message";
$MESS["B_VARIABLE_ALIASES"] = "Alias des variables";
$MESS["BPH_BLOG_VAR"] = "Nom de la variable pour l'identificateur de l'utilisateur";
$MESS["BPH_POST_VAR"] = "Le nom de la variable pour l'identificateur du message du blog";
$MESS["BPH_PAGE_VAR"] = "Nom de la variable pour la page";
$MESS["BPH_BLOG_URL"] = "Adresse du blogue à afficher";
$MESS["BC_DATE_TIME_FORMAT"] = "Format d'affichage de la date et de l'heure";
$MESS["BPH_PATH_TO_TRACKBACK"] = "Modèle de chemin trackback";
?>