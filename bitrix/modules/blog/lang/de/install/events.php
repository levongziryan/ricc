<?
$MESS["NEW_BLOG_COMMENT2COMMENT_WITHOUT_TITLE_DESC"] = "#BLOG_ID# - Blog ID 
#BLOG_NAME# - Blogüberschrift
#BLOG_URL# - Blog URL
#COMMENT_TITLE# - Kommentarüberschrift
#COMMENT_TEXT# - Kommentartext
#COMMENT_DATE# - Kommentardatum
#COMMENT_PATH# - Adresse (URL) zum Kommentar
#AUTHOR# - Autor
#EMAIL_FROM# - E-Mail des Absenders
#EMAIL_TO# - E-Mail des Empfängers";
$MESS["NEW_BLOG_COMMENT_WITHOUT_TITLE_DESC"] = "#BLOG_ID# - Blog ID 
#BLOG_NAME# - Blogüberschrift
#BLOG_URL# - Blog URL
#COMMENT_TITLE# - Kommentarüberschrift
#COMMENT_TEXT# - Kommentartext
#COMMENT_DATE# - Kommentardatum
#COMMENT_PATH# - Adresse (URL) zum Kommentar
#AUTHOR# - Autor
#EMAIL_FROM# - E-Mail des Absenders
#EMAIL_TO# - E-Mail des Empfängers";
$MESS["NEW_BLOG_COMMENT_DESC"] = "#BLOG_ID# - Blog ID 
#BLOG_NAME# - Blogüberschrift
#BLOG_URL# - Blog URL
#COMMENT_TITLE# - Kommentarüberschrift
#COMMENT_TEXT# - Kommentartext
#COMMENT_DATE# - Kommentardatum
#COMMENT_PATH# - Adresse (URL) zum Kommentar
#AUTHOR# - Autor
#EMAIL_FROM# - E-Mail des Absenders
#EMAIL_TO# - E-Mail des Empfängers";
$MESS["NEW_BLOG_COMMENT2COMMENT_DESC"] = "#BLOG_ID# - Blog ID 
#BLOG_NAME# - Blogüberschrift
#BLOG_URL# - Blog URL
#COMMENT_TITLE# - Kommentarüberschrift
#COMMENT_TEXT# - Kommentartext
#COMMENT_DATE# - Kommentardatum
#COMMENT_PATH# - Adresse (URL) zum Kommentar
#AUTHOR# - Autor
#EMAIL_FROM# - E-Mail des Absenders
#EMAIL_TO# - E-Mail des Empfängers";
$MESS["NEW_BLOG_MESSAGE_DESC"] = "#BLOG_ID# - Blog ID 
#BLOG_NAME# - Blogüberschrift
#BLOG_URL# - Blog URL
#MESSAGE_TITLE# - Überschrift
#MESSAGE_TEXT#  - Text
#MESSAGE_DATE# - Datum
#MESSAGE_PATH# - Adresse (URL) zum Beitrag
#AUTHOR# - Autor
#EMAIL_FROM# - E-Mail des Absenders
#EMAIL_TO# - E-Mail des Empfängers";
$MESS["BLOG_YOUR_BLOG_TO_USER_DESC"] = "#BLOG_ID# - Blog-ID
#BLOG_NAME# - Blogname
#BLOG_URL# - Blogname, nur lateinische Buchstaben
#BLOG_ADR# - Blogaddresse
#USER_ID# - Benutzer-ID
#USER# - Benutzer
#USER_URL# - Benutzer-URL
#EMAIL_FROM# - Absender-E-Mail
#EMAIL_TO# - Empfänger-E-Mail
";
$MESS["BLOG_YOU_TO_BLOG_DESC"] = "#BLOG_ID# - Blog-ID
#BLOG_NAME# - Blogname
#BLOG_URL# - Blogname, nur lateinische Buchstaben
#BLOG_ADR# - Blogaddresse
#USER_ID# - Benutzer-ID
#USER# - Benutzer
#USER_URL# - Benutzer-URL
#EMAIL_FROM# - Absender-E-Mail
#EMAIL_TO# - Empfänger-E-Mail
";
$MESS["BLOG_BLOG_TO_YOU_DESC"] = "#BLOG_ID# - Blog-ID
#BLOG_NAME# - Blogname
#BLOG_URL# - Blogname, nur lateinische Buchstaben
#BLOG_ADR# - Blogaddresse
#USER_ID# - Benutzer-ID
#USER# - Benutzer
#USER_URL# - Benutzer-URL
#EMAIL_FROM# - Absender-E-Mail
#EMAIL_TO# - Empfänger-E-Mail
";
$MESS["BLOG_USER_TO_YOUR_BLOG_DESC"] = "#BLOG_ID# - Blog-ID
#BLOG_NAME# - Blogname
#BLOG_URL# - Blogname, nur lateinische Buchstaben
#BLOG_ADR# - Blogaddresse
#USER_ID# - Benutzer-ID
#USER# - Benutzer
#USER_URL# - Benutzer-URL
#EMAIL_FROM# - Absender-E-Mail
#EMAIL_TO# - Empfänger-E-Mail";
$MESS["NEW_BLOG_MESSAGE_SUBJECT"] = "#SITE_NAME#: [B] #BLOG_NAME# : #MESSAGE_TITLE#";
$MESS["NEW_BLOG_COMMENT_WITHOUT_TITLE_SUBJECT"] = "#SITE_NAME#: [B] #MESSAGE_TITLE#";
$MESS["NEW_BLOG_COMMENT2COMMENT_WITHOUT_TITLE_SUBJECT"] = "#SITE_NAME#: [B] #MESSAGE_TITLE#";
$MESS["NEW_BLOG_COMMENT_SUBJECT"] = "#SITE_NAME#: [B] #MESSAGE_TITLE# : #COMMENT_TITLE#";
$MESS["NEW_BLOG_COMMENT2COMMENT_SUBJECT"] = "#SITE_NAME#: [B] #MESSAGE_TITLE# : #COMMENT_TITLE#";
$MESS["BLOG_USER_TO_YOUR_BLOG_SUBJECT"] = "#SITE_NAME#: [B] Ihr Blog \"#BLOG_NAME#\" hat einen neuen Freund #USER#.";
$MESS["BLOG_BLOG_TO_YOU_SUBJECT"] = "#SITE_NAME#: [B] Der Blog \"#BLOG_NAME#\" wurde zu Ihren Freunden hinzugefügt.";
$MESS["BLOG_YOU_TO_BLOG_SUBJECT"] = "#SITE_NAME#: [B] Sie wurden zum  \"#BLOG_NAME#\" als Freund hinzugefügt.";
$MESS["BLOG_YOUR_BLOG_TO_USER_SUBJECT"] = "#SITE_NAME#: [B] Ihr Blog \"#BLOG_NAME#\" wurde als Freund von #USER#'s hinzugefügt.";
$MESS["BLOG_BLOG_TO_YOU_NAME"] = "Ein Blog wurde zu Ihren Freunden hinzugefügt";
$MESS["BLOG_USER_TO_YOUR_BLOG_NAME"] = "Ein Freund wurde zu Ihrem Blog hinzugefügt";
$MESS["NEW_BLOG_COMMENT2COMMENT_WITHOUT_TITLE_MESSAGE"] = "Informationsnachricht von #SITE_NAME#
------------------------------------------

Es gibt einen neuen Kommentar zu Ihrem Kommentar im Blog \"#BLOG_NAME#\" zur Nachricht \"#MESSAGE_TITLE#\"

Autor: #AUTHOR#
Datum: #COMMENT_DATE#

Nachrichtentext:
#COMMENT_TEXT#

Adresse:
#COMMENT_PATH#

Dies ist eine automatisch generierte Nachricht.";
$MESS["NEW_BLOG_COMMENT2COMMENT_MESSAGE"] = "Informationsnachricht von #SITE_NAME#
------------------------------------------

Es gibt einen neuen Kommentar zu Ihrem Kommentar im Blog \"#BLOG_NAME#\" zur Nachricht \"#MESSAGE_TITLE#\"

Thema:
#COMMENT_TITLE#
Autor: #AUTHOR#
Datum: #COMMENT_DATE#

Nachrichtentext:
#COMMENT_TEXT#

Adresset:
#COMMENT_PATH#

Dies ist eine automatisch generierte Nachricht.";
$MESS["NEW_BLOG_COMMENT_WITHOUT_TITLE_MESSAGE"] = "Informationsnachricht von #SITE_NAME#
------------------------------------------

Es gibt einen neuen Kommentar im Blog \"#BLOG_NAME#\" zur Nachricht \"#MESSAGE_TITLE#\"

Autor: #AUTHOR#
Datum: #COMMENT_DATE#

Nachrichtentext:
#COMMENT_TEXT#

Adresse:
#COMMENT_PATH#

Dies ist eine automatisch generierte Nachricht.";
$MESS["NEW_BLOG_COMMENT_MESSAGE"] = "Informationsnachricht von #SITE_NAME#
------------------------------------------

Es gibt einen neuen Kommentar im Blog \"#BLOG_NAME#\" zur Nachricht \"#MESSAGE_TITLE#\"

Thema:
#COMMENT_TITLE#
Autor: #AUTHOR#
Datum: #COMMENT_DATE#

Nachrichtentext:
#COMMENT_TEXT#

Adresse:
#COMMENT_PATH#

Dies ist eine automatisch generierte Nachricht.";
$MESS["NEW_BLOG_MESSAGE_MESSAGE"] = "Informationsnachricht von #SITE_NAME#
------------------------------------------

Es gibt eine neue Nachricht im Blog \"#BLOG_NAME#\"

Thema:
#MESSAGE_TITLE#

Autor: #AUTHOR#
Datum: #MESSAGE_DATE#

Nachrichtentext:
#MESSAGE_TEXT#

Adresse:
#MESSAGE_PATH#

Dies ist eine automatisch generierte Nachricht.";
$MESS["BLOG_USER_TO_YOUR_BLOG_MESSAGE"] = "Nachricht von #SITE_NAME#
------------------------------------------

#USER# wurde als Ihr Freund Ihres Blogs \"#BLOG_NAME#\" hinzugefügt.

Benutzerprofil: #USER_URL#

Ihre Blogseite: #BLOG_ADR#

Diese Nachricht wurde automatisch erstellt.
";
$MESS["BLOG_BLOG_TO_YOU_MESSAGE"] = "Nachricht von #SITE_NAME#
------------------------------------------

Das Blog \"#BLOG_NAME#\" wurde zu Ihren Freunden hinzugefügt.

Die Blogadresse: #BLOG_ADR#

Ihr Profil: #USER_URL#

Diese Nachricht wurde automatisch erstellt.
";
$MESS["BLOG_YOU_TO_BLOG_MESSAGE"] = "Nachricht von #SITE_NAME#
------------------------------------------

Sie wurden als Freund zu \"#BLOG_NAME#\" hinzugefügt.

Blog-URL: #BLOG_ADR#

Ihr Profil: #USER_URL#

Diese Nachricht wurde automatisch erstellt.
";
$MESS["BLOG_YOUR_BLOG_TO_USER_MESSAGE"] = "Nachricht von #SITE_NAME#
------------------------------------------

Ihr Blog \"#BLOG_NAME#\" wurde als Freund von #USER# hinzugefügt.

Benutzerprofil: #USER_URL#

Ihre Blog-URL: #BLOG_ADR#

Diese Nachricht wurde automatisch erstellt.
";
$MESS["NEW_BLOG_MESSAGE_NAME"] = "Neuer Beitrag im Blog";
$MESS["NEW_BLOG_COMMENT2COMMENT_NAME"] = "Neuer Kommentar zu Ihrem Kommentar im Blog";
$MESS["NEW_BLOG_COMMENT2COMMENT_WITHOUT_TITLE_NAME"] = "Neuer Kommentar zu Ihrem Kommentar im Blog (ohne Thema)";
$MESS["NEW_BLOG_COMMENT_NAME"] = "Neuer Kommentar im Blog";
$MESS["NEW_BLOG_COMMENT_WITHOUT_TITLE_NAME"] = "Neuer Kommentar im Blog (ohne Thema)";
$MESS["BLOG_YOU_TO_BLOG_NAME"] = "Sie wurden als Blogfreund hinzugefügt";
$MESS["BLOG_YOUR_BLOG_TO_USER_NAME"] = "Ihr Blog wurde zu den Freunden hinzugefügt";
$MESS["BLOG_SONET_NEW_POST_NAME"] = "Neuer Beitrag wurde hinzugefügt";
$MESS["BLOG_SONET_NEW_COMMENT_NAME"] = "Kommentar hinzugefügt";
$MESS["BLOG_SONET_NEW_POST_DESC"] = "#EMAIL_TO# - E-Mail des Empfängers
#POST_ID# - ID des Beitrags
#URL_ID# - URL des Beitrags";
$MESS["BLOG_SONET_NEW_COMMENT_DESC"] = "#EMAIL_TO# - E-Mail des Empfängers
#COMMENT_ID# - ID des Kommentars
#POST_ID# - ID des Beitrags
#URL_ID# - URL des Beitrags";
$MESS["BLOG_SONET_POST_SHARE_NAME"] = "Neuer Empfänger hinzugefügt";
$MESS["BLOG_SONET_POST_SHARE_DESC"] = "#EMAIL_TO# - E-Mail des Empfängers
#POST_ID# - ID des Beitrags
#URL_ID# - URL des Beitrags";
$MESS["BLOG_POST_BROADCAST_NAME"] = "Neue Nachricht hinzugefügt";
$MESS["BLOG_POST_BROADCAST_DESC"] = "
#MESSAGE_TITLE# - Betreff der Nachricht
#MESSAGE_TEXT# - Text der Nachricht 
#MESSAGE_PATH# - URL der Nachricht
#AUTHOR# - Autor der Nachricht
#EMAIL_TO# - E-Mail des Empfängers
";
$MESS["BLOG_POST_BROADCAST_SUBJECT"] = "#SITE_NAME#: #MESSAGE_TITLE#";
$MESS["BLOG_POST_BROADCAST_MESSAGE"] = "Meldung von der Website #SITE_NAME#
------------------------------------------

Eine neue Nachricht wurde hinzugefügt.

Betreff:
#MESSAGE_TITLE#

Autor: #AUTHOR#

Nachrichtentext:
#MESSAGE_TEXT#

Nachrichtenadresse:
#MESSAGE_PATH#

Diese Nachricht wurde automatisch erzeugt.
";
?>