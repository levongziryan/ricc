<?
$MESS ['BLOG_IMAGE_NOTE'] = "(empfohlene Abmessungen für Smileys: 16 x 16px, Icons: 15 x 15px)";
$MESS ['BLOG_TYPING_NOTE'] = "(Sie können mehrere Varianten angeben, indem Sie diese mit einem Leerzeichen trennen)";
$MESS ['FSN_DELETE_SMILE_CONFIRM'] = "Wollen Sie diesen Smiley wirklich löschen?";
$MESS ['FSN_TAB_SMILE'] = "Blogsmiley";
$MESS ['FSN_TAB_SMILE_DESCR'] = "Blogsmiley";
$MESS ['BLOG_CODE'] = "Code";
$MESS ['FSN_DELETE_SMILE'] = "Smiley löschen";
$MESS ['BLOG_EDIT_RECORD'] = "Smiley Nr. #ID# ändern";
$MESS ['ERROR_ADD_SMILE'] = "Beim Erstellen des Smileys ist ein Fehler aufgetreten";
$MESS ['ERROR_EDIT_SMILE'] = "Beim Ändern des Smileys ist ein Fehler aufgetreten";
$MESS ['ERROR_COPY_IMAGE'] = "Beim Hochladen des Bildes ist ein Fehler aufgetreten. Wahrscheinlich haben Sie nicht genügend Schreibrechte";
$MESS ['FSE_ICON'] = "Icon";
$MESS ['FSE_ERROR_EXT'] = "Unzulässige Erweiterung der Bilddatei";
$MESS ['ERROR_EXISTS_IMAGE'] = "Ein Bild mit diesem Namen existiert bereits";
$MESS ['BLOG_NAME'] = "Überschrift";
$MESS ['ERROR_NO_NAME'] = "Die sprachabhängige Bezeichnung des Smileys wurde nicht angegeben";
$MESS ['FSN_NEW_SMILE'] = "Neuer Smiley";
$MESS ['BLOG_NEW_RECORD'] = "Neuer Smiley";
$MESS ['ERROR_NO_IMAGE'] = "Geben Sie das Bild für den Smiley an.";
$MESS ['ERROR_NO_TYPE'] = "Geben Sie den Smiley-Typ an.";
$MESS ['FSE_SMILE'] = "Smiley";
$MESS ['BLOG_IMAGE'] = "Smiley-Bild";
$MESS ['BLOG_TYPE'] = "Smiley-Typ";
$MESS ['BLOG_TYPING'] = "Schreibweise des Smileys";
$MESS ['FSN_2FLIST'] = "Smileys";
$MESS ['BLOG_SORT'] = "Sortierung";
?>