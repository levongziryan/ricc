<?
$MESS["SMILE_DEL_CONF"] = "Wollen Sie diesen Smiley wirklich löschen?";
$MESS["SMILE_TITLE"] = "Blogsmileys";
$MESS["FSAN_ADD_NEW_ALT"] = "Klicken Sie hier, um einen neuen Smiley hizuzufügen";
$MESS["BLOG_DELETE_DESCR"] = "Smiley löschen";
$MESS["BLOG_EDIT_DESCR"] = "Smiley-Parameter bearbeiten";
$MESS["ERROR_DEL_SMILE"] = "Beim Löschen des Smileys ist ein Fehler aufgetreten.";
$MESS["SMILE_TYPE_ICON"] = "Icon";
$MESS["SMILE_ID"] = "ID";
$MESS["BLOG_SMILE_ICON"] = "Bild";
$MESS["BLOG_NAME"] = "Überschrift";
$MESS["FSAN_ADD_NEW"] = "Neuer Smiley";
$MESS["PAGES"] = "Smileys";
$MESS["SMILE_TYPE_SMILE"] = "Smiley";
$MESS["SMILE_SORT"] = "Sort.";
$MESS["SMILE_TYPE"] = "Typ";
$MESS["BLOG_TYPING"] = "Schreibweise";
$MESS["MAIN_ADMIN_LIST_SELECTED"] = "Gesamt:";
$MESS["MAIN_ADMIN_LIST_CHECKED"] = "Ausgewählt:";
$MESS["MAIN_ADMIN_LIST_DELETE"] = "löschen";
?>