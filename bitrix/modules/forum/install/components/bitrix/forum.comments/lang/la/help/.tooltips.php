<?
$MESS["FORUM_ID_TIP"] = "ID de la discusión del Foro";
$MESS["IBLOCK_TYPE_TIP"] = "Tipo de Block de información (sólo para verificación)";
$MESS["IBLOCK_ID_TIP"] = "ID del Block de Información";
$MESS["ELEMENT_ID_TIP"] = "ID del elemento";
$MESS["URL_TEMPLATES_READ_TIP"] = "Página de vista del tema del foro";
$MESS["URL_TEMPLATES_DETAIL_TIP"] = "Página del elemento del Bock de Información";
$MESS["URL_TEMPLATES_PROFILE_VIEW_TIP"] = "Página del perfil del usuario";
$MESS["MESSAGES_PER_PAGE_TIP"] = "Número de Mensajes Por Página";
$MESS["PAGE_NAVIGATION_TEMPLATE_TIP"] = "Plantilla del breadcrumb de navegación";
$MESS["DATE_TIME_FORMAT_TIP"] = "Formato de Fecha y Hora";
$MESS["USE_CAPTCHA_TIP"] = "Utiliza CAPTCHA";
$MESS["PREORDER_TIP"] = "Mostrar primeros los primeros mensajes";
$MESS["CACHE_TYPE_TIP"] = "Tipo de cache";
$MESS["CACHE_TIME_TIP"] = "Duración del Caché (seg.)";
$MESS["EDITOR_CODE_DEFAULT_TIP"] = "Por defecto editor en modo de texto plano";
$MESS["SHOW_AVATAR_TIP"] = "Mostrar avatars";
$MESS["SHOW_RATING_TIP"] = "Mostrar calificación";
$MESS["SHOW_MINIMIZED_TIP"] = "Colapsar formulario de respuesta";
$MESS["ENTITY_TYPE_TIP"] = "Una entidad de tipo ID, debe contener sólo dos caracteres Latinos. Ejemplo: tipo de entidad tarea es TK.";
$MESS["ENTITY_ID_TIP"] = "Un ID de entidad numérica. Ejemplo: para un ID de tarea es la 348 y el ID de la entidad es 348.";
$MESS["ENTITY_XML_ID_TIP"] = "Un ID entidad alfanumérico (ID XML). Puede incluir caracteres alfanuméricos y un guión bajo. Ejemplo: para un ID de tarea es la 348, la entidad ID alfanumérico es TASKS_348.";
$MESS["PERMISSION_TIP"] = "Los permios externos sobreescriben los permisos de usuarios del foro. A < E < I < P < U < Y. A - acceso denegado; E - leer; I - responder; Q - moderar; U - editar; Y - acceso completo.";
?>