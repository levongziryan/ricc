<?
$MESS["PM_FOLDER_ID_1"] = "Boîte de réception";
$MESS["JS_DEL_MESSAGES"] = "Etes-vous sûr de vouloir supprimer tous les messages dans les dossiers?";
$MESS["JS_DEL_FOLDERS"] = "tes-vous sûr de vouloir supprimer les dossiers?";
$MESS["PM_POST_FULLY"] = "Rempissage de la boîte:";
$MESS["F_EDIT"] = "Editer";
$MESS["PM_FOLDER_ID_2"] = "Messages sortants";
$MESS["PM_FOLDER_ID_4"] = "Corbeille";
$MESS["PM_FOLDER_TITLE"] = "Dénomination du dossier:";
$MESS["JS_NO_DATA"] = "Aucun dossier n'a été choisi. Choisissez un dossier.";
$MESS["PM_HEAD_NEW_FOLDER"] = "Créer un nouveau dossier";
$MESS["PM_HEAD_NEW_MESSAGE"] = "Nouveau message";
$MESS["PM_FOLDER_ID_3"] = "Envoyés";
$MESS["F_REMOVE"] = "Effacer";
$MESS["F_FOLDER"] = "Dossier";
$MESS["PM_PM"] = "messages";
$MESS["F_MESSAGES"] = "...messages";
$MESS["F_DELETE"] = "Supprimer";
?>