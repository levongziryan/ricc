<?
$MESS["LU_TITLE_LTA"] = "Auteur des thèmes";
$MESS["F_POINTS"] = "Nombre de points:";
$MESS["LU_TITLE_ALL"] = "Tous les messages de l'utilisateur";
$MESS["F_ALL_FORUMS"] = "Tous les forums";
$MESS["LU_DATE_CREATE"] = "Ajouté(e)s";
$MESS["F_CLOSED"] = "Accompli";
$MESS["F_EDIT_HEAD"] = "Changé:";
$MESS["F_EDIT"] = "Date de modification";
$MESS["FR_EMPTY"] = "Introuvable";
$MESS["LU_BY_MESSAGE"] = "par messages";
$MESS["LU_BY_TOPIC"] = "d'après les sujets";
$MESS["F_ATTACH_FILES"] = "Copies des billets";
$MESS["F_AUTHOR_PROFILE"] = "Directions d'activité";
$MESS["F_DATE_REGISTER"] = "Enregistrement:";
$MESS["F_ANCHOR_TITLE"] = "Copier l'adresse de référence à ce message dans la Presse-papiers";
$MESS["LU_MESSAGE"] = "Message";
$MESS["LU_USER_POSTS_ON_TOPIC"] = "...de messages dans le thème";
$MESS["F_NUM_MESS"] = "Messages:";
$MESS["F_ANCHOR"] = "Rééférence à ce message";
$MESS["LU_TOPIC"] = "Sujet";
$MESS["LU_SORT"] = "Trier par nom";
$MESS["LU_TITLE_LT"] = "Membre des sujets";
$MESS["LU_FORUM"] = "Forum";
?>