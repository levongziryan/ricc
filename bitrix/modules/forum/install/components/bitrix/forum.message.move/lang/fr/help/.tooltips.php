<?
$MESS["MID_TIP"] = "ID du message";
$MESS["TID_TIP"] = "ID du sujet";
$MESS["FID_TIP"] = "Forum ID";
$MESS["CACHE_TIME_TIP"] = "Temps de mise en cache (s).";
$MESS["WORD_LENGTH_TIP"] = "Longueur du mot";
$MESS["SET_NAVIGATION_TIP"] = "Afficher la navigation";
$MESS["PATH_TO_ICON_TIP"] = "Chemin vers le dossier avec les icônes pour les sujets (relativement à la racine du site).";
$MESS["PATH_TO_SMILE_TIP"] = "Chemin d'accès au dossier avec smileys (par rapport à la racine du site)";
$MESS["IMAGE_SIZE_TIP"] = "Taille de l'image attachée (px)";
$MESS["URL_TEMPLATES_TOPIC_SEARCH_TIP"] = "Page de recherche thème du forum";
$MESS["URL_TEMPLATES_PROFILE_VIEW_TIP"] = "Page du profil de l'utilisateur";
$MESS["URL_TEMPLATES_LIST_TIP"] = "Page de la liste des sujets";
$MESS["URL_TEMPLATES_INDEX_TIP"] = "Liste des forums";
$MESS["URL_TEMPLATES_READ_TIP"] = "Page de lecture du sujet";
$MESS["CACHE_TYPE_TIP"] = "Type de la mise en cache";
$MESS["SET_TITLE_TIP"] = "Installer l'en-tête de la page";
$MESS["DATE_FORMAT_TIP"] = "Format d'affichage de la date";
$MESS["DATE_TIME_FORMAT_TIP"] = "Format d'affichage de la date et de l'heure";
?>