<?
$MESS["LU_FILTER_AVATAR"] = "Avatar";
$MESS["F_ALLOW_POST"] = "pode postar";
$MESS["F_FORBID_POST"] = "não pode postar";
$MESS["FLU_HEAD_DATE_REGISTER"] = "Cadastrado";
$MESS["LU_FILTER_LAST_VISIT"] = "ltima visita";
$MESS["FLU_HEAD_LAST_VISIT"] = "ltima visita";
$MESS["LU_FILTER_SORT_LAST_VISIT"] = "data da última visita";
$MESS["LU_FILTER_SORT_NAME"] = "nome";
$MESS["FLU_HEAD_NAME"] = "Nome:";
$MESS["F_STATUS_NONE"] = "nenhum";
$MESS["LU_FILTER_AVATAR_TITLE"] = "Somente com avatar";
$MESS["FLU_HEAD_POINTS"] = "Pontos";
$MESS["LU_FILTER_SORT_POINTS"] = "Pontos";
$MESS["FLU_HEAD_POST"] = "Posts";
$MESS["LU_FILTER_SORT_NUM_POSTS"] = "posts";
$MESS["F_AUTHOR_PROFILE"] = "Perfil";
$MESS["FLU_EMPTY"] = "Não foram encontrados usuários registrados no fórum.";
$MESS["LU_FILTER_SORT_DATE_REGISTER"] = "data de registro";
$MESS["LU_FILTER_SORT"] = "Ordenar por";
$MESS["F_STATUS_USER"] = "Status";
$MESS["LU_TITLE_USER"] = "Lista de Usuários";
$MESS["LU_FILTER_USER_NAME"] = "Nome de usuário";
?>