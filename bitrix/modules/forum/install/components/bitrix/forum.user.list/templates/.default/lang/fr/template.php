<?
$MESS["LU_FILTER_AVATAR"] = "Avatar";
$MESS["FLU_HEAD_POINTS"] = "Par le nombre de points";
$MESS["LU_FILTER_LAST_VISIT"] = "Date de la dernière activité";
$MESS["F_FORBID_POST"] = "il est interdit d'écrire";
$MESS["FLU_EMPTY"] = "Les utilisateurs enregistrés du forum sont introuvables.";
$MESS["FLU_HEAD_NAME"] = "Dénomination";
$MESS["LU_FILTER_USER_NAME"] = "Le nom contient";
$MESS["F_STATUS_NONE"] = "aucun";
$MESS["LU_FILTER_SORT_LAST_VISIT"] = "d'après la date de dernière visite";
$MESS["LU_FILTER_SORT_DATE_REGISTER"] = "Date d'enregistrement";
$MESS["LU_FILTER_SORT_NAME"] = "par le nom";
$MESS["LU_FILTER_SORT_POINTS"] = "par le nombre de points";
$MESS["LU_FILTER_SORT_NUM_POSTS"] = "par le nombre de messages";
$MESS["LU_TITLE_USER"] = "Liste des utilisateurs";
$MESS["FLU_HEAD_LAST_VISIT"] = "Date de la dernière activité";
$MESS["F_AUTHOR_PROFILE"] = "Directions d'activité";
$MESS["F_ALLOW_POST"] = "il est permis d'écrire";
$MESS["FLU_HEAD_DATE_REGISTER"] = "a adhéré au groupe";
$MESS["FLU_HEAD_POST"] = "Messages";
$MESS["F_STATUS_USER"] = "Statut";
$MESS["LU_FILTER_AVATAR_TITLE"] = "Seulement avec avatar";
$MESS["LU_FILTER_SORT"] = "Trier par nom";
?>