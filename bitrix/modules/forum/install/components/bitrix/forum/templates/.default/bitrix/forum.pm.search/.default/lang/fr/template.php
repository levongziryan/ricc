<?
$MESS["PM_SEARCH_NOTHING"] = "Désolé, votre recherche n'a donné aucun résultat.";
$MESS["PM_IS_FINED"] = "Transférer un message uniquement lors d'envoi aux utilisateurs autorisés";
$MESS["PM_SEARCH"] = "Recherche";
$MESS["PM_NOT_FINED"] = "Ne pas déchargé";
$MESS["PM_CANCEL"] = "Annuler";
$MESS["PM_SEARCH_INSERT"] = "Chaîne de recherche";
$MESS["PM_TITLE"] = "Recherche de l'utilisateur";
$MESS["PM_SEARCH_PATTERN"] = "Au cours de la recherche utilisez '*'.<br/>Exemple: 'I*' - tous les noms commençant par I.";
?>