<?
$MESS["PM_HEAD_AUTHOR"] = "Auteur";
$MESS["PM_ACT_IN"] = "dans";
$MESS["PM_HEAD_DATE"] = "Date";
$MESS["PM_POST_FULLY"] = "Rempissage de la boîte:";
$MESS["JS_NO_MESSAGES"] = "Aucun message a été séléctionné. Sélectionnez les messages.";
$MESS["PM_EMPTY_FOLDER"] = "Aucun message";
$MESS["PM_HEAD_SENDER"] = "Expéditeur";
$MESS["PM_ACT_MOVE"] = "Déplacement";
$MESS["PM_HEAD_RECIPIENT"] = "Dans";
$MESS["JS_DEL_MESSAGE"] = "Les messages seront supprimés sans possibilité de restauration. Continuer?";
$MESS["PM_HEAD_SUBJ"] = "En-tête";
$MESS["PM_ACT_DELETE"] = "Supprimer";
?>