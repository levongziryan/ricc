<?
$MESS["FORUM_ID_TIP"] = "ID du forum pour les avis d'utilisateurs";
$MESS["ELEMENT_ID_TIP"] = "Identifiant de l'élément";
$MESS["CACHE_TIME_TIP"] = "Temps de mise en cache (s).";
$MESS["SHOW_RATING_TIP"] = "Afficher le classement";
$MESS["PREORDER_TIP"] = "Afficher les messages en ordre direct";
$MESS["USE_CAPTCHA_TIP"] = "Utiliser CAPTCHA";
$MESS["IBLOCK_ID_TIP"] = "Code du bloc d'information";
$MESS["MESSAGES_PER_PAGE_TIP"] = "Nombre de messages sur une page";
$MESS["PAGE_NAVIGATION_TEMPLATE_TIP"] = "Modèle de pagination";
$MESS["EDITOR_CODE_DEFAULT_TIP"] = "Afficher par défaut le mode non visuel de l'éditeur";
$MESS["SHOW_AVATAR_TIP"] = "Afficher les avatars des utilisateurs";
$MESS["PATH_TO_SMILE_TIP"] = "Chemin par rapport à la racine du site vers le dossier avec des smileys";
$MESS["SHOW_MINIMIZED_TIP"] = "Pliez la forme d'ajout d'une opinion";
$MESS["URL_TEMPLATES_PROFILE_VIEW_TIP"] = "Modèle d'un lien au profil d'utilisateur";
$MESS["URL_TEMPLATES_READ_TIP"] = "Page de lecture du sujet du forum";
$MESS["URL_TEMPLATES_DETAIL_TIP"] = "lément de bloc de l'information n'a pas été trouvé";
$MESS["IBLOCK_TYPE_TIP"] = "Type du bloc d'information (n'est utilisé que pour la vérification)";
$MESS["CACHE_TYPE_TIP"] = "Type de la mise en cache";
$MESS["DATE_TIME_FORMAT_TIP"] = "Format d'affichage de la date et de l'heure";
?>