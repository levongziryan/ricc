<?
$MESS["FORUM_ID_TIP"] = "ID do fórum de discussão";
$MESS["IBLOCK_TYPE_TIP"] = "Tipo de bloco de informação (somente para verificação)";
$MESS["IBLOCK_ID_TIP"] = "ID do Bloco de informações";
$MESS["ELEMENT_ID_TIP"] = "ID do elemento";
$MESS["POST_FIRST_MESSAGE_TIP"] = "Usar texto do elemento como primeiro post";
$MESS["POST_FIRST_MESSAGE_TEMPLATE_TIP"] = "Texto para o primeiro post do tópico";
$MESS["URL_TEMPLATES_READ_TIP"] = "Página de visualização de tópico do fórum";
$MESS["URL_TEMPLATES_DETAIL_TIP"] = "Página do elemento bloco de informação";
$MESS["URL_TEMPLATES_PROFILE_VIEW_TIP"] = "Página de perfil de usuário";
$MESS["MESSAGES_PER_PAGE_TIP"] = "Número de mensagens por página";
$MESS["PAGE_NAVIGATION_TEMPLATE_TIP"] = "Modelo de Navegação Breadcrumb";
$MESS["DATE_TIME_FORMAT_TIP"] = "Formato de data e hora";
$MESS["PATH_TO_SMILE_TIP"] = "Caminho da pasta Smiley (em relação à raiz do site)";
$MESS["USE_CAPTCHA_TIP"] = "Usar CAPTCHA";
$MESS["PREORDER_TIP"] = "Mostrar os primeiros posts primeiro";
$MESS["CACHE_TYPE_TIP"] = "Tipo de cache";
$MESS["CACHE_TIME_TIP"] = "Tempo de cache (seg.)";
$MESS["EDITOR_CODE_DEFAULT_TIP"] = "Padrão para o modo de edição de texto simples";
$MESS["SHOW_AVATAR_TIP"] = "Mostrar avatares";
$MESS["SHOW_RATING_TIP"] = "Mostrar classificação";
$MESS["SHOW_MINIMIZED_TIP"] = "Expandir formulário de resposta";
?>