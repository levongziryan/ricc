<?
$MESS["forum_template_forums_all"] = "(tous les forums)";
$MESS["F_THEME_FLUXBB"] = "FluxBB";
$MESS["F_THEME_BEIGE"] = "Beige";
$MESS["F_THEME_WHITE"] = "Blanc";
$MESS["forum_template_theme"] = "Thème visuel:";
$MESS["forum_template_vote_channel_select"] = "Choisir un groupe existant";
$MESS["F_THEME_BLUE"] = "Bleu";
$MESS["forum_template_vote_channel"] = "Groupe de sondages:";
$MESS["forum_template_vote_groups"] = "Groupes des utilisateurs qui peuvent créer une enquête:";
$MESS["F_THEME_GREEN"] = "Vert";
$MESS["F_THEME_RED"] = "Rouge";
$MESS["forum_template_vote"] = "Réglages de l'enquête";
$MESS["forum_template_settings"] = "Réglages des forums";
$MESS["forum_template_vote_name"] = "Les votes rapides de forum";
$MESS["F_THEME_ORANGE"] = "Orange";
$MESS["forum_template_forums"] = "Afficher seulement les forums sélectionnés:";
$MESS["forum_template_vote_enable"] = "Autoriser des enquêtes:";
$MESS["F_THEME_GRAY"] = "Grèce";
$MESS["forum_template_vote_channel_new"] = "Créer un Nouveau Groupe d'Enquêtes";
$MESS["forum_template_desc"] = "Page d'aide par forum";
$MESS["forum_template_name"] = "Forum (commande numérique)";
?>