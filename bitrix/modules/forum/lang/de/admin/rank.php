<?
$MESS ['RANK_TITLE'] = "Ränge des Forums";
$MESS ['RANK_DEL_CONF'] = "Wollen Sie diesen Rang wirklich löschen? Wenn es noch User mit diesem Rang gibt, werden sie konvertiert (dies kann viel Zeit in Anspruch nehmen)";
$MESS ['RANK_NAV'] = "Ränge";
$MESS ['RANK_ID'] = "ID";
$MESS ['RANK_MIN_NUM_POSTS'] = "Minimale Anzahl der Beiträge";
$MESS ['RANK_DEL'] = "Löschen";
$MESS ['ERROR_DEL_RANK'] = "Beim Löschen des Ranges ist ein Fehler aufgetreten.";
$MESS ['FORUM_ADD_RANK'] = "Neuer Rang";
$MESS ['FORUM_NAME'] = "Überschrift";
$MESS ['FORUM_ACTIONS'] = "Aktionen";
$MESS ['FORUM_EDIT_DESCR'] = "Rang-Parameter bearbeiten";
$MESS ['FORUM_EDIT'] = "Ändern";
$MESS ['FORUM_DELETE_DESCR'] = "Rang löschen";
$MESS ['FR_SUCCESS_DEL'] = "Der Rang wurde erfolgreich gelöscht";
?>