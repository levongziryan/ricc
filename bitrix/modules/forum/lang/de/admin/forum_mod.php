<?
$MESS ['ACCESS_DENIED'] = "Zugriff verweigert!";
$MESS ['FORUM_MODERATE'] = "Foren: Moderation";
$MESS ['BACK'] = "Zurück zu den Foren";
$MESS ['CREATED'] = "Erstellt";
$MESS ['SHOW_DESCR'] = "Beitrag Gästen anzeigen";
$MESS ['SHOW'] = "Genehmigen";
$MESS ['DEL_DESCR'] = "Beitrag löschen. Wenn das der letzte Beitrag im Thema ist, wird das Thema ebenfalls gelöscht";
$MESS ['DEL'] = "Löschen";
$MESS ['TO_TOP'] = "Nach oben";
$MESS ['MESSAGES'] = "Nachrichten";
$MESS ['FM_NO_PERMS2DEL'] = "Sie haben nicht genügend Rechte, um einen Beitrag zu löschen";
$MESS ['FM_MODER'] = "Moderator";
$MESS ['FM_EDITOR'] = "Redakteur";
$MESS ['FM_ADMIN'] = "Administrator";
$MESS ['FM_GUEST'] = "Gast";
$MESS ['FM_NUM_ALL_MESS'] = "Beiträge insgesamt";
$MESS ['FM_DATE_REG'] = "Registrierungsdatum";
$MESS ['FM_GUEST_ID'] = "Gast ID";
$MESS ['FM_REAL'] = "real";
?>