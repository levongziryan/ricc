<?
$MESS ['FFI_TITLE'] = "Foren";
$MESS ['FFI_TAB_DESCR'] = "Forenstatistik";
$MESS ['FFI_FORUM_CNT'] = "Gesamt Foren";
$MESS ['FFI_FORUM_ACTIVE_CNT'] = "Aktive Foren";
$MESS ['FFI_FORUM_LANG_CNT'] = "Foren auf der Seite &quot;#LANG#&quot;";
$MESS ['FFI_TOPICS_CNT'] = "Gesamt Themen im Forum";
$MESS ['FFI_MESSAGES_CNT'] = "Gesamt Foren-Beiträge";
$MESS ['FFI_USERS_CNT'] = "Gesamt Forum-User";
$MESS ['FFI_USERS_ACTIVE_CNT'] = "davon aktiv";
?>