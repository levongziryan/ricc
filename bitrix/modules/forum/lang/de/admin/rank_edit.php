<?
$MESS ['FORUM_EDIT_RECORD'] = "Eintrag Nr. #ID# bearbeiten";
$MESS ['FORUM_NEW_RECORD'] = "Neuer Rang";
$MESS ['FORUM_RECORDS_LIST'] = "Ränge";
$MESS ['FORUM_SAVE'] = "Speichern";
$MESS ['FORUM_ADD'] = "Hinzufügen";
$MESS ['FORUM_APPLY'] = "Anwenden";
$MESS ['FORUM_RESET'] = "Abbrechen";
$MESS ['ERROR_NO_MIN_NUM_POSTS'] = "Geben Sie die minimale Anzahl der Beiträge für diesen Rang an.";
$MESS ['ERROR_NO_NAME'] = "Die sprachabhängige Rangbezeichnung wurde nicht angegeben";
$MESS ['ERROR_EDIT_RANK'] = "Beim Ändern des Ranges ist ein Fehler aufgetreten.";
$MESS ['ERROR_ADD_RANK'] = "Beim Erstellen des Ranges ist ein Fehler aufgetreten.";
$MESS ['FORUM_SORT'] = "Minimale Anzahl der Beiträge";
$MESS ['FORUM_CODE'] = "ID";
$MESS ['FORUM_NAME'] = "Überschrift";
$MESS ['FORUM_PT_PROPS'] = "Parameter";
?>