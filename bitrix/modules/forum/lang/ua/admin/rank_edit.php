<?
$MESS["FORUM_RECORDS_LIST"] = "До списку звань";
$MESS["FORUM_ADD"] = "Додати";
$MESS["FORUM_EDIT_RECORD"] = "Змінення звання ##ID#";
$MESS["FORUM_CODE"] = "Код";
$MESS["FORUM_SORT"] = "Мінімальна кількість повідомлень";
$MESS["FORUM_NAME"] = "Назва";
$MESS["ERROR_NO_NAME"] = "Не зазначена назва звання мовою";
$MESS["FORUM_NEW_RECORD"] = "Нове звання";
$MESS["FORUM_RESET"] = "Скасування";
$MESS["ERROR_ADD_RANK"] = "Помилка додавання звання.";
$MESS["ERROR_EDIT_RANK"] = "Помилка змінення звання.";
$MESS["FORUM_PT_PROPS"] = "Параметри";
$MESS["FORUM_APPLY"] = "Застосувати";
$MESS["FORUM_SAVE"] = "Зберегти";
$MESS["ERROR_NO_MIN_NUM_POSTS"] = "Встановіть мінімальну кількість повідомлень для цього звання.";
?>