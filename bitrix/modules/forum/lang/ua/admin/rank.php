<?
$MESS["RANK_DEL_CONF"] = "Ви впевнені, що хочете видалити це звання? Якщо є користувачі з цим званням, то вони будуть перераховані (це може зайняти тривалий час).";
$MESS["FORUM_ACTIONS"] = "Дії";
$MESS["FORUM_ADD_RANK"] = "Додати звання";
$MESS["FR_SUCCESS_DEL"] = "Звання успішно видалено";
$MESS["RANK_NAV"] = "Звання";
$MESS["RANK_TITLE"] = "Звання форуму";
$MESS["FORUM_EDIT"] = "Змінити";
$MESS["FORUM_EDIT_DESCR"] = "Змінити параметри звання";
$MESS["RANK_ID"] = "Код";
$MESS["RANK_MIN_NUM_POSTS"] = "Мінімальна кількість повідомлень";
$MESS["FORUM_NAME"] = "Назва";
$MESS["ERROR_DEL_RANK"] = "Помилка видалення звання.";
$MESS["RANK_DEL"] = "Видалити";
$MESS["FORUM_DELETE_DESCR"] = "Видалити звання";
?>