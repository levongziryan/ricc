<?
$MESS["FFI_FORUM_ACTIVE_CNT"] = "Активних форумів";
$MESS["FFI_USERS_CNT"] = "Усього відвідувачів форума";
$MESS["FFI_MESSAGES_CNT"] = "Усього повідомлень на форумах";
$MESS["FFI_TOPICS_CNT"] = "Усього тем на форумах";
$MESS["FFI_FORUM_CNT"] = "Усього форумів";
$MESS["FFI_USERS_ACTIVE_CNT"] = "з них активних";
$MESS["FFI_TAB_DESCR"] = "Статистика за форумами";
$MESS["FFI_FORUM_LANG_CNT"] = "Форумів на сайті &quot;#LANG#&quot;";
$MESS["FFI_TITLE"] = "Форуми";
?>