<?php

namespace Bitrix\Disk\Internals\Engine;

use \Bitrix\Main\Engine;
use Bitrix\Main\EventManager;

class Controller extends Engine\Controller
{
	protected function init()
	{
		parent::init();

		Engine\Binder::registerParameterDependsOnName(
			\Bitrix\Disk\Internals\Model::className(),
			function($className, $id){
				/** @var \Bitrix\Disk\Internals\Model $className */
				return $className::getById($id);
			}
		);
	}

	protected function processAfterAction(Engine\Action $action, $result)
	{
		if ($this->errorCollection->getErrorByCode(Engine\ActionFilter\Csrf::ERROR_INVALID_CSRF))
		{
			return Engine\Response\AjaxJson::createDenied()->setStatus('403 Forbidden');
		}

		return $result;
	}
}