<?
$MESS["DISK_JS_STATUS_ACTION_SUCCESS"] = "Успішно";
$MESS["DISK_JS_STATUS_ACTION_ERROR"] = "Виникла помилка";
$MESS["DISK_JS_SHARING_LABEL_RIGHTS_FOLDER"] = "Загальний доступ до папки";
$MESS["DISK_JS_SHARING_LABEL_NAME_RIGHTS_USER"] = "Користувачі";
$MESS["DISK_JS_SHARING_LABEL_NAME_RIGHTS"] = "Права доступу";
$MESS["DISK_JS_SHARING_LABEL_NAME_ADD_RIGHTS_USER"] = "Додати ще";
$MESS["DISK_JS_SHARING_LABEL_NAME_ALLOW_SHARING_RIGHTS_USER"] = "Дозволити налаштування загального доступу";
$MESS["DISK_JS_SHARING_LABEL_RIGHT_READ"] = "Читання";
$MESS["DISK_JS_SHARING_LABEL_RIGHT_EDIT"] = "Редагування";
$MESS["DISK_JS_SHARING_LABEL_RIGHT_FULL"] = "Повний доступ";
$MESS["DISK_JS_SHARING_LABEL_TOOLTIP_SHARING"] = "Всі співробітники, що мають доступ до папки,<br/>зможуть додавати нових учасників загального доступу";
$MESS["DISK_JS_SHARING_LABEL_OWNER"] = "Власник";
$MESS["DISK_JS_SHARING_LABEL_TITLE_MODAL"] = "Параметри загального доступу";
$MESS["DISK_JS_BTN_CLOSE"] = "Закрити";
$MESS["DISK_JS_BTN_SAVE"] = "Зберегти";
$MESS["DISK_JS_SHARING_LABEL_TITLE_MODAL_2"] = "Загальний доступ";
$MESS["DISK_JS_SERVICE_CHOICE_TITLE_SMALL"] = "Вибір способу роботи";
$MESS["DISK_JS_SERVICE_CHOICE_TITLE"] = "Оберіть зручний спосіб роботи з документами";
$MESS["DISK_JS_SERVICE_CHANGE_TEXT"] = "Змінити вибір можна буде за посиланням Ще або в налаштуваннях Диска";
$MESS["DISK_JS_SERVICE_LOCAL_TITLE"] = "Локально";
$MESS["DISK_JS_SERVICE_LOCAL_TEXT"] = "Документи завжди будуть відкриватися в програмі на вашому комп'ютері";
$MESS["DISK_JS_SERVICE_CLOUD_TITLE"] = "В хмарі";
$MESS["DISK_JS_SERVICE_CLOUD_TEXT"] = "Ви зможете працювати з документами Google Docs або MS Office Web App";
$MESS["DISK_JS_SERVICE_HELP_TEXT"] = "Змінити ваш вибір можна буде за посиланням Ще";
$MESS["DISK_JS_SERVICE_HELP_TEXT_2"] = "Змінити ваш вибір можна буде у налаштуваннях";
$MESS["DISK_JS_SHARING_LABEL_RIGHT_ADD"] = "Додавання";
$MESS["DISK_JS_USER_LOCKED_DOCUMENT"] = "Заблокував документ";
$MESS["DISK_JS_PLAYER_ERROR_MESSAGE"] = "На жаль, ваш браузер не може відтворити цей файл.<br />Ви можете <span class=\"disk-player-download\">завантажити</span> та переглянути його на комп'ютері";
?>