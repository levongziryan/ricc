<?
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_TITLE"] = "Документ не збережено!";
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_WAS_LOCKED_FORKED_COPY"] = "Під час редагування документ був заблокований. Всі внесені вами зміни буде збережено у новому документі, на вашому диску в папці <a class=\"disk-locked-document-popup-content-link\" href=\"#LINK#\" target=\"_blank\">Збережені</a>.";
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_GO_TO_FILE"] = "Перейти до файлу";
?>