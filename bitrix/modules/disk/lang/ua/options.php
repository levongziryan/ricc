<?
$MESS["DISK_ALLOW_CREATE_FILE_BY_CLOUD"] = "Включити роботу з документами через зовнішні служби (Google Docs, MS Office Online та інші)";
$MESS["DISK_ALLOW_AUTOCONNECT_SHARED_OBJECTS"] = "Автоматично підключати Диск групи<br/>при вступі користувача в групу";
$MESS["DISK_ALLOW_EDIT_OBJECT_IN_UF"] = "Дозволити редагувати документи, які беруть участь в обговореннях,<br/>завданнях, коментарях або інших подіях, всім учасникам, за замовчуванням<br/>(у кожній окремій події можна буде відключити цю опцію вручну)";
$MESS["DISK_ALLOW_INDEX_FILES"] = "Індексувати модулем \"Пошук\" документи";
$MESS["DISK_DEFAULT_VIEWER_SERVICE"] = "Переглядати документи за допомогою";
$MESS["DISK_DEFAULT_VIEWER_SERVICE_NOTICE_SOC_SERVICE"] = "Не налаштований <a href='/bitrix/admin/settings.php?lang=#LANG#&mid=socialservices' target='_blank'>соціальний сервіс #NAME#</a>.";
$MESS["DISK_ENABLE_NGINX_MOD_ZIP_SUPPORT"] = "Підтримка скачування файлів архівом";
$MESS["DISK_ENABLE_NGINX_MOD_ZIP_SUPPORT_NOTICE"] = "Не встановлений модуль <a href='#LINK#' target='_blank'>mod_zip</a>";
$MESS["DISK_ENABLE_RESTRICTION_STORAGE_SIZE_SUPPORT"] = "Дозволити квотування сховищ";
$MESS["DISK_ALLOW_USE_EXTERNAL_LINK"] = "Дозволити використовувати публічні посилання";
$MESS["DISK_VERSION_LIMIT_PER_FILE"] = "Максимальна кількість записів в історії документа";
$MESS["DISK_VERSION_LIMIT_PER_FILE_UNLIMITED"] = "Не обмежено";
$MESS["DISK_ENABLE_OBJECT_LOCK_SUPPORT"] = "Включити можливість блокування документів";
$MESS["DISK_ALLOW_DOCUMENT_TRANSFORMATION"] = "Ввімкнути автоматичну генерацію pdf і jpg файлів для документів";
$MESS["DISK_MAX_SIZE_FOR_DOCUMENT_TRANSFORMATION"] = "Максимальний розмір документа, з якого буде згенеровано pdf та jpg (Мб)";
$MESS["DISK_ALLOW_VIDEO_TRANSFORMATION"] = "Ввімкнути автоматичну генерацію mp4 та jpg для відео файлів";
$MESS["DISK_MAX_SIZE_FOR_VIDEO_TRANSFORMATION"] = "Максимальний розмір відео, з якого буде згенеровано mp4 та jpg (Мб)";
$MESS["DISK_TRANSFORM_FILES_ON_OPEN"] = "Відправляти файли на конвертацію при відкритті";
?>