<?
$MESS["DISK_FW_CONVERT_FAILED"] = "Erro ao converter dados do módulo da Biblioteca de Documentos";
$MESS["DISK_FW_PROCESSED_SUMMARY"] = "Itens convertidos (cumulativo):";
$MESS["DISK_FW_CONVERT_IN_PROGRESS"] = "Ainda convertendo";
$MESS["DISK_FW_CONVERT_COMPLETE"] = "Os dados foram convertidos.";
$MESS["DISK_FW_CONVERT_TITLE"] = "Conversão de Dados da Biblioteca de Documentos ";
$MESS["DISK_FW_CONVERT_TAB"] = "Conversão de Dados";
$MESS["DISK_FW_CONVERT_TAB_TITLE"] = "Conversão de Dados";
$MESS["DISK_FW_CONVERT_START_BUTTON"] = "Converter";
$MESS["DISK_FW_CONVERT_STOP_BUTTON"] = "Parar";
?>