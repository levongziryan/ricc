<?
$MESS["OP_NAME_DISK_READ"] = "Visualizar itens";
$MESS["OP_NAME_DISK_EDIT"] = "Editar itens";
$MESS["OP_NAME_DISK_DELETE"] = "Excluir";
$MESS["OP_NAME_DISK_SETTINGS"] = "Configurações de armazenamento";
$MESS["OP_NAME_DISK_RIGHTS"] = "Gerenciar permissões de acesso";
$MESS["OP_NAME_DISK_SHARING"] = "Compartilhar itens";
$MESS["OP_NAME_DISK_CREATE_WF"] = "Criar modelos de processos comerciais";
$MESS["OP_NAME_DISK_START_BP"] = "Executar processos comerciais";
?>