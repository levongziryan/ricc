<?
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_TITLE"] = "O documento não foi salvo!";
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_WAS_LOCKED_FORKED_COPY"] = "O documento foi bloqueado enquanto você estava editando. Todas as suas alterações foram salvas num novo documento na <a class=\"disco-locked-documento-pop-content-link\" href=\"#LINK#\" target=\"_blank\">Pasta</a> armazenada.";
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_GO_TO_FILE"] = "Ir para arquivo";
?>