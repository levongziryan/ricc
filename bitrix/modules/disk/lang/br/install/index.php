<?
$MESS["DISK_INSTALL_DESCRIPTION"] = "Um módulo para criar, editar e gerenciar arquivos e pastas";
$MESS["DISK_INSTALL_NAME"] = "Drive";
$MESS["DISK_INSTALL_TITLE"] = "Instalação do Módulo";
$MESS["DISK_UNINSTALL_TITLE"] = "Desinstalação do Módulo";
$MESS["DISK_NOTIFY_MIGRATE_WEBDAV"] = "Converter <a href=\"#LINK#\">dados do módulo da Biblioteca de Documentos</a> para migrar para o módulo Drive.";
$MESS["DISK_UNINSTALL_ERROR_MIGRATE_PROCESS"] = "O módulo não pode ser desinstalado porque os dados do módulo da Biblioteca de Documentos ainda estão sendo convertidos.";
?>