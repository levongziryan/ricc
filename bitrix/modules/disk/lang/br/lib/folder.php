<?
$MESS["DISK_FOLDER_MODEL_ERROR_NON_UNIQUE_NAME"] = "O nome do objeto não é exclusivo.";
$MESS["DISK_FOLDER_MODEL_ERROR_COULD_NOT_DELETE_WITH_CODE"] = "Não é possível excluir a pasta com o código #CODE#.";
$MESS["DISK_FOLDER_MODEL_ERROR_COULD_NOT_SAVE_FILE"] = "Não é possível salvar o arquivo.";
$MESS["DISK_FOLDER_MODEL_IM_NEW_FILE"] = "Um novo arquivo #TITLE# foi carregado para o #group_name# grupo.";
$MESS["DISK_FOLDER_SPECIFIC_FOR_CREATED_FILES_NAME"] = "Arquivos criados";
$MESS["DISK_FOLDER_SPECIFIC_FOR_SAVED_FILES_NAME"] = "Arquivos armazenados";
$MESS["DISK_FOLDER_SPECIFIC_FOR_UPLOADED_FILES_NAME"] = "Arquivos carregados";
$MESS["DISK_FOLDER_SPECIFIC_FOR_DROPBOX_FILES_NAME"] = "Arquivos Dropbox";
$MESS["DISK_FOLDER_SPECIFIC_FOR_ONEDRIVE_FILES_NAME"] = "Arquivos OneDrive";
$MESS["DISK_FOLDER_SPECIFIC_FOR_GDRIVE_FILES_NAME"] = "Arquivos Google Drive";
$MESS["DISK_FOLDER_SPECIFIC_FOR_BOX_FILES_NAME"] = "Arquivos Box";
$MESS["DISK_FOLDER_SPECIFIC_FOR_YANDEXDISK_FILES_NAME"] = "Arquivos Yandex.Disk";
$MESS["DISK_FOLDER_MODEL_IM_NEW_FILE2"] = "Novos #LINK_FOLDER_START#arquivos#LINK_END# carregados para o grupo #group_name#.";
?>