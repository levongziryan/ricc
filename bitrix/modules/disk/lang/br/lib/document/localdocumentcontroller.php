<?
$MESS["DISK_LOCAL_DOC_CONTROLLER_NAME"] = "Bitrix24.Drive";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_ADD_VERSION"] = "Não é possível adicionar uma nova versão do arquivo.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_VERSION"] = "A versão não pôde ser encontrada.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_FOLDER"] = "A pasta não pôde ser encontrada.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_FILE"] = "O arquivo não pôde ser encontrado.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_BAD_RIGHTS"] = "Permissão insuficiente para lidar com o arquivo.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_SAVE_FILE"] = "Não é possível salvar o arquivo.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_STORAGE"] = "O armazenamento não pôde ser encontrado.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_FOLDER_FOR_CREATED_FILES"] = "Não é possível encontrar uma pasta para salvar o arquivo.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_CREATE_FILE"] = "Não é possível criar um arquivo em um armazenamento.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_NAME_2_WORK_WITH"] = "aplicativos de computador";
?>