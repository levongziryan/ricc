<?
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_VERSION"] = "A versão não pôde ser encontrada.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_FILE"] = "O arquivo não pôde ser encontrado.";
$MESS["DISK_DOC_CONTROLLER_ERROR_BAD_RIGHTS"] = "Permissão insuficiente para lidar com o arquivo.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_EDIT_SESSION"] = "A sessão de edição não existe.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_DELETE_SESSION"] = "Não foi possível excluir a sessão de edição.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_SAVE_FILE"] = "Não é possível salvar o arquivo.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_ADD_VERSION"] = "Não é possível adicionar a versão.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_GET_FILE"] = "Não é possível recuperar arquivo.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_STORAGE"] = "O armazenamento não pôde ser encontrado.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_FOLDER_FOR_CREATED_FILES"] = "Não é possível encontrar uma pasta para salvar o arquivo.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_CREATE_FILE"] = "Não é possível criar um arquivo em um armazenamento.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_WORK_WITH_TOKEN_SERVICE"] = "O Serviço social #NAME# não está configurado. Entre em contato com o administrador do portal.";
?>