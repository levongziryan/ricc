<?
$MESS["DISK_GOOGLE_HANDLER_NAME"] = "Google Docs";
$MESS["DISK_GOOGLE_HANDLER_ERROR_NOT_INSTALLED_SOCSERV"] = "O módulo de Serviços Sociais não está instalado.";
$MESS["DISK_GOOGLE_HANDLER_ERROR_COULD_NOT_VIEW_FILE"] = "Não é possível visualizar este arquivo usando o Google Drive.";
$MESS["DISK_GOOGLE_HANDLER_NAME_STORAGE"] = "Google Drive";
?>