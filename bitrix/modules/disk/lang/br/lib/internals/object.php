<?
$MESS["DISK_OBJECT_ENTITY_STORAGE_ID_FIELD"] = "Armazenamento";
$MESS["DISK_OBJECT_ENTITY_PARENT_ID_FIELD"] = "Primário";
$MESS["DISK_OBJECT_ENTITY_ERROR_UPDATE_STORAGE_ID"] = "Não é possível atualizar \"#FIELD#\" para um objeto existente.";
$MESS["DISK_OBJECT_ENTITY_ERROR_UPDATE_PARENT_ID"] = "Não é possível atualizar \"#FIELD#\" para um objeto existente. Use a ação \"mover\".";
$MESS["DISK_OBJECT_ENTITY_ERROR_NAME"] = "Valor inválido de #FIELD#.";
$MESS["DISK_OBJECT_ENTITY_ERROR_DELETE_NODE"] = "Não é possível excluir o nó da árvore. Excluir todos os itens secundários primeiro.";
$MESS["DISK_OBJECT_ENTITY_ERROR_REQUIRED_FILE_ID"] = "O #FIELD# do arquivo não está especificado.";
$MESS["DISK_OBJECT_ENTITY_ERROR_LINK_FILE_ID"] = "O #FIELD# está especificado para um arquivo symlink.";
$MESS["DISK_OBJECT_ENTITY_ERROR_FIELD_NAME_HAS_INVALID_CHARS"] = "O nome contém caracteres inválidos.";
$MESS["DISK_OBJECT_ENTITY_NAME_FIELD"] = "Nome";
?>