<?
$MESS["DISK_SHARING_MODEL_AUTOCONNECT_NOTIFY"] = "A pasta compartilhada <b>\"#NAME#\"</b> está agora conectada ao seu Drive. #DISCONNECT_LINK#
#DESCRIPTION#";
$MESS["DISK_SHARING_MODEL_APPROVE_N"] = "Desconectar";
$MESS["DISK_SHARING_MODEL_APPROVE_Y"] = "Conectar a pasta";
$MESS["DISK_SHARING_MODEL_TEXT_DISCONNECT_LINK"] = "Cancelar";
$MESS["DISK_SHARING_MODEL_TEXT_APPROVE_CONFIRM"] = "Você deseja conectar a pasta compartilhada <b>\"#NAME#\"</b> ao seu Drive?";
$MESS["DISK_SHARING_MODEL_TEXT_SELF_DISCONNECT"] = "#USERNAME# disconectou da sua pasta compartilhada <b>\"#NAME#\"</b>";
$MESS["DISK_SHARING_MODEL_ERROR_EMPTY_USER_ID"] = "O usuário não está especificado.";
$MESS["DISK_SHARING_MODEL_ERROR_EMPTY_REAL_OBJECT"] = "Nenhum objeto a compartilhar foi especificado.";
$MESS["DISK_SHARING_MODEL_ERROR_COULD_NOT_FIND_USER_STORAGE"] = "Não é possível encontrar armazenamento para usuário (#USER_ID#).";
$MESS["DISK_SHARING_MODEL_ERROR_COULD_NOT_FIND_STORAGE"] = "O armazenamento não pôde ser encontrado.";
$MESS["DISK_SHARING_MODEL_ERROR_COULD_NOT_CREATE_LINK"] = "Não foi possível criar um symlink para um objeto.";
$MESS["DISK_SHARING_MODEL_AUTOCONNECT_NOTIFY_FILE"] = "O arquivo compartilhado <b>\"#NAME#\"</b> foi anexado ao seu drive. #DISCONNECT_LINK#
#DESCRIPTION#";
$MESS["DISK_SHARING_MODEL_APPROVE_Y_FILE"] = "Anexar arquivo";
$MESS["DISK_SHARING_MODEL_TEXT_APPROVE_CONFIRM_FILE"] = "Você deseja anexar o arquivo compartilhado <b>\"#NAME#\"</b> ao seu drive?";
$MESS["DISK_SHARING_MODEL_TEXT_SELF_DISCONNECT_FILE"] = "O #USERNAME# desconectou do seu arquivo compartilhado <b>\"#NAME#\"</b>";
$MESS["DISK_SHARING_MODEL_APPROVE_N_2_DECLINE"] = "Cancelar";
?>