<?
$MESS["DISK_FILE_MODEL_ERROR_COULD_NOT_SAVE_FILE"] = "Não é possível salvar o arquivo.";
$MESS["DISK_FILE_MODEL_ERROR_COULD_NOT_COPY_FILE"] = "Não é possível copiar o arquivo.";
$MESS["DISK_FILE_MODEL_ERROR_COULD_NOT_RESTORE_FROM_ANOTHER_OBJECT"] = "Não é possível restaurar versão de outro arquivo.";
$MESS["DISK_FILE_MODEL_UPLOAD_NEW_VERSION_IN_COMMENT_M"] = "uma nova versão do arquivo foi carregada";
$MESS["DISK_FILE_MODEL_UPLOAD_NEW_VERSION_IN_COMMENT_F"] = "uma nova versão do arquivo foi carregada";
$MESS["DISK_FILE_MODEL_ERROR_SIZE_RESTRICTION"] = "Não foi possível salvar o arquivo porque o tamanho do arquivo excede a cota.";
$MESS["DISK_FILE_MODEL_ERROR_EXCLUSIVE_LOCK"] = "Não é possível atualizar o arquivo porque ele foi bloqueado por outro usuário.";
$MESS["DISK_FILE_MODEL_ERROR_ALREADY_LOCKED"] = "Não é possível bloquear o arquivo porque ele foi bloqueado por outro usuário.";
$MESS["DISK_FILE_MODEL_ERROR_INVALID_LOCK_TOKEN"] = "Não é possível desbloquear o arquivo: incompatibilidade de token.";
$MESS["DISK_FILE_MODEL_ERROR_INVALID_USER_FOR_UNLOCK"] = "Não é possível desbloquear o arquivo porque ele foi bloqueado por outro usuário.";
$MESS["DISK_FILE_MODEL_UPLOAD_NEW_HEAD_VERSION_IN_COMMENT_M"] = "Editado o arquivo";
$MESS["DISK_FILE_MODEL_UPLOAD_NEW_HEAD_VERSION_IN_COMMENT_F"] = "Editado o arquivo";
?>