<?
$MESS["DISK_BZ_D_FIELD_ID"] = "ID do arquivo";
$MESS["DISK_BZ_D_FIELD_CREATE_TIME"] = "Data e hora em que um arquivo foi criado";
$MESS["DISK_BZ_D_FIELD_CREATED_BY"] = "Criado por";
$MESS["DISK_BZ_D_FIELD_UPDATE_TIME"] = "Data e hora em que um arquivo foi modificado";
$MESS["DISK_BZ_D_FIELD_UPDATED_BY"] = "Modificado por";
$MESS["DISK_BZ_D_FIELD_DELETE_TIME"] = "Data e hora em que um arquivo foi excluído";
$MESS["DISK_BZ_D_FIELD_DELETED_BY"] = "Excluído por";
$MESS["DISK_BZ_D_IDENTIFICATOR"] = "(ID)";
$MESS["DISK_BZ_D_NAME_LASTNAME"] = "(Nome / Sobrenome)";
$MESS["DISK_BZ_D_FIELD_STORAGE_ID"] = "Armazenamento";
$MESS["DISK_BZ_D_FIELD_NAME"] = "Nome do arquivo";
$MESS["DISK_BZ_D_FIELD_SIZE"] = "Tamanho do arquivo";
$MESS["DISK_BZ_D_FIELD_CODE"] = "Código mnemônico";
$MESS["DISK_BZ_D_FIELD_FILE"] = "Arquivo";
$MESS["DISK_BZ_D_AUTHOR"] = "Criado por";
$MESS["DISK_IBD_DOCUMENT_XFORMOPTIONS1"] = "Especificar cada variante em uma nova linha. Se o valor da variante e o nome da variante são diferentes, coloque o nome com o valor entre colchetes. Por exemplo: [v1]Variante 1";
$MESS["DISK_IBD_DOCUMENT_XFORMOPTIONS2"] = "Clicar em \"Configurar\" quando terminar.";
$MESS["DISK_IBD_DOCUMENT_XFORMOPTIONS3"] = "Configurar";
$MESS["DISK_FILED_NOT_SET"] = "não configurado";
$MESS["DISK_DOC_TYPE_STRING"] = "Cadeia";
$MESS["DISK_DOC_TYPE_TEXT"] = "Texto multilinhas";
$MESS["DISK_DOC_TYPE_DOUBLE"] = "Número";
$MESS["DISK_DOC_TYPE_INT"] = "Inteiro";
$MESS["DISK_DOC_TYPE_SELECT"] = "Lista";
$MESS["DISK_DOC_TYPE_BOOL"] = "Sim/Não";
$MESS["DISK_DOC_TYPE_DATA"] = "Data";
$MESS["DISK_DOC_TYPE_DATETIME"] = "Data/Hora";
$MESS["DISK_DOC_TYPE_USER"] = "Associar ao usuário";
$MESS["DISK_DOC_TYPE_FILE"] = "Arquivo";
$MESS["DISK_YES"] = "Sim";
$MESS["DISK_NO"] = "Não";
$MESS["DISK_ADD"] = "Adicionar";
$MESS["DISK_INVALID1"] = "O valor do campo não é inteiro.";
$MESS["DISK_UNKNOW"] = "Erro desconhecido.";
$MESS["DISK_BZ_D_NAME_NOT_CODE"] = "Não foi fornecido código mnemônico.";
?>