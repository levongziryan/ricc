<?
$MESS["DISK_FILE_USER_TYPE_ERROR_COULD_NOT_FIND_ATTACHED_OBJECT"] = "Não é possível encontrar o objeto anexado.";
$MESS["DISK_FILE_USER_TYPE_ERROR_COULD_NOT_FIND_FILE"] = "O arquivo não pôde ser encontrado.";
$MESS["DISK_FILE_USER_TYPE_ERROR_INVALID_VALUE"] = "Valor inválido: deve ser um número.";
$MESS["DISK_FILE_USER_TYPE_ERROR_USER_ID"] = "Usuário não está logado.";
$MESS["DISK_FILE_USER_TYPE_ERROR_BAD_RIGHTS"] = "Permissão insuficiente.";
$MESS["DISK_FILE_USER_TYPE_NAME"] = "Arquivo (Drive)";
?>