<?
$MESS["DISK_UF_CONTROLLER_SAVE_DOCUMENT_TITLE"] = "Selecionar uma pasta para salvar o documento";
$MESS["DISK_UF_CONTROLLER_SELECT_DOCUMENT_TITLE"] = "Selecionar um ou mais documentos";
$MESS["DISK_UF_CONTROLLER_MY_DOCUMENTS"] = "Meu drive";
$MESS["DISK_UF_CONTROLLER_SHARED_DOCUMENTS"] = "Drive compartilhado";
$MESS["DISK_UF_CONTROLLER_MY_GROUPS"] = "Drives do grupo";
$MESS["DISK_UF_CONTROLLER_TITLE_SELECT"] = "Selecionar documento";
$MESS["DISK_UF_CONTROLLER_SELECT_FOLDER"] = "Selecionar pasta atual";
$MESS["DISK_UF_CONTROLLER_SELECT_DOCUMENT"] = "Selecionar documento";
$MESS["DISK_UF_CONTROLLER_CANCEL"] = "Cancelar";
$MESS["DISK_UF_CONTROLLER_TITLE_NAME"] = "Nome";
$MESS["DISK_UF_CONTROLLER_TITLE_FILE_SIZE"] = "Tamanho";
$MESS["DISK_UF_CONTROLLER_TITLE_MODIFIED_BY"] = "Modificado por";
$MESS["DISK_UF_CONTROLLER_TITLE_TIMESTAMP"] = "Modificado em";
$MESS["DISK_UF_CONTROLLER_DESCR_DISABLE_ATTACH_NON_PUBLIC_FILE"] = "O documento não foi publicado. Executar a publicação do documento do processo comercial.";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_FIND_USER_STORAGE"] = "Não é possível encontrar armazenamento para usuário.";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_FIND_FIND_FOLDER"] = "A pasta não pode ser encontrada.";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_WORK_WITH_TOKEN_SERVICE"] = "O Serviço Social #NAME# não está configurado. Entre em contato com o administrador do portal.";
$MESS["DISK_UF_CONTROLLER_RECENTLY_USED"] = "Itens recentes";
$MESS["DISK_UF_CONTROLLER_SELECT_ONE_DOCUMENT_TITLE"] = "Selecionar documento";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_WORK_WITH_TOKEN_SERVICE_B24"] = "O serviço de rede social #NAME# não está configurado. Entre em contato com seu administrador do Bitrix24.";
?>