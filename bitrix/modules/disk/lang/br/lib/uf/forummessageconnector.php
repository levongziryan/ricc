<?
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE01"] = "Fórum de postagem";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE02"] = "Comentário";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE03"] = "Comentário da tarefa";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE04"] = "Comentário do calendário de eventos";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE05"] = "Comentário do documento";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE06"] = "Comentário do CRM";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE07"] = "Comentário do usuário do documento";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE08"] = "Postagem do fórum do grupo";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE09"] = "Postagem do fórum do usuário";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE10"] = "Comentário de notícias";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE11"] = "Comentário de foto";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE12"] = "Comentário Wiki";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE13"] = "Comentário do relatório de trabalho";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE14"] = "Comentário da gravação do tempo de trabalho";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE15"] = "Comentário do item do bloco de informação";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE16"] = "Comentário da reunião";
?>