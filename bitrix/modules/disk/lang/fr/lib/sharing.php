<?
$MESS["DISK_SHARING_MODEL_AUTOCONNECT_NOTIFY"] = "Le fichier partagé <b>'#NAME#'</b> vient d'être joint à votre Drive. #DISCONNECT_LINK#
#DESCRIPTION#";
$MESS["DISK_SHARING_MODEL_APPROVE_N"] = "Déconnecter";
$MESS["DISK_SHARING_MODEL_APPROVE_Y"] = "Connectez dossier";
$MESS["DISK_SHARING_MODEL_TEXT_DISCONNECT_LINK"] = "Annuler";
$MESS["DISK_SHARING_MODEL_TEXT_APPROVE_CONFIRM"] = "Voulez vous joindre le fichier partagé  <b>'#NAME#'</b> à votre disque?";
$MESS["DISK_SHARING_MODEL_TEXT_SELF_DISCONNECT"] = "#USERNAME# s'est deconnecté de votre fichier partagé <b>'#NAME#'</b>";
$MESS["DISK_SHARING_MODEL_ERROR_EMPTY_USER_ID"] = "L'utilisateur n'a pas été désigné.";
$MESS["DISK_SHARING_MODEL_ERROR_EMPTY_REAL_OBJECT"] = "L'objet à partager n'a pas été indiqué.";
$MESS["DISK_SHARING_MODEL_ERROR_COULD_NOT_FIND_USER_STORAGE"] = "Emplacement de stockage introuvable pour l'utilisateur (#USER_ID#).";
$MESS["DISK_SHARING_MODEL_ERROR_COULD_NOT_FIND_STORAGE"] = "Emplacement de stockage introuvable";
$MESS["DISK_SHARING_MODEL_ERROR_COULD_NOT_CREATE_LINK"] = "Echec de la création du lien symbolique vers l'objet.";
$MESS["DISK_SHARING_MODEL_AUTOCONNECT_NOTIFY_FILE"] = "Le fichier partagé <b>'#NAME#'</b> vient d'être joint à votre espace de stokage. #DISCONNECT_LINK#
#DESCRIPTION#";
$MESS["DISK_SHARING_MODEL_APPROVE_Y_FILE"] = "Attacher le fichier";
$MESS["DISK_SHARING_MODEL_TEXT_APPROVE_CONFIRM_FILE"] = "Voulez vous joindre le fichier partagé  <b>'#NAME#'</b> à votre disque?";
$MESS["DISK_SHARING_MODEL_TEXT_SELF_DISCONNECT_FILE"] = "#USERNAME# s'est deconnecté de votre fichier partagé <b>'#NAME#'</b>";
$MESS["DISK_SHARING_MODEL_APPROVE_N_2_DECLINE"] = "Annuler";
?>