<?
$MESS["DISK_OBJECT_ENTITY_STORAGE_ID_FIELD"] = "Emplacement de stockage";
$MESS["DISK_OBJECT_ENTITY_PARENT_ID_FIELD"] = "Parent";
$MESS["DISK_OBJECT_ENTITY_ERROR_UPDATE_STORAGE_ID"] = "Impossible de mettre à jour '#FIELD#' pour un objet déjà existant.";
$MESS["DISK_OBJECT_ENTITY_ERROR_UPDATE_PARENT_ID"] = "Impossible de mettre à jour '#FIELD#' pour un objet déjà existant. Veuillez utiliser l'action 'déplacer'.";
$MESS["DISK_OBJECT_ENTITY_ERROR_NAME"] = "Valeur de #FIELD# incorrecte.";
$MESS["DISK_OBJECT_ENTITY_ERROR_DELETE_NODE"] = "Impossible de supprimer le noeud de l'arborescence. Veuillez effacer d'abord les objets enfants.";
$MESS["DISK_OBJECT_ENTITY_ERROR_REQUIRED_FILE_ID"] = "Le #FIELD# du fichier n'est pas spécifié.";
$MESS["DISK_OBJECT_ENTITY_ERROR_LINK_FILE_ID"] = "#FIELD#  est spécifié pour un fichier-lien";
$MESS["DISK_OBJECT_ENTITY_ERROR_FIELD_NAME_HAS_INVALID_CHARS"] = "Le nom contient des caractères invalides.";
$MESS["DISK_OBJECT_ENTITY_NAME_FIELD"] = "Nom";
?>