<?
$MESS["DISK_LOCAL_DOC_CONTROLLER_NAME"] = "Bitrix.Drive";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_ADD_VERSION"] = "Impossible d'ajouter une nouvelle version du fichier.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_VERSION"] = "Version introuvable";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_FOLDER"] = "Dossier introuvable.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_FILE"] = "Fichier introuvable.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_BAD_RIGHTS"] = "Permission insuffisante pour éditer le fichier.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_SAVE_FILE"] = "Impossible de sauvegarder le fichier.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_STORAGE"] = "Emplacement de stockage introuvable";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_FOLDER_FOR_CREATED_FILES"] = "Impossible de trouver un dossier la sauvegarde du fichier.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_CREATE_FILE"] = "Impossible de créer un fichier dans l'emplacement de stockage.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_NAME_2_WORK_WITH"] = "programme sur l'ordinateur";
?>