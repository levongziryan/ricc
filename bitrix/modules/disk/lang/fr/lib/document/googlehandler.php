<?
$MESS["DISK_GOOGLE_HANDLER_NAME"] = "Google Docs";
$MESS["DISK_GOOGLE_HANDLER_ERROR_COULD_NOT_VIEW_FILE"] = "Impossible de visualiser le fichier avec Google.Drive";
$MESS["DISK_GOOGLE_HANDLER_ERROR_NOT_INSTALLED_SOCSERV"] = "Le module de services sociaux n'a pas été installé.";
$MESS["DISK_GOOGLE_HANDLER_NAME_STORAGE"] = "Google Drive";
?>