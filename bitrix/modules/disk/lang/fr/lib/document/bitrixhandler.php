<?
$MESS["DISK_BITRIX_HANDLER_NAME"] = "Bitrix24.Viewer";
$MESS["DISK_BITRIX_HANDLER_ERROR_METHOD_IS_NOT_SUPPORTED"] = "#NAME# ne prend pas en charge cette action.";
$MESS["DISK_BITRIX_HANDLER_ERROR_NO_VIEW"] = "Le document ne peut être affiché en #NAME#.";
$MESS["DISK_BITRIX_HANDLER_ERROR_NO_VIEW_SEND_TO_TRANSFORM"] = "Nous sommes en train de préparer le visualiseur de document. Veuillez réessayer plus tard.";
?>