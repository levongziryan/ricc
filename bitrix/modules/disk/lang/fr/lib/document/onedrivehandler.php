<?
$MESS["DISK_ONE_DRIVE_HANDLER_NAME"] = "MS Office Online";
$MESS["DISK_ONE_DRIVE_HANDLER_ERROR_COULD_NOT_VIEW_FILE"] = "Impossible de visualiser le fichier avec MS Office Online";
$MESS["DISK_ONE_DRIVE_HANDLER_ERROR_COULD_NOT_FIND_EMBED_LINK"] = "Impossible de trouver le lien à utiliser.";
$MESS["DISK_ONE_DRIVE_HANDLER_NAME_STORAGE"] = "OneDrive";
?>