<?
$MESS["DISK_DROPBOX_HANDLER_ERROR_NOT_INSTALLED_SOCSERV"] = "Le module de services sociaux n'a pas été installé.";
$MESS["DISK_DROPBOX_HANDLER_NAME"] = "Dropbox";
$MESS["DISK_DROPBOX_HANDLER_ERROR_METHOD_IS_NOT_SUPPORTED"] = "Dropbox ne supporte pas cette action.";
?>