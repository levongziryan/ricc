<?
$MESS["DISK_GOOGLE_VIEWER_HANDLER_NAME"] = "Google Viewer";
$MESS["DISK_GOOGLE_VIEWER_HANDLER_ERROR_METHOD_IS_NOT_SUPPORTED"] = "Google Viewer ne peut pas prendre en charge cette action.";
$MESS["DISK_GOOGLE_VIEWER_HANDLER_ERROR_COULD_NOT_FIND_EXT_LINK"] = "Impossible de trouver le lien externe vers ce document.";
?>