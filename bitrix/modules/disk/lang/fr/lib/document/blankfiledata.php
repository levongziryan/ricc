<?
$MESS["DISK_BLANK_FILE_DATA_TYPE_DOCX"] = "Document";
$MESS["DISK_BLANK_FILE_DATA_TYPE_XLSX"] = "Table";
$MESS["DISK_BLANK_FILE_DATA_TYPE_PPTX"] = "Présentation";
$MESS["DISK_BLANK_FILE_DATA_NEW_FILE_DOCX"] = "Création d'un nouveau document";
$MESS["DISK_BLANK_FILE_DATA_NEW_FILE_XLSX"] = "Nouveau tableau";
$MESS["DISK_BLANK_FILE_DATA_NEW_FILE_PPTX"] = "Nouvelle présentation";
?>