<?
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_WORK_WITH_TOKEN_SERVICE"] = "Le service social #NAME# n'a pas été configuré. Veuillez vous adresser à l'administrateur du portail.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_ADD_VERSION"] = "Impossible d'ajouter la version.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_VERSION"] = "Version introuvable";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_FOLDER_FOR_CREATED_FILES"] = "Impossible de trouver un dossier la sauvegarde du fichier.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_EDIT_SESSION"] = "Session d'édition introuvable.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_FILE"] = "Fichier introuvable.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_STORAGE"] = "Emplacement de stockage introuvable";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_GET_FILE"] = "Impossible de recevoir le fichier.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_CREATE_FILE"] = "Impossible de créer un fichier dans l'emplacement de stockage.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_SAVE_FILE"] = "Impossible de sauvegarder le fichier.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_DELETE_SESSION"] = "Impossible de supprimer la session d'édition.";
$MESS["DISK_DOC_CONTROLLER_ERROR_BAD_RIGHTS"] = "Permission insuffisante pour éditer le fichier.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_WORK_WITH_TOKEN_SERVICE_B24"] = "Le service de réseau social #NAME# n'est pas configuré. Veuillez contacter votre administrateur Bitrix24.";
?>