<?
$MESS["DISK_FILE_MODEL_UPLOAD_NEW_VERSION_IN_COMMENT_M"] = "a téléchargé une nouvelle version du fichier";
$MESS["DISK_FILE_MODEL_UPLOAD_NEW_VERSION_IN_COMMENT_F"] = "a téléchargé une nouvelle version du fichier";
$MESS["DISK_FILE_MODEL_ERROR_COULD_NOT_COPY_FILE"] = "Impossible de copier le fichier.";
$MESS["DISK_FILE_MODEL_ERROR_COULD_NOT_SAVE_FILE"] = "Impossible de sauvegarder le fichier.";
$MESS["DISK_FILE_MODEL_ERROR_COULD_NOT_RESTORE_FROM_ANOTHER_OBJECT"] = "Impossible de restaurer la version à partir d'un autre fichier.";
$MESS["DISK_FILE_MODEL_ERROR_SIZE_RESTRICTION"] = "Impossible d'enregistrer le fichier parce que sa taille dépasse le quota.";
$MESS["DISK_FILE_MODEL_ERROR_EXCLUSIVE_LOCK"] = "Impossible de mettre à jour le fichier parce qu'il a été verrouillé par un autre utilisateur.";
$MESS["DISK_FILE_MODEL_ERROR_ALREADY_LOCKED"] = "Impossible de verrouiller le fichier parce qu'il a été verrouillé par un autre utilisateur.";
$MESS["DISK_FILE_MODEL_ERROR_INVALID_LOCK_TOKEN"] = "Impossible de déverrouiller le fichier: le jeton ne correspond pas.";
$MESS["DISK_FILE_MODEL_ERROR_INVALID_USER_FOR_UNLOCK"] = "Impossible de déverrouiller le fichier parce qu'il a été verrouillé par un autre utilisateur.";
$MESS["DISK_FILE_MODEL_UPLOAD_NEW_HEAD_VERSION_IN_COMMENT_M"] = "Modifié dans le fichier";
$MESS["DISK_FILE_MODEL_UPLOAD_NEW_HEAD_VERSION_IN_COMMENT_F"] = "Modifié dans le fichier";
?>