<?
$MESS["DISK_VERSION_USER_TYPE_NAME"] = "Version du fichier (Drive)";
$MESS["DISK_VERSION_USER_TYPE_ERROR_COULD_NOT_FIND_ATTACHED_OBJECT"] = "Imposible de trouver la pièces jointe";
$MESS["DISK_VERSION_USER_TYPE_ERROR_COULD_NOT_FIND_FILE"] = "Fichier introuvable.";
$MESS["DISK_VERSION_USER_TYPE_ERROR_INVALID_VALUE"] = "Donnée invalide: doit être une valeur numérique.";
$MESS["DISK_VERSION_USER_TYPE_ERROR_BAD_RIGHTS"] = "Permission insuffisante.";
$MESS["DISK_VERSION_USER_TYPE_ERROR_USER_ID"] = "L'utilisateur n'est pas connecté.";
?>