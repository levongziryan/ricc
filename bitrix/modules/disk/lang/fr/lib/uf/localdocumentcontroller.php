<?
$MESS["DISK_UF_LOCAL_DOC_CONTROLLER_UPLOAD_NEW_VERSION_IN_COMMENT_M"] = "a téléchargé un nouveau fichier";
$MESS["DISK_UF_LOCAL_DOC_CONTROLLER_UPLOAD_NEW_VERSION_IN_COMMENT_F"] = "a téléchargé un nouveau fichier";
$MESS["DISK_UF_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_ATTACHED_OBJECT"] = "Imposible de trouver la pièces jointe";
$MESS["DISK_UF_LOCAL_DOC_CONTROLLER_ERROR_BAD_RIGHTS"] = "Permission insuffisante pour éditer le fichier.";
?>