<?
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE14"] = "Commentaire sur l'enregistrement du temp de travail";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE06"] = "Commentaire sur la CRM";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE12"] = "Commentaire sur le wiki";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE05"] = "Commentaire sur le document";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE07"] = "Commentaire sur un document de l'utilisateur";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE03"] = "Commentaire sur la tâche";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE10"] = "Commentaire sur l'actualité";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE13"] = "Commentaire sur le rapport d'activité";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE16"] = "Commentaire sur la réunion";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE04"] = "Commentaire sur un événement du calendrier";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE11"] = "Commentaire sur la photo";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE15"] = "Commentaire sur un élément du bloc d'information";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE02"] = "Commentaire";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE08"] = "Publication d'un groupe dans le forum";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE09"] = "Publication d'un utilisateur dans le forum";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE01"] = "Publication du forum";
?>