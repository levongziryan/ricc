<?
$MESS["DISK_FOLDER_MODEL_ERROR_NON_UNIQUE_NAME"] = "Le nom de l'objet n'est pas unique.";
$MESS["DISK_FOLDER_MODEL_ERROR_COULD_NOT_DELETE_WITH_CODE"] = "Impossible de supprimer le dossier avec le code #CODE#.";
$MESS["DISK_FOLDER_MODEL_ERROR_COULD_NOT_SAVE_FILE"] = "Impossible de sauvegarder le fichier.";
$MESS["DISK_FOLDER_MODEL_IM_NEW_FILE"] = "Un nouveau fichier #TITLE# a été chargé dans le groupe #group_name#.";
$MESS["DISK_FOLDER_SPECIFIC_FOR_CREATED_FILES_NAME"] = "Fichiers créés";
$MESS["DISK_FOLDER_SPECIFIC_FOR_SAVED_FILES_NAME"] = "Fichiers conservés";
$MESS["DISK_FOLDER_SPECIFIC_FOR_UPLOADED_FILES_NAME"] = "Fichiers téléchargés";
$MESS["DISK_FOLDER_SPECIFIC_FOR_DROPBOX_FILES_NAME"] = "Fichiers Dropbox";
$MESS["DISK_FOLDER_SPECIFIC_FOR_ONEDRIVE_FILES_NAME"] = "Fichiers OneDrive";
$MESS["DISK_FOLDER_SPECIFIC_FOR_GDRIVE_FILES_NAME"] = "Fichiers Google Drive";
$MESS["DISK_FOLDER_SPECIFIC_FOR_BOX_FILES_NAME"] = "Fichiers Box";
$MESS["DISK_FOLDER_SPECIFIC_FOR_YANDEXDISK_FILES_NAME"] = "Fichiers Yandex.Disk";
$MESS["DISK_FOLDER_MODEL_IM_NEW_FILE2"] = "Nouveaux #LINK_FOLDER_START#fichiers#LINK_END# téléchargés dans le groupe #group_name#.";
?>