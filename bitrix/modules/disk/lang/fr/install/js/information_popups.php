<?
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_TITLE"] = "Le document n'a pas été enregistré !";
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_WAS_LOCKED_FORKED_COPY"] = "Le document a été verrouillé pendant que vous l'éditiez. Toutes vos modifications ont été enregistrées dans un nouveau document dans le dossier <a class=\"disk-locked-document-popup-content-link\" href=\"#LINK#\" target=\"_blank\">Conservés</a>.";
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_GO_TO_FILE"] = "Aller au fichier";
?>