<?
$MESS["DISK_JS_FILE_DIALOG_OAUTH_NOTICE"] = "Pour charger un fichier à partir d'un disque externe, <br/> s'il vous plaît vous connecter à votre <a id=\"bx-js-disk-run-oauth-modal\" href='#'>#SERVICE#</a> compte.";
$MESS["DISK_JS_FILE_DIALOG_OAUTH_NOTICE_DETAIL"] = "<p>Le fichier sera enregistré dans le dossier Téléchargée Bitrix24.Drive. Le fichier externe ne sera pas regardé pour d'autres mises à jour.</p>
<p>Une fois votre compte externe est ajouté à Bitrix24, il sera accessible par toute fonction qui peut l'utiliser.</p>";
$MESS["DISK_JS_FILE_DIALOG_OAUTH_NOTICE_DETAIL_B24"] = "<p>Le fichier sera enregistré dans le dossier Téléchargée Bitrix24.Drive. Le fichier externe ne sera pas regardé pour d'autres mises à jour.</p>
<p>Une fois votre compte externe est ajouté à Bitrix24, il sera accessible par toute fonction qui peut l'utiliser.</p><p>Utilisez votre page Bitrix.Passport pour déconnecter les services extérieurs.</p>";
?>