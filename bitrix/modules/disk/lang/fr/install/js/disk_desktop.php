<?
$MESS["DISK_DESKTOP_JS_SETTINGS"] = "Paramètres";
$MESS["DISK_DESKTOP_JS_SETTINGS_LABEL_ENABLE"] = "Activer Bitrix24.Drive";
$MESS["DISK_DESKTOP_JS_SETTINGS_TITLE"] = "Bitrix24.Drive";
$MESS["DISK_DESKTOP_JS_SETTINGS_LABEL_FILE_CLICK_ACTION"] = "Cliquer un fichier dans \"Téléchargements récents\"";
$MESS["DISK_DESKTOP_JS_SETTINGS_LABEL_FILE_CLICK_ACTION_OPEN_FOLDER"] = "Ouvre le dossier le contenant";
$MESS["DISK_DESKTOP_JS_SETTINGS_LABEL_FILE_CLICK_ACTION_OPEN_FILE"] = "Ouvre le fichier";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_FOLDER_NOTICE"] = "Vous pouvez voir ces dossiers parce qu'ils vous ont été partagés, bien que vous n'en soyez pas le propriétaire. Cette liste contient également les fichiers des groupes auxquels vous appartenez.";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_EMPTY_OWN_FOLDER_NOTICE"] = "Votre lecteur ne contient pas de dossier Vous ne possédez que les fichiers qui sont automatiquement synchronisés avec votre PC. Pour synchroniser seulement certains fichiers, créez-leur des dossiers.";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_EMPTY_SHARED_FOLDER_NOTICE"] = "Vous n'avez pas de dossier partagé sur votre disque. Aucun de vos collègues n'a pour l'instant partagé de dossier avec vous.

Quand des dossiers partagés seront présents sur votre disque, vous pourrez en sélectionner un ou plus pour les synchroniser avec votre PC.";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_TAB_OWNER"] = "Dossiers privés";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_TAB_SHARED"] = "Dossiers partagés";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_MAKE_CHOICE"] = "Sélectionnez les dossiers à synchroniser avec cette ordinateur";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_ALL"] = "Tous";
?>