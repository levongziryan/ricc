<?
$MESS["DISK_INSTALL_DESCRIPTION"] = "Un module destiné au stockage, à la création, à lédition de fichiers et de dossiers";
$MESS["DISK_INSTALL_NAME"] = "Drive";
$MESS["DISK_INSTALL_TITLE"] = "Installation de la solution";
$MESS["DISK_UNINSTALL_TITLE"] = "Désinstallation du module";
$MESS["DISK_NOTIFY_MIGRATE_WEBDAV"] = "Veuillez <a href=\"#LINK#\">convertir le Document de la Bibliothèque du module de données</a> pour migrer vers le module de Drive.";
$MESS["DISK_UNINSTALL_ERROR_MIGRATE_PROCESS"] = "Le module ne peut pas être désinstallé car la Bibliothèque de documents module de données est encore en cours d'aménagement.";
?>