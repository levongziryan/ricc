<?
$MESS["DISK_FW_CONVERT_IN_PROGRESS"] = "La totalité des données n'a pas été convertie";
$MESS["DISK_FW_CONVERT_TAB"] = "Conversion des données";
$MESS["DISK_FW_CONVERT_TAB_TITLE"] = "Conversion des données";
$MESS["DISK_FW_CONVERT_COMPLETE"] = "La conversion de données est terminée.";
$MESS["DISK_FW_CONVERT_TITLE"] = "Conversion des données du module de la 'bibliothèque des documents'";
$MESS["DISK_FW_CONVERT_START_BUTTON"] = "Commencer la conversion";
$MESS["DISK_FW_CONVERT_STOP_BUTTON"] = "Stop";
$MESS["DISK_FW_CONVERT_FAILED"] = "Une erreur est survenue lors de la conversion des données de la bibliothèque des documents";
$MESS["DISK_FW_PROCESSED_SUMMARY"] = "Eléments convertis (cumul):";
$MESS["DISK_FW_CONVERT_POPUP_NOTICE"] = "Dans la version actuelle du module Bibliothèque de documents, les documents non publiés sont disponibles pour l'auteur et les utilisateurs disposant de la permission \"Éditer un élément dans n'importe quel statut du flux de travail\". Dans la nouvelle version du module Lecteur, les documents non publiés ne seront disponibles que pour l'auteur. ";
$MESS["DISK_FW_CONVERT_HELP_1"] = "Après conversion, certaines fonctions ne sont plus disponibles.";
$MESS["DISK_FW_CONVERT_HELP_2"] = "La nouvelle version ne prend plus en charge :";
$MESS["DISK_FW_CONVERT_HELP_3"] = "L'ancien module Flux de travail (les processus d'entreprise SONT présent)";
$MESS["DISK_FW_CONVERT_HELP_4"] = "Descriptions des fichiers";
$MESS["DISK_FW_CONVERT_HELP_75"] = "Mots-clés";
$MESS["DISK_FW_CONVERT_HELP_5"] = "Commentaires du fichier ";
$MESS["DISK_FW_CONVERT_HELP_6"] = "\"J'aime\" sur les documents";
$MESS["DISK_FW_CONVERT_HELP_7"] = "Versions des fichiers actuellement affectés par un Processus d'entreprise actif";
$MESS["DISK_FW_CONVERT_HELP_8"] = "Vous pouvez utiliser les modèles de Processus d'entreprise actuels, mais certaines fonctions du nouveau module ne seront pas disponibles. Nous recommandons de recréer les modèles standards et de supprimer les anciens (ils seront marqués comme Anciens dans la liste). Nous recommandons également de vérifier la bonne marche des modèles de Processus d'entreprise personnalisés (ceux que vous avez créé). ";
$MESS["DISK_FW_CONVERT_HELP_9"] = "Avant une conversion, choisissez comment le processus de conversion sera géré par les documents non publiés.";
$MESS["DISK_FW_CONVERT_HELP_10"] = "Publier";
$MESS["DISK_FW_CONVERT_HELP_11"] = "Laisser les documents non publiés";
$MESS["DISK_FW_CONVERT_HELP_12"] = "(les utilisateurs qui y avaient précédemment accès ne pourront plus voir ces documents)";
$MESS["DISK_FW_CONVERT_HELP_13"] = "NE SUPPRIMEZ PAS le module Bibliothèque de documents (tous les fichiers de ce module sera déplacés dans le nouveau module Lecteur. Ils ne prendront pas d'espace disque supplémentaire).";
?>