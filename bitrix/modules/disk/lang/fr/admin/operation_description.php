<?
$MESS["OP_NAME_DISK_SHARING"] = "Partager les éléments";
$MESS["OP_NAME_DISK_START_BP"] = "Lancer la procédure d'entreprise";
$MESS["OP_NAME_DISK_SETTINGS"] = "Configuration du stockage";
$MESS["OP_NAME_DISK_READ"] = "Voir les éléments";
$MESS["OP_NAME_DISK_EDIT"] = "Editer les éléments";
$MESS["OP_NAME_DISK_CREATE_WF"] = "Créer un modèle de procédure d'entreprise";
$MESS["OP_NAME_DISK_DELETE"] = "Supprimer";
$MESS["OP_NAME_DISK_RIGHTS"] = "Gérer les autorisations d'accès pour les éléments de flux de travail et des sections";
$MESS["OP_NAME_DISK_ADD"] = "Ajouter des éléments";
$MESS["OP_NAME_DISK_DELETE_2"] = "Envoyer les éléments dans la Corbeille";
$MESS["OP_NAME_DISK_DISK_DESTROY"] = "Supprimer les éléments de la Corbeille";
$MESS["OP_NAME_DISK_DISK_RESTORE"] = "Récupérer les éléments de la Corbeille";
?>