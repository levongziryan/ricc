<?
$MESS["DISK_VERSION_USER_TYPE_ERROR_COULD_NOT_FIND_ATTACHED_OBJECT"] = "Das angehängte Element wurde nicht gefunden.";
$MESS["DISK_VERSION_USER_TYPE_ERROR_COULD_NOT_FIND_FILE"] = "Die Datei wurde nicht gefunden.";
$MESS["DISK_VERSION_USER_TYPE_ERROR_INVALID_VALUE"] = "Ungültiger Wert. Es muss eine Zahl sein.";
$MESS["DISK_VERSION_USER_TYPE_ERROR_USER_ID"] = "Nutzer ist nicht eingeloggt.";
$MESS["DISK_VERSION_USER_TYPE_ERROR_BAD_RIGHTS"] = "Ungenügende Zugriffsrechte";
$MESS["DISK_VERSION_USER_TYPE_NAME"] = "Dateiversion (Drive)";
?>