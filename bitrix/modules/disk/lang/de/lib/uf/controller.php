<?
$MESS["DISK_UF_CONTROLLER_SAVE_DOCUMENT_TITLE"] = "Wählen Sie einen Ordner aus, wo das Dokument gespeichert werden soll";
$MESS["DISK_UF_CONTROLLER_SELECT_DOCUMENT_TITLE"] = "Wählen Sie ein oder mehrere Dokumente aus";
$MESS["DISK_UF_CONTROLLER_MY_DOCUMENTS"] = "Mein Drive";
$MESS["DISK_UF_CONTROLLER_SHARED_DOCUMENTS"] = "Allgemeiner Drive";
$MESS["DISK_UF_CONTROLLER_MY_GROUPS"] = "Dateien der Gruppen";
$MESS["DISK_UF_CONTROLLER_TITLE_SELECT"] = "Wählen Sie ein Dokument aus";
$MESS["DISK_UF_CONTROLLER_SELECT_FOLDER"] = "Aktuellen Ordner auswählen";
$MESS["DISK_UF_CONTROLLER_SELECT_DOCUMENT"] = "Dokument auswählen";
$MESS["DISK_UF_CONTROLLER_CANCEL"] = "Abbrechen";
$MESS["DISK_UF_CONTROLLER_TITLE_NAME"] = "Überschrift";
$MESS["DISK_UF_CONTROLLER_TITLE_FILE_SIZE"] = "Größe";
$MESS["DISK_UF_CONTROLLER_TITLE_MODIFIED_BY"] = "Geändert von";
$MESS["DISK_UF_CONTROLLER_TITLE_TIMESTAMP"] = "Geändert";
$MESS["DISK_UF_CONTROLLER_DESCR_DISABLE_ATTACH_NON_PUBLIC_FILE"] = "Das Dokument wurde nicht veröffentlicht. Initiieren Sie einen Geschäftsprozess für die Dokumentveröffentlichung.";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_FIND_USER_STORAGE"] = "Der Speicher für den Nutzer konnte nicht gefunden werden.";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_FIND_FIND_FOLDER"] = "Ordner konnte nicht gefunden werden.";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_WORK_WITH_TOKEN_SERVICE"] = "Sozialer Service #NAME# ist nicht konfiguriert. Wenden Sie sich bitte an den Administrator Ihres Portals.";
$MESS["DISK_UF_CONTROLLER_RECENTLY_USED"] = "Neulich";
$MESS["DISK_UF_CONTROLLER_SELECT_ONE_DOCUMENT_TITLE"] = "Wählen Sie ein Dokument aus";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_WORK_WITH_TOKEN_SERVICE_B24"] = "Der Service der Sozialen Netzwerke #NAME# ist nicht konfiguriert. Wenden Sie sich bitte an Ihren Bitrix24-Administrator.";
$MESS["DISK_UF_CONTROLLER_FILE_IS_TOO_BIG_FOR_TRANSFORMATION"] = "Die Videodatei ist zu groß, um in Bitrix24 angesehen zu werden. <a class=\"transformer-upgrade-popup\" href=\"javascript:void(0);\">Details</a>";
$MESS["DISK_UF_CONTROLLER_TRANSFORMATION_UPGRADE_POPUP_TITLE"] = "Verfügbar nur im erweiterten Bitrix24.Drive";
$MESS["DISK_UF_CONTROLLER_TRANSFORMATION_UPGRADE_POPUP_CONTENT"] = "<b>Ihre Videodatei ist leider zu groß, um die in Bitrix24 anzusehen.</b><br />
Die Datei bleibt hier gespeichert, aber die Nutzer werden die Datei erste herunterladen müssen, bevor sie sich diese anschauen können.<br /><br />
Dateien bis zu 300 MB können im Tarif Free wiedergegeben werden.<br />
In kostenpflichtigen Tarifen können Dateien bis zu 3 GB wiedergegeben werden.<br /><br />
Sie sollten upgraden, wenn Die noch weitere praktische Funktionen nutzen möchten:
<ul class=\"hide-features-list\">
<li class=\"hide-features-list-item\">Unbegrenzte Anzahl von Sicherungskopien</li>
<li class=\"hide-features-list-item\">Sperrfunktion für Dokumente</li>
<li class=\"hide-features-list-item\">Öffentliche Links zu Ordnern</li>
<li class=\"hide-features-list-item\">Workflows im Unternehmensdrive</li>
</ul>
Schauen Sie sich die Vergleichstabelle der Tarife und die vollständige Beschreibung der Funktionen <a target=\"_blank\" href=\"https://www.bitrix24.de/prices/index.php\">hier</a> an.
";
?>