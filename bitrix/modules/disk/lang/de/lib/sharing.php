<?
$MESS["DISK_SHARING_MODEL_AUTOCONNECT_NOTIFY"] = "Freigegebener Ordner <b>\"#NAME#\"</b> wurde an Ihren Drive angeschlossen. #DISCONNECT_LINK#
#DESCRIPTION#
";
$MESS["DISK_SHARING_MODEL_APPROVE_N"] = "Deaktivieren";
$MESS["DISK_SHARING_MODEL_APPROVE_Y"] = "Freigegebenen Ordner aktivieren";
$MESS["DISK_SHARING_MODEL_TEXT_DISCONNECT_LINK"] = "Verzichten";
$MESS["DISK_SHARING_MODEL_TEXT_APPROVE_CONFIRM"] = "Soll der freigegebene Ordner <b>\"#NAME#\"</b> an Ihren Drive angeschlossen werden?";
$MESS["DISK_SHARING_MODEL_TEXT_SELF_DISCONNECT"] = "#USERNAME# hat auf den Zugriff auf Ihren freigegebenen Ordner <b>\"#NAME#\"</b> verzichtet.";
$MESS["DISK_SHARING_MODEL_ERROR_EMPTY_USER_ID"] = "Nutzer ist nicht angegeben.";
$MESS["DISK_SHARING_MODEL_ERROR_EMPTY_REAL_OBJECT"] = "Das Element zur Freigabe ist nicht angegeben.";
$MESS["DISK_SHARING_MODEL_ERROR_COULD_NOT_FIND_USER_STORAGE"] = "Der Speicher für Nutzer (#USER_ID#) wurde nicht gefunden.";
$MESS["DISK_SHARING_MODEL_ERROR_COULD_NOT_FIND_STORAGE"] = "Der Speicher wurde nicht gefunden.";
$MESS["DISK_SHARING_MODEL_ERROR_COULD_NOT_CREATE_LINK"] = "Der symbolische Link zum Element konnte nicht erstellt werden.";
$MESS["DISK_SHARING_MODEL_AUTOCONNECT_NOTIFY_FILE"] = "Freigegebene Datei <b>\"#NAME#\"</b>wurde an Ihren Drive angefügt. #DISCONNECT_LINK#
#DESCRIPTION#
";
$MESS["DISK_SHARING_MODEL_APPROVE_Y_FILE"] = "Datei anfügen";
$MESS["DISK_SHARING_MODEL_TEXT_APPROVE_CONFIRM_FILE"] = "Möchten Sie die freigegebene Datei <b>\"#NAME#\"</b> an Ihren Drive anfügen?";
$MESS["DISK_SHARING_MODEL_TEXT_SELF_DISCONNECT_FILE"] = "#USERNAME# hat die Verbindung zu Ihrer freigegebenen Datei <b>\"#NAME#\"</b> getrennt";
$MESS["DISK_SHARING_MODEL_APPROVE_N_2_DECLINE"] = "Abbrechen";
?>