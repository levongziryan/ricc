<?
$MESS["DISK_FILE_MODEL_ERROR_COULD_NOT_SAVE_FILE"] = "Die Datei konnte nicht gespeichert werden.";
$MESS["DISK_FILE_MODEL_ERROR_COULD_NOT_COPY_FILE"] = "Die Datei konnte nicht kopiert werden.";
$MESS["DISK_FILE_MODEL_ERROR_COULD_NOT_RESTORE_FROM_ANOTHER_OBJECT"] = "Die Version aus einer anderen Datei konnte nicht wiederhergestellt werden.";
$MESS["DISK_FILE_MODEL_UPLOAD_NEW_VERSION_IN_COMMENT_M"] = "hat eine neue Dateiversion hochgeladen";
$MESS["DISK_FILE_MODEL_UPLOAD_NEW_VERSION_IN_COMMENT_F"] = "hat eine neue Dateiversion hochgeladen";
$MESS["DISK_FILE_MODEL_ERROR_SIZE_RESTRICTION"] = "Die Datei konnte nicht gespeichert werden, weil die Dateigröße die Quote überschreitet.";
$MESS["DISK_FILE_MODEL_ERROR_EXCLUSIVE_LOCK"] = "Die Datei kann nicht aktualisiert werden, weil sie durch einen anderen Nutzer gesperrt wurde.";
$MESS["DISK_FILE_MODEL_ERROR_ALREADY_LOCKED"] = "Die Datei kann nicht gesperrt werden, weil sie bereits durch einen anderen Nutzer gesperrt wurde.";
$MESS["DISK_FILE_MODEL_ERROR_INVALID_LOCK_TOKEN"] = "Die Datei kann nicht entsperrt werden: Token stimmt nicht überein.";
$MESS["DISK_FILE_MODEL_ERROR_INVALID_USER_FOR_UNLOCK"] = "Die Datei kann nicht entsperrt werden, weil sie durch einen anderen Nutzer gesperrt wurde.";
$MESS["DISK_FILE_MODEL_UPLOAD_NEW_HEAD_VERSION_IN_COMMENT_M"] = "Hat die Datei bearbeitet";
$MESS["DISK_FILE_MODEL_UPLOAD_NEW_HEAD_VERSION_IN_COMMENT_F"] = "Hat die Datei bearbeitet";
?>