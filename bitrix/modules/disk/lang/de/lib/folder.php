<?
$MESS["DISK_FOLDER_MODEL_ERROR_NON_UNIQUE_NAME"] = "Name des Elements ist nicht einmalig.";
$MESS["DISK_FOLDER_MODEL_ERROR_COULD_NOT_DELETE_WITH_CODE"] = "Der Ordner mit dem Code #CODE# konnte nicht gelöscht werden.";
$MESS["DISK_FOLDER_MODEL_ERROR_COULD_NOT_SAVE_FILE"] = "Die Datei konnte nicht gespeichert werden.";
$MESS["DISK_FOLDER_MODEL_IM_NEW_FILE"] = "Eine neue Datei #TITLE# wurde in die Gruppe #group_name# geladen.";
$MESS["DISK_FOLDER_SPECIFIC_FOR_CREATED_FILES_NAME"] = "Erstellte Dateien";
$MESS["DISK_FOLDER_SPECIFIC_FOR_SAVED_FILES_NAME"] = "Gespeicherte Dateien";
$MESS["DISK_FOLDER_SPECIFIC_FOR_UPLOADED_FILES_NAME"] = "Hochgeladene Dateien";
$MESS["DISK_FOLDER_SPECIFIC_FOR_DROPBOX_FILES_NAME"] = "Dropbox Dateien";
$MESS["DISK_FOLDER_SPECIFIC_FOR_ONEDRIVE_FILES_NAME"] = "OneDrive Dateien";
$MESS["DISK_FOLDER_SPECIFIC_FOR_GDRIVE_FILES_NAME"] = "Google Drive Dateien";
$MESS["DISK_FOLDER_SPECIFIC_FOR_BOX_FILES_NAME"] = "Box-Dateien";
$MESS["DISK_FOLDER_SPECIFIC_FOR_YANDEXDISK_FILES_NAME"] = "Yandex.Disk-Dateien";
$MESS["DISK_FOLDER_MODEL_IM_NEW_FILE2"] = "Es wurden neue #LINK_FOLDER_START#Dateien#LINK_END# in der Gruppe #group_name# hochgeladen.";
$MESS["DISK_FOLDER_SPECIFIC_FOR_RECORDED_FILES_NAME"] = "Aufgezeichnete Dateien";
?>