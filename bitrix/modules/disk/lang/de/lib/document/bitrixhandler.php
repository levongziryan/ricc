<?
$MESS["DISK_BITRIX_HANDLER_NAME"] = "Bitrix24.Viewer";
$MESS["DISK_BITRIX_HANDLER_ERROR_METHOD_IS_NOT_SUPPORTED"] = "#NAME# unterstützt nicht diese Aktion.";
$MESS["DISK_BITRIX_HANDLER_ERROR_NO_VIEW"] = "Dokument kann nicht in #NAME# angezeigt werden.";
$MESS["DISK_BITRIX_HANDLER_ERROR_NO_VIEW_SEND_TO_TRANSFORM"] = "Das Dokument-Viewer wird gerade vorbereitet, versuchen Sie bitte später erneut.";
?>