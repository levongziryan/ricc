<?
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_VERSION"] = "Neue Version wurde nicht gefunden.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_FILE"] = "Datei wurde nicht gefunden.";
$MESS["DISK_DOC_CONTROLLER_ERROR_BAD_RIGHTS"] = "Sie haben nicht genügend Rechte, um die Datei zu bearbeiten.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_EDIT_SESSION"] = "Bearbeitungssitzung wurde nicht gefunden.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_DELETE_SESSION"] = "Bearbeitungssitzung konnte nicht gelöscht werden.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_SAVE_FILE"] = "Die Datei konnte nicht gespeichert werden.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_ADD_VERSION"] = "Die Version konnte nicht hinzugefügt werden.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_GET_FILE"] = "De Datei konnte nicht bekommen werden.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_STORAGE"] = "Der Speicher konnte nicht gefunden werden.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_FOLDER_FOR_CREATED_FILES"] = "Der Ordner zum Speichern der Datei konnte nicht gefunden werden.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_CREATE_FILE"] = "Die Datei im Speicher konnte nicht erstellt werden.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_WORK_WITH_TOKEN_SERVICE"] = "Sozialer Service #NAME# ist nicht konfiguriert. Wenden Sie sich bitte an den Administrator Ihres Portals.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_WORK_WITH_TOKEN_SERVICE_B24"] = "Der Service der Sozialen Netzwerke #NAME# ist nicht konfiguriert. Wenden Sie sich bitte an Ihren Bitrix24-Administrator.";
?>