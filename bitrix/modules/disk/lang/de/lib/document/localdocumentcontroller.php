<?
$MESS["DISK_LOCAL_DOC_CONTROLLER_NAME"] = "Bitrix.Drive";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_ADD_VERSION"] = "Die neue Version der Datei konnte nicht hinzugefügt werden.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_VERSION"] = "Die Version wurde nicht gefunden.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_FOLDER"] = "Der Ordner wurde nicht gefunden.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_FILE"] = "Die Datei wurde nicht gefunden.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_BAD_RIGHTS"] = "Sie haben nicht genügend Rechte, um die Datei zu bearbeiten.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_SAVE_FILE"] = "Die Datei konnte nicht gespeichert werden.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_STORAGE"] = "Der Speicher konnte nicht gefunden werden.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_FOLDER_FOR_CREATED_FILES"] = "Der Ordner zum Speichern der Datei konnte nicht gefunden werden.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_ERROR_COULD_NOT_CREATE_FILE"] = "Die Datei im Speicher konnte nicht erstellt werden.";
$MESS["DISK_LOCAL_DOC_CONTROLLER_NAME_2_WORK_WITH"] = "Programme auf dem Computer";
?>