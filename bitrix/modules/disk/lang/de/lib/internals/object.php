<?
$MESS["DISK_OBJECT_ENTITY_STORAGE_ID_FIELD"] = "Speicher";
$MESS["DISK_OBJECT_ENTITY_PARENT_ID_FIELD"] = "Übergeordnetes Element";
$MESS["DISK_OBJECT_ENTITY_ERROR_UPDATE_STORAGE_ID"] = "Das Feld #FIELD# bei einem bereits erstellten Element kann nicht aktualisiert werden.";
$MESS["DISK_OBJECT_ENTITY_ERROR_UPDATE_PARENT_ID"] = "Das Feld #FIELD# bei einem bereits erstellten Element kann nicht aktualisiert werden. Benutzen Sie die Aktion Verschieben.";
$MESS["DISK_OBJECT_ENTITY_ERROR_NAME"] = "Ungültiger Wert für #FIELD#.";
$MESS["DISK_OBJECT_ENTITY_ERROR_DELETE_NODE"] = "Der Knoten konnte nicht gelöscht werden. Sie müssen zuerst alle untergeordneten Elemente löschen.";
$MESS["DISK_OBJECT_ENTITY_ERROR_REQUIRED_FILE_ID"] = "Das Feld #FIELD# für die Datei ist nicht angegeben.";
$MESS["DISK_OBJECT_ENTITY_ERROR_LINK_FILE_ID"] = "Das Feld #FIELD# ist für die Datei angegeben, welche ein Link ist.";
$MESS["DISK_OBJECT_ENTITY_ERROR_FIELD_NAME_HAS_INVALID_CHARS"] = "Der Name enthält ungültige Zeichen.";
$MESS["DISK_OBJECT_ENTITY_NAME_FIELD"] = "Name";
?>