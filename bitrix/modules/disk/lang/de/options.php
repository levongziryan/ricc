<?
$MESS["DISK_ALLOW_CREATE_FILE_BY_CLOUD"] = "Dokumentenbearbeitung via externe Services (Google Docs, MS Office Online etc.) aktivieren";
$MESS["DISK_ALLOW_AUTOCONNECT_SHARED_OBJECTS"] = "Gruppendrive mit dem Drive des Nutzers automatisch verbinden,<br/>wenn der Nutzer der Gruppe beitritt
";
$MESS["DISK_ALLOW_EDIT_OBJECT_IN_UF"] = "Alle beteiligten Nutzer können Dokumente bearbeiten,<br/>die an Diskussionen, Aufgaben, Kommentare oder andere Ereignisse angehängt sind.<br/>Dieses Standardverhalten kann in jedem dieser Ereignisse separat geändert werden";
$MESS["DISK_ALLOW_INDEX_FILES"] = "Dokumente zum Index des Moduls \"Suche\" hinzufügen";
$MESS["DISK_DEFAULT_VIEWER_SERVICE"] = "Dokumente anzeigen via";
$MESS["DISK_DEFAULT_VIEWER_SERVICE_NOTICE_SOC_SERVICE"] = "Der Service der Sozialen Netzwerke  <a href='/bitrix/admin/settings.php?lang=#LANG#&mid=socialservices' target='_blank'>#NAME#</a> ist nicht konfiguriert.";
$MESS["DISK_ENABLE_NGINX_MOD_ZIP_SUPPORT"] = "Option \"Dateien als Archiv herunterladen\" aktivieren";
$MESS["DISK_ENABLE_NGINX_MOD_ZIP_SUPPORT_NOTICE"] = "Das Modul <a href='#LINK#' target='_blank'>mod_zip</a> ist nicht installiert";
$MESS["DISK_ENABLE_RESTRICTION_STORAGE_SIZE_SUPPORT"] = "Speicherplatzquote aktivieren";
$MESS["DISK_ALLOW_USE_EXTERNAL_LINK"] = "Öffentliche Links erlauben";
$MESS["DISK_VERSION_LIMIT_PER_FILE"] = "Max. Anzahl der Einträge in der Dokument-History";
$MESS["DISK_VERSION_LIMIT_PER_FILE_UNLIMITED"] = "Unbegrenzt";
$MESS["DISK_ENABLE_OBJECT_LOCK_SUPPORT"] = "Die Sperrfunktion für Dokumente erlauben";
$MESS["DISK_ALLOW_DOCUMENT_TRANSFORMATION"] = "PDF- und JPG-Dateien für Dokumente automatisch generieren";
$MESS["DISK_MAX_SIZE_FOR_DOCUMENT_TRANSFORMATION"] = "Maximale Größe der Dokumente, aus denen PDF und JPG generiert werden ( in MB)";
$MESS["DISK_ALLOW_VIDEO_TRANSFORMATION"] = "MP4- und JPG-Dateien für Videos automatisch generieren";
$MESS["DISK_MAX_SIZE_FOR_VIDEO_TRANSFORMATION"] = "Maximale Größe der Dateien, aus denen MP4 und JPG generiert werden (in MB)";
$MESS["DISK_TRANSFORM_FILES_ON_OPEN"] = "Datei konvertieren, wenn sie geöffnet wird";
?>