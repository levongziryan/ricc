<?
$MESS["DISK_FW_CONVERT_FAILED"] = "Beim Versuch, Daten des Moduls \"Dokumentenbibliothek\" zu konvertieren, ist ein Fehler aufgetreten.";
$MESS["DISK_FW_PROCESSED_SUMMARY"] = "Elemente konvertiert (aufsteigend):";
$MESS["DISK_FW_CONVERT_IN_PROGRESS"] = "Es wurden noch nicht alle Daten konvertiert";
$MESS["DISK_FW_CONVERT_COMPLETE"] = "Daten-Konvertierung ist abgeschlossen";
$MESS["DISK_FW_CONVERT_TITLE"] = "Konvertierung des Daten des Moduls \"Dokumentenbibliothek\"";
$MESS["DISK_FW_CONVERT_TAB"] = "Daten-Konvertierung";
$MESS["DISK_FW_CONVERT_TAB_TITLE"] = "Daten-Konvertierung";
$MESS["DISK_FW_CONVERT_START_BUTTON"] = "Konvertierung starten";
$MESS["DISK_FW_CONVERT_STOP_BUTTON"] = "Konvertierung unterbrechen";
$MESS["DISK_FW_CONVERT_POPUP_NOTICE"] = "Im aktuellen Modul der Dokumentenbibliothek sind die Dokumente, die nicht veröffentlicht sind, für den Autor sowie für Nutzer mit dem Zugriffsrecht \"Element mit beliebigem Workflow-Status bearbeiten\" verfügbar. Im neuen Drive-Modul werden die nicht veröffentlichten Dokumente nur für den Autor verfügbar sein. ";
$MESS["DISK_FW_CONVERT_HELP_1"] = "Nach der Konvertierung werden einige Funktionen nicht mehr verfügbar sein.";
$MESS["DISK_FW_CONVERT_HELP_2"] = "In der neuen Version werden nicht unterstützt:";
$MESS["DISK_FW_CONVERT_HELP_3"] = "Das alte Workflow-Modul (Geschäftsprozesse bleiben verfügbar)";
$MESS["DISK_FW_CONVERT_HELP_4"] = "Dateibeschreibungen";
$MESS["DISK_FW_CONVERT_HELP_75"] = "Tags";
$MESS["DISK_FW_CONVERT_HELP_5"] = "Kommentare zu Dateien ";
$MESS["DISK_FW_CONVERT_HELP_6"] = "\"Likes\" für Dokumente";
$MESS["DISK_FW_CONVERT_HELP_7"] = "Datei-Versionen, welche aktuell in aktive Geschäftsprozesse involviert sind";
$MESS["DISK_FW_CONVERT_HELP_8"] = "Sie können aktuelle Vorlagen der Geschäftsprozesse benutzen, aber dann werden einige Funktionen des neuen Moduls nicht verfügbar sein. Wir empfehlen Ihnen, Standardvorlagen neu zu erstellen und die alten Vorlagen zu löschen (die werden dann auch in der Liste als alt markiert sein). Außerdem empfehlen wir Ihnen, die Funktionsweise der eigens erstellten Vorlagen von Geschäftsprozessen zu überprüden (falls zutreffend). ";
$MESS["DISK_FW_CONVERT_HELP_9"] = "Vor der Konvertierung wählen Sie bitte aus, was mit den nicht veröffentlichten Dokumenten geschehen soll";
$MESS["DISK_FW_CONVERT_HELP_10"] = "Veröffentlichen";
$MESS["DISK_FW_CONVERT_HELP_11"] = "Nicht veröffentlicht lassen";
$MESS["DISK_FW_CONVERT_HELP_12"] = "(Nutzer, welche früher darauf zugreifen konnten, werden diese Dokumente nicht mehr sehen können)";
$MESS["DISK_FW_CONVERT_HELP_13"] = "LÖSCHEN SIE NICHT das Modul Dokumentenbibliothek (alle Dateien dieses Moduls werden ins neue Modul Drive übertragen. Sie werden also keinen extra Speicherplatz belegen).";
?>