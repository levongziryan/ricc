<?
$MESS["OP_NAME_DISK_READ"] = "Element anzeigen";
$MESS["OP_NAME_DISK_ADD"] = "Elemente hinzufügen";
$MESS["OP_NAME_DISK_EDIT"] = "Element bearbeiten";
$MESS["OP_NAME_DISK_DELETE"] = "Element löschen";
$MESS["OP_NAME_DISK_SETTINGS"] = "Speicher konfigurieren";
$MESS["OP_NAME_DISK_RIGHTS"] = "Zugriffsrechte verwalten";
$MESS["OP_NAME_DISK_SHARING"] = "Option zur Element-Freigabe";
$MESS["OP_NAME_DISK_CREATE_WF"] = "Vorlagen der Geschäftsprozesse erstellen";
$MESS["OP_NAME_DISK_START_BP"] = "Geschäftsprozess starten";
$MESS["OP_NAME_DISK_DELETE_2"] = "Elemente in den Papierkorb verschieben";
$MESS["OP_NAME_DISK_DISK_DESTROY"] = "Elemente im Papierkorb löschen";
$MESS["OP_NAME_DISK_DISK_RESTORE"] = "Elemente aus dem Papierkorb wiederherstellen";
?>