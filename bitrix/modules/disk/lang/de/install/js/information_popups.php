<?
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_TITLE"] = "Das Dokument wurde nicht gespeichert!";
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_WAS_LOCKED_FORKED_COPY"] = "Das Dokument wurde gesperrt, während Sie es bearbeitet haben. Alle Ihre Änderungen wurden in einem neuen Dokument im Ordner <a class=\"disk-locked-document-popup-content-link\" href=\"#LINK#\" target=\"_blank\">Abgespeicherte</a> gespeichert.";
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_GO_TO_FILE"] = "Zur Datei";
?>