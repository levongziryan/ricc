<?
$MESS["DISK_DESKTOP_JS_SETTINGS"] = "Einstellungen";
$MESS["DISK_DESKTOP_JS_SETTINGS_LABEL_ENABLE"] = "Bitrix24.Drive aktivieren";
$MESS["DISK_DESKTOP_JS_SETTINGS_TITLE"] = "Bitrix24.Drive";
$MESS["DISK_DESKTOP_JS_SETTINGS_LABEL_FILE_CLICK_ACTION"] = "Per Klick auf eine Datei in \"Neulich hochgeladen\"";
$MESS["DISK_DESKTOP_JS_SETTINGS_LABEL_FILE_CLICK_ACTION_OPEN_FOLDER"] = "Ordner mit Inhalten öffnen";
$MESS["DISK_DESKTOP_JS_SETTINGS_LABEL_FILE_CLICK_ACTION_OPEN_FILE"] = "Datei öffnen";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_FOLDER_NOTICE"] = "Diese Ordner sind für Sie sichtbar, weil sie für Sie freigegeben wurden, obwohl Sie nicht der Besitzer dieser Ordner sind. Hier sind auch Ordner von Gruppen enthalten, in denen Sie Teilnehmer sind.";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_EMPTY_OWN_FOLDER_NOTICE"] = "Ihr Drive enthält keine Ordner. Sie haben lediglich Dateien, welche mit Ihrem PC automatisch synchronisiert werden. Möchten Sie nur bestimmte Dateien synchronisieren, müssen Sie dafür Ordner erstellen.";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_EMPTY_SHARED_FOLDER_NOTICE"] = "Ihr Drive enthält keine freigegebenen Ordner. Keiner Ihrer Kollegen hat einen Ordner für Sie freigegeben.

Sobald freigegebene Ordner auf Ihrem Drive erscheinen, können Sie einen oder mehrere Ordner auswählen, um sie mit Ihrem PC zu synchronisieren.
";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_TAB_OWNER"] = "Private Ordner";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_TAB_SHARED"] = "Freigegebene Ordner";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_MAKE_CHOICE"] = "Wählen Sie Ordner aus, um sie mit Ihrem PC zu synchronisieren";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_ALL"] = "Alle";
?>