<?
$MESS["DISK_INSTALL_DESCRIPTION"] = "Modul zum Erstellen und Bearbeiten von Dateien und Ordnern";
$MESS["DISK_INSTALL_NAME"] = "Drive";
$MESS["DISK_INSTALL_TITLE"] = "Modul installieren";
$MESS["DISK_UNINSTALL_TITLE"] = "Modul deinstallieren";
$MESS["DISK_NOTIFY_MIGRATE_WEBDAV"] = "Sie sollten <a href=\"#LINK#\">Daten des Moduls Dokumentenbibliothek konvertieren</a> , um zum Modul Drive migrieren zu können.";
$MESS["DISK_UNINSTALL_ERROR_MIGRATE_PROCESS"] = "Das Modul kann nicht deinstalliert werden, weil die Daten des Moduls Dokumentenbibliothek immer noch konvertiert werden.";
?>