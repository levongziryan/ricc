<?
$MESS["DISK_UF_CONTROLLER_SAVE_DOCUMENT_TITLE"] = "Seleccione una carpeta para guardar el documento";
$MESS["DISK_UF_CONTROLLER_SELECT_DOCUMENT_TITLE"] = "Seleccione uno o más documentos";
$MESS["DISK_UF_CONTROLLER_MY_DOCUMENTS"] = "Mi drive";
$MESS["DISK_UF_CONTROLLER_SHARED_DOCUMENTS"] = "Dirve de la Compañía";
$MESS["DISK_UF_CONTROLLER_MY_GROUPS"] = "Drives del grupo";
$MESS["DISK_UF_CONTROLLER_TITLE_SELECT"] = "Seleccione el documento";
$MESS["DISK_UF_CONTROLLER_SELECT_FOLDER"] = "Seleccionar carpeta actual";
$MESS["DISK_UF_CONTROLLER_SELECT_DOCUMENT"] = "Seleccione el documento";
$MESS["DISK_UF_CONTROLLER_CANCEL"] = "Cancelar";
$MESS["DISK_UF_CONTROLLER_TITLE_NAME"] = "Nombre";
$MESS["DISK_UF_CONTROLLER_TITLE_FILE_SIZE"] = "Tamaño";
$MESS["DISK_UF_CONTROLLER_TITLE_MODIFIED_BY"] = "Modificado por";
$MESS["DISK_UF_CONTROLLER_TITLE_TIMESTAMP"] = "Modificado el";
$MESS["DISK_UF_CONTROLLER_DESCR_DISABLE_ATTACH_NON_PUBLIC_FILE"] = "El documento no fue publicado. Ejecutar la publicación de documento del proceso de negocio.";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_FIND_USER_STORAGE"] = "No se puede encontrar el almacenamiento para el usuario.";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_FIND_FIND_FOLDER"] = "No se pudo encontrar la carpeta.";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_WORK_WITH_TOKEN_SERVICE"] = "Social service #NAME# no está configurado. Por favor, póngase en contacto con el administrador del portal.";
$MESS["DISK_UF_CONTROLLER_RECENTLY_USED"] = "Elementos recientes";
$MESS["DISK_UF_CONTROLLER_SELECT_ONE_DOCUMENT_TITLE"] = "Seleccione documento";
$MESS["DISK_UF_CONTROLLER_ERROR_COULD_NOT_WORK_WITH_TOKEN_SERVICE_B24"] = "El servicio de redes sociales #NAME# no está configurado. Por favor, póngase en contacto con el administrador de Bitrix24.";
$MESS["DISK_UF_CONTROLLER_FILE_IS_TOO_BIG_FOR_TRANSFORMATION"] = "El archivo de video es demasiado grande para verlo en Bitrix24. <a class=\"transformer-upgrade-popup\" href=\"jav * ascript:void(0);\">Detalles</a>";
$MESS["DISK_UF_CONTROLLER_TRANSFORMATION_UPGRADE_POPUP_TITLE"] = "Disponible solo en Bitrix24.Drive extendido";
$MESS["DISK_UF_CONTROLLER_TRANSFORMATION_UPGRADE_POPUP_CONTENT"] = "<b>Lamentablemente, el tamaño de su archivo de video es demasiado grande para verlo en Bitrix24.</b><br />
Estamos guardando el archivo, pero los usuarios tendrán que descargarlo antes de poder verlo.<br /><br />
Se pueden reproducir archivos de hasta 300 MB en plan gratuito.<br />
Los planes comerciales pueden reproducir archivos de hasta 3 GB.<br /><br />
Actualízate ahora y disfruta de más funciones útiles:
<ul class=\"hide-features-list\">
<li class=\"hide-features-list-item\">Copias de archivos de copia de seguridad ilimitadas</li>
<li class=\"hide-features-list-item\">Bloqueo de documentos</li>
<li class=\"hide-features-list-item\">Enlaces a carpetas públicas</li>
<li class=\"hide-features-list-item\">Utilice flujos de trabajo con Drive de la compañía</li>
</ul>
Consulte la tabla de comparación de planes y la descripción completa de las funciones <a target=\"_blank\" href=\"https://www.bitrix24.com/prices/index.php\">aquí</a>.";
?>