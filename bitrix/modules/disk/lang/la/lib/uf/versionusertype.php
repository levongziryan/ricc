<?
$MESS["DISK_VERSION_USER_TYPE_ERROR_COULD_NOT_FIND_ATTACHED_OBJECT"] = "No se puede encontrar el objeto adjunto.";
$MESS["DISK_VERSION_USER_TYPE_ERROR_COULD_NOT_FIND_FILE"] = "El archivo no se pudo encontrar.";
$MESS["DISK_VERSION_USER_TYPE_ERROR_INVALID_VALUE"] = "Valor no válido: debe ser un número.";
$MESS["DISK_VERSION_USER_TYPE_ERROR_USER_ID"] = "El usuario no está conectado.";
$MESS["DISK_VERSION_USER_TYPE_ERROR_BAD_RIGHTS"] = "Permiso insuficiente.";
$MESS["DISK_VERSION_USER_TYPE_NAME"] = "Versión del archivo (Drive) ";
?>