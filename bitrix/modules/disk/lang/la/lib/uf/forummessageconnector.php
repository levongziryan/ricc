<?
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE01"] = "Mensajes en el foro ";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE02"] = "Comentarios";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE03"] = "Comentarios de la tarea";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE04"] = "Comentarios del calendario de eventos";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE05"] = "Comentario del documento";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE06"] = "Comentario en el CRM";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE07"] = "Comentario del usuario del documento";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE08"] = "Mensajes del foro de grupo";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE09"] = "Mensajes del usuario del foro";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE10"] = "Comentarios de las noticias";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE11"] = "Comentarios de las fotos";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE12"] = "Comentarios del wiki";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE13"] = "Comentarios del reporte de trabajo";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE14"] = "Comentarios del registro de tiempo de trabajo";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE15"] = "Comentario del elemento del Bloque de información";
$MESS["DISK_UF_FORUM_MESSAGE_CONNECTOR_MESSAGE16"] = "Comentario de reuniones";
?>