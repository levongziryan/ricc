<?
$MESS["DISK_FILE_MODEL_ERROR_COULD_NOT_SAVE_FILE"] = "No se puede guardar el archivo.";
$MESS["DISK_FILE_MODEL_ERROR_COULD_NOT_COPY_FILE"] = "No se puede copiar el archivo.";
$MESS["DISK_FILE_MODEL_ERROR_COULD_NOT_RESTORE_FROM_ANOTHER_OBJECT"] = "No se puede restaurar la versión desde otro archivo.";
$MESS["DISK_FILE_MODEL_UPLOAD_NEW_VERSION_IN_COMMENT_M"] = "cargó una nueva versión del archivo";
$MESS["DISK_FILE_MODEL_UPLOAD_NEW_VERSION_IN_COMMENT_F"] = "cargó una nueva versión del archivo";
$MESS["DISK_FILE_MODEL_ERROR_SIZE_RESTRICTION"] = "No se pudo guardar el archivo porque el tamaño de este supera la cuota.";
$MESS["DISK_FILE_MODEL_ERROR_EXCLUSIVE_LOCK"] = "No se puede actualizar el archivo, ya que fue bloqueado por otro usuario.";
$MESS["DISK_FILE_MODEL_ERROR_ALREADY_LOCKED"] = "No se puede bloquear el archivo, ya que fue bloqueado por otro usuario.";
$MESS["DISK_FILE_MODEL_ERROR_INVALID_LOCK_TOKEN"] = "No se puede desbloquear el archivo: no coincide el token.";
$MESS["DISK_FILE_MODEL_ERROR_INVALID_USER_FOR_UNLOCK"] = "No se puede abrir el archivo porque estaba bloqueado por otro usuario.";
$MESS["DISK_FILE_MODEL_UPLOAD_NEW_HEAD_VERSION_IN_COMMENT_M"] = "Archivo editado";
$MESS["DISK_FILE_MODEL_UPLOAD_NEW_HEAD_VERSION_IN_COMMENT_F"] = "Archivo editado";
?>