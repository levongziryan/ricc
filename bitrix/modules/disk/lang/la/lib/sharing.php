<?
$MESS["DISK_SHARING_MODEL_AUTOCONNECT_NOTIFY"] = "Carpeta compartida <b>\"#NAME#\"</b> ya está conectado al Drive. #DISCONNECT_LINK#
#DESCRIPTION#";
$MESS["DISK_SHARING_MODEL_APPROVE_N"] = "Desconectar";
$MESS["DISK_SHARING_MODEL_APPROVE_Y"] = "Conectar carpeta";
$MESS["DISK_SHARING_MODEL_TEXT_DISCONNECT_LINK"] = "Cancelar";
$MESS["DISK_SHARING_MODEL_TEXT_APPROVE_CONFIRM"] = "¿Desea conectar una carpeta compartida <b>\"#NAME#\"</b> para la Drive?";
$MESS["DISK_SHARING_MODEL_TEXT_SELF_DISCONNECT"] = "#USERNAME# desconectado de su carpeta compartida <b>\"#NAME#\"</b>";
$MESS["DISK_SHARING_MODEL_ERROR_EMPTY_USER_ID"] = "No se especifica el usuario.";
$MESS["DISK_SHARING_MODEL_ERROR_EMPTY_REAL_OBJECT"] = "No se especificó ningún objeto para compartir.";
$MESS["DISK_SHARING_MODEL_ERROR_COULD_NOT_FIND_USER_STORAGE"] = "No puede encontrar almacenamiento para el usuario (#USER_ID#).";
$MESS["DISK_SHARING_MODEL_ERROR_COULD_NOT_FIND_STORAGE"] = "No se pudo encontrar almacenamiento.";
$MESS["DISK_SHARING_MODEL_ERROR_COULD_NOT_CREATE_LINK"] = "No se puede crear un enlace simbólico a un objeto.";
$MESS["DISK_SHARING_MODEL_AUTOCONNECT_NOTIFY_FILE"] = "Archivo compartido <b>\"#NAME#\"</b> se ha unido al drive. #DISCONNECT_LINK#
#DESCRIPTION#";
$MESS["DISK_SHARING_MODEL_APPROVE_Y_FILE"] = "Adjuntar archivo";
$MESS["DISK_SHARING_MODEL_TEXT_APPROVE_CONFIRM_FILE"] = "¿Desea adjuntar el archivo compartido <b>\"#NAME#\"</b> al drive?";
$MESS["DISK_SHARING_MODEL_TEXT_SELF_DISCONNECT_FILE"] = "#USERNAME# desconectado de su archivo compartido <b>\"#NAME#\"</b>";
$MESS["DISK_SHARING_MODEL_APPROVE_N_2_DECLINE"] = "Cancelar";
?>