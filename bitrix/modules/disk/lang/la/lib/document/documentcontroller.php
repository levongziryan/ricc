<?
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_VERSION"] = "La versión no pudo ser encontrado.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_FILE"] = "El archivo no se pudo encontrar.";
$MESS["DISK_DOC_CONTROLLER_ERROR_BAD_RIGHTS"] = "Permiso insuficiente para manejar archivos.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_EDIT_SESSION"] = "No existe sección de edición";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_DELETE_SESSION"] = "No se puede eliminar la sesión de edición.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_SAVE_FILE"] = "No se puede guardar el archivo.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_ADD_VERSION"] = "No puede agregar la versión.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_GET_FILE"] = "No se puede recuperar el archivo.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_STORAGE"] = "No se puede encontrar almacenamiento.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_FIND_FOLDER_FOR_CREATED_FILES"] = "No puede encontrar una carpeta para guardar el archivo.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_CREATE_FILE"] = "No puede crear un archivo en el almacenamiento.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_WORK_WITH_TOKEN_SERVICE"] = "Servicio Social #NAME# no está configurado. Por favor, póngase en contacto con el administrador del portal.";
$MESS["DISK_DOC_CONTROLLER_ERROR_COULD_NOT_WORK_WITH_TOKEN_SERVICE_B24"] = "El servicio de redes sociales #NAME# no está configurado. Por favor, póngase en contacto con el administrador de Bitrix24.";
?>