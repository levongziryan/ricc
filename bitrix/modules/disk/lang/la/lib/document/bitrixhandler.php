<?
$MESS["DISK_BITRIX_HANDLER_NAME"] = "Bitrix24.Viewer";
$MESS["DISK_BITRIX_HANDLER_ERROR_METHOD_IS_NOT_SUPPORTED"] = "#NAME# no es compatible con esta acción.";
$MESS["DISK_BITRIX_HANDLER_ERROR_NO_VIEW"] = "El documento no se puede ver en #NAME#.";
$MESS["DISK_BITRIX_HANDLER_ERROR_NO_VIEW_SEND_TO_TRANSFORM"] = "Ahora estamos preparando el visor de documentos. Por favor, inténtelo de nuevo más tarde.";
?>