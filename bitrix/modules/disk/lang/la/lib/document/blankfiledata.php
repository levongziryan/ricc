<?
$MESS["DISK_BLANK_FILE_DATA_TYPE_DOCX"] = "Documento";
$MESS["DISK_BLANK_FILE_DATA_TYPE_XLSX"] = "Hoja de cálculo";
$MESS["DISK_BLANK_FILE_DATA_TYPE_PPTX"] = "Presentación";
$MESS["DISK_BLANK_FILE_DATA_NEW_FILE_DOCX"] = "Nuevo documento";
$MESS["DISK_BLANK_FILE_DATA_NEW_FILE_XLSX"] = "Nueva hoja de cálculo";
$MESS["DISK_BLANK_FILE_DATA_NEW_FILE_PPTX"] = "Nueva presentación";
?>