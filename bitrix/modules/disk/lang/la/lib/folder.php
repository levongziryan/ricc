<?
$MESS["DISK_FOLDER_MODEL_ERROR_NON_UNIQUE_NAME"] = "El nombre del objeto no es único.";
$MESS["DISK_FOLDER_MODEL_ERROR_COULD_NOT_DELETE_WITH_CODE"] = "No puede eliminar la carpeta con el código #CODE#.";
$MESS["DISK_FOLDER_MODEL_ERROR_COULD_NOT_SAVE_FILE"] = "No se puede guardar el archivo.";
$MESS["DISK_FOLDER_MODEL_IM_NEW_FILE"] = "Un nuevo archivo #TITLE# se ha cargado al #group_name# group.";
$MESS["DISK_FOLDER_SPECIFIC_FOR_CREATED_FILES_NAME"] = "Archivos creados";
$MESS["DISK_FOLDER_SPECIFIC_FOR_SAVED_FILES_NAME"] = "Archivos almacenados";
$MESS["DISK_FOLDER_SPECIFIC_FOR_UPLOADED_FILES_NAME"] = "Archivos cargados";
$MESS["DISK_FOLDER_SPECIFIC_FOR_DROPBOX_FILES_NAME"] = "Archivos Dropbox";
$MESS["DISK_FOLDER_SPECIFIC_FOR_ONEDRIVE_FILES_NAME"] = "Archivos OneDrive";
$MESS["DISK_FOLDER_SPECIFIC_FOR_GDRIVE_FILES_NAME"] = "Archivos Google Drive";
$MESS["DISK_FOLDER_SPECIFIC_FOR_BOX_FILES_NAME"] = "Archivos Box";
$MESS["DISK_FOLDER_SPECIFIC_FOR_YANDEXDISK_FILES_NAME"] = "Archivos Yandex.Disk";
$MESS["DISK_FOLDER_MODEL_IM_NEW_FILE2"] = "Nuevo #LINK_FOLDER_START#files#LINK_END# cargado al grupo #group_name#.";
$MESS["DISK_FOLDER_SPECIFIC_FOR_RECORDED_FILES_NAME"] = "Archivos grabados";
?>