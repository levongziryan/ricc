<?
$MESS["DISK_OBJECT_ENTITY_STORAGE_ID_FIELD"] = "Almacenamiento";
$MESS["DISK_OBJECT_ENTITY_ERROR_UPDATE_STORAGE_ID"] = "No se puede actualizar \"#FIELD#\" un objeto existente.";
$MESS["DISK_OBJECT_ENTITY_ERROR_UPDATE_PARENT_ID"] = "No se puede actualizar \"#FIELD#\" un objeto existente. Por favor utilice la acción \"mover\".";
$MESS["DISK_OBJECT_ENTITY_ERROR_NAME"] = "Valor no válido #FIELD#.";
$MESS["DISK_OBJECT_ENTITY_ERROR_DELETE_NODE"] = "No puede eliminar el tree node. Primero elimine todos los elementos secundarios.";
$MESS["DISK_OBJECT_ENTITY_ERROR_REQUIRED_FILE_ID"] = "Archivos #FIELD# no se especificados.";
$MESS["DISK_OBJECT_ENTITY_ERROR_LINK_FILE_ID"] = "#FIELD# especificado para un enlace simbólico.";
$MESS["DISK_OBJECT_ENTITY_ERROR_FIELD_NAME_HAS_INVALID_CHARS"] = "El nombre contiene caracteres inválidos.";
$MESS["DISK_OBJECT_ENTITY_NAME_FIELD"] = "Nombre";
$MESS["DISK_OBJECT_ENTITY_PARENT_ID_FIELD"] = "Principal";
?>