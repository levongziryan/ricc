<?
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_TITLE"] = "El documento no se guardó!";
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_WAS_LOCKED_FORKED_COPY"] = "El documento ha sido bloqueado mientras se estaba editando. Todos los cambios se han guardado en un nuevo documento en la <a class=\"disk-locked-document-popup-content-link\" href=\"#LINK#\" target=\"_blank\">Stored</a> folder.";
$MESS["DISK_JS_INF_POPUPS_LOCKED_DOC_GO_TO_FILE"] = "Ir al archivo";
?>