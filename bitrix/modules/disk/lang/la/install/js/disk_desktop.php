<?
$MESS["DISK_DESKTOP_JS_SETTINGS"] = "Ajustes";
$MESS["DISK_DESKTOP_JS_SETTINGS_LABEL_ENABLE"] = "Habilitar Bitrix24.Drive";
$MESS["DISK_DESKTOP_JS_SETTINGS_TITLE"] = "Bitrix24.Drive";
$MESS["DISK_DESKTOP_JS_SETTINGS_LABEL_FILE_CLICK_ACTION"] = "Hacer clic en el archivo \"Cargas Recientes\"";
$MESS["DISK_DESKTOP_JS_SETTINGS_LABEL_FILE_CLICK_ACTION_OPEN_FOLDER"] = "Abrir la carpeta contenedora";
$MESS["DISK_DESKTOP_JS_SETTINGS_LABEL_FILE_CLICK_ACTION_OPEN_FILE"] = "Abrir el archivo";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_FOLDER_NOTICE"] = "Estas carpetas son visibles para usted porque son compartidos con usted, aunque usted no es un propietario de estas carpetas. Esta lista también incluye carpetas de los grupos a los que pertenece.";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_EMPTY_OWN_FOLDER_NOTICE"] = "El drive no tiene carpetas. Usted tiene sólo los archivos que se sincronizan automáticamente con su PC. Para sincronizar sólo algunos de los archivos, crear carpetas para ellos.";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_EMPTY_SHARED_FOLDER_NOTICE"] = "Usted no tiene carpetas compartidas en su drive. Ninguno de sus colegas ha compartido una carpeta con usted todavía.

Una vez allí se comparten las carpetas en su udrive, puede seleccionar uno o más de ellos para sincronizar con su PC.";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_TAB_OWNER"] = "Carpetas privadas";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_TAB_SHARED"] = "Carpetas compartidas";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_MAKE_CHOICE"] = "Seleccione las carpetas para sincronizar con este ordenador";
$MESS["DISK_DESKTOP_JS_SETTINGS_SYNC_ALL"] = "Todo";
?>