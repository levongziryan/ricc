<?
$MESS["DISK_INSTALL_DESCRIPTION"] = "Un módulo para crear, editar y gestionar archivos y carpetas";
$MESS["DISK_INSTALL_NAME"] = "Drive";
$MESS["DISK_INSTALL_TITLE"] = "Módulo de instalación";
$MESS["DISK_UNINSTALL_TITLE"] = "Desinstalación del módulo";
$MESS["DISK_NOTIFY_MIGRATE_WEBDAV"] = "Por favor, <a href=\"#LINK#\">convertir los datos del módulo de la biblioteca de documentos</a> para migrar al módulo Drive.";
$MESS["DISK_UNINSTALL_ERROR_MIGRATE_PROCESS"] = "El módulo no se puede desinstalar porque los datos del módulo Librería de Documentos todavía se están convirtiendo.";
?>