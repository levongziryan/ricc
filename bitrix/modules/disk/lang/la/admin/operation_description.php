<?
$MESS["OP_NAME_DISK_READ"] = "Ver elementos";
$MESS["OP_NAME_DISK_EDIT"] = "Editar elementos";
$MESS["OP_NAME_DISK_DELETE"] = "Eliminar";
$MESS["OP_NAME_DISK_SETTINGS"] = "Ajustes de almacenamiento";
$MESS["OP_NAME_DISK_RIGHTS"] = "Administrar los permisos de acceso";
$MESS["OP_NAME_DISK_SHARING"] = "Compartir elementos";
$MESS["OP_NAME_DISK_CREATE_WF"] = "Creación de plantillas de procesos de negocio";
$MESS["OP_NAME_DISK_START_BP"] = "Ejecutar los procesos de negocio";
$MESS["OP_NAME_DISK_ADD"] = "Agregar elementos";
$MESS["OP_NAME_DISK_DELETE_2"] = "Eliminar elementos de la Papelera de Reciclaje.";
$MESS["OP_NAME_DISK_DISK_DESTROY"] = "Eliminar elementos desde la Papelera de Reciclaje.";
$MESS["OP_NAME_DISK_DISK_RESTORE"] = "Recuperar elementos de la Papelera de Reciclaje";
?>