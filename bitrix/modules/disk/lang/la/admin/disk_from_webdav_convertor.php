<?
$MESS["DISK_FW_CONVERT_FAILED"] = "Error al convertir datos del módulo de Librería de Documentos";
$MESS["DISK_FW_PROCESSED_SUMMARY"] = "Elementos convertidos (acumulado):";
$MESS["DISK_FW_CONVERT_IN_PROGRESS"] = "Aún sin convertir";
$MESS["DISK_FW_CONVERT_COMPLETE"] = "Los datos se han convertido.";
$MESS["DISK_FW_CONVERT_TITLE"] = "Conversión de Datos de la Libreria de Documentos";
$MESS["DISK_FW_CONVERT_TAB"] = "Conversión de Datos";
$MESS["DISK_FW_CONVERT_TAB_TITLE"] = "Conversión de Datos";
$MESS["DISK_FW_CONVERT_START_BUTTON"] = "Convertir";
$MESS["DISK_FW_CONVERT_STOP_BUTTON"] = "Parar";
$MESS["DISK_FW_CONVERT_HELP_75"] = "Etiquetas";
$MESS["DISK_FW_CONVERT_HELP_5"] = "Comentarios del archivo";
$MESS["DISK_FW_CONVERT_HELP_6"] = "\"Likes\" para el documento";
$MESS["DISK_FW_CONVERT_HELP_10"] = "Publicar";
$MESS["DISK_FW_CONVERT_POPUP_NOTICE"] = "En el módulo Librería de Documentos actuales, los docuemntos no publicados están disponibles para el autor y para los usuarios con permiso para \"Editar elementos en cualquier estado del flujo de trabajo\". En el nuevo módulo Drive, los documentos no publicados serán accesibles sólo el autor.";
$MESS["DISK_FW_CONVERT_HELP_1"] = "Después de la conversión, algunas funciones no estarán disponibles.";
$MESS["DISK_FW_CONVERT_HELP_2"] = "La nueva versión no es compatible:";
$MESS["DISK_FW_CONVERT_HELP_3"] = "El antiguo módulo de Flujo de Trabajo (Business Process están presentes)";
$MESS["DISK_FW_CONVERT_HELP_4"] = "Descripciones de archivos";
$MESS["DISK_FW_CONVERT_HELP_7"] = "Las versiones de los archivos que en la actualidad están sujetos a un activo Proceso de Negocios";
$MESS["DISK_FW_CONVERT_HELP_8"] = "Puede utilizar la plantilla actual del Proceso de Negocios, pero algunas de las funciones del nuevo módulo no estarán disponible. Le recomendamos volver a crear las plantillas estándar y eliminar los antiguos (que será marcado como antiguo en la lista). También le recomendamos que la funcionalidad de las plantillas de procesos de negocio personalizadas (que ha creado) se han comprobadas.";
$MESS["DISK_FW_CONVERT_HELP_9"] = "Antes de convertir, elegir la forma en el proceso de conversión y se encargará de documentos no publicados";
$MESS["DISK_FW_CONVERT_HELP_11"] = "Dejar documentos no publicados";
$MESS["DISK_FW_CONVERT_HELP_12"] = "(Los usuarios que anteriormente tenían acceso ya no serán capaces de ver estos documentos)";
$MESS["DISK_FW_CONVERT_HELP_13"] = "NO ELIMINE el módulo de Librería de Documentos (todos los archivos de este módulo se migrarán al nuevo módulo Drive. Ellos no ocupan espacio adicional en disco).";
?>