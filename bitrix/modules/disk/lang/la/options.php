<?
$MESS["DISK_ALLOW_CREATE_FILE_BY_CLOUD"] = "Activar la edición de documentos utilizando servicios externos (Google Docs, MS Office Online etc.)";
$MESS["DISK_ALLOW_AUTOCONNECT_SHARED_OBJECTS"] = "Conexión automática al Drive de Grupo<br/>cuando un usuario se une al grupo";
$MESS["DISK_ALLOW_EDIT_OBJECT_IN_UF"] = "Todos los usuarios involucrados pueden editar documentos adjuntos a las discusiones, tareas, <br/>comentarios u otros eventos. Este comportamiento predeterminado se puede cambiar <br/>en cualquiera de los eventos de forma individual.";
$MESS["DISK_ALLOW_INDEX_FILES"] = "Agregar documentos al índice del módulo \"Buscador\"";
$MESS["DISK_DEFAULT_VIEWER_SERVICE"] = "Ver documentos utilizando";
$MESS["DISK_DEFAULT_VIEWER_SERVICE_NOTICE_SOC_SERVICE"] = "El servicio de redes sociales <a href='/bitrix/admin/settings.php?lang=#LANG#&mid=socialservices' target='_blank'>#NAME#</a> no está configurado.";
$MESS["DISK_ENABLE_NGINX_MOD_ZIP_SUPPORT"] = "Habilitar la característica \"descargar carpetas como archivo\"";
$MESS["DISK_ENABLE_NGINX_MOD_ZIP_SUPPORT_NOTICE"] = "El módulo <a href='#LINK#' target='_blank'>mod_zip</a> no está instalado";
$MESS["DISK_ENABLE_RESTRICTION_STORAGE_SIZE_SUPPORT"] = "Habilitar cuota de almacenamiento";
$MESS["DISK_ALLOW_USE_EXTERNAL_LINK"] = "Permitir enlaces públicos";
$MESS["DISK_VERSION_LIMIT_PER_FILE"] = "Máx. de entradas en el historial del documento";
$MESS["DISK_VERSION_LIMIT_PER_FILE_UNLIMITED"] = "Ilimitado";
$MESS["DISK_ENABLE_OBJECT_LOCK_SUPPORT"] = "Permitir el bloqueo de documentos";
$MESS["DISK_ALLOW_DOCUMENT_TRANSFORMATION"] = "Genera automáticamente archivos PDF y JPG para documentos";
$MESS["DISK_MAX_SIZE_FOR_DOCUMENT_TRANSFORMATION"] = "El tamaño máximo del documento fuente para la generación de PDF y JPG (MB)";
$MESS["DISK_ALLOW_VIDEO_TRANSFORMATION"] = "Genera automáticamente archivos MP4 y JPG para medios de video";
$MESS["DISK_MAX_SIZE_FOR_VIDEO_TRANSFORMATION"] = "Tamaño máximo de los medios de origen para la generación de MP4 y JPG (MB)";
$MESS["DISK_TRANSFORM_FILES_ON_OPEN"] = "Convertir archivo tan pronto como se abra";
?>