<?
$MESS["BPDD_PD_SOURCE_ID"] = "Objeto de origem";
$MESS["BPDD_PD_SOURCE_ID_DESCR"] = "Arquivo ou pasta do Drive";
$MESS["BPDD_PD_LABEL_DISK_CHOOSE_FILE"] = "Selecionar arquivo";
$MESS["BPDD_PD_LABEL_DISK_CHOOSE_FOLDER"] = "Selecionar pasta";
$MESS["BPDD_PD_LABEL_DISK_EMPTY"] = "Nenhuma seleção";
?>