<?
$MESS["BPDD_PD_SOURCE_ID"] = "Objet source";
$MESS["BPDD_PD_SOURCE_ID_DESCR"] = "Fichier ou dossier du Drive";
$MESS["BPDD_PD_LABEL_DISK_CHOOSE_FILE"] = "Sélectionnez un fichier";
$MESS["BPDD_PD_LABEL_DISK_CHOOSE_FOLDER"] = "Sélectionnez un dossier";
$MESS["BPDD_PD_LABEL_DISK_EMPTY"] = "Aucune sélection";
?>