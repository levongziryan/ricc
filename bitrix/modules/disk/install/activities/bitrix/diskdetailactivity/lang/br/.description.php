<?
$MESS["BPDD_DESCR_NAME2"] = "Informações do objeto do Drive";
$MESS["BPDD_DESCR_DESCR2"] = "Obter informações do objeto do Drive";
$MESS["BPDD_DESCR_TYPE"] = "Tipo de objeto";
$MESS["BPDD_DESCR_NAME"] = "Nome";
$MESS["BPDD_DESCR_SIZE_BYTES"] = "Tamanho, bytes";
$MESS["BPDD_DESCR_SIZE_FORMATTED"] = "Tamanho (formatado)";
$MESS["BPDD_DESCR_DETAIL_URL"] = "Visualizar URL";
$MESS["BPDD_DESCR_DOWNLOAD_URL"] = "Baixar URL";
$MESS["BPDD_DESCR_CATEGORY"] = "Drive";
?>