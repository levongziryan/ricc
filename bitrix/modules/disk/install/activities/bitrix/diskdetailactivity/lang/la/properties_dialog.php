<?
$MESS["BPDD_PD_SOURCE_ID"] = "Objeto de origen";
$MESS["BPDD_PD_SOURCE_ID_DESCR"] = "Archivo o carpeta del drive";
$MESS["BPDD_PD_LABEL_DISK_CHOOSE_FILE"] = "Seleccione archivo";
$MESS["BPDD_PD_LABEL_DISK_CHOOSE_FOLDER"] = "Seleccione carpeta";
$MESS["BPDD_PD_LABEL_DISK_EMPTY"] = "No se ha seleccionado";
?>