<?
$MESS["BPDCM_PD_ENTITY"] = "Para";
$MESS["BPDCM_PD_ENTITY_TYPE_USER"] = "Drive do Usuário";
$MESS["BPDCM_PD_ENTITY_TYPE_SG"] = "Drive do grupo de rede social";
$MESS["BPDCM_PD_ENTITY_TYPE_COMMON"] = "Drive Público";
$MESS["BPDCM_PD_ENTITY_TYPE_FOLDER"] = "Pasta Driver";
$MESS["BPDCM_PD_SOURCE_ID"] = "Objeto de origem";
$MESS["BPDCM_PD_SOURCE_ID_DESCR"] = "Arquivo ou pasta do Drive";
$MESS["BPDCM_PD_OPERATION"] = "Operação";
$MESS["BPDCM_PD_OPERATION_COPY"] = "Copiar";
$MESS["BPDCM_PD_OPERATION_MOVE"] = "Mover";
$MESS["BPDCM_PD_ENTITY_ID_USER"] = "Usuário";
$MESS["BPDCM_PD_ENTITY_ID_SG"] = "Grupo";
$MESS["BPDCM_PD_ENTITY_ID_COMMON"] = "Drive";
$MESS["BPDCM_PD_ENTITY_ID_FOLDER"] = "Pasta";
$MESS["BPDCM_PD_LABEL_CHOOSE"] = "Selecionar:";
$MESS["BPDCM_PD_LABEL_DISK_CHOOSE_FILE"] = "Selecionar arquivo";
$MESS["BPDCM_PD_LABEL_DISK_CHOOSE_FOLDER"] = "Selecionar pasta";
$MESS["BPDCM_PD_LABEL_DISK_EMPTY"] = "Nenhuma seleção";
$MESS["BPDCM_PD_OPERATOR"] = "Executar como";
?>