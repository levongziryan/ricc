<?
$MESS["BPDCM_ACCESS_DENIED"] = "Acesso negado a qualquer pessoa, exceto os administradores do portal.";
$MESS["BPDCM_EMPTY_ENTITY_TYPE"] = "O tipo de armazenamento está faltando.";
$MESS["BPDCM_EMPTY_ENTITY_ID"] = "O armazenamento não está especificado.";
$MESS["BPDCM_EMPTY_SOURCE_ID"] = "Nenhum objeto especificado para copiar ou mover.";
$MESS["BPDCM_TARGET_ERROR"] = "Não foi possível resolver nova localização da pasta.";
$MESS["BPDCM_SOURCE_ERROR"] = "Não foi possível resolver o objeto de origem.";
$MESS["BPDCM_OPERATION_ERROR"] = "Erro ao executar operação.";
$MESS["BPDCM_ADD_FOLDER_ERROR"] = "Não foi possível criar uma nova pasta.";
?>