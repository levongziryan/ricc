<?
$MESS["BPDCM_PD_ENTITY"] = "Vers";
$MESS["BPDCM_PD_ENTITY_TYPE_USER"] = "Lecteur utilisateur";
$MESS["BPDCM_PD_ENTITY_TYPE_SG"] = "Drive de groupe de réseau social";
$MESS["BPDCM_PD_ENTITY_TYPE_COMMON"] = "Drive public";
$MESS["BPDCM_PD_ENTITY_TYPE_FOLDER"] = "Dossier du lecteur";
$MESS["BPDCM_PD_SOURCE_ID"] = "Objet source";
$MESS["BPDCM_PD_SOURCE_ID_DESCR"] = "Fichier ou dossier du Drive";
$MESS["BPDCM_PD_OPERATION"] = "Opération";
$MESS["BPDCM_PD_OPERATION_COPY"] = "Copier";
$MESS["BPDCM_PD_OPERATION_MOVE"] = "Déplacer";
$MESS["BPDCM_PD_ENTITY_ID_USER"] = "Utilisateur";
$MESS["BPDCM_PD_ENTITY_ID_SG"] = "Groupe";
$MESS["BPDCM_PD_ENTITY_ID_COMMON"] = "Drive";
$MESS["BPDCM_PD_ENTITY_ID_FOLDER"] = "Dossier";
$MESS["BPDCM_PD_LABEL_CHOOSE"] = "Sélectionnez :";
$MESS["BPDCM_PD_LABEL_DISK_CHOOSE_FILE"] = "Sélectionnez un fichier";
$MESS["BPDCM_PD_LABEL_DISK_CHOOSE_FOLDER"] = "Sélectionnez un dossier";
$MESS["BPDCM_PD_LABEL_DISK_EMPTY"] = "Aucune sélection";
$MESS["BPDCM_PD_OPERATOR"] = "Exécuter en tant que";
?>