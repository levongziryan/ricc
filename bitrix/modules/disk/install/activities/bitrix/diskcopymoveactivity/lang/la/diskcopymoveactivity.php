<?
$MESS["BPDCM_ACCESS_DENIED"] = "Acceso denegado excepto a los administradores del portal.";
$MESS["BPDCM_EMPTY_ENTITY_TYPE"] = "Falta el tipo de almacenamiento.";
$MESS["BPDCM_EMPTY_ENTITY_ID"] = "No se especifica de almacenamiento.";
$MESS["BPDCM_EMPTY_SOURCE_ID"] = "No hay ningún objeto para copiar o mover especificado.";
$MESS["BPDCM_TARGET_ERROR"] = "No se pudo resolver la nueva ubicación de la carpeta.";
$MESS["BPDCM_SOURCE_ERROR"] = "No se pudo resolver el objeto del origen.";
$MESS["BPDCM_OPERATION_ERROR"] = "Error al realizar la operación.";
$MESS["BPDCM_ADD_FOLDER_ERROR"] = "No se puede crear una nueva carpeta.";
?>