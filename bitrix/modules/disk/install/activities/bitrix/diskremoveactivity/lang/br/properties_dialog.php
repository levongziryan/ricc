<?
$MESS["BPDRMV_PD_SOURCE_ID"] = "Objeto de origem";
$MESS["BPDRMV_PD_SOURCE_ID_DESCR"] = "Arquivo ou pasta do Drive";
$MESS["BPDRMV_PD_LABEL_DISK_CHOOSE_FILE"] = "Selecionar arquivo";
$MESS["BPDRMV_PD_LABEL_DISK_CHOOSE_FOLDER"] = "Selecionar pasta";
$MESS["BPDRMV_PD_LABEL_DISK_EMPTY"] = "Nenhuma seleção";
$MESS["BPDRMV_PD_DELETED_BY"] = "Excluir como";
?>