<?
$MESS["BPDRMV_PD_SOURCE_ID"] = "Objet source";
$MESS["BPDRMV_PD_SOURCE_ID_DESCR"] = "Fichier ou dossier du lecteur";
$MESS["BPDRMV_PD_LABEL_DISK_CHOOSE_FILE"] = "Choisir un fichier";
$MESS["BPDRMV_PD_LABEL_DISK_CHOOSE_FOLDER"] = "Sélectionnez un dossier";
$MESS["BPDRMV_PD_LABEL_DISK_EMPTY"] = "Aucune sélection";
$MESS["BPDRMV_PD_DELETED_BY"] = "Supprimer en tant que";
?>