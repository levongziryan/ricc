<?
$MESS["BPDRMV_ACCESS_DENIED"] = "Accès refusé à tout le monde sauf les administrateurs du portail.";
$MESS["BPDRMV_EMPTY_SOURCE_ID"] = "L'objet source n'a pas été spécifié.";
$MESS["BPDRMV_SOURCE_ERROR"] = "L'objet source n'a pas été trouvé.";
$MESS["BPDRMV_REMOVE_ERROR"] = "Impossible de supprimer l'objet.";
?>