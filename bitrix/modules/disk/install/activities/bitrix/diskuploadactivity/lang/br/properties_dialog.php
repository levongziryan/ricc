<?
$MESS["BPDUA_PD_ENTITY"] = "Local de destino";
$MESS["BPDUA_PD_ENTITY_TYPE_USER"] = "Drive do Usuário";
$MESS["BPDUA_PD_ENTITY_TYPE_SG"] = "Drive do grupo de rede social";
$MESS["BPDUA_PD_ENTITY_TYPE_COMMON"] = "Drive Público";
$MESS["BPDUA_PD_ENTITY_TYPE_FOLDER"] = "Pasta Driver";
$MESS["BPDUA_PD_SOURCE_FILE"] = "Arquivo a carregar";
$MESS["BPDUA_PD_ENTITY_ID_USER"] = "Usuário";
$MESS["BPDUA_PD_ENTITY_ID_SG"] = "Grupo";
$MESS["BPDUA_PD_ENTITY_ID_COMMON"] = "Drive";
$MESS["BPDUA_PD_ENTITY_ID_FOLDER"] = "Pasta";
$MESS["BPDUA_PD_LABEL_CHOOSE"] = "Selecionar:";
$MESS["BPDUA_PD_LABEL_DISK_CHOOSE"] = "Selecionar pasta";
$MESS["BPDUA_PD_LABEL_DISK_EMPTY"] = "Nenhuma seleção";
$MESS["BPDUA_PD_CREATED_BY"] = "Carregar como";
?>