<?
$MESS["BPDUA_ACCESS_DENIED"] = "Acceso denegado excepto a los administradores del portal.";
$MESS["BPDUA_EMPTY_ENTITY_TYPE"] = "Tipo de almacenamiento no está especificado.";
$MESS["BPDUA_EMPTY_ENTITY_ID"] = "Almacenamiento no está especificado.";
$MESS["BPDUA_EMPTY_SOURCE_FILE"] = "No se ha seleccionado ningún archivo para cargar.";
$MESS["BPDUA_TARGET_ERROR"] = "No se pudo resolver la ubicación de carga.";
$MESS["BPDUA_SOURCE_ERROR"] = "Error al obtener el archivo a cargar.";
$MESS["BPDUA_UPLOAD_ERROR"] = "Error al cargar el archivo.";
?>