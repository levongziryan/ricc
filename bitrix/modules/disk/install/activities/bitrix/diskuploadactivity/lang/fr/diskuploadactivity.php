<?
$MESS["BPDUA_ACCESS_DENIED"] = "Accès refusé à tout le monde sauf les administrateurs du portail.";
$MESS["BPDUA_EMPTY_ENTITY_TYPE"] = "Le type de stockage n'a pas été spécifié.";
$MESS["BPDUA_EMPTY_ENTITY_ID"] = "Le stockage n'est pas précisé.";
$MESS["BPDUA_EMPTY_SOURCE_FILE"] = "Le fichier à charger n'est pas indiqué.";
$MESS["BPDUA_TARGET_ERROR"] = "Impossible de résoudre l'emplacement de téléchargement.";
$MESS["BPDUA_SOURCE_ERROR"] = "Erreur lors de la récupération du fichier à télécharger.";
$MESS["BPDUA_UPLOAD_ERROR"] = "Erreur de téléchargement du fichier.";
?>