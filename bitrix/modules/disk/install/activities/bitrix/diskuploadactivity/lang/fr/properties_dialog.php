<?
$MESS["BPDUA_PD_ENTITY"] = "Emplacement de destination";
$MESS["BPDUA_PD_ENTITY_TYPE_USER"] = "Lecteur utilisateur";
$MESS["BPDUA_PD_ENTITY_TYPE_SG"] = "Drive de groupe de réseau social";
$MESS["BPDUA_PD_ENTITY_TYPE_COMMON"] = "Drive public";
$MESS["BPDUA_PD_ENTITY_TYPE_FOLDER"] = "Dossier du lecteur";
$MESS["BPDUA_PD_SOURCE_FILE"] = "Fichier à télécharger";
$MESS["BPDUA_PD_ENTITY_ID_USER"] = "Utilisateur";
$MESS["BPDUA_PD_ENTITY_ID_SG"] = "Groupe";
$MESS["BPDUA_PD_ENTITY_ID_COMMON"] = "Drive";
$MESS["BPDUA_PD_ENTITY_ID_FOLDER"] = "Dossier";
$MESS["BPDUA_PD_LABEL_CHOOSE"] = "Sélectionnez :";
$MESS["BPDUA_PD_LABEL_DISK_CHOOSE"] = "Sélectionnez un dossier";
$MESS["BPDUA_PD_LABEL_DISK_EMPTY"] = "Aucune sélection";
$MESS["BPDUA_PD_CREATED_BY"] = "Télécharger en tant que";
?>