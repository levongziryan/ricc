<?
$MESS["BPDUA_PD_ENTITY"] = "Ubicación de destino";
$MESS["BPDUA_PD_ENTITY_TYPE_USER"] = "Drive del usuario";
$MESS["BPDUA_PD_ENTITY_TYPE_SG"] = "Red social del drive del grupo";
$MESS["BPDUA_PD_ENTITY_TYPE_COMMON"] = "Drive pública";
$MESS["BPDUA_PD_ENTITY_TYPE_FOLDER"] = "Carpeta del drive";
$MESS["BPDUA_PD_SOURCE_FILE"] = "Archivo para cargar";
$MESS["BPDUA_PD_ENTITY_ID_USER"] = "Usuario";
$MESS["BPDUA_PD_ENTITY_ID_SG"] = "Grupo";
$MESS["BPDUA_PD_ENTITY_ID_COMMON"] = "Drive";
$MESS["BPDUA_PD_ENTITY_ID_FOLDER"] = "Carpeta";
$MESS["BPDUA_PD_LABEL_CHOOSE"] = "Seleccionar:";
$MESS["BPDUA_PD_LABEL_DISK_CHOOSE"] = "Seleccionar carpeta";
$MESS["BPDUA_PD_LABEL_DISK_EMPTY"] = "No se ha seleccionado";
$MESS["BPDUA_PD_CREATED_BY"] = "Cargar como";
?>