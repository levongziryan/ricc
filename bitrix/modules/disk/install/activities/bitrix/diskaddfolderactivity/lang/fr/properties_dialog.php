<?
$MESS["BPDAF_PD_ENTITY"] = "Créer dans";
$MESS["BPDAF_PD_ENTITY_TYPE_USER"] = "Lecteur utilisateur";
$MESS["BPDAF_PD_ENTITY_TYPE_SG"] = "Drive de groupe de réseau social";
$MESS["BPDAF_PD_ENTITY_TYPE_COMMON"] = "Drive public";
$MESS["BPDAF_PD_ENTITY_TYPE_FOLDER"] = "Dossier du Drive";
$MESS["BPDAF_PD_FOLDER_NAME"] = "Nom du dossier";
$MESS["BPDAF_PD_ENTITY_ID_USER"] = "Utilisateur";
$MESS["BPDAF_PD_ENTITY_ID_SG"] = "Groupe";
$MESS["BPDAF_PD_ENTITY_ID_COMMON"] = "Drive";
$MESS["BPDAF_PD_ENTITY_ID_FOLDER"] = "Dossier";
$MESS["BPDAF_PD_LABEL_CHOOSE"] = "Sélectionnez :";
$MESS["BPDAF_PD_LABEL_DISK_CHOOSE"] = "Sélectionnez un dossier";
$MESS["BPDAF_PD_LABEL_DISK_EMPTY"] = "Aucun dossier sélectionné";
$MESS["BPDAF_PD_FOLDER_AUTHOR"] = "Créé par";
?>