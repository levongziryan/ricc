<?
$MESS["BPDAF_ACCESS_DENIED"] = "Accès refusé à tout le monde sauf les administrateurs du portail.";
$MESS["BPDAF_EMPTY_ENTITY_TYPE"] = "Le type de stockage est manquant.";
$MESS["BPDAF_EMPTY_ENTITY_ID"] = "Le stockage n'est pas précisé.";
$MESS["BPDAF_EMPTY_FOLDER_NAME"] = "Le nom du dossier n'est pas indiqué.";
$MESS["BPDAF_TARGET_ERROR"] = "Impossible de résoudre l'emplacement du nouveau dossier.";
$MESS["BPDAF_ADD_FOLDER_ERROR"] = "Impossible de créer un nouveau dossier.";
?>