<?
$MESS["BPDAF_ACCESS_DENIED"] = "Acesso negado a qualquer pessoa, exceto os administradores do portal.";
$MESS["BPDAF_EMPTY_ENTITY_TYPE"] = "O tipo de armazenamento está faltando.";
$MESS["BPDAF_EMPTY_ENTITY_ID"] = "O armazenamento não está especificado.";
$MESS["BPDAF_EMPTY_FOLDER_NAME"] = "O nome da pasta não está especificado.";
$MESS["BPDAF_TARGET_ERROR"] = "Não foi possível resolver nova localização da pasta.";
$MESS["BPDAF_ADD_FOLDER_ERROR"] = "Não foi possível criar uma nova pasta.";
?>