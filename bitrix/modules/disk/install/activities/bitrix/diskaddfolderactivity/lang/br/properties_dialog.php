<?
$MESS["BPDAF_PD_ENTITY"] = "Criar em";
$MESS["BPDAF_PD_ENTITY_TYPE_USER"] = "Drive do Usuário";
$MESS["BPDAF_PD_ENTITY_TYPE_SG"] = "Drive do grupo de rede social";
$MESS["BPDAF_PD_ENTITY_TYPE_COMMON"] = "Drive Público";
$MESS["BPDAF_PD_ENTITY_TYPE_FOLDER"] = "Pasta do Drive";
$MESS["BPDAF_PD_FOLDER_NAME"] = "Nome da pasta";
$MESS["BPDAF_PD_ENTITY_ID_USER"] = "Usuário";
$MESS["BPDAF_PD_ENTITY_ID_SG"] = "Grupo";
$MESS["BPDAF_PD_ENTITY_ID_COMMON"] = "Drive";
$MESS["BPDAF_PD_ENTITY_ID_FOLDER"] = "Pasta";
$MESS["BPDAF_PD_LABEL_CHOOSE"] = "Selecionar:";
$MESS["BPDAF_PD_LABEL_DISK_CHOOSE"] = "Selecionar pasta";
$MESS["BPDAF_PD_LABEL_DISK_EMPTY"] = "Nenhuma pasta selecionada";
$MESS["BPDAF_PD_FOLDER_AUTHOR"] = "Criado por";
?>