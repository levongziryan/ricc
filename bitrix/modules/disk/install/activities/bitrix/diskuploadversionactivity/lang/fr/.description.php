<?
$MESS["BPDUV_DESCR_DESCR2"] = "Télécharger une nouvelle version sur le Drive";
$MESS["BPDUV_DESCR_NAME2"] = "Télécharger une nouvelle version sur le Drive";
$MESS["BPDUV_DESCR_OBJECT_ID"] = "ID de la nouvelle version";
$MESS["BPDUV_DESCR_DETAIL_URL"] = "Afficher l'URL";
$MESS["BPDUV_DESCR_DOWNLOAD_URL"] = "Télécharger l'URL";
$MESS["BPDUV_DESCR_CATEGORY"] = "Drive";
?>