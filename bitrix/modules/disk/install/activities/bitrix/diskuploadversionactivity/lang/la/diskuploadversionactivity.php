<?
$MESS["BPDUV_ACCESS_DENIED"] = "Acceso denegado excepto a los administradores del portal.";
$MESS["BPDUV_EMPTY_SOURCE_FILE"] = "No se ha seleccionado ningún archivo para carga.";
$MESS["BPDUV_EMPTY_SOURCE_ID"] = "No se especifica el archivo de origen.";
$MESS["BPDUV_SOURCE_ID_ERROR"] = "No se ha encontrado el archivo de origen.";
$MESS["BPDUV_SOURCE_FILE_ERROR"] = "Error al obtener el archivo a cargar.";
$MESS["BPDUV_UPLOAD_ERROR"] = "Error al cargar la nueva versión.";
?>