<?
$MESS["DISK_FOLDER_TOOLBAR_UPLOAD_FILE_TEXT"] = "Cargar";
$MESS["DISK_FOLDER_TOOLBAR_UPLOAD_FILE_TITLE"] = "Cargar nuevos documentos en esta carpeta";
$MESS["DISK_FOLDER_TOOLBAR_CREATE_DOC_TEXT"] = "Nuevos documentos";
$MESS["DISK_FOLDER_TOOLBAR_CREATE_DOC_TITLE"] = "Crear nuevos documentos";
$MESS["DISK_FOLDER_TOOLBAR_CREATE_FOLDER_TEXT"] = "Crear carpeta";
$MESS["DISK_FOLDER_TOOLBAR_CREATE_FOLDER_TITLE"] = "Crea una carpeta secundaria";
$MESS["DISK_FOLDER_TOOLBAR_EMPTY_TRASHCAN_TEXT"] = "Vaciar la Papelera de Reciclaje";
$MESS["DISK_FOLDER_TOOLBAR_EMPTY_TRASHCAN_TITLE"] = "Eliminar todos los archivos y carpetas de la Papelera de Reciclaje para siempre";
$MESS["DISK_FOLDER_TOOLBAR_EXTERNAL_LINK_LIST_TEXT"] = "Publicado";
$MESS["DISK_FOLDER_TOOLBAR_EXTERNAL_LINK_LIST_TITLE"] = "Mostrar todos los archivos que tienen vínculos públicos";
$MESS["DISK_FOLDER_TOOLBAR_TRASHCAN_TEXT"] = "Eliminar";
$MESS["DISK_FOLDER_TOOLBAR_TRASHCAN_TITLE"] = "Mostrar archivos eliminados y carpetas";
$MESS["DISK_FOLDER_TOOLBAR_FOLDER_LIST_TEXT"] = "Todo";
$MESS["DISK_FOLDER_TOOLBAR_FOLDER_LIST_TITLE"] = "Mostrar todos los archivos y carpetas";
$MESS["DISK_FOLDER_TOOLBAR_ERROR_COULD_NOT_FIND_OBJECT"] = "No se pudo encontrar el objeto.";
$MESS["DISK_FOLDER_TOOLBAR_EXTERNAL_LINK_LIST_GO_BACK_TEXT"] = "Volver";
$MESS["DISK_FOLDER_TOOLBAR_EXTERNAL_LINK_LIST_GO_BACK_TITLE"] = "Volver a los archivos y carpetas";
$MESS["DISK_FOLDER_TOOLBAR_TRASHCAN_TEXT_2"] = "Papelera de Reciclaje";
$MESS["DISK_FOLDER_TOOLBAR_EXTERNAL_LINK_LIST_TEXT_2"] = "Archivos públicos";
?>