<?
$MESS["DISK_FILE_UPLOAD_ERROR_COULD_NOT_FIND_FOLDER"] = "No se pudo encontrar la carpeta";
$MESS["DISK_FILE_UPLOAD_ERROR_BAD_RIGHTS"] = "Permisos insuficiente para agregar un archivo.";
$MESS["DISK_FILE_UPLOAD_ERROR_BAD_RIGHTS2"] = "Permisos insuficiente para agregar la actualizaciín de una versión.";
$MESS["DISK_FILE_UPLOAD_ERROR_COULD_NOT_FIND_FILE"] = "No se pudo encontrar el archivo.";
$MESS["DISK_FILE_UPLOAD_ERROR_COULD_UPLOAD_VERSION"] = "No se pudo actualizar el archivo.";
?>