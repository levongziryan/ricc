<?
$MESS["DISK_FILE_UPLOAD_ERROR_COULD_NOT_FIND_FOLDER"] = "A pasta não pôde ser encontrada.";
$MESS["DISK_FILE_UPLOAD_ERROR_BAD_RIGHTS"] = "Permissão insuficiente para adicionar um arquivo.";
$MESS["DISK_FILE_UPLOAD_ERROR_BAD_RIGHTS2"] = "Permissão insuficiente para adicionar versão atualizada do arquivo.";
$MESS["DISK_FILE_UPLOAD_ERROR_COULD_NOT_FIND_FILE"] = "O arquivo não pôde ser encontrado.";
$MESS["DISK_FILE_UPLOAD_ERROR_COULD_UPLOAD_VERSION"] = "Não foi possível atualizar o arquivo.";
?>