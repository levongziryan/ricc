<?
$MESS["DISK_EXT_LINK_TITLE"] = "Télécharger le document";
$MESS["DISK_EXT_LINK_B24"] = "Bitrix<span>24</span>";
$MESS["DISK_EXT_LINK_FILE_SIZE"] = "Taille";
$MESS["DISK_EXT_LINK_FILE_UPDATE_TIME"] = "Date de modification";
$MESS["DISK_EXT_LINK_FILE_DOWNLOAD"] = "Télécharger";
$MESS["DISK_EXT_LINK_FILE_COPY_LINK"] = "Partager le lien";
$MESS["DISK_EXT_LINK_B24_ADV_TEXT"] = "Espace multifonctionnel unifié";
$MESS["DISK_EXT_LINK_B24_ADV_1"] = "Tâches & Projets";
$MESS["DISK_EXT_LINK_B24_ADV_2"] = "CRM";
$MESS["DISK_EXT_LINK_B24_ADV_3"] = "Chat collaboratif";
$MESS["DISK_EXT_LINK_B24_ADV_4"] = "Documents";
$MESS["DISK_EXT_LINK_B24_ADV_5"] = "Drive de groupe";
$MESS["DISK_EXT_LINK_B24_ADV_6"] = "Calendrier";
$MESS["DISK_EXT_LINK_B24_ADV_CREATE_LINK_TEXT"] = "Créez votre compte Bitrix24";
$MESS["DISK_EXT_LINK_B24_ADV_CREATE_LINK_HREF"] = "http://www.bitrix24.com/?utm_source=fileshare_button&utm_medium=referral&utm_campaign=fileshare_button";
$MESS["DISK_EXT_LINK_PROTECT_BY_PASSWORD"] = "Ce fichier est protégé par un mot de passe.";
$MESS["DISK_EXT_LINK_PROTECT_BY_PASSWORD_DESCR"] = "Veuillez saisir votre mot de passe dans le champ ci-dessous.";
$MESS["DISK_EXT_LINK_LABEL_PASSWORD"] = "Mot de passe";
$MESS["DISK_EXT_LINK_LABEL_BTN"] = "continuer";
$MESS["DISK_EXT_LINK_PROTECT_BY_WRONG_PASSWORD"] = "Le mot de passe est incorrect";
$MESS["DISK_EXT_LINK_OPEN_GRAPH_MADE_BY_B24"] = "Le partage de fichier est développé par Bitrix24";
$MESS["DISK_EXT_LINK_FOLDER_DOWNLOAD"] = "Télécharger en tant qu'archive";
$MESS["DISK_EXT_LINK_FOLDER_PROTECT_BY_PASSWORD"] = "The dossier est protégé par un mot de passe.";
$MESS["DISK_EXT_LINK_OPEN_FOLDER_GRAPH_MADE_BY_B24"] = "Le dossier est partagé via Bitrix24";
$MESS["DISK_LABEL_GRID_TOTAL"] = "Total";
?>