<?
$MESS["DISK_EXT_LINK_INVALID"] = "El link no es válido.";
$MESS["DISK_EXT_LINK_TITLE"] = "Archivo no encontrado";
$MESS["DISK_EXT_LINK_TEXT"] = "Archivo no encontrado";
$MESS["DISK_EXT_LINK_B24"] = "Bitrix<span>24</span>";
$MESS["DISK_EXT_LINK_DESCRIPTION"] = "The file might have been unpublished, or you clicked an invalid link. <br/>Please contact the file owner.";
$MESS["DISK_EXT_LINK_B24_ADV_TEXT"] = "Suit unificada de colaboración";
$MESS["DISK_EXT_LINK_B24_ADV_1"] = "Tareas y proyectos";
$MESS["DISK_EXT_LINK_B24_ADV_2"] = "CRM";
$MESS["DISK_EXT_LINK_B24_ADV_3"] = "Mensajería";
$MESS["DISK_EXT_LINK_B24_ADV_4"] = "Documentos";
$MESS["DISK_EXT_LINK_B24_ADV_5"] = "Drive de proyectos";
$MESS["DISK_EXT_LINK_B24_ADV_6"] = "Calendario";
$MESS["DISK_EXT_LINK_B24_ADV_CREATE_LINK_TEXT"] = "Cree su cuenta Bitrix24 ahora";
$MESS["DISK_EXT_LINK_B24_ADV_CREATE_LINK_HREF"] = "http://www.bitrix24.es/?utm_source=fileshare_button&utm_medium=referral&utm_campaign=fileshare_button";
?>