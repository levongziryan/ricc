<?
$MESS["DISK_EXT_LINK_TITLE"] = "Descargar archivo";
$MESS["DISK_EXT_LINK_B24"] = "Bitrix<span>24</span>";
$MESS["DISK_EXT_LINK_FILE_SIZE"] = "Tamaño";
$MESS["DISK_EXT_LINK_FILE_UPDATE_TIME"] = "Modificado el";
$MESS["DISK_EXT_LINK_FILE_DOWNLOAD"] = "Descargar";
$MESS["DISK_EXT_LINK_FILE_COPY_LINK"] = "Compartir link";
$MESS["DISK_EXT_LINK_B24_ADV_TEXT"] = "Suit unificada de colaboración";
$MESS["DISK_EXT_LINK_B24_ADV_1"] = "Tareas y Proyectos";
$MESS["DISK_EXT_LINK_B24_ADV_2"] = "CRM";
$MESS["DISK_EXT_LINK_B24_ADV_3"] = "Mensajería";
$MESS["DISK_EXT_LINK_B24_ADV_4"] = "Documentos";
$MESS["DISK_EXT_LINK_B24_ADV_5"] = "Drive grupal";
$MESS["DISK_EXT_LINK_B24_ADV_6"] = "Calendario";
$MESS["DISK_EXT_LINK_B24_ADV_CREATE_LINK_TEXT"] = "Cree su cuenta Bitrix24 ahora";
$MESS["DISK_EXT_LINK_B24_ADV_CREATE_LINK_HREF"] = "http://www.bitrix24.es/?utm_source=fileshare_button&utm_medium=referral&utm_campaign=fileshare_button";
$MESS["DISK_EXT_LINK_PROTECT_BY_PASSWORD"] = "El archivo está protegidfo por una contraseña";
$MESS["DISK_EXT_LINK_PROTECT_BY_PASSWORD_DESCR"] = "Ingrese la contraseña en el campo de abajo";
$MESS["DISK_EXT_LINK_LABEL_PASSWORD"] = "Contraseña";
$MESS["DISK_EXT_LINK_LABEL_BTN"] = "proceder";
$MESS["DISK_EXT_LINK_PROTECT_BY_WRONG_PASSWORD"] = "Contraseña es incorrecta";
$MESS["DISK_EXT_LINK_OPEN_GRAPH_MADE_BY_B24"] = "Compartir archivos impulsado por Bitrix24";
$MESS["DISK_EXT_LINK_FOLDER_DOWNLOAD"] = "Descargar como archivo";
$MESS["DISK_EXT_LINK_FOLDER_PROTECT_BY_PASSWORD"] = "Esta carpeta está protegida con contraseña.";
$MESS["DISK_EXT_LINK_OPEN_FOLDER_GRAPH_MADE_BY_B24"] = "La carpeta se comparte con Bitrix24";
$MESS["DISK_LABEL_GRID_TOTAL"] = "Total";
?>