<?
$MESS["DISK_EXT_LINK_TITLE"] = "Baixar arquivo";
$MESS["DISK_EXT_LINK_B24"] = "Bitrix<span>24</span>";
$MESS["DISK_EXT_LINK_FILE_SIZE"] = "Tamanho";
$MESS["DISK_EXT_LINK_FILE_UPDATE_TIME"] = "Modificado em";
$MESS["DISK_EXT_LINK_FILE_DOWNLOAD"] = "Baixar";
$MESS["DISK_EXT_LINK_FILE_COPY_LINK"] = "Compartilhar link";
$MESS["DISK_EXT_LINK_B24_ADV_TEXT"] = "Conjunto de aplicações de colaboração unificado";
$MESS["DISK_EXT_LINK_B24_ADV_1"] = "Tarefas e projetos";
$MESS["DISK_EXT_LINK_B24_ADV_2"] = "CRM";
$MESS["DISK_EXT_LINK_B24_ADV_3"] = "IM&Bate-papo do grupo";
$MESS["DISK_EXT_LINK_B24_ADV_4"] = "Documentos";
$MESS["DISK_EXT_LINK_B24_ADV_5"] = "Unidade do Grupo";
$MESS["DISK_EXT_LINK_B24_ADV_6"] = "Calendário";
$MESS["DISK_EXT_LINK_B24_ADV_CREATE_LINK_TEXT"] = "Crie sua conta Bitrix24 agora";
$MESS["DISK_EXT_LINK_PROTECT_BY_PASSWORD"] = "Este arquivo está protegido por senha.";
$MESS["DISK_EXT_LINK_PROTECT_BY_PASSWORD_DESCR"] = "Digite a senha no campo abaixo.";
$MESS["DISK_EXT_LINK_LABEL_PASSWORD"] = "Senha";
$MESS["DISK_EXT_LINK_LABEL_BTN"] = "continuar";
?>