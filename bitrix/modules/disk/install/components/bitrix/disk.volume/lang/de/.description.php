<?
$MESS["DISK_SERVICE"] = "Drive";
$MESS["DISK_VOLUME_NAME"] = "Genutzten Speicherplatz berechnen";
$MESS["DISK_VOLUME_DESC"] = "Berechnet den genutzten Speicherplatz und stellt ein Tool zum Bereinigen des Drives bereit, falls erforderlich.";
?>