<?
$MESS["DISK_VOLUME_FILE_DELETE_OK"] = "El archivo ha sido eliminado";
$MESS["DISK_VOLUME_GROUP_FILE_DELETE_OK"] = "Los archivos han sido eliminados";
$MESS["DISK_VOLUME_FILE_VERSION_DELETE_OK"] = "Las versiones de los archivos no utilizados se han eliminado";
$MESS["DISK_VOLUME_GROUP_FILE_VERSION_DELETE_OK"] = "Las versiones de archivos no utilizados se han eliminado";
$MESS["DISK_VOLUME_UNNECESSARY_VERSION_DELETE_OK"] = "Las versiones no utilizadas se han eliminado";
$MESS["DISK_VOLUME_FOLDER_DELETE_OK"] = "La carpeta ha sido eliminada";
$MESS["DISK_VOLUME_FOLDER_EMPTY_OK"] = "Los contenidos de la carpeta han sido eliminados";
$MESS["DISK_VOLUME_NOTIFY_STORAGE_STORAGE"] = "¡Advertencia! Sus archivos en el drive <a href=\"#URL#\">\"#TITLE#\"</a> están ocupando mucho espacio: #FILE_SIZE#. ";
$MESS["DISK_VOLUME_NOTIFY_STORAGE_USER"] = "¡Advertencia! Sus archivos en <a href=\"#URL#\">drive</a> están ocupando mucho espacio: #FILE_SIZE#.";
$MESS["DISK_VOLUME_NOTIFY_STORAGE_GROUP"] = "¡Advertencia! Sus archivos en #TITLE# workgroup <a href=\"#URL#\">drive</a> están ocupando mucho espacio: #FILE_SIZE#.";
$MESS["DISK_VOLUME_NOTIFY_STORAGE_TRASHCAN"] = "¡Advertencia! Sus archivos en <a href=\"#URL#\">Papelera de Reciclaje</a> están ocupando mucho espacio: #FILE_SIZE#.";
$MESS["DISK_VOLUME_NOTIFY_STORAGE_UPLOADED"] = "¡Advertencia! Sus archivos en <a href=\"#URL#\">Archivos Cargados</a> carpeta están ocupando mucho espacio: #FILE_SIZE#.";
$MESS["DISK_VOLUME_NOTIFY_FOLDER"] = "¡Advertencia! Sus archivos en <a href=\"#URL#\">\"#TITLE#\"</a> carpeta están ocupando mucho espacio: #FILE_SIZE#.";
$MESS["DISK_VOLUME_NOTIFY_RECOMMENDATION"] = "Consejo: mover archivos a otro almacenamiento; <a href=\"#URL_TRASHCAN#\">vacía la Papelera de Reciclaje</a>; <a href=\"#URL_CLEAR#\">eliminar archivos no utilizados</a>. El administrador puede limpiar su almacenamiento a su propia discreción si no libera espacio pronto.";
$MESS["DISK_VOLUME_NOTIFICATION_SEND_OK"] = "Advertencia ha sido enviada";
$MESS["DISK_VOLUME_DATA_DELETED"] = "Los datos han sido eliminados";
$MESS["DISK_VOLUME_DATA_DELETED_QUEUE"] = "Datos eliminados. Paso #QUEUE_STEP#/#QUEUE_LENGTH#";
$MESS["DISK_VOLUME_PERFORMING_QUEUE"] = "Recopilación de datos. Paso #QUEUE_STEP# of #QUEUE_LENGTH#";
$MESS["DISK_VOLUME_ERROR_BAD_RIGHTS_FILE"] = "Permisos de acceso insuficientes a archivos.";
$MESS["DISK_VOLUME_ERROR_BAD_RIGHTS_FOLDER"] = "Permisos de acceso insuficientes a carpetas.";
?>