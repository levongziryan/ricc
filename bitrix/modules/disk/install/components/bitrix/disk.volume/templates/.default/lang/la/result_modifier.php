<?
$MESS["DISK_VOLUME_PAGE_TITLE"] = "Limpieza del Drive";
$MESS["DISK_VOLUME_MORE"] = "Detalles";
$MESS["DISK_VOLUME_OPEN"] = "Abrir";
$MESS["DISK_VOLUME_USING_COUNT_NONE"] = "Ninguna";
$MESS["DISK_VOLUME_USING_COUNT_END1"] = "lugar";
$MESS["DISK_VOLUME_USING_COUNT_END2"] = "lugares";
$MESS["DISK_VOLUME_USING_COUNT_END3"] = "lugares";
$MESS["DISK_VOLUME_EXTERNAL_LINK_COUNT"] = "Referencias externas";
$MESS["DISK_VOLUME_SHARING_COUNT"] = "Archivo compartido";
$MESS["DISK_VOLUME_PARENT_FOLDER"] = "Carpeta principal";
$MESS["DISK_VOLUME_IM_AUTHOR"] = "Creado por";
$MESS["DISK_VOLUME_IM_PRIVATE"] = "Chat de persona a persona";
$MESS["DISK_VOLUME_IM_CALL"] = "Llamada telefónica";
$MESS["DISK_VOLUME_IM_LINES"] = "Canal Abierto";
$MESS["DISK_VOLUME_IM_LIVECHAT"] = "Canal Abierto";
$MESS["DISK_VOLUME_IM_OPEN"] = "Chat público";
$MESS["DISK_VOLUME_IM_CHAT"] = "Grupo de chat";
$MESS["DISK_VOLUME_IM_CHAT_2"] = "Chat privado";
$MESS["DISK_VOLUME_USING_CHAT"] = "Mensaje de chat";
$MESS["DISK_VOLUME_ROOT_FILES"] = "Archivos en la carpeta root";
?>