<?
$MESS["DISK_FILE_VIEW_ERROR_COULD_NOT_FIND_OBJECT"] = "No se pudo encontrar el objeto.";
$MESS["DISK_FILE_VIEW_ERROR_COULD_NOT_CREATE_FIND_EXT_LINK"] = "No se pudo crear o encontrar un enlace público al objeto. ";
$MESS["DISK_FILE_VIEW_ERROR_COULD_NOT_FIND_VERSION"] = "No se pudo encontrar la versión.";
$MESS["DISK_FILE_VIEW_BIZPROC_LOAD"] = "No se puede incluir el módulo de Procesos de Negocio.";
?>