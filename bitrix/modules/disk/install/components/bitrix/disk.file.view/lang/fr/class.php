<?
$MESS["DISK_FILE_VIEW_ERROR_COULD_NOT_FIND_OBJECT"] = "Impossible de  trouver l'objet.";
$MESS["DISK_FILE_VIEW_ERROR_COULD_NOT_FIND_VERSION"] = "Version introuvable";
$MESS["DISK_FILE_VIEW_ERROR_COULD_NOT_SAVE_FILE"] = "Impossible de sauvegarder le fichier.";
$MESS["DISK_FILE_VIEW_GO_BACK_TEXT"] = "Précédent";
$MESS["DISK_FILE_VIEW_GO_BACK_TITLE"] = "Précédent";
$MESS["DISK_FILE_VIEW_COPY_LINK_TEXT"] = "Copier le lien";
$MESS["DISK_FILE_VIEW_COPY_LINK_TITLE"] = "Copier le lien du fichier";
$MESS["DISK_FILE_VIEW_HISTORY_ACT_DOWNLOAD"] = "Télécharger";
$MESS["DISK_FILE_VIEW_HISTORY_ACT_RESTORE"] = "Agrandir";
$MESS["DISK_FILE_VIEW_HISTORY_ACT_DELETE"] = "Supprimer";
$MESS["DISK_FILE_VIEW_VERSION_COLUMN_NAME"] = "Nom";
$MESS["DISK_FILE_VIEW_VERSION_COLUMN_FORMATTED_SIZE"] = "Taille";
$MESS["DISK_FILE_VIEW_VERSION_COLUMN_CREATE_TIME"] = "Créé le";
$MESS["DISK_FILE_VIEW_VERSION_COLUMN_CREATE_USER"] = "Créé par";
$MESS["DISK_FILE_VIEW_VERSION_COLUMN_CREATE_TIME_2"] = "Date de modification";
$MESS["DISK_FILE_VIEW_VERSION_COLUMN_CREATE_USER_2"] = "Modifié(e)s par";
$MESS["DISK_FILE_VIEW_BPABL_TYPE_1"] = "Action en cours d'exécution '#ACTIVITY#'#NOTE#";
$MESS["DISK_FILE_VIEW_BPABL_TYPE_2"] = "Action réalisée '#ACTIVITY#', état de retour: '#STATUS#', résultat: '#RESULT#'#NOTE#";
$MESS["DISK_FILE_VIEW_BPABL_TYPE_3"] = "Action annulée '#ACTIVITY#'#NOTE#";
$MESS["DISK_FILE_VIEW_BPABL_TYPE_4"] = "chec d'action '#ACTIVITY#'#NOTE#";
$MESS["DISK_FILE_VIEW_BPABL_TYPE_5"] = "Action '#ACTIVITY#'#NOTE#";
$MESS["DISK_FILE_VIEW_BPABL_TYPE_6"] = "Quelque chose a été fait avec l'action '#ACTIVITY#' #NOTE#";
$MESS["DISK_FILE_VIEW_BPABL_STATUS_1"] = "Date de l'initialisation";
$MESS["DISK_FILE_VIEW_BPABL_STATUS_2"] = "En cours";
$MESS["DISK_FILE_VIEW_BPABL_STATUS_3"] = "Annulation en cours";
$MESS["DISK_FILE_VIEW_BPABL_STATUS_4"] = "Achevé(e)s";
$MESS["DISK_FILE_VIEW_BPABL_STATUS_5"] = "Erreur";
$MESS["DISK_FILE_VIEW_BPABL_STATUS_6"] = "Indéfini";
$MESS["DISK_FILE_VIEW_BPABL_RES_1"] = "Non";
$MESS["DISK_FILE_VIEW_BPABL_RES_2"] = "Avec succès";
$MESS["DISK_FILE_VIEW_BPABL_RES_3"] = "Annulé";
$MESS["DISK_FILE_VIEW_BPABL_RES_4"] = "Erreur";
$MESS["DISK_FILE_VIEW_BPABL_RES_5"] = "Non initialisé";
$MESS["DISK_FILE_VIEW_BPABL_RES_6"] = "Indéfini";
?>