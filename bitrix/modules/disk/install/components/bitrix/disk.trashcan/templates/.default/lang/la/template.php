<?
$MESS["DISK_TRASHCAN_LABEL_GRID_TOTAL"] = "Total";
$MESS["DISK_TRASHCAN_TITLE_MODAL_MOVE_TO"] = "Mover \"#NAME#\" a la carpeta";
$MESS["DISK_TRASHCAN_TITLE_MODAL_COPY_TO"] = "Copiar \"#NAME#\" a la carpeta";
$MESS["DISK_TRASHCAN_TITLE_MODAL_MANY_COPY_TO"] = "Copiar elementos a la carpeta";
$MESS["DISK_TRASHCAN_TITLE_SIDEBAR_MANY_RESTORE_BUTTON"] = "Restaurar";
$MESS["DISK_TRASHCAN_TITLE_SIDEBAR_MANY_DELETE_BUTTON"] = "Eliminar";
$MESS["DISK_TRASHCAN_TITLE_MODAL_MOVE_TO_BUTTON"] = "Mover";
$MESS["DISK_TRASHCAN_TITLE_MODAL_COPY_TO_BUTTON"] = "Copiar";
$MESS["DISK_TRASHCAN_TITLE_GRID_TOOLBAR_COPY_BUTTON"] = "Copiar";
$MESS["DISK_TRASHCAN_TITLE_GRID_TOOLBAR_MOVE_BUTTON"] = "Mover";
$MESS["DISK_TRASHCAN_TITLE_GRID_TOOLBAR_DEST_LABEL"] = "Nivel superior";
$MESS["DISK_TRASHCAN_TITLE_SIDEBAR_INT_LINK"] = "Enlace interno";
$MESS["DISK_TRASHCAN_TITLE_SIDEBAR_EXT_LINK"] = "Enlace público";
$MESS["DISK_TRASHCAN_TITLE_SIDEBAR_EXT_LINK_ON"] = "Encendido";
$MESS["DISK_TRASHCAN_TITLE_SIDEBAR_EXT_LINK_OFF"] = "Apagado";
$MESS["DISK_TRASHCAN_SELECTED_OBJECT_1"] = "#COUNT# elemento seleccionado";
$MESS["DISK_TRASHCAN_SELECTED_OBJECT_21"] = "#COUNT# elementos seleccionados";
$MESS["DISK_TRASHCAN_SELECTED_OBJECT_2_4"] = "#COUNT# elementos seleccionados";
$MESS["DISK_TRASHCAN_SELECTED_OBJECT_5_20"] = "#COUNT# elementos seleccionados";
?>