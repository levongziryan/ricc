<?
$MESS["DISK_TRASHCAN_ERROR_COULD_NOT_RESTORE_OBJECT"] = "L'objet n'a pas pu être restauré.";
$MESS["DISK_TRASHCAN_ERROR_COULD_NOT_FIND_OBJECT"] = "Impossible de  trouver l'objet.";
$MESS["DISK_TRASHCAN_ERROR_COULD_NOT_MOVE_OBJECT"] = "Impossible de déplacer l'objet.";
$MESS["DISK_TRASHCAN_ERROR_COULD_NOT_COPY_OBJECT"] = "Impossible de copier l'objet.";
$MESS["DISK_TRASHCAN_ERROR_COULD_NOT_CREATE_FIND_EXT_LINK"] = "Impossible de créer ou trouver le lien public vers l'objet.";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FOLDER_MARK_DELETED"] = "Le dossier a été supprimé";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FOLDER_DELETED"] = "Le dossier a été supprimé";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FILE_MARK_DELETED"] = "La fichier a été supprimé";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FILE_DELETED"] = "La fichier a été supprimé";
?>