<?
$MESS["BIZPROC_WFEDIT_MENU_PARAMS_TITLE"] = "Paramètres des modèles; variables; autorun";
$MESS["BIZPROC_WFEDIT_MENU_PARAMS"] = "Paramètres de modèle";
$MESS["BIZPROC_WFEDIT_MENU_LIST"] = "Liste de modèles";
$MESS["BIZPROC_WFEDIT_MENU_LIST_TITLE"] = "Accéder à la liste de modèles";
$MESS["BIZPROC_WFEDIT_MENU_ADD_WARN"] = "Si vous avez modifié le modèle et n'avez pas sauvegardé les modifications, elles seront perdues. Continuer?";
$MESS["BIZPROC_WFEDIT_MENU_ADD_STATE"] = "Procédure d'entreprise à statut";
$MESS["BIZPROC_WFEDIT_MENU_ADD_STATE_TITLE"] = "Le processus d'affaires avec les statuts est une longue procédure d'entrepris avec le partage des droits d'accès lors du travail avec les documents dans de différents statuts";
$MESS["BIZPROC_WFEDIT_MENU_ADD_SEQ"] = "Créer un processus d'affaires consécutif";
$MESS["BIZPROC_WFEDIT_MENU_ADD_SEQ_TITLE"] = "Un processus d'affaires simple qui lance le document pour le traitement séquentiel";
$MESS["BIZPROC_WFEDIT_MENU_ADD"] = "Créer un Modèle";
$MESS["BIZPROC_WFEDIT_MENU_ADD_TITLE"] = "Ajouter un nouveau modèle";
$MESS["BIZPROC_WFEDIT_SAVE_BUTTON"] = "Sauvegarder";
$MESS["BIZPROC_WFEDIT_APPLY_BUTTON"] = "Appliquer";
$MESS["BIZPROC_WFEDIT_CANCEL_BUTTON"] = "Annuler";
$MESS["BIZPROC_WFEDIT_MENU_EXPORT"] = "Décharger";
$MESS["BIZPROC_WFEDIT_MENU_EXPORT_TITLE"] = "Exportation d'un modèle de procédure d'entreprise";
$MESS["BIZPROC_WFEDIT_MENU_IMPORT"] = "Charger";
$MESS["BIZPROC_WFEDIT_MENU_IMPORT_TITLE"] = "Importation de Business Process Template";
$MESS["BIZPROC_IMPORT_BUTTON"] = "Charger";
$MESS["BIZPROC_IMPORT_TITLE"] = "Importation d'un modèle";
$MESS["BIZPROC_IMPORT_FILE"] = "Fichier";
$MESS["BIZPROC_WFEDIT_MENU_IMPORT_PROMT"] = "Le modèle actuel du processus d'affaires sera remplacé par celui qui est importé. Continuer?";
$MESS["BIZPROC_IMPORT_FILE_OLD_TEMPLATE"] = "Ancien modèle";
$MESS["BIZPROC_IMPORT_FILE_OLD_TEMPLATE_P1"] = "Le nouveau module \"Drive\" est installé. Les modèles créés avant la mise-à-jour du module Lecteur peuvent ne pas fonctionner correctement.";
$MESS["BIZPROC_IMPORT_FILE_OLD_TEMPLATE_P2"] = "Si vous importez un modèle créé sur un site différent avant la parution du nouveau module Lecteur (avant #DATE#), marquez-le comme \"Ancien\" pour que les paramètres et variables des modèles soient correctement gérés par les processus internes.";
?>