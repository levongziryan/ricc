<?
$MESS["BIZPROC_WFEDIT_SAVE_ERROR"] = "Ocorreu um erro ao salvar um objeto:";
$MESS["BIZPROC_WFEDIT_DEFAULT_TITLE"] = "Modelo de Processo de Negócio";
$MESS["BIZPROC_WFEDIT_CATEGORY_CONSTR"] = "Construções";
$MESS["BIZPROC_WFEDIT_CATEGORY_DOC"] = "Processamento de Documentos";
$MESS["BIZPROC_WFEDIT_TITLE_EDIT"] = "Editar Modelo de Processo de Negócio";
$MESS["BIZPROC_WFEDIT_CATEGORY_INTER"] = "Configurações Interativas";
$MESS["BIZPROC_WFEDIT_CATEGORY_MAIN"] = "Principais";
$MESS["BIZPROC_WFEDIT_TITLE_ADD"] = "Novo Modelo de Processo de Negócio";
$MESS["BIZPROC_WFEDIT_CATEGORY_OTHER"] = "Outros";
$MESS["BIZPROC_WFEDIT_ERROR_TYPE"] = "O tipo de documento é necessário.";
$MESS["BIZPROC_WFEDIT_IMPORT_ERROR"] = "Erro ao importar o modelo de processo de negócio.";
$MESS["BIZPROC_USER_PARAMS_SAVE_ERROR"] = "Uma ou mais atividades na barra \"Minhas Atividades\" são muito grandes. As alterações não serão salvas.";
?>