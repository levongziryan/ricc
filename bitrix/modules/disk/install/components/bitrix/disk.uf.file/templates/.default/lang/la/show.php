<?
$MESS["WDUF_FILES"] = "Archivos.";
$MESS["WDUF_MORE_ACTIONS"] = "Más...";
$MESS["WDUF_FILE_EDIT"] = "Editar";
$MESS["WDUF_PHOTO"] = "Foto:";
$MESS["DISK_UF_FILE_SETTINGS_DOCS"] = "Configurar para trabajar con documentos";
$MESS["DISK_UF_FILE_RUN_FILE_IMPORT"] = "Obtener última versión";
$MESS["DISK_UF_FILE_STATUS_PROCESS_LOADING"] = "Cargando";
$MESS["DISK_UF_FILE_STATUS_SUCCESS_LOADING"] = "La versión más reciente se ha cargado correctamente.";
$MESS["DISK_UF_FILE_STATUS_HAS_LAST_VERSION"] = "Usted tiene la última versión.";
$MESS["DISK_UF_FILE_STATUS_FAIL_LOADING"] = "Error al cargar la versión más reciente.";
$MESS["DISK_UF_FILE_DISABLE_AUTO_COMMENT"] = "Desactivar comentarios automáticos";
$MESS["DISK_UF_FILE_ENABLE_AUTO_COMMENT"] = "Habilitar comentarios automáticos";
$MESS["DISK_UF_FILE_IS_DELETED"] = "(Eliminado)";
$MESS["DISK_UF_FILE_RESTORE"] = "Restaurar";
?>