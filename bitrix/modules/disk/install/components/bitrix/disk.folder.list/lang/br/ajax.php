<?
$MESS["DISK_FOLDER_ACTION_MESSAGE_FOLDER_MARK_DELETED"] = "A pasta não pôde ser encontrada.";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FILE_MARK_DELETED"] = "O arquivo foi excluído.";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FOLDER_DELETED"] = "A pasta foi excluída.";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FILE_DELETED"] = "O arquivo foi excluído";
$MESS["DISK_FOLDER_LIST_ERROR_COULD_NOT_FIND_OBJECT"] = "O objeto não pôde ser encontrado.";
$MESS["DISK_FOLDER_LIST_ERROR_COULD_NOT_COPY_OBJECT"] = "O objeto não pôde ser copiado.";
$MESS["DISK_FOLDER_LIST_ERROR_COULD_NOT_MOVE_OBJECT"] = "O objeto não pôde ser movido.";
$MESS["DISK_FOLDER_LIST_ERROR_COULD_NOT_CREATE_FIND_EXT_LINK"] = "Não é possível criar ou encontrar um link público para o objeto.";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FOLDER_DETACH"] = "Pasta desconectada";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FILE_DETACH"] = "Arquivo desconectado";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FOLDER_MARK_DELETED_2"] = "A pasta foi movida para a Lixeira";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FILE_MARK_DELETED_2"] = "O arquivo foi movido para a Lixeira";
$MESS["DISK_FOLDER_LIST_ERROR_COULD_NOT_SAVE"] = "Erro ao salvar.";
$MESS["DISK_FOLDER_LIST_ERROR_VALIDATE_BIZPROC"] = "Preencha todos os campos solicitados.";
$MESS["DISK_FOLDER_LIST_MESSAGE_CONNECT_GROUP_DISK"] = "A unidade do grupo foi conectada com sucesso à sua Bitrix24.Drive";
$MESS["DISK_FOLDER_LIST_LOCK_IS_DISABLED"] = "O bloqueio de documento está desativado. Entre em contato com o administrador do Bitrix24.";
?>