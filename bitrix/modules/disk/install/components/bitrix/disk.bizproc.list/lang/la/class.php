<?
$MESS["BPATT_TITLE"] = "Plantillas de Procesos de Negocios";
$MESS["BPATT_AE_CREATE"] = "Crear";
$MESS["BPATT_AE_DELETE"] = "Eliminar";
$MESS["BPATT_AE_NONE"] = "No";
$MESS["BPATT_NO_ENTITY"] = "No hay ninguna entidad especificada para el proceso de negocio que va a crear.";
$MESS["BPATT_NO_DOCUMENT_TYPE"] = "El tipo de documento es necesario.";
$MESS["BPATT_NO_MODULE_ID"] = "El ID del módulo es necesario.";
$MESS["BPATT_AE_EDIT"] = "Actualización";
$MESS["BPATT_NO_PERMS"] = "Usted no tiene permiso para ejecutar un proceso de negocio para este documento.";
$MESS["BPATT_DO_DELETE1_CONFIRM"] = "¿Seguro que quieres eliminar esta plantilla?";
$MESS["BPATT_DO_DELETE1"] = "Eliminar";
$MESS["BPATT_DO_N_LOAD_CREATE_TITLE"] = "No ejecute un proceso de negocio al crear un documento";
$MESS["BPATT_DO_N_LOAD_EDIT_TITLE"] = "No ejecutar un proceso de negocio al modificar un documento";
$MESS["BPATT_DO_N_LOAD_CREATE"] = "No ejecutar durante la creación";
$MESS["BPATT_DO_N_LOAD_EDIT"] = "No ejecutar durante la modificación";
$MESS["BPATT_DO_EDIT1"] = "Editar";
$MESS["BPATT_DO_LOAD_CREATE_TITLE"] = "Ejecutar un proceso de negocio al crear un documento";
$MESS["BPATT_DO_LOAD_EDIT_TITLE"] = "Ejecutar un proceso de negocio al modificar un documento";
$MESS["BPATT_DO_LOAD_CREATE"] = "Ejecutar durante la creación";
$MESS["BPATT_DO_LOAD_EDIT"] = "Ejecutar durante la modificación";
$MESS["BPATT_DO_EDIT_VARS"] = "Cambiar los valores iniciales de la variables";
$MESS["BPATT_DO_EDIT_VARS1"] = "Variables";
$MESS["BPATT_DO_OLD_TEMPLATE"] = "(Versión antigua)";
?>