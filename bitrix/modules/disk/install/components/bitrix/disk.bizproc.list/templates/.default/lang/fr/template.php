<?
$MESS["BPATT_AUTO_EXECUTE"] = "Autochargement";
$MESS["BPATT_HELP1_TEXT"] = "Le workflow axé c'est un long workflow de manière continue vos processus avec les droits d'accès partagé lors de travail avec des documents dans les statut différents.";
$MESS["BPATT_HELP2_TEXT"] = "Le workflow séquentiel c'est un workflow simple qui exécute une série des actions successives dans sur le document.";
$MESS["BPATT_NAME"] = "Nom";
$MESS["BPATT_MODIFIED"] = "Modifié";
$MESS["BPATT_USER"] = "Modifié par";
$MESS["WD_EMPTY"] = "Il n'y a pas de modèle de procédure d'entreprise. Créez un <a href=#HREF#>Business Processes</a> standard.";
$MESS["WD_EMPTY_NEW"] = "Il n'y a pas de nouveaux modèles Version Business Process.  <a href=#HREF#>Créer</a>.";
$MESS["BPATT_ALL"] = "Total";
$MESS["PROMPT_OLD_TEMPLATE"] = "La mise à jour d'entraînement récente a fait quelques-uns des modèles de processus d'affaires obsolètes. Vous pouvez toujours les utiliser. Les nouvelles formes d'édition modèle sera un peu différent, cependant. Les modèles existants sont mis en évidence.";
?>