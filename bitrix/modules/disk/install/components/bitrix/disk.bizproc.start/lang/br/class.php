<?
$MESS["BPABS_EMPTY_DOC_ID"] = "Nenhum ID de documento especificado para o qual o processo de negócio deva ser criado.";
$MESS["BPABS_TITLE"] = "Executar Processo de Negócio";
$MESS["BPABS_EMPTY_DOC_TYPE"] = "O tipo de documento é necessário.";
$MESS["BPATT_NO_MODULE_ID"] = "O módulo ID é necessário.";
$MESS["BPABS_NO_PERMS"] = "Você não tem permissão para executar um processo de negócio para este documento.";
?>