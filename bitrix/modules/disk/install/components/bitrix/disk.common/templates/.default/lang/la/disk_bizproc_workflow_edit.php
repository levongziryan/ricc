<?
$MESS["DISK_BIZPROC_STATUS_TEXT"] = "Un proceso de negocio impulsado por el Estado es un proceso de negocio continuo con la distribución de permisos de acceso para manejar documentos en varios estados.";
$MESS["DISK_BIZPROC_SERIAL_TEXT"] = "Un proceso de negocio secuencial es un proceso de negocio simple que lleva a cabo una serie de acciones consecutivas en un documento.";
$MESS["DISK_BIZPROC_STATUS_TITLE"] = "Crear un proceso de negocio estado impulsada";
$MESS["DISK_BIZPROC_SERIAL_TITLE"] = "Crear un proceso de negocio secuencial";
?>