<?
$MESS["DISK_NETWORK_DRIVE_SHAREDDRIVE_TITLE"] = "Mostrar instruções para conectar como uma unidade de rede";
$MESS["DISK_NETWORK_DRIVE_CONNECTOR_HELP_MAPDRIVE"] = "<h3>Mapeando a Biblioteca como uma Unidade de Rede</h3> 
<p>Para conectar a biblioteca como um disco de rede usando <b>gerenciador de arquivo</b>:
<ul> 
<li>Executar o Windows Explorer</b>;</li> 
<li>Selecione <i>Ferramentas > Mapear Unidade de Rede</i>. O assistente de disco de rede abrirá:
<br /> <br /><a href=\"javascript:ShowImg( \"#TEMPLATE_FOLDER#/images/en/network_storage.png',629,459, 'Map Network Drive');\"> 
<img width=\"250\" height=\"183\" border=\"0\" src=\" #TEMPLATE_FOLDER#/images/en/network_storage_sm.png\"style=\"cursor: pointer;\" alt=\"Click to Enlarge\" /></a></li> 
<li>No <b>campo</b> Unidade, especifique uma letra para mapear a pasta para;</li> 
<li>No <b>campo</b> Pasta, digite o caminho para a biblioteca: <i>http://&lt;your_server&gt;#TEMPLATE_LINK#</i>. Se você deseja que esta pasta esteja disponível quando o sistema inicia, verifique a opção <b>Reconectar-se durante o</b> logon;</li> 
<li>Clique em <b>Pronto</b>. Se for solicitado um Nome de usuário e Senha, digite seu login e senha e, em seguida, clique em <b>OK</b>.</li> 
</ul> 
</p> 
<p>Mais tarde, você pode abrir a pasta no Windows Explorer, onde a pasta será mostrada como uma unidade em Meu Computador, ou em qualquer gerenciador de arquivo.</p>";
$MESS["DISK_NETWORK_DRIVE_REGISTERPATCH"] = "As atuais preferências de segurança exigem que você <a href=\"#LINK#\">faça algumas alterações no Registro</a> a fim de conectar uma unidade de rede.";
$MESS["DISK_NETWORK_DRIVE_USECOMMANDLINE"] = "Para conectar a biblioteca como uma unidade de rede utilizando HTTPS/SSL, use <b>Iniciar > Executar > cmd</b>. Digite os seguintes comandos na linha de comando:";
$MESS["DISK_NETWORK_DRIVE_MACOS_TITLE"] = "Mapear biblioteca de documentos no Mac OS X";
$MESS["DISK_NETWORK_DRIVE_HELP_OSX"] = "<h3>Conectando a Biblioteca do Mac OS e Mac OS X</h3> 
<ul> 
<li>Selecione <i>Finder Go->o comando Conectar ao Servidor</i>;</li> 
<li>Digite no endereço da biblioteca em <b>Endereço do Servidor</b>:<p> 
<p><a href= \"javascript:ShowImg(\"#TEMPLATE_FOLDER#/images/en/macos.png',465,550,'Mac OS X');\"> 
<img width=\"235\" height=\" 278\" border=\"0\" src=\"#TEMPLATE_FOLDER#/images/en/macos_sm.png\" style=\"cursor: pointer;\" alt=\"Clique to Enlarge\"/></a></li> 
</ul>";
?>