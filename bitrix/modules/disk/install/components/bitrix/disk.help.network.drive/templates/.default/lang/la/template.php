<?
$MESS["DISK_NETWORK_DRIVE_SHAREDDRIVE_TITLE"] = "Mostrar instrucciones para conectarse como drive de red";
$MESS["DISK_NETWORK_DRIVE_CONNECTOR_HELP_MAPDRIVE"] = "<h3>Mapeo de la libreria como una unidad de red</h3>
<p>Para conectar una librería como un disco de red mediante<b>administrador de archivos</b>:
<ul>
<li>Ejecute el Explorador de Windows</b>;</li>
<li>Seleccione<i>Herramientas>Asignar unidad de red</i>. El asistente de disco de red se abrirá:
<br /><br /><a href=\"javascript:ShowImg('#TEMPLATE_FOLDER#/images/en/network_storage.png',629,459,'Map Network Drive');\">
<img width=\"250\" height=\"183\" border=\"0\" src=\"#TEMPLATE_FOLDER#/images/en/network_storage_sm.png\" style=\"cursor: pointer;\" alt=\"Click to Enlarge\" /></a></li>
<li> En el <b>Drive</b> especifique, una letra para asignar la carpeta a;</li>
<li> En el <b>Folder</b> escriba, la ruta de acceso a la biblioteca: <i>http://&lt;your_server&gt;#TEMPLATE_LINK#</i>. Si desea que esta carpeta esté disponible cuando se inicia el sistema, compruebe el <b>Conectar de nuevo al inicio de sesión</b> la opción</li>
<li>Haga clic en<b>Listo</b>. Si se le pide un nombre de usuario y contraseña, introduzca su usuario y contraseña y, a continuación, haga clic en  <b>OK</b>.</li>
</ul>
</p>
<p>Despues, puede abrir la carpeta en el Explorador de Windows en el que se mostrará la carpeta como una unidad en Mi PC o en cualquier administrador de archivos.</p>";
$MESS["DISK_NETWORK_DRIVE_REGISTERPATCH"] = "Las preferencias de seguridad actuales requieren <a href=\"#LINK#\"> hacer algún cambio de registro</a> con el fin de conectar una unidad de red.";
$MESS["DISK_NETWORK_DRIVE_USECOMMANDLINE"] = "Para conectar la biblioteca como una unidad de red utilizando HTTPS/SSL, use <b>Inicio > Ejecutar > cmd</b>. Escriba los siguientes comandos en la línea de comandos:";
$MESS["DISK_NETWORK_DRIVE_MACOS_TITLE"] = "Mapa de biblioteca de documentos en Mac OS X";
$MESS["DISK_NETWORK_DRIVE_HELP_OSX"] = "<h3>Conexión de la Biblioteca en Mac OS y Mac OS X</h3>
<ul>
<li>Seleccione<i>Ir al Buscador->Conectar al servidor de comando</i>;</li>
<li>Escriba la dirección de la biblioteca en <b>Dirección del servidor</b>:</p>
<p><a href=\"javascript:ShowImg('#TEMPLATE_FOLDER#/images/en/macos.png',465,550,'Mac OS X');\">
<img width=\"235\" height=\"278\" border=\"0\" src=\"#TEMPLATE_FOLDER#/images/en/macos_sm.png\" style=\"cursor: pointer;\" alt=\"Click to Enlarge\" /></a></li>
</ul>";
?>