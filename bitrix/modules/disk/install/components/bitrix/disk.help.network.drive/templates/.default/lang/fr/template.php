<?
$MESS["DISK_NETWORK_DRIVE_SHAREDDRIVE_TITLE"] = "Comment brancher la bibliothèque comme un disque du réseau";
$MESS["DISK_NETWORK_DRIVE_REGISTERPATCH"] = "Pour la connexion du disque de réseau avec des réglages courants de la sécurité du site il faut <a href='#LINK#'>faire des modifications dans le registre</a>.";
$MESS["DISK_NETWORK_DRIVE_USECOMMANDLINE"] = "Pour connecter la bibliothèque en tant que disque réseau par le protocole de sécurité HTTPS/SSL: exécuter l'ordre <b>Démarrage > Exécuter > cmd</b>. Dans la ligne de commande saisissez:";
$MESS["DISK_NETWORK_DRIVE_MACOS_TITLE"] = "Connexion en Mac OS X";
$MESS["DISK_NETWORK_DRIVE_CONNECTOR_HELP_MAPDRIVE"] = "<h3>Cartographie de la bibliothèque comme Network Drive</h3>
<p>Pour connecter une bibliothèque comme un disque réseau en utilisant <b> gestionnaire de fichiers</b>:
<ul>
<li>Exécuter Windows Explorer</b>;</li>
<li>Sélectionnez <i>Tools > Map Network Drive</i>. L'assistant de disque réseau sera ouvert:
<br /><br /><a href='javascript:ShowImg('#TEMPLATE_FOLDER#/images/en/network_storage.png',629,459,'Map Network Drive');'>
<img width='250' height='183' border='0' src='#TEMPLATE_FOLDER#/images/en/network_storage_sm.png' style='cursor: pointer;' alt='Cliquez pour agrandir' /></a></li>
<li>Dans le <b>Drive</b> spécifiez une lettre de cartographier le dossier;</li>
<li>Dans le <b>Dossier</b> entrez le chemin vers la bibliothèque: <i>http://&lt;your_server&gt;#TEMPLATE_LINK#</i>. Si vous voulez ce dossier soit disponible lorsque le système démarre, cochez la case <b>Se reconnecter à l'ouverture de session</b> option;</li>
<li>Cliquez sur<b>Prêt</b>. ISi vous êtes invité à entrer un nom d'utilisateur et mot de passe, entrez votre login et votre mot de passe, puis cliquez sur <b>OK</b>.</li>
</ul>
</p>
<p>Plus tard, vous pouvez ouvrir le dossier dans Windows Esplorer, où le dossier sera affiché comme un lecteur sous Poste de travail, ou en tout gestionnaire de fichiers.</p>";
$MESS["DISK_NETWORK_DRIVE_HELP_OSX"] = "<h3>Connexion à La Bibliothèque de Mac OS et Mac OS X</h3>
<ul>
<li>Sélectionnez <i>Finder Aller->Connectez-vous à commande Server</i>;</li>
<li>Tapez l'adresse de la bibliothèque dans<b>Adresse du serveur </b>:</p>
<p><a href='javascript:ShowImg('#TEMPLATE_FOLDER#/images/en/macos.png',465,550,'Mac OS X');'>
<img width='235' height='278' border='0' src='#TEMPLATE_FOLDER#/images/en/macos_sm.png' style='cursor: pointer;' alt='Cliquez pour agrandir ' /></a></li>
</ul>";
?>