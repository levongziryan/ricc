<?
$MESS["DISK_AGGREGATOR_DESCRIPTION"] = "Os links para todas as bibliotecas de documentos que um funcionário tem acesso: documentos privados; documentos pessoais de outros usuários; as páginas de Documentos; documentos dos grupos da extranet e intranet.";
$MESS["DISK_AGGREGATOR_NETWORK_DRIVE"] = "Para ter acesso a todas as bibliotecas Bitrix24 disponíveis e lidar com elas como pastas e arquivos, conecte esta página como uma unidade de rede.";
$MESS["DISK_AGGREGATOR_ND"] = "Mapear Unidade de Rede";
$MESS["DISK_AGGREGATOR_NETWORK_DRIVE_URL_TITLE"] = "Utilize este endereço para mapear uma unidade de rede";
$MESS["DISK_AGGREGATOR_TITLE_NETWORK_DRIVE"] = "Unidade de Rede";
$MESS["DISK_AGGREGATOR_TITLE_NETWORK_DRIVE_DESCR_MODAL"] = "Utilize este endereço para conectar";
$MESS["DISK_AGGREGATOR_BTN_CLOSE"] = "Fechar";
?>