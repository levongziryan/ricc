<?
$MESS["CRM_JS_BUTTON_OK"] = "Si";
$MESS["CRM_JS_BUTTON_CANCEL"] = "Cancelar";
$MESS["CRM_JS_DELETE_CONFIRM"] = "¿Está seguro de que desea eliminar este elemento?";
$MESS["CRM_JS_DELETE_CONFIRM_TITLE"] = "Eliminar elemento";
$MESS["CRM_JS_GRID_SORT"] = "Clasificado por";
$MESS["CRM_JS_GRID_FIELDS"] = "Campos visibles";
$MESS["CRM_JS_GRID_FILTER"] = "Configurar filtro";
$MESS["CRM_JS_MORE"] = "Más...";
$MESS["CRM_JS_EDIT"] = "Editar";
$MESS["CRM_JS_DELETE"] = "Eliminar";
$MESS["CRM_JS_ERROR"] = "Error";
$MESS["CRM_JS_ERROR_DELETE"] = "Se ha producido un error al eliminar el elemento.";
?>