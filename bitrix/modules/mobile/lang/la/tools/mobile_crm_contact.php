<?
$MESS["CRM_CONTACT_NOT_FOUND"] = "El contacto no fue encontrado.";
$MESS["CRM_CONTACT_ID_NOT_DEFINED"] = "El ID del contacto no fue encontrado.";
$MESS["CRM_CONTACT_ACCESS_DENIED"] = "Acceso denegado";
$MESS["CRM_CONTACT_DELETE_ERROR"] = "Se ha producido un error al eliminar un objeto.";
?>