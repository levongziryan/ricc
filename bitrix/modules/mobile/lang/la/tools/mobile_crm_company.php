<?
$MESS["CRM_COMPANY_NOT_FOUND"] = "La compañía no fue encontrada.";
$MESS["CRM_COMPANY_ID_NOT_DEFINED"] = "ID de la compañía no fue encontrada.";
$MESS["CRM_COMPANY_ACCESS_DENIED"] = "Acceso denegada";
$MESS["CRM_COMPANY_DELETE_ERROR"] = "Se ha producido un error al eliminar un objeto.";
?>