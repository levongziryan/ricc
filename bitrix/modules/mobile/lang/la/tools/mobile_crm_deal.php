<?
$MESS["CRM_DEAL_NOT_FOUND"] = "No se ha encontrado la negociación.";
$MESS["CRM_DEAL_ID_NOT_DEFINED"] = "El ID de la negociación no se ha encontrado";
$MESS["CRM_DEAL_ACCESS_DENIED"] = "Acceso denegado";
$MESS["CRM_DEAL_DELETE_ERROR"] = "Se ha producido un error al eliminar un objeto.";
$MESS["CRM_DEAL_ERROR_CHANGE_STATUS"] = "Error al cambiar el estado";
?>