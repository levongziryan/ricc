<?
$MESS["CRM_QUOTE_NOT_FOUND"] = "No se ha encontrado la cotización.";
$MESS["CRM_QUOTE_ID_NOT_DEFINED"] = "ID de la cotización no se ha encontrado";
$MESS["CRM_QUOTE_ACCESS_DENIED"] = "Acceso denegado";
$MESS["CRM_QUOTE_DELETE_ERROR"] = "Se ha producido un error al eliminar un objeto.";
$MESS["CRM_QUOTE_ERROR_CHANGE_STATUS"] = "Error al cambiar el estado";
?>