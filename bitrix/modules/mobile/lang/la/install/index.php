<?
$MESS["APP_MODULE_NAME"] = "Aplicación móvil";
$MESS["APP_MODULE_DESCRIPTION"] = "Una aplicación móvil para su uso con el portal";
$MESS["APP_INSTALL_TITLE"] = "Instalación del módulo \"Aplicaciones móviles\"";
$MESS["APP_UNINSTALL_TITLE"] = "Desinstalación del módulo de \"Aplicación móvil\"";
$MESS["APP_DENIED"] = "Acceso denegado";
?>