<?
$MESS["MUI_B24DISK"] = "Bitrix24.Drive";
$MESS["MUI_CHOOSE_PHOTO"] = "Seleccionar de la galería";
$MESS["MUI_CAMERA_ROLL"] = "Tomar foto";
$MESS["MUI_CHOOSE_FILE_TITLE"] = "Archivos";
$MESS["MUI_CANCEL"] = "Cancelar";
$MESS["MUI_COPY"] = "Copiar";
$MESS["MUI_TEXT_COPIED"] = "El texto ha sido copiado al Portapapeles";
?>