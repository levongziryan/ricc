<?
$MESS["CRM_LEAD_NOT_FOUND"] = "O cliente potencial não foi encontrado.";
$MESS["CRM_LEAD_ID_NOT_DEFINED"] = "O ID do cliente potencial não foi encontrado";
$MESS["CRM_LEAD_ACCESS_DENIED"] = "Acesso negado";
$MESS["CRM_LEAD_ERROR_CHANGE_STATUS"] = "Erro ao alterar o status";
?>