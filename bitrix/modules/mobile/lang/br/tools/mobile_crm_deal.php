<?
$MESS["CRM_DEAL_NOT_FOUND"] = "A negociação não foi encontrada.";
$MESS["CRM_DEAL_ID_NOT_DEFINED"] = "O ID da negociação não foi encontrado.";
$MESS["CRM_DEAL_ACCESS_DENIED"] = "Acesso negado";
$MESS["CRM_DEAL_DELETE_ERROR"] = "Ocorreu um erro ao excluir um objeto.";
$MESS["CRM_DEAL_ERROR_CHANGE_STATUS"] = "Erro ao alterar o status";
?>