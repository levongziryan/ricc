<?
$MESS["CRM_CONTACT_NOT_FOUND"] = "O contato não foi encontrado.";
$MESS["CRM_CONTACT_ID_NOT_DEFINED"] = "O ID do contato não foi encontrado";
$MESS["CRM_CONTACT_ACCESS_DENIED"] = "Acesso negado";
$MESS["CRM_CONTACT_DELETE_ERROR"] = "Ocorreu um erro ao excluir um objeto.";
?>