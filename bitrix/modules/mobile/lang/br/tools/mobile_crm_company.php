<?
$MESS["CRM_COMPANY_NOT_FOUND"] = "A empresa não foi encontrada.";
$MESS["CRM_COMPANY_ID_NOT_DEFINED"] = "O ID da empresa não foi encontrado";
$MESS["CRM_COMPANY_ACCESS_DENIED"] = "Acesso negado";
$MESS["CRM_COMPANY_DELETE_ERROR"] = "Ocorreu um erro ao excluir um objeto.";
?>