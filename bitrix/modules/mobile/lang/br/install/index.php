<?
$MESS["APP_MODULE_NAME"] = "Aplicação mobile";
$MESS["APP_MODULE_DESCRIPTION"] = "Uma aplicação móvel para utilização com o portal";
$MESS["APP_INSTALL_TITLE"] = "Instalação do Módulo de \"Aplicação Mobile\"";
$MESS["APP_UNINSTALL_TITLE"] = "Desinstalação do Módulo de \"Aplicação Mobile\"";
$MESS["APP_DENIED"] = "Acesso negado";
?>