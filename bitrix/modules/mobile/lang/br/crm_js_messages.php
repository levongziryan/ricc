<?
$MESS["CRM_JS_BUTTON_OK"] = "Sim";
$MESS["CRM_JS_BUTTON_CANCEL"] = "Cancelar";
$MESS["CRM_JS_DELETE_CONFIRM"] = "Você tem certeza que deseja excluir este item?";
$MESS["CRM_JS_DELETE_CONFIRM_TITLE"] = "Excluir item";
$MESS["CRM_JS_GRID_SORT"] = "Classificar por";
$MESS["CRM_JS_GRID_FIELDS"] = "Campos visíveis";
$MESS["CRM_JS_GRID_FILTER"] = "Configurar filtro";
$MESS["CRM_JS_MORE"] = "Mais...";
$MESS["CRM_JS_EDIT"] = "Editar";
$MESS["CRM_JS_DELETE"] = "Excluir";
$MESS["CRM_JS_ERROR"] = "Erro";
$MESS["CRM_JS_ERROR_DELETE"] = "Ocorreu um erro ao excluir o elemento.";
?>