<?
$MESS["MUI_B24DISK"] = "Bitrix24.Drive";
$MESS["MUI_CHOOSE_PHOTO"] = "Aus der Galerie auswählen";
$MESS["MUI_CAMERA_ROLL"] = "Foto machen";
$MESS["MUI_CHOOSE_FILE_TITLE"] = "Dateien";
$MESS["MUI_CANCEL"] = "Abbrechen";
$MESS["MUI_COPY"] = "Kopieren";
$MESS["MUI_TEXT_COPIED"] = "Text wurde in den Zwischenspeicher abgelegt";
?>