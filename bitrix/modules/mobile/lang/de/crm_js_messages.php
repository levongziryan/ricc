<?
$MESS["CRM_JS_BUTTON_OK"] = "Ja";
$MESS["CRM_JS_BUTTON_CANCEL"] = "Abbrechen";
$MESS["CRM_JS_DELETE_CONFIRM"] = "Möchten Sie dieses Element wirklich löschen?";
$MESS["CRM_JS_DELETE_CONFIRM_TITLE"] = "Element löschen";
$MESS["CRM_JS_GRID_SORT"] = "Sortieren nach";
$MESS["CRM_JS_GRID_FIELDS"] = "Sichtbare Felder";
$MESS["CRM_JS_GRID_FILTER"] = "Filter einstellen";
$MESS["CRM_JS_MORE"] = "Mehr";
$MESS["CRM_JS_EDIT"] = "Bearbeiten";
$MESS["CRM_JS_DELETE"] = "Löschen";
$MESS["CRM_JS_ERROR"] = "Fehler";
$MESS["CRM_JS_ERROR_DELETE"] = "Fehler beim Löschen des Elements";
?>