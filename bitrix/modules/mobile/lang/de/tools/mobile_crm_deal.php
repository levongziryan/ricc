<?
$MESS["CRM_DEAL_NOT_FOUND"] = "Auftrag wurde nicht gefunden.";
$MESS["CRM_DEAL_ID_NOT_DEFINED"] = "ID des Auftrags wurde nicht gefunden.";
$MESS["CRM_DEAL_ACCESS_DENIED"] = "Zugriff verweigert";
$MESS["CRM_DEAL_DELETE_ERROR"] = "Fehler beim Löschen";
$MESS["CRM_DEAL_ERROR_CHANGE_STATUS"] = "Fehler beim Ändern des Status";
?>