<?
$MESS["CRM_CONTACT_NOT_FOUND"] = "Kontakt wurde nicht gefunden.";
$MESS["CRM_CONTACT_ID_NOT_DEFINED"] = "ID des Kontakts wurde nicht gefunden.";
$MESS["CRM_CONTACT_ACCESS_DENIED"] = "Zugriff verweigert";
$MESS["CRM_CONTACT_DELETE_ERROR"] = "Fehler beim Löschen";
?>