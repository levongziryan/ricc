<?
$MESS["CRM_QUOTE_NOT_FOUND"] = "Angebot wurde nicht gefunden.";
$MESS["CRM_QUOTE_ID_NOT_DEFINED"] = "ID des Angebots wurde nicht gefunden.";
$MESS["CRM_QUOTE_ACCESS_DENIED"] = "Zugriff verweigert";
$MESS["CRM_QUOTE_DELETE_ERROR"] = "Fehler beim Löschen";
$MESS["CRM_QUOTE_ERROR_CHANGE_STATUS"] = "Fehler beim Ändern des Status";
?>