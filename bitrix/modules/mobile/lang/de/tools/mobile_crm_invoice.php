<?
$MESS["CRM_INVOICE_NOT_FOUND"] = "Rechnung wurde nicht gefunden.";
$MESS["CRM_INVOICE_ID_NOT_DEFINED"] = "ID der Rechnung wurde nicht gefunden.";
$MESS["CRM_INVOICE_ACCESS_DENIED"] = "Zugriff verweigert";
$MESS["CRM_INVOICE_ERROR_CHANGE_STATUS"] = "Fehler beim Ändern des Status";
?>