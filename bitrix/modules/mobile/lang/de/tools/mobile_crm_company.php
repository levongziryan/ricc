<?
$MESS["CRM_COMPANY_NOT_FOUND"] = "Unternehmen wurde nicht gefunden.";
$MESS["CRM_COMPANY_ID_NOT_DEFINED"] = "ID des Unternehmens wurde nicht gefunden.";
$MESS["CRM_COMPANY_ACCESS_DENIED"] = "Zugriff verweigert";
$MESS["CRM_COMPANY_DELETE_ERROR"] = "Fehler beim Löschen";
?>