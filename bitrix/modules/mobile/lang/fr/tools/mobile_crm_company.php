<?
$MESS["CRM_COMPANY_NOT_FOUND"] = "Entreprise introuvable.";
$MESS["CRM_COMPANY_ID_NOT_DEFINED"] = "ID d'entreprise introuvable";
$MESS["CRM_COMPANY_ACCESS_DENIED"] = "Accès refusé";
$MESS["CRM_COMPANY_DELETE_ERROR"] = "Une erreur s'est produite lors de la suppression d'un objet.";
?>