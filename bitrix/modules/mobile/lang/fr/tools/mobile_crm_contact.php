<?
$MESS["CRM_CONTACT_NOT_FOUND"] = "Contact introuvable.";
$MESS["CRM_CONTACT_ID_NOT_DEFINED"] = "ID de contact introuvable";
$MESS["CRM_CONTACT_ACCESS_DENIED"] = "Accès refusé";
$MESS["CRM_CONTACT_DELETE_ERROR"] = "Une erreur s'est produite lors de la suppression d'un objet.";
?>