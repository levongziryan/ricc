<?
$MESS["CRM_QUOTE_NOT_FOUND"] = "Devis introuvable.";
$MESS["CRM_QUOTE_ID_NOT_DEFINED"] = "ID de devis introuvable";
$MESS["CRM_QUOTE_ACCESS_DENIED"] = "Accès refusé";
$MESS["CRM_QUOTE_DELETE_ERROR"] = "Une erreur s'est produite lors de la suppression d'un objet.";
$MESS["CRM_QUOTE_ERROR_CHANGE_STATUS"] = "Erreur de changement du statut";
?>