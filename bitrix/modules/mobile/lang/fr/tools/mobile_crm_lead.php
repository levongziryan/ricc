<?
$MESS["CRM_LEAD_NOT_FOUND"] = "Client potentiel introuvable.";
$MESS["CRM_LEAD_ID_NOT_DEFINED"] = "ID de client potentiel introuvable";
$MESS["CRM_LEAD_ACCESS_DENIED"] = "Accès refusé";
$MESS["CRM_LEAD_ERROR_CHANGE_STATUS"] = "Erreur de changement du statut";
?>