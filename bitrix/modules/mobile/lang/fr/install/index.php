<?
$MESS["APP_UNINSTALL_TITLE"] = "Désinstallation du module 'Application mobile'";
$MESS["APP_DENIED"] = "Accès interdit";
$MESS["APP_MODULE_NAME"] = "Application mobile";
$MESS["APP_MODULE_DESCRIPTION"] = "Application mobile pour le portail";
$MESS["APP_INSTALL_TITLE"] = "Installation du module 'Application mobile'";
?>