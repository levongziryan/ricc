<?
$MESS["CRM_JS_BUTTON_OK"] = "Oui";
$MESS["CRM_JS_BUTTON_CANCEL"] = "Annuler";
$MESS["CRM_JS_DELETE_CONFIRM"] = "Voulez-vous vraiment supprimer cet objet?";
$MESS["CRM_JS_DELETE_CONFIRM_TITLE"] = "Supprimer l'objet";
$MESS["CRM_JS_GRID_SORT"] = "Trier par";
$MESS["CRM_JS_GRID_FIELDS"] = "Champs visibles";
$MESS["CRM_JS_GRID_FILTER"] = "Configurer le filtre";
$MESS["CRM_JS_MORE"] = "Plus...";
$MESS["CRM_JS_EDIT"] = "Modifier";
$MESS["CRM_JS_DELETE"] = "Supprimer";
$MESS["CRM_JS_ERROR"] = "Erreur";
$MESS["CRM_JS_ERROR_DELETE"] = "Une erreur s'est produite lors de la suppression de l'élément.";
?>