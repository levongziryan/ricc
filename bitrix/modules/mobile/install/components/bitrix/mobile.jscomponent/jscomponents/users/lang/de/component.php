<?
$MESS["LOAD_MORE_USERS"] = "Mehr laden...";
$MESS["LOAD_MORE_RESULT"] = "Mehr laden...";
$MESS["SEARCH_LOADING"] = "Suchen...";
$MESS["RECENT_SEARCH"] = "Zuletzt gesucht";
$MESS["SEARCH_EMPTY_RESULT"] = "Leider wurden keine Suchergebnisse auf Ihre Suchanfrage gefunden.";
$MESS["USER_LOADING"] = "Suchen...";
$MESS["SEARCH_PLACEHOLDER"] = "Namen oder Abteilung eingeben";
$MESS["ACTION_DELETE"] = "Löschen";
$MESS["INVITE_USERS"] = "Mitarbeiter einladen";