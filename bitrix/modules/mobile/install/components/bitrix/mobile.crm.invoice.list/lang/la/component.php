<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["M_CRM_INVOICE_LIST_PRESET_MY_UNPAID"] = "Mis facturas pendientes";
$MESS["M_CRM_INVOICE_LIST_PRESET_MY_PAID"] = "Mis facturas pagadas";
$MESS["M_CRM_INVOICE_LIST_FILTER_CUSTOM"] = "Resultados de búsqueda";
$MESS["M_CRM_INVOICE_LIST_FILTER_NONE"] = "Todas las facturas";
$MESS["M_CRM_LEAD_LIST_PRESET_USER"] = "Filtro personalizado";
$MESS["M_CRM_INVOICE_LIST_SEND"] = "Enviar";
$MESS["M_CRM_INVOICE_LIST_CHANGE_STATUS"] = "Cambiar estado";
$MESS["M_CRM_INVOICE_LIST_MORE"] = "Más...";
$MESS["M_CRM_INVOICE_LIST_EDIT"] = "Editar";
$MESS["M_CRM_INVOICE_LIST_DELETE"] = "Eliminar";
$MESS["M_CRM_INVOICE_LIST_COMPANY"] = "Enlace a la compañía";
$MESS["M_CRM_INVOICE_LIST_CONTACT"] = "Enlace al contacto";
$MESS["M_CRM_INVOICE_LIST_DEAL"] = "Enlace a la negociación";
$MESS["M_CRM_INVOICE_LIST_QUOTE"] = "Enlace a la cotización";
?>