<?
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["M_CRM_PRODUCT_LIST_PRESET_USER"] = "Filtre personnalisé";
$MESS["M_CRM_PRODUCT_LIST_FILTER_NONE"] = "Tous les produits";
$MESS["M_CRM_PRODUCT_LIST_EDIT"] = "Modifier";
$MESS["M_CRM_PRODUCT_LIST_DELETE"] = "Supprimer";
?>