<?
$MESS["CALENDAR_MODULE_IS_NOT_INSTALLED"] = "El módulo \"Event Calendar\" no está instalado.";
$MESS["EVENTS_GROUP_TODAY"] = "Hoy";
$MESS["EVENTS_GROUP_TOMORROW"] = "Mañana";
$MESS["EVENTS_GROUP_LATE"] = "Más tarde";
$MESS["MB_CAL_NO_EVENTS"] = "No hay próximos eventos";
$MESS["MB_CAL_EVENTS_COUNT"] = "Total de eventos: #COUNT#";
$MESS["MB_CAL_EVENT_ALL_DAY"] = "todo el día";
$MESS["MB_CAL_EVENT_DATE_FROM_TO"] = "Desde #DATE_FROM# hasta #DATE_TO#";
$MESS["MB_CAL_EVENT_TIME_FROM_TO_TIME"] = "desde #TIME_FROM# hasta #TIME_TO#";
$MESS["MB_CAL_EVENT_DATE_FORMAT"] = "D., M. d, Y";
$MESS["MB_CAL_EVENT_TIME_FORMAT_AMPM"] = "h:i a";
$MESS["MB_CAL_EVENT_TIME_FORMAT"] = "G:i";
?>