<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["M_CRM_COMM_SELECTOR_OWNER_NOT_FOUND"] = "No se encontró el propietario.";
$MESS["M_CRM_COMM_SELECT_SEARCH_PLACEHOLDER_PERSON"] = "Nombre";
$MESS["M_CRM_COMM_SELECT_SEARCH_PLACEHOLDER_PHONE"] = "Nombre o teléfono";
$MESS["M_CRM_COMM_SELECT_SEARCH_PLACEHOLDER_EMAIL"] = "Nombre o correo electrónico";
?>