<?
$MESS["M_CRM_COMM_SELECTOR_OWNER_NOT_FOUND"] = "Le propriétaire n'a pas été trouvé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["M_CRM_COMM_SELECT_SEARCH_PLACEHOLDER_PERSON"] = "Dénomination";
$MESS["M_CRM_COMM_SELECT_SEARCH_PLACEHOLDER_EMAIL"] = "Nom, dénomination ou e-mail";
$MESS["M_CRM_COMM_SELECT_SEARCH_PLACEHOLDER_PHONE"] = "Nom ou le téléphone";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
?>