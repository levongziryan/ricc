<?
$MESS["M_CRM_COMM_SELECTOR_NOTHING_FOUND"] = "Rien n'a été trouvé.";
$MESS["M_CRM_COMM_SELECTOR_LOAD_TEXT"] = "Mise à jour...";
$MESS["M_CRM_COMM_SELECTOR_DOWN_TEXT"] = "Relâcher pour mettre à jour...";
$MESS["M_CRM_COMM_SELECT_SEARCH_BUTTON"] = "Recherche";
$MESS["M_CRM_COMM_SELECTOR_PULL_TEXT"] = "Tirer pour mettre à jour...";
?>