<?
$MESS["MD_EMPLOYEES_TITLE"] = "Funcionários";
$MESS["MD_EMPLOYEES_ALL"] = "Todos os funcionários";
$MESS["MD_GROUPS_TITLE"] = "Grupos de trabalho";
$MESS["MD_EXTRANET"] = "Extranet";
$MESS["MD_DISK_TABLE_FOLDERS_FILES"] = "Pastas: #FOLDERS# Arquivos: #FILES#";
$MESS["MD_MOBILE_APPLICATION"] = "Aplicativo para celular";
$MESS["MD_GENERATE_BY_MOBILE"] = "Gerado por aplicativo";
?>