<?
$MESS["MB_TASKS_TASKS_FILTER_PULLDOWN_PULL"] = "Zum Aktualisieren herunterziehen";
$MESS["MB_TASKS_TASKS_FILTER_PULLDOWN_DOWN"] = "Zum Aktualisieren freigeben";
$MESS["MB_TASKS_TASKS_FILTER_PULLDOWN_LOADING"] = "Daten werden aktualisiert...";
$MESS["TASKS_FILTER_BUILDER_PRESET_ORIGINATOR_NAME"] = "Erstellt von";
$MESS["TASKS_FILTER_BUILDER_PRESET_RESPONSIBLE_NAME"] = "Verantwortlich";
$MESS["TASKS_FILTER_BUILDER_PRESET_CLOSER_NAME"] = "Abgeschlossen von";
$MESS["TASKS_FILTER_BUILDER_PRESET_PRIORITY_NAME"] = "Priorität";
$MESS["TASKS_FILTER_BUILDER_PRESET_PRIORITY_LOW"] = "Niedrig";
$MESS["TASKS_FILTER_BUILDER_PRESET_PRIORITY_MIDDLE"] = "Normal";
$MESS["TASKS_FILTER_BUILDER_PRESET_PRIORITY_HIGH"] = "Hoch";
$MESS["TASKS_FILTER_BUILDER_PRESET_STATUS_NAME"] = "Status";
$MESS["TASKS_FILTER_BUILDER_PRESET_STATUS_ACTIVE"] = "Aktive";
$MESS["TASKS_FILTER_BUILDER_PRESET_STATUS_DEFERRED"] = "Verschobene";
$MESS["TASKS_FILTER_BUILDER_PRESET_STATUS_DECLINED"] = "Abgelehnte";
$MESS["TASKS_FILTER_BUILDER_PRESET_STATUS_EXPIRED"] = "Überfällige";
?>