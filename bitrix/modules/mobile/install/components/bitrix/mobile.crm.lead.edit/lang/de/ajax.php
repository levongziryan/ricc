<?
$MESS["CRM_ACCESS_DENIED"] = "Zugriff verweigert";
$MESS["CRM_ENTITY_TYPE_NOT_SUPPORTED"] = "TYPE_NOT_SUPPORTED: Typ '#ENTITY_TYPE#' wird im aktuellen Kontext nicht unterstützt.";
$MESS["CRM_ENTITY_DATA_NOT_FOUND"] = "DATA_NOT_FOUND: Es wurden keine Daten zur Verarbeitung gefunden.";
$MESS["CRM_ENTITY_ID_NOT_FOUND"] = "ID_NOT_FOUND: Die ID wurde nicht gefunden.";
$MESS["CRM_LEAD_STATUS_NOT_FOUND"] = "STATUS_NOT_FOUND: Status wurde nicht gefunden.";
$MESS["CRM_LEAD_TITLE_NOT_ASSIGNED"] = "Geben Sie bitte die Bezeichnung des Leads an.";
$MESS["CRM_LEAD_NOT_FOUND"] = "Der Lead '#ID#' wurde nicht gefunden.";
$MESS["CRM_LEAD_COULD_NOT_DELETE"] = "Der Lead kann nicht gelöscht werden.";
?>