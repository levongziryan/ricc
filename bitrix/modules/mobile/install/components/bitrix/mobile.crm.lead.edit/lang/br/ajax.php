<?
$MESS["CRM_ACCESS_DENIED"] = "Acesso negado.";
$MESS["CRM_ENTITY_TYPE_NOT_SUPPORTED"] = "TYPE_NOT_SUPPORTED: Tipo '#ENTITY_TYPE#' não é compatível com o contexto atual.";
$MESS["CRM_ENTITY_DATA_NOT_FOUND"] = "DATA_NOT_FOUND: Não há dados para processar.";
$MESS["CRM_ENTITY_ID_NOT_FOUND"] = "ID_NOT_FOUND: A ID não pôde ser encontrado.";
$MESS["CRM_LEAD_STATUS_NOT_FOUND"] = "STATUS_NOT_FOUND: O status não pôde ser encontrado.";
$MESS["CRM_LEAD_TITLE_NOT_ASSIGNED"] = "Digite o nome do lead.";
$MESS["CRM_LEAD_NOT_FOUND"] = "Não foi possível encontrar lead ##ID#.";
$MESS["CRM_LEAD_COULD_NOT_DELETE"] = "O lead não pode ser excluído.";
?>