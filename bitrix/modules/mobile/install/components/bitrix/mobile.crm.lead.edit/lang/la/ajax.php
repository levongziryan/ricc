<?
$MESS["CRM_ACCESS_DENIED"] = "Acceso denegado.";
$MESS["CRM_ENTITY_TYPE_NOT_SUPPORTED"] = "TYPE_NOT_SUPPORTED: Type '#ENTITY_TYPE#' no se admite en el contexto actual.";
$MESS["CRM_ENTITY_DATA_NOT_FOUND"] = "DATA_NOT_FOUND: No hay datos para procesar.";
$MESS["CRM_ENTITY_ID_NOT_FOUND"] = "ID_NOT_FOUND: El ID no se pudo encontrar.";
$MESS["CRM_LEAD_STATUS_NOT_FOUND"] = "STAGE_NOT_FOUND: No se puedo encontrar el estado.";
$MESS["CRM_LEAD_TITLE_NOT_ASSIGNED"] = "Por favor, introduzca el nombre de la propuesta.";
$MESS["CRM_LEAD_NOT_FOUND"] = "No se puede encontrar";
$MESS["CRM_LEAD_COULD_NOT_DELETE"] = "El prospecto no se pudo eliminar.";
?>