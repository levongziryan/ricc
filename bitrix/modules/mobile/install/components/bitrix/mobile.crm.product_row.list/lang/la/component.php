<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_PRODUCT_LIST_OWNER_TYPE_NOT_DEFINED"] = "El tipo de propietario no se especifica..";
$MESS["CRM_PRODUCT_LIST_OWNER_TYPE_NOT_SUPPORTED"] = "El tipo de propietario especificado no es compatible.";
$MESS["CRM_PRODUCT_LIST_OWNER_ID_NOT_DEFINED"] = "No se especifica el ID del propietario.";
?>