<?
$MESS["M_FILTER_PULL_TEXT"] = "Tirez vers le bas pour rafraîchir...";
$MESS["M_FILTER_DOWN_TEXT"] = "Lâchez pour rafraîchir...";
$MESS["M_FILTER_LOAD_TEXT"] = "Mise à jour du filtre...";
$MESS["M_FILTER_TITLE"] = "Configurer le filtre";
$MESS["M_FILTER_BUTTON_APPLY"] = "Appliquer";
$MESS["M_FILTER_BUTTON_CANCEL"] = "Annuler";
?>