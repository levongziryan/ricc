<?
$MESS["interface_filter_no_no_no_1"] = "(no)";
$MESS["interface_filter_today"] = "hoy";
$MESS["interface_filter_yesterday"] = "ayer";
$MESS["interface_filter_week"] = "esta semana";
$MESS["interface_filter_week_ago"] = "la semana pasada";
$MESS["interface_filter_month"] = "este mes";
$MESS["interface_filter_month_ago"] = "el mes pasado";
$MESS["interface_filter_last"] = "reciente";
$MESS["interface_filter_exact"] = "exactamente";
$MESS["interface_filter_later"] = "después";
$MESS["interface_filter_earlier"] = "antes de";
$MESS["interface_filter_interval"] = "rango de fechas";
?>