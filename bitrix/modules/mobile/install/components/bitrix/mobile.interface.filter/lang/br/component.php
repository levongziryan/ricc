<?
$MESS["interface_filter_no_no_no_1"] = "(não)";
$MESS["interface_filter_today"] = "hoje";
$MESS["interface_filter_yesterday"] = "ontem";
$MESS["interface_filter_week"] = "esta semana";
$MESS["interface_filter_week_ago"] = "semana passada";
$MESS["interface_filter_month"] = "este mês";
$MESS["interface_filter_month_ago"] = "mês passado";
$MESS["interface_filter_last"] = "recente";
$MESS["interface_filter_exact"] = "exatamente";
$MESS["interface_filter_later"] = "após";
$MESS["interface_filter_earlier"] = "antes";
$MESS["interface_filter_interval"] = "intervalo de data";
?>