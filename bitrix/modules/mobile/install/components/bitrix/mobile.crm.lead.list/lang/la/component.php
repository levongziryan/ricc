<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["M_CRM_LEAD_LIST_PRESET_NEW"] = "Nuevo prospecto";
$MESS["M_CRM_LEAD_LIST_PRESET_MY"] = "Mis prospectos";
$MESS["M_CRM_LEAD_LIST_FILTER_CUSTOM"] = "Resutados de búsqueda";
$MESS["M_CRM_LEAD_LIST_FILTER_NONE"] = "Todos los prospectos";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["M_CRM_LEAD_LIST_EDIT"] = "Editar";
$MESS["M_CRM_LEAD_LIST_DELETE"] = "Eliminar";
$MESS["M_CRM_LEAD_LIST_DECLINE"] = "Rechazar";
$MESS["M_CRM_LEAD_LIST_CREATE_BASE"] = "Crear usando el origen";
$MESS["M_CRM_LEAD_LIST_MORE"] = "Más";
$MESS["M_CRM_LEAD_LIST_CREATE_DEAL"] = "Negociación";
$MESS["M_CRM_LEAD_LIST_ADD_DEAL"] = "Agregar actividad";
$MESS["M_CRM_LEAD_LIST_CALL"] = "Hacer una llamada";
$MESS["M_CRM_LEAD_LIST_MEETING"] = "Establecer una reunion";
$MESS["M_CRM_LEAD_LIST_MAIL"] = "Enviar mensaje";
$MESS["M_CRM_LEAD_LIST_NO_FILTER"] = "Todos los prospectos";
$MESS["M_CRM_LEAD_LIST_PRESET_USER"] = "Filtro personalizado";
$MESS["M_CRM_LEAD_LIST_JUNK"] = "Basura";
?>