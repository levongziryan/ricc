<?
$MESS["M_CRM_LEAD_LIST_FILTER_NONE"] = "Tous les leads";
$MESS["M_CRM_LEAD_LIST_LOAD_TEXT"] = "Mise à jour...";
$MESS["M_CRM_LEAD_LIST_DOWN_TEXT"] = "Relâcher pour mettre à jour...";
$MESS["M_CRM_LEAD_LIST_SEARCH_BUTTON"] = "Recherche";
$MESS["M_CRM_LEAD_LIST_SEARCH_PLACEHOLDER"] = "Recherche par la dénomination";
$MESS["M_CRM_LEAD_LIST_PULL_TEXT"] = "Tirer pour mettre à jour...";
$MESS["M_CRM_LEAD_LIST_FILTER_CUSTOM"] = "Résultats de recherche";
$MESS["M_CRM_LEAD_LIST_TITLE"] = "Tous les clients potentiels";
$MESS["M_CRM_LEAD_ADD"] = "Créer un client potentiel";
$MESS["M_CRM_LEAD_CONV_OPEN_ENTITY_SEL_CONTACT"] = "Choisir un contact de la liste...";
$MESS["M_CRM_LEAD_CONV_OPEN_ENTITY_SEL_COMPANY"] = "Choisir une entreprise de la liste...";
?>