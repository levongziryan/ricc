<?
$MESS["M_CRM_CLIENT_LIST_PULL_TEXT"] = "Tirer vers le bas pour rafraîchir...";
$MESS["M_CRM_CLIENT_LIST_DOWN_TEXT"] = "Relâcher pour rafraîchir...";
$MESS["M_CRM_CLIENT_LIST_LOAD_TEXT"] = "Mise à jour...";
$MESS["M_CRM_CLIENT_LIST_SEARCH_PLACEHOLDER"] = "Recherche";
$MESS["M_CRM_CLIENT_LIST_SEARCH_BUTTON"] = "Recherche";
$MESS["M_CRM_CLIENT_LIST_ENTITY_TYPE_CONTACT"] = "Contacts";
$MESS["M_CRM_CLIENT_LIST_ENTITY_TYPE_COMPANY"] = "Entreprises";
$MESS["M_CRM_CLIENT_LIST_ACCEPT_BUTTON_TITLE"] = "Enregistrer";
$MESS["M_CRM_CLIENT_LIST_CANCEL_BUTTON_TITLE"] = "Annuler";
$MESS["M_CRM_CLIENT_LIST_COMPANY_BUTTON_TITLE"] = "Choisir une entreprise";
$MESS["M_CRM_CLIENT_LIST_CONTACT_BUTTON_TITLE"] = "Choisir un contact";
$MESS["M_CRM_CLIENT_LIST_NO_CONTACTS"] = "Aucun contact";
$MESS["M_CRM_CLIENT_LIST_NO_COMPANIES"] = "Aucune entreprise";
$MESS["M_CRM_CLIENT_LIST_ADD_CONTACT"] = "Ajouter un contact";
$MESS["M_CRM_CLIENT_LIST_ADD_COMPANY"] = "Ajouter une entreprise";
?>