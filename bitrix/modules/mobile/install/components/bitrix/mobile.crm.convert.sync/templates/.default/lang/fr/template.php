<?
$MESS["M_CRM_CONVERT_SYNC_TITLE"] = "Créer en utilisant la source";
$MESS["M_CRM_CONVERT_SYNC_PULL_TEXT"] = "Tirer vers le bas pour rafraîchir...";
$MESS["M_CRM_CONVERT_SYNC_DOWN_TEXT"] = "Relâcher pour rafraîchir...";
$MESS["M_CRM_CONVERT_SYNC_LOAD_TEXT"] = "Mise à jour...";
$MESS["M_CRM_CONVERT_SYNC_FIELDS"] = "Ces champs seront créés";
$MESS["M_CRM_CONVERT_SYNC_CONTINUE"] = "Continuer";
$MESS["M_CRM_CONVERT_SYNC_MORE"] = "Plus";
$MESS["M_CRM_CONVERT_SYNC_ENTITIES"] = "Choisissez les documents qui seront reconstitués avec des champs manquants";
$MESS["M_CRM_CONVERT_SYNC_LEGEND_LEAD"] = "Les entités sélectionnées n'ont pas de champs pouvant stocker des données de clients potentiels. Choisissez les entités dans lesquelles les champs manquants seront créés pour accueillir toutes les informations disponibles.";
$MESS["M_CRM_CONVERT_SYNC_LEGEND_DEAL"] = "Les entités sélectionnées n'ont pas de champs pouvant stocker des données d'affaires. Choisissez les entités dans lesquelles les champs manquants seront créés pour accueillir toutes les informations disponibles.";
$MESS["M_CRM_CONVERT_SYNC_LEGEND_QUOTE"] = "Les entités sélectionnées n'ont pas de champs pouvant stocker des données de devis. Choisissez les entités dans lesquelles les champs manquants seront créés pour accueillir toutes les informations disponibles.";
?>