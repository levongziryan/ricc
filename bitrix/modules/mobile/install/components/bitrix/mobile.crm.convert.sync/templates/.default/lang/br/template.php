<?
$MESS["M_CRM_CONVERT_SYNC_TITLE"] = "Criar usando fonte";
$MESS["M_CRM_CONVERT_SYNC_PULL_TEXT"] = "Puxe para baixo para atualizar...";
$MESS["M_CRM_CONVERT_SYNC_DOWN_TEXT"] = "Solte para atualizar...";
$MESS["M_CRM_CONVERT_SYNC_LOAD_TEXT"] = "Atualizando...";
$MESS["M_CRM_CONVERT_SYNC_FIELDS"] = "Estes campos serão criados";
$MESS["M_CRM_CONVERT_SYNC_CONTINUE"] = "Continuar";
$MESS["M_CRM_CONVERT_SYNC_MORE"] = "Mais";
$MESS["M_CRM_CONVERT_SYNC_ENTITIES"] = "Selecione documentos que serão repostos com campos faltantes";
$MESS["M_CRM_CONVERT_SYNC_LEGEND_LEAD"] = "As entidades selecionadas não têm campos que podem armazenar dados de cliente potencial. Selecione entidades em que campos faltantes serão criados para acomodar todas as informações disponíveis.";
$MESS["M_CRM_CONVERT_SYNC_LEGEND_DEAL"] = "As entidades selecionadas não têm campos que podem armazenar dados de negociações. Selecione entidades em que campos faltantes serão criados para acomodar todas as informações disponíveis.";
$MESS["M_CRM_CONVERT_SYNC_LEGEND_QUOTE"] = "As entidades selecionadas não têm campos que podem armazenar dados de cotações. Selecione entidades em que campos faltantes serão criados para acomodar todas as informações disponíveis.";
?>