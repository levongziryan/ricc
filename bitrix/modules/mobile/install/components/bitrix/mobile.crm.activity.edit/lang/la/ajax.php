<?
$MESS["CRM_ACTIVITY_EDIT_ACTION_DEFAULT_SUBJECT"] = "Nueva actividad (#DATE#)";
$MESS["CRM_ACTIVITY_EDIT_INCOMING_CALL_ACTION_DEFAULT_SUBJECT_EXT"] = "Llamada entrante #TITLE#";
$MESS["CRM_ACTIVITY_EDIT_OUTGOING_CALL_ACTION_DEFAULT_SUBJECT_EXT"] = "Llamada saliente #TITLE#";
$MESS["CRM_ACTIVITY_EDIT_MEETING_ACTION_DEFAULT_SUBJECT_EXT"] = "Reunirse con #TITLE#";
$MESS["CRM_ACTIVITY_EDIT_EMAIL_ACTION_DEFAULT_SUBJECT"] = "Nuevo e-mail (#DATE#)";
$MESS["CRM_ACTIVITY_EDIT_EMAIL_EMPTY_FROM_FIELD"] = "Por favor, especifique el remitente del mensaje.";
$MESS["CRM_ACTIVITY_EDIT_EMAIL_EMPTY_TO_FIELD"] = "Por favor, especifique el destinatario del mensaje.";
$MESS["CRM_ACTIVITY_EDIT_INVALID_EMAIL"] = "'#VALUE#' no es una dirección de e-mail válida.";
$MESS["CRM_ACTIVITY_EDIT_TITLE_EMAIL_SUBJECT"] = "Asunto";
$MESS["CRM_ACTIVITY_EDIT_TITLE_EMAIL_FROM"] = "De";
$MESS["CRM_ACTIVITY_EDIT_TITLE_EMAIL_TO"] = "Para";
$MESS["CRM_ACCESS_DENIED"] = "Acceso denegado.";
$MESS["CRM_ACTIVITY_OWNER_TYPE_NOT_FOUND"] = "No se encontró el tipo de propietario.";
$MESS["CRM_ACTIVITY_OWNER_ID_NOT_FOUND"] = "El ID del propietario no se encontró.";
$MESS["CRM_ACTIVITY_NOT_FOUND"] = "No se pudo encontrar la actividad ##ID#.";
$MESS["CRM_ACTIVITY_COULD_NOT_DELETE"] = "La actividad no se pudo eliminar.";
$MESS["CRM_ENTITY_TYPE_NOT_SUPPORTED"] = "TYPE_NOT_SUPPORTED: the '#ENTITY_TYPE#' no se admite en el contexto actual.";
$MESS["CRM_ACTIVITY_OWNER_NOT_FOUND"] = "No se puede determinar objeto.";
$MESS["CRM_ACTIVITY_SELECT_COMMUNICATION"] = "Seleccione el contacto";
?>