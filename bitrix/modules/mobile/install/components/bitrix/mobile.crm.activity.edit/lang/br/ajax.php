<?
$MESS["CRM_ACCESS_DENIED"] = "Acesso negado.";
$MESS["CRM_ENTITY_TYPE_NOT_SUPPORTED"] = "TYPE_NOT_SUPPORTED: \"#ENTITY_TYPE#' não é compatível com o contexto atual.  ";
$MESS["CRM_ACTIVITY_OWNER_TYPE_NOT_FOUND"] = "Tipo do proprietário não encontrado.";
$MESS["CRM_ACTIVITY_OWNER_ID_NOT_FOUND"] = "A ID do proprietário não foi encontrada.";
$MESS["CRM_ACTIVITY_OWNER_NOT_FOUND"] = "Não é possível determinar objeto.";
$MESS["CRM_ACTIVITY_NOT_FOUND"] = "Não foi possível encontrar atividade ##ID#.";
$MESS["CRM_ACTIVITY_COULD_NOT_DELETE"] = "A atividade não pôde ser excluída.";
$MESS["CRM_ACTIVITY_SELECT_COMMUNICATION"] = "Selecionar contato";
$MESS["CRM_ACTIVITY_EDIT_ACTION_DEFAULT_SUBJECT"] = "Nova atividade (#DATE#)";
$MESS["CRM_ACTIVITY_EDIT_INCOMING_CALL_ACTION_DEFAULT_SUBJECT_EXT"] = "Recebendo chamada #TITLE#";
$MESS["CRM_ACTIVITY_EDIT_OUTGOING_CALL_ACTION_DEFAULT_SUBJECT_EXT"] = "Chamada realizada #TITLE#";
$MESS["CRM_ACTIVITY_EDIT_MEETING_ACTION_DEFAULT_SUBJECT_EXT"] = "Reunião com #TITLE#";
$MESS["CRM_ACTIVITY_EDIT_EMAIL_ACTION_DEFAULT_SUBJECT"] = "Novo e-mail (#DATE#)";
$MESS["CRM_ACTIVITY_EDIT_EMAIL_EMPTY_FROM_FIELD"] = "Por favor, especifique o remetente da mensagem.";
$MESS["CRM_ACTIVITY_EDIT_EMAIL_EMPTY_TO_FIELD"] = "Por favor, especifique o destinatário da mensagem.";
$MESS["CRM_ACTIVITY_EDIT_INVALID_EMAIL"] = "#VALUE#' não é um endereço de e-mail válido.";
$MESS["CRM_ACTIVITY_EDIT_TITLE_EMAIL_SUBJECT"] = "Assunto";
$MESS["CRM_ACTIVITY_EDIT_TITLE_EMAIL_FROM"] = "De";
$MESS["CRM_ACTIVITY_EDIT_TITLE_EMAIL_TO"] = "Para";
?>