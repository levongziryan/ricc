<?
$MESS["CRM_ACTIVITY_EDIT_INVALID_EMAIL"] = "#VALUE#' n'est pas adresseemail correcte.";
$MESS["CRM_ENTITY_TYPE_NOT_SUPPORTED"] = "TYPE_NOT_SUPPORTED: Le type '#ENTITY_TYPE#' n'est pas soutenu dans le contexte courant.";
$MESS["CRM_ACTIVITY_EDIT_MEETING_ACTION_DEFAULT_SUBJECT_EXT"] = "Rencontre avec #TITLE#";
$MESS["CRM_ACTIVITY_EDIT_INCOMING_CALL_ACTION_DEFAULT_SUBJECT_EXT"] = "Appel entrant #TITLE#";
$MESS["CRM_ACCESS_DENIED"] = "Accès interdit.";
$MESS["CRM_ACTIVITY_EDIT_OUTGOING_CALL_ACTION_DEFAULT_SUBJECT_EXT"] = "Appel sortant #TITLE#";
$MESS["CRM_ACTIVITY_EDIT_TITLE_EMAIL_TO"] = "Dans";
$MESS["CRM_ACTIVITY_OWNER_ID_NOT_FOUND"] = "ID du propriétaire introuvable.";
$MESS["CRM_ACTIVITY_OWNER_TYPE_NOT_FOUND"] = "Type de propriétaire introuvable.";
$MESS["CRM_ACTIVITY_NOT_FOUND"] = "Chec de recherche du dossier à identificateur '#ID#'.";
$MESS["CRM_ACTIVITY_OWNER_NOT_FOUND"] = "Impossible de déterminer le propriétaire.";
$MESS["CRM_ACTIVITY_COULD_NOT_DELETE"] = "Impossible de supprimer une transaction.";
$MESS["CRM_ACTIVITY_EDIT_ACTION_DEFAULT_SUBJECT"] = "Nouvelle action (#DATE#)";
$MESS["CRM_ACTIVITY_EDIT_EMAIL_ACTION_DEFAULT_SUBJECT"] = "Une nouvelle lettre (#DATE#)";
$MESS["CRM_ACTIVITY_EDIT_TITLE_EMAIL_FROM"] = "De qui";
$MESS["CRM_ACTIVITY_SELECT_COMMUNICATION"] = "Veuillez choisir un membre";
$MESS["CRM_ACTIVITY_EDIT_EMAIL_EMPTY_FROM_FIELD"] = "Veuillez indiquer l'expéditeur dans le champ 'de'.";
$MESS["CRM_ACTIVITY_EDIT_EMAIL_EMPTY_TO_FIELD"] = "Veuillez indiquer le destinataire dans le champ 'à qui'.";
$MESS["CRM_ACTIVITY_EDIT_TITLE_EMAIL_SUBJECT"] = "En-tête";
?>