<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["M_CRM_QUOTE_LIST_PRESET_NEW"] = "Nueva cotización";
$MESS["M_CRM_QUOTE_LIST_PRESET_MY"] = "Mueva cotización";
$MESS["M_CRM_QUOTE_LIST_FILTER_NONE"] = "Todas las cotizaciones";
$MESS["M_CRM_QUOTE_LIST_PRESET_USER"] = "Filtro personalizado";
$MESS["M_CRM_QUOTE_LIST_SEND"] = "Enviar";
$MESS["M_CRM_QUOTE_LIST_CHANGE_STATUS"] = "Cambiar estado";
$MESS["M_CRM_QUOTE_LIST_MORE"] = "Más...";
$MESS["M_CRM_QUOTE_LIST_EDIT"] = "Editar";
$MESS["M_CRM_QUOTE_LIST_DELETE"] = "Eliminar";
$MESS["M_CRM_QUOTE_LIST_COMPANY"] = "Enlace a la compañía";
$MESS["M_CRM_QUOTE_LIST_CONTACT"] = "Enlace al contacto";
$MESS["M_CRM_QUOTE_LIST_DEAL"] = "Enlace a la negociación";
$MESS["M_CRM_QUOTE_LIST_LEAD"] = "Enlace al prospecto";
$MESS["M_CRM_QUOTE_LIST_CREATE_BASE"] = "Crear usando el origen";
?>