<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["M_CRM_DEAL_LIST_PRESET_NEW"] = "Novas negociações";
$MESS["M_CRM_DEAL_LIST_PRESET_MY"] = "Minhas negociações";
$MESS["M_CRM_DEAL_LIST_PRESET_MY_NOT_COMPLETED"] = "Minhas negociações incompletaas";
$MESS["M_CRM_DEAL_LIST_PRESET_NOT_COMPLETED"] = "Incompleto";
$MESS["M_CRM_DEAL_LIST_PRESET_COMPLETED"] = "Concluído";
$MESS["M_CRM_DEAL_LIST_PRESET_WON"] = "Venceu";
$MESS["M_CRM_DEAL_LIST_PRESET_FAILED"] = "Falhou";
$MESS["M_CRM_DEAL_LIST_FILTER_CUSTOM"] = "Resultados de busca";
$MESS["M_CRM_DEAL_LIST_FILTER_NONE"] = "Todas as negociações";
$MESS["M_CRM_QUOTE_LIST_PRESET_NEW"] = "Novas cotações";
$MESS["M_CRM_QUOTE_LIST_PRESET_MY"] = "Minhas cotações";
$MESS["M_CRM_QUOTE_LIST_FILTER_NONE"] = "Todas as cotações";
$MESS["M_CRM_QUOTE_LIST_PRESET_USER"] = "Filtro personalizado";
$MESS["M_CRM_QUOTE_LIST_SEND"] = "Enviar";
$MESS["M_CRM_QUOTE_LIST_CHANGE_STATUS"] = "Alterar status";
$MESS["M_CRM_QUOTE_LIST_MORE"] = "Mais...";
$MESS["M_CRM_QUOTE_LIST_EDIT"] = "Editar";
$MESS["M_CRM_QUOTE_LIST_DELETE"] = "Excluir";
$MESS["M_CRM_QUOTE_LIST_COMPANY"] = "Vinculado à empresa";
$MESS["M_CRM_QUOTE_LIST_CONTACT"] = "Vinculado ao contato";
$MESS["M_CRM_QUOTE_LIST_DEAL"] = "Vinculado à negociação";
$MESS["M_CRM_QUOTE_LIST_LEAD"] = "Vinculado ao cliente potencial";
$MESS["M_CRM_QUOTE_LIST_CREATE_BASE"] = "Criar usando fonte";
?>