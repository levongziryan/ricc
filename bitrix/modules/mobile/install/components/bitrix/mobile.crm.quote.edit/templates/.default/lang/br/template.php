<?
$MESS["M_CRM_QUOTE_EDIT_SAVE_BTN"] = "Salvar";
$MESS["M_CRM_QUOTE_EDIT_CONTINUE_BTN"] = "Continuar";
$MESS["M_CRM_QUOTE_EDIT_CANCEL_BTN"] = "Cancelar";
$MESS["M_CRM_QUOTE_EDIT_CREATE_TITLE"] = "Criar cotação";
$MESS["M_CRM_QUOTE_EDIT_EDIT_TITLE"] = "Editar";
$MESS["M_CRM_QUOTE_EDIT_VIEW_TITLE"] = "Editar cotação";
$MESS["M_CRM_QUOTE_EDIT_CONVERT_TITLE"] = "Cotação";
$MESS["M_DETAIL_PULL_TEXT"] = "Puxe para baixo para atualizar...";
$MESS["M_DETAIL_DOWN_TEXT"] = "Solte para atualizar...";
$MESS["M_DETAIL_LOAD_TEXT"] = "Atualizando...";
$MESS["M_CRM_QUOTE_MENU_EDIT"] = "Editar";
$MESS["M_CRM_QUOTE_MENU_DELETE"] = "Excluir";
$MESS["M_CRM_QUOTE_CONVERSION_NOTIFY"] = "Campos obrigatórios";
$MESS["M_CRM_QUOTE_MENU_CREATE_ON_BASE"] = "Criar usando fonte";
$MESS["M_CRM_QUOTE_MENU_HISTORY"] = "Histórico";
?>