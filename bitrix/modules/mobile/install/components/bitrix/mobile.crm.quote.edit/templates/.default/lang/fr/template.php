<?
$MESS["M_CRM_QUOTE_EDIT_SAVE_BTN"] = "Enregistrer";
$MESS["M_CRM_QUOTE_EDIT_CONTINUE_BTN"] = "Continuer";
$MESS["M_CRM_QUOTE_EDIT_CANCEL_BTN"] = "Annuler";
$MESS["M_CRM_QUOTE_EDIT_CREATE_TITLE"] = "Créer un devis";
$MESS["M_CRM_QUOTE_EDIT_EDIT_TITLE"] = "Modifier";
$MESS["M_CRM_QUOTE_EDIT_VIEW_TITLE"] = "Modifier le devis";
$MESS["M_CRM_QUOTE_EDIT_CONVERT_TITLE"] = "Devis";
$MESS["M_DETAIL_PULL_TEXT"] = "Tirer vers le bas pour rafraîchir...";
$MESS["M_DETAIL_DOWN_TEXT"] = "Relâcher pour rafraîchir...";
$MESS["M_DETAIL_LOAD_TEXT"] = "Mise à jour...";
$MESS["M_CRM_QUOTE_MENU_EDIT"] = "Modifier";
$MESS["M_CRM_QUOTE_MENU_DELETE"] = "Supprimer";
$MESS["M_CRM_QUOTE_CONVERSION_NOTIFY"] = "Champs requis";
$MESS["M_CRM_QUOTE_MENU_CREATE_ON_BASE"] = "Créer en utilisant la source";
$MESS["M_CRM_QUOTE_MENU_HISTORY"] = "Historique";
?>