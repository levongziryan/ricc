<?
$MESS["CRM_ENTITY_DATA_NOT_FOUND"] = "DATA_NOT_FOUND: Impossible de trouver les données pour le traitement.";
$MESS["CRM_ENTITY_ID_NOT_FOUND"] = "ID_NOT_FOUND: ID n'est pas trouvé.";
$MESS["CRM_DEAL_STAGE_NOT_FOUND"] = "STAGE_NOT_FOUND: Impossible de trouver le stade.";
$MESS["CRM_ENTITY_TYPE_NOT_SUPPORTED"] = "TYPE_NOT_SUPPORTED: Le type '#ENTITY_TYPE#' n'est pas soutenu dans le contexte courant.";
$MESS["CRM_ACCESS_DENIED"] = "Accès interdit.";
$MESS["CRM_DEAL_NOT_FOUND"] = "La recherche de la transaction avec l'identifiant ##ID# n'était pas réussie.";
$MESS["CRM_DEAL_COULD_NOT_DELETE"] = "Impossible de supprimer la transaction.";
$MESS["CRM_DEAL_TITLE_NOT_ASSIGNED"] = "Veuillez indiquer le nom de la transaction.";
?>