<?
$MESS["M_CRM_REQUISITE_EDIT_PULL_TEXT"] = "Puxe para baixo para atualizar...";
$MESS["M_CRM_REQUISITE_EDIT_DOWN_TEXT"] = "Solte para atualizar...";
$MESS["M_CRM_REQUISITE_EDIT_LOAD_TEXT"] = "Atualizando...";
$MESS["M_CRM_REQUISITE_EDIT_TITLE"] = "Editar detalhes de pagamento";
$MESS["M_CRM_REQUISITE_EDIT_SAVE_BTN"] = "Salvar";
$MESS["M_CRM_REQUISITE_EDIT_CANCEL_BTN"] = "Cancelar";
?>