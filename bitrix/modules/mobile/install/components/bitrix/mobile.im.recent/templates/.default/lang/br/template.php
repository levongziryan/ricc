<?
$MESS["IM_RESENT_TODAY"] = "Hoje";
$MESS["IM_RESENT_NEVER"] = "Nunca";
$MESS["IM_RESENT_MESSAGE_EMPTY"] = "- Não existem mensagens -";
$MESS["IM_RESENT_CHAT_EMPTY"] = "Não há conversas.";
$MESS["IM_RESENT_PULLTEXT"] = "Puxe para recarregar";
$MESS["IM_RESENT_DOWNTEXT"] = "Solte para recarregar";
$MESS["IM_RESENT_LOADTEXT"] = "Carregando ...";
$MESS["IM_RESENT_FORMAT_DATE"] = "j F";
$MESS["IM_QUOTE"] = "Citar";
$MESS["IM_RESENT_SEARCH"] = "Pesquisar";
$MESS["IM_RESENT_LOADING"] = "Carregando... Aguarde.";
?>