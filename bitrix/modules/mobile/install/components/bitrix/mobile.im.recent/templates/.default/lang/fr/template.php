<?
$MESS["IM_RESENT_TODAY"] = "Aujourd'hui";
$MESS["IM_RESENT_NEVER"] = "Jamais";
$MESS["IM_RESENT_MESSAGE_EMPTY"] = "- il n'y a pas de messages disponibles -";
$MESS["IM_RESENT_CHAT_EMPTY"] = "Il n'y a pas de dialogues ouverts";
$MESS["IM_RESENT_PULLTEXT"] = "Traînez pour recharger";
$MESS["IM_RESENT_DOWNTEXT"] = "Sortie de redémarrage";
$MESS["IM_RESENT_LOADTEXT"] = "Redémarrage...";
$MESS["IM_RESENT_FORMAT_DATE"] = "J F";
$MESS["IM_QUOTE"] = "Citation";
$MESS["IM_RESENT_SEARCH"] = "Recherche";
$MESS["IM_RESENT_LOADING"] = "Chargement ... Veuillez patienter.";
?>