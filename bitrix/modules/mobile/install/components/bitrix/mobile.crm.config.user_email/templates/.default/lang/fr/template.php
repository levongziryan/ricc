<?
$MESS["M_CRM_CONFIG_USER_EMAIL_INPUT_LEGEND"] = "Veuillez saisir votre E-mail";
$MESS["M_CRM_CONFIG_USER_EMAIL_LOAD_TEXT"] = "Mise à jour...";
$MESS["M_CRM_CONFIG_USER_EMAIL_CANCEL_BTN"] = "Annuler";
$MESS["M_CRM_CONFIG_USER_EMAIL_DOWN_TEXT"] = "Relâcher pour mettre à jour...";
$MESS["M_CRM_CONFIG_USER_EMAIL_PULL_TEXT"] = "Tirer pour mettre à jour...";
$MESS["M_CRM_CONFIG_USER_EMAIL_SAVE_BTN"] = "Sauvegarder";
?>