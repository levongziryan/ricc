<?
$MESS["M_CRM_CONFIG_USER_EMAIL_PULL_TEXT"] = "Tire hacia abajo para actualizar...";
$MESS["M_CRM_CONFIG_USER_EMAIL_DOWN_TEXT"] = "Suelte para actualizar...";
$MESS["M_CRM_CONFIG_USER_EMAIL_LOAD_TEXT"] = "Actualizando...";
$MESS["M_CRM_CONFIG_USER_EMAIL_INPUT_LEGEND"] = "Ingresar el correo electrónico";
$MESS["M_CRM_CONFIG_USER_EMAIL_SAVE_BTN"] = "Guardar";
$MESS["M_CRM_CONFIG_USER_EMAIL_CANCEL_BTN"] = "Cancelar";
?>