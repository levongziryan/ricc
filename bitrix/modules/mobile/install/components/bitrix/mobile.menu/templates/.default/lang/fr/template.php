<?
$MESS["MB_LIVE_FEED"] = "Flux d'activités";
$MESS["MB_MESSAGES"] = "Messages";
$MESS["MB_COMPANY"] = "Employés";
$MESS["MB_SEC_FAVORITE"] = "Mon Espace de Travail";
$MESS["MB_SEC_GROUPS"] = "Groupes";
$MESS["MB_SEC_EXTRANET"] = "Groupes extranet";
$MESS["MB_EXIT"] = "Sortie";
$MESS["MB_HELP"] = "Aide";
$MESS["MB_TASKS_MAIN_MENU_ITEM"] = "Tâches";
$MESS["MB_CALENDAR_LIST"] = "Calendrier";
$MESS["MB_CURRENT_USER_FILES_MAIN_MENU_ITEM"] = "Mon Drive";
$MESS["MB_SHARED_FILES_MAIN_MENU_ITEM"] = "Drive Entreprise";
$MESS["PULL_TEXT"] = "Tirer pour mettre à jour";
$MESS["DOWN_TEXT"] = "Relâcher pour mettre à jour";
$MESS["LOAD_TEXT"] = "Renouvellement du menu...";
$MESS["MB_CRM_ACTIVITY"] = "Mes activités";
$MESS["MB_CRM_CONTACT"] = "Contacts";
$MESS["MB_CRM_COMPANY"] = "Entreprise";
$MESS["MB_CRM_DEAL"] = "Affaires";
$MESS["MB_CRM_LEAD"] = "Conversion";
$MESS["MB_CRM_INVOICE"] = "Factures";
$MESS["MB_CURRENT_USER_FILES_MAIN_MENU_ITEM_NEW"] = "Mon Drive";
$MESS["MB_SHARED_FILES_MAIN_MENU_ITEM_NEW"] = "Drive de l'Entreprise";
$MESS["MB_CRM_PRODUCT"] = "Produits";
?>