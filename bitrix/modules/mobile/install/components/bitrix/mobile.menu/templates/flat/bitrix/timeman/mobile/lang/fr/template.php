<?
$MESS["TM_STATUS_WORK"] = "Je travaille";
$MESS["TM_STATUS_COMPLETED"] = "A enregistré son départ";
$MESS["TM_STATUS_START"] = "Enregistrer son arrivée";
$MESS["TM_STATUS_PAUSED"] = "En pause";
$MESS["TM_STATUS_EXPIRED"] = "Départ non-enregistré";
$MESS["TM_NOTIF_EXPIRED"] = "Vous n'avez pas fermé la journée de travail précédente";
$MESS["TM_NOTIF_START"] = "Enregistrer son arrivée";
?>