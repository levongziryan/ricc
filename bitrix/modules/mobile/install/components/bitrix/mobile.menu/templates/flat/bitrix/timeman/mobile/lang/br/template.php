<?
$MESS["TM_STATUS_WORK"] = "Trabalhando";
$MESS["TM_STATUS_COMPLETED"] = "Hora de saída";
$MESS["TM_STATUS_START"] = "Hora de entrada";
$MESS["TM_STATUS_PAUSED"] = "Em intervalo";
$MESS["TM_STATUS_EXPIRED"] = "Não marcou a saída";
$MESS["TM_NOTIF_EXPIRED"] = "Você não encerrou o dia útil anterior";
$MESS["TM_NOTIF_START"] = "Hora de entrada";
?>