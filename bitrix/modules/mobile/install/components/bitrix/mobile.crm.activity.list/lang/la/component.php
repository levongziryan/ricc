<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_MY"] = "Mis actividades";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_MY_NOT_COMPLETED"] = "Mis actividades incompletas";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_MY_COMPLETED"] = "Mis actividades realizadas";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_NOT_COMPLETED"] = "Incompletas";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_COMPLETED"] = "Realizadas";
?>