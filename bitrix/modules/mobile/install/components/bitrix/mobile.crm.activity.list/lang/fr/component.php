<?
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_COMPLETED"] = "Achevé(e)s";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_MY"] = "Mes activités";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_MY_COMPLETED"] = "Mes affaires accomplies";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_MY_NOT_COMPLETED"] = "Ce que moi n'a pas achevé";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_NOT_COMPLETED"] = "Inachevé(e)s";
?>