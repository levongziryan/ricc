<?
$MESS["M_CRM_ACTIVITY_LIST_PULL_TEXT"] = "Puxe para baixo para atualizar ...";
$MESS["M_CRM_ACTIVITY_LIST_DOWN_TEXT"] = "Solte para atualizar ...";
$MESS["M_CRM_ACTIVITY_LIST_LOAD_TEXT"] = "Atualizando ...";
$MESS["M_CRM_ACTIVITY_LIST_SEARCH_PLACEHOLDER"] = "Pesquisa por nome";
$MESS["M_CRM_ACTIVITY_LIST_SEARCH_BUTTON"] = "Pesquisar";
$MESS["M_CRM_ACTIVITY_LIST_FILTER_NONE"] = "Todas as atividades";
$MESS["M_CRM_ACTIVITY_LIST_FILTER_CUSTOM"] = "Resultados da Pesquisa";
$MESS["M_CRM_ACTIVITY_LIST_IMPORTANT"] = "Importante";
$MESS["M_CRM_ACTIVITY_LIST_RUBRIC_FILTER_NONE"] = "Tudo";
$MESS["M_CRM_ACTIVITY_LIST_RUBRIC_LEGEND"] = "(Atividades)";
$MESS["M_CRM_ACTIVITY_LIST_TIME_NOT_DEFINED"] = "[Não definido]";
$MESS["M_CRM_ACTIVITY_LIST_CREATE_CALL"] = "Nova chamada";
$MESS["M_CRM_ACTIVITY_LIST_CREATE_MEETING"] = "Nova reunião";
$MESS["M_CRM_ACTIVITY_LIST_CREATE_EMAIL"] = "Novo e-mail";
$MESS["M_CRM_ACTIVITY_LIST_NOTHING_FOUND"] = "Nenhuma atividade encontrada.";
?>