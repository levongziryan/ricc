<?
$MESS["M_CRM_LOCATION_LIST_LOAD_TEXT"] = "Mise à jour...";
$MESS["M_CRM_LOCATION_LIST_DOWN_TEXT"] = "Relâcher pour mettre à jour...";
$MESS["M_CRM_LOCATION_LIST_SEARCH_BUTTON"] = "Recherche";
$MESS["M_CRM_LOCATION_LIST_SEARCH_PLACEHOLDER"] = "Recherche par ville ou région";
$MESS["M_CRM_LOCATION_LIST_PULL_TEXT"] = "Tirer pour mettre à jour...";
?>