<?
$MESS["M_CRM_INVOICE_VIEW_PULL_TEXT"] = "Lassen Sie los, um zu aktualisieren...";
$MESS["M_CRM_INVOICE_VIEW_DOWN_TEXT"] = "Lassen Sie los, um zu aktualisieren...";
$MESS["M_CRM_INVOICE_VIEW_LOAD_TEXT"] = "Formular wird aktualisiert...";
$MESS["M_CRM_INVOICE_VIEW_ID"] = "##ID#";
$MESS["M_CRM_IVOICE_VIEW_PAYMENT_DATE"] = "Zahlungsdatum";
$MESS["M_CRM_IVOICE_VIEW_PAYMENT_DOC"] = "Zahlungsdokument";
$MESS["M_CRM_IVOICE_VIEW_PAYMENT_COMMENT"] = "Zahlungskommentar";
$MESS["M_CRM_IVOICE_VIEW_CANCEL_DATE"] = "Stornierungsdatum";
$MESS["M_CRM_IVOICE_VIEW_CANCEL_REASON"] = "Grund der Stornierung";
$MESS["M_CRM_INVOICE_VIEW_ACTION_CALL_TO_CLIENT"] = "Anrufen";
$MESS["M_CRM_INVOICE_VIEW_PRODUCT_ROWS"] = "Produkte";
$MESS["M_CRM_INVOICE_VIEW_EVENT_LIST"] = "History";
$MESS["M_CRM_INVOICE_VIEW_RESPONSIBLE"] = "Verantwortlich";
$MESS["M_CRM_INVOICE_VIEW_MANAGER_COMMENTS"] = "Kommentar des verantwortlichen MA";
$MESS["M_CRM_INVOICE_VIEW_USER_COMMENTS"] = "Kommentar (wird in der Rechnung angezeigt)";
$MESS["M_CRM_INVOICE_VIEW_DATE_BILL"] = "Ausgestellt am";
$MESS["M_CRM_INVOICE_VIEW_DATE_PAY_BEFORE"] = "Zahlungsfrist";
$MESS["M_CRM_INVOICE_VIEW_PAYER_INFO"] = "Bankverbindung";
$MESS["M_CRM_INVOICE_VIEW_EDIT"] = "Bearbeiten";
$MESS["M_CRM_INVOICE_VIEW_DELETE"] = "Löschen";
$MESS["M_CRM_INVOICE_VIEW_DELETION_TITLE"] = "Rechnung löschen";
$MESS["M_CRM_INVOICE_VIEW_DELETION_CONFIRMATION"] = "Möchten Sie die Rechnung wirklich löschen?";
$MESS["M_CRM_INVOICE_VIEW_SEND_EMAIL"] = "Rechnung per E-Mail senden";
$MESS["M_CRM_INVOICE_VIEW_CLIENT_EMAIL_NOT_FOUND"] = "Es gibt keine E-Mail-Adresse, die diesem Kunden zugewiesen ist. Weisen Sie bitte eine E-Mail-Adresse im entsprechenden Bearbeitungsformular zu und versuchen Sie dann erneut.";
?>