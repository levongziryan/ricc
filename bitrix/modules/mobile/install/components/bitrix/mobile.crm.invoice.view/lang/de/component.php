<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Das Modul CRM ist nicht installiert.";
$MESS["CRM_PERMISSION_DENIED"] = "Zugriff verweigert";
$MESS["CRM_INVOICE_VIEW_NOT_FOUND"] = "Die Rechnung '#ID#' wurde nicht gefunden.";
$MESS["CRM_INVOICE_VIEW_RESPONSIBLE_NOT_ASSIGNED"] = "[nicht definiert]";
$MESS["CRM_INVOICE_VIEW_EMAIL_SUBJECT"] = "Rechnung ##NUMBER#";
?>