<?
$MESS["TASK_COLUMN_CREATED_BY"] = "criado por";
$MESS["TASK_COLUMN_RESPONSIBLE_ID"] = "pessoa responsável";
$MESS["TASK_COLUMN_DEADLINE"] = "prazo";
$MESS["TASK_COLUMN_PRIORITY"] = "prioridade";
$MESS["TASK_COLUMN_STATUS"] = "status";
$MESS["TASKS_PRIORITY_2"] = "Alta Prioridade";
$MESS["TASKS_STATUS_METASTATE_EXPIRED"] = "Atrasado";
$MESS["TASKS_STATUS_STATE_NEW"] = "Novo";
$MESS["TASKS_STATUS_STATE_PENDING"] = "Pendente";
$MESS["TASKS_STATUS_STATE_IN_PROGRESS"] = "Em andamento";
$MESS["TASKS_STATUS_STATE_SUPPOSEDLY_COMPLETED"] = "Na dependência de revisão";
$MESS["TASKS_STATUS_STATE_COMPLETED"] = "Concluído";
$MESS["TASKS_STATUS_STATE_DEFERRED"] = "Adiado";
$MESS["TASKS_STATUS_STATE_DECLINED"] = "Declinado";
$MESS["TASKS_STATUS_STATE_UNKNOWN"] = "Desconhecido";
$MESS["TASK_EXPIRED"] = "Atrasado";
$MESS["TASKS_EMPTY_LIST"] = "A lista de tarefas está vazia";
$MESS["TASKS_EMPTY_LIST1"] = "Criar tarefa";
$MESS["TASKS_EMPTY_LIST2"] = "Nenhuma tarefa foi encontrada";
$MESS["TASKS_ALL_TASKS"] = "Todos";
?>