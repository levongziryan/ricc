<?
$MESS["MB_TASKS_PANEL_TAB_ALL"] = "Todo";
$MESS["MB_TASKS_PANEL_TAB_PROJECTS"] = "Proyectos";
$MESS["MB_TASKS_ROLES_TASK_ADD"] = "Nueva tarea";
$MESS["MB_TASKS_CONTROLS_TITLE"] = "Categorías de tareas";
$MESS["MB_TASKS_ROLES_VIEW_TASK_CATEGORY_WO_DEADLINE"] = "Sin fecha límite<br> (Asignado a mí)";
$MESS["MB_TASKS_ROLES_VIEW_TASK_CATEGORY_NEW"] = "No leído";
$MESS["MB_TASKS_ROLES_VIEW_TASK_CATEGORY_EXPIRED_CANDIDATES"] = "Por vencer";
$MESS["MB_TASKS_ROLES_VIEW_TASK_CATEGORY_EXPIRED"] = "Atrasado";
$MESS["MB_TASKS_ROLES_VIEW_TASK_CATEGORY_WAIT_CTRL"] = "Revisión pendiente";
?>