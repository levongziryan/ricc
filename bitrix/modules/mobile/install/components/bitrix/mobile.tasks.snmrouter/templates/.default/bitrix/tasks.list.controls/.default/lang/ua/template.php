<?
$MESS["MB_TASKS_TASKS_LIST_PULLDOWN_PULL"] = "Потягніть, щоб оновити";
$MESS["MB_TASKS_TASKS_LIST_PULLDOWN_DOWN"] = "Відпустіть, щоб оновити";
$MESS["MB_TASKS_TASKS_LIST_PULLDOWN_LOADING"] = "Оновлення даних ...";
$MESS["MB_TASKS_PANEL_TAB_ALL"] = "Всі";
$MESS["MB_TASKS_PANEL_TAB_PROJECTS"] = "Проекти";
$MESS["MB_TASKS_CONTROLS_TITLE"] = "Категорії завдань";
$MESS["MB_TASKS_ROLES_TASK_ADD"] = "Додати завдання";
$MESS["MB_TASKS_ROLES_VIEW_TASK_CATEGORY_WO_DEADLINE"] = "Без крайнього терміну <br> (Доручені мною)";
$MESS["MB_TASKS_ROLES_VIEW_TASK_CATEGORY_NEW"] = "Непереглянуті";
$MESS["MB_TASKS_ROLES_VIEW_TASK_CATEGORY_EXPIRED_CANDIDATES"] = "Майже прострочені";
$MESS["MB_TASKS_ROLES_VIEW_TASK_CATEGORY_EXPIRED"] = "Прострочені";
$MESS["MB_TASKS_ROLES_VIEW_TASK_CATEGORY_WAIT_CTRL"] = "Чекають контролю";
?>