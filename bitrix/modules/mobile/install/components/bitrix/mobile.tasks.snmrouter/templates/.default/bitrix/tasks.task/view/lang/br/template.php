<?
$MESS["TASKS_DEPENDENCY_START"] = "Início";
$MESS["TASKS_DEPENDENCY_END"] = "Fim";
$MESS["TASKS_STATUS_METASTATE_EXPIRED"] = "Atrasado";
$MESS["TASKS_STATUS_STATE_NEW"] = "Novo";
$MESS["TASKS_STATUS_STATE_PENDING"] = "Pendente";
$MESS["TASKS_STATUS_STATE_IN_PROGRESS"] = "Em andamento";
$MESS["TASKS_STATUS_STATE_SUPPOSEDLY_COMPLETED"] = "Na dependência de revisão";
$MESS["TASKS_STATUS_STATE_COMPLETED"] = "Concluído";
$MESS["TASKS_STATUS_STATE_DEFERRED"] = "Adiado";
$MESS["TASKS_STATUS_STATE_DECLINED"] = "Declinado";
$MESS["TASKS_STATUS_STATE_UNKNOWN"] = "Desconhecido";
$MESS["MB_TASKS_TASK_DETAIL_BTN_EDIT"] = "Editar";
$MESS["MB_TASKS_TASK_DETAIL_BTN_REMOVE"] = "Excluir";
$MESS["MB_TASKS_TASK_DETAIL_BTN_ACCEPT_TASK"] = "Aceitar tarefa";
$MESS["MB_TASKS_TASK_DETAIL_BTN_START_TASK"] = "Iniciar tarefa";
$MESS["MB_TASKS_TASK_DETAIL_BTN_DECLINE_TASK"] = "Recusar";
$MESS["MB_TASKS_TASK_DETAIL_BTN_RENEW_TASK"] = "Retomar";
$MESS["MB_TASKS_TASK_DETAIL_BTN_CLOSE_TASK"] = "Concluir";
$MESS["MB_TASKS_TASK_DETAIL_BTN_PAUSE_TASK"] = "Pausar";
$MESS["MB_TASKS_TASK_DETAIL_BTN_APPROVE_TASK"] = "Aprovar";
$MESS["MB_TASKS_TASK_DETAIL_BTN_REDO_TASK"] = "Concluir";
$MESS["MB_TASKS_TASK_DETAIL_BTN_DELEGATE_TASK"] = "Delegar";
$MESS["MB_TASKS_TASK_DETAIL_TASK_WAS_REMOVED"] = "A tarefa ##TASK_ID# foi excluída.";
$MESS["MB_TASKS_TASK_DETAIL_TASK_WAS_REMOVED_OR_NOT_ENOUGH_RIGHTS"] = "A tarefa ##TASK_ID# foi excluída ou você não tem permissões suficientes.";
$MESS["MB_TASKS_TASK_DETAIL_NOTIFICATION_EXPIRED"] = "A tarefa está atrasada!";
$MESS["MB_TASKS_TASK_DETAIL_NOTIFICATION_EXPIRED_BRIEF"] = "Atrasado";
$MESS["MB_TASKS_TASK_DETAIL_NOTIFICATION_STATE_SUPPOSEDLY_COMPLETED"] = "A tarefa está pendente de revisão.";
$MESS["MB_TASKS_TASK_DETAIL_USER_SELECTOR_BTN_CANCEL"] = "Cancelar";
$MESS["MB_TASKS_TASK_DETAIL_CONFIRM_REMOVE"] = "Você realmente deseja excluir a tarefa?";
$MESS["MB_TASKS_GENERAL_TITLE"] = "Tarefa";
$MESS["MB_TASKS_TASK_REMOVE_CONFIRM_TITLE"] = "Excluir tarefa";
$MESS["MB_TASKS_TASK_DETAIL_TASK_ADD"] = "Nova tarefa";
$MESS["MB_TASKS_TASK_DETAIL_BTN_ADD_FAVORITE_TASK"] = "Adicionar aos favoritos";
$MESS["MB_TASKS_TASK_DETAIL_BTN_DELETE_FAVORITE_TASK"] = "Remover dos favoritos";
$MESS["MB_TASKS_BASE_SETTINGS"] = "Principais parâmetros de tarefa";
$MESS["MB_TASKS_BASE_SETTINGS_TITLE_TASK"] = "tarefa # #TASK_ID#";
$MESS["MB_TASKS_BASE_SETTINGS_DESCRIPTION"] = "coisas a fazer";
$MESS["MB_TASKS_BASE_SETTINGS_DESCRIPTION_PLACEHOLDER"] = "Descrição da tarefa";
$MESS["MB_TASKS_BASE_SETTINGS_PRIORITY"] = "prioridade";
$MESS["MB_TASKS_BASE_SETTINGS_PRIORITY_VALUE"] = "Alta Prioridade";
$MESS["MB_TASKS_TASK_SETTINGS_RESPONSIBLE"] = "pessoa responsável";
$MESS["MB_TASKS_TASK_SETTINGS_CHECKLIST"] = "lista de verificação";
$MESS["MB_TASKS_TASK_SETTINGS_DEADLINE"] = "prazo";
$MESS["MB_TASKS_TASK_SETTINGS_DEADLINE_PLACEHOLDER"] = "nenhum";
$MESS["MB_TASKS_TASK_SETTINGS_AUDITORS"] = "observadores";
$MESS["MB_TASKS_TASK_SETTINGS_DISK_UF"] = "arquivos";
$MESS["MB_TASKS_TASK_SETTINGS_TAGS"] = "marcadores";
$MESS["MB_TASKS_TASK_SETTINGS_TAGS_PLACEHOLDER"] = "pesquisar marcadores";
$MESS["MB_TASKS_TASK_SETTINGS_GROUP_ID"] = "tarefa no projeto";
$MESS["MB_TASKS_TASK_SETTINGS_PARENT_ID"] = "tarefa principal";
$MESS["MB_TASKS_TASK_SETTINGS_STATUS"] = "status";
$MESS["MB_TASKS_TASK_SETTINGS_DURATION_PLAN"] = "Estimativa de tempo";
$MESS["MB_TASKS_TASK_SETTINGS_AUTHOR_ID"] = "criado por";
$MESS["MB_TASKS_TASK_SETTINGS_MARK"] = "pontuação";
$MESS["MB_TASKS_TASK_SETTINGS_MARK_NULL"] = "Sem pontuação";
$MESS["MB_TASKS_TASK_SETTINGS_MARK_P"] = "Positivo";
$MESS["MB_TASKS_TASK_SETTINGS_MARK_N"] = "Negativo";
$MESS["MB_TASKS_TASK_ADD"] = "adicionar";
$MESS["MB_TASKS_TASK_ADD_SEPARATOR"] = "separador";
$MESS["MB_TASKS_TASK_PLACEHOLDER"] = "Não selecionado";
$MESS["TASKS_LIST_GROUP_ACTION_ERROR1"] = "Resposta inesperada do servidor. Tente novamente.";
$MESS["MB_TASKS_TASK_DELETE"] = "Excluir";
$MESS["MB_TASKS_TASK_EDIT"] = "Editar";
$MESS["MB_TASKS_TASK_CHECK"] = "Executar";
$MESS["MB_TASKS_TASK_UNCHECK"] = "Cancelar";
$MESS["MB_TASKS_TASK_COMMENT"] = "Comentário";
$MESS["MB_TASKS_TASK_CHECKLIST_PLACEHOLDER"] = "Digitar num item de lista de verificação";
$MESS["MB_TASKS_TASK_ERROR1"] = "Erro desconhecido.";
$MESS["MB_TASKS_TASK_ERROR2"] = "Salvar novamente.";
$MESS["TASKS_PRIORITY_0"] = "Alta Prioridade";
$MESS["TASKS_PRIORITY_2"] = "Alta Prioridade";
$MESS["MB_TASKS_TASK_SETTINGS_TIMETRACKING"] = "Gerenciamento de tempo";
$MESS["TASKS_TT_START"] = "Iniciar gerenciador de tempo";
$MESS["TASKS_TT_CONTINUE"] = "Continuar";
$MESS["TASKS_TT_PAUSE"] = "Parar";
$MESS["MB_TASKS_TASK_SETTINGS_SUBTASKS"] = "subtarefas";
$MESS["MB_TASKS_TASK_SETTINGS_CRM_TASK"] = "itens de CRM";
$MESS["TASKS_TT_CANCEL"] = "Cancelar";
$MESS["TASKS_TT_ERROR1_TITLE"] = "O gerenciador de tempo está sendo usado agora com outra tarefa.";
$MESS["TASKS_TT_ERROR1_DESC"] = "Você está atualmente gerenciando o tempo para a tarefa \"#TITLE#\". Esta ação irá suspendê-lo. Deseja continuar?";
$MESS["MB_TASKS_TASK_SETTINGS_ACCOMPLICES"] = "participantes";
$MESS["MB_TASKS_TASK_DETAIL_TASK_ADD_SUBTASK"] = "Criar subtarefa";
?>