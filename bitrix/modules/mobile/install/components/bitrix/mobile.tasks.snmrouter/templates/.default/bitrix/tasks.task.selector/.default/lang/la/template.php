<?
$MESS["TASK_COLUMN_CREATED_BY"] = "creado por ";
$MESS["TASK_COLUMN_RESPONSIBLE_ID"] = "persona responsable";
$MESS["TASK_COLUMN_DEADLINE"] = "fecha límite";
$MESS["TASK_COLUMN_PRIORITY"] = "prioridad";
$MESS["TASK_COLUMN_STATUS"] = "estados";
$MESS["TASKS_PRIORITY_2"] = "Prioridad Alta";
$MESS["TASKS_STATUS_METASTATE_EXPIRED"] = "Atrasado";
$MESS["TASKS_STATUS_STATE_NEW"] = "Nuevo";
$MESS["TASKS_STATUS_STATE_PENDING"] = "Pendiente";
$MESS["TASKS_STATUS_STATE_IN_PROGRESS"] = "En progreso";
$MESS["TASKS_STATUS_STATE_SUPPOSEDLY_COMPLETED"] = "Revisión pendiente";
$MESS["TASKS_STATUS_STATE_COMPLETED"] = "Completado";
$MESS["TASKS_STATUS_STATE_DEFERRED"] = "Diferido";
$MESS["TASKS_STATUS_STATE_DECLINED"] = "Declinado";
$MESS["TASKS_STATUS_STATE_UNKNOWN"] = "Desconocido";
$MESS["TASK_EXPIRED"] = "Atrasado";
$MESS["TASKS_EMPTY_LIST"] = "La lista de tareas está vacía";
$MESS["TASKS_EMPTY_LIST1"] = "Tarea creada";
$MESS["TASKS_EMPTY_LIST2"] = "No se encontraron tareas";
$MESS["TASKS_ALL_TASKS"] = "Todo";
?>