<?
$MESS["TASK_RESTRICTED_USER1"] = "<h1>Votre abonnement limite l'accès à cette option</h1><p>Les outils professionnels dans votre abonnement sont disponibles pour jusqu'à 24 utilisateurs.</p><p>Si vous devez avoir accès à des outils professionnels, veuillez contacter votre administrateur Bitrix24.</p>";
$MESS["TASK_RESTRICTED_USER2"] = "Envoyer la demande";
$MESS["TASK_RESTRICTED_USER3"] = "La demande a été envoyée";
$MESS["TASK_RESTRICTED_ADMIN1"] = "<h1>Votre abonnement limite l'accès à cette option</h1><p>Les outils professionnels dans votre abonnement sont disponibles pour jusqu'à 24 utilisateurs.</p><p>Vous pouvez accorder ou retirer l'accès d'un utilisateur aux outils professionnels uniquement dans la version complète de Bitrix24.</p>";
$MESS["TASK_RESTRICTED_ADMIN2"] = "Ouvrir la version complète";
?>