<?
$MESS["TASK_RESTRICTED_USER1"] = "<h1>Su plan limita el acceso a esta opción</h1><p>Herramientas de negocio  en su plan están disponibles para hasta 24 usuarios.</p><p>Si necesita acceso a herramientas de negocio, póngase en contacto con el administrador de Bitrix24.</p>
";
$MESS["TASK_RESTRICTED_USER2"] = "Enviar solicitud ";
$MESS["TASK_RESTRICTED_USER3"] = "Su solicitud ha sido enviada";
$MESS["TASK_RESTRICTED_ADMIN1"] = "<h1>Su plan limita el acceso a esta opción</h1><p>Herramientas de negocio  en su plan están disponibles para hasta 24 usuarios.</p><p>Puede conceder o revocar el acceso de un usuario a herramientas de negocios sólo en la versión completa de Bitrix24.</p>";
$MESS["TASK_RESTRICTED_ADMIN2"] = "Abrir la versión completa";
?>