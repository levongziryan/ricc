<?
$MESS["MB_TASKS_TASK_SNMROUTER_TASK_WAS_REMOVED"] = "Tarea ha sido eliminado.";
$MESS["MB_TASKS_TASKS_LIST_PULLDOWN_PULL"] = "Tire hacia abajo para actualizar";
$MESS["MB_TASKS_TASKS_LIST_PULLDOWN_DOWN"] = "Suéltelo para actualizar";
$MESS["MB_TASKS_TASKS_LIST_PULLDOWN_LOADING"] = "Actualizando datos...";
$MESS["MB_TASKS_TASKS_LIST_MENU_CREATE_NEW_TASK"] = "Nueva tarea";
$MESS["MB_TASKS_TASKS_LIST_MENU_GOTO_FILTER"] = "Configurar el filtro";
$MESS["MB_TASKS_PANEL_TAB_ALL"] = "Todo";
$MESS["MB_TASKS_PANEL_TAB_PROJECTS"] = "Proyectos";
$MESS["MB_TASKS_GENERAL_TITLE"] = "Tareas";
$MESS["MB_TASKS_ROLES_TASK_ADD"] = "Nueva tarea";
$MESS["MB_TASKS_TASKS_LIST_NO_TASKS"] = "No hay tareas";
$MESS["MB_TASKS_TASK_ERROR_TITLE"] = "Error";
?>