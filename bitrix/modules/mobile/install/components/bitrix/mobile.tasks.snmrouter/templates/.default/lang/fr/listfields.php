<?
$MESS["TASK_COLUMN_CREATED_BY"] = "Créé par";
$MESS["TASK_COLUMN_RESPONSIBLE_ID"] = "Responsable";
$MESS["TASK_COLUMN_DEADLINE"] = "Date limite";
$MESS["TASK_COLUMN_MARK"] = "Évaluation";
$MESS["TASK_COLUMN_PRIORITY"] = "Priorité";
$MESS["TASK_COLUMN_STATUS"] = "Statut";
$MESS["TASK_COLUMN_GROUP_ID"] = "Groupe";
$MESS["TASK_COLUMN_TIME_ESTIMATE"] = "Estimation temporelle requise";
$MESS["TASK_COLUMN_ALLOW_TIME_TRACKING"] = "Suivre le temps passé";
$MESS["TASK_COLUMN_TIME_SPENT_IN_LOGS"] = "Temps passé";
$MESS["TASK_COLUMN_ALLOW_CHANGE_DEADLINE"] = "Le responsable peut changer la date limite";
$MESS["TASK_COLUMN_CREATED_DATE"] = "Créé le";
$MESS["TASK_COLUMN_CHANGED_DATE"] = "Date de modification";
$MESS["TASK_COLUMN_CLOSED_DATE"] = "Terminé le";
$MESS["TASK_COLUMN_FAVORITES"] = "Favoris";
?>