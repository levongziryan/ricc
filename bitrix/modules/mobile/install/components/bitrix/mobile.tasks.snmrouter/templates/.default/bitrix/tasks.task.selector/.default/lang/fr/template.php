<?
$MESS["MB_TASKS_CANCEL_S"] = "annuler";
$MESS["MB_TASKS_SELECT"] = "Sélectionner";
$MESS["TASK_COLUMN_CREATED_BY"] = "créé par";
$MESS["TASK_COLUMN_RESPONSIBLE_ID"] = "personne responsable";
$MESS["TASK_COLUMN_DEADLINE"] = "date limite";
$MESS["TASK_COLUMN_PRIORITY"] = "priorité";
$MESS["TASK_COLUMN_STATUS"] = "statut";
$MESS["TASKS_PRIORITY_2"] = "Haute priorité";
$MESS["TASKS_STATUS_METASTATE_EXPIRED"] = "En retard";
$MESS["TASKS_STATUS_STATE_NEW"] = "Nouveau";
$MESS["TASKS_STATUS_STATE_PENDING"] = "En attente";
$MESS["TASKS_STATUS_STATE_IN_PROGRESS"] = "En cours";
$MESS["TASKS_STATUS_STATE_SUPPOSEDLY_COMPLETED"] = "En attente de révision";
$MESS["TASKS_STATUS_STATE_COMPLETED"] = "Terminé";
$MESS["TASKS_STATUS_STATE_DEFERRED"] = "Différé";
$MESS["TASKS_STATUS_STATE_DECLINED"] = "Refusé";
$MESS["TASKS_STATUS_STATE_UNKNOWN"] = "Inconnu";
$MESS["TASK_EXPIRED"] = "En retard";
$MESS["TASKS_EMPTY_LIST"] = "La liste de tâches est vide";
$MESS["TASKS_EMPTY_LIST1"] = "Créer une tâche";
$MESS["TASKS_EMPTY_LIST2"] = "Aucune tâche trouvée";
$MESS["TASKS_ALL_TASKS"] = "Tout";
?>