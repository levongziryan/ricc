<?
$MESS["CRM_ACTIVITY_VIEW_RESPONSIBLE_NOT_ASSIGNED"] = "[non affecté]";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_ACTIVITY_VIEW_NOT_FOUND"] = "Chec de recherche du dossier à identificateur '#ID#'.";
?>