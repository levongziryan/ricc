<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_ACTIVITY_VIEW_NOT_FOUND"] = "Não foi possível encontrar a atividade ##ID#.";
$MESS["CRM_ACTIVITY_VIEW_RESPONSIBLE_NOT_ASSIGNED"] = "[Atribuído]";
?>