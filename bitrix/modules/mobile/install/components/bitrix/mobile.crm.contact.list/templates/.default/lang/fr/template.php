<?
$MESS["M_CRM_CONTACT_LIST_RUBRIC_LEGEND"] = "(Contacts)";
$MESS["M_CRM_CONTACT_LIST_FILTER_NONE"] = "Listes des contacts";
$MESS["M_CRM_CONTACT_LIST_ADD_ITEM"] = "Ajouter un contact";
$MESS["M_CRM_CONTACT_LIST_LOAD_TEXT"] = "Mise à jour...";
$MESS["M_CRM_CONTACT_LIST_DOWN_TEXT"] = "Relâcher pour mettre à jour...";
$MESS["M_CRM_CONTACT_LIST_SEARCH_BUTTON"] = "Recherche";
$MESS["M_CRM_CONTACT_LIST_SEARCH_PLACEHOLDER"] = "Recherche par la dénomination";
$MESS["M_CRM_CONTACT_LIST_PULL_TEXT"] = "Tirer pour mettre à jour...";
$MESS["M_CRM_CONTACT_LIST_FILTER_CUSTOM"] = "Résultats de recherche";
$MESS["M_CRM_CONTACT_LIST_TITLE"] = "Tous les contacts";
$MESS["M_CRM_CONTACT_ADD"] = "Ajouter un contact";
$MESS["M_CRM_CONTACT_ADD2"] = "Ajouter";
$MESS["M_CRM_CONTACT_BIZCARD"] = "Carte de visite";
$MESS["M_CRM_CONTACT_ADD_MANUAL"] = "Ajouter manuellement";
$MESS["M_CRM_CONTACT_FROM_BIZCARD"] = "Scanner une carte de visite";
$MESS["M_CRM_CONTACT_BIZCARD_LIMIT_REACHED"] = "Malheureusement, vous avez dépassé votre quota mensuel de travail de reconnaissance. Vous devez passer à un abonnement supérieur afin de pouvoir traiter plus de cartes de visite.";
$MESS["M_CRM_CONTACT_BIZCARD_UNKNOWN_ERROR"] = "Une erreur s'est produite. Veuillez réessayer plus tard.";
$MESS["M_CRM_CONTACT_BIZCARD_IMAGE_UPLOAD"] = "Chargement de l'image...";
$MESS["M_CRM_CONTACT_BIZCARD_RECOGNIZING"] = "Reconnaissance...";
?>