<?
$MESS["M_CRM_CONTACT_LIST_TITLE"] = "Todos los contactos";
$MESS["M_CRM_CONTACT_ADD"] = "Agregar contacto";
$MESS["M_CRM_CONTACT_ADD2"] = "Agregar";
$MESS["M_CRM_CONTACT_ADD_MANUAL"] = "Agregar manualmente";
$MESS["M_CRM_CONTACT_BIZCARD"] = "Tarjeta de presentación";
$MESS["M_CRM_CONTACT_FROM_BIZCARD"] = "Escanear de tarjeta de presentación";
$MESS["M_CRM_CONTACT_BIZCARD_LIMIT_REACHED"] = "Lamentablemente ha superado su cuota mensual de trabajo de reconocimiento.
Usted tendrá que actualizar a un plan superior para procesar más tarjetas de negocios.";
$MESS["M_CRM_CONTACT_BIZCARD_UNKNOWN_ERROR"] = "Se ha producido un error. Por favor, inténtelo de nuevo más tarde.";
$MESS["M_CRM_CONTACT_BIZCARD_IMAGE_UPLOAD"] = "Cargar imagen...";
$MESS["M_CRM_CONTACT_BIZCARD_RECOGNIZING"] = "Reconociendo...";
$MESS["M_CRM_CONTACT_BIZCARD_GO_TO_TARIFF_TABLE"] = "Ver precios";
$MESS["M_CRM_CONTACT_BIZCARD_CLOSE"] = "Cerrar";
?>