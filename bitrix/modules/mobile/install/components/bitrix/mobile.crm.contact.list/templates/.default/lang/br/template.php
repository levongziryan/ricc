<?
$MESS["M_CRM_CONTACT_LIST_PULL_TEXT"] = "Puxe para baixo para atualizar ...";
$MESS["M_CRM_CONTACT_LIST_DOWN_TEXT"] = "Solte para atualizar ...";
$MESS["M_CRM_CONTACT_LIST_LOAD_TEXT"] = "Atualizando ...";
$MESS["M_CRM_CONTACT_LIST_SEARCH_PLACEHOLDER"] = "Pesquisa por nome";
$MESS["M_CRM_CONTACT_LIST_SEARCH_BUTTON"] = "Pesquisar";
$MESS["M_CRM_CONTACT_LIST_FILTER_NONE"] = "Todos os contatos";
$MESS["M_CRM_CONTACT_LIST_FILTER_CUSTOM"] = "Resultados da Pesquisa";
$MESS["M_CRM_CONTACT_LIST_RUBRIC_LEGEND"] = "(Contatos)";
$MESS["M_CRM_CONTACT_LIST_ADD_ITEM"] = "Adicionar contato";
$MESS["M_CRM_CONTACT_LIST_TITLE"] = "Todos os contatos";
$MESS["M_CRM_CONTACT_ADD"] = "Adicionar contato";
$MESS["M_CRM_CONTACT_ADD2"] = "Adicionar";
$MESS["M_CRM_CONTACT_BIZCARD"] = "Cartão de visita";
$MESS["M_CRM_CONTACT_ADD_MANUAL"] = "Adicionar manualmente";
$MESS["M_CRM_CONTACT_FROM_BIZCARD"] = "Escanear cartão de visita";
$MESS["M_CRM_CONTACT_BIZCARD_LIMIT_REACHED"] = "Infelizmente, você excedeu sua cota mensal de reconhecimento. Você terá que fazer um upgrade para um plano superior para processar mais cartões de visita.";
$MESS["M_CRM_CONTACT_BIZCARD_UNKNOWN_ERROR"] = "Houve um erro. Tente novamente mais tarde.";
$MESS["M_CRM_CONTACT_BIZCARD_IMAGE_UPLOAD"] = "Carregar imagem...";
$MESS["M_CRM_CONTACT_BIZCARD_RECOGNIZING"] = "Reconhecendo...";
?>