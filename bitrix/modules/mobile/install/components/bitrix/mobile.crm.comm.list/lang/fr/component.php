<?
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_COMM_LIST_INVALID_ENTITY_TYPE"] = "Le type de propriétaire spécifié n'est pas soutenu.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_COMM_LIST_ENTITY_ID_NOT_DEFINED"] = "ID du propriétaire non renseigné.";
$MESS["CRM_COMM_LIST_ENTITY_TYPE_NOT_DEFINED"] = "Type de propriétaire non renseigné.";
?>