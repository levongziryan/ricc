<?
$MESS["SONET_SET_NAVCHAIN"] = "Definir Mapa de Navegação";
$MESS["SONET_PATH_TO_USER"] = "Modelo do Caminho do Perfil do Usuário";
$MESS["SONET_PATH_TO_MESSAGE_FORM"] = "Modelo do Caminho da Página do Formulário do Post";
$MESS["SONET_PAGE_VAR"] = "Variável página";
$MESS["SONET_USER_VAR"] = "Variável usuário";
$MESS["SONET_USER_ID"] = "ID de usuário";
$MESS["SONET_VARIABLE_ALIASES"] = "Alias variáveis";
$MESS["SONET_PATH_TO_SMILE"] = "Caminho para Smileys Pasta (em relação à raiz do site)";
$MESS["SONET_PATH_TO_MESSAGES_USERS_MESSAGES"] = "Modelo do Caminho da Página de Log de Mensagem";
$MESS["SONET_PATH_TO_MESSAGES_USERS"] = "Modelo do Caminho da Página de Troca de Mensagens";
$MESS["SONET_PATH_TO_MESSAGES_CHAT"] = "Modelo do Caminho da Página de Log de Chat";
$MESS["SONET_ITEMS_COUNT"] = "Itens por página";
$MESS["SONET_GROUP_VAR"] = "Variável do Grupo";
$MESS["SONET_PATH_TO_GROUP"] = "Modelo do Caminho da página de Grupo";
$MESS["SONET_LOG_DATE_DAYS"] = "Número de dias";
$MESS["SONET_LOG_SUBSCRIBE_ONLY"] = "Incluir assinaturas";
$MESS["SONET_LOG_LOG_CNT"] = "Máximo de Itens Visíveis";
$MESS["SONET_USE_COMMENTS"] = "Mostrar comentários como evento relacionado";
?>