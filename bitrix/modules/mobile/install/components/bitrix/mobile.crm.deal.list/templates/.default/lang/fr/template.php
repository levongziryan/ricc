<?
$MESS["M_CRM_DEAL_LIST_RUBRIC_LEGEND"] = "(Transactions)";
$MESS["M_CRM_DEAL_LIST_RUBRIC_FILTER_NONE"] = "Tous";
$MESS["M_CRM_DEAL_LIST_FILTER_NONE"] = "Liste de transactions";
$MESS["M_CRM_DEAL_LIST_LOAD_TEXT"] = "Mise à jour...";
$MESS["M_CRM_DEAL_LIST_DOWN_TEXT"] = "Relâcher pour mettre à jour...";
$MESS["M_CRM_DEAL_LIST_SEARCH_BUTTON"] = "Recherche";
$MESS["M_CRM_DEAL_LIST_SEARCH_PLACEHOLDER"] = "Recherche par la dénomination";
$MESS["M_CRM_DEAL_LIST_PULL_TEXT"] = "Tirer pour mettre à jour...";
$MESS["M_CRM_DEAL_LIST_FILTER_CUSTOM"] = "Résultats de recherche";
$MESS["M_CRM_DEAL_LIST_TITLE"] = "Toutes les affaires";
$MESS["M_CRM_DEAL_ADD"] = "Créer une affaire";
?>