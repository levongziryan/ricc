<?
$MESS["M_CRM_DEAL_LIST_PULL_TEXT"] = "Puxe para baixo para atualizar ...";
$MESS["M_CRM_DEAL_LIST_DOWN_TEXT"] = "Solte para atualizar ...";
$MESS["M_CRM_DEAL_LIST_LOAD_TEXT"] = "Atualizando ...";
$MESS["M_CRM_DEAL_LIST_SEARCH_PLACEHOLDER"] = "Pesquisa por nome";
$MESS["M_CRM_DEAL_LIST_SEARCH_BUTTON"] = "Pesquisar";
$MESS["M_CRM_DEAL_LIST_FILTER_NONE"] = "Todas as ofertas";
$MESS["M_CRM_DEAL_LIST_RUBRIC_FILTER_NONE"] = "Tudo";
$MESS["M_CRM_DEAL_LIST_FILTER_CUSTOM"] = "Resultados da Pesquisa";
$MESS["M_CRM_DEAL_LIST_RUBRIC_LEGEND"] = "(Ofertas)";
$MESS["M_CRM_DEAL_LIST_TITLE"] = "Todas as negociações";
$MESS["M_CRM_DEAL_ADD"] = "Criar negociação";
?>