<?
$MESS["M_CRM_DEAL_LIST_FILTER_NONE"] = "Liste de transactions";
$MESS["M_CRM_DEAL_LIST_PRESET_WON"] = "Avec succès";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["M_CRM_DEAL_LIST_PRESET_COMPLETED"] = "Achevé(e)s";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["M_CRM_DEAL_LIST_PRESET_MY_NOT_COMPLETED"] = "Mes... non fermé(e)s";
$MESS["M_CRM_DEAL_LIST_PRESET_MY"] = "Mes transactions";
$MESS["M_CRM_DEAL_LIST_PRESET_NOT_COMPLETED"] = "Inachevé(e)s";
$MESS["M_CRM_DEAL_LIST_PRESET_NEW"] = "Nouvelles transactions";
$MESS["M_CRM_DEAL_LIST_PRESET_FAILED"] = "Chec";
$MESS["M_CRM_DEAL_LIST_FILTER_CUSTOM"] = "Résultats de recherche";
$MESS["M_CRM_LEAD_LIST_PRESET_USER"] = "Filtre personnalisé";
$MESS["M_CRM_DEAL_LIST_EDIT"] = "Modifier";
$MESS["M_CRM_DEAL_LIST_DELETE"] = "Supprimer";
$MESS["M_CRM_DEAL_LIST_CHANGE_STAGE"] = "Modifier l'étape";
$MESS["M_CRM_DEAL_LIST_BILL"] = "Créer une facture";
$MESS["M_CRM_DEAL_LIST_MORE"] = "Plus...";
$MESS["M_CRM_DEAL_LIST_CREATE_DEAL"] = "Affaire";
$MESS["M_CRM_DEAL_LIST_ADD_DEAL"] = "Ajouter une activité";
$MESS["M_CRM_DEAL_LIST_CALL"] = "Passer un appel";
$MESS["M_CRM_DEAL_LIST_MEETING"] = "Organiser une réunion";
$MESS["M_CRM_DEAL_LIST_MAIL"] = "Envoyer un message";
$MESS["M_CRM_DEAL_LIST_CREATE_BASE"] = "Créer en utilisant la source";
?>