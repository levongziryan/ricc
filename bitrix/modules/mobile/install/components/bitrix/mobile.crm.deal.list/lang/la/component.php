<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["M_CRM_DEAL_LIST_PRESET_NEW"] = "Nuevas negociaciones";
$MESS["M_CRM_DEAL_LIST_PRESET_MY"] = "Mis negociaciones";
$MESS["M_CRM_DEAL_LIST_FILTER_CUSTOM"] = "Resultados de Búsqueda";
$MESS["M_CRM_DEAL_LIST_FILTER_NONE"] = "Todas las negociaciones";
$MESS["M_CRM_LEAD_LIST_PRESET_USER"] = "Filtro personalizado";
$MESS["M_CRM_DEAL_LIST_EDIT"] = "Editar";
$MESS["M_CRM_DEAL_LIST_DELETE"] = "Eliminar";
$MESS["M_CRM_DEAL_LIST_CHANGE_STAGE"] = "Cambiar fase";
$MESS["M_CRM_DEAL_LIST_BILL"] = "Crear factura";
$MESS["M_CRM_DEAL_LIST_MORE"] = "Más...";
$MESS["M_CRM_DEAL_LIST_CREATE_DEAL"] = "Negociación";
$MESS["M_CRM_DEAL_LIST_ADD_DEAL"] = "Agregar actividad";
$MESS["M_CRM_DEAL_LIST_CALL"] = "Hacer una llamada";
$MESS["M_CRM_DEAL_LIST_MEETING"] = "Establecer una reunion";
$MESS["M_CRM_DEAL_LIST_MAIL"] = "Enviar mensaje";
$MESS["M_CRM_DEAL_LIST_CREATE_BASE"] = "Crear usando el origen";
?>