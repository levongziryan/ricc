<?
$MESS["CRM_ACCESS_DENIED"] = "Acesso negado.";
$MESS["CRM_ENTITY_TYPE_NOT_SUPPORTED"] = "TYPE_NOT_SUPPORTED: Tipo '#ENTITY_TYPE#' não é compatível com o contexto atual.";
$MESS["CRM_ENTITY_DATA_NOT_FOUND"] = "DATA_NOT_FOUND: Não há dados para processar.";
$MESS["CRM_ENTITY_ID_NOT_FOUND"] = "ID_NOT_FOUND: A ID não pôde ser encontrado.";
$MESS["CRM_INVOICE_STATUS_NOT_FOUND"] = "STATUS_NOT_FOUND: O status não pôde ser encontrado.";
$MESS["CRM_INVOICE_COULD_NOT_SAVE_STATUS"] = "Não foi possível salvar status da fatura.";
$MESS["CRM_INVOICE_PRODUCT_ROWS_NOT_FOUND"] = "Nenhum SKU foi encontrado.";
$MESS["CRM_INVOICE_COULD_NOT_RECALCULATE"] = "Não foi possível recalcular SKU.";
$MESS["CRM_INVOICE_TOPIC_IS_NOT_ASSIGNED"] = "Por favor, especifique o tópico da fatura.";
$MESS["CRM_INVOICE_CLIENT_IS_NOT_ASSIGNED"] = "Por favor, especifique o cliente.";
$MESS["CRM_INVOICE_STATUS_IS_NOT_ASSIGNED"] = "Por favor, especifique o status da fatura.";
$MESS["CRM_INVOICE_PRODUCT_ROWS_ARE_EMPTY"] = "Por favor, selecione pelo menos um produto.";
$MESS["CRM_INVOICE_EXISTING_ACCOUNT_NUMBER"] = "Uma fatura com este número já existe.";
$MESS["CRM_INVOICE_FIELD_CHECK_GENERAL_ERROR"] = "Não foi possível salvar a fatura porque os campos não validam.";
$MESS["CRM_INVOICE_SAVING_GENERAL_ERROR"] = "Não foi possível salvar a fatura devido a um erro desconhecido.";
$MESS["CRM_INVOICE_NOT_FOUND"] = "O ID da fatura ##ID# não pôde ser encontrado.";
$MESS["CRM_INVOICE_COULD_NOT_DELETE"] = "Não foi possível excluir a fatura.";
$MESS["CRM_INVOICE_COULD_NOT_LOAD_SALE_MODULE"] = "O módulo e-Store falhou ao carregar.";
$MESS["CRM_INVOICE_COULD_NOT_FIND_SALE_ORDER"] = "Pedido não encontrado.";
$MESS["CRM_INVOICE_COULD_NOT_FIND_SALE_ORDER_PAY_SYSTEM"] = "Método de pagamento não encontrado.";
$MESS["CRM_INVOICE_COULD_NOT_FIND_SALE_ORDER_PERSON_TYPE"] = "Tipo de cliente não encontrado.";
$MESS["CRM_INVOICE_COULD_NOT_FIND_PAY_SYSTEM_HANDLER"] = "Manipulador do sistema de pagamento não encontrado.";
$MESS["CRM_INVOICE_COULD_NOT_CREATE_FILE"] = "Não é possível criar arquivo.";
$MESS["CRM_INVOICE_NO_PDF_CONTENT"] = "Não é possível criar documento PDF.";
$MESS["CRM_INVOICE_COULD_NOT_CREATE_WEBDAV_ELEMENT"] = "Não é possível salvar o documento na Biblioteca de Documentos.";
?>