<?
$MESS["CRM_ACCESS_DENIED"] = "Zugriff verweigert";
$MESS["CRM_ENTITY_TYPE_NOT_SUPPORTED"] = "TYPE_NOT_SUPPORTED: Typ '#ENTITY_TYPE#' wird im aktuellen Kontext nicht unterstützt.";
$MESS["CRM_ENTITY_DATA_NOT_FOUND"] = "DATA_NOT_FOUND: Es wurden keine Daten zur Verarbeitung gefunden.";
$MESS["CRM_ENTITY_ID_NOT_FOUND"] = "ID_NOT_FOUND: Die ID wurde nicht gefunden.";
$MESS["CRM_INVOICE_STATUS_NOT_FOUND"] = "STATUS_NOT_FOUND: Status wurde nicht gefunden.";
$MESS["CRM_INVOICE_COULD_NOT_SAVE_STATUS"] = "Der Rechnungsstatus kann nicht gespeichert werden.";
$MESS["CRM_INVOICE_PRODUCT_ROWS_NOT_FOUND"] = "Es wurden keine Produktvarianten gefunden.";
$MESS["CRM_INVOICE_COULD_NOT_RECALCULATE"] = "Die Produktvarianten können nicht neu kalkuliert werden.";
$MESS["CRM_INVOICE_TOPIC_IS_NOT_ASSIGNED"] = "Geben Sie bitte den Betreff der Rechnung an.";
$MESS["CRM_INVOICE_CLIENT_IS_NOT_ASSIGNED"] = "Geben Sie bitte den Kunden an.";
$MESS["CRM_INVOICE_STATUS_IS_NOT_ASSIGNED"] = "Geben Sie bitte den Rechnungsstatus an.";
$MESS["CRM_INVOICE_PRODUCT_ROWS_ARE_EMPTY"] = "Wählen Sie bitte mindestens ein Produkt aus.";
$MESS["CRM_INVOICE_EXISTING_ACCOUNT_NUMBER"] = "Eine Rechnung mit dieser Nummer existiert bereits.";
$MESS["CRM_INVOICE_FIELD_CHECK_GENERAL_ERROR"] = "Die Rechnung kann nicht gespeichert werden, weil die Felder nicht gültig sind.";
$MESS["CRM_INVOICE_SAVING_GENERAL_ERROR"] = "Die Rechnung kann wegen eines unbekannten Fehlers nicht gespeichert werden.";
$MESS["CRM_INVOICE_NOT_FOUND"] = "Die Rechnung mit ID ##ID# kann nicht gefunden werden.";
$MESS["CRM_INVOICE_COULD_NOT_DELETE"] = "Die Rechnung kann nicht gelöscht werden.";
$MESS["CRM_INVOICE_COULD_NOT_LOAD_SALE_MODULE"] = "Das Modul Online-Shop konnte nicht geladen werden.";
$MESS["CRM_INVOICE_COULD_NOT_FIND_SALE_ORDER"] = "Bestellung wurde nicht gefunden.";
$MESS["CRM_INVOICE_COULD_NOT_FIND_SALE_ORDER_PAY_SYSTEM"] = "Zahlungsoption wurde nicht gefunden.";
$MESS["CRM_INVOICE_COULD_NOT_FIND_SALE_ORDER_PERSON_TYPE"] = "Kundentyp wurde nicht gefunden.";
$MESS["CRM_INVOICE_COULD_NOT_FIND_PAY_SYSTEM_HANDLER"] = "Handler des Zahlungssystems wurde nicht gefunden.";
$MESS["CRM_INVOICE_COULD_NOT_CREATE_FILE"] = "Die Datei kann nicht erstellt werden.";
$MESS["CRM_INVOICE_NO_PDF_CONTENT"] = "Das PFD-Dokument kann nicht erstellt werden.";
$MESS["CRM_INVOICE_COULD_NOT_CREATE_WEBDAV_ELEMENT"] = "Das Dokument kann nicht in der Dokumentenbibliothek gespeichert werden.";
?>