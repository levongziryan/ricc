<?
$MESS["CRM_CATALOG_ID"] = "Catálogo Comercial";
$MESS["CRM_PRODUCT_ID"] = "ID do produto";
$MESS["CRM_PRODUCT_ID_PARAM"] = "Nome variável do ID do produto";
$MESS["CRM_PATH_TO_PRODUCT_LIST"] = "Modelo de caminho da página de produtos";
$MESS["CRM_PATH_TO_PRODUCT_EDIT"] = "Modelo de caminho da página de edição de produto";
$MESS["CRM_PATH_TO_PRODUCT_SHOW"] = "Modelo de caminho da página de visualização de produto";
$MESS["CRM_CATALOG_NOT_SELECTED"] = "[não selecionado]";
?>