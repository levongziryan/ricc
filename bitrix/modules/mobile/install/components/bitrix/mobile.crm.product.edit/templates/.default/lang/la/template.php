<?
$MESS["M_CRM_PRODUCT_EDIT_SAVE_BTN"] = "Guardar";
$MESS["M_CRM_PRODUCT_EDIT_CANCEL_BTN"] = "Cancelar";
$MESS["M_CRM_PRODUCT_EDIT_CREATE_TITLE"] = "Crear producto";
$MESS["M_CRM_PRODUCT_EDIT_EDIT_TITLE"] = "Editar";
$MESS["M_CRM_PRODUCT_EDIT_VIEW_TITLE"] = "Ver producto";
$MESS["M_DETAIL_PULL_TEXT"] = "Pulsar para actualizar...";
$MESS["M_DETAIL_DOWN_TEXT"] = "Soltar para actualizar...";
$MESS["M_DETAIL_LOAD_TEXT"] = "Actualizando...";
$MESS["M_CRM_PRODUCT_MENU_EDIT"] = "Editar";
$MESS["M_CRM_PRODUCT_MENU_DELETE"] = "Eliminar";
?>