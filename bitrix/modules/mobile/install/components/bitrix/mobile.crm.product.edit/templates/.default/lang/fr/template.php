<?
$MESS["M_CRM_PRODUCT_EDIT_SAVE_BTN"] = "Enregistrer";
$MESS["M_CRM_PRODUCT_EDIT_CANCEL_BTN"] = "Annuler";
$MESS["M_CRM_PRODUCT_EDIT_CREATE_TITLE"] = "Créer un produit";
$MESS["M_CRM_PRODUCT_EDIT_EDIT_TITLE"] = "Modifier";
$MESS["M_CRM_PRODUCT_EDIT_VIEW_TITLE"] = "Voir le produit";
$MESS["M_DETAIL_PULL_TEXT"] = "Tirer vers le bas pour rafraîchir...";
$MESS["M_DETAIL_DOWN_TEXT"] = "Relâcher pour rafraîchir...";
$MESS["M_DETAIL_LOAD_TEXT"] = "Mise à jour...";
$MESS["M_CRM_PRODUCT_MENU_EDIT"] = "Modifier";
$MESS["M_CRM_PRODUCT_MENU_DELETE"] = "Supprimer";
?>