<?
$MESS["BX24_INVITE_DIALOG_INVITED"] = "<b> Félicitations! </b><br>Les invitations sur le portail ont été envoyées aux adresses E-mails communiquées.";
$MESS["BX24_INVITE_DIALOG_EMAIL"] = "Veuillez saisir les E-mails des employés que vous voulez inviter sur le portail en les séparant par des virgules ou des espaces.";
$MESS["BX24_INVITE_DIALOG_INVITE_LIMIT"] = "Vous ne pouvez pas inviter plus d'employés à cause de la limitation de votre plan de tarif";
$MESS["LOAD_TEXT"] = "Mise à jour...";
$MESS["DOWN_TEXT"] = "Relâcher pour mettre à jour...";
$MESS["BX24_INVITE_DIALOG_CONF_PAGE_TITLE"] = "Confirmation de l'inscription";
$MESS["PULL_TEXT"] = "Tirer pour mettre à jour...";
$MESS["BX24_INVITE_DIALOG_INVITE"] = "Inviter";
$MESS["BX24_INVITE_DIALOG_INVITE_MORE"] = "Inviter encore";
$MESS["BX24_INVITE_DIALOG_INVITE_TITLE"] = "Inviter l'employé";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT"] = "Je vous invite au portail corporatif de notre compagnie. Ici nous pourrons travailler ensemble sur nos projets et tâches, gérer les documents, planifier nos entretiens et réunions, être en contact dans les blogs et beaucoup d'autres choses.";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TITLE"] = "Texte de l'invitation";
?>