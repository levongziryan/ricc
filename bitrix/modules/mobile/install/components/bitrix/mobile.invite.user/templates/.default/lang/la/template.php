<?
$MESS["BX24_INVITE_DIALOG_EMAIL"] = "Introduzca las direcciones de correo electrónico de las personas que desea invitar. Separe las entradas múltiples con una coma o un espacio.";
$MESS["BX24_INVITE_DIALOG_INVITED"] = "<b>Felicitaciones!</b><br>Las invitaciones han sido enviadas a los correos electrónicos indicados.";
$MESS["BX24_INVITE_DIALOG_INVITE"] = "Invitar";
$MESS["BX24_INVITE_DIALOG_INVITE_MORE"] = "Invitar a más usuarios";
$MESS["BX24_INVITE_DIALOG_CONF_PAGE_TITLE"] = "Confirmar inscripción";
$MESS["BX24_INVITE_DIALOG_INVITE_LIMIT"] = "No puedes invitar a más empleados, ya que superará los términos de licencia.";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TITLE"] = "Texto de Invitación";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT"] = "Únase a nosotros en nuestra nueva cuenta de Bitrix24. Este es un lugar donde todos pueden comunicarse, colaborar en tareas y proyectos, administrar clientes y hacer mucho más.";
$MESS["PULL_TEXT"] = "Pulsar para actualizar...";
$MESS["DOWN_TEXT"] = "Suelte para actualizar...";
$MESS["LOAD_TEXT"] = "Actualizar...";
$MESS["BX24_INVITE_DIALOG_INVITE_TITLE"] = "Invitar a un usuario";
?>