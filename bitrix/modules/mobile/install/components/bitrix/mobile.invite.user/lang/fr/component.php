<?
$MESS["BX24_INVITE_DIALOG_MAX_COUNT_ERROR"] = "Le nombre d'employés invités surpasse la limite prévue du plan tarifaire.";
$MESS["BX24_INVITE_DIALOG_EMAIL_ERROR"] = "Email incorrect.";
$MESS["BX24_INVITE_DIALOG_USER_EXIST_ERROR"] = "Les utilisateurs avec ces adresses Adresse emailexistent déjà.";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT"] = "Je vous invite au portail corporatif de notre compagnie. Ici nous pourrons travailler ensemble sur nos projets et tâches, gérer les documents, planifier nos entretiens et réunions, être en contact dans les blogs et beaucoup d'autres choses.";
?>