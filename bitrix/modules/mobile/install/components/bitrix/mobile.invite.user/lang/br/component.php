<?
$MESS["BX24_INVITE_DIALOG_EMAIL_ERROR"] = "Os endereços de e-mail estão incorretos.";
$MESS["BX24_INVITE_DIALOG_USER_EXIST_ERROR"] = "Já existem os usuários com esses endereços de e-mail.";
$MESS["BX24_INVITE_DIALOG_MAX_COUNT_ERROR"] = "O número de pessoas convidadas excede os termos de licença.";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT"] = "Por favor, se junte a mim na nossa Intranet corporativa. É o lugar para todas as pessoas envolvidas contribuirem para projetos essenciais e gerenciar tarefas, planejar eventos e reuniões; comunicar-se socialmente e fazer muitas outras atividades.";
?>