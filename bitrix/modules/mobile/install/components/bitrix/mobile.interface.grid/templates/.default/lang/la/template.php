<?
$MESS["M_GRID_PULL_TEXT"] = "Pulse hacia abajo para actualizar...";
$MESS["M_GRID_DOWN_TEXT"] = "Suéltelo para actualizar...";
$MESS["M_GRID_LOAD_TEXT"] = "Actualizando...";
$MESS["M_GRID_MORE_BUTTON"] = "Más";
$MESS["M_GRID_SEARCH"] = "Buscar";
$MESS["M_GRID_EMPTY_SEARCH"] = "No se han encontrado entradas.";
$MESS["M_GRID_EMPTY_LIST"] = "La lista está vacía.";
?>