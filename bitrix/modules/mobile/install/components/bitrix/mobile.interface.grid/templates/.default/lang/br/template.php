<?
$MESS["M_GRID_PULL_TEXT"] = "Puxe para baixo para atualizar...";
$MESS["M_GRID_DOWN_TEXT"] = "Solte para atualizar...";
$MESS["M_GRID_LOAD_TEXT"] = "Atualizando...";
$MESS["M_GRID_MORE_BUTTON"] = "Mais";
$MESS["M_GRID_SEARCH"] = "Pesquisar";
$MESS["M_GRID_EMPTY_SEARCH"] = "Não foram encontradas entradas.";
?>