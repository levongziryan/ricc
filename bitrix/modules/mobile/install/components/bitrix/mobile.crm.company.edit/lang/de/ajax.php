<?
$MESS["CRM_ACCESS_DENIED"] = "Zugriff verweigert";
$MESS["CRM_ENTITY_TYPE_NOT_SUPPORTED"] = "TYPE_NOT_SUPPORTED: Typ '#ENTITY_TYPE#' wird im aktuellen Kontext nicht unterstützt.";
$MESS["CRM_ENTITY_DATA_NOT_FOUND"] = "DATA_NOT_FOUND: Es wurden keine Daten zur Verarbeitung gefunden.";
$MESS["CRM_ENTITY_ID_NOT_FOUND"] = "ID_NOT_FOUND: Die ID wurde nicht gefunden.";
$MESS["CRM_COMPANY_TITLE_NOT_ASSIGNED"] = "Geben Sie bitte den Unternehmensnamen an.";
$MESS["CRM_COMPANY_NOT_FOUND"] = "Das Unternehmen '#ID#' wurde nicht gefunden.";
$MESS["CRM_COMPANY_COULD_NOT_DELETE"] = "Das Unternehmen kann nicht gelöscht werden.";
?>