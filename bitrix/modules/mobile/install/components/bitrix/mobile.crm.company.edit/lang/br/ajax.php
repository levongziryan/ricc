<?
$MESS["CRM_ACCESS_DENIED"] = "Acesso negado.";
$MESS["CRM_ENTITY_TYPE_NOT_SUPPORTED"] = "TYPE_NOT_SUPPORTED: Tipo '#ENTITY_TYPE#' não é compatível com o contexto atual.";
$MESS["CRM_ENTITY_DATA_NOT_FOUND"] = "DATA_NOT_FOUND: Não há dados para processar.";
$MESS["CRM_ENTITY_ID_NOT_FOUND"] = "ID_NOT_FOUND: A ID não pôde ser encontrado.";
$MESS["CRM_COMPANY_TITLE_NOT_ASSIGNED"] = "Por favor, forneça o nome da empresa.";
$MESS["CRM_COMPANY_NOT_FOUND"] = "Não foi possível encontrar empresa ##ID#.";
$MESS["CRM_COMPANY_COULD_NOT_DELETE"] = "Não foi possível excluir a empresa.";
?>