<?
$MESS["CRM_ENTITY_DATA_NOT_FOUND"] = "DATA_NOT_FOUND: Impossible de trouver les données pour le traitement.";
$MESS["CRM_ENTITY_ID_NOT_FOUND"] = "ID_NOT_FOUND: ID n'est pas trouvé.";
$MESS["CRM_ENTITY_TYPE_NOT_SUPPORTED"] = "TYPE_NOT_SUPPORTED: Le type '#ENTITY_TYPE#' n'est pas soutenu dans le contexte courant.";
$MESS["CRM_ACCESS_DENIED"] = "Accès interdit.";
$MESS["CRM_COMPANY_NOT_FOUND"] = "Impossible de trouver la entreprise avec l'identificateur '#ID#'.";
$MESS["CRM_COMPANY_COULD_NOT_DELETE"] = "Impossible de supprimer la entreprise.";
$MESS["CRM_COMPANY_TITLE_NOT_ASSIGNED"] = "Veuillez indiquer le nom de l'entreprise.";
?>