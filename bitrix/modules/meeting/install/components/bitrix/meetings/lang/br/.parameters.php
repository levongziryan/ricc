<?
$MESS["M_PARAM_list"] = "Modelo do Caminho da Página de Reuniões";
$MESS["M_PARAM_meeting"] = "Modelo do Caminho da Página de Visualização de Reuniçao";
$MESS["M_PARAM_meeting_edit"] = "Modelo do Caminho da Página de Edição de Reunião";
$MESS["M_PARAM_meeting_copy"] = "Modelo do Caminho da Página de Criação de Nova Reunião";
$MESS["M_PARAM_meeting_item"] = "Modelo do Caminho da Página de Detalhes de Tópicos da Agenda";
$MESS["INTL_IBLOCK_TYPE"] = "Tipo de bloco de informação da sala de reuniões";
$MESS["INTL_IBLOCK"] = "Bloco de informação da sala de reuniões";
$MESS["INTL_IBLOCK_TYPE_V"] = "Tipo de bloco de Informação da Sala de video-reuniões ";
$MESS["INTL_IBLOCK_V"] = "Bloco de Informação da Sala de video-reuniões ";
$MESS["M_MEETINGS_COUNT"] = "Reuniões por página";
$MESS["M_NAME_TEMPLATE"] = "Modelo de exibição de nome";
?>