<?
$MESS["ME_ITEM_TITLE"] = "Número de Tema #ID#";
$MESS["ME_MEETING_ITEM_NOT_FOUND"] = "El tema no fue encontrado.";
$MESS["ME_MEETING_ACCESS_DENIED"] = "Acceso denegado";
$MESS["ME_MODULE_NOT_INSTALLED"] = "El módulo \"Reuniones y Sesiones\" no está instalado.";
?>