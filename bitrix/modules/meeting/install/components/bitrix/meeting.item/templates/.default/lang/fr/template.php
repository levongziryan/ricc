<?
$MESS["MI_MEETING_TITLE"] = "##ID# #TITLE#";
$MESS["ME_BACK"] = "Retour à la réunion";
$MESS["ME_LIST_TITLE"] = "Retour à la liste des réunions";
$MESS["MI_REPORT_MEETING"] = "Question évoquée lors de la réunion";
$MESS["MI_REPORT_DATE_START"] = "Date";
$MESS["MI_TASK_ADD"] = "Créer une nouvelle tâche";
$MESS["MI_EDIT_FINISH"] = "Achever l'édition";
$MESS["MI_TASKS"] = "Tâches";
$MESS["MI_SAVING"] = "Sauvegarde";
$MESS["MI_COMMENTS"] = "Commentaire";
$MESS["MI_REPORT_NO_RESPONSIBLE"] = "aucun";
$MESS["MI_REPORT_NO_REPORT"] = "Absent";
$MESS["MI_EDIT_DESCRIPTION"] = "Description";
$MESS["MI_BLOCK_TITLE"] = "Sujet Page de recherche";
$MESS["MI_REPORT_RESPONSIBLES"] = "Responsables";
$MESS["MI_REPORT_RESPONSIBLE"] = "Responsable";
$MESS["MI_TASK_DETACH"] = "Détacher la tâche";
$MESS["MI_TASK_DETACH_TITLE"] = "Détacher la tâche de la question";
$MESS["MI_CANCEL"] = "Annuler";
$MESS["MI_REPORT"] = "Rapport";
$MESS["MI_SAVED"] = "Le rapport est sauvegardé dans #TIME#";
$MESS["MI_REPORTS"] = "Liste de rapports";
$MESS["MI_HISTORY_SHOW"] = "Afficher l'historique du problème (#CNT#)";
$MESS["MI_TASK_ATTACH"] = "Assigner à la tâche existante";
$MESS["MI_EDIT"] = "Editer";
$MESS["MI_HISTORY_HIDE"] = "Cacher l'historique de la question";
$MESS["MI_SAVE"] = "Sauvegarder";
$MESS["MI_REPORT_DEADLINE"] = "Délai";
$MESS["MI_REPORT_STATE"] = "Statut";
$MESS["MI_EDIT_TITLE"] = "d'après les sujets";
$MESS["ME_FILES"] = "Fichiers";
?>