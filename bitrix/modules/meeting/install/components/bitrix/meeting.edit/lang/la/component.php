<?
$MESS["ME_MODULE_NOT_INSTALLED"] = "El módulo de \"Reuniones y Eventos\" no está instalado.";
$MESS["ME_MEETING_NOT_FOUND"] = "La reunión no fue encontrada.";
$MESS["ME_MEETING_ACCESS_DENIED"] = "Acceso denegado";
$MESS["ME_MEETING_EDIT"] = "Reunión ##ID# del #DATE#";
$MESS["ME_MEETING_EDIT_NO_DATE"] = "Reunión ##ID#";
$MESS["ME_MEETING_ADD"] = "Crear una nueva reunión";
$MESS["ME_MEETING_COPY"] = "Crear otra reunión";
$MESS["ME_MEETING_TITLE_DEFAULT"] = "(sin título)";
$MESS["ME_MEETING_TITLE_DEFAULT_1"] = "Tema";
?>