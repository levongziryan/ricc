<?
$MESS["ME_MEETING_TITLE_DEFAULT"] = "(sans nom)";
$MESS["ME_MEETING_TITLE_DEFAULT_1"] = "d'après les sujets";
$MESS["ME_MEETING_ACCESS_DENIED"] = "Accès interdit";
$MESS["ME_MODULE_NOT_INSTALLED"] = "Le module des entretiens de planification et des réunions n'a pas été installé.";
$MESS["ME_MEETING_EDIT_NO_DATE"] = "Réunion #ID#";
$MESS["ME_MEETING_EDIT"] = "Réunion #ID# du #DATE#";
$MESS["ME_MEETING_NOT_FOUND"] = "La réunion n'est pas retrouvée.";
$MESS["ME_MEETING_ADD"] = "Création de la nouvelle réunion";
$MESS["ME_MEETING_COPY"] = "Créer la prochaine réunion";
?>