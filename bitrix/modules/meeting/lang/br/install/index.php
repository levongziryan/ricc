<?
$MESS["MEETING_MODULE_NAME"] = "Reuniões e briefings";
$MESS["MEETING_MODULE_DESCRIPTION"] = "Um módulo para organizar reuniões e briefings.";
$MESS["MEETING_INSTALL_TITLE"] = "Instalação do módulo de Reuniões e Briefings ";
$MESS["MEETING_UNINSTALL_TITLE"] = "Desinstalação do módulo de Reuniões e Briefings ";
$MESS["MEETING_INSTALL_PUBLIC"] = "Instalar seção pública?";
$MESS["MEETING_INSTALL_GO"] = "Abrir a seção pública";
?>