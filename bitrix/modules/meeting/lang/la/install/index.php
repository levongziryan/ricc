<?
$MESS["MEETING_MODULE_NAME"] = "Reuniones y Sesiones";
$MESS["MEETING_MODULE_DESCRIPTION"] = "Módulo para organizar las reuniones y sesiones informativas.";
$MESS["MEETING_INSTALL_TITLE"] = "Instalación del Módulo de Reuniones y Sesiones";
$MESS["MEETING_UNINSTALL_TITLE"] = "Desintalación del módulo de Reuniones y Sesiones informativas";
$MESS["MEETING_INSTALL_PUBLIC"] = "¿Instalar sección pública?";
$MESS["MEETING_INSTALL_GO"] = "Abrir sección pública";
?>