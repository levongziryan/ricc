<?
$MESS["MEETING_MODULE_NAME"] = "Meetings und Briefings";
$MESS["MEETING_MODULE_DESCRIPTION"] = "Ein Modul zur Vorbereitung und Durchführung von Besprechungen und Sitzungen.";
$MESS["MEETING_INSTALL_TITLE"] = "Installation des Moduls \"Meetings und Briefings\"";
$MESS["MEETING_UNINSTALL_TITLE"] = "Deinstallation des Moduls \"Meetings und Briefings\"";
$MESS["MEETING_INSTALL_PUBLIC"] = "Sollen Demodaten im Bereich Ansicht installiert werden?";
$MESS["MEETING_INSTALL_GO"] = "In den Bereich \"Ansicht\" wechseln";
?>