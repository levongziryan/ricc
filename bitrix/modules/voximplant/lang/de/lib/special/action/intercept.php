<?
$MESS["VOX_ACTION_INTERCEPT_CALL_NOT_FOUND"] = "Es wurde kein Anruf zur Übernahme gefunden";
$MESS["VOX_ACTION_INTERCEPT_HANGUP_TO_ACCEPT_CALL"] = "Bitte aufhängen, um den Anruf anzunehmen";
$MESS["VOX_ACTION_INTERCEPT_LICENSE_ERROR"] = "Anrufübernahme ist in Ihrem Tarif nicht verfügbar";
?>