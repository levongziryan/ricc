<?
$MESS["CALL_ENTITY_ID_FIELD"] = "ID";
$MESS["CALL_ENTITY_SEARCH_ID_FIELD"] = "Suchzeile";
$MESS["CALL_ENTITY_CALL_ID_FIELD"] = "Anruf-ID";
$MESS["CALL_ENTITY_CALLER_ID_FIELD"] = "Nummer des eingehenden Anrufs";
$MESS["CALL_ENTITY_STATUS_FIELD"] = "Status";
$MESS["CALL_ENTITY_ACCESS_URL_FIELD"] = "URL der Anrufverwaltung";
$MESS["CALL_ENTITY_DATE_CREATE_FIELD"] = "Anruf angefangen am";
$MESS["CALL_ENTITY_USER_ID_FIELD"] = "Nutzer-ID";
$MESS["CALL_ENTITY_TRANSFER_USER_ID_FIELD"] = "Nutzer-ID für die Anrufweiterleitung";
$MESS["CALL_ENTITY_CONFIG_ID_FIELD"] = "Einstellungen-ID";
$MESS["CALL_ENTITY_PORTAL_USER_ID_FIELD"] = "Nutzer-ID auf dem Portal";
?>