<?php
$MESS["VI_TTS_SPEED_X_SLOW"] = "Sehr langsam";
$MESS["VI_TTS_SPEED_SLOW"] = "Langsam";
$MESS["VI_TTS_SPEED_MEDIUM"] = "Normal";
$MESS["VI_TTS_SPEED_FAST"] = "Schnell";
$MESS["VI_TTS_SPEED_X_FAST"] = "Sehr schnell";
