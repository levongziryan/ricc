<?php
$MESS["VI_TTS_VOLUME_X_SOFT"] = "Sehr leise";
$MESS["VI_TTS_VOLUME_SOFT"] = "Leise";
$MESS["VI_TTS_VOLUME_MEDIUM"] = "Normal";
$MESS["VI_TTS_VOLUME_LOUD"] = "Laut";
$MESS["VI_TTS_VOLUME_X_LOUD"] = "Sehr laut";
