<?
$MESS["ADDRESS_VERIFICATION_NOTIFY"] = "Dokumente, die erforderlich sind, um eine Telefonnummer mieten zu können, wurden geprüft.";
$MESS["ADDRESS_VERIFICATION_NOTIFY_HEAD_ACCEPTED"] = "Ihre Dokumente wurden geprüft und genehmigt.";
$MESS["ADDRESS_VERIFICATION_NOTIFY_BODY_ACCEPTED"] = "Jetzt können Sie eine Telefonnummer mieten und alle Abrufe direkt in Ihrem BItrix24 verwalten.";
$MESS["ADDRESS_VERIFICATION_NOTIFY_LINK_ACCEPTED"] = "Nummern verwalten";
$MESS["ADDRESS_VERIFICATION_NOTIFY_HEAD_REJECTED"] = "Ihre Dokumente wurden geprüft und abgelehnt.";
$MESS["ADDRESS_VERIFICATION_NOTIFY_BODY_REJECTED"] = "Grund: #REJECT_REASON#. Klicken Sie bitte auf \"Nummern verwalten\", um Dokumente erneut einzureichen.";
$MESS["ADDRESS_VERIFICATION_NOTIFY_LINK_REJECTED"] = "Nummern verwalten";
?>