<?
$MESS["IVR_LICENSE_POPUP_HEADER"] = "Verfügbar nur in den Tarifen Standard und Professional.";
$MESS["IVR_LICENSE_POPUP_TEXT"] = "Sprachmenü (IVR) wird eingehende Anrufe unter zuständigen Mitarbeitern oder Abteilungen in Ihrem Unternehmen verteilen. Dabei gibt es einige Optionen für die Einstellung des Sprachmenüs:";
$MESS["IVR_LICENSE_POPUP_ITEM_1"] = "Verschachtelte Menüs";
$MESS["IVR_LICENSE_POPUP_ITEM_2"] = "Nutzen Sie benutzerdefinierte Audiodateien, um Nachrichten zu Ihren Kunden zu sprechen";
$MESS["IVR_LICENSE_POPUP_ITEM_3"] = "Nutzen Sie automatische Sprachtextprogramme (verschiedene Stimmen sind verfügbar; Geschwindigkeit und Lautstärke können eingestellt werden)";
$MESS["IVR_LICENSE_POPUP_ITEM_4"] = "Anruf an einen Mitarbeiter, eine Queue-Gruppe oder externe Nummer weiterleiten";
$MESS["IVR_LICENSE_POPUP_ITEM_5"] = "Anruf an eine externe Durchwahlnummer weiterleiten";
$MESS["IVR_LICENSE_POPUP_MORE"] = "Mehr erfahren";
$MESS["IVR_LICENSE_POPUP_FOOTER"] = "Sprachmenü (IVR) ist nur in den Tarifen Standard und Professional verfügbar.";
?>