<?
$MESS["STATISTIC_ENTITY_ID_FIELD"] = "ID";
$MESS["STATISTIC_ENTITY_ACCOUNT_ID_FIELD"] = "Account";
$MESS["STATISTIC_ENTITY_APPLICATION_ID_FIELD"] = "ID der Anwendung";
$MESS["STATISTIC_ENTITY_APPLICATION_NAME_FIELD"] = "Name der Anwendung";
$MESS["STATISTIC_ENTITY_PORTAL_USER_ID_FIELD"] = "Nutzer-ID";
$MESS["STATISTIC_ENTITY_PHONE_NUMBER_FIELD"] = "Telefon";
$MESS["STATISTIC_ENTITY_INCOMING_FIELD"] = "Eingehend";
$MESS["STATISTIC_ENTITY_CALL_ID_FIELD"] = "Anruf-ID";
$MESS["STATISTIC_ENTITY_CALL_DIRECTION_FIELD"] = "Richtung";
$MESS["STATISTIC_ENTITY_CALL_DURATION_FIELD"] = "Dauer";
$MESS["STATISTIC_ENTITY_CALL_START_DATE_FIELD"] = "Startdatum";
$MESS["STATISTIC_ENTITY_CALL_STATUS_FIELD"] = "Status";
$MESS["STATISTIC_ENTITY_COST_FIELD"] = "Preis";
$MESS["STATISTIC_ENTITY_COST_CURRENCY_FIELD"] = "Währung";
$MESS["STATISTIC_ENTITY_CALL_FAILED_CODE_FIELD"] = "Code";
$MESS["STATISTIC_ENTITY_CALL_FAILED_REASON_FIELD"] = "Codebeschreibung";
$MESS["STATISTIC_ENTITY_CALL_RECORD_ID_FIELD"] = "Aufzeichnen";
$MESS["STATISTIC_ENTITY_CALL_WEBDAV_ID_FIELD"] = "Eintrag (webdav)";
$MESS["STATISTIC_ENTITY_PORTAL_NUMBER_FIELD"] = "Portalnummer";
$MESS["STATISTIC_ENTITY_CALL_LOG_FIELD"] = "Anruf-Details";
$MESS["STATISTIC_ENTITY_CALL_VOTE_FIELD"] = "Anruf-Ranking";
?>