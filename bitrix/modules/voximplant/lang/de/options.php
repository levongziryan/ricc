<?
$MESS["VI_TAB_SETTINGS"] = "Einstellungen";
$MESS["VI_TAB_TITLE_SETTINGS_2"] = "Verbindungsparameter";
$MESS["VI_ACCOUNT_ERROR_PUBLIC"] = "Sie haben eine ungültige öffentliche Adresse eingegeben.";
$MESS["VI_ACCOUNT_URL"] = "Öffentliche Adresse der Website";
$MESS["VI_ACCOUNT_DEBUG"] = "Debug-Modus";
$MESS["VI_ACCOUNT_NAME"] = "Name des Accounts";
$MESS["VI_ACCOUNT_BALANCE"] = "Guthaben";
$MESS["VI_ACCOUNT_ERROR_LICENSE"] = "Überprüfung des Lizenzschlüssels ist fehlgeschlagen. Stellen Sie bitte sicher, dass die eingegebenen Daten korrekt sind, dann versuchen Sie erneut.";
$MESS["VI_ACCOUNT_ERROR"] = "Erhalten von Daten ist fehlgeschlagen. Versuchen Sie bitte später erneut.";
?>