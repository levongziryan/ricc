<?
$MESS["VI_MODULE_NAME_2"] = "Telefonie";
$MESS["VI_MODULE_DESCRIPTION_2"] = "Unterstützt die Cloud-basierte Telefonie.";
$MESS["VI_INSTALL_TITLE_2"] = "Das Modul Telefonie installieren";
$MESS["VI_UNINSTALL_TITLE_2"] = "Das Modul Telefonie deinstallieren";
$MESS["VI_CHECK_PULL"] = "Das Modul \"Push and Pull\" ist nicht installiert, oder das Modul nginx-push-stream-module ist nicht konfiguriert.";
$MESS["VI_CHECK_IM"] = "Das Modul Instant Messenger ist nicht installiert.";
$MESS["VI_CHECK_MAIN"] = "Aktualisieren Sie bitte den Systemkernel (das Hauptmodul) auf die Version 14.9.2.";
$MESS["VI_CHECK_INTRANET"] = "Aktualisieren Sie bitte das Modul Intranet auf die Version 14.5.6.";
$MESS["VI_CHECK_INTRANET_INSTALL"] = "Das Modul Intranet ist nicht installiert.";
$MESS["VI_CHECK_PUBLIC_PATH"] = "Sie haben eine ungültige öffentliche Adresse eingegeben.";
$MESS["VOXIMPLANT_ROLE_ADMIN"] = "Administrator";
$MESS["VOXIMPLANT_ROLE_CHIEF"] = "Geschäftsführer";
$MESS["VOXIMPLANT_ROLE_DEPARTMENT_HEAD"] = "Abteilungsleiter";
$MESS["VOXIMPLANT_ROLE_MANAGER"] = "Manager";
$MESS["VOXIMPLANT_DEFAULT_GROUP"] = "Standardmäßige Queue-Gruppe";
$MESS["VOXIMPLANT_DEFAULT_IVR"] = "Standardmenü";
$MESS["VOXIMPLANT_DEFAULT_ITEM"] = "Danke für Ihren Anruf. Wählen Sie bitte das 0, um mit einem Operator verbunden zu werden.";
?>