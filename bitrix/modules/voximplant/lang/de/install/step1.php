<?
$MESS["VI_PUBLIC_PATH_DESC"] = "Eine gültige öffentliche Adresse der Website muss eingegeben werden, damit Telefonie korrekt funktioniert.";
$MESS["VI_PUBLIC_PATH_DESC_2"] = "Wenn der Zugriff auf die Website via Internet eingeschränkt ist, muss der Zugriff auf bestimmte Seiten gewährt werden. Nähere Informationen dazu finden Sie in der #LINK_START#Dokumentation#LINK_END#.";
$MESS["VI_PUBLIC_PATH"] = "Öffentliche Adresse der Website:";
?>