<?
$MESS["VI_USER_PASS_ERROR"] = "Passwort ist nicht korrekt";
$MESS["VI_ERROR_EMAIL_NOT_CONFIRMED"] = "Sie müssen Ihre E-Mail-Adresse bestätigen, um Telefone anschließen zu können.";
$MESS["VI_ERROR_EMAIL_NOT_CONFIRMED_2"] = "Sie müssen Ihre E-Mail-Adresse verifizieren";
$MESS["VI_ERROR_USER_NOT_FOUND"] = "Nutzer wurde nicht gefunden.";
$MESS["VI_ERROR_USER_NOT_REGISTERED"] = "Nutzer ist auf dem Telefonie-Controller nicht registriert";
$MESS["VI_ERROR_USER_NO_EXTRANET"] = "Extranet-Nutzer können keine Telefonie nutzen";
?>