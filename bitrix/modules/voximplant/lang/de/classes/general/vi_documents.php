<?
$MESS["VI_DOCS_COUNTRY_RU"] = "Russland";
$MESS["VI_DOCS_STATUS_REQUIRED"] = "Unterlagen werden erwartet";
$MESS["VI_DOCS_STATUS_IN_PROGRESS"] = "Unterlagen werden geprüft";
$MESS["VI_DOCS_STATUS_VERIFIED"] = "Unterlagen wurden geprüft";
$MESS["VI_DOCS_STATUS_DECLINED"] = "Unterlagen wurden abgelehnt";
$MESS["VI_DOCS_STATUS_REJECTED"] = "Unterlagen wurden abgelehnt";
$MESS["VI_DOCS_DOCUMENT_STATUS_ACCEPTED"] = "Geprüft";
$MESS["VI_DOCS_DOCUMENT_STATUS_IN_PROGRESS"] = "Wird geprüft";
$MESS["VI_DOCS_DOCUMENT_STATUS_INCOMPLETE_SET"] = "Unterlagen sind nicht vollständig";
$MESS["VI_DOCS_DOCUMENT_STATUS_REJECTED"] = "Abgelehnt";
$MESS["VI_DOCS_IS_INDIVIDUAL_Y"] = "Natürliche Person";
$MESS["VI_DOCS_IS_INDIVIDUAL_N"] = "Juristische Person";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY"] = "Dokumente, die erforderlich sind, um eine Telefonnummer mieten zu können, wurden geprüft.";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_HEAD_VERIFIED"] = "Ihre Dokumente wurden geprüft und genehmigt.";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_BODY_VERIFIED"] = "Jetzt können Sie eine Telefonnummer mieten und alle Abrufe direkt in Ihrem BItrix24 verwalten.";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_LINK_VERIFIED"] = "Nummern verwalten";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_HEAD_REQUIRED"] = "Ihre Dokumente wurden geprüft und abgelehnt.";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_BODY_REQUIRED"] = "Grund: #REJECT_REASON#. Klicken Sie bitte auf \"Nummern verwalten\", um Dokumente erneut einzureichen.";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_LINK_REQUIRED"] = "Nummern verwalten";
?>
