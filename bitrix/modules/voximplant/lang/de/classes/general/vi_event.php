<?
$MESS["ERROR_WORK_PHONE"] = "Das Feld \"Telefon geschäftlich\" ist nicht korrekt.";
$MESS["ERROR_PERSONAL_PHONE"] = "Das Feld \"Telefon\" ist nicht korrekt.";
$MESS["ERROR_PERSONAL_MOBILE"] = "Das Feld \"Mobiltelefon\" ist nicht korrekt.";
$MESS["ERROR_NUMBER"] = "Die Telefonnummer muss im internationalen Format sein.<br/> Zum Beispiel: +49 1234 5678";
$MESS["ERROR_PHONE_INNER"] = "Die Durchwahl ist nicht gültig, weil sie bereits durch einen anderen Mitarbeiter genutzt wird.";
$MESS["ERROR_PHONE_INNER_2"] = "Das Feld \"Interne Durchwahl\" ist nicht korrekt ausgefüllt: die Nummer muss eine Zahl von 1 bis 9999 sein.";
$MESS["VI_EVENTS_NOTIFICATIONS"] = "Telefonie-Benachrichtigungen";
?>