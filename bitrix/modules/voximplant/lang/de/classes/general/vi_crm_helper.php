<?
$MESS["VI_CRM_CALL_TITLE"] = "Anruf";
$MESS["VI_CRM_CALLBACK_TITLE"] = "Rückruf";
$MESS["VI_CRM_CALL_DURATION"] = "Anrufdauer: #DURATION#.";
$MESS["VI_CRM_CALL_TO_PORTAL_NUMBER"] = "Anruf geleitet an: #PORTAL_NUMBER#.";
$MESS["VI_CRM_CALL_STATUS"] = "Anrufstatus:";
$MESS["VI_CRM_CALL_CANCEL"] = "Anruf abgebrochen";
$MESS["VI_CRM_CALL_INCOMING"] = "Eingehender Anruf";
$MESS["VI_CRM_CALL_OUTGOING"] = "Ausgehender Anruf";
$MESS["VI_CRM_CALL_MISSED"] = "Verpasster Anruf";
$MESS["VI_CRM_ACTIVITY_SUBJECT_INCOMING"] = "Eingehender von #NUMBER#";
$MESS["VI_CRM_ACTIVITY_SUBJECT_OUTGOING"] = "Ausgehender an #NUMBER#";
$MESS["VI_CRM_ACTIVITY_SUBJECT_CALLBACK"] = "Rückruf an #NUMBER#";
$MESS["VI_CRM_CALL_CALLBACK"] = "Rückruf";
$MESS["VI_WORKTIME_SKIPPED_CALL"] = "Anruf erfolgte außerhalb der Bürozeiten.";
?>