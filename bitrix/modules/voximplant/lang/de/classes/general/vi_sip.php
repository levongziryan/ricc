<?
$MESS["VI_SIP_CONFIG_ID_NULL"] = "Die ID der Nummerkonfiguration ist nicht angegeben.";
$MESS["VI_SIP_NUMBER_EXISTS"] = "Diese Telefonnummer wurde bereits registriert.";
$MESS["VI_SIP_CONFIG_NOT_FOUND"] = "Konfiguration wurde nicht gefunden.";
$MESS["VI_SIP_NUMBER_ERR"] = "Die Nummer muss im vollständigen internationalen Format angegeben werden.";
$MESS["VI_SIP_NUMBER_0"] = "SIP-Telefonnummer ist nicht angegeben.";
$MESS["VI_SIP_SERVER_100"] = "Serveradresse darf nicht über 100 Zeichen enthalten.";
$MESS["VI_SIP_SERVER_0"] = "Serveradresse ist nicht angegeben.";
$MESS["VI_SIP_LOGIN_100"] = "Serverlogin darf nicht über 100 Zeichen enthalten.";
$MESS["VI_SIP_LOGIN_0"] = "Serverlogin ist nicht angegeben.";
$MESS["VI_SIP_PASSWORD_100"] = "Serverpasswort darf nicht über 100 Zeichen enthalten.";
$MESS["VI_SIP_INC_SERVER_100"] = "Serveradresse für eingehende Anrufe darf nicht über 100 Zeichen enthalten.";
$MESS["VI_SIP_INC_SERVER_0"] = "Serveradresse für eingehende Anrufe ist nicht angegeben.";
$MESS["VI_SIP_INC_LOGIN_100"] = "Serverlogin für eingehende Anrufe darf nicht über 100 Zeichen enthalten.";
$MESS["VI_SIP_INC_LOGIN_0"] = "Serverlogin  für eingehende Anrufe ist nicht angegeben.";
$MESS["VI_SIP_INC_PASSWORD_100"] = "Serverpasswort für eingehende Anrufe darf nicht über 100 Zeichen enthalten.";
$MESS["VI_SIP_NUMBER_ERR_2"] = "Sie müssen eine Telefonnummer eingeben, welche für ausgehende Anrufe von Ihrer Vermittlungsstelle angezeigt";
$MESS["VI_SIP_TITLE_EXISTS"] = "Der angegebene Name der Verbindung wird im System bereits genutzt.";
$MESS["VI_SIP_SEARCH_ID_EXISTS"] = "Die angegebene Verbindungs-ID wird im System bereits genutzt.";
$MESS["VI_SIP_TYPE_ERR"] = "Typ der Vermittlungsstelle ist nicht angegeben.";
$MESS["VI_SIP_NEW_PBX"] = "Neue Vermittlungsstelle";
$MESS["VI_SIP_ADD_CLOUD_ERR"] = "Sie können nicht mehr als #NUMBER# Vermittlungsstellen in der Cloud anbinden.";
?>