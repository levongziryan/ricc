<?
$MESS["VI_ACCOUNT_BALANCE"] = "Le reste final";
$MESS["VI_TAB_SETTINGS"] = "Paramètres";
$MESS["VI_ACCOUNT_ERROR_PUBLIC"] = "Vous avez entré une adresse publique valide.";
$MESS["VI_ACCOUNT_NAME"] = "Nom du compte";
$MESS["VI_ACCOUNT_ERROR_LICENSE"] = "Erreur vérification de la clé de licence. S'il vous plaît vérifier votre saisie et essayez à nouveau.";
$MESS["VI_TAB_TITLE_SETTINGS"] = "Paramètres d'intégration";
$MESS["VI_ACCOUNT_ERROR"] = "Erreur d'obtention des données. S'il vous plaît réessayer plus tard.";
$MESS["VI_ACCOUNT_URL"] = "Adresse publique du site";
$MESS["VI_ACCOUNT_DEBUG"] = "Le mode de débogage";
$MESS["VI_TAB_TITLE_SETTINGS_2"] = "Paramètres de connexion";
?>