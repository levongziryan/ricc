<?
$MESS["VI_SIP_INC_SERVER_100"] = "Adresse du serveur d'appel entrant ne doit pas dépasser 100 caractères.";
$MESS["VI_SIP_SERVER_100"] = "Adresse du serveur ne doit pas dépasser 100 caractères.";
$MESS["VI_SIP_NUMBER_ERR_2"] = "Vous devez entrer un numéro de téléphone qui sera affiché pour les appels sortants de votre PBX";
$MESS["VI_SIP_ADD_CLOUD_ERR"] = "Vous ne pouvez pas connecter plus de #NUMBER# cloud hébergé PBX.";
$MESS["VI_SIP_NUMBER_0"] = "Numéro de téléphone SIP est pas spécifié.";
$MESS["VI_SIP_INC_LOGIN_100"] = "Connexion pour le serveur d'appel entrant ne doit pas dépasser 100 caractères.";
$MESS["VI_SIP_LOGIN_100"] = "Connexion au serveur ne doit pas dépasser 100 caractères.";
$MESS["VI_SIP_CONFIG_NOT_FOUND"] = "Configuration n'a pas été trouvé.";
$MESS["VI_SIP_SERVER_0"] = "Adresse du serveur est pas spécifié.";
$MESS["VI_SIP_INC_SERVER_0"] = "Adresse du serveur d'appel entrant est pas spécifié.";
$MESS["VI_SIP_CONFIG_ID_NULL"] = "L'ID de configuration du numéro est pas spécifié.";
$MESS["VI_SIP_LOGIN_0"] = "Connexion au serveur est pas spécifié.";
$MESS["VI_SIP_INC_LOGIN_0"] = "Connexion pour le serveur d'appel entrant est pas spécifié.";
$MESS["VI_SIP_TYPE_ERR"] = "Type de PBX est pas spécifié.";
$MESS["VI_SIP_NEW_PBX"] = "Nouveau PBX";
$MESS["VI_SIP_NUMBER_ERR"] = "Le nombre doit être spécifié au format international complet.";
$MESS["VI_SIP_INC_PASSWORD_100"] = "Mot de passe pour le serveur d'appel entrant ne doit pas dépasser 100 caractères.";
$MESS["VI_SIP_PASSWORD_100"] = "Le mot de passe du serveur ne doit pas dépasser 100 caractères.";
$MESS["VI_SIP_SEARCH_ID_EXISTS"] = "L'identifiant de connexion spécifié est déjà utilisé dans le système.";
$MESS["VI_SIP_TITLE_EXISTS"] = "Le nom de connexion spécifié est déjà utilisé dans le système.";
$MESS["VI_SIP_NUMBER_EXISTS"] = "Ce numéro de téléphone a déjà été enregistrée.";
?>