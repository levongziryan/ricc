<?
$MESS["VI_CRM_CALL_INCOMING"] = "Appel reçu";
$MESS["VI_CRM_CALL_OUTGOING"] = "Appel sortant";
$MESS["VI_CRM_CALL_DURATION"] = "Longueur Appel: #DURATION#";
$MESS["VI_CRM_CALL_TO_PORTAL_NUMBER"] = "Appel routé vers: #PORTAL_NUMBER#.";
$MESS["VI_CRM_CALL_CANCEL"] = "Appel annulé";
$MESS["VI_CRM_CALL_STATUS"] = "Etat appel:";
$MESS["VI_CRM_TITLE"] = "Appel téléphonique";
$MESS["VI_CRM_CALL_TITLE"] = "Appel téléphonique";
$MESS["VI_CRM_CALLBACK_TITLE"] = "Rappel";
$MESS["VI_CRM_CALL_MISSED"] = "Appel manqué";
$MESS["VI_CRM_ACTIVITY_SUBJECT_INCOMING"] = "Reçu de #NUMBER#";
$MESS["VI_CRM_ACTIVITY_SUBJECT_OUTGOING"] = "Sortant vers #NUMBER#";
$MESS["VI_CRM_ACTIVITY_SUBJECT_CALLBACK"] = "Rappel à #NUMBER#";
$MESS["VI_CRM_CALL_CALLBACK"] = "Rappel";
$MESS["VI_WORKTIME_SKIPPED_CALL"] = "L'appel a été effectué en dehors des heures de travail.";
?>