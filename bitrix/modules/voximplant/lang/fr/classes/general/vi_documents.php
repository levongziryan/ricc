<?
$MESS["VI_DOCS_COUNTRY_RU"] = "Russie";
$MESS["VI_DOCS_STATUS_REQUIRED"] = "En attente des documents";
$MESS["VI_DOCS_STATUS_IN_PROGRESS"] = "Vérification des documents";
$MESS["VI_DOCS_STATUS_VERIFIED"] = "Documents vérifiés";
$MESS["VI_DOCS_STATUS_DECLINED"] = "Documents rejetés";
$MESS["VI_DOCS_STATUS_REJECTED"] = "Documents rejetés";
$MESS["VI_DOCS_DOCUMENT_STATUS_ACCEPTED"] = "Vérifiés";
$MESS["VI_DOCS_DOCUMENT_STATUS_IN_PROGRESS"] = "Vérification en cours";
$MESS["VI_DOCS_DOCUMENT_STATUS_INCOMPLETE_SET"] = "Documents incomplets";
$MESS["VI_DOCS_DOCUMENT_STATUS_REJECTED"] = "Refusée";
$MESS["VI_DOCS_IS_INDIVIDUAL_Y"] = "Personne naturelle";
$MESS["VI_DOCS_IS_INDIVIDUAL_N"] = "Personne légale";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY"] = "Les documents nécessaires pour louer des numéros de téléphone ont été vérifiés.";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_HEAD_VERIFIED"] = "Vos documents ont été vérifiés.";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_BODY_VERIFIED"] = "Vous pouvez maintenant louer un numéro de téléphone et gérer les appels dans votre Bitrix24 !";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_LINK_VERIFIED"] = "Gérer les numéros";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_HEAD_REQUIRED"] = "Malheureusement vos documents ont été rejetés.";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_BODY_REQUIRED"] = "Raison: #REJECT_REASON#. Veuillez cliquer sur \"Gérer les numéros\" pour renvoyer les documents.";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_LINK_REQUIRED"] = "Gérer les numéros";
?>