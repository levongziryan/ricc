<?
$MESS["ERROR_NUMBER"] = "Le numéro de téléphone doit être indiqué au format international.<br/> Par exemple: +44 20 1234 5678";
$MESS["ERROR_PHONE_INNER_2"] = "Mauvaise valeur saisie dans le champ 'Numéro interne'. Le nombre doit être compris entre 1 et 9999.";
$MESS["ERROR_PHONE_INNER"] = "Le numéro de poste téléphonique est pas valable parce que ce numéro est déjà réservé par un autre employé.";
$MESS["ERROR_PERSONAL_MOBILE"] = "Le champ 'Mobile' est incorrect.";
$MESS["ERROR_WORK_PHONE"] = "Le champ de 'Téléphone au Travail' est incorrect.";
$MESS["ERROR_PERSONAL_PHONE"] = "Le champ 'Téléphone' est incorrect.";
$MESS["VI_EVENTS_NOTIFICATIONS"] = "Notifications de téléphonie";
?>