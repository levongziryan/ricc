<?
$MESS["VI_CHECK_IM"] = "La question a un statut invalide.";
$MESS["VI_CHECK_INTRANET_INSTALL"] = "Le module 'Intranet' n'a pas été installé.";
$MESS["VI_CHECK_MAIN"] = "S'il vous plaît mettre à jour le noyau du système (le module principal) à la version 14.9.2.";
$MESS["VI_CHECK_INTRANET"] = "S'il vous plaît mettre à jour le module Intranet à la version 14.5.6.";
$MESS["VI_CHECK_PUBLIC_PATH"] = "Vous avez entré une adresse publique valide.";
$MESS["VI_MODULE_NAME"] = "Intégration VoxImplant";
$MESS["VI_MODULE_DESCRIPTION"] = "Ajoute VoxImplant support de plate-forme de nuage pour le module Messenger Web.";
$MESS["VI_CHECK_PULL"] = "Le module 'Push and Pull 'est pas installé', ou nginx-push-stream-module est pas configuré.";
$MESS["VI_UNINSTALL_TITLE"] = "Voximplant intégration »Module désinstallation";
$MESS["VI_INSTALL_TITLE"] = "Installation du module Voximplant intégration";
$MESS["VI_MODULE_NAME_2"] = "Téléphonie";
$MESS["VI_MODULE_DESCRIPTION_2"] = "Fournit une assistance téléphonie en dématérialisé.";
$MESS["VI_INSTALL_TITLE_2"] = "Installation du module Telephony";
$MESS["VI_UNINSTALL_TITLE_2"] = "Désinstallation du module Telephony";
$MESS["VOXIMPLANT_ROLE_ADMIN"] = "Administrateur";
$MESS["VOXIMPLANT_ROLE_CHIEF"] = "Directeur";
$MESS["VOXIMPLANT_ROLE_DEPARTMENT_HEAD"] = "Chef de département";
$MESS["VOXIMPLANT_ROLE_MANAGER"] = "Manager";
$MESS["VOXIMPLANT_DEFAULT_GROUP"] = "Groupe de file d’attente par défaut";
$MESS["VOXIMPLANT_DEFAULT_IVR"] = "Menu par défaut";
$MESS["VOXIMPLANT_DEFAULT_ITEM"] = "Merci de nous avoir contactés. Veuillez composer le 0 pour être connecté à un opérateur.";
?>