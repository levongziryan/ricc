<?
$MESS["VI_PUBLIC_PATH_DESC"] = "Pour la téléphonie fonctionne correctement, une adresse de site public valide doit être saisi.";
$MESS["VI_PUBLIC_PATH_DESC_2"] = "Si l'accès au site à partir de l'Internet est limité, l'accès doit être accordé à certaines pages. Les détails concernant ce qui peut être trouvé dans la #LINK_START#documentation#LINK_END#.";
$MESS["VI_PUBLIC_PATH"] = "Adresse publique du site:";
?>