<?
$MESS["CALL_ENTITY_ID_FIELD"] = "ID";
$MESS["CALL_ENTITY_SEARCH_ID_FIELD"] = "Chaîne de recherche";
$MESS["CALL_ENTITY_STATUS_FIELD"] = "Statut";
$MESS["CALL_ENTITY_CALL_ID_FIELD"] = "Appel ID";
$MESS["CALL_ENTITY_DATE_CREATE_FIELD"] = "Appel a commencé le";
$MESS["CALL_ENTITY_CONFIG_ID_FIELD"] = "Réglage d'identifiant";
$MESS["CALL_ENTITY_USER_ID_FIELD"] = "Identifiant de l'utilisateur";
$MESS["CALL_ENTITY_TRANSFER_USER_ID_FIELD"] = "ID utilisateur pour redirection";
$MESS["CALL_ENTITY_CALLER_ID_FIELD"] = "Numéro d'appel";
$MESS["CALL_ENTITY_ACCESS_URL_FIELD"] = "La gestion Appel URL";
$MESS["CALL_ENTITY_PORTAL_USER_ID_FIELD"] = "ID utilisateur du portail";
?>