<?
$MESS["ADDRESS_VERIFICATION_NOTIFY"] = "Les documents nécessaires pour louer des numéros de téléphone ont été vérifiés.";
$MESS["ADDRESS_VERIFICATION_NOTIFY_HEAD_ACCEPTED"] = "Vos documents ont été vérifiés.";
$MESS["ADDRESS_VERIFICATION_NOTIFY_BODY_ACCEPTED"] = "Vous pouvez maintenant louer un numéro de téléphone et gérer les appels dans votre Bitrix24 !";
$MESS["ADDRESS_VERIFICATION_NOTIFY_LINK_ACCEPTED"] = "Gérer les numéros";
$MESS["ADDRESS_VERIFICATION_NOTIFY_HEAD_REJECTED"] = "Malheureusement vos documents ont été rejetés.";
$MESS["ADDRESS_VERIFICATION_NOTIFY_BODY_REJECTED"] = "Raison: #REJECT_REASON#. Veuillez cliquer sur \"Gérer les numéros\" pour renvoyer les documents.";
$MESS["ADDRESS_VERIFICATION_NOTIFY_LINK_REJECTED"] = "Gérer les numéros";
?>