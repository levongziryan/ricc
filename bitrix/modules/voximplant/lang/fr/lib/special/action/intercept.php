<?
$MESS["VOX_ACTION_INTERCEPT_CALL_NOT_FOUND"] = "Impossible de trouver un appel à intercepter";
$MESS["VOX_ACTION_INTERCEPT_HANGUP_TO_ACCEPT_CALL"] = "Raccrochez pour accepter un appel";
$MESS["VOX_ACTION_INTERCEPT_LICENSE_ERROR"] = "L'interception d'appel n'est pas disponible avec votre offre";
?>