<?
$MESS["INCOMING_CONFIG_ENTITY_ID_FIELD"] = "ID";
$MESS["INCOMING_CONFIG_ENTITY_TYPE_FIELD"] = "Entité";
$MESS["INCOMING_CONFIG_ENTITY_SEARCH_ID_FIELD"] = "Chaîne de recherche";
$MESS["INCOMING_CONFIG_ENTITY_PHONE_NAME_FIELD"] = "Dénomination";
$MESS["INCOMING_CONFIG_ENTITY_PHONE_COUNTRY_CODE_FIELD"] = "Code du pays du numéro loué";
$MESS["INCOMING_CONFIG_ENTITY_PHONE_VERIFIED_FIELD"] = "Statut de vérification du numéro de télépohone";
$MESS["INCOMING_CONFIG_ENTITY_CRM_FIELD"] = "Vérifiez le numéro d'appel contre la base de données CRM";
$MESS["INCOMING_CONFIG_ENTITY_CRM_RULE_FIELD"] = "Règles pour le renvoi d'appel si Responsable gestionnaire absente";
$MESS["INCOMING_CONFIG_ENTITY_CRM_SOURCE_FIELD"] = "Source d'appel pour l'entité CRM";
$MESS["INCOMING_CONFIG_ENTITY_CRM_CREATE_FIELD"] = "Règle si aucun enregistrement dans CRM";
$MESS["INCOMING_CONFIG_ENTITY_QUEUE_TYPE_FIELD"] = "Distribution des appels entre les employés";
$MESS["INCOMING_CONFIG_ENTITY_QUEUE_TIME_FIELD"] = "Nombre de sonneries avant le transfert";
$MESS["INCOMING_CONFIG_ENTITY_QUEUE_LAST_ID_FIELD"] = "Commutateur de l'opérateur";
$MESS["INCOMING_CONFIG_ENTITY_DIRECT_CODE_FIELD"] = "Extension traitement des nombres";
$MESS["INCOMING_CONFIG_ENTITY_DIRECT_CODE_RULE_FIELD"] = "Règles pour l'expédition si l'utilisateur demandé est absent";
$MESS["INCOMING_CONFIG_ENTITY_VOTE_FIELD"] = "Évaluation de la qualité du service";
$MESS["INCOMING_CONFIG_ENTITY_RECORDING_FIELD"] = "Enregistrer tous les appels";
$MESS["INCOMING_CONFIG_ENTITY_RECORDING_NOTICE"] = "Alerte \"votre appel sera enregistré\"";
$MESS["INCOMING_CONFIG_ENTITY_RECORDING_TIME_FIELD"] = "Appel enregistré de période de rétention";
$MESS["INCOMING_CONFIG_ENTITY_MELODY_LANG_FIELD"] = "Par défaut stand-by langue à tons";
$MESS["INCOMING_CONFIG_ENTITY_MELODY_WELCOME_FIELD"] = "Message de bienvenue";
$MESS["INCOMING_CONFIG_ENTITY_MELODY_WELCOME_ENABLE_FIELD"] = "Activer la tonalité de bienvenue";
$MESS["INCOMING_CONFIG_ENTITY_MELODY_WAIT_FIELD"] = "Tonalité de garde Appel";
$MESS["INCOMING_CONFIG_ENTITY_MELODY_HOLD_FIELD"] = "Attente signal sonore";
$MESS["INCOMING_CONFIG_ENTITY_MELODY_RECORDING_FIELD"] = "Enregistrement vocal \"Votre appel sera enregistré\"";
$MESS["INCOMING_CONFIG_ENTITY_MELODY_VOTE_FIELD"] = "Musique d'évaluation de qualité";
$MESS["INCOMING_CONFIG_ENTITY_MELODY_VOTE_END_FIELD"] = "Musique de fin d'évaluation de qualité";
$MESS["INCOMING_CONFIG_ENTITY_MELODY_VOICEMAIL_FIELD"] = "Messagerie vocale ton";
$MESS["INCOMING_CONFIG_ENTITY_VOICEMAIL_FIELD"] = "Utilisez la messagerie vocale";
$MESS["INCOMING_CONFIG_ENTITY_NO_ANSWER_RULE_FIELD"] = "Aucune règle de réponse";
$MESS["INCOMING_CONFIG_ENTITY_DATE_DELETE_FIELD"] = "Annulée le";
$MESS["INCOMING_CONFIG_ENTITY_TO_DELETE_FIELD"] = "Dele";
$MESS["INCOMING_CONFIG_ENTITY_FORWARD_NUMBER_FIELD"] = "Numéro de transfert";
$MESS["INCOMING_CONFIG_ENTITY_FORWARD_LINE_FIELD"] = "Ligne de transfert";
$MESS["INCOMING_CONFIG_ENTITY_TIMEMAN_FIELD"] = "Travailler soutien à temps";
$MESS["INCOMING_CONFIG_ENTITY_CRM_FORWARD_FIELD"] = "CRM: transfert à gestionnaire";
$MESS["INCOMING_CONFIG_ENTITY_WORKTIME_ENABLE_FIELD"] = "Activer heures de téléphone";
$MESS["INCOMING_CONFIG_ENTITY_WORKTIME_FROM_FIELD"] = "Heures de téléphone commencent à";
$MESS["INCOMING_CONFIG_ENTITY_WORKTIME_TO_FIELD"] = "Heures de téléphone se terminent à";
$MESS["INCOMING_CONFIG_ENTITY_WORKTIME_TIMEZONE_FIELD"] = "Téléphone temps de nombre zone";
$MESS["INCOMING_CONFIG_ENTITY_WORKTIME_HOLIDAYS_FIELD"] = "Vacances cette année";
$MESS["INCOMING_CONFIG_ENTITY_WORKTIME_DAYOFF_FIELD"] = "Jours de congé hebdomadaire";
$MESS["INCOMING_CONFIG_ENTITY_WORKTIME_DAYOFF_RULE_FIELD"] = "Règle des heures creuses";
$MESS["INCOMING_CONFIG_ENTITY_WORKTIME_DAYOFF_NUMBER_FIELD"] = "En avant";
$MESS["INCOMING_CONFIG_ENTITY_WORKTIME_DAYOFF_MELODY_FIELD"] = "Salutation enregistrement pendant les heures creuses";
$MESS["INCOMING_CONFIG_ENTITY_CRM_TRANSFER_CHANGE_FIELD"] = "Mettre automatiquement à jour le responsable quand vous transférez manuellement un appel";
$MESS["INCOMING_CONFIG_ENTITY_IVR_FIELD"] = "Utiliser le menu vocal";
?>