<?
$MESS["IVR_LICENSE_POPUP_HEADER"] = "Disponible uniquement dans les abonnements Standard et Professional.";
$MESS["IVR_LICENSE_POPUP_TEXT"] = "Le menu vocal (SVI) va distribuer les appels entrants entre les employés ou départements concernés de votre société. Différentes options sont disponibles pour configurer un menu vocal totalement fonctionnel :";
$MESS["IVR_LICENSE_POPUP_ITEM_1"] = "Menus imbriqués";
$MESS["IVR_LICENSE_POPUP_ITEM_2"] = "Utilisez des fichiers audio personnalisés pour communiquer des messages aux clients";
$MESS["IVR_LICENSE_POPUP_ITEM_3"] = "Lisez le texte à l’aide d'un moteur de reconnaissance vocale (différentes voix sont disponibles ; options pour régler la vitesse et volume)";
$MESS["IVR_LICENSE_POPUP_ITEM_4"] = "Transférer l’appel à un employé, au groupe de file d’attente ou à un numéro externe";
$MESS["IVR_LICENSE_POPUP_ITEM_5"] = "Transférer l’appel à un numéro d'extension";
$MESS["IVR_LICENSE_POPUP_MORE"] = "En savoir plus";
$MESS["IVR_LICENSE_POPUP_FOOTER"] = "Le menu vocal (SVI) est disponible uniquement dans les abonnements Standard et Professional.";
?>