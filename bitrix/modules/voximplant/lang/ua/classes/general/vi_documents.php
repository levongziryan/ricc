<?
$MESS["VI_DOCS_COUNTRY_RU"] = "Росія";
$MESS["VI_DOCS_STATUS_REQUIRED"] = "Потрібне завантаження документів";
$MESS["VI_DOCS_STATUS_IN_PROGRESS"] = "Документи на перевірці";
$MESS["VI_DOCS_STATUS_VERIFIED"] = "Документи підтверджені";
$MESS["VI_DOCS_DOCUMENT_STATUS_ACCEPTED"] = "Підтверджено";
$MESS["VI_DOCS_DOCUMENT_STATUS_IN_PROGRESS"] = "На перевірці";
$MESS["VI_DOCS_DOCUMENT_STATUS_INCOMPLETE_SET"] = "Не повний набір";
$MESS["VI_DOCS_DOCUMENT_STATUS_REJECTED"] = "Відхилено";
$MESS["VI_DOCS_IS_INDIVIDUAL_Y"] = "Фізична особа";
$MESS["VI_DOCS_IS_INDIVIDUAL_N"] = "Юридична особа";
$MESS["VI_DOCS_STATUS_DECLINED"] = "Документи відхилені";
$MESS["VI_DOCS_STATUS_REJECTED"] = "Документи відхилені";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY"] = "Перевірка документів для оренди номеру завершена";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_HEAD_VERIFIED"] = "Ваші документи перевірені.";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_BODY_VERIFIED"] = "Тепер ви можете орендувати телефонний номер і працювати з дзвінками в своєму Бітрікс24!";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_LINK_VERIFIED"] = "Перейти до управління номерами";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_HEAD_REQUIRED"] = "На жаль, ваші документи не пройшли перевірку.";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_BODY_REQUIRED"] = "Причина відмови: #REJECT_REASON#. Перейдіть на сторінку Управління номерами щоб повторно завантажити документи для перевірки.";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_LINK_REQUIRED"] = "Перейти до управління номерами";
?>