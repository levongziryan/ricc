<?
$MESS["VI_DOCS_COUNTRY_RU"] = "Rusia";
$MESS["VI_DOCS_STATUS_REQUIRED"] = "Documentación pendiente";
$MESS["VI_DOCS_STATUS_IN_PROGRESS"] = "Verificar documentación";
$MESS["VI_DOCS_STATUS_VERIFIED"] = "Documentación verificada";
$MESS["VI_DOCS_DOCUMENT_STATUS_ACCEPTED"] = "Verificado ";
$MESS["VI_DOCS_DOCUMENT_STATUS_IN_PROGRESS"] = "Verificación en progreso";
$MESS["VI_DOCS_DOCUMENT_STATUS_INCOMPLETE_SET"] = "Documentación incompleta";
$MESS["VI_DOCS_DOCUMENT_STATUS_REJECTED"] = "Declinado";
$MESS["VI_DOCS_IS_INDIVIDUAL_Y"] = "Persona natural";
$MESS["VI_DOCS_IS_INDIVIDUAL_N"] = "Persona jurídica";
$MESS["VI_DOCS_STATUS_DECLINED"] = "Documentos rechazados";
$MESS["VI_DOCS_STATUS_REJECTED"] = "Documentos rechazados";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY"] = "Documentación requerida para alquilar números de teléfono ha sido verificada.";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_HEAD_VERIFIED"] = "La documentación ha sido verificada.";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_BODY_VERIFIED"] = "Ahora puede alquilar un número de teléfono y manejar llamadas en su Bitrix24!";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_LINK_VERIFIED"] = "Administrar Números";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_HEAD_REQUIRED"] = "Por desgracia, la documentación ha sido rechazada.";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_BODY_REQUIRED"] = "Motivo: #REJECT_REASON#. Por favor, haga clic en \"Administrar números\" para presentar la documentación de nuevo.";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_LINK_REQUIRED"] = "Administrador de Números";
?>