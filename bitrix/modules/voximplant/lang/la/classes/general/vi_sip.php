<?
$MESS["VI_SIP_CONFIG_ID_NULL"] = "No se especifica el número de ID de la configuración.";
$MESS["VI_SIP_NUMBER_EXISTS"] = "Este número de teléfono ya ha sido registrado.";
$MESS["VI_SIP_CONFIG_NOT_FOUND"] = "La configuración no se ha encontrado.";
$MESS["VI_SIP_NUMBER_ERR"] = "El número debe ser especificado en formato internacional.";
$MESS["VI_SIP_NUMBER_0"] = "No se especifica el número de teléfono SIP.";
$MESS["VI_SIP_SERVER_100"] = "Dirección del servidor no debe exceder los 100 caracteres.";
$MESS["VI_SIP_SERVER_0"] = "No se especifica la dirección del servidor.";
$MESS["VI_SIP_LOGIN_100"] = "Inicio de sesión en el servidor no debe superar los 100 caracteres.";
$MESS["VI_SIP_LOGIN_0"] = "No se especifica servidor de inicio de sesión.";
$MESS["VI_SIP_PASSWORD_100"] = "La contraseña del servidor no debe exceder los 100 caracteres.";
$MESS["VI_SIP_INC_SERVER_100"] = "Dirección de servidor de llamada entrante no debe superar los 100 caracteres.";
$MESS["VI_SIP_INC_SERVER_0"] = "No se ha especificado dirección de servidor de llamada entrante.";
$MESS["VI_SIP_INC_LOGIN_100"] = "Inicio de sesión para servidor de llamada entrante no debe superar los 100 caracteres.";
$MESS["VI_SIP_INC_LOGIN_0"] = "No se ha especificado Inicio de sesión para el servidor de llamadas entrantes.";
$MESS["VI_SIP_INC_PASSWORD_100"] = "Contraseña para el servidor de llamadas entrantes no debe superar los 100 caracteres.";
$MESS["VI_SIP_NUMBER_ERR_2"] = "Debe introducir un número de teléfono que se muestra para las llamadas salientes de su PBX";
$MESS["VI_SIP_TITLE_EXISTS"] = "El nombre de conexión especificado ya está en uso en el sistema.";
$MESS["VI_SIP_SEARCH_ID_EXISTS"] = "El ID de conexión especificado ya está en uso en el sistema.
";
$MESS["VI_SIP_TYPE_ERR"] = "No se especifica el tipo PBX.";
$MESS["VI_SIP_NEW_PBX"] = "Nuevo PBX";
$MESS["VI_SIP_ADD_CLOUD_ERR"] = "No puede conectar más de #NUMBER# en el hosted PBX de la nube.";
?>