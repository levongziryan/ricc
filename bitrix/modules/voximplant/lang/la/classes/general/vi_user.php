<?
$MESS["VI_USER_PASS_ERROR"] = "Contraseña incorrecta";
$MESS["VI_ERROR_EMAIL_NOT_CONFIRMED"] = "Debe confirmar su dirección de e-mail para conectar los teléfonos.";
$MESS["VI_ERROR_EMAIL_NOT_CONFIRMED_2"] = "Debes verificar tu dirección de correo electrónico";
$MESS["VI_ERROR_USER_NOT_FOUND"] = "El usuario no fue encontrado.";
$MESS["VI_ERROR_USER_NOT_REGISTERED"] = "El usuario no está registrado en el controlador de telefonía";
$MESS["VI_ERROR_USER_NO_EXTRANET"] = "Los usuarios de extranet no pueden usar telefonía";
?>