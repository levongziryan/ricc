<?
$MESS["VI_TTS_SPEED_X_SLOW"] = "Muy lento";
$MESS["VI_TTS_SPEED_SLOW"] = "Lento";
$MESS["VI_TTS_SPEED_MEDIUM"] = "Normal";
$MESS["VI_TTS_SPEED_FAST"] = "Rapido";
$MESS["VI_TTS_SPEED_X_FAST"] = "Muy rapido";
$MESS["VI_TTS_VOLUME_X_SOFT"] = "Muy silencioso";
$MESS["VI_TTS_VOLUME_SOFT"] = "Silencioso";
$MESS["VI_TTS_VOLUME_MEDIUM"] = "Normal";
$MESS["VI_TTS_VOLUME_LOUD"] = "Fuerte";
$MESS["VI_TTS_VOLUME_X_LOUD"] = "Muy fuerte";
?>