<?
$MESS["VI_CRM_CALL_TITLE"] = "Llamada telefónica";
$MESS["VI_CRM_CALLBACK_TITLE"] = "Volver a llamar";
$MESS["VI_CRM_CALL_DURATION"] = "Duración de la llamada: #DURATION#";
$MESS["VI_CRM_CALL_TO_PORTAL_NUMBER"] = "Llamada dirigida a: #PORTAL_NUMBER#.";
$MESS["VI_CRM_CALL_STATUS"] = "Estado de la llamada:";
$MESS["VI_CRM_CALL_CANCEL"] = "Llamada cancelada";
$MESS["VI_CRM_CALL_INCOMING"] = "Llamada entrante";
$MESS["VI_CRM_CALL_OUTGOING"] = "Llamada saliente";
$MESS["VI_CRM_CALL_MISSED"] = "Llamada perdida";
$MESS["VI_CRM_ACTIVITY_SUBJECT_INCOMING"] = "Entrante desde #NUMBER#";
$MESS["VI_CRM_ACTIVITY_SUBJECT_OUTGOING"] = "Saliente a #NUMBER#";
$MESS["VI_CRM_ACTIVITY_SUBJECT_CALLBACK"] = "Devolución de llamada a #NUMBER#";
$MESS["VI_CRM_CALL_CALLBACK"] = "Devolución de llamada";
$MESS["VI_WORKTIME_SKIPPED_CALL"] = "La llamada se realizó durante horas no laborables.";
?>