<?
$MESS["ERROR_WORK_PHONE"] = "El campo \"Telfono del trabajo\" no es correcto.";
$MESS["ERROR_PERSONAL_PHONE"] = "El campo \"Telfono\"  no es correcto.";
$MESS["ERROR_PERSONAL_MOBILE"] = "El campo \"Mvil\"  no es correcto.";
$MESS["ERROR_NUMBER"] = "El nmero de telfono debe estar en formato internacional.<br/>Ejemplo: +44 20 1234 5678
";
$MESS["ERROR_PHONE_INNER"] = "El número de teléfono de extensión no es válido porque este número ya está reservado por otro empleado.";
$MESS["ERROR_PHONE_INNER_2"] = "Valor introducido en el campo \"número interno\" es incorrecto. El número debe estar entre 1 y 9999.";
$MESS["VI_EVENTS_NOTIFICATIONS"] = "Notificaciones telefónicas";
?>