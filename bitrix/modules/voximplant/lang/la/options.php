<?
$MESS["VI_TAB_SETTINGS"] = "Configuración";
$MESS["VI_ACCOUNT_NAME"] = "Nombre de la cuenta";
$MESS["VI_ACCOUNT_BALANCE"] = "Balance";
$MESS["VI_ACCOUNT_ERROR_LICENSE"] = "Error al verificar la clave de licencia. Por favor verifique e inténtelo de nuevo.";
$MESS["VI_ACCOUNT_ERROR"] = "Error al obtener los datos. Por favor, inténtelo de nuevo más tarde.";
$MESS["VI_ACCOUNT_ERROR_PUBLIC"] = "Ha introducido una dirección pública inválida.";
$MESS["VI_ACCOUNT_URL"] = "Dirección pública de sitio";
$MESS["VI_ACCOUNT_DEBUG"] = "Modo de depuración";
$MESS["VI_TAB_TITLE_SETTINGS_2"] = "Parámetros de conexión";
?>