<?
$MESS["ADDRESS_VERIFICATION_NOTIFY"] = "Por favor, haga clic en \"Administrar números\" para presentar la documentación de nuevo.";
$MESS["ADDRESS_VERIFICATION_NOTIFY_HEAD_ACCEPTED"] = "La documentación ha sido verificada.";
$MESS["ADDRESS_VERIFICATION_NOTIFY_BODY_ACCEPTED"] = "Ahora puede alquilar un número de teléfono y atender llamadas en su Bitrix24!";
$MESS["ADDRESS_VERIFICATION_NOTIFY_LINK_ACCEPTED"] = "Administrar Números";
$MESS["ADDRESS_VERIFICATION_NOTIFY_HEAD_REJECTED"] = "Por desgracia, la documentación ha sido rechazada.";
$MESS["ADDRESS_VERIFICATION_NOTIFY_BODY_REJECTED"] = "Motivo: #REJECT_REASON#. Por favor, haga clic en \"Administrar números\" para presentar la documentación de nuevo.";
$MESS["ADDRESS_VERIFICATION_NOTIFY_LINK_REJECTED"] = "Administrar Números";
?>