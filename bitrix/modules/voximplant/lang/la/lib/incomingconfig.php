<?
$MESS["INCOMING_CONFIG_ENTITY_ID_FIELD"] = "ID";
$MESS["INCOMING_CONFIG_ENTITY_TYPE_FIELD"] = "Tipo";
$MESS["INCOMING_CONFIG_ENTITY_SEARCH_ID_FIELD"] = "Cadena de búsqueda";
$MESS["INCOMING_CONFIG_ENTITY_PHONE_NAME_FIELD"] = "Nombre";
$MESS["INCOMING_CONFIG_ENTITY_CRM_FIELD"] = "Comprobar número de llamada contra bases de datos del CRM";
$MESS["INCOMING_CONFIG_ENTITY_CRM_RULE_FIELD"] = "Reglas para la transferencia de llamadas en caso el gerente Responsable este ausente";
$MESS["INCOMING_CONFIG_ENTITY_QUEUE_TIME_FIELD"] = "Número de timbres antes de transferir";
$MESS["INCOMING_CONFIG_ENTITY_QUEUE_LAST_ID_FIELD"] = "Interruptor del operador";
$MESS["INCOMING_CONFIG_ENTITY_DIRECT_CODE_RULE_FIELD"] = "Reglas de desvío de llamadas si el usuario requerido está ausente";
$MESS["INCOMING_CONFIG_ENTITY_RECORDING_FIELD"] = "Registro de todas las llamadas";
$MESS["INCOMING_CONFIG_ENTITY_MELODY_WELCOME_FIELD"] = "Mensaje de bienvenida";
$MESS["INCOMING_CONFIG_ENTITY_MELODY_WELCOME_ENABLE_FIELD"] = "Activar el tono de bienvenida";
$MESS["INCOMING_CONFIG_ENTITY_MELODY_WAIT_FIELD"] = "Tono de llamada en espera";
$MESS["INCOMING_CONFIG_ENTITY_MELODY_HOLD_FIELD"] = "Tono de espera";
$MESS["INCOMING_CONFIG_ENTITY_MELODY_VOICEMAIL_FIELD"] = "Tono de correo de voz";
$MESS["INCOMING_CONFIG_ENTITY_VOICEMAIL_FIELD"] = "Usar el correo de voz";
$MESS["INCOMING_CONFIG_ENTITY_DIRECT_CODE_FIELD"] = "Procesamiento de número de extensión";
$MESS["INCOMING_CONFIG_ENTITY_RECORDING_TIME_FIELD"] = "Registro de llamadas y período de retención";
$MESS["INCOMING_CONFIG_ENTITY_NO_ANSWER_RULE_FIELD"] = "Ninguna regla de respuesta";
$MESS["INCOMING_CONFIG_ENTITY_MELODY_LANG_FIELD"] = "Dejar de forma predeterminada el idioma del tono";
?>