<?
$MESS["IVR_LICENSE_POPUP_HEADER"] = "Disponible sólo en planes Standard y Professional.";
$MESS["IVR_LICENSE_POPUP_TEXT"] = "El menú de voz (IVR) distribuirá las llamadas entrantes a través de los empleados o departamentos adecuados de su compañía. Hay varias opciones disponibles para configurar el menú de voz totalmente funcional:";
$MESS["IVR_LICENSE_POPUP_ITEM_1"] = "Menús anidados";
$MESS["IVR_LICENSE_POPUP_ITEM_2"] = "Utilizar archivos de audio personalizados para dejar mensajes a los clientes";
$MESS["IVR_LICENSE_POPUP_ITEM_3"] = "Reproducir el texto usando el motor de voz (diferentes voces disponibles, las opciones para fijar velocidad y volumen)";
$MESS["IVR_LICENSE_POPUP_ITEM_4"] = "Reenviar llamada a un empleado, cola o número externo";
$MESS["IVR_LICENSE_POPUP_ITEM_5"] = "Reenviar llamada a un número de extensión";
$MESS["IVR_LICENSE_POPUP_MORE"] = "Leer más";
$MESS["IVR_LICENSE_POPUP_FOOTER"] = "El menú de voz (IVR) sólo está disponible en los planes Standard y Professional.";
?>