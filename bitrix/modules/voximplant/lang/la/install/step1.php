<?
$MESS["VI_PUBLIC_PATH_DESC"] = "Para que la telefonía funcione correctamente, una dirección de un sitio público válido debe ser introducido.";
$MESS["VI_PUBLIC_PATH_DESC_2"] = "Si el acceso al sitio de Internet es limitado, el acceso debe concederse a determinadas páginas. Los detalles relativos a esta se puede encontrar en #LINK_START#documentation#LINK_END#.";
$MESS["VI_PUBLIC_PATH"] = "Dirección pública de sitio:";
?>