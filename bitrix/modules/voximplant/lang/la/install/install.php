<?
$MESS["VI_CHECK_PULL"] = "El módulo de \"Push and Pull\" no está instalado, o el módulo nginx-push-corriente no está configurado.";
$MESS["VI_CHECK_IM"] = "El módulo de \"Instant Messenger\" no está instalado.";
$MESS["VI_CHECK_MAIN"] = "Por favor, actualice el núcleo del sistema (módulo principal) a la versión 14.9.2.";
$MESS["VI_CHECK_INTRANET"] = "Por favor, actualice el módulo Intranet a la versión 14.5.6.";
$MESS["VI_CHECK_INTRANET_INSTALL"] = "El módulo de \"Intranet\" no está instalado.";
$MESS["VI_CHECK_PUBLIC_PATH"] = "Ha introducido una dirección pública inválida.
";
$MESS["VI_MODULE_NAME_2"] = "Telefonía";
$MESS["VI_MODULE_DESCRIPTION_2"] = "Proporciona soporte de telefonía basados en la nube.";
$MESS["VI_INSTALL_TITLE_2"] = "Instalación del módulo de telefonía";
$MESS["VI_UNINSTALL_TITLE_2"] = "Desinstalación del módulo de telefonía";
$MESS["VOXIMPLANT_ROLE_ADMIN"] = "Administrador";
$MESS["VOXIMPLANT_ROLE_CHIEF"] = "Director";
$MESS["VOXIMPLANT_ROLE_DEPARTMENT_HEAD"] = "Jefe del Departamento";
$MESS["VOXIMPLANT_ROLE_MANAGER"] = "Gerente";
$MESS["VOXIMPLANT_DEFAULT_GROUP"] = "Cola predeterminada";
$MESS["VOXIMPLANT_DEFAULT_IVR"] = "Menú predeterminado";
$MESS["VOXIMPLANT_DEFAULT_ITEM"] = "Gracias por contactarnos. Marque 0 para conectarse a un operador.";
?>