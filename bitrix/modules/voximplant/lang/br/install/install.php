<?
$MESS["VI_MODULE_NAME"] = "Integração VoxImplant";
$MESS["VI_MODULE_DESCRIPTION"] = "Adicionar o suporte da plataforma em nuvem VoxImplant ao módulo de Mensagens";
$MESS["VI_INSTALL_TITLE"] = "Instalação do módulo \"Integração com Voximplant\"";
$MESS["VI_UNINSTALL_TITLE"] = "Remoção do módulo \"Integração com Voximplant\"";
$MESS["VI_CHECK_PULL"] = "O módulo \"Push and Pull\" não está instalado ou o nginx-push-stream-module não está configurado.";
$MESS["VI_CHECK_IM"] = "O módulo Instant Messenger não está instalado.";
$MESS["VI_CHECK_MAIN"] = "Atualizar o sistema de kernel (o módulo Principal) para versão 14.9.2.";
$MESS["VI_CHECK_INTRANET"] = "Atualizar o módulo Intranet para versão 14.5.6. ";
$MESS["VI_CHECK_INTRANET_INSTALL"] = "O módulo Intranet não está instalado.";
$MESS["VI_CHECK_PUBLIC_PATH"] = "Você inseriu um endereço público inválido.";
$MESS["VI_MODULE_NAME_2"] = "Telefonia";
$MESS["VI_MODULE_DESCRIPTION_2"] = "Fornece suporte à telefonia baseada na nuvem.";
$MESS["VI_INSTALL_TITLE_2"] = "Instalação do Módulo de Telefonia";
$MESS["VI_UNINSTALL_TITLE_2"] = "Desinstalação do Módulo de Telefonia";
$MESS["VOXIMPLANT_ROLE_ADMIN"] = "Administrador";
$MESS["VOXIMPLANT_ROLE_CHIEF"] = "Diretor executivo";
$MESS["VOXIMPLANT_ROLE_DEPARTMENT_HEAD"] = "Chefe de departamento";
$MESS["VOXIMPLANT_ROLE_MANAGER"] = "Gerente";
$MESS["VOXIMPLANT_DEFAULT_GROUP"] = "Grupo de fila padrão";
$MESS["VOXIMPLANT_DEFAULT_IVR"] = "Menu padrão";
$MESS["VOXIMPLANT_DEFAULT_ITEM"] = "Obrigado por entrar em contato conosco. Disque 0 para se conectar a um operador.";
?>