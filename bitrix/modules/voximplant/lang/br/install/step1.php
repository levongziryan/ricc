<?
$MESS["VI_PUBLIC_PATH_DESC"] = "Para a telefonia funcionar adequadamente, deve ser inserido um endereço de site público válido.";
$MESS["VI_PUBLIC_PATH_DESC_2"] = "Se o acesso ao site a partir da Intranet é limitado, deve ser concedido acesso a certas páginas. Detalhes referentes a isso podem ser encontrados em #LINK_START#documentação#LINK_END#.";
$MESS["VI_PUBLIC_PATH"] = "Endereço público do site:";
?>