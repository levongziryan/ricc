<?
$MESS["VI_TAB_SETTINGS"] = "Configurações";
$MESS["VI_TAB_TITLE_SETTINGS"] = "Parâmetros de Integração";
$MESS["VI_ACCOUNT_NAME"] = "Nome da conta";
$MESS["VI_ACCOUNT_BALANCE"] = "Saldo";
$MESS["VI_TAB_TITLE_SETTINGS_2"] = "Parâmetros de conexão";
$MESS["VI_ACCOUNT_ERROR_PUBLIC"] = "Você inseriu um endereço público inválido.";
$MESS["VI_ACCOUNT_URL"] = "Endereço público do site";
$MESS["VI_ACCOUNT_DEBUG"] = "Modo de depuração";
$MESS["VI_ACCOUNT_ERROR_LICENSE"] = "Erro ao verificar a chave de licença. Verifique sua entrada e tente novamente.";
$MESS["VI_ACCOUNT_ERROR"] = "Erro ao obter dados. Tente novamente mais tarde.";
?>