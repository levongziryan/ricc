<?
$MESS["ADDRESS_VERIFICATION_NOTIFY"] = "A documentação necessária para alugar números de telefone foi verificada.";
$MESS["ADDRESS_VERIFICATION_NOTIFY_HEAD_ACCEPTED"] = "Sua documentação foi verificada.";
$MESS["ADDRESS_VERIFICATION_NOTIFY_BODY_ACCEPTED"] = "Agora, você pode alugar um número de telefone e processar chamadas em seu Bitrix24!";
$MESS["ADDRESS_VERIFICATION_NOTIFY_LINK_ACCEPTED"] = "Gerenciar Números";
$MESS["ADDRESS_VERIFICATION_NOTIFY_HEAD_REJECTED"] = "Infelizmente, sua documentação foi rejeitada.";
$MESS["ADDRESS_VERIFICATION_NOTIFY_BODY_REJECTED"] = "Motivo: #REJECT_REASON#. Clique em \"Gerenciar Números\" para enviar a documentação novamente.";
$MESS["ADDRESS_VERIFICATION_NOTIFY_LINK_REJECTED"] = "Gerenciar Números";
?>