<?
$MESS["IVR_LICENSE_POPUP_HEADER"] = "Disponível somente nos planos Standard e Professional.";
$MESS["IVR_LICENSE_POPUP_TEXT"] = "O menu de voz (IVR) vai distribuir as chamadas recebidas entre funcionários ou departamentos adequados da sua empresa. Várias opções estão disponíveis para configurar o menu de voz totalmente funcional:";
$MESS["IVR_LICENSE_POPUP_ITEM_1"] = "Menus aninhados";
$MESS["IVR_LICENSE_POPUP_ITEM_2"] = "Use arquivos de áudio personalizados para falar mensagens aos clientes";
$MESS["IVR_LICENSE_POPUP_ITEM_3"] = "Falar o texto usando mecanismo de voz (vozes diferentes estão disponíveis; opções para definir a velocidade e o volume)";
$MESS["IVR_LICENSE_POPUP_ITEM_4"] = "Transferir chamada para um funcionário, grupo de fila ou número externo";
$MESS["IVR_LICENSE_POPUP_ITEM_5"] = "Transferir chamada para um número de ramal";
$MESS["IVR_LICENSE_POPUP_MORE"] = "Saiba mais";
$MESS["IVR_LICENSE_POPUP_FOOTER"] = "O menu de voz (IVR) está disponível apenas nos planos Standard e Professional.";
?>