<?
$MESS["STATISTIC_ENTITY_ID_FIELD"] = "ID";
$MESS["STATISTIC_ENTITY_ACCOUNT_ID_FIELD"] = "Conta";
$MESS["STATISTIC_ENTITY_APPLICATION_ID_FIELD"] = "ID da aplicação";
$MESS["STATISTIC_ENTITY_APPLICATION_NAME_FIELD"] = "Nome da aplicação";
$MESS["STATISTIC_ENTITY_PORTAL_USER_ID_FIELD"] = "ID de usuário";
$MESS["STATISTIC_ENTITY_PHONE_NUMBER_FIELD"] = "Telefone";
$MESS["STATISTIC_ENTITY_INCOMING_FIELD"] = "Recebidas";
$MESS["STATISTIC_ENTITY_CALL_ID_FIELD"] = "ID da Chamada";
$MESS["STATISTIC_ENTITY_CALL_DIRECTION_FIELD"] = "Direção";
$MESS["STATISTIC_ENTITY_CALL_DURATION_FIELD"] = "Duração";
$MESS["STATISTIC_ENTITY_CALL_START_DATE_FIELD"] = "Data de início";
$MESS["STATISTIC_ENTITY_CALL_STATUS_FIELD"] = "Status";
$MESS["STATISTIC_ENTITY_COST_FIELD"] = "Preço";
$MESS["STATISTIC_ENTITY_CALL_RECORD_ID_FIELD"] = "Registro";
$MESS["STATISTIC_ENTITY_CALL_WEBDAV_ID_FIELD"] = "Registro (WebDAV)";
$MESS["STATISTIC_ENTITY_COST_CURRENCY_FIELD"] = "Moeda";
$MESS["STATISTIC_ENTITY_CALL_FAILED_CODE_FIELD"] = "Código";
$MESS["STATISTIC_ENTITY_CALL_FAILED_REASON_FIELD"] = "Descrição Código";
$MESS["STATISTIC_ENTITY_PORTAL_NUMBER_FIELD"] = "Número de telefone do portal";
$MESS["STATISTIC_ENTITY_CALL_LOG_FIELD"] = "Detalhes de Chamada";
$MESS["STATISTIC_ENTITY_CALL_VOTE_FIELD"] = "Classificação de chamada";
?>