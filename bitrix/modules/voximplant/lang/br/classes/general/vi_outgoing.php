<?
$MESS["VI_TTS_SPEED_X_SLOW"] = "Muito lento";
$MESS["VI_TTS_SPEED_SLOW"] = "Lento";
$MESS["VI_TTS_SPEED_MEDIUM"] = "Normal";
$MESS["VI_TTS_SPEED_FAST"] = "Rápido";
$MESS["VI_TTS_SPEED_X_FAST"] = "Muito rápido";
$MESS["VI_TTS_VOLUME_X_SOFT"] = "Muito silencioso";
$MESS["VI_TTS_VOLUME_SOFT"] = "Silencioso";
$MESS["VI_TTS_VOLUME_MEDIUM"] = "Normal";
$MESS["VI_TTS_VOLUME_LOUD"] = "Alto";
$MESS["VI_TTS_VOLUME_X_LOUD"] = "Muito alto";
?>