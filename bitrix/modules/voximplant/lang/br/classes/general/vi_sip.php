<?
$MESS["VI_SIP_CONFIG_ID_NULL"] = "O ID de configuração do número não está especificado.";
$MESS["VI_SIP_NUMBER_EXISTS"] = "O número de telefone já foi cadastrado.";
$MESS["VI_SIP_CONFIG_NOT_FOUND"] = "A configuração não foi encontrada.";
$MESS["VI_SIP_NUMBER_ERR"] = "O número precisa ser especificado no formato internacional completo.";
$MESS["VI_SIP_NUMBER_0"] = "O número de telefone SIP não está especificado.";
$MESS["VI_SIP_SERVER_100"] = "O endereço do servidor não deve exceder 100 caracteres.";
$MESS["VI_SIP_SERVER_0"] = "O endereço do servidor não está especificado.";
$MESS["VI_SIP_LOGIN_100"] = "O endereço de login não deve exceder 100 caracteres.";
$MESS["VI_SIP_LOGIN_0"] = "O endereço de login não está especificado.";
$MESS["VI_SIP_PASSWORD_100"] = "A senha do servidor não deve exceder 100 caracteres.";
$MESS["VI_SIP_INC_SERVER_100"] = "O endereço do servidor de chamada recebida não deve exceder 100 caracteres.";
$MESS["VI_SIP_INC_SERVER_0"] = "O endereço do servidor de chamada recebida não está especificado.";
$MESS["VI_SIP_INC_LOGIN_100"] = "O endereço do servidor de chamada recebida não deve exceder 100 caracteres.";
$MESS["VI_SIP_INC_LOGIN_0"] = "O login do servidor de chamada recebida não está especificado.";
$MESS["VI_SIP_INC_PASSWORD_100"] = "A senha do servidor de chamada recebida não deve exceder 100 caracteres.";
$MESS["VI_SIP_NUMBER_ERR_2"] = "Você deve inserir o número de telefone que será exibido nas chamadas realizadas do seu PBX";
$MESS["VI_SIP_TITLE_EXISTS"] = "O nome da conexão especificada já é usado no sistema.";
$MESS["VI_SIP_SEARCH_ID_EXISTS"] = "O ID da conexão especificada já é usado no sistema.";
$MESS["VI_SIP_TYPE_ERR"] = "O tipo de PBX não está especificado.";
$MESS["VI_SIP_NEW_PBX"] = "Novo PBX";
$MESS["VI_SIP_ADD_CLOUD_ERR"] = "Você não pode conectar mais do que #NUMBER# PBXs hospedados na nuvem.";
?>