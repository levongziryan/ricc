<?
$MESS["VI_CRM_TITLE"] = "Chamada telefônica";
$MESS["VI_CRM_CALL_DURATION"] = "Duração da chamada: #DURATION#";
$MESS["VI_CRM_CALL_STATUS"] = "Status da chamada:";
$MESS["VI_CRM_CALL_CANCEL"] = "Chamada cancelada";
$MESS["VI_CRM_CALL_INCOMING"] = "Chamada recebida";
$MESS["VI_CRM_CALL_OUTGOING"] = "Chamada realizada";
$MESS["VI_CRM_CALL_TO_PORTAL_NUMBER"] = "Chamada direcionada para: #PORTAL_NUMBER#.";
$MESS["VI_CRM_CALL_TITLE"] = "Ligação telefônica";
$MESS["VI_CRM_CALLBACK_TITLE"] = "Retorno de Chamada";
$MESS["VI_CRM_CALL_MISSED"] = "Chamada Perdida";
$MESS["VI_CRM_ACTIVITY_SUBJECT_INCOMING"] = "Recebida de #NUMBER#";
$MESS["VI_CRM_ACTIVITY_SUBJECT_OUTGOING"] = "Feita para #NUMBER#";
$MESS["VI_CRM_ACTIVITY_SUBJECT_CALLBACK"] = "Retorno de chamada para #NUMBER#";
$MESS["VI_CRM_CALL_CALLBACK"] = "Retorno de Chamada";
$MESS["VI_WORKTIME_SKIPPED_CALL"] = "A chamada foi feita fora do horário de expediente.";
?>