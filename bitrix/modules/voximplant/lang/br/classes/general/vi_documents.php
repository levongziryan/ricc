<?
$MESS["VI_DOCS_COUNTRY_RU"] = "Rússia";
$MESS["VI_DOCS_STATUS_REQUIRED"] = "Documentos pendentes";
$MESS["VI_DOCS_STATUS_IN_PROGRESS"] = "Verificando documentos";
$MESS["VI_DOCS_STATUS_VERIFIED"] = "Documentos verificados";
$MESS["VI_DOCS_STATUS_DECLINED"] = "Documentos rejeitados";
$MESS["VI_DOCS_STATUS_REJECTED"] = "Documentos rejeitados";
$MESS["VI_DOCS_DOCUMENT_STATUS_ACCEPTED"] = "Verificado";
$MESS["VI_DOCS_DOCUMENT_STATUS_IN_PROGRESS"] = "Verificação em andamento";
$MESS["VI_DOCS_DOCUMENT_STATUS_INCOMPLETE_SET"] = "Documentos incompletos";
$MESS["VI_DOCS_DOCUMENT_STATUS_REJECTED"] = "Recusado";
$MESS["VI_DOCS_IS_INDIVIDUAL_Y"] = "Pessoa física";
$MESS["VI_DOCS_IS_INDIVIDUAL_N"] = "Pessoa jurídica";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY"] = "A documentação necessária para alugar números de telefone foi verificada.";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_HEAD_VERIFIED"] = "Sua documentação foi verificada.";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_BODY_VERIFIED"] = "Agora, você pode alugar um número de telefone e processar chamadas em seu Bitrix24!";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_LINK_VERIFIED"] = "Gerenciar Números";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_HEAD_REQUIRED"] = "Infelizmente, sua documentação foi rejeitada.";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_BODY_REQUIRED"] = "Motivo: #REJECT_REASON#. Clique em \"Gerenciar Números\" para enviar a documentação novamente.";
$MESS["DOCUMENTS_VERIFICATION_NOTIFY_LINK_REQUIRED"] = "Gerenciar Números";
?>