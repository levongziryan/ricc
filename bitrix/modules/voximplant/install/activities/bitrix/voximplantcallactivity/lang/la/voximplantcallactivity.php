<?
$MESS["BPVICA_INCLUDE_MODULE"] = "El módulo Telephony no está instalado.";
$MESS["BPVICA_RESULT_TRUE"] = "Éxito";
$MESS["BPVICA_RESULT_FALSE"] = "Falló";
$MESS["BPVICA_ERROR_OUTPUT_NUMBER"] = "Número de teléfono no especificado.";
$MESS["BPVICA_ERROR_NUMBER"] = "No se ha especificado el número de suscriptores.";
$MESS["BPVICA_ERROR_TEXT"] = "No se proporciono el texto para hablar.";
$MESS["BPVICA_ERROR_AUDIO_FILE"] = "El archivo de audio no fue especificado.";
$MESS["BPVICA_TRACK_SUBSCR"] = "Esperando por resultados de la robollamada";
?>