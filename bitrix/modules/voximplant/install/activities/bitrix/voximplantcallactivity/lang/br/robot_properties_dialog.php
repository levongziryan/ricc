<?
$MESS["BPVICA_RPD_NO_OUTPUT_NUMBER"] = "Alugue um número de telefone ou conecte telefonia SIP para utilizar esta opção.";
$MESS["BPVICA_RPD_CALL_TYPE"] = "Ligar usando";
$MESS["BPVICA_RPD_CALL_TYPE_TEXT"] = "Name";
$MESS["BPVICA_RPD_CALL_TYPE_AUDIO"] = "Arquivo de áudio";
$MESS["BPVICA_RPD_OUTPUT_NUMBER"] = "Número de origem";
$MESS["BPVICA_RPD_NUMBER"] = "Número de destino";
$MESS["BPVICA_RPD_TEXT"] = "Converter texto em voz";
$MESS["BPVICA_RPD_VOICE_LANGUAGE"] = "Idioma e voz";
$MESS["BPVICA_RPD_VOICE_SPEED"] = "Velocidade da fala";
$MESS["BPVICA_RPD_VOICE_VOLUME"] = "Volume da fala";
$MESS["BPVICA_RPD_AUDIO_FILE"] = "URL do arquivo de áudio (mp3)";
?>