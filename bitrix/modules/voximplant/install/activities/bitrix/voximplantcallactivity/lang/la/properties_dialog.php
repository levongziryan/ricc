<?
$MESS["BPVICA_PD_NO_OUTPUT_NUMBER"] = "Por favor, alquile un número de teléfono o un conectar de telefonía SIP para utilizar esta opción.";
$MESS["BPVICA_PD_CALL_TYPE_TEXT"] = "Texto";
$MESS["BPVICA_PD_CALL_TYPE_AUDIO"] = "Archivo de Audio";
$MESS["BPVICA_PD_OUTPUT_NUMBER"] = "Número de origen";
$MESS["BPVICA_PD_NUMBER"] = "Número de destino";
$MESS["BPVICA_PD_TEXT"] = "Convertir texto a voz";
$MESS["BPVICA_PD_VOICE_LANGUAGE"] = "Idioma y voz";
$MESS["BPVICA_PD_VOICE_SPEED"] = "Velocidad de voz";
$MESS["BPVICA_PD_VOICE_VOLUME"] = "Volumen de voz";
$MESS["BPVICA_PD_AUDIO_FILE"] = "URL del archivo de audio (mp3)";
$MESS["BPVICA_PD_WAIT_FOR_RESULT"] = "Esperar resultado";
$MESS["BPVICA_PD_YES"] = "Si";
$MESS["BPVICA_PD_NO"] = "No";
$MESS["BPVICA_PD_CALL_TYPE"] = "Llamar utilizando";
$MESS["BPVICA_PD_USE_DOCUMENT_PHONE_NUMBER"] = "Usar el número de teléfono actual de la entidad en el CRM";
?>