<?
$MESS["VI_NUMBERS_CONFIG"] = "Configurer";
$MESS["VI_NUMBERS_SAVE"] = "Sauvegarder";
$MESS["VI_NUMBERS_APPLY"] = "Appliquer";
$MESS["VI_NUMBERS_CANCEL"] = "Annuler";
$MESS["VI_NUMBERS_TITLE_2"] = "Configurez numéros par défaut";
$MESS["VI_NUMBERS_CONFIG_BACKPHONE"] = "Numéro par défaut pour les appels sortants";
$MESS["VI_NUMBERS_CONFIG_BACKPHONE_TITLE"] = "Votre homologue verra ce nombre quand vous les appelez à l'aide Bitrix24";
$MESS["VI_NUMBERS_EDIT"] = "Editer";
?>