<?
$MESS["VI_INTERFACE_TITLE"] = "Configure la interface de llamadas";
$MESS["VI_INTERFACE_SAVE"] = "Guardar";
$MESS["VI_INTERFACE_CHAT_TITLE"] = "Visualizar las llamadas telefónicas en Messenger:";
$MESS["VI_INTERFACE_CHAT_ADD"] = "Crear chat por separado para cada llamada";
$MESS["VI_INTERFACE_CHAT_APPEND"] = "Crear chat único para todas las llamadas";
$MESS["VI_INTERFACE_CHAT_NONE"] = "No se muestran las llamadas telefónicas en Messenger";
?>