<?
$MESS["VI_INTERFACE_SAVE"] = "Sauvegarder";
$MESS["VI_INTERFACE_TITLE"] = "Configurer interface d'appel";
$MESS["VI_INTERFACE_CHAT_TITLE"] = "Afficher les appels téléphoniques dans Messenger:";
$MESS["VI_INTERFACE_CHAT_ADD"] = "Créer le chat séparé pour chaque appel";
$MESS["VI_INTERFACE_CHAT_APPEND"] = "Créer un chat unique pour tous les appels";
$MESS["VI_INTERFACE_CHAT_NONE"] = "Ne pas afficher les appels téléphoniques dans Messenger";
?>