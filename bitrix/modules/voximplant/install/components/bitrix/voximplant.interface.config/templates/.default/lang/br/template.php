<?
$MESS["VI_INTERFACE_TITLE"] = "Configurar interface de chamada";
$MESS["VI_INTERFACE_CHAT_TITLE"] = "Exibir chamadas telefônicas no Messenger: ";
$MESS["VI_INTERFACE_CHAT_ADD"] = "Criar bate-papo separado para cada chamada";
$MESS["VI_INTERFACE_CHAT_APPEND"] = "Criar bate-papo único para todas as chamadas";
$MESS["VI_INTERFACE_CHAT_NONE"] = "Não exibir chamadas telefônicas no Messenger:";
$MESS["VI_INTERFACE_SAVE"] = "Salvar";
?>