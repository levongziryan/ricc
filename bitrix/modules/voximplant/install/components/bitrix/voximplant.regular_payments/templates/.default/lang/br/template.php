<?
$MESS["VI_REGULAR_TITLE"] = "Pagamentos recorrentes";
$MESS["VI_REGULAR_TABLE_NUMBER"] = "Número";
$MESS["VI_REGULAR_TABLE_PAID_BEFORE"] = "Pago antes";
$MESS["VI_REGULAR_TABLE_FEE"] = "Taxa mensal";
$MESS["VI_REGULAR_TABLE_STATUS_Y"] = "Ativado";
$MESS["VI_REGULAR_TABLE_STATUS_N"] = "Desativado, pagamento obrigatório";
$MESS["VI_REGULAR_FEE_RUR"] = "RUB #MONEY#";
$MESS["VI_REGULAR_FEE_EUR"] = "EUR #MONEY#";
$MESS["VI_REGULAR_FEE_USD"] = "\$#MONEY#";
$MESS["VI_REGULAR_NO_MONEY"] = "Você não tem crédito suficiente para pagamento recorrente automático. Você precisa aumentar seu saldo até #DATA#.";
$MESS["VI_REGULAR_CONFIG_RENT"] = "Gerenciar Números Alugados";
$MESS["VI_REGULAR_NOTICE"] = "Atenção: prolongamento automático do número de telefone está acionado.";
$MESS["VI_REGULAR_NO_VERIFY"] = "A lei exige que você forneça documentação legal para usar os números alugados.<br><br>Você tem que fazer upload dos documentos até #DATE# ou seus números serão desconectados. <br><br>#URL_START#Carregue os documentos agora#URL_END#";
?>