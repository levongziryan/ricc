<?
$MESS["VI_REGULAR_TABLE_NUMBER"] = "Chiffre";
$MESS["VI_REGULAR_TABLE_STATUS_Y"] = "Activé(e)s";
$MESS["VI_REGULAR_FEE_EUR"] = "EUR #MONEY#";
$MESS["VI_REGULAR_FEE_RUR"] = "RUB #MONEY#";
$MESS["VI_REGULAR_FEE_USD"] = "\$#MONEY#";
$MESS["VI_REGULAR_TABLE_FEE"] = "Frais mensuels";
$MESS["VI_REGULAR_TABLE_STATUS_N"] = "Handicapés, le paiement est exigé";
$MESS["VI_REGULAR_NOTICE"] = "Attention: prolongation automatique de numéros de téléphone est engagé.";
$MESS["VI_REGULAR_TABLE_PAID_BEFORE"] = "Payée avant";
$MESS["VI_REGULAR_TITLE"] = "Paiements récurrents";
$MESS["VI_REGULAR_NO_MONEY"] = "Vous ne disposez pas de suffisamment de crédit pour le paiement récurrent automatisé. Vous devez recharger votre balance jusqu'à #DATE#.";
$MESS["VI_REGULAR_CONFIG_RENT"] = "Gérer les Numéros Loués";
$MESS["VI_REGULAR_NO_VERIFY"] = "La loi demande que vous fournissiez des documents légaux avant de pouvoir utiliser les numéros loués.<br><br>Vous avez jusqu'au #DATE# pour télécharger les documents, sinon vos numéros seront désactivés <br><br>#URL_START#Télécharger maintenant les documents#URL_END#";
$MESS["VI_REGULAR_FEE_UAH"] = "UAH #MONEY#.";
?>