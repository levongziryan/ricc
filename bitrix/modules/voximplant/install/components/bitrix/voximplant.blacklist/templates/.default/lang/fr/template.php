<?
$MESS["BLACKLIST_NUMBER_DELETE"] = "Supprimer";
$MESS["BLACKLIST_TEXT3"] = "minutes";
$MESS["BLACKLIST_SAVE"] = "Sauvegarder";
$MESS["BLACKLIST_TITLE"] = "Gérer la liste noire";
$MESS["BLACKLIST_DELETE_CONFIRM"] = "tes-vous sûr que vous voulez supprimer le numéro de la liste?";
$MESS["BLACKLIST_DELETE_ERROR"] = "Erreur retirer le numéro de téléphone.";
$MESS["BLACKLIST_ENABLE"] = "Les numéros de téléphone de bloc automatique en dehors des heures de bureau";
$MESS["BLACKLIST_ENABLE_TEXT"] = "Les heures creuses sont spécifiés individuellement pour chaque numéro de téléphone sur la page des paramètres.";
$MESS["BLACKLIST_TEXT1"] = "Si un appel provient de la même identification de l'appelant";
$MESS["BLACKLIST_TEXT2"] = "fois de suite au sein de";
$MESS["BLACKLIST_NUMBERS"] = "Numéros sur liste noire";
$MESS["BLACKLIST_NUMBER_ADD"] = "Ajouter un numéro";
$MESS["BLACKLIST_ABOUT_2"] = "Un appel sera rejeté si l'identification de l'appelant se trouve dans la liste. Vous trouverez des informations sur cet appel ici: '#LINK#'";
$MESS["BLACKLIST_ABOUT_LINK"] = "Journal des Appels";
?>