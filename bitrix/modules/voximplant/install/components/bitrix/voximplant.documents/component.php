<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
 * @var $arParams array
 * @var $arResult array
 * @var $this CBitrixComponent
 * @var $APPLICATION CMain
 * @var $USER CUser
 */

if (!CModule::IncludeModule('voximplant'))
	return;

$ViDocs = new CVoxImplantDocuments();
$arResult['DOCUMENTS'] = $ViDocs->GetStatus();
$arResult['VI_DOCS'] = $ViDocs;

if (!empty($arResult['DOCUMENTS']) && !(isset($arParams['TEMPLATE_HIDE']) && $arParams['TEMPLATE_HIDE'] == 'Y'))
	$this->IncludeComponentTemplate();

return $arResult;
?>