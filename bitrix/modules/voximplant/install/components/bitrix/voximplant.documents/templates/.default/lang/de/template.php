<?
$MESS["VI_DOCS_TITLE"] = "Unterlagen hochladen, um Telefonnummern zu mieten";
$MESS["VI_DOCS_BODY"] = "Entsprechend der Gesetzgebung in einigen Ländern kann der Telefonie-Anbieter von Ihnen verlangen, dass Sie juristische Unterlagen bereitstellen, um gemietete Nummern nutzen zu dürfen.<br>Sie müssen Unterlagen bereitstellen, damit die Nummer während der ganzen Zeit der Miete für Sie reserviert bleibt.";
$MESS["VI_DOCS_UPDATE_BTN"] = "Neue Unterlagen hochladen";
$MESS["VI_DOCS_UPLOAD_BTN"] = "Unterlagen hochladen";
$MESS["VI_DOCS_WAIT"] = "Wird hochgeladen... Bitte warten";
$MESS["VI_DOCS_COUNTRY_RU"] = "Russland";
$MESS["VI_DOCS_TABLE_LINK"] = "Upload-History des Dokuments";
$MESS["VI_DOCS_TABLE_UPLOAD"] = "Hochgeladen am";
$MESS["VI_DOCS_TABLE_STATUS"] = "Prüfstatus";
$MESS["VI_DOCS_TABLE_TYPE"] = "Rechtsträger";
$MESS["VI_DOCS_TABLE_COMMENT"] = "Kommentar";
$MESS["VI_DOCS_STATUS"] = "Status:";
$MESS["VI_DOCS_UNTIL_DATE"] = "Sie müssen Unterlagen bis zum #DATE# hochladen";
$MESS["VI_DOCS_UNTIL_DATE_NOTICE"] = "Nach dem angegebenen Datum werden reservierte Nummern deaktiviert, der Betrag wird auf Ihren Account zurückgezahlt.<br><br>Die gemieteten Nummern sind bis zum Schluss der Laufzeit der Miete.";
$MESS["VI_DOCS_UPLOAD_NOTICE"] = "Beachten Sie bitte, dass Ihre Unterlagen direkt zu Fastcom LLC hochgeladen werden, wo sie überprüft werden, ob sie der Gesetzgebung des jeweiligen Landes entsprechen. Bitrix Inc. speichert bzw. verarbeitet keine Daten, die Sie zum Prüfen an Fastcom LLC weitergeben.";
$MESS["VI_DOCS_SERVICE_ERROR"] = "Fehler beim Senden der Anfrage an den Service zum Dokumentenupload";
?>