<?
$MESS["VI_DOCS_TITLE"] = "Télécharger les documents";
$MESS["VI_DOCS_BODY"] = "La loi demande dans certains pays que les fournisseurs en téléphonie s'assurent que vous fournissiez tous les documents légaux avant de pouvoir louer des numéros.<br>Vous devez procurer ces documents avant de pouvoir réserver le numéro pendant toute la durée de la location.";
$MESS["VI_DOCS_UPDATE_BTN"] = "Télécharger de nouveaux documents";
$MESS["VI_DOCS_UPLOAD_BTN"] = "Télécharger les documents";
$MESS["VI_DOCS_WAIT"] = "Téléchargement... Veuillez patienter";
$MESS["VI_DOCS_COUNTRY_RU"] = "Russie";
$MESS["VI_DOCS_TABLE_LINK"] = "Historique des téléchargements de document";
$MESS["VI_DOCS_TABLE_UPLOAD"] = "Date de téléchargement";
$MESS["VI_DOCS_TABLE_STATUS"] = "Vérifier le statut";
$MESS["VI_DOCS_TABLE_TYPE"] = "Entité légale";
$MESS["VI_DOCS_TABLE_COMMENT"] = "Commentaire";
$MESS["VI_DOCS_STATUS"] = "Statut :";
$MESS["VI_DOCS_UNTIL_DATE"] = "Vous avez jusqu'au #DATE# pour télécharger les documents";
$MESS["VI_DOCS_UNTIL_DATE_NOTICE"] = "Une fois la date indiquée passée, les numéros réservés seront déconnectés et les fonds retourneront sur votre compte.<br><br>Les numéros loués resteront actifs jusqu'à la fin de la location.";
$MESS["VI_DOCS_UPLOAD_NOTICE"] = "Notez que les documents sont téléchargés directement sur le serveur de Fastcom LLC afin d'être traités selon la législation des pays respectifs. Bitrix Inc. ne collecte, transfert ou traite les informations personnelles d'une quelconque manière.";
?>