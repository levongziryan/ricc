<?
$MESS["VI_DOCS_TITLE"] = "Fazer upload da documentação para alugar números de telefone";
$MESS["VI_DOCS_BODY"] = "Para alguns países, é exigido por lei que um provedor de serviços de telefonia verifique seu status residencial para conceder números de telefone alugados.<br>Poderá ser necessário enviar a papelada correspondente para poder alugar um número de telefone por toda a duração do aluguel.";
$MESS["VI_DOCS_UPDATE_BTN"] = "Fazer upload de novos documentos";
$MESS["VI_DOCS_UPLOAD_BTN"] = "Carregar documentos";
$MESS["VI_DOCS_WAIT"] = "Carregando... Aguarde";
$MESS["VI_DOCS_COUNTRY_RU"] = "Rússia";
$MESS["VI_DOCS_TABLE_LINK"] = "Histórico de carregamento de documentos";
$MESS["VI_DOCS_TABLE_UPLOAD"] = "Carregado em";
$MESS["VI_DOCS_TABLE_STATUS"] = "Verificar status";
$MESS["VI_DOCS_TABLE_TYPE"] = "Entidade jurídica";
$MESS["VI_DOCS_TABLE_COMMENT"] = "Comentário";
$MESS["VI_DOCS_STATUS"] = "Status:";
$MESS["VI_DOCS_UNTIL_DATE"] = "Você tem que fazer upload da documentação até #DATE#";
$MESS["VI_DOCS_UNTIL_DATE_NOTICE"] = "Após a data indicada, os números reservados serão desconectados, fundos devolvidos para sua conta.<br><br>Os números alugados estarão ativos até o fim do termo de locação.";
$MESS["VI_DOCS_UPLOAD_NOTICE"] = "Observe que a papelada que você está prestes a enviar será carregada diretamente para Fastcom LLC e será tratada de acordo com a legislação do país correspondente. Bitrix Inc. não irá coletar, armazenar ou processar quaisquer dados associados a esses documentos.";
?>