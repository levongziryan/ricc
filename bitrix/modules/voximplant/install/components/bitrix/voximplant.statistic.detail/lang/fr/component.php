<?
$MESS["TELEPHONY_HEADER_LOG"] = "Détails";
$MESS["TELEPHONY_HEADER_RECORD"] = "Enregistrement";
$MESS["TELEPHONY_HEADER_PHONE"] = "Numéro de téléphone";
$MESS["TELEPHONY_HEADER_USER"] = "Employé";
$MESS["TELEPHONY_HEADER_STATUS"] = "Statut";
$MESS["TELEPHONY_HEADER_COST"] = "Coût";
$MESS["TELEPHONY_HEADER_INCOMING"] = "Type d'appel";
$MESS["TELEPHONY_BILLING"] = "Facturation";
$MESS["TELEPHONY_PORTAL_PHONE_SIP_CLOUD"] = "Couverture PBX hébergé (#ID#)";
$MESS["TELEPHONY_STATUS_3"] = "Crédit top-up";
$MESS["TELEPHONY_HEADER_DURATION"] = "Durée de l'appel";
$MESS["TELEPHONY_HEADER_START_DATE"] = "Date d'appel";
$MESS["TELEPHONY_STATUS_409"] = "Solde corrigé après qu'il a été vérifié par un spécialiste. Pour plus de détails, s'il vous plaît contacter le support technique.";
$MESS["TELEPHONY_PORTAL_PHONE_EMPTY"] = "Non identifié";
$MESS["TELEPHONY_HEADER_PORTAL_PHONE"] = "Numéro de téléphone Portal";
$MESS["TELEPHONY_PORTAL_PHONE_SIP_OFFICE"] = "Bureau PBX (#ID#)";
$MESS["TELEPHONY_STATUS_2"] = "Crédit bonus d'inscription chargé";
$MESS["TELEPHONY_STATUS_1"] = "Inscrivez-vous sur le crédit de bonus chargée. Pour plus de détails, s'il vous plaît contacter le support technique.";
$MESS["TELEPHONY_HEADER_VOTE"] = "Évaluation";
?>