<?
$MESS["TELEPHONY_STATUS_1"] = "Bonuspunkte wurden abgehoben. Kontaktieren Sie bitte den technischen Support, um nähere Informationen zu bekommen.";
$MESS["TELEPHONY_STATUS_2"] = "Bonuspunkte wurden abgehoben";
$MESS["TELEPHONY_STATUS_3"] = "Guthaben aufladen";
$MESS["TELEPHONY_STATUS_409"] = "Guthaben wurde nach der Überprüfung durch einen Spezialist korrigiert. Kontaktieren Sie bitte den technischen Support, um nähere Informationen zu bekommen.";
$MESS["TELEPHONY_HEADER_USER"] = "Mitarbeiter";
$MESS["TELEPHONY_HEADER_PHONE"] = "Telefonnummer";
$MESS["TELEPHONY_HEADER_INCOMING"] = "Anruftyp";
$MESS["TELEPHONY_HEADER_DURATION"] = "Anrufdauer";
$MESS["TELEPHONY_HEADER_START_DATE"] = "Anrufdatum";
$MESS["TELEPHONY_HEADER_STATUS"] = "Status";
$MESS["TELEPHONY_HEADER_COST"] = "Anrufkosten";
$MESS["TELEPHONY_HEADER_RECORD"] = "Aufzeichnen";
$MESS["TELEPHONY_BILLING"] = "Billing";
$MESS["TELEPHONY_HEADER_PORTAL_PHONE"] = "Portalnummer";
$MESS["TELEPHONY_PORTAL_PHONE_EMPTY"] = "Unbekannt";
$MESS["TELEPHONY_HEADER_LOG"] = "Details";
$MESS["TELEPHONY_PORTAL_PHONE_SIP_OFFICE"] = "Vermittlungsstelle im Büro (#ID#)";
$MESS["TELEPHONY_PORTAL_PHONE_SIP_CLOUD"] = "Vermittlungsstelle in der Cloud (#ID#)";
$MESS["TELEPHONY_HEADER_VOTE"] = "Bewertung";
?>