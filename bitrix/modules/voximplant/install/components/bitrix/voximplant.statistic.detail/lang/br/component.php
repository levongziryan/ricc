<?
$MESS["TELEPHONY_STATUS_1"] = "Bônus de inscrição creditado. Para detalhes, por favor contate o suporte técnico.";
$MESS["TELEPHONY_STATUS_2"] = "Bônus de inscrição creditado.";
$MESS["TELEPHONY_STATUS_3"] = "Crédito faltante";
$MESS["TELEPHONY_STATUS_409"] = "Saldo corrigido depois de verificado por um especialista. Para mais detalhes, entre em contato com o suporte técnico.";
$MESS["TELEPHONY_HEADER_USER"] = "Colaborador";
$MESS["TELEPHONY_HEADER_PHONE"] = "Telefone";
$MESS["TELEPHONY_HEADER_INCOMING"] = "Tipo de chamada";
$MESS["TELEPHONY_HEADER_DURATION"] = "Duração da chamada";
$MESS["TELEPHONY_HEADER_START_DATE"] = "Data da chamada";
$MESS["TELEPHONY_HEADER_COST"] = "Custo";
$MESS["TELEPHONY_HEADER_RECORD"] = "Registro";
$MESS["TELEPHONY_BILLING"] = "Faturamento";
$MESS["TELEPHONY_HEADER_PORTAL_PHONE"] = "Número de telefone do portal";
$MESS["TELEPHONY_PORTAL_PHONE_EMPTY"] = "não identificado";
$MESS["TELEPHONY_HEADER_LOG"] = "Detalhes";
$MESS["TELEPHONY_PORTAL_PHONE_SIP_OFFICE"] = "PBX do escritório (#ID#)";
$MESS["TELEPHONY_PORTAL_PHONE_SIP_CLOUD"] = "PBX hospedado na nuvem (#ID#)";
$MESS["TELEPHONY_HEADER_STATUS"] = "Status";
$MESS["TELEPHONY_HEADER_VOTE"] = "Avaliação";
?>