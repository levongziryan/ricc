<?
$MESS["CT_BLL_SELECTED"] = "Contagem de registros";
$MESS["TEL_STAT_BACK"] = "Voltar";
$MESS["TEL_STAT_BACK_TITLE"] = "Voltar";
$MESS["TEL_STAT_USER_ID_CANCEL"] = "Reiniciar filtro de funcionários";
$MESS["TEL_STAT_USER_ID_CANCEL_TITLE"] = "Reiniciar filtro de funcionários";
$MESS["TEL_STAT_FILTER_CANCEL"] = "Redefinir filtro";
$MESS["TEL_STAT_FILTER_CANCEL_TITLE"] = "Redefinir filtro";
$MESS["TEL_STAT_EXPORT_TO_EXCEL"] = "Exportar para Excel";
?>