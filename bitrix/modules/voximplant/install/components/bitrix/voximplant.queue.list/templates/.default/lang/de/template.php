<?
$MESS["VOX_QUEUE_LIST_SELECTED"] = "Queue-Gruppen gesamt";
$MESS["VOX_QUEUE_LIST_ADD"] = "Queue-Gruppe hinzufügen";
$MESS["VOX_QUEUE_DELETE_ERROR"] = "Fehler beim Löschen der Queue-Gruppe.";
$MESS["VOX_QUEUE_IS_USED"] = "Diese Queue-Gruppe wird aktuell genutzt in:";
$MESS["VOX_QUEUE_NUMBER"] = "Nummer";
$MESS["VOX_QUEUE_CLOSE"] = "Schließen";
?>