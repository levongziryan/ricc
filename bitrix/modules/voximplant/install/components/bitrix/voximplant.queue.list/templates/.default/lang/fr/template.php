<?
$MESS["VOX_QUEUE_LIST_SELECTED"] = "Files d'attente totales";
$MESS["VOX_QUEUE_LIST_ADD"] = "Ajouter un groupe";
$MESS["VOX_QUEUE_DELETE_ERROR"] = "Erreur lors de la suppression du groupe.";
$MESS["VOX_QUEUE_IS_USED"] = "Ce groupe est actuellement utilisé dans :";
$MESS["VOX_QUEUE_NUMBER"] = "Numéro";
$MESS["VOX_QUEUE_CLOSE"] = "Fermer";
$MESS["VOX_QUEUE_IVR"] = "SVI";
?>