<?
$MESS["VOX_QUEUE_LIST_SELECTED"] = "Total de colas";
$MESS["VOX_QUEUE_LIST_ADD"] = "Agregar grupo";
$MESS["VOX_QUEUE_DELETE_ERROR"] = "Error al eliminar el grupo.";
$MESS["VOX_QUEUE_IS_USED"] = "Este grupo está actualmente en uso en:";
$MESS["VOX_QUEUE_NUMBER"] = "Número";
$MESS["VOX_QUEUE_IVR"] = "IVR";
$MESS["VOX_QUEUE_CLOSE"] = "Cerrar";
?>