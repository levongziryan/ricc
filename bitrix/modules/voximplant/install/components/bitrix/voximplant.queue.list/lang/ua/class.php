<?
$MESS["VOX_QUEUE_LIST_ID"] = "ID";
$MESS["VOX_QUEUE_LIST_NAME"] = "Назва";
$MESS["VOX_QUEUE_LIST_TYPE"] = "Тип";
$MESS["VOX_QUEUE_LIST_TYPE_EVENLY"] = "Рівномірно";
$MESS["VOX_QUEUE_LIST_TYPE_STRICTLY"] = "Строго по черзі";
$MESS["VOX_QUEUE_LIST_CREATE_GROUP"] = "Створити групу";
$MESS["VOX_QUEUE_LIST_EDIT"] = "Редагувати";
$MESS["VOX_QUEUE_LIST_TYPE_ALL"] = "Всім одночасно";
$MESS["VOX_QUEUE_LIST_DELETE"] = "Видалити";
?>