<?
$MESS["VOX_QUEUE_LIST_ID"] = "ID";
$MESS["VOX_QUEUE_LIST_NAME"] = "Name";
$MESS["VOX_QUEUE_LIST_TYPE"] = "Typ";
$MESS["VOX_QUEUE_LIST_TYPE_EVENLY"] = "Gleichmäßig";
$MESS["VOX_QUEUE_LIST_TYPE_STRICTLY"] = "Entsprechend der Warteschlange";
$MESS["VOX_QUEUE_LIST_TYPE_ALL"] = "An alle";
$MESS["VOX_QUEUE_LIST_CREATE_GROUP"] = "Queue-Gruppe erstellen";
$MESS["VOX_QUEUE_LIST_EDIT"] = "Bearbeiten";
$MESS["VOX_QUEUE_LIST_DELETE"] = "Löschen";
?>