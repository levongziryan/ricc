<?
$MESS["VOX_QUEUE_LIST_ID"] = "ID";
$MESS["VOX_QUEUE_LIST_NAME"] = "Nome";
$MESS["VOX_QUEUE_LIST_TYPE"] = "Tipo";
$MESS["VOX_QUEUE_LIST_TYPE_EVENLY"] = "Equilibradamente";
$MESS["VOX_QUEUE_LIST_TYPE_STRICTLY"] = "Exatamente como na fila";
$MESS["VOX_QUEUE_LIST_TYPE_ALL"] = "Para todos";
$MESS["VOX_QUEUE_LIST_CREATE_GROUP"] = "Criar fila";
$MESS["VOX_QUEUE_LIST_EDIT"] = "Editar";
$MESS["VOX_QUEUE_LIST_DELETE"] = "Excluir";
?>