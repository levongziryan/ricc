<?
$MESS["VI_CONFIG_RENT_PHONES"] = "Vous louez les numéros de téléphone suivants :";
$MESS["VI_CONFIG_RENT_NA"] = "Pour le moment, aucun numéro de téléphone à louer n'est disponible, mais réessayez bientôt : le problème n'est que temporaire.";
$MESS["VI_CONFIG_RENT_PHONE_CONFIGURE"] = "Configurer le numéro";
$MESS["VI_CONFIG_RENT_FORM_BTN"] = "Demander un numéro de téléphone";
$MESS["VI_CONFIG_RENT_ORDER_BTN"] = "Envoyer la demande";
$MESS["VI_CONFIG_RENT_ORDER_DESC"] = "Louer un numéro de téléphone est facile avec Bitrix24 ! Envoyez votre requête et attendez un appel du représentant de votre société de télécommunication.";
$MESS["VI_CONFIG_RENT_INCLUDE_2"] = "Vous profiterez :<br>- d'un nombre illimité de lignes entrantes<br>- d'un nombre illimité d'appels entrants<br>-
de numéros internes pour tous vos employés<br>- de règles de traitement des appels entrants<br>-
de notifications d'appels manqués<br>- d'un système d'enregistrement d'appels téléphoniques<br>- de journaux d'appels entrants, sortants et manqués<br>- de statistiques détaillées pour tous les appels<br>- d'une intégration CRM<br>-
de la possibilité de connecter des téléphones physiques";
$MESS["VI_CONFIG_RENT_BUY_CONFIGURE"] = "Configurer le numéro";
$MESS["VI_CONFIG_RENT_ORDER_COMPLETE"] = "Merci ! Votre demande a bien été envoyée. Une représentant de la société de télécommunication vous contactera sous 3 heures de bureau.";
$MESS["VI_CONFIG_RENT_ORDER_COMPANY_NAME"] = "Nom de la société";
$MESS["VI_CONFIG_RENT_ORDER_COMPANY_CONTACT"] = "Personne à contacter";
$MESS["VI_CONFIG_RENT_ORDER_COMPANY_REG_CODE"] = "Numéro d'enregistrement de l'entreprise";
$MESS["VI_CONFIG_RENT_ORDER_COMPANY_PHONE"] = "Téléphone de contact";
$MESS["VI_CONFIG_RENT_ORDER_COMPANY_EMAIL"] = "E-mail de contact";
$MESS["VI_CONFIG_RENT_ORDER_INFO_TITLE_1"] = "Résumé de la demande :";
$MESS["VI_CONFIG_RENT_ORDER_INFO_TITLE_2"] = "Informations sur le compte :";
$MESS["VI_CONFIG_RENT_ORDER_INFO_ACCOUNT"] = "Compte de téléphonie :";
$MESS["VI_CONFIG_RENT_ORDER_INFO_STATUS"] = "Statut de la demande :";
$MESS["VI_CONFIG_RENT_ORDER_INFO_IN_PROCESS"] = "En attente";
$MESS["VI_CONFIG_RENT_ORDER_INFO_DECLINE"] = "Refusée";
$MESS["VI_CONFIG_RENT_ORDER_INFO_ACCEPT"] = "Acceptée";
$MESS["VI_CONFIG_RENT_ORDER_INFO_WAIT"] = "En cours d'étude";
$MESS["VI_CONFIG_RENT_ORDER_INFO_DATE"] = "Date de la demande :";
$MESS["VI_CONFIG_RENT_ORDER_INFO_OID"] = "Numéro du compte de la société de télécommunication :";
$MESS["VI_CONFIG_RENT_ORDER_ALL_FIELD_REQUIRED"] = "Vous devez remplir tous les champs avant d'envoyer votre demande.";
$MESS["VI_CONFIG_RENT_FORM_TITLE"] = "Veuillez fournir vos informations personnelles et de contact en ukrainien pour pouvoir demander un numéro de téléphone.";
$MESS["VI_CONFIG_RENT_ORDER_INFO_ACTIVE_PARTIAL_BLOCKED"] = "Partiellement bloqué";
$MESS["VI_CONFIG_RENT_ORDER_INFO_ACTIVE_BLOCKED"] = "Complètement bloqué";
$MESS["VI_CONFIG_RENT_ORDER_INFO_ACTIVE_TERMINATION"] = "Annulé";
$MESS["VI_CONFIG_RENT_ORDER_INFO_NA"] = "Contactez le fournisseur en téléphonie pour plus d'informations";
$MESS["VI_CONFIG_RENT_ORDER_INFO_DATE_MODIFY"] = "Date de modification du statut :";
$MESS["VI_CONFIG_RENT_ORDER_EXTRA_TITLE"] = "Commander des services supplémentaires";
$MESS["VI_CONFIG_RENT_ORDER_EXTRA_BTN"] = "Commander";
$MESS["VI_CONFIG_RENT_ORDER_EXTRA_TOLLFREE"] = "Numéro d'appel gratuit";
$MESS["VI_CONFIG_RENT_ORDER_EXTRA_LINE"] = "Lignes téléphoniques supplémentaires";
$MESS["VI_CONFIG_RENT_ORDER_EXTRA_NUMBER"] = "Numéros supplémentaires";
$MESS["VI_CONFIG_RENT_ORDER_EXTRA_CITY"] = "Autres numéros locaux";
$MESS["VI_CONFIG_RENT_ORDER_EXTRA_CHANGE"] = "Changer les services actuels";
$MESS["VI_CONFIG_RENT_FORM_TITLE_UA"] = "Veuillez fournir vos informations de contact en ukrainien pour faire votre demande de numéro de téléphone.";
$MESS["VI_CONFIG_RENT_FORM_TITLE_KZ"] = "Veuillez fournir vos informations de contact pour faire votre demande de numéro de téléphone :";
$MESS["VI_CONFIG_RENT_ORDER_COMPANY_BIN"] = "BIC";
$MESS["VI_CONFIG_RENT_ORDER_ERROR"] = "Erreur";
?>