<?
$MESS["VI_CONFIG_RENT_PHONES"] = "Usted está rentando los siguientes números telefónicos";
$MESS["VI_CONFIG_RENT_PHONE_CONFIGURE"] = "Configure el número";
$MESS["VI_CONFIG_RENT_FORM_BTN"] = "Solicitar número de teléfono";
$MESS["VI_CONFIG_RENT_ORDER_BTN"] = "Enviar solicitud";
$MESS["VI_CONFIG_RENT_BUY_CONFIGURE"] = "Configure el número";
$MESS["VI_CONFIG_RENT_ORDER_COMPANY_NAME"] = "Nombre de la compañía";
$MESS["VI_CONFIG_RENT_ORDER_COMPANY_CONTACT"] = "Contacto personal";
$MESS["VI_CONFIG_RENT_ORDER_COMPANY_PHONE"] = "Teléfono de contacto";
$MESS["VI_CONFIG_RENT_ORDER_COMPANY_EMAIL"] = "e-Mail de contacto";
$MESS["VI_CONFIG_RENT_ORDER_INFO_TITLE_1"] = "Resumen de su solicitud:";
$MESS["VI_CONFIG_RENT_ORDER_INFO_TITLE_2"] = "Información de la cuenta";
$MESS["VI_CONFIG_RENT_ORDER_INFO_ACCOUNT"] = "Cuenta de telefonía:";
$MESS["VI_CONFIG_RENT_ORDER_INFO_STATUS"] = "Estatus de su solicitud:";
$MESS["VI_CONFIG_RENT_ORDER_INFO_IN_PROCESS"] = "Pendiente";
$MESS["VI_CONFIG_RENT_ORDER_INFO_DECLINE"] = "Rechazada";
$MESS["VI_CONFIG_RENT_ORDER_INFO_ACCEPT"] = "Aceptada";
$MESS["VI_CONFIG_RENT_ORDER_INFO_WAIT"] = "En revisión";
$MESS["VI_CONFIG_RENT_ORDER_INFO_DATE"] = "Fecha de la solicitud";
$MESS["VI_CONFIG_RENT_ORDER_COMPLETE"] = "Gracias ! Su solicitud ha sido enviada. Un representante telefónico de la compañía lo estará contactando en las siguientes 3 horas. ";
$MESS["VI_CONFIG_RENT_ORDER_COMPANY_REG_CODE"] = "Número de registro de la empresa";
$MESS["VI_CONFIG_RENT_ORDER_INFO_OID"] = "Número de Cuenta de telefonía de la empresa";
$MESS["VI_CONFIG_RENT_ORDER_ALL_FIELD_REQUIRED"] = "Usted debe completar los campos de abajo y enviar su solicitud. ";
$MESS["VI_CONFIG_RENT_NA"] = "En este momento, no hay números disponible para rentar, pero trate nuevmaente pronto, este es sólo un problema temporal. ";
$MESS["VI_CONFIG_RENT_ORDER_DESC"] = "Alquilar un número telefónico con Bitrix24 es fácil! Envíe su solicitud y espere por la llamada telefónica de una representante de la compañía. ";
$MESS["VI_CONFIG_RENT_INCLUDE_2"] = "Usted disfrutará de:<br>- líneas entrantes ilimitadas<br>- llamadas entrantes ilimitadas<br>-
número interno para todos sus empleados<br>- reglas de procesamiento de llamadas entrantes<br>-
notificaciones de llamadas perdidas<br>- grabación de llamadas<br>- registro de llamadas entrantes 
y salientes perdidas<br>- detalle estadístico para todas las llamadas<br>- Integración con el CRM<br>-
posibilidad de contar con teléfonos físicos";
$MESS["VI_CONFIG_RENT_ORDER_INFO_ACTIVE_PARTIAL_BLOCKED"] = "Parcialmente bloqueado";
$MESS["VI_CONFIG_RENT_ORDER_INFO_ACTIVE_BLOCKED"] = "Totalmente bloqueado";
$MESS["VI_CONFIG_RENT_ORDER_INFO_ACTIVE_TERMINATION"] = "Finalizado";
$MESS["VI_CONFIG_RENT_ORDER_INFO_NA"] = "Contactar al proveedor de telecomunicaciones para más detalles";
$MESS["VI_CONFIG_RENT_ORDER_INFO_DATE_MODIFY"] = "Fecha de cambio de estado:";
$MESS["VI_CONFIG_RENT_ORDER_EXTRA_TITLE"] = "Solicite servicios adicionales";
$MESS["VI_CONFIG_RENT_ORDER_EXTRA_BTN"] = "Orden";
$MESS["VI_CONFIG_RENT_ORDER_EXTRA_TOLLFREE"] = "Número de teléfono gratuito";
$MESS["VI_CONFIG_RENT_ORDER_EXTRA_LINE"] = "Líneas telefónicas adicionales";
$MESS["VI_CONFIG_RENT_ORDER_EXTRA_NUMBER"] = "Números adicionales";
$MESS["VI_CONFIG_RENT_ORDER_EXTRA_CITY"] = "Otros números locales";
$MESS["VI_CONFIG_RENT_ORDER_EXTRA_CHANGE"] = "Cambiar los servicios actuales";
$MESS["VI_CONFIG_RENT_FORM_TITLE_UA"] = "Por favor proporcione sus datos e información de contacto en Ucrania para solicitar un número de teléfono.";
$MESS["VI_CONFIG_RENT_FORM_TITLE_KZ"] = "Por favor proporcione sus datos e información de contacto para solicitar un número de teléfono:";
$MESS["VI_CONFIG_RENT_ORDER_COMPANY_BIN"] = "PAPELERA";
$MESS["VI_CONFIG_RENT_ORDER_ERROR"] = "Error";
?>