<?
$MESS["TELEPHONY_PUT_PHONE"] = "Introduzca el número de teléfono de la compañía";
$MESS["TELEPHONY_EMPTY_PHONE"] = "No hay un número de teléfono";
$MESS["TELEPHONY_EMPTY_PHONE_DESC"] = "La persona a la que llama verá un número de teléfono de relay";
$MESS["TELEPHONY_CONFIRM_DATE"] = "El número se ha confirmado, estará activo hasta #DATE#, después de lo cual se desactivará automáticamente hasta que se confirme nuevamente. Puede ampliar la fecha de la próxima confirmación y confirmar en cualquier momento.";
$MESS["TELEPHONY_NUMBER_CONFIG"] = "Configure el número de teléfono";
$MESS["TELEPHONY_CALLERID_NUMBER"] = "Usted tiene un número conectado #CALLER_ID#";
$MESS["TELEPHONY_NOT_CONFIRMED"] = "Un número de teléfono no se verificó. Usted tiene que verificar este número de teléfono para hacer su ID de llamadas visible para otras personas.";
?>