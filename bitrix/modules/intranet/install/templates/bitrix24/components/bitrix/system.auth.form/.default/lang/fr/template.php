<?
$MESS["AUTH_PROFILE"] = "Ma page";
$MESS["AUTH_PROFILE_B24NET"] = "Ma page Bitrix24.Network";
$MESS["AUTH_CHANGE_PROFILE"] = "Modifier les Données Personnelles";
$MESS["AUTH_CHANGE_NOTIFY"] = "Réglages de notifications";
$MESS["AUTH_CHANGE_MAIL"] = "Réglage de l'intégration de la poste";
$MESS["AUTH_MANAGE_MAIL"] = "Gestion de boîtes";
$MESS["AUTH_HELP"] = "Aide";
$MESS["AUTH_LOGOUT"] = "Sortir";
$MESS["AUTH_AUTH"] = "Autorisation";
$MESS["B24_HELP_LOADER"] = "Chargement...";
$MESS["B24_HELP_TITLE_NEW"] = "Soutien";
$MESS["B24_HELP_URL"] = "//helpdesk.bitrix24.com/widget/";
$MESS["B24_HELP_RELOAD_URL"] = "//helpdesk.bitrix24.com/widget/?start=Y";
$MESS["B24_LICENSE_ALL"] = "Mettre à niveau";
$MESS["AUTH_THEME_DIALOG"] = "Thèmes";
$MESS["AUTH_ADMIN_SECTION"] = "Administration";
$MESS["AUTH_THEME_DIALOG_HINT"] = "Personnalisez votre Bitrix24 ! <br>Sélectionnez une image ou une vidéo d'arrière-plan.";
?>