<?
$MESS["ISL_EMAIL"] = "E-mail";
$MESS["ISL_ID"] = "ID";
$MESS["ISL_PERSONAL_WWW"] = "Page WWW";
$MESS["ISL_XML_ID"] = "ID externe";
$MESS["ISL_PERSONAL_CITY"] = "Ville";
$MESS["ISL_DATE_REGISTER"] = "Date d'enregistrement:";
$MESS["ISL_PERSONAL_BIRTHDAY"] = "Date de naissance";
$MESS["ISL_WORK_POSITION"] = "Emplacement";
$MESS["ISL_PERSONAL_NOTES"] = "Notes";
$MESS["INTR_ISL_TPL_PARAM_USER_PROPERTY"] = "Champs supplémentaires";
$MESS["ISL_ADMIN_NOTES"] = "Notes de l'administrateur";
$MESS["ISL_NAME"] = "Prénom";
$MESS["ISL_LOGIN"] = "Connexion";
$MESS["ISL_PERSONAL_MOBILE"] = "Portable";
$MESS["ISL_PERSONAL_STATE"] = "Région";
$MESS["ISL_SECOND_NAME"] = "Deuxième prénom";
$MESS["ISL_PERSONAL_PAGER"] = "Pager";
$MESS["ISL_PERSONAL_GENDER"] = "Sexe";
$MESS["ISL_PERSONAL_POST_ADDRESS"] = "Adresse postale";
$MESS["ISL_PERSONAL_ZIP"] = "Code Postal";
$MESS["ISL_PERSONAL_MAILBOX"] = "Boîte aux lettres";
$MESS["ISL_PERSONAL_PROFESSION"] = "Fonction";
$MESS["ISL_WORK_PHONE"] = "Téléphone professionnel";
$MESS["ISL_PERSONAL_COUNTRY"] = "Pays";
$MESS["INTR_ISL_TPL_PARAM_PM_URL"] = "Page de l'envoi du message privé";
$MESS["ISL_PERSONAL_PHONE"] = "Numéro de téléphone";
$MESS["ISL_PERSONAL_PHONES"] = "Téléphones";
$MESS["ISL_PERSONAL_STREET"] = "Adresse réelle";
$MESS["ISL_FULL_NAME"] = "Nom, prénom";
$MESS["ISL_PERSONAL_FAX"] = "Fax";
$MESS["ISL_LAST_NAME"] = "Nom";
$MESS["ISL_PERSONAL_PHOTO"] = "Photo";
$MESS["ISL_PERSONAL_ICQ"] = "Numéro ICQ";
?>