<?
$MESS["BITRIX24_THEME_DEFAULT"] = "Bitrix24";
$MESS["BITRIX24_THEME_SUNSET"] = "Sunset";
$MESS["BITRIX24_THEME_GREENFIELD"] = "Campo verde";
$MESS["BITRIX24_THEME_TULIPS"] = "Tulipanes";
$MESS["BITRIX24_THEME_GRASS"] = "Césped";
$MESS["BITRIX24_THEME_CLOUD_SEA"] = "Mar nuboso";
$MESS["BITRIX24_THEME_PINK_FENCER"] = "Rosa Pastel";
$MESS["BITRIX24_THEME_GRASS_EARS"] = "Hojas de hierba";
$MESS["BITRIX24_THEME_SAFARI"] = "Safari";
$MESS["BITRIX24_THEME_ATMOSPHERE"] = "Atmósfera";
$MESS["BITRIX24_THEME_PARADISE"] = "Paraíso";
$MESS["BITRIX24_THEME_VILLAGE"] = "Campo de mañana";
$MESS["BITRIX24_THEME_MOUNTAINS"] = "Montañas";
$MESS["BITRIX24_THEME_BEACH"] = "Playa";
$MESS["BITRIX24_THEME_SEA_SUNSET"] = "Mar atardecer";
$MESS["BITRIX24_THEME_SNOW_VILLAGE"] = "Noche polar";
$MESS["BITRIX24_THEME_MEDITATION"] = "Meditación";
$MESS["BITRIX24_THEME_STARFISH"] = "Estrella de mar";
$MESS["BITRIX24_THEME_SEA_STONES"] = "Piedras del mar";
$MESS["BITRIX24_THEME_SEASHELLS"] = "Conchas marinas";
$MESS["BITRIX24_THEME_PATTERN_THINGS"] = "Patrón violeta";
$MESS["BITRIX24_THEME_PATTERN_BLUISH_GREEN"] = "Patrón de aguamarina";
$MESS["BITRIX24_THEME_PATTERN_BLUE"] = "Patrón azul";
$MESS["BITRIX24_THEME_PATTERN_GREY"] = "Patrón gris";
$MESS["BITRIX24_THEME_PATTERN_SKY_BLUE"] = "Patrón de cielo azul";
$MESS["BITRIX24_THEME_PATTERN_PINK"] = "Patrón rosa";
$MESS["BITRIX24_THEME_PATTERN_PRESENTS"] = "Regalos";
$MESS["BITRIX24_THEME_PATTERN_CHECKED"] = "Tartán";
$MESS["BITRIX24_THEME_DEFAULT_WITH_PATTERN"] = "Bitrix24 con patrón";
$MESS["BITRIX24_THEME_PATTERN_LIGHT_GREY"] = "Patrón gris claro";
$MESS["BITRIX24_THEME_VIDEO_STAR_SKY"] = "Estrellas en el cielo";
$MESS["BITRIX24_THEME_VIDEO_WAVES"] = "Olas";
$MESS["BITRIX24_THEME_VIDEO_JELLYFISHES"] = "Medusa";
$MESS["BITRIX24_THEME_VIDEO_SUNSET"] = "Sunset";
$MESS["BITRIX24_THEME_VIDEO_RAIN"] = "Lluvia";
$MESS["BITRIX24_THEME_VIDEO_RAIN_DROPS"] = "Gotas de lluvia";
$MESS["BITRIX24_THEME_VIDEO_GRASS"] = "Césped";
$MESS["BITRIX24_THEME_VIDEO_STONES"] = "Piedras";
$MESS["BITRIX24_THEME_VIDEO_WATERFALL"] = "Cascada";
$MESS["BITRIX24_THEME_VIDEO_SHINING"] = "Brillar";
$MESS["BITRIX24_THEME_VIDEO_BEACH"] = "Playa";
$MESS["BITRIX24_THEME_VIDEO_RIVER"] = "Río";
$MESS["BITRIX24_THEME_ARCHITECTURE"] = "Arquitectura";
$MESS["BITRIX24_THEME_SKYSCRAPER"] = "Rascacielos";
$MESS["BITRIX24_THEME_WALL"] = "Pared";
$MESS["BITRIX24_THEME_FLOWER"] = "Flor";
$MESS["BITRIX24_THEME_METRO"] = "Subterraneo";
$MESS["BITRIX24_THEME_SHINING"] = "Brillar";
$MESS["BITRIX24_THEME_STARS"] = "Estrellas";
$MESS["BITRIX24_THEME_CLOUDS"] = "Nubes";
$MESS["BITRIX24_THEME_CANYON"] = "Cañón";
$MESS["BITRIX24_THEME_VALLEY"] = "Valle";
$MESS["BITRIX24_THEME_LEAFS"] = "Hojas";
$MESS["BITRIX24_THEME_WIND"] = "Viento";
$MESS["BITRIX24_THEME_TREE"] = "Árbol";
$MESS["BITRIX24_THEME_RED_FIELD"] = "Campo de Amaranto";
$MESS["BITRIX24_THEME_TREES"] = "Árboles";
$MESS["BITRIX24_THEME_ICE"] = "Hielo";
$MESS["BITRIX24_THEME_PLANT"] = "Sembrar";
$MESS["BITRIX24_THEME_COUNTRYSIDE"] = "Pueblo";
$MESS["BITRIX24_THEME_MORNING"] = "Mañana";
?>