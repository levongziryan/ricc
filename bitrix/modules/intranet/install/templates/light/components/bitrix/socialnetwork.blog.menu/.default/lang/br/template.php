<?
$MESS["BLOG_MENU_DRAFT_MESSAGES"] = "rascunhos";
$MESS["BLOG_MENU_DRAFT_MESSAGES_TITLE"] = "Ver conversas não publicadas";
$MESS["BLOG_MENU_MODERATION_MESSAGES"] = "moderação";
$MESS["BLOG_MENU_MODERATION_MESSAGES_TITLE"] = "Exibir mensagens aguardando a moderação";
$MESS["BLOG_MENU_4ME_ALL"] = "Tudo";
$MESS["BLOG_MENU_MINE"] = "Meu";
$MESS["BLOG_MENU_4ME"] = "Pra mim";
$MESS["BLOG_MENU_MINE_TITLE"] = "Exibir minhas mensagens ";
$MESS["BLOG_MENU_4ME_TITLE"] = "Exibir mensagens enviadas pra mim";
$MESS["BLOG_MENU_4ME_ALL_TITLE"] = "exibir todas as conversas";
$MESS["BLOG_MENU_4ME_DR"] = "Meu departamento";
$MESS["BLOG_MENU_4ME_DR_TITLE"] = "exibir conversas no meu departamento";
?>