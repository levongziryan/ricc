<?
$MESS["SAF_TP_PATH_TO_SONET_BIZPROC"] = "Image du chemin vers la page de la procédure d'entreprise";
$MESS["SAF_TP_PATH_TO_BLOG"] = "Modèle de chemin d'accès à la page du blog de l'utilisateur";
$MESS["SAF_TP_PATH_TO_SONET_GROUPS"] = "Modèle de chemin d'accès à la page des groupes d'un utilisateur";
$MESS["SAF_TP_PATH_TO_TASKS"] = "Modèle de chemin d'accès à la page des tâches de l'utilisateur";
$MESS["SAF_TP_PATH_TO_CALENDAR"] = "Modèle de chemin d'accès à la page du calendrier de l'utilisateur";
$MESS["SAF_TP_PATH_TO_SONET_LOG"] = "Modèle de chemin d'accès à la page des mises à jour de l'utilisateur";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGE_FORM_MESS"] = "Modèle de chemin d'accès à la page de réponse à un message";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGE_FORM"] = "La taille du message:";
$MESS["SAF_TP_PATH_TO_MYPORTAL"] = "Modèle de chemin d'accès à la page du portail de l'utilisateur";
$MESS["SAF_TP_PATH_TO_SONET_PROFILE"] = "Modèle de chemin d'accès à la page du profil d'utilisateur";
$MESS["SAF_TP_PATH_TO_SONET_GROUP"] = "Modèle de chemin d'accès à la page d'un groupe de travail";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGES"] = "Modèle de chemin d'accès à la page des messages de l'utilisateur";
$MESS["SAF_TP_PATH_TO_FILES"] = "Modèle de chemin d'accès à la page des fichiers utilisateur";
$MESS["SAF_TP_PATH_TO_PHOTO"] = "Modèle de chemin d'accès à la galerie photos de l'utilisateur";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGES_CHAT"] = "Modèle de chemin d'accès à la page du chat";
?>