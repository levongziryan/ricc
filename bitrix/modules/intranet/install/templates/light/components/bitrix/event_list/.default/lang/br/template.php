<?
$MESS["EVENT_LIST_SUBMIT"] = "selecionar";
$MESS["EVENT_LIST_FILTER_CREATED_BY"] = "creado por";
$MESS["EVENT_LIST_FILTER_DATE"] = "data";
$MESS["EVENT_LIST_FILTER_FEATURES_TITLE"] = "eventos";
$MESS["EVENT_LIST_FILTER_TITLE"] = "conjuto de filtros";
$MESS["EVENT_LIST_NO_UPDATES"] = "sem atualização";
$MESS["EVENT_LIST_PAGE_NAV"] = "Mostrando registros";
$MESS["EVENT_LIST_DATE_FILTER_DAYS"] = "dias";
$MESS["EVENT_LIST_NO_ACTIVE_FEATURES_ERROR"] = "Nenhum evento selecionado no filtro";
?>