<?
$MESS["SONET_UM_GENERAL"] = "General";
$MESS["SONET_UM_LOG"] = "Flujo de Actividad";
$MESS["SONET_UM_FRIENDS"] = "Amigos";
$MESS["SONET_UM_GROUPS"] = "Grupos";
$MESS["SONET_UM_PHOTO"] = "Foto";
$MESS["SONET_UM_FORUM"] = "Foro";
$MESS["SONET_UM_CALENDAR"] = "Calendario";
$MESS["SONET_UM_FILES"] = "Archivo";
$MESS["SONET_UM_BLOG"] = "Conversaciones";
$MESS["SONET_UM_TASKS"] = "Tareas";
$MESS["SONET_UM_SEND_MESSAGE"] = "Enviar mensaje";
$MESS["SONET_UM_VIDEO_CALL"] = "Videollamada";
$MESS["SONET_UM_ONLINE"] = "En línea";
$MESS["SONET_UM_ABSENT"] = "(fuera de la oficina)";
$MESS["SONET_UM_MESSAGES"] = "Mensajes";
$MESS["SONET_UM_BIRTHDAY"] = "Hoy es el cumpleaños de un usuario";
$MESS["SONET_UM_HONOUR"] = "El usuario está en el cuadro de honor";
$MESS["SONET_UM_EDIT_PROFILE"] = "Editar perfil";
$MESS["SONET_UM_EDIT_SETTINGS"] = "Editar Configuración de Privacidad";
$MESS["SONET_UM_EDIT_FEATURES"] = "Editar Ajustes";
$MESS["SONET_UM_SUBSCRIBE"] = "Suscripción";
$MESS["SONET_UM_REQUESTS"] = "Invitaciones y solicitudes";
?>