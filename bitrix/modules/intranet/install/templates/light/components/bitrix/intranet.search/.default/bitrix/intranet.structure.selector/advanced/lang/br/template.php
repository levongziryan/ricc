<?
$MESS["INTR_ISS_PARAM_DEPARTMENT"] = "Departamento";
$MESS["INTR_ISS_PARAM_EMAIL"] = "E-mail";
$MESS["INTR_ISS_PARAM_FIO"] = "Nome";
$MESS["INTR_ISS_BUTTON_SUBMIT"] = "Pesquisar";
$MESS["INTR_ISS_BUTTON_CANCEL"] = "Cancelar";
$MESS["INTR_COMP_IS_TPL_SECH"] = "Pesquisar";
$MESS["INTR_COMP_IS_TPL_MY_OFFICE"] = "Mensagem Log";
$MESS["INTR_ISS_PARAM_PHONE"] = "Telefone";
$MESS["INTR_ISS_PARAM_PHONE_INNER"] = "Telefone Interno";
$MESS["INTR_ISS_PARAM_KEYWORDS"] = "(Fora do escritório)";
?>