<?
$MESS["P_ALBUM_IS_PASSWORDED"] = "Album protégé par un mot de passe.";
$MESS["P_ALBUM_IS_NOT_ACTIVE"] = "Album dissimulé.";
$MESS["P_SECTION_DELETE_ASK"] = "Etes-vous sûr de supprimer l'album avec tous les sous-albums sans possibilité de restauration?";
$MESS["P_SECTION_EDIT"] = "Editer";
$MESS["P_SECTION_EDIT_TITLE"] = "Modification de l'album";
$MESS["P_EDIT_ICON"] = "Choix de la couverture";
$MESS["P_EDIT_ICON_TITLE"] = "Changer la couverture de l'album";
$MESS["P_ALBUM_IS_NOT_ACTIVE_AND_PASSWORDED"] = "L'album caché est protégé par le mot de passe.";
$MESS["P_EMPTY_DATA"] = "La liste d'albums est vide.";
$MESS["P_SECTION_DELETE"] = "Supprimer";
$MESS["P_SECTION_DELETE_TITLE"] = "Supprimer l'album";
$MESS["P_PHOTOS"] = "photo";
?>