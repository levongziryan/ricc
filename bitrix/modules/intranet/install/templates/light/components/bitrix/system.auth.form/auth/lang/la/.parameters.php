<?
$MESS["SAF_TP_PATH_TO_MYPORTAL"] = "Ruta a la Plantilla de la Página de Escritorio del Usuario";
$MESS["SAF_TP_PATH_TO_SONET_PROFILE"] = "Ruta a la Plantilla de la Página Perfil del Usuario";
$MESS["SAF_TP_PATH_TO_SONET_BIZPROC"] = "Ruta a la Plantilla de la Página Procesos de Negocios";
$MESS["SAF_TP_PATH_TO_SONET_GROUP"] = "Ruta a la Plantilla de la Página del Grupo de Trabajo";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGES"] = "Ruta a la Plantilla de la Página de Mensajes del Usuario";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGE_FORM"] = "Ruta a la Plantilla del Formulario de Mensaje de Entrada";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGE_FORM_MESS"] = "Ruta a la Plantilla del Formulario de Respuesta";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGES_CHAT"] = "Ruta a la Plantilla de la Página de Chat";
$MESS["SAF_TP_PATH_TO_SONET_GROUPS"] = "Ruta a la Plantilla de la Página de Grupo de Usuarios";
$MESS["SAF_TP_PATH_TO_SONET_LOG"] = "Ruta a la Plantilla de la Página de Actualizaciones de Usuario";
$MESS["SAF_TP_PATH_TO_BLOG"] = "Ruta a la Plantilla de la Página del Blog de Usuario";
$MESS["SAF_TP_PATH_TO_PHOTO"] = "Ruta a la Plantilla de la Página Galería de Fotos de Usuario";
$MESS["SAF_TP_PATH_TO_CALENDAR"] = "Ruta a la Plantilla de la Página de Calendario del Usuario";
$MESS["SAF_TP_PATH_TO_TASKS"] = "URL a la Plantilla de la Página de Tareas del Usuario";
$MESS["SAF_TP_PATH_TO_FILES"] = "Ruta a la Plantilla de la Página de Archivos del Usuario";
?>