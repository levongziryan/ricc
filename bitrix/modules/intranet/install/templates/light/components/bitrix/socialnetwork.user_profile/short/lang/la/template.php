<?
$MESS["SONET_P_USER_SEX"] = "Sexo:";
$MESS["SONET_P_USER_BIRTHDAY"] = "Cumpleaños:";
$MESS["SONET_C38_T_ONLINE"] = "En línea";
$MESS["SONET_C38_T_HONOURED"] = "Honrado";
$MESS["SONET_C38_T_BIRTHDAY"] = "Cumpleaños";
$MESS["SONET_C39_ABSENT"] = "Fuera de la Oficina";
$MESS["SONET_C39_SEND_MESSAGE"] = "Enviar Mensaje";
$MESS["SONET_C39_SHOW_MESSAGES"] = "Mostrar registro de mensajes";
$MESS["SONET_C39_FR_DEL"] = "Desagregar";
$MESS["SONET_C39_FR_ADD"] = "Agregar a Amigos";
$MESS["SONET_C39_INV_GROUP"] = "Invitar al Grupo";
$MESS["SONET_C39_EDIT_PROFILE"] = "Editar Perfil";
$MESS["SONET_C39_EDIT_SETTINGS"] = "Editar Configuración de Privacidad";
$MESS["SONET_C39_EDIT_FEATURES"] = "Editar ajustes";
$MESS["SONET_C39_CONTACT_TITLE"] = "Información del Contacto";
$MESS["SONET_C39_CONTACT_UNAVAIL"] = "La información de contacto no está disponible.";
$MESS["SONET_C39_PERSONAL_TITLE"] = "Detalles Personales";
$MESS["SONET_C39_PERSONAL_UNAVAIL"] = "La información personal no está disponible.";
$MESS["SONET_C38_TP_NO_PERMS"] = "No tiene permiso para ver el perfil de este usuario.";
$MESS["SONET_C39_VIDEO_CALL"] = "Videollamada";
?>