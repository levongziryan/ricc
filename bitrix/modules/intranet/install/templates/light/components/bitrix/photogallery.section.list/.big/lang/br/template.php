<?
$MESS["P_PHOTOS"] = "Fotos";
$MESS["P_SECTION_EDIT_TITLE"] = "Editar album";
$MESS["P_SECTION_DELETE"] = "Excluir";
$MESS["P_SECTION_DELETE_TITLE"] = "excluir album";
$MESS["P_SECTION_DELETE_ASK"] = "Tem certeza que deseja excluir o álbum de forma irreversível?";
$MESS["P_EDIT_ICON"] = "Escolher capa";
$MESS["P_EDIT_ICON_TITLE"] = "Escolher capa do album";
$MESS["P_ALBUM_IS_PASSWORDED"] = "Senha protegida";
$MESS["P_EMPTY_DATA"] = "Nenhuma album adicionado ainda";
$MESS["P_SECTION_EDIT"] = "Editar";
$MESS["P_ALBUM_IS_NOT_ACTIVE_AND_PASSWORDED"] = "O álbum está escondido e senha protegida.";
$MESS["P_ALBUM_IS_NOT_ACTIVE"] = "O álbum está escondido.";
?>