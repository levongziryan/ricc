<?
$MESS["NO_OF_COUNT"] = "#NO# de #TOTAL#";
$MESS["P_EDIT"] = "Editar";
$MESS["P_EDIT_TITLE"] = "Editar propiedades de la imagen";
$MESS["P_DROP"] = "Eliminar";
$MESS["P_DROP_TITLE"] = "Eliminar imagen";
$MESS["P_ORIGINAL"] = "Original";
$MESS["P_ORIGINAL_TITLE"] = "Imagen original";
$MESS["P_SLIDE_SHOW"] = "Mostrar slide";
$MESS["P_SLIDE_SHOW_TITLE"] = "Iniciar la presentación de slide desde esta imagen";
$MESS["P_DROP_CONFIM"] = "¿Usted está seguro que quiere eliminar la foto irreversiblemente?";
$MESS["P_PREV"] = "volver";
$MESS["P_GO_TO_PREV"] = "Foto anterior";
$MESS["P_GO_TO_NEXT"] = "Siguiente foto";
$MESS["P_NEXT"] = "siguiente";
$MESS["P_TAGS"] = "Etiquetas";
$MESS["P_UNKNOWN_ERROR"] = "Error al guardar datos";
?>