<?
$MESS["INTR_IS_TPL_HEAD"] = "Assigner le dirigeant";
$MESS["INTR_IS_TPL_SEARCH"] = "Rechercher un utilisateur";
$MESS["INTR_IS_TPL_SEARCH_DEPARTMENT"] = "Trouver l'employé dans cette section";
$MESS["INTR_IS_TPL_OUTLOOK_TITLE"] = "Synchronisation de la liste des employés avec les contacts Microsoft Outlook";
$MESS["INTR_IS_TPL_OUTLOOK"] = "Exportation des collaborateurs vers Outlook";
?>