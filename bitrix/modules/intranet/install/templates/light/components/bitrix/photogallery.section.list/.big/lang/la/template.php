<?
$MESS["P_PHOTOS"] = "fotos";
$MESS["P_SECTION_EDIT"] = "Editar";
$MESS["P_SECTION_EDIT_TITLE"] = "Editar álbum";
$MESS["P_SECTION_DELETE"] = "Eliminar";
$MESS["P_SECTION_DELETE_TITLE"] = "Eliminar álbum";
$MESS["P_SECTION_DELETE_ASK"] = "¿Usted está seguro que quiere eliminar el álbum irreversiblemente?";
$MESS["P_EDIT_ICON"] = "Elige la tapa";
$MESS["P_EDIT_ICON_TITLE"] = "Elige la portada del álbum";
$MESS["P_ALBUM_IS_NOT_ACTIVE"] = "El álbum está oculto.";
$MESS["P_ALBUM_IS_NOT_ACTIVE_AND_PASSWORDED"] = "El álbum está oculto y protegido con contraseña.";
$MESS["P_ALBUM_IS_PASSWORDED"] = "Contraseña protegida.";
$MESS["P_EMPTY_DATA"] = "No hay álbumes agregados todavía";
?>