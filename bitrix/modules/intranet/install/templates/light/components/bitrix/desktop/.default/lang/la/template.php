<?
$MESS["CMDESKTOP_TDEF_ERR1"] = "Error al guardar el cargo del gadget en el servidor.";
$MESS["CMDESKTOP_TDEF_ERR2"] = "Error al agregar gadget al servidor.";
$MESS["CMDESKTOP_TDEF_CONF"] = "La configuración de su Panel de control personal se aplicará de manera predeterminada para todos los usuarios nuevos o no autorizados. ¿Continuar?";
$MESS["CMDESKTOP_TDEF_CONF_USER"] = "La configuración del escritorio se aplicará a todos los nuevos perfiles de usuario de forma predeterminada. ¿Quieres continuar?";
$MESS["CMDESKTOP_TDEF_CONF_GROUP"] = "Su configuración de escritorio se aplicará a todos los grupos de trabajo nuevos de forma predeterminada. ¿Quieres continuar?";
$MESS["CMDESKTOP_TDEF_ADD"] = "Agregar gadget";
$MESS["CMDESKTOP_TDEF_SET"] = "Guardar como configuración predeterminada";
$MESS["CMDESKTOP_TDEF_CLEAR"] = "Restablecer la configuración actual";
$MESS["CMDESKTOP_TDEF_CANCEL"] = "Cancelar";
$MESS["CMDESKTOP_DESC_NAME"] = "Panel Personal";
$MESS["CMDESKTOP_TDEF_DELETE"] = "Eliminar";
$MESS["CMDESKTOP_TDEF_SETTINGS"] = "Ajustes";
$MESS["CMDESKTOP_DEMO_DATA_BLOCK_TITLE"] = "Limpieza de datos de demostración";
$MESS["CMDESKTOP_DEMO_DATA_BLOCK_DESC"] = "Para eliminar datos de demostración de su portal, use <b>Asistente de limpieza</b>. Para ejecutar el asistente, haga clic en 'Asistente de limpieza' en la barra de herramientas del panel de control cuando esté en el modo de navegación, o use <a href='#LINK_TO_WIZARD#'>este enlace</a>.";
$MESS["CMDESKTOP_TDEF_CLEAR_CONF"] = "Los parámetros predeterminados se aplicarán a su escritorio. ¿Continuar?";
$MESS["CMDESKTOP_TDEF_HIDE"] = "Ocultar/Mostrar";
?>