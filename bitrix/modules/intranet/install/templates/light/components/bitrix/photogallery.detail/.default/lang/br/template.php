<?
$MESS["NO_OF_COUNT"] = "#NO# of #TOTAL#";
$MESS["P_EDIT"] = "editar";
$MESS["P_EDIT_TITLE"] = "Editar propriedades da imagem";
$MESS["P_DROP"] = "Excluir";
$MESS["P_DROP_TITLE"] = "Excluir mensagem";
$MESS["P_ORIGINAL"] = "Original";
$MESS["P_ORIGINAL_TITLE"] = "Imagem original";
$MESS["P_SLIDE_SHOW"] = "Slide show";
$MESS["P_SLIDE_SHOW_TITLE"] = "Iniciar apresentação de slides a partir desta imagem";
$MESS["P_DROP_CONFIM"] = "Tem certeza de que deseja excluir a foto de forma irreversível?";
$MESS["P_PREV"] = "voltar";
$MESS["P_GO_TO_PREV"] = "Foto anterior";
$MESS["P_GO_TO_NEXT"] = "Próxima foto";
$MESS["P_NEXT"] = "próximo";
$MESS["P_TAGS"] = "tags";
$MESS["P_UNKNOWN_ERROR"] = "Salvar erro de dados";
?>