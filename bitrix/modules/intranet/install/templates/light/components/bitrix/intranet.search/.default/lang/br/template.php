<?
$MESS["INTR_COMP_IS_TPL_FILTER_ADV"] = "buscar avançada";
$MESS["INTR_COMP_IS_TPL_FILTER_AZ"] = "A-Z";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_VIEW_TABLE"] = "detalhes";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_EXCEL"] = "Excel";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_OUTLOOK_TITLE"] = "Exporta lista de empregados como contatos Outlook";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_EXCEL_TITLE"] = "Exportar resultados da pesquisa para o MS Excel";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_LETTER"] = "letra";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_VIEW_LIST"] = "lista";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_OUTLOOK"] = "Outlook";
$MESS["INTR_COMP_IS_TPL_FILTER_SIMPLE"] = "Pesquisar";
$MESS["INTR_COMP_IS_TPL_FILTER_ALPH"] = "pesquisar por alfabeto";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_VIEW"] = "Exibir";
$MESS["INTR_COMP_IS_TPL_BANNER_OUTLOOK_BUTTON"] = "Exportar";
$MESS["INTR_COMP_IS_TPL_BANNER_OUTLOOK"] = "Você pode exportar os funcionários como os contactos do Microsoft Outlook";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_CARDDAV"] = "Sicronizar via CardDAV";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_CARDDAV_TITLE"] = "Sincronizar os funcionários do registro com software e hardware de apoio CardDAV (iPhone, iPad etc.)";
?>