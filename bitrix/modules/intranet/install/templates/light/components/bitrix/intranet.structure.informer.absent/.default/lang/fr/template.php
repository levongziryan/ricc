<?
$MESS["INTR_ISIA_TPL_ALL"] = "Tous";
$MESS["INTR_ISIA_TPL_DAYS_1"] = "jour";
$MESS["INTR_ISIA_TPL_DAYS"] = "jours";
$MESS["INTR_ISIA_TPL_DAYS_2_4"] = "jour";
$MESS["INTR_ISIA_TPL_TOMORROW"] = "Demain";
$MESS["INTR_ISIA_TPL_NO_ABSENCES"] = "Il n'y a pas d'absents";
$MESS["INTR_ISIA_TPL_TILL"] = "jusqu'à";
$MESS["INTR_ISIA_TPL_SHOW"] = "Afficher:";
$MESS["INTR_ISIA_TPL_THE_DAY_AFTER_TOMORROW"] = "Après-demain";
$MESS["INTR_ISIA_TPL_FROM"] = "de";
$MESS["INTR_ISIA_TPL_TODAY"] = "Aujourd'hui";
$MESS["INTR_ISIA_TPL_NOW"] = "Maintenant";
$MESS["INTR_ISIA_TPL_IN"] = "Dans";
?>