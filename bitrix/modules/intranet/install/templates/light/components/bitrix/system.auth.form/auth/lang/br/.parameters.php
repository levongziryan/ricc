<?
$MESS["SAF_TP_PATH_TO_MYPORTAL"] = "Modelo da Página desktop do usuário";
$MESS["SAF_TP_PATH_TO_SONET_PROFILE"] = "modelo da página do perfil do usuário";
$MESS["SAF_TP_PATH_TO_SONET_BIZPROC"] = "Modelo de página de processo de negocios";
$MESS["SAF_TP_PATH_TO_SONET_GROUP"] = "Modelo de trabalho de grupo de trabalho";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGES"] = "Modelo de página de mensangens do Usuário";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGE_FORM"] = "Modelo de Página de mensangens postada";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGE_FORM_MESS"] = "Modelo de Página de prenchimento de formulario";
$MESS["SAF_TP_PATH_TO_SONET_MESSAGES_CHAT"] = "Modelo de página de chat";
$MESS["SAF_TP_PATH_TO_SONET_GROUPS"] = "Modelo de página dos Usuários do grupo";
$MESS["SAF_TP_PATH_TO_SONET_LOG"] = "Modelo de página de atualizações dos usuários";
$MESS["SAF_TP_PATH_TO_BLOG"] = "Modelo de página dos blogs dos usuários";
$MESS["SAF_TP_PATH_TO_PHOTO"] = "Modelo de página da galeria de fotos do usuário";
$MESS["SAF_TP_PATH_TO_CALENDAR"] = "Modelo de página do calendário dos usuários";
$MESS["SAF_TP_PATH_TO_TASKS"] = "Modelo de página das Tarefas do Usuário URL";
$MESS["SAF_TP_PATH_TO_FILES"] = "Modelo de página dos arquivos dos usuários";
?>