<?
$MESS["SONET_UM_GENERAL"] = "geral";
$MESS["SONET_UM_LOG"] = "Fluxo de Atividades";
$MESS["SONET_UM_FRIENDS"] = "amigos";
$MESS["SONET_UM_GROUPS"] = "grupos";
$MESS["SONET_UM_PHOTO"] = "Foto";
$MESS["SONET_UM_FORUM"] = "forum";
$MESS["SONET_UM_CALENDAR"] = "calendario";
$MESS["SONET_UM_FILES"] = "arquivos";
$MESS["SONET_UM_BLOG"] = "Conversas";
$MESS["SONET_UM_TASKS"] = "tarefas";
$MESS["SONET_UM_SEND_MESSAGE"] = "Enviar mensagem";
$MESS["SONET_UM_VIDEO_CALL"] = "chamada de video";
$MESS["SONET_UM_ONLINE"] = "Online";
$MESS["SONET_UM_ABSENT"] = "(Ausente)";
$MESS["SONET_UM_MESSAGES"] = "mensagens";
$MESS["SONET_UM_BIRTHDAY"] = "hoje é aniversario de um usuário";
$MESS["SONET_UM_HONOUR"] = "O usuário está no Conselho de Honra";
$MESS["SONET_UM_EDIT_PROFILE"] = "Editar Perfil";
$MESS["SONET_UM_EDIT_SETTINGS"] = "editar configurações privada";
$MESS["SONET_UM_EDIT_FEATURES"] = "editar configurações";
$MESS["SONET_UM_SUBSCRIBE"] = "inscrever";
$MESS["SONET_UM_REQUESTS"] = "Convites e pedidos";
?>