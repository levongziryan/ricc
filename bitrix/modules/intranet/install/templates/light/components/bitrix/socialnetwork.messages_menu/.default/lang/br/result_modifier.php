<?
$MESS["SONET_UM_BLOG"] = "Blog";
$MESS["SONET_UM_CALENDAR"] = "calendario";
$MESS["SONET_UM_FILES"] = "arquivos";
$MESS["SONET_UM_FORUM"] = "forum";
$MESS["SONET_UM_FRIENDS"] = "amigos";
$MESS["SONET_UM_GENERAL"] = "geral";
$MESS["SONET_UM_GROUPS"] = "Grupos";
$MESS["SONET_UM_PHOTO"] = "Foto";
$MESS["SONET_UM_TASKS"] = "tarefas";
$MESS["SONET_UM_ONLINE"] = "Online";
$MESS["SONET_UM_ABSENT"] = "(Ausente)";
$MESS["SONET_UM_MICROBLOG"] = "Microblog";
$MESS["SONET_UM_SEARCH"] = "Pesquisar";
?>