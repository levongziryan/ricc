<?
$MESS["SONET_UM_MY_MESSAGES"] = "Mes messages";
$MESS["SONET_UM_MESSAGES"] = "...messages";
$MESS["SONET_UM_INPUT"] = "A/pour moi";
$MESS["SONET_UM_OUTPUT"] = "Emis";
$MESS["SONET_UM_USER"] = "Mon profil";
$MESS["SONET_UM_USER_BAN"] = "Liste noire";
$MESS["SONET_UM_MUSERS"] = "...messages";
$MESS["SONET_UM_LOG"] = "Flux d'activités";
$MESS["SONET_UM_SUBSCRIBE"] = "Abonnement";
$MESS["SONET_UM_TASKS"] = "Abonnement";
$MESS["SONET_UM_BIZPROC"] = "De processus business";
$MESS["SONET_UM_ONLINE"] = "En ligne";
$MESS["SONET_UM_ABSENT"] = "(hors du bureau)";
$MESS["SONET_UM_BIRTHDAY"] = "L'utilisateur a son anniversaire aujourd'hui";
$MESS["SONET_UM_HONOUR"] = "L'utilisateur figure au tableau d'honneur";
$MESS["SONET_UM_EDIT_PROFILE"] = "Modifier le profil";
$MESS["SONET_UM_EDIT_SETTINGS"] = "Changer le mode privé";
$MESS["SONET_UM_EDIT_FEATURES"] = "Changer les paramètres";
?>