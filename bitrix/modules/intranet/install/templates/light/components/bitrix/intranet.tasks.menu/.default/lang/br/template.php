<?
$MESS["INTMT_DELETE_VIEW_CONF"] = "Tem certeza de que quer apagar este ponto de vista?";
$MESS["INTMT_BACK2LIST_DESCR"] = "Voltar para lista de tarefas";
$MESS["INTMT_OUTLOOK"] = "Conecte-se ao Outlook";
$MESS["INTMT_CREATE_FOLDER"] = "criar uma pasta";
$MESS["INTMT_CREATE_TASK_DESCR"] = "Criar uma nova tarefa";
$MESS["INTMT_CREATE_TASK"] = "Criar Tarefa";
$MESS["INTMT_CREATE_VIEW"] = "Criar Exibição";
$MESS["INTMT_CREATE_FOLDER_DESCR"] = "Criar uma nova pasta";
$MESS["INTMT_DEFAULT"] = "Padrão";
$MESS["INTMT_DELETE_VIEW"] = "deletar exibição";
$MESS["INTMT_EDIT_TASK_DESCR"] = "Editar tarefa atual";
$MESS["INTMT_EDIT_TASK"] = "Editar tarefa";
$MESS["INTMT_EDIT_VIEW"] = "Editar exibição";
$MESS["INTMT_OUTLOOK_TITLE"] = "Sincronização de tarefas do Outlook";
$MESS["INTMT_BACK2LIST"] = "tarefas";
$MESS["INTMT_VIEW"] = "exibir";
$MESS["INTMT_VIEW_TASK_DESCR"] = "Exibir tarefa atual";
$MESS["INTMT_VIEW_TASK"] = "Exibir tarefas";
?>