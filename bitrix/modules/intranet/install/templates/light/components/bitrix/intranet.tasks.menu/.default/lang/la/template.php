<?
$MESS["INTMT_BACK2LIST_DESCR"] = "Volver a la lista de tareas";
$MESS["INTMT_BACK2LIST"] = "Tareas";
$MESS["INTMT_EDIT_TASK_DESCR"] = "Editar Tarea Actual";
$MESS["INTMT_EDIT_TASK"] = "Editar Tarea";
$MESS["INTMT_VIEW_TASK_DESCR"] = "Ver Tarea Actual";
$MESS["INTMT_VIEW_TASK"] = "Ver Tarea";
$MESS["INTMT_CREATE_TASK_DESCR"] = "Crear Nueva Tarea";
$MESS["INTMT_CREATE_TASK"] = "Crear Tarea";
$MESS["INTMT_CREATE_FOLDER_DESCR"] = "Crea una nueva carpeta";
$MESS["INTMT_CREATE_FOLDER"] = "Crea Carpeta";
$MESS["INTMT_VIEW"] = "Ver";
$MESS["INTMT_DEFAULT"] = "Predeterminado";
$MESS["INTMT_CREATE_VIEW"] = "Crear Vsta";
$MESS["INTMT_EDIT_VIEW"] = "Editar vista";
$MESS["INTMT_DELETE_VIEW_CONF"] = "¿Usetd está seguro que quiere eliminar esta vista?";
$MESS["INTMT_DELETE_VIEW"] = "Eliminar Vista";
$MESS["INTMT_OUTLOOK"] = "Conéctate a Outlook";
$MESS["INTMT_OUTLOOK_TITLE"] = "Sincronización de tareas de Outlook";
?>