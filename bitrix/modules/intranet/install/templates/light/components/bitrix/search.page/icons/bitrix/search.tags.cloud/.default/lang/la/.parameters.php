<?
$MESS["SEARCH_FONT_MAX"] = "Tamaño de fuente más grande (px)";
$MESS["SEARCH_FONT_MIN"] = "Tamaño de fuente más pequeño (px)";
$MESS["SEARCH_COLOR_OLD"] = "El último color de etiqueta (por ejemplo, \"FEFEFE\")";
$MESS["SEARCH_COLOR_NEW"] = "Primer color de etiqueta (por ejemplo, \"C0C0C0\")";
$MESS["SEARCH_PERIOD_NEW_TAGS"] = "Considerar etiqueta nueva durante (días)";
$MESS["SEARCH_SHOW_CHAIN"] = "Mostrar navegación de ruta de navegación.";
$MESS["SEARCH_COLOR_TYPE"] = "Usa colores degradados";
$MESS["SEARCH_WIDTH"] = "Ancho de la nube de etiquetas (por ejemplo, \"100%\", \"100px\", \"100pt\" o \"100in\")";
?>