<?
$MESS["INTR_COMP_IS_TPL_FILTER_SIMPLE"] = "Buscar";
$MESS["INTR_COMP_IS_TPL_FILTER_ADV"] = "Búsqueda Avanzada";
$MESS["INTR_COMP_IS_TPL_FILTER_ALPH"] = "Buscar por Alfabeto";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_LETTER"] = "Letra";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_VIEW"] = "Ver";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_VIEW_LIST"] = "lista";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_VIEW_TABLE"] = "detalles";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_EXCEL"] = "Excel";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_EXCEL_TITLE"] = "Exportar resultados de búsqueda a MS Excel";
$MESS["INTR_COMP_IS_TPL_FILTER_AZ"] = "A-Z";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_OUTLOOK"] = "Outlook";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_OUTLOOK_TITLE"] = "Lista de exportación de empleados como contactos de Outlook";
$MESS["INTR_COMP_IS_TPL_BANNER_OUTLOOK"] = "Puede exportar empleados como contactos para Microsoft Outlook";
$MESS["INTR_COMP_IS_TPL_BANNER_OUTLOOK_BUTTON"] = "Export";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_CARDDAV"] = "Sincronizar a través de CardDAV";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_CARDDAV_TITLE"] = "Sincronice el registro de empleados con el software y el hardware compatibles con CardDAV (iPhone, iPad, etc.)";
?>