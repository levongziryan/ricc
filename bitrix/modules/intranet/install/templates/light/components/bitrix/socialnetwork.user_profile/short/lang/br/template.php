<?
$MESS["SONET_P_USER_SEX"] = "Sexo:";
$MESS["SONET_P_USER_BIRTHDAY"] = "aniversário:";
$MESS["SONET_C38_T_ONLINE"] = "Online";
$MESS["SONET_C38_T_HONOURED"] = "homenageado";
$MESS["SONET_C38_T_BIRTHDAY"] = "aniversário";
$MESS["SONET_C39_ABSENT"] = "Ausente";
$MESS["SONET_C39_SEND_MESSAGE"] = "Enviar mensagem";
$MESS["SONET_C39_SHOW_MESSAGES"] = "Mostrar Mensagem Log ";
$MESS["SONET_C39_FR_DEL"] = "Sem amizade";
$MESS["SONET_C39_FR_ADD"] = "adicionar amigos";
$MESS["SONET_C39_INV_GROUP"] = "convidar grupo";
$MESS["SONET_C39_EDIT_PROFILE"] = "Editar Perfil";
$MESS["SONET_C39_EDIT_SETTINGS"] = "editar configurações privada";
$MESS["SONET_C39_EDIT_FEATURES"] = "editar configurações";
$MESS["SONET_C39_CONTACT_TITLE"] = "informação do contato";
$MESS["SONET_C39_CONTACT_UNAVAIL"] = "As informações de contato não está disponível. ";
$MESS["SONET_C39_PERSONAL_TITLE"] = "Detalhes pessoais";
$MESS["SONET_C39_PERSONAL_UNAVAIL"] = "As informações pessoais não está disponível. ";
$MESS["SONET_C38_TP_NO_PERMS"] = "Você não tem permissão para ver o perfil deste usuário.";
$MESS["SONET_C39_VIDEO_CALL"] = "Chamada de vídeo";
?>