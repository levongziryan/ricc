<?
$MESS["ISL_ID"] = "ID";
$MESS["ISL_FULL_NAME"] = "Nombre Completo";
$MESS["ISL_NAME"] = "Nombre";
$MESS["ISL_LAST_NAME"] = "Apellido";
$MESS["ISL_SECOND_NAME"] = "Segundo Nombre";
$MESS["ISL_EMAIL"] = "E-Mail";
$MESS["ISL_LOGIN"] = "Iniciar sesión";
$MESS["ISL_DATE_REGISTER"] = "Registrado El:";
$MESS["ISL_PERSONAL_WWW"] = "Sitio web";
$MESS["ISL_PERSONAL_PROFESSION"] = "Título profesional";
$MESS["ISL_PERSONAL_ICQ"] = "ICQ";
$MESS["ISL_PERSONAL_GENDER"] = "Sexo";
$MESS["ISL_PERSONAL_BIRTHDAY"] = "Fecha de Cumpleaños";
$MESS["ISL_PERSONAL_PHOTO"] = "Foto";
$MESS["ISL_PERSONAL_PHONE"] = "Teléfono";
$MESS["ISL_PERSONAL_FAX"] = "Fax";
$MESS["ISL_PERSONAL_MOBILE"] = "Móvil";
$MESS["ISL_PERSONAL_PAGER"] = "Buscador";
$MESS["ISL_PERSONAL_PHONES"] = "Teléfonos";
$MESS["ISL_PERSONAL_POST_ADDRESS"] = "Dirección de envio";
$MESS["ISL_PERSONAL_CITY"] = "Ciudad";
$MESS["ISL_PERSONAL_STREET"] = "Dirección de la calle";
$MESS["ISL_PERSONAL_STATE"] = "Región/Estado";
$MESS["ISL_PERSONAL_MAILBOX"] = "P.O. Box";
$MESS["ISL_PERSONAL_ZIP"] = "Zip";
$MESS["ISL_PERSONAL_COUNTRY"] = "País";
$MESS["ISL_PERSONAL_NOTES"] = "Notas";
$MESS["ISL_PERSONAL_POSITION"] = "Cargo";
$MESS["ISL_WORK_PHONE"] = "Teléfono del Trabajo";
$MESS["ISL_ADMIN_NOTES"] = "Notas del Administrador";
$MESS["ISL_XML_ID"] = "ID externo";
$MESS["INTR_IS_TPL_PARAM_SHOW_FROM_ROOT"] = "Mostrar siempre la estructura desde el Root";
$MESS["INTR_IS_TPL_PARAM_MAX_DEPTH"] = "Profundidad máx. del árbol (0 - todo)";
$MESS["INTR_IS_TPL_PARAM_MAX_DEPTH_FIRST"] = "Profundi máx. del árbol en la primera página (0 - todo)";
$MESS["INTR_IS_TPL_PARAM_COLUMNS"] = "Recuento de Columnas";
$MESS["INTR_IS_TPL_PARAM_COLUMNS_FIRST"] = "Columnas en la Primera Página";
$MESS["INTR_IS_TPL_PARAM_SHOW_SECTION_INFO"] = "Mostrar información del departamento";
$MESS["INTR_IS_TPL_PARAM_USER_PROPERTY"] = "Campos personalizados para exportación";
$MESS["ISL_WORK_FAX"] = "Fax de trabajo";
?>