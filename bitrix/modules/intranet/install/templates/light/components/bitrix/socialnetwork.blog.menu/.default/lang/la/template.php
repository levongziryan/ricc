<?
$MESS["BLOG_MENU_DRAFT_MESSAGES"] = "Borradores";
$MESS["BLOG_MENU_DRAFT_MESSAGES_TITLE"] = "Ver conversaciones no publicadas";
$MESS["BLOG_MENU_MODERATION_MESSAGES"] = "Moderación";
$MESS["BLOG_MENU_MODERATION_MESSAGES_TITLE"] = "Ver mensajes en espera de moderación";
$MESS["BLOG_MENU_4ME_ALL"] = "Todo";
$MESS["BLOG_MENU_MINE"] = "Mi";
$MESS["BLOG_MENU_4ME"] = "Para mi";
$MESS["BLOG_MENU_MINE_TITLE"] = "Ver mis mensajes";
$MESS["BLOG_MENU_4ME_TITLE"] = "Ver mensajes dirigidos a mí";
$MESS["BLOG_MENU_4ME_ALL_TITLE"] = "Ver todas las conversaciones";
$MESS["BLOG_MENU_4ME_DR"] = "Mi departamento";
$MESS["BLOG_MENU_4ME_DR_TITLE"] = "Ver conversaciones de mi departamento";
?>