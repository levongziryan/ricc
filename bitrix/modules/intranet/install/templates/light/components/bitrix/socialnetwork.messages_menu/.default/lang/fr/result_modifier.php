<?
$MESS["SONET_UM_ABSENT"] = "(hors du bureau)";
$MESS["SONET_UM_BLOG"] = "Blog";
$MESS["SONET_UM_GROUPS"] = "Groupes";
$MESS["SONET_UM_FRIENDS"] = "Amis";
$MESS["SONET_UM_TASKS"] = "Tâches";
$MESS["SONET_UM_CALENDAR"] = "Calendrier";
$MESS["SONET_UM_MICROBLOG"] = "messages";
$MESS["SONET_UM_ONLINE"] = "En ligne";
$MESS["SONET_UM_SEARCH"] = "Recherche";
$MESS["SONET_UM_GENERAL"] = "De base";
$MESS["SONET_UM_FILES"] = "Fichiers";
$MESS["SONET_UM_FORUM"] = "Forum";
$MESS["SONET_UM_PHOTO"] = "Photo";
?>