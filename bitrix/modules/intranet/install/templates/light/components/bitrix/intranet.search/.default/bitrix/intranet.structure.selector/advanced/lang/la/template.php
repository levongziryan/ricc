<?
$MESS["INTR_ISS_PARAM_DEPARTMENT"] = "Departamento";
$MESS["INTR_ISS_PARAM_FIO"] = "Nombre";
$MESS["INTR_COMP_IS_TPL_SECH"] = "Buscar";
$MESS["INTR_ISS_PARAM_EMAIL"] = "E-mail";
$MESS["INTR_ISS_BUTTON_SUBMIT"] = "Buscar";
$MESS["INTR_COMP_IS_TPL_MY_OFFICE"] = "mi oficina";
$MESS["INTR_ISS_BUTTON_CANCEL"] = "Cancelar";
$MESS["INTR_ISS_PARAM_PHONE"] = "Teléfono";
$MESS["INTR_ISS_PARAM_PHONE_INNER"] = "Número de extensión";
$MESS["INTR_ISS_PARAM_KEYWORDS"] = "Palabra clave";
?>