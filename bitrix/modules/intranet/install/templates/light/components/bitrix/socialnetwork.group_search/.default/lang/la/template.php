<?
$MESS["SONET_C24_T_SEARCH_TITLE"] = "Buscar en Grupos";
$MESS["SONET_C24_T_SEARCH"] = "Buscar";
$MESS["SONET_C24_T_SUBJECT"] = "El Tema";
$MESS["SONET_C24_T_ANY"] = "Alguna";
$MESS["SONET_C24_T_DO_SEARCH"] = "&nbsp;&nbsp;Buscar&nbsp;&nbsp;";
$MESS["SONET_C24_T_DO_CANCEL"] = "&nbsp;&nbsp;Cancelar&nbsp;&nbsp;";
$MESS["SONET_C24_T_CREATE_GROUP"] = "Crear Grupo";
$MESS["SONET_C24_T_SUBJ"] = "Tema";
$MESS["SONET_C24_T_MEMBERS"] = "Miembros";
$MESS["SONET_C24_T_ACTIVITY"] = "Ultimo acceso";
$MESS["SONET_C24_T_ORDER_REL"] = "Clasificar por relevancia";
$MESS["SONET_C24_T_ORDER_DATE"] = "Clasificar por Fecha";
$MESS["SONET_C39_ARCHIVE_GROUP"] = "Archivos de Grupo";
?>