<?
$MESS["P_ADD_ALBUM"] = "Novo album";
$MESS["P_UPLOAD"] = "carregar foto";
$MESS["P_SECTION_EDIT"] = "Editar album";
$MESS["P_SECTION_EDIT_ICON"] = "editar edição do album";
$MESS["P_SECTION_DELETE"] = "excluir album";
$MESS["P_SECTION_DELETE_ASK"] = "Tem certeza que deseja excluir o álbum de forma irreversível?";
$MESS["P_ALBUM_IS_NOT_ACTIVE"] = "O álbum está escondido.";
$MESS["P_ALBUM_IS_NOT_ACTIVE_AND_PASSWORDED"] = "O álbum está escondido e senha protegida.";
$MESS["P_ALBUM_IS_PASSWORDED"] = "O álbum é protegido por senha.";
?>