<?
$MESS["TP_BSP_USE_SUGGEST"] = "Mostrar indicaciones de frase de búsqueda";
$MESS["TP_BSP_SHOW_ITEM_TAGS"] = "Mostrar Etiquetas de Documentos";
$MESS["TP_BSP_TAGS_INHERIT"] = "Área de búsqueda estrecha";
$MESS["TP_BSP_SHOW_ITEM_DATE_CHANGE"] = "Mostrar fecha modificada";
$MESS["TP_BSP_SHOW_ORDER_BY"] = "Mostrar Orden de Clasificación";
$MESS["TP_BSP_SHOW_TAGS_CLOUD"] = "Mostrar Nube de Etiquetas";
$MESS["TP_BSP_FONT_MAX"] = "Tamaño de fuente más grande (px)";
$MESS["TP_BSP_FONT_MIN"] = "Tamaño de fuente más pequeña (px)";
$MESS["TP_BSP_COLOR_OLD"] = "El último color de etiqueta (por ejemplo, \"FEFEFE\")";
$MESS["TP_BSP_COLOR_NEW"] = "Primer color de etiqueta (por ejemplo, \"C0C0C0\")";
$MESS["TP_BSP_CNT"] = "Por frecuencia";
$MESS["TP_BSP_COLOR_TYPE"] = "Usa colores degradados";
$MESS["TP_BSP_NAME"] = "Por nombre";
$MESS["TP_BSP_PAGE_ELEMENTS"] = "Numero de etiquetas";
$MESS["TP_BSP_PERIOD"] = "Buscar etiquetas dentro de (días)";
$MESS["TP_BSP_PERIOD_NEW_TAGS"] = "Considerar etiqueta nueva durante (días)";
$MESS["TP_BSP_WIDTH"] = "Ancho de la nube de etiquetas (por ejemplo, \"100%\", \"100px\", \"100pt\" o \"100in\")";
$MESS["TP_BSP_URL_SEARCH"] = "Ruta a la página de búsqueda (relativa a la raíz del sitio)";
$MESS["TP_BSP_SHOW_CHAIN"] = "Mostrar la ruta de navegación";
$MESS["TP_BSP_SORT"] = "Rango de etiquetas";
?>