<?
$MESS["EXTRANET_NEW_MESSAGE_SUBJECT"] = "#SITE_NAME# : Vous avez un nouveau message";
$MESS["EXTRANET_NEW_MESSAGE_MESSAGE"] = "Bienvenue de la part de #SITE_NAME#!
------------------------------------------

Bonjour, #USER_NAME# !

Vous avez reçu un message de la part de #SENDER_NAME# #SENDER_LAST_NAME# :

------------------------------------------
#MESSAGE#
------------------------------------------

Lien vers le message :

http://#SERVER_NAME#/company/personal/messages/chat/#SENDER_ID#/

Cette notification est générée automatiquement.
";
?>