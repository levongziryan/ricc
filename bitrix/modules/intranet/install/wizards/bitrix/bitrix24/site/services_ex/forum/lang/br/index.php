<?
$MESS["COMMENTS_EXTRANET_GROUP_NAME"] = "Comentários: Extranet";
$MESS["HIDDEN_EXTRANET_GROUP_NAME"] = "Fóruns ocultos: Extranet";
$MESS["USERS_AND_GROUPS_EXTRANET_FORUM_NAME"] = "Usuário e grupo de Fóruns: Extranet";
$MESS["USERS_AND_GROUPS_EXTRANET_FORUM_DESCRIPTION"] = "Fóruns de usuários individuais e grupos de site da extranet.";
$MESS["GROUPS_AND_USERS_FILES_COMMENTS_EXTRANET_NAME"] = "Comentários em arquivos do usuário: Extranet";
$MESS["GROUPS_AND_USERS_TASKS_COMMENTS_EXTRANET_NAME"] = "Comentários no usuário e grupo Tarefas: Extranet";
$MESS["GROUPS_AND_USERS_PHOTOGALLERY_COMMENTS_EXTRANET_NAME"] = "Comentários Sobre usuário e grupo Galerias de Fotos: Extranet";
?>