<?
$MESS["iblock_dep_dep"] = "Service";
$MESS["iblock_dep_created"] = "Créé";
$MESS["iblock_dep_changed"] = "Modifié";
$MESS["iblock_dep_name"] = "Nom du service";
$MESS["iblock_dep_parent"] = "Service supérieur";
$MESS["iblock_dep_chief"] = "Chef";
$MESS["iblock_dep_pict"] = "Image";
$MESS["iblock_dep_desc"] = "Description";
$MESS["iblock_dep_addit"] = "Supplémentaire";
$MESS["iblock_dep_act"] = "Le service est actif";
$MESS["iblock_dep_sort"] = "Classification";
$MESS["iblock_dep_code"] = "Code symbolique";
$MESS["iblock_dep_det_pict"] = "Image détaillée";
$MESS["iblock_dep_userprop"] = "Propriété utilisateur";
$MESS["iblock_dep_userprop_add"] = "Ajouter une propriété utilisateur";
$MESS["iblock_dep_name1"] = "Bitrix";
$MESS["iblock_dep_name2"] = "Service comptabilité";
$MESS["iblock_dep_name3"] = "Service des ventes";
$MESS["iblock_dep_name4"] = "Service informatique";
$MESS["iblock_dep_name5"] = "Service marketing";
?>