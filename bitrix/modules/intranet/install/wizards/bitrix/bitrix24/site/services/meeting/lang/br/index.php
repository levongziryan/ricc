<?
$MESS["MEETING_TITLE"] = "Portal de Implementação Intranet";
$MESS["MEETING_DESCRIPTION"] = "Nós vamos ter que realizar uma reunião para discutir a implantação do portal intranet em nossa empresa.";
$MESS["MEETING_PLACE"] = "Escritório do CEO";
$MESS["MEETING_ITEM_TITLE_1"] = "Análise de soluções disponíveis";
$MESS["MEETING_ITEM_TITLE_2"] = "Definir etapas de implementação; nomear pessoas responsáveis";
$MESS["MEETING_ITEM_TITLE_3"] = "Planos para o Natal";
?>