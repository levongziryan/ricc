<?
$MESS["NEWS_TYPE_NAME"] = "Actualités";
$MESS["NEWS_ELEMENT_NAME"] = "Actualités";
$MESS["STRUCTURE_TYPE_NAME"] = "Structure de l’entreprise";
$MESS["STRUCTURE_ELEMENT_NAME"] = "Éléments";
$MESS["STRUCTURE_SECTION_NAME"] = "Sections";
$MESS["SERVICES_TYPE_NAME"] = "Services";
$MESS["SERVICES_ELEMENT_NAME"] = "Éléments";
$MESS["SERVICES_SECTION_NAME"] = "Sections";
$MESS["EVENTS_TYPE_NAME"] = "Calendrier";
$MESS["EVENTS_ELEMENT_NAME"] = "Évènements";
$MESS["EVENTS_SECTION_NAME"] = "Calendriers";
$MESS["PHOTOS_TYPE_NAME"] = "Galerie photo";
$MESS["PHOTOS_ELEMENT_NAME"] = "Photos";
$MESS["PHOTOS_SECTION_NAME"] = "Albums";
$MESS["LISTS_TYPE_NAME"] = "Listes";
$MESS["LISTS_ELEMENT_NAME"] = "Éléments";
$MESS["LISTS_SECTION_NAME"] = "Sections";
$MESS["LISTS_SOCNET_TYPE_NAME"] = "Listes des réseaux sociaux";
$MESS["LISTS_SOCNET_ELEMENT_NAME"] = "Éléments";
$MESS["LISTS_SOCNET_SECTION_NAME"] = "Sections";
?>