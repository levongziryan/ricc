<?
$MESS["BLG_NAME"] = "Блог користувача";
$MESS["BLOG_DEMO_GROUP_SOCNET"] = "Група для соціальної мережі";
$MESS["BLOG_DEMO_CATEGORY_1"] = "Поради";
$MESS["BLOG_DEMO_CATEGORY_2"] = "Підказки";
$MESS["BLOG_DEMO_MESSAGE_TITLE_1"] = "Офіційно відкрито корпоративний портал нашої компанії";
$MESS["BLOG_DEMO_MESSAGE_BODY_1"] = "Сьогодні офіційно почав роботу корпоративний портал нашої компанії.

Кожен співробітник отримав право на користування загальною інформацією та індивідуальний пароль для управління своїм персональним розділом.

Доступ, що налаштовується до певних розділів порталу, дозволяє співробітникам відділів спільно працювати над документами, створювати робочі групи з проектів та працювати над загальною звітністю.

Мета створення порталу — максимально спростити процедуру пошуку потрібних співробітників в різних відділах компанії.
Швидкий і сучасний спосіб комунікації з використанням інструментів порталу — надійний спосіб відійти від паперів, перевести інформаційний потік в електронний вигляд.

Інформацію технічного, юридичного характеру, нормативні акти, необхідні в роботі різних підрозділів, співробітники компанії зможуть отримувати через новий корпоративний портал.

Користувачі мають право брати участь у форумах, залишати заявки в сервісні служби компанії, обмінюватися документами, обговорювати новини компанії і завжди отримувати саму оперативну інформацію безпосередньо від керівників компанії.

Портал повинен стати універсальним та зручним інструментом у житті нашої компанії!

Ми вітаємо всіх з відкриттям нашого корпоративного порталу! ";
$MESS["BLOG_DEMO_COMMENT_TITLE"] = "Цікаво";
$MESS["BLOG_DEMO_COMMENT_BODY"] = "Треба буде обов'язково спробувати!";
$MESS["BPC_SONET_POST_TITLE"] = "додав(-ла) повідомлення \"#TITLE#\" в блог";
$MESS["BPC_SONET_COMMENT_TITLE"] = "додав(-ла) коментар до повідомлення \"#TITLE#\" в блозі";
$MESS["BLOG_DEMO_MESSAGE_TITLE"] = "Починаємо роботу в нашому «Бітрікс24»!";
$MESS["BLOG_DEMO_MESSAGE_BODY"] = "\"Сьогодні ми створили свій «Бітрікс24»! Це наше з вами спільний робочий простір. Тут ми зможемо спілкуватися, працювати разом над проектами та багато іншого. І не важливо, в офісі ви, у відрядженні або працюєте віддалено — тепер ми завжди на зв'язку і всі питання моментально обговоримо і вирішимо у Бітрікс24.

У Бітрікс24 можна обговорювати питання з колегами в «живий стрічці», листуватися один на один, спілкуватися в групових чатах, робити відеодзвінки колегам, ставити завдання, планувати свій день у календарях і т. д.

[B]З чого почати?[/B]

1. Почнемо з тренування Поставте \"лайк\" цьому повідомленню. Подивіться, кому з ваших колег це вже подобається.

[IMG WIDTH=600 HEIGHT=350]/images/demo_post/ru/1.png[/IMG]

2. Додайте свій коментар або напишіть своє повідомлення на всіх працівників або тільки на деяких.

[IMG WIDTH=400 HEIGHT=200]/images/demo_post/ru/2.png[/IMG]

3. Встановіть собі безкоштовний мобільний додаток «Бітрікс24» на смартфон iOS або Android і десктоп-додаток на ваш комп'ютер (Windows, MacOS або Linux):

[URL=https://itunes.apple.com/ru/app/bitrix24/id561683423?l=ru&ls=1&mt=8][IMG]/images/demo_post/appstore.png[/IMG][/URL] [URL=https://play.google.com/store/apps/details?id=com.bitrix24.android][IMG]/images/demo_post/googleplay.png[/IMG][/URL] [URL=http://dl.bitrix24.com/b24/bitrix24_desktop.exe][IMG]/images/demo_post/windows.png[/IMG][/URL] [URL=http://dl.bitrix24.com/b24/bitrix24_desktop.dmg][IMG]/images/demo_post/mac.png[/IMG][/URL] [URL=https://github.com/buglloc/brick][IMG]/images/demo_post/linux.png[/IMG][/URL]

Якщо у вас з'являться питання по роботі з Бітрікс24, відкрийте Підтримку24 — там є відповіді практично на 95% питань, а ще відеоуроки і навчальні курси.\"";
$MESS["BLOG_DEMO_MESSAGE_TITLE1"] = "Починаємо працювати в Бітрікс24!";
$MESS["BLOG_DEMO_MESSAGE_BODY1"] = "Тут ми будемо спілкуватися в «живий стрічці» і чатах, вести проекти, ставити завдання, планувати день, працювати з клієнтами і багато іншого. У офісі, у відрядженні, віддалено — з Бітрікс24 ми завжди на зв'язку і всі питання моментально обговоримо і вирішимо. 

З чого почати? Поставте «лайк» цьому повідомленню, прокоментуйте, напишіть своє повідомлення на всіх працівників або тільки на деяких. 

[IMG WIDTH=1004 HEIGHT=565]https://www.bitrix24.ua/images/b24_ua/post_n.png[/IMG]

[B]Встановіть безкоштовний мобільний застосунок «Бітрікс24» [/B]на смартфон iOS або Android і [B]десктоп-застосунок на ваш комп'ютер[/B] (Windows, MacOS або Linux). Чат, повідомлення в Живій стрічці, завдання, файли, контакти клієнтів і операції в CRM - все буде доступно з телефону.

[IMG WIDTH=300 HEIGHT=201]https://www.bitrix24.ua/images/b24_ua/mob_tr.png[/IMG][IMG WIDTH=340 HEIGHT=180]https://www.bitrix24.ua/images/b24_ua/desk_tr.png[/IMG]

[IMG WIDTH=60 HEIGHT=1]https://www.bitrix24.ua/images/b24_ua/i.png[/IMG][URL=https://itunes.apple.com/ua/app/bitrix24/id561683423?l=ru&ls=1&mt=8][IMG WIDTH=91 HEIGHT=28]https://www.bitrix24.ua/images/b24_ua/as001.png[/IMG][/URL] [URL=https://play.google.com/store/apps/details?id=com.bitrix24.android][IMG WIDTH=98 HEIGHT=25]https://www.bitrix24.ua/images/b24_ua/gp005.png[/IMG][/URL][IMG WIDTH=120 HEIGHT=1]https://www.bitrix24.ua/images/b24_ua/i.png[/IMG][URL=http://dl.bitrix24.com/b24/bitrix24_desktop.dmg][IMG WIDTH=74 HEIGHT=31]https://www.bitrix24.ua/images/b24_ua/mac001.png[/IMG][/URL] [URL=http://dl.bitrix24.com/b24/bitrix24_desktop.exe][IMG WIDTH=88 HEIGHT=26]https://www.bitrix24.ua/images/b24_ua/win002.png[/IMG][/URL] [URL=https://github.com/buglloc/brick][IMG WIDTH=67 HEIGHT=28]https://www.bitrix24.ua/images/b24_ua/linux003.png[/IMG][/URL]";
$MESS["BLOG_DEMO_MESSAGE_TITLE2"] = "Як працювати із завданнями Бітрікс24";
$MESS["BLOG_DEMO_MESSAGE_BODY2"] = "Наведіть порядок у своїх завданнях, встигайте більше, ефективніше контролюйте, як виконуються ваші доручення. 
[IMG WIDTH=1367 HEIGHT=713]https://www.bitrix24.ua/images/b24_ua/tasks_how.png[/IMG] 

[B]З чого почати?[/B]

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.ru/images/b24_ua/1p.png[/IMG] Перенесіть ваші завдання в Бітрікс24, оцініть, наскільки це зручно і швидко. 

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.ru/images/b24_ua/2p.png[/IMG] Слідкуйте за строками через гру «Боротьба з лічильниками» :) Мета - жодної «червоної лампочки» в списку завдань, адже кожен лічильник — це не виконане вчасно завдання. 

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.ru/images/b24_ua/3p.png[/IMG] Поставте автоматичні нагадування: собі або відповідальному за завдання.";
$MESS["BLOG_DEMO_MESSAGE_TITLE3"] = "З чого почати роботу в CRM";
$MESS["BLOG_DEMO_MESSAGE_BODY3"] = "В [URL=/crm/stream/]Бітрікс24.CRM[/URL] збирається інформація про клієнтів – про компанії та співробітників, угоди, комерційні пропозиції, рахунки, а також вся історія (дзвінки, завдання, листування, інформація про зустрічі). 

[IMG WIDTH=1115 HEIGHT=573]https://www.bitrix24.ua/images/b24_ua/b24crm.png[/IMG]

[B]
[/B][B]Почати працювати в CRM дуже просто! [/B]

Підключіть до Бітрікс24: 

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.ua/images/b24_ua/1p.png[/IMG] Електронну пошту через [URL=/company/personal/mail/]Email-трекер[/URL]
[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.ua/images/b24_ua/2p.png[/IMG] [URL=/settings/telephony/lines.php]Віртуальну АТС[/URL] для дзвінків клієнтам
[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.ua/images/b24_ua/3p.png[/IMG] Групи у соцмережах до [URL=/settings/openlines/]Відкритих ліній[/URL]
[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.ua/images/b24_ua/4p.png[/IMG] [URL=/settings/openlines/]Безкоштовний онлайн-чат[/URL] на сайт 
[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.ua/images/b24_ua/5p.png[/IMG] [URL=/crm/webform/]CRM-форми[/URL] 
[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.ua/images/b24_ua/6p.png[/IMG] 1С через [URL=/marketplace/app/2/]1С-трекер[/URL] 

і CRM буде непомітно забирати все нові контакти, зберігати історію спілкування з клієнтами, і вам не доведеться переносити все це вручну. Звичайно, ви завжди можете додати клієнта вручну або імпортувати базу з .csv або поштових програм. 
[B]
[/B]Після цього можна налаштувати стадії угод у розділі Налаштування > Довідники (ваші стадії можуть бути будь-які та відрізнятися від стандартних) і переходити до роботи. 

[URL=/crm/stream/][IMG WIDTH=133 HEIGHT=32]https://www.bitrix24.ua/images/b24_ua/go_to_crm.png[/IMG][/URL] 



[B]Вам потрібно більше інформації про роботу в Бітрікс24?[/B] 

[IMG WIDTH=160 HEIGHT=134]https://www.bitrix24.ua/images/b24_ua/cloudman24.png[/IMG] [IMG WIDTH=138 HEIGHT=138]https://www.bitrix24.ua/images/b24_ua/marta.png[/IMG]
Подивіться підказки в Підтримці24 - в правому верхньому куті на кожній сторінці є іконка [IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.ua/images/b24_ua/help24.png[/IMG]. Тут зібрані поради, як працювати з CRM, Завданнями, Диском та іншими інструментами, відеоуроки та навчальні курси. 

Ще ви можете запитати Марту - це ваш особистий віртуальний помічник у чаті Бітрікс24, її легко знайти пошуком [IMG WIDTH=16 HEIGHT=16]/bitrix/images/blog/smile/icon_smile.png[/IMG] ";
?>