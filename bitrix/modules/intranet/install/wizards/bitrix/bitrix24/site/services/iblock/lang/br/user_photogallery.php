<?
$MESS["W_IB_USER_PHOTOG_TAB1"] = "edit1--#--Foto--,--ACTIVE--#--  Ativo--,--NAME--#--*Titulo--,--IBLOCK_ELEMENT_PROP_VALUE--#----Valores da Propriedade--,--PREVIEW_PICTURE--#--  Prévisualizar Imagem--,--DETAIL_PICTURE--#--  Imagem Detalhada--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB2"] = "--#--  Original--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB3"] = "--#--  Classificação--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB4"] = "--#--  Votos--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB5"] = "--#--  Total de Votos--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB6"] = "--#--  O item foi aprovado--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB7"] = "--#--  Item Publicado--;--edit6--#--Descrição--,--PREVIEW_TEXT--#--  Prévisualizar Texto--,--DETAIL_TEXT--#--  Descrição Detalhada--;--edit2--#--Seções--,--SECTIONS--#--  Seções--;--edit3--#--Informação Adicional--,--SORT--#--  Índice de Class";
$MESS["SONET_PHOTO_LOG_1"] = "#AUTHOR_NAME# Adicionou uma nova foto.";
$MESS["SONET_PHOTOPHOTO_LOG_1"] = "#AUTHOR_NAME# Adicionou uma nova foto \"#TITLE#\".";
$MESS["SONET_PHOTO_LOG_2"] = "fotos (#COUNT#)";
$MESS["SONET_PHOTO_LOG_TEXT"] = "Novas fotos:<div class='notificationlog'>#LINKS#</div><a href=\"#HREF#\">Ver álbum</a>.";
$MESS["SONET_PHOTO_LOG_MAIL_TEXT"] = "Novas fotos inseridas como \"#TITLE#\".";
$MESS["SONET_LOG_GUEST"] = "Convidado";
?>