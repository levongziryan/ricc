<?
$MESS["W_IB_USER_PHOTOG_TAB1"] = "edit1--#--Foto--,--ACTIVE--#--  Eintrag ist aktiv--,--NAME--#--*Überschrift--,--IBLOCK_ELEMENT_PROP_VALUE--#----Eigenschaften--,--PREVIEW_PICTURE--#--  Vorschaubild--,--DETAIL_PICTURE--#--  Bild--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB2"] = "--#--  Original--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB3"] = "--#--  Bewertung--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB4"] = "--#--  Stimmen--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB5"] = "--#--  Summe der abgegebenen Stimmen--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB6"] = "--#--  Genehmigt--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB7"] = "--#--  Veröffentlicht--;--edit6--#--Beschreibung--,--PREVIEW_TEXT--#--  Vorschautext--,--DETAIL_TEXT--#--  Detaillierte Beschreibung--;--edit2--#--Bereiche--,--SECTIONS--#--  Bereiche--;--edit3--#--Weitere Infos--,--SORT--#--  Sortierindex--,--CODE--#";
$MESS["SONET_PHOTO_LOG_1"] = "#AUTHOR_NAME# hat ein Foto hinzugefügt";
$MESS["SONET_PHOTOPHOTO_LOG_1"] = "#AUTHOR_NAME# hat ein neues Foto \"#TITLE#\" hinzugefügt.";
$MESS["SONET_PHOTO_LOG_2"] = "Fotos (#COUNT#)";
$MESS["SONET_PHOTO_LOG_TEXT"] = "Neue Fotos: <div class='notificationlog'>#LINKS#</div> <a href=\"#HREF#\">Album anzeigen</a>.";
$MESS["SONET_PHOTO_LOG_MAIL_TEXT"] = "Neue Fotos wurden zum Album \"#TITLE#\" hinzugefügt.";
$MESS["SONET_LOG_GUEST"] = "Besucher";
?>