<?
$MESS["CRM_TOP_LINKS_ITEM_NAME"] = "CRM";
$MESS["CRM_GADGET_MY_LEAD_TITLE"] = "Meus Leads";
$MESS["CRM_GADGET_NEW_LEAD_TITLE"] = "Novos Leads";
$MESS["CRM_GADGET_CLOSED_DEAL_TITLE"] = "Fechar Deals";
$MESS["CRM_GADGET_LAST_EVENT_TITLE"] = "Eventos Recentes";
$MESS["CRM_GADGET_NEW_CONTACT_TITLE"] = "Novos Contactos";
$MESS["CRM_GADGET_NEW_COMPANY_TITLE"] = "Novas Empresas";
$MESS["CRM_ROLE_ADMIN"] = "Administrador";
$MESS["CRM_ROLE_DIRECTOR"] = "Diretor geral";
$MESS["CRM_ROLE_CHIF"] = "Chefe de departamento";
$MESS["CRM_ROLE_MAN"] = "Gerente";
?>