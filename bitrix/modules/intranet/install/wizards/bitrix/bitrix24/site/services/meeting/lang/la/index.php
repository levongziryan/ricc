<?
$MESS["MEETING_TITLE"] = "Implementación del Portal de Intranet";
$MESS["MEETING_DESCRIPTION"] = "Tendremos que celebrar una reunión para discutir el despliegue del portal de intranet en nuestra empresa.";
$MESS["MEETING_PLACE"] = "Oficina del CEO";
$MESS["MEETING_ITEM_TITLE_1"] = "Revisión de las soluciones disponibles";
$MESS["MEETING_ITEM_TITLE_2"] = "Definir pasos de implementación; Nombrar personas responsables";
$MESS["MEETING_ITEM_TITLE_3"] = "Planes para navidad";
?>