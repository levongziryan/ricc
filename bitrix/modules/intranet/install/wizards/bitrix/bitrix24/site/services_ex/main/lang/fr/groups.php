<?
$MESS["EXTRANET_ADMIN_GROUP_DESC"] = "Les administrateurs disposent de toutes les permissions pour accéder, gérer et éditer les ressources du site extranet.";
$MESS["EXTRANET_GROUP_DESC"] = "Tous les utilisateurs ayant accès au site extranet.";
$MESS["EXTRANET_CREATE_WG_GROUP_DESC"] = "Tous les utilisateurs disposant des permissions nécessaires pour créer des groupes utilisateur sur le site extranet.";
$MESS["EXTRANET_CREATE_WG_GROUP_NAME"] = "Peuvent créer des groupes utilisateur extranet";
$MESS["EXTRANET_MENUITEM_NAME"] = "Extranet";
$MESS["EXTRANET_ADMIN_GROUP_NAME"] = "Administrateurs de site extranet";
$MESS["EXTRANET_SUPPORT_GROUP_NAME"] = "Assistance extranet";
$MESS["EXTRANET_GROUP_NAME"] = "Utilisateurs extranet";
$MESS["EXTRANET_SUPPORT_GROUP_DESC"] = "Personnel chargé de l'assistance du site extranet.";
?>