<?
$MESS["BLG_NAME"] = "Blog de";
$MESS["BLOG_DEMO_GROUP_SOCNET"] = "Grupo de rede social";
$MESS["BLOG_DEMO_CATEGORY_1"] = "Conselho";
$MESS["BLOG_DEMO_CATEGORY_2"] = "Dicas";
$MESS["BLOG_DEMO_MESSAGE_TITLE_1"] = "A abertura do portal intranet da empresa";
$MESS["BLOG_DEMO_MESSAGE_BODY_1"] = "Caros colegas, o Portal Intranet começou oficialmente hoje.

Efetivamente, isto significa que cada funcionário possui agora a permissão para exibir informações públicas, e dado uma senha para acessar sua área privada.

Acesso configurável para as áreas do portal nos proporciona características do Trabalho em equipe: agora podemos editar documentos, criar grupos de trabalho, elaborar em relatórios - juntos!

O objectivo do portal é tornar a cadeia de comunicação entre os trabalhadores o mais curto possível. O portal, sendo o meio rápido e moderno de comunicação, é o melhor método para evitar o trabalho de papel e transformar o fluxo de informações da empresa em um curso eletrônico.
 
Usando o portal intranet, os funcionários da empresa podem obter qualquer refêrencia técnica ou informação jurídica, incluindo os padrões e identidade corporativa.

Os usuários podem postar em fóruns, enviar solicitações de suporte técnico e serviços de abastecimento; compartilhamento e troca de documentos; discutir as notícias da empresa e obter as informações mais recentes da gestão da empresa.

O portal será a parte essencial do negócio e a vida de nossa empresa! ";
$MESS["BLOG_DEMO_COMMENT_TITLE"] = "Interessante!";
$MESS["BLOG_DEMO_COMMENT_BODY"] = "Vou tentar isso!";
$MESS["BPC_SONET_POST_TITLE"] = "criou um post no blog \"#TITLE#\"";
$MESS["BPC_SONET_COMMENT_TITLE"] = "adicionou um comentário no blog post \"#TITLE#\"";
$MESS["BLOG_DEMO_MESSAGE_TITLE"] = "Bitrix24: Estamos começando!";
$MESS["BLOG_DEMO_MESSAGE_BODY"] = "Hoje é o glorioso dia em que criamos nosso próprio Bitrix24, nossa espaço de trabalho compartilhado. Este é o lugar para se comunicar, trabalhar, desenvolver projetos, resolver problemas e fazer outras coisas, de uma maneira nova e mais produtiva. Não importa onde você esteja, no escritório, viajando a trabalho ou trabalhando de casa, nós podemos alcançá-lo e você pode nos alcançar, a qualquer momento que tenhamos trabalho a fazer ou algo a discutir.

O Bitrix24 é uma forma natural de organizar discussões em grupo em Fluxo de Atividade, se comunicar em bate-papos individuais ou em grupo, fazer chamadas em vídeo, criar e controlar tarefas, planejar seu dia de trabalho e muito mais.

[B]Então, por onde começamos?[/B]

1. Vamos começar com algo simples. Como esta postagem e ver quem também curte.

[IMG WIDTH=600 HEIGHT=300]/images/demo_post/en/1.png[/IMG]

2. Adicione seu comentário e envie uma mensagem para todas as pessoas ou apenas para pessoas selecionadas.

[IMG WIDTH=600 HEIGHT=310]/images/demo_post/en/2.png[/IMG]

3. Instale, gratuitamente, o app móvel Bitrix24 no seu celular iOS ou Android e o app desktop no seu computador (Windows, MacOS ou Linux):

[URL=https://itunes.apple.com/app/bitrix24/id561683423][IMG]/images/demo_post/appstore.png[/IMG][/URL] [URL=https://play.google.com/store/apps/details?id=com.bitrix24.android][IMG]/images/demo_post/googleplay.png[/IMG][/URL]  [URL=http://dl.bitrix24.com/b24/bitrix24_desktop.exe][IMG]/images/demo_post/windows.png[/IMG][/URL] [URL=http://dl.bitrix24.com/b24/bitrix24_desktop.dmg][IMG]/images/demo_post/mac.png[/IMG][/URL] [URL=https://github.com/buglloc/brick][IMG]/images/demo_post/linux.png[/IMG][/URL]

Sempre que você tiver dúvidas sobre como usar o nosso Bitrix24, consulte o Support24, onde você pode encontrar respostas para quase todas as perguntas que novos usuários fazem, bem como aulas em vídeo e cursos de treinamento.";
$MESS["BLOG_DEMO_MESSAGE_TITLE1"] = "Participe!";
$MESS["BLOG_DEMO_MESSAGE_BODY1"] = "O Bitrix24 é uma plataforma gratuita de comunicação e colaboração. Com o Bitrix24, documentos, calendários, bate-papos em grupo, tarefas, projetos, CRM, videoconferência, telefonia, intranet e outras ferramentas estão sempre à sua disposição no escritório, em casa e em trânsito.

Comece convidando colegas, parceiros de negócio e clientes para a sua conta Bitrix24. Comece a colaborar enviando um mensagem a todos os usuários que aderiram à sua conta.

[IMG WIDTH=1004 HEIGHT=565]https://www.bitrix24.com/images/b24_en/post_n.png[/IMG]

Instale o app móvel gratuito para iOS ou Android para permanecer em contato quando você não tiver acesso ao seu computador. Confira nossos apps desktop para Mac, PC ou Linux para saber como eles ajudam você a trabalhar com documentos e a sincronizar seus arquivos entre os dispositivos. 

[IMG WIDTH=300 HEIGHT=201]https://www.bitrix24.com/images/b24_en/mob_tr.png[/IMG][IMG WIDTH=340 HEIGHT=180]https://www.bitrix24.com/images/b24_en/desk_tr.png[/IMG]

[IMG WIDTH=60 HEIGHT=1]https://www.bitrix24.com/images/b24_en/i.png[/IMG][URL=https://itunes.apple.com/en/app/bitrix24/id561683423?l=en&ls=1&mt=8][IMG WIDTH=91 HEIGHT=28]https://www.bitrix24.com/images/b24_en/as001.png[/IMG][/URL] [URL=https://play.google.com/store/apps/details?id=com.bitrix24.android&hl=en][IMG WIDTH=98 HEIGHT=25]https://www.bitrix24.com/images/b24_en/gp005.png[/IMG][/URL][IMG WIDTH=120 HEIGHT=1]https://www.bitrix24.com/images/b24_en/i.png[/IMG][URL=http://dl.bitrix24.com/b24/bitrix24_desktop.dmg][IMG WIDTH=74 HEIGHT=31]https://www.bitrix24.com/images/b24_en/mac001.png[/IMG][/URL] [URL=http://dl.bitrix24.com/b24/bitrix24_desktop.exe][IMG WIDTH=88 HEIGHT=26]https://www.bitrix24.com/images/b24_en/win002.png[/IMG][/URL] [URL=https://github.com/buglloc/brick][IMG WIDTH=67 HEIGHT=28]https://www.bitrix24.com/images/b24_en/linux003.png[/IMG][/URL]";
$MESS["BLOG_DEMO_MESSAGE_TITLE2"] = "Fácil gerenciamento de tarefa com o Bitrix24!";
$MESS["BLOG_DEMO_MESSAGE_BODY2"] = "Cansado de confusão e prazos perdidos? O Bitrix24 permite que você organize suas tarefas e garante que tudo seja feito a tempo.
[IMG WIDTH=1367 HEIGHT=713]https://www.bitrix24.com/images/b24_en/tasks_how.png[/IMG]   

[B]Por onde eu começo?[/B]

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.com/images/b24_en/1p.png[/IMG] Personalize o formulário de criação de tarefa para atender às suas necessidades. 

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.com/images/b24_en/2p.png[/IMG] Crie tarefas recorrentes e modelos de tarefa para gerenciar a rotina com mais eficiência. 

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.com/images/b24_en/3p.png[/IMG] Use diagramas de Gantt, listas de verificações e dependências de tarefa para coordenar projetos complexos.

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.com/images/b24_en/4p.png[/IMG] Defina lembretes para nunca perder um prazo.

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.com/images/b24_en/5p.png[/IMG] Saiba como criar tarefas a partir de e-mails, comentários e postagens.";
$MESS["BLOG_DEMO_MESSAGE_TITLE3"] = "Primeiros passos em CRM";
$MESS["BLOG_DEMO_MESSAGE_BODY3"] = "[URL=/crm/stream/]O Bitrix24.CRM[/URL] armazena todas as informações sobre seus clientes e potenciais clientes: informações de contato, informações da empresa, negociações, cotações, faturas, pedidos, reuniões, e-mails, ligações e mais.
 
[IMG WIDTH=1115 HEIGHT=573]https://www.bitrix24.com/images/b24_en/b24crm.png[/IMG]

[B]
[/B][B]Deixe o nosso CRM fazer o trabalho para você![/B]

Você pode conectar o Bitrix24.CRM ao: 

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.com/images/b24_en/1p.png[/IMG] Seu [URL=/company/personal/mail/]endereço de e-mail[/URL] para sincronizar todos os e-mails entre o CRM e sua conta de e-mail.
[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.com/images/b24_en/2p.png[/IMG] Seu [URL=/telephony/lines.php]sistema de telefone[/URL] para fazer, receber e gravar ligações.
[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.com/images/b24_en/3p.png[/IMG] Suas [URL=/settings/openlines/]contas em mídias sociais[/URL] para se comunicar com seus seguidores.
[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.com/images/b24_en/4p.png[/IMG] Seu site para falar com visitantes via [URL=/settings/openlines/]chat ao vivo[/URL]. 
[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.com/images/b24_en/5p.png[/IMG] Seus [URL=/crm/webform/]formulários do site[/URL] para armazenar os dados coletados no CRM.

Não importa como você escolhe se comunicar com seus clientes, por e-mail, telefone, mídias sociais ou seu site, esta informação acabará no seu CRM, facilmente acessível e pesquisável. Naturalmente, você pode adicionar novos dados manualmente, migrar de outro CRM ou importar dados do cliente de um arquivo CSV.
[B]
[/B]Campos personalizados, limitações de diretos de acesso, canais de vendas e vários pipelines também estão disponíveis. 

[URL=/crm/stream/][IMG WIDTH=133 HEIGHT=32]https://www.bitrix24.com/images/b24_en/go_to_crm.png[/IMG][/URL] 



[B]Precisa saber mais sobre o Bitrix24?[/B] 

[IMG WIDTH=160 HEIGHT=134]https://www.bitrix24.com/images/b24_en/cloudman24.png[/IMG]
Procure este ícone no canto superior direito [IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.com/images/b24_en/help24.png[/IMG]. Ao clicar no ponto de interrogação, ele levará você ao Support24. ";
?>