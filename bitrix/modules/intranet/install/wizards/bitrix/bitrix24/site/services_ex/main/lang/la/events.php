<?
$MESS["EXTRANET_NEW_MESSAGE_SUBJECT"] = "# SITE_NAME #: Tienes un mensaje nuevo";
$MESS["EXTRANET_NEW_MESSAGE_MESSAGE"] = "Saludos desde # SITE_NAME #!
------------------------------------------

Hola # USER_NAME #!

Tiene un mensaje nuevo de # SENDER_NAME # # SENDER_LAST_NAME #:

------------------------------------------
#MENSAJE#
------------------------------------------

Enlace al mensaje:

Http: // # SERVER_NAME # / empresa / personal / mensajes / chat / # SENDER_ID # /

Esta es una notificación generada automáticamente.";
?>