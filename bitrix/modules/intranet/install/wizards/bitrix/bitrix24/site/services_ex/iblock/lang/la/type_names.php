<?
$MESS["NEWS_TYPE_NAME"] = "Noticias";
$MESS["NEWS_ELEMENT_NAME"] = "Nuevos artículos";
$MESS["STRUCTURE_TYPE_NAME"] = "Estructura de la compañía";
$MESS["STRUCTURE_ELEMENT_NAME"] = "Elementos";
$MESS["STRUCTURE_SECTION_NAME"] = "Secciones";
$MESS["SERVICES_TYPE_NAME"] = "Servicios";
$MESS["SERVICES_ELEMENT_NAME"] = "Elementos";
$MESS["SERVICES_SECTION_NAME"] = "Secciones";
$MESS["EVENTS_TYPE_NAME"] = "Calendario";
$MESS["EVENTS_ELEMENT_NAME"] = "Eventos";
$MESS["EVENTS_SECTION_NAME"] = "Calendarios";
$MESS["LIBRARY_TYPE_NAME"] = "Documentos";
$MESS["LIBRARY_ELEMENT_NAME"] = "Archivos";
$MESS["LIBRARY_SECTION_NAME"] = "Folders";
$MESS["PHOTOS_TYPE_NAME"] = "Fotogalería";
$MESS["PHOTOS_ELEMENT_NAME"] = "Fotos";
$MESS["PHOTOS_SECTION_NAME"] = "Albums";
?>