<?
$MESS["W_IB_USER_PHOTOG_TAB1"] = "edit1--#--Photo--,--ACTIVE--#--  Actif--,--NAME--#--*Intitulé--,--IBLOCK_ELEMENT_PROP_VALUE--#----Valeurs de propriété--,--PREVIEW_PICTURE--#--  Aperçu--,--DETAIL_PICTURE--#--  Image détaillée--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB2"] = "--#--  Original--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB3"] = "--#--  Évaluation--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB4"] = "--#--  Votes--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB5"] = "--#--  Total des votes--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB6"] = "--#--  L'article a été approuvé--,--PROPERTY_";
$MESS["W_IB_USER_PHOTOG_TAB7"] = "--#--  Article publié--;--edit6--#--Description--,--PREVIEW_TEXT--#--  Aperçu du texte--,--DETAIL_TEXT--#--  Description détaillée--;--edit2--#--Sections--,--SECTIONS--#--  Sections--;--edit3--#--Infos supplémentaires--,--SORT--#--  Index de tri--,--CODE--#--  Code symbolique--,--TAGS--#--  Balises--;--";
$MESS["SONET_PHOTO_LOG_1"] = "#AUTHOR_NAME# a ajouté une photo";
$MESS["SONET_PHOTOPHOTO_LOG_1"] = "#AUTHOR_NAME# a ajouté une nouvelle photo : \"#TITLE#\".";
$MESS["SONET_PHOTO_LOG_2"] = "photos (#COUNT#)";
$MESS["SONET_PHOTO_LOG_TEXT"] = "Nouvelles photos : <div class='notificationlog'>#LINKS#</div> <a href=\"#HREF#\">Afficher l'album</a>.";
$MESS["SONET_PHOTO_LOG_MAIL_TEXT"] = "Nouvelles photos à téléverser sur \"#TITLE#\".";
$MESS["SONET_LOG_GUEST"] = "Invité";
?>