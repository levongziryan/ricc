<?
$MESS["iblock_dep_dep"] = "Departamento";
$MESS["iblock_dep_created"] = "Criado";
$MESS["iblock_dep_changed"] = "Modificado";
$MESS["iblock_dep_name"] = "Nome do departamento";
$MESS["iblock_dep_parent"] = "Top Nível de departamento";
$MESS["iblock_dep_chief"] = "Cabeça";
$MESS["iblock_dep_pict"] = "Imagem";
$MESS["iblock_dep_desc"] = "Descrição";
$MESS["iblock_dep_addit"] = "Adicional";
$MESS["iblock_dep_act"] = "Departamento está ativo";
$MESS["iblock_dep_sort"] = "Classificar por";
$MESS["iblock_dep_code"] = "Código simbólico";
$MESS["iblock_dep_det_pict"] = "Imagem Detalhada";
$MESS["iblock_dep_userprop"] = "Propriedade de usuário";
$MESS["iblock_dep_userprop_add"] = "Adicionar uma propriedade de usuário";
$MESS["iblock_dep_name1"] = "Minha Empresa";
$MESS["iblock_dep_name2"] = "Departamento de contas";
$MESS["iblock_dep_name3"] = "Departamento de Vendas";
$MESS["iblock_dep_name4"] = "Departamento de TI";
$MESS["iblock_dep_name5"] = "Departamento de marketing";
?>