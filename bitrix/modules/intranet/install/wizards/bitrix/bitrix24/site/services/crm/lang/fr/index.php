<?
$MESS["CRM_TOP_LINKS_ITEM_NAME"] = "GRC";
$MESS["CRM_GADGET_MY_LEAD_TITLE"] = "Mes pistes";
$MESS["CRM_GADGET_NEW_LEAD_TITLE"] = "Nouvelles pistes";
$MESS["CRM_GADGET_CLOSED_DEAL_TITLE"] = "Transactions réussies";
$MESS["CRM_GADGET_LAST_EVENT_TITLE"] = "Événements récents";
$MESS["CRM_GADGET_NEW_CONTACT_TITLE"] = "Nouveaux contacts";
$MESS["CRM_GADGET_NEW_COMPANY_TITLE"] = "Nouvelles sociétés";
$MESS["CRM_ROLE_ADMIN"] = "Administrateur";
$MESS["CRM_ROLE_DIRECTOR"] = "Directeur";
$MESS["CRM_ROLE_CHIF"] = "Chef de service";
$MESS["CRM_ROLE_MAN"] = "Responsable";
?>