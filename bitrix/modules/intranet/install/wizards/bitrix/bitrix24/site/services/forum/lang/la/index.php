<?
$MESS["GENERAL_GROUP_NAME"] = "Foros generales";
$MESS["COMMENTS_GROUP_NAME"] = "Foro para comentarios";
$MESS["HIDDEN_GROUP_NAME"] = "Foros ocultos";
$MESS["GENERAL_FORUM_NAME"] = "Foro general";
$MESS["GENERAL_FORUM_DESCRIPTION"] = "Foro público para los empleados de la empresa. Discuta su negocio e intercambie opiniones aquí.";
$MESS["GENERAL_FORUM_TOPIC_TITLE"] = "Noticias del Portal";
$MESS["GENERAL_FORUM_MESSAGE_BODY"] = "¡Atención! Los empleados de la empresa ahora pueden mantener su archivo de almacenamiento en su área privada.

Encontrará la información detallada sobre la gestión de almacenamiento de archivos y el método de asignar el almacenamiento a una unidad de red en la sección de ayuda: \"Mi perfil - Archivos\".

Si tiene alguna pregunta sobre la configuración del almacenamiento de archivos, envíe sus solicitudes a los ingenieros de techsupport utilizando el formulario de solicitud de asistencia.";
$MESS["PHOTOGALLERY_COMMENTS_FORUM_NAME"] = "Discusión de la galería de fotos";
$MESS["USERS_AND_GROUPS_FORUM_NAME"] = "Usuarios y grupos";
$MESS["USERS_AND_GROUPS_FORUM_DESCRIPTION"] = "Foros personales y grupales";
$MESS["DOCS_SHARED_COMMENTS_NAME"] = "Comentarios sobre la biblioteca de archivos comunes";
$MESS["GROUPS_AND_USERS_COMMENTS_NAME"] = "Comentarios de los archivos";
?>