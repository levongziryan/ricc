<?
$MESS["ADMIN_SECTION_GROUP_DESC"] = "Les membres de ce groupe peuvent accéder au Panneau de configuration.";
$MESS["ADMIN_SECTION_GROUP_NAME"] = "Utilisateurs du Panneau de configuration";
$MESS["CREATE_GROUPS_GROUP_DESC"] = "Les membres de ce groupe peuvent créer des groupes de travail.";
$MESS["CREATE_GROUPS_GROUP_NAME"] = "Administrateur de groupe de travail";
$MESS["DIRECTION_GROUP_DESC"] = "Conseil d'administration de l'entreprise.";
$MESS["DIRECTION_GROUP_NAME"] = "Conseil d'administration";
$MESS["EMPLOYEES_GROUP_DESC"] = "Tous les employés de l'entreprise inscrits sur le portail.";
$MESS["EMPLOYEES_GROUP_NAME"] = "Employés";
$MESS["MARKETING_AND_SALES_GROUP_DESC"] = "Personnel du service ventes et marketing.";
$MESS["MARKETING_AND_SALES_GROUP_NAME"] = "Ventes et marketing";
$MESS["PERSONNEL_DEPARTMENT_GROUP_DESC"] = "Personnel du service Ressources humaines.";
$MESS["PERSONNEL_DEPARTMENT_GROUP_NAME"] = "Service Ressources humaines";
$MESS["PORTAL_ADMINISTRATION_GROUP_DESC"] = "Les administrateurs du portail ont accès à tous les services du portail.";
$MESS["PORTAL_ADMINISTRATION_GROUP_NAME"] = "Administrateurs du portail";
$MESS["SUPPORT_GROUP_DESC"] = "Spécialiste de l'assistance technique.";
$MESS["SUPPORT_GROUP_NAME"] = "Assistance technique";
?>