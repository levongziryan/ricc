<?
$MESS["PHOTOS_SECTION_NAME"] = "Albums";
$MESS["EVENTS_TYPE_NAME"] = "Calendrier";
$MESS["EVENTS_SECTION_NAME"] = "Calendriers";
$MESS["STRUCTURE_TYPE_NAME"] = "Structure de l’entreprise";
$MESS["LIBRARY_TYPE_NAME"] = "Documents";
$MESS["STRUCTURE_ELEMENT_NAME"] = "Éléments";
$MESS["SERVICES_ELEMENT_NAME"] = "Éléments";
$MESS["EVENTS_ELEMENT_NAME"] = "Évènements";
$MESS["LIBRARY_ELEMENT_NAME"] = "Fichiers";
$MESS["LIBRARY_SECTION_NAME"] = "Dossiers";
$MESS["NEWS_TYPE_NAME"] = "Actualités";
$MESS["NEWS_ELEMENT_NAME"] = "Actualités";
$MESS["PHOTOS_TYPE_NAME"] = "Galerie photo";
$MESS["PHOTOS_ELEMENT_NAME"] = "Photos";
$MESS["STRUCTURE_SECTION_NAME"] = "Sections";
$MESS["SERVICES_SECTION_NAME"] = "Sections";
$MESS["SERVICES_TYPE_NAME"] = "Services";
?>