<?
$MESS["EXTRANET_ADMIN_GROUP_DESC"] = "Адміністратори сайту екстранету — користувачі з повними можливостями з управління сервісами екстранету.";
$MESS["EXTRANET_GROUP_DESC"] = "Всі користувачі, які мають доступ на сайт екстранету.";
$MESS["EXTRANET_CREATE_WG_GROUP_DESC"] = "Всі користувачі, які мають право на створення робочих груп на сайті екстранету.";
$MESS["EXTRANET_CREATE_WG_GROUP_NAME"] = "Можуть створювати робочі групи в екстранету";
$MESS["EXTRANET_MENUITEM_NAME"] = "Екстранет";
$MESS["EXTRANET_ADMIN_GROUP_NAME"] = "Адміністратори сайту екстранету";
$MESS["EXTRANET_SUPPORT_GROUP_NAME"] = "Ремонт сайту екстранету";
$MESS["EXTRANET_GROUP_NAME"] = "Користувачі екстранету";
$MESS["EXTRANET_SUPPORT_GROUP_DESC"] = "Співробітники техпідтримки сайту екстранету.";
?>