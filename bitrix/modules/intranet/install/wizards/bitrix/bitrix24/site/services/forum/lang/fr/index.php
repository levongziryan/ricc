<?
$MESS["COMMENTS_GROUP_NAME"] = "Forum pour commentaires";
$MESS["DOCS_SHARED_COMMENTS_NAME"] = "Commentaires sur la bibliothèque des fichiers communs";
$MESS["GENERAL_FORUM_DESCRIPTION"] = "Forum public pour les employés de la société. Discutez de vos affaires et échangez vos avis.";
$MESS["GENERAL_FORUM_MESSAGE_BODY"] = "Attention ! Les employés d'une société peuvent maintenant gérer leur stockage de fichiers dans leur espace privé.

Vous trouverez dans la section assistance les informations détaillées sur la gestion du stockage de fichiers et la méthode de traçage d'un stockage vers un lecteur de réseau : \"Mon profil - Fichiers\".

Si vous avez des questions sur la configuration du stockage de fichiers, vous pouvez envoyer une demande aux ingénieurs de l'assistance technique en utilisant le formulaire de demande d'assistance.";
$MESS["GENERAL_FORUM_NAME"] = "Forum général";
$MESS["GENERAL_FORUM_TOPIC_TITLE"] = "Actualités du portail";
$MESS["GENERAL_GROUP_NAME"] = "Forums généraux";
$MESS["GROUPS_AND_USERS_COMMENTS_NAME"] = "Commentaires sur les fichiers";
$MESS["HIDDEN_GROUP_NAME"] = "Forums cachés";
$MESS["PHOTOGALLERY_COMMENTS_FORUM_NAME"] = "Discussion sur la galerie photos";
$MESS["USERS_AND_GROUPS_FORUM_DESCRIPTION"] = "Forums personnels et des groupes";
$MESS["USERS_AND_GROUPS_FORUM_NAME"] = "Utilisateurs et groupes ";
?>