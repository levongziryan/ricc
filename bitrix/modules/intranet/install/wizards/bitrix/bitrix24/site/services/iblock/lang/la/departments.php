<?
$MESS["iblock_dep_dep"] = "Departamento";
$MESS["iblock_dep_created"] = "Creado";
$MESS["iblock_dep_changed"] = "Modificado";
$MESS["iblock_dep_name"] = "Nombre del departamento";
$MESS["iblock_dep_parent"] = "Departamento de nivel superior";
$MESS["iblock_dep_chief"] = "Cabeza";
$MESS["iblock_dep_pict"] = "Imagen";
$MESS["iblock_dep_desc"] = "Descripción";
$MESS["iblock_dep_addit"] = "Adicional";
$MESS["iblock_dep_act"] = "El departamento está activo";
$MESS["iblock_dep_sort"] = "Orden de clasificación";
$MESS["iblock_dep_code"] = "Código simbólico";
$MESS["iblock_dep_det_pict"] = "Detalle de la imagen";
$MESS["iblock_dep_userprop"] = "Propiedad del usuario";
$MESS["iblock_dep_userprop_add"] = "Agregar una propiedad de usuario";
$MESS["iblock_dep_name1"] = "Bitrix";
$MESS["iblock_dep_name2"] = "Departamento de cuentas";
$MESS["iblock_dep_name3"] = "Departamento de ventas";
$MESS["iblock_dep_name4"] = "Departamento de TI";
$MESS["iblock_dep_name5"] = "Departamento de marketing";
?>