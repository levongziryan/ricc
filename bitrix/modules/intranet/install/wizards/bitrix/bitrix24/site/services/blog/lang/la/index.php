<?
$MESS["BLOG_DEMO_GROUP_SOCNET"] = "Grupo de redes sociales";
$MESS["BLG_NAME"] = "Blog de";
$MESS["BLOG_DEMO_CATEGORY_1"] = "Consejo";
$MESS["BLOG_DEMO_CATEGORY_2"] = "Tips";
$MESS["BLOG_DEMO_MESSAGE_TITLE"] = "Bitrix24: ¡Estamos empezando!";
$MESS["BLOG_DEMO_MESSAGE_BODY"] = "Hoy es el día glorioso que hemos creado nuestro propio Bitrix24, nuestro espacio de trabajo compartido. Esta es la arena para comunicarse, trabajar, desarrollar proyectos, abordar problemas y hacer otras cosas de una manera nueva y más productiva. No importa donde usted está - en la oficina, o viajando por negocios o trabajando desde casa - podemos llegar a usted y usted puede comunicarse con nosotros en cualquier momento tenemos trabajo que hacer, o una cosa para discutir.

Bitrix24 es una forma natural de establecer discusiones de grupo en el Activity Stream, comunicar conversaciones de persona a persona o en grupo, realizar videollamadas, crear y controlar tareas, planificar su día de trabajo y mucho más.

[B]Entonces, ¿por dónde empezamos?[/B]

1. Comencemos con algo simple. Como este post y ver quién también le gusta.

[IMG WIDTH=600 HEIGHT=300]/images/demo_post/en/1.png[/IMG]

2. Agregue su comentario, o envíe un mensaje a todas las personas o solo a personas seleccionadas.

[IMG WIDTH=600 HEIGHT=310]/images/demo_post/en/2.png[/IMG]

3. Instale una aplicación móvil Bitrix24 gratuita en su teléfono iOS o Android y una aplicación de escritorio en su computadora (Windows, MacOS o Linux):

[URL=https://itunes.apple.com/app/bitrix24/id561683423][IMG]/images/demo_post/appstore.png[/IMG][/URL][URL=https://play.google.com/store/apps/details?id=com.bitrix24.android][IMG]/images/demo_post/googleplay.png[/IMG][/URL][URL=http://dl.bitrix24.com/b24/bitrix24_desktop.exe][IMG]/images/demo_post/windows.png[/IMG][/URL][URL=http://dl.bitrix24.com/b24/bitrix24_desktop.dmg][IMG]/images/demo_post/mac.png[/IMG][/URL][URL=https://github.com/buglloc/brick][IMG]/images/demo_post/linux.png[/IMG][/URL]

Siempre que tenga dudas sobre el uso de nuestro Bitrix24, consulte Support24 donde puede encontrar respuestas a casi todas las preguntas que hacen los nuevos usuarios, así como lecciones en video y cursos de formación.";
$MESS["BLOG_DEMO_COMMENT_TITLE"] = "¡Interesante!";
$MESS["BLOG_DEMO_COMMENT_BODY"] = "¡Lo intentaré!";
$MESS["BPC_SONET_POST_TITLE"] = "Una entrada en el blog creada \"#TITLE#\"";
$MESS["BPC_SONET_COMMENT_TITLE"] = "Añadió un comentario en la entrada del blog \"#TITLE#\"";
$MESS["BLOG_DEMO_MESSAGE_TITLE1"] = "¡Colaboración a Distancia!";
$MESS["BLOG_DEMO_MESSAGE_BODY1"] = "Bitrix24 es una plataforma de comunicación y colaboración gratuita. Con los documentos Bitrix24, calendarios, chats de grupo, tareas, proyectos, CRM, videoconferencia, telefonía, intranet social y otras herramientas están siempre a su alcance en la oficina, en casa y en cualquier lugar.

Comience invitando a colegas, socios comerciales y clientes a su cuenta Bitrix24. Inicie la colaboración enviando un mensaje a todos los usuarios que se unieron a su cuenta.

[IMG WIDTH=1004 HEIGHT=565]https://www.bitrix24.com/images/b24_en/post_n.png[/IMG]

Instala la aplicación móvil gratuita para iOS o Android para que permanezcas en contacto cuando no tengas acceso a tu computadora. Echa un vistazo a nuestras aplicaciones de escritorio para Mac, PC o Linux para aprender cómo le ayuda a trabajar con documentos y sincronizar sus archivos a través de dispositivos.

[IMG WIDTH=300 HEIGHT=201]https://www.bitrix24.com/images/b24_en/mob_tr.png[/IMG][IMG WIDTH=340 HEIGHT=180]https://www.bitrix24.com/images/b24_en/desk_tr.png[/IMG]

[IMG WIDTH=60 HEIGHT=1]https://www.bitrix24.com/images/b24_en/i.png[/IMG][URL=https://itunes.apple.com/es/app/bitrix24/id561683423?L=es&ls=1&mt=8][IMG WIDTH=91 HEIGHT=28]https://www.bitrix24.com/images/b24_en/as001.png[/IMG][/URL][URL=https://play.google.com/store/apps/details?id=com.bitrix24.android&hl=es][IMG WIDTH=98 HEIGHT=25]https://www.bitrix24.com/images/b24_en/gp005.png[/IMG][/URL][IMG WIDTH=120 HEIGHT=1]https://www.bitrix24.com/images/b24_en/i.png[/IMG][URL=http://dl.bitrix24.com/b24/bitrix24_desktop.dmg][IMG WIDTH=74 HEIGHT=31] https://www.bitrix24.com/images/b24_en/mac001.png[/IMG][/URL][URL=http://dl.bitrix24./B24/bitrix24_desktop.exe][IMG WIDTH=88 HEIGHT=26] https://www.bitrix24.com/images/b24_en/win002.png[/IMG][/URL][URL=https://github.com/buglloc/brick][IMG WIDTH=67 HEIGHT=28] https://www.bitrix24.com/images/b24_en/linux003.png[/IMG][/URL]";
$MESS["BLOG_DEMO_MESSAGE_TITLE2"] = "Fácil gestión de tareas con Bitrix24";
$MESS["BLOG_DEMO_MESSAGE_BODY2"] = "";
$MESS["BLOG_DEMO_MESSAGE_TITLE3"] = "Primeros pasos en CRM";
$MESS["BLOG_DEMO_MESSAGE_BODY3"] = "[URL=/crm/stream/]Bitrix24.CRM[/URL] almacena toda la información sobre sus clientes y prospectos - información de contacto, detalles de la empresa, ofertas, presupuestos, peticiones, reuniones, correos electrónicos, llamadas y mucho más.
 
[IMG WIDTH=1115 HEIGHT=573]https://www.bitrix24.es/images/b24_en/b24crm.png[/IMG]

[B]
[/B][B]¡Deje que nuestro CRM haga el trabajo por usted![/B]

Puede conectar Bitrix24.CRM a:

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.es/images/b24_en/1p.png[/IMG]Para sincronizar todos los [URL=/company/personal/mail/]correos electrónicos[/URL] entre CRM y su cuenta de correo electrónico.
[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.es/images/b24_en/2p.png[/IMG]Su [URL=/telefonia/lines.php]Telefonía[/URL] para hacer, recibir y grabar llamadas telefónicas.
[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.es/images/b24_en/3p.png[/IMG]Su [URL=/settings/openlines/]cuentas de redes sociales[/URL] para comunicarse con sus suscriptores.
[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.es/images/b24_en/4p.png[/IMG]Su sitio web para hablar con los visitantes a través de [URL=/settings/openlines/] chat en vivo[/URL].
[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.es/images/b24_en/5p.png[/IMG]Su [URL=/crm/webform/]formularios web[/URL] para almacenar datos recogidos dentro de CRM.

No importa cómo usted elige comunicarse con sus clientes - vía email, teléfono, medios sociales o su Web site - esta información terminará para arriba en su CRM, fácilmente accesible y searchable. Naturalmente, puede agregar nuevos datos manualmente, migrar desde otro CRM o importar datos de cliente desde un archivo CSV.
[B]
[/B]Los campos personalizados, las limitaciones de los derechos de acceso, los embudos de ventas y las múltiples tuberías también están disponibles para usted.

[URL=/crm/stream/][IMG WIDTH=133 HEIGHT=32]https://www.bitrix24.es/images/b24_en/go_to_crm.png[/IMG][/URL]



[B]¿Necesita aprender más sobre Bitrix24?[/B]

[IMG WIDTH=160 HEIGHT=134] https://www.bitrix24.es/images/b24_en/cloudman24.png[/IMG]
Mire en la esquina superior derecha de este icono [IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.com/images/b24_en/help24.png[/IMG]. Al hacer clic en el signo de interrogación, lo llevará a la sección Support24.";
?>