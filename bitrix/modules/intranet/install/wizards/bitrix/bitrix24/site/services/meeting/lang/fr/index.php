<?
$MESS["MEETING_TITLE"] = "Déploiement du portail Intranet";
$MESS["MEETING_DESCRIPTION"] = "Nous devrons organiser une réunion pour discuter du déploiement du portail intranet de notre entreprise.";
$MESS["MEETING_PLACE"] = "Bureau du PDG";
$MESS["MEETING_ITEM_TITLE_1"] = "Analyse des solutions disponibles";
$MESS["MEETING_ITEM_TITLE_2"] = "Définir des étapes de mise en oeuvre, nommer des responsables";
$MESS["MEETING_ITEM_TITLE_3"] = "Projets pour les fêtes de fin d'année";
?>