<?
$MESS["EMPLOYEES_GROUP_NAME"] = "Empleados";
$MESS["EMPLOYEES_GROUP_DESC"] = "Todos los empleados de la empresa, registrados en el portal.";
$MESS["PERSONNEL_DEPARTMENT_GROUP_NAME"] = "Departamento de recursos humanos";
$MESS["PERSONNEL_DEPARTMENT_GROUP_DESC"] = "Personal del departamento de recursos humanos.";
$MESS["DIRECTION_GROUP_NAME"] = "Comité administrativo";
$MESS["DIRECTION_GROUP_DESC"] = "Junta Directiva de la Compañía.";
$MESS["PORTAL_ADMINISTRATION_GROUP_NAME"] = "Administradores del portal";
$MESS["PORTAL_ADMINISTRATION_GROUP_DESC"] = "Los administradores del portal tienen acceso a todos los servicios del portal.";
$MESS["ADMIN_SECTION_GROUP_NAME"] = "Usuarios del panel de control";
$MESS["ADMIN_SECTION_GROUP_DESC"] = "Los miembros de este grupo pueden acceder al Panel de control.";
$MESS["CREATE_GROUPS_GROUP_NAME"] = "Administradores de grupo de trabajo";
$MESS["CREATE_GROUPS_GROUP_DESC"] = "Los miembros de este grupo pueden crear nuevos grupos de trabajo.";
$MESS["MARKETING_AND_SALES_GROUP_NAME"] = "Ventas y marketing";
$MESS["MARKETING_AND_SALES_GROUP_DESC"] = "Personal de ventas y marketing.";
$MESS["SUPPORT_GROUP_NAME"] = "Helpdesk";
$MESS["SUPPORT_GROUP_DESC"] = "Helpdesk especialista";
?>