<?
$MESS["COMMENTS_EXTRANET_GROUP_NAME"] = "Commentaires : Extranet";
$MESS["HIDDEN_EXTRANET_GROUP_NAME"] = "Forums cachés : extranet";
$MESS["USERS_AND_GROUPS_EXTRANET_FORUM_NAME"] = "Forums des utilisateurs et des groupes : extranet";
$MESS["USERS_AND_GROUPS_EXTRANET_FORUM_DESCRIPTION"] = "Forums des utilisateurs individuels et des groupes du site extranet.";
$MESS["GROUPS_AND_USERS_FILES_COMMENTS_EXTRANET_NAME"] = "Commentaires sur les fichiers d'utilisateurs : extranet";
$MESS["GROUPS_AND_USERS_TASKS_COMMENTS_EXTRANET_NAME"] = "Commentaires sur les tâches d'utilisateurs et de groupes : extranet";
$MESS["GROUPS_AND_USERS_PHOTOGALLERY_COMMENTS_EXTRANET_NAME"] = "Commentaires sur les galeries photos des utilisateurs et des groupes : extranet";
?>