<?
$MESS["PHOTOS_SECTION_NAME"] = "lbuns";
$MESS["EVENTS_TYPE_NAME"] = "Calendário";
$MESS["EVENTS_SECTION_NAME"] = "Calendários";
$MESS["STRUCTURE_TYPE_NAME"] = "Estrutura da Empresa";
$MESS["LIBRARY_TYPE_NAME"] = "Documentos";
$MESS["STRUCTURE_ELEMENT_NAME"] = "Elementos";
$MESS["SERVICES_ELEMENT_NAME"] = "Elementos";
$MESS["EVENTS_ELEMENT_NAME"] = "Eventos";
$MESS["LIBRARY_ELEMENT_NAME"] = "Arquivos";
$MESS["LIBRARY_SECTION_NAME"] = "Pastas";
$MESS["NEWS_TYPE_NAME"] = "Notícia";
$MESS["NEWS_ELEMENT_NAME"] = "Novos Itens";
$MESS["PHOTOS_TYPE_NAME"] = "Galeria de Fotos";
$MESS["PHOTOS_ELEMENT_NAME"] = "Fotos";
$MESS["STRUCTURE_SECTION_NAME"] = "Seções";
$MESS["SERVICES_SECTION_NAME"] = "Seções";
$MESS["SERVICES_TYPE_NAME"] = "Serviços";
?>