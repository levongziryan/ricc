<?
$MESS["EXTRANET_GROUP_NAME"] = "Usuarios de extranet";
$MESS["EXTRANET_GROUP_DESC"] = "Todos los usuarios que tienen acceso a la extranet del sitio.";
$MESS["EXTRANET_CREATE_WG_GROUP_NAME"] = "Se permite crear grupos de usuarios de extranet";
$MESS["EXTRANET_CREATE_WG_GROUP_DESC"] = "Todos los usuarios que tienen permiso para crear grupos de usuarios en el sitio de extranet.";
$MESS["EXTRANET_ADMIN_GROUP_NAME"] = "Administradores de sitios de Extranet";
$MESS["EXTRANET_ADMIN_GROUP_DESC"] = "Los administradores tienen permiso completo para acceder, administrar y editar recursos del sitio de extranet.";
$MESS["EXTRANET_SUPPORT_GROUP_NAME"] = "Soporte de Extranet";
$MESS["EXTRANET_SUPPORT_GROUP_DESC"] = "Persons in charge of providing assistance on the extranet site.";
$MESS["EXTRANET_MENUITEM_NAME"] = "Extranet";
?>