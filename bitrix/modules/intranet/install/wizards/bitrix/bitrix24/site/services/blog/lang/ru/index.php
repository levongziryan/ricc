<?
$MESS["BLOG_DEMO_GROUP_SOCNET"] = "Группа для социальной сети";
$MESS["BLG_NAME"] = "Блог пользователя";
$MESS["BLOG_DEMO_CATEGORY_1"] = "Советы";
$MESS["BLOG_DEMO_CATEGORY_2"] = "Подсказки";
$MESS["BLOG_DEMO_MESSAGE_TITLE"] = "Начинаем работу в нашем «Битрикс24»! ";
$MESS["BLOG_DEMO_MESSAGE_BODY"] = "Сегодня мы создали свой «Битрикс24»! Это наше с вами общее рабочее пространство. Здесь мы сможем общаться, работать вместе над проектами и многое другое. И не важно, в офисе вы, в командировке или работаете удаленно — теперь мы всегда на связи и все вопросы моментально обсудим и решим в Битрикс24.

В Битрикс24 можно обсуждать вопросы с коллегами в «живой ленте», переписываться один на один, общаться в групповых чатах, делать видеозвонки коллегам, ставить задачи, планировать свой день в календарях и т.д.

[B]С чего начать?[/B]

1. Начнем с тренировки  Поставьте \"лайк\" этому сообщению. Посмотрите, кому из ваших коллег это уже нравится.

[IMG WIDTH=600 HEIGHT=350]/images/demo_post/ru/1.png[/IMG]

2. Добавьте свой комментарий или напишите свое сообщение на всех сотрудников или только на некоторых.

[IMG WIDTH=400 HEIGHT=200]/images/demo_post/ru/2.png[/IMG]

3. Установите себе бесплатное мобильное приложение «Битрикс24» на смартфон iOS или Android и десктоп-приложение на ваш компьютер (Windows, MacOS или Linux):

[URL=https://itunes.apple.com/ru/app/bitrix24/id561683423?l=ru&ls=1&mt=8][IMG]/images/demo_post/appstore.png[/IMG][/URL] [URL=https://play.google.com/store/apps/details?id=com.bitrix24.android][IMG]/images/demo_post/googleplay.png[/IMG][/URL]  [URL=http://dl.bitrix24.com/b24/bitrix24_desktop.exe][IMG]/images/demo_post/windows.png[/IMG][/URL] [URL=http://dl.bitrix24.com/b24/bitrix24_desktop.dmg][IMG]/images/demo_post/mac.png[/IMG][/URL] [URL=https://github.com/buglloc/brick][IMG]/images/demo_post/linux.png[/IMG][/URL]

Если у вас появятся вопросы по работе с Битрикс24, откройте Поддержку24 — там есть ответы практически на 95% вопросов, а еще видеоуроки и учебные курсы.";
$MESS["BLOG_DEMO_COMMENT_TITLE"] = "Интересно";
$MESS["BLOG_DEMO_COMMENT_BODY"] = "Надо будет обязательно попробовать!";
$MESS["BPC_SONET_POST_TITLE"] = "добавил(а) сообщение \"#TITLE#\" в блог";
$MESS["BPC_SONET_COMMENT_TITLE"] = "добавил(а) комментарий к сообщению \"#TITLE#\" в блоге";
$MESS["BLOG_DEMO_MESSAGE_TITLE1"] = "Начинаем работать в Битрикс24!";
$MESS["BLOG_DEMO_MESSAGE_BODY1"] = "Здесь мы будем общаться в «живой ленте» и чатах, вести проекты, ставить задачи, планировать день, работать с клиентами и многое другое. В офисе, в командировке, удаленно — с Битрикс24 мы всегда на связи и все вопросы моментально обсудим и решим. 

С чего начать? Поставьте «лайк» этому сообщению, прокомментируйте, напишите свое сообщение на всех сотрудников или только на некоторых. 

[IMG WIDTH=1004 HEIGHT=565]https://www.bitrix24.ru/images/b24/post_n.png[/IMG]

[B]Установите бесплатное мобильное приложение «Битрикс24» [/B]на смартфон iOS или Android и [B]десктоп-приложение на ваш компьютер[/B] (Windows, MacOS или Linux). Чат, сообщения в Живой ленте, задачи, файлы, контакты клиентов и сделки в CRM - все будет доступно с телефона.

[IMG WIDTH=300 HEIGHT=201]https://www.bitrix24.ru/images/b24/mob_tr.png[/IMG][IMG WIDTH=340 HEIGHT=180]https://www.bitrix24.ru/images/b24/desk_tr.png[/IMG]

[IMG WIDTH=60 HEIGHT=1]https://www.bitrix24.ru/images/b24/i.png[/IMG][URL=https://itunes.apple.com/ru/app/bitrix24/id561683423?l=ru&ls=1&mt=8][IMG WIDTH=91 HEIGHT=28]https://www.bitrix24.ru/images/b24/as001.png[/IMG][/URL] [URL=https://play.google.com/store/apps/details?id=com.bitrix24.android][IMG WIDTH=98 HEIGHT=25]https://www.bitrix24.ru/images/b24/gp005.png[/IMG][/URL][IMG WIDTH=120 HEIGHT=1]https://www.bitrix24.ru/images/b24/i.png[/IMG][URL=http://dl.bitrix24.com/b24/bitrix24_desktop.dmg][IMG WIDTH=74 HEIGHT=31]https://www.bitrix24.ru/images/b24/mac001.png[/IMG][/URL] [URL=http://dl.bitrix24.com/b24/bitrix24_desktop.exe][IMG WIDTH=88 HEIGHT=26]https://www.bitrix24.ru/images/b24/win002.png[/IMG][/URL] [URL=https://github.com/buglloc/brick][IMG WIDTH=67 HEIGHT=28]https://www.bitrix24.ru/images/b24/linux003.png[/IMG][/URL]";
$MESS["BLOG_DEMO_MESSAGE_TITLE2"] = "Как работать с задачами в Битрикс24";
$MESS["BLOG_DEMO_MESSAGE_BODY2"] = "Наведите порядок в своих задачах, успевайте больше, эффективнее контролируйте, как выполняются ваши поручения.  
[IMG WIDTH=1367 HEIGHT=713]https://www.bitrix24.ru/images/b24/tasks_how.png[/IMG]   

[B]С чего начать?[/B]

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.ru/images/b24/1p.png[/IMG] Перенесите ваши задачи в Битрикс24, оцените, насколько это удобно и быстро. 

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.ru/images/b24/2p.png[/IMG]  Следите за сроками через игру «Борьба со счетчиками» :) Цель - ни одной «красной лампочки» в списке задач, ведь каждый счетчик — это не выполненная вовремя задача. 

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.ru/images/b24/3p.png[/IMG]  Поставьте автоматические напоминания: себе или ответственному за задачу.";
$MESS["BLOG_DEMO_MESSAGE_TITLE3"] = "С чего начать работу в CRM";
$MESS["BLOG_DEMO_MESSAGE_BODY3"] = "В [URL=/crm/start/]Битрикс24.CRM[/URL] собирается информация о клиентах - о компаниях и сотрудниках, сделки, коммерческие предложения, счета, а также вся история (звонки, задачи, переписка, информация о встречах). 
 
[IMG WIDTH=1115 HEIGHT=573]https://www.bitrix24.ru/images/b24/b24crm.png[/IMG]

[B]
[/B][B]Начать работать в CRM очень просто! [/B]

Подключите к Битрикс24: 

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.ru/images/b24/1p.png[/IMG] Электронную почту через [URL=/company/personal/mail/]Email-трекер[/URL]
[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.ru/images/b24/2p.png[/IMG] [URL=/telephony/lines.php]Виртуальную АТС[/URL]  для звонков клиентам
[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.ru/images/b24/3p.png[/IMG] Группы в соцсетях к [URL=/openlines/list/]Открытым линиям[/URL]
[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.ru/images/b24/4p.png[/IMG] [URL=/openlines/connector/?ID=livechat]Бесплатный онлайн-чат[/URL] на сайт 
[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.ru/images/b24/5p.png[/IMG] [URL=/crm/webform/]CRM-формы[/URL]  
[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.ru/images/b24/6p.png[/IMG] 1С через [URL=/marketplace/app/11/]1С-трекер[/URL]

и CRM будет незаметно забирать все новые контакты, сохранять историю общения с клиентами, и вам не придется переносить все это вручную. Конечно, вы всегда можете добавить клиента вручную или импортировать базу из .csv или почтовых программ. 
[B]
[/B]После этого можно настроить стадии сделок в разделе Настройки > Справочники (ваши стадии могут быть любые и отличаться от стандартных) и переходить к работе.  

[URL=/crm/start/][IMG WIDTH=133 HEIGHT=32]https://www.bitrix24.ru/images/b24/go_to_crm.png[/IMG][/URL] 



[B]Вам нужно больше информации о работе в Битрикс24?[/B] 

[IMG WIDTH=160 HEIGHT=134]https://www.bitrix24.ru/images/b24/cloudman24.png[/IMG] [IMG WIDTH=138 HEIGHT=138]https://www.bitrix24.ru/images/b24/marta.png[/IMG]
Посмотрите подсказки в Поддержке24 - в правом верхнем углу экрана на каждой странице есть иконка [IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.ru/images/b24/help24.png[/IMG]. Здесь собраны советы, как работать с CRM, Задачами, Диском и другими инструментами, видеоуроки и учебные курсы. 

Еще вы в любое время можете спросить Марту - это ваш личный виртуальный помощник в чате Битрикс24, ее легко найти поиском [IMG WIDTH=16 HEIGHT=16]/bitrix/images/blog/smile/icon_smile.png[/IMG] ";
?>