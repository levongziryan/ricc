<?
$MESS["ADMIN_SECTION_GROUP_DESC"] = "Os membros deste grupo podem acessar o Painel de Controle.";
$MESS["ADMIN_SECTION_GROUP_NAME"] = "Usuários do Painel de Controle";
$MESS["CREATE_GROUPS_GROUP_DESC"] = "Os membros deste grupo podem criar novos grupos de trabalho.";
$MESS["CREATE_GROUPS_GROUP_NAME"] = "Administradores do grupo de trabalho";
$MESS["DIRECTION_GROUP_DESC"] = "Conselho de Administração da Empresa";
$MESS["DIRECTION_GROUP_NAME"] = "Conselho de Administração";
$MESS["EMPLOYEES_GROUP_DESC"] = "Todos os funcionários da empresa, registrados no portal.";
$MESS["EMPLOYEES_GROUP_NAME"] = "Funcionários";
$MESS["MARKETING_AND_SALES_GROUP_DESC"] = "Pessoal de vendas e departamento de Marketing.";
$MESS["MARKETING_AND_SALES_GROUP_NAME"] = "Vendas e Marketing";
$MESS["PERSONNEL_DEPARTMENT_GROUP_DESC"] = "Pessoal do departamento de RH.";
$MESS["PERSONNEL_DEPARTMENT_GROUP_NAME"] = "Departamento de RH";
$MESS["PORTAL_ADMINISTRATION_GROUP_DESC"] = "Portal Administradores têm acesso a todos os serviços do portal.";
$MESS["PORTAL_ADMINISTRATION_GROUP_NAME"] = "Portal Administradores";
$MESS["SUPPORT_GROUP_DESC"] = "Especialista Helpdesk.";
$MESS["SUPPORT_GROUP_NAME"] = "Helpdesk";
?>