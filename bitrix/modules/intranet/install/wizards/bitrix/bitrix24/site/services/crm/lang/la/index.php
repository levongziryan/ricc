<?
$MESS["CRM_TOP_LINKS_ITEM_NAME"] = "CRM";
$MESS["CRM_GADGET_MY_LEAD_TITLE"] = "Mis prospectos";
$MESS["CRM_GADGET_NEW_LEAD_TITLE"] = "Nuevos prospectos";
$MESS["CRM_GADGET_CLOSED_DEAL_TITLE"] = "Negociaciones cerradas";
$MESS["CRM_GADGET_LAST_EVENT_TITLE"] = "Eventos recientes";
$MESS["CRM_GADGET_NEW_CONTACT_TITLE"] = "Nuevos contactos";
$MESS["CRM_GADGET_NEW_COMPANY_TITLE"] = "Nuevas compañías";
$MESS["CRM_ROLE_ADMIN"] = "Administrador";
$MESS["CRM_ROLE_DIRECTOR"] = "Director";
$MESS["CRM_ROLE_CHIF"] = "Jefe del departamento";
$MESS["CRM_ROLE_MAN"] = "Manager";
?>