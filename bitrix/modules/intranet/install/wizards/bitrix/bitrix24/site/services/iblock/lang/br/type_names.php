<?
$MESS["NEWS_TYPE_NAME"] = "Notícia";
$MESS["NEWS_ELEMENT_NAME"] = "Notícia";
$MESS["STRUCTURE_TYPE_NAME"] = "Estrutura da empresa";
$MESS["STRUCTURE_ELEMENT_NAME"] = "Elementos";
$MESS["STRUCTURE_SECTION_NAME"] = "Seções";
$MESS["SERVICES_TYPE_NAME"] = "Serviços";
$MESS["SERVICES_ELEMENT_NAME"] = "Elementos";
$MESS["SERVICES_SECTION_NAME"] = "Seções";
$MESS["EVENTS_TYPE_NAME"] = "Calendário";
$MESS["EVENTS_ELEMENT_NAME"] = "Eventos";
$MESS["EVENTS_SECTION_NAME"] = "Calendários";
$MESS["LIBRARY_TYPE_NAME"] = "Documentos";
$MESS["LIBRARY_ELEMENT_NAME"] = "Arquivos";
$MESS["LIBRARY_SECTION_NAME"] = "Pastas";
$MESS["PHOTOS_TYPE_NAME"] = "Galeria de Fotos";
$MESS["PHOTOS_ELEMENT_NAME"] = "Fotos";
$MESS["PHOTOS_SECTION_NAME"] = "lbuns";
$MESS["LISTS_TYPE_NAME"] = "Listas";
$MESS["LISTS_ELEMENT_NAME"] = "Elementos";
$MESS["LISTS_SECTION_NAME"] = "Seções";
$MESS["LISTS_SOCNET_TYPE_NAME"] = "Listas da Rede Social";
$MESS["LISTS_SOCNET_ELEMENT_NAME"] = "Elementos";
$MESS["LISTS_SOCNET_SECTION_NAME"] = "Seções";
?>