<?
$MESS ['COMMENTS_GROUP_DESCRIPTION'] = "";
$MESS ['COMMENTS_GROUP_NAME'] = "Foren für Kommentare";
$MESS ['DOCS_SHARED_COMMENTS_DECRIPTION'] = "";
$MESS ['DOCS_SHARED_COMMENTS_NAME'] = "Kommentare für allgemeine Dokumente";
$MESS ['GENERAL_FORUM_DESCRIPTION'] = "Forum für alle Mitarbeiter. Diskussionen und Meinungsaustausch über interne Vorgänge.";
$MESS ['GENERAL_FORUM_MESSAGE_BODY'] = "Mitarbeiter haben jetzt die Möglichkeit, eigene Dateien in ihrem persönlichen Bereich zu speichern.

Weitere Informationen über die Arbeit mit dem Dateiarchiv sowie Informationen über Netzwerkressourcen (webDAV) finden Sie auf der Seite \"Mein Profil\" im Abschnitt \"Dateien.\"

Bei Fragen wenden Sie sich bitte über das Web-Formular auf unserer Seite an die Spezialisten des Technischen Supports.";
$MESS ['GENERAL_FORUM_NAME'] = "Allgemeines";
$MESS ['GENERAL_FORUM_TOPIC_DESCRIPTION'] = "";
$MESS ['GENERAL_FORUM_TOPIC_TITLE'] = "Portal News";
$MESS ['GENERAL_GROUP_DESCRIPTION'] = "";
$MESS ['GENERAL_GROUP_NAME'] = "Allgemeine Foren";
$MESS ['GROUPS_AND_USERS_COMMENTS_NAME'] = "Kommentare für Dokumente";
$MESS ['GROUPS_AND_USERS_FILES_COMMENTS_DECRIPTION'] = "";
$MESS ['HIDDEN_GROUP_DESCRIPTION'] = "";
$MESS ['HIDDEN_GROUP_NAME'] = "Versteckte Foren";
$MESS ['PHOTOGALLERY_COMMENTS_FORUM_DESCRIPTION'] = "";
$MESS ['PHOTOGALLERY_COMMENTS_FORUM_NAME'] = "Kommentare für Fotogalerien";
$MESS ['USERS_AND_GROUPS_FORUM_DESCRIPTION'] = "User- und Gruppenforen";
$MESS ['USERS_AND_GROUPS_FORUM_NAME'] = "User- und Gruppenforen";
?>