<?
$MESS["CRM_TOP_LINKS_ITEM_NAME"] = "CRM";
$MESS["CRM_GADGET_MY_LEAD_TITLE"] = "Meine Leads";
$MESS["CRM_GADGET_NEW_LEAD_TITLE"] = "Neue Leads";
$MESS["CRM_GADGET_CLOSED_DEAL_TITLE"] = "Abgeschlossene Aufträge";
$MESS["CRM_GADGET_LAST_EVENT_TITLE"] = "Jüngste Aktivitäten";
$MESS["CRM_GADGET_NEW_CONTACT_TITLE"] = "Neue Kontakte";
$MESS["CRM_GADGET_NEW_COMPANY_TITLE"] = "Neue Unternehmen";
$MESS["CRM_ROLE_ADMIN"] = "Administrator";
$MESS["CRM_ROLE_DIRECTOR"] = "Geschäftsführer";
$MESS["CRM_ROLE_CHIF"] = "Abteilungsleiter";
$MESS["CRM_ROLE_MAN"] = "Manager";
?>