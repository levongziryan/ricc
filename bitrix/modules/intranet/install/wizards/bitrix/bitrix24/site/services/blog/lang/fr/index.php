<?
$MESS["BLOG_DEMO_GROUP_SOCNET"] = "Groupe de réseau social";
$MESS["BLG_NAME"] = "Blog de";
$MESS["BLOG_DEMO_CATEGORY_1"] = "Conseil";
$MESS["BLOG_DEMO_CATEGORY_2"] = "Astuces";
$MESS["BLOG_DEMO_MESSAGE_TITLE"] = "Bitrix24 : nous ne faisons que commencer !";
$MESS["BLOG_DEMO_MESSAGE_BODY"] = "Aujourd'hui est le jour glorieux de la création de notre propre Bitrix24, notre espace de travail partagé. Il s'agit d'une arène destinée à communiquer, travailler, développer des projets, résoudre des problèmes, et faire tout un tas de trucs d'une manière innovante et plus productive. Peu importe où vous êtes - au bureau, en voyage d'affaire ou à domicile -, nous pouvons vous contacter et vous pouvez nous contacter dès qu'il y a une tâche à accomplir ou quelque chose à discuter.

Bitrix24 est un moyen naturel d'organiser des discussions de groupe grâce au Flux d'activité, de communiquer en tête à tête ou en chats de groupe, d'appeler avec vidéo, de créer et de contrôler des tâches, de planifier vos journées de travail, et bien plus encore.

[B]Alors, par où on commence ?[/B]

1. Commençons par quelque chose de très simple. Ajoutez un \"j'aime\" à ce message et regardez qui a fait de même.

[IMG WIDTH=600 HEIGHT=300]/images/demo_post/en/1.png[/IMG]

2. Ajoutez votre commentaire, ou envoyez un message à tout le monde ou juste à certaines personnes en particulier.

[IMG WIDTH=600 HEIGHT=310]/images/demo_post/en/2.png[/IMG]

3. Installez l'application mobile gratuite Bitrix24 sur votre téléphone - Android comme iOS -, et une application de bureau sur votre ordinateur (Windows, MacOS or Linux) :

[URL=https://itunes.apple.com/app/bitrix24/id561683423][IMG]/images/demo_post/appstore.png[/IMG][/URL] [URL=https://play.google.com/store/apps/details?id=com.bitrix24.android][IMG]/images/demo_post/googleplay.png[/IMG][/URL]  [URL=http://dl.bitrix24.com/b24/bitrix24_desktop.exe][IMG]/images/demo_post/windows.png[/IMG][/URL] [URL=http://dl.bitrix24.com/b24/bitrix24_desktop.dmg][IMG]/images/demo_post/mac.png[/IMG][/URL] [URL=https://github.com/buglloc/brick][IMG]/images/demo_post/linux.png[/IMG][/URL]

Dès que vous avez une question sur le fonctionnement de notre Bitrix24, vous pouvez consulter Support24 où vous trouverez les réponses à la plupart des questions posées par de nouveaux utilisateurs, ainsi que des vidéos de formation et des cours d'entraînement.";
$MESS["BLOG_DEMO_COMMENT_TITLE"] = "Intéressant !";
$MESS["BLOG_DEMO_COMMENT_BODY"] = "Je vais essayer !";
$MESS["BPC_SONET_POST_TITLE"] = "a ajouté un message au blog \"#TITLE#\"";
$MESS["BPC_SONET_COMMENT_TITLE"] = "a ajouté un commentaire au message du blog \"#TITLE#\"";
$MESS["BLOG_DEMO_MESSAGE_TITLE1"] = "Collaborez !";
$MESS["BLOG_DEMO_MESSAGE_BODY1"] = "Bitrix24 est une plateforme gratuite de collaboration et de communication. Grâce à Bitrix24, les documents, calendriers, chats de groupe, tâches, projets, GRC, conférences vidéo, téléphonies, intranets sociaux et autres outils sont toujours à portée de main, que vous soyez au bureau, chez vous ou en déplacement.

Commencez par inviter vos collègues partenaires commerciaux et clients sur votre compte Bitrix24. Démarrez une collaboration en envoyant un message à tous les utilisateurs qui ont rejoint votre compte.

[IMG WIDTH=1004 HEIGHT=565]https://www.bitrix24.com/images/b24_en/post_n.png[/IMG]

Installez l'application mobile gratuite pour iOS ou Android afin de rester en contact quand vous n'avez pas accès à votre ordinateur. Jetez un œil à nos applications de bureau pour Mac, PC ou Linux et apprenez comment vous pouvez travailler plus facilement avec vos documents et synchroniser de vos fichiers entre vos appareils. 

[IMG WIDTH=300 HEIGHT=201]https://www.bitrix24.com/images/b24_en/mob_tr.png[/IMG][IMG WIDTH=340 HEIGHT=180]https://www.bitrix24.com/images/b24_en/desk_tr.png[/IMG]

[IMG WIDTH=60 HEIGHT=1]https://www.bitrix24.com/images/b24_en/i.png[/IMG][URL=https://itunes.apple.com/en/app/bitrix24/id561683423?l=en&ls=1&mt=8][IMG WIDTH=91 HEIGHT=28]https://www.bitrix24.com/images/b24_en/as001.png[/IMG][/URL] [URL=https://play.google.com/store/apps/details?id=com.bitrix24.android&hl=en][IMG WIDTH=98 HEIGHT=25]https://www.bitrix24.com/images/b24_en/gp005.png[/IMG][/URL][IMG WIDTH=120 HEIGHT=1]https://www.bitrix24.com/images/b24_en/i.png[/IMG][URL=http://dl.bitrix24.com/b24/bitrix24_desktop.dmg][IMG WIDTH=74 HEIGHT=31]https://www.bitrix24.com/images/b24_en/mac001.png[/IMG][/URL] [URL=http://dl.bitrix24.com/b24/bitrix24_desktop.exe][IMG WIDTH=88 HEIGHT=26]https://www.bitrix24.com/images/b24_en/win002.png[/IMG][/URL] [URL=https://github.com/buglloc/brick][IMG WIDTH=67 HEIGHT=28]https://www.bitrix24.com/images/b24_en/linux003.png[/IMG][/URL]";
$MESS["BLOG_DEMO_MESSAGE_TITLE2"] = "Une gestion des tâches facilitée grâce à Bitrix24 !";
$MESS["BLOG_DEMO_MESSAGE_BODY2"] = "Fatigué de la confusion et des délais dépassés ? Bitrix24 vous permet d'organiser vos tâches et de vous assurer que tout soit terminé à temps.
[IMG WIDTH=1367 HEIGHT=713]https://www.bitrix24.com/images/b24_en/tasks_how.png[/IMG]   

[B]Par où commencer ?[/B]

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.com/images/b24_en/1p.png[/IMG] Personnalisez le formulaire de création de tâches pour qu'il corresponde à vos besoins. 

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.com/images/b24_en/2p.png[/IMG] Créez des tâches récurrentes et des modèles pour gérer plus efficacement la routine. 

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.com/images/b24_en/3p.png[/IMG] Utilisez les diagrammes de Gantt, les listes de contrôle et les dépendances de tâche pour coordonner les projets les plus complexes.

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.com/images/b24_en/4p.png[/IMG] Définissez des rappels pour ne plus manquer de date limite.

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.com/images/b24_en/5p.png[/IMG] Apprenez comment créer des tâches à partir d'e-mails, de commentaires ou de messages.";
$MESS["BLOG_DEMO_MESSAGE_TITLE3"] = "Les premiers pas en GRC";
$MESS["BLOG_DEMO_MESSAGE_BODY3"] = "[URL=/crm/stream/]Bitrix24.CRM[/URL] conserve toutes les informations sur vos clients et perspectives - informations de contact, détails des sociétés, transactions, devis, factures, requêtes, réunions, e-mails, appels, et autres.
 
[IMG WIDTH=1115 HEIGHT=573]https://www.bitrix24.com/images/b24_en/b24crm.png[/IMG]

[B]
[/B][B]Laissez votre GRC faire le boulot pour vous ![/B]

Vous pouvez connecter Bitrix24.CRM à : 

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.com/images/b24_en/1p.png[/IMG] Votre [URL=/company/personal/mail/]adresse e-mail[/URL] pour synchroniser tous les e-mails entre la GRC et votre compte e-mail.
[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.com/images/b24_en/2p.png[/IMG] Votre [URL=/telephony/lines.php]système téléphonique[/URL] pour passer, recevoir et enregistrer des appels téléphoniques.
[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.com/images/b24_en/3p.png[/IMG] Vos [URL=/settings/openlines/]comptes de réseaux sociaux[/URL] pour communiquer avec vos abonnés.
[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.com/images/b24_en/4p.png[/IMG] Votre site internet pour discuter avec vos visiteurs via le [URL=/settings/openlines/]chat en direct[/URL]. 
[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.com/images/b24_en/5p.png[/IMG] Vos [URL=/crm/webform/]formulaires en ligne[/URL] pour conserver dans la GRC les données collectées.

Peu importe comment vous choisissez de communiquer avec vos clients - via e-mail, téléphone, réseau social ou votre site internet -, les informations finiront dans votre GRC et seront facilement accessibles et retrouvables. Naturellement, vous pouvez aussi ajouter manuellement de nouvelles données, migrer depuis une autre GRC ou importer des données de clients à partir d'un fichier CSV.
[B]
[/B]Les champs personnalisés, les limitations de droits d'accès, les entonnoirs des ventes et les multiples pipelines seront tous à votre disposition. 

[URL=/crm/stream/][IMG WIDTH=133 HEIGHT=32]https://www.bitrix24.com/images/b24_en/go_to_crm.png[/IMG][/URL] 



[B]Vous voulez en savoir plus sur Bitrix24 ?[/B] 

[IMG WIDTH=160 HEIGHT=134]https://www.bitrix24.com/images/b24_en/cloudman24.png[/IMG]
Cherchez cette icône en haut à droite : [IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.com/images/b24_en/help24.png[/IMG]. Quand vous cliquez sur le point d'interrogation, vous êtes renvoyé vers la section Support24. ";
?>