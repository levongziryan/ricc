<?
$MESS["GENERAL_GROUP_NAME"] = "Forum geral";
$MESS["COMMENTS_GROUP_NAME"] = "Fórum para comentários";
$MESS["HIDDEN_GROUP_NAME"] = "Fóruns ocultos";
$MESS["GENERAL_FORUM_NAME"] = "Fórum geral";
$MESS["GENERAL_FORUM_DESCRIPTION"] = "Fórum público para os funcionários da empresa. Discuta o seu negócio e troque opiniões aqui.";
$MESS["GENERAL_FORUM_TOPIC_TITLE"] = "Portal de Notícias";
$MESS["GENERAL_FORUM_MESSAGE_BODY"] = "Atenção! Os funcionários da empresa podem agora manter o seu armazenamento de arquivos na sua área privada.

Você vai encontrar informações detalhadas sobre o gerenciamento de armazenamento do arquivo e o método de mapeamento de armazenamento para uma unidade de rede na seção de ajuda: \"Meu Perfil - Arquivos\".

Se você tiver alguma dúvida sobre arquivo de configuração de armazenamento, por favor, envie suas solicitações para os engenheiros da equipe de suporte técnico usando o formulário de solicitação de apoio.";
$MESS["PHOTOGALLERY_COMMENTS_FORUM_NAME"] = "Discussão da  Galeria de Fotos";
$MESS["USERS_AND_GROUPS_FORUM_NAME"] = "Usuários e grupos";
$MESS["USERS_AND_GROUPS_FORUM_DESCRIPTION"] = "Fóruns pessoais e de grupo";
$MESS["DOCS_SHARED_COMMENTS_NAME"] = "Comentários para biblioteca de arquivos comuns";
$MESS["GROUPS_AND_USERS_COMMENTS_NAME"] = "Comentários para Arquivos";
?>