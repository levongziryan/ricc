<?
$MESS["NEWS_TYPE_NAME"] = "Noticias";
$MESS["NEWS_ELEMENT_NAME"] = "Noticias";
$MESS["STRUCTURE_TYPE_NAME"] = "Estructura de la compañía";
$MESS["STRUCTURE_ELEMENT_NAME"] = "Elementos";
$MESS["STRUCTURE_SECTION_NAME"] = "Secciones";
$MESS["SERVICES_TYPE_NAME"] = "Servicios";
$MESS["SERVICES_ELEMENT_NAME"] = "Elementos";
$MESS["SERVICES_SECTION_NAME"] = "Secciones";
$MESS["EVENTS_TYPE_NAME"] = "Calendario";
$MESS["EVENTS_ELEMENT_NAME"] = "Eventos";
$MESS["EVENTS_SECTION_NAME"] = "Calendarios";
$MESS["PHOTOS_TYPE_NAME"] = "Fotogalería";
$MESS["PHOTOS_ELEMENT_NAME"] = "Fotos";
$MESS["PHOTOS_SECTION_NAME"] = "Albums";
$MESS["LISTS_TYPE_NAME"] = "Listas";
$MESS["LISTS_ELEMENT_NAME"] = "Elementos";
$MESS["LISTS_SECTION_NAME"] = "Secciones";
$MESS["LISTS_SOCNET_TYPE_NAME"] = "Listas de redes sociales";
$MESS["LISTS_SOCNET_ELEMENT_NAME"] = "Elementos";
$MESS["LISTS_SOCNET_SECTION_NAME"] = "Secciones";
?>