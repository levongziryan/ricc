<?
$MESS["BLOG_DEMO_GROUP_SOCNET"] = "Gruppe des sozialen Netzwerks";
$MESS["BLG_NAME"] = "Blog von";
$MESS["BLOG_DEMO_CATEGORY_1"] = "Ratschläge";
$MESS["BLOG_DEMO_CATEGORY_2"] = "Tipps";
$MESS["BLOG_DEMO_MESSAGE_TITLE"] = "Bitrix24: Jetzt geht´s los!";
$MESS["BLOG_DEMO_MESSAGE_BODY"] = "Wir haben es geschafft: heute haben wir unser eigenes Bitrix24 eingerichtet - unseren virtuellen Arbeitsplatz, wo wir miteinander kommunizieren und zusammenarbeiten können. Egal, wo Sie sich befinden, - im Büro, zu Hause oder unterwegs - von überall können Sie auf unser Bitrix24 zugreifen. So stehen wir immer im Kontakt miteinander und können alle Fragen sofort klären.

Mit Bitrix24 können wir Diskussionen im Activity Stream oder in Chats initiieren, Aufgaben erstellen und verwalten, unseren Arbeitstag planen, und vieles andere mehr.

[B]Womit sollen wir anfangen?[/B]

1. Zunächst sollten wir etwas ganz Einfaches machen: versuchen Sie mal diese Nachricht zu liken, dann sehen Sie auch, wer sonst noch die Nachricht geliket hat.

[IMG WIDTH=600 HEIGHT=300]/images/demo_post/de/1.png[/IMG]

2. Sie können diese Nachricht auch kommentieren oder eine eigene Nachricht an Kollegen schicken.

[IMG WIDTH=600 HEIGHT=310]/images/demo_post/de/2.png[/IMG]

3. Installieren Sie die kostenlose Bitrix24 Mobile App auf Ihr iOS- oder Android-Gerät, und dazu noch die Desktop-App auf Ihren Computer (Windows, MacOS oder Linux):

[URL=https://itunes.apple.com/de/app/bitrix24/id561683423][IMG]/images/demo_post/appstore.png[/IMG][/URL] [URL=https://play.google.com/store/apps/details?id=com.bitrix24.android&hl=de][IMG]/images/demo_post/googleplay.png[/IMG][/URL]  [URL=http://dl.bitrix24.com/b24/bitrix24_desktop.exe][IMG]/images/demo_post/windows.png[/IMG][/URL] [URL=http://dl.bitrix24.com/b24/bitrix24_desktop.dmg][IMG]/images/demo_post/mac.png[/IMG][/URL] [URL=https://github.com/buglloc/brick][IMG]/images/demo_post/linux.png[/IMG][/URL]

Wenn Sie Fragen zur Nutzung von Bitrix24 haben, öffnen Sie einfach den Bereich Support24 (Hilfe), wo die meisten Fragen, welche neue Nutzer stellen, bereits beantwortet sind. Außerdem finden Sie hier Videos zu verschiedenen Themen sowie Trainingskurse.";
$MESS["BLOG_DEMO_COMMENT_TITLE"] = "Interessant!";
$MESS["BLOG_DEMO_COMMENT_BODY"] = "Ich versuch's!";
$MESS["BPC_SONET_POST_TITLE"] = "hat einen Beitrag \"#TITLE#\" im Blog erstellt";
$MESS["BPC_SONET_COMMENT_TITLE"] = "hat einen Kommentar zum Beitrag  \"#TITLE#\" im Blog hinzugefügt";
$MESS["BLOG_DEMO_MESSAGE_TITLE1"] = "Zusammenarbeit fängt hier an";
$MESS["BLOG_DEMO_MESSAGE_BODY1"] = "Bitrix24 ist eine Plattform für Kommunikation und Zusammenarbeit. In Bitrix24 stehen Ihnen Dokumente, Kalender, Aufgaben, Gruppenchats, CRM, Projektgruppen und andere Tools jederzeit zur Verfügung, egal wo Sie sich gerade befinden - im Büro, zu Hause oder unterwegs.

Laden Sie Ihre Kollegen, Geschäftspartner und Kunden in Ihr Bitrix24 ein. Starten Sie die Zusammenarbeit, indem Sie eine Nachricht an alle Nutzer in Ihrem Account senden.

[IMG WIDTH=1004 HEIGHT=565]https://www.bitrix24.de/images/b24_de/post_n.png[/IMG]

Installieren Sie die kostenlose mobile App für iOS oder Android, um auch dann in Kontakt zu bleiben, wenn Sie keinen Zugriff auf Ihren Computer haben. Testen Sie unsere Desktop App für Mac, Windows oder Linux, die eine Synchronisierung von Dateien ermöglicht und somit die Arbeit mit Dokumenten wesentlich erleichetert. 

[IMG WIDTH=300 HEIGHT=201]https://www.bitrix24.de/images/b24_de/mob_tr.png[/IMG][IMG WIDTH=340 HEIGHT=180]https://www.bitrix24.de/images/b24_de/desk_tr.png[/IMG]

[IMG WIDTH=60 HEIGHT=1]https://www.bitrix24.de/images/b24_de/i.png[/IMG][URL=https://itunes.apple.com/de/app/bitrix24/id561683423?l=de&ls=1&mt=8][IMG WIDTH=91 HEIGHT=28]https://www.bitrix24.de/images/b24_de/as001.png[/IMG][/URL] [URL=https://play.google.com/store/apps/details?id=com.bitrix24.android&hl=de][IMG WIDTH=98 HEIGHT=25]https://www.bitrix24.de/images/b24_de/gp005.png[/IMG][/URL][IMG WIDTH=120 HEIGHT=1]https://www.bitrix24.de/images/b24_de/i.png[/IMG][URL=http://dl.bitrix24.com/b24/bitrix24_desktop.dmg][IMG WIDTH=74 HEIGHT=31]https://www.bitrix24.de/images/b24_de/mac001.png[/IMG][/URL] [URL=http://dl.bitrix24.com/b24/bitrix24_desktop.exe][IMG WIDTH=88 HEIGHT=26]https://www.bitrix24.de/images/b24_de/win002.png[/IMG][/URL] [URL=https://github.com/buglloc/brick][IMG WIDTH=67 HEIGHT=28]https://www.bitrix24.de/images/b24_de/linux003.png[/IMG][/URL]";
$MESS["BLOG_DEMO_MESSAGE_TITLE2"] = "Aufgabenmanagement in Bitrix24";
$MESS["BLOG_DEMO_MESSAGE_BODY2"] = "Mit Bitrix24 können Sie alle Ihre Aufgaben klar strukturieren und meistern - effektiv und rechtzeitig.
[IMG WIDTH=1367 HEIGHT=713]https://www.bitrix24.de/images/b24_de/tasks_how.png[/IMG]   

[B]Womit soll ich anfangen?[/B]

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.de/images/b24_de/1p.png[/IMG] Passen Sie das Formular zur Aufgabenerstellung so an, dass es Ihren Anforderungen entspricht;

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.de/images/b24_de/2p.png[/IMG] Erstellen Sie Serienaufgaben sowie Aufgabenvorlagen, um Ihre Arbeitsroutine schneller zu erledigen;

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.de/images/b24_de/3p.png[/IMG] Nutzen Sie Gantt-Diagramm, Checklisten und Aufgabenabhängigkeiten, um auch komplexe Projekte koordinieren zu können;

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.de/images/b24_de/4p.png[/IMG] Stellen Sie Erinnerungen ein - so werden keine Fristen vergessen;

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.de/images/b24_de/5p.png[/IMG] Erfahren Sie, wie Sie Aufgaben aus E-Mails oder aus Nachrichten und Kommentaren erstellen können.";
$MESS["BLOG_DEMO_MESSAGE_TITLE3"] = "Die ersten Schritte im CRM";
$MESS["BLOG_DEMO_MESSAGE_BODY3"] = "Im Bitrix24.CRM können Sie alle relevanten Informationen über Ihre Kunden und Kontakte abspeichern. Hier geben Sie Informationen zu Unternehmen an, erstellen Angebote und Rechnungen, versenden E-Mails, vereinbaren Termine usw.
 
[IMG WIDTH=1115 HEIGHT=573]https://www.bitrix24.de/images/b24_de/b24crm.png[/IMG]

[B]
[/B][B]Unser CRM kann Ihre Arbeit komplett übernehmen.[/B]

Verbinden Sie Ihr Bitrix24.CRM mit

[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.de/images/b24_de/1p.png[/IMG] Ihrer [URL=/company/personal/mail/]E-Mail-Adresse[/URL], um alle Nachrichten zwischen CRM und Ihrem Postfach zu synchronisieren;
[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.de/images/b24_de/2p.png[/IMG] Ihrer [URL=/telephony/lines.php]Telefonanlage[/URL], um anzurufen sowie Anrufe erhalten zu können;
[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.de/images/b24_de/3p.png[/IMG] Ihren [URL=/settings/openlines/]Accounts in den sozialen Netzwerken[/URL], um so mit Ihren Abonnenten zu kommunizieren;
[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.de/images/b24_de/4p.png[/IMG] Ihrer Website, um mit Besuchern via [URL=/settings/openlines/]Onlinechat[/URL] zu sprechen; 
[IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.de/images/b24_de/5p.png[/IMG] Ihren [URL=/crm/webform/]CRM-Formularen[/URL], um Daten zu sammeln und sie im CRM abzuspeichern.

Egal, welchen Kommunikationskanal Sie für Ihre Kunden bevorzugen - E-Mail, Telefon, soziale Netzwerke oder Website, alle Informationen landen in Ihrem CRM. Neue Daten können Sie entweder manuell eingeben oder sie aus einer CSV-Datei importieren. Eine Migration aus einem anderen CRM-System ist dabei auch möglich.
[B]
[/B]Darüber hinaus finden Sie im CRM benutzerdefinierte Felder, Rollenverteilung inkl. Zugriffsrechte, Sales Funnel und mehrfache Pipelines und vieles andere.

[URL=/crm/stream/][IMG WIDTH=133 HEIGHT=32]https://www.bitrix24.de/images/b24_de/go_to_crm.png[/IMG][/URL] 



[B]Möchten Sie mehr über Bitrix24 erfahren?[/B] 

[IMG WIDTH=160 HEIGHT=134]https://www.bitrix24.de/images/b24_de/cloudman24.png[/IMG]
Finden Sie oben rechts dieses Icon [IMG WIDTH=20 HEIGHT=20]https://www.bitrix24.de/images/b24_de/help24.png[/IMG]. Per Klick auf das Fragezeichen wird Ihnen der Support-Bereich angezeigt. ";
?>