<?
$MESS["NEXT_BUTTON"] = "Siguiente";
$MESS["PREVIOUS_BUTTON"] = "Regresar";
$MESS["INSTALL_SERVICE_FINISH_STATUS"] = "La configuración está completa";
$MESS["INST_ERROR_OCCURED"] = "¡Atención! Ocurrió un error en este paso de la instalación.";
$MESS["INST_TEXT_ERROR"] = "Mensaje de error";
$MESS["INST_ERROR_NOTICE"] = "Repita el paso actual. Si el error persiste, omita el paso.";
$MESS["INST_RETRY_BUTTON"] = "Volver a procesar";
$MESS["INST_SKIP_BUTTON"] = "Saltar";
$MESS["FINISH_STEP_TITLE"] = "Finalización de la configuración";
$MESS["FINISH_STEP_CONTENT"] = "<B> ¡Enhorabuena! </ B> <br /> <br /> La configuración ya está completa. Ahora puedes abrir tu Bitrix24 y empezar a trabajar. <br />";
$MESS["WIZARD_WAIT_WINDOW_TEXT"] = "Installing data...";
$MESS["INST_JAVASCRIPT_DISABLED"] = "El asistente requiere que JavaScript esté habilitado en su sistema. JavaScript está deshabilitado o no es compatible con su navegador. Modifica la configuración del navegador y <a href=\"\"> vuelve a intentarlo </a>.";
$MESS["wiz_ldap_warn"] = "¡Atención! <br /> Si desea que su portal de intranet soporte Active Directory, asegúrese de que el módulo LDAP está instalado en PHP.";
$MESS["wiz_slogan"] = "Bitrix";
$MESS["wiz_company_name"] = "Nombre de la compañía:";
$MESS["wiz_install_data"] = "Instalación de datos";
$MESS["wiz_go"] = "Iniciar Bitrix24";
$MESS["wiz_install_data_extranet"] = "Instalación de extranet";
?>