<?
$MESS["NEXT_BUTTON"] = "Próximo";
$MESS["PREVIOUS_BUTTON"] = "De volta";
$MESS["INSTALL_SERVICE_FINISH_STATUS"] = "A configuração foi Concluída";
$MESS["INST_ERROR_OCCURED"] = "Atenção! Ocorreu um erro nesta etapa da instalação.";
$MESS["INST_TEXT_ERROR"] = "Mensagem de erro";
$MESS["INST_ERROR_NOTICE"] = "Por favor, repita o passo atual. Se o erro persistir, pule a etapa.";
$MESS["INST_RETRY_BUTTON"] = "Tentar novamente";
$MESS["INST_SKIP_BUTTON"] = "Pular";
$MESS["FINISH_STEP_TITLE"] = "Finalizando a configuração";
$MESS["FINISH_STEP_CONTENT"] = "<b>Parabéns!<b><br/><br/> A configuração está concluída. Agora você pode abrir o seu Bitrix Intranet e começar a trabalhar.<br/>";
$MESS["WIZARD_WAIT_WINDOW_TEXT"] = "Instalando dados ...";
$MESS["INST_JAVASCRIPT_DISABLED"] = "O assistente requer que o JavaScript esteja habilitado no seu sistema. JavaScript está desabilitado ou não é suportado pelo seu navegador. Por favor, altere as configurações do navegador e<a href=\"\"> tente novamente</a>.";
$MESS["wiz_ldap_warn"] = "Atenção!<br/> Se você quer que seu portal intranet ofereça suporte ao Active Directory, garanta que o módulo LDAP está instalado no PHP.";
$MESS["wiz_slogan"] = "Minha Empresa";
$MESS["wiz_company_name"] = "Nome da Empresa:";
$MESS["wiz_install_data"] = "Dados da instalação";
$MESS["wiz_go"] = "Lançamento Bitrix Intranet";
?>