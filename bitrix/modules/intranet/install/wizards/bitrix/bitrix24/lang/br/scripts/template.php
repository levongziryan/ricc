<?
$MESS["MAIN_WIZARD_WANT_TO_CANCEL"] = " ";
$MESS["MAIN_WIZARD_WAIT_WINDOW_TEXT"] = " ";
$MESS["WIZARD_TITLE"] = "Bitrix Intranet 11,0 Configuração";
$MESS["COPYRIGHT"] = "Copyright © 2001 - #CURRENT_YEAR# Bitrix, Inc.";
$MESS["SUPPORT"] = "<a href=\"http://www.bitrixsoft.com/?r1=intranet&r2=install\" target=\"_blank\">www.bitrixsoft.com</a> |<a href=\"http://www.bitrixsoft.com/support/?r1=intranet&r2=install\" target=\"_blank\">Suporte Técnico</a>";
$MESS["INST_JAVASCRIPT_DISABLED"] = "O assistente requer que o JavaScript está habilitado em seu sistema. JavaScript está desabilitado ou não é suportado pelo seu navegador. Por favor, altere as configurações do navegador e<a href=\"\"> tente novamente</a>.";
?>