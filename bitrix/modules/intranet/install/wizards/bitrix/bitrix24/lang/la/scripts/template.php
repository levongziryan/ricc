<?
$MESS["WIZARD_TITLE"] = "Configuración Bitrix24.CRM";
$MESS["COPYRIGHT"] = "Copyright &copy; 2001-#CURRENT_YEAR# Bitrix, Inc.";
$MESS["SUPPORT"] = "<a href=\"http://www.bitrixsoft.com/?r1=intranet&r2=install\" target=\"_blank\">www.bitrixsoft.com</a> | <a href=\"http://www.bitrixsoft.com/support/?r1=intranet&r2=install\" target=\"_blank\">Techsupport</a>";
$MESS["INST_JAVASCRIPT_DISABLED"] = "El asistente requiere que JavaScript esté habilitado en su sistema. JavaScript está deshabilitado o no es compatible con su navegador. Modifica la configuración del navegador y <a href=\"\"> vuelve a intentarlo </a>.";
?>