<?
$MESS["WIZARD_TITLE"] = "Configuration de Bitrix24.CRM";
$MESS["COPYRIGHT"] = "Copyright &copy; 2001-#CURRENT_YEAR# Bitrix, Inc.";
$MESS["INST_JAVASCRIPT_DISABLED"] = "L'assistant nécessite que JavaScript soit activé sur votre système. JavaScript est désactivé ou n'est pas pris en charge par votre navigateur. Veuillez modifier les paramètres de votre navigateur et <a href=\"\">réessayer</a>.";
?>