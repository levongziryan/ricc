<?
$MESS["NEXT_BUTTON"] = "Suivant";
$MESS["PREVIOUS_BUTTON"] = "Retour";
$MESS["INSTALL_SERVICE_FINISH_STATUS"] = "La configuration est terminée";
$MESS["INST_ERROR_OCCURED"] = "Attention ! Une erreur est survenue pendant cette étape de l'installation.";
$MESS["INST_TEXT_ERROR"] = "Message d'erreur";
$MESS["INST_ERROR_NOTICE"] = "Veuillez reproduire l'étape actuelle. Si l'erreur persiste, passez-la.";
$MESS["INST_RETRY_BUTTON"] = "Réessayer";
$MESS["INST_SKIP_BUTTON"] = "Passer";
$MESS["FINISH_STEP_TITLE"] = "Finalisation de l'installation";
$MESS["FINISH_STEP_CONTENT"] = "<b>Félicitations !</b><br /><br />La configuration est terminée. Vous pouvez maintenant ouvrir Bitrix24 et commencer à travailler.<br />";
$MESS["WIZARD_WAIT_WINDOW_TEXT"] = "Installation des données...";
$MESS["INST_JAVASCRIPT_DISABLED"] = "L'assistant nécessite que JavaScript soit activé sur votre système. JavaScript est désactivé ou n'est pas pris en charge par votre navigateur. Veuillez modifier les paramètres de votre navigateur et <a href=\"\">réessayer</a>.";
$MESS["wiz_ldap_warn"] = "Attention !<br />Si vous voulez que votre portail intranet prenne en charge Active Directory, assurez-vous que le module LDAP soit installé dans PHP.";
$MESS["wiz_slogan"] = "Bitrix";
$MESS["wiz_company_name"] = "Nom de l’entreprise :";
$MESS["wiz_install_data"] = "Installation des données";
$MESS["wiz_go"] = "Lancer Bitrix24";
$MESS["wiz_install_data_extranet"] = "Installation extranet";
?>