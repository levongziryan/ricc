<?
$MESS["NEXT_BUTTON"] = "Weiter";
$MESS["PREVIOUS_BUTTON"] = "Zurück";
$MESS["INSTALL_SERVICE_FINISH_STATUS"] = "Die Konfiguration ist beendet";
$MESS["INST_ERROR_OCCURED"] = "Achtung! Bei diesem Schritt der Produktinstallation ist ein Fehler aufgetreten.";
$MESS["INST_TEXT_ERROR"] = "Fehlertext";
$MESS["INST_ERROR_NOTICE"] = "Wiederholen Sie die Konfiguration des aktuellen Schritts. Wenn der Fehler wieder auftritt, überspringen Sie diesen Schritt.";
$MESS["INST_RETRY_BUTTON"] = "Den Schritt wiederholen";
$MESS["INST_SKIP_BUTTON"] = "Den Schritt überspringen";
$MESS["FINISH_STEP_TITLE"] = "Konfiguration beenden";
$MESS["FINISH_STEP_CONTENT"] = "<b>Glückwunsch!</b><br />
<br />
Die Konfiguration Ihres  &laquo;Bitrix24&raquo; ist beendet. Sie können das Portal nun öffnen und die Arbeit damit fortsetzen.<br />";
$MESS["WIZARD_WAIT_WINDOW_TEXT"] = "Daten werden installiert...";
$MESS["INST_JAVASCRIPT_DISABLED"] = "Für eine korrekte Arbeitsweise des Installationsassistenten  muss JavaScript eingeschaltet sein. Wahrscheinlich wird JavaScript von Ihrem Browser nicht unterstützt oder ist ausgeschaltet. Ändern Sie die Browser-Einstellungen, und <a href=\"\">versuchen Sie es erneut</a>.";
$MESS["wiz_ldap_warn"] = "Achtung!<br />Wenn eine Active Directory-Integration in das Intranet erforderlich ist, wird PHP mit einem voreingestellten LDAP-Modul benötigt (php_ldap).";
$MESS["wiz_slogan"] = "Bitrix";
$MESS["wiz_company_name"] = "Unternehmensname:";
$MESS["wiz_install_data"] = "Dateninstallation";
$MESS["wiz_go"] = "Zum Bitrix24 wechseln";
$MESS["wiz_install_data_extranet"] = "Extranet installieren";
?>