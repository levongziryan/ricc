<?
$MESS["GD_TASKS_P_PATH_TO_TASK_NEW"] = "URL de création de la tâche";
$MESS["GD_TASKS_P_PATH_TO_TASK"] = "URL de la liste de tâches";
$MESS["GD_TASKS_P_ORDER_BY_D2"] = "date de création";
$MESS["GD_TASKS_P_TYPE_Z"] = "assignés à moi";
$MESS["GD_TASKS_P_ORDER_BY_D1"] = "par la date de l'achèvement";
$MESS["GD_TASKS_P_TYPE"] = "Afficher les tâches";
$MESS["GD_TASKS_P_ORDER_BY_D3"] = "priorité";
$MESS["GD_TASKS_P_TYPE_U"] = "créés par moi";
$MESS["GD_TASKS_P_ORDER_BY"] = "Mettre en ordre par";
?>