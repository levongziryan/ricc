<?
$MESS["GD_LOG_BLOG_USER"] = "blogs";
$MESS["GD_LOG_BLOG"] = "blogues/rapports";
$MESS["GD_LOG_ALL"] = "tous";
$MESS["GD_LOG_GROUP"] = "groupe";
$MESS["GD_LOG_SYSTEM_GROUPS"] = "groupe";
$MESS["GD_LOG_SYSTEM_FRIENDS"] = "amis";
$MESS["GD_LOG_TITLE"] = "Flux d'activités";
$MESS["GD_LOG_MORE"] = "Flux d'activités";
$MESS["GD_LOG_TASKS"] = "tâches";
$MESS["GD_LOG_CALENDAR"] = "calendriers";
$MESS["GD_LOG_FORUM_GROUP"] = "discussions";
$MESS["GD_LOG_BLOG_GROUP"] = "liste de rapports";
$MESS["GD_LOG_USER"] = "aux utilisateurs";
$MESS["GD_LOG_SYSTEM"] = "système";
$MESS["GD_LOG_FILES"] = "fichiers";
$MESS["GD_LOG_FORUM_USER"] = "forums";
$MESS["GD_LOG_FORUM"] = "forums / débats";
$MESS["GD_LOG_PHOTO"] = "photo";
?>