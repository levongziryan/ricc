<?
$MESS["GD_LOG_P_URL"] = "URL de la Page avec la liste de Mises à jour";
$MESS["GD_LOG_P_EVENT_ID_VALUE_BLOG"] = "blogues/rapports";
$MESS["GD_LOG_P_ENTITY_TYPE_VALUE_ALL"] = "tous";
$MESS["GD_LOG_P_EVENT_ID_VALUE_ALL"] = "tous";
$MESS["GD_LOG_P_ENTITY_TYPE_VALUE_GROUP"] = "groupe";
$MESS["GD_LOG_P_EVENT_ID_VALUE_SYSTEM_GROUPS"] = "groupes (utilisateurs seulement)";
$MESS["GD_LOG_P_EVENT_ID_VALUE_SYSTEM_FRIENDS"] = "amis (uniquement utilisateurs)";
$MESS["GD_LOG_P_EVENT_ID_VALUE_TASKS"] = "tâches";
$MESS["GD_LOG_P_EVENT_ID_VALUE_CALENDAR"] = "calendriers";
$MESS["GD_LOG_P_LOG_CNT"] = "Nombre des enregistrements affichés";
$MESS["GD_LOG_P_ENTITY_TYPE_VALUE_USER"] = "aux utilisateurs";
$MESS["GD_LOG_AVATAR_SIZE_COMMENT"] = "Taille de photo dans le Flux d'activités (commentaires)";
$MESS["GD_LOG_AVATAR_SIZE"] = "Taille de photo du profil dans les Actions Récentes (conversation)";
$MESS["GD_LOG_P_EVENT_ID_VALUE_SYSTEM"] = "système";
$MESS["GD_LOG_P_ENTITY_TYPE"] = "Type d'abonnement";
$MESS["GD_LOG_P_EVENT_ID"] = "Type de message";
$MESS["GD_LOG_P_EVENT_ID_VALUE_FILES"] = "fichiers";
$MESS["GD_LOG_P_EVENT_ID_VALUE_FORUM"] = "forums / débats";
$MESS["GD_LOG_P_EVENT_ID_VALUE_PHOTO"] = "photo";
?>