<?
$MESS["GD_LOG_TITLE"] = "Fluxo de Atividades";
$MESS["GD_LOG_MORE"] = "Fluxo de Atividades";
$MESS["GD_LOG_ALL"] = "Todos";
$MESS["GD_LOG_BLOG_USER"] = "blogs";
$MESS["GD_LOG_BLOG"] = "blogs/relatórios";
$MESS["GD_LOG_CALENDAR"] = "calendários";
$MESS["GD_LOG_FORUM_GROUP"] = "discussões";
$MESS["GD_LOG_FILES"] = "Arquivos";
$MESS["GD_LOG_FORUM_USER"] = "Fóruns";
$MESS["GD_LOG_FORUM"] = "Fóruns/Discussões";
$MESS["GD_LOG_SYSTEM_FRIENDS"] = "amigos";
$MESS["GD_LOG_GROUP"] = "grupos";
$MESS["GD_LOG_SYSTEM_GROUPS"] = "grupos";
$MESS["GD_LOG_PHOTO"] = "foto";
$MESS["GD_LOG_BLOG_GROUP"] = "Relatórios";
$MESS["GD_LOG_SYSTEM"] = "sistema";
$MESS["GD_LOG_TASKS"] = "tarefas";
$MESS["GD_LOG_USER"] = "usuários";
?>