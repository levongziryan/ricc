<?
$MESS["GD_LOG_P_URL"] = "URL de la página de actualización de comunicaciones";
$MESS["GD_LOG_P_ENTITY_TYPE"] = "Tipo de suscripción";
$MESS["GD_LOG_P_ENTITY_TYPE_VALUE_USER"] = "usuarios";
$MESS["GD_LOG_P_ENTITY_TYPE_VALUE_GROUP"] = "grupos";
$MESS["GD_LOG_P_ENTITY_TYPE_VALUE_ALL"] = "todo";
$MESS["GD_LOG_P_EVENT_ID"] = "Tipo de mensaje";
$MESS["GD_LOG_P_EVENT_ID_VALUE_FORUM"] = "foros/discusiones";
$MESS["GD_LOG_P_EVENT_ID_VALUE_BLOG"] = "blogs/reportes";
$MESS["GD_LOG_P_EVENT_ID_VALUE_PHOTO"] = "foto";
$MESS["GD_LOG_P_EVENT_ID_VALUE_FILES"] = "archivos";
$MESS["GD_LOG_P_EVENT_ID_VALUE_CALENDAR"] = "calendarios";
$MESS["GD_LOG_P_EVENT_ID_VALUE_TASKS"] = "tareas";
$MESS["GD_LOG_P_EVENT_ID_VALUE_SYSTEM"] = "sistema";
$MESS["GD_LOG_P_EVENT_ID_VALUE_SYSTEM_GROUPS"] = "grupos (sólo usuarios)";
$MESS["GD_LOG_P_EVENT_ID_VALUE_SYSTEM_FRIENDS"] = "amigos (sólo usuarios)";
$MESS["GD_LOG_P_EVENT_ID_VALUE_ALL"] = "todo";
$MESS["GD_LOG_P_LOG_CNT"] = "Número de productos a mostrar";
$MESS["GD_LOG_AVATAR_SIZE"] = "Tamaño de la imagen de avatar en el flujo de actividades (conversación)";
$MESS["GD_LOG_AVATAR_SIZE_COMMENT"] = "Tamaño de la imagen de avatar en el flujo de actividades (para uso en comentarios)";
?>