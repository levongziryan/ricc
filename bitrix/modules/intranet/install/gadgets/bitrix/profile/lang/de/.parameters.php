<?
$MESS["GD_PROFILE_P_PATH_TO_PHOTO_NEW"] = "URL - Fotos hochladen";
$MESS["GD_PROFILE_P_PATH_TO_BLOG"] = "URL - Blog";
$MESS["GD_PROFILE_P_PATH_TO_CAL"] = "URL - Kalender";
$MESS["GD_PROFILE_P_PATH_TO_GROUP_NEW"] = "URL - neue Gruppe";
$MESS["GD_PROFILE_P_PATH_TO_PROFILE_EDIT"] = "URL - Profil bearbeiten";
$MESS["GD_PROFILE_P_PATH_TO_LIB"] = "Dateiseite-URL";
$MESS["GD_PROFILE_P_PATH_TO_FORUM"] = "URL - Forum";
$MESS["GD_PROFILE_P_PATH_TO_GENERAL"] = "URL - Allgemeine Seite";
$MESS["GD_PROFILE_P_PATH_TO_GROUPS"] = "URL - Meine Gruppen";
$MESS["GD_PROFILE_P_PATH_TO_MSG"] = "URL - Meine Nachrichten";
$MESS["GD_PROFILE_P_PATH_TO_PHOTO"] = "URL - Meine Fotos";
$MESS["GD_PROFILE_P_PATH_TO_SUBSCR"] = "URL - Meine Abonnements";
$MESS["GD_PROFILE_P_PATH_TO_BLOG_NEW"] = "URL - Blogbeitrag schreiben";
$MESS["GD_PROFILE_P_PATH_TO_TASK_NEW"] = "URL -  Erstellen neuer Aufgaben";
$MESS["GD_PROFILE_P_SHOW_BLOG"] = "Blog anzeigen";
$MESS["GD_PROFILE_P_SHOW_CAL"] = "Kalender anzeigen";
$MESS["GD_PROFILE_P_SHOW_LIB"] = "Dateien anzeigen";
$MESS["GD_PROFILE_P_SHOW_FORUM"] = "Forum anzeigen";
$MESS["GD_PROFILE_P_SHOW_GENERAL"] = "Allgemeines anzeigen";
$MESS["GD_PROFILE_P_SHOW_GROUPS"] = "Gruppen anzeigen";
$MESS["GD_PROFILE_P_SHOW_PHOTO"] = "Foto anzeigen";
$MESS["GD_PROFILE_P_SHOW_TASK"] = "Aufgaben anzeigen";
$MESS["GD_PROFILE_P_PATH_TO_TASK"] = "URL - Aufgaben";
$MESS["GD_PROFILE_P_PATH_TO_LOG"] = "URL - Meine Aktualisierungen";
?>