<?
$MESS["GD_PROFILE_GROUPS"] = "Groupes";
$MESS["GD_PROFILE_TASK_NEW"] = "Ajouter une tâche";
$MESS["GD_PROFILE_LOG"] = "Flux d'activités";
$MESS["GD_PROFILE_PHOTO_NEW"] = "Charger";
$MESS["GD_PROFILE_TASKS"] = "Tâches";
$MESS["GD_PROFILE_CH_PROFILE"] = "Modifier le profil";
$MESS["GD_PROFILE_CAL"] = "Calendrier";
$MESS["GD_PROFILE_MSG"] = "Mes messages";
$MESS["GD_PROFILE_SUBSCR"] = "Gestion de ma souscription";
$MESS["GD_PROFILE_BLOG_NEW"] = "Nouvelle carte de crédit";
$MESS["GD_PROFILE_GENERAL"] = "De base";
$MESS["GD_PROFILE_GROUP_NEW"] = "Créer un groupe";
$MESS["GD_PROFILE_BLOG"] = "Conversations";
$MESS["GD_PROFILE_LIB"] = "Fichiers";
$MESS["GD_PROFILE_FORUM"] = "Forum";
$MESS["GD_PROFILE_PHOTO"] = "Photo";
?>