<?
$MESS["GD_PROFILE_BLOG"] = "Blog";
$MESS["GD_PROFILE_CAL"] = "Calendário";
$MESS["GD_PROFILE_GROUP_NEW"] = "Criar Grupo";
$MESS["GD_PROFILE_CH_PROFILE"] = "Editar Perfil";
$MESS["GD_PROFILE_LIB"] = "Arquivos";
$MESS["GD_PROFILE_FORUM"] = "Fórum";
$MESS["GD_PROFILE_GENERAL"] = "Geral";
$MESS["GD_PROFILE_GROUPS"] = "Grupos";
$MESS["GD_PROFILE_MSG"] = "Minhas Mensagens";
$MESS["GD_PROFILE_SUBSCR"] = "Minha Inscrição";
$MESS["GD_PROFILE_TASK_NEW"] = "Nova Tarefa";
$MESS["GD_PROFILE_PHOTO"] = "Foto";
$MESS["GD_PROFILE_BLOG_NEW"] = "Postar ao Blog";
$MESS["GD_PROFILE_TASKS"] = "Tarefas";
$MESS["GD_PROFILE_LOG"] = "Atualizações";
$MESS["GD_PROFILE_PHOTO_NEW"] = "Enviar fotos";
?>