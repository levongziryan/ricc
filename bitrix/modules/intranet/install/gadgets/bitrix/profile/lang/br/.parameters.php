<?
$MESS["GD_PROFILE_P_PATH_TO_PHOTO_NEW"] = "URL da Página para Adicionar Foto";
$MESS["GD_PROFILE_P_PATH_TO_BLOG"] = "URL da Página do Blog";
$MESS["GD_PROFILE_P_PATH_TO_CAL"] = "URL da Página de Calendário";
$MESS["GD_PROFILE_P_PATH_TO_GROUP_NEW"] = "URL da Página de Criar Grupo";
$MESS["GD_PROFILE_P_PATH_TO_PROFILE_EDIT"] = "URL da Página Editar Perfil";
$MESS["GD_PROFILE_P_PATH_TO_LIB"] = "URL da Página de Arquivos";
$MESS["GD_PROFILE_P_PATH_TO_FORUM"] = "URL da Página de Fórum";
$MESS["GD_PROFILE_P_PATH_TO_GENERAL"] = "URL de Página de Informações Gerais";
$MESS["GD_PROFILE_P_PATH_TO_GROUPS"] = "URL da Página Meus Grupos";
$MESS["GD_PROFILE_P_PATH_TO_MSG"] = "URL da Página Minhas Mensagens";
$MESS["GD_PROFILE_P_PATH_TO_PHOTO"] = "URL da Página Minhas Fotos";
$MESS["GD_PROFILE_P_PATH_TO_SUBSCR"] = "URL da Página Minhas Inscrições";
$MESS["GD_PROFILE_P_PATH_TO_BLOG_NEW"] = "URL da Página de Post em Novo Blog";
$MESS["GD_PROFILE_P_PATH_TO_TASK_NEW"] = "URL da Página de Nova Tarefa";
$MESS["GD_PROFILE_P_SHOW_BLOG"] = "Exibir Blog";
$MESS["GD_PROFILE_P_SHOW_CAL"] = "Exibir Calendário";
$MESS["GD_PROFILE_P_SHOW_LIB"] = "Exibir Arquivos";
$MESS["GD_PROFILE_P_SHOW_FORUM"] = "Exibir Fórum";
$MESS["GD_PROFILE_P_SHOW_GENERAL"] = "Exibir Geral";
$MESS["GD_PROFILE_P_SHOW_GROUPS"] = "Exibir Grupos";
$MESS["GD_PROFILE_P_SHOW_PHOTO"] = "Exibir Fotos";
$MESS["GD_PROFILE_P_SHOW_TASK"] = "Exibir Tarefas";
$MESS["GD_PROFILE_P_PATH_TO_TASK"] = "URL da Página de Tarefas";
$MESS["GD_PROFILE_P_PATH_TO_LOG"] = "URL da Página de Atualizações";
?>