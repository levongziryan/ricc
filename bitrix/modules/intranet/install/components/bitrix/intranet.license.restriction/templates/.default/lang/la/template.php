<?
$MESS["LICENSE_RESTR_TITLE"] = "Violación de términos de EULA";
$MESS["LICENSE_RESTR_TEXT"] = "Desafortunadamente ha superado el número máximo de usuarios permitido para su Bitrix24.CRM.";
$MESS["LICENSE_RESTR_TEXT2"] = "De acuerdo con la <a href=\"https://www.bitrix24.com/eula/\" target=\"_blank\" class=\"intranet-license-restriction-link\">EULA</a> you can add <span class=\"intranet-license-restriction-text-bold\">up to #NUM# active users</span>.";
$MESS["LICENSE_RESTR_TEXT3"] = "Su Bitrix24.CRM contiene actualmente <span class=\"intranet-license-restriction-count\">#NUM#</span> users";
$MESS["LICENSE_RESTR_TEXT4"] = "Póngase en contacto con su administrador Bitrix24 para ocultar esta notificación.";
?>