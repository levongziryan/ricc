<?
$MESS["MAIN_DUMP_PARTS"] = "partes:";
$MESS["MAIN_DUMP_LOCAL"] = "locamente";
$MESS["BACKUP_ACTION_DOWNLOAD"] = "Descargar";
$MESS["BACKUP_ACTION_DELETE"] = "Eliminar";
$MESS["BACKUP_ACTION_LINK"] = "Obtener enlace de respaldo";
$MESS["BACKUP_ACTION_RESTORE"] = "Restaurar";
$MESS["BACKUP_ACTION_RENAME"] = "Renombrar";
$MESS["DUMP_DELETE_ERROR"] = "No se puede eliminar el archivo #FILE#";
$MESS["MAIN_DUMP_ERROR"] = "Error";
$MESS["MAIN_DUMP_ERR_COPY_FILE"] = "¡Error! No se puede copiar el archivo:";
$MESS["MAIN_DUMP_USE_THIS_LINK"] = "Use este enlace para migrar a otro servidor usando";
$MESS["MAIN_DUMP_ERR_FILE_RENAME"] = "Error al renombrar el archivo:";
$MESS["MAIN_DUMP_ERR_NAME"] = "El nombre de archivo puede incluir solo caracteres latinos, números, guiones y puntos.";
?>