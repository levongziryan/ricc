<?
$MESS["INTST_OUTLOOK_WARNING"] = "<small><b>Achtung!</b>Bevor Sie die Aufgaben mit Microsoft Outlook synchronisieren, empfehlen wir, die Nutzer zu synchronisieren.</small>";
$MESS["INTST_CANCEL"] = "Abbrechen";
$MESS["INTST_CLOSE"] = "Schließen";
$MESS["INTST_DELETE"] = "Löschen";
$MESS["INTST_FOLDER_NAME"] = "Ordnername";
$MESS["INTST_SAVE"] = "Speichern";
?>