<?
$MESS["INTST_OUTLOOK_WARNING"] = "<small><b>Atenção!</b> É recomendado sincronizar os contatos antes de iniciar a sincronização de tarefas com Microsoft Outlook.</small>";
$MESS["INTST_CANCEL"] = "Cancelar";
$MESS["INTST_CLOSE"] = "Fechar";
$MESS["INTST_DELETE"] = "Excluir";
$MESS["INTST_FOLDER_NAME"] = "Nome da Pasta";
$MESS["INTST_SAVE"] = "Salvar";
?>