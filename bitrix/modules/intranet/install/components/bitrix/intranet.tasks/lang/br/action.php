<?
$MESS["EC_SONET_MODULE_NOT_INSTALLED"] = "O módulo Rede Social não está instalado.";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "O módulo Blocos de Informação não está instalado.";
$MESS["INTL_TASK_NOT_FOUND"] = "A tarefa não foi encontrada.";
$MESS["INTL_TASK_INTERNAL_ERROR"] = "Erro interno.";
$MESS["INTL_FOLDER_NOT_FOUND"] = "A pasta não foi encontrada.";
$MESS["INTL_ERROR_DELETE_TASK"] = "Erro ao excluir a tarefa.";
$MESS["INTL_VIEW_NOT_FOUND"] = "A visualização não foi encontrada.";
$MESS["INTL_WRONG_VIEW"] = "A visualização está incorreta.";
$MESS["INTL_NO_VIEW_PERMS"] = "Você não tem permissão para editar visualizações comuns.";
$MESS["INTL_CAN_NOT_APPLY"] = "Você não pode aplicar esta tarefa.";
$MESS["INTL_APPLY_MESSAGE"] = "A tarefa \"#NAME#\" foi aceita.

[url=#URL_VIEW#]Visualizar detalhes[/url]";
$MESS["INTL_CAN_NOT_REJECT"] = "Você não pode rejeitar esta tarefa.";
$MESS["INTL_CAN_NOT_REJECT_OWN"] = "Você não pode rejeitar sua própria tarefa.";
$MESS["INTL_REJECT_MESSAGE"] = "A tarefa \"#NAME#\" não foi concluída.

[url=#URL_VIEW#]Visualizar detalhes[/url]";
$MESS["INTL_CAN_NOT_FINISH"] = "Você não pode concluir esta tarefa.";
$MESS["INTL_FINISH_MESSAGE"] = "A tarefa \"#NAME#\" foi concluída.

[url=#URL_VIEW#]Visualizar detalhes[/url]";
$MESS["INTL_SECURITY_ERROR"] = "Ocorreu um erro de segurança.";
$MESS["INTL_NO_FOLDER_PERMS"] = "Você não tem permissão para editar pastas.";
$MESS["INTL_EMPTY_FOLDER_NAME"] = "O nome da pasta não está especificado.";
$MESS["INTL_NO_FOLDER_ID"] = "O ID da pasta não está especificado.";
$MESS["INTL_FOLDER_DELETE_ERROR"] = "Erro ao excluir a pasta.";
?>