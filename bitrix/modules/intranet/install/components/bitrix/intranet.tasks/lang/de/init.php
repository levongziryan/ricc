<?
$MESS["INTASK_2LOG_ADD"] = "Neue Aufgabe #TITLE# wurde erstellt.";
$MESS["INTI_TASKFINISH_L"] = "Tatsächliches Datum der Fertigstellung";
$MESS["INTI_TASKFINISH"] = "Tatsächliches Datum der Fertigstellung";
$MESS["INTASK_I_ASSIGNED2ME_ACT"] = "Mir zugeteilte (aktive)";
$MESS["INTASK_I_BY_PRIORITY"] = "Mir zugeteilte (nach Priorität)";
$MESS["INTASK_I_ASSIGNED2ME_FIN"] = "Mir zugeteilte (abgeschlossene)";
$MESS["INTI_CREATED_BY"] = "Autor";
$MESS["INTI_CREATED_BY_F"] = "Autor";
$MESS["INTI_TASKSTATUS_4"] = "Abgeschlossen";
$MESS["INTASK_I_COMPLETED"] = "Abgeschlossen";
$MESS["INTASK_I_FIN"] = "Abgeschlossen";
$MESS["INTI_TASKCOMPLETE"] = "Erledigt (%)";
$MESS["INTI_TASKCOMPLETE_L"] = "Erledigt (%)";
$MESS["INTASK_I_CREATED_BY_ACT"] = "Von mir erstellte (aktive)";
$MESS["INTASK_I_CREATED_BY_FIN"] = "Von mir erstellte (abgeschlossene)";
$MESS["INTI_DATE_CREATE"] = "Erstellt";
$MESS["INTI_DATE_CREATE_F"] = "Erstellt";
$MESS["INTI_ACTIVE_DATE"] = "In Bearbeitung";
$MESS["INTI_ACTIVE_DATE_F"] = "In Bearbeitung";
$MESS["INTI_TASKSTATUS_6"] = "Zurückgestellt";
$MESS["INTI_DATE_ACTIVE_TO"] = "Ende";
$MESS["INTI_DATE_ACTIVE_TO_F"] = "Ende";
$MESS["INTI_XML_ID"] = "Externe ID";
$MESS["INTI_XML_ID_F"] = "Externe ID";
$MESS["INTI_TASKFILES"] = "Datei";
$MESS["INTI_TASKFILES_L"] = "Datei";
$MESS["INTI_IBLOCK_SECTION"] = "Ordner";
$MESS["INTI_IBLOCK_SECTION_F"] = "Ordner";
$MESS["INTASK_I_GANT"] = "Gantt-Diagramm";
$MESS["INTI_TASKPRIORITY_1"] = "Hoch";
$MESS["INTI_TASKSIZEREAL_L"] = "Aufgewendet (h)";
$MESS["INTI_TASKSIZEREAL"] = "Aufgewendet (Stunden)";
$MESS["INTI_ID"] = "ID";
$MESS["INTI_ID_F"] = "ID";
$MESS["INTI_TASKSTATUS_3"] = "In Bearbeitung";
$MESS["INTI_TASKPRIORITY_3"] = "Niedrig";
$MESS["INTI_CODE"] = "Mnemonischer Code";
$MESS["INTI_CODE_F"] = "Mnemonischer Code";
$MESS["INTI_MODIFIED_BY"] = "Geändert von";
$MESS["INTI_MODIFIED_BY_F"] = "Geändert von";
$MESS["INTI_TASKPRIORITY_2"] = "Normal";
$MESS["INTI_TASKSTATUS_1"] = "Nicht angenommen";
$MESS["INTI_TASKSTATUS_2"] = "Nicht angefangen";
$MESS["INTI_TASKALERT_L"] = "Benachrichtigen";
$MESS["INTI_TASKALERT"] = "Über Aktualisierungen benachrichtigen";
$MESS["INTI_TASKSTATUS_5"] = "Unerledigt";
$MESS["INTASK_I_PERSONAL"] = "Persönliche";
$MESS["INTI_TASKPRIORITY"] = "Priorität";
$MESS["INTI_TASKPRIORITY_L"] = "Priorität";
$MESS["INTI_TASKREMIND"] = "An die Aufgabe erinnern";
$MESS["INTI_TASKREMIND_L"] = "An die Aufgabe erinnern";
$MESS["INTI_TASKASSIGNEDTO"] = "Ausführend";
$MESS["INTI_TASKASSIGNEDTO_L"] = "Ausführend";
$MESS["INTI_DATE_ACTIVE_FROM"] = "Anfang";
$MESS["INTI_DATE_ACTIVE_FROM_F"] = "Anfang";
$MESS["INTI_TASKSTATUS"] = "Status";
$MESS["INTI_TASKSTATUS_L"] = "Status";
$MESS["INTI_DETAIL_TEXT"] = "Aufgabenbeschreibung";
$MESS["INTI_DETAIL_TEXT_F"] = "Aufgabenbeschreibung";
$MESS["INTASK_2LOG_UPDATE"] = "Die Aufgabe #TITLE# wurde geändernt";
$MESS["INTASK_2LOG_DELETE"] = "Die Aufgabe #TITLE# wurde gelöscht";
$MESS["INTI_NAME"] = "Überschrift";
$MESS["INTI_NAME_F"] = "Überschrift";
$MESS["INTASK_I_TODAY"] = "Heute";
$MESS["INTI_TIMESTAMP_X"] = "Aktualisiert";
$MESS["INTI_TIMESTAMP_X_F"] = "Aktualisiert";
$MESS["INTI_VERSION"] = "Version";
$MESS["INTI_VERSION_L"] = "Version";
$MESS["INTI_TASKSIZE_L"] = "Arbeitsumfang (St.)";
$MESS["INTI_TASKSIZE"] = "Arbeitsumfang (Stunden)";
$MESS["INTI_TASKREPORT_L"] = "Arbeitsbericht";
$MESS["INTI_TASKREPORT"] = "Arbeitsbericht";
?>