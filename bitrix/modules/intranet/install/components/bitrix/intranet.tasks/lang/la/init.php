<?
$MESS["INTI_ID"] = "ID";
$MESS["INTI_ID_F"] = "ID";
$MESS["INTI_TIMESTAMP_X"] = "Actualizado";
$MESS["INTI_TIMESTAMP_X_F"] = "Actualizado";
$MESS["INTI_NAME"] = "Título";
$MESS["INTI_NAME_F"] = "Título";
$MESS["INTI_CODE"] = "Código Nemónico";
$MESS["INTI_CODE_F"] = "Código Nemónico";
$MESS["INTI_XML_ID"] = "ID Externo";
$MESS["INTI_XML_ID_F"] = "ID Externo";
$MESS["INTI_MODIFIED_BY"] = "Modificado por";
$MESS["INTI_MODIFIED_BY_F"] = "Modificado por";
$MESS["INTI_DATE_CREATE"] = "Creado en ";
$MESS["INTI_DATE_CREATE_F"] = "Fecha de Creación";
$MESS["INTI_CREATED_BY"] = "Autor";
$MESS["INTI_CREATED_BY_F"] = "Autor";
$MESS["INTI_DATE_ACTIVE_FROM"] = "Inicio";
$MESS["INTI_DATE_ACTIVE_FROM_F"] = "Inicio";
$MESS["INTI_DATE_ACTIVE_TO"] = "Final";
$MESS["INTI_DATE_ACTIVE_TO_F"] = "Final";
$MESS["INTI_ACTIVE_DATE"] = "Actualmente en progreso";
$MESS["INTI_IBLOCK_SECTION"] = "Carpeta";
$MESS["INTI_IBLOCK_SECTION_F"] = "Carpeta";
$MESS["INTI_DETAIL_TEXT"] = "Descripción de la tarea";
$MESS["INTI_DETAIL_TEXT_F"] = "Descripción de la tarea";
$MESS["INTI_TASKPRIORITY"] = "Prioridad";
$MESS["INTI_TASKPRIORITY_1"] = "Alto";
$MESS["INTI_TASKPRIORITY_2"] = "Normal";
$MESS["INTI_TASKPRIORITY_3"] = "Bajo";
$MESS["INTI_TASKSTATUS"] = "Estado";
$MESS["INTI_TASKSTATUS_1"] = "No es aceptada";
$MESS["INTI_TASKSTATUS_2"] = "No está iniciado";
$MESS["INTI_TASKSTATUS_3"] = "En progreso";
$MESS["INTI_TASKSTATUS_4"] = "Finalizado";
$MESS["INTI_TASKSTATUS_5"] = "Pendiente";
$MESS["INTI_TASKSTATUS_6"] = "Aplazado";
$MESS["INTI_TASKCOMPLETE"] = "Finalizado (%)";
$MESS["INTI_TASKASSIGNEDTO"] = "Responsable";
$MESS["INTI_TASKALERT"] = "Notificar en actualizaciones";
$MESS["INTI_TASKSIZE"] = "Ámbito de trabajo (horas)";
$MESS["INTI_TASKSIZEREAL"] = "Horas utilizadas";
$MESS["INTI_TASKFINISH"] = "Fecha actual de Finalización";
$MESS["INTI_TASKFILES"] = "Archivo";
$MESS["INTI_TASKREPORT"] = "Informe de estado del trabajo ";
$MESS["INTI_TASKREMIND"] = "Recordar acerca de la tarea";
$MESS["INTI_VERSION"] = "Versión";
$MESS["INTASK_I_COMPLETED"] = "Finalizada";
$MESS["INTI_TASKPRIORITY_L"] = "Prioridad";
$MESS["INTI_TASKSTATUS_L"] = "Estado";
$MESS["INTI_TASKCOMPLETE_L"] = "Finalizado (%)";
$MESS["INTI_TASKASSIGNEDTO_L"] = "Responsable";
$MESS["INTI_TASKALERT_L"] = "Notificar e actualizaciones";
$MESS["INTI_TASKSIZE_L"] = "Ámbito del trabajo (horas)";
$MESS["INTI_TASKSIZEREAL_L"] = "Horas gastadas";
$MESS["INTI_TASKFINISH_L"] = "Fecha actual de finalización";
$MESS["INTI_TASKFILES_L"] = "Archivo";
$MESS["INTI_TASKREPORT_L"] = "Informe del estado de trabajo";
$MESS["INTI_TASKREMIND_L"] = "Recordar acerca de la tarea";
$MESS["INTI_VERSION_L"] = "Versión";
$MESS["INTASK_I_ASSIGNED2ME_ACT"] = "Mi asignación (activo)";
$MESS["INTASK_I_ASSIGNED2ME_FIN"] = "Mi asignación (finalidad)";
$MESS["INTASK_I_CREATED_BY_ACT"] = "Creado por mí (activo)";
$MESS["INTASK_I_CREATED_BY_FIN"] = "Creado por mí (activo)";
$MESS["INTASK_I_PERSONAL"] = "Personal";
$MESS["INTASK_I_BY_PRIORITY"] = "Mi asignación (por prioridad)";
$MESS["INTASK_I_TODAY"] = "Hoy";
$MESS["INTASK_I_FIN"] = "Finalizado";
$MESS["INTASK_I_GANT"] = "Gráfico de Gantt";
$MESS["INTASK_2LOG_ADD"] = "Una nueva tarea #TITLE# ha sido creada.";
$MESS["INTASK_2LOG_UPDATE"] = "Una nueva tarea #TITLE# ha sido cambiada.";
$MESS["INTASK_2LOG_DELETE"] = "Una nueva tarea #TITLE# ha sido borrada.";
$MESS["INTI_ACTIVE_DATE_F"] = "Actualmente en progreso";
?>