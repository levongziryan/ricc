<?
$MESS["INTR_MAIL_MANAGE_ADD_MAILBOX"] = "Agregar buzón";
$MESS["INTR_MAIL_MANAGE_ADD_MAILBOX2"] = "Agregar";
$MESS["INTR_MAIL_MANAGE_DOMAIN_ADD"] = "Conectar dominio";
$MESS["INTR_MAIL_MANAGE_DOMAIN_EDIT"] = "Configurar la conexión";
$MESS["INTR_MAIL_MANAGE_DOMAIN_EDIT2"] = "Editar ajustes";
$MESS["INTR_MAIL_MANAGE_SETTINGS"] = "Parámetros comunes";
$MESS["INTR_MAIL_MANAGE_SEARCH_TITLE"] = "Filtro";
$MESS["INTR_MAIL_MANAGE_SEARCH_PROMPT"] = "Buscar buzones";
$MESS["INTR_MAIL_MANAGE_SEARCH_BTN"] = "Encontrar";
$MESS["INTR_MAIL_MANAGE_SEARCH_CANCEL"] = "Cancelar";
$MESS["INTR_MAIL_MANAGE_SETUP"] = "Configuración";
$MESS["INTR_MAIL_MANAGE_SETUP_BLACKLIST"] = "Volver a la lista";
$MESS["INTR_MAIL_MANAGE_SETUP_BLACKLIST_HINT"] = "Correo electrónico y dominios a rechazar";
$MESS["INTR_MAIL_MANAGE_SETUP_BLACKLIST_MORE"] = "y";
$MESS["INTR_MAIL_MANAGE_SETUP_BLACKLIST_ADD"] = "Agregar más";
$MESS["INTR_MAIL_MANAGE_SETUP_BLACKLIST_PLACEHOLDER"] = "separar múltiples correos electrónicos y dominios con una coma o una nueva línea";
$MESS["INTR_MAIL_MANAGE_SETUP_ALLOW_CRM"] = "Permitir que los empleados conecten buzones al CRM";
$MESS["INTR_MAIL_MANAGE_SETUP_SAVE"] = "Guardar";
$MESS["INTR_MAIL_MANAGE_SETUP_SAVE_OK"] = "Los parámetros se han guardado correctamente.";
$MESS["INTR_MAIL_MANAGE_SETUP_SAVE_ERROR"] = "Error al guardar las preferencias";
$MESS["INTR_MAIL_MANAGE_GRID_NAME"] = "Usuarios";
$MESS["INTR_MAIL_MANAGE_GRID_EMAIL"] = "Direccion actual";
$MESS["INTR_MAIL_MANAGE_CONNECT_TITLE"] = "Integración de buzones";
$MESS["INTR_MAIL_MANAGE_CREATE_TITLE"] = "Crear buzón";
$MESS["INTR_MAIL_MANAGE_RELEASE_TITLE"] = "Desconectar buzón";
$MESS["INTR_MAIL_MANAGE_PASSWORD_TITLE"] = "Cambia la contraseña";
$MESS["INTR_MAIL_MANAGE_DELETE_TITLE"] = "Eliminar buzón";
$MESS["INTR_MAIL_MANAGE_SELECT_SUBFORM"] = "Seleccione existente";
$MESS["INTR_MAIL_MANAGE_CREATE_SUBFORM"] = "Crear nuevo";
$MESS["INTR_MAIL_MANAGE_INP_PASSWORD"] = "Crear contraseña";
$MESS["INTR_MAIL_MANAGE_INP_NEW_PASSWORD"] = "Nueva contraseña";
$MESS["INTR_MAIL_MANAGE_INP_PASSWORD2"] = "Confirmar contraseña";
$MESS["INTR_MAIL_MANAGE_DELETE"] = "Eliminar";
$MESS["INTR_MAIL_MANAGE_DELETE_WT"] = "¡Atención!";
$MESS["INTR_MAIL_MANAGE_DELETE_WARNING"] = "El buzón y todos los mensajes en él se eliminarán irreversiblemente!";
$MESS["INTR_MAIL_MANAGE_DELETE_CONFIRM"] = "¿Está seguro que desea eliminar el buzón?<br><br>Todos los mensajes se eliminarán junto con el buzón de forma irreversible!";
$MESS["INTR_MAIL_MANAGE_RELEASE_CONFIRM"] = "¿Está seguro que desea desconectar el buzón?";
$MESS["INTR_MAIL_MANAGE_ERR_AJAX"] = "Error. Vuelve a intentarlo.";
$MESS["INTR_MAIL_MANAGE_HINT"] = "Cree un buzón para cada empleado de su empresa. Utilice esta interfaz para administrar buzones corporativos: cree, adjunte o elimine buzones, así como cambie las contraseñas de los buzones.";
$MESS["INTR_MAIL_MANAGE_MODE_USER"] = "Usuarios";
$MESS["INTR_MAIL_MANAGE_MODE_MAILBOX"] = "Buzones";
$MESS["INTR_MAIL_MANAGE_INP_LOGIN"] = "Login";
$MESS["INTR_MAIL_MANAGE_INP_DOMAIN"] = "Dominio";
$MESS["INTR_MAIL_MANAGE_INP_EXIST_MB"] = "Buzones sin asignar";
$MESS["INTR_MAIL_MANAGE_INP_USER"] = "Asignar al usuario";
$MESS["INTR_MAIL_MANAGE_INP_USER_SELECT"] = "Seleccionar";
$MESS["INTR_MAIL_MANAGE_INP_USER_REPLACE"] = "Editar";
$MESS["INTR_MAIL_MANAGE_FINP_EMAIL"] = "Buzón";
$MESS["INTR_MAIL_MANAGE_SAVE_BTN"] = "Guardar";
$MESS["INTR_MAIL_MANAGE_CONNECT_BTN"] = "Conectar";
$MESS["INTR_MAIL_MANAGE_CREATE_BTN"] = "Crear";
$MESS["INTR_MAIL_MANAGE_RELEASE_BTN"] = "Desconectar";
$MESS["INTR_MAIL_MANAGE_CANCEL_BTN"] = "Cancelar";
?>