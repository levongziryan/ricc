<?
$MESS["INTRANET_USTAT_PERIOD_BUTTON_TODAY"] = "Hoje";
$MESS["INTRANET_USTAT_PERIOD_BUTTON_WEEK"] = "Semana";
$MESS["INTRANET_USTAT_PERIOD_BUTTON_MONTH"] = "Mês";
$MESS["INTRANET_USTAT_PERIOD_BUTTON_YEAR"] = "Ano";
$MESS["INTRANET_USTAT_PERIOD_TITLE"] = "Período:";
$MESS["INTRANET_USTAT_TOGGLE_PEOPLE"] = "Pessoas";
$MESS["INTRANET_USTAT_TOGGLE_COMPANY"] = "Empresa";
$MESS["INTRANET_USTAT_SECTION_SOCNET_HELP_GENERAL"] = "Número total de mensagens e comentários publicados no período de tempo determinado.";
$MESS["INTRANET_USTAT_SECTION_SOCNET_HELP_INVOLVEMENT"] = "Mostra a porcentagem de usuários que utilizando a funcionalidade de rede social no período de tempo determinado.";
$MESS["INTRANET_USTAT_SECTION_LIKES_HELP_GENERAL"] = "Número total de \"Curtir\" nas mensagens, comentários, tarefas e outros elementos na intranet, no período de tempo determinado.";
$MESS["INTRANET_USTAT_SECTION_LIKES_HELP_INVOLVEMENT"] = "Percentual de usuários que utilizaram a função \"Curtir\" no período de tempo determinado.";
$MESS["INTRANET_USTAT_SECTION_TASKS_HELP_GENERAL"] = "Número total de tarefas alteradas (criada, comentada, modificada, etc) no período de tempo determinado.";
$MESS["INTRANET_USTAT_SECTION_TASKS_HELP_INVOLVEMENT"] = "Percentual de usuários que utilizaram tarefas no período de tempo determinado.";
$MESS["INTRANET_USTAT_SECTION_IM_HELP_GENERAL"] = "Número total de mensagens instantâneas enviadas e chamadas feitas no período de tempo determinado.";
$MESS["INTRANET_USTAT_SECTION_IM_HELP_INVOLVEMENT"] = "Mostra a porcentagem de usuários que enviaram uma mensagem instantânea ou fizeram uma chamada de vídeo/áudio no período de tempo determinado.";
$MESS["INTRANET_USTAT_SECTION_DISK_HELP_GENERAL"] = "Número total de arquivos enviados ou modificados usando o Bitrix24.Drive no período de tempo determinado.";
$MESS["INTRANET_USTAT_SECTION_DISK_HELP_INVOLVEMENT"] = "Porcentagem de usuários implementando Bitrix24.Drive.";
$MESS["INTRANET_USTAT_SECTION_MOBILE_HELP_GENERAL"] = "Número total de ações no aplicativo móvel no período de tempo determinado (mensagens, mudanças de tarefas, CRM, etc.)";
$MESS["INTRANET_USTAT_SECTION_MOBILE_HELP_INVOLVEMENT"] = "Percentual de usuários que utilizaram o aplicativo móvel no período de tempo determinado.";
$MESS["INTRANET_USTAT_SECTION_CRM_HELP_GENERAL"] = "Número total de ações no CRM (modificações nos Leads, contatos, etc) em um determinado período de tempo.";
$MESS["INTRANET_USTAT_SECTION_CRM_HELP_INVOLVEMENT"] = "Porcentagem de usuários que estiveram ativos no CRM no período de tempo determinado. Este número é/pode ser limitado pelo número de usuários tendo acesso ao CRM.";
?>