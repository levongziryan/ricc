<?
$MESS["INTRANET_USTAT_TELLABOUT_SOCNET_TITLE"] = "Unidade da força de trabalho";
$MESS["INTRANET_USTAT_TELLABOUT_SOCNET_TEXT"] = "Todos nós podemos ver que a tecnologia está se desenvolvendo com velocidade incrível. As redes sociais têm aglomerado a outras formas de comunicação por causa de sua conveniência e simplicidade de troca de mensagens e meios de comunicação entre computadores e dispositivos móveis. Então, vamos usar as mesmas técnicas em nosso local de trabalho.
[IMG WIDTH=500 HEIGHT=338]https://www.bitrix24.com/images/ustat/en/socnet1.png[/IMG]
[B] Por que precisamos de uma rede social interna? [/B]
[Lista] 
[*] em primeiro lugar, precisamos consolidar nossos dados, documentos, conhecimentos e contatos em um único sistema; 
[*] isso vai simplificar e acelerar as operações regulares (é muito mais rápido do que ter um grupo grande que pesam sobre um tema usando uma linha de discussão, em vez de um e-mail em massa); 
[*] você não vai mais perder mensagens importantes; 
[*] pode oferecer ideias, compartilhar opiniões, publicar informações úteis - as mensagens serão vistas por todos (ou por aqueles que apenas você coloca no [I] Para: [/I] campo); 
[*] e, por último, você pode 'publicar' as mensagens e impulsionar a moral :)
[Lista]

Eu sugiro começar conhecendo a funcionalidade da nossa intranet sociais agora. Eu vou te dizer como fazer isso.
[VIDEO WIDTH=400 HEIGHT=300]https://www.youtube.com/watch?v=fN6BsyjOOtY[/VIDEO]
[B]Como enviar uma mensagem [/B] 

Em nossa intranet você pode compartilhar todas as informações relevantes para o trabalho. As mensagens podem ser enviadas a todos os funcionários, um grupo individual, ou para um único usuário. Os destinatários irão ver a sua mensagem no seu geral [I]Fluxo de atividades[/I].
[IMG WIDTH=500 HEIGHT=313]https://www.bitrix24.com/images/ustat/en/socnet2.png[/IMG]
Você pode anexar arquivos, imagens, adicionar links, tags, e até mesmo vídeos para suas mensagens, e você pode mencionar colegas individuais:
[IMG WIDTH=500 HEIGHT=330]https://www.bitrix24.com/images/ustat/en/socnet3.png[/IMG]
[B]Como criar ou entrar em um grupo [/B]

Grupos de trabalho são uma ferramenta útil para os projetos. Para criar o seu próprio grupo e convidar membros para isso, use o botão \"Adicionar\" (o botão verde no canto superior esquerdo). Para aderir a um grupo existente, você pode abri-lo a partir do [I]seção do menu grupos de trabalho[/I] e clique na opção de aderir dentro do grupo (conforme o caso, alguns grupos são privados).
[B]Mostrando apreciação[/B]

Não se esqueça de chamar a atenção para as realizações de seus colegas, funcionários e até mesmo executivos: clique em \"Mais\" no menu de mensagens e selecione \"Avaliação\", a seleção que você quer honrar publicamente e selecione o ícone apropriado sob o texto do mensagem.
[IMG WIDTH=500 HEIGHT=350]https://www.bitrix24.com/images/ustat/en/socnet4.png[/IMG]
[B]Avaliar Bitrix24 como uma ferramenta interna[/B] 

Além disso, você pode verificar se o seu time aprendeu a utilizar as várias ferramentas dentro Bitrix24 - basta usar o recurso de Pulso Empresa e olhar para a atividade.
[IMG WIDTH=500 HEIGHT=271]https://www.bitrix24.com/images/ustat/en/socnet5.png[/IMG]
O Pulso Empresa ajuda a manter o controle de como bom a rede social está integrada no funcionamento diário. Cada funcionário tem uma pontuação que pode ser comparado com outros colegas, o seu departamento ou toda a empresa.
[B]Se você quiser saber mais[/B]

Se você gostaria de aprender mais sobre os recursos da intranet, convidamo-os a olhar para o [url=http://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=55&LESSON_ID=5179]training course[/url].
[B]Comece agora![/B] 

Podemos começar agora a discutir esta mensagem - postar um comentário, me dê um 'like' - Eu gosto disso;) e não se esqueça de adicionar este post para seus favoritos;)";
$MESS["INTRANET_USTAT_TELLABOUT_LIKES_TITLE"] = "Curtir! Curtir! Curtir!";
$MESS["INTRANET_USTAT_TELLABOUT_LIKES_TEXT"] = "Meus amigos! Vamos parar de ser tímidos demais e começar a falar sobre todas as coisas legais que estão acontecendo na empresa. Claro, nossa intranet não é bem uma rede social, mas tem \"curtidas\" e é uma ótima maneira de garantir que você saiba o que está acontecendo.

Confira as mensagens, comentários, fotos e arquivos e \"curta\" alguns deles.

[IMG WIDTH=500 HEIGHT=341]https://www.bitrix24.com/images/ustat/en/likes1.png[/IMG]

[B]Como mostrar que você curtiu alguma coisa[/B]

Se uma postagem ou mensagem de um colega está no Fluxo de Atividades, você pode 'curti-la' usando o botão curtir naquela postagem. Curtir leva apenas um segundo, mas deixa a pessoa que escreveu saber que a contribuição foi valiosa para alguém e permite que os outros saibam que ela pode ser útil para eles.

[B]Quem curte o que[/B]

Você pode ver uma lista de usuários que 'curtiram' uma postagem ou comentário passando o mouse sobre a estrela ao lado da palavra curtir. Essas curtidas são importantes porque o recurso de pesquisa dará prioridade ao conteúdo apreciado que é relevante para qualquer pesquisa .

[IMG WIDTH=500 HEIGHT=255]https://www.bitrix24.com/images/ustat/en/likes2.png[/IMG]

[B]Por que existe um sistema de classificação com curtidas?[/B]

Pesquisa social - ou seja, resultados de pesquisa que são influenciados por curtidas, ajudam os usuários a encontrar documentos e discussões que têm sido particularmente útil.

[IMG WIDTH=500 HEIGHT=426]https://www.bitrix24.com/images/ustat/en/likes3.png[/IMG]

Mensagens com mais 'curtidas' são exibidas na seção 'Postagens mais populares' na primeira página.

[B]O que mais você pode curtir?[/B]

As mensagens e comentários não são as únicas coisas que podem ser curtidos. Você pode curtir tarefas quando são concluídas para motivar os colegas de trabalho a continuar empenhados no trabalho. Os arquivos e fotos também podem ser curtidos.


[IMG WIDTH=500 HEIGHT=322]https://www.bitrix24.com/images/ustat/en/likes4.png[/IMG]

[B]Avaliando sua atividade de curtidas[/B]

Para ver quão ativo você está usando a função \"curtir\", utilize o Pulso da Empresa e compare sua pontuação com a de seu departamento ou com a de toda a empresa.

[IMG WIDTH=500 HEIGHT=271]https://www.bitrix24.com/images/ustat/en/likes5.png[/IMG]

[B]Comece a curtir![/B]

Você pode começar a utilizar o botão 'curtir' agora, nesta mesma postagem. Não se esqueça de colocar curtir em arquivos e tarefas que você acha que são importantes ou que você tenha usado como referência. Incentive seus colegas e trabalhe um pouco mais como uma equipe - o botão curtir torna isso simples! 
Não se esqueça de curtir esta mensagem :)";
$MESS["INTRANET_USTAT_TELLABOUT_TASKS_TITLE"] = "Tarefas e Tarefas!";
$MESS["INTRANET_USTAT_TELLABOUT_TASKS_TEXT"] = "Olhe atentamente para seu ambiente de trabalho: você tem certeza que você não tem todos os números ao lado de sua lista de tarefas? ;) Ou qualquer nota no jornal ou lembretes do calendário, ou talvez bandeiras na caixa de correio? Como você pode manter-se de esquecer todas as tarefas e levá-los tudo feito?

Hoje eu convido você a experimentar uma nova ferramenta - Tarefas em Bitrix24. Esta ferramenta fornece uma maneira rápida e fácil de organizar tarefas para si ou para seus colegas. Lembre-se, o mais importante - que Bitrix24 irá enviar lembretes sobre as tarefas para se certificar que são feitas na hora.

[VIDEO WIDTH=400 HEIGHT=300]https://www.youtube.com/watch?v=WyDI_eZzRb4[/VIDEO]

[IMG WIDTH=500 HEIGHT=330]https://www.bitrix24.com/images/ustat/en/task1.png[/IMG]

Se alguém ainda não está familiarizado com as tarefas, aqui está como eles funcionam: 

[B]Como posso criar uma tarefa para mim ou para outra pessoa?[/B]

Para criar uma tarefa, clique em Adicionar> Tarefas e, na janela que aparece colocar uma descrição da tarefa, atribuí-la a uma pessoa responsável, e fixou um prazo. Se necessário, selecione os participantes adicionais e adicionar arquivos. Salve as alterações. Se for o caso, colocar a tarefa em um grupo de trabalho para que a equipe de projeto pode vê-lo.

[IMG WIDTH=500 HEIGHT=359]https://www.bitrix24.com/images/ustat/en/task2.png[/IMG]

[B]Como colaborar com as tarefas[/B]

Discutir soluções e detalhes da tarefa com seus colegas - basta adicionar um comentário a uma tarefa e ele será adicionado ao Fluxo de Atividades - e outros podem reagir instantaneamente a ela. É muito mais fácil e mais rápido do que uma discussão por e-mail ou uma reunião demorada. Além disso, toda a história da tarefa estará sempre ao seu alcance: comentários e arquivos são armazenados na tarefa.

[IMG WIDTH=500 HEIGHT=398]https://www.bitrix24.com/images/ustat/en/task3.png[/IMG]

[B]Como manter os prazos das tarefas[/B]

Para certificar-se que os prazos não são perdidos , Bitrix24 inclui um contador que lhe diz o que requer sua atenção mais imediata . Ele está disponível nas \" Minhas Tarefas\"' e na guia ' Tarefas ' em seu perfil pessoal.   

[IMG WIDTH=500 HEIGHT=340]https://www.bitrix24.com/images/ustat/en/task4.png[/IMG]

O contador mostra tarefas incompletas : tarefas em atraso , as tarefas que ainda não foram analisados, e aqueles que não tem um prazo definido. Tente manter o número do contador o mais baixo possível - estabelecer prazos razoáveis, movê-los , se necessário , e fechar todas as tarefas quando são concluídas. Quanto mais você colocar em sistematizar as suas tarefas, a mais significativa o número no contador será , e se for de zero - você vai saber que você está indo muito bem :)

[B]Como manter a noção do tempo gasto em tarefas[/B]

Tempo gasto em tarefas de monitoramento é fácil - basta adicionar a tarefa ao seu plano diário e quando você começar a trabalhar nele , mova o status da tarefa de forma adequada. Você também pode adicionar o tempo gasto no próprio formulário de tarefas.

Quando você escolhe a \"Registrar tempo gasto\"opção quando você está criando uma tarefa, a duração da tarefa serão rastreadas.

[IMG WIDTH=500 HEIGHT=446]https://www.bitrix24.com/images/ustat/en/task5.png[/IMG]

Agora, quando você clicar em 'Iniciar execução' na tarefa, o controle de tempo começa.

[IMG]https://www.bitrix24.com/images/ustat/en/task6.png[/IMG]

O tempo é contado até que você aperte o botão de pausa (ou terminar a tarefa). Se você clicar em pausa, paradas de rastreamento de tempo e você pode fazer uma pausa ou parar o seu dia de trabalho.

[IMG WIDTH=500 HEIGHT=399]https://www.bitrix24.com/images/ustat/en/task7.png[/IMG]

[B]Se você quiser saber mais[/B]

Se você quiser saber mais detalhes sobre esta ferramenta, eu recomendo o [URL=http://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=55&CHAPTER_ID=04690]online training course[/URL], que é especialmente destinado para os usuários do serviço de nuvem da Bitrix24.

[B]Avaliando o seu trabalho com as tarefas[/B]

O Pulso Empresa mostra o quanto você ou seus colegas têm tarefas integrada em sua rotina diária:

[IMG WIDTH=500 HEIGHT=269]https://www.bitrix24.com/images/ustat/en/task8.png[/IMG]

Suas idéias, comentários e impressões são bem-vindos - basta deixar um comentário a esta mensagem.

Boa sorte com as tarefas - não se esqueça de curtir esta mensagem :)

P.S. Interessante - o que será sua primeira tarefa na Bitrix24?";
$MESS["INTRANET_USTAT_TELLABOUT_IM_TITLE"] = "Colabore com prazer!";
$MESS["INTRANET_USTAT_TELLABOUT_IM_TEXT"] = "Todos: quais as ferramentas que você usa quando você debater questões urgentes com o outro? E Mail, telefone, sms, Skype, ICQ, Facebook? Ter opções é grande, mas você sabe onde encontrar as informações duas semanas mais tarde? Veja como ele pode trabalhar em Bitrix24.

[B]Chat[/B]

[IMG WIDTH=500 HEIGHT=339]https://www.bitrix24.com/images/ustat/en/im1.png[/IMG] 

Por favor, note - em nosso site tem o seu próprio chat, que já inclui todos os nossos funcionários (e sem contatos pessoais!) . Bate-papo com cada pessoa ou de cada grupo são armazenadas individualmente, há um botão de histórico , onde podem ser recuperados. Conversar pode ser 1 -pra- 1 , o grupo de bate-papo , ou você pode até mesmo fazer chamadas de voz e vídeo de esta interface !

[B]Contatos já estão lá[/B]

Começar no bate-papo é fácil, porque você não tem que adicionar qualquer um - todos os Colaboradores já estão incluídos em sua lista de contatos. Qualquer colega pode ser encontrada simplesmente digitando no campo ; guia recentes mostram suas últimas conversas , por isso é mais fácil de encontrar as pessoas que você falar para a maioria.

[IMG WIDTH = 500 HEIGHT = 339 ] https://www.bitrix24.com/images/ustat/en/im2.png [/IMG]

Observe que, se alguém está em online (isto é , disponível no momento) , o indicador ao lado de sua foto será verde. Se eles estiverem offline ou marcar-se como ocupado , será vermelho.

Tudo no bate-papo acontece em tempo real. Você pode ver que o seu amigo está digitando uma mensagem e você vai ter a confirmação de que as mensagens foram lidas.

[IMG WIDTH = 500 HEIGHT = 345 ] https://www.bitrix24.com/images/ustat/en/im3.png [/IMG]

[B] bate-papo Grupo [/B]

Se você precisa falar com várias pessoas ao mesmo tempo , você pode evitar a interrupção de jornada de trabalho de todos e apenas criar um chat em grupo . O próprio bate-papo será salvo e pode ser reaberto mais tarde , assim você não tem que criar o mesmo grupo novamente.

[IMG WIDTH = 500 HEIGHT = 349 ] https://www.bitrix24.com/images/ustat/en/im4.png [/IMG]

[B] Histórico de mensagens [/B]

Todos os chats são salvos na intranet, e a qualquer momento você pode facilmente encontrá-los , abra  \"histórico de conversa ' no canto superior direito, e procurar por uma palavra ou frase.

[IMG WIDTH = 500 HEIGHT = 345 ] https://www.bitrix24.com/images/ustat/en/im5.png [/IMG]

[B]bate-papo móvel[/B]

Chat é acessível através de aplicativos móveis , junto com os outros recursos de mensagens , tais como notificações, recebendo convites para reuniões e atualizações sobre curtidas , tarefas e comentários.

O aplicativo móvel Bitrix24 pode ser facilmente instalado em smartphones e tablets da [URL = https://itunes.apple.com/app/bitrix24/id561683423 ] a Apple AppStore[/URL] e [URL=https://play.google.com/store/apps/details?id=com.bitrix24.android]Google Play[/URL].

[IMG WIDTH = 500 HEIGHT = 461 ] https://www.bitrix24.com/images/ustat/en/im6.jpg[/IMG]

[B]chamada de vídeo[/B]

Às vezes você precisa de mais de chat - você precisa de discussão e interação. O bate-papo de vídeo e áudio na mesma interface do mensageiro proporciona exatamente isso. Não se limita a incluir até 4 usuários , mas é gratuito e uma ótima maneira de unir os trabalhadores distantes.

[IMG WIDTH = 500 HEIGHT = 305 ] https://www.bitrix24.com/images/ustat/en/im7.png [/IMG]

Você pode silenciar o seu próprio microfone , se necessário.

Vídeo chat é suportado em navegadores que suportam WebRTC , como Chrome. Se você trabalha com um navegador diferente , você pode usar vídeo chat através do aplicativo desktop.

[B]Aplicativo Desktop[/B]

Você pode ficar em contato , mesmo sem um navegador aberto , usando o applicativo Bitrix24 Desktop. Ele suporta todas as funções de mensagem , incluindo chamadas de vídeo , e tem funções de sincronização de arquivos adicionais para arquivos pessoais. Ele também aparece em sua barra de ferramentas e permite que você saiba quando novas mensagens vêm dentro

[IMG WIDTH = 500 HEIGHT = 313 ] https://www.bitrix24.com/images/ustat/en/im8.jpg[/IMG]

[Lista]
[*]Para MacOS [URL ] http://dl.bitrix24.com/b24/bitrix24_desktop.dmg [/URL]
[*] Para Windows [URL ] http://dl.bitrix24.com/b24/bitrix24_desktop.exe[/URL]
[/LIST]
Resumindo o que temos dito até agora : temos um instrumento que pode substituir e unidade de todos os tipos de mensagem, telefonemas internos , e até mesmo SMSs - todos os quais ainda estão sendo usados de uma forma ou de outra. Usando o bate-papo de vídeo e chamadas em Bitrix24 , podemos nos comunicar exatamente como necessário - dependendo da urgência da conversa.

[B]Avalie a sua atividade no messenger[/B]

Você pode ver o seu nível de atividade no messenger e compará-lo com o seu departamento e da empresa no pulso da Empresa.

[IMG WIDTH = 500 HEIGHT = 270 ] https://www.bitrix24.com/images/ustat/en/im9.png[/IMG]

[B]Para saber mais[/B]

Se você quiser mais detalhes sobre como trabalhar com o messenger , chat ou chamadas de vídeo , consulte a seguinte documentação:

[Lista]
[*][URL=https://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=55&LESSON_ID=5170]Chat[/URL]
[*] [URL = https://www.bitrix24.com/features/mobile-and-desktop-apps.php ] aplicativo desktop e mobile chat[/URL]
[/LIST]
Se você ainda tiver dúvidas, pode contactar-me pelo vídeo :)

Mantenha este post em seus favoritos até que você esteja familiarizado com as mensagens e suas características.

Vamos começar a fazer comunicação mais fácil e agradável .";
$MESS["INTRANET_USTAT_TELLABOUT_DISK_TITLE"] = "Editando documento... Em 60 segundos";
$MESS["INTRANET_USTAT_TELLABOUT_DISK_TEXT"] = "Como estamos a colaborar com o documentos agora? Como fazer dois ou mais do nosso povo co- autor de um comunicado de imprensa ou programa de treinamento ? Enviamos um ao outro e-mail , e para trás , com versões diferentes e às vezes com a mudança de grupos de destinatários . É fácil ficar confuso como este - você já deve ter notado .

Usando a unidade compartilhada local, há pouco para saber quem fez o que muda , e mais importante, você não pode obter o documento se você não está no escritório, e alguém pode ter uma nova cópia em sua própria máquina , que não tem ainda não foi baixado .

A intranet social, simplifica muito a colaboração em documentos.

[VIDEO WIDTH = 400 HEIGHT = 300 ] https://www.youtube.com/watch?v=-yepvqmabFw[/VIDEO]

[B]compartilhando um arquivo[/B]

Não há nada complicado sobre o compartilhamento de arquivos - você pode até criar um arquivo na intranet e compartilhá-lo imediatamente. Para compartilhar um arquivo com os colegas para discussão, aprimoramento , ou apenas para sua informação , você pode começar apenas iniciando um novo post no Fluxo de Atividades. Faça o upload do arquivo , ou criar um novo ( ver parte inferior direita na imagem abaixo).

[IMG WIDTH = 500 HEIGHT = 579 ] https://www.bitrix24.com/images/ustat/en/disk1.png [/IMG]

Todos os destinatários desta mensagem será capaz de editar o documento , se a caixa de seleção mostrada acima for marcado . Além disso, o documento será aberto imediatamente em uma janela de pré-visualização quando alguém clica sobre ele.

[IMG WIDTH = 500 HEIGHT = 562 ] https://www.bitrix24.com/images/ustat/en/disk2.png [/IMG]

[B]Editando um documento online [/B]

Para editar documentos , você não tem que baixar , salvar e , em seguida, fazer o upload . Tudo que você precisa fazer a partir da janela de pré-visualização é optar por editá-lo usando o Google Docs ou o MS Office Online. Você pode escolher a partir do menu suspenso.

[IMG WIDTH = 500 HEIGHT = 331 ]https://www.bitrix24.com/images/ustat/en/disk3.png[/IMG]

Para visualizar ou editar documentos desta forma, você vai precisar estar conectado no Microsoft ou Google. Estes são serviços gratuitos.

Agora você pode editar o texto e salvar as alterações .

Depois de editar o documento e salvar, o Fluxo de Atividades criará um novo comentário que afirma que uma nova versão do documento foi criado por você .

[IMG WIDTH = 500 HEIGHT = 428 ] https://www.bitrix24.com/images/ustat/en/disk4.png[/IMG]

[B]Onde salvar documentos - como configurar uma troca de arquivos [/B]

Para se certificar de que seus arquivos estão sempre disponíveis para os seus colegas , recomendamos o uso do Bitrix24.Drive . Esta é uma aplicação de sincronização de arquivos que fica dentro da área de trabalho Bitrix App . Seus arquivos locais serão sincronizadas com a biblioteca de documentos intranet Bitrix24 .

Vá para o item de menu Meu Site > Arquivos e instalar o aplicativo de desktop Bitrix24 em sua máquina local.

[IMG WIDTH = 500 HEIGHT = 194 ] https://www.bitrix24.com/images/ustat/en/disk5.png[/IMG]

O aplicativo Desktop pode ser baixado em www.bitrix24.com :
[Lista]
[*] Para MacOS [URL ] http://dl.bitrix24.com/b24/bitrix24_desktop.dmg[/URL]
[*] Para Windows [URL ] http://dl.bitrix24.com/b24/bitrix24_desktop.exe[/URL]
[/LIST]
Bitrix24.Drive irá criar uma pasta em seu gerenciador de arquivos que você pode usar como qualquer outra pasta . No entanto, o conteúdo dessa pasta será sincronizado com uma biblioteca de arquivos em Bitrix24 . Se você estiver trabalhando offline, em seguida, os arquivos serão salvos , como de costume , e assim que uma conexão com Bitrix24 é restaurada , o sistema irá sincronizar automaticamente.

[IMG WIDTH = 500 HEIGHT = 397 ] https://www.bitrix24.com/images/ustat/en/disk6.png[/IMG]

Se você salvar ou editar um arquivo nessa pasta , um arquivo idêntico em sua Minha biblioteca Drive irá aparecer. Da mesma forma, se você abrir o arquivo do seu portal e editá-lo , o arquivo em seu computador será editado imediatamente.

[IMG WIDTH = 500 HEIGHT = 358 ] https://www.bitrix24.com/images/ustat/en/disk7.png[/IMG]

[B] Como permitir ou negar o acesso a arquivos [/B]

O [I]Meu Drive[/I] biblioteca é o seu armazenamento de arquivos pessoais , e você pode colocar limites de acesso na biblioteca em todos, ou em arquivos individuais. O acesso pode ser concedido a qualquer subconjunto de seus colegas de trabalho , incluindo grupos de trabalho ou departamentos.

Você pode ajustar o acesso em cada documento , conforme necessário , e você pode criar links para download especiais para que pessoas de fora do portal pode baixar o documento dado . Esses links para download podem ser limitadas no tempo ou protegidos por senhas .

[IMG WIDTH = 500 HEIGHT = 319 ] https://www.bitrix24.com/images/ustat/en/disk8.png [/IMG]

[B] Avalie a sua atividade no Bitrix24.Drive e documentos [/B]

Para avaliar como você está ativo em trabalhar com documentos em relação ao seu departamento e da empresa como um todo, abrir o pulso da Empresa e comparar a sua classificação com os seus colegas de trabalho.

[IMG WIDTH = 500 HEIGHT = 271 ] https://www.bitrix24.com/images/ustat/en/disk9.png [/IMG]

[B] Para saber mais [/B]

Os documentos relativos a bibliotecas de documentos ea Bitrix24.Drive pode ser encontrada aqui :

[Lista]
[*][URL=https://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=55&CHAPTER_ID=05754]Working com arquivos [/URL]
[*][URL=https://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=55&LESSON_ID=5760]Working com Bitrix24.Drive [/URL]
[/LIST]
O Bitrix24.Drive eo editor on-line disponível na intranet vamos editar documentos e colaborar de forma mais rápida e facilmente. Vamos começar a usar essas ferramentas agora !

[B] Comece agora ! [/B]

Para a prática , sugiro que todos que lêem esta aberto o documento anexado a esta mensagem e adicionar o seu nome para o arquivo . Eu acho que você vai encontrá-lo mais fácil do que você jamais esperava !

Não se esqueça de adicionar este post para Favoritos :)";
$MESS["INTRANET_USTAT_TELLABOUT_MOBILE_TITLE"] = "Mobilizar imediatamente!";
$MESS["INTRANET_USTAT_TELLABOUT_MOBILE_TEXT"] = "Oi todos! Muitas vezes temos de trabalhar fora do escritório - em viagens de negócios , reuniões com clientes , feiras, etc Ter seu laptop com você o tempo todo não é possível, mas você precisa fazer para ficar em contato com seus colegas. Por isso, sugiro usar o aplicativo móvel Bitrix24 - é fácil de instalar em um smartphone ou tablet a partir do [url = https://itunes.apple.com/app/bitrix24/id561683423 ] a Apple AppStore [/url] ou [url = https://play.google.com/store/apps/detalhes?id=com.bitrix24.android ] Google Play [/url] .

[ IMG] https://www.bitrix24.com/images/ustat/en/mob1.gif [ /IMG]

Agora você pode obter atualizações, leia o Fluxo de Atividades, comentar sobre as mensagens, receber notificações e responder aos colegas, não importa onde você está.

[B] contatos colegas de trabalho estão sempre com você [/B]

Você tem a informação completa contato sempre atualizado com todos os Colaboradores da empresa no aplicativo móvel, por isso, se você precisa entrar em contato com alguém rapidamente , mesmo que seja uma pessoa que você não chamar muitas vezes , você pode encontrá-los em seu telefone e alcançá-los tão facilmente como na intranet.

[ IMG] https://www.bitrix24.com/images/ustat/en/mob3.png [/IMG ] [ IMG] https://www.bitrix24.com/images/ustat/en/mob4.png [/IMG]

Envie uma mensagem para os seus colegas e ele aparecerá na intranet e você receberá notificações sobre comentários via dispositivo móvel se você estiver fora do escritório .

[ IMG] https://www.bitrix24.com/images/ustat/en/mob5.png [/IMG ] [ IMG] https://www.bitrix24.com/images/ustat/en/mob6.png [/IMG]

[B] O que pode fazer o aplicativo móvel ? [/B]

O aplicativo móvel dá-lhe acesso a todas as principais ferramentas na intranet Bitrix24: Fluxo de Atividades, comentários, curtidas, calendários com uma relação das próximas reuniões, tarefas de pesquisa e bibliotecas de documentos. Independentemente de saber se você está em seu escritório ou não, você vai sempre estar ciente do que está acontecendo e ser capaz de se envolver em discussões em tempo real.

[IMG] https://www.bitrix24.com/images/ustat/en/mob7.png [/IMG ] [IMG] https://www.bitrix24.com/images/ustat/en/mob8.png [/IMG] [IMG] https://www.bitrix24.com/images/ustat/en/mob9.png [/IMG]

Se você estiver fora do escritório , você ainda pode definir tarefas , comentá-los e monitorar o progresso em si.

[IMG] https://www.bitrix24.com/images/ustat/en/mob10.png [/IMG ] [IMG] https://www.bitrix24.com/images/ustat/en/mob11.png [/IMG]

[B] mobile CRM [/B]

Em toda a probabilidade , a melhor parte - e trabalhar no CRM a partir do aplicativo móvel ? O aplicativo é totalmente funcional , de modo que quando você está em movimento, você pode obter informações sobre os clientes , fazer chamadas para eles, verificar o catálogo de produtos, modificar transações e até mesmo criar faturas. Basta imaginar o quanto conveniente que é - alterar o status de lead ou tratar ali mesmo no escritório do cliente !

[IMG WIDTH = 500 HEIGHT = 320] https://www.bitrix24.com/images/ustat/en/mob12.jpg [/IMG]

[B] Avalie seu trabalho no aplicativo para celular [/B]

Você pode monitorar ativamente como você usa o aplicativo móvel e comparar isso com a sua empresa ou departamento no pulso da Empresa.

[IMG WIDTH = 500 HEIGHT = 269 ] https://www.bitrix24.com/images/ustat/en/mob13.png [/IMG]

Informações adicionais sobre o aplicativo móvel está disponível em [URL = https://www.bitrix24.com/features/mobile-and-desktop-apps.php ] website Bitrix24 [/URL] .

Não se esqueça de adicionar este post aos Favoritos :)

Espero que com o aplicativo móvel que vai se tornar mais eficientes e que, mesmo quando estão fora do escritório nós vamos ser capazes de manter tudo em movimento para a frente.";
$MESS["INTRANET_USTAT_TELLABOUT_CRM_TITLE"] = "CRM: trabalhando com os clientes";
$MESS["INTRANET_USTAT_TELLABOUT_CRM_TEXT"] = "Os consumidores e clientes , quer potencial , existente ou de longa data , são os negócios. Nosso trabalho é aprender a trabalhar com cada cliente da forma mais eficiente possível. Cada compra , cada interação - tudo a ver com o cliente deve estar conectado e disponível.

O CRM em Bitrix24 é ideal para fazer exatamente isso , e podemos usá-lo sem sair do ambiente familiar da nossa intranet. E sem ter que lembrar as senhas de algum outro serviço .

[VIDEO WIDTH = 510 HEIGHT = 390 ] https://www.youtube.com/watch?v=N6K3udpfEOA [/VIDEO]

[IMG WIDTH = 500 HEIGHT = 315 ] https://www.bitrix24.com/images/ustat/en/crm1.png [/IMG]

[B] Cliente e contato de base [/B]

Todas as informações sobre os clientes ( tanto empresas como pessoas) , podem ser registrados e facilmente encontrado acima na base de contatos do CRM. Além disso, todas as interações podem ser listados e futuras interações podem ser agendadas . As compras ou transações , chamadas de \"negócios\" no CRM , podem ser mantidas individualmente e associados a cada cliente.

[IMG WIDTH = 500 HEIGHT = 347 ] https://www.bitrix24.com/images/ustat/en/crm2.png [/IMG]

Cada novo cliente que aparece deve ser adicionado ao CRM - não se esqueça de fazer isso ! Os clientes potenciais (leads) pode ser colocado à mão , levou automaticamente a partir de contato do nosso site ou formulário de feedback, via e-mail, ou importados a partir de um arquivo de dados.

[IMG WIDTH = 500 HEIGHT = 278 ] https://www.bitrix24.com/images/ustat/en/crm3.png [/IMG]

[B] Trabalhando com clientes [/B]

Lembre-se que o CRM não é apenas uma base de contatos . É uma ferramenta para ajudar a trazer e acompanhar os clientes através da compra ou o processo de tomada de decisão . Você pode planejar as chamadas e reuniões, tarefas definidas , ou escrever e-mails diretamente do CRM.

[IMG WIDTH = 500 HEIGHT = 385 ] https://www.bitrix24.com/images/ustat/en/crm4.png [/IMG]

[B] A atribuição de contatos para os associados de vendas [/B]

Podemos atribuir leva a diferentes vendedores automaticamente. Tudo o que precisamos fazer é criar um processo de negócio que conduz ' classifica ' de acordo com o que quer que as condições de que precisamos e , em seguida, define a pessoa responsável pela nossa equipe de vendas . Por exemplo, se a \"oportunidade\" na liderança é de US \$ 9999, podemos enviar que levam direto para o gerente de vendas VIP.

Ofertas do CRM deve ser criado logo que a informação sobre uma compra potencial é obtido. O negócio não só acompanha o andamento do processo de tomada de decisão, mas fornece dados para o funil de vendas e faz com que um piscar de olhos a criação da fatura.

[IMG WIDTH = 500 HEIGHT = 309 ] https://www.bitrix24.com/images/ustat/en/crm5.png [/IMG]

[B] interações contínuas com os clientes [/B]

Os dados coletados no CRM ajuda você a futuras interações com os clientes . Você também pode programar reuniões , chamadas de follow-up de telefone, e trabalhar com clientes em massa ou individualmente, desde que você pode classificar e filtrar os registros do cliente , conforme necessário. Atividades é a seção que você todas as suas futuras interações mostra eo contador ao lado dele dizer quantas requerem atenção mais cedo possível!

[IMG WIDTH = 500 HEIGHT = 171 ] https://www.bitrix24.com/images/ustat/en/crm6.png [/IMG]

O sistema permite o envio de e-mails ( a partir do seu endereço de e-mail ) e criar faturas nas interfaces muito convenientes. Clique no número de telefone de um contato para iniciar uma chamada de telefonia IP . Quando você envia uma fatura para um cliente, nota fiscal está incluído como um anexo em PDF .

[IMG WIDTH = 500 HEIGHT = 335 ] https://www.bitrix24.com/images/ustat/en/crm7.png [/IMG]

Não se esqueça que você pode analisar os seus dados a qualquer momento - o [B] funil de vendas [/B] mostra ofertas em seus vários estágios e permite filtrar ofertas apresentadas por vários critérios .

[IMG WIDTH = 500 HEIGHT = 359 ] https://www.bitrix24.com/images/ustat/en/crm8.png [/IMG]

[B] mobile CRM [/B]

By the way, para aqueles de vocês que estão na estrada e fora reuniões com clientes no campo, o aplicativo móvel tem o CRM na mesma. Você pode puxar para cima todas as informações do cliente, editar os detalhes de um acordo após negociações , ou atualizar outras informações - e , em seguida, criar uma fatura imediatamente.

[IMG WIDTH = 500 HEIGHT = 320] https://www.bitrix24.com/images/ustat/en/crm9.jpg [/IMG]

[B] Avalie seu trabalho no CRM [/B]

Use o CRM , tanto quanto você pode - as recompensas de dados de vendas ter organizado são inumeráveis. Para avaliar como sua atividade CRM compara ao seu departamento ou empresa como um todo, verificar o pulso Company.

[IMG WIDTH = 500 HEIGHT = 269 ] https://www.bitrix24.com/images/ustat/en/crm10.png [/IMG]

[B] Para mais informações [/B]

Se você quiser saber mais detalhes sobre CRM , eu recomendo o [URL=https://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=55&CHAPTER_ID=05423]online curso de formação [/URL] .

Não se esqueça de como este post! :)

[B] Vamos começar agora ! [/B]

Para começar, tente digitar algumas informações para o CRM sobre um cliente que você trabalha. Esta é uma ferramenta fundamental para aumentar a eficiência de vendas e melhorar o atendimento ao cliente !";
?>