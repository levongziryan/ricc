<?
$MESS["EC_SONET_MODULE_NOT_INSTALLED"] = "El módulo Social Network no está instalada.";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "El módulo Information Blocks no están instalados.";
$MESS["INTV_NO_IBLOCK_PERMS"] = "Usted no tiene permiso para ver el block de información de la tarea.";
$MESS["INTV_TASKS_OFF"] = "La función de las tareas está deshabilitada.";
$MESS["INTV_NO_SONET_PERMS"] = "Usted no tiene permiso para ver las tareas.";
$MESS["INTV_INTERNAL_ERROR"] = "Ocurrió un error de sistema.";
$MESS["INTV_CREATE_TITLE"] = "Nueva vista";
$MESS["INTV_EDIT_TITLE"] = "Editar Vista \"#NAME#\"";
?>