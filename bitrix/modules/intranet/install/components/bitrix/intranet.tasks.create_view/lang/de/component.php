<?
$MESS["INTV_EDIT_TITLE"] = "Ansicht \"#NAME#\" bearbeiten";
$MESS["INTV_CREATE_TITLE"] = "Neue Ansicht erstellen";
$MESS["INTV_INTERNAL_ERROR"] = "Systemfehler";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "Das Modul \"Informationsblöcke\" wurde nicht installiert.";
$MESS["EC_SONET_MODULE_NOT_INSTALLED"] = "Das Modul \"Soziales Netz\" wurde nicht installiert";
$MESS["INTV_TASKS_OFF"] = "Die Funktion \"Aufgaben\" ist deaktiviert";
$MESS["INTV_NO_SONET_PERMS"] = "Sie haben nicht genügend Rechte, Aufgaben anzusehen";
$MESS["INTV_NO_IBLOCK_PERMS"] = "Sie haben nicht genügend Rechte, Informationsblock für die Aufgaben anzuzeigen";
?>