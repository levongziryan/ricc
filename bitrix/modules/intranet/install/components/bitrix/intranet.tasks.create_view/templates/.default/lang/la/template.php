<?
$MESS["INTVT_NOT_SET"] = "(no se estableció)";
$MESS["INTVT_YES"] = "Si";
$MESS["INTVT_NO"] = "No";
$MESS["INTVT_CUR_DATE"] = "Fecha actual";
$MESS["INTVT_THIS_DATE"] = "Otra fecha:";
$MESS["INTVT_CUR_USER"] = "Usuario actual";
$MESS["INTVT_THIS_USER"] = "Otro usuario:";
$MESS["INTVT_SELECT_FORMAT"] = "Seleccionar formato de la vista";
$MESS["INTVT_START_EXIST"] = "Iniciar desde la vista ";
$MESS["INTVT_NO_EXIST"] = "No existen vistas actualmente.<br />
Este panel muestra las vistas desde el que se pueden obtener nuevas vistas.";
$MESS["INTVT_EDIT_VIEW"] = "Editar vista";
$MESS["INTVT_CREATE_VIEW"] = "Nueva Vista";
$MESS["INTVT_NAME"] = "Título";
$MESS["INTVT_PUBLIC"] = "Público";
$MESS["INTVT_PUBLIC_N"] = "Crear una vista privada";
$MESS["INTVT_PUBLIC_Y"] = "Crear una vista pública ";
$MESS["INTVT_COLUMNS"] = "Columnas";
$MESS["INTVT_COLUMNS_SHOW"] = "Mostrar";
$MESS["INTVT_COLUMNS_NAME"] = "Título";
$MESS["INTVT_COLUMNS_ORDER"] = "Pedido";
$MESS["INTVT_SORT"] = "Clasificar";
$MESS["INTVT_FIRST_SORT"] = "Pase para la primera clasificación";
$MESS["INTVT_NOT_SORT"] = "No ordenar";
$MESS["INTVT_SORT_ASC"] = "ascendente (A-Z)";
$MESS["INTVT_SORT_DESC"] = "descendente (A-Z)";
$MESS["INTVT_SECOND_SORT"] = "Pase para la segunda clasificación";
$MESS["INTVT_FILTER_FIELD"] = "Campo";
$MESS["INTVT_FILTER"] = "Filtrar";
$MESS["INTVT_FILTER_QUERY"] = "Condición";
$MESS["INTVT_FOLDERS"] = "Carpetas";
$MESS["INTVT_FOLDERS_THROW"] = "Vista plana de tareas";
$MESS["INTVT_FOLDERS_F"] = "Ver Carpeta";
$MESS["INTVT_SAVE"] = "Guardar";
$MESS["INTVT_CANCEL"] = "Cancelar";
$MESS["INTVT_STATUS_ACTIVE"] = "Activar tareas";
$MESS["INTVT_STATUS_SELECTED"] = "En el estado seleccionado";
?>