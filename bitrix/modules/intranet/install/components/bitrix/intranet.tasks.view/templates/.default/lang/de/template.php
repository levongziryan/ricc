<?
$MESS["INTDT_ACTIONS"] = "Aktionen";
$MESS["INTDT_ALL_TASKS"] = "Alle Aufgaben";
$MESS["INTDT_DC_UP"] = "In eine höhere Ebene mit einem Doppelklick wechseln";
$MESS["INTDT_DC_FOLDER"] = "Ordner mit einem Doppelklick öffnen ";
$MESS["INTDT_DC_TASK"] = "Aufgabendetails mit einem Doppelklick anzeigen";
$MESS["INTDT_NO_TASKS"] = "Aufgaben wurden nicht gefunden";
$MESS["INTDT_PERSONAL_TASKS"] = "persönliche Aufgaben";
$MESS["INTDT_SHOW"] = "Anzeigen";
$MESS["INTDT_ASSIGNED_TASKS"] = "Mir zugeteilte Aufgaben";
$MESS["INTDT_CREATED_TASKS"] = "Von mir erstellten Aufgaben";
?>