<?
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_TOTAL"] = "Uso de recursos";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_SOCNET"] = "Uso de recursos: Rede Social";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_LIKES"] = "Uso de recursos: Curtidas";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_TASKS"] = "Uso de recursos: Tarefas";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_IM"] = "Uso de recursos: Bate-papo";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_DISK"] = "Uso de função: Drive";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_MOBILE"] = "Uso de recursos: Mobile App";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_CRM"] = "Uso de recursos: CRM";
$MESS["INTRANET_USTAT_COMPANY_GRAPH_TITLE"] = "Pulso da Empresa";
$MESS["INTRANET_USTAT_COMPANY_GRAPH_ACTIVITY"] = "Ativo";
$MESS["INTRANET_USTAT_COMPANY_GRAPH_INVOLVEMENT"] = "Uso de recursos";
$MESS["INTRANET_USTAT_COMPANY_RATING_TITLE"] = "Classificação Geral";
$MESS["INTRANET_USTAT_COMPANY_ACTIVITY_TITLE"] = "Taxa<br>de atividade";
$MESS["INTRANET_USTAT_COMPANY_SECTION_INVOLVEMENT"] = "Uso de recursos";
$MESS["INTRANET_USTAT_COMPANY_SECTION_ACTIVITY"] = "Atividade";
$MESS["INTRANET_USTAT_COMPANY_SECTION_TELL_ABOUT"] = "Dizer";
$MESS["INTRANET_USTAT_COMPANY_HELP_GENERAL"] = "Pulso da Empresa é um indicador geral da atividade do usuário no portal no momento presente (composto por todos os usuários na hora anterior).  ";
$MESS["INTRANET_USTAT_COMPANY_HELP_ACTIVITY"] = "Índice de Atividade: composto por todas as atividades dos usuários em todas as funções da intranet para um determinado período de tempo. O índice mostra como os usuários estão trabalhando ativamente em diversas ferramentas.";
$MESS["INTRANET_USTAT_COMPANY_HELP_INVOLVEMENT"] = "Nível de engajamento: este é um indicador-chave que mostra o quão familiarizados os usuários se tornaram com as capacidades de Bitrix24. Ele mostra qual percentual da empresa utiliza pelo menos um quarto das ferramentas fornecidas.";
$MESS["INTRANET_USTAT_COMPANY_HELP_RATING"] = "A classificação é determinada pela média do índice de atividade individual de todos os usuários que realizaram pelo menos uma ação no período de tempo determinado.";
?>