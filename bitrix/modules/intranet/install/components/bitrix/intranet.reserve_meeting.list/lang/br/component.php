<?
$MESS["INTASK_C23_DELETE_CONF"] = "Tem certeza de que deseja excluir esta sala de reunião?";
$MESS["INTASK_C23_RESERV"] = "Reservar";
$MESS["INTASK_C23_RESERV_DESCR"] = "Reservar uma Sala da Reunião";
$MESS["INTASK_C23_DELETE"] = "Excluir";
$MESS["INTASK_C23_DELETE_DESCR"] = "Excluir Sala de Reunião";
$MESS["INAF_F_DESCRIPTION"] = "Descrição";
$MESS["INTASK_C23_EDIT"] = "Editar";
$MESS["INAF_F_FLOOR"] = "Andar";
$MESS["INAF_F_ID"] = "ID";
$MESS["INTASK_C23_EDIT_DESCR"] = "Editor da Sala de Reunião";
$MESS["INTASK_C23_GRAPH_DESCR"] = "Agenda da Sala de Reunião";
$MESS["INTASK_C36_PAGE_TITLE"] = "Salas de Reunião";
$MESS["INAF_F_PHONE"] = "Telefone";
$MESS["INTASK_C23_GRAPH"] = "Horários";
$MESS["INAF_F_PLACE"] = "Lugares";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "O módulo Blocos de Informação não está instalado.";
$MESS["EC_INTRANET_MODULE_NOT_INSTALLED"] = "O módulo Intranet não está instalado.";
$MESS["INAF_F_NAME"] = "Título";
$MESS["INTS_NO_IBLOCK_PERMS"] = "Você não tem permissão para visualizar o bloco de informações da tarefa.";
?>