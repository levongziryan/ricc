<?
$MESS["INTR_ISS_PARAM_DEPARTMENT"] = "Departmento";
$MESS["INTR_ISS_PARAM_DEPARTMENT_MINE"] = "Sólo mi oficina";
$MESS["INTR_ISS_PARAM_POST"] = "Cargo";
$MESS["INTR_ISS_PARAM_WORK_COMPANY"] = "Compañía";
$MESS["INTR_ISS_PARAM_FIO"] = "Nombre";
$MESS["INTR_ISS_PARAM_EMAIL"] = "E-mail";
$MESS["INTR_ISS_PARAM_KEYWORDS"] = "Palabras clave";
$MESS["INTR_ISS_BUTTON_SUBMIT"] = "Buscar";
$MESS["INTR_ISS_BUTTON_CANCEL"] = "Cancelar";
$MESS["INTR_ISS_PARAM_PHONE_INNER"] = "Número de teléfono Interno";
?>