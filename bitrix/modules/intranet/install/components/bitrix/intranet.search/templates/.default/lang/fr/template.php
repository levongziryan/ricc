<?
$MESS["INTR_COMP_IS_TPL_TOOLBAR_CARDDAV"] = "CardDAV";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_EXCEL"] = "Excel";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_OUTLOOK"] = "Outlook";
$MESS["INTR_COMP_IS_TPL_FILTER_AZ"] = "A-Z";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_LETTER"] = "Caractère";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_VIEW"] = "Affichage";
$MESS["INTR_COMP_IS_TPL_BANNER_OUTLOOK"] = "Vous pouvez exporter la liste des collaborateurs sous forme de contacts Microsoft Outlook";
$MESS["INTR_COMP_IS_TPL_BANNER_OUTLOOK_BUTTON"] = "Décharger";
$MESS["INTR_COMP_IS_TPL_FILTER_SIMPLE"] = "Recherche";
$MESS["INTR_COMP_IS_TPL_FILTER_ALPH"] = "Par ordre alphabétique";
$MESS["INTR_COMP_IS_TPL_FILTER_ADV"] = "Recherche élargie";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_OUTLOOK_TITLE"] = "Synchronisation de la liste des employés avec les contacts Microsoft Outlook";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_CARDDAV_TITLE"] = "Synchronisation de la liste des employés avec les applications et les périphériques qui supportent le protocole CardDAV (iPhone, iPad, etc.)";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_VIEW_LIST"] = "liste";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_VIEW_TABLE"] = "table";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_EXCEL_TITLE"] = "Exportation des résultats de recherche dans Microsoft Excel";
?>