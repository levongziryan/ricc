<?
$MESS["INTR_ISIA_PARAM_DETAIL_URL"] = "Ver detalle de la página";
$MESS["INTR_ISIN_PARAM_NUM_USERS"] = "Mostrar registros";
$MESS["INTR_PREDEF_DEPARTMENT"] = "Departamento/Oficina";
$MESS["INTR_ISIN_PARAM_PM_URL"] = "Página de Mensajes Personales";
$MESS["INTR_ISIN_PARAM_PATH_TO_CONPANY_DEPARTMENT"] = "Plantilla de la página de la Ruta del Departamento";
$MESS["INTR_ISIN_PARAM_DATE_FORMAT"] = "Formato de Fecha";
$MESS["INTR_ISIN_PARAM_DATE_TIME_FORMAT"] = "Formato de Fecha y Hora";
$MESS["INTR_ISIN_PARAM_SHOW_YEAR"] = "Mostrar Año de cumpleaños";
$MESS["INTR_ISIN_PARAM_SHOW_YEAR_VALUE_Y"] = "todo";
$MESS["INTR_ISIN_PARAM_SHOW_YEAR_VALUE_M"] = "sólo hombres";
$MESS["INTR_ISIN_PARAM_SHOW_YEAR_VALUE_N"] = "nadie";
$MESS["INTR_ISIN_PARAM_NAME_TEMPLATE"] = "Formato del nombre";
$MESS["INTR_ISIN_PARAM_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["INTR_ISIN_PARAM_SHOW_LOGIN"] = "Mostrar nombre de usuario si no requiriese los campos de nombre que estén disponibles";
?>