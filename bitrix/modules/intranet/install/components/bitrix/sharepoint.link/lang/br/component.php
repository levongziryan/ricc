<?
$MESS["SL_ERROR_NO_IBLOCK"] = "O bloco de informação não está especificado.";
$MESS["SL_ERROR_WRONG_URL"] = "Endereço do servidor inválido.";
$MESS["SL_ERROR_ACCESS_DENIED"] = "Acesso negado";
$MESS["SL_LINK_EDIT"] = "Configurações";
$MESS["SL_LINK_SYNC"] = "Sincronização";
$MESS["SL_LINK_ADD"] = "Criar Link";
?>