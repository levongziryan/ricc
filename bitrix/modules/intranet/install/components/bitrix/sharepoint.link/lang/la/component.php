<?
$MESS["SL_ERROR_NO_IBLOCK"] = "El block de información no está especificado.";
$MESS["SL_ERROR_WRONG_URL"] = "La Dirección del servidor es inválida.";
$MESS["SL_ERROR_ACCESS_DENIED"] = "Acceso Denegado.";
$MESS["SL_LINK_EDIT"] = "Configuraciones";
$MESS["SL_LINK_SYNC"] = "Sincronización";
$MESS["SL_LINK_ADD"] = "Crear Vínculo";
?>