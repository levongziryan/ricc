<?
$MESS["EC_INTRANET_MODULE_NOT_INSTALLED"] = "El modulo de intranet no esta instalado.";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "El modulo de Bloque informativo no esta instalado.";
$MESS["INTS_NO_IBLOCK_PERMS"] = "Usted no tienen permiso para ver las tareas del bloque informativo.";
$MESS["INTASK_C36_PAGE_TITLE"] = "Búsqueda de la sala de reunión";
$MESS["INAF_F_ID"] = "ID";
$MESS["INAF_F_NAME"] = "Titulo";
$MESS["INAF_F_DESCRIPTION"] = "Descripción";
$MESS["INAF_F_FLOOR"] = "Piso";
$MESS["INAF_F_PLACE"] = "Asientos";
$MESS["INAF_F_PHONE"] = "Teléfono";
?>