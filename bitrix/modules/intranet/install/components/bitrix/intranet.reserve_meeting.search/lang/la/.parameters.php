<?
$MESS["INTL_VARIABLE_ALIASES"] = "Variable del alias";
$MESS["INTL_IBLOCK_TYPE"] = "Tipo de Bloque Informativo";
$MESS["INTL_IBLOCK"] = "Bloque Informativo";
$MESS["INTL_MEETING_VAR"] = "Variable del ID de la sala de reunión";
$MESS["INTL_PAGE_VAR"] = "Variable de la Página";
$MESS["INTL_PATH_TO_MEETING"] = "Página para programar la reserva de la sala de reunión";
$MESS["INTL_PATH_TO_MEETING_LIST"] = "Página principal de la reserva de la sala de reunión";
$MESS["INTL_PATH_TO_RESERVE_MEETING"] = "Página de Reserva de Sala de Reuniones";
$MESS["INTL_PATH_TO_MODIFY_MEETING"] = "Página del editor de parámetros de la reserva de sala de reuniones";
$MESS["INTL_SET_NAVCHAIN"] = "Establecer atajos";
$MESS["INTL_USERGROUPS_MODIFY"] = "Grupos de usuarios permitidos para editar el horario de la sala de reunión";
$MESS["INTL_USERGROUPS_RESERVE"] = "Grupos de usuarios permitidos para reservar las salas de reunión";
$MESS["INTL_USERGROUPS_CLEAR"] = "Grupos de usuarios permitidos para cancelar las reservadas de las salas de reuniones";
$MESS["INTL_MEETING_ID"] = "ID de la Sala de Reunión";
?>