<?
$MESS["INAF_F_DESCRIPTION"] = "Descrição";
$MESS["INAF_F_FLOOR"] = "Andar";
$MESS["INAF_F_ID"] = "ID";
$MESS["INTASK_C36_PAGE_TITLE"] = "Pesquisar Sala de Reunião";
$MESS["INAF_F_PHONE"] = "Telefone";
$MESS["INAF_F_PLACE"] = "Lugares";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "O módulo Blocos de Informação não está instalado.";
$MESS["EC_INTRANET_MODULE_NOT_INSTALLED"] = "O módulo Intranet não está instalado.";
$MESS["INAF_F_NAME"] = "Título";
$MESS["INTS_NO_IBLOCK_PERMS"] = "Você não tem permissão para visualizar o bloco de informações da tarefa.";
?>