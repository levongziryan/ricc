<?
$MESS["INTASK_C31T_ANY"] = "(qualquer)";
$MESS["INTASK_C31T_FRESERVE"] = "Reservar";
$MESS["INTASK_C31T_FDATE"] = "Data";
$MESS["INTASK_C31T_SDATE"] = "Data";
$MESS["INTASK_C31T_FFLOOR"] = "Andar";
$MESS["INTASK_C31T_FROM"] = "de";
$MESS["INTDT_NO_TASKS"] = "Nenhuma sala de reunião";
$MESS["INTASK_C31T_DURATION"] = "Comprimento da reserva em horas";
$MESS["INTASK_C31T_SPLACE"] = "Lugares Requeridos";
$MESS["INTASK_C31T_FROOM"] = "SALA ";
$MESS["INTASK_C31T_ROOM"] = "SALA ";
$MESS["INTASK_C31T_SEARCH"] = "Buscar";
$MESS["INTASK_C31T_FPLACE"] = "Lugares";
$MESS["INTASK_C31T_STIME"] = "Tempo";
$MESS["INTASK_C31T_TO"] = "até";
$MESS["INTASK_C31T_FFREE"] = "Tempo vago";
?>