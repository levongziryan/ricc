<?
$MESS["EC_GROUP_NOT_FOUND"] = "Groupe introuvable.";
$MESS["EC_IBLOCK_ACCESS_DENIED"] = "Accès interdit";
$MESS["EC_CALENDAR_OLD_VERSION_INST"] = "Le composant utilisé 'Calendrier des événements' n'est plus actuel.  <a href='/bitrix/admin/calendar_convert.php'>Faites la conversion de données</a>
pour utiliser un nouveau module 'Calendrier des événements'.";
$MESS["EC_CALENDAR_OLD_VERSION"] = "Votre composant 'Calendrier des événements' n'est pas à jour. <a href='/bitrix/admin/module_admin.php'>Installez le nouveau 'Calendrier des événements'</a>.";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "Le module des blocs d'information n'a pas été installé.";
$MESS["EC_INTRANET_MODULE_NOT_INSTALLED"] = "Module du portail corporatif non installé.";
$MESS["EC_USER_NOT_FOUND"] = "Utilisateur introuvable";
?>