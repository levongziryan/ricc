<?
$MESS["EC_INTRANET_MODULE_NOT_INSTALLED"] = "O módulo \"Portal de Intranet\" não está instalado.";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "O módulo \"Blocos de Informação\" não está instalado.";
$MESS["EC_IBLOCK_ACCESS_DENIED"] = "Acesso negado";
$MESS["EC_USER_NOT_FOUND"] = "Usuário não encontrado";
$MESS["EC_GROUP_NOT_FOUND"] = "Grupo não encontrado";
$MESS["EC_CALENDAR_OLD_VERSION"] = "A versão atual do componente Calendário de Eventos está desatualizada. <a href=\"/bitrix/admin/module_admin.php\">Instale a nova versão</a>.";
$MESS["EC_CALENDAR_OLD_VERSION_INST"] = "O componente \"Calendário de Eventos\" que você está usando está desatualizado. Por favor <a href=\"/bitrix/admin/calendar_convert.php\">coverta seus dados</a> para a nova versão do Calendário de Eventos.";
?>