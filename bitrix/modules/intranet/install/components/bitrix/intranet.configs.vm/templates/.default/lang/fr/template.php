<?
$MESS["CONFIG_VM_HEADER_SETTINGS"] = "Paramètres";
$MESS["CONFIG_VM_SMTP_TITLE"] = "Configuration e-mail";
$MESS["CONFIG_VM_SMTP_HOST"] = "Serveur SMTP";
$MESS["CONFIG_VM_SMTP_PORT"] = "Port SMTP";
$MESS["CONFIG_VM_SMTP_USER"] = "Identifiant utilisateur SMTP";
$MESS["CONFIG_VM_SMTP_PASSWORD"] = "Mot de passe utilisateur";
$MESS["CONFIG_VM_SMTP_REPEAT_PASSWORD"] = "Confirmer le mot de passe";
$MESS["CONFIG_VM_SMTP_EMAIL"] = "Adresse e-mail";
$MESS["CONFIG_VM_SMTP_TLS"] = "Utiliser TLS";
$MESS["CONFIG_VM_SMTP_AUTH"] = "Utiliser l’authentification";
$MESS["CONFIG_VM_SMTP_ERROR"] = "Erreur lors de la configuration e-mail";
$MESS["CONFIG_VM_BXENV_UPDATE"] = "La Machine virtuelle doit être mise à jour.";
$MESS["CONFIG_VM_SAVE_SUCCESSFULLY"] = "Mes paramètres ont bien été mis à jour";
$MESS["CONFIG_VM_CERTIFICATE_TITLE"] = "Paramètres du certificat";
$MESS["CONFIG_VM_CERTIFICATE_TYPE"] = "Type de certificat";
$MESS["CONFIG_VM_CERTIFICATE_LETS_ENCRYPT_CONF"] = "Configurer le certificat Let's Encrypt";
$MESS["CONFIG_VM_CERTIFICATE_SELF_CONF"] = "Configurer le certificat SSL";
$MESS["CONFIG_VM_CERTIFICATE_LETS_ENCRYPT_EMAIL"] = "Adresse e-mail de notification";
$MESS["CONFIG_VM_CERTIFICATE_LETS_ENCRYPT_DNS"] = "Noms de DNS valides pour ce certificat";
$MESS["CONFIG_VM_CERTIFICATE_SELF_KEY_PATH"] = "Chemin d'accès de la clé privée";
$MESS["CONFIG_VM_CERTIFICATE_SELF_PATH"] = "Chemin d'accès du certificat";
$MESS["CONFIG_VM_CERTIFICATE_SELF_PATH_CHAIN"] = "Chemin d'accès de la chaîne du certificat";
$MESS["CONFIG_VM_SAVE"] = "Enregistrer";
$MESS["CONFIG_VM_START"] = "Début";
$MESS["CONFIG_VM_UPLOAD"] = "Télécharger";
$MESS["CONFIG_VM_CERTIFICATE_PROCESS"] = "Le certificat est en cours de configuration. Cela peut prendre un moment.";
?>