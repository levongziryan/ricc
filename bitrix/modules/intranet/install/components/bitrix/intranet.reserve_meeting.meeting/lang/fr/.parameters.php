<?
$MESS["INTL_P_SAN_F"] = "Dimanche";
$MESS["INTL_P_TUE_F"] = "Mardi";
$MESS["INTL_P_WEEK_HOLIDAYS"] = "Jours de repos hebdomadaires";
$MESS["INTL_USERGROUPS_CLEAR"] = "Groupes d'utilisateurs disposant des droits pour annuler une réservation de salles de réunion";
$MESS["INTL_MEETING_VAR"] = "Nom de la variable pour l'identificateur de la salle de réunion";
$MESS["INTL_ITEM_VAR"] = "Nom de la variable pour l'identifiant de la réservation";
$MESS["INTL_PAGE_VAR"] = "Nom de la variable pour la page";
$MESS["INTL_IBLOCK"] = "Bloc d'information";
$MESS["INTL_MEETING_ID"] = "Code de la salle de réunions";
$MESS["INTL_P_MON_F"] = "Lundi";
$MESS["INTL_VARIABLE_ALIASES"] = "Alias des variables";
$MESS["INTL_PATH_TO_MEETING_LIST"] = "Chemin vers la page principale de réservation des salles de réunion";
$MESS["INTL_PATH_TO_MEETING"] = "Chemin vers le graphique de la salle de réunion";
$MESS["INTL_PATH_TO_MODIFY_MEETING"] = "Chemin vers la page de modification de la salle de réunion";
$MESS["INTL_PATH_TO_VIEW_ITEM"] = "Chemin vers la page de l'affichage de réservation de la salle dé réunion";
$MESS["INTL_PATH_TO_RESERVE_MEETING"] = "Chemin vers la page de réservation des salles de réunion";
$MESS["INTL_P_FRI_F"] = "Vendredi";
$MESS["INTL_P_WEN_F"] = "Mercredi";
$MESS["INTL_P_SAT_F"] = "Samedi";
$MESS["INTL_IBLOCK_TYPE"] = "Type de bloc d'information";
$MESS["INTL_SET_NAVCHAIN"] = "Configurer le fil d'Ariane";
$MESS["INTL_P_THU_F"] = "Jeudi";
?>