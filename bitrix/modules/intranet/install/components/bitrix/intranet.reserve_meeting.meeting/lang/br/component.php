<?
$MESS["INAF_F_DESCRIPTION"] = "Descrição";
$MESS["INAF_F_FLOOR"] = "Andar";
$MESS["INAF_F_ID"] = "ID";
$MESS["INTASK_C36_PAGE_TITLE1"] = "Reserva da Sala de Reunião";
$MESS["INTASK_C36_PAGE_TITLE"] = "Agenda da Sala de Reunião";
$MESS["INAF_F_PHONE"] = "Telefone";
$MESS["INAF_F_PLACE"] = "Lugares";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "O módulo Blocos de Informação não está instalado.";
$MESS["EC_INTRANET_MODULE_NOT_INSTALLED"] = "O módulo Intranet não está instalado.";
$MESS["INAF_MEETING_NOT_FOUND"] = "A sala de reunião não foi encontrada.";
$MESS["INTASK_C25_CONFLICT1"] = "Conflito de tempo durante #TIME# entre \"#RES1#\" e \"#RES2#\"";
$MESS["INTASK_C25_CONFLICT2"] = "Conflito de tempo durante #TIME# entre \"#RES1#\" e \"#RES2#\"";
$MESS["INAF_F_NAME"] = "Título";
$MESS["INTS_NO_IBLOCK_PERMS"] = "Você não tem permissão para visualizar o bloco de informações da tarefa.";
$MESS["INTR_IRMM_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
?>