<?
$MESS["INTL_FOLDER_DELETE_ERROR"] = "Beim Löschen des Ordners ist ein Fehler aufgetreten";
$MESS["INTL_ERROR_DELETE_TASK"] = "Beim Löschen der Aufgabe ist ein Fehler aufgetreten";
$MESS["INTL_TASK_INTERNAL_ERROR"] = "Interner Fehler";
$MESS["INTL_SECURITY_ERROR"] = "Es ist ein Sicherheitsfehler aufgetreten";
$MESS["INTL_TASK_NOT_FOUND"] = "Die Aufgabe wurde nicht gefunden";
$MESS["INTL_NO_FOLDER_ID"] = "Die Ordner ID wurde nicht angegeben";
$MESS["INTL_EMPTY_FOLDER_NAME"] = "Der Ordnername wurde nicht angegeben";
$MESS["INTL_FOLDER_NOT_FOUND"] = "Der Ordner wurde nicht gefunden";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "Das Modul \"Informationsblöcke\" wurde nicht installiert.";
$MESS["EC_SONET_MODULE_NOT_INSTALLED"] = "Das Modul \"Soziales Netz\" wurde nicht installiert";
$MESS["INTL_APPLY_MESSAGE"] = "Die Aufgabe wurde angenommen \"#NAME#\"

[url=#URL_VIEW#]Details anzeigen[/url]";
$MESS["INTL_FINISH_MESSAGE"] = "Die Aufgabe \"#NAME#\" wurde abgeschlossen

[url=#URL_VIEW#]Details anzeigen[/url]";
$MESS["INTL_REJECT_MESSAGE"] = "Die Aufgabe wurde nicht angenommen \"#NAME#\"

[url=#URL_VIEW#]Details anzeigen[/url]";
$MESS["INTL_WRONG_VIEW"] = "Die Ansicht ist falsch";
$MESS["INTL_VIEW_NOT_FOUND"] = "Die Ansicht wurde nicht gefunden";
$MESS["INTL_CAN_NOT_APPLY"] = "Sie dürfen diese Aufgabe nicht annehmen";
$MESS["INTL_CAN_NOT_FINISH"] = "Sie dürfen diese Aufgabe nicht erledigen";
$MESS["INTL_CAN_NOT_REJECT"] = "Sie dürfen diese Aufgabe nicht ablehnen";
$MESS["INTL_CAN_NOT_REJECT_OWN"] = "Sie dürfen eigene Aufgabe nicht ablehnen";
$MESS["INTL_NO_VIEW_PERMS"] = "Sie haben nicht genügend Rechte, um allgemeine Ansichten zu ändern";
$MESS["INTL_NO_FOLDER_PERMS"] = "Sie haben nicht genügend Rechte, Ordner zu ändern";
?>