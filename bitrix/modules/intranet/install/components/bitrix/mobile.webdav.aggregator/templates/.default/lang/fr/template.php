<?
$MESS["WD_AG_HELP3"] = "Dans le champ <i>Disque</i> attribuez une lettre pour le dossier à connecter.";
$MESS["WD_AG_HELP4"] = "Saisissez le chemin vers la bibliothèque dans le champ <i>Dossier</i>";
$MESS["WD_AG_HELP2"] = "Sélectionnez & laquo;Outils&laquo; -> &laquo;Map Network Drive...&raquo; dans le menu de l'explorateur.";
$MESS["WD_AG_HELP6"] = "Pour avoir des informations supplémentaires sur l'utilisation sous Windows et Mac OS X accédez #STARTLINK# au guide sur le travail avec la bibliothèque des documents#ENDLINK#.";
$MESS["WD_AG_MSGTITLE"] = "Pour connecter la bibliothèque comme un player réseau:";
$MESS["WD_AG_HELP5"] = "Cliquez sur <i>C'est fait</i>.";
$MESS["WD_NO_LIBRARIES"] = "Il n'y a pas de bibliothèques de documents disponibles";
$MESS["WD_AG_HELP1"] = "Ouvrez <i>Explorateur Windows</i>.";
?>