<?
$MESS["WD_AG_MSGTITLE"] = "Para conectar la biblioteca como el drive de la red de trabajo:";
$MESS["WD_AG_HELP1"] = "Activar <i>Explorador del Windows</i>.";
$MESS["WD_AG_HELP2"] = "Seleccionar &laquo;Tools&laquo; -> &laquo;Mapa del Drive de la Red de Contactos...&raquo; in the Explorer menu.";
$MESS["WD_AG_HELP4"] = "Proporcionar la ruta de la biblioteca en el campo <i>Folder</i>:";
$MESS["WD_AG_HELP5"] = "Oprimir <i>Acabar</i>.";
$MESS["WD_NO_LIBRARIES"] = "No has bibliotecas de documentos disponibles";
$MESS["WD_AG_HELP3"] = "Seleccionar la letra del drive para conectar la carpeta.";
$MESS["WD_AG_HELP6"] = "Para más información en el uso de bibliotecas en Windows y Mac OS X por favor remítase a la biblioteca del documento #STARTLINK# de la sección de ayuda de la librería #ENDLINK#.";
?>