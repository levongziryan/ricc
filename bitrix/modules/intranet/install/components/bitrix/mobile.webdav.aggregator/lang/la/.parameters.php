<?
$MESS["WD_USER_FILE_PATH"] = "Página de los Documentos del Usuario";
$MESS["WD_GROUP_FILE_PATH"] = "Página de los documentos del Grupo de trabajo";
$MESS["WD_IBLOCK_TYPE"] = "Tipo de Block de Información";
$MESS["WD_IBLOCK_OTHER_ID"] = "Block de Información de los Documentos";
$MESS["WD_IBLOCK_GROUP_ID"] = "Block de Información de los Documentos del Grupo de Trabajo";
$MESS["WD_IBLOCK_USER_ID"] = "Block de Información de los Documentos del Usuario";
$MESS["WD_NAME_TEMPLATE"] = "Formato del Nombre";
$MESS["WD_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
?>