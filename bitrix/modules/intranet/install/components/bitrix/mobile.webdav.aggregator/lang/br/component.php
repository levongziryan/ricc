<?
$MESS["WD_WD_MODULE_IS_NOT_INSTALLED"] = "O módulo Biblioteca de Documentos não está instalado.";
$MESS["WD_IB_MODULE_IS_NOT_INSTALLED"] = "O módulo \"Blocos de Informação\" não está instalado.";
$MESS["WD_SN_MODULE_IS_NOT_INSTALLED"] = "O módulo Rede Social não está instalado.";
$MESS["WD_SHARED"] = "Documentos Compartilhados";
$MESS["WD_GROUP"] = "Grupos de Trabalho";
$MESS["WD_PRIVATE"] = "Documentos Privados";
$MESS["WD_USER"] = "Documentos do usuário";
$MESS["WD_ROOT"] = "Raiz";
$MESS["WD_IB_GROUP_IS_NOT_FOUND"] = "O bloco de informação de documentos do grupo de trabalho não foi encontrado.";
$MESS["WD_IB_USER_IS_NOT_FOUND"] = "O bloco de informações documentos de usuário não foi encontrado.";
$MESS["WD_DAV_INSUFFICIENT_RIGHTS"] = "Você não tem permissão para esta ação.";
$MESS["WD_DAV_UNSUPORTED_METHOD"] = "Este método não é suportado.";
$MESS["WD_GROUP_SECTION_FILES_NOT_FOUND"] = "A seção de documentos do grupo não foi encontrada.";
$MESS["WD_USER_SECTION_FILES_NOT_FOUND"] = "A seção de documentos do usuário não foi encontrada.";
$MESS["WD_USER_NOT_FOUND"] = "O usuário não foi encontrado.";
$MESS["WD_NOT_SEF_MODE"] = "URL do Search Engine Friendly (SEF) não está habilitada nos parâmetros de componente.";
$MESS["WD_SOCNET_LANG_NOT_FOUND"] = "Os componentes de rede social estão faltando. Verifique o caminho raiz do servidor web nas configurações do site.";
$MESS["WD_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
?>