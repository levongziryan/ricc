<?
$MESS["UPDATES_ACTIVATE_SITE_TEXT"] = "Si vous n'êtes pas inscrit sur <a href=\"http://www.bitrixsoft.com\" target=\"_blank\">www.bitrixsoft.com</a>, veuillez vous assurer que l'option \"Créer un utilisateur\"
 <br/>soit bien cochée, et saisissez vos informations (nom et prénoms, identifiant et mot de passe) 
<br/>dans les champs du formulaire. Une inscription sur www.bitrixsoft.com vous permet d'utiliser le
<br/><a href=\"http://www.bitrixsoft.com/support/\" target=\"_blank\">service technique</a> et le <a href=\"http://www.bitrixsoft.com/support/forum/\" target=\"_blank\">forum privé</a> pour résoudre vos problèmes et obtenir
<br/>des réponses à vos questions.";
$MESS["UPDATES_ACTIVATE_GENERATE_USER"] = "Créer un utilisateur sur www.bitrixsoft.com";
$MESS["UPDATES_ACTIVATE_GENERATE_USER_NO"] = "J'ai déjà un compte utilisateur et je voudrait l'utiliser pour accéder aux sections Assistance technique et Téléchargement ;";
$MESS["SUP_REGISTERED"] = "Inscrit pour";
$MESS["SUP_LICENSE_KEY"] = "Clé de licence";
$MESS["SUP_EDITION"] = "Édition";
$MESS["SUP_SITES"] = "Nombre de sites";
$MESS["SUP_USERS"] = "Utilisateurs maximum";
$MESS["SUP_CURRENT_NUMBER_OF_USERS"] = "; décompte actuel d'utilisateurs : #NUM#";
$MESS["SUP_USERS_IS_NOT_LIMITED"] = "Votre licence n'a pas de limite d'utilisateurs.";
$MESS["SUP_CURRENT_NUMBER_OF_USERS1"] = "Utilisateurs actifs : #NUM#.";
$MESS["UPDATES_LICENSE_TITLE"] = "Licence Bitrix24";
$MESS["UPDATES_LICENSE_KEY"] = "Clé de licence";
$MESS["UPDATES_LICENSE_SAVE"] = "Enregistrer";
$MESS["UPDATES_LICENSE_ACTIVATE"] = "Activer";
$MESS["UPDATES_ACTIVATE_LICENSE_TITLE"] = "Activation de la clé de licence";
$MESS["UPDATES_ACTIVATE_NAME"] = "Nom complet du propriétaire (société ou individu)";
$MESS["UPDATES_ACTIVATE_SITE_URL"] = "Tous les domaines seront gérés<br/>par cette instance du Gestionnaire de sites Bitrix, y compris les domaines de test";
$MESS["UPDATES_ACTIVATE_PHONE"] = "Téléphone du propriétaire de la copie du produit";
$MESS["UPDATES_ACTIVATE_EMAIL"] = "Licence et utilisation de l'e-mail de contact";
$MESS["UPDATES_ACTIVATE_CONTACT_PERSON"] = "Le contact responsable de cette copie du produit";
$MESS["UPDATES_ACTIVATE_CONTACT_EMAIL"] = "E-mail du contact";
$MESS["UPDATES_ACTIVATE_CONTACT_PHONE"] = "Téléphone du contact";
$MESS["UPDATES_ACTIVATE_CONTACT_INFO"] = "Autres contacts importants";
$MESS["UPDATES_ACTIVATE_USER_NAME"] = "Prénom";
$MESS["UPDATES_ACTIVATE_USER_LAST_NAME"] = "Nom";
$MESS["UPDATES_ACTIVATE_USER_LOGIN_A"] = "Identifiant (3 caractères au minimum)";
$MESS["UPDATES_ACTIVATE_USER_PASSWORD"] = "Mot de passe (6 caractères au minimum)";
$MESS["UPDATES_ACTIVATE_USER_PASSWORD_CONFIRM"] = "Confirmer le mot de passe";
$MESS["UPDATES_ACTIVATE_USER_EMAIL"] = "E-mail";
$MESS["UPDATES_COUPON_TITLE"] = "Activater le coupon";
$MESS["UPDATES_COUPON_KEY"] = "Saisissez le code du coupon";
$MESS["UPDATES_COUPON_SUCCESS"] = "Le coupon a bien été appliqué";
$MESS["UPDATES_ACTIVATE_SUCCESS"] = "La clé a bien été activée";
$MESS["SUP_ACTIVE_TITLE"] = "La clé de licence est valide";
$MESS["SUP_ACTIVE_PERIOD_TO"] = "jusqu'au #DATE_TO#";
$MESS["UPDATES_ACTIVATE_SITE_URL_NEW"] = "Tous les domaines seront gérés<br/>par cette instance du système, y compris les domaines de test";
?>