<?
$MESS["SUPA_AERR_NAME"] = "Por favor, introduzca el nombre de la compañía propietaria de la clave";
$MESS["SUPA_AERR_EMAIL"] = "Por favor, introduzca el e-mail de contacto";
$MESS["SUPA_AERR_EMAIL1"] = "Por favor, compruebe que la dirección de correo electrónico es correcta";
$MESS["SUPA_AERR_URI"] = "Por favor, introduzca la dirección del sitio que se utilizará con la clave";
$MESS["SUPA_AERR_FNAME"] = "Por favor, introduzca el nombre de usuario de una cuenta en <ahref=\"http://www.bitrixsoft.ru\">www.bitrixsoft.ru</a> se creará";
$MESS["SUPA_AERR_LNAME"] = "Por favor, introduzca el apellido del usuario de una cuenta en <ahref=\"http://www.bitrixsoft.ru\">www.bitrixsoft.ru</a> sera creará";
$MESS["SUPA_AERR_LOGIN"] = "Por favor ingrese el login para usarlo en <a href=\"http://www.bitrixsoft.ru\">www.bitrixsoft.ru</a>";
$MESS["SUPA_AERR_LOGIN1"] = "Nombre de login en <a href=\"http://www.bitrixsoft.ru\">www.bitrixsoft.ru</a> debe contener al menos 3 símbolos";
$MESS["SUPA_AERR_PASSW"] = "Por favor, introduzca la contraseña para su uso en <a href=\"http://www.bitrixsoft.ru\">www.bitrixsoft.ru</a>";
$MESS["SUPA_AERR_PASSW_CONF"] = "La contraseña y la confirmación de contraseña son diferentes.";
$MESS["SUPA_AERR_PHONE"] = "Por favor, escriba el número de teléfono del propietario de la copia del producto";
$MESS["SUPA_AERR_CONTACT_PERSON"] = "Por favor, escriba el nombre y el apellido de una persona de contacto";
$MESS["SUPA_AERR_CONTACT_EMAIL"] = "Por favor escriba la dirección de correo electrónico de una persona de contacto";
$MESS["SUPA_AERR_CONTACT_EMAIL1"] = "Por favor, compruebe que la dirección de e-mail de la persona de contacto sea correcta";
$MESS["SUPA_AERR_CONTACT_PHONE"] = "Por favor, escriba el número de teléfono de una persona de contacto";
$MESS["SUPA_ACE_CPN"] = "No se proporcionó el código del cupón.";
$MESS["SUPA_ACE_ACT"] = "Error al intentar activar el cupón";
?>