<?
$MESS["INTRANET_USTAT_USER_GRAPH_DEPT"] = "En moyenne dans le département %DEPT%";
$MESS["INTRANET_USTAT_USER_GRAPH_COMPANY"] = "En moyenne dans l'entreprise";
$MESS["INTRANET_USTAT_USER_GRAPH_ME"] = "Moi";
$MESS["INTRANET_USTAT_USER_GRAPH_TITLE"] = "Pouls de la société";
$MESS["INTRANET_USTAT_USER_RATING_TITLE"] = "Mes <br>évaluations";
$MESS["INTRANET_USTAT_USER_RATING_TITLE_OTHER"] = "Classement";
$MESS["INTRANET_USTAT_USER_ACTIVITY_TITLE"] = "Mon indice<br>d'activité";
$MESS["INTRANET_USTAT_USER_ACTIVITY_TITLE_OTHER"] = "Index <br> de l'activité";
$MESS["INTRANET_USTAT_USER_ACTIVITY_DEPT_TITLE"] = "Moyenne<br>par service";
$MESS["INTRANET_USTAT_USER_ACTIVITY_COMPANY_TITLE"] = "Moyen(ne) <br> pour toute la compagnie";
$MESS["INTRANET_USTAT_USER_SECTION_ACTIVITY"] = "Actif(ve)";
$MESS["INTRANET_USTAT_USER_SECTION_MAX_ACTIVITY"] = "Max.";
$MESS["INTRANET_USTAT_USER_SECTION_TELL_ABOUT"] = "Dire";
$MESS["INTRANET_USTAT_USER_HELP_GENERAL"] = "<b>Pouls d'activité</b> - indicateur global de votre activité dans 'Bitrix24' pour une période sélectionnée.";
$MESS["INTRANET_USTAT_USER_HELP_RATING"] = "Votre position au <b>classement d'activité</b> parmi tous les employés de la compagnie qui ont utilisé au moins une fois les instruments ''Bitrix24' au cours de la période sélectionnée.";
$MESS["INTRANET_USTAT_USER_HELP_ACTIVITY"] = "Somme de différentes actions que vous avez effectuées sur le portail en utilisant l'un de sept outils pour une période de temps sélectionnée.";
$MESS["INTRANET_USTAT_USER_HELP_ACTIVITY_DEPT"] = "Une somme moyenne des opérations effectuées par les employés de votre département sur le Portail en utilisant un des sept instruments pendant une période sélectionnée du temps. <br><br> <b>Un index moyen par département permet</b> de comparer son activité avec celle de tout le département.";
$MESS["INTRANET_USTAT_USER_HELP_ACTIVITY_COMPANY"] = "Montant moyen d'actions accomplies par les employés de toute l'entreprise sur le portail en utilisant l'un de sept outils pour une période de temps choisue.<br><br> <b>L'indice moyen pour l'entreprise</b>permet de comparer son activité et l'activité de son département avec celle de l'entreprise.";
?>