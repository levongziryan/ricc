<?
$MESS["INTRANET_USTAT_USER_GRAPH_DEPT"] = "Promedio del dpto. %DEPT%";
$MESS["INTRANET_USTAT_USER_GRAPH_COMPANY"] = "Promedio de la compañía";
$MESS["INTRANET_USTAT_USER_GRAPH_ME"] = "Yo";
$MESS["INTRANET_USTAT_USER_RATING_TITLE"] = "Mi<br>calificación";
$MESS["INTRANET_USTAT_USER_RATING_TITLE_OTHER"] = "Calificación";
$MESS["INTRANET_USTAT_USER_ACTIVITY_TITLE"] = "Mi<br>tasa de actividad";
$MESS["INTRANET_USTAT_USER_ACTIVITY_TITLE_OTHER"] = "Tasa<br>Actividad";
$MESS["INTRANET_USTAT_USER_ACTIVITY_DEPT_TITLE"] = "Promedio por<br/>departamento";
$MESS["INTRANET_USTAT_USER_ACTIVITY_COMPANY_TITLE"] = "Promedio por<br/>compañía";
$MESS["INTRANET_USTAT_USER_SECTION_ACTIVITY"] = "Activo";
$MESS["INTRANET_USTAT_USER_SECTION_MAX_ACTIVITY"] = "Máx.";
$MESS["INTRANET_USTAT_USER_SECTION_TELL_ABOUT"] = "Informar";
$MESS["INTRANET_USTAT_USER_GRAPH_TITLE"] = "Pulso de la compañía";
$MESS["INTRANET_USTAT_USER_HELP_GENERAL"] = "<b>Pulso de la Compañía</b> permite hacer visible las actividades que usted hizo en Bitrix24 para un período de tiempo especificado.";
$MESS["INTRANET_USTAT_USER_HELP_RATING"] = "Su posición en el <b>índice de actividad</b> resumen el listado de todos los empleados que han utilizado Bitrix24 por lo menos una vez durante el período de tiempo especificado.";
$MESS["INTRANET_USTAT_USER_HELP_ACTIVITY"] = "Valor total de las actividades, para el período de tiempo especificado, que usted hizo en Bitrix24 utilizando una de las siete herramientas disponibles.";
$MESS["INTRANET_USTAT_USER_HELP_ACTIVITY_DEPT"] = "Valor promedio de las acciones que su departamento hizo en Bitrix24 utilizando una de las siete herramientas disponibles para el período de tiempo especificado. <br><br> <b> Utilice el valor promedio del departamento para ver cómo su actividad se compara con las de los demás.";
$MESS["INTRANET_USTAT_USER_HELP_ACTIVITY_COMPANY"] = "Valor promedio de las acciones que su compañía hizo en Bitrix24 utilizando una de las siete herramientas disponibles para el período de tiempo especificado. <br><br> <b> Utilice el valor promedio de la compañía para ver cómo su actividad se compara con la de toda la empresa.";
?>