<?
$MESS["INTR_ISBN_PARAM_NUM_USERS"] = "Exibir Usuários";
$MESS["INTR_ISBN_PARAM_DETAIL_URL"] = "Página Perfil de Usuário";
$MESS["INTR_ISH_PARAM_NAME_TEMPLATE"] = "Formato de Nome";
$MESS["INTR_ISH_PARAM_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["INTR_ISH_PARAM_SHOW_LOGIN"] = "Exibir Nome de Login se nenhum campo de nome de usuário obrigatório está disponível";
$MESS["INTR_ISH_PARAM_DATE_TIME_FORMAT"] = "Formato de Data e Hora";
$MESS["INTR_ISH_PARAM_SHOW_YEAR"] = "Exibir Ano de Nascimento";
$MESS["INTR_ISH_PARAM_SHOW_YEAR_VALUE_Y"] = "Todos";
$MESS["INTR_ISH_PARAM_SHOW_YEAR_VALUE_M"] = "apenas homens";
$MESS["INTR_ISH_PARAM_SHOW_YEAR_VALUE_N"] = "ninguém";
$MESS["INTR_ISH_PARAM_PM_URL"] = "Página de Mensagem Pessoal";
$MESS["INTR_ISH_PARAM_PATH_TO_CONPANY_DEPARTMENT"] = "Caminho de Página de Modelo de Departamento";
$MESS["INTR_ISH_PARAM_DATE_FORMAT"] = "Formato de Data";
$MESS["INTR_ISH_PARAM_DATE_FORMAT_NO_YEAR"] = "Formato de Data (sem o ano)";
$MESS["INTR_ISH_PARAM_PATH_TO_VIDEO_CALL"] = "Página chamada de vídeo";
?>