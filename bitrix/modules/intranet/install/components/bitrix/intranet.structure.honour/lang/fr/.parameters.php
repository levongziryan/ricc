<?
$MESS["INTR_ISH_PARAM_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["INTR_ISH_PARAM_SHOW_YEAR_VALUE_Y"] = "tous";
$MESS["INTR_ISBN_PARAM_NUM_USERS"] = "Nombre d'utilisateurs affichables";
$MESS["INTR_ISH_PARAM_SHOW_YEAR_VALUE_N"] = "personne";
$MESS["INTR_ISH_PARAM_NAME_TEMPLATE"] = "Affichage du nom";
$MESS["INTR_ISH_PARAM_SHOW_YEAR"] = "Afficher l'année de naissance";
$MESS["INTR_ISH_PARAM_SHOW_LOGIN"] = "Afficher le nom d'utilisateur si le nom n'est pas spécifié";
$MESS["INTR_ISH_PARAM_PATH_TO_VIDEO_CALL"] = "Page de l'appel vidéo";
$MESS["INTR_ISH_PARAM_PM_URL"] = "Page de l'envoi du message privé";
$MESS["INTR_ISBN_PARAM_DETAIL_URL"] = "Modèle d'un lien au profil d'utilisateur";
$MESS["INTR_ISH_PARAM_SHOW_YEAR_VALUE_M"] = "seulement aux hommes";
$MESS["INTR_ISH_PARAM_DATE_FORMAT"] = "Format d'affichage de la date";
$MESS["INTR_ISH_PARAM_DATE_FORMAT_NO_YEAR"] = "Format d'affichage de la date sans indication de l'année";
$MESS["INTR_ISH_PARAM_DATE_TIME_FORMAT"] = "Format d'affichage de la date et de l'heure";
$MESS["INTR_ISH_PARAM_PATH_TO_CONPANY_DEPARTMENT"] = "Modèle de chemin d'accès à la page d'une section";
?>