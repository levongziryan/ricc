<?
$MESS["INTR_EMP_WINDOW_TITLE"] = "Selecionar colaborador";
$MESS["INTR_EMP_WINDOW_CLOSE"] = "Fechar";
$MESS["INTR_EMP_WAIT"] = "Carregando...";
$MESS["INTR_EMP_SUBMIT"] = "Selecionar";
$MESS["INTR_EMP_SUBMIT_TITLE"] = "Escolha o colaborador selecionado";
$MESS["INTR_EMP_CANCEL"] = "Cancelar";
$MESS["INTR_EMP_CANCEL_TITLE"] = "Cancelar Seleção de colaboradors";
$MESS["INTR_EMP_HEAD"] = "chefe do departamento";
$MESS["INTR_EMP_LAST"] = "Itens Recentes";
$MESS["INTR_EMP_NOTHING_FOUND"] = "a pesquisa retornou nenhum resultado";
$MESS["INTR_EMP_SEARCH"] = "buscar colaborador";
$MESS["INTR_EMP_EXTRANET"] = "Extranet";
?>