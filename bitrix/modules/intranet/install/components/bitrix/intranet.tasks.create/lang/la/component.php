<?
$MESS["EC_SONET_MODULE_NOT_INSTALLED"] = "El módulo Social Network no está instalado.";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "El módulo Information Blocks no está instalado.";
$MESS["INTE_NO_IBLOCK_PERMS"] = "Usted no tiene permisos para ver el block de información de la tarea.";
$MESS["INTE_TASKS_OFF"] = "La función de las tareas está desactivada.";
$MESS["INTE_NO_SONET_PERMS"] = "Usted no tiene permiso para ver las tareas.";
$MESS["INTE_NO_CREATE_PERMS"] = "Usted no tiene permiso para ver las tareas.";
$MESS["INTE_INTERNAL_ERROR"] = "Un error de sistema ha ocurrido.";
$MESS["INTE_NO_STATUS_PROP"] = "La propiedad del 'Estado' del block de información no fue encontrada.";
$MESS["INTE_NO_ASSIGNEDTO_PROP"] = "La propiedad del 'Persona asignada' del block de información no fue encontrada.";
$MESS["INTE_WRONG_STATUS_VAL"] = "La propiedad del 'Estado' de valores del block de información no fue encontrada.";
$MESS["INTE_CREATE_TITLE"] = "Nueva Tarea";
$MESS["INTE_EDIT_TITLE"] = "Editar Tarea ##ID#";
$MESS["INTE_VIEW_TITLE"] = "Tarea ##ID#";
$MESS["INTE_TASK_NOT_FOUND"] = "La tarea no fue encontrada.";
$MESS["INTE_YES"] = "Si";
$MESS["INTE_NO"] = "No";
$MESS["INTE_NO_ASSIGNEDTO_USER"] = "La tarea no está asignada a ningún usuario.";
$MESS["INTE_NO_VIEW_PERMS"] = "Usted no tiene permiso para ver esta tarea.";
$MESS["INTE_NO_APPLY_REJECT_PERMS"] = "Usted no puede aceptar o rechazar esta tarea.";
$MESS["INTE_NO_REJECT_PERMS"] = "Usted no puede rechazar esta tarea.";
$MESS["INTE_APPLY_MESSAGE"] = "La tarea \"#NAME#\" ha sido aceptada.\\r\\n\\r\\n[url=#URL_VIEW#]Ver detalles[/url]";
$MESS["INTE_REJECT_MESSAGE"] = "La tarea \"#NAME#\" ha sido aceptada.\\r\\n\\r\\n[url=#URL_VIEW#]Ver detalles[/url]";
$MESS["INTE_ERROR_SAVE_TASK"] = "Error al guardar la tarea.";
$MESS["INTE_WRONG_ASSIGNEDTO_USER"] = "Usted no tiene asignada una tarea para este usuario.";
$MESS["INTE_ADD_TASK_MESSAGE"] = "Una nueva tarea \"#NAME#\" ha sido creada.\\r\\n\\r\\n[url=#URL_VIEW#]Ver detalles de la tarea[/url]\\r\\n[url=#URL_EDIT#]Edit task[/url]";
$MESS["INTE_ADD_TASK_MESSAGE1"] = "Una nueva tarea ha sido creada: \"#NAME#\"
Prioridad: #TASK_PRIORITY#
Fecha de vencimiento: #TASK_DATES#

[url=#URL_VIEW#]Ver detalles[/url]
[url=#URL_EDIT#]Editar tarea[/url]";
$MESS["INTE_EDIT_TASK_MESSAGE"] = "Los parámetros de la tarea \"#NAME#\" han sido cambiados.\\r\\n\\r\\n[url=#URL_VIEW#]Ver detalles de la tarea[/url]";
$MESS["INTE_ADD_TASK_DATES_EMPTY"] = "no se estableció";
$MESS["INTASK_TO_DATE_TLP"] = "antes #DATE#";
$MESS["INTASK_FROM_DATE_TLP"] = "desde #DATE#";
$MESS["INTAST_T6654_LOG"] = "Prioridad: #PRIORITY#\\r\\nDeadline: #TIME#\\r\\nResponsible: #RESP#";
$MESS["INTE_TASKS_EMPTY_FIELD"] = "El campo \"#FIELD#\" está vacío.";
$MESS["INTE_TASKS_RESPONSIBLE"] = "Responsable";
$MESS["INTE_TASKS_PERMS_EVENT"] = "Usted no tiene permiso para hacer funcionar esta acción.";
$MESS["INTE_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["INTE_EDIT_TASK_MESSAGE1"] = "Los parámetros del \"#NAME#\" de la tarea ha sido cambiado.
Prioridad: #TASK_PRIORITY#
Fecha de vencimiento: #TASK_DATES#

[url=#URL_VIEW#]Ver detalles[/url]";
$MESS["INTE_FINISH_MESSAGE"] = "La tarea \"#NAME#\" ha finalizado.

[url=#URL_VIEW#]Ver detalles[/url]";
?>