<?
$MESS["EC_SONET_MODULE_NOT_INSTALLED"] = "O módulo Rede Social não está instalado.";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "O módulo Blocos de Informação não está instalado.";
$MESS["INTE_NO_IBLOCK_PERMS"] = "Você não tem permissão para visualizar o bloco de informações da tarefa.";
$MESS["INTE_TASKS_OFF"] = "O recurso Tarefas está desabilitado.";
$MESS["INTE_NO_SONET_PERMS"] = "Você não tem permissão para visualizar tarefas.";
$MESS["INTE_NO_CREATE_PERMS"] = "Você não tem permissão para criar tarefas.";
$MESS["INTE_INTERNAL_ERROR"] = "Ocorreu um erro de sistema.";
$MESS["INTE_NO_STATUS_PROP"] = "A propriedade do bloco de informação 'Status' não foi encontrada.";
$MESS["INTE_NO_ASSIGNEDTO_PROP"] = "A propriedade do bloco de informação 'Pessoa Atribuída' não foi encontrada.";
$MESS["INTE_WRONG_STATUS_VAL"] = "Os valores de propriedade do bloco de informação 'Status' foi alterado.";
$MESS["INTE_CREATE_TITLE"] = "Nova Tarefa";
$MESS["INTE_EDIT_TITLE"] = "Editar Tarefa ##ID#";
$MESS["INTE_VIEW_TITLE"] = "Tarefa ##ID#";
$MESS["INTE_TASK_NOT_FOUND"] = "A tarefa não foi encontrada.";
$MESS["INTE_YES"] = "Sim";
$MESS["INTE_NO"] = "Não";
$MESS["INTE_NO_ASSIGNEDTO_USER"] = "A tareda não está atribuída a usuário algum.";
$MESS["INTE_NO_VIEW_PERMS"] = "Você não tem permissão para visualizar esta tarefa.";
$MESS["INTE_NO_APPLY_REJECT_PERMS"] = "Você não pode aceitar ou rejeitar esta tarefa.";
$MESS["INTE_NO_REJECT_PERMS"] = "Você não pode rejeitar esta tarefa.";
$MESS["INTE_APPLY_MESSAGE"] = "A tarefa \"#NAME#\" foi aceita.\\r\\n\\r\\n[url=#URL_VIEW#]Visualizar detalhes[/url]";
$MESS["INTE_REJECT_MESSAGE"] = "A tarefa \"#NAME#\" não foi concluída.\\r\\n\\r\\n[url=#URL_VIEW#]Visualizar detalhes[/url]";
$MESS["INTE_ERROR_SAVE_TASK"] = "Erro ao salvar a tarefa.";
$MESS["INTE_WRONG_ASSIGNEDTO_USER"] = "Você não pode atribuir a tarefa a este usuário.";
$MESS["INTE_ADD_TASK_MESSAGE"] = "Uma nova tarefa \"#NAME#\" foi criada.\\r\\n\\r\\n[url=#URL_VIEW#]Visualizar detalhes da tarefa[/url]\\r\\n[url=#URL_EDIT#]Editar tarefa[/url]";
$MESS["INTE_EDIT_TASK_MESSAGE"] = "Os parâmetros da tarefa \"#NAME#\" foram alterados.\\r\\n\\r\\n[url=#URL_VIEW#]Vizualizar detalhas da tarefa[/url]";
$MESS["INTE_ADD_TASK_DATES_EMPTY"] = "não definido";
$MESS["INTASK_TO_DATE_TLP"] = "até #DATE#";
$MESS["INTASK_FROM_DATE_TLP"] = "de #DATE#";
$MESS["INTAST_T6654_LOG"] = "Prioridade: #PRIORITY#\\r\\nPrazo: #TIME#\\r\\nResponsável: #RESP#";
$MESS["INTE_TASKS_EMPTY_FIELD"] = "O campo \"#FIELD#\" está vazio.";
$MESS["INTE_TASKS_RESPONSIBLE"] = "Responsável";
$MESS["INTE_TASKS_PERMS_EVENT"] = "Você não tem permissão para realizar esta ação.";
$MESS["INTE_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["INTE_ADD_TASK_MESSAGE1"] = "Foi criada uma nova tarefa: \"#NAME#\" 
Prioridade: #TASK_PRIORITY#
Data de vencimento: #TASK_DATES# 

[url= #URL_VIEW#]Ver detalhes[/url] 
[url= #URL_EDIT#]Editar tarefa[/url]

";
$MESS["INTE_EDIT_TASK_MESSAGE1"] = "Os parâmetros da tarefa \"#NAME#\" foram alterados.
Prioridade: #TASK_PRIORITY#
Data de vencimento: #TASK_DATES# 

[url= #URL_VIEW#]Ver detalhes[/url]
";
$MESS["INTE_FINISH_MESSAGE"] = "A tarefa \"#NAME# foi concluída.

[url= #URL_VIEW#]Visualizar detalhes[/url].";
?>