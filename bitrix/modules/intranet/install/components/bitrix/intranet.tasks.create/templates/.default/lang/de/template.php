<?
$MESS["INTET_NOT_SET"] = "(nicht festgelegt)";
$MESS["INTET_CONFIRM"] = "Annehmen";
$MESS["INTET_ADD"] = "Hinzufügen";
$MESS["INTET_APPLY"] = "Anwenden";
$MESS["INTET_CANCEL"] = "Abbrechen";
$MESS["INTET_FINISH_BUTTON"] = "Abschließen";
$MESS["INTET_EDIT_TITLE"] = "Einstellungen für die Aufgabe Nr. #ID# bearbeiten";
$MESS["INTET_CREATE_TITLE"] = "Neue Aufgabe erstellen";
$MESS["INTET_REJECT"] = "Ablehnen";
$MESS["INTET_SAVE"] = "Speichern";
$MESS["INTET_VIEW_TITLE"] = "Aufgabe Nr. #ID# anzeigen";
$MESS["INTET_VE_INS_SUBTASK"] = "Neue Unteraufgabe";
$MESS["INTET_CURRENT_STATUS"] = "Aktueller Status";
$MESS["INTET_U_DEL"] = "Löschen";
$MESS["INTET_RESPONSIBLE"] = "Ausführend";
$MESS["INTET_SEND_COMMAND"] = "Befehl senden";
$MESS["INTET_VE_INS_SUBTASK1"] = "Unteraufgabe";
$MESS["INTET_VE_INS_SUBTASK2"] = "Name der Unteraufgabe";
?>