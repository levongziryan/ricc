<?
$MESS["INTET_NOT_SET"] = "(no se estableció)";
$MESS["INTET_ADD"] = "Agregar";
$MESS["INTET_CREATE_TITLE"] = "Nueva Tarea";
$MESS["INTET_EDIT_TITLE"] = "Editar Tarea ##ID#";
$MESS["INTET_VIEW_TITLE"] = "Ver Tarea ##ID#";
$MESS["INTET_SAVE"] = "Guardar";
$MESS["INTET_APPLY"] = "Aplicar";
$MESS["INTET_CANCEL"] = "Cancelar";
$MESS["INTET_CONFIRM"] = "Aceptar";
$MESS["INTET_REJECT"] = "Rechazar";
$MESS["INTET_FINISH_BUTTON"] = "Finalizar";
$MESS["INTET_CURRENT_STATUS"] = "Estado Actual";
$MESS["INTET_SEND_COMMAND"] = "Enviar Comando";
$MESS["INTET_RESPONSIBLE"] = "Responsable";
$MESS["INTET_VE_INS_SUBTASK"] = "Agregar la Sub tarea";
$MESS["INTET_VE_INS_SUBTASK1"] = "Sub tarea";
$MESS["INTET_VE_INS_SUBTASK2"] = "Nombre de la Sub tarea";
$MESS["INTET_U_DEL"] = "Borrar";
?>