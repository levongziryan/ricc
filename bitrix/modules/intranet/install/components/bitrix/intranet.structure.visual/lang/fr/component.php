<?
$MESS["ISV_ERROR_dpt_not_empty"] = "Vous devez déplacer tous les employés du départelent avant la suppression.";
$MESS["ISV_move_department"] = "Le département <b>#DEPARTMENT#</b>. est subordonné au département <b>#DEPARTMENT_TO#</b>.";
$MESS["ISV_set_department_head"] = "L'employé <b>#NAME#</b> est nommé comme le chef de la (sub)division <b>#DEPARTMENT#</b>.";
$MESS["ISV_change_department"] = "L'employé <b>#NAME#</b> est transféré dans la (sub)division <b>#DEPARTMENT#</b>.";
?>