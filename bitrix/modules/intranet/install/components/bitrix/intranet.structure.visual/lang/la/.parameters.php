<?
$MESS["INTR_ISV_PARAM_NAME_TEMPLATE"] = "Formato del nombre";
$MESS["INTR_ISV_PARAM_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["INTR_ISV_PARAM_SHOW_LOGIN"] = "Mostrar nombre del login si no requiere los campos del usuario que estén disponibles";
$MESS["INTR_ISV_PARAM_USE_USER_LINK"] = "Mostrar tarjetas del usuario a visualizar";
$MESS["INTR_ISV_PARAM_DETAIL_URL"] = "Página de la estructura de la compañía";
$MESS["INTR_ISV_PARAM_PM_URL"] = "Dirección de la ventana del mensaje instantáneo";
$MESS["INTR_ISV_PARAM_PROFILE_URL"] = "Página del perfil personal";
$MESS["INTR_ISV_PARAM_PATH_TO_VIDEO_CALL"] = "Página de Video llamada";
?>