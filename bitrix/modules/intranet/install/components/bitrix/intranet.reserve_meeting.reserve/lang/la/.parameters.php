<?
$MESS["INTL_VARIABLE_ALIASES"] = "Variable del alias";
$MESS["INTL_IBLOCK_TYPE"] = "Tipo de bloque Informativo";
$MESS["INTL_IBLOCK"] = "Bloque Informativo";
$MESS["INTL_MEETING_VAR"] = "Variable del ID para la sala de reunión";
$MESS["INTL_ITEM_VAR"] = "ID variable de la reserva";
$MESS["INTL_PAGE_VAR"] = "Variable de la página";
$MESS["INTL_PATH_TO_MEETING"] = "Página para programar reserva de sala de reuniones";
$MESS["INTL_PATH_TO_MEETING_LIST"] = "Página principal de reserva de sala de reuniones";
$MESS["INTL_PATH_TO_RESERVE_MEETING"] = "Página de reserva de sala de reuniones";
$MESS["INTL_PATH_TO_MODIFY_MEETING"] = "Página del editor de parámetros de la reserva de sala de reuniones";
$MESS["INTL_SET_NAVCHAIN"] = "Establecer atajos";
$MESS["INTL_USERGROUPS_MODIFY"] = "Grupos de usuario permitidos para editar el programa de la sala de reunión";
$MESS["INTL_USERGROUPS_RESERVE"] = "Grupos de usuario permitidos para reservar salas de reuniones";
$MESS["INTL_USERGROUPS_CLEAR"] = "Grupos de usuario permitidos para cancelar reservas de salas de reuniones";
$MESS["INTL_MEETING_ID"] = "ID de la Sala de Reunión";
$MESS["INTL_ITEM_ID"] = "ID de la Reserva";
?>