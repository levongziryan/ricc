<?
$MESS["INTMT_DELETE_VIEW_CONF"] = "Tem certeza de que deseja excluir esta visualização?";
$MESS["INTMT_BACK2LIST_DESCR"] = "Voltar a Lista de Tarefas";
$MESS["INTMT_OUTLOOK"] = "Conectar ao Outlook";
$MESS["INTMT_CREATE_FOLDER"] = "Criar Pasta";
$MESS["INTMT_CREATE_TASK_DESCR"] = "Criar Nova Tarefa";
$MESS["INTMT_CREATE_TASK"] = "Criar Tarefa";
$MESS["INTMT_CREATE_VIEW"] = "Criar Visualização";
$MESS["INTMT_CREATE_FOLDER_DESCR"] = "Cria uma nova pasta";
$MESS["INTMT_DEFAULT"] = "Padrão";
$MESS["INTMT_DELETE_VIEW"] = "Excluir Visualização";
$MESS["INTMT_EDIT_TASK_DESCR"] = "Editar Tarefa Atual";
$MESS["INTMT_EDIT_TASK"] = "Editar Tarefa";
$MESS["INTMT_EDIT_VIEW"] = "Editar Visualização";
$MESS["INTMT_OUTLOOK_TITLE"] = "Sincronização de tarefas Outlook";
$MESS["INTMT_BACK2LIST"] = "Tarefas";
$MESS["INTMT_VIEW"] = "Visualizar";
$MESS["INTMT_VIEW_TASK_DESCR"] = "Ver tarefa atual";
$MESS["INTMT_VIEW_TASK"] = "Visualizar Tarefa";
?>