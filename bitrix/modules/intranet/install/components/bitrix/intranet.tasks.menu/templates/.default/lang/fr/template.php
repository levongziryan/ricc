<?
$MESS["INTMT_BACK2LIST_DESCR"] = "Revenir à la liste des tâches";
$MESS["INTMT_DELETE_VIEW_CONF"] = "tes-vous sûr de vouloir supprimer cette présentation?";
$MESS["INTMT_EDIT_TASK"] = "Changer la tâche";
$MESS["INTMT_EDIT_VIEW"] = "Modification de l'affichage";
$MESS["INTMT_EDIT_TASK_DESCR"] = "Modifier la tâche courante";
$MESS["INTMT_DEFAULT"] = "Par défaut";
$MESS["INTMT_VIEW"] = "Affichage";
$MESS["INTMT_VIEW_TASK"] = "Affichage de la tâche";
$MESS["INTMT_VIEW_TASK_DESCR"] = "Revue de la tâche courante";
$MESS["INTMT_OUTLOOK_TITLE"] = "Synchronisation des tâches avec Microsoft Outlook";
$MESS["INTMT_OUTLOOK"] = "Synchroniser le calendrier avec MS Outlook";
$MESS["INTMT_CREATE_TASK"] = "Créer une Tâche";
$MESS["INTMT_CREATE_TASK_DESCR"] = "Créer une nouvelle Tâche";
$MESS["INTMT_CREATE_FOLDER_DESCR"] = "Créer un nouveau dossier";
$MESS["INTMT_CREATE_FOLDER"] = "Créer le dossier";
$MESS["INTMT_CREATE_VIEW"] = "Créer la présentation";
$MESS["INTMT_BACK2LIST"] = "Tâches";
$MESS["INTMT_DELETE_VIEW"] = "Supprimer la représentation";
?>