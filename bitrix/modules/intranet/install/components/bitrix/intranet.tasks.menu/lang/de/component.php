<?
$MESS["W_IBLOCK_IS_NOT_INSTALLED"] = "Das Modul \"Informationsblöcke\" wurde nicht installiert.";
$MESS["W_SONET_IS_NOT_INSTALLED"] = "Das Modul \"Soziales Netz\" wurde nicht installiert.";
$MESS["INTM_TASKS_OFF"] = "Die Funktion \"Aufgaben\" ist deaktiviert";
$MESS["INTM_NO_SONET_PERMS"] = "Sie haben nicht genügend Rechte, Aufgaben anzusehen";
?>