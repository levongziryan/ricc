<?
$MESS["CONNECTION_ERROR2"] = ", перевірте, будь ласка, параметри з'єднання, а також чи доступний сервер.";
$MESS["C1C_TABLELINE"] = "<m:РядокТаблиці>";
$MESS["SET_UP_CONNECTION_PARAMETERS"] = "Для відображення звіту, будь ласка, задайте параметри підключення до сервера";
$MESS["INTRANET_MODULE_NOT_INSTALLED"] = "Для роботи компонента необхідна редакція продукту «Корпоративний портал».";
$MESS["C1C_METHOD_REP_ID"] = "ІдЗвіту";
$MESS["C1C_METHODNAME_REP"] = "КороткийЗвіт";
$MESS["TEMP_FILE_OPEN_ERROR"] = "Не вдається відкрити для запису тимчасовий файл:";
$MESS["HTTP_WRITE_ERROR"] = "Не вдається відіслати SOAP запит. Помилка протоколу HTTP.";
$MESS["HTTP_READ_ERROR"] = "Не вдається отримати SOAP відповідь. Помилка протоколу HTTP.";
$MESS["CONNECTION_ERROR1"] = "Не вдається встановити підключення до сервера";
$MESS["MODULE_NOT_INSTALLED"] = "Не встановлено модуль:";
$MESS["C1C_METHOD_REP"] = "Звіт";
$MESS["SOCK_OPEN_ERROR"] = "Помилка ініціалізації з'єднання на wеb-сервері.";
$MESS["TEMP_FILE_WRITE_ERROR"] = "Помилка при запису в тимчасовий файл:";
$MESS["C1C_TABLELINE_F"] = "Поле1";
$MESS["C1C_TABLELINE_F2"] = "Поле2";
$MESS["HTTP_READ_TIMEOUT"] = "Перевищено часовий інтервал очікування HTTP-відповіді від сервера 1С.";
$MESS["C1C_METHODNAME_LIST"] = "СписокЗвітів";
$MESS["LINK_REPORTLIST"] = "Список звітів";
$MESS["LINK_SHORTREPORT"] = "Короткий звіт";
$MESS["LINK_REPORT"] = "Звіт";
$MESS["C1C_TABLELINE_REP"] = "КороткийЗвіт";
$MESS["C1C_TABLELINE_LIST"] = "СписокЗвітів";
$MESS["C1C_REP"] = "m: Звіт";
$MESS["C1C_REP_ID"] = "m: ІдЗвіту";
?>