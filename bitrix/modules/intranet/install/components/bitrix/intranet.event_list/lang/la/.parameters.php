<?
$MESS["ECL_GROUP_BASE_SETTINGS"] = "Ajustes principales";
$MESS["ECL_P_IBLOCK_TYPE"] = "Tipo de bloque informativo";
$MESS["ECL_P_IBLOCK"] = "Bloque informativo";
$MESS["ECL_P_IBLOCK_SECTION_ID"] = "ID del bloque informativo de la sección";
$MESS["ECL_P_INIT_DATE"] = "Iniciado";
$MESS["ECL_P_SHOW_CUR_DATE"] = "-Fecha actual-";
$MESS["ECL_P_FUTURE_MONTH_COUNT"] = "Mostrar los eventos mas recientes por (meses)";
$MESS["ECL_P_LOAD_MODE"] = "Cargar Eventos";
$MESS["ECL_P_EVENT_LIST_MODE"] = "Mostrar solo lista de eventos";
$MESS["ECL_P_DETAIL_URL"] = "Ver detalles del URL de la página";
$MESS["ECL_P_EVENTS_COUNT"] = "Lista de eventos";
$MESS["ECL_P_CACHE_TIME"] = "Tiempo de vida del caché (seg.)";
$MESS["ECL_P_ALLOW_SUPERPOSE"] = "Permitir calendarios favoritos";
$MESS["ECL_P_USER_IBLOCK_ID"] = "Bloque de información para calendarios de usuario";
$MESS["ECL_P_CUR_USER_EVENT_LIST"] = "Mostrar Eventos actuales del usuario";
?>