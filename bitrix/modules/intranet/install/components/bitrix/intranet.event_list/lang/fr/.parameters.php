<?
$MESS["ECL_P_IBLOCK_SECTION_ID"] = "Sections ID de bloc d'information";
$MESS["ECL_P_DETAIL_URL"] = "Adresse de la page pour une consultation détaillée";
$MESS["ECL_P_CACHE_TIME"] = "Temps de mise en cache (s).";
$MESS["ECL_P_INIT_DATE"] = "Date de l'initialisation";
$MESS["ECL_P_LOAD_MODE"] = "Chargement des événements";
$MESS["ECL_P_IBLOCK"] = "Bloc d'information";
$MESS["ECL_P_USER_IBLOCK_ID"] = "Bloc d'information de calendriers utilisateurs";
$MESS["ECL_P_EVENTS_COUNT"] = "Quantité d'événements dans la liste";
$MESS["ECL_GROUP_BASE_SETTINGS"] = "Principaux paramètres de souscription";
$MESS["ECL_P_EVENT_LIST_MODE"] = "Afficher uniquement la liste des événements";
$MESS["ECL_P_FUTURE_MONTH_COUNT"] = "Montrer les événements les plus proches pour nombre de mois";
$MESS["ECL_P_CUR_USER_EVENT_LIST"] = "Afficher les événements d'un utilisateur courant";
$MESS["ECL_P_ALLOW_SUPERPOSE"] = "Autoriser l'utilisation de Calendriers choisis";
$MESS["ECL_P_SHOW_CUR_DATE"] = "-Date Actuelle-";
$MESS["ECL_P_IBLOCK_TYPE"] = "Type de bloc d'information";
?>