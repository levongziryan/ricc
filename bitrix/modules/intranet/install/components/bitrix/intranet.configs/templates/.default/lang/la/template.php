<?
$MESS["CONFIG_HEADER_SETTINGS"] = "Configuraciones";
$MESS["CONFIG_COMPANY_NAME"] = "Nombre de la compañía";
$MESS["CONFIG_COMPANY_TITLE_NAME"] = "Nombre de la compañía para mostrar en el encabezado";
$MESS["CONFIG_SAVE_SUCCESSFULLY"] = "Los ajustes se han actualizado correctamente";
$MESS["CONFIG_SAVE"] = "Guardar";
$MESS["config_rating_label_likeY"] = "Texto de botón \"Like\" no votado";
$MESS["config_rating_label_likeN"] = "Texto de botón \"Like\" votado";
$MESS["CONFIG_EMAIL_FROM"] = "Correo electrónico del administrador del sitio<br>(dirección de remitente predeterminada)";
$MESS["CONFIG_LOGO_24"] = "Agregar \"24\" al logotipo de la compañía";
$MESS["CONFIG_WEBDAV_SERVICES_GLOBAL"] = "Activar la edición de documentos por vía externa<br/>servicios (Google Docs, MS Office Online y otros)";
$MESS["CONFIG_WEBDAV_SERVICES_LOCAL"] = "Permitir que usuarios individuales y grupos activen la edición de documentos a través de servicios externos";
$MESS["CONFIG_WEBDAV_ALLOW_AUTOCONNECT_SHARE_GROUP_FOLDER"] = "Conectar automáticamente Group Drive cuando<br/>el usuario se une al grupo";
$MESS["CONFIG_DISK_ALLOW_EDIT_OBJECT_IN_UF"] = "Permitir a los usuarios involucrados editar documentos adjuntos a publicaciones, tareas, comentarios, etc. <br>(El acceso se puede configurar manualmente en Mi Drive - Archivos cargados en cualquier momento)";
$MESS["CONFIG_DATE_FORMAT"] = "Formato de fecha";
$MESS["CONFIG_TIME_FORMAT"] = "Formato de hora";
$MESS["CONFIG_TIME_FORMAT_12"] = "12 horas";
$MESS["CONFIG_TIME_FORMAT_24"] = "24 horas";
$MESS["CONFIG_WEEK_START"] = "Primer día de la semana";
$MESS["DAY_OF_WEEK_0"] = "Domingo";
$MESS["DAY_OF_WEEK_1"] = "Lunes";
$MESS["DAY_OF_WEEK_2"] = "Martes";
$MESS["DAY_OF_WEEK_3"] = "Miércoles";
$MESS["DAY_OF_WEEK_4"] = "Jueves";
$MESS["DAY_OF_WEEK_5"] = "Viernes";
$MESS["DAY_OF_WEEK_6"] = "Sábado";
$MESS["CONFIG_CLIENT_LOGO"] = "Logo de la compañía";
$MESS["CONFIG_CLIENT_LOGO_DESCR"] = "Su logotipo debe ser un <b>PNG</b> file.<br/>Las dimensiones máximas son 222px by 55px.";
$MESS["CONFIG_ADD_LOGO_BUTTON"] = "Cargar Logo";
$MESS["CONFIG_ADD_LOGO_DELETE"] = "Eliminar Logo";
$MESS["CONFIG_ADD_LOGO_DELETE_CONFIRM"] = "¿Estás seguro que quiere eliminar el logotipo?";
$MESS["CONFIG_ALLOW_TOALL"] = "Permitir \"Todos los usuarios\" como opción en el Flujo de actividad";
$MESS["CONFIG_IM_CHAT_RIGHTS"] = "Permitir a los usuarios enviar mensajes en el chat General";
$MESS["CONFIG_DEFAULT_TOALL"] = "Utilizar \"Todos los usuarios\" como destinatario predeterminado";
$MESS["CONFIG_TOALL_RIGHTS_ADD"] = "Agregar";
$MESS["CONFIG_TOALL_RIGHTS"] = "Configuración de la opción \"Todos los empleados\"";
$MESS["CONFIG_TOALL_DEL"] = "eliminar";
$MESS["CONFIG_FEATURES_TITLE"] = "Servicios";
$MESS["CONFIG_FEATURES_EXTRANET"] = "Extranet";
$MESS["CONFIG_FEATURES_TIMEMAN"] = "Gestión del tiempo y Reportes de trabajo";
$MESS["CONFIG_FEATURES_MEETING"] = "Reuniones y sesiones informativas";
$MESS["CONFIG_FEATURES_LISTS"] = "Lista";
$MESS["CONFIG_FEATURES_CRM"] = "CRM";
$MESS["CONFIG_FEATURES_PROCESSES"] = "Flujos de trabajo administrativos";
$MESS["CONFIG_IP_TITLE"] = "Restricciones de IP (permiten el acceso sólo desde direcciones IP especificadas o rangos de direcciones. Ejemplo: 192.168.0.7; 192.168.0.1-192.168.0.100)";
$MESS["CONFIG_WORK_TIME"] = "Parámetros del tiempo de trabajo";
$MESS["CONFIG_WEEK_HOLIDAYS"] = "Fin de semana";
$MESS["CAL_OPTION_FIRSTDAY_MO"] = "Lunes";
$MESS["CAL_OPTION_FIRSTDAY_TU"] = "Martes";
$MESS["CAL_OPTION_FIRSTDAY_WE"] = "Miércoles";
$MESS["CAL_OPTION_FIRSTDAY_TH"] = "Jueves";
$MESS["CAL_OPTION_FIRSTDAY_FR"] = "Viernes";
$MESS["CAL_OPTION_FIRSTDAY_SA"] = "Sábado";
$MESS["CAL_OPTION_FIRSTDAY_SU"] = "Domingo";
$MESS["CONFIG_YEAR_HOLIDAYS"] = "Fines de semana y feriados";
$MESS["CONFIG_HEADER_SECUTIRY"] = "Configuraciones de seguridad";
$MESS["CONFIG_OTP_SECURITY"] = "Habilitar la autenticación de dos pasos para todos los usuarios";
$MESS["CONFIG_OTP_SECURITY2"] = "Hacer obligatoria la autorización de dos pasos para todos los usuarios";
$MESS["CONFIG_OTP_SECURITY_SWITCH_OFF_INFO"] = "Tenga en cuenta que al desmarcar esta opción no se detendrá el uso de contraseñas de una sola vez por los usuarios que ya tienen habilitada la autenticación de dos pasos.
<br/><br/>Todavía tendrán que usar OTP al iniciar sesión en Bitrix24.
<br/><br/>Puede deshabilitar la autenticación de dos pasos en su perfil de usuario respectivo.";
$MESS["CONFIG_OTP_SECURITY_DAYS"] = "Especifique el período de tiempo dentro del cual todos los empleados<br/>tendrán que habilitar la autenticación de dos pasos";
$MESS["CONFIG_OTP_SECURITY_INFO"] = "Hemos desarrollado un procedimiento de habilitación de autenticación de dos pasos, fácil de usar, que cualquier empleado puede tomar sin la asistencia de un experto.<br/><br/>
Se enviará un mensaje a cada empleado notificándoles que tendrán que habilitar la autenticación de dos pasos dentro del período de tiempo ingresado. Los usuarios que no lo hagan
Por lo que no se le permitirá iniciar sesión más.";
$MESS["CONFIG_OTP_SECURITY_INFO2"] = "<br/>Para habilitar la autenticación en dos pasos, el usuario debe instalar una aplicación Bitrix24 OTP en su teléfono móvil. Esta aplicación se puede descargar desde AppStore o GooglePlay.<br/><br/>
Los empleados que no tienen un dispositivo móvil adecuado deben estar equipados con hardware especial - eToken Pass. Hay una gama de vendedores de los cuales puedes obtenerlos, por ejemplo:
<a target=_blank href=\"http://www.safenet-inc.com/multi-factor-authentication/authenticators/pki-usb-authentication/etoken-pro/\">www.safenet-inc.com</a>,
<a target=_blank href=\"http://www.authguard.com/eToken-PASS.asp\">www.authguard.com</a>.
Encuentra otros proveedores en <a target=_blank href=\"https://www.google.com/?q=buy+eToken+PASS&spell=1#safe=off&q=buy+eToken+PASS\">Google</a>.
<br/><br/>
Como alternativa, puede deshabilitar la autenticación de dos pasos para algunos de los empleados. Sin embargo, esto aumentará el riesgo de acceso no autorizado a su Bitrix24 si el nombre de usuario y la contraseña de uno de esos empleados son robados. Como administrador, puede deshabilitar la autenticación de dos pasos para un empleado en el perfil de usuario.";
$MESS["CONFIG_MORE"] = "Leer más";
$MESS["CONFIG_OTP_POPUP_TITLE"] = "¿Qué hay de publicar en el Flujo de Actividad?";
$MESS["CONFIG_OTP_POPUP_TEXT"] = "Aquí está el texto que puede publicar en el Flujo de Actividad<br/>para que sus empleados lo lean.<br/><br/>
Deje que sus colegas conozcan la autenticación en dos pasos,<br/>el procedimiento de configuración y el nuevo método de autenticación<br/>que tendrán que usar para iniciar sesión en su Bitrix24.";
$MESS["CONFIG_OTP_POPUP_SHARE"] = "Compartir";
$MESS["CONFIG_OTP_POPUP_CLOSE"] = "No, gracias";
$MESS["CONFIG_OTP_ADMIN_IS_REQUIRED_INFO"] = "Antes de migrar a sus empleados a un sistema de autenticación de dos pasos, configúrelo primero para su cuenta.<br/><br/>Por favor, proceda activándolo en la página Seguridad de su perfil.";
$MESS["CONFIG_OTP_IMPORTANT_TITLE"] = "Autenticación en dos pasos";
$MESS["CONFIG_OTP_IMPORTANT_TEXT"] = "Hoy, utiliza su nombre de usuario y contraseña para iniciar sesión en Bitrix24. Los datos comerciales y personales están protegidos por la tecnología de cifrado de datos. Sin embargo, hay herramientas que una persona maliciosa puede emplear para entrar en su computadora y robar sus credenciales de inicio de sesión.

Buscando protección contra posibles amenazas, hemos habilitado una función de seguridad adicional para Bitrix24: la autenticación en dos pasos.

La autenticación en dos pasos es un método especial para protegerse contra el software de hackers, especialmente el robo de contraseñas. Cada vez que inicie sesión en el sistema, tendrá que pasar dos niveles de verificación. En primer lugar, ingresará su correo electrónico y contraseña, luego ingresará un código de seguridad único obtenido de su dispositivo móvil.

Esto hará que nuestros datos comerciales sean seguros incluso si el usuario y la contraseña de cualquier empleado son robados por un atacante.

Tienes 5 días para habilitar esta función.

Para configurar el nuevo procedimiento de autenticación, vaya a su perfil y seleccione la página \"Configuración de seguridad\".

Si no tiene un dispositivo móvil adecuado para ejecutar la aplicación, o si experimenta algún tipo de problema, háganoslo saber comentando esta publicación.";
$MESS["CONFIG_MARKETPLACE_TITLE"] = "Migrar datos a Bitrix24 de otros sistemas";
$MESS["CONFIG_MARKETPLACE_MORE"] = "Leer más";
$MESS["CONFIG_NAME_FORMAT"] = "Formato de nombre";
$MESS["CONFIG_CULTURE_OTHER"] = "Otros";
$MESS["CONFIG_ORGANIZATION"] = "Tipo de organización";
$MESS["CONFIG_ORGANIZATION_DEF"] = "Compañía y empleados";
$MESS["CONFIG_ORGANIZATION_PUBLIC"] = "Organización y usuarios";
$MESS["CONFIG_ORGANIZATION_GOV"] = "Organización y empleados";
$MESS["CONFIG_URL_PREVIEW_ENABLE"] = "Habilitar enlaces multimedia";
$MESS["CONFIG_DISK_VIEWER_SERVICE"] = "Ver documentos utilizados";
$MESS["CONFIG_ALLOW_SELF_REGISTER"] = "Permitir registro rápido";
$MESS["CONFIG_ALLOW_INVITE_USERS"] = "Permitir a todos invitar a nuevos usuarios a este cuenta de Bitrix24";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_JOIN"] = "Notificar sobre nuevos empleados en el chat general";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_LEAVE"] = "Notificar sobre empleados/usuario despedidos en el chat general";
$MESS["CONFIG_ALLOW_NEW_USER_LF"] = "Notificar acerca de las nuevas contrataciones en el Flujo de Actividad";
$MESS["CONFIG_DISK_VERSION_LIMIT_PER_FILE"] = "Entradas máx. en el historial del documento";
$MESS["CONFIG_DISK_ALLOW_USE_EXTERNAL_LINK"] = "Permitir enlaces públicos";
$MESS["CONFIG_DISK_OBJECT_LOCK_ENABLED"] = "Permitir bloqueo de documentos";
$MESS["CONFIG_DISK_LOCK_POPUP_TEXT"] = "Características Avanzadas del Drive:

<br/><br/>
- Bloquee los archivos para evitar que otros puedan editar los documentos que está trabajando actualmente.
<br/><br/>
- Deshabilitar vínculos públicos para evitar que otros usuarios puedan compartir archivos públicamente con usuarios fuera de su cuenta de Bitrix24
<br/><br/>
<a href=\"https://www.bitrix24.com/pro/drive.php\" target='_blank'>Learn More</a>
<br/><br/>
Las funciones avanzadas de Bitrix24.Drive están disponibles en suscripciones comerciales que comienzan con el plan Standard de Bitrix24.
";
$MESS["CONFIG_NETWORK_AVAILABLE"] = "Permitir que los usuarios de mi Bitrix24 se comuniquen en la red global Bitrix24";
$MESS["CONFIG_NETWORK_AVAILABLE_NOT_CONFIRMED"] = "Esta función estará disponible una vez que el administrador haya confirmado la cuenta.";
$MESS["CONFIG_NETWORK_AVAILABLE_TITLE"] = "Disponible sólo en planes comerciales de Bitrix24";
$MESS["CONFIG_NETWORK_AVAILABLE_TEXT_NEW"] = "La comunicación en Bitrix24.Network solo está disponible para usuarios comerciales de Bitrix24.<br/><br/>
Estas son las ventajas de conectar Bitrix24.Network:<br/>
<ul>
<li>Todos sus contactos comerciales y socios están conectados en una red</li>
<li>Comunicaciones rápidas y convenientes con clientes y usuarios externos</li>
<li>Comunicaciones sin interrupciones entre usuarios en diferentes cuentas de Bitrix24</li>
</ul>
<b>Todas las herramientas de Bitrix24.Network están disponibles comenzando con Bitrix24 Plus a #PRICE# por mes.</b>";
$MESS["CONFIG_DISK_LIMIT_LOCK_POPUP_TITLE"] = "Disponible sólo en Bitrix24.Drive extendido";
$MESS["CONFIG_DISK_LIMIT_LOCK_POPUP_TEXT"] = "Disfrute aún más de las funciones útiles de Bitrix24 con Advanced Drive:<br/><br/>
+ Historial de actualización de documentos (modificado cuando y por quién)<br/>
+ Recuperar cualquier versión del documento anterior en el historial<br/><br/>
<a href=\"https://www.bitrix24.com/pro/drive.php\" target='_blank'>Leer más</a><br/><br/>
Advanced Drive está disponible en el plan \"Standard\" y superior.";
$MESS["CONFIG_NAME_CHANGE_SECTION"] = "Cambiar la dirección de Bitrix24";
$MESS["CONFIG_NAME_CHANGE_ACTION"] = "Cambiar";
$MESS["CONFIG_NAME_CHANGE_INFO"] = "Nota: sólo puede cambiar la dirección de Bitrix24 una vez.";
$MESS["CONFIG_DISK_LOCK_POPUP_TITLE"] = "Disponible sólo en avanzadas Bitrix24 Drive";
$MESS["CONFIG_DISK_ALLOW_DOCUMENT_TRANSFORMATION"] = "Genera automáticamente archivos PDF y JPG para documentos";
$MESS["CONFIG_DISK_ALLOW_VIDEO_TRANSFORMATION"] = "Genera automáticamente archivos MP4 y JPG para medios de video";
$MESS["CONFIG_DISK_TRANSFORM_FILES_ON_OPEN"] = "Convertir archivo tan pronto como se abra";
$MESS["CONFIG_NAME_GOOGLE_API_KEY"] = "Preferencias de integración de Google API";
$MESS["CONFIG_NAME_GOOGLE_API_KEY_FIELD"] = "API de Google Maps para clave de integración Bitrix24";
$MESS["CONFIG_NAME_GOOGLE_API_HOST_HINT"] = "La clave se obtuvo para el dominio <b>#domain#</b>. Si no puede hacer que Google Maps funcione, cambie la configuración de las teclas o <a href=\"https://developers.google.com/maps/documentation/javascript/get-api-key\" target=\"_blank\">consiga uno nuevo</a>.";
$MESS["CONFIG_NAME_GOOGLE_API_KEY_HINT"] = "Se requiere una clave de API de Google para usar mapas. Para obtener tu clave, por favor <a href=\"https://developers.google.com/maps/documentation/javascript/get-api-key\" target=\"_blank\">usa este formulario</a>.";
$MESS["CONFIG_PHONE_NUMBER_DEFAULT_COUNTRY"] = "Formato del número de teléfono: país predeterminado";
?>