<?
$MESS["CONFIG_EMAIL_ERROR"] = "El e-mail del administrador del sitio web es incorrecto.";
$MESS["CONFIG_IP_ERROR"] = "La dirección IP es incorrecta.";
$MESS["CONFIG_FORMAT_NAME_ERROR"] = "El formato del nombre es incorrecto. Las macros siguientes son posibles: #TITLE#, #NAME#, #LAST_NAME#, #SECOND_NAME#, #NAME_SHORT#, #LAST_NAME_SHORT#, #SECOND_NAME_SHORT#, #EMAIL#";
$MESS["DISK_VERSION_LIMIT_PER_FILE_UNLIMITED"] = "Ilimitado";
$MESS["CONFIG_ACCESS_DENIED"] = "Acceso denegado";
$MESS["CONFIG_SMTP_PASS_ERROR"] = "Las contraseñas de servidor SMTP suministradas no coinciden.";
?>