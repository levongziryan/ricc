<?
$MESS["CONFIG_EMAIL_ERROR"] = "O E-mail do administrador do site está incorreto.";
$MESS["CONFIG_IP_ERROR"] = "Os endereços IP estão incorretos.";
$MESS["CONFIG_FORMAT_NAME_ERROR"] = "O formato do nome está incorreto. Os seguintes macros são possíveis: #TITLE#, #NAME#, #LAST_NAME#, #SECOND_NAME#, #NAME_SHORT#, #LAST_NAME_SHORT#, #SECOND_NAME_SHORT#, #EMAIL#";
$MESS["DISK_VERSION_LIMIT_PER_FILE_UNLIMITED"] = "Ilimitado";
$MESS["CONFIG_ACCESS_DENIED"] = "Acesso negado";
$MESS["CONFIG_SMTP_PASS_ERROR"] = "As senhas do servidor SMTP fornecidas não coincidem.";
?>