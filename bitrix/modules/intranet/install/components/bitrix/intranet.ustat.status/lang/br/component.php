<?
$MESS["INTRANET_USTAT_WIDGET_TITLE"] = "Pulso da Empresa";
$MESS["INTRANET_USTAT_WIDGET_LOADING"] = "Carregando...";
$MESS["INTRANET_USTAT_WIDGET_ACTIVITY_HELP"] = "Nível de atividade atual da empresa (composto de todos os usuários na última hora)";
$MESS["INTRANET_USTAT_WIDGET_INVOLVEMENT_HELP"] = "Engajamento atual de usuários. Isso mostra a porcentagem de todos os usuários hoje que usaram pelo menos quatro ferramentas diferentes na intranet.";
?>