<?
$MESS["INTRANET_USTAT_WIDGET_TITLE"] = "Pulso de la compañía";
$MESS["INTRANET_USTAT_WIDGET_LOADING"] = "Cargando...";
$MESS["INTRANET_USTAT_WIDGET_ACTIVITY_HELP"] = "Nivel actual de actividad de la compañía (compuesto de todos los usuarios en la última hora)";
$MESS["INTRANET_USTAT_WIDGET_INVOLVEMENT_HELP"] = "El compromiso actual de los usuarios. Esto muestra el porcentaje de todos los usuarios que  hoy han utilizado al menos cuatro herramientas diferentes en la intranet.";
?>