<?
$MESS["INTRANET_RESMIT"] = "Points de négociations";
$MESS["INTRANET_RESMIT_LIST_DESCRIPTION"] = "Composant pour l'affichage de la liste des salles de réunion.";
$MESS["INTR_GROUP_NAME"] = "Intranet - portail d'entreprise";
$MESS["ITSRM_NAME"] = "Menu";
$MESS["ITSRM_DESCRIPTION"] = "Affiche le menu du composant.";
$MESS["INTRANET_RESMIT_LIST"] = "Liste des salles de réunion";
?>