<?
$MESS["INTASK_C27T_GRAPH"] = "Graphique";
$MESS["INTASK_C27T_GRAPH_TITLE"] = "Planning de réservation de la salle de réunion";
$MESS["INTASK_C27T_RESERVE"] = "Faire la réservation";
$MESS["INTASK_C27T_RESERVE_TITLE"] = "Réservation de la salle de réunion";
$MESS["INTASK_C27T_EDIT_TITLE"] = "Modifier les paramètres de la salle de réunions";
$MESS["INTASK_C27T_EDIT"] = "Changement de la salle des Réunions";
$MESS["ITSRM1_MEETING_SEARCH_DESCR"] = "Accéder à la recherche de salles de réunion";
$MESS["ITSRM1_MEETING_LIST_DESCR"] = "Accéder à la liste des salles de réunions";
$MESS["ITSRM1_MEETING_SEARCH"] = "Accéder à la recherche de salles de réunion";
$MESS["INTASK_C27T_CRAETE_TITLE"] = "Créer un point d'échanges";
$MESS["INTASK_C27T_CREATE"] = "Créer un point d'échanges";
$MESS["ITSRM1_MEETING_LIST"] = "Liste des salles de réunion";
?>