<?
$MESS["NTR_MAIL_PAGE_TITLE"] = "Intégration de boîte aux lettres";
$MESS["INTR_MAIL_B24_PAGE_TITLE"] = "Bitrix24.Mail";
$MESS["INTR_MAIL_DOMAIN_PAGE_TITLE"] = "Ajouter un domaine de messagerie";
$MESS["MAIL_MODULE_NOT_INSTALLED"] = "Le module Courrier n'est pas installé.";
$MESS["INTR_MAIL_UNAVAILABLE"] = "Les services de messagerie électronique ne sont pas disponibles. Veuillez contacter votre administrateur intranet. ";
$MESS["INTR_MAIL_INP_DOMAIN_EMPTY"] = "Le nom du domaine est requis.";
$MESS["INTR_MAIL_INP_TOKEN_EMPTY"] = "Le jeton d'accès est requis.";
$MESS["INTR_MAIL_INP_DOMAINTOKEN_BAD"] = "Le jeton d'accès indiqué ne correspond pas au domaine.";
$MESS["INTR_MAIL_INP_DOMAIN_WAIT"] = "En attente que le domaine termine l'attachement à la messagerie hébergée sur Yandex";
$MESS["INTR_MAIL_INP_DOMAIN_REMOVE"] = "Le domaine a été détaché avec succès.";
$MESS["INTR_MAIL_INP_PASSWORD2_BAD"] = "La confirmation du mot de passe ne correspond pas au mot de passe.";
$MESS["INTR_MAIL_SAVE_ERROR"] = "Erreur lors de la sauvegarde des données.";
$MESS["INTR_MAIL_CSRF"] = "Erreur de sécurité lors de l'envoi du formulaire.";
$MESS["INTR_MAIL_AUTH"] = "Erreur d'authentification";
$MESS["INTR_MAIL_FORM_ERROR"] = "Erreur lors du traitement du formulaire.";
$MESS["INTR_MAIL_AJAX_ERROR"] = "Erreur lors du traitement de la requête.";
$MESS["INTR_MAIL_CONTROLLER_INVALID"] = "Service indisponible.";
$MESS["INTR_MAIL_MANAGE_CREATE"] = "Créer";
$MESS["INTR_MAIL_MANAGE_CHANGE"] = "Modifier";
$MESS["INTR_MAIL_MANAGE_PASSWORD"] = "Modifier le mot de passe";
$MESS["INTR_MAIL_MANAGE_DELETE"] = "Supprimer la boîte aux lettres";
$MESS["INTR_MAIL_MAILBOX_OCCUPIED"] = "La boîte est utilisée par un autre utilisateur.";
$MESS["INTR_MAIL_DOMAIN_ICON"] = "b24mail-en.png";
$MESS["INTR_MAIL_IMAP_DIRS"] = "Sélectionnez les dossiers à synchroniser";
$MESS["INTR_MAIL_MAX_AGE_ERROR"] = "Veuillez préciser un intervalle de synchronisation.";
$MESS["INTR_MAIL_INP_EMAIL_BAD"] = "Adresse e-mail invalide";
$MESS["INTR_MAIL_IMAP_AUTH_ERR_EXT"] = "Erreur d’authentification. Veuillez vérifier que l'identifiant et le mot de passe sont corrects.<br>Notez que si vous utilisez les mots de passe de l'application et si la double authentification est activée, vous devez utiliser un mot de passe d'intégration spécial.";
$MESS["INTR_MAIL_IMAP_OAUTH_ACC"] = "Erreur lors de l'obtention des données des boîtes aux lettres";
?>