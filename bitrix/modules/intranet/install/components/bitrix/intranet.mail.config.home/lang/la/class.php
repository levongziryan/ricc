<?
$MESS["NTR_MAIL_PAGE_TITLE"] = "Integración de buzones";
$MESS["INTR_MAIL_B24_PAGE_TITLE"] = "Bitrix24.Mail";
$MESS["INTR_MAIL_DOMAIN_PAGE_TITLE"] = "Agregar dominio al correo electrónico";
$MESS["MAIL_MODULE_NOT_INSTALLED"] = "El módulo Mail no está instalado.";
$MESS["INTR_MAIL_UNAVAILABLE"] = "Los servicios de correo electrónico no están disponibles. Póngase en contacto con su administrador de intranet.";
$MESS["INTR_MAIL_INP_DOMAIN_EMPTY"] = "Se requiere un nombre de dominio.";
$MESS["INTR_MAIL_INP_TOKEN_EMPTY"] = "El token es requerido.";
$MESS["INTR_MAIL_INP_DOMAINTOKEN_BAD"] = "El token proporcionado no coincide con el dominio.";
$MESS["INTR_MAIL_INP_DOMAIN_WAIT"] = "Esperando que se complete la conexión del dominio a Yandex Hosted E-Mail";
$MESS["INTR_MAIL_INP_DOMAIN_REMOVE"] = "El dominio se ha desvinculado correctamente.";
$MESS["INTR_MAIL_INP_PASSWORD2_BAD"] = "Su contraseña y confirmación de contraseña no coinciden.";
$MESS["INTR_MAIL_SAVE_ERROR"] = "Error al guardar datos de conexión.";
$MESS["INTR_MAIL_CSRF"] = "Error de seguridad al enviar el formulario.";
$MESS["INTR_MAIL_AUTH"] = "Error de autenticación";
$MESS["INTR_MAIL_FORM_ERROR"] = "Error al procesar el formulario.";
$MESS["INTR_MAIL_AJAX_ERROR"] = "Error al procesar la solicitud.";
$MESS["INTR_MAIL_CONTROLLER_INVALID"] = "Servicio no disponible.";
$MESS["INTR_MAIL_MANAGE_CREATE"] = "Crear";
$MESS["INTR_MAIL_MANAGE_CHANGE"] = "Editar";
$MESS["INTR_MAIL_MANAGE_PASSWORD"] = "Cambiar contraseña";
$MESS["INTR_MAIL_MANAGE_DELETE"] = "Eliminar buzón";
$MESS["INTR_MAIL_MAILBOX_OCCUPIED"] = "Este buzón está en uso por otro usuario.";
$MESS["INTR_MAIL_IMAP_DIRS"] = "Seleccionar carpetas para sincronizar";
$MESS["INTR_MAIL_MAX_AGE_ERROR"] = "Especifique el intervalo de sincronización.";
$MESS["INTR_MAIL_INP_EMAIL_BAD"] = "Dirección de correo electrónico no válida";
$MESS["INTR_MAIL_IMAP_AUTH_ERR_EXT"] = "Error de autenticación. Compruebe que el inicio de sesión y la contraseña sean correctas. Tenga en cuenta que si está utilizando contraseñas de la aplicación y la autenticación en dos pasos está habilitada, debe utilizar una contraseña de integración especial.";
$MESS["INTR_MAIL_IMAP_OAUTH_ACC"] = "Error al obtener los datos del buzón";
$MESS["INTR_MAIL_DOMAIN_ICON"] = "b24mail-en.png";
?>