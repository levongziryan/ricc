<?
$MESS["INTRANET_OTP_PASS"] = "La primera línea de defensa: la contraseña";
$MESS["INTRANET_OTP_CODE"] = "La segunda línea de defensa: el código de verificación";
$MESS["INTRANET_OTP_GOTO"] = "Empezar";
$MESS["INTRANET_OTP_CLOSE"] = "Recordármelo más tarde";
$MESS["INTRANET_OTP_MANDATORY_TITLE"] = "Seguridad adicional para los datos de su compañía";
$MESS["INTRANET_OTP_MANDATORY_DESCR"] = "<p>En la actualidad, Bitrix24 está protegido por la tecnología de encriptación de datos y un par de nombres de usuario y contraseñas para cada usuario. Sin embargo, hay herramientas que un usuario malintencionado puede emplear para entrar en su computadora y robar estos datos. </p>
<p>El administrador del sistema ha habilitado la opción de seguridad adicional y ahora está pidiendo habilitar la autenticación de dos pasos.</p>
<p>La autenticación de dos pasos significa que usted tendrá que pasar dos niveles de
verificación al iniciar sesión. En primer lugar, usted tendrá que introducir la contraseña. A continuación, tendrá que introduzca un código de seguridad de una sola vez enviada a su dispositivo móvil.</p>
<p>Esto hará que sus datos y de negocios más seguro.</p>";
$MESS["INTRANET_OTP_MANDATORY_DESCR2"] = "<p>Usted tiene #NUM#. Usted no será capaz de acceder a su Bitrix24 si no habilita la autenticación de dos pasos hasta entonces.</ P>";
$MESS["INTRANET_OTP_CLOSE_FOREVER"] = "No mostrar de nuevo";
?>