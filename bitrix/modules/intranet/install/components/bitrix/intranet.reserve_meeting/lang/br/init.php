<?
$MESS["INTASK_C29_PERIOD_ADDITIONAL"] = "Adicional";
$MESS["INAF_F_PLACE"] = "Capacidade";
$MESS["INTASK_C29_UF_RES_TYPEA"] = "Consulta";
$MESS["INAF_F_DESCRIPTION"] = "Descrição";
$MESS["INTASK_C29_EVENT_LENGTH"] = "Duração";
$MESS["INAF_F_FLOOR"] = "Andar";
$MESS["INAF_F_ID"] = "ID";
$MESS["INTASK_C29_UF_PREPARE_ROOM"] = "Preparação da Sala de Reunião";
$MESS["INAF_F_NAME"] = "Nome";
$MESS["INTASK_C29_UF_RES_TYPEC"] = "Negociação";
$MESS["INTASK_C29_UF_RES_TYPED"] = "Outros";
$MESS["INTASK_C29_PERIOD_TYPE"] = "Tipo de período";
$MESS["INTASK_C29_PERIOD_COUNT"] = "Periodicidade";
$MESS["INTASK_C29_UF_PERSONS"] = "Pessoas";
$MESS["INAF_F_PHONE"] = "Telefone";
$MESS["INTASK_C29_UF_RES_TYPEB"] = "Apresentação";
$MESS["INTASK_C29_UF_RES_TYPE"] = "Tipo de Reunião";
?>