<?
$MESS["INTRANET_DISK_PROMO_TITLE_MACOS"] = "Application de bureau Bitrix24 pour MacOS";
$MESS["INTRANET_DISK_PROMO_TITLE_WINDOWS"] = "Application de bureau Bitrix24 pour Windows";
$MESS["INTRANET_DISK_PROMO_HEADER"] = "Renforcez votre Bitrix24 avec encore plus de fonctionnalités de gestion des fichiers.";
$MESS["INTRANET_DISK_PROMO_DESC"] = "De nos jours, vous pouvez accéder à Internet presque n'importe où et n'importe quand. Et pourtant, dans certaines situations il s'avère impossible de trouver un stockage de fichiers en ligne disponible: par exemple, dans un avion. Ceci ne signifie pas que vous devez interrompre votre travail! L'application de bureau Bitrix24 va synchroniser tous vos fichiers avec des copies locales sur votre ordinateur. Modifiez vos fichiers en hors ligne - tous les changements seront automatiquement synchronisés avec le cloud dès qu'une connexion Internet sera disponible.";
$MESS["INTRANET_DISK_PROMO_DESC_SUB"] = "Installez le programme sur votre ordinateur et continuez votre travail n'importe où et n'importe quand!";
$MESS["INTRANET_DISK_PROMO_STEP1_TITLE"] = "Téléchargez et installez le programme #LINK_START#Bitrix24#LINK_END#";
$MESS["INTRANET_DISK_PROMO_STEP2_TITLE"] = "Connectez Bitrix24.Drive";
$MESS["INTRANET_DISK_PROMO_STEP2_DESC"] = "Exécutez le programme pour synchroniser vos fichiers sans surveillance nécessaire. Vous pouvez sélectionner les dossiers selon votre choix qui seront disponibles localement ou dans le navigateur.";
?>