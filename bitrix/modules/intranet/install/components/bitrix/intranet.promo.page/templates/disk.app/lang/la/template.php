<?
$MESS["INTRANET_DISK_PROMO_TITLE_MACOS"] = "Aplicación Bitrix24 para MacOS";
$MESS["INTRANET_DISK_PROMO_TITLE_WINDOWS"] = "Aplicación Bitrix24 para Windows";
$MESS["INTRANET_DISK_PROMO_HEADER"] = "Potencie su Bitrix24 incluso con más características de administración de archivos.";
$MESS["INTRANET_DISK_PROMO_DESC"] = "Hoy en día podemos acceder a Internet casi en cualquier lugar en cualquier momento. Sin embargo, podemos encontrar situaciones en las que el acceso al almacenamiento de archivos en línea no está disponible: por ejemplo, en un avión. Esto no significa que usted tiene que interrumpir su trabajo! La aplicación de escritorio Bitrix24.Drive sincronizará todos sus archivos con copias locales en su computadora. Edite sus archivos sin conexión - todos los cambios se sincronizarán automáticamente con la nube tan pronto como la conexión a Internet esté disponible.";
$MESS["INTRANET_DISK_PROMO_DESC_SUB"] = "Instale la aplicación en su computadora y continúe su trabajo en cualquier lugar en cualquier momento!";
$MESS["INTRANET_DISK_PROMO_STEP1_TITLE"] = "Descargar e instalar #LINK_START#Bitrix24#LINK_END# aplicación";
$MESS["INTRANET_DISK_PROMO_STEP2_TITLE"] = "Conectar Bitrix24.Drive";
$MESS["INTRANET_DISK_PROMO_STEP2_DESC"] = "Ejecute la aplicación para sincronizar sus archivos de forma autónoma. Puede seleccionar específicamente las carpetas que estarán disponibles localmente o en el navegador.";
?>