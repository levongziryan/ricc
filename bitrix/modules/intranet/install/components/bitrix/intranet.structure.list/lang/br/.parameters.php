<?
$MESS["INTR_ISL_PARAM_NAV_TITLE"] = "Título Navegação em Trilha";
$MESS["INTR_ISL_PARAM_DETAIL_URL"] = "Página de Visualização de Detalhe";
$MESS["INTR_ISL_PARAM_FILTER_SECTION_CURONLY_VALUE_Y"] = "direcionar";
$MESS["INTR_ISL_PARAM_SHOW_ERROR_ON_NULL"] = "Exibir Aviso em Resultado Vazio";
$MESS["INTR_ISL_PARAM_NAV_TITLE_DEFAULT"] = "Funcionários";
$MESS["INTR_ISL_PARAM_FILTER_SECTION_CURONLY"] = "Filtro por Departamentos";
$MESS["INTR_ISL_PARAM_FILTER_NAME"] = "Nome do Filtro";
$MESS["INTR_ISL_GROUP_FILTER"] = "Parâmetros do Filtro";
$MESS["INTR_ISL_PARAM_NAME_TEMPLATE"] = "Formato de Nome";
$MESS["INTR_ISL_PARAM_FILTER_SECTION_CURONLY_VALYE_N"] = "recorrente";
$MESS["INTR_ISL_PARAM_SHOW_NAV_TOP"] = "Exibir trilha de Navegação Acima dos Resultados";
$MESS["INTR_ISL_PARAM_SHOW_NAV_BOTTOM"] = "Exibir trilha de Navegação Abaixo dos Resultados";
$MESS["INTR_ISL_PARAM_FILTER_1C_USERS"] = "Exibir Apenas Usuários Sincronizados com 1C";
$MESS["INTR_ISL_PARAM_SHOW_UNFILTERED_LIST"] = "Exibir Lista Não Filtrada";
$MESS["INTR_ISL_PARAM_USERS_PER_PAGE"] = "Usuários por página";
$MESS["INTR_ISL_PARAM_SHOW_DEP_HEAD_ADDITIONAL"] = "Mostrar supervisor quando o filtro departamento estiver ativo";
$MESS["INTR_ISL_PARAM_DISPLAY_USER_PHOTO"] = "Mostrar foto do usurio";
?>