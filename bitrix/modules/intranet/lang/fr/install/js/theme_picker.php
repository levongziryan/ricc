<?
$MESS["BITRIX24_THEME_DIALOG_TITLE"] = "Thèmes";
$MESS["BITRIX24_THEME_DIALOG_NEW_THEME"] = "Thème personnalisé";
$MESS["BITRIX24_THEME_CREATE_YOUR_OWN_THEME"] = "Créer un thème personnalisé";
$MESS["BITRIX24_THEME_REMOVE_THEME"] = "Supprimer le thème";
$MESS["BITRIX24_THEME_DIALOG_SAVE_BUTTON"] = "Enregistrer";
$MESS["BITRIX24_THEME_DIALOG_CANCEL_BUTTON"] = "Annuler";
$MESS["BITRIX24_THEME_DIALOG_CREATE_BUTTON"] = "Créer";
$MESS["BITRIX24_THEME_THEME_BG_COLOR"] = "Couleur d'arrière-plan";
$MESS["BITRIX24_THEME_THEME_BG_IMAGE"] = "Image d'arrière-plan";
$MESS["BITRIX24_THEME_THEME_TEXT_COLOR"] = "Couleur du texte";
$MESS["BITRIX24_THEME_THEME_LIGHT_COLOR"] = "Clair";
$MESS["BITRIX24_THEME_THEME_DARK_COLOR"] = "Sombre";
$MESS["BITRIX24_THEME_UPLOAD_BG_IMAGE"] = "Téléchargez une image d'arrière-plan";
$MESS["BITRIX24_THEME_DRAG_BG_IMAGE"] = "ou faites glisser une image ici";
$MESS["BITRIX24_THEME_WRONG_FILE_TYPE"] = "Le fichier n’est pas une image.";
$MESS["BITRIX24_THEME_FILE_SIZE_EXCEEDED"] = "Taille du fichier max. dépassée (#LIMIT#).";
$MESS["BITRIX24_THEME_WRONG_BG_COLOR"] = "Couleur d’arrière-plan incorrecte.";
$MESS["BITRIX24_THEME_EMPTY_FORM_DATA"] = "Sélectionner une image ou une couleur d’arrière-plan.";
$MESS["BITRIX24_THEME_UNKNOWN_ERROR"] = "Impossible de terminer l'action. Veuillez réessayer.";
$MESS["BITRIX24_THEME_SET_AS_DEFAULT"] = "Définir comme par défaut";
$MESS["BITRIX24_THEME_DEFAULT_THEME"] = "Par défaut";
?>