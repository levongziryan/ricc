<?
$MESS["UF_1C"] = "Utilisateur de 1C";
$MESS["UF_DEPARTMENT"] = "Services";
$MESS["UF_DISTRICT"] = "District";
$MESS["UF_INN"] = "NIF";
$MESS["UF_PHONE_INNER"] = "Numéro d'extension";
$MESS["UF_STATE_HISTORY"] = "Historique des statuts";
$MESS["UF_STATE_LAST"] = "Dernier statut";
$MESS["UF_SKYPE"] = "Skype";
$MESS["UF_TWITTER"] = "Twitter";
$MESS["UF_FACEBOOK"] = "Facebook";
$MESS["UF_LINKEDIN"] = "LinkedIn";
$MESS["UF_XING"] = "Xing";
$MESS["UF_WEB_SITES"] = "Autres sites internet";
$MESS["UF_SKILLS"] = "Compétences";
$MESS["UF_INTERESTS"] = "Intérêts";
?>