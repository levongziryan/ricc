<?
$MESS["INTR_PHP_L439"] = "Vous utilisez la version PHP #VERS#, pour fonctionner le module a besoin d'une version 5.0.0 ou plus récente. Veuillez mettre à jour PHP ou contactez le service d'appui technique de votre hébergement.";
$MESS["INTR_MODULE_NAME"] = "Intranet";
$MESS["INTR_MODULE_DESCRIPTION"] = "Intranet - portail d'entreprise";
$MESS["INTR_INSTALL_RATING_RULE"] = "Calculs de votes supplémentaires pour l'autorité de la structure de entreprise";
$MESS["INTR_UNINSTALL_TITLE"] = "Suppression de module Intranet";
$MESS["INTR_INSTALL_TITLE"] = "Installation de la solution";
?>