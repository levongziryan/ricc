<?
$MESS["INTRANET_USER_ADD_DESC"] = "#EMAIL_TO# - Adresse email du nouvel employé 
#LINK# - lien vers le portail";
$MESS["INTRANET_USER_INVITATION_DESC"] = "#EMAIL_TO# - Adresse email de l'utilisateur invité
#LINK# - lien vers la page d'activation du nouvel utilisateur";
$MESS["INTRANET_USER_ADD_MESSAGE"] = "#USER_TEXT#

#LINK#

Pour vous connecter, utilisez votre adresse email comme identifiant et le mot de passe suivant: #PASSWORD#";
$MESS["INTRANET_USER_INVITATION_MESSAGE"] = "#USER_TEXT#

#LINK#

Pour vous connecter, utilisez votre adresee email comme identifiant. Au cours de la première connexion sur le portail collaboratif vous devrez choisir un mot de passe.";
$MESS["INTRANET_USER_ADD_SUBJECT"] = "Vous avez été ajouté au portail corporatif";
$MESS["INTRANET_USER_ADD_NAME"] = "Ajout des employés (utilisateurs)";
$MESS["INTRANET_USER_INVITATION_SUBJECT"] = "Invitation à un portail de entreprise";
$MESS["INTRANET_USER_INVITATION_NAME"] = "Inviter des utilisateurs";
$MESS["INTRANET_MAILDOMAIN_NOCOMPLETE_NAME"] = "Finaliser l'ajout de domaine d'e-mail";
$MESS["INTRANET_MAILDOMAIN_NOCOMPLETE_DESC"] = "#EMAIL_TO# - E-Mail de l'administrateur
#LEARNMORE_LINK# - URL de documentation
#SUPPORT_LINK# - URL de l'assistance technique";
$MESS["INTRANET_MAILDOMAIN_NOCOMPLETE_SUBJECT"] = "Vous n'avez pas finalisé l'ajout de votre domaine";
$MESS["INTRANET_MAILDOMAIN_NOCOMPLETE_MESSAGE"] = "<p>Nous avons remarqué que vous avez tenté de créer un domaine de société mais n'avez jamais finalisé l'enregistrement.</p>

<p>Si vous avez besoin d'aide, ouvrez la page des paramètres d'e-mail pour obtenir des instructions détaillées sur la configuration de votre service d'e-mail et pour finaliser l'enregistrement de domaine Bitrix24.</p>

<p>Si vous voulez voir le déroulement habituel, nous pouvons mettre à votre disposition un certain nombre d'exemples d'utilisation de domaines d'e-mail. Les exemples mettront en relief la magie derrière l'enregistrement de domaine, la création de boîte mail et l'utilisation du tout avec Bitrix24.</p>

<p><a href=\"#LEARNMORE_LINK#\">En savoir plus</a></p>

<p>Pour toute question, veuillez contacter notre <a href=\"#SUPPORT_LINK#\">assistance</a>.</p>";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX_NAME"] = "Créer des boîtes mail dans un domaine";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX_DESC"] = "#EMAIL_TO# - E-Mail de l'administrateur
#LEARNMORE_LINK# - URL de documentation
#SUPPORT_LINK# - URL de l'assistance technique";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX_SUBJECT"] = "Votre e-mail professionnel dans Bitrix24";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX_MESSAGE"] = "<p>Votre domaine a bien été ajouté à Bitrix24, mais il ne contient pour le moment aucune boîte mail.</p>
<p>Nous pouvons mettre à votre disposition un certain nombre d'exemples d'utilisation de domaines d'e-mail. Grâce aux exemples vous apprendrez comment créer une boîte mail et comment l'utiliser dans Bitrix24.</p>

<p><a href=\"#LEARNMORE_LINK#\">En savoir plus</a></p>

<p>Pour toute question, veuillez contacter notre <a href=\"#SUPPORT_LINK#\">assistance</a>.</p>
";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_NAME"] = "Créer des boîtes mail pour les employés dans un domaine";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_DESC"] = "#EMAIL_TO# - E-Mail de l'administrateur
#LEARNMORE_LINK# - URL de documentation
#SUPPORT_LINK# - URL de l'assistance technique";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_SUBJECT"] = "E-mail professionnel pour vos employés";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_MESSAGE"] = "<p>Vous avez enregistré le domaine et créé votre boîte mail. Cependant vos employés ne savent pas qu'ils peuvent créer leur propre boîte mail dans le domaine de la société.</p>

<p>Partagez vos connaissances et aidez-les à apprendre à créer leur propre boîte mail, ou à enregistrer une boîte mail sur leur page Bitrix24.</p>

<p><a href=\"#LEARNMORE_LINK#\">Comment enregistrer des boîtes mail pour vos employés</a></p>

<p>Pour toute question, veuillez contacter notre <a href=\"#SUPPORT_LINK#\">assistance</a>.</p>";
$MESS["INTRANET_MAILDOMAIN_NOREG_NAME"] = "Enregistrer un domaine d'e-mail";
$MESS["INTRANET_MAILDOMAIN_NOREG_DESC"] = "#EMAIL_TO# - E-Mail de l'administrateur
#LEARNMORE_LINK# - URL de documentation
#SUPPORT_LINK# - URL de l'assistance technique";
$MESS["INTRANET_MAILDOMAIN_NOREG_SUBJECT"] = "Vous n'avez pas finalisé l'ajout de votre domaine";
$MESS["INTRANET_MAILDOMAIN_NOREG_MESSAGE"] = "<p>Il semblerait que vous alliez enregistrer un domaine pour votre société mais ne parveniez pas à choisir un bon nom.</p>

<p>Nous pouvons mettre à votre disposition un certain nombre d'exemples d'utilisation de domaines d'e-mail pour vous inspirer. Grâce aux exemples vous apprendrez comment créer une boîte mail et comment l'utiliser dans Bitrix24.</p>

<p><a href=\"#LEARNMORE_LINK#\">Comment enregistrer des boîtes mail pour vos employés</a></p>

<p>Pour toute question, veuillez contacter notre <a href=\"#SUPPORT_LINK#\">assistance</a>.</p>";
?>