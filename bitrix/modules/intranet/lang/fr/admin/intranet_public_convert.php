<?
$MESS["INTRANET_PUBLIC_CONVERT_TITLE"] = "Convertir les pages publiques";
$MESS["INTRANET_PUBLIC_CONVERT_DESC"] = "Le nouveau modèle Bitrix24 est livré avec une navigation à jour et améliorée et un nouveau petit menu de gauche personnalisé. Les utilisateurs peuvent maintenant personnaliser l'apparence et les sensations de chaque section. L'espace de travail est à présent 20% plus large. Le menu du haut arbore dorénavant un système de navigation unifié.";
$MESS["INTRANET_PUBLIC_CONVERT_DESC_TITLE"] = "Convertissez tout de suite vos pages publiques pour essayer dès maintenant les nouvelles fonctionnalités :";
$MESS["INTRANET_PUBLIC_CONVERT_DESC2"] = "<ul> 
<li>Menu de gauche facilement personnalisable</li>
<li>Tout le nécessaire à portée de main</li>
<li>Personnalisez vos outils les plus utilisés (GRC, Tâches ou Flux d'activités) adaptables pour correspondre aux différents groupes de clients</li>
<li>L'espace de travail est augmenté de 20% : réduisez le menu de gauche quand vous ne l'utilisez plus</li>
<li>Navigation du menu du haut unifiée : les menus du deuxième tiers partagent une structure commune</li>
<li>Trouver plus vite les pages en saisissant leur nom dans la barre de recherche</li>
</ul>";
$MESS["INTRANET_PUBLIC_CONVERT_OPTIONS_TITLE"] = "Convertissez tout de suite vos pages publiques";
$MESS["INTRANET_PUBLIC_CONVERT_OPTIONS_DESC"] = "(Il est recommandé de créer une copie de sauvegarde de vos pages publiques au cas où vous souhaiteriez revenir plus tard à la structure précédente)";
$MESS["INTRANET_PUBLIC_CONVERT_SECTIONS"] = "Sections (\"Temps et rapports\", \"Calendrier\", \"Flux de travail\" et \"Bitrix24.Drive\" seront créées)";
$MESS["INTRANET_PUBLIC_CONVERT_INDEX"] = "Transformez n'importe quelle section en page d'accueil (en écrasant le fichier index.php et en copiant le Flux d'activités dans le dossier /stream/)";
$MESS["INTRANET_PUBLIC_CONVERT_TAB"] = "Conversion";
$MESS["INTRANET_PUBLIC_CONVERT_TAB_TITLE"] = "Éditer les préférences de conversion";
$MESS["INTRANET_PUBLIC_CONVERT_BUTTON"] = "Convertir maintenant";
$MESS["INTRANET_PUBLIC_CONVERT_FINISH"] = "La conversion est terminée.";
$MESS["INTRANET_PUBLIC_SKIP_CONVERT_BUTTON"] = "Ne pas convertir";
$MESS["INTRANET_PUBLIC_SKIP_CONVERT_FINISH"] = "La conversion a été annulée";
?>