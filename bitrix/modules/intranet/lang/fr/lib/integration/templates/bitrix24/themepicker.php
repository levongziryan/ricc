<?
$MESS["INTRANET_B24_INTEGRATION_CANT_CREATE_THEME"] = "Impossible de créer un thème à partir des données fournies";
$MESS["INTRANET_B24_INTEGRATION_UPLOAD_ERROR"] = "Erreur lors du téléversement du fichier.";
$MESS["INTRANET_B24_INTEGRATION_THEMES_LIMIT_EXCEEDED"] = "Le maximum de thèmes personnalisés est dépassé (#NUM#).";
?>