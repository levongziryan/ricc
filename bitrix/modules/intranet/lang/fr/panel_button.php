<?
$MESS["SOL_BUTTON_TEST_TEXT"] = "Ajouter#BR#une subdivision";
$MESS["SOL_BUTTON_TEST_TEXT_HINT"] = "Démarrage de la création de la subdivision.";
$MESS["SOL_BUTTON_TEST_MENU_HINT"] = "Cliquez sur ce bouton pour ajouter un nouveau département, supprimer ce bouton du Panneau de configuration ou bien passer à un autre département.";
$MESS["SOL_BUTTON_GOTOSITE"] = "Accéder au site";
$MESS["SOL_BUTTON_TEST_TITLE"] = "Créer un nouveau site et lancer l'assistant d'installation du nouveau site du service";
?>