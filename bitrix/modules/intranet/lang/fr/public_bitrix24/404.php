<?
$MESS["ERROR_404_TITLE"] = "Erreur 404";
$MESS["ERROR_404_TEXT1"] = "Malheureusement la page demandée<br> est introuvable.";
$MESS["ERROR_404_TEXT2"] = "Si vous considérez que c'est une anomalie<br>, veuillez contacter l'administrateur du site web.";
?>