<?
$MESS["CONFIG_USERS2"] = "Employés:";
$MESS["CONFIG_INVITED_USERS"] = "Est invité:";
$MESS["demo"] = "Mode démo";
$MESS["LICENSE_FEATURE_TASKS"] = "Tâches & Projets";
$MESS["LICENSE_FEATURE_CALENDAR"] = "Calendriers";
$MESS["LICENSE_FEATURE_CRM"] = "CRM";
$MESS["LICENSE_FEATURE_MEETING"] = "Réunion & Briefings";
$MESS["LICENSE_FEATURE_WORKREPORT"] = "Rapports d'activité";
$MESS["LICENSE_FEATURE_DOMAIN"] = "Son propre nom de domaine";
$MESS["LICENSE_PRICE_PROJECT"] = "Gratuit !";
$MESS["LICENSE_PRICE_TEAM"] = "99  / mois";
$MESS["LICENSE_PRICE_COMPANY"] = "199  / mois";
$MESS["LICENSE_PROLONG"] = "Prolonger";
$MESS["LICENSE_GO"] = "Acheter maintenant!";
$MESS["LICENSE_BUY"] = "Acheter maintenant!";
$MESS["LICENSE_HISTORY"] = "Historique des commandes";
$MESS["CONFIG_LICENSE_TEST"] = "Mode démo";
$MESS["CONFIG_LICENSE_TEST_START"] = "Activer";
$MESS["CONFIG_LICENSE_TEST_FINISH"] = "Désactiver";
$MESS["CONFIG_LICENSE_TEST_DAYS_LEFT"] = "Il reste #NUM#.";
$MESS["LICENSE_ORDER"] = "Commander";
$MESS["corporation"] = "Entreprise";
$MESS["EMPTY_FIELD"] = "Champs obligatoires '#NAME#'";
$MESS["FIELD_name"] = "Prénom";
$MESS["FIELD_last_name"] = "Nom";
$MESS["CORPORATION_ORDER_CALL"] = "Vous souhaitez être rappelé(e)";
$MESS["CONFIG_DISC_CORPORATION"] = "10 Gb par utilisateur";
$MESS["CONFIG_USER_CORPORATION"] = "Partir de 250 utilisateurs";
$MESS["LICENSE_FEATURE_IP"] = "Accès limité par l'IP";
$MESS["LICENSE_FEATURE_ADIT_SUPPORT"] = "Assistance étendue";
$MESS["LICENSE_FEATURE_LOCAL_COPY"] = "Sauvegarde locale";
$MESS["LICENSE_FEATURE_ADIT"] = "Disponible pour un coût supplémentaire:";
$MESS["CONFIG_USERS_COUNT"] = "Utilisateurs";
$MESS["CONFIG_USERS"] = "Utilisateurs:";
$MESS["CONFIG_EXTRANET_USERS"] = "Utilisateurs d'Extranet";
$MESS["CONFIG_USERS_FREE"] = "Comptes d'utilisateurs libres";
$MESS["CONFIG_DISC_SPACE"] = "Disque dans le cloud";
$MESS["CONFIG_DISC_SPACE_LIMIT"] = "Espace disque";
$MESS["CONFIG_DISC_USAGE"] = "Espace disque occupé";
$MESS["CONFIG_DISC_SPACE_FREE"] = "Espace libre";
$MESS["CONFIG_DISC_SPACE_GB"] = "Gb";
$MESS["CONFIG_NO_LIMIT"] = "Illimité";
$MESS["CONFIG_12_LIMIT"] = "12 utilisateurs";
$MESS["CONFIG_SAVE_SUCCESSFULLY"] = "La mise à jour des paramètres a été effectuée avec succès.";
$MESS["CONFIG_LICENSE_NAME"] = "Abonnement";
$MESS["CONFIG_LICENSE_TILL"] = "Jusqu'à #LICENSETILL#";
$MESS["CONFIG_SAVE"] = "Sauvegarder";
$MESS["CONFIG_ACTIVATE"] = "Activer";
$MESS["project"] = "Free";
$MESS["team"] = "Standard";
$MESS["company"] = "Professional";
$MESS["nfr"] = "NFR";
$MESS["LICENSE_FEATURE_TIMEMAN"] = "Gestion du temps";
$MESS["CONFIG_LICENSE_TEST_START_DESCRIPTION"] = "Vous pouvez tester les possibilités du tarif 'entreprise' durant une période de 30 jours";
$MESS["CONFIG_LICENSE_TEST_FINISH_DESCRIPTION"] = "Vous êtes sur la démo-version du tarif 'Professional'.";
$MESS["CONFIG_LICENSE_TEST_FINISH_DESCRIPTION_CONFIRM"] = "Attention ! Vous ne pourrez plus utiliser le mode démo après l'avoir quitté.";
$MESS["FIELD_post_info"] = "Fonction";
$MESS["FIELD_phone"] = "Téléphone";
$MESS["FIELD_phone_time"] = "Heure souhaitée pour le rappel";
$MESS["FIELD_company"] = "Entreprise";
$MESS["FIELD_empl_num"] = "Nombre d'employés";
$MESS["FIELD_questions"] = "Questions que vous souhaiteriez voir abordées";
$MESS["FIELD_law"] = "J'ai lu et pris connaissance de <a target='blank' href='http://www.bitrix24.ru/anketa_info.php'>terms and regulations</a>";
$MESS["REQUEST_OK"] = "Merci, votre demande va être traitée. Nous vous contacterons au moment qui vous convient le mieux.";
$MESS["LICENSE_FEATURE_LDAP"] = "Integration avec  AD/LDAP et MS Exchange";
$MESS["LICENCE_12_USERS_ADD"] = "Ajouter plus d'utilisateurs";
$MESS["LICENCE_SPACE_ADD"] = "Ajouter un emplacement de stockage en ligne";
$MESS["CONFIG_TITLE"] = "Information sur l'abonnement";
$MESS["CONFIG_USERS_COUNT_LIMIT"] = "Nombre maximal d'utilisateurs";
$MESS["CONFIG_DB_USAGE"] = "Taille de la base de donnée";
$MESS["CONFIG_HEADER_SETTINGS"] = "Paramètres";
$MESS["CONFIG_COMPANY_NAME"] = "Nom de l'entreprise";
$MESS["CONFIG_LICENSE_ACTIVATE"] = "Activer le coupon";
$MESS["CONFIG_COUPON"] = "Veuillez saisir le code coupon";
$MESS["CONFIG_ACTIVATE_ERROR"] = "Erreur d'activation. Veuillez contacter le service commercial.";
$MESS["LICENSE_FEATURE_ADIT_INFO"] = "<ul style='margin: 0; padding: 0; margin-top: 10px; margin-left: 28px; line-height: 17px;'><li>Branding</li><li>Private cloud</li><li>Access via VPN</li></ul><a href='http://www.bitrix24.com/prices/enterprise.php' style='margin-left: 11px;'>And more...</a>";
$MESS["CONFIG_EXTRANET_USERS2"] = "Utilisateurs externes (extranet):";
$MESS["LICENSE_DEMO"] = "Activer la version d'essai 30 jours gratuites";
$MESS["CONFIG_LICENSE_TEST_ALL_TARIFF"] = "Mettre à niveau";
$MESS["LICENSE_TF_FEATURES_DESC"] = "<div class=\"bpp-cnr\">
			<div class=\"bpp-title\">
				Obtenez plus avec Bitrix24 Plus
			</div>
			<div class=\"bpp-content-cnr\">
				<div class=\"bpp-content-header-cnr\">
					<div class=\"bpp-content-header-item-cnr\">
						<div class=\"bpp-content-header-item-title\">Outils commerciaux:</div>
						<div class=\"bpp-content-header-item-value\">24 utilisateurs</div>
					</div>
					<div class=\"bpp-content-header-item-cnr\">
						<div class=\"bpp-content-header-item-title\">Stockage en dématérialisé:</div>
						<div class=\"bpp-content-header-item-value\">24GB</div>
					</div>
					<div class=\"bpp-content-header-item-cnr\">
						<div class=\"bpp-content-header-item-title\">Coût:</div>
						<div class=\"bpp-content-header-item-value\">#PRICE#</div>
					</div>
				</div>
				<div class=\"bpp-content-body-cnr\">
					<div class=\"bpp-content-body-item-cnr\">
						<div class=\"bpp-content-body-item-icon crm\"></div>
						<div class=\"bpp-content-body-item-title\">CRM</div>
						<div class=\"bpp-content-body-item-advantages-cnr\">
							<ul class=\"bpp-content-list\">
								<li class=\"bpp-content-list-item\">Historique CRM</li>
								<li class=\"bpp-content-list-item\">Journal des accès</li>
								<li class=\"bpp-content-list-item\">Recherche de doubles étendue</li>
								<li class=\"bpp-content-list-item\">et plus encore</li>
							</ul>
						</div>
					</div>
					<div class=\"bpp-content-body-item-cnr\">
						<div class=\"bpp-content-body-item-icon phone\"></div>
						<div class=\"bpp-content-body-item-title\">Téléphonie</div>
						<div class=\"bpp-content-body-item-advantages-cnr\">
							<ul class=\"bpp-content-list\">
								<li class=\"bpp-content-list-item\">Enregistrement d'appels illimité</li>
								<li class=\"bpp-content-list-item\">Rapports sur les sources des appels</li>
								<li class=\"bpp-content-list-item\">Évaluation de la qualité des appels</li>
								<li class=\"bpp-content-list-item\">et plus encore</li>
							</ul>
						</div>
					</div>
					<div class=\"bpp-content-body-item-cnr\">
						<div class=\"bpp-content-body-item-icon task\"></div>
						<div class=\"bpp-content-body-item-title\">Tâches</div>
						<div class=\"bpp-content-body-item-advantages-cnr\">
							<ul class=\"bpp-content-list\">
								<li class=\"bpp-content-list-item\">Dépendances des tâches</li>
								<li class=\"bpp-content-list-item\">et plus encore</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>";
$MESS["CONFIG_DISC_GB"] = "#NUM#GB";
$MESS["CONFIG_DISC_SPACE_LIMIT_DESC"] = "(mises-à-jour quotidiennes des données)";
$MESS["CONFIG_LICENSE_COUPON_DESC"] = "Utilisez des coupons pour:<br>
1) activer un abonnement payant;<br>
2) étendre un abonnement payant de 1, 6 ou 12 mois (en respect de votre choix précédent);<br>
3) changer d'offre;<br>
4) augmenter la taille du lecteur cloud;<br>
5) recharger le solde de votre téléphone;
6) installer des solutions commerciales du Marché.";
$MESS["tf"] = "Plus";
$MESS["ftf"] = "24x24";
$MESS["LICENSE_6_12_DISCOUNT"] = "Réduction de 5% à 10% lors du paiement d'un abonnement 6 ou 12 mois";
$MESS["LICENSE_TITLE"] = "Choisissez une offre Bitrix24";
$MESS["LICENSE_MAIN_DESC"] = "Accédez aux fonctionnalités téléphoniques et CRM avancées en passant à un abonnement Bitrix24 commercial.
<br/><br/>Comparer les offres";
$MESS["LICENSE_TARIFF"] = "Tarifs";
$MESS["LICENSE_FEATURE_PRICE"] = "Prix";
$MESS["LICENSE_FEATURE_USERS"] = "Utilisateurs";
$MESS["LICENSE_FEATURE_BUSINESS_USERS"] = "Utilisateurs d'outils commerciaux";
$MESS["LICENSE_FEATURE_SPACE"] = "Espace disque";
$MESS["LICENSE_FEATURES_TITLE"] = "Fonctionnalités";
$MESS["LICENSE_FEATURES_SOCNET_TITLE"] = "Outils de communication";
$MESS["LICENSE_FEATURES_SOCNET_TITLE2"] = "Communications";
$MESS["LICENSE_FEATURES_BUSINESS_TOOLS_TITLE"] = "Outils commerciaux";
$MESS["LICENSE_FEATURES"] = "Fonctionnalités";
$MESS["LICENSE_FEATURE_LIFE_FEED"] = "Flux d'activités";
$MESS["LICENSE_FEATURE_IM"] = "Chat et Appels";
$MESS["LICENSE_FEATURE_MAIL"] = "Courrier";
$MESS["LICENSE_FEATURE_NETWORK"] = "Réseau";
$MESS["LICENSE_FEATURE_HR"] = "RH";
$MESS["LICENSE_FEATURE_MOBILE"] = "Appli mobile";
$MESS["LICENSE_FEATURE_DISK"] = "Drive";
$MESS["LICENSE_FEATURE_TELEPHONY"] = "Téléphonie";
$MESS["LICENSE_FEATURE_BP"] = "Automatisation du flux de travail";
$MESS["LICENSE_FEATURE_LOGO24"] = "Retirez le logo Bitrix24";
$MESS["LICENSE_FEATURE_LOGO"] = "Insérez votre propre logo";
$MESS["LICENSE_FEATURE_BACKUP"] = "Restauration des sauvegardes";
$MESS["LICENSE_FEATURE_LOGS"] = "Accès aux journaux et relevés des accès";
$MESS["LICENSE_FEATURE_DOMEN"] = "Utilisez votre propre domaine";
$MESS["LICENSE_SETTINGS_TITLE"] = "Paramètres";
$MESS["LICENSE_CRM_TITLE"] = "CRM avancée";
$MESS["LICENSE_ADD_TITLE"] = "Services premium";
$MESS["LICENSE_ADD_DESC"] = "Il peut vous arriver d'avoir besoin de l'assistance des techniciens Bitrix24  pour les services premium disponibles uniquement pour les utilisateurs commerciaux";
$MESS["LICENSE_CRM_FULL"] = "CRM avancée";
$MESS["LICENSE_CRM_DESC"] = "La CRM avancée vous donne des options supplémentaires comme l'historique des modifications, la restauration, le journal d'accès CRM, les conversions d'enregistrement CRM, et bien plus encore.";
$MESS["LICENSE_CRM_FEATURE_1"] = "CRM cœur: Prospects, Contacts, Sociétés, Transactions";
$MESS["LICENSE_CRM_FEATURE_2"] = "Devis";
$MESS["LICENSE_CRM_FEATURE_3"] = "Factures";
$MESS["LICENSE_CRM_FEATURE_4"] = "Catalogue des produits";
$MESS["LICENSE_CRM_FEATURE_5"] = "Import";
$MESS["LICENSE_CRM_FEATURE_6"] = "E-mail au client";
$MESS["LICENSE_CRM_FEATURE_7"] = "Planification";
$MESS["LICENSE_CRM_FEATURE_8"] = "Rapports";
$MESS["LICENSE_CRM_FEATURE_9"] = "Entonnoir des ventes";
$MESS["LICENSE_CRM_FEATURE_10"] = "Intégration de la téléphonie";
$MESS["LICENSE_CRM_FEATURE_11"] = "Intégration à la boutique en ligne";
$MESS["LICENSE_CRM_FEATURE_12"] = "Automatisation du flux de travail";
$MESS["LICENSE_CRM_FEATURE_13"] = "Conversions d'enregistrement CRM";
$MESS["LICENSE_CRM_FEATURE_14"] = "Recherche de doubles avancée";
$MESS["LICENSE_CRM_FEATURE_15"] = "Historique des modifications et restauration";
$MESS["LICENSE_CRM_FEATURE_16"] = "Journal d'accès CRM";
$MESS["LICENSE_CRM_FEATURE_17"] = "Lister les vues dépassant 5000 enregistrements";
$MESS["LICENSE_CURRENCY_RUR"] = "#NUM# RUB";
$MESS["LICENSE_CURRENCY_EUR"] = "#NUM#&euro;";
$MESS["LICENSE_CURRENCY_USD"] = "#NUM#\$.";
$MESS["DEMO_TITLE"] = "Version d'essai 30 jours gratuite ";
$MESS["CONFIG_LICENSE_TEST_NOT_ALLOWED"] = "Vous avez déjà utilisé la version d'essai de l'offre Professional";
$MESS["DEMO_DESC"] = "Activez la version d'essai 30 jours gratuite pour accéder à toutes les fonctionnalités de l'offre Professional de Bitrix24 ";
$MESS["DEMO_START"] = "Activer l'offre d'essai";
$MESS["DEMO_DESC2"] = "Les fonctionnalités suivantes seront activées:";
$MESS["DEMO_FEATURE_1"] = "Utilisateurs illimités";
$MESS["DEMO_FEATURE_2"] = "Espace de stockage illimité";
$MESS["DEMO_FEATURE_3"] = "CRM avancée";
$MESS["DEMO_FEATURE_4"] = "Automatisation du flux de travail";
$MESS["DEMO_FEATURE_5"] = "Suivi des heures";
$MESS["DEMO_FEATURE_6"] = "Rapports d'activité";
$MESS["DEMO_FEATURE_7"] = "Réunions et briefings";
$MESS["DEMO_FEATURE_8"] = "et plus encore";
$MESS["DEMO_INFO_TITLE"] = "Et après la fin de l'offre d'essai ?";
$MESS["DEMO_INFO_DESC"] = "Une fois l'offre d'essai terminée, votre compte reviendra automatiquement à l'offre Gratuite. <b>Toutes les données seront conservées</b> (pour tout outil avancé utilisé pendant l'offre d'essai), mais leur accès <b>pourra être limité</b>. L'accès à ces données sera automatiquement rétabli quand vous passerez à une offre Bitrix24 appropriée.";
$MESS["DEMO_INFO_1"] = "Utilisateurs";
$MESS["DEMO_INFO_2"] = "Enregistrements des appels téléphoniques";
$MESS["DEMO_INFO_3"] = "Stockage cloud";
$MESS["DEMO_INFO_4"] = "Limite des enregistrements des vues CRM";
$MESS["DEMO_INFO_DESC_1"] = "Si vous avez invité plus de 12 utilisateurs pendant la version d'essai, une fois cette dernière terminée seuls les 12 premiers utilisateurs auront accès à leur compte Bitrix24. Si vous voulez rétablie les accès de tous les utilisateurs, vous devez <a class=\"link\" href=\"/settings/license_all.php\">changer d'offre</a>. ";
$MESS["DEMO_INFO_DESC_2"] = "La limite des enregistrements d'appels téléphoniques est levée pendant l'offre d'essai. La limite de l'offre gratutie est de 100 enregistrements par mois, donc si vous avez enregistré 40 appels pendant l'offre d'essai, vous pourrez encore en enregistrer 60 d'ici la fin du mois. Pour lever la limite, vous devez <a class=\"link\" href=\"/settings/license_all.php\">changer d'offre</a>.";
$MESS["DEMO_INFO_DESC_4"] = "Toutes les offres de Bitrix24 ont accès aux enregistrements CRM illimités. L'offre gratuite, par contre, dispose d'une limite de 5000. Si vous avez ajouté plus de 5000 enregistrements pendant l'offre d'essai, ils seront tous accessibles mais votre limite de vue de liste sera configurée à pas plus de 5000 enregistrements à la fois. Pour pouvoir lever cette limite, vous devez <a class=\"link\" href=\"/settings/license_all.php\">changer d'offre</a>.";
$MESS["LICENSE_BILLING_CURRENCY"] = "Afficher les prix en:";
$MESS["LICENSE_BILLING_CURRENCY_RUR"] = "Rouble russe";
$MESS["LICENSE_BILLING_CURRENCY_USD"] = "Dollars américains";
$MESS["LICENSE_BILLING_CURRENCY_EUR"] = "Euro";
$MESS["LICENSE_BILLING_CURRENCY_BRL"] = "Réal brésilien";
$MESS["LICENSE_BILLING_CURRENCY_CNY"] = "Yuan Renminbi chinois";
$MESS["LICENSE_BILLING_CURRENCY_INR"] = "Roupie indienne";
$MESS["LICENSE_BILLING_CURRENCY_UAH"] = "Hryvnia ukrainienne";
$MESS["LICENSE_PRICE_AUTO"] = "#PRICE#/mo";
$MESS["LICENSE_CRM_SHOW_MORE"] = "Afficher les fonctionnalités CRM des diverses offres";
$MESS["LICENSE_TEL_SHOW_MORE"] = "Afficher les fonctionnalités de téléphonie des diverses offres";
$MESS["LICENSE_TASKS_SHOW_MORE"] = "Afficher les fonctionnalités de tâche des diverses offres";
$MESS["LICENSE_HIDE_MORE"] = "masquer le tableau";
$MESS["LICENSE_TARIFF_BACK"] = "retour aux offres";
$MESS["LICENSE_TARIFF_COMPARE"] = "comparer les offres";
$MESS["LICENSE_EXTENDED_TOOLS"] = "Outils avancés";
$MESS["LICENSE_CHOOSE_TARIFF"] = "Sélectionner une offre";
$MESS["CONFIG_USERS_COUNT_BUSINESS_TOOLS_LIMIT"] = "A accès aux outils commerciaux:";
$MESS["CONFIG_USERS_BUSINESS_TOOLS_COUNT"] = "Peut actuellement utiliser les outils commerciaux:";
$MESS["DEMO_POPUP_TITLE"] = "Partager les actus avec tout le monde ?";
$MESS["DEMO_POPUP_SHARE"] = "Partager les nouvelles";
$MESS["DEMO_POPUP_CLOSE"] = "Non, merci";
$MESS["startup"] = "StartUp";
$MESS["startup_25"] = "StartUp25";
$MESS["LICENSE_FEATURE_TIMEREPORT"] = "Feuilles de temps";
$MESS["LICENSE_FEATURE_SOON"] = "bientôt";
$MESS["LICENSE_CRM_FEATURE_18"] = "Numéroteur progressif (bientôt)";
$MESS["LICENSE_CRM_FEATURE_20"] = "E-mails de déclenchement (bientôt)";
$MESS["LICENSE_CRM_FEATURE_21"] = "Documents CRM avancés (bientôt)";
$MESS["LICENSE_CRM_FEATURE_22"] = "et bien plus encore(bientôt)";
$MESS["LICENSE_TEL_TITLE"] = "Téléphonie Bitrix24 avancée";
$MESS["LICENSE_TEL_FULL"] = "Téléphonie avancée";
$MESS["LICENSE_TEL_DESC"] = "La téléphonie avancée vous permet d'enregistrer plus de 100 appels par mois, de suivre divers canaux de marketing et d'accéder à d'autres outils";
$MESS["LICENSE_TEL_FEATURE_2"] = "Location de numéro de téléphone local et sans frais";
$MESS["LICENSE_TEL_FEATURE_3"] = "Numéro de téléphone relai";
$MESS["LICENSE_TEL_FEATURE_4"] = "Nombre de lignes entrantes illimité";
$MESS["LICENSE_TEL_FEATURE_5"] = "Routage d'appels téléphoniques";
$MESS["LICENSE_TEL_FEATURE_6"] = "Transfert d'appels";
$MESS["LICENSE_TEL_FEATURE_7"] = "Intégration CRM";
$MESS["LICENSE_TEL_FEATURE_8"] = "Intégration PBX";
$MESS["LICENSE_TEL_FEATURE_9"] = "Prise en charge des casques téléphoniques SIP/VoIP";
$MESS["LICENSE_TEL_FEATURE_11"] = "Extensions employés";
$MESS["LICENSE_TEL_FEATURE_12"] = "Heures de téléphone";
$MESS["LICENSE_TEL_FEATURE_13"] = "Répondeur et messages d'accueil";
$MESS["LICENSE_TEL_FEATURE_14"] = "Enregistrement des appels (jusqu'à 100 par mois)";
$MESS["LICENSE_TEL_FEATURE_15"] = "Enregistrement illimité des appels";
$MESS["LICENSE_TEL_FEATURE_16"] = "Appel simultané à tous les employés disponibles";
$MESS["LICENSE_TEL_FEATURE_17"] = "Évaluation de la qualité des appels";
$MESS["LICENSE_TEL_FEATURE_18"] = "Pistage de la source des appels (CRM)";
$MESS["LICENSE_TEL_FEATURE_19"] = "Rapports et analytique des appels";
$MESS["LICENSE_TEL_FEATURE_20"] = "IVR (bientôt)";
$MESS["LICENSE_TEL_FEATURE_21"] = "et bien plus encore(bientôt)";
$MESS["LICENSE_ADD_FEATURE_1"] = "Extension de domaine gratuite pour le courrier";
$MESS["LICENSE_ADD_FEATURE_2"] = "Désactivation OTP (Admin uniquement)";
$MESS["LICENSE_ADD_FEATURE_3"] = "Transfert de domaine pour le courrier";
$MESS["LICENSE_ADD_FEATURE_4"] = "Changer du nom de compte Bitrix24";
$MESS["LICENSE_ADD_FEATURE_5"] = "Déplacer un compte Bitrix24 vers une autre zone de domaine";
$MESS["LICENSE_TASKS_TITLE"] = "Tâches avancées";
$MESS["LICENSE_TASKS_DESC"] = "En plus de l'offre gratuite, essayez d'utiliser les tâches avancées pour établir des dépendances de tâches et déplacer toutes les contraintes temporelles en même temps. Disponible uniquement dans les offres commerciales de Bitrix24.";
$MESS["LICENSE_TASKS_FULL"] = "Tâches avancées";
$MESS["LICENSE_TASKS_FEATURE_1"] = "Contrôle des contraintes temporelles";
$MESS["LICENSE_TASKS_FEATURE_2"] = "Liste de contrôle";
$MESS["LICENSE_TASKS_FEATURE_3"] = "Rappels";
$MESS["LICENSE_TASKS_FEATURE_4"] = "Délégation";
$MESS["LICENSE_TASKS_FEATURE_5"] = "Suivi des heures";
$MESS["LICENSE_TASKS_FEATURE_6"] = "Modèles de tâches";
$MESS["LICENSE_TASKS_FEATURE_7"] = "Groupes de travail de projet";
$MESS["LICENSE_TASKS_FEATURE_8"] = "Évaluation des tâches terminées";
$MESS["LICENSE_TASKS_FEATURE_9"] = "Rapports";
$MESS["LICENSE_TASKS_FEATURE_10"] = "Intégration CRM, flux de travail, calendriers et Bitrix24.Drive";
$MESS["LICENSE_TASKS_FEATURE_11"] = "E-mail vers tâche";
$MESS["LICENSE_TASKS_FEATURE_12"] = "Organigramme de Gantt";
$MESS["LICENSE_TASKS_FEATURE_13"] = "Restriction des tâches aux jours/heures de bureau";
$MESS["LICENSE_TASKS_FEATURE_14"] = "Quatre types de dépendances de tâche";
$MESS["LICENSE_PRICE_TF"] = "39\$/mois";
$MESS["LICENSE_FREE"] = "Gratuit";
$MESS["LICENSE_FULL_COMPARE"] = "Comparer les offres";
$MESS["LICENSE_FULL_COMPARE_LINK"] = "https://www.bitrix24.com/prices/";
$MESS["LICENSE_DEMO2"] = "Activer la démo 30 jours";
$MESS["LICENSE_MORE_FEATURES"] = "Ajouter à mon Bitrix24";
$MESS["LICENSE_MORE_FEATURES_DESC"] = "Ces fonctionnalités utiles (et bien d'autres encore) pour votre &#8220;Bitrix24&#8221; sont disponibles à partir de l'offre Plus pour seulement #PRICE#/mois.";
$MESS["LICENSE_MORE_USERS"] = "Plus d'utilisateurs";
$MESS["LICENSE_MORE_SPACE"] = "Plus d'espace sur le cloud";
$MESS["LICENSE_TEL_BALANCE"] = "Votre solde";
$MESS["LICENSE_TEL_BALANCE_ADD"] = "Ajouter au solde";
$MESS["LICENSE_TEL_REMOVE_RESTR"] = "Retirer les limites";
$MESS["LICENSE_TEL_RECORDS"] = "Enregistrement des appels:  <b>#NUM# sur #LIMIT# ce mois</b>";
$MESS["LICENSE_TEL_BALANCE_UA"] = "Le solde actuel disponible sur le compte personnel de chaque opérateur";
$MESS["DEMO_INFO_DESC_3"] = "Si vous avez utilisé plus de 5GB d'espace de stockage dématérialisé pendant l'offre d'essai, toutes les données seront conservées, mais vous ne pourrez plus ajouter de nouveaux documents, seulement lire ceux déjà existants. Pour pouvoir ajouter plus de documents, vous pouvez en supprimer des indésirables pour rester sous la limite de 5GB ou vous pouvez <a class=\"link\" href=\"/settings/license_all.php\">changer d'offre</a>. 
";
$MESS["DEMO_POPUP_TEXT"] = "Merci d'avoir activé l'offre d'essai 30 jours pour Bitrix24 Professional<br/><br/>
Dites à vos collègues de profiter de la plupart des nouvelles fonctionnalités excitantes.<br/><br/>
Nous avons préparé pour vous un modèle de message que vous pouvez envoyer directement ou modifier comme il vous plait.";
$MESS["DEMO_IMPORTANT_TITLE"] = "L'offre d'essai 30 jours pour Birtix24 Professional a été activée";
$MESS["DEMO_IMPORTANT_TEXT"] = "Chers amis !

Nous venons d'activer l'offre d'essai de Bitrix24 Professional pour pouvoir tester toutes les fonctionnalités disponibles. Essayez les nouveaux outils de productivité et laissez un commentaire dans le Flux d'activités.

Jetez un œil aux fonctionnalités premium:
[LIST=1]
[*]Aucune restriction sur le nombre d'employés pouvant utiliser Bitrix24; pas de restriction sur l'espace cloud.
[*]La gestion du temps de travail est activée: ce que vous voyez en haut n'est pas juste une horloge: c'est un planificateur:

[LEFT][IMG WIDTH=600 HEIGHT=350]/images/demo_license/en/timeman.png[/IMG]

[/LEFT]

[*][URL=https://www.bitrix24.com/pro/crm.php]CRM étendue[/URL], [URL=https://www.bitrix24.com/pro/call.php]Téléphonie étendue[/URL] et [URL=https://www.bitrix24.com/pro/tasks.php]Tâches étendues[/URL] sont maintenant disponibles. Tous les outils qui étaient restreints sont maintenant prêts à être utilisés: conversion des transactions, des factures et des devis ; recherche de doubles avancée ; journal des modifications ; évaluation de la qualité des appels des clients.
[*]Les chefs de service peuvent maintenant demander des rapports fonctionnels de leurs subordonnés (les rapports obligatoires planifiés sont pris en charge). Le planificateur d'événements et de réunions est facile à utiliser: vous le trouverez dans l'espace \"Société\".

[LEFT][IMG  WIDTH=600 HEIGHT=350]/images/demo_license/en/work_report.png[/IMG][/LEFT]
[/LIST]
Ce n'est qu'un échantillon des fonctionnalités premium maintenant disponibles. N'hésitez pas à consulter la page \"Offres avancées\" pour voir la liste complète.";
$MESS["LICENSE_EXPLODE"] = "Mise à niveau";
$MESS["LICENSE_FEATURE_OPENLINES"] = "Canaux ouverts";
$MESS["LICENSE_FEATURE_ONLINECHAT"] = "Chat live du site";
$MESS["LICENSE_FEATURE_LINE_1"] = "1 canal";
$MESS["LICENSE_FEATURE_LINE_2"] = "2 canaux";
$MESS["LICENSE_FEATURE_CHAT_1"] = "1 chat";
$MESS["LICENSE_FEATURE_CHAT_2"] = "2 chats";
$MESS["LICENSE_CRM_FEATURE_19"] = "Intégration 1C";
$MESS["LICENSE_CRM_FEATURE_23"] = "Formulaires CRM actifs";
$MESS["LICENSE_CRM_FEATURE_24"] = "Masquer le lien Bitrix24 sur le formulaire CRM public";
$MESS["LICENSE_CRM_FEATURE_25"] = "Factures en ligne";
$MESS["LICENSE_CRM_FEATURE_26"] = "Trackeur d'e-mail";
$MESS["LICENSE_CRM_FEATURE_27"] = "Historique de téléchargement";
$MESS["LICENSE_CRM_FEATURE_28"] = "Trackeur 1C";
$MESS["LICENSE_CRM_FEATURE_29"] = "Pipelines multiples";
$MESS["LICENSE_BUSINESS_USERS_12"] = "12 utilisateurs professionnels";
$MESS["LICENSE_BUSINESS_USERS_24"] = "24 utilisateurs professionnels";
$MESS["LICENSE_DAYS_3"] = "3 jours";
$MESS["LICENSE_TEL_FEATURE_22"] = "Droits d'accès aux paramètres d'appel téléphonique et de téléphonie";
$MESS["LICENSE_TASKS_FEATURE_15"] = "Champs de tâche personnalisés";
$MESS["LICENSE_TASKS_FEATURE_16"] = "Participation aux tâches";
$MESS["LICENSE_DISK_TITLE"] = "Advanced Drive";
$MESS["LICENSE_DISK_DESC"] = "Advanced Drive vous permet de verrouiller un document pour modification, empêchant ainsi d'autres utilisateurs de le modifier lorsque vous y apportez des changements. Une autre fonctionnalité utile vous permet d'autoriser ou d'interdire à vos employés de créer des liens de documents pour accès externe.";
$MESS["LICENSE_DISK_FULL"] = "Advanced Drive";
$MESS["LICENSE_DISK_FEATURE_1"] = "Disque privé de l'employé";
$MESS["LICENSE_DISK_FEATURE_2"] = "Disque partagé de l'entreprise";
$MESS["LICENSE_DISK_FEATURE_3"] = "Synchroniser avec un ordinateur local";
$MESS["LICENSE_DISK_FEATURE_4"] = "Intégrer avec One Drive, Google Drive et Dropbox";
$MESS["LICENSE_DISK_FEATURE_5"] = "Modifier des documents en ligne dans GoogleDocs";
$MESS["LICENSE_DISK_FEATURE_6"] = "Modifier des documents en ligne dans Microsoft Office Online";
$MESS["LICENSE_DISK_FEATURE_7"] = "Modifier des documents en ligne sur un ordinateur local (Microsoft Office, OpenOffice, LibreOffice etc.)";
$MESS["LICENSE_DISK_FEATURE_8"] = "Rechercher les contenus de documents";
$MESS["LICENSE_DISK_FEATURE_9"] = "Historique de révision";
$MESS["LICENSE_DISK_FEATURE_10"] = "Assigner des autorisations d'accès";
$MESS["LICENSE_DISK_FEATURE_11"] = "Partager des documents (à l'aide de liens externes)";
$MESS["LICENSE_DISK_FEATURE_12"] = "Désactiver des liens publics";
$MESS["LICENSE_DISK_FEATURE_13"] = "Verrouiller des documents";
$MESS["LICENSE_DISK_SHOW_MORE"] = "afficher les fonctionnalités de Drive dans différents abonnements";
$MESS["LICENSE_ADMIN_NOTIFY_LICENSE_REQUEST"] = "Demande d’achat de l'offre <a href=\"#LICENSE_LINK#\">#LICENSE_NAME#</a>";
$MESS["LICENSE_FEATURE_SOCIAL_COMMUNICATION"] = "Communication  sociale";
$MESS["LICENSE_FEATURE_FILES"] = "Fichiers & Documents";
$MESS["LICENSE_FEATURE_EXTRANET"] = "Extranet";
$MESS["LICENSE_PRICE"] = "Coût";
$MESS["LICENSE_PROJECT_FREE"] = "Gratuit !";
$MESS["CONFIG_CHANGE_LICENSE"] = "Choix de l'abonnement";
$MESS["LICENCE_FEATURE_R_GENERAL"] = "Principales fonctionnalités";
$MESS["LICENCE_FEATURE_R_EXTRANET"] = "Utilisateurs externes (Extranet)";
$MESS["LICENCE_FEATURE_R_CRM_1C"] = "Votre logo";
$MESS["LICENCE_FEATURE_R_OTCH"] = "Rapports d'activité";
$MESS["LICENCE_FEATURE_R_BP"] = "Procédures d'entreprise dans documents partagés";
$MESS["LICENCE_FEATURE_R_MORE"] = "Plus";
$MESS["CONFIG_LICENSE_DESCRIPTION_NEW"] = "Le coupon peut être utilisé pour:<br>
1) Activer un abonnement payant;<br>
2) Prolonger un abonnement payant pour 6 ou 12 mois (selon le type de licence que vous avez acheté);<br>
3) Passer à une autre formule tarifaire
4) Ajouter des utilisateurs;<br>
5) Augmenter l'disque dans le cloud;<br>
6) Recharger votre solde téléphonique;<br>
7) Activer la solution payante Marketplace";
$MESS["LICENSE_FEATURE_LISTS"] = "Gestion des archives";
$MESS["LICENSE_PRICE_CORPORATION"] = "Partir de 8  par utilisateur/mois";
$MESS["LICENSE_TITLE_CORPORATION"] = "S'abonner à la formule 'Entreprise'";
$MESS["CONFIG_LICENSE_DESCRIPTION"] = "Le coupon peut être utilisé pour:<br>
1) Activer un abonnement payant;<br>
2) Prolonger un abonnement payant pour 6 ou 12 mois (selon le type de licence que vous avez acheté);<br>
3) Passer à une autre formule tarifaire";
$MESS["CONFIG_CHANGE_LICENSE_INFO"] = "Prolonger ou <br>acheter une nouvel<br>abonnement<br><br>Pour plus de<br>détails:<br><a href='http://www.bitrix24.com/prices' target='_blank'>Bitrix24</a>";
$MESS["CONFIG_LICENSE_INVOICE_BUTTON"] = "Créer une facture";
$MESS["LICENSE_F_R_GENERAL_L"] = "http://www.bitrix24.com/prices/";
$MESS["LICENCE_FEATURE_R_EXTRANET_L"] = "http://www.bitrix24.com/features/tasks.php#extranet";
$MESS["LICENCE_FEATURE_R_OTCH_L"] = "http://www.bitrix24.com/features/company.php#work_reports";
$MESS["LICENSE_FEATURE_TIMEMAN_L"] = "http://www.bitrix24.com/features/company.php#time_management";
$MESS["LICENSE_FEATURE_MEETING_L"] = "http://www.bitrix24.com/features/calendars.php#meetings";
$MESS["LICENCE_FEATURE_R_BP_L"] = "http://www.bitrix24.com/features/company.php#lists";
?>