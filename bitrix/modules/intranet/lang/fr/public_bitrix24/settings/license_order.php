<?
$MESS["LICENSE_TITLE"] = "Acheter la licence";
$MESS["LICENSE_CHOOSE"] = "Veuillez sélectionner le mode de souscription";
$MESS["LICENSE_ORDER_PARTNER_TITLE"] = "Acheter une licence <br><strong>via notre partenaire</strong>";
$MESS["LICENSE_ORDER_RESEIVE_TEXT"] = "<strong>Vous allez recevoir:</strong>";
$MESS["LICENSE_ORDER_PARTNER_TEXT_1"] = "Assistance technique fournie par Bitrix24 ainsi que par la communauté sur le forum.";
$MESS["LICENSE_ORDER_PARTNER_TEXT_2"] = "Si vous avez des questions sur le fonctionnement de Bitrix24, des consultations téléphonique sont assurées par nos partenaires.";
$MESS["LICENSE_ORDER_PARTNER_TEXT_3"] = "Réduction de 5% à 10% (selon la période de prolongation)";
$MESS["LICENSE_ORDER_PARTNER_NAME"] = "Votre partenaire:";
$MESS["LICENSE_ORDER_PARTNER_TEL"] = "Téléphone:";
$MESS["LICENSE_ORDER_PARTNER_CHANGE"] = "Changer de partenaire";
$MESS["LICENSE_ORDER_PARTNER_CHOOSE"] = "Selectionner votre partenaire";
$MESS["LICENSE_ORDER_PARTNER_BUY"] = "Acheter";
$MESS["LICENSE_ORDER_OR"] = "OU";
$MESS["LICENSE_ORDER_MYSELF_TITLE"] = "Acheter la licence <br><strong>directement</strong>";
$MESS["LICENSE_ORDER_MYSELF_TEXT_1"] = "Assistance technique fournie par Bitrix24 ainsi que par la communauté sur le forum.";
$MESS["LICENSE_ORDER_MYSELF_TEXT_2"] = "Différents mode de paiement pour obtenir votre licence immédiatement.";
$MESS["LICENSE_ORDER_PARTNER_SEND"] = "La demande d'achat vient d'être envoyée <br> à votre partenaire";
?>