<?
$MESS["EXTRANET_404_TITLE"] = "Rien n'a été trouvé.";
$MESS["EXTRANET_404_TEXT"] = "<p>Vous ne pouvez pas accéder à l'Extranet.</p>
<p>- cliquez sur \"Retour\" dans votre navigateur pour retourner à la page précédente</p>
<p>- ou ouvrez le <a href=\"/\">Flux d'activités</a></p>";
?>