<?
$MESS["MENU_TELEPHONY_BALANCE"] = "Solde et statistiques";
$MESS["MENU_TELEPHONY"] = "Paramètres de téléphonie";
$MESS["MENU_TELEPHONY_PERMISSIONS"] = "Autorisations d'accès";
$MESS["MENU_TELEPHONY_LINES"] = "Numéros de téléphone";
$MESS["MENU_TELEPHONY_USERS"] = "Utilisateurs de téléphonie";
$MESS["MENU_TELEPHONY_PHONES"] = "Téléphones SIP";
$MESS["MENU_TELEPHONY_CONNECT"] = "Connexion";
$MESS["MENU_TELEPHONY_IVR"] = "Configurer le SVI";
$MESS["MENU_TELEPHONY_GROUPS"] = "Files d'attente";
?>