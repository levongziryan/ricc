<?
$MESS["CONFIG_TITLE"] = "Paramètres";
$MESS["CONFIG_USERS_COUNT"] = "Utilisateurs";
$MESS["CONFIG_USERS_COUNT_LIMIT"] = "Utilisateurs max :";
$MESS["CONFIG_USERS"] = "Utilisateurs :";
$MESS["CONFIG_EXTRANET_USERS"] = "Utilisateurs d'Extranet";
$MESS["CONFIG_USERS_FREE"] = "Comptes utilisateur libres :";
$MESS["CONFIG_DISC_SPACE"] = "Espace de stockage dématérialisé";
$MESS["CONFIG_DISC_SPACE_LIMIT"] = "Espace disque";
$MESS["CONFIG_DISC_USAGE"] = "Espace disque occupé";
$MESS["CONFIG_DB_USAGE"] = "Taille de la base de données";
$MESS["CONFIG_DISC_SPACE_FREE"] = "Espace libre :";
$MESS["CONFIG_NO_LIMIT"] = "Illimité";
$MESS["CONFIG_HEADER_SETTINGS"] = "Paramètres";
$MESS["CONFIG_COMPANY_NAME"] = "Nom de la société";
$MESS["CONFIG_SAVE_SUCCESSFULLY"] = "Les paramètres ont bien été mis à jour";
$MESS["CONFIG_LICENSE_NAME"] = "Licence";
$MESS["CONFIG_LICENSE_TILL"] = " Jusqu'à #LICENSETILL#";
$MESS["CONFIG_SAVE"] = "Enregistrer";
$MESS["CONFIG_LICENSE_ACTIVATE"] = "Activer la licence";
$MESS["CONFIG_COUPON"] = "Entrez le code de votre coupon";
$MESS["CONFIG_LICENSE_DESCRIPTION"] = "Utilisez des coupons pour :<br>
1) activer un abonnement payant;<br>
2) renouveler un abonnement payant de 6 ou 12 mois (en fonction de votre licence);<br>
3) changer d'abonnement.";
$MESS["CONFIG_ACTIVATE"] = "Activer";
$MESS["CONFIG_ACTIVATE_ERROR"] = "Erreur d'activation. Veuillez contacter le service des ventes";
$MESS["project"] = "Basique";
$MESS["team"] = "Standard";
$MESS["company"] = "Professionnel";
$MESS["nfr"] = "Licence partenaire";
$MESS["config_rating_label_likeY"] = "Texte du bouton 'j'aime' avant le vote";
$MESS["config_rating_label_likeN"] = "Texte du bouton 'j'aime' après le vote";
$MESS["CONFIG_EMAIL_FROM"] = "E-mail de l'administrateur du site <br>(adresse de l'expéditeur par défaut)";
$MESS["CONFIG_LOGO_24"] = "Ajouter '24' au logo de l'entreprise";
$MESS["CONFIG_EMAIL_ERROR"] = "L'adresse mail de l'administrateur du site est incorrecte.";
?>