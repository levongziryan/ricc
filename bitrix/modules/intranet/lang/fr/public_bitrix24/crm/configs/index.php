<?
$MESS["TITLE"] = "Paramètres";
$MESS["STATUS"] = "Listes de sélection";
$MESS["PERMS"] = "Permissions d'accès";
$MESS["BP"] = "Procédures d'entreprise";
$MESS["FIELDS"] = "Champs personnalisés";
$MESS["CONFIG"] = "Autres paramètres";
$MESS["SENDSAVE"] = "Intégration des mails";
$MESS["CURRENCY"] = "Devises";
$MESS["EXTERNAL_SALE"] = "Boutiques en ligne";
$MESS["MAIL_TEMPLATES"] = "Modèles d'E-mails";
$MESS["TAX"] = "Impôts & taxes";
$MESS["PS"] = "Mode de paiement";
$MESS["LOCATIONS"] = "Situations";
$MESS["MEASURE"] = "Unités de mesure";
$MESS["PRODUCT_PROPS"] = "Propriétés des produits";
$MESS["EXCH1C"] = "Intégration 1C";
$MESS["SLOT"] = "Rapports analytiques";
?>