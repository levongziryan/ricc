<?
$MESS["I_NEW_USER_TITLE"] = "Un nouvel utilisateur vient de nous rejoindre";
$MESS["I_NEW_USER_TITLE_SETTINGS"] = "Nouveaux employés";
$MESS["I_NEW_USER_TITLE_LIST"] = "Nouvel employé";
$MESS["I_NEW_USER_MENTION_M"] = "A fait référence à vous dans un commentaire sur une publication au sujet d'un utilisateur nous ayant rejoint récemment #title#.";
$MESS["I_NEW_USER_MENTION"] = "A fait référence à vous dans un commentaire sur une publication au sujet d'un utilisateur nous ayant rejoint récemment #title#.";
$MESS["I_NEW_USER_MENTION_F"] = "A fait référence à vous dans un commentaire sur une publication au sujet d'un utilisateur nous ayant rejoint récemment #title#.";
$MESS["I_NEW_USER_EXTERNAL_TITLE"] = "Nouveau utilisateur externe ajouté";
?>