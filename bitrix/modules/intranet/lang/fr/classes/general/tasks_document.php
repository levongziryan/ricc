<?
$MESS["INTASK_TD_FIELD_ID"] = "ID de la tâche";
$MESS["INTASK_TD_FIELD_TIMESTAMP_X"] = "Date de modification de la tâche";
$MESS["INTASK_TD_FIELD_MODIFIED"] = "Utilisateur ayant modifié la tâche";
$MESS["INTASK_TD_FIELD_DATE_CREATE"] = "Date de création du groupe";
$MESS["INTASK_TD_FIELD_CREATED"] = "Créé par";
$MESS["INTASK_TD_FIELD_IBLOCK_SECTION_ID"] = "Dossier de la tâche";
$MESS["INTASK_TD_FIELD_DATE_ACTIVE_FROM"] = "Commencer";
$MESS["INTASK_TD_FIELD_DATE_ACTIVE_TO"] = "Achèvement";
$MESS["INTASK_TD_FIELD_NAME"] = "Dénomination de la tâche";
$MESS["INTASK_TD_FIELD_DETAIL_TEXT"] = "Description de la tâche";
$MESS["INTASK_TD_OPERATIONS_READ"] = "Affichage";
$MESS["INTASK_TD_OPERATIONS_WRITE"] = "Mettre à jour";
$MESS["INTASK_TD_OPERATIONS_COMMENT"] = "Commentaire";
$MESS["INTASK_TD_OPERATIONS_DELETE"] = "Supprimer";
$MESS["INTASK_TD_USER_GROUPS_AUTHOR"] = "Auteur";
$MESS["INTASK_TD_USER_GROUPS_RESP"] = "Responsable";
$MESS["INTASK_TD_USER_GROUPS_TRACK"] = "???";
$MESS["INTASK_TD_USER_GROUPS_FRIEND"] = "Amis";
$MESS["INTASK_TD_USER_GROUPS_FRIEND2"] = "Amis des amis";
$MESS["INTASK_TD_USER_GROUPS_ALL"] = "Tous les visiteurs";
$MESS["INTASK_TD_USER_GROUPS_AUTHORIZED"] = "Utilisateurs autorisés";
$MESS["INTASK_TD_USER_GROUPS_OWNER"] = "Organisateur";
$MESS["INTASK_TD_USER_GROUPS_MODS"] = "Modérateur";
$MESS["INTASK_TD_USER_GROUPS_MEMBERS"] = "Participants";
?>