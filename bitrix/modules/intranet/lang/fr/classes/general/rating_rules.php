<?
$MESS["PP_USER_CONDITION_SUBORDINATE_T4"] = "100% de la valeur du responsable";
$MESS["PP_USER_CONDITION_SUBORDINATE_T2"] = "50% de la valeur du responsable";
$MESS["PP_USER_CONDITION_SUBORDINATE_T3"] = "75% de la valeur du responsable";
$MESS["PP_USER_CONDITION_SUBORDINATE_TEXT"] = "Le calcul des voix supplémentaires pour l'autorité de l'utilisateur sera effectué en fonction de la structure de la entreprise compte tenu de la valeur initiale de l'autorité.";
$MESS["PP_USER_CONDITION_SUBORDINATE_T1"] = "Nombre de votes pour subordonnés";
$MESS["PP_USER_CONDITION_SUBORDINATE_T0"] = "Quantité maximale de votes pour le chef";
$MESS["PP_USER_CONDITION_SUBORDINATE_NAME"] = "Structure de l'entreprise";
?>