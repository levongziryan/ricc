<?
$MESS["INTR_MAIL_DOMAINREADY_NOTICE"] = "Le nom de domaine [b]#DOMAIN#[/b] a été connecté.<a href='#SERVER#/company/personal/mail/?page=manage'>Gérer les boîtes aux lettres</a>";
$MESS["INTR_MAIL_DOMAIN_SUPPORT_LINK"] = "http://www.bitrixsoft.com/support/index.php?utm_source=regru&utm_medium=email&utm_campaign=regru_email_support";
$MESS["INTR_MAIL_DOMAIN_SUPPORTB24_LINK"] = "https://www.bitrix24.com/support/helpdesk/?utm_source=regru&utm_medium=email&utm_campaign=regru_email_support";
$MESS["INTR_MAIL_DOMAIN_LEARNMORE_LINK"] = "http://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=55&CHAPTER_ID=06527?utm_source=regru&utm_medium=email&utm_campaign=regru_email_howto";
$MESS["INTR_MAIL_DOMAIN_LEARNMOREB24_LINK"] = "http://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=55&CHAPTER_ID=06527?utm_source=regru&utm_medium=email&utm_campaign=regru_email_howto";
$MESS["INTR_SYNC_OUTLOOK_NOWEBSERVICE"] = "Le module Services web est nécessaire pour synchroniser avec Outlook. Veuillez contacter votre administrateur portail.";
?>