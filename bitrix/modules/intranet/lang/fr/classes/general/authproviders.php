<?
$MESS["authprov_panel_search_text"] = "Veuillez saisir le prénom, le nom ou l'identifiant de l'utilisateur ou le nom du service.";
$MESS["authprov_check_d"] = "Tous les employés du bureau";
$MESS["authprov_check_dr"] = "Tous les collaborateurs du département avec la subdivision";
$MESS["authprov_panel_group"] = "Choisir dans la structure";
$MESS["authprov_group"] = "Département";
$MESS["authprov_name_out_group"] = "Département";
$MESS["authprov_panel_search"] = "Recherche";
$MESS["authprov_panel_last"] = "Dernier";
$MESS["authprov_name_out_user1"] = "L'employé et ses dirigeants";
$MESS["authprov_name"] = "Départements";
$MESS["authprov_group_extranet"] = "Extranet";
?>