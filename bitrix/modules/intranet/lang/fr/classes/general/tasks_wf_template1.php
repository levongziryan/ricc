<?
$MESS["INTASK_WF_TMPL_HACK_WE"] = "Attend l'exécution";
$MESS["INTASK_WF_TMPL_HACK_WAITING"] = "Attend l'exécution";
$MESS["INTASK_WF_TMPL_HACK_INPROGRESS"] = "En cours";
$MESS["INTASK_WF_TMPL_HACK_COMPLETED"] = "Achevé(e)s";
$MESS["INTASK_WF_TMPL_HACK_CE"] = "Achever";
$MESS["INTASK_WF_TMPL_HACK_CLOSED"] = "Accompli";
$MESS["INTASK_WF_TMPL_HACK_CLE"] = "Fermer";
$MESS["INTASK_WF_TMPL_HACK_IPE"] = "Commencer l'exécution";
$MESS["INTASK_WF_TMPL_HACK_NS"] = "N'a pas commencé";
$MESS["INTASK_WF_TMPL_HACK_NOTSTARTED"] = "N'a pas commencé";
$MESS["INTASK_WF_TMPL_HACK_NOTACCEPTED"] = "Non accepté(e)";
$MESS["INTASK_WF_TMPL_HACK_DEFERRED"] = "Différé";
$MESS["INTASK_WF_TMPL_HACK_DE"] = "Reporter";
$MESS["INTASK_WF_TMPL_HACK_AE"] = "Accepter";
$MESS["INTASK_WF_TMPL_HACK_SRE"] = "Changer le responsable";
?>