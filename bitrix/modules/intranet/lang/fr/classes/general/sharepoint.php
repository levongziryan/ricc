<?
$MESS["SP_LIST_FIELD_CREATE_NONE"] = "(choisissez le type de champ)";
$MESS["SP_IGNORE"] = "(inconnu)";
$MESS["SP_LIST_FIELD_TIMESTAMP_X"] = "Date de modification";
$MESS["SP_LIST_FIELD_DATE_CREATE"] = "Date de création";
$MESS["SP_LIST_FIELD_DETAIL_PICTURE"] = "Affichage détaillé";
$MESS["SP_LIST_FIELD_DETAIL_TEXT"] = "Description détaillée";
$MESS["SP_LIST_FIELD_PREVIEW_PICTURE"] = "Image de l'annonce";
$MESS["SP_ERROR_IBLOCK_EXISTS"] = "Le bloc d'information lié avec la liste SharePoint.";
$MESS["SP_LIST_FIELD_MODIFIED_BY"] = "Modifié par";
$MESS["SP_LIST_FIELD_CREATED_BY"] = "Créé par";
$MESS["SP_ERROR_CLASS_NOT_EXISTS"] = "Le gestionnaire classe n'existe pas.";
$MESS["SP_ERROR_METHOD_NOT_EXISTS"] = "Client #CLASS# ne supporte pas la méthode #METHOD#.";
$MESS["SP_ERROR_MODULE_NOT_INSTALLED"] = "Module non installé.";
$MESS["SP_LIST_FIELD_NAME"] = "Dénomination";
$MESS["SP_LIST_FIELD_ACTIVE_FROM"] = "Début de l'activité";
$MESS["SP_ERROR_WRONG_URL"] = "URL du serveur SharePoint est incorrect.";
$MESS["SP_ERROR_WRONG_SP_LIST_ID"] = "L'identificateur de la liste est incorrect.";
$MESS["SP_ERROR_WRONG_IBLOCK_ID"] = "Bloc d'information incorrect.";
$MESS["SP_LIST_FIELD_ACTIVE_TO"] = "Fin de l'activité";
$MESS["SP_ERROR_MAX_ERRORS"] = "La quantité maximale des erreurs du serveur est surpassée.";
$MESS["SP_LIST_FIELD_G"] = "Liste des rubriques à rattacher";
$MESS["SP_LIST_FIELD_E"] = "Rattachement aux éléments";
$MESS["SP_LIST_FIELD_SORT"] = "Classification";
$MESS["SP_LIST_FIELD_L"] = "Liste";
$MESS["SP_ERROR_LIST_EXISTS"] = "La liste SharePoint est déjà lier avec un bloc d'information.";
$MESS["SP_LIST_FIELD_S"] = "Ligne";
$MESS["SP_LIST_FIELD_PREVIEW_TEXT"] = "Texte de l'annonce";
$MESS["SP_LIST_FIELD_F"] = "Fichier";
$MESS["SP_LIST_FIELD_N"] = "Chiffre";
?>