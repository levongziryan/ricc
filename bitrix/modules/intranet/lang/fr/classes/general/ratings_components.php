<?
$MESS["INTRANET_RATING_USER_SUBORDINATE_FORMULA_DESC"] = "SubordinateValue - votes complémentaires; StartValue - valeur initiale de l'évaluation; K - coefficient définit par l'utilisateur.";
$MESS["INTRANET_RATING_USER_SUBORDINATE_NAME"] = "Voix complémentaires selon la structure de la compagnie";
$MESS["INTRANET_RATING_NAME"] = "Intranet";
$MESS["INTRANET_RATING_USER_SUBORDINATE_DESC"] = "L'évaluation est basée sur des données calculées par la règle de traitement 'Structure de la compagnie' (la règle prend en compte la valeur initiale de l'autorité qui est donnée à chaque utilisateur séparément dans ses paramètres).";
?>