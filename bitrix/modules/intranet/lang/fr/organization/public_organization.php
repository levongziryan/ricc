<?
$MESS["INTR_MAIL_INP_PUBLIC_DOMAIN"] = "Les utilisateurs peuvent enregistrer une boîte mail sur ce domaine";
$MESS["INTR_MAIL_MANAGE"] = "Configurer les boîtes mail des utilisateurs";
$MESS["INTR_MAIL_DESCR_B24"] = "Serveur e-mail gratuit pour votre entreprise ! Stockage illimité pour votre e-mail, et anti-virus/anti-spam pour garder votre boîte de réception propre. Migrez votre e-mail professionnel sur Bitrix24 ! Créez vos propres boîtes mail sur le domaine de votre entreprise, sur Bitrix24, ou attachez des boîtes mail d'autres services.";
$MESS["INTR_MAIL_DESCR_BOX"] = "Prêt à utiliser le serveur e-mail pour votre entreprise. Profitez d'un stockage illimité, d'un anti-virus et d'un anti-spam. Créez des boîtes mail sur votre domaine professionnel ou attachez des services e-mail existants.";
$MESS["INTR_MAIL_DOMAIN_PUBLIC"] = "permet aux utilisateurs d'enregistrer des boîtes mail sur le nouveau domaine";
$MESS["INTR_MAIL_NODOMAIN_USER_INFO"] = "Veuillez contacter un administrateur de votre intranet pour attacher le domaine professionnel.";
$MESS["INTR_MAIL_MANAGE_HINT"] = "Créez une boîte mail pour chaque utilisateur de votre organisation. Utilisez cette interface pour gérer les boîtes mail professionnelles : créez, attachez ou supprimez des boîtes mail, mais aussi changez les mots de passe.";
$MESS["INTR_MAIL_MANAGE_SETUP_ALLOW_CRM"] = "Les membres peuvent connecter des boîtes mail vers le CRM";
$MESS["INTRANET_OTP_MANDATORY_TITLE"] = "Une sécurité supplémentaire pour vos données professionnelles";
$MESS["INTR_ABSC_TPL_EDIT_ENTRIES"] = "Gestion des utilisateurs";
$MESS["ISL_TPL_NOTE_NULL"] = "Aucun utilisateur retrouvé.";
$MESS["INTR_ABSC_TPL_IMPORT"] = "Importer des utilisateurs";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_OUTLOOK_TITLE"] = "Exporter la liste des utilisateurs comme contacts Outlook";
$MESS["INTR_COMP_IS_TPL_BANNER_OUTLOOK"] = "Vous pouvez exporter la liste des utilisateurs sous forme de contacts Microsoft Outlook";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_CARDDAV_TITLE"] = "Synchroniser le registre des utilisateurs avec les applications et les périphériques qui prennnent en charge le protocole CardDAV (iPhone, iPad, etc.)";
$MESS["INTR_IS_TPL_SEARCH"] = "Trouver utilisateur";
$MESS["INTR_IS_TPL_SEARCH_DEPARTMENT"] = "Trouver un utilisateur dans cette division";
$MESS["INTR_IS_TPL_OUTLOOK"] = "Export des utilisateurs vers Outlook";
$MESS["INTR_IS_TPL_OUTLOOK_TITLE"] = "Exporter la liste des utilisateurs comme contacts Outlook";
$MESS["INTR_ISBN_TPL_FILTER_ALL"] = "Pour toute l'organisation";
$MESS["INTR_ISIN_TPL_ALL"] = "Pour toute l'organisation";
$MESS["INTR_ISL_TPL_NOTE_NULL"] = "Aucun utilisateur retrouvé.";
$MESS["ISV_ERROR_dpt_not_empty"] = "Vous devez relocaliser tous les utilisateurs de la division avant de supprimer cette dernière.";
$MESS["ISV_EMP_LIST"] = "Liste des utilisateurs";
$MESS["ISV_confirm_delete_department"] = "Voulez-vous vraiment supprimer cette division ? Cela déplacera toutes les sous-divisions et utilisateurs qui en dépendent.";
$MESS["ISV_confirm_set_head"] = "Assigner #EMP_NAME# comme chef de la division #DPT_NAME# ?";
$MESS["ISV_confirm_change_department_0"] = "Transférer #EMP_NAME# vers #DPT_NAME# ?";
$MESS["ISV_confirm_change_department_1"] = "Assigner #EMP_NAME# à #DPT_NAME# ?";
$MESS["ISV_add_emp"] = "Ajouter un utilisateur";
$MESS["ISV_EMP_COUNT_1"] = "#NUM# utilisateur";
$MESS["ISV_EMP_COUNT_2"] = "#NUM# utilisateurs";
$MESS["ISV_EMP_COUNT_3"] = "#NUM# utilisateurs";
$MESS["ISV_EMP_COUNT_4"] = "#NUM# utilisateurs";
$MESS["ISV_EMP_COUNT_21"] = "#NUM# utilisateur";
$MESS["ISV_EMP_COUNT_22"] = "#NUM# utilisateurs";
$MESS["ISV_EMP_COUNT_23"] = "#NUM# utilisateurs";
$MESS["ISV_EMP_COUNT_24"] = "#NUM# utilisateurs";
$MESS["ISV_EMP_COUNT_31"] = "#NUM# utilisateur";
$MESS["ISV_EMP_COUNT_32"] = "#NUM# utilisateurs";
$MESS["ISV_EMP_COUNT_33"] = "#NUM# utilisateurs";
$MESS["ISV_EMP_COUNT_34"] = "#NUM# utilisateurs";
$MESS["ISV_EMP_COUNT_41"] = "#NUM# utilisateur";
$MESS["ISV_EMP_COUNT_42"] = "#NUM# utilisateurs";
$MESS["ISV_EMP_COUNT_43"] = "#NUM# utilisateurs";
$MESS["ISV_EMP_COUNT_44"] = "#NUM# utilisateurs";
$MESS["ISV_EMP_COUNT_MUL"] = "#NUM# utilisateurs";
$MESS["ISV_B24_INVITE"] = "Inviter des utilisateurs";
$MESS["INTR_EMP_WINDOW_TITLE"] = "Sélectionner un utilisateur";
$MESS["INTR_EMP_SUBMIT_TITLE"] = "Choisir l'utilisateur sélectionné";
$MESS["INTR_EMP_CANCEL_TITLE"] = "Annuler la sélection d'utilisateur";
$MESS["INTR_EMP_SEARCH"] = "rechercher un utilisateur";
$MESS["INTRANET_USTAT_SECTION_SOCNET_HELP_INVOLVEMENT"] = "Affiche le pourcentage d'utilisateurs ayant utilisé la fonctionnalité réseau social pendant la période donnée.";
$MESS["INTRANET_USTAT_SECTION_LIKES_HELP_INVOLVEMENT"] = "Pourcentage d'utilisateurs ayant utilisé la fonction \"J'aime\" pendant la période donnée.";
$MESS["INTRANET_USTAT_SECTION_TASKS_HELP_INVOLVEMENT"] = "Pourcentage d'utilisateurs ayant utilisé les tâches pendant la période donnée.";
$MESS["INTRANET_USTAT_SECTION_IM_HELP_INVOLVEMENT"] = "Affiche le pourcentage d'utilisateurs ayant envoyé un message instantané ou effectué un appel vidéo/audio pendant la période donnée.";
$MESS["INTRANET_USTAT_SECTION_DISK_HELP_INVOLVEMENT"] = "Pourcentage d'utilisateurs implémentant Bitrix24.Drive.";
$MESS["INTRANET_USTAT_SECTION_MOBILE_HELP_INVOLVEMENT"] = "Pourcentage d'utilisateurs ayant utilisé l'appli mobile pendant la période donnée.";
$MESS["INTRANET_USTAT_SECTION_CRM_HELP_INVOLVEMENT"] = "Pourcentage d'utilisateurs qui ont été actifs dans la CRM pendant la période donnée. Ce nombre est/peut être limité par le nombre d'utilisateurs ayant accès à la CRM.";
$MESS["INTRANET_USTAT_TELLABOUT_SOCNET_TITLE"] = "Unité dans la force de travail";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_SOCNET"] = "Utilisation de la fonctionnalité : Réseau social";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_LIKES"] = "Utilisation de la fonctionnalité : J'aime";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_TASKS"] = "Utilisation de la fonctionnalité : Tâches";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_IM"] = "Utilisation de la fonctionnalité : Chat";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_DISK"] = "Utilisation de la fonctionnalité: Drive";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_MOBILE"] = "Utilisation de la fonctionnalité : Appli mobile";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_CRM"] = "Utilisation de la fonctionnalité: CRM";
$MESS["INTRANET_USTAT_COMPANY_GRAPH_TITLE"] = "Pulsation d'organisation";
$MESS["INTRANET_USTAT_COMPANY_HELP_GENERAL"] = "La pulsation d'organisation est un indicateur général de l'activité des utilisateurs sur le portail au moment présent (composé de tous les utilisateurs de l'heure précédente).";
$MESS["INTRANET_USTAT_COMPANY_HELP_ACTIVITY"] = "Index d'activité : composé de toutes les activités des utilisateurs sur toutes les fonctionnalités intranet pendant une période donnée. L'Index affiche le niveau d'activité des utilisateurs quand ils utilisent divers outils.";
$MESS["INTRANET_USTAT_COMPANY_HELP_INVOLVEMENT"] = "Niveau d'engagement :  c'est un indicateur clé qui affiche à quel point les utilisateurs se sont familiarisés avec les capacités de Bitrix24. Il affiche quel pourcentage de l'organisation utilise au moins un quart des outils proposés.";
$MESS["INTRANET_USTAT_COMPANY_HELP_RATING"] = "Cette note est déterminée par la moyenne individuelle de l'activité sur l'index de tous les utilisateurs qui ont réalisé au moins 1 action pendant la période donnée.";
$MESS["INTRANET_USTAT_WIDGET_TITLE"] = "Pulsation d'organisation";
$MESS["INTRANET_USTAT_WIDGET_ACTIVITY_HELP"] = "Le niveau d'activité actuel de l'organisation (composé de tous les utilisateurs de l'heure précédente)";
$MESS["INTRANET_USTAT_WIDGET_INVOLVEMENT_HELP"] = "Engagement actuel des utilisateurs. Cet indicateur montre le pourcentage de tous les utilisateurs du jour qui ont utilisé au moins quatre outils différents dans l'intranet.";
$MESS["INTRANET_USTAT_USER_GRAPH_COMPANY"] = "Moyenne de l'organisation";
$MESS["INTRANET_USTAT_USER_ACTIVITY_COMPANY_TITLE"] = "Moyenne de<br>l'organisation";
$MESS["INTRANET_USTAT_USER_HELP_RATING"] = "Votre position dans le résumé de la <b>note d'activité</b> listant tous les utilisateurs qui ont utilisé Bitrix24 au moins une fois pendant la période donnée.";
$MESS["INTRANET_USTAT_USER_HELP_ACTIVITY_DEPT"] = "Valeur moyenne des actions de votre division sur Bitrix24 via un des sept outils disponibles pendant la période donnée.<br><br><br>Utilisez la moyenne de la division pour voir si votre activité est meilleure que celle des autres.";
$MESS["INTRANET_USTAT_USER_HELP_ACTIVITY_COMPANY"] = "Valeur moyenne des actions de votre organisation sur Bitrix24 via un des sept outils disponibles pendant la période donnée.<br><br><br>Utilisez la moyenne de la organisation pour voir si votre activité est meilleure que celle de toute l'organisation.";
$MESS["BM_USR_CNT"] = "No. d'utilisateurs";
$MESS["WD_USER"] = "Documents utilisateur";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_USER_ADD"] = "Inviter un utilisateur";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_USER_INVITE"] = "Inviter un utilisateur";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_USER_ACTIVE"] = "Utilisateurs";
$MESS["INTR_IS_TPL_ACTION_INVITE"] = "Inviter des utilisateurs";
$MESS["INTR_ISE_TPL_NOTE_NULL"] = "Aucun utilisateur retrouvé.";
$MESS["INTR_CONFIRM_FIRE"] = "L'utilisateur ne pourra ni se connecter au portail, ni apparaître dans la structure de l'entreprise. Cependant, toutes ses données personnelles (fichiers, messages, tâches, etc.) resteront intactes.\\n\\nVoulez-vous vraiment révoquer l'accès de cet utilisateur ?";
$MESS["INTR_CONFIRM_RESTORE"] = "L'utilisateur pourra se connecter au portail et apparaître dans la structure de l'entreprise.\\n\\Voulez-vous vraiment autoriser l'accès de cet utilisateur ?";
$MESS["INTR_CONFIRM_DELETE"] = "L'invitation de l'utilisateur sera définitivement supprimée.\\n\\nVoulez-vous vraiment supprimer l'utilisateur ?";
$MESS["CT_BST_SEARCH_HINT"] = "Chercher un employé, un document ou autre...";
$MESS["SOCNET_CONFIRM_FIRE"] = "L'utilisateur ne pourra ni se connecter au portail, ni apparaître dans la structure de l'entreprise. Cependant, toutes ses données personnelles (fichiers, messages, tâches, etc.) seront conservées.

Voulez-vous vraiment fermer l'accès de cet utilisateur ?";
$MESS["SOCNET_CONFIRM_RECOVER"] = "L'utilisateur pourra se connecter au portail et apparaître dans la structure de l'entreprise.

Voulez-vous vraiment ouvrir l'accès de cet utilisateur ?";
$MESS["SOCNET_CONFIRM_DELETE"] = "L'invitation sera supprimée irrévocablement.

Voulez-vous vraiment supprimer cet utilisateur ?";
$MESS["ISL_WORK_COUNTRY"] = "Pays";
$MESS["ISL_WORK_CITY"] = "Ville";
$MESS["ISL_WORK_LOGO"] = "Logo de l'organisation";
$MESS["SOCNET_CONFIRM_FIRE1"] = "L'utilisateur ne pourra ni se connecter au portail, ni apparaître dans la structure de l'entreprise. Cependant, toutes ses données personnelles (fichiers, messages, tâches, etc.) resteront intactes.\\n\\nVoulez-vous vraiment révoquer l'accès de cet utilisateur ?";
$MESS["SOCNET_CONFIRM_RECOVER1"] = "L'utilisateur pourra se connecter au portail et apparaître dans la structure de l'entreprise.\\n\\Voulez-vous vraiment autoriser l'accès de cet utilisateur ?";
$MESS["TEMPLATE_DESCRIPTION"] = "Ce modèle est conçu pour accentuer les aspects sociaux de l'intranet et combiner les outils de réalisation et de productivité conventionnels dans un contexte qui facilite la communication sociale. La mise en page de l'intranet social est un booster de productivité hautement intuitif et ne demande que très peu de temps pour être adopté et orienté.";
$MESS["BITRIX24_INVITE"] = "Inviter des utilisateurs";
$MESS["BITRIX24_INVITE_ACTION"] = "Inviter des utilisateurs";
$MESS["BITRIX24_HELP_VIDEO_TITLE_13"] = "Structure de l'organisation";
$MESS["BITRIX24_HELP_VIDEO_TITLE_FULL_13"] = "Structure de l'organisation";
$MESS["AUTH_OTP_HELP_TEXT"] = "Pour sécuriser encore plus vos données Bitrix24, votre administrateur a activé une option de sécurité supplémentaire : l'identification en deux étapes.<br/><br/>
L'identification en deux étapes comprend deux niveaux de vérification. Le premier est votre mot de passe principal. Le deuxième niveau est un code à usage unique envoyé sur votre téléphone mobile (ou votre clé électronique physique).<br/><br/>
<div class=\"login-text-img\"><img src=\"#PATH#/images/en/img1.png\"></div>
<br/>
Une fois l'identification en deux étapes activée, vous devez passer deux écrans d'authentification :<br/><br/>
- entrez votre e-mail et votre mot de passe ;<br/>
- puis entrez le code à usage unique qui peut être obtenu via l'appli mobile Bitrix OTP que vous avez installé quand vous avez activé l'identification en deux étapes.<br/><br/>
Exécutez l'appli sur votre téléphone mobile :<br/><br/>
<div class=\"login-text-img\"><img src=\"#PATH#/images/en/img2.png\"></div>
<br/>
Entrez le code que vous voyez à l'écran :<br/><br/>
<div class=\"login-text-img\"><img src=\"#PATH#/images/en/img3.png\"></div>
<br/>
Si vous utilisez plusieurs Bitrix24, assurez-vous que le code soit pour le bon Bitrix24 avant de le saisir.<br/><br/>
<div class=\"login-text-img\"><img src=\"#PATH#/images/en/img4.png\"></div>
<br/>
Si vous n'avez jamais eu l'occasion d'activer et de configurer l'identification en deux étapes, ou si vous avez perdu votre téléphone, veuillez contacter votre administrateur pour qu'il restaure votre accès à Bitrix24.<br/><br/>
Si vous êtes l'administrateur, veuillez contacter <a href=\"http://www.bitrix24.com/support/helpdesk/\">l'assistance Bitrix</a> pour restaurer votre accès.";
$MESS["SERVICE_COMPANY_STRUCTURE"] = "Structure de l'organisation";
$MESS["wiz_slogan"] = "Mon organisation";
$MESS["wiz_company_name"] = "Nom de l'organisation :";
$MESS["wiz_company_logo"] = "Logo de l'organisation :";
$MESS["wiz_demo_structure"] = "Installer l'échantillon de structure d'organisation";
$MESS["IFS_EF_staff"] = "Personnel ; documents de référence ; structure d'organisation ; utilisateurs honorés";
$MESS["IFS_EF_Blog"] = "Flux de blog de groupe et d'utilisateur";
$MESS["IFS_EF_Gallery"] = "Galeries photo des utilisateurs";
$MESS["BLOG_DEMO_MESSAGE_TITLE_1"] = "L'ouverture du portail intranet de l'organisation ";
$MESS["BLOG_DEMO_MESSAGE_BODY_1"] = "Chers collègues, le Portail de l'intranet est officiellement lancé aujourd'hui.

Cela veut donc dire que chaque utilisateur peut dès maintenant voir les informations publiques et accéder à son espace privé au moyen d'un mot de passe.

Les accès configurables aux espaces du portail nous apportent des fonctionnalités de travail en équipe : nous pouvons éditer des documents, créer des groupes de travail, détailler des rapports, et le tout ensembles !

Le but du portail est de rendre la chaîne de communication entre les utilisateurs aussi courte que possible. Le portail, outil de communication à la fois rapide et moderne, est le meilleur moyen d'éviter la paperasse et de transformer le flux d'informations professionnelles en solution électronique.
 
En utilisant le portail intranet, les utilisateurs de l'organisation peuvent obtenir toute information technique, de référence ou juridique, notamment les standards et identités d'entreprise.

Les utilisateurs peuvent poster sur les forums, envoyer des requêtes à l'assistance technique et au service d'approvisionnement, échanger et partager des documents, discuter des actualités de l'organisation, et recevoir les dernières informations de la direction.

Le portail deviendra un élément essentiel de votre organisation !";
$MESS["CAL_TYPE_COMPANY_NAME"] = "Calendriers d'organisation";
$MESS["CAL_COMPANY_SECT_DESC_0"] = "Conférences et salons dans lesquels nous sommes impliqués";
$MESS["CAL_COMPANY_SECT_DESC_1"] = "Événements du bureau de Londres";
$MESS["CAL_COMPANY_SECT_DESC_2"] = "Événements du bureau de Paris";
$MESS["CAL_COMP_EVENT_DESC_3"] = "Formation pour les nouveaux utilisateurs";
$MESS["CRM_GADGET_NEW_COMPANY_TITLE"] = "Nouvelles sociétés";
$MESS["CRM_DEMO_EVENT_12_EVENT_NAME"] = "Champ \"Utilisateurs\" changé";
$MESS["CRM_DEMO_EVENT_15_EVENT_NAME"] = "Champ \"Utilisateurs\" changé";
$MESS["GENERAL_FORUM_DESCRIPTION"] = "Forum public pour les utilisateurs d'organisation. Discutez ici de votre société et échangez vos opinions.";
$MESS["GENERAL_FORUM_MESSAGE_BODY"] = "Attention ! Les utilisateurs d'organisation peuvent maintenant gérer leur stockage de fichier depuis leur espace personnel.

Nous fournirons des informations détaillées sur la gestion du stockage de fichiers et sur les méthodes pour diriger le stockage vers un disque réseau dans la section aide : \"Mon Profil - Fichiers\".

Pour toute question sur la configuration du stockage de fichiers, veuillez contacter les ingénieurs de l'assistance technique via le formulaire de demande d'aide.";
$MESS["ABSENCE_FORM_2"] = "Choisissez une utilisateur absent
";
$MESS["W_IB_CLIENTS_TAB1"] = "edit1--#--Client--,--NAME--#--*Nom de la société--,--PROPERTY_PERSON--#--Directeur--,--PROPERTY_PHONE--#--Téléphone--;--";
$MESS["HONOR_FORM_2"] = "Sélectionner l'utilisateur à honorer";
$MESS["STATE_FORM_2"] = "Choisir l'utilisateur dont il faut changer le statut";
$MESS["W_IB_ABSENCE_2_PREV"] = "Voyage d'affaire à la sucursale.";
$MESS["EMPLOYEES_GROUP_DESC"] = "Tous les utilisateurs enregistrés sur le portail.";
$MESS["DIRECTION_GROUP_DESC"] = "Administration.";
$MESS["main_opt_user_comp_name"] = "Nom de l'organisation";
$MESS["main_opt_user_comp_logo"] = "Logo de l'organisation";
$MESS["ML_COL_NAME_0"] = "Photos d'utilisateur";
$MESS["ML_COL_DESC_0"] = "Collection de photos d'utilisateur";
$MESS["MEETING_DESCRIPTION"] = "Nous allons devoir organiser une réunion pour discuter du déploiement du portail intranet de notre organisation.";
$MESS["SONET_GROUP_DESCRIPTION_3"] = "Vie sportive";
$MESS["SUBSCRIBE_POSTING_SUBJECT"] = "Nos photos !";
$MESS["SUBSCRIBE_POSTING_BODY"] = "Salut ! Nous avons téléchargé de nouvelles photos de vacances";
$MESS["authprov_check_d"] = "Utilisateurs de toutes les divisions";
$MESS["authprov_check_dr"] = "Utilisateurs de toutes les divisions et sous-divisions";
$MESS["EC_ADD_MEMBERS_FROM_STR"] = "De la structure de l'organisation";
$MESS["EC_ADD_MEMBERS_FROM_STR_TITLE"] = "Ajouter les participants à l'événement de la structure de l'organisation";
$MESS["EC_COMPANY_STRUCTURE"] = "Structure de l'organisation";
$MESS["EC_NO_COMPANY_STRUCTURE"] = "La structure de l'organisation est manquante.";
$MESS["BX24_INVITE_TITLE_INVITE"] = "Inviter des utilisateurs";
$MESS["BX24_INVITE_TITLE_ADD"] = "Ajouter un utilisateur";
$MESS["BX24_INVITE_DIALOG_MAX_COUNT_ERROR"] = "Le nombre d'invités dépasse les conditions de votre licence.";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT"] = "Veuillez me rejoindre sur notre nouvel intranet. C'est un endroit où tout le monde collabore sur des projets, coordonne des tâches et des emplois du temps, et construit notre base de connaissance. Mais c'est aussi facile à utiliser.";
$MESS["I_NEW_USER_TITLE"] = "Un nouvel utilisateur a été ajouté";
$MESS["I_NEW_USER_TITLE_SETTINGS"] = "Nouveaux utilisateurs";
$MESS["I_NEW_USER_TITLE_LIST"] = "Nouvel utilisateur";
$MESS["I_NEW_USER_MENTION"] = "vous a mentionné dans le commentaire d'un message à propos d'un utilisateur nouvellement ajouté #title#.";
$MESS["I_NEW_USER_MENTION_M"] = "vous a mentionné dans le commentaire d'un message à propos d'un utilisateur nouvellement ajouté #title#.";
$MESS["I_NEW_USER_MENTION_F"] = "vous a mentionné dans le commentaire d'un message à propos d'un utilisateur nouvellement ajouté #title#.";
$MESS["PP_USER_CONDITION_SUBORDINATE_NAME"] = "Structure de l'organisation";
$MESS["PP_USER_CONDITION_SUBORDINATE_TEXT"] = "Calculer les votes supplémentaires pour l'autorité d'un utilisateur en fonction de la structure de l'organisation.";
$MESS["INTRANET_RATING_USER_SUBORDINATE_NAME"] = "Structure de l'organisation en fonction des votes supplémentaires";
$MESS["INTRANET_RATING_USER_SUBORDINATE_DESC"] = "La valeur est basée sur les données calculées par la règle \"Structure de l'organisation\".";
$MESS["INTR_IBLOCK_TOP_SECTION_WARNING"] = "La structure de l'organisation peut ne contenir qu'une seule section niveau supérieur.";
$MESS["INTRANET_USER_INVITATION_NAME"] = "Inviter des personnes";
$MESS["INTRANET_USER_INVITATION_DESC"] = "#EMAIL_TO# - adresse e-mail de la personne invitée
#LINK# - lien d'activation du nouvel utilisateur";
$MESS["INTRANET_USER_ADD_NAME"] = "Ajouter des utilisateurs";
$MESS["INTRANET_USER_ADD_DESC"] = "#EMAIL_TO# - adresse e-mail du nouvel utilisateur
#LINK# - URL de l'intranet";
$MESS["INTRANET_MAILDOMAIN_NOCOMPLETE_MESSAGE"] = "<p>Nous avons remarqué que vous avez tenté de créer un domaine pour votre organisation mais n'avez jamais finalisé l'enregistrement.</p>

<p>Si vous avez besoin d'aide, ouvrez la page des paramètres d'e-mail pour obtenir des instructions détaillées sur la configuration de votre service d'e-mail et pour finaliser l'enregistrement de domaine Bitrix24.</p>

<p>Si vous voulez voir le déroulement habituel, nous pouvons mettre à votre disposition un certain nombre d'exemples d'utilisation de domaines d'e-mail. Les exemples mettront en relief la magie derrière l'enregistrement de domaine, la création de boîte mail et l'utilisation du tout avec Bitrix24.</p>

<p><a href=\"#LEARNMORE_LINK#\">En savoir plus</a></p>

<p>Pour toute question, veuillez contacter notre <a href=\"#SUPPORT_LINK#\">assistance</a>.</p>
";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_NAME"] = "Créer des boîtes mail pour les utilisateurs dans un domaine";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_SUBJECT"] = "E-mail professionnel pour vos utilisateurs";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_MESSAGE"] = "<p>Vous avez enregistré le domaine et créé votre boîte mail. Cependant vos employés ne savent pas qu'ils peuvent créer leur propre boîte mail dans votre domaine.</p>

<p>Partagez vos connaissances et aidez-les à apprendre à créer leur propre boîte mail, ou à enregistrer une boîte mail sur leur page Bitrix24.</p>

<p><a href=\"#LEARNMORE_LINK#\">Comment enregistrer des boîtes mail pour vos employés</a></p>

<p>Pour toute question, veuillez contacter notre <a href=\"#SUPPORT_LINK#\">assistance</a>.</p>
";
$MESS["INTRANET_MAILDOMAIN_NOREG_MESSAGE"] = "<p>Il semblerait que vous alliez enregistrer un domaine pour votre société mais ne parveniez pas à choisir un bon nom.</p>

<p>Nous pouvons mettre à votre disposition un certain nombre d'exemples d'utilisation de domaines d'e-mail pour vous inspirer. Grâce aux exemples vous apprendrez comment créer une boîte mail et comment l'utiliser dans Bitrix24.</p>

<p><a href=\"#LEARNMORE_LINK#\">Comment enregistrer des boîtes mail pour vos employés</a></p>

<p>Pour toute question, veuillez contacter notre <a href=\"#SUPPORT_LINK#\">assistance</a>.</p>";
$MESS["INTR_INSTALL_RATING_RULE"] = "Calculer les votes supplémentaires pour l'autorité en fonction de la structure de l'organisation.";
$MESS["INTR_OPTION_IBLOCK_STRUCTURE"] = "Bloc d'information de la structure de l'organisation";
$MESS["INTR_OPTION_IBLOCK_CALENDAR"] = "Bloc d'information pour les calendriers d'utilisateur";
$MESS["INTR_PROP_EMP_TITLE"] = "Lien vers l'utilisateur";
$MESS["COMPANY_MENU_EMPLOYEES"] = "Trouver utilisateur";
$MESS["COMPANY_MENU_STRUCTURE"] = "Structure de l'organisation";
$MESS["COMPANY_TITLE"] = "Trouver utilisateur";
$MESS["SERVICES_MENU_NOVICE"] = "Pour les nouveaux utilisateurs";
$MESS["SERVICES_TITLE"] = "Informations pour les nouveaux utilisateurs";
$MESS["SERVICES_ORG_LIST"] = "Mon organisation";
$MESS["INTR_ABSENCE_USER"] = "Sélectionner une personne absente *";
$MESS["BX24_INVITE_DIALOG_EMAIL"] = "Entrez les adresses e-mail des personnes que vous voulez inviter. Séparez les différentes entrées d'une virgule ou d'un espace.";
$MESS["BX24_INVITE_DIALOG_INVITED"] = "<b>C'est bon !</b><br>Les invitations ont été envoyées aux adresses indiquées.<br><br><a style=\"white-space:nowrap;\" href=\"/company/?show_user=inactive\">Cliquez ici</a> pour voir les utilisateurs que vous venez d'inviter.";
$MESS["BX24_INVITE_DIALOG_ADDED"] = "<b>Félicitations !</b><br>Une notification d'adhésion à l'intranet a été envoyée à cet utilisateur.<br><br>Vérifiez quels nouveaux utilisateurs vous avez invité dans le <a style=\"white-space:nowrap;\" href=\"#SITE_DIR#company/?show_user=inactive\">répertoire des utilisateurs</a>.";
$MESS["BX24_INVITE_DIALOG_INVITE_LIMIT"] = "Vous ne pouvez pas inviter plus d'utilisateurs parce que vous dépasseriez les conditions de votre licence.";
$MESS["BX24_INVITE_DIALOG_EMPLOYEE"] = "utilisateur";
$MESS["CONFIG_COMPANY_NAME"] = "Nom de l'organisation";
$MESS["CONFIG_COMPANY_TITLE_NAME"] = "Le nom de l'organisation à afficher dans l'en-tête";
$MESS["CONFIG_LOGO_24"] = "Ajouter \"24\" au logo de l'entreprise";
$MESS["CONFIG_CLIENT_LOGO"] = "Logo de l'organisation";
$MESS["CONFIG_OTP_SECURITY"] = "Activer l'identification en deux étapes pour tous les utilisateurs";
$MESS["CONFIG_OTP_SECURITY2"] = "Rendre l'identification en deux étapes obligatoire pdour tous les utilisateurs";
$MESS["CONFIG_OTP_SECURITY_DAYS"] = "Préciser combien de temps tous les utilisateurs ont<br/>pour activer l'identification en deux étapes";
$MESS["CONFIG_OTP_SECURITY_INFO"] = "Nous avons développé une procédure d'activation conviviale de l'identification en deux étapes pour que chaque utilisateur puisse le faire sans assistance experte.<br/><br/>
Un message sera envoyé à chaque utilisateur pour l'informer qu'il doit activer l'identification en deux étapes avant la fin de la période indiquée. Les utilisateurs qui ne le font pas 
ne seront plus en mesure de s'identifier.";
$MESS["CONFIG_OTP_SECURITY_INFO2"] = "<br/>Pour activer l'identification en deux étapes, un utilisateur doit installer l'application OTP Bitrix24 sur son téléphone portable. Cette appli peut être téléchargée sur l'AppStore ou GooglePlay.<br/><br/>
Les utilisateurs qui ne disposent pas d'un appareil mobile approprié doivent s'équiper d'un outil spécial : l'eToken Pass. Vous pouvez en trouver un chez plusieurs revendeurs, comme :
<a target=_blank href=\"http://www.safenet-inc.com/multi-factor-authentication/authenticators/pki-usb-authentication/etoken-pro/\">www.safenet-inc.com</a>,
<a target=_blank href=\"http://www.authguard.com/eToken-PASS.asp\">www.authguard.com</a>.
Retrouvez d'autres revendeurs sur <a target=_blank href=\"https://www.google.com/?q=buy+eToken+PASS&spell=1#safe=off&q=buy+eToken+PASS\">Google</a>.
<br/><br/>
Vous pouvez aussi désactiver l'identification en deux étapes pour certains utilisateurs. Cependant, cette solution augmentera le risque d'accès non autorisé à votre Bitrix24 si les identifiants d'un de ces utilisateurs venaient à être volés. En tant qu'administrateur, vous pouvez désactiver l'identification en deux étapes d'un utilisateur dans son profil.";
$MESS["CONFIG_OTP_POPUP_TEXT"] = "Voici un texte que vous pouvez poster sur le Flux d'activités<br/>pour que vos utilisateurs puissent le lire.<br/><br/>
Informez vos collègues de l'identification en deux étapes,<br/>de la procédure de configuration et de la nouvelle méthode d'identification<br/>qu'ils devront utiliser pour se connecter à leur Bitrix24.";
$MESS["CONFIG_OTP_ADMIN_IS_REQUIRED_INFO"] = "Avant de migrer vos utilisateurs vers le système d'identification en deux étapes, configurez-le pour votre compte.<br/><br/>Veuillez continuer en l'activant dans la page Sécurité de votre profil.";
$MESS["CONFIG_OTP_IMPORTANT_TEXT"] = "Aujourd'hui, vous utilisez votre identifiant et votre mot de passe pour vous connecter à votre Bitrix24. Les données personnelles et professionnelles sont protégées par un cryptage de données. Cependant, des personnes dotées de mauvaises intentions peuvent utiliser certains outils pour s'introduire dans votre ordinateur et voler ces identifiants.

Afin de vous protéger contre ces menaces possibles, nous avons activé une sécurité supplémentaire pour Bitrix24 : l'identification en deux étapes.

L'identification est une système conçu spécialement pour protéger contre les logiciels de piratages, et tout particulièrement contre le vol de mot de passe. À chaque fois que vous vous connectez au système, vous devrez passer deux niveaux de vérification. Tout d'abord, vous devrez saisir votre identifiant et votre mot de passe, puis vous devrez fournir un code de sécurité à usage unique envoyé sur votre téléphone portable.

Cette sécurité supplémentaire rendra les données professionnelles encore plus sûres, même si les identifiants venaient à être volés.

Vous avez 5 jours pour activer cette fonctionnalité.

Pour configurer la nouvelle procédure d'identification, veuillez vous rendre sur votre profil et sélectionner la page \"Paramètres de sécurité\".

Si vous ne disposez pas d'un appareil mobile approprié pour faire tourner l'application, ou si vous rencontrez un quelconque problème, veuillez nous en informer en commentant ce message.";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_JOIN"] = "Publier une nouvelle notification utilisateur sur le chat public";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_LEAVE"] = "Publier une notification de révocation sur le chat public";
$MESS["CONFIG_ALLOW_NEW_USER_LF"] = "Publier une nouvelle notification utilisateur sur le Flux d'activité";
$MESS["MP_ADD_APP_SCOPE_DEPARTMENT"] = "Structure de l'organisation";
$MESS["APP_RIGHTS"] = "Permissions d'accès des utilisateurs";
$MESS["iblock_dep_name1"] = "Mon organisation";
$MESS["main_site_name"] = "Mon organisation";
$MESS["SONET_TASK_TITLE_2"] = "Inviter de nouveaux utilisateurs";
$MESS["SONET_TASK_DESCRIPTION_2"] = "Inviter de nouveaux utilisateurs à rejoindre le portail intranet";
$MESS["UF_PUBLIC"] = "Visible à tous dans l'extranet";
$MESS["B24_NEW_USER_TITLE"] = "Un nouvel utilisateur a été ajouté";
$MESS["B24_NEW_USER_TITLE_SETTINGS"] = "Nouveaux utilisateurs";
$MESS["B24_NEW_USER_TITLE_LIST"] = "Nouvel utilisateur";
$MESS["B24_NEW_USER_MENTION"] = "vous a mentionné dans le commentaire d'un message à propos d'un utilisateur nouvellement ajouté #title#.";
$MESS["B24_NEW_USER_MENTION_M"] = "vous a mentionné dans le commentaire d'un message à propos d'un utilisateur nouvellement ajouté #title#.";
$MESS["B24_NEW_USER_MENTION_F"] = "vous a mentionné dans le commentaire d'un message à propos d'un utilisateur nouvellement ajouté #title#.";
$MESS["BITRIX24_USER_INVITATION_NAME"] = "Inviter des personnes";
$MESS["BITRIX24_USER_INVITATION_DESC"] = "#EMAIL_FROM# - e-mail de l'utilisateur invitant
#EMAIL_TO# - e-mail de l'utilisateur invité
#LINK# - lien d'activation du nouvel utilisateur";
$MESS["MENU_STRUCTURE"] = "Structure de l'organisation";
$MESS["TITLE1"] = "Structure de l'organisation";
$MESS["TITLE2"] = "Structure de l'organisation";
$MESS["FIELD_empl_num"] = "Nombre d'utilisateurs";
$MESS["LICENSE_CRM_FEATURE_16"] = "Journal d'accès CRM";
$MESS["LICENSE_TEL_FEATURE_11"] = "Extensions d'utilisateur";
$MESS["LICENSE_TEL_FEATURE_16"] = "Appel simultané à tous les utilisateurs disponibles";
$MESS["LICENSE_MORE_USERS"] = "Plus d'utilisateurs";
$MESS["DEMO_FEATURE_1"] = "Utilisateurs illimités";
$MESS["DEMO_INFO_DESC_1"] = "Si vous avez invité plus de 12 utilisateurs pendant la version d'essai, une fois cette dernière terminée seuls les 12 premiers utilisateurs auront accès à leur compte Bitrix24. Si vous voulez rétablie les accès de tous les utilisateurs, vous devez <a class=\"link\" href=\"/settings/license_all.php\">changer d'offre</a>. ";
$MESS["BX24_EXTRANET2INTRANET_DESC"] = "<b>#FULL_NAME#</b> est un utilisateur externe.<br><br>Veuillez noter que vous <b>ne pourrez pas</b> retransférer plus tard cet utilisateur de l'intranet vers l'extranet.<br><br>Pour transférer #FULL_NAME# vers l'intranet, veuillez sélectionner une division de destination.";
$MESS["BX24_EXTRANET2INTRANET_SUCCESS"] = "L'utilisateur a été bien transféré. Ils peuvent maintenant accéder au contenu interne de l'intranet selon leurs permissions.";
$MESS["MENU_EMPLOYEE"] = "Utilisateurs";
$MESS["CONFIG_USERS2"] = "Utilisateurs :";
$MESS["DEMO_INFO_1"] = "Utilisateurs";
$MESS["ISV_set_department_head"] = "<b>#NAME#</b> a été nommé chef de la division <b>#DEPARTMENT#</b>.";
$MESS["ISV_change_department"] = "<b>#NAME#</b> a été transféré vers <b>#DEPARTMENT#</b>.";
$MESS["authprov_name"] = "Utilisateurs et divisions";
$MESS["LM_POPUP_TAB_STRUCTURE"] = "Utilisateurs et divisions";
$MESS["LM_POPUP_CHECK_STRUCTURE"] = "Utilisateurs de toutes les divisions et sous-divisions";
$MESS["BITRIX24_SEARCH_EMPLOYEE"] = "Utilisateurs";
$MESS["SONET_GL_DESTINATION_G2"] = "À tous les utilisateurs";
$MESS["BLOG_DESTINATION_ALL"] = "À tous les utilisateurs";
$MESS["MPF_DESTINATION_1"] = "Ajouter des utilisateurs, groupes ou divisions";
$MESS["EC_DESTINATION_1"] = "Ajouter des utilisateurs, groupes ou divisions";
$MESS["MPF_DESTINATION_3"] = "Tous les utilisateurs";
$MESS["INTRANET_USTAT_TOGGLE_COMPANY"] = "Organisation";
$MESS["INTRANET_TAB_USER_STRUCTURE"] = "Organisation";
$MESS["MENU_COMPANY"] = "ORGANISATION";
$MESS["BLOG_GRATMEDAL_1"] = "Ajouter des utilisateurs";
$MESS["SONET_GCE_T_USER_INTRANET"] = "Utilisateurs de l'organisation :";
$MESS["SONET_GCE_T_INVITATION_EMPLOYEES"] = "Inviter des utilisateurs";
$MESS["SONET_GCE_T_ADD_EMPLOYEE"] = "Ajouter un utilisateur";
$MESS["SONET_GCE_T_DEST_TITLE_EMPLOYEE"] = "Inviter des utilisateurs:";
$MESS["EC_COMPANY_CALENDAR"] = "Calendrier de l'organisation";
$MESS["MENU_COMPANY_CALENDAR"] = "Calendrier de l'organisation";
$MESS["SONET_UP1_WORK_POSITION"] = "Position dans la communauté";
$MESS["ISV_EMPLOYEES"] = "UTILISATEURS";
$MESS["MENU_COMPANY_SECTION"] = "Communauté";
$MESS["IM_CL_STRUCTURE"] = "Communauté";
$MESS["IM_CL_USER_B24"] = "Membre";
$MESS["IM_C_ABOUT_CHAT"] = "Un chat privé est disponible uniquement pour les utilisateurs invités. #BR##BR# Ce chat est parfait pour les discussions d'affaires impliquant et concernant certaines personnes. #BR##BR# Les invitations à rejoindre ce chat ne sont pas limitées à vos collègues ; envoyez des invitations à vos clients, partenaires et d'autres personnes liées à votre entreprise pouvant utiliser Bitrix24. Pour ajouter un nouveau membre au chat, entrez les nom et prénom, l'adresse e-mail ou leur nom affiché. #BR##BR#Complétez #PROFILE_START#votre profil#PROFILE_END# pour que les autres personnes puissent vous trouver.";
$MESS["IM_C_ABOUT_OPEN"] = "Un chat public est ouvert à l'ensemble de vos collègues. #BR##BR# Utilisez ce chat pour discuter des sujets ayant une importance pour n'importe quelle personne de votre entreprise.#BR##BR# Lorsqu'un chat public est créé, une notification est envoyée au #CHAT_START#chat commun#CHAT_END#. Vos collègues peuvent ensuite voir les messages du chat et le rejoindre s'ils le souhaitent.#BR##BR# Votre premier message est essentiel pour que les autres personnes soient intéressées à rejoindre votre nouveau chat.";
$MESS["IM_C_ABOUT_PRIVATE"] = "Un chat de personne à personne peut uniquement être vu par vous et par votre contact. #BR##BR# Trouvez une personne avec laquelle vous souhaitez parler avec son nom, sa fonction ou son département. #BR##BR# Vous pouvez également discuter avec n'importe quel client, partenaire ou toute autre personne utilisant Bitrix24. Trouvez-les avec leur nom, leur adresse e-mail ou le nom affiché que vous pensez qu'ils utilisent. #BR##BR#Complétez #PROFILE_START#votre profil#PROFILE_END# pour que les autres personnes puissent vous trouver.";
$MESS["IM_CTL_CHAT_STRUCTURE"] = "Structure de la communauté";
$MESS["IM_M_CALL_ST_TRANSFER"] = "Redirection d'appel : en attente d'une réponse";
$MESS["IM_M_CALL_ST_TRANSFER_1"] = "Redirection d'appel : l'utilisateur n'a pas répondu";
$MESS["TITLE"] = "Trouver utilisateur";
?>