<?
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT"] = "Veuillez nous rejoindre dans notre nouveau compte Bitrix24. C'est un endroit où tout le monde peut communiquer, collaborer sur des tâches et des projets, gérer des clients, et bien plus encore.";
$MESS["INTR_MAIL_INP_PUBLIC_DOMAIN"] = "Les utilisateurs peuvent créer des boîtes de réception sur ce domaine";
$MESS["INTR_MAIL_MANAGE"] = "Configurer les boîtes de réception d'utilisateur";
$MESS["INTR_MAIL_DESCR_B24"] = "Un serveur d'e-mail gratuit pour votre société ! Un stockage illimité pour vos e-mails accompagné d'un antivirus et d'un antispam pour garder votre boîte propre. Migrez vos e-mails professionnels sur Bitrix24 ! Créez vos propres boîtes de réception avec le domaine de votre société, sur Bitrix24, ou associez des boîtes de réception d'autres services d'e-mail.";
$MESS["INTR_MAIL_DESCR_BOX"] = "Un serveur e-mail prêt à l'emploi pour votre société. Profitez d'un stockage illimité, d'un antivirus et d'un antispam. Créez des boîtes de réception sur le domaine de votre société ou associez des services d'e-mails existants.";
$MESS["INTR_MAIL_DOMAIN_PUBLIC"] = "autoriser les utilisateurs à créer des boîtes de réception sur le nouveau domaine";
$MESS["INTR_MAIL_NODOMAIN_USER_INFO"] = "Veuillez contacter votre administrateur intranet pour associer un domaine professionnel.";
$MESS["INTR_MAIL_MANAGE_HINT"] = "Créez une boîte de réception pour chaque utilisateur de votre organisation. Utilisez cette interface pour gérer des boîtes de réception professionnelles : créez, associez ou supprimez des boîtes de réception, ou même modifiez-en les mots de passe.";
$MESS["INTR_MAIL_MANAGE_SETUP_ALLOW_CRM"] = "Les employés peuvent connecter leur boîte de réception à la GRC";
$MESS["INTRANET_OTP_MANDATORY_TITLE"] = "Une sécurité supplémentaire pour vos données professionnelles";
$MESS["INTR_ABSC_TPL_EDIT_ENTRIES"] = "Gestion des utilisateurs";
$MESS["ISL_TPL_NOTE_NULL"] = "Votre recherche n'a renvoyé aucun utilisateur.";
$MESS["INTR_ABSC_TPL_IMPORT"] = "Importer des utilisateurs";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_OUTLOOK_TITLE"] = "Exporter la liste d'utilisateurs sous la forme de contacts Outlook";
$MESS["INTR_COMP_IS_TPL_BANNER_OUTLOOK"] = "Vous pouvez exporter des utilisateurs sous la forme de contacts Microsoft Outlook";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_CARDDAV_TITLE"] = "Synchronisez le registre des utilisateurs avec les logiciels et matériels prenant en charge CardDAV (iPhone, iPad, etc.)";
$MESS["INTR_IS_TPL_SEARCH"] = "Trouver un utilisateur";
$MESS["INTR_IS_TPL_SEARCH_DEPARTMENT"] = "Trouver un utilisateur de ce département";
$MESS["INTR_IS_TPL_OUTLOOK"] = "Exporter les utilisateurs vers Outlook";
$MESS["INTR_IS_TPL_OUTLOOK_TITLE"] = "Exporter la liste d'utilisateurs sous la forme de contacts Outlook";
$MESS["INTR_ISBN_TPL_FILTER_ALL"] = "Pour toute l'organisation";
$MESS["INTR_ISIN_TPL_ALL"] = "Pour toute l'organisation";
$MESS["INTR_ISL_TPL_NOTE_NULL"] = "Votre recherche n'a renvoyé aucun utilisateur.";
$MESS["ISV_ERROR_dpt_not_empty"] = "Vous devez déplacer tous les utilisateurs du département avant de pouvoir supprimer ce dernier.";
$MESS["ISV_EMP_LIST"] = "Liste des utilisateurs";
$MESS["ISV_confirm_delete_department"] = "Voulez-vous vraiment supprimer ce département ? Cela déplacera tous les sous-départements et utilisateurs concernés.";
$MESS["ISV_confirm_set_head"] = "Désigner #EMP_NAME# chef du département #DPT_NAME# ?";
$MESS["ISV_confirm_change_department_0"] = "Transférer #EMP_NAME# vers #DPT_NAME# ?";
$MESS["ISV_confirm_change_department_1"] = "Assigner #EMP_NAME# à #DPT_NAME# ?";
$MESS["ISV_add_emp"] = "Ajouter un utilisateur";
$MESS["ISV_EMP_COUNT_1"] = "#NUM# utilisateur";
$MESS["ISV_EMP_COUNT_2"] = "#NUM# utilisateurs";
$MESS["ISV_EMP_COUNT_3"] = "#NUM# utilisateurs";
$MESS["ISV_EMP_COUNT_4"] = "#NUM# utilisateurs";
$MESS["ISV_EMP_COUNT_21"] = "#NUM# utilisateur";
$MESS["ISV_EMP_COUNT_22"] = "#NUM# utilisateurs";
$MESS["ISV_EMP_COUNT_23"] = "#NUM# utilisateurs";
$MESS["ISV_EMP_COUNT_24"] = "#NUM# utilisateurs";
$MESS["ISV_EMP_COUNT_31"] = "#NUM# utilisateur";
$MESS["ISV_EMP_COUNT_32"] = "#NUM# utilisateurs";
$MESS["ISV_EMP_COUNT_33"] = "#NUM# utilisateurs";
$MESS["ISV_EMP_COUNT_34"] = "#NUM# utilisateurs";
$MESS["ISV_EMP_COUNT_41"] = "#NUM# utilisateur";
$MESS["ISV_EMP_COUNT_42"] = "#NUM# utilisateurs";
$MESS["ISV_EMP_COUNT_43"] = "#NUM# utilisateurs";
$MESS["ISV_EMP_COUNT_44"] = "#NUM# utilisateurs";
$MESS["ISV_EMP_COUNT_MUL"] = "#NUM# utilisateurs";
$MESS["ISV_B24_INVITE"] = "Inviter des utilisateurs";
$MESS["INTR_EMP_WINDOW_TITLE"] = "Sélectionner un utilisateur";
$MESS["INTR_EMP_SUBMIT_TITLE"] = "Choisissez l'utilisateur sélectionné";
$MESS["INTR_EMP_CANCEL_TITLE"] = "Annuler la sélection d'utilisateur";
$MESS["INTR_EMP_SEARCH"] = "rechercher un utilisateur";
$MESS["INTRANET_USTAT_SECTION_SOCNET_HELP_INVOLVEMENT"] = "Affiche le pourcentage d'employés qui utilisent le réseau social pendant la période choisie.";
$MESS["INTRANET_USTAT_SECTION_LIKES_HELP_INVOLVEMENT"] = "Pourcentage d'utilisateurs qui ont utilisé la fonction \"J'aime\" pendant la période choisie.";
$MESS["INTRANET_USTAT_SECTION_TASKS_HELP_INVOLVEMENT"] = "Pourcentage d'utilisateurs qui ont utilisé des tâches pendant la période choisie.";
$MESS["INTRANET_USTAT_SECTION_IM_HELP_INVOLVEMENT"] = "Affiche le pourcentage d'utilisateurs qui ont envoyé un message instantané ou passé un appel vidéo/audio pendant la période choisie.";
$MESS["INTRANET_USTAT_SECTION_DISK_HELP_INVOLVEMENT"] = "Pourcentage d'utilisateurs implémentant Bitrix24.Drive.";
$MESS["INTRANET_USTAT_SECTION_MOBILE_HELP_INVOLVEMENT"] = "Pourcentage d'utilisateurs qui ont utilisé l'application mobile pendant la période choisie.";
$MESS["INTRANET_USTAT_SECTION_CRM_HELP_INVOLVEMENT"] = "Pourcentage d'utilisateurs qui ont été actifs dans la GRC pendant la période choisie. Ce nombre est/peut être limité par le nombre d'utilisateurs ayant accès à la GRC.";
$MESS["INTRANET_USTAT_TELLABOUT_SOCNET_TITLE"] = "Unité dans la force de travail";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_SOCNET"] = "Utilisation de fonctionnalité : réseau social";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_LIKES"] = "Utilisation de fonctionnalité : j'aime";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_TASKS"] = "Utilisation de fonctionnalité : tâches";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_IM"] = "Utilisation de fonctionnalité : chat";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_DISK"] = "Utilisation de fonctionnalité : lecteur";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_MOBILE"] = "Utilisation de fonctionnalité : application mobile";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_CRM"] = "Utilisation de fonctionnalité : GRC";
$MESS["INTRANET_USTAT_COMPANY_GRAPH_TITLE"] = "Impulsion d'organisation";
$MESS["INTRANET_USTAT_COMPANY_HELP_GENERAL"] = "L'Impulsion d'organisation est un indicateur général de l'activité des utilisateurs sur le portail au moment présent (composé de tous les utilisateurs de l'heure précédente).";
$MESS["INTRANET_USTAT_COMPANY_HELP_ACTIVITY"] = "Index d'activité : composé de toutes les activités des utilisateurs sur tout l'intranet pour la période choisie. L'index afficher le niveau d'activité des utilisateurs travaillant avec divers outils.";
$MESS["INTRANET_USTAT_COMPANY_HELP_INVOLVEMENT"] = "Niveau d'engagement: ceci est un indicateur clé capable d'afficher à quel point les utilisateurs se sont familiarisés avec les capacités de Bitrix24. Il indique le pourcentage d'utilisation à l'échelle de l'organisation d'au moins un quart des outils fournis.";
$MESS["INTRANET_USTAT_COMPANY_HELP_RATING"] = "La note est déterminée par l'index d'activité individuelle moyen de tous les utilisateurs qui ont effectué au moins 1 action pendant la période choisie.";
$MESS["INTRANET_USTAT_WIDGET_TITLE"] = "Impulsion d'organisation";
$MESS["INTRANET_USTAT_WIDGET_ACTIVITY_HELP"] = "Niveau d'activité actuel de l'organisation (composé de tous les utilisateurs de l'heure précédente)";
$MESS["INTRANET_USTAT_WIDGET_INVOLVEMENT_HELP"] = "Engagement actuel des utilisateurs. Cela affiche le pourcentage de tous les utilisateurs du jour qui ont utilisé au moins quatre outils différents dans l'intranet.";
$MESS["INTRANET_USTAT_USER_GRAPH_COMPANY"] = "Moyenne de l'organisation";
$MESS["INTRANET_USTAT_USER_ACTIVITY_COMPANY_TITLE"] = "Moyenne de<br>l'organisation";
$MESS["INTRANET_USTAT_USER_HELP_RATING"] = "Votre position dans le résumé de la <b>note d'activité</b> listant tous les utilisateurs qui ont utilisé Bitrix24 au moins une fois pendant la période choisie.";
$MESS["INTRANET_USTAT_USER_HELP_ACTIVITY_DEPT"] = "Valeur moyenne des actions que votre département a effectuées sur Bitrix24 via l'un des sept outils disponibles pendant la période choisie.<br><br> <b>Utilisez la valeur moyenne du département pour voir si votre activité est meilleure que celle des autres.";
$MESS["INTRANET_USTAT_USER_HELP_ACTIVITY_COMPANY"] = "Valeur moyenne des actions que votre organisation a effectuées sur Bitrix24 via l'un des sept outils disponibles pendant la période choisie.<br><br> <b>Utilisez la valeur moyenne de l'organisation pour voir si votre activité est meilleure que celle de toute l'organisation.";
$MESS["BM_USR_CNT"] = "Nombre d'utilisateurs";
$MESS["WD_USER"] = "Documents des utilisateurs";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_USER_ADD"] = "Inviter un utilisateur";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_USER_INVITE"] = "Inviter des utilisateurs";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_USER_ACTIVE"] = "Utilisateurs";
$MESS["INTR_IS_TPL_ACTION_INVITE"] = "Inviter des utilisateurs";
$MESS["INTR_ISE_TPL_NOTE_NULL"] = "Votre recherche n'a renvoyé aucun utilisateur.";
$MESS["INTR_CONFIRM_FIRE"] = "L'utilisateur ne pourra plus accéder au portail et ne sera plus affiché dans la structure de l'organisation, mais toutes ses données (fichiers, messages, tâches etc) seront sauvegardés.\\n\\nVoulez-vous vraiment refuser l'accès à cet utilisateur ?";
$MESS["INTR_CONFIRM_RESTORE"] = "L'utilisateur pourra se connecter au portail et apparaître dans la structure de l'entreprise.\\n\\nVoulez-vous vraiment donner l'accès à cet utilisateur ?";
$MESS["INTR_CONFIRM_DELETE"] = "L'invitation sera définitivement supprimée.\\n\\nVoulez-vous vraiment supprimer l'utilisateur ?";
$MESS["CT_BST_SEARCH_HINT"] = "trouver des personnes, des documents, et plus";
$MESS["SOCNET_CONFIRM_FIRE"] = "L'utilisateur ne pourra plus accéder au portail et ne sera plus affiché dans la structure de l'organisation, mais toutes ses données (fichiers, messages, tâches etc) seront sauvegardés.

Voulez-vous vraiment révoquer l'accès de cet utilisateur ?";
$MESS["SOCNET_CONFIRM_RECOVER"] = "L'utilisateur pourra se connecter au portail et apparaître dans la structure de l'entreprise.

Voulez-vous vraiment ouvrir l'accès à cet utilisateur ?";
$MESS["SOCNET_CONFIRM_DELETE"] = "L'invitation sera définitivement supprimée.

Voulez-vous vraiment supprimer l'utilisateur ?";
$MESS["ISL_WORK_COUNTRY"] = "Pays";
$MESS["ISL_WORK_CITY"] = "Ville";
$MESS["ISL_WORK_LOGO"] = "Logo de l'organisation";
$MESS["SOCNET_CONFIRM_FIRE1"] = "L'utilisateur ne pourra plus accéder au portail et ne sera plus affiché dans la structure de l'organisation, mais toutes ses données (fichiers, messages, tâches etc) seront sauvegardés.\\n\\nVoulez-vous vraiment refuser l'accès à cet utilisateur ?";
$MESS["SOCNET_CONFIRM_RECOVER1"] = "L'utilisateur pourra se connecter au portail et apparaître dans la structure de l'entreprise.\\n\\nVoulez-vous vraiment donner l'accès à cet utilisateur ?";
$MESS["TEMPLATE_DESCRIPTION"] = "Ce modèle est conçu pour accentuer les aspects sociaux de l'intranet et combiner les outils de réalisation et de productivité conventionnels dans un contexte qui facilite la communication sociale. La mise en page Intranet social est amplificateur de productivité très intuitif et ne nécessite que peu de temps d'adoption et d'orientation.";
$MESS["BITRIX24_INVITE"] = "Inviter des utilisateurs";
$MESS["BITRIX24_INVITE_ACTION"] = "Inviter des utilisateurs";
$MESS["BITRIX24_HELP_VIDEO_TITLE_13"] = "Structure de l'organisation";
$MESS["BITRIX24_HELP_VIDEO_TITLE_FULL_13"] = "Structure de l'organisation";
$MESS["AUTH_OTP_HELP_TEXT"] = "Pour améliorer la sécurité de votre Bitrix24, votre administrateur a activé une option supplémentaire : l'identification en deux étapes.<br/><br/>
L'identification en deux étapes comprend deux étapes successives de vérification. La première nécessite votre mot de passe principal. La seconde est constituée d'un code envoyé sur votre téléphone mobile (ou une clé électronique matérielle).<br/><br/>
<div class=\"login-text-img\"><img src=\"#PATH#/images/en/img1.png\"></div>
<br/>
Une fois l'identification en deux étapes activée, vous devrez passer deux écrans d'authentification :<br/><br/>
- saisissez votre e-mail et votre mot de passe ;<br/>
- puis saisissez un code à usage unique que vous obtenez via l'application pour mobile Bitrix OTP installée au moment d'activer l'identification en deux étapes.<br/><br/>
Exécutez l'application sur votre téléphone mobile :<br/><br/>
<div class=\"login-text-img\"><img src=\"#PATH#/images/en/img2.png\"></div>
<br/>
Saisissez le code affiché à l'écran :<br/><br/>
<div class=\"login-text-img\"><img src=\"#PATH#/images/en/img3.png\"></div>
<br/>
Si vous utilisez plusieurs Bitrix24, assurez-vous que le code corresponde au bon Bitrix24 avant de le saisir.<br/><br/>
<div class=\"login-text-img\"><img src=\"#PATH#/images/en/img4.png\"></div>
<br/>
Si vous n'avez pas eu l'occasion d'activer et de configurer l'identification en deux étapes ou si vous avez perdu votre téléphone, veuillez contacter votre administrateur pour récupérer l'accès à votre Bitrix24.<br/><br/>
Si vous êtes l'administrateur, veuillez contacter <a href=\"http://www.bitrix24.com/support/helpdesk/\">l'assistance technique de Bitrix</a> pour récupérer l'accès.";
$MESS["SERVICE_COMPANY_STRUCTURE"] = "Structure de la société";
$MESS["wiz_slogan"] = "Bitrix";
$MESS["wiz_company_name"] = "Nom de l’entreprise :";
$MESS["wiz_company_logo"] = "Logo de l'organisation :";
$MESS["wiz_demo_structure"] = "Installer la structure de démonstration de l'organisation";
$MESS["IFS_EF_staff"] = "Personnel ; documents de référence ; structure de l'organisation ; utilisateurs honorés";
$MESS["IFS_EF_Blog"] = "Flux des blogs d'utilisateur ou de groupe";
$MESS["IFS_EF_Gallery"] = "Galeries photos des utilisateurs";
$MESS["BLOG_DEMO_MESSAGE_TITLE_1"] = "L'ouverture du portail intranet de l'organisation ";
$MESS["BLOG_DEMO_MESSAGE_BODY_1"] = "Cher collègues, le portail intranet est officiellement ouvert depuis aujourd'hui.

Cela signifie que chaque utilisateur dispose à présent des permissions pour consulter les informations publiques et a reçu un mot de passe pour accéder à son espace privé.

L'accès configurable aux espaces du portail nous apporte des fonctionnalités pour travailler en équipe : nous pouvons maintenant éditer des documents, créer des groupes de travail, élaborer des rapports... ensemble !

L'objectif du portail est de rendre les chaînes de communication entre utilisateurs aussi courtes que possible. Le portail est un moyen de communication rapide et moderne et se trouve être le meilleur moyen d'éviter la paperasse et de transformer les flux d'informations de l'entreprise en cours électroniques.

En utilisant le portail intranet, les utilisateurs de l'organisation peuvent obtenir des informations techniques, référentielles et juridiques, dont les standards et l'identité d'entreprise.

Les utilisateurs peuvent publier sur les forums, envoyer des demandes à l'assistance technique, fournir des services, partager et échanger des documents, discuter des dernières actualités de l'organisation, et recevoir les dernières informations des responsables.

Le portail deviendra un élément essentiel de notre organisation !";
$MESS["CAL_TYPE_COMPANY_NAME"] = "Calendriers de l'organisation";
$MESS["CAL_COMPANY_SECT_DESC_0"] = "Salons et conférences auxquels vous êtes impliqués";
$MESS["CAL_COMPANY_SECT_DESC_1"] = "Événements ayant lieu aux bureaux de Londres";
$MESS["CAL_COMPANY_SECT_DESC_2"] = "Événements ayant lieu aux bureaux de Paris";
$MESS["CAL_COMP_EVENT_DESC_3"] = "Formation pour les nouveaux utilisateurs";
$MESS["CRM_GADGET_NEW_COMPANY_TITLE"] = "Nouvelles sociétés";
$MESS["CRM_DEMO_EVENT_12_EVENT_NAME"] = "Champ \"Utilisateurs\" modifié";
$MESS["CRM_DEMO_EVENT_15_EVENT_NAME"] = "Champ \"Utilisateurs\" modifié";
$MESS["GENERAL_FORUM_DESCRIPTION"] = "Forum public pour les employés de la société. Discutez de vos affaires et échangez vos avis.";
$MESS["GENERAL_FORUM_MESSAGE_BODY"] = "Attention ! Les employés d'une société peuvent maintenant gérer leur stockage de fichiers dans leur espace privé.

Vous trouverez dans la section assistance les informations détaillées sur la gestion du stockage de fichiers et la méthode de traçage d'un stockage vers un lecteur de réseau : \"Mon profil - Fichiers\".

Si vous avez des questions sur la configuration du stockage de fichiers, vous pouvez envoyer une demande aux ingénieurs de l'assistance technique en utilisant le formulaire de demande d'assistance.";
$MESS["ABSENCE_FORM_2"] = "Choisissez une employé absent";
$MESS["W_IB_CLIENTS_TAB1"] = "edit1--#--Client--,--NAME--#--*Nom de la société--,--PROPERTY_PERSON--#--Directeur--,--PROPERTY_PHONE--#--Téléphone--;--";
$MESS["HONOR_FORM_2"] = "Sélectionner l'utilisateur à honorer";
$MESS["STATE_FORM_2"] = "Choisissez l'utilisateur dont le statut a été modifié";
$MESS["W_IB_ABSENCE_2_PREV"] = "Voyage d'affaire à la succursale.";
$MESS["EMPLOYEES_GROUP_DESC"] = "Tous les employés de l'entreprise inscrits sur le portail.";
$MESS["DIRECTION_GROUP_DESC"] = "Conseil d'administration de l'entreprise.";
$MESS["main_opt_user_comp_name"] = "Nom de la société";
$MESS["main_opt_user_comp_logo"] = "Logo de la société";
$MESS["ML_COL_NAME_0"] = "Photos des utilisateurs";
$MESS["ML_COL_DESC_0"] = "Collection de photos des utilisateurs";
$MESS["MEETING_DESCRIPTION"] = "Nous devrons organiser une réunion pour discuter du déploiement du portail intranet de notre entreprise.";
$MESS["SONET_GROUP_DESCRIPTION_3"] = "Sport et loisirs";
$MESS["SUBSCRIBE_POSTING_SUBJECT"] = "Nos photos !";
$MESS["SUBSCRIBE_POSTING_BODY"] = "Salut ! Nous avons téléversé de nouvelles photos de vacances.";
$MESS["authprov_check_d"] = "Tous les utilisateurs du département";
$MESS["authprov_check_dr"] = "Tous les utilisateurs du département et du sous département";
$MESS["EC_ADD_MEMBERS_FROM_STR"] = "De la structure de l'organisation";
$MESS["EC_ADD_MEMBERS_FROM_STR_TITLE"] = "Ajouter des participants à l'événement depuis la structure de l'organisation";
$MESS["EC_COMPANY_STRUCTURE"] = "Structure de l'organisation";
$MESS["EC_NO_COMPANY_STRUCTURE"] = "La structure de l'organisation n'a pas été trouvée.";
$MESS["BX24_INVITE_TITLE_INVITE"] = "Inviter des utilisateurs";
$MESS["BX24_INVITE_TITLE_ADD"] = "Ajouter un utilisateur";
$MESS["BX24_INVITE_DIALOG_MAX_COUNT_ERROR"] = "Le nombre d'invité dépasse les conditions de votre licence.";
$MESS["I_NEW_USER_TITLE"] = "Un nouvel utilisateur a été ajouté";
$MESS["I_NEW_USER_TITLE_SETTINGS"] = "Nouveaux utilisateurs";
$MESS["I_NEW_USER_TITLE_LIST"] = "Nouvel utilisateur";
$MESS["I_NEW_USER_MENTION"] = "vous a mentionné dans un commentaire sur une publication au sujet d'un nouvel utilisateur #title#.";
$MESS["I_NEW_USER_MENTION_M"] = "vous a mentionné dans un commentaire sur une publication au sujet d'un nouvel utilisateur #title#.";
$MESS["I_NEW_USER_MENTION_F"] = "vous a mentionné dans un commentaire sur une publication au sujet d'un nouvel utilisateur #title#.";
$MESS["PP_USER_CONDITION_SUBORDINATE_NAME"] = "Structure de l'organisation";
$MESS["PP_USER_CONDITION_SUBORDINATE_TEXT"] = "Calculer les vois supplémentaires pour l'autorité de l'utilisateur en fonction de la structure de l'organisation.";
$MESS["INTRANET_RATING_USER_SUBORDINATE_NAME"] = "Structure de l'organisation en fonction des voix supplémentaires";
$MESS["INTRANET_RATING_USER_SUBORDINATE_DESC"] = "La valeur est basée sur les données calculées par la règle \"Structure de l'organisation\".";
$MESS["INTR_IBLOCK_TOP_SECTION_WARNING"] = "La structure de l'organisation ne peut contenir qu'une seule section de niveau supérieur.";
$MESS["INTRANET_USER_INVITATION_NAME"] = "Inviter des personnes";
$MESS["INTRANET_USER_INVITATION_DESC"] = "#EMAIL_TO# - l'adresse e-mail de la personne invitée
#LINK# - le lien d'activation du nouvel utilisateur";
$MESS["INTRANET_USER_ADD_NAME"] = "Ajouter des utilisateurs";
$MESS["INTRANET_USER_ADD_DESC"] = "#EMAIL_TO# - adresse e-mail du nouvel utilisateur
#LINK# - URL intranet";
$MESS["INTRANET_MAILDOMAIN_NOCOMPLETE_MESSAGE"] = "<p>Nous avons remarqué que vous avez essayé de créer un domaine pour votre organisation mais n'avez jamais terminé l'inscription.</p>

<p>Si vous avez besoin d'aide, ouvrez la page des paramètres d'e-mail, lisez les instructions détaillées sur la configuration de votre service d'e-mail et finalisez l'inscription de votre domaine Bitrix24.</p>

<p>Si vous voulez voir comment cela se passe généralement, nous pouvons vous proposer plusieurs exemples d'utilisation de domaine d'e-mail. Ces exemples vous révéleront la magie cachée derrière l'inscription de domaine, la création d'une boîte de réception et l'utilisation du tout avec Bitrix24.</p>

<p><a href=\"#LEARNMORE_LINK#\">En apprendre plus</a></p>

<p>Si vous avez toujours des questions, vous pouvez contacter notre <a href=\"#SUPPORT_LINK#\">assistance technique</a>.</p>
";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_NAME"] = "Créer des boîtes de réception d'utilisateur dans ce domaine";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_SUBJECT"] = "Des e-mails d'entreprise pour vos utilisateurs";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_MESSAGE"] = "<p>Vous avez inscrit le domaine et créé votre boîte de réception. Vos employés ou utilisateurs n'ont cependant aucune idée de comment créer leur boîte de réception sur votre domaine.</p>

<p>Partagez vos connaissances et aidez-les à apprendre comment créer ou inscrire leur boîte de réception sur leur page Bitrix24.</p>

<p><a href=\"#LEARNMORE_LINK#\">Comment inscrire des boîtes de réception pour vos employés</a></p>

<p>Si vous avez des questions, veuillez contacter notre <a href=\"#SUPPORT_LINK#\">assistance technique</a>.</p>
";
$MESS["INTRANET_MAILDOMAIN_NOREG_MESSAGE"] = "<p>Vous étiez sur le point d'inscrire une domaine pour votre société, mais il semblerait que vous ne parveniez à trouver un bon nom.</p>

<p>Nous pouvons vous proposer plusieurs exemples d'utilisation de domaine d'e-mail. Grâce à ces exemples, vous apprendrez comment créer et utiliser une boîte de réception avec Bitrix24.</p>

<p><a href=\"#LEARNMORE_LINK#\">Comment inscrire des boîtes de réception pour vos employés</a></p>

<p>Si vous avez toujours des questions, veuillez contacter notre <a href=\"#SUPPORT_LINK#\">assistance technique</a>.</p>\"";
$MESS["INTR_INSTALL_RATING_RULE"] = "Calculez les voix supplémentaires pour l'autorité en fonction de la structure de l'organisation";
$MESS["INTR_OPTION_IBLOCK_STRUCTURE"] = "Bloc d'information de la Structure de l'organisation";
$MESS["INTR_OPTION_IBLOCK_CALENDAR"] = "Bloc d'information des calendriers d'utilisateur";
$MESS["INTR_PROP_EMP_TITLE"] = "Lien vers l'utilisateur";
$MESS["COMPANY_MENU_EMPLOYEES"] = "Trouver un utilisateur";
$MESS["COMPANY_MENU_STRUCTURE"] = "Structure de l'organisation";
$MESS["COMPANY_TITLE"] = "Trouver un utilisateur";
$MESS["SERVICES_MENU_NOVICE"] = "Pour les nouveaux utilisateurs";
$MESS["SERVICES_TITLE"] = "Informations pour les nouveaux utilisateurs";
$MESS["SERVICES_ORG_LIST"] = "Mon organisation";
$MESS["INTR_ABSENCE_USER"] = "Sélectionnez une personne absente *";
$MESS["BX24_INVITE_DIALOG_EMAIL"] = "Saisissez les adresses e-mail des personnes que vous voulez inviter. En cas d'entrées multiples, veuillez les séparer avec une virgule ou un espace.";
$MESS["BX24_INVITE_DIALOG_INVITED"] = "<b>Bravo !</b><br>Les invitations ont été envoyées aux adresses indiquées.<br><br>Pour voir les utilisateurs que vous venez d'inviter, <a style=\"white-space:nowrap;\" href=\"/company/?show_user=inactive\">cliquez ici</a>.";
$MESS["BX24_INVITE_DIALOG_ADDED"] = "<b>Félicitations !</b><br>Une notification d'adhésion à l'intranet a été envoyée à cet utilisateur.<br><br>Passez en revue les nouveaux utilisateurs que vous avez invité depuis le <a style=\"white-space:nowrap;\" href=\"#SITE_DIR#company/?show_user=inactive\">répertoire des utilisateurs</a>.";
$MESS["BX24_INVITE_DIALOG_INVITE_LIMIT"] = "Vous ne pouvez pas inviter plus d'utilisateur parce que vous allez dépasser les conditions de votre licence.";
$MESS["BX24_INVITE_DIALOG_EMPLOYEE"] = "utilisateur";
$MESS["CONFIG_COMPANY_NAME"] = "Nom de l'organisation";
$MESS["CONFIG_COMPANY_TITLE_NAME"] = "Nom de l'organisation à afficher en en-tête";
$MESS["CONFIG_LOGO_24"] = "Ajouter \"24\" au logo de l'entreprise";
$MESS["CONFIG_CLIENT_LOGO"] = "Logo de l'organisation";
$MESS["CONFIG_OTP_SECURITY"] = "Activer l'identification en deux étapes pour tous les utilisateurs";
$MESS["CONFIG_OTP_SECURITY2"] = "Rendre l'identification en deux étapes obligatoire pour tous les utilisateurs";
$MESS["CONFIG_OTP_SECURITY_DAYS"] = "Précisez la période pendant laquelle tous les utilisateurs<br/>devront activer l'identification en deux étapes";
$MESS["CONFIG_OTP_SECURITY_INFO"] = "Nous avons développé une identification en deux étapes conviviale pour permettre à tout utilisateur d'activer la procédure sans assistance experte.<br/><br/>
Un message sera envoyé à chaque employé pour l'informer qu'il devra activer l'identification en deux étapes avant la fin de la période entrée. Les utilisateurs qui ne le feront pas
ne pourront plus s'identifier.";
$MESS["CONFIG_OTP_SECURITY_INFO2"] = "<br/>Pour activer l'identification à deux étapes, un utilisateur doit installer l'application OTP Bitrix24 sur son téléphone mobile. Cette application peut être téléchargée sur l'AppStore ou sur GooglePlay.<br/><br/>
Les utilisateurs ne disposant pas de mobile approprié doivent s'équiper d'un équipement spécial : l'eToken Pass. De nombreux revendeurs en proposent, comme :
<a target=_blank href=\"http://www.safenet-inc.com/multi-factor-authentication/authenticators/pki-usb-authentication/etoken-pro/\">www.safenet-inc.com</a>,
<a target=_blank href=\"http://www.authguard.com/eToken-PASS.asp\">www.authguard.com</a>.
Trouvez d'autres revendeurs sur <a target=_blank href=\"https://www.google.com/?q=buy+eToken+PASS&spell=1#safe=off&q=buy+eToken+PASS\">Google</a>.
<br/><br/>
Vous pouvez aussi désactiver l'identification à deux étapes pour certains utilisateurs. Vous augmentez cependant les risques d'accès non autorisé à votre Bitrix24 si les identifiants de ces utilisateurs venaient à être volés. En tant qu'administrateur, vous pouvez désactiver l'identification à deux étapes d'un utilisateur dans son profil utilisateur.";
$MESS["CONFIG_OTP_POPUP_TEXT"] = "Vous pouvez publier le message suivant dans le flux d'activités<br/>pour communiquer à l'ensemble des utilisateurs.<br/><br/>Informez vos collègues sur l'identification en 2 étapes,<br/>afin qu'ils sachent comment paramétrer leur accès<br/>et qu'ils se familiarisent avec le nouveau mode de connexion à Bitrix24.";
$MESS["CONFIG_OTP_ADMIN_IS_REQUIRED_INFO"] = "Avant de faire passer vos utilisateurs à l'identification en deux étapes, configurez-la d'abord sur votre compte. <br/><br/>Veuillez continuer pour l'activer sur la page Sécurité de votre profil.";
$MESS["CONFIG_OTP_IMPORTANT_TEXT"] = "Aujourd'hui, vous utilisez votre identifiant et votre mot de passe pour vous connecter à votre Bitrix24. Les données personnelles et professionnelles sont protégées par un système de cryptage de données. Cependant, une personne dotée de mauvaises intentions peut utiliser certains outils pour s'introduire dans votre ordinateur et voler vos identifiants.

Afin de vous protéger contre le plus de menaces possible, nous avons ajouté une couche de sécurité supplémentaire à Bitrix24 : l'identification à deux étapes.

L'identification à deux étapes est un méthode particulière de protection contre les logiciels de piratages, notamment le vol de mot de passe. À chaque fois que vous vous connectez au système, vous devrez passer une vérification à deux niveaux. Tout d'abord vous devrez entrer votre e-mail et votre mot de passe, puis vous devrez fournir un code de sécurité à usage unique envoyé sur votre mobile.

Ainsi vos données professionnelles resteront en sécurité même en cas de vol de l'identifiant et du mot de passe d'un utilisateur.

Vous avez 5 jours pour activer cette fonctionnalité.

Pour configurer une nouvelle procédure d'identification, veuillez vous rendre dans votre profil, puis à la plge \"Paramètres de sécurité\".

Si vous ne possédez pas de mobile capable d'utiliser l'application, ou si vous rencontrez le moindre problème, veuillez nous en informer en commentant ce message.";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_JOIN"] = "Publier une notification de nouvel employé sur le chat public";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_LEAVE"] = "Publier une notification de révocation sur le chat public";
$MESS["CONFIG_ALLOW_NEW_USER_LF"] = "Publier une notification de nouvel employé sur le Flux d'activités";
$MESS["MP_ADD_APP_SCOPE_DEPARTMENT"] = "Structure de l'organisation";
$MESS["APP_RIGHTS"] = "Permissions d'accès de l'utilisateur";
$MESS["iblock_dep_name1"] = "Bitrix";
$MESS["main_site_name"] = "Bitrix";
$MESS["SONET_TASK_TITLE_2"] = "Inviter de nouveaux employés";
$MESS["SONET_TASK_DESCRIPTION_2"] = "Inviter de nouveaux employés à rejoindre le portail intranet";
$MESS["UF_PUBLIC"] = "Visible à tous dans l'extranet";
$MESS["B24_NEW_USER_TITLE"] = "Un nouvel utilisateur a été ajouté";
$MESS["B24_NEW_USER_TITLE_SETTINGS"] = "Nouveaux utilisateurs";
$MESS["B24_NEW_USER_TITLE_LIST"] = "Nouvel utilisateur";
$MESS["B24_NEW_USER_MENTION"] = "vous a mentionné dans un commentaire sur une publication au sujet d'un nouvel utilisateur #title#.";
$MESS["B24_NEW_USER_MENTION_M"] = "vous a mentionné dans un commentaire sur une publication au sujet d'un nouvel utilisateur #title#.";
$MESS["B24_NEW_USER_MENTION_F"] = "vous a mentionné dans un commentaire sur une publication au sujet d'un nouvel utilisateur #title#.";
$MESS["BITRIX24_USER_INVITATION_NAME"] = "Inviter des personnes";
$MESS["BITRIX24_USER_INVITATION_DESC"] = "#EMAIL_FROM# - e-mail de l'utilisateur invitant
#EMAIL_TO# - e-mail de l'utilisateur invité
#LINK# - lien d'activation du nouvel utilisateur";
$MESS["MENU_STRUCTURE"] = "Structure de l'organisation";
$MESS["TITLE1"] = "Structure de l'organisation";
$MESS["TITLE2"] = "Structure de l'organisation";
$MESS["FIELD_empl_num"] = "Nombre d'utilisateurs";
$MESS["LICENSE_CRM_FEATURE_16"] = "Journal d'accès GRC";
$MESS["LICENSE_TEL_FEATURE_11"] = "Extensions d'utilisateur";
$MESS["LICENSE_TEL_FEATURE_16"] = "Appel simultané à tous les utilisateurs disponibles";
$MESS["LICENSE_MORE_USERS"] = "Plus d'utilisateurs";
$MESS["DEMO_FEATURE_1"] = "Utilisateurs illimités";
$MESS["DEMO_INFO_DESC_1"] = "Si vous avez invité plus de 12 utilisateurs pendant la version d'essai, une fois cette dernière terminée seuls les 12 premiers utilisateurs auront accès à leur compte Bitrix24. Si vous voulez restaurer les accès de tous les utilisateurs, vous devez <a class=\"link\" href=\"/settings/license_all.php\">changer d'offre</a>. ";
$MESS["BX24_EXTRANET2INTRANET_DESC"] = "<b>#FULL_NAME#</b> est un utilisateur externe.<br><br>Veuillez noter que le transfert inverse d'un utilisateur de l'intranet vers l'extranet sera <b>impossible</b>. <br><br>Pour transférer #FULL_NAME# vers l'intranet, veuillez sélectionner le département de destination.";
$MESS["BX24_EXTRANET2INTRANET_SUCCESS"] = "L'utilisateur a bien été transféré. Il peut à présent accéder au contenu interne de votre intranet en fonction de ses permissions.";
$MESS["MENU_EMPLOYEE"] = "Utilisateurs";
$MESS["CONFIG_USERS2"] = "Utilisateurs :";
$MESS["DEMO_INFO_1"] = "Utilisateurs";
$MESS["ISV_set_department_head"] = "<b>#NAME#</b> a été nommé chef du service <b>#DEPARTMENT#</b>.";
$MESS["ISV_change_department"] = "<b>#NAME#</b> a été transféré à <b>#DEPARTMENT#</b>.";
$MESS["authprov_name"] = "Utilisateurs et départements";
$MESS["LM_POPUP_TAB_STRUCTURE"] = "Utilisateurs et départements";
$MESS["LM_POPUP_CHECK_STRUCTURE"] = "Tous les utilisateurs du département et du sous département";
$MESS["BITRIX24_SEARCH_EMPLOYEE"] = "Utilisateurs";
$MESS["SONET_GL_DESTINATION_G2"] = "À tous les utilisateurs";
$MESS["BLOG_DESTINATION_ALL"] = "À tous les utilisateurs";
$MESS["MPF_DESTINATION_1"] = "Ajouter des utilisateurs, des groupes ou des départements";
$MESS["EC_DESTINATION_1"] = "Ajouter des utilisateurs, des groupes ou des départements";
$MESS["MPF_DESTINATION_3"] = "Tous les utilisateurs";
$MESS["INTRANET_USTAT_TOGGLE_COMPANY"] = "Organisation";
$MESS["INTRANET_TAB_USER_STRUCTURE"] = "Organisation";
$MESS["MENU_COMPANY"] = "ORGANISATION";
$MESS["BLOG_GRATMEDAL_1"] = "Ajouter des utilisateurs";
$MESS["SONET_GCE_T_USER_INTRANET"] = "Utilisateurs de l'organisation :";
$MESS["SONET_GCE_T_INVITATION_EMPLOYEES"] = "Inviter des utilisateurs";
$MESS["SONET_GCE_T_ADD_EMPLOYEE"] = "Ajouter un utilisateur";
$MESS["SONET_GCE_T_DEST_TITLE_EMPLOYEE"] = "Inviter les utilisateurs :";
$MESS["EC_COMPANY_CALENDAR"] = "Calendrier de l'organisation";
$MESS["MENU_COMPANY_CALENDAR"] = "Calendrier de l'organisation";
$MESS["SONET_UP1_WORK_POSITION"] = "Position";
$MESS["ISV_EMPLOYEES"] = "EMPLOYÉS";
$MESS["MENU_COMPANY_SECTION"] = "Institution";
$MESS["IM_CL_STRUCTURE"] = "Service";
$MESS["IM_CL_USER_B24"] = "Employé";
$MESS["IM_C_ABOUT_CHAT"] = "Seules les personnes invitées peuvent voir le chat privé. #BR##BR# En plus de vos collègues et des employés de vos bureaux, vous pouvez inviter des clients, partenaires, fournisseurs... toute personne ayant son propre Bitrix24. Pour ajouter une nouvelle personne, saisissez son nom, e-mail ou surnom. #BR##BR##PROFILE_START#Éditez votre profil#PROFILE_END# pour permettre aux autres de vous retrouver.";
$MESS["IM_C_ABOUT_OPEN"] = "Un chat public est ouvert à l'ensemble de vos collègues. #BR##BR# Utilisez ce chat pour discuter des sujets ayant une importance pour n'importe quelle personne de votre entreprise.#BR##BR# Une fois une chat public créé, une notification est envoyée sur le #CHAT_START#chat commun#CHAT_END#. Vos collègues peuvent ensuite voir les messages du chat et le rejoindre s'ils le souhaitent.#BR##BR# Votre premier message est essentiel pour que les autres personnes soient intéressées à rejoindre votre nouveau chat.";
$MESS["IM_C_ABOUT_PRIVATE"] = "Un chat en tête à tête ne peut être vu que par vous et votre contact. #BR##BR# Trouvez la personne avec qui vous souhaitez discuter via son nom, sa fonction ou son service. #BR##BR# Vous pouvez également discuter avec n'importe quel client, partenaire, ou quiconque utilisant Bitrix24. Trouvez-les grâce à leur nom, adresse e-mail ou nom que vous pensez qu'ils utilisent. #BR##BR#Remplissez #PROFILE_START#votre profil#PROFILE_END# pour que les autres puissent vous retrouver.";
$MESS["IM_CTL_CHAT_STRUCTURE"] = "Structure des bureaux";
$MESS["IM_M_CALL_ST_TRANSFER"] = "Redirection d'appel : en attente d'une réponse";
$MESS["IM_M_CALL_ST_TRANSFER_1"] = "Redirection d'appel : l'utilisateur n'a pas répondu";
?>