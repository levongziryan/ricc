<?
$MESS["INTR_STRUCTURE_IBLOCK_MODULE"] = "Le module des blocs d'informations n'a pas été installé.";
$MESS["INTR_EDIT_TITLE"] = "Editer la sous-section";
$MESS["INTR_ADD_TITLE"] = "Ajouter la subdivision";
$MESS["INTR_STRUCTURE_NAME"] = "Nom de la subdivision";
$MESS["INTR_STRUCTURE_HEAD"] = "Dirigeant";
$MESS["INTR_STRUCTURE_DEPARTMENT"] = "Subdivision supérieure";
$MESS["INTR_STRUCTURE_SUCCESS"] = "Un service été ajouté.";
$MESS["INTR_STRUCTURE_ADD"] = "Ajouter";
$MESS["INTR_STRUCTURE_ADD_MORE"] = "Ajouter plus";
$MESS["INTR_STRUCTURE_EDIT"] = "Sauvegarder";
$MESS["INTR_UF_HEAD_CHOOSE"] = "Choisir dans la structure";
$MESS["INTR_USER_ERR_NO_RIGHT"] = "Vous ne disposez pas des droits nécessaires pour modifier";
?>