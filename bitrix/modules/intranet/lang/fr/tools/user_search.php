<?
$MESS["INTR_EMP_WINDOW_TITLE"] = "Choix d'un employé";
$MESS["INTR_EMP_SUBMIT"] = "Choisir";
$MESS["INTR_EMP_SUBMIT_TITLE"] = "Choisir un employé sélectionné";
$MESS["INTR_EMP_WINDOW_CLOSE"] = "Fermer";
$MESS["INTR_EMP_CANCEL"] = "Annuler";
$MESS["INTR_EMP_CANCEL_TITLE"] = "Annuler le choix de l'employé";
$MESS["INTR_EMP_WAIT"] = "En cours de chargement...";
?>