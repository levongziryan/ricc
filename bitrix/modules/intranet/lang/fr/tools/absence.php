<?
$MESS["INTR_ABSENCE_NO_TYPE"] = "(inconnu)";
$MESS["INTR_ABSENCE_USER"] = "Sélectionnez l'employé absent *";
$MESS["INTR_USER_CHOOSE"] = "Choisir dans la structure";
$MESS["INTR_ABSENCE_ADD"] = "Ajouter";
$MESS["INTR_ABSENCE_ADD_MORE"] = "Ajouter plus";
$MESS["INTR_ADD_TITLE"] = "Ajouter l'absence";
$MESS["INTR_ABSENCE_IBLOCK_MODULE"] = "Le module des blocs d'informations n'a pas été installé.";
$MESS["INTR_ABSENCE_ACTIVE_FROM"] = "Début:";
$MESS["INTR_USER_ERR_NO_RIGHT"] = "Droits d'accès à modifier sont insuffisants ";
$MESS["INTR_ABSENCE_ACTIVE_TO"] = "Achèvement:";
$MESS["INTR_ABSENCE_SUCCESS"] = "Une absence a été ajoutée";
$MESS["INTR_ABSENCE_PERIOD"] = "Période d'absence";
$MESS["INTR_ABSENCE_NAME"] = "Raison de l'absence *";
$MESS["INTR_ABSENCE_EDIT"] = "Editer";
$MESS["INTR_EDIT_TITLE"] = "Editer l'absence";
$MESS["INTR_ABSENCE_TYPE"] = "Type d'absence";
$MESS["INTR_ABSENCE_FROM_TO_ERR"] = "La date de début doit être antérieure à la date de fin.";
?>