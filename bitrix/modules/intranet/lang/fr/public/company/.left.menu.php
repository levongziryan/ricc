<?
$MESS["COMPANY_MENU_EMPLOYEES"] = "Rechercher un utilisateur";
$MESS["COMPANY_MENU_TELEPHONES"] = "Annuaire téléphonique";
$MESS["COMPANY_MENU_STRUCTURE"] = "Organigramme";
$MESS["COMPANY_MENU_EVENTS"] = "Changements  du personnel";
$MESS["COMPANY_MENU_ABSENCE"] = "Tableau des Absences";
$MESS["COMPANY_MENU_TIMEMAN"] = "Temps de travail";
$MESS["COMPANY_MENU_WORKREPORT"] = "Rapport d'activités";
$MESS["COMPANY_MENU_REPORT"] = "Efficacité";
$MESS["COMPANY_MENU_LEADERS"] = "Tableau d'honneur";
$MESS["COMPANY_MENU_BIRTHDAYS"] = "Anniversaires";
$MESS["COMPANY_MENU_GALLERY"] = "Galerie photos";
$MESS["COMPANY_MENU_MY_PROCESSES"] = "Mes Demandes";
?>