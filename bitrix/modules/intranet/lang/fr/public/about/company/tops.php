<?
$MESS["ABOUT_TOP1_INFO"] = "<p><h2>Bandurin Stanislav S.</h2></p>
<p><b>le Directeur général</b></p>
<p>Né le 28 Mars 1951 à la région de Vitebsk. En 1977, il est diplômé de l'Institut de Leningrad de génie civil, spécialité &laquo;construction industriels et civile&raquo;.Est titulaire de diplômes: Docteur en sciences techniques (1999), doctorat en architecture (1999), docteur en sciences économiques (2001), professeur de gestion à l'État de l'Architecture et de la Construction (2004). Possède un badge d'honneur 'leader de l'économie russe.'</p>
<p>Marié, il a un fils.
<br />
</p>";
$MESS["ABOUT_TOP2_INFO"] = "<p></p>
<h2>Jean Fabien</h2>
<p><b>Directeur général adjoint</b></p>
<p>Né le 8 janvier 1973 à Paris. En 1996 il a terminé ses études à l'académie d'état d'instrumentation aérospatiale de Paris dans la spécialité 'conception et technologie des appareils radioélectroniques'. En 1999 il obtient un diplôme du second degré en 'jurisprudence' à l'université du Ministère de l'intérieur russe de Saint-Pétersbourg. Il est titulaire d'un doctorat en sciences (2003).</p>
";
$MESS["ABOUT_TOP3_INFO"] = "<p></p>
<h2>Phillipe Fourcade</h2>
<p><b>Directeur financier</b></p>
<p>Né le 9 mai 1975 à Caen. A terminé la faculté de droit de l'Université d'Etat de Paris et, ensuite, le troisième cycle. Docteur en droit.</p>";
$MESS["ABOUT_TITLE"] = "Gestion";
$MESS["ABOUT_TOP1_IMG"] = "#SITE#images/en/company/about/management/bandurin.jpg";
$MESS["ABOUT_TOP2_IMG"] = "#SITE#images/en/company/about/management/pankrashev.jpg";
$MESS["ABOUT_TOP3_IMG"] = "#SITE#images/en/company/about/management/porshnev.jpg";
?>