<?
$MESS["ABOUT_INFO"] = "<p><b>Adresse du bureau central:</b></p>
bât. 23, 63 avenue Leninski, Moscou
<p><b>Plan d'accès au bureau:</b></p>

<p><img title='Comment nous trouver. Plan d'accès.' height='379' alt='Comment nous trouver. Plan d'accès.' src='#SITE#images/company/about/address.png' width='416' /></p>

<p></p>

<p><b>Horaires du bureau:</b></p>
Lundi-jeudi: 9.00 - 18.00
<br />
Vendredi: 9.00 - 17.00
<br />

<p><b>Téléphone:</b> </p>

<p> +7 (495) 366-33-55 - secrétariat</p>

<p><b>Fax:</b></p>

<p> +7 (495) 233-66-22
 <br />
+7 (495) 233-55-00</p>

<p><b>E-mail:</b></p>

<p>info@example.com</p>

<p><b>Téléphones supplémentaires</b>:</p>

<p> +7 (495) 233-66-11  Service du personnel
 <br />
+7 (495) 233-55-77  Service de gardiennage</p>

<p><b>Adresse postale</b>:</p>
119049 Moscou,
<br />
bât. 2, 63, avenue Leninski,
<br />
SARL &laquo; Buisness groupe industriel &raquo;
<br />";
$MESS["ABOUT_TITLE"] = "Contacts";
?>