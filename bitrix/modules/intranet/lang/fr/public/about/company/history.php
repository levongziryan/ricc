<?
$MESS["ABOUT_INFO"] = "<br />

<br />
 <b>1992-1997
 <br />

 <br />
 </b>L'entreprise existe depuis 1992 et jusqu'à 1996 c'était une entreprise du commerce en gros s'occupant des ventes de produits achetés à Moscou à Kaliningrad. Au début de 1996 une nouvelle loi &laquo;Sur la zone économique spéciale&raquo; entre en vigueur et l'entreprise devient l'un des plus grands importateurs des produits du groupe FMCG dans la région de Kaliningrad. Tout au long de cette période l'entreprise s'est développée comme une entreprise moderne utilisant des technologies avancées du management et de la gestion des affaires.
<br />

<br />
 <b>1998-1999</b>
<br />
 
<br />
 En 1998 un nouveau projet de la distribution directe des produits sur étagère chez les points de vente de débitant dans la région a été mis en oeuvre. Il était lancé avec la participation de consultants européens ayant l'expérience de la réalisation des projets similaires à l'étranger. On a acheté des minibus-transporteurs, équipements coûteux et outils informatiques (proposés par la entreprise canadienne 'Norand') - c'est-à-dire, tout ce qui est nécessaire pour les travailleurs mobiles dans le monde entier. Les quatre premiers DDR (Direct Delivery Representative - l'agent commercial) ont commencé leur travail sur l'itinéraire en mars 1999. En automne 1999 nous avions déjà huit itinéraires de la livraison, en moyenne trente visites par jour des points de vente par l'agent commercial et près de 900 points de vente à Kaliningrad.
<br />

<br />
 <b>1999-2000</b>
<br />

<br />
 Depuis la fin 1999 on n'utilise plus l'équipement et les outils informatiques étrangers en vertu de l'absence de la corrélation aux workflows russes et aux exigences de la législation russe. En 2000 sur la base du département d'informatique il s'est formé un groupe de travail spécial qui avait pour but d'automatiser le système existant de la distribution, et dans quelque temps ce groupe a continué son travail en tant qu'une entreprise indépendante - SARL &laquo;'Technologies systémiques'&raquo;.
<br />

<br />
 <b>2001</b>
<br />

<br />
 Après avoir mis en oeuvre de notre propre système automatisé au printemps 2001 la productivité de nos agents commerciaux a augmenté de 120 %! Toutefois, au lieu de réduire le nombre d'agents commerciaux, nous avons pris la décision d'entrer sur les marchés voisins et d'élargir l'assortiment des produits livrés. Le nombre de points de vente desservis par chaque agent a augmenté jusqu'à 45 par jour, et celui des points de ventes desservis par nous à Kaliningrad a augmenté jusqu'à 1050. À la suite des investissements importants et grâce au professionnalisme de notre équipe, nous avons mis en place un établissement qui fonctionne parfaitement bien. La technologie bien réglée a transformé le procèssus de la présentation de nouveaux produits sur le marché de Kaliningrad en quelque chose d'habituel et cela a stimulé une augmentation considérable des ventes et l'élargissement de l'assortiment. Nous avons obtenu la possibilité de mettre sur le marché n'importe quel produit de consommation en 2-3 jours après la réception dans notre entrepôt, en assurant sa disponibilité dans tous les magasins importants de la ville. Nous avons commencé à utiliser un nouvel outil unique pour influencer sur le marché. Dans ces nouvelles conditions les clients ont bien apprécié la coopération avec nous. Par conséquent, en 2001 notre assortiment s'est grandement accru. De nouvelles positions apparaissent dans ces groupes de produits: café, jus, thé, produits d'épicerie et de confiserie.
<br />

<br />
 <b>2002</b>
<br />

<br />
L'entreprisea connu un nouvel élan dans son développement à la fin de 2002, quand en complément au processus d'affaires existants de la distribution par la vente directe (Van-Selling) nous avons lancé la technologie automatisée du travail par précommande (Pre-Selling). L'entreprisea élargi ses moyens de coopération avec les clients et a réussi à répondre à leurs besoins et demandes. Cela nous a permis d'élargir l'assortiment, y compris avec des produits à la demande réduite.
<br />

<br />
 <b>2003</b>
<br />

<br />
 Après avoir mis au point des workflows dans le commerce mobile, la vente en gros et par la précommande, nous avons pu en 2003 consacrer plus de temps à l'élargissement de notre segment du marché. Nous faisons passer régulièrement la publicité à la radio régionale et la télévision. On travaille sur un bon étalage des produits, l'aménagement des points de vente, le contrôle de l'assortiment, un soutien réel du producteur et du détaillant avec le but d'améliorer la promotion de nos produits sur le marché. On organise des activités de promotion(sales promotion) avec la participation des producteurs et organisées uniquement par nos soins. Le principe essentiel de ces activités de promotion - c'est la planification des activités succédée par la promotion des produits dont la première étape c'est la garantie de la présence maximale de notre production dans les points de vente de la ville, et à la deuxième étape est le travail avec le consommateur final des produits.
<br />

<br />
 <b>2004</b>
<br />

<br />
En mai 2004 l'administration de la corporation internationale 'Intel' a choisi L'entreprise&laquo;Groupe Business Baltique&raquo; à titre d'exemple d'une entreprise mettant en oeuvre les technologies les plus avancées et modernes en Russie. Parmi de grand nombre des organisations russes utilisant dans leurs workflows des technologies numériques récentes la corporation 'Intel' n'a choisi que trois: une grande corporation internationale d'innovation Paradigm Geophysical, le gouvernement de la Tchouvachie avec le projet pilote &laquo;Gouvernement électronqiue&raquo; dans le cadre du programme cible &laquo;Russie électronique&raquo; et L'entreprisecommerciale de Kaliningrad &laquo;Groupe Business Baltique&raquo;.
<br />
 L'administration du &laquo;Groupe Business Baltique&raquo; a pris part au forum d'affaires consacré à l'avenir des technologies numériques.
<br />

<br />
 En décembre L'entrepriseest entrée avec succès sur le marché de la livraison d'alcool.
<br />

<br />
 <b>2005</b>
<br />

<br />
 En avril 2005 L'entreprisea connu une division de ses opérations de distribution: livraison par Van-selling et livraison par Pre-selling. Un itinéraire séparé pour desservir le ségment HoReCaDi a été mis en place. Une équipe à part a été formée pour élaborer de nouveaux itinéraires et élargir la clientèle &ndash; les hunters. La distribution des produits d'alcool a aussi connu un développement dynamique en tant qu'une branche importante d'activité de la compagnie. Au cours de cette année L'entrepriseavait une dynamique positive des ventes de produits de ce groupe.
<br />

<br />
 <b>2006</b>
<br />

<br />
 Depuis le 1-er juin 2006, on a changé le schéma du travail dans la ville ayant pour objet l'optimisation des processus commerciaux et le renforcement d'accent des agents commerciaux sur l'assortiment:
<br />

<br />
 Les produits du tabac ont formé une direction des ventes à part.
<br />

<br />
 4 agents commerciaux ont formé une équipe indépendante pour l'un des fournisseurs d'alcool et 10 agents commerciaux sont chargés de tout l'assortiment de L'entreprisedistribué par &laquo;Van Selling + Pre Selling&raquo;. Les perspectives du développement de L'entrepriseprévoient &ndash; la création des commandes spécialisées et orientées à l'assortiment concret et/ou aux producteurs concrets.
<br />

<br />
 En septembre 2006 un nouveau complexe moderne d'entrepôts avec la superficie totale de 2300 mètres carrés environ a été mis en exploitation.
<br />

<br />
 Notre compagnie d'aujourd'hui - c'est une entreprise qui se développe d'une façon dynamique et qui a sa propre place dans l'espace business d'élite dans la région. Nous livrons des produits directement vers plus de 2200 points de vente dans la région! Pour le stockage des produits nous utilisons nos locaux modernes dotés de systèmes de chauffage et de contrôle de l'humidité. Les technologies numériques de la logistique du stockage permettent de faire un suivi de l'historique des produits d'après leur durées de conservation, la disponibilité et le déplacement entre les lieux du stockage, le statut de qualité, les numéros des chargements et Déclarations en douane. L'utilisation des méthodes modernes assure le contrôle du déplacement des produits du vendeur au consommateur et permet d'analyser la distribution d'après tous les paramètres possibles ainsi que de garantir aux producteurs l'efficacité des opérations sur le marché.
<br />

<br />
 L'entreprisepossède l'expérience de travail avec ces groupes de produits: café, chocolat, jus, thé, confitures, préservatifs, briquets, produits de bonneterie, produits du tabac et d'alcool.
<br />
";
$MESS["ABOUT_TITLE"] = "Histoire & Développement";
?>