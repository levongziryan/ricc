<?
$MESS["ABOUT_INFO2"] = "<p><b>Concours</b></p>

<p>Notre entreprise organise des concours pour mettre en vedette le meilleur employé dans les diverses départements. Cette bonne tradition a été lancée il y a quelques années et depuis ce temps-là de nombreux employés sont devenus gagnants dans différents domaines.</p>

<p>Chaque mois on choisi le meilleur employé qui est nommé &laquo;Employé du mois&raquo;, et stimulé par une prime. Les noms des meilleurs employés sont publiés dans la 'Galerie d'honneur' sur notre portail corporatif.</p>

<p><b>Portail corporatif</b></p>

<p>Notre portail corporatif présente les nouvelles sur les activités de l'entreprise, les projets et les perspectives de son développement, les rapports sur l'activité des différents départements du Bureau central, les interviews avec les gestionnaires et le personnel, les articles actuels sur la formation et bien plus encore.</p>";
$MESS["ABOUT_INFO1"] = "<p><b>Equipe soudée</b></p>

<p>La priorité de notre entreprise &ndash; consiste en nos employés. La condition essentielle pour le travail réussi ce sont les relations saines dans l'équipe: elles sont basées sur la confiance, l'honnêteté et l'intégrité.</p>

<p><b>Fêtes corporatives</b></p>

<p>Les soirées corporatives, voyages à la campagne, anniversaires des employés fêtés ensemble par départements tout cela nous permet de connaître mieux l'un l'autre.</p>";
$MESS["ABOUT_TITLE"] = "Culture de entreprise";
?>