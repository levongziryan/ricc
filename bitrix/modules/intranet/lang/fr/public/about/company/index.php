<?
$MESS["ABOUT_TITLE"] = "La compagnie Aujourd'hui";
$MESS["ABOUT_INFO"] = "Avec la mise en place des magasins d'épicerie de supermarchés alimentaires mondiales dans le début des années 1990, nous avons jeté les bases d'une stratégie de marque cohérente de détail. Nos clients sont assurés d'avoir la meilleure sélection et les offres possibles à leur disposition.
<br />

<br />
 Les directeurs des magasins individuels décident de leurs besoins en matière de publicité locale, le choix des produits qu'ils offrent, la tarification et la planification du personnel. Cela leur permet de réagir avec souplesse aux besoins de leurs clients particuliers.
<br />

<br />
 Global Food bénéficie d'une croissance régulière et est devenu le premier détaillant en alimentation en Europe. Il ya actuellement plus de 1200 magasins dans 10 pays, ce qui a généré des ventes nettes de 27,1 milliards d'euros en 2007. Le groupe de sociétés a emploie actuellement plus de 65.000 personnes- plus de 1.500 d'entre eux sont employés dans le groupe & rsquo; s siège.
<br />

<br />
 Les clients peuvent toujours vous attendre à une gamme complète de produits dans les magasins alimentaires mondiales. Selon la taille et le type, les sorties offrent moins de 20 000 différents produits alimentaires. Global Food maintient un stock important de tous les produits de sa gamme, dans une variété d'options d'emballage, le tout à des prix compétitifs et avec une qualité sans compromis.
<br />

<br />
 <b>Les normes locales et magasin structuration spécifique</b>
<br />

<br />
 Global Food opère dans différents pays avec trois formats de magasins. La surface de vente d'un magasin le plus commun est de 10.000 à 16.000 mètres carrés. Ce format est principalement utilisé dans les pays d'Europe occidentale. En Europe de l'Est, les magasins moyennes de 7000 à 9000 sont généralement construits. Les petits magasins ont des zones de 2500 à 4000 mètres carrés de vente.
<br />

<br />
 <b>L'adaptation aux besoins locaux</b>
<br />

<br />
 Une caractéristique particulière de la notion de vente alimentaire mondiale est que la gamme de produits de chaque magasin est adaptée aux besoins du groupe cible local. Les produits sur nos tablettes et des unités de réfrigération sont soigneusement sélectionnés en fonction du comportement des consommateurs et les demandes de la région respective. Environ 90 pour cent de la marchandise sur nos magasins est acheté à des producteurs et des fournisseurs locaux dans le pays concerné.
<br />

<br />
 La société aide les producteurs et les fournisseurs à développer des méthodes de culture, de transformation et de distribution modernes. La politique d'achat local alimentaire mondiale soutient les économies & ndash régionales; par exemple, générer de nouvelles affaires pour les fabricants et les agriculteurs. En outre, les magasins répondent aux besoins spéciaux avec une large sélection de produits internationaux, tels que les aliments offerts turcs en Allemagne.
<br />

<br />

<h2>Faits sur le Global Food</h2>
 Les états financiers en millions d'euros
<br />

<br />
 <img width='217' height='193' src=''#SITE#images/company/about/facts.png' />
<br />

<h2>Engagement social</h2>
 Les sociétés et leurs emplacements font partie de la communauté. Pour réussir, ils comptent sur leur environnement. Cela comprend bien le fonctionnement des institutions et de l'infrastructure, un bon système d'éducation, et les possibilités culturelles et de loisirs attrayants pour l'entreprise & rsquo; s propres employés. En devenant impliqué dans la communauté, les entreprises peuvent contribuer à renforcer l'efficacité et l'attractivité de leur environnement. Dans le même temps, ils indiquent leur volonté de prendre la responsabilité de la société dans son ensemble.
<br />

<br />
 La proximité des magasins alimentaires mondiales à des clients, fournisseurs et employés a toujours caractérisé notre compréhension de la responsabilité sociale. Nous voulons que les gens dans les marchés dans lesquels nous exerçons nos activités à gagner un avantage tangible de nos activités. Tout comme nos activités visent à générer une croissance rentable, notre engagement social vise à créer des structures durables autour de nos sites de l'entreprise. Nous apportons notre compétence et l'expérience des projets et des initiatives communautaires que nous pouvons concilier significative et crédible avec notre cur de métier.
";
?>