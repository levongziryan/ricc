<?
$MESS["SERVICES_INFO"] = "Pour réserver une salle de conférences: trouvez les heures pendant lesquelles une salle est libre, contactez le manager chargé des réservations qui confirmera votre réservation et l'ajoutera aux horaires d'occupation des salles de conférences.";
$MESS["SERVICES_INIT_DATE"] = "-Montrer la Date courante-";
$MESS["SERVICES_TITLE"] = "Points de négociations";
$MESS["SERVICES_LINK"] = "Réservation de salles de conférences au moyen du tableau";
?>