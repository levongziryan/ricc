<?
$MESS["SERVICES_TITLE"] = "Moyen universel pour communication";
$MESS["SERVICES_INFO"] = "Le Portail dispose d'une salle de <b>XMPP/JABBER serveur</b>! Se connecter et de <b>être en ligne!</b> l'Échange de messages et de fichiers,
profitez de la voix et le chat vidéo - le tout avec votre mobile préféré ou de votre logiciel de messagerie!<br />
<br />

<br />

<table cellspacing='1' cellpadding='1' border='0' width='100%'>
<tbody>
<tr><td valign='top'><img hspace='10' height='15' width='12' src='#SITE#images/docs/cp/bullet-n.gif' /> message
de change (IM);
<br />
<img hspace='10' height='15' width='12' src='#SITE#images/docs/cp/bullet-n.gif' /> l'échange de fichiers;
<br />
<img hspace='10' height='15' width='12' src='#SITE#images/docs/cp/bullet-n.gif' /> toujours en ligne;
<br />
<img hspace='10' height='15' width='12' src='#SITE#images/docs/cp/bullet-n.gif' /> support pour les plus populaires
messenger applications;
<br />
<img hspace='10' height='15' width='12' src='#SITE#images/docs/cp/bullet-n.gif' /> intégration de la téléphonie ip, vidéo
chat;
<br />
<img hspace='10' height='15' width='12' src='#SITE#images/docs/cp/bullet-n.gif' /> Portail mobile;
<br />
<img hspace='10' height='15' width='12' src='#SITE#images/docs/cp/bullet-n.gif'/> pré-créé des listes de contacts;
<br />
<img hspace='10' height='15' width='12' src='#SITE#images/docs/cp/bullet-n.gif' /> message commun de l'histoire;
<br />
<img hspace='10' height='15' width='12' src='#SITE#images/docs/cp/bullet-n.gif' /> support de SSL.
<br />

<br />
</td><td>
<br />
</td><td valign='top'>
<br />
<img height='63' width='130' src='#SITE#images/docs/cp/m.jpg' />
<br /><br />
<a href='http://www.bitrixsoft.com/download/Miranda_IM.zip'>Télécharger Miranda!</a>
<br />

<br />
</td></tr>
</tbody>
</table>

<br />

<h2>Privé <b>Serveur XMPP</b></h2>
Le privé <b>serveur XMPP</b> a été créé afin de résoudre les problème essentiel de toute entreprise susceptible de rencontrer: la création de
<b>système de messagerie sécurisé</b>. Le serveur privé permet d'éviter l'utilisation de dangereux ICQ ou d'autres messages destinés au public
services tout en gardant l'outil que vous avez utilisé pour! Le Portal server s'exécute sur l' <b>Jabber</b> protocole
ce qui est pris en charge par la plupart des populaires applications de messagerie. Effectivement, cela signifie que vous pouvez communiquer
avec vos collègues à l'aide de vos applications préférées: <b>Miranda, Gaim, Psi</b> et d'autres.
<br />

<br />
En outre, vous pouvez envoyer et recevoir des messages texte, mais aussi <b>fichiers audio et vidéo</b> messages. Veux encore
plus? Pas de problème - <b>mobile</b> l'envoi et la réception de messages. Le Portail est maintenant universelle
communication point d'échange!
<br />

<br />

<h2>Comment faire?</h2>
Le <b>XMPP/JABBER serveur</b> est une application totalement fonctionnelle, testés et stables et un logiciel d'application.
<br />

<br />

<table cellspacing='0' cellpadding='0' border='0' width='100%'>
<tbody>
<tr> </tr>

<tr><td valign='top'>

<br />
</td><td>
<br />
</td><td valig='top'>outre les services de messagerie, ce serveur permet à votre Portail de gestion avec l'
indication des utilisateurs sur l'état de la ligne. Vos employés seront désormais toujours être en ligne, où qu'ils soient - la
gamme de plates-formes le serveur XMPP prend en charge varie de serveurs Unix pour les appareils mobiles. Il suffit d'installer un
Jabber activé messenger sur votre mobile et <b>être
sur la ligne!</b>
<br />

<br />
Pour la plupart des applications de messagerie (Miranda, Psi, etc.) <a name= \"contacts\" ></a> vous n'avez pas
pour créer des listes de contacts manuellement. Les listes sont générées automatiquement lors de l'installation.
La résultante de la liste de contacts contient la structure de l'entreprise, dont les utilisateurs sont regroupés par la société
les départements. Si votre système de messagerie prend en charge les groupes imbriqués, les groupes dans la liste de contacts messenger va
précisément miroir le ministère de la structure. </td><td>
<br />
</td></tr>
</tbody>
</table>

<br />

<table cellspacing='1' cellpadding='1' border='0' width='100%'>
<tbody>
<tr><td valign='top'>Selon le client de messagerie que vous utilisez, le
serveur toujours stocker l'historique de conversation! Ouvrir &quot;Mes Messages&quot; dans votre profil, et vous
va voir l'intégralité du message de journal.<br />
<h2>Qui Messenger Devrais-je Choisir?</h2>
Quel est le client de messagerie qui fonctionne le mieux? Tout qui prennent en charge le protocole XMPP. Nous avons testé plusieurs messagers et peut vous recommander d'essayer <a href='http://www.bitrixsoft.com/download/Miranda_IM.zip'>Miranda IM</a>. Vous trouverez le messager de configuration
instructions à l' <a href='http://www.bitrixsoft.com/support/training/course/lesson.php?COURSE_ID=27&ID=1641'>Portail Intranet Cours pour
Les débutants</a>.<i>

<br />
</i>
<h2><a name='Miranda'></a>Miranda s'il vous Plaît!</h2>

<p class= \" a \" >Les seuls paramètres que vous devez savoir pour configurer Miranda sont votre login et votre mot de passe vous
utilisez pour ouvrir une session dans le Portail et le portail de nom de domaine et le numéro de port. Consultez votre système
administrateur à propos de ces options, et demandez-lui si votre serveur utilise le protocole SSL. Entrez les paramètres dans la Miranda
boîte de dialogue options. Cliquez sur la capture d'écran ci-dessous pour afficher un exemple.</p>
</td><td>
<br />
</td><td valign='top'>
<a href='javascript:ShowImg('#SITE#images/docs/cp/im2.png', 266, 405, la Société \"liste de contacts\")'>
<img src='#SITE#images/docs/cp/im2-s.png' width='100' height='152' border='0' alt='Société de la liste de contacts' title='Société de la liste de contacts' />
</a>
<br />

<br />
</td></tr>
</tbody>
</table>

<br />

<table cellspacing='0' cellpadding='0' border='0' width='100%' style='border-collapse: l'effondrement;'>
<tbody>


<tr><td valign='top'>


<a href='javascript:ShowImg('#SITE#images/docs/cp/cp8_miranda.png', 648, 472, Miranda paramètres')'>
<img src='#SITE#images/docs/cp/cp8_miranda-s.png' border='0' width='100' height='73' alt='Miranda paramètres' title='Miranda' paramètres />
</a>
<br />
</td><td valign='top'> <span class='texte'>
<ul>
<li>entrez votre portail Intranet login et mot de passe dans le <b>Utilisateur </b>et <b>Mot de passe</b>fields;</li>

<li>type de domaine de votre serveur dans le <b>Serveur</b>field;</li>

<li>demander votre portail Intranet de l'administrateur pour vous conseiller sur le serveur XMPP port. Tapez le numéro de port
nombre dans le champ correspondant;</li>

<li>demandez à votre administrateur si le protocole SSL est utilisé par le serveur. Si oui, consultez le <b>SSL</b>box.</li>

<li>Laissez les autres champs tels qu'ils sont. Cliquez sur <b>OK </b>pour enregistrer les modifications.</li>


</ul>
</span></td></tr>
</tbody>
</table>


<p>Si vous êtes habitué à une autre application de messagerie, vous n'avez pas à passer à Miranda. Trouver configuration similaire
options dans votre messagerie les paramètres de la fenêtre et de fournir les mêmes données comme vous le feriez avec Miranda.
Rendez-vous au Portail! </p>";
?>