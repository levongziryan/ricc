<?
$MESS["SERVICES_INFO2"] = "<br />
 </td></tr>
 </tbody>
 </table>

<h2>En quoi consiste l'avantage (bénéfice) de la synchronisation bilatérale?</h2>

<p>Cette synchronisation permet de maintenir en état actuel des données sur le Portail et  dans MS Outlook en même temps, par exemple, vos contacts personnels. Essayez de le faire vous-même maintenant et vous allez appréciez cette amélioration de votre travail. Pourquoi? Par exemple, à bord d'un avion vous avez prévu et planifié beaucoup de rencontres, tâches et activités différentes pour la compagnie. Alors, utilisez la possibilité de la <b>synchronisation bilatérale des calendriers avec MS Outlook</b> - et tous vos projets seront affichés dans un calendrier sur le Portail! Il n'y aura <b>aucun doublage manuel de l'information</b>, et vos moyens de travail restent habituels. Rapide, commode et très efficace!
<a name='useful'></a>
</p>

<h2>Commencez par la synchronisation des utilisateurs</h2>

<p>Pour assigner des tâches à quelqu'un (et recevoir des tâches envoyées par quelqu'un) il faut ajouter ce contact dans votre MS Outlook. C'est très facile à faire: sur la page <a href='/company/index.php' target='_blank' />Recherche de l'employé</a> du votre Portail cliquez sur le lien <b>Synchroniser avec Outlook</b> (pour le modèle 'Bitrix24': cliquer sur le bouton <b>Plus</b>, sélectionner dans un menu déroulant <b>Synchroniser avec Outlook</b>) et tout va se passer automatiquement. Vous aurez juste à valider deux fenêtres apparues. Il reste de patienter jusqu'à ce que les données sur tous les employés de la compagnie aient chargées...</p>

<p>Après cela une liste de vos employés apparaîtra dans le calendrier:</p>

<div align='center'>

 <table cellspacing='1' cellpadding='1' border='0' style='border-collapse: collapse;'>
 <tbody>
 <tr><td>
 <div align='center'><a href='javascript:ShowImg('/images/docs/main.png', 1039, 705, 'Employés ajoutés')' ><img width='500' height='339' border='0' src='/images/docs/main_sm.png' alt='Employés ajoutés' title='Employés ajoutés' /> </a>
 <br />
 </div>

 <div align='center'><i>Employés ajoutés</i>
 <br />
 </div>
 </td></tr>
 </tbody>
 </table>
 </div>

<p>La suppression des utilisateurs dans MS Outlook ne vas pas amener à leur suppression sur le Portail: uniquement l'Administrateur du Portail a le droit de le faire.</p>


<a name='my_kalendar'></a>
<h2>Synchronisation des calendriers avec MS Outlook</h2>
<p>Vous pouvez mettre en synchronisation avec les calendriers MS Outlook <b>tous les calendriers</b> sur le Portail: vos calendriers personnels, les calendriers de vos employés ou les calendriers communs de la compagnie. Essayer de le faire tout de suite! Accédez à la page avec un calendrier, choisissez dans un menu d'actions <b>&lt;Joindre à Outlook&gt;</b> et lancez la synchronisation!
</p>

<p><div align='center'><img width='302' height='244' border='0' src='/images/docs/go_to.png' />
 <br />
 <i>Joindre à Outlook!</i></div></p>

<p>Ne faites pas attention aux messages MS Outlook et validez toujours, puisque ces messages apparaissent juste à titre d'information. Par exemple, vous allez voir la question: <b>&lt;Joindre à Outlook le dossier &quot;SharePoint&quot; le calendrier?&gt;</b> - alors cliquez sans hésitation sur <b>&lt;Oui&gt;</b>. Pourquoi? Parce que l'intégration à été effectuée en toute conformité avec la spécification de la compagnie Microsoft et il n'y aura aucun problème! </p>

<p><div align='center'><img width='482' height='202' src='/images/docs/step1.png' /> </div></p>

<p>En principe, vous pouvez ne pas vous dépêcher et cliquer sur <b>&lt;Supplément...&gt;</b>, pour créer la description du calendrier. </p>

<div align='center'>

 <table cellspacing='1' cellpadding='1' border='0' style='border-collapse: collapse;'>
 <tbody>
 <tr><td>
 <div align='center'><a href='javascript:ShowImg('/images/docs/step2.png', 518, 448, 'Description du calendrier')' ><img width='299' height='259' border='0' src='/images/docs/step2_sm.png' alt='Description du calendrier' title='Description du calendrier' /> </a>
 <br />
 </div>

 <div align='center'><i>Description du calendrier</i>
 <br />
 </div>
 </td></tr>
 </tbody>
 </table>
 </div>



<a name='company_calendar'></a>
<p>Quel est le résultat? Dans votre compte utilisateur Outlook vous allez voir un nouveau <b>calendrier déjà rempli</b> avec tous les événements affichés! Pourquoi c'est commode? Par exemple, après votre absence prolongée en mission d'affaires on a prévu beaucoup d'activités différentes dans la compagnie. Pour rester en contact activez et synchronisez les calendriers nécessaires avec votre compte Outlook - vous aurez l'accès à tous les événements. </p>

<div align='center'>

 <table cellspacing='1' cellpadding='1' border='0' style='border-collapse: collapse;'>
 <tbody>
 <tr><td>
 <div align='center'><a href='javascript:ShowImg('/images/docs/calendar1.png', 550, 434, 'Calendrier ajouté à Outlook!')' ><img width='300' height='237' border='0' src='/images/docs/calendar1_sm.png' alt='Calendrier ajouté à Outlook!' title='Calendrier ajouté à Outlook!' /> </a>
 <br />
 </div>

 <div align='center'><i>Calendrier ajouté à Outlook!</i>
 <br />
 </div>
 </td></tr>
 </tbody>
 </table>
 </div>



<a name='kalendars'></a>
<p>Maintenant <b>ajoutez</b> de la même manière l'un par l'autre <b>tous les calendriers dont vous avez besoin</b> sur le Portail. Affichez ces calendriers dans une seule grille et le calendrier dans votre Outlook sera absolument identique au calendrier du Portail! </p>


<div align='center'>
 <table cellspacing='1' cellpadding='1' border='0' style='border-collapse: collapse;'>
 <tbody>
 <tr><td>
 <div align='center'><a href='javascript:ShowImg('/images/docs/calendar2.png', 550, 434, 'Calendriers exportés à MS Outlook')' ><img width='300' height='237' border='0' src='/images/docs/calendar2_sm.png' alt='Calendriers exportés à MS Outlook' title='Calendriers exportés à MS Outlook' /> </a>
 <br />
 </div>

 <div align='center'><i>Calendriers MS Outlook dans une seule grille</i>
 <br />
 </div>
 </td></tr>
 </tbody>
 </table>
 </div>



<h2>Comment tout cela fonctionne? Ajoutons un événement!</h2>
<p>Comment fonctionne la synchronisation bilatérale? <b>Ajoutez un nouvel événement dans le calendrier MS Outlook et cet événement va apparaître automatiquement dans le calendrier sur le Portail!</b> Faites-le maintenant:</p>

<ul>
 <li>dans la grille du calendrier choisissez le jour pour un nouvel événement;
 <br />
 </li>

 <li>par un double-clic ouvrez la fenêtre d'ajout du nouvel événement; </li>

 <li>remplissez les champs Thème, Début, Fin et Description; </li>

 <li>cliquez sur Sauvegarder et fermer; </li>

 <li>le nouvel événement <b>sera ajouté dans un calendrier </b>MS Outlook. </li>
 </ul>

<div align='center'>
 <table cellspacing='1' cellpadding='1' border='0' style='border-collapse: collapse;'>
 <tbody>
 <tr><td>
 <div align='center'><a href='javascript:ShowImg('/images/docs/event.png', 600, 378, 'Calendriers exportés à MS Outlook')' ><img width='300' height='189' border='0' src='/images/docs/event_sm.png' alt='Calendriers exportés à MS Outlook' title='Calendriers exportés à MS Outlook' /> </a>
 <br />
 </div>

 <div align='center'><i>Calendriers MS Outlook dans une seule grille</i>
 <br />
 </div>
 </td></tr>
 </tbody>
 </table>
 </div>

<p class='a1'>
<a name='sinhr'></a>
Vous n'avez rien à faire pour la synchronisation avec le Portail - cette opération est automatique et elle est en conformité avec la fréquence de vérification de la messagerie par Outlook. <b>Le progrès de la synchronisation</b> sera affiché dans un coin droit en bas de l'écran.</p>

<p align='center' class='a1'><img width='370' height='26' src='/images/docs/status.png' /></p>
 Et voici le même événement <b>dans un calendrier sur le Portail</b>!
<br />

<p><div align='center'><img width='477' height='294' src='/images/docs/event_portal.png' /></div></p>

<p>De la même manière vous pourrez <b>modifier et supprimer des événements</b> - dans les calendriers MS Outlook et dans les calendriers sur le Portail. Tous les changements sont soumise à la <b>synchronisation bilatérale</b>! Maintenant vous savez ce que cela signifie: la synchronisation automatique va mener au changement ou à la suppression de cet événement à MS Outlook ou sur le Portail. </p>

<a name='useful1'></a>

<h2>Synchronisation des tâches personnelles</h2>
<p>Maintenant, sans entrer en détails, nous allons faire la synchronisation des tâches. On vous rappelle que cette synchronisation va aussi se passer sans votre participation.</p>


<div align='center'>
 <div align='center'><a href='javascript:ShowImg('/images/docs/tasks.png', 700, 442, 'Synchronisation des tâches personnelles')' ><img width='400' height='252' border='0' src='/images/docs/tasks_sm.png' alt='Synchronisation des tâches personnelles' title='Synchronisation des tâches personnelles' /> </a>
 <br />
 </div>

 <div align='center'><i>Synchronisation des tâches personnelles</i>
 <br />
 </div>
 </div>

<p><b>Suivez la consigne:</b> </p>

<ul>
 <li>faites la synchronisation des utilisateurs (bouton Outlook sur la page Recherche des utilisateurs); </li>

 <li>entrez le mot de passe demandé le mot de passe par AD (mot de passe pour le Portail); </li>

 <li>la liste des utilisateurs va être intégrée automatiquement au MS Outlook; </li>

 <li>envoyez une tâche à l'un des utilisateurs à l'aide des instruments MS Outlook; </li>

 <li>MS Outlook va s'adresser au Portail et va synchroniser des tâches. </li>
 </ul>";
$MESS["SERVICES_TITLE"] = "Intégration bilatérale avec Microsoft Outlook";
$MESS["SERVICES_INFO1"] = "Ce n'est pas une simple intégration qui est réalisée sur le Portail, mais <b>;l'intégration bilatérale avec Microsoft Outlook</b>. Alors, vous pouvez non seulement importer les données du Portail dans un logiciel postal populaire. Le Portail et MS Outlook seront syncronisés et tous les changements dans les calendriers personnels, contacts des utilisateurs et vos tâches dans un logicel vont automatiquement être mise à jour dans un autre logiciel!
<br />

<br />

<table width='100%' cellspacing='1' cellpadding='1' border='0'>
 <tbody>
 <tr><td valign='top'><img width='12' hspace='10' height='15' src='/images/docs/cp/bullet-n.gif' /><a href='#my_kalendar' >synchronisation de calendriers personnels</a>;
 <br />
 <img width='12' hspace='10' height='15' src='/images/docs/cp/bullet-n.gif' /><a href='#company_calendar' >synchronisation des calendriers de l'entreprise; </a>
 <br />
 <img width='12' hspace='10' height='15' src='/images/docs/cp/bullet-n.gif' /><a href='#useful' >synchronisation de contacts personnels</a>;
 <br />
 <img width='12' hspace='10' height='15' src='/images/docs/cp/bullet-n.gif' /><a href='#kalendars' >exporation de plusieurs calendriers; </a>
 <br />
 <img width='12' hspace='10' height='15' src='/images/docs/cp/bullet-n.gif' /><a href='#kalendars' >affichage de calendriers dans une seule grille dans MS Outlook.</a>
 <br />
 <img width='12' hspace='10' height='15' src='/images/docs/cp/bullet-n.gif' /><a href='#useful1' >synchronisation de tâches personnelles</a>;
 <br />
 </td><td>
 <br />
 </td><td valign='top'> <b>Joins à Outlook dès maintenant!</b>
";
$MESS["SERVICES_LINK2"] = "Connectez Mes Tâches";
$MESS["SERVICES_LINK1"] = "Connectez Contacts";
$MESS["SERVICES_CONNECT"] = "Connecter";
?>