<?
$MESS["SERVICES_INFO"] = "<img width='173' vspace='5' hspace='5' height='170' align='right' src='#SITE#images/new_sm.jpg' />Il vaut mieux prendre connaissance de l'entreprise par<a href='#SITE#services/learning/'>Un cours spécial pour de nouveaux utilisateurs</a>.
<br />

<p>Après cela il est recommandé de faire attention aux pages suivantes de notre portail:</p>

<p><a href='#SITE#about/company/'>Notre entreprise</a> - informations officielles et non officielles sur notre entreprise. Vous allez faire connaissance de l'histoire de sa création et de ses branches d'activité. Dans cette section il y a l'information utile: coordonnées de l'entreprise, adresses et numéros de téléphone de différents bureaux et subdivisions. On recommande d'utiliser ces informations au cours de contacts avec les clients, partenaires et contractants de notre entreprise.</p>

<p><a href='#SITE#company/structure.php'>Organigramme de l'entreprise </a>- une présentation claire de la hiérarchie des départements, bureaux et subdivisions. Si vous avez besoin de retrouver rapidement un département qui est responsable d'une certaine fonction, alors c'est ici. Sur cette page vous pouvez aussi retrouver un utilisateur chargé de certaines fonctions dans la cadre du département.
 <br />
 </p>

<p><a href='#SITE#company/index.php'>Recherche de l'utilisateur</a> - permet de retrouver rapidement un utilisateur selon n'importe quel paramètre, de savoir si cet utilisateur se trouve à sa place de travail à ce moment ? d'obtenir l'information de son contact. Il est possible d'utiliser une recherche simple et une recherche avancée. Chaque utilisateur a son profil où il peut publier l'information personnelles supplémentaires: photos, documents. Sur cette page il est possible d'envoyer un message pour cet utilisateur à son forum personnel, lire ses opinions dans son blog personnel etc.
 <br />
 </p>

<p><a href='#SITE#about/index.php'>Informations officielles</a> - Cette section donne la possibilité aux utilisateurs d'être au courant d'événements actuels liés à l'activité de l'entreprise, lire des ordres et des dispositions, recevoir des notes de service de la part des chefs de l'entreprise ou responsables de départements.</p>
 <a href='#SITE#about/life.php'>Notre vie</a> - la vie d'aujourd'hui dans notre entreprise: événements différents et anniversaires, jubilés et fêtes, sortie en commun et enseignement collaboratif. Alors, c'est tout ce qui nous unit au sein de notre collaboratif et nous rend plus proches.
<br />

<p><a href='#SITE#about/calendar.php'>Calendrier des événements</a> - C'est un instrument commode qui permet de planifier et afficher des événements courants de l'entreprise ainsi que voir des événements à venir pour chaque mois.</p>

<p><a href='#SITE#about/calendar.php'>Anniversaires</a> - Cette page sert à rappeler aux utilisateurs les dates des anniversaires de leurs collègues et amis.</p>

<p><a href='#SITE#docs/'>Bibliothèque de documents</a> - C'est une base des documents de l'entreprise qui sont disponibles pour l'affichage et le travail collaboratif, il est possible d'organiser des sections fermées de la bibliothèque pour le travail collaboratif des utilisateurs de différents départements.</p>

<p>Avant de commencer votre travail sur le Portail, il faut suivre un cours spécial de formation: <a href='#SITE#services/learning/' target='_blank'>Cours sur le travail avec le Portail</a>. Il sert à expliquer les possibilités d'utilisation du Portail pour le travail de tous les jours et l'organisation du travail collaboratif.</p>

<p>Bon courage avec votre travail!
 <br />
 </p>
 </p>
";
$MESS["SERVICES_TITLE"] = "Information pour les nouveaux collaborateurs";
?>