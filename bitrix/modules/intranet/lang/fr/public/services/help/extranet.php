<?
$MESS["SERVICES_TITLE"] = "Extranet";
$MESS["SERVICES_INFO"] = "<h3>Unification de deux espaces d'information </h3>

<p>Le module<b>&laquo;Extranet&raquo;</b> &ndash; c'est lexpansion du portail de la entreprise qui permet à la entreprise d'avoir les contacts confidentiels avec des fournisseurs, des distributeurs et d'autres utilisateurs externes sans accès aux informations internes </p>

<p><span style='font-weight: bold;'>Extranet</span> - c'est <b>une espace d'information croisée</b> <b>sécurisée</b> pour la coopération avec l'&laquo;le monde&raquo; extérieur. Cette espace Web commune est spécialement créée pour la coopération efficace avec d'autres entreprises. Invitez aux groupes Extranet les collègues de entreprises-amies: fournisseurs, distributeurs, partenaires - et vous pourrez résoudre avec eux les tâches communes. La communication avec &laquo;les utilisateurs&raquo;externes reste néanmoins confidentielle et Intranet reste sécurisé. </p>

<div align='center'> <img width='302' height='218' src='#SITE#images/docs/cp/extranet_main-s300.png' complete='complete' />
  <br />
 <i>Page principale Extranet</i>   </div>

<h3>Accès et sécurité</h3>

<p>Pour l'utilisateur standart du portail de la entreprise le travail à l'Extranet ne se différe pas du travail sur le portail.</p>

<p> Les utilisateurs invités ne faisant pas partie de la entreprise, reçoivent des <span style='font-weight: bold;'> droits d'accès limités </span>, mais, <b> seulement à l'Extranet </b> - sans possibilité de consulter l'information interne sur le portail. En même temps, les collaborateurs de votre entreprise pourront aussi se connecter suivant une invitation des groupes de travail Extranet.  Il peuvent discuter les problèmes avec les utilisateurs invités en groupes (forums, blogs), travailler ensemble avec les documents partagés, programmer l'activité dans le calendrier, mettre les tâches et suivre leur réalisation, publier des rapports et ainsi de suite.
 <br />
 </p>

<h3>Travail en équipe</h3>

<p>Quels sont <span style='font-weight: bold;'>les avantages d'Extranet</span>, comme le territoire neutre, la piste commune et l'espace pour les réunions? C'est une ressource qui est basée sur les mêmes principes que l'Intranet. C'est pourquoi, vous ne devez pas réapprendre au personnel de gérer le nouveau projet et travailler avec les utilisateurs externes. Le travail en équipe Extranet est tout à fait analogue au travail de groupes de travail sur le portail de la entreprise - dans le même espace confortable et familier. La seule différence c'est une &ndash; politique plus stricte. </p>

<h3>Comment installer?</h3>

<p>Lors de l'installation du produit <i> &laquo; Bitrix Intranet Portal &raquo; </i>s' installe automatiquement le module <b> Extranet </b>. L'administrateur doit seulement passer <b> à l'Assistant de configuration Extranet </b>. Si pour une raison quelconque (par exemple, la transition d'éditeur adjoint à l'éditeur en chef) le module n'a pas été installé, pour commencer à travailler il est nécessaire <a href='/bitrix/admin/module_admin.php'>d'installer le module Extranet</a>. </p>

<div align='center'><img width='382' height='210' src='/images/docs/cp/extranet_setup.png' complete='complete' /> 
 <br />
 <i>Installation du module Extranet</i> </div>

<p>Après l'installation du module il vous fera proposé <a href='/bitrix/admin/wizard_install.php?lang=fr&amp;wizardName=bitrix:extranet&amp;<?=bitrix_sessid_get()?>' >de faire le démarrage d'Assistant du site Extranet</a>, au cours duquel vous serez invité à configurer le site Extranet.</p>

<p>Lorsque l'Assistant termine son travail, vous pouvez commencer à travailler!</p>

<table cellspacing='0' cellpadding='0' border='0' width='100%' style='border-collapse: collapse;'>
 <tbody>
 <tr><td valign='top'>
 <br />
 </td><td valign='top'>         <span class='text'><b>En premier lieu il vous faut:</b>

 <ul>
 <li>utiliser <b> outils spéciaux Extranet </b> pour simplifier le travail avec Extranet; </li>

 <li>publier à Extranet <b>l'information accessible a tous</b> (remplir la section &laquo;entreprise&raquo; et guides); </li>

 <li><span class='text'><b>inviter les collegues</b> d'autres entreprises pour le travail commun sur le terrain commun;</span> </li>
 </ul>
 </span></td></tr>
 </tbody>
 </table>";
?>