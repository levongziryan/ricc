<?
$MESS["SERVICES_TITLE"] = "Planifier les événements en groupe";
$MESS["SERVICES_INFO"] = "Voulez-vous organiser une réunion? Soucieux, si le temps arrangé convient à tous vos collègues? Pas de problème pour réunir tous ensemble! Si le calendrier de l'employé n'a pas d'événements avec le statut <b>Je suis absent</b> à cette date, alors l'invitation à la réunion est possible! Pour faire cela:
<ul>
 <li>créer un événement dans le calendrier à l'aide de l'ordre <b>planificateur d'événements</b>:

<p><img height='248' border='0' width='700' src='/images/docs/event_plan1.png' /></p>

<p>Formulaire d'ajout de l'événement s'ouvrira:</p>

<p><img height='518' border='0' width='700' src='/images/docs/event_plan.png' /></p>

</li>

<li>Utilisez la référence <b>Ajouter les participants</b> pour choisir les employés nécessaires.</li>
 </ul>
Le portail vérifiera lui-même si les employés choisis sont libres à ce moment-là et, le cas échéant, vous informera de leur occupation: le temps quand l'employé est absent, sera affiché en orange. Le planificateur d'événements vous permet de choisir le lieu pour la rencontre et de trouver visuellement le temps convenable à tous les employés pour l'événement collectif.

<br />
 
Et comment spécifier que vous êtes occupé? C'est très facile, créez l'événement dans votre calendrier personnel! Apès quoi l'événement apparaîtra non seulement dans votre calendrier, mais aussi sur <b>Horaire général d'absences!</b> Lors de l'édition de l'événement déjà créé il est possible de marquer son absence - le résultat restera le même.

<p><b>Faites-le maintenant:</b></p>

<ul>
 <li>passez au calendrier personnel sur votre page personnelle;</li>

 <li>créez le nouveau événement sur le signet <b>Evènement</b> dans le champ <b>Occupation de l'utilisateur</b> choisissez<b>Absent (Introduire dans l'horaire d'absences)</b>:

<p><img height='387' border='0' width='532' src='#SITE#images/docs/new_personal_absence.png' /></p>
</li>

 <li>sauvegardez les modifications introduites.</li>
 </ul>


<p>En conséquence, sur la page<b>Horaire d'absences</b> dans la section <b>Mois</b> la note sur votre absence sera ajoutée.</p>

<p><img height='294' border='0' width='853' src='#SITE#images/docs/graph.png' /></p>

<p> Vous planifiez un voyage, vous allez au théâtre ou au concert ou tout simplement à la clinique ou dans le département du logement - <b>affichez cela dans le calendrier</b>! Vous serez plus calme et il sera plus facile pour votre compagnie de <b>planifier les événements collectifs </b>, contrôler l'emploi et l'absence des employés dans le bureau.</p>
<p>Les dates de vos vacances seront marquées dans le calendrier général par les représentats du service d personnel, quand votre demande est signée par le chef et transmise .</p>
";
?>