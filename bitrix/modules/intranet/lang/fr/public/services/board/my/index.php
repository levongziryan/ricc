<?
$MESS["SERVICES_MESSAGE_ADD"] = "Votre annonce a été ajoutée";
$MESS["SERVICES_MESSAGE_EDIT"] = "Votre annonce est sauvegardée";
$MESS["SERVICES_TITLE"] = "Titre";
$MESS["SERVICES_CATEGORY"] = "Catégorie";
$MESS["SERVICES_DATE_ACTIVE_TO"] = "Délai de la publication jusqu'à";
$MESS["SERVICES_TEXT"] = "Texte d'annonce";
?>