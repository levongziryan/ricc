<?
$MESS["SERVICES_MENU_TELEPHONY_BALANCE"] = "Balance et statistiques";
$MESS["SERVICES_MENU_TELEPHONY_NUMBER"] = "Configuration des numéros de téléphone";
$MESS["SERVICES_MENU_TELEPHONY_LINES"] = "Numéros de téléphone";
$MESS["SERVICES_MENU_TELEPHONY_USERS"] = "Les utilisateurs de téléphonie";
$MESS["SERVICES_MENU_TELEPHONY_PHONES"] = "Téléphones SIP";
$MESS["SERVICES_MENU_TELEPHONY"] = "Autres paramètres";
$MESS["SERVICES_MENU_TELEPHONY_PERMISSIONS"] = "Permissions d'accès";
$MESS["SERVICES_MENU_TELEPHONY_CONNECT"] = "Connexion";
$MESS["SERVICES_MENU_TELEPHONY_GROUPS"] = "Files d'attente";
$MESS["SERVICES_MENU_TELEPHONY_IVR"] = "Configurer le SVI";
?>