<?
$MESS["CRM_MENU_STATUS"] = "Indicatrices";
$MESS["CRM_MENU_CURRENCY"] = "Devises";
$MESS["CRM_MENU_TAX"] = "Impôts";
$MESS["CRM_MENU_LOCATIONS"] = "Emplacements";
$MESS["CRM_MENU_PS"] = "Moyens du paiement";
$MESS["CRM_MENU_PERMS"] = "Droits d'accès";
$MESS["CRM_MENU_BP"] = "Processus d'affaires";
$MESS["CRM_MENU_FIELDS"] = "Champs d'utilisateur";
$MESS["CRM_MENU_PRODUCT_PROPS"] = "Caractéristiques de marchandises";
$MESS["CRM_MENU_CONFIG"] = "Autres paramètres";
$MESS["CRM_MENU_SENDSAVE"] = "Intégration avec la poste";
$MESS["CRM_MENU_SALE"] = "Boutiques en ligne";
$MESS["CRM_MENU_MEASURE"] = "Liste des unités de mesure";
$MESS["CRM_MENU_MAILTEMPLATE"] = "Liste des modèles postaux";
$MESS["CRM_MENU_INFO"] = "Aide";
$MESS["CRM_MENU_PRODUCTPROPS"] = "Caractéristiques de marchandises";
$MESS["CRM_MENU_PRESET"] = "Modèles de détails de client";
$MESS["CRM_MENU_EXCH1C"] = "Intégration &quot;1C:Enterprise&quot;";
$MESS["CRM_MENU_MYCOMPANY"] = "Détails de ma société";
?>