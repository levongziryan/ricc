<?
$MESS["CRM_TITLE"] = "Aide";
$MESS["CRM_INFO"] = "<h3>Qu'est ce que c'est une CRM/h3>
<p><b>Système de gestion de la relation client</b> (ou <b>CRM</b>) &mdash; est un système destiné à l'augmentation du niveau des ventes, l'optimisation du marketing et l'amélioration du service client. Dans la CRM l'information sur les clients et l'historique des relations avec eux sont stockés pour l'analyse ultérieure des résultats.</p>
<p>A l'aide de la CRM le manager et le prospect font le voyage d'un Prospect à une Transaction réalisée. </p>
<p>Définissons d'abord quelques notions:</p>
<ul>
<li><b>Contact</b>: Données sur la personne avec qui le dialogue sur l'activité profesionnelle a eu lieu, en d'autres termes c'est unecarte de visite. </li>
<li><b>Entreprise</b>: Données sur l'entreprise avec lesquelles votre Entreprise a (ou peut avoir) quelque forme de coopération. </li>
<li><b>Prospect</b>: Données sur une forme de contact (téléphone, adresse email, rendez-vous d'affaire etc.), qui a le potentiel de se développer en une transaction. </li>
<li><b>Evènement</b>: tout changement aux données Contact, Prospect, Entreprise, par exemple: l'ajout d'une nouvelle adresse email. </li>
<li><b>Transaction</b>: Données sur le travail avec le Client, l'Entreprise, à propos de la vente du produit. </li>
</ul>
<h3>Comment fonctionne -t-elle?</h3>
<p><b>La CRM</b> peut être utilisé en deux façons:</p>
<ol>
<li>comme <b>base de données</b> de Contacts et Entreprises,</li>

<li>comme la <b>CRM</b></li> classique 
</ol>
<h4>1. CRM comme base de données</h4>
<p>L'utilisation du système CRM comme base de données de contacts et Entreprises permet de mener l'historique des relations avec eux. L'objectif principal de base dans ce cas sont le Contact et l'Entreprise dans lesquels on gère l'historique des relations à l'aide des Évènements. L'utilisation de la CRM comme base de données n'exclut pas de création, à la suite de quelques Évènements, d'autres Prospects et leur conversion en Transactions.</p>
<p><img height='430' border='0' width='900' src='/upload/crm/cim/01.png' /></p>
<h4>2. CRM classique</h4>
<p>. L'objectif principal de la CRM est le Prospect, qui est ajouté au système à la main par le manager et automatiquement à partir de &quot;Bitrix: Gestion du site&quot; ou par d'autres moyens. Après l'ajout et le traitement le Prospect peut être converti en un Contact, une entreprise ou une Transaction. Dans les deux premiers cas le Prospect devient un élément ordinaire de la base de données. Dans le deuxième cas, après avoir passé par l'Entonnoir des ventes il devient une vente réelle.</p>
<p><img height='430' border='0' width='900' src='/upload/CRM/cim/03.png' /></p>
";
?>