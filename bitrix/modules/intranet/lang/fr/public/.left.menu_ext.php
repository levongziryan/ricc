<?
$MESS["LEFT_MENU_LIVE_FEED"] = "Flux d'activités";
$MESS["LEFT_MENU_IM_MESSENGER"] = "Chat et appels";
$MESS["LEFT_MENU_TASKS"] = "Tâches";
$MESS["LEFT_MENU_CALENDAR"] = "Calendrier";
$MESS["LEFT_MENU_DISC"] = "Mon Drive";
$MESS["LEFT_MENU_PHOTO"] = "Mes Photos";
$MESS["LEFT_MENU_BLOG"] = "Conversations";
$MESS["LEFT_MENU_MAIL"] = "Courriel";
$MESS["LEFT_MENU_MAIL_SETTING"] = "Modifier les paramètres";
$MESS["LEFT_MENU_BP"] = "Flux de documents";
$MESS["LEFT_MENU_CRM"] = "CRM";
$MESS["LEFT_MENU_MY_PROCESS"] = "Mes Demandes";
$MESS["TOP_MENU_GROUPS"] = "Groupes";
$MESS["TOP_MENU_GROUPS_EXTRANET"] = "Groupes extranet";
$MESS["TOP_MENU_DEPARTMENTS"] = "Services";
$MESS["TOP_MENU_TELEPHONY"] = "Téléphonie";
$MESS["TOP_MENU_MARKETPLACE"] = "Applications";
$MESS["TOP_MENU_OPENLINES"] = "Canaux ouverts";
$MESS["TOP_MENU_COMPANY"] = "Société";
$MESS["TOP_MENU_EMPLOYEES"] = "Employés";
$MESS["TOP_MENU_DISCS"] = "Documents";
$MESS["TOP_MENU_SERVICES"] = "Services";
?>