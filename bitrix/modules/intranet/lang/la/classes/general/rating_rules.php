<?
$MESS["PP_USER_CONDITION_SUBORDINATE_NAME"] = "Estructura de la Compañía";
$MESS["PP_USER_CONDITION_SUBORDINATE_TEXT"] = "Calcular los votos extras para las autorizaciones del usuario basado en la estructura de la compañía.";
$MESS["PP_USER_CONDITION_SUBORDINATE_T0"] = "Máximo de votos para supervisar";
$MESS["PP_USER_CONDITION_SUBORDINATE_T1"] = "Votos de suboordinados";
$MESS["PP_USER_CONDITION_SUBORDINATE_T2"] = "50% del máximo supervisor";
$MESS["PP_USER_CONDITION_SUBORDINATE_T3"] = "75% del máximo supervisor";
$MESS["PP_USER_CONDITION_SUBORDINATE_T4"] = "100% del máximo supervisor";
?>