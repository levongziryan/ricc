<?
$MESS["authprov_group"] = "Departamento";
$MESS["authprov_check_d"] = "Todos los empleados del departamento";
$MESS["authprov_check_dr"] = "Todos los empleados del departamento y del subdepartamento";
$MESS["authprov_panel_last"] = "Último";
$MESS["authprov_panel_group"] = "Seleccionar de la estructura";
$MESS["authprov_panel_search"] = "Buscar";
$MESS["authprov_panel_search_text"] = "Ingresar el login del usuario, o el primer nombre o apellido, o el nombre del departamento.";
$MESS["authprov_name_out_group"] = "Departamento";
$MESS["authprov_name"] = "Departamentos";
$MESS["authprov_group_extranet"] = "Extranet";
$MESS["authprov_name_out_user1"] = "Empleado y Supervisor";
?>