<?
$MESS["INTRANET_RATING_NAME"] = "Intranet";
$MESS["INTRANET_RATING_USER_SUBORDINATE_NAME"] = "La estructura de la Compañía basada en votos extras";
$MESS["INTRANET_RATING_USER_SUBORDINATE_DESC"] = "El valor está basado en la base de datos calculado por la regla de la \"Estructura de la compañía\".";
$MESS["INTRANET_RATING_USER_SUBORDINATE_FORMULA_DESC"] = "Valorsubordinado - votos extra; K - el usuario define el ratio.";
?>