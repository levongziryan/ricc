<?
$MESS["BX24_INVITE_TITLE_INVITE"] = "Invitar empleados";
$MESS["BX24_INVITE_TITLE_ADD"] = "Agregar empleado";
$MESS["BX24_INVITE_BUTTON"] = "Invitar";
$MESS["BX24_CLOSE_BUTTON"] = "Cerrar";
$MESS["BX24_LOADING"] = "Cargando...";
$MESS["BX24_INVITE_DIALOG_EMAIL_ERROR"] = "Las direcciones de correo electrónico son incorrectas:<br/>";
$MESS["BX24_INVITE_DIALOG_EMAIL_LIMIT_EXCEEDED"] = "Se ha superado el número máximo de direcciones de correo electrónico en la invitación.";
$MESS["BX24_INVITE_DIALOG_USER_ID_NO_EXIST_ERROR"] = "El ID de usuario es incorrecto.";
$MESS["BX24_INVITE_DIALOG_USER_EXIST_ERROR"] = "Ya existen usuarios con estas direcciones de correo electrónico.";
$MESS["BX24_INVITE_DIALOG_MAX_COUNT_ERROR"] = "El número de invitados excede los términos de licencia.";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT"] = "Únase a nosotros en nuestra nueva cuenta de Bitrix24. Este es un lugar donde todos pueden comunicarse, colaborar en tareas y proyectos, administrar clientes y hacer mucho más.";
$MESS["BX24_INVITE_DIALOG_ERROR_EMPTY_EMAIL_LIST"] = "Direcciones de e-mail no especificadas.";
$MESS["BX24_INVITE_DIALOG_ERROR_EMPTY_EMAIL"] = "Dirección de e-mail no especificada.";
$MESS["BX24_INVITE_DIALOG_ERROR_EMPTY_LAST_NAME"] = "El apellido es obligatorio.";
$MESS["BX24_INVITE_DIALOG_ERROR_EMPTY_PASSWORD"] = "La contraseña no especificada.";
$MESS["BX24_INVITE_DIALOG_ERROR_WRONG_PASSWORD_CONFIRM"] = "La contraseña y su confirmación no coinciden.";
$MESS["BX24_INVITE_DIALOG_ERROR_WRONG_USER"] = "Correo electrónico del usuario especificado es incorrecto.";
$MESS["BX24_INVITE_DIALOG_ERROR_USER_TRANSFER"] = "No se puede convertir el usuario de correo electrónico.";
?>