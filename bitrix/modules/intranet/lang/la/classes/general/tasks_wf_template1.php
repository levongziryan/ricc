<?
$MESS["INTASK_WF_TMPL_HACK_SRE"] = "Establecer la persona responsable";
$MESS["INTASK_WF_TMPL_HACK_IPE"] = "Iniciar Ejecución";
$MESS["INTASK_WF_TMPL_HACK_CE"] = "Finalizar";
$MESS["INTASK_WF_TMPL_HACK_CLE"] = "Cerrar";
$MESS["INTASK_WF_TMPL_HACK_WE"] = "Pendiente";
$MESS["INTASK_WF_TMPL_HACK_DE"] = "Diferir";
$MESS["INTASK_WF_TMPL_HACK_NS"] = "No se ha iniciado";
$MESS["INTASK_WF_TMPL_HACK_NOTSTARTED"] = "No se ha iniciado";
$MESS["INTASK_WF_TMPL_HACK_INPROGRESS"] = "En progreso";
$MESS["INTASK_WF_TMPL_HACK_CLOSED"] = "Cerrado";
$MESS["INTASK_WF_TMPL_HACK_WAITING"] = "Pendiente";
$MESS["INTASK_WF_TMPL_HACK_NOTACCEPTED"] = "No fue aceptada";
$MESS["INTASK_WF_TMPL_HACK_COMPLETED"] = "Finalizada";
$MESS["INTASK_WF_TMPL_HACK_DEFERRED"] = "Diferir";
$MESS["INTASK_WF_TMPL_HACK_AE"] = "Aceptar";
?>