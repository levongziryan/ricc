<?
$MESS["INTR_MAIL_DOMAINREADY_NOTICE"] = "El dominio [b]#DOMAIN#[/b] se ha conectado.<a href=\"#SERVER#/company/personal/mail/?page=manage\">Manage mailboxes</a>";
$MESS["INTR_MAIL_DOMAIN_SUPPORT_LINK"] = "http://www.bitrixsoft.com/support/index.php?utm_source=regru&utm_medium=email&utm_campaign=regru_email_support";
$MESS["INTR_MAIL_DOMAIN_SUPPORTB24_LINK"] = "https://www.bitrix24.com/support/helpdesk/?utm_source=regru&utm_medium=email&utm_campaign=regru_email_support";
$MESS["INTR_MAIL_DOMAIN_LEARNMORE_LINK"] = "http://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=55&CHAPTER_ID=06527?utm_source=regru&utm_medium=email&utm_campaign=regru_email_howto";
$MESS["INTR_MAIL_DOMAIN_LEARNMOREB24_LINK"] = "http://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=55&CHAPTER_ID=06527?utm_source=regru&utm_medium=email&utm_campaign=regru_email_howto";
$MESS["INTR_SYNC_OUTLOOK_NOWEBSERVICE"] = "El necesario el módulo de Web Services para sincronizar con el Outlook. Póngase en contacto con el administrador del portal.";
?>