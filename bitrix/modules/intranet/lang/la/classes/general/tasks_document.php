<?
$MESS["INTASK_TD_FIELD_ID"] = "ID de la Tarea";
$MESS["INTASK_TD_FIELD_TIMESTAMP_X"] = "Fecha de modificación de la tarea";
$MESS["INTASK_TD_FIELD_MODIFIED"] = "Tarea modificada por";
$MESS["INTASK_TD_FIELD_DATE_CREATE"] = "Fecha de creación de la tarea";
$MESS["INTASK_TD_FIELD_CREATED"] = "Creado por";
$MESS["INTASK_TD_FIELD_IBLOCK_SECTION_ID"] = "Carpeta de la tarea";
$MESS["INTASK_TD_FIELD_DATE_ACTIVE_FROM"] = "Inicio";
$MESS["INTASK_TD_FIELD_DATE_ACTIVE_TO"] = "Final";
$MESS["INTASK_TD_FIELD_NAME"] = "Nombre de la Tarea";
$MESS["INTASK_TD_FIELD_DETAIL_TEXT"] = "Descripción de la tarea";
$MESS["INTASK_TD_OPERATIONS_READ"] = "Vista";
$MESS["INTASK_TD_OPERATIONS_WRITE"] = "Actualizar";
$MESS["INTASK_TD_OPERATIONS_COMMENT"] = "Comentario";
$MESS["INTASK_TD_OPERATIONS_DELETE"] = "Suprimir";
$MESS["INTASK_TD_USER_GROUPS_AUTHOR"] = "Autor";
$MESS["INTASK_TD_USER_GROUPS_RESP"] = "Responsable";
$MESS["INTASK_TD_USER_GROUPS_TRACK"] = "Rastreadores";
$MESS["INTASK_TD_USER_GROUPS_FRIEND"] = "Amigos";
$MESS["INTASK_TD_USER_GROUPS_FRIEND2"] = "Amigos de mis amigos";
$MESS["INTASK_TD_USER_GROUPS_ALL"] = "Todos los visitantes";
$MESS["INTASK_TD_USER_GROUPS_AUTHORIZED"] = "Usuarios Autorizados";
$MESS["INTASK_TD_USER_GROUPS_OWNER"] = "Propietario";
$MESS["INTASK_TD_USER_GROUPS_MODS"] = "Moderadores";
$MESS["INTASK_TD_USER_GROUPS_MEMBERS"] = "Miembros";
?>