<?
$MESS["I_NEW_USER_TITLE"] = "Se ha añadido un nuevo empleado";
$MESS["I_NEW_USER_TITLE_SETTINGS"] = "Nuevos empleados";
$MESS["I_NEW_USER_TITLE_LIST"] = "Nuevos empleados";
$MESS["I_NEW_USER_MENTION"] = "los han mencionado a usted en un comentario acerca de un nuevo empleado #title#. ";
$MESS["I_NEW_USER_MENTION_M"] = "los han mencionado a usted en un comentario acerca de un nuevo empleado #title#";
$MESS["I_NEW_USER_MENTION_F"] = "los han mencionado a usted en un comentario acerca de un nuevo empleado #title#";
$MESS["I_NEW_USER_EXTERNAL_TITLE"] = "Nuevo usuario externo agregado";
?>