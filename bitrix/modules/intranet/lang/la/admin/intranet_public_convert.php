<?
$MESS["INTRANET_PUBLIC_CONVERT_TITLE"] = "Convertir Páginas Públicas";
$MESS["INTRANET_PUBLIC_CONVERT_DESC"] = "La nueva plantilla Bitrix24 viene con navegación actualizada y mejorada, y un nuevo menú personalizado pequeño a la izquierda. Ahora los usuarios pueden personalizar la apariencia de cada sección. El área de trabajo es ahora un 20 por ciento más amplia. El menú superior ahora cuenta con un sistema de navegación unificado.";
$MESS["INTRANET_PUBLIC_CONVERT_DESC_TITLE"] = "Convertir sus páginas públicas ahora para probar las nuevas e interesantes características:
";
$MESS["INTRANET_PUBLIC_CONVERT_DESC2"] = "<ul> 
<li>Menú izquierdo fácilmente personalizable</li>
<li>Todos los elementos esenciales son siempre útiles</li>
<li>Personalice la herramienta más utilizada (CRM, Tareas o Flujo de actividad) adaptable para que coincida con diferentes grupos de clientes</li>
<li>El área de trabajo se amplía en un 20 por ciento: contraer el menú de la izquierda cuando no lo utilice</li>
<li>Menú superior unificada: menús de navegación de segundo nivel compartir estructura común</li>
<li>Buscar páginas por nombre más rápido usando la barra de búsqueda</li>
</ul>";
$MESS["INTRANET_PUBLIC_CONVERT_OPTIONS_TITLE"] = "Convertir páginas públicas ahora mismo";
$MESS["INTRANET_PUBLIC_CONVERT_OPTIONS_DESC"] = "(Se recomienda crear una copia de seguridad de sus páginas públicas si desea volver a la estructura anterior más adelante)";
$MESS["INTRANET_PUBLIC_CONVERT_SECTIONS"] = "Secciones (\"Tiempo y Reportes\", \"Calendario\", \"Flujos de trabajo\" y \"Bitrix24.Drive\" se creará)";
$MESS["INTRANET_PUBLIC_CONVERT_INDEX"] = "Haz que cualquier sección sea tu página de inicio (sobrescribiendo el archivo index.php y copiando en el Flujo de Actividad en /stream/ folder)";
$MESS["INTRANET_PUBLIC_CONVERT_TAB"] = "Conversión";
$MESS["INTRANET_PUBLIC_CONVERT_TAB_TITLE"] = "Editar preferencias de conversión";
$MESS["INTRANET_PUBLIC_CONVERT_BUTTON"] = "Convertir Ahora";
$MESS["INTRANET_PUBLIC_CONVERT_FINISH"] = "La conversión ha finalizado.";
$MESS["INTRANET_PUBLIC_SKIP_CONVERT_BUTTON"] = "No convertir";
$MESS["INTRANET_PUBLIC_SKIP_CONVERT_FINISH"] = "Se canceló la conversión";
?>