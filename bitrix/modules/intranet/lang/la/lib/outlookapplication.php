<?
$MESS["WS_OUTLOOK_APP_TITLE"] = "Servicios de Microsoft Outlook";
$MESS["WS_OUTLOOK_APP_OPTIONS_CAPTION"] = "Conectar";
$MESS["WS_OUTLOOK_APP_TITLE_OPTION"] = "Contactos, calendarios, tareas";
$MESS["WS_OUTLOOK_APP_DESC"] = "Obtener una contraseña para Microsoft Outlook para configurar la sincronización de los calendarios, contactos o tareas.";
?>