<?
$MESS["INTRANET_B24_INTEGRATION_CANT_CREATE_THEME"] = "No se puede crear un tema utilizando datos proporcionados.";
$MESS["INTRANET_B24_INTEGRATION_UPLOAD_ERROR"] = "Error de carga de archivos.";
$MESS["INTRANET_B24_INTEGRATION_THEMES_LIMIT_EXCEEDED"] = "Se superó el número máximo de temas personalizados (#NUM#).";
?>