<?
$MESS["SONET_TASK_TITLE"] = "Completar el perfil";
$MESS["SONET_TASK_DESCRIPTION"] = "Proporcione más información para su perfil";
$MESS["SONET_TASK_DESCRIPTION_V2"] = "Complete su perfil, cargue su imagen de usuario y proporcione su información personal.

#ANCHOR_EDIT_PROFILE#Continúe con su perfil#ANCHOR_END#
";
$MESS["SONET_INVITE_TASK_TITLE"] = "Invitar a colegas";
$MESS["SONET_INVITE_TASK_DESCRIPTION"] = "Invite a colegas a su Bitrix24.

#ANCHOR_INVITE#Invite#ANCHOR_END#
";
$MESS["SONET_INSTALL_APP_TASK_TITLE"] = "Descargar la aplicación Bitrix24";
$MESS["SONET_INSTALL_APP_TASK_DESCRIPTION"] = "¡Instale la aplicación Bitrix24 en su dispositivo móvil y manténgase conectado!

Instale la aplicación de escritorio en su computadora (Mac, Windows o Linux) para comunicarse con sus colegas cuando su navegador esté cerrado.

[URL=https://www.bitrix24.com/features/mobile.php]Download Bitrix24 applications[/URL]
";
?>