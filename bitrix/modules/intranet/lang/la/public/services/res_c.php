<?
$MESS["SERVICES_TITLE"] = "Reserva de Sala de Reuniones";
$MESS["SERVICES_INIT_DATE"] = "-Mostrar Fecha Actual-";
$MESS["SERVICES_INFO"] = "Cada vez que desee reservar una sala de reuniones, encuentre el tiempo libre en el calendario; llame a su gerente para confirmar la reserva y pongalo en el calendario de reservas.";
$MESS["SERVICES_LINK"] = "Ver Tabla de Reservas de Sala de Reuniones ";
?>