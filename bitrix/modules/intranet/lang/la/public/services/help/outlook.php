<?
$MESS["SERVICES_TITLE"] = "Integración de dos vías con Microsoft Outlook ";
$MESS["SERVICES_INFO2"] = "<br />
      </td></tr>
  </tbody>
</table>

<h2>¿Qué tiene de grandioso la sincronización de dos vías? </h2>
<p>Este tipo de sincronización le permite mantener los datos (por ejemplo, contactos personales) actualizados, tanto en el portal como en MS Outlook de forma simultánea. Intente sincronizarlos ahora mismo y vea lo fácil que puede ser! Suponga que usted está a bordo del avión de la planificación de reuniones, tareas y presentaciones para su empresa. Después de hacer la planificación en MS Outlook, utilice la función de sincronización de dos vías para que todos sus nuevos planes se puedan ver desde el calendario del portal inmediatamente. Manténgase al tanto de sus compromisos sin el tedioso proceso de copia manual! Rápido de crear, fácil de manejar!<a name='useful'></a></p>

<h2>Sincronización de usuarios</h2>

<p>Para asignar tareas o recibir tareas de otras personas, debe añadir a estas personas en su Microsoft Outlook. Esto es muy sencillo: seleccione empleados &gt; Búsqueda de Empleados y haga clic en <strong>Sincronizar con Outlook</strong>.</p>
<p>Aquellos de ustedes que prefieren la plantilla Bitrix24: seleccione Empleados &gt; Búsqueda de empleados (en el menú de la izquierda de la pantalla) para abrir la página de explorador de  usuarios (empleados). Ahora, haga clic en <strong>Más</strong>, en la barra de herramientas y seleccione <strong>Outlook</strong>.</p>
<p>El procedimiento de importación es totalmente automatizado; sólo tiene que confirmar la operación una o dos veces. Ahora espere mientras se realiza la importación.</p>
<p>Después de que el procedimiento de importación se ha completado, sus empleados se mostrarán en el calendario:</p>
<div align='center'>
  <table style='BORDER-COLLAPSE: collapse' border='0' cellspacing='1' cellpadding='1'>
    <tbody>
      <tr><td>
          <div align='center'><a href='javascript:ShowImg('#SITE#'images/docs/big/outlook.png', 600, 413, 'Adding an employee')'><img title='Agregando empleados' border='0' alt='Agregando empleados' src='#SITE#'images/docs/outlook_sm.png' width='300' height='207' /> </a>
            <br />
          </div>

          <div align='center'><i>Empleados agregados desde el portal</i><br />
          </div>
        </td></tr>
    </tbody>
  </table>
</div>

<p>No es peligroso borrar empleados desde el MS Outlook, ya que estos no serán eliminados del portal. Sólo eliminándolos del portal podermos borrarlos definitivamente.</p>

<h2><a name='my_kalendar'></a>Sinconizando Calensarios con MS Outlook</h2>
<p>Puede sincronizar los calendarios de portal con calendarios de Microsoft Outlook: ya sean estos calendarios personales, de  empleados o de la empresa. Intente sincronizarlos ahora mismo! Abra una página de calendario, seleccione <strong>Conectar a Outlook</strong> en el menú de acción e inicie la sincronización:</p>

<div align='center'><img src='#SITE#'images/docs/outlook_1.png'  />
<br>
<i> Conectando a Outlook</i></div>

<p>No preste atención a los cuadros de mensaje de MS Outlook: la mayoría de ellos son sólo  notificaciones; confirme las operaciones siempre que se requiera su confirmación. Por ejemplo, si se encuentra con un mensaje &quot;Conecte este calendario de SharePoint a Outlook?&quot; haga clic en Sí, sin dudarlo. La integración de dos vías de MS Outlook se ajusta plenamente a las especificaciones y normas de Microsoft que garantizan una operación absolutamente transparente.</p>

<div align='center'><img src='#SITE#'images/docs/outlook_2_sm.png' />
  <br />
</div>

<p>Si usted desea controlar los detalles del proceso, puede hacer click en <strong>Avanzado</strong> y proveer descripción de su calendario:</p>

<div align='center'>
  <table style='BORDER-COLLAPSE: collapse' border='0' cellspacing='1' cellpadding='1'>
    <tbody>
      <tr><td>
          <div align='center'><a href='javascript:ShowImg('#SITE#'images/docs/big/outlook_3.png', 509, 449, 'Descripción del calendario')'><img title='Descripción del calendario' border='0' alt='Calendar description' src='#SITE#'images/docs/outlook_3_sm.png' /> </a>
            <br />
          </div>

          <div align='center'><i>Descripción del calendario</i>
            <br />
          </div>
        </td></tr>
    </tbody>
  </table>
</div>

<p><a name='company_calendar'></a>Ahora mire el resultado. Su Outlook cuenta con un nuevo calendario que muestra todos sus eventos! ¿Qué podría ser más conveniente que la visualización de eventos de la empresa en Outlook después de una larga ausencia?. Manténgase al día con su empresa, conecte y sincronice calendarios y este en el centro de los acondtecimientos!</p>

<div align='center'>
  <table style='BORDER-COLLAPSE: collapse' border='0' cellspacing='1' cellpadding='1'>
    <tbody>
      <tr><td>
          <div align='center'><a href='javascript:ShowImg('#SITE#'images/docs/big/outlook_4.png', 709, 575, 'Calendar has been added to Outlook!')'><img title='Nuevo calendario agregado al Outlook!' border='0' alt='Nuevo calendario agregado al Outlook!' src='#SITE#'images/docs/outlook_4_sm.png'  /> </a>
            <br />
          </div>

          <div align='center'><i>Nuevo calendario agregado al Outlook!</i>
            <br />
          </div>
        </td></tr>
    </tbody>
  </table>
</div>

<p><a name='kalendars'></a>Ahora conecte todos los calendarios del portal de la misma forma. Muestrelos muestrelos sobre el mismo grid y verá el calendario de OutLook, tal como ve sus calendarios en el portal de intranet!</p>

<div align='center'>
  <table style='BORDER-COLLAPSE: collapse' border='0' cellspacing='1' cellpadding='1'>
    <tbody>
      <tr><td>
          <div align='center'><a href='javascript:ShowImg('#SITE#'images/docs/big/outlook_5.png', 709, 575, 'Export calendar(s) to Outlook')'><img title='Export calendar(s) to Outlook' border='0' alt='Export calendar(s) to Outlook' src='#SITE#'images/docs/outlook_5_sm.png'  /> </a>
            <br />
          </div>

          <div align='center'><i> Calendatios de MS Outlook en un mismo grid</i></div>
        </td></tr>
    </tbody>
  </table>
</div>

<h2>Agregando eventos</h2>
<p><br />
¿Cómo funciona la sincronización de dos vías en la vida real? Trate de añadir un nuevo evento a un calendario de MS Outlook - verá aparecer, dicho evento, en el calendario del portal! Hágalo ahora:</p>
<ul>
  <li>seleccione la fecha del evento en la cuadrícula del calendario;</li>
  <li>haga doble clic para abrir la ventana para crear un nuevo evento;</li>
  <li>compelte los campos Tema, Inicio, Fin y descripción;</li>
  <li>haga clic en <strong>Guardar y Cerrar</strong>.</li>
</ul>
<p>Ahora veamos el nuevo evento agregado a la agenda de MS Outlook.</p>
<div align='center'><a href='javascript:ShowImg('#SITE#'images/docs/big/outlook_6.png',650,500,'New Event')'> <img height='192' border='0' width='250' src='#SITE#'images/docs/outlook_6_sm.png' title='Click para agrandar' style='cursor: pointer;' /></a></p>
</div>
<p class='a1'><a name='sinhr'></a>
Usted no necesita preocuparse por la sincronización de los calendarios de Microsoft Outlook con los calendarios de portal. Esta tarea se lleva a cabo de forma automática junto con la revisión de correo electrónico de Outlook. La esquina inferior derecha de la ventana de Outlook mostrará el progreso de la sincronización.</p>

<p class='a1' align='center'><img src='/images/docs/outlook_8.png' /></p>
<p>La siguiente imagen muestra el mismo evento en el calendario del portal:</p>

<div align='center'><a href='javascript:ShowImg('#SITE#'images/docs/big/outlook_9.png',452,272,'New Event in the Calendar')'> <img height='150' border='0' width='249' src='#SITE#'images/docs/outlook_9_sm.png' title='Click para agrandar' style='cursor: pointer;' /></a></div>

<p>Usted puede editar y eliminar eventos desde MS Outlook y desde el portal de la misma forma. Cualquier cambio hecho en una aplicación será reflejada en la otra.</p>

<a name='useful1'></a>
<h2>Sincronizando tareas personales</h2>
<p>Ahora trate de sincronizar sus tareas. Tenga en mente que este proceso es automático y no requiere de su atención.</p>

<div align='center'>
  <div align='center'><a href='javascript:ShowImg('#SITE#'images/docs/big/outlook_13.png', 600, 413, 'Synchronize personal tasks')'><img title='Sincronizando tareas personales' border='0' alt='Sincronizando tareas personales' src='#SITE#'images/docs/outlook_13_sm.png' width='400' height='276' /> </a>
    <br />
  </div>
  <div align='center'><i>Sincronizando tareas personales</i></div>
</div>

<p>para sincronizar tareas:</p>
<ul>
  <li>sibncronice usuarios (click en la página de <strong>Busqueda de Usuarios</strong> del <b>Outlook</b> ); </li>
  <li>cuando se le solicite, ingrese la contraseña de acceso al portal; </li>
  <li>la lista de usuarios será agregada al MS Outlook automáticamente; </li>
  <li>use los comandos de MS Outlook para asignar tareas a cualquier usuario; </li>
  <li>MS Outlook se conectará con el portal y sincronizará las tareas. </li>
</ul>";
$MESS["SERVICES_INFO1"] = " <br />
      </td></tr>
  </tbody>
</table>

<h2>¿Qué tiene de grandioso la sincronización de dos vías? </h2>
<p>Este tipo de sincronización le permite mantener los datos (por ejemplo, contactos personales) actualizados, tanto en el portal como en MS Outlook de forma simultánea. Intente sincronizarlos ahora mismo y vea lo fácil que puede ser! Suponga que usted está a bordo del avión de la planificación de reuniones, tareas y presentaciones para su empresa. Después de hacer la planificación en MS Outlook, utilice la función de sincronización de dos vías para que todos sus nuevos planes se puedan ver desde el calendario del portal inmediatamente. Manténgase al tanto de sus compromisos sin el tedioso proceso de copia manual! Rápido de crear, fácil de manejar!<a name='useful'></a></p>

<h2>Sincronización de usuarios</h2>

<p>Para asignar tareas o recibir tareas de otras personas, debe añadir a estas personas en su Microsoft Outlook. Esto es muy sencillo: seleccione mpleados &gt; Búsqueda de Empleados y haga clic en <strong>Sincronizar con Outlook</strong>.</p>
<p>Aquellos de ustedes que prefieren la plantilla Bitrix24: seleccione Empleados &gt; Búsqueda de empleados (en el menú de la izquierda de la pantalla) para abrir la página de explorador de  usuarios (empleados). Ahora, haga clic en <strong>Más</strong>, en la barra de herramientas y seleccione <strong>Outlook</strong>.</p>
<p>El procedimiento de importación es totalmente automatizado; sólo tiene que confirmar la operación una o dos veces. Ahora espere mientras se realiza la importación.</p>
<p>Después de que el procedimiento de importación se ha completado, sus empleados se mostrarán en el calendario:</p>
<div align='center'>
  <table style='BORDER-COLLAPSE: collapse' border='0' cellspacing='1' cellpadding='1'>
    <tbody>
      <tr><td>
          <div align='center'><a href='javascript:ShowImg('#SITE#'images/docs/big/outlook.png', 600, 413, 'Adding an employee')'><img title='Agregando empleados' border='0' alt='Agregando empleados' src='#SITE#'images/docs/outlook_sm.png' width='300' height='207' /> </a>
            <br />
          </div>

          <div align='center'><i>Empleados agregados desde el portal</i><br />
          </div>
        </td></tr>
    </tbody>
  </table>
</div>

<p>No es peligroso borrar empleados desde el MS Outlook, ya que estos no serán eliminados del portal. Sólo eliminándolos del portal podermos borrarlos definitivamente.</p>

<h2><a name='my_kalendar'></a>Sinconizando Calensarios con MS Outlook</h2>
<p>Puede sincronizar los calendarios de portal con calendarios de Microsoft Outlook: ya sean estos calendarios personales, de  empleados o de la empresa. Intente sincronizarlos ahora mismo! Abra una página de calendario, seleccione <strong>Conectar a Outlook</strong> en el menú de acción e inicie la sincronización:</p>

<div align='center'><img src='#SITE#'images/docs/outlook_1.png'  />
<br>
<i> Conectando a Outlook</i></div>

<p>No preste atención a los cuadros de mensaje de MS Outlook: la mayoría de ellos son sólo  notificaciones; confirme las operaciones siempre que se requiera su confirmación. Por ejemplo, si se encuentra con un mensaje &quot;Conecte este calendario de SharePoint a Outlook?&quot; haga clic en Sí, sin dudarlo. La integración de dos vías de MS Outlook se ajusta plenamente a las especificaciones y normas de Microsoft que garantizan una operación absolutamente transparente.</p>

<div align='center'><img src='#SITE#'images/docs/outlook_2_sm.png' />
  <br />
</div>

<p>Si usted desea controlar los detalles del proceso, puede hacer click en <strong>Avanzado</strong> y proveer descripción de su calendario:</p>

<div align='center'>
  <table style='BORDER-COLLAPSE: collapse' border='0' cellspacing='1' cellpadding='1'>
    <tbody>
      <tr><td>
          <div align='center'><a href='javascript:ShowImg('#SITE#'images/docs/big/outlook_3.png', 509, 449, 'Descripción del calendario')'><img title='Descripción del calendario' border='0' alt='Calendar description' src='#SITE#'images/docs/outlook_3_sm.png' /> </a>
            <br />
          </div>

          <div align='center'><i>Descripción del calendario</i>
            <br />
          </div>
        </td></tr>
    </tbody>
  </table>
</div>

<p><a name='company_calendar'></a>Ahora mire el resultado. Su Outlook cuenta con un nuevo calendario que muestra todos sus eventos! ¿Qué podría ser más conveniente que la visualización de eventos de la empresa en Outlook después de una larga ausencia?. Manténgase al día con su empresa, conecte y sincronice calendarios y este en el centro de los acondtecimientos!</p>

<div align='center'>
  <table style='BORDER-COLLAPSE: collapse' border='0' cellspacing='1' cellpadding='1'>
    <tbody>
      <tr><td>
          <div align='center'><a href='javascript:ShowImg('#SITE#'images/docs/big/outlook_4.png', 709, 575, 'Calendar has been added to Outlook!')'><img title='Nuevo calendario agregado al Outlook!' border='0' alt='Nuevo calendario agregado al Outlook!' src='#SITE#'images/docs/outlook_4_sm.png'  /> </a>
            <br />
          </div>

          <div align='center'><i>Nuevo calendario agregado al Outlook!</i>
            <br />
          </div>
        </td></tr>
    </tbody>
  </table>
</div>

<p><a name='kalendars'></a>Ahora conecte todos los calendarios del portal de la misma forma. Muestrelos muestrelos sobre el mismo grid y verá el calendario de OutLook, tal como ve sus calendarios en el portal de intranet!</p>

<div align='center'>
  <table style='BORDER-COLLAPSE: collapse' border='0' cellspacing='1' cellpadding='1'>
    <tbody>
      <tr><td>
          <div align='center'><a href='javascript:ShowImg('#SITE#'images/docs/big/outlook_5.png', 709, 575, 'Export calendar(s) to Outlook')'><img title='Export calendar(s) to Outlook' border='0' alt='Export calendar(s) to Outlook' src='#SITE#'images/docs/outlook_5_sm.png'  /> </a>
            <br />
          </div>

          <div align='center'><i> Calendatios de MS Outlook en un mismo grid</i></div>
        </td></tr>
    </tbody>
  </table>
</div>

<h2>Agregando eventos</h2>
<p>
¿Cómo funciona la sincronización de dos vías en la vida real? Trate de añadir un nuevo evento a un calendario de MS Outlook - verá aparecer, dicho evento, en el calendario del portal! Hágalo ahora:</p>
<ul>
  <li>seleccione la fecha del evento en la cuadrícula del calendario;</li>
  <li>haga doble clic para abrir la ventana para crear un nuevo evento;</li>
  <li>compelte los campos Tema, Inicio, Fin y descripción;</li>
  <li>haga clic en <strong>Guardar y Cerrar</strong>.</li>
</ul>
<p>Ahora veamos el nuevo evento agregado a la agenda de MS Outlook.</p>
<div align='center'><a href='javascript:ShowImg('#SITE#'images/docs/big/outlook_6.png',650,500,'New Event')'> <img height='192' border='0' width='250' src='#SITE#'images/docs/outlook_6_sm.png' title='Click para agrandar' style='cursor: pointer;' /></a></p>
</div>
<p class='a1'><a name='sinhr'></a>Usted no necesita preocuparse más por la sincronización de los calendarios de Microsoft Outlook con los calendarios de portal. Esta tarea se lleva a cabo de forma automática junto con la revisión del correo electrónico de Outlook. En la esquina inferior derecha de la ventana de Outlook se mostrará el progreso de la sincronización.</p>

<p class='a1' align='center'><img src='/images/docs/outlook_8.png' /></p>
<p>La siguiente imagen muestra el mismo evento en el calendario del portal:</p>

<div align='center'><a href='javascript:ShowImg('#SITE#'images/docs/big/outlook_9.png',452,272,'New Event in the Calendar')'> <img height='150' border='0' width='249' src='#SITE#'images/docs/outlook_9_sm.png' title='Click para agrandar' style='cursor: pointer;' /></a></div>

<p>Usted puede editar y eliminar eventos en MS Outlook y en los calendarios del portal indistintamente, cualquier cambio hecho en una de las aplicaciones será reflejado automáticamente en la otra.</p>

<a name='useful1'></a>
<h2>Sincronizando tareas personales</h2>
<p>Ahora trate de sincronizar sus tareas. Tenga en mente que este proceso es automático y no requiere de su atención.</p>

<div align='center'>
  <div align='center'><a href='javascript:ShowImg('#SITE#'images/docs/big/outlook_13.png', 600, 413, 'Sincronizando tareas personales')'><img title='Sincronizando tareas personales' border='0' alt='Sincronizando tareas personales' src='#SITE#'images/docs/outlook_13_sm.png' width='400' height='276' /> </a>
    <br />
  </div>
  <div align='center'><i>Sincronizando tareas personales</i></div>
</div>

<p>para sincronizar tareas:</p>
<ul>
  <li>Sincronice usuarios (click en la página de Búsqueda de Usuarios en <b>Outlook</b>); </li>
  <li>cuando se le solicite, ingrese su contraseña de acceso al portal; </li>
  <li>la lista de usuarios será agregada a MS Outlook automáticamente; </li>
  <li>use comandos MS Outlook para asignar tareas a cualquier usuario; </li>
  <li>MS Outlook s conectará con el portal y sincronizará las tareas. </li>
</ul>";
$MESS["SERVICES_LINK1"] = "Conecte los contactos";
$MESS["SERVICES_LINK2"] = "Conecte Mis tareas";
$MESS["SERVICES_CONNECT"] = "Conectar";
?>