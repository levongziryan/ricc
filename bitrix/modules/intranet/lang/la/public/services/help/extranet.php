<?
$MESS["SERVICES_TITLE"] = "Extranet";
$MESS["SERVICES_INFO"] = "<h3>Unificando espacios de información externas</h3>

<p>El módulo Extranet es una ampliación del Portal de Intranet que permite a una empresa el establecer una comunicación confidencial con contratistas, proveedores, distribuidores u otros usuarios externos, gestionando prohibiciones a terceros de tal manera que se evita su acceso a información privada de la empresa.</p>
<p>La Extranet es un espacio de información segura para interactuar con el entorno externo. El espacio web colectivo está especialmente diseñado para una cooperación eficaz con socios y otros agentes externos. Invite a sus socios y colegas a los grupos de trabajo de la extranet y trabaje sobre sus tareas y problemas compartidos de forma colectiva. Esta solución garantiza que su comunicación es segura mientras se mantine la integridad de la intranet.<br />
</p>

<div align='center'> <img width='302' height='218' src='#SITE#images/docs/cp/extranet_main-s300.png' complete='complete' />
  <br />
<i>Página principal de extranet</i> &nbsp; </div>

<h3>Acceso y seguridad</h3>

<p>Los usuarios invitados que no son empleados de la compañía obtendrán un permiso de acceso limitado y, lo que es esencial, accesos sólo a las páginas Extranet. A estos usuarios no se les permite ver información privada de la empresa. Al mismo tiempo, los empleados de la compañía pueden ser invitados y unirse a los grupos de trabajo de Extranet para trabajar junto con colegas y socios: discutir problemas esenciales en foros y blogs; trabajar con documentos compartidos; planear actividades en los calendarios; asignar tareas y ver su progreso; publicar reportes y muchos más.<br />
</p>

<h3>Equipo de trabajo</h3>

<p>Entonces ¿Cuáles son las ventajas de la Extranet al ser un área neutral para reuniones y colaboración? Fundamentalmente y tecnológicamente, la solución utiliza los mismos principios en los que basa el Portal de Intranet del tal manera que el trabajo en equipo desde la Extranet es absolutamente análogo al de los grupos de trabajo en la Intranet. La única diferencia radica en políticas de seguridad más estrictas.<br />
</p>

<h3>¿Cómo instalar?</h3>

<p>Al instalar Bitrix24, el sistema instala el módulo de extranet automáticamente, sin embargo este no podría ser el caso cuando se hace un upgrade desde una versión antigua. Si el módulo de Extranet no está presente en la lista de módulos existentes, usted debe <a href=\"/bitrix/admin/module_admin.php\">instalarlo</a> antes de usarlo.</p>

<div align='center'><img width='361' height='205' src='#SITE#images/docs/cp/extranet_setup.png' complete='complete' />
  <br />
  <i>Instalando el módulo Extranet</i> </div>

<p>Después de haber instalado satisfactoriamente el módulo, usted deberá ejecutar el <a href='/bitrix/admin/wizard_install.php?lang=en&amp;wizardName=bitrix:extranet&amp;<?=bitrix_sessid_get()?>' >Asistente para Sitios de Extranet</a> desde el cual podrá configurar los parámetros necesarios para el correcto funcionamiento de la extranet.</p>

<p>Tan pronto como el asitente se complete, un item de <strong>Extranet</strong> será mostrado en el menú superior. El sistema de <strong>Extranet</strong> está levantado, ejecutándose y disponible para ser visitado.</p>

<table width='100%' style='BORDER-COLLAPSE: collapse' border='0' cellspacing='0' cellpadding='0'>
  <tbody>
    <tr><td valign='top'>
        <br />
      </td>
    <td valign='top'>         <span class='text'><b>Cosas a hacer:</b>
          <ul>

            <li>use las <strong>herramientas espaciales de extranet</strong> para facilitar su experiencia de trabajo; </li>

            <li>provea información pública (considere las secciones de <b>Compañía</b> o <b>Acerca de Nosotros</b>); </li>

            <li><b>invite </b>a sus colegas en compañías externas a los equipos de trabajo.</li>
        </ul>
      </span></td></tr>
  </tbody>
</table>";
?>