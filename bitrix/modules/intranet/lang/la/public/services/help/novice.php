<?
$MESS["SERVICES_TITLE"] = "Información para Nuevos Empleados";
$MESS["SERVICES_INFO"] = "<img width='173' vspace='5' hspace='5' height='170' align='right' src='#SITE#images/new_sm.jpg' />Los nuevos miembros del equipo de trabajo deben de tomar el <a href=\"#SITE#services/learning/\">Curso para Empleados Nuevos</a>, con el fin de familiarizarse con la empresa.<br />

<p>Después de tomar el curso, debe ser revisada la siguiente información:</p>

<p><a href='#SITE#about/company/'>Acerca de la Compañía</a> - información oficial y no oficial que debe saber sobre la empresa. Aquí usted leerá acerca de la fundación de la empresa y las principales áreas de actividad. Esta sección también contiene información útil: datos bancarios de la empresa, direcciones y números de teléfono. Utilice esta información en su trabajo con los clientes, filiales y contratistas.</p>

<p><a href='#SITE#company/structure.php'>Estructura de la compañía</a> - proporciona una representación visual de la jerarquía de departamentos de la empresa así como sus diferentes oficinas. Aquí usted puede encontrar rápidamente el departamento que se encarga de la función de negocio de interés para usted.<br />
</p>

<p><a href='#SITE#company/index.php'>Buscar empleados</a> - te permite encontrar a cualquier persona en la empresa usando cualquier criterio; cerciorarte si una persona está en la oficina, y obtener su información de contacto. Usted puede encontrar a una persona por departamento y/o nombre, o usted puede tomar ventaja de las funciones de búsqueda ampliadas, las mismas que pueden buscar por dirección de correo electrónico y palabras clave. Cada empleado (o usuario en términos del portal) tiene una página personal donde puede publicar otra información: fotos, documentos, etc. Usted puede abrir la página personal de un usuario y comentar sus publicaciones sobre el blog; enviar mensajes privados y más.<br />
</p>

<p><a href='#SITE#about/index.php'>Importación de Noticias</a> - muestra encabezados noticiosos para mantenerlo informado sobre diferentes eventos.<br />


<p><a href='#SITE#about/life.php'>Vida de la comañía</a> - el día a día de la compañía: eventos y cunpleaños, celabraciones y feriados, formación de equipos y capacitación o cualquier información común para todos.<br />


<p><a href='#SITE#about/calendar.php'>Calendario de eventos</a> - una manera muy conveniente para ver los eventos de la compañía.</p>

<p><a href='#SITE#company/birthdays.php'>Cumpleaños</a> - para no olvidar ninguno de los cumpleaños de los colegas.</p>

<p><a href='#SITE#docs/'>Librería de documentos</a> - una colección pública de documentos de la empresa disponible para el equipo de trabajo. De ser necesario, la librería puede tener directorios privados que sólo podrán ser accesibles por miembror de ciertos departamentos.</p>

<p>Por favor lea el  <a href='#SITE#services/learning/' target='_blank'>Curso de Usuario del Poral</a> antes de que empiece a usarlo. Usted aprenderá acerca de todas las funciones de portal, su uso diario y sus características para trabajo en equipo.</p>
<p>Tenga un buen día!
</p>";
?>