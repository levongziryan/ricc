<?
$MESS["SERVICES_TITLE"] = "Comunicaciones unificadas";
$MESS["SERVICES_INFO"] = "<p>El portal cuenta con un servidor privado <b>XMPP/JABBER</b>! Mánténgase conectado y <b>en línea!, </b>intercambie mensajes y archivos, disfrute de chats de audio y video, todo desde su software movil o de mensajería favorito!</p>
<table cellspacing='1' cellpadding='1' border='0' width='100%'>
  <tbody>
    <tr>
      <td valign='top'><img hspace='10' height='15' width='12' src='#SITE#images/docs/cp/bullet-n.gif' />intercambio de mensajes (IM);
        <br />
       <img hspace='10' height='15' width='12' src='#SITE#images/docs/cp/bullet-n.gif' />intercambio de archivos;
        <br />
       <img hspace='10' height='15' width='12' src='#SITE#images/docs/cp/bullet-n.gif' />siempre en linea; <br />
       <img hspace='10' height='15' width='12' src='#SITE#images/docs/cp/bullet-n.gif' />soportado por la aplicaciones de mensajería más populares; <br />
       <img hspace='10' height='15' width='12' src='#SITE#images/docs/cp/bullet-n.gif' />integración video chat VoIP; <br />
       <img hspace='10' height='15' width='12' src='#SITE#images/docs/cp/bullet-n.gif' />poral móvil; <br />
       <img hspace='10' height='15' width='12' src='#SITE#images/docs/cp/bullet-n.gif' />listas de contactos pre creadas; <br />
       <img hspace='10' height='15' width='12' src='#SITE#images/docs/cp/bullet-n.gif' />historial de mensaría común; <br />
       <img hspace='10' height='15' width='12' src='#SITE#images/docs/cp/bullet-n.gif' />Soporte SSL. <br />

        <br />
      </td><td>
        <br />
       </td><td valign='top'>
        <br />
       <img height='63' width='130' src='#SITE#images/docs/cp/m.jpg' />
        <br /><br />
        <a href='http://www.bitrixsoft.com/download/Miranda_IM.zip'>Descargue Miranda!</a>
        <br />

        <br />
       </td></tr>
  </tbody>
</table>

<br />

<h2>Servidor privado <b>XMPP</b> </h2>
<p>El <strong>servidor privado XMPP</strong> fue creado para resolver un problema esencial encontrado en cualquier negocio: la creación de un <strong>sistema de mensajería segura</strong>. El servidor privado permite evitar el uso del inseguro ICQ u otros servicios de mensajería pública, manteniendo la herramienta que usted está acostumbrado a usar. El servidor de Portal se ejecuta sobre el protocolo <strong>Jabber</strong>, el mismo que es soportado por la mayoría y más populares aplicaciones de mensajería. Efectivamente, esto significa que usted puede comunicarse con sus colegas usando sus aplicaciones favoritas: <strong>Miranda, Pidgin, Psi </strong>y otras.</p>
 <p>Además, usted no sólo puede enviar y recibir mensajes de texto, sino también mensajes de <strong>archivo, audio y video</strong>. ¿Aún quiere más? No hay problema - <strong>tendrá su móvil</strong> enviando y recibiendo mensajes. El Portal es ahora su punto universal de intercambio de comunicación!</p>
<h2>¿Cómo trabaja?</h2>
El <strong>servidor privado XMPP</strong> es una aplicación completamente funcional, y estable.
<br />

<br />

<table cellspacing='0' cellpadding='0' border='0' width='100%'>
  <tbody>
    <tr> </tr>

    <tr><td valign='top'>

        <br />
                                          </td><td>
        <br />
       </td><td valig='top'><p>Además de los servicios de mensajería, este servidor permite, a su portal, administrar la indicación del estado de los usuarios en línea. Sus empleados podrán ahora estar siempre en línea donde quiera que estén - la gama de plataformas de servidor XMPP soportada varía desde servidores Unix hasta dispositivos móviles. Sólo tiene que instalar un mensajero habilitado para Jabber en su móvil y <strong>estar en línea</strong>!</p>
         <p>Para las aplicaciones de mensajería más comunes (Miranda, Psi, etc.) <a name='contacts'></a> no tiene que crear listas de contactos manualmente. Las listas se generan automáticamente durante la instalación. La lista de contactos resultante contiene la estructura de la empresa cuyos usuarios se agrupan según los departamentos de la misma. Si su cliente de mensajería es compatible con grupos anidados, los grupos de la lista de contactos de mensajería reflejarán con precisión la estructura del departamento.</p></td><td>
        <br />
       </td></tr>
  </tbody>
</table>

<br />

<table cellspacing='1' cellpadding='1' border='0' width='100%'>
  <tbody>
    <tr>
      <td valign='top'>Sea cual sea el mensajero que usted utiliza, el servidor siempre almacenará el historial de la conversación! Abra &quot;mis mensajes&quot; en su perfil, y verá el registro completo de mensajes.<br />
				<h2>¿Qué Messenger debería elegir?</h2>
				<p>¿Cuál es el cliente de mensajería que funciona mejor? Cualquiera que soporte el protocolo XMPP. Hemos probado varios mensajeros y puedo recomendar probar <a href='http://www.bitrixsoft.com/download/Miranda_IM.zip'>Miranda IM</a>. Encontrará las instrucciones de configuración de mensajería en el <a href=\"http://bitrix.es/elearning/cursos/course11/lesson554/?LESSON_PATH=761.916.554\" title=\"Configuración del Messenger Instantáneo\" target=\"_blank\">Curso de Principiantes del Portal Intranet</a>.<i>

          <br />
                </i>                </p>
<h2><a name='Miranda'></a>Miranda, por favor!</h2>

        <p class='a'>Los únicos parámetros que tiene que saber para configurar Miranda son su nombre de usuario,  contraseña (los mismos que utiliza para iniciar sesión en el Portal), el nombre de dominio del portal y el número de puerto. Consulte a su administrador del sistema acerca de estas opciones y consulte si su servidor utiliza SSL. Introduzca los ajustes en el cuadro de diálogo de Opciones de Miranda. Haga clic en la imagen de abajo para ver un ejemplo.</p>
      </td><td>
        <br />
       </td><td valign='top'>
        <a href='javascript:ShowImg('#SITE#images/docs/cp/im2.png', 266, 405, 'Company contact list')'>
        <img src='#SITE#images/docs/cp/im2-s.png' width='100' height='152' border='0' alt='Lista de contactos de la compañía' title='Lista de contactos de la compañía' />
        </a>
        <br />

        <br />
       </td></tr>
  </tbody>
</table>

<br />

<table cellspacing='0' cellpadding='0' border='0' width='100%' style='border-collapse: collapse;'>
  <tbody>


    <tr><td valign='top'>


    <a href='javascript:ShowImg('#SITE#images/docs/cp/cp8_miranda.png', 648, 472, 'Miranda settings')'>
    <img src='#SITE#images/docs/cp/cp8_miranda-s.png' border='0' width='100' height='73' alt='Configuración de Miranda' title='Configuración de Miranda' />
    </a>
        <br />
       </td><td valign='top'> <span class='text'>
          <ul>
            <li>ingrese su Nombre de usuario y contrase en los campor provistos para estos datos;</li>

            <li>tipee el dominio de su servidor en el campo <strong>Server</strong>;</li>

            <li>consulte a su administrador del Portal sobre el puero XMPP del servidor. Tipee el puero en el campo correspondiente;</li>

            <li>pregunt a su administrador si SSl es usado por el servidor. Si es así hag check sobre <b>SSL</b>.</li>

            <li>Deje los otros campos tal y como están. Haga click en <b>OK </b>para guardar los campos.</li>


        </ul>
         </span></td></tr>
  </tbody>
</table>


<p>Si usted está acostumbrado a alguna otra aplicación de mensajería, usted no tiene que cambiarse a Miranda. Encontrará opciones de configuración similares en la ventana de configuración de la otra aplicación y deberá proporcionar los mismos datos que le indicamos para Miranda. Nos vemos en el Portal!</p>";
?>