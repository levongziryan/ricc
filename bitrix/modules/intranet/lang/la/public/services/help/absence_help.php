<?
$MESS["SERVICES_TITLE"] = "Planificación de los Eventos";
$MESS["SERVICES_INFO"] = "<p>¿En la planificación de una reunión usted se rompe la cabeza definiendo el momento más adeuado para usted y sus colegas? El Portal de Bitrix24 es su mejor opción en este problema! Cualquier empleado puede ser invitado siempre y cuando su estado en el sistema no sea Fuera de la oficina: </p>
<ul>
  <li>Empiece por crear un nuevo calendario de eventos. Click en <b>Agregar</b> y seleccione el <strong>programador de eventos</strong>:
<p><img height='236' border='0' width='328' src='#SITE#images/docs/event_plan1.png' /></p>

<p>Esto abrirá el formulario de programación de eventos:</p>

<p><img height='496' border='0' width='704' src='#SITE#images/docs/event_plan.png' /></p>

</li>

<li>Use el link <strong>Agregar asistentes</strong> para seleccionar a las personas a quienes usted desea invitar a su renión.</li>
</ul>
<p>Use el planificador para encontrar el mejor lugar y tiempo para reunirse. Esto le ayudará a comprobar si las personas seleccionadas están disponibles en fecha y hora fijada para la reunión propuesta, y le notificará si algunos de ellos no pueden participar en la reunión: el período de tiempo, durante el cual, un empleado no está disponible aparecerá resaltado.<br />
¿Qué sucede si, usted está tan ocupado, que no puede asistir a la reunión? ? Comunicar a las otras personas sobre tal infortunio, es un tema muy sencillo: cree el evento en su agenda personal, ocupando el tiempo en el que estará ocupado o fuera del aoficina y listo, sus colegas serán notificados de su estatus no disponible, además el evento será visible publicamente desde el Gráfico de Ausencias. Si ya tiene este tipo de eventos, cuyo calendario se cruza con el momento de la reunión, sólo tiene que seleccionar Fuera de la oficina (Agregar al Gráfico Ausencias) en el campo de disponibilidad del usuario, lo que en última instancia va a producir el mismo resultado.</p>
<p><b>Ejemplo:</b>
</p>
<ul>
  <li>abra su calendario personal;</li>

          <li>cree un nuevo evento y seleccione <strong>fuera de la oficina</strong> en el campo de <strong>disponibilidad del usuario</strong>;
<p><img height='387' border='0' width='532' src='#SITE#images/docs/new_personal_absence.png' /></p>
</li>

          <li>guarde los cambios.</li>
</ul>



<p>Ahora, usted puede ver el <strong>Gráfico de Ausencias</strong> y este mostrarña su estatus fuera de la oficina (en la viñeta <strong>Mes</strong>).</p>

<p><img height='294' border='0' width='853' src='#SITE#images/docs/graph.png' /></p>

<p>Si usted está planeando solicitar una licencia, un evento público o una visita a su médico, debe reflejar su actividad en el calendario. Para usted, esto es, sin duda la mejor manera de evitar problemas. Para la empresa, es muy conveniente para racionalizar la planificación de eventos y simplificar el proceso de control personal. Haga su vida más cómoda.</p>
<p>Tenga en cuenta que sus salidas regulares y vacaciones están bajo la supervisión del departamento de recursos humanos. Los gerentes de recursos humanos reflejarán sus permisos en su calendario una vez que sea aprobado por su jefe.
</p>";
?>