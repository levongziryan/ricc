<?
$MESS["SERVICES_TITLE"] = "Subcripciones";
$MESS["SERVICES_INFO"] = "Usted puede tener notas de prensa entregadas a través de e-mail. Revise las categorías de noticias que desea recibir y haga clic en <i>Suscríbete</i>.";
?>