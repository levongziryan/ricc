<?
$MESS["ABOUT_TITLE"] = "Oportunidades de Empleo";
$MESS["ABOUT_DET_PAGE_TITLE"] = "Página";
$MESS["ABOUT_PAGE_TITLE"] = "Vacantes";
$MESS["ABOUT_INFO"] = "<p>Una vez que haya visto una vacante en la que usted este interesado en solicitar, por favor complete el <a href=\"resume.php\" >formulario de solicitud de empleo</a>, o envie su CV a <a href=\"mailto:hr@example.com\" >hr@example.com</a>. Any information you provide will remain confidential.</p>

<p>Si tiene alguna pregunta, por favor no dude en ponerse en contacto con el Departamento de Recursos Humanos:</p>

<p><a href=\"mailto:hr@example.com\" >hr@example.com</a></p>";
?>