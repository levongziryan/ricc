<?
$MESS["ABOUT_MENU_OFFICIAL"] = "Información Oficial";
$MESS["ABOUT_MENU_CALENDAR"] = "Calendario de Eventos";
$MESS["ABOUT_MENU_LIFE"] = "Nuestra Vida";
$MESS["ABOUT_MENU_ABOUT"] = "Acerca de la Compañía";
$MESS["ABOUT_MENU_PHOTO"] = "Galería de Fotos";
$MESS["ABOUT_MENU_VIDEO"] = "Vídeo";
$MESS["ABOUT_MENU_CAREER"] = "Profesión";
$MESS["ABOUT_MENU_NEWS"] = "Noticias de la Compañía (RSS)";
?>