<?
$MESS["ABOUT_TITLE"] = "Administración";
$MESS["ABOUT_TOP1_INFO"] = "<p></p>
<h2>Atkinson Morgan</h2>
<p><b>Director ejecutivo </b></p>
<p>Morgan fue nombrado en junio de 2004. Antes de 2004 trabajó en una serie de cadenas de supermercados, como ejecutivo. En 1999, se unió a Non-Global Food (ahora fusionada) e inmediatamente implementó una estrategia global. También trabajó en cuestiones de política relativas a las finanzas de alimentos, la propiedad de la tienda y la Norma de Calidad de Alimentos antes de unirse a nosotros en 2003.</p>";
$MESS["ABOUT_TOP2_INFO"] = "<p> </ p>
<h2> Milburn Leo </h2>
<p> <b> Director de Inversión</b></p>
<p> Leo ha trabajado en el sector de la producción de alimentos y en una variedad de posiciones en Comunidades del Alimento. Él era director de operaciones y luego director de área (1997-2004), liderando la oferta pública en la bolsa de París. Como Jefe de Inversiones, Bertram supervisó la expansión del programa de inversión en alimentos asequibles y la introducción de la equidad común asistiendo a compradores primerizos. </p>";
$MESS["ABOUT_TOP3_INFO"] = "<p></p>
<h2>Tharp Andrew</h2>
<p><b>Director de Regulaciones e Inspectoría</b></p>
<p>Andrew fue nombrado director del Reglamento e Inspecciones en junio de 2003. Andrew ha trabajado en la industria alimentaria durante los últimos 10 años, pasando de la generación de políticas y planificación a la regulación de la producción de alimentos e inspecciones.</p>
<p>En este rol, está desarrollando un nuevo marco para evaluar el asegurar el desempeño Global Food.</p>";
$MESS["ABOUT_TOP1_IMG"] = "#SITE#images/en/company/about/management/bandurin.jpg";
$MESS["ABOUT_TOP2_IMG"] = "#SITE#images/en/company/about/management/pankrashev.jpg";
$MESS["ABOUT_TOP3_IMG"] = "#SITE#images/en/company/about/management/porshnev.jpg";
?>