<?
$MESS["ABOUT_TITLE"] = "Cultura Corporativa";
$MESS["ABOUT_INFO1"] = "<p>Nuestra cultura corporativa es la fuente de los resultados que producimos.
Es lo que somos como organización, y el contexto en el que nos movemos.
Estamos comprometidos con la potenciar e inspirar a cada uno de nuestros miembros de equipo a tener avances y lograr resultados extraordinarios tanto internamente como con nuestros socios comerciales.</P>
<p>concienzudamente Trabajamos en equipo para crear esta cultura corporativa, un envirnoment de trabajo que anima a tomar la responsabilidades y resultados en:</p>
<p><b>Capacitar uno al otro </b></p>
<p>Estamos comprometidos con el éxito de los demás mediante la mejora y el fortalecimiento de la capacidad de cada uno para lograr avances y resultados extraordinarios.</p>";
$MESS["ABOUT_INFO2"] = "<p><b>Asumir la responsabilidad</b></p>
<p>Destacando la creatividad y la producción total hace que sea más fácil para los empleados tomar tanta responsabilidad como sea posible y limita el miedo al fracaso.</p>
<p><b>La comunicación abierta, honesta y completa</b></p>
<p>Hacemos todo lo posible para que la comunicación abierta dentro y fuera de la empresa, elimine barreras y cree oportunidades para una nueva cooperación y eficiencia.</p>
<p>Nuestra cultura corporativa cultiva un ambiente para todos los miembros del equipo y crea valor de manera consistente para nuestra línea de fondo,
nuestra cartera de empresas y, en última instancia, para sí mismos. También estamos comprometidos a inspirar y autorizar a las empresas en las que invertir tiene resultados importantes.</P>";
?>