<?
$MESS["ABOUT_TITLE"] = "Contactos";
$MESS["ABOUT_INFO"] = "<p><b>Sede Corporativa:</b></p>
 <dl> <dd><b>Compañía</b>, Inc.</dd><dt>
    <br />
  </dt> <dd>*** Pine Avenue,</dd><dt>
    <br />
  </dt> <dd>Long Beach, CA 90802</dd><dt>
    <br />
  </dt> <dd>USA</dd><dt>
    <br />
  </dt> </dl>
<p><b>Horario de Oficina:</b></p>

<p>Lunes - Viernes (Cerrado durante días fetivos), 8 AM - 5 PM GMT - 8:00.</p>

<p><b>Véase el esquema de cómo llegar a la oficina:</b></p>

<p><img height='449' width='500' src='#SITE#images/company/about/address_1.png' /></p>

<p><b>Oficina en Minnesota:</b></p>
 <dl> <dd>*** Gilbert Building</dd><dt>
    <br />
  </dt> <dd>*** Wacouta Street</dd><dt>
    <br />
  </dt> <dd>St. Paul, MN 55101</dd><dt>
    <br />
  </dt> <dd>USA</dd><dt>
    <br />
  </dt> </dl>
<p><b>Véase el esquema de cómo llegar a la oficina:</b></p>

<p><img height='418' width='490' src='#SITE#images/company/about/address_2.png' /></p>";
?>