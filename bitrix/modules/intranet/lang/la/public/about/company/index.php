<?
$MESS["ABOUT_TITLE"] = "Actualidades de la Compañía";
$MESS["ABOUT_INFO"] = "<p>Con el establecimiento de las tiendas Global Food en la década de 1990, hemos sentado las bases de una estrategia coherente de marca comercial. Nuestros clientes pueden estar seguros de contar con la mejor selección y ofertas posible a su disposición.</p>
<p>Los gerentes de tienda individuales deciden sobre sus necesidades de publicidad locales, la selección de productos que ofrecen, los precios y la planificación del personal. Esto les permite responder con flexibilidad a las necesidades particulares de sus clientes.</p>
<p>Global Food goza de un crecimiento constante y se ha convertido en el principal minorista de comestibles en Europa. Actualmente hay más de 1.200 tiendas en 10 países, lo que generó ventas netas de €27,1 mil millones en 2007. El grupo empresarial emplea actualmente a más de 65.000 personas - más de 1.500 de las cuales son empleados en la sede del grupo.</p>
<p>Los clientes siempre pueden esperar encontrar una amplia gama de productos en <br />
  Global Food. Dependiendo del tamaño y tipo, los puntos de venta ofrecen hasta 20.000 productos diferentes de alimentos. Global Food mantiene un amplio stock de todos los productos de su gama, una variedad de opciones de empaquetado, precios competitivos y calidad sin compromisos.<br />

<br />
 <b>Estándard de Locales y specifiaciones de estructura de tienda</b><br />

<br />
 Global Food opera en varios países con tres formatos de tienda. El área de ventas de una tienda es comúnmente de 10.000 a 16.000 metros cuadrados. Este formato se utiliza principalmente en los países de Europa Occidental. En Europa del Este, se contruyen tiendas medianas genralmente (7000 a 9000 metros cuadrados), mientras que, las tiendas pequeñas tienen áreas de venta de entre 2.500 y 4.000 metros cuadrados.
<br />

<br />
<b>Adaptación a necesidades globales</b><br />

<br />
Una característica especial del concepto de ventas de Global Food, es que la gama de productos de cada tienda se adapta a las necesidades locales. Los productos en nuestros estantes y unidades de refrigeración son cuidadosamente seleccionados de acuerdo con el comportamiento y la demanda respectiva al consumidor de cada región. Aproximadamente el 90 por ciento de la mercancía en nuestras tiendas se compra a los productores proveedores locales.</p>
<p>La compañía ayuda a productores y proveedores en el desarrollo de modernos métodos de cultivo, procesamiento y distribución. La política de adquisición local de Global Food apoya las economías regionales - por ejemplo, mediante la generación de nuevas oportunidades de negocio para fabricantes y agricultores. Además de atender las necesidades especiales con una amplia selección de productos internacionales, como la oferta de alimentos turcos en Alemania</p>
<h2>Datos sobre Global Food</h2>
 Estados financieros en millones de euros<br />

<br />
<img width='217' height='193' src='#SITE#images/company/about/facts.png' />
<br />

<h2>Compromiso Social</h2>
<p>  Las empresas y entornos son parte de la comunidad. Para tener éxito, ellos deben confíar en su entorno. Esto incluye el buen funcionamiento de la infraestructura y de las instituciones, un buen sistema educativo, y atractivas oportunidades culturales y de ocio para los propios empleados de la empresa. Al participar en la comunidad, las empresas pueden contribuir al fortalecimiento de la eficiencia y el atractivo de su entorno. Al mismo tiempo, indican su voluntad de asumir la responsabilidad de la sociedad en su conjunto.</p>
<p>Global Food, siempre muy cercano a clientes, proveedores y empleados, históricamente se ha caracterizado por su comprensión a lo que responsabilidad social se refiere. Queremos que las personas, en los mercados en los que operamos, puedan obtener un beneficio tangible de nuestras actividades. Al igual que nuestras actividades comerciales están orientadas a generar un crecimiento rentable, nuestro compromiso social tiene como objetivo crear estructuras sostenibles alrededor de las tiendas y oficinas de nuestra empresa. Aportamos nuestra competencia y experiencia en proyectos comunitarios e iniciativas que podemos reconciliar de manera significativa y creíble con nuestro negocio principal.</p>";
?>