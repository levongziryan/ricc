<?
$MESS["ABOUT_TITLE"] = "Misión y Estrategia";
$MESS["ABOUT_INFO"] = "<br />
El objetivo de nuestra estrategia corporativa es incrementar el valor de Global Food en el largo plazo<br />

<br />
Hoy, que ya generamos casi el 50 por ciento del total de nuestras ventas en el exterior. Nuestras actividades de expansión internacional se centran en los mercados emergentes de Europa del Este y Asia. Acelerada expansión internacional y el desarrollo de nuestra red de ventas son la piedra angular de nuestro crecimiento rentable. Aquí, ponemos énfasis en el éxito, no el tamaño. Esto significa que el retorno es más importante para nosotros que el crecimiento del volumen simple.<br />

<br />
Nuestro objetivo es ofrecer, a los inversores, un rendimiento óptimo en su inversión. Vamos a desarrollar aún más nuestra cartera de empresas sobre una base a largo plazo utilizando un enfoque de valor-orientado. Para aumentar continuamente el valor de nuestras marcas de ventas, nos enfocamos nuestras actividades de negocios y la asignación de capital exclusivamente en los segmentos rentables y sostenibles en el mercado.
<br />

<br />
El programa de cumplimiento estandarizado de todo el Grupo se introdujo en 2008 combina y complementa las medidas existentes para garantizar el cumplimiento de estas normas. El objetivo del programa es apoyar el cumplimiento de todos los empleados de Global Food en la observación de las leyes, reglamentos y códigos de conducta.";
?>