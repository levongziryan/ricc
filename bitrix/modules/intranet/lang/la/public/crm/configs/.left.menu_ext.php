<?
$MESS["CRM_MENU_STATUS"] = "Listas de selección";
$MESS["CRM_MENU_CURRENCY"] = "Monedas";
$MESS["CRM_MENU_TAX"] = "Impuestos";
$MESS["CRM_MENU_LOCATIONS"] = "Ubicación";
$MESS["CRM_MENU_PS"] = "Formas de pago";
$MESS["CRM_MENU_PERMS"] = "Permisos de Acceso";
$MESS["CRM_MENU_BP"] = "Business processes";
$MESS["CRM_MENU_FIELDS"] = "Campos personalizados";
$MESS["CRM_MENU_PRODUCT_PROPS"] = "Características del Producto";
$MESS["CRM_MENU_CONFIG"] = "Otras configuraciones";
$MESS["CRM_MENU_SENDSAVE"] = "Integración de E-mail";
$MESS["CRM_MENU_SALE"] = "e-Stores";
$MESS["CRM_MENU_MEASURE"] = "Unidades de medida";
$MESS["CRM_MENU_MAILTEMPLATE"] = "Plantillas de E-mail";
$MESS["CRM_MENU_INFO"] = "Ayuda";
$MESS["CRM_MENU_PRODUCTPROPS"] = "Características del Producto";
$MESS["CRM_MENU_PRESET"] = "Plantillas de datos del cliente";
$MESS["CRM_MENU_EXCH1C"] = "Intergración; 1C:Enterprise";
$MESS["CRM_MENU_MYCOMPANY"] = "Detalles de la compañía";
?>