<?
$MESS["CRM_TITLE"] = "Ayuda";
$MESS["CRM_INFO"] = "<h3>¿Qué es CRM?</h3>
<p><b>Administrador de relaciones con el cliente ó </b>(<b>CRM por sus siglas en inglés</b>) es un sistema que apunta incrementar las ventas, optimizar los esfuerzos de marketing y mejorar el servicio al cliente. EL CRM almacena los datos de los clientes y la relación hitórica que ha habido con ellos, para futuros análisis.</p>
<p><span id=\"result_box\" lang=\"es\" xml:lang=\"es\">Usando el CRM, un ejecutivo atiende a un cliente potencial durante todo el proceso de ventas, desde la etapa de prospecto hasta que cerrar negociaciónes exitosas con él.</span></p>
<p>Los siguientes términos son comunes cuando hablamos de CRM.&nbsp;</p>
<ul>
	<li><b>Contacto</b>: un registro contiene los datos de una persona con quien los ejecutivos de la compañía han tenido algún tipo de comunicación respecto al neogocio. </li>
	<li><b>Compañía</b>: un registro contiene información con datos de una compañía con la que se tiene o tuvo) alguna forma de relación de negocios. </li>
	<li><b>Prospecto</b>: información sobre cualquier tipo de comunicación o actividad (llamadas telefónicas, e-mails, reuniones, etc.) que son potencialmente negocios para nuestra compañía. </li>
	<li><b>Evento</b>: detalla cualquier cambio aplicado a un contacto, prospecto o compañía. Por ejemplo la adición de una nueva direcciónd e e-mail. </li>
	<li><b>Negociación</b>: un registro contiene datos acerca de las actividades destinadas a culminar una transacción de negocio deseada con un cliente (por ejemplo una venta). </li>
</ul>
<h3>¿Cómo trabaja?</h3>
<p>El CRM puede ser usado:</p>
<ol>
	<li>Como una <strong>base de datos</strong> de compañías y contactos,</li>

	<li>Como un <b>CRM </b>clásico.</li>
</ol>
<h4>1. CRM como una base de datos</h4>
<p> El CRM se puede utilizar como una base de datos destinada a almacenar los datos de contactos y empresas, manteniendo un historial de las relaciones. En este caso, las entidades principales son los contactos y las empresa sobre las cuales la interacción se definen mediante la creación de eventos. Tenga en cuenta que, incluso si usted utiliza su CRM sólo como una base de datos, aún es posible derivar un prospecto como resultado de un evento, y finalmente convertirlo en una negociación.</p>
<p><img height='430' border='0' width='900' src='/upload/crm/cim/01.png'  /></p>
<h4>2. CRM clásico</h4>
<p>En un sistema CRM clásico, la entidad de origen es un prospecto que se puede ser agregado manualmente por un administrador, desde Bitrix Site Manager u otra fuente. Una vez añadido, el prospecto se puede convertir en una negociación o en una empresa representada por una entrada de base de datos común. Sin embargo, si un prospecto pasa todas las etapas del embudo de ventas, entonces se convierte en un acuerdo.</p>
<p><img height='430' border='0' width='900' src='/upload/crm/cim/03.png'  /></p>";
?>