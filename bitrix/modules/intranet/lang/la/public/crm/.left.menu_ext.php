<?
$MESS["MENU_CRM_STREAM"] = "Flujo de Actividad";
$MESS["MENU_CRM_ACTIVITY"] = "Mis Actividades";
$MESS["MENU_CRM_CONTACT"] = "Contactos";
$MESS["MENU_CRM_COMPANY"] = "Compañías";
$MESS["MENU_CRM_DEAL"] = "Negociaciones";
$MESS["MENU_CRM_INVOICE"] = "Facturas";
$MESS["MENU_CRM_LEAD"] = "Prospectos";
$MESS["MENU_CRM_PRODUCT"] = "Catálogo";
$MESS["MENU_CRM_HISTORY"] = "Historial";
$MESS["MENU_CRM_REPORT"] = "Reportes";
$MESS["MENU_CRM_FUNNEL"] = "Embudo de Ventas";
$MESS["MENU_CRM_SETTINGS"] = "Configuración";
$MESS["MENU_CRM_DESKTOP"] = "Escritorio CRM";
$MESS["MENU_CRM_QUOTE"] = "Cotizaciones";
$MESS["MENU_CRM_WEBFORM"] = "Formularios del CRM";
$MESS["MENU_CRM_BUTTON"] = "Widget del sitio web";
?>