<?
$MESS["COMPANY_MENU_EMPLOYEES"] = "Buscar Empleado";
$MESS["COMPANY_MENU_TELEPHONES"] = "Directorio Telefónico";
$MESS["COMPANY_MENU_STRUCTURE"] = "Estructura de la Compañía";
$MESS["COMPANY_MENU_EVENTS"] = "Cambios en el Personal";
$MESS["COMPANY_MENU_ABSENCE"] = "Gráfico de Ausencias";
$MESS["COMPANY_MENU_TIMEMAN"] = "Control de Tiempo";
$MESS["COMPANY_MENU_WORKREPORT"] = "Reportes";
$MESS["COMPANY_MENU_REPORT"] = "Reporte de Eficiencia";
$MESS["COMPANY_MENU_LEADERS"] = "Empleados Honorables";
$MESS["COMPANY_MENU_BIRTHDAYS"] = "Cumpleaños";
$MESS["COMPANY_MENU_GALLERY"] = "Fotos Compartidas";
$MESS["COMPANY_MENU_MY_PROCESSES"] = "Mis Solicitudes";
?>