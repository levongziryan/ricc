<?
$MESS["TOP_MENU_FAVORITE"] = "Mi Área de Trabajo";
$MESS["TOP_MENU_GROUPS"] = "Grupos de Trabajo";
$MESS["TOP_MENU_GROUPS_EXTRANET"] = "Grupos de Trabajo de la Extranet";
$MESS["TOP_MENU_DEPARTMENTS"] = "Departamentos";
$MESS["TOP_MENU_TELEPHONY"] = "Telefonía";
$MESS["TOP_MENU_MARKETPLACE"] = "Aplicaciones";
$MESS["TOP_MENU_ONEC"] = "1C + Bitrix24 CRM";
$MESS["TOP_MENU_OPENLINES"] = "Canales Abiertos";
$MESS["TOP_MENU_LIVE_FEED"] = "Flujo de Actividad";
$MESS["TOP_MENU_TASKS"] = "Tareas";
$MESS["TOP_MENU_CALENDAR"] = "Calendario";
$MESS["TOP_MENU_DISK"] = "Drive";
$MESS["TOP_MENU_PHOTO"] = "Fotos";
$MESS["TOP_MENU_BLOG"] = "Conversaciones";
$MESS["TOP_MENU_MAIL"] = "Mail";
$MESS["TOP_MENU_CRM"] = "CRM";
$MESS["TOP_MENU_BIZPROC"] = "Flujo de Trabajo";
$MESS["TOP_MENU_TIMEMAN"] = "Tiempo y Reportes";
$MESS["TOP_MENU_IM_MESSENGER"] = "Chat y Llamadas";
$MESS["TOP_MENU_COMPANY"] = "Empleados";
$MESS["TOP_MENU_CONFIGS"] = "Configuraciones";
?>