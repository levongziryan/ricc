<?
$MESS["LEFT_MENU_LIVE_FEED"] = "Flujo de actividad";
$MESS["LEFT_MENU_IM_MESSENGER"] = "Chat y Llamadas";
$MESS["LEFT_MENU_TASKS"] = "Tareas";
$MESS["LEFT_MENU_CALENDAR"] = "Calendario";
$MESS["LEFT_MENU_DISC"] = "My Drive";
$MESS["LEFT_MENU_PHOTO"] = "Mis Fotos";
$MESS["LEFT_MENU_BLOG"] = "Conversaciones";
$MESS["LEFT_MENU_MAIL"] = "Correo";
$MESS["LEFT_MENU_MAIL_SETTING"] = "Editar configuración";
$MESS["LEFT_MENU_BP"] = "Flujo de trabajo";
$MESS["LEFT_MENU_CRM"] = "CRM";
$MESS["LEFT_MENU_MY_PROCESS"] = "Mis Solicitudes";
$MESS["TOP_MENU_GROUPS"] = "Grupos";
$MESS["TOP_MENU_GROUPS_EXTRANET"] = "Grupos de Extranet";
$MESS["TOP_MENU_DEPARTMENTS"] = "Departamentos";
$MESS["TOP_MENU_TELEPHONY"] = "Telefonía";
$MESS["TOP_MENU_MARKETPLACE"] = "Aplicaciones";
$MESS["TOP_MENU_OPENLINES"] = "Canal Abierto";
$MESS["TOP_MENU_COMPANY"] = "Compañía";
$MESS["TOP_MENU_EMPLOYEES"] = "Empleados";
$MESS["TOP_MENU_DISCS"] = "Documentos";
$MESS["TOP_MENU_SERVICES"] = "Servicios";
?>