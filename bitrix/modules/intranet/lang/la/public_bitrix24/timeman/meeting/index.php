<?
$MESS["TITLE"] = "Reuniones y Sesiones Informativas";
$MESS["TARIFF_RESTRICTION_TEXT"] = "Bitrix24 puede ayudarlo a planificar y organizar eventos y reuniones con un planificador especial de reuniones y briefings. Las tareas serán asignadas a las personas designadas, los debates y los procedimientos registrados, las personas responsables se les pedirá que creen un informe para sus respectivos temas de la agenda.
<a href=\"https://helpdesk.bitrix24.com/open/1343758/\">Detalles</a>";
$MESS["TARIFF_RESTRICTION_TEXT2"] = "Las reuniones y reuniones informativas solo están disponibles en el plan Professional.";
?>