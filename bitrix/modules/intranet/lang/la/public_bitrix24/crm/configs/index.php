<?
$MESS["TITLE"] = "Ajustes";
$MESS["STATUS"] = "Listas de selección";
$MESS["PERMS"] = "Permisos de acceso";
$MESS["BP"] = "Business processes";
$MESS["FIELDS"] = "Campos personalizados";
$MESS["CONFIG"] = "Otros ajustes";
$MESS["SENDSAVE"] = "Integración de correo electrónico";
$MESS["CURRENCY"] = "Monedas";
$MESS["EXTERNAL_SALE"] = "Tiendas web";
$MESS["EXCH1C"] = "1C integración";
$MESS["MAIL_TEMPLATES"] = "Plantillas de correo electrónico";
$MESS["TAX"] = "Impuestos";
$MESS["PS"] = "Pago y facturas";
$MESS["LOCATIONS"] = "Ubicaciones";
$MESS["MEASURE"] = "Unidades de medida";
$MESS["PRODUCT_PROPS"] = "Propiedades del producto";
$MESS["SLOT"] = "Reportes Analíticos";
?>