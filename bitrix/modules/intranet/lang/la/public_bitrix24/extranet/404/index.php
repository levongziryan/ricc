<?
$MESS["EXTRANET_404_TITLE"] = "No se han encontrado entradas.";
$MESS["EXTRANET_404_TEXT"] = "<p>No se puede acceder a la Extranet.</p>
<p>- Haga clic en \"Atrás\" en su navegador para llegar a la página anterior</p>
<p>- O abrir <a href=\"/\">Flujo de Actividad</a></p>";
?>