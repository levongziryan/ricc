<?
$MESS["ERROR_404_TITLE"] = "Error 404";
$MESS["ERROR_404_TEXT1"] = "Desafortunadamente la página que solicitó no fue encontrada.";
$MESS["ERROR_404_TEXT2"] = "Si cree que esto no debería haber ocurrido, póngase en contacto con el administrador del sitio web.";
?>