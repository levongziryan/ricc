<?
$MESS["MENU_TELEPHONY_BALANCE"] = "Balance y Estadísticas";
$MESS["MENU_TELEPHONY"] = "Configuración de Telefonía";
$MESS["MENU_TELEPHONY_PERMISSIONS"] = "Permisos de Acceso";
$MESS["MENU_TELEPHONY_LINES"] = "Números de Teléfono";
$MESS["MENU_TELEPHONY_CONNECT"] = "Conexión";
$MESS["MENU_TELEPHONY_USERS"] = "Usuarios de Telefonía";
$MESS["MENU_TELEPHONY_PHONES"] = "Teléfonos SIP";
$MESS["MENU_TELEPHONY_GROUPS"] = "Colas";
$MESS["MENU_TELEPHONY_IVR"] = "Configurar IVR";
?>