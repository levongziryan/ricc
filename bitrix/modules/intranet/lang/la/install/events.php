<?
$MESS["INTRANET_USER_INVITATION_NAME"] = "Invitar a la gente";
$MESS["INTRANET_USER_INVITATION_DESC"] = "#EMAIL_TO# - la dirección de correo electrónico de la persona invitada
#LINK# - Enlace de activación del empleado nuevo ";
$MESS["INTRANET_USER_INVITATION_SUBJECT"] = "Bienvenidos a la Intranet";
$MESS["INTRANET_USER_INVITATION_MESSAGE"] = "#USER_TEXT#

#LINK#

Utilice su dirección de correo electrónico como nombre de usuario para la autenticación. Usted tendrá que crear su contraseña personal cuando se realiza la autenticación por primera vez.";
$MESS["INTRANET_USER_ADD_NAME"] = "Agregar empleados";
$MESS["INTRANET_USER_ADD_DESC"] = "#EMAIL_TO# - dirección de e-mail del nuevo empleado
#LINK# -  URL intranet";
$MESS["INTRANET_USER_ADD_SUBJECT"] = "Usted ha sido agregado al sitio de intranet";
$MESS["INTRANET_USER_ADD_MESSAGE"] = "#USER_TEXT#

#LINK#

Para autorizarse, use su e-mail como login y la siguiente contraseña: #PASSWORD#";
$MESS["INTRANET_MAILDOMAIN_NOCOMPLETE_NAME"] = "Agregar y completar un dominio de correo electrónico";
$MESS["INTRANET_MAILDOMAIN_NOCOMPLETE_DESC"] = "#EMAIL_TO# - administración de E-Mail 
#LEARNMORE_LINK# - URL de la documentación
#SUPPORT_LINK# - URL de Soporte Técnico";
$MESS["INTRANET_MAILDOMAIN_NOCOMPLETE_SUBJECT"] = "No se ha completado la adición del dominio";
$MESS["INTRANET_MAILDOMAIN_NOCOMPLETE_MESSAGE"] = "<p>Nos dimos cuenta de que usted ha hecho un intento de crear un dominio de la empresa, pero nunca completo la inscripción.</p>

<p>Si usted necesita ayuda, abra la página de configuración de correo electrónico para obtener instrucciones detalladas para configurar el servicio de correo electrónico y completar su registro de dominio Bitrix24. </p>

<p>Si desea ver cómo se realiza por lo general, tenemos una serie de ejemplos de uso de dominios de correo electrónico para que usted vea. Los ejemplos revelarán la magia detrás de registro de dominio, la creación de un buzón de correo y el uso de todo el asunto con Bitrix24.</P>

<p><a href=\"#LEARNMORE_LINK#\">Más información</a></p>

<p>Si usted todavía tiene preguntas, por favor póngase en contacto con nuestro <a href=\"#SUPPORT_LINK#\">helpdesk</a>.</p>";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX_NAME"] = "Crear buzones de correo de dominio";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX_DESC"] = "#EMAIL_TO# - administración de E-Mail 
#LEARNMORE_LINK# - URL de la documentación
#SUPPORT_LINK# - URL de Soporte Técnico";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX_SUBJECT"] = "Su correo electrónico corporativo de Bitrix24";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX_MESSAGE"] = "<p>Su dominio ha sido agregado con éxito a Bitrix24, pero no existe el buzón de correo todavía.</p>

<p>Tenemos una serie de ejemplos de uso de dominios de correo electrónico para que usted vea. Con los ejemplos, usted aprenderá cómo crear un buzón de correo y como utilizarlo con Bitrix24.</P>

<p><a href=\"#LEARNMORE_LINK#\">Más información</a></p>

<p>Si usted todavía tiene preguntas, por favor póngase en contacto con nuestro <a href=\"#SUPPORT_LINK#\">helpdesk</a>.</p>";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_NAME"] = "Crear buzones de correo de los empleados en el dominio";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_DESC"] = "#EMAIL_TO# - administración de E-Mail 
#LEARNMORE_LINK# - URL de la documentación
#SUPPORT_LINK# - URL de Soporte Técnico";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_SUBJECT"] = "Correo electrónico corporativo para sus empleados";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_MESSAGE"] = "<p>Tiene registrado el dominio y creó el buzón de correo. Sin embargo, aún los empleados no tienen idea de que puede crear sus propios buzones en el dominio de la empresa.</p>

<p>Compartir sus conocimientos y ayudarles a aprender a crear sus propios buzones o registrar los buzones en la página de Bitrix24.</p>

<p><a href=\"#LEARNMORE_LINK#\">como registrar los buzones de correo para sus empleados</a></p>

<p>Si usted todavía tiene preguntas, por favor póngase en contacto con nuestro <a href=\"#SUPPORT_LINK#\">helpdesk</a>.</p> ";
$MESS["INTRANET_MAILDOMAIN_NOREG_NAME"] = "Registrar dominio de correo electrónico";
$MESS["INTRANET_MAILDOMAIN_NOREG_DESC"] = "#EMAIL_TO# - administración de E-Mail 
#LEARNMORE_LINK# - URL de la documentación
#SUPPORT_LINK# - URL de Soporte Técnico";
$MESS["INTRANET_MAILDOMAIN_NOREG_SUBJECT"] = "No ha completado la adición de su dominio";
$MESS["INTRANET_MAILDOMAIN_NOREG_MESSAGE"] = "<p>Usted va a registrar un dominio para su empresa, pero no podía escoger un buen nombre, por lo que se ve.</p>

<p> Tenemos una serie de ejemplos de uso de dominios de correo electrónico para que usted vea. Con los ejemplos, usted aprenderá cómo crear un buzón de correo y como utilizarlo con Bitrix24.</P>

<p><a href=\"#LEARNMORE_LINK#\">como registrar los buzones de correo para sus empleados</a></p>

<p> Si usted todavía tiene preguntas, por favor póngase en contacto con nuestro<a href=\"#SUPPORT_LINK#\">helpdesk</a>.</p>";
?>