<?
$MESS["INTR_MODULE_NAME"] = "Intranet";
$MESS["INTR_MODULE_DESCRIPTION"] = "Portal de Intranet";
$MESS["INTR_PHP_L439"] = "Usted está utilizando una versión de PHP #VERS#, pero el módulo requiere la versión 5.0.0 o superior. Por favor, actualice su instalación de PHP o contacte con el soporte técnico.";
$MESS["INTR_INSTALL_TITLE"] = "Instalación del Módulo";
$MESS["INTR_UNINSTALL_TITLE"] = "Desinstalación del Módulo de Intranet";
$MESS["INTR_INSTALL_RATING_RULE"] = "Calcular votos extras para la autorización basada en el estructura de la compañía";
?>