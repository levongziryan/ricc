<?
$MESS["BITRIX24_THEME_DIALOG_TITLE"] = "Temas visuales";
$MESS["BITRIX24_THEME_DIALOG_SAVE_BUTTON"] = "Guardar";
$MESS["BITRIX24_THEME_DIALOG_CANCEL_BUTTON"] = "Cancelar";
$MESS["BITRIX24_THEME_DIALOG_CREATE_BUTTON"] = "Crear";
$MESS["BITRIX24_THEME_DIALOG_NEW_THEME"] = "Tema personalizado";
$MESS["BITRIX24_THEME_CREATE_YOUR_OWN_THEME"] = "Crear tema personalizado";
$MESS["BITRIX24_THEME_THEME_BG_COLOR"] = "Color de fondo";
$MESS["BITRIX24_THEME_THEME_BG_IMAGE"] = "Imagen de fondo";
$MESS["BITRIX24_THEME_THEME_TEXT_COLOR"] = "Color de texto";
$MESS["BITRIX24_THEME_THEME_LIGHT_COLOR"] = "Ligero";
$MESS["BITRIX24_THEME_THEME_DARK_COLOR"] = "Oscuro";
$MESS["BITRIX24_THEME_UPLOAD_BG_IMAGE"] = "Cargar imagen de fondo";
$MESS["BITRIX24_THEME_DRAG_BG_IMAGE"] = "o arrastre una imagen aquí";
$MESS["BITRIX24_THEME_WRONG_FILE_TYPE"] = "El archivo no es una imagen.";
$MESS["BITRIX24_THEME_FILE_SIZE_EXCEEDED"] = "Se ha superado el tamaño máximo de archivo (#LIMIT#).";
$MESS["BITRIX24_THEME_WRONG_BG_COLOR"] = "Color de fondo incorrecto";
$MESS["BITRIX24_THEME_EMPTY_FORM_DATA"] = "Seleccione una imagen o color de fondo.";
$MESS["BITRIX24_THEME_UNKNOWN_ERROR"] = "No se puede completar la acción. Inténtalo de nuevo.";
$MESS["BITRIX24_THEME_SET_AS_DEFAULT"] = "Establecer por defecto";
$MESS["BITRIX24_THEME_DEFAULT_THEME"] = "Por defecto";
$MESS["BITRIX24_THEME_REMOVE_THEME"] = "Eliminar tema";
?>