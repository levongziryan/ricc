<?
$MESS["UF_1C"] = "Usuario desde 1C";
$MESS["UF_INN"] = "INN";
$MESS["UF_PHONE_INNER"] = "Número de extensión";
$MESS["UF_DISTRICT"] = "Distrito";
$MESS["UF_STATE_HISTORY"] = "Historial del estado";
$MESS["UF_STATE_LAST"] = "Último estado";
$MESS["UF_DEPARTMENT"] = "Departamentos";
$MESS["UF_SKYPE"] = "Skype";
$MESS["UF_TWITTER"] = "Twitter";
$MESS["UF_FACEBOOK"] = "Facebook";
$MESS["UF_LINKEDIN"] = "LinkedIn";
$MESS["UF_XING"] = "Xing";
$MESS["UF_WEB_SITES"] = "Otros sitios web";
$MESS["UF_SKILLS"] = "Aptitudes";
$MESS["UF_INTERESTS"] = "Intereses";
?>