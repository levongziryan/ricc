<?
$MESS["INTR_EMP_WINDOW_TITLE"] = "Seleccionar empleado";
$MESS["INTR_EMP_WINDOW_CLOSE"] = "Cerrar";
$MESS["INTR_EMP_WAIT"] = "Cargando...";
$MESS["INTR_EMP_SUBMIT"] = "Seleccionar";
$MESS["INTR_EMP_SUBMIT_TITLE"] = "Elegir al empleado seleccionado";
$MESS["INTR_EMP_CANCEL"] = "Cancelar";
$MESS["INTR_EMP_CANCEL_TITLE"] = "Cancelar Selección de empleado";
?>