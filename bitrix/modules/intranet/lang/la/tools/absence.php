<?
$MESS["INTR_ABSENCE_IBLOCK_MODULE"] = "No está instalado el módulo de blocks de información.";
$MESS["INTR_ADD_TITLE"] = "Agregar entrada";
$MESS["INTR_EDIT_TITLE"] = "Editar entrada de ausencia";
$MESS["INTR_ABSENCE_NAME"] = "Motivo de ausencia *";
$MESS["INTR_ABSENCE_NO_TYPE"] = "(no especificado)";
$MESS["INTR_ABSENCE_USER"] = "Seleccione persona ausente *";
$MESS["INTR_ABSENCE_TYPE"] = "Tipo de ausencia";
$MESS["INTR_ABSENCE_PERIOD"] = "Tiempo de ausencia";
$MESS["INTR_ABSENCE_ACTIVE_FROM"] = "Inicio:";
$MESS["INTR_ABSENCE_ACTIVE_TO"] = "Final:";
$MESS["INTR_ABSENCE_SUCCESS"] = "Una entrada de Ausencia se ha añadido.";
$MESS["INTR_ABSENCE_ADD"] = "Agregar";
$MESS["INTR_ABSENCE_EDIT"] = "Editar";
$MESS["INTR_ABSENCE_ADD_MORE"] = "Agregar más";
$MESS["INTR_USER_CHOOSE"] = "Seleccione desde la estructura";
$MESS["INTR_USER_ERR_NO_RIGHT"] = "No tiene accesos para realizar cambios";
$MESS["INTR_ABSENCE_FROM_TO_ERR"] = "La fecha de inicio debe ser anterior a la fecha de finalización.";
?>