<?
$MESS["INTR_STRUCTURE_IBLOCK_MODULE"] = "No está instalado el módulo de bloques de información.";
$MESS["INTR_EDIT_TITLE"] = "Editar departamento";
$MESS["INTR_ADD_TITLE"] = "Agregar departamento";
$MESS["INTR_STRUCTURE_NAME"] = "Nombre del departamento";
$MESS["INTR_STRUCTURE_HEAD"] = "Supervisor";
$MESS["INTR_STRUCTURE_DEPARTMENT"] = "Departamento padre";
$MESS["INTR_STRUCTURE_SUCCESS"] = "Se ha agregado el departamento.";
$MESS["INTR_STRUCTURE_ADD"] = "Agregar";
$MESS["INTR_STRUCTURE_ADD_MORE"] = "Agregar más";
$MESS["INTR_STRUCTURE_EDIT"] = "Guardar";
$MESS["INTR_UF_HEAD_CHOOSE"] = "Seleccione desde la estructura";
$MESS["INTR_USER_ERR_NO_RIGHT"] = "Permisos insuficientes para realizar cambios.";
?>