<?
$MESS["INTR_MAIL_INP_PUBLIC_DOMAIN"] = "Os usuários podem cadastrar caixas de correio neste domínio";
$MESS["INTR_MAIL_MANAGE"] = "Configurar caixas de correio do usuário";
$MESS["INTR_MAIL_DESCR_B24"] = "Servidor de e-mail gratuito para a sua empresa! Armazenamento ilimitado para seu e-mail, antivírus e antispam para manter sua caixa de entrada limpa. Migrar seu e-mail corporativo para o Bitrix24! Crie suas próprias caixas de correio no domínio empresarial no Bitrix24 ou anexe caixas de correio de outros serviços de e-mail.";
$MESS["INTR_MAIL_DESCR_BOX"] = "Pronto para usar o servidor de e-mail para a sua empresa. Desfrute de armazenamento ilimitado, antivírus e antispam. Crie caixas de correio no domínio corporativo ou anexe a serviços de e-mail existentes.";
$MESS["INTR_MAIL_DOMAIN_PUBLIC"] = "permite aos usuários cadastrarem caixas de correio no novo domínio";
$MESS["INTR_MAIL_NODOMAIN_USER_INFO"] = "Entre em contato com o administrador da intranet para anexar o domínio corporativo.";
$MESS["INTR_MAIL_MANAGE_HINT"] = "Crie uma caixa de correio para cada usuário na sua organização. Usar essa interface para gerenciar as caixas de correio corporativas: criar, anexar ou excluir caixas de correio, bem como alterar senhas.";
$MESS["INTR_MAIL_MANAGE_SETUP_ALLOW_CRM"] = "Membros podem conectar caixas de correio ao CRM CRM";
$MESS["INTRANET_OTP_MANDATORY_TITLE"] = "Segurança extra para seus dados de negócios";
$MESS["INTR_ABSC_TPL_EDIT_ENTRIES"] = "Gerenciamento de Usuários";
$MESS["ISL_TPL_NOTE_NULL"] = "Sua pesquisa não encontrou nenhum usuário.";
$MESS["INTR_ABSC_TPL_IMPORT"] = "Importar Usuários";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_OUTLOOK_TITLE"] = "Exportar Lista de Usuários como contatos do Outlook";
$MESS["INTR_COMP_IS_TPL_BANNER_OUTLOOK"] = "Você pode exportar usuários como contatos para o Microsoft Outlook";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_CARDDAV_TITLE"] = "Sincronizar registros de usuários com o software e o hardware compatível com CardDAV (iPhone, iPad, etc)";
$MESS["INTR_IS_TPL_SEARCH"] = "Encontrar Usuário";
$MESS["INTR_IS_TPL_SEARCH_DEPARTMENT"] = "Encontrar Usuário Nesta Divisão";
$MESS["INTR_IS_TPL_OUTLOOK"] = "Exportar Usuários para Outlook";
$MESS["INTR_IS_TPL_OUTLOOK_TITLE"] = "Exportar Lista de Usuários como contatos do Outlook";
$MESS["INTR_ISBN_TPL_FILTER_ALL"] = "Para toda a organização";
$MESS["INTR_ISIN_TPL_ALL"] = "Para toda a organização";
$MESS["INTR_ISL_TPL_NOTE_NULL"] = "Sua pesquisa não encontrou nenhum usuário.";
$MESS["ISV_ERROR_dpt_not_empty"] = "Você tem que realocar todos os usuários da divisão antes de excluir a divisão.";
$MESS["ISV_EMP_LIST"] = "Lista de usuários";
$MESS["ISV_confirm_delete_department"] = "Tem certeza de que deseja excluir a divisão? Isso irá mover todas as subdivisões subjacentes e usuários.";
$MESS["ISV_confirm_set_head"] = "Designar #EMP_NAME# como chefe da divisão #DPT_NAME#?";
$MESS["ISV_confirm_change_department_0"] = "Transferir #EMP_NAME# para #DPT_NAME#?";
$MESS["ISV_confirm_change_department_1"] = "Designar #EMP_NAME# para #DPT_NAME#?";
$MESS["ISV_add_emp"] = "Adicionar usuário";
$MESS["ISV_EMP_COUNT_1"] = "#NUM# usuário";
$MESS["ISV_EMP_COUNT_2"] = "#NUM# usuários";
$MESS["ISV_EMP_COUNT_3"] = "#NUM# usuários";
$MESS["ISV_EMP_COUNT_4"] = "#NUM# usuários";
$MESS["ISV_EMP_COUNT_21"] = "#NUM# usuário";
$MESS["ISV_EMP_COUNT_22"] = "#NUM# usuários";
$MESS["ISV_EMP_COUNT_23"] = "#NUM# usuários";
$MESS["ISV_EMP_COUNT_24"] = "#NUM# usuários";
$MESS["ISV_EMP_COUNT_31"] = "#NUM# usuário";
$MESS["ISV_EMP_COUNT_32"] = "#NUM# usuários";
$MESS["ISV_EMP_COUNT_33"] = "#NUM# usuários";
$MESS["ISV_EMP_COUNT_34"] = "#NUM# usuários";
$MESS["ISV_EMP_COUNT_41"] = "#NUM# usuário";
$MESS["ISV_EMP_COUNT_42"] = "#NUM# usuários";
$MESS["ISV_EMP_COUNT_43"] = "#NUM# usuários";
$MESS["ISV_EMP_COUNT_44"] = "#NUM# usuários";
$MESS["ISV_EMP_COUNT_MUL"] = "#NUM# usuários";
$MESS["ISV_B24_INVITE"] = "Convidar usuários";
$MESS["INTR_EMP_WINDOW_TITLE"] = "Selecionar Usuário";
$MESS["INTR_EMP_SUBMIT_TITLE"] = "Escolha o Usuário selecionado";
$MESS["INTR_EMP_CANCEL_TITLE"] = "Cancelar Seleção de Usuário";
$MESS["INTR_EMP_SEARCH"] = "pesquisar usuário";
$MESS["INTRANET_USTAT_SECTION_SOCNET_HELP_INVOLVEMENT"] = "Mostra a porcentagem de usuários usando a funcionalidade da rede social em determinado período de tempo.";
$MESS["INTRANET_USTAT_SECTION_LIKES_HELP_INVOLVEMENT"] = "Porcentagem de usuários que utilizaram a função \"Curtir\" em determinado período de tempo.";
$MESS["INTRANET_USTAT_SECTION_TASKS_HELP_INVOLVEMENT"] = "Porcentagem de usuários que usaram tarefas em determinado período de tempo.";
$MESS["INTRANET_USTAT_SECTION_IM_HELP_INVOLVEMENT"] = "Mostra a porcentagem de usuários que enviaram uma mensagem instantânea ou fizeram uma chamada de áudio/vídeo em determinado período de tempo.";
$MESS["INTRANET_USTAT_SECTION_DISK_HELP_INVOLVEMENT"] = "Porcentagem de usuários implementando Bitrix24.Drive.";
$MESS["INTRANET_USTAT_SECTION_MOBILE_HELP_INVOLVEMENT"] = "Porcentagem de usuários que utilizaram o app para celular em determinado período de tempo.";
$MESS["INTRANET_USTAT_SECTION_CRM_HELP_INVOLVEMENT"] = "Porcentagem de usuários que estiveram ativos no CRM em determinado período de tempo. Este número é/poderia ser limitado pelo número de usuários com acesso ao CRM.";
$MESS["INTRANET_USTAT_TELLABOUT_SOCNET_TITLE"] = "Unidade na força de trabalho";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_SOCNET"] = "Uso de Recurso: Rede Social";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_LIKES"] = "Uso de Recurso: Curtidas";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_TASKS"] = "Uso de Recurso: Tarefas";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_IM"] = "Uso de Recurso: Bate-papo";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_DISK"] = "Uso de Recurso: Drive";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_MOBILE"] = "Uso de Recurso: App para Celular";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_CRM"] = "Uso de Recurso: CRM";
$MESS["INTRANET_USTAT_COMPANY_GRAPH_TITLE"] = "Pulso da Organização";
$MESS["INTRANET_USTAT_COMPANY_HELP_GENERAL"] = "O Pulso da Organização é um indicador geral da atividade do usuário no portal no momento atual (composto para todos os usuários na hora anterior).";
$MESS["INTRANET_USTAT_COMPANY_HELP_ACTIVITY"] = "Índice de Atividade: composto de todas as atividades dos usuários em todos os recursos da intranet para um determinado período de tempo. O índice mostra quão ativamente os usuários estão trabalhando em diversas ferramentas.";
$MESS["INTRANET_USTAT_COMPANY_HELP_INVOLVEMENT"] = "Nível de envolvimento:  Este é um indicador chave que mostra quão familiarizados os usuários se tornaram com as capacidades do Bitrix24. Ele mostra o percentual de uso da organização de pelo menos um quarto das ferramentas fornecidas.";
$MESS["INTRANET_USTAT_COMPANY_HELP_RATING"] = "A classificação é determinada pela média do índice de atividade individual de todos os usuários que usaram pelo menos 1 ação em determinado período de tempo.";
$MESS["INTRANET_USTAT_WIDGET_TITLE"] = "Pulso da Organização";
$MESS["INTRANET_USTAT_WIDGET_ACTIVITY_HELP"] = "Atual nível de atividade da organização (composto de todos os usuários na última hora)";
$MESS["INTRANET_USTAT_WIDGET_INVOLVEMENT_HELP"] = "Atual envolvimento dos usuários. Isto mostra a porcentagem de todos os usuários hoje que usaram pelo menos quatro diferentes ferramentas na intranet.";
$MESS["INTRANET_USTAT_USER_GRAPH_COMPANY"] = "Média da organização";
$MESS["INTRANET_USTAT_USER_ACTIVITY_COMPANY_TITLE"] = "Média da<br>organização";
$MESS["INTRANET_USTAT_USER_HELP_RATING"] = "Sua posição na <b>classificação de atividade</b> que lista todos os usuários que usaram o Bitrix24 pelo menos uma vez durante o período de tempo especificado.";
$MESS["INTRANET_USTAT_USER_HELP_ACTIVITY_DEPT"] = "Valor médio das ações que sua divisão fez no Bitrix24 usando uma das sete ferramentas disponíveis para o período de tempo especificado.<br><br> <b>Use o valor médio da divisão para ver como sua atividade é melhor do que a dos outros.";
$MESS["INTRANET_USTAT_USER_HELP_ACTIVITY_COMPANY"] = "Valor médio das ações que sua organização fez no Bitrix24 usando uma das sete ferramentas disponíveis para o período de tempo especificado.<br><br> <b>Use o valor médio da organização para ver como sua atividade é melhor do que a de toda a organização.";
$MESS["BM_USR_CNT"] = "Nº de usuários";
$MESS["WD_USER"] = "Documentos do Usuário";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_USER_ADD"] = "Convidar usuário";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_USER_INVITE"] = "Convidar usuário";
$MESS["INTR_COMP_IS_TPL_TOOLBAR_USER_ACTIVE"] = "Usuários";
$MESS["INTR_IS_TPL_ACTION_INVITE"] = "Convidar usuários";
$MESS["INTR_ISE_TPL_NOTE_NULL"] = "A pesquisa não encontrou nenhum usuário.";
$MESS["INTR_CONFIRM_FIRE"] = "O usuário não poderá fazer login no portal, não será mostrado na estrutura da organização. No entanto, todos os seus dados pessoais (arquivos, mensagens, tarefas, etc) permanecerão intactos.\\n\\nVocê tem certeza de que deseja negar o acesso a este usuário?";
$MESS["INTR_CONFIRM_RESTORE"] = "O usuário poderá fazer login no portal e será mostrado na estrutura da organização.\\n\\nVocê tem certeza de que deseja permitir o acesso deste usuário?";
$MESS["INTR_CONFIRM_DELETE"] = "O convite será excluído irrevogavelmente.\\n\\nVocê tem certeza de que deseja excluir o usuário?";
$MESS["CT_BST_SEARCH_HINT"] = "encontrar pessoas, documentos e muito mais";
$MESS["SOCNET_CONFIRM_FIRE"] = "O usuário não poderá fazer login no portal, não será mostrado na estrutura da organização. No entanto, todos os seus dados pessoais (arquivos, mensagens, tarefas, etc) permanecerão intactos.

Você tem certeza de que deseja fechar o acesso para este usuário?";
$MESS["SOCNET_CONFIRM_RECOVER"] = "O usuário poderá fazer login no portal e será mostrado na estrutura da organização.

Você tem certeza de que deseja abrir o acesso para este usuário?";
$MESS["SOCNET_CONFIRM_DELETE"] = "O convite será excluído irrevogavelmente.

Você tem certeza de que deseja excluir o usuário?";
$MESS["ISL_WORK_COUNTRY"] = "País";
$MESS["ISL_WORK_CITY"] = "Cidade";
$MESS["ISL_WORK_LOGO"] = "Logotipo da organização";
$MESS["SOCNET_CONFIRM_FIRE1"] = "O usuário não poderá fazer login no portal, não será mostrado na estrutura da organização. No entanto, todos os seus dados pessoais (arquivos, mensagens, tarefas, etc) permanecerão intactos.\\n\\nVocê tem certeza de que deseja negar o acesso a este usuário?";
$MESS["SOCNET_CONFIRM_RECOVER1"] = "O usuário poderá fazer login no portal e será mostrado na estrutura da organização.\\n\\nVocê tem certeza de que deseja permitir o acesso deste usuário?";
$MESS["TEMPLATE_DESCRIPTION"] = "Este modelo é elaborado para acentuar os aspectos sociais da intranet e combina ferramentas convencionais de criação e produtividade num contexto que facilita a comunicação social. O layout da Intranet Social é um intensificador de produtividade altamente intuitivo e requer o mínimo de tempo para adoção e orientação.";
$MESS["BITRIX24_INVITE"] = "Convidar usuários";
$MESS["BITRIX24_INVITE_ACTION"] = "Convidar usuários";
$MESS["BITRIX24_HELP_VIDEO_TITLE_13"] = "Estrutura da Organização";
$MESS["BITRIX24_HELP_VIDEO_TITLE_FULL_13"] = "Estrutura da Organização";
$MESS["AUTH_OTP_HELP_TEXT"] = "Para tornar os dados do seu Bitrix24 mais seguros, seu administrador ativou a opção de segurança extra: autenticação em dois passos.<br/><br/>
A autenticação em dois passos envolve duas etapas subsequentes de verificação. A primeira requer sua senha principal. A segunda etapa inclui um código único enviado para o seu telefone celular (ou usando um dongle de hardware).<br/><br/>
 <div class=\"login-text-img\"><img src=\"#PATH#/images/en/img1.png\"></div>
 <br/>
 Uma vez que você tenha ativado a autenticação em dois passos, você vai ter que passar por duas telas de autenticação:<br/><br/>
 - digite seu e-mail e senha;<br/>
 - em seguida, digite o código único que pode ser obtido usando o app para celular Bitrix OTP que você instalou quando ativou a autenticação em dois passos.<br/><br/>
 Execute o app em seu telefone celular:<br/><br/>
 <div class=\"login-text-img\"><img src=\"#PATH#/images/en/img2.png\"></div>
 <br/>
 Digite o código que você vê na tela:<br/><br/>
 <div class=\"login-text-img\"><img src=\"#PATH#/images/en/img3.png\"></div>
 <br/>
 Se você usar vários Bitrix24, certifique-se de que o código é para o Bitrix24 correto antes de digitá-lo.<br/><br/>
 <div class=\"login-text-img\"><img src=\"#PATH#/images/en/img4.png\"></div>
 <br/>
 Caso você nunca teve a chance de ativar e configurar a autenticação em dois passos ou perdeu seu telefone, entre em contato com o seu administrador para recuperar o acesso ao seu Bitrix24.<br/><br/>
 Se você for o administrador, contate a <a href=\"http://www.bitrix24.com/support/helpdesk/\">Assistência Técnica Bitrix</a> para recuperar o acesso.";
$MESS["SERVICE_COMPANY_STRUCTURE"] = "Estrutura da Organização";
$MESS["wiz_slogan"] = "Minha Organização";
$MESS["wiz_company_name"] = "Nome da Organização:";
$MESS["wiz_company_logo"] = "Logotipo da Organização:";
$MESS["wiz_demo_structure"] = "Instalar a Amostra de Estrutura de Organização";
$MESS["IFS_EF_staff"] = "Pessoal; documentos de referência; estrutura da organização; usuários honrados";
$MESS["IFS_EF_Blog"] = "Feeds de Blog de Usuário e de Grupo";
$MESS["IFS_EF_Gallery"] = "Galerias de Fotos de Usuários";
$MESS["BLOG_DEMO_MESSAGE_TITLE_1"] = "A abertura do portal da intranet da organização ";
$MESS["BLOG_DEMO_MESSAGE_BODY_1"] = "Caros colegas, o Portal da Intranet começou oficialmente hoje.

Efetivamente, isso significa que foi concedida permissão a cada usuário para ver informações públicas e uma senha para acessar sua área privada.

Acesso configurável às áreas do portal nos oferecem recursos de trabalho em equipa: agora, podemos juntos editar documentos; criar grupos de trabalho; elaborar relatórios!

O objetivo do portal é tornar a cadeia de comunicação entre os usuários o mais curta possível. O portal, sendo um rápido e moderno meio de comunicação, é o melhor método para evitar papelada e tornar eletrônico o fluxo de informações da empresa.
 
Usando o portal da intranet,  os usuários da organização podem obter qualquer informação técnica, de referência ou jurídica, incluindo os padrões corporativos e identidade.

Os usuários podem postar nos fóruns; enviar solicitações de suporte técnico e de serviços de fornecimento; compartilhar e trocar documentos; discutir notícias da organização e obter as informações mais recentes da gerência.

O portal se tornará a parte essencial da nossa organização!";
$MESS["CAL_TYPE_COMPANY_NAME"] = "Calendários da organização";
$MESS["CAL_COMPANY_SECT_DESC_0"] = "Apresentações e conferências que estamos envolvidos";
$MESS["CAL_COMPANY_SECT_DESC_1"] = "Eventos que acontecem no escritório em Londres";
$MESS["CAL_COMPANY_SECT_DESC_2"] = "Eventos que acontecem no escritório em Paris";
$MESS["CAL_COMP_EVENT_DESC_3"] = "Treinamento para os novos usuários";
$MESS["CRM_GADGET_NEW_COMPANY_TITLE"] = "Novas Empresas";
$MESS["CRM_DEMO_EVENT_12_EVENT_NAME"] = "Mudou o Campo \"Usuários\"";
$MESS["CRM_DEMO_EVENT_15_EVENT_NAME"] = "Mudou o Campo \"Usuários\"";
$MESS["GENERAL_FORUM_DESCRIPTION"] = "Fórum Público para os usuários da organização. Discuta seu negócio e troque opiniões aqui.";
$MESS["GENERAL_FORUM_MESSAGE_BODY"] = "Atenção! Agora, os usuários da organização podem manter o armazenamento de arquivo em sua área privada.

Você encontrará informações detalhadas sobre administração do armazenamento de arquivo e o método de mapeamento de armazenamento para uma unidade de rede na seção de ajuda: \"Meu Perfil - Arquivos\".

Caso tenha quaisquer dúvidas relativas à configuração de armazenamento de arquivo, envie suas solicitações para os engenheiros de suporte técnico usando o formulário de solicitação de suporte.\"";
$MESS["ABSENCE_FORM_2"] = "Escolha um usuário ausente
";
$MESS["W_IB_CLIENTS_TAB1"] = "edit1--#--Client--,--NAME--#--*Company name--,--PROPERTY_PERSON--#--Director--,--PROPERTY_PHONE--#--Telephone--;--";
$MESS["HONOR_FORM_2"] = "Selecione o usuário a honrar";
$MESS["STATE_FORM_2"] = "Escolha o Usuário cujo status foi alterado";
$MESS["W_IB_ABSENCE_2_PREV"] = "Viagem de negócios para filial da empresa.";
$MESS["EMPLOYEES_GROUP_DESC"] = "Todos os usuários, cadastrados no portal.";
$MESS["DIRECTION_GROUP_DESC"] = "Gestão.";
$MESS["main_opt_user_comp_name"] = "Nome da organização";
$MESS["main_opt_user_comp_logo"] = "Logotipo da organização";
$MESS["ML_COL_NAME_0"] = "Fotos de Usuário";
$MESS["ML_COL_DESC_0"] = "Coleção de fotos de usuário";
$MESS["MEETING_DESCRIPTION"] = "Vamos ter que fazer uma reunião para discutir a implantação do portal de intranet em nossa organização.";
$MESS["SONET_GROUP_DESCRIPTION_3"] = "Vida esportiva";
$MESS["SUBSCRIBE_POSTING_SUBJECT"] = "Nossas fotos!";
$MESS["SUBSCRIBE_POSTING_BODY"] = "Olá! Carregamos novas fotos de férias.";
$MESS["authprov_check_d"] = "Todos os usuários da divisão";
$MESS["authprov_check_dr"] = "Todos os usuários da divisão e subdivisão";
$MESS["EC_ADD_MEMBERS_FROM_STR"] = "Da Estrutura da Organização";
$MESS["EC_ADD_MEMBERS_FROM_STR_TITLE"] = "Adicionar participantes do evento a partir da estrutura da organização";
$MESS["EC_COMPANY_STRUCTURE"] = "Estrutura da Organização";
$MESS["EC_NO_COMPANY_STRUCTURE"] = "A estrutura da organização está faltando.";
$MESS["BX24_INVITE_TITLE_INVITE"] = "Convidar usuários";
$MESS["BX24_INVITE_TITLE_ADD"] = "Adicionar usuário";
$MESS["BX24_INVITE_DIALOG_MAX_COUNT_ERROR"] = "O número de convidados ultrapassa os termos da sua licença.";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT"] = "Junte-se a nós na nossa nova conta Bitrix24, onde todos podem colaborar em projetos, ter uma comunicação ideal, coordenar tarefas, gerenciar clientes e mais recursos.";
$MESS["I_NEW_USER_TITLE"] = "Um novo usuário foi adicionado";
$MESS["I_NEW_USER_TITLE_SETTINGS"] = "Novos usuários";
$MESS["I_NEW_USER_TITLE_LIST"] = "Novo usuário";
$MESS["I_NEW_USER_MENTION"] = "mencionou você em um comentário em uma mensagem sobre um usuário adicionado recentemente #title#.";
$MESS["I_NEW_USER_MENTION_M"] = "mencionou você em um comentário em uma mensagem sobre um usuário adicionado recentemente #title#.";
$MESS["I_NEW_USER_MENTION_F"] = "mencionou você em um comentário em uma mensagem sobre um usuário adicionado recentemente #title#.";
$MESS["PP_USER_CONDITION_SUBORDINATE_NAME"] = "Estrutura da organização";
$MESS["PP_USER_CONDITION_SUBORDINATE_TEXT"] = "Calcular votos extras para autoridade do usuário com base na estrutura da organização.";
$MESS["INTRANET_RATING_USER_SUBORDINATE_NAME"] = "Estrutura da organização com base em votos extras";
$MESS["INTRANET_RATING_USER_SUBORDINATE_DESC"] = "O valor é baseado em dados calculados pela regra \"Estrutura da organização\".";
$MESS["INTR_IBLOCK_TOP_SECTION_WARNING"] = "A estrutura da organização pode conter apenas uma seção de nível superior.";
$MESS["INTRANET_USER_INVITATION_NAME"] = "Convidar pessoas";
$MESS["INTRANET_USER_INVITATION_DESC"] = "#EMAIL_PARA# - endereço de e-mail da pessoa convidada
 #LINK# - link de ativação de novo usuário";
$MESS["INTRANET_USER_ADD_NAME"] = "Adicionar usuários";
$MESS["INTRANET_USER_ADD_DESC"] = "#EMAIL_PARA# - endereço de e-mail do novo usuário
 #LINK# - URL da intranet";
$MESS["INTRANET_MAILDOMAIN_NOCOMPLETE_MESSAGE"] = "<p>Constatamos que você tentou criar um domínio para a sua organização mas não concluiu o registro.</p>

 <p>Se você precisar de ajuda, abra a página configurações de e-mail, leia para obter instruções detalhadas para configurar seu serviço de e-mail e completar o registro do domínio de seu Bitrix24.</p>

 <p>Se você quiser ver como geralmente funciona, temos vários exemplos do uso de domínios de e-mail para você ver. Os exemplos vão mostrar a mágica por trás do registro de domínio, criação da caixa de correio e toda a utilização do Bitrix24.</p>

 <p><a href=\"#LEARNMORE_LINK#\">Saiba mais</a></p>

 <p>Se você ainda tem perguntas, entre em contato com nossa <a href=\"#SUPPORT_LINK#\">assistência técnica</a>.</p>
";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_NAME"] = "Crie caixas de correio de usuários no domínio";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_SUBJECT"] = "E-mail corporativo para seus usuários";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_MESSAGE"] = "<p>Você registrou o domínio e criou sua caixa de correio. No entanto, seus empregados ou usuários ainda não têm ideia que podem criar suas próprias caixas de correio no seu domínio.</p>

 <p>Compartilhe seus conhecimentos e ajude-os a aprender a criar suas próprias caixas de correio ou registrar caixas de correio na página Bitrix24.</p>

 <p><a href=\"#LEARNMORE_LINK#\">Como registrar caixas de correio para seus empregados</a></p>

 <p>Se você ainda tem perguntas, entre em contato com nossa <a href=\"#SUPPORT_LINK#\">assistência técnica</a>.</p>
";
$MESS["INTRANET_MAILDOMAIN_NOREG_MESSAGE"] = "<p>Você iria registrar um domínio para sua empresa mas não conseguiu escolher um bom nome para ele.</p>

 <p>Temos vários exemplos do uso de domínios de e-mail para você ver. Com os exemplos, você vai aprender como criar uma caixa de correio e usá-la com o Bitrix24.</p>

 <p><a href=\"#LEARNMORE_LINK#\">Como registrar caixas de correio para seus empregados</a></p>

 <p>Se você ainda tem perguntas, entre em contato com nossa <a href=\"#SUPPORT_LINK#\">assistência técnica</a>.</p>\"";
$MESS["INTR_INSTALL_RATING_RULE"] = "Calcule votos extras de autoridade com base na estrutura da organização";
$MESS["INTR_OPTION_IBLOCK_STRUCTURE"] = "Bloco de informações da Estrutura da Organização";
$MESS["INTR_OPTION_IBLOCK_CALENDAR"] = "Bloco de informações de Calendários do Usuário";
$MESS["INTR_PROP_EMP_TITLE"] = "Link para Usuário";
$MESS["COMPANY_MENU_EMPLOYEES"] = "Encontrar Usuário";
$MESS["COMPANY_MENU_STRUCTURE"] = "Estrutura da Organização";
$MESS["COMPANY_TITLE"] = "Encontrar Usuário";
$MESS["SERVICES_MENU_NOVICE"] = "Para Novos Usuários";
$MESS["SERVICES_TITLE"] = "Informações para Novos Usuários";
$MESS["SERVICES_ORG_LIST"] = "Minha organização";
$MESS["INTR_ABSENCE_USER"] = "Selecionar pessoa ausente *";
$MESS["BX24_INVITE_DIALOG_EMAIL"] = "Digite os endereços de e-mail das pessoas que você deseja convidar. Separe várias entradas com uma vírgula ou espaço.";
$MESS["BX24_INVITE_DIALOG_INVITED"] = "<b>Sucesso!</b><br>Os convites foram enviados para os endereços especificados.<br><br>Para visualizar os usuários que você acabou de convidar, <a style=\"white-space:nowrap;\" href=\"/company/?show_user=inactive\">clique aqui</a>.";
$MESS["BX24_INVITE_DIALOG_ADDED"] = "<b>Parabéns!</b><br>Uma notificação de adesão à intranet foi enviada para este usuário.<br><br>Reveja os novos usuários que você convidou no <a style=\"white-space:nowrap;\" href=\"#SITE_DIR#company/?show_user=inactive\">diretório de usuário</a>.";
$MESS["BX24_INVITE_DIALOG_INVITE_LIMIT"] = "Não é possível convidar mais usuários porque irá exceder seus termos de licença.";
$MESS["BX24_INVITE_DIALOG_EMPLOYEE"] = "usuário";
$MESS["CONFIG_COMPANY_NAME"] = "Nome da organização";
$MESS["CONFIG_COMPANY_TITLE_NAME"] = "Nome da organização para exibir no cabeçalho";
$MESS["CONFIG_LOGO_24"] = "Adicionar \"24\" ao logotipo da organização";
$MESS["CONFIG_CLIENT_LOGO"] = "Logotipo da organização";
$MESS["CONFIG_OTP_SECURITY"] = "Ativar a autenticação em dois passos para todos os usuários";
$MESS["CONFIG_OTP_SECURITY2"] = "Tornar a autorização em dois passos obrigatória para todos os usuários";
$MESS["CONFIG_OTP_SECURITY_DAYS"] = "Especificar o período de tempo dentro do qual todos os usuários<br/>terão que ativar a autenticação em dois passos.";
$MESS["CONFIG_OTP_SECURITY_INFO"] = "Desenvolvemos um amigável procedimento de ativação da autenticação em dois passos que qualquer usuário pode fazer sem a ajuda de um especialista.<br/><br/>
Será enviada uma mensagem a cada usuário, notificando que eles terão que ativar a autenticação em dois passos dentro do período de tempo introduzido. Os usuários que não fizerem isso, não poderão mais entrar.";
$MESS["CONFIG_OTP_SECURITY_INFO2"] = "<br/>Para ativar a autenticação em dois passos, o usuário deve instalar o aplicativo OTP Bitrix24 em seu telefone celular. Este aplicativo pode ser baixado da AppStore ou GooglePlay.<br/><br/>
Os usuários que não têm um dispositivo móvel adequado devem ser equipados com o hardware especial eToken Pass. Há vários fornecedores de quem você pode obtê-lo, por exemplo:
<a target=_blank href=\"Http://www.safenet-inc.com/multi-factor-authentication/authenticators/pki-usb-authentication/etoken-pro/\">www.safenet-inc.com</a>,
 <a target=_blank href=\"http://www.authguard.com/eToken-PASS.asp\">www.authguard.com</a>.
Encontre outros fornecedores no <a target=_blank href=\"https://www.google.com/?q=buy+eToken+PASS&spell=1#safe=off&q=buy+eToken+PASS\">Google</a>.
<br/><br/>
Como opção, você pode desativar a autenticação em dois passos para alguns dos usuários. No entanto, isso irá aumentar o risco de acesso não autorizado ao seu Bitrix24 se o login e a senha de um desses usuários forem roubados. Como administrador, você pode desativar a autenticação em dois passos para um usuário no perfil do usuário.";
$MESS["CONFIG_OTP_POPUP_TEXT"] = "Aqui está o texto que você pode postar no Fluxo de Atividade<br/>para seus usuários lerem.<br/><br/>
Informe seus colegas sobre o procedimento de configuração da autenticação em dois passos<br/>e o novo método de autenticação<br/>que eles terão que usar para fazer login no Bitrix24.";
$MESS["CONFIG_OTP_ADMIN_IS_REQUIRED_INFO"] = "Antes de migrar seus usuários para o sistema de autenticação em dois passos, primeiro configure-o para a sua conta.<br/><br/>Prossiga, ativando-o na página de Segurança do seu perfil.";
$MESS["CONFIG_OTP_IMPORTANT_TEXT"] = "Hoje, você pode usar seu login e senha para acessar seu Bitrix24. Os dados empresariais e pessoais são protegidos por tecnologia de criptografia de dados. No entanto, existem ferramentas que uma pessoa mal-intencionada pode empregar para entrar em seu computador e roubar suas credenciais de login.

Para proteger contra possíveis ameaças, ativamos um recurso extra de segurança para o Bitrix24:  autenticação em dois passos.

A autenticação em dois passos é um método especial para proteger de hackers de software, particularmente roubo de senha. Cada vez que você faz login no sistema, você vai ter que passar por dois níveis de verificação. Primeiro, você digitará seu endereço de e-mail e senha. Então, você digitará um único código de segurança, obtido a partir do seu dispositivo móvel.

Isso tornará os dados empresariais seguros, mesmo se o login e a senha de qualquer usuário forem roubadas por um invasor.

Você tem 5 dias para ativar este recurso.

Para configurar o novo procedimento de autenticação,  vá para o seu perfil e selecione a página \"Configurações de Segurança\".

Se você não tiver um dispositivo móvel adequado para executar o aplicativo ou se você tiver qualquer tipo de problema, entre em contato conosco, comentando nesta postagem.";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_JOIN"] = "Postar notificação de novo usuário no bate-papo público";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_LEAVE"] = "Postar notificação de descarte de usuário no bate-papo público";
$MESS["CONFIG_ALLOW_NEW_USER_LF"] = "Postar notificação de novo usuário no Fluxo de Atividade";
$MESS["MP_ADD_APP_SCOPE_DEPARTMENT"] = "Estrutura da organização";
$MESS["APP_RIGHTS"] = "Permissões de acesso do usuário";
$MESS["iblock_dep_name1"] = "Minha Organização";
$MESS["main_site_name"] = "Minha organização";
$MESS["SONET_TASK_TITLE_2"] = "Convidar novos usuários";
$MESS["SONET_TASK_DESCRIPTION_2"] = "Convidar novos usuários para participar do portal da intranet";
$MESS["UF_PUBLIC"] = "Visível a todos na Extranet";
$MESS["B24_NEW_USER_TITLE"] = "Um novo usuário foi adicionado";
$MESS["B24_NEW_USER_TITLE_SETTINGS"] = "Novos usuários";
$MESS["B24_NEW_USER_TITLE_LIST"] = "Novo usuário";
$MESS["B24_NEW_USER_MENTION"] = "mencionou você em um comentário em uma mensagem sobre um usuário adicionado recentemente #title#.";
$MESS["B24_NEW_USER_MENTION_M"] = "mencionou você em um comentário em uma mensagem sobre um usuário adicionado recentemente #title#.";
$MESS["B24_NEW_USER_MENTION_F"] = "mencionou você em um comentário em uma mensagem sobre um usuário adicionado recentemente #title#.";
$MESS["BITRIX24_USER_INVITATION_NAME"] = "Convidar pessoas";
$MESS["BITRIX24_USER_INVITATION_DESC"] = "#EMAIL_FROM# - convidando o E-mail do usuário
 #EMAIL_TO# - E-mail do usuário convidado
 #LINK# - link de ativação do novo usuário";
$MESS["MENU_STRUCTURE"] = "Estrutura da Organização";
$MESS["TITLE1"] = "Estrutura da Organização";
$MESS["TITLE2"] = "Estrutura da Organização";
$MESS["FIELD_empl_num"] = "Número de usuários";
$MESS["LICENSE_CRM_FEATURE_16"] = "Log de acesso de CRM";
$MESS["LICENSE_TEL_FEATURE_11"] = "Extensões de usuário";
$MESS["LICENSE_TEL_FEATURE_16"] = "Chamada simultânea para todos os usuários disponíveis";
$MESS["LICENSE_MORE_USERS"] = "Mais usuários";
$MESS["DEMO_FEATURE_1"] = "Usuários ilimitados";
$MESS["DEMO_INFO_DESC_1"] = "Se você convidou mais de 12 usuários durante o teste, depois que acabar, apenas os primeiros 12 usuários poderão acessar sua conta Bitrix24. Se você quiser restabelecer o acesso para todos os usuários, você precisa <a class=\"link\" href=\"/settings/license_all.php\">fazer upgrade</a>. ";
$MESS["BX24_EXTRANET2INTRANET_DESC"] = "<b>#FULL_NAME#</b> é um usuário externo.<br><br>Observe que você <b>não poderá</b> transferir o usuário da intranet para a extranet mais tarde.<br><br>Para transferir #FULL_NAME# para intranet, selecione uma divisão de destino.";
$MESS["BX24_EXTRANET2INTRANET_SUCCESS"] = "O usuário foi transferido com sucesso. Agora, eles podem acessar seu conteúdo interno da intranet de acordo com suas permissões.";
$MESS["MENU_EMPLOYEE"] = "Usuários";
$MESS["CONFIG_USERS2"] = "Usuários:";
$MESS["DEMO_INFO_1"] = "Usuários";
$MESS["ISV_set_department_head"] = "<b>#NAME#</b> foi nomeado como chefe da <b>#DEPARTMENT#</b> divisão.";
$MESS["ISV_change_department"] = "<b>#NAME#</b> foi transferido para <b>#DEPARTMENT#</b>.";
$MESS["authprov_name"] = "Usuários e divisões";
$MESS["LM_POPUP_TAB_STRUCTURE"] = "Usuários e divisões";
$MESS["LM_POPUP_CHECK_STRUCTURE"] = "Todos os usuários da divisão e subdivisão";
$MESS["BITRIX24_SEARCH_EMPLOYEE"] = "Usuários";
$MESS["SONET_GL_DESTINATION_G2"] = "Para todos os usuários";
$MESS["BLOG_DESTINATION_ALL"] = "Para todos os usuários";
$MESS["MPF_DESTINATION_1"] = "Adicionar usuários, grupos ou divisões";
$MESS["EC_DESTINATION_1"] = "Adicionar usuários, grupos ou divisões";
$MESS["MPF_DESTINATION_3"] = "Todos os usuários";
$MESS["INTRANET_USTAT_TOGGLE_COMPANY"] = "Organização";
$MESS["INTRANET_TAB_USER_STRUCTURE"] = "Organização";
$MESS["MENU_COMPANY"] = "ORGANIZAÇÃO";
$MESS["BLOG_GRATMEDAL_1"] = "Adicionar usuários";
$MESS["SONET_GCE_T_USER_INTRANET"] = "Usuários da organização:";
$MESS["SONET_GCE_T_INVITATION_EMPLOYEES"] = "Convidar usuários";
$MESS["SONET_GCE_T_ADD_EMPLOYEE"] = "Adicionar usuário";
$MESS["SONET_GCE_T_DEST_TITLE_EMPLOYEE"] = "Convidar usuários:";
$MESS["EC_COMPANY_CALENDAR"] = "Calendário da organização";
$MESS["MENU_COMPANY_CALENDAR"] = "Calendário da organização";
$MESS["SONET_UP1_WORK_POSITION"] = "Posição na comunidade";
$MESS["ISV_EMPLOYEES"] = "USUÁRIOS";
$MESS["MENU_COMPANY_SECTION"] = "Comunidade";
$MESS["IM_CL_STRUCTURE"] = "Comunidade";
$MESS["IM_CL_USER_B24"] = "Membro";
$MESS["IM_C_ABOUT_CHAT"] = "Um bate-papo privado está disponível somente para os usuários convidados. #BR##BR# Este bate-papo é perfeito para discussões de negócios que envolvem e dizem respeito a certas pessoas. #BR##BR# Não é somente seus colegas de trabalho que você pode convidar para este bate-papo; envie convites aos seus clientes, parceiros e outras pessoas relacionadas ao seu negócio que usam Bitrix24. Para adicionar um novo membro de bate-papo, digite nome e sobrenome, e-mail ou nome de exibição. #BR##BR#Complete #PROFILE_START#seu perfil#PROFILE_END# para permitir que outras pessoas encontrem você.";
$MESS["IM_C_ABOUT_OPEN"] = "Um bate-papo público é aberto a todos os seus colegas. #BR##BR# Use este bate-papo para discutir temas de importância para qualquer um em sua empresa.#BR##BR# Uma vez que um bate-papo público é criado, uma notificação é enviada ao #CHAT_START#Bate-papo Geral#CHAT_FINAL#. Então, seus colegas podem visualizar mensagens do bate-papo e participar do bate-papo se quiserem.#BR##BR# Sua mensagem inicial é fundamental para despertar o interesse dos outros no seu novo bate-papo.";
$MESS["IM_C_ABOUT_PRIVATE"] = "Um bate-papo de pessoa para pessoa é visível apenas para você e seu contato. #BR##BR# Encontre uma pessoa com quem você deseja conversar pelo seu nome, cargo ou departamento. #BR##BR# Você também pode conversar com qualquer um dos seus clientes, parceiros ou outras pessoas usando Bitrix24. Encontre-os por nome, e-mail ou nome de exibição que você acha que eles usam. #BR##BR# Complete #PROFILE_START#seu perfil#PROFILE_END# para ajudar outras pessoas a encontrar você.";
$MESS["IM_CTL_CHAT_STRUCTURE"] = "Estrutura da comunidade";
$MESS["IM_M_CALL_ST_TRANSFER"] = "Redirecionamento de chamada: aguardando resposta";
$MESS["IM_M_CALL_ST_TRANSFER_1"] = "Redirecionamento de chamada: usuário não respondeu";
$MESS["TITLE"] = "Encontrar Usuário";
?>