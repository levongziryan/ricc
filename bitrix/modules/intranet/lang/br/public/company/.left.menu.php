<?
$MESS["COMPANY_MENU_EMPLOYEES"] = "Procurar Colaborador";
$MESS["COMPANY_MENU_TELEPHONES"] = "Lista telefónica";
$MESS["COMPANY_MENU_STRUCTURE"] = "Estrutura da Empresa";
$MESS["COMPANY_MENU_EVENTS"] = "Mudança de Equipe";
$MESS["COMPANY_MENU_ABSENCE"] = "Gráfico de ausência ";
$MESS["COMPANY_MENU_TIMEMAN"] = "Conômetro ";
$MESS["COMPANY_MENU_WORKREPORT"] = "Relatório ";
$MESS["COMPANY_MENU_REPORT"] = "Relatório de eficiência ";
$MESS["COMPANY_MENU_LEADERS"] = "Colaboradores Honrados ";
$MESS["COMPANY_MENU_BIRTHDAYS"] = "Aniversários ";
$MESS["COMPANY_MENU_GALLERY"] = "Fotos compartilhadas";
$MESS["COMPANY_MENU_MY_PROCESSES"] = "Meus Pedidos";
?>