<?
$MESS["SERVICES_TITLE"] = "Integração com o Microsoft Outlook em dois sentidos";
$MESS["SERVICES_INFO1"] = "<p> Bitrix24 implementa integração bidirecional com o Microsoft Outlook.
Essencialmente, isso significa que você pode fazer mais do que apenas informações de exportação a partir do portal
para Microsoft Outlook. Quando conectado , duas aplicações realizar negociação e sincronizar , de modo que ambos irão refletir as alterações feitas para
calendários pessoais , contatos e tarefas , no outro extremo . </p>

<table border='0' cellspacing='1' cellpadding='1' width='100%'>
  <tbody>
    <td valign='top'> <img hspace='10' src='#SITE#'images/docs/cp/bullet-n.gif' width='12' height='15' /> <a href='#my_kalendar' > Sincronizar
        calendários pessoais </a>
        <br />
      <img hspace='10' src='#SITE#'images/docs/cp/bullet-n.gif' width='12' height='15' /> <a href='#company_calendar' > sincronizar
        empresa calendários </a>
        <br />
      <img hspace='10' src='#SITE#'images/docs/cp/bullet-n.gif' width='12' height='15' /> <a href='#useful' > sincronizar
        contatos pessoais </a >
        <br />
      <img hspace='10' src='#SITE#'images/docs/cp/bullet-n.gif' width='12' height='15' /> <a href='#kalendars' > exportação
        vários calendários </a>
        <br />
      <img hspace='10' src='#SITE#'images/docs/cp/bullet-n.gif' width='12' height='15' /> <a href='#kalendars' > Vista
        calendários em uma grade comum em MS Outlook </a>
        <br />
      <img hspace='10' src='#SITE#'images/docs/cp/bullet-n.gif' width='12' height='15' /> <a href='#useful1' > sincronizar
        tarefas pessoais </a >
      </td> <td>
        <br />
      </td> <td valign='top'>
        <b> conectar ao Microsoft Outlook agora mesmo! </b>
";
$MESS["SERVICES_INFO2"] = " <br />
      </td></tr>
  </tbody>
</table>

<h2>What's So Great About Two-Way Synchronization? </h2>
<p>This type of synchronization allows you to keep data (e.g. personal contacts) up
to date in the portal and in MS Outlook simultaneously. Try synchronizing them right
now and see how easy it can be! Assume you are aboard a plane planning
meetings, tasks and presentations for your company. After doing the planning in MS
Outlook, use the two-way synchronization function - all your new plans will appear
in the portal calendar immediately. Keep on top of your schedule without tedious
manual copying! Fast to create, easy to manage! <a name='useful'></a></p>

<h2>User Synchronization</h2>

<p>To assign tasks to or receive tasks from other persons, you have to add these persons to your Microsoft Outlook. It?s even simpler than simple: select Employees > Search Employee and click <b>Synchronize with Outlook</b>.</p>
<p>Those of you who prefer the Bitrix24 template: select Employees > Search employees in the menu on the left of the screen to open the user (employee) browse page. Now, click <b>More</b> on the toolbar and select <b>Outlook</b>.</p>
<p>The import procedure is fully automated; you just have to confirm the operation once or twice. Now wait while import is being performed.</p>
<p>After the import procedure has completed, your employees will show in the calendar:</p>
<div align='center'>
  <table style='BORDER-COLLAPSE: collapse' border='0' cellspacing='1' cellpadding='1'>
    <tbody>
      <tr><td>
          <div align='center'><a href='javascript:ShowImg('#SITE#'images/docs/big/outlook.png', 600, 413, 'Adding an employee')'><img title='Adding an employee' border='0' alt='Adding an employee' src='#SITE#'images/docs/outlook_sm.png' width='300' height='207' /> </a>
            <br />
          </div>

          <div align='center'><i>Employees Added from the Portal</i>
            <br />
          </div>
        </td></tr>
    </tbody>
  </table>
</div>

<p>There is no danger in deleting employees from MS Outlook: this will not delete them
from the portal. Only the portal administrator can do that.</p>

<h2><a name='my_kalendar'></a>Synchronizing Calendars with MS Outlook</h2>
<p>You can synchronize any Portal calendars with MS Outlook calendars: personal,
employee or company calendars. Try to synchronize right now! Open a calendar
page, select <b> Connect To Outlook</b> in the action menu and start synchronization:</p>

<div align='center'><img src='#SITE#'images/docs/outlook_1.png'  />
<br><i> Connect To Outlook</i></div>

<p>Pay no attention to MS Outlook message boxes: most of them are just
notifications; confirm the operations whenever your confirmation is required.
For example, if you encounter a <b>&quot;Connect this SharePoint calendar to
outlook?&quot;</b> message - click <b>Yes</b> without hesitation. The two-way MS
Outlook integration fully conforms to Microsoft specifications and standards which
guarantees an absolutely smooth operation.</p>

<div align='center'><img src='#SITE#'images/docs/outlook_2_sm.png' />
  <br />
</div>

<p>If you want to control the process in detail, you can click <b>Advanced...</b> to
provide a description to your calendar:</p>

<div align='center'>
  <table style='BORDER-COLLAPSE: collapse' border='0' cellspacing='1' cellpadding='1'>
    <tbody>
      <tr><td>
          <div align='center'><a href='javascript:ShowImg('#SITE#'images/docs/big/outlook_3.png', 509, 449, 'Calendar description')'><img title='Calendar description' border='0' alt='Calendar description' src='#SITE#'images/docs/outlook_3_sm.png' /> </a>
            <br />
          </div>

          <div align='center'><i>Calendar description</i>
            <br />
          </div>
        </td></tr>
    </tbody>
  </table>
</div>

<p><a name='company_calendar'></a>Now look at the result. Your Outlook has a new
calendar showing all your events! What could be more convenient than viewing
the company's events in Outlook after a long leave. Keep up with your company,
connect and synchronize calendars, and be in the thick of things!</p>

<div align='center'>
  <table style='BORDER-COLLAPSE: collapse' border='0' cellspacing='1' cellpadding='1'>
    <tbody>
      <tr><td>
          <div align='center'><a href='javascript:ShowImg('#SITE#'images/docs/big/outlook_4.png', 709, 575, 'Calendar has been added to Outlook!')'><img title='Calendar has been added to Outlook!' border='0' alt='Calendar has been added to Outlook!' src='#SITE#'images/docs/outlook_4_sm.png'  /> </a>
            <br />
          </div>

          <div align='center'><i>New calendar added to Outlook!</i>
            <br />
          </div>
        </td></tr>
    </tbody>
  </table>
</div>

<p><a name='kalendars'></a>Now connect all the required portal calendars in the
same way. Show them in a single grid and voila - now your calendar looks just
like the portal calendar!</p>

<div align='center'>
  <table style='BORDER-COLLAPSE: collapse' border='0' cellspacing='1' cellpadding='1'>
    <tbody>
      <tr><td>
          <div align='center'><a href='javascript:ShowImg('#SITE#'images/docs/big/outlook_5.png', 709, 575, 'Export calendar(s) to Outlook')'><img title='Export calendar(s) to Outlook' border='0' alt='Export calendar(s) to Outlook' src='#SITE#'images/docs/outlook_5_sm.png'  /> </a>
            <br />
          </div>

          <div align='center'><i> MS Outlook calendars in a single grid</i>
            <br />
          </div>
        </td></tr>
    </tbody>
  </table>
</div>

<h2>Adding Events</h2>
<p>How does the two-way integration work in real life? Try adding a new event to a MS
Outlook calendar - you will see it appear in the portal calendar! Do it now:</p>

<ul>
  <li>select the event date in the calendar grid;</li>
  <li>double-click it to open a create new event window; </li>
  <li>fill in the Topic, Start, End and Description fields; </li>
  <li>click <b>Save And Close</b>. </li>
</ul>

Now see the new event added to the MS Outlook calendar.

<div align='center'><a href='javascript:ShowImg('#SITE#'images/docs/big/outlook_6.png',650,500,'New Event')'> <img height='192' border='0' width='250' src='#SITE#'images/docs/outlook_6_sm.png' title='Click to Enlarge' style='cursor: pointer;' /></a></p>
  </div>
<p class='a1'><a name='sinhr'></a>You don't need to bother about synchronizing
your MS Outlook calendars with the portal calendars. This task is performed
automatically along with the Outlook e-mail check. The bottom right
corner of the Outlook window will show the synchronization progress.</p>

<p class='a1' align='center'><img src='/images/docs/outlook_8.png' /></p>
<p>The following picture shows the same event in the portal calendar:</p>

<div align='center'><a href='javascript:ShowImg('#SITE#'images/docs/big/outlook_9.png',452,272,'New Event in the Calendar')'> <img height='150' border='0' width='249' src='#SITE#'images/docs/outlook_9_sm.png' title='Click to Enlarge' style='cursor: pointer;' /></a></div>

<p>You can edit and delete events in the MS Outlook and portal calendars in the
same way. Any changes made on one side will be reflected on the other side.</p>

<a name='useful1'></a>
<h2>Synchronizing Personal Tasks</h2>
<p>Now try to synchronize your tasks. Keep in mind that this process is also
automated and does not require your assistance.</p>

<div align='center'>
  <div align='center'><a href='javascript:ShowImg('#SITE#'images/docs/big/outlook_13.png', 600, 413, 'Synchronize personal tasks')'><img title='Synchronize personal tasks' border='0' alt='Synchronize personal tasks' src='#SITE#'images/docs/outlook_13_sm.png' width='400' height='276' /> </a>
    <br />
  </div>
  <div align='center'><i>Synchronizing personal tasks</i>
  </div>
</div>

<p>To synchronize tasks:</p>
<ul>
  <li>synchronize users (click <b>Outlook</b> on the <b>Search Users</b> page); </li>
  <li>when prompted, enter the portal access password; </li>
  <li>the user list will be added to MS Outlook automatically; </li>
  <li>use MS Outlook commands to assign a task to any user; </li>
  <li>MS Outlook will connect to the portal and synchronize tasks. </li>
</ul>
";
$MESS["SERVICES_LINK1"] = "Conectar Contatos";
$MESS["SERVICES_LINK2"] = "Conectar Minhas Tarefas";
$MESS["SERVICES_CONNECT"] = "Conectar";
?>