<?
$MESS["SERVICES_TITLE"] = "Extranet";
$MESS["SERVICES_INFO"] = "<h3>Unificar as informações vizinha Spaces</h3>

<p>O<b>Extranet</b> módulo é uma extensão para Intranet Portal possibilitando uma
empresa para estabelecer a comunicação confidencial com empreiteiros, fornecedores ,
distribuidores ou outros utilizadores externos , enquanto que proíbem a terceiros para acesso
informação corporativa privada.
</p>
<p><b>Extranet</b> é um espaço de informação segura para interagir com externo
ambiente . O espaço na web coletivo é projetado especialmente para a efetiva
a cooperação com os parceiros. Convide seus parceiros e colegas de extranet
grupos de trabalho e mais de seus trabalhos , tarefas e problemas coletivos compartilhados. este
solução garante que a sua comunicação é segura , preservando a Intranet
segurança . </p>

<div align='center'> <img width='302' height='218' src='#SITE#images/docs/cp/extranet_main-s300.png' complete='complete' />
  <br />
  <i>A página principal Extranet</i></div>

<h3>Acesso e Segurança</h3>

<p> Os usuários convidados que não são funcionários da empresa ter acesso limitado
permissão e, o que é essencial, permissão para visualizar apenas as páginas Extranet .
Esses usuários não têm permissão para visualizar informações privadas da empresa. Ao mesmo
tempo, os funcionários da empresa podem ser convidados e participar de grupos de trabalho para Extranet
trabalhar com os seus colegas e parceiros em conjunto : discutir os problemas essenciais em
fóruns e blogs ; trabalhar com documentos compartilhados ; planejar atividades em calendários ;
atribuir tarefas e ver o seu progresso ; publicar relatórios e muito mais. </p>

<h3> Trabalho em equipe </h3>

<p> Então, quais são as vantagens da Extranet sendo águas neutras para reuniões e
colaboração? Fundamentalmente e tecnologicamente , a solução utiliza a mesma
princípios sobre os quais Intranet Portal se baseia. Extranet teamworking é absolutamente
análogo ao que, nos grupos de trabalho Intranet Portal. A única diferença é
política mais rigorosa de segurança. </p>

<h3> Como instalar ? </h3 >

<p> Ao instalar Bitrix24 , você começa o módulo Extranet instalado automaticamente. No entanto, isso pode não ser o caso quando a atualização de uma versão mais antiga. Se o módulo Extranet não está presente na lista de módulos já existentes , você tem que <a href='/bitrix/admin/module_admin.php' > instalá-lo </a> antes de prosseguir. </p>

<div align='center'> <img width='361' height='205' src='#SITE#images/docs/cp/extranet_setup.png' complete='complete' />
  <br />
  <i> Instalando o módulo Extranet </i> </div>

<p> Depois de ter instalado com sucesso o módulo , você será solicitado a executar <a href='/bitrix/admin/wizard_install.php?lang=en&amp;wizardName=bitrix:extranet&amp;<?=bitrix_sessid_get()?>' > Extranet site Assistente </a>
em que você vai configurar os parâmetros de sites de extranet . </p>

<p> Assim que o assistente for concluído , uma <b> Extranet </b> item irá aparecer na
no menu superior. O sistema está instalado e funcionando e você está pronto para ir! </p>

<table width='100%' style='BORDER-COLLAPSE: collapse' border='0' cellspacing='0' cellpadding='0'>
  <tbody>
    <td valign='top'>
        <br />
      </td > <td valign='top'> <span class='text'> <b> coisas a fazer : </b>
          <ul>

            <li> usar ferramentas especiais <b> extranet </b> para facilitar a experiência de trabalho ; </li>

            <li> fornecer <b> informação pública </b> (considere <b> Empresa </b> ou <b> Sobre
              Somos </b> seções ); </li>

            <li> <b> convidar seus colegas </b> de empresas parceiras para
              trabalho em equipe . </li>
          </ul>
        </span> </td> </tr>
  </tbody>
</table>";
?>