<?
$MESS["SERVICES_TITLE"] = "Reserva da Sala de Reuniões";
$MESS["SERVICES_INIT_DATE"] = "-Mostrar Data Atual-";
$MESS["SERVICES_INFO"] = "Sempre que você quiser reservar uma sala de reuniões, procure pelo horário vago no calendário; ligue para o seu gerente para confirmar a reserva e colocá-lo no calendário reserva.";
$MESS["SERVICES_LINK"] = "Exibir Quadro de Reserva da Sala de Reunião ";
?>