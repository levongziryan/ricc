<?
$MESS["SERVICES_TITLE"] = "Título";
$MESS["SERVICES_MESSAGE_ADD"] = "Seu anúncio foi adicionado ";
$MESS["SERVICES_MESSAGE_EDIT"] = "Seu anúncio foi salvo";
$MESS["SERVICES_DATE_ACTIVE_TO"] = "Válido até";
$MESS["SERVICES_CATEGORY"] = "Categoria ";
$MESS["SERVICES_TEXT"] = "Texto do Anúncio";
?>