<?
$MESS["LEFT_MENU_LIVE_FEED"] = "Fluxo de Atividades";
$MESS["LEFT_MENU_TASKS"] = "Tarefas";
$MESS["LEFT_MENU_CALENDAR"] = "Calendario";
$MESS["LEFT_MENU_DISC"] = "Meus Arquivos";
$MESS["LEFT_MENU_PHOTO"] = "Minhas Imagens";
$MESS["LEFT_MENU_BLOG"] = "Conversas";
$MESS["LEFT_MENU_MAIL"] = "E-mail";
$MESS["LEFT_MENU_MAIL_SETTING"] = "Editar configurações";
$MESS["LEFT_MENU_BP"] = "Fluxo de Trabalho (BPM)";
$MESS["LEFT_MENU_CRM"] = "CRM";
$MESS["LEFT_MENU_MY_PROCESS"] = "Meus Pedidos";
?>