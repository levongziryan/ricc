<?
$MESS["ABOUT_TITLE"] = "Sobre a Empresa";
$MESS["ABOUT_INFO"] = "Com o estabelecimento do Global Food supermercados supermercado no início de 1990, temos as bases para uma estratégia consistente marca de varejo. Nossos clientes tem a certeza de ter a melhor seleção possível e ofertas disponíveis para eles.
<br />

<br />
Os gerentes das lojas individuais decidir sobre as suas necessidades de publicidade locais, a seleção dos produtos que oferecem, preços e planejamento de pessoal. Isto lhes permite responder de forma flexível às necessidades dos seus clientes particulares.
<br />

<br />
 Global Food goza de um crescimento constante e se tornou o líder varejista de supermercado na Europa. Atualmente mais de 1200 lojas em 10 países, o que gerou vendas líquidas de 27,1 bilhões de euros em 2007. O grupo empresarial tem atualmente emprega mais de 65.000 pessoas-mais de 1.500 dos quais são empregados na sede do grupo.
<br />

<br />
 Os clientes podem sempre esperar encontrar uma gama abrangente de produtos da Global Food lojas. Dependendo do tamanho e tipo, os estabelecimentos oferecem cerca de 20.000 diferentes produtos alimentares. Global Food mantém um grande estoque de todos os produtos na sua gama, em uma variedade de opções de empacotamento, todos a preços competitivos e com qualidade sem igual.
<br />

<br />
 <b>Normas locais e estruturação loja específica</b>
<br />

<br />
 Global Food atua em diversos países com três formatos de loja. A área de vendas de uma loja mais comum é de 10.000 a 16.000 metros quadrados. Este formato é usado principalmente em países da Europa Ocidental. Na Europa Oriental, as lojas médias de 7.000 a 9.000 são geralmente construídos. As pequenas lojas têm áreas de vendas de 2.500 a 4.000 metros quadrados.
<br />

<br />
 <b>Adaptar-se a atender às necessidades locais</b>
<br />

<br />
Uma característica especial do conceito de vendas global de alimentos é que a gama de produtos de cada loja é adaptado às necessidades do grupo-alvo local. Os produtos em nossas prateleiras e unidades de refrigeração são cuidadosamente selecionados de acordo com o comportamento e as exigências da respectiva região de consumo. Cerca de 90 por cento da mercadoria em nossas lojas é comprada de produtores locais e fornecedores dentro do respectivo país.
<br />

<br />
 A empresa ajuda os produtores e fornecedores no desenvolvimento de métodos de cultivo, processamento e distribuição modernos. A Política Global de Alimentos de aquisição local suporta economias regionais - por exemplo, através da geração de novos negócios para os fabricantes e agricultores. Além disso, as lojas de lidar com as necessidades especiais, com uma grande variedade de produtos internacionais, tais como os alimentos oferecidos turcos na Alemanha.
<br />

<br />

<h2>Fatos sobre a Global Food</h2>
 demonstrações financeiras em milhões de euros
<br />

<br />
 <img width='217' height='193' src=''#SITE#images/company/about/facts.png' />
<br />

<h2>compromisso social</h2>
 As empresas e os seus locais fazem parte da comunidade. Para ser bem sucedido, eles dependem de seu ambiente. Isso inclui a infra-estrutura que funcione bem e instituições, um bom sistema educacional, e atraentes oportunidades culturais e de lazer para os próprios funcionários da empresa. Ao tornar-se envolvido na comunidade, as empresas podem contribuir para o reforço da eficiência e atratividade de seu ambiente. Ao mesmo tempo, eles indicam a sua vontade de assumir a responsabilidade para a sociedade como um todo.
<br />

<br />
Perto da Global Food lojas para os clientes, fornecedores e colaboradores sempre caracterizou a nossa compreensão da responsabilidade social. Queremos que as pessoas nos mercados em que operamos para ganhar um benefício tangível de nossas atividades. Assim como nossas atividades são voltadas para gerar crescimento rentável, o nosso compromisso social visa criar estruturas sustentáveis em torno de nossos locais da empresa. Nós contribuímos nossa competência e experiência para projetos comunitários e iniciativas que podemos conciliar de forma significativa e credível com o nosso core business.";
?>