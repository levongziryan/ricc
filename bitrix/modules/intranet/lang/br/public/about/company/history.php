<?
$MESS["ABOUT_TITLE"] = "Histórico e Desenvolvimento";
$MESS["ABOUT_INFO"] = "<br />

<br />
<b>1990-1996
  <br />

  <br />
 </b>A história Global Food começou em 1990, quando abrimos a primeira loja da empresa em Paris. Cinco anos mais tarde, havia 24 Global Food lojas em França tocando até US \$ 11,5 milhões em vendas. Em 1998, a empresa oficialmente constituída como Global Food Stores Inc. e em 01 de Abril do mesmo ano, a empresa estreou no mercado de ações. Em 12 de agosto de 1996, as ações globais de alimentos foram coletados para o primeiro tempo na Stock Index CAC40 Paris (símbolo: PIADA). Global Food encerrou o ano de 1996 como uma das 20 maiores empresas de capital aberto da França. Ele também avançou seu processo de internacionalização: a empresa se expande para a Alemanha e Itália.
<br />

<br />
 <b>1997-1999</b>
<br />

<br />
 Global Food avança ainda mais sua expansão fora da França: a abertura da primeira loja de atacado Global Food na República Checa e no mercado polaco.
<br />

<br />
No ano de 1998, o lucro antes de juros e impostos (EBIT) crescer 57,5 por cento. Nossas ações superam todas as outras reservas CAC40. O grupo agiliza ainda mais o seu portfólio: Global Food tornou-se uma corporação bem estruturada. O progresso é feito também na internacionalização: em 1998, o negócio no exterior já contribui 35,2 por cento do volume de negócios total. Em 1999, 16 lojas globais de alimentos abrir no exterior. A percentagem do volume de negócios gerado fora da França cresce para 40 por cento.
<br />

<br />
 <b>2000-2004</b>
<br />

<br />
 Global Food entrou no novo milênio com a publicação na revista Fortune que classificou a empresa quinto em sua \" global Mais Admiradas All- Stars\" lista. Pela primeira vez , o grupo lança o seu relatório financeiro para o ano de 2000, em conformidade com as Normas Internacionais de Contabilidade ( IAS ) , para alcançar uma maior transparência em sua contabilidade . A empresa faz mais progressos em seu curso de expansão internacional : 80 novos locais são adicionados em 2001, incluindo a primeira loja na Rússia. Até 2004, a Global Food opera em 8 países . Negócios na Europa Oriental e na Ásia faz uma especialmente forte contribuição para o desenvolvimento positivo de suas vendas . Global Food abre sua primeira loja na Ucrânia. Sendo uma das empresas líderes no seu setor , Global Food implementa a nova tecnologia inovadora baseada em computador para facilitar mais eficiente gestão de armazém, durante a prestação de compras mais rápido, mais individual e conveniente. A partir de agosto de 2004, os consumidores podem experimentar o futuro do varejo na loja Global Food em Paris.
<br />

<br />
 <b>2005-2006</b>
<br />

<br />
 Global Food marcou uma viragem significativa em 2005, com um novo compromisso de trazer a sustentabilidade ambiental em seus negócios. Global Food criado lojas experimentais que economizam energia, conservação de recursos naturais e reduzir a poluição. A empresa atendeu mais de 13 milhões de clientes por semana na Europa. Em 2006, o número de clientes semanais cresceu para mais de 17 milhões. Global Food teve vendas líquidas recordes de US \$ 34 bilhões.
<br />

<br />
 <b>2007</b>
<br />

<br />
 Global Food continua a sua expansão internacional com a abertura da primeira loja no Paquistão e Turquia. A fim de ajudar os clientes a ter uma dieta equilibrada, Global Food está implementando fatos de nutrição para as suas próprias marcas. A empresa também dá um exemplo em relação ao equilíbrio entre trabalho e família.";
?>