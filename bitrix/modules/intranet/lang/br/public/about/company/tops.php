<?
$MESS["ABOUT_TITLE"] = "Direção";
$MESS["ABOUT_TOP1_INFO"] = "<p></p>
<h2>Atkinson Morgan</h2>
<p><b>Chefe Executivo</b></p>
<p>Morgan foi nomeado em junho de 2004. Antes de 2004, ele trabalhou em uma série de redes de supermercados, como um executivo. Em 1999, ele se juntou a comida não global (agora fundido) e imediatamente implementada uma estratégia global. Ele também trabalhou em questões de política de gestão de fundos de alimentos, armazenamento de propriedade e da Qualidade Alimentar padrão antes de se juntar a nós em 2003.</p>";
$MESS["ABOUT_TOP2_INFO"] = "<p></p>
<h2>Milburn Leo</h2>
<p><b>Diretor de Investimento</b></p>
<p>Leo trabalhou no setor de produção de alimentos e em uma variedade de posições em Comunidades do Alimento. Foi diretor de operação, em seguida, diretor da área de 1997-2004, levando a oferta pública na bolsa de Paris. Como Chefe de Investimento, Bertram supervisionou a expansão do programa de investimentos de alimentos a preços acessíveis e introdução do patrimônio comum para ajudar os compradores de primeira vez. </p>";
$MESS["ABOUT_TOP3_INFO"] = "<p></p>
<h2>Tharp Andrew</h2>
<p><b>Regulação e Diretor de Inspeção</b></p>
<p>Andrew foi nomeado diretor do Regulamento e Inspeção em junho de 2003. Andrew tem trabalhado na indústria de alimentos nos últimos 10 anos, passando de política e planejamento em regulação da produção de alimentos e inspeção. </p>
<p>este papel conosco, ele está desenvolvendo um novo quadro para avaliar o desempenho da Global Food.</p>";
?>