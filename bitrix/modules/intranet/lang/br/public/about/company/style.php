<?
$MESS["ABOUT_TITLE"] = "Cultura Corporativa";
$MESS["ABOUT_INFO1"] = "<p>Nossa cultura corporativa é a fonte dos resultados que produzimos. É que somos como uma organização, e o contexto em que estamos inseridos. Estamos empenhados em capacitar e inspirar cada um dos membros de nossa equipe ter avanços e alcançar resultados extraordinários, tanto interna como nossos parceiros de negócios.</p>
<p>Nós conscientemente trabalhar como uma equipe para criar esta cultura corporativa, um ambiente de trabalho que incentiva a assumir responsabilidades e resultados em:</p>
<p><b>Capacitando o outro</b></p>
<p>Estamos comprometidos com o sucesso do outro, aumentando e fortalecendo a capacidade de cada um para conseguir avanços e resultados extraordinários.</p>";
$MESS["ABOUT_INFO2"] = "<p><b>Taking responsibility</b></p>
<p>Enfatizando a criatividade ea produção total torna mais fácil para os funcionários a tomar tanta responsabilidade quanto eles podem e limita o medo do fracasso.</p>
<p><b>A comunicação aberta, honesta e completa</b></p>
<p>Fazemos todo o possível para tornar a comunicação dentro aberto e fora da empresa, eliminar as barreiras e criar oportunidades para uma nova cooperação e eficiência.</p>
<p>A nossa cultura corporativa cultiva um ambiente para todos os membros da equipe para criar consistentemente valor para nossa linha de fundo, 
empresas do nosso portfólio e, em última análise, por si mesmos. Também estamos comprometidos a inspirar e capacitar as empresas em que investimos para ter resultados inovadores.</p>";
?>