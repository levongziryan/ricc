<?
$MESS["ABOUT_TITLE"] = "Oportunidades de Carreira";
$MESS["ABOUT_DET_PAGE_TITLE"] = "Página";
$MESS["ABOUT_PAGE_TITLE"] = "Vagas";
$MESS["ABOUT_INFO"] = "<p>Depois de ter visto a vaga que você está interessado em candidatar-se, por favor preencha o <a href=\"resume.php\" >formulário de solicitação</a>, ou envie seu  CV para <a href=\"mailto:hr@example.com\" >hr@example.com</a>. Qualquer informação que você fornecer será mantida confidencial.</p>

<p>Se você tem alguma dúvida, não hesite em contatar o Departamento de RH:</p>

<p><a href=\"mailto:hr@example.com\" >hr@example.com</a></p>
";
?>