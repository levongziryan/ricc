<?
$MESS["MENU_CRM_DESKTOP"] = "Desktop CRM";
$MESS["MENU_CRM_STREAM"] = "Fluxo de Atividades do CRM";
$MESS["MENU_CRM_ACTIVITY"] = "Minhas Atividades";
$MESS["MENU_CRM_CONTACT"] = "Contatos";
$MESS["MENU_CRM_COMPANY"] = "Empresas";
$MESS["MENU_CRM_DEAL"] = "Negócios";
$MESS["MENU_CRM_QUOTE"] = "Orçamentos";
$MESS["MENU_CRM_INVOICE"] = "Faturas";
$MESS["MENU_CRM_LEAD"] = "Leads";
$MESS["MENU_CRM_PRODUCT"] = "Catálogo";
$MESS["MENU_CRM_HISTORY"] = "Histórico";
$MESS["MENU_CRM_REPORT"] = "Relatórios";
$MESS["MENU_CRM_FUNNEL"] = "Funil de Vendas";
$MESS["MENU_CRM_SETTINGS"] = "Configurações";
?>