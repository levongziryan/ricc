<?
$MESS["CRM_TITLE"] = "Ajuda";
$MESS["CRM_INFO"] = "<h3>O que é CRM?</h3>
<p><b>Gerenciamento de Relação com o cliente</b> (<b>CRM</b>) é um sistema que visa
o aumento das vendas , otimizando os esforços de marketing e melhorar o serviço ao cliente . CRM
mantém os dados dos clientes e histórico de relacionamento para análise posterior. </p>
<p>Com CRM , gerente atende a um cliente em potencial tanto como viajar de um Lead para
negócio bem sucedido. </p>
<p> É comum usar os seguintes termos quando se trata de CRM. </p>
<ul>
        <li><b>Contato</b>: a registros que contêm dados sobre uma pessoa com quem os
      gestores da empresa teve qualquer tipo de comunicação empresarial. </li>
<li><b>Empresa</b>: um registro com os dados sobre uma empresa que tem (ou teve) qualquer
      forma de relação de negócio com sua empresa. </li>
<li> <b>Lead</b>: informações sobre qualquer tipo de atividade de comunicação (telefone
      falar , e-mails , atendendo etc ), que tem potencial para se tornar um negocio bem-sucedido
      . </li>
<li><b>Evento</b>: descreve qualquer alteração feita a um contato , uma ligação ou uma
      empresa . Por exemplo, adicionar um novo endereço de e-mail . </li>
<li><b> Negócio </b>: um registro que contém dados sobre as atividades desenvolvidas para
      um cliente e teve como objetivo completar uma transação comercial desejado ( por exemplo, um
      venda ) . </li>
</ul>
<h3>Como isso funciona? </h3>
<p>CRM pode ser usado:</p>
<ol>
        <li>como um contato e empresa <b>bando de dados</b>, </li>

        <li> como clássico <b>CRM</b>.</li>
</ol>
<h4>1. CRM como um banco de dados </h4>
<p> CRM pode ser usado um banco de dados para armazenar contatos e dados da empresa e manter um
história das relações. Aqui, as principais entidades são um contato e uma empresa em
qual as relações são definidas através da criação de Eventos . Observe que mesmo se você
usar seu CRM apenas como uma base de dados , ainda é possível derivar uma vantagem quando um
resultado de um evento e, finalmente, converter que levam a um acordo. </p>
<p><img height='430' border='0' width='900' src='/upload/crm/cim/01.png' /></p>
<h4>2. Clássico CRM</h4>
<p> Em um sistema de CRM clássico , a entidade de origem é uma vantagem que pode ser adicionado
manualmente por um gerente , ou do Administrador do Site Bitrix ou outra fonte . Uma vez adicionado,
o lead pode ser convertido para um contacto ou de uma empresa , que são representados por uma
entrada de banco de dados comum. No entanto, se o lead passa todas as etapas de uma venda
funil, torna-se então um acordo.</p>
<p><img height='430' border='0' width='900' src='/upload/crm/cim/03.png'  /></p>";
?>