<?
$MESS["CRM_TITLE"] = "Configurações";
$MESS["CRM_MENU_STATUS"] = "Listas de seleção";
$MESS["CRM_MENU_CURRENCY"] = "Moedas";
$MESS["CRM_MENU_TAX"] = "Taxas";
$MESS["CRM_MENU_LOCATIONS"] = "Endereços";
$MESS["CRM_MENU_PS"] = "Meios de pagamento";
$MESS["CRM_MENU_PERMS"] = "Permissões de acesso";
$MESS["CRM_MENU_BP"] = "Processos de negócio (BPM)";
$MESS["CRM_MENU_FIELDS"] = "Campos personalizados";
$MESS["CRM_MENU_CONFIG"] = "Outras configurações";
$MESS["CRM_MENU_SENDSAVE"] = "Integração com E-mail";
$MESS["CRM_MENU_SALE"] = "e-Stores";
$MESS["CRM_MENU_MAILTEMPLATE"] = "Modelos de E-mail ";
$MESS["CRM_MENU_MEASURE"] = "Unidades de medida";
$MESS["CRM_MENU_INFO"] = "Ajuda";
$MESS["CRM_MENU_PRODUCT_PROPS"] = "Características do produto";
?>