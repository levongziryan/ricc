<?
$MESS["INTRANET_USER_INVITATION_NAME"] = "Convidar pessoas";
$MESS["INTRANET_USER_INVITATION_SUBJECT"] = "Bem-vindo à Intranet";
$MESS["INTRANET_USER_INVITATION_MESSAGE"] = "#USER_TEXT#

#LINK#

Utilize o seu endereço de e-mail como nome de usuário para autenticação. Você vai ter que vir com sua senha pessoal na autenticação pela primeira vez.";
$MESS["CALENDAR_INVITATION_NAME"] = "Convite";
$MESS["CALENDAR_INVITATION_DESC"] = "#EMAIL_TO# - E-mail do convidado
#TITLE# - Agenda
#MESSAGE# - Detalhes do evento
";
$MESS["CALENDAR_INVITATION_AUTO_GENERATED"] = "---------------------------------------------------------------------

Esta mensagem foi gerada automaticamente.";
$MESS["INTRANET_USER_ADD_NAME"] = "Adicionar funcionários";
$MESS["INTRANET_USER_ADD_DESC"] = "#EMAIL_TO# - endereços de e-mail de novos funcionários
#LINK# - intranet URL";
$MESS["INTRANET_USER_ADD_SUBJECT"] = "Você foi adicionado ao site da intranet";
$MESS["INTRANET_USER_ADD_MESSAGE"] = "#USER_TEXT#

#LINK#

Para acessar, use seu e-mail como login e esta senha: #PASSWORD#";
$MESS["INTRANET_USER_INVITATION_DESC"] = "#EMAIL_TO# - os endereços de e-mail das pessoas convidades
 #LINK# - link de ativação de novo funcionário";
$MESS["INTRANET_MAILDOMAIN_NOCOMPLETE_NAME"] = "Concluir adição de um domínio de e-mail";
$MESS["INTRANET_MAILDOMAIN_NOCOMPLETE_DESC"] = "#EMAIL_TO# - E-Mail do administrador 
#LEARNMORE_LINK# - URL da documentação
 #SUPPORT_LINK# - URL de suporte técnico";
$MESS["INTRANET_MAILDOMAIN_NOCOMPLETE_SUBJECT"] = "Você não concluiu a adição do seu domínio";
$MESS["INTRANET_MAILDOMAIN_NOCOMPLETE_MESSAGE"] = "<p>Percebemos que você fez uma tentativa de criar um domínio de empresa, mas nunca concluiu o registro.</p> 

<p>Se você precisar de ajuda, abra a página ler configurações de e-mail para obter instruções detalhadas para configurar seu serviço de e-mail e concluir seu registro de domínio do Bitrix24.</p> 

<p>Se você quiser ver como é feito normalmente, temos uma série de exemplos de uso de domínios de e-mail para você ver. Os exemplos vão revelar a mágica por trás do registro de domínio, criação de uma caixa de e-mail e a utilização total do Bitrix24.</p> 

<p><a href=\"#LEARNMORE_LINK#\">Saiba mais</a></p> 

<p>Se você ainda tiver perguntas, entre em contato com o nosso <a href=\"#SUPPORT_LINK#\">suporte técnico</a>.</p>";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX_NAME"] = "Criar caixas de correio no domínio";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX_DESC"] = "#EMAIL_TO# - E-Mail do administrador 
#LEARNMORE_LINK# - URL da documentação
#SUPPORT_LINK# - URL de suporte técnico";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX_SUBJECT"] = "Seu e-mail corporativo no Bitrix24";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX_MESSAGE"] = "<p>Seu domínio foi adicionado com sucesso ao Bitrix24, mas ainda não existe caixa de correio nele.</p> 
<p>Temos uma série de exemplos de uso de domínios de e-mail para você ver. Com os exemplos, você vai aprender a criar uma caixa de correio e utilizá-la com o Bitrix24.</p> 

<p><a href=\"#LEARNMORE_LINK#\">Saiba mais</a></p> 

<p>Se você ainda tiver perguntas, entre em contato com o nosso<a href=\" #SUPPORT_LINK#\">suporte técnico</a>.</p>
";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_NAME"] = "Criar caixas de correio dos funcionários no domínio";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_DESC"] = "#EMAIL_TO# - E-Mail do administrador 
#LEARNMORE_LINK# - URL da documentação
#SUPPORT_LINK# - URL de suporte técnico";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_SUBJECT"] = "E-mails corporativos para os seus funcionários";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_MESSAGE"] = "<p>Você registrou o domínio e criou sua caixa de correio. No entanto, seus funcionários ainda não têm a menor idéia que eles podem criar suas próprias caixas de correio no domínio da empresa.</p> 

<p>Compartilhe seu conhecimento e ajude-os a aprender a criar suas próprias caixas de correio ou registrar caixas de correio em sua página Bitrix24.</p> 

<p><a href=\"#LEARNMORE_LINK#\">Como registrar caixas de correio para seus funcionários</a></p> 

<p>Se você ainda tiver perguntas, entre em contato com o nosso <a href=\"#SUPPORT_LINK#\">suporte técnico</a>.</p>";
$MESS["INTRANET_MAILDOMAIN_NOREG_NAME"] = "Registrar domínio de e-mail";
$MESS["INTRANET_MAILDOMAIN_NOREG_DESC"] = "#EMAIL_TO# - E-Mail do administrador 
#LEARNMORE_LINK# - URL da documentação
#SUPPORT_LINK# - URL de suporte técnico";
$MESS["INTRANET_MAILDOMAIN_NOREG_SUBJECT"] = "Você não concluiu a adição do seu domínio";
$MESS["INTRANET_MAILDOMAIN_NOREG_MESSAGE"] = "<p>Você foi registrar um domínio para a sua empresa mas conseguiu escolher um bom nome, ao que parece.</p> 

<p>Temos uma série de exemplos de uso de domínios de e-mail para você ver. Com os exemplos, você vai aprender a criar uma caixa de correio e utilizá-la com o Bitrix24.</p> 

<p> <a href=\"#LEARNMORE_LINK#\">Como registrar caixas de correio para os seus funcionários</a></p> 

<p>Se você ainda tiver perguntas, entre em contato com o nosso <a href= \"#SUPPORT_LINK#\">suporte técnico</a>.</p>";
?>