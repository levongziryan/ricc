<?
$MESS["INTR_MODULE_NAME"] = "Intranet";
$MESS["INTR_MODULE_DESCRIPTION"] = "Portal Intranet";
$MESS["INTR_INSTALL_TITLE"] = "Instalação de Módulo";
$MESS["INTR_PHP_L439"] = "Você está usando a versão PHP #VERS#, mas o módulo requer a versão 5.0.0 ou superior. Por favor, atualize sua instalação PHP ou entre em contato com o suporte técnico.";
$MESS["INTR_UNINSTALL_TITLE"] = "Desinstalação do Módulo de Intranet[";
$MESS["INTR_INSTALL_RATING_RULE"] = "Calcular votos extras para autoridade baseado na estrutura da empresa";
?>