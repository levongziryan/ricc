<?
$MESS["BITRIX24_THEME_DIALOG_TITLE"] = "Temas de Fundo";
$MESS["BITRIX24_THEME_DIALOG_NEW_THEME"] = "Tema de fundo personalizado";
$MESS["BITRIX24_THEME_CREATE_YOUR_OWN_THEME"] = "Criar tema de fundo personalizado";
$MESS["BITRIX24_THEME_REMOVE_THEME"] = "Eliminar tema de fundo";
$MESS["BITRIX24_THEME_DIALOG_SAVE_BUTTON"] = "Salvar";
$MESS["BITRIX24_THEME_DIALOG_CANCEL_BUTTON"] = "Cancelar";
$MESS["BITRIX24_THEME_DIALOG_CREATE_BUTTON"] = "Criar";
$MESS["BITRIX24_THEME_THEME_BG_COLOR"] = "Cor de fundo";
$MESS["BITRIX24_THEME_THEME_BG_IMAGE"] = "Imagem de fundo";
$MESS["BITRIX24_THEME_THEME_TEXT_COLOR"] = "Cor do texto";
$MESS["BITRIX24_THEME_THEME_LIGHT_COLOR"] = "Claro";
$MESS["BITRIX24_THEME_THEME_DARK_COLOR"] = "Escuro";
$MESS["BITRIX24_THEME_UPLOAD_BG_IMAGE"] = "Carregue a imagem de fundo";
$MESS["BITRIX24_THEME_DRAG_BG_IMAGE"] = "ou arraste uma imagem aqui";
$MESS["BITRIX24_THEME_WRONG_FILE_TYPE"] = "O arquivo não é uma imagem.";
$MESS["BITRIX24_THEME_FILE_SIZE_EXCEEDED"] = "Tamanho máx. do arquivo excedido (#LIMIT#).";
$MESS["BITRIX24_THEME_WRONG_BG_COLOR"] = "Cor de fundo incorreta.";
$MESS["BITRIX24_THEME_EMPTY_FORM_DATA"] = "Selecione uma imagem ou cor de fundo.";
$MESS["BITRIX24_THEME_UNKNOWN_ERROR"] = "Não é possível concluir a ação. Tente novamente.";
$MESS["BITRIX24_THEME_SET_AS_DEFAULT"] = "Definir como padrão";
$MESS["BITRIX24_THEME_DEFAULT_THEME"] = "Padrão";
?>