<?
$MESS["MENU_LIVE_FEED"] = "Fluxo de Atividades";
$MESS["MENU_TASKS"] = "Tarefas";
$MESS["MENU_BLOG"] = "Conversas";
$MESS["MENU_FILES"] = "Arquivos";
$MESS["MENU_EMPLOYEE"] = "Funcionários";
$MESS["MENU_CONTACT"] = "Contatos";
$MESS["MENU_FAVOURITE"] = "FAVORITOS";
$MESS["MENU_GROUPS_EXTRANET"] = "Grupos Extranet";
$MESS["MENU_COMPANY"] = "EMPRESA";
$MESS["MENU_GROUPS"] = "Grupos de Trabalho";
$MESS["MENU_GROUPS_EXTRANET_ALL"] = "Todos os Grupos de Trabalho";
?>