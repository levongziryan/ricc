<?
$MESS["MENU_TELEPHONY_BALANCE"] = "Saldo e Estatísticas";
$MESS["MENU_TELEPHONY"] = "Configurações de Telefonia";
$MESS["MENU_TELEPHONY_PERMISSIONS"] = "Permissões de Acesso";
$MESS["MENU_TELEPHONY_LINES"] = "Números de Telefone";
$MESS["MENU_TELEPHONY_USERS"] = "Usuários de Telefonia";
$MESS["MENU_TELEPHONY_PHONES"] = "Telefones SIP";
$MESS["MENU_TELEPHONY_CONNECT"] = "Conexão";
$MESS["MENU_TELEPHONY_IVR"] = "Configurar IVR";
?>