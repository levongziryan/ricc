<?
$MESS["TITLE"] = "Configurações";
$MESS["STATUS"] = "Listas de seleção";
$MESS["CURRENCY"] = "Moedas";
$MESS["TAX"] = "Impostos";
$MESS["PS"] = "Pagamentos e faturas";
$MESS["LOCATIONS"] = "Endereços";
$MESS["PERMS"] = "Permissões de acesso";
$MESS["BP"] = "Processos de Negócios";
$MESS["FIELDS"] = "Campos personalizados";
$MESS["CONFIG"] = "Configurações";
$MESS["SENDSAVE"] = "Enviar e Salvar Integração";
$MESS["MEASURE"] = "Unidades de medida";
$MESS["EXTERNAL_SALE"] = "Lojas da web";
$MESS["MAIL_TEMPLATES"] = "Modelos de e-mail";
$MESS["PRODUCT_PROPS"] = "Características do produto";
$MESS["EXCH1C"] = "Integração 1C";
$MESS["SLOT"] = "Relatórios Analíticos";
?>