<?
$MESS["CONFIG_TITLE"] = "Configurações";
$MESS["CONFIG_USERS_COUNT"] = "Usuários";
$MESS["CONFIG_USERS_COUNT_LIMIT"] = "Máximo de usuários:";
$MESS["CONFIG_USERS"] = "Usuários:";
$MESS["CONFIG_EXTRANET_USERS"] = "Usuários Extranet";
$MESS["CONFIG_USERS_FREE"] = "Contas de Usuários de Férias:";
$MESS["CONFIG_DISC_SPACE"] = "Espaço de armazenamento em nuvem";
$MESS["CONFIG_DISC_SPACE_LIMIT"] = "Espaço em disco";
$MESS["CONFIG_DISC_USAGE"] = "O espaço em disco utilizado";
$MESS["CONFIG_DB_USAGE"] = "Tamanho do banco de dados";
$MESS["CONFIG_DISC_SPACE_FREE"] = "Espaço livre:";
$MESS["CONFIG_NO_LIMIT"] = "ilimitado";
$MESS["CONFIG_HEADER_SETTINGS"] = "Configurações";
$MESS["CONFIG_COMPANY_NAME"] = "Nome da empresa";
$MESS["CONFIG_SAVE_SUCCESSFULLY"] = "As Configurações foram atualizadas com sucesso";
$MESS["CONFIG_LICENSE_NAME"] = "Licença";
$MESS["CONFIG_LICENSE_TILL"] = " até #LICENSETILL#";
$MESS["CONFIG_SAVE"] = "Salvar";
$MESS["CONFIG_LICENSE_ACTIVATE"] = "Ativar a licença";
$MESS["CONFIG_COUPON"] = "Digite o seu cupom";
$MESS["CONFIG_LICENSE_DESCRIPTION"] = "Usar cupons para:<br>
1) ativar assinatura paga;<br>
2) Renovar assinatura paga para 6 ou 12 meses (depende da sua licença);<br>
3) mudar para outro plano tarifário.";
$MESS["CONFIG_ACTIVATE"] = "Ativar";
$MESS["CONFIG_ACTIVATE_ERROR"] = "Erro de ativação. Entre em contato com o departamento de vendas.";
$MESS["project"] = "Básico";
$MESS["team"] = "Padrão";
$MESS["company"] = "Profissional";
$MESS["nfr"] = "Licença parceiro";
$MESS["config_rating_label_likeY"] = "Texto do Botão Like não votado";
$MESS["config_rating_label_likeN"] = "Votou o texto do botão \"Like\"";
$MESS["CONFIG_EMAIL_FROM"] = "E-mail do administrador do site<br>(endereço do remetente padrão)";
$MESS["CONFIG_LOGO_24"] = "Adicionar \"24\" a logomarca da empresa";
$MESS["CONFIG_EMAIL_ERROR"] = "O E-mail do administrador do site está incorreto.";
?>