<?
$MESS["ERROR_404_TITLE"] = "Erro 404";
$MESS["ERROR_404_TEXT1"] = "Infelizmente, a página que você solicitou <br> não foi encontrada.";
$MESS["ERROR_404_TEXT2"] = "Se você acha que isso não deveria ter acontecido <br> entre em contato com o administrador do site.";
?>