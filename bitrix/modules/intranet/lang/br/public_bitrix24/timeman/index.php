<?
$MESS["TITLE"] = "Gráfico de Ausência";
$MESS["TARIFF_RESTRICTION_TEXT"] = "O gráfico de ausência fornece uma apresentação visual rápida da localização de uma pessoa e a razão da sua ausência: licença maternidade, médica ou profissional. O gráfico também é útil ao planejar férias porque todas as férias simultâneas indesejadas de várias pessoas podem ser vistas de uma só vez. É fácil de usar: licenças podem ser adicionadas por um funcionário ou gerente de RH. O sistema também pode adicionar licenças automaticamente com base nos fluxos de trabalho de aprovação de solicitação de licença.<a href=\"https://helpdesk.bitrix24.com/open/4879027/\">Informações</a>";
$MESS["TARIFF_RESTRICTION_TEXT2"] = "O gráfico de ausência está disponível nos planos Standard e Professional.";
?>