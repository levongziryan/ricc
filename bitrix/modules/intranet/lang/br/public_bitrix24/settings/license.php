<?
$MESS["CONFIG_USERS2"] = "Funcionários:";
$MESS["CONFIG_INVITED_USERS"] = "Convidados:";
$MESS["demo"] = "Modo demonstração";
$MESS["LICENSE_FEATURE_TASKS"] = "Tarefas e Projetos";
$MESS["LICENSE_FEATURE_CALENDAR"] = "Calendários";
$MESS["LICENSE_FEATURE_CRM"] = "CRM";
$MESS["LICENSE_FEATURE_MEETING"] = "Reuniões";
$MESS["LICENSE_FEATURE_WORKREPORT"] = "Relatórios";
$MESS["LICENSE_FEATURE_DOMAIN"] = "Nome próprio de domínio";
$MESS["LICENSE_PRICE_PROJECT"] = "Custo zero!";
$MESS["LICENSE_PRICE_TEAM"] = "\$99/mês";
$MESS["LICENSE_PRICE_COMPANY"] = "\$199/mês";
$MESS["LICENSE_PROLONG"] = "Alterar período";
$MESS["LICENSE_GO"] = "Comprar agora!";
$MESS["LICENSE_BUY"] = "Comprar agora!";
$MESS["LICENSE_HISTORY"] = "Histórico de Compras";
$MESS["CONFIG_LICENSE_TEST"] = "Modo demonstração";
$MESS["CONFIG_LICENSE_TEST_START"] = "Ativar";
$MESS["CONFIG_LICENSE_TEST_FINISH"] = "Desativar";
$MESS["CONFIG_LICENSE_TEST_DAYS_LEFT"] = "Faltam #NUM#.";
$MESS["LICENSE_ORDER"] = "Contatar Vendedor";
$MESS["corporation"] = "Enterprise";
$MESS["EMPTY_FIELD"] = "Campo obrigatório \"#NAME#\"";
$MESS["FIELD_name"] = "Nome";
$MESS["FIELD_last_name"] = "Sobrenome";
$MESS["CORPORATION_ORDER_CALL"] = "Contatar Vendas";
$MESS["CONFIG_DISC_CORPORATION"] = "10 Gb por usuário";
$MESS["CONFIG_USER_CORPORATION"] = "a partir de 250 usuários";
$MESS["LICENSE_FEATURE_IP"] = "Limitações de acesso por IP";
$MESS["LICENSE_FEATURE_ADIT_SUPPORT"] = "Suporte estendido";
$MESS["LICENSE_FEATURE_LOCAL_COPY"] = "Backup local";
$MESS["LICENSE_FEATURE_ADIT"] = "Disponível à um custo adicional:";
$MESS["CONFIG_USERS_COUNT"] = "Usuários";
$MESS["CONFIG_USERS"] = "Usuários";
$MESS["CONFIG_EXTRANET_USERS"] = "Usuários Extranet";
$MESS["CONFIG_USERS_FREE"] = "Contas de Usuários de Férias";
$MESS["CONFIG_DISC_SPACE"] = "Espaço de armazenamento em nuvem";
$MESS["CONFIG_DISC_SPACE_LIMIT"] = "Espaço em disco";
$MESS["CONFIG_DISC_USAGE"] = "O espaço em disco utilizado";
$MESS["CONFIG_DISC_SPACE_FREE"] = "Espaço livre";
$MESS["CONFIG_DISC_SPACE_GB"] = "Gb";
$MESS["CONFIG_NO_LIMIT"] = "ilimitado";
$MESS["CONFIG_12_LIMIT"] = "12 usuários";
$MESS["CONFIG_SAVE_SUCCESSFULLY"] = "As Configurações foram atualizadas com sucesso";
$MESS["CONFIG_LICENSE_NAME"] = "Licença";
$MESS["CONFIG_LICENSE_TILL"] = "Até #LICENSETILL#";
$MESS["CONFIG_SAVE"] = "Salvar";
$MESS["CONFIG_ACTIVATE"] = "Ativar";
$MESS["project"] = "Free";
$MESS["team"] = "Standard";
$MESS["company"] = "Professional";
$MESS["nfr"] = "NFR";
$MESS["LICENSE_FEATURE_TIMEMAN"] = "Gestão do Tempo";
$MESS["CONFIG_LICENSE_TEST_START_DESCRIPTION"] = "Você pode testar o plano profissional por 30 dias";
$MESS["CONFIG_LICENSE_TEST_FINISH_DESCRIPTION"] = "Você está usando o modo de demonstração do plano Professional.";
$MESS["CONFIG_LICENSE_TEST_FINISH_DESCRIPTION_CONFIRM"] = "Aviso! Você está deixando o modo de demonstração. Você não poderá usar o modo de demonstração após sair.";
$MESS["FIELD_post_info"] = "Cargo";
$MESS["FIELD_phone"] = "Telefone";
$MESS["FIELD_phone_time"] = "Horário preferencial para contato";
$MESS["FIELD_company"] = "Empresa";
$MESS["FIELD_empl_num"] = "Número de colaboradores";
$MESS["FIELD_questions"] = "Assuntos do seu interesse";
$MESS["FIELD_law"] = "Eu li e concordo com os <a href=\"http://www.bitrix24.ru/anketa_info.php\" target=\"blank\">termos e condições</a>.";
$MESS["REQUEST_OK"] = "Obrigado! Sua solicitação foi recebida. Um dos nossos representantes irá contatá-lo em breve.";
$MESS["LICENSE_FEATURE_LDAP"] = "Integração com AD/LDAP e MS Exchange";
$MESS["LICENCE_12_USERS_ADD"] = "Mais usuários";
$MESS["LICENCE_SPACE_ADD"] = "Mais espaço em disco";
$MESS["CONFIG_TITLE"] = "Informações sobre licenças";
$MESS["CONFIG_USERS_COUNT_LIMIT"] = "Máximo de usuários";
$MESS["CONFIG_DB_USAGE"] = "Tamanho do banco de dados";
$MESS["CONFIG_HEADER_SETTINGS"] = "Configurações";
$MESS["CONFIG_COMPANY_NAME"] = "Nome da empresa";
$MESS["CONFIG_LICENSE_ACTIVATE"] = "Ativar a licença";
$MESS["CONFIG_COUPON"] = "Digite o seu cupom";
$MESS["CONFIG_ACTIVATE_ERROR"] = "Erro de ativação. Entre em contato com o departamento de vendas.";
$MESS["LICENSE_FEATURE_ADIT_INFO"] = "<ul style=\"margin: 0; padding: 0; margin-top: 10px; margin-left: 28px; line-height: 17px;\"><li>Customização para a empresa</li><li>Private cloud</li><li>Acesso via VPN</li></ul><a href=\"http://www.bitrix24.com/prices/enterprise.php\" style=\"margin-left: 11px;\">E mais...</a>";
$MESS["CONFIG_EXTRANET_USERS2"] = "Usuários externos (extranet):";
$MESS["LICENSE_DEMO"] = "Ative 30 dias de teste	";
$MESS["CONFIG_LICENSE_TEST_ALL_TARIFF"] = "Upgrade";
$MESS["LICENSE_TF_FEATURES_DESC"] = "<div class=\"bpp-cnr\">
			<div class=\"bpp-title\">
				Receba mais com Bitrix24 Plus
			</div>
			<div class=\"bpp-content-cnr\">
				<div class=\"bpp-content-header-cnr\">
					<div class=\"bpp-content-header-item-cnr\">
						<div class=\"bpp-content-header-item-title\">Ferramentas de negócio:</div>
						<div class=\"bpp-content-header-item-value\">24 usuários</div>
					</div>
					<div class=\"bpp-content-header-item-cnr\">
						<div class=\"bpp-content-header-item-title\">Armazenamento em nuvem:</div>
						<div class=\"bpp-content-header-item-value\">24 GB</div>
					</div>
					<div class=\"bpp-content-header-item-cnr\">
						<div class=\"bpp-content-header-item-title\">Preço:</div>
						<div class=\"bpp-content-header-item-value\">#PRICE#</div>
					</div>
				</div>
				<div class=\"bpp-content-body-cnr\">
					<div class=\"bpp-content-body-item-cnr\">
						<div class=\"bpp-content-body-item-icon crm\"></div>
						<div class=\"bpp-content-body-item-title\">CRM</div>
						<div class=\"bpp-content-body-item-advantages-cnr\">
							<ul class=\"bpp-content-list\">
								<li class=\"bpp-content-list-item\">Histórico de CRM</li>
								<li class=\"bpp-content-list-item\">Log de acesso</li>
								<li class=\"bpp-content-list-item\">Busca expandida duplicada</li>
								<li class=\"bpp-content-list-item\">e mais</li>
							</ul>
						</div>
					</div>
					<div class=\"bpp-content-body-item-cnr\">
						<div class=\"bpp-content-body-item-icon phone\"></div>
						<div class=\"bpp-content-body-item-title\">Telefone</div>
						<div class=\"bpp-content-body-item-advantages-cnr\">
							<ul class=\"bpp-content-list\">
								<li class=\"bpp-content-list-item\">Gravação de chamada ilimitada</li>
								<li class=\"bpp-content-list-item\">Relatórios de fonte da chamada</li>
								<li class=\"bpp-content-list-item\">Avaliação de qualidade da chamada</li>
								<li class=\"bpp-content-list-item\">e mais</li>
							</ul>
						</div>
					</div>
					<div class=\"bpp-content-body-item-cnr\">
						<div class=\"bpp-content-body-item-icon task\"></div>
						<div class=\"bpp-content-body-item-title\">Tarefas</div>
						<div class=\"bpp-content-body-item-advantages-cnr\">
							<ul class=\"bpp-content-list\">
								<li class=\"bpp-content-list-item\">Dependências da tarefa</li>
								<li class=\"bpp-content-list-item\">e mais</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>";
$MESS["CONFIG_DISC_GB"] = "#NUM#GB";
$MESS["CONFIG_DISC_SPACE_LIMIT_DESC"] = "(atualizações diárias de dados)";
$MESS["CONFIG_LICENSE_COUPON_DESC"] = "Use cupons para:<br>
 1) ativar uma assinatura paga;<br>
 2) prolongar uma assinatura paga para 1, 6 ou 12 meses (no que diz respeito à sua opção anterior);<br>
 3) mudar para outro plano;<br>
 4) aumentar o espaço cloud do drive;<br>
 5) recarregar o saldo do seu telefone;
 6) instalar soluções comerciais do mercado.";
$MESS["tf"] = "Plus";
$MESS["LICENSE_6_12_DISCOUNT"] = "5% a 10% de desconto para pagamento de assinaturas de 6 ou 12 meses";
$MESS["LICENSE_TITLE"] = "Escolha um plano Bitrix24";
$MESS["LICENSE_MAIN_DESC"] = "Obtenha acesso a recursos avançados de CRM e de telefonia depois de fazer o upgrade para a assinatura comercial Bitrix24.
<br/><br/>Comparar planos";
$MESS["LICENSE_TARIFF"] = "Tarifa";
$MESS["LICENSE_FEATURE_PRICE"] = "Preço";
$MESS["LICENSE_FEATURE_USERS"] = "Usuários";
$MESS["LICENSE_FEATURE_BUSINESS_USERS"] = "Usuários de ferramenta empresariais";
$MESS["LICENSE_FEATURE_SPACE"] = "Espaço em disco";
$MESS["LICENSE_FEATURES_TITLE"] = "Recursos";
$MESS["LICENSE_FEATURES_SOCNET_TITLE"] = "Ferramentas de comunicação";
$MESS["LICENSE_FEATURES_SOCNET_TITLE2"] = "Comunicações";
$MESS["LICENSE_FEATURES_BUSINESS_TOOLS_TITLE"] = "Ferramentas de negócio";
$MESS["LICENSE_FEATURES"] = "Recursos";
$MESS["LICENSE_FEATURE_LIFE_FEED"] = "Fluxo de Atividade";
$MESS["LICENSE_FEATURE_IM"] = "Bate-papo e Ligações";
$MESS["LICENSE_FEATURE_MAIL"] = "E-mail";
$MESS["LICENSE_FEATURE_NETWORK"] = "Rede";
$MESS["LICENSE_FEATURE_HR"] = "RH";
$MESS["LICENSE_FEATURE_MOBILE"] = "Aplicativo móvel";
$MESS["LICENSE_FEATURE_DISK"] = "Drive";
$MESS["LICENSE_FEATURE_TELEPHONY"] = "Telefonia";
$MESS["LICENSE_FEATURE_BP"] = "Automação do fluxo de trabalho";
$MESS["LICENSE_FEATURE_LOGO24"] = "Remover o logotipo Bitrix24";
$MESS["LICENSE_FEATURE_LOGO"] = "Inserir seu próprio logotipo";
$MESS["LICENSE_FEATURE_BACKUP"] = "Restaurar backup";
$MESS["LICENSE_FEATURE_LOGS"] = "Logs de acesso & trilha de auditoria";
$MESS["LICENSE_FEATURE_DOMEN"] = "Use seu próprio domínio";
$MESS["LICENSE_SETTINGS_TITLE"] = "Configurações";
$MESS["LICENSE_CRM_TITLE"] = "CRM Avançado";
$MESS["LICENSE_ADD_TITLE"] = "Serviços Premium";
$MESS["LICENSE_ADD_DESC"] = "Às vezes você pode precisar da ajuda da assistência técnica Bitrix24 para serviços premium que estão disponíveis apenas para usuários comerciais";
$MESS["LICENSE_CRM_FULL"] = "CRM Avançado";
$MESS["LICENSE_CRM_DESC"] = "CRM avançado oferece a você opções extras como histórico de alterações, restaurar, log de acesso de CRM, conversões de registro de CRM e muito mais.";
$MESS["LICENSE_CRM_FEATURE_1"] = "CRM Básico: Clientes Potenciais, Contatos, Empresas, Negociações";
$MESS["LICENSE_CRM_FEATURE_2"] = "Cotações";
$MESS["LICENSE_CRM_FEATURE_3"] = "Faturas";
$MESS["LICENSE_CRM_FEATURE_4"] = "Catálogo de produtos";
$MESS["LICENSE_CRM_FEATURE_5"] = "Importar";
$MESS["LICENSE_CRM_FEATURE_6"] = "E-mail para o cliente";
$MESS["LICENSE_CRM_FEATURE_7"] = "Agendamento";
$MESS["LICENSE_CRM_FEATURE_8"] = "Relatórios";
$MESS["LICENSE_CRM_FEATURE_9"] = "Canal de vendas";
$MESS["LICENSE_CRM_FEATURE_10"] = "Integração de telefonia";
$MESS["LICENSE_CRM_FEATURE_11"] = "Integração com a loja na Internet";
$MESS["LICENSE_CRM_FEATURE_12"] = "Automação do fluxo de trabalho";
$MESS["LICENSE_CRM_FEATURE_13"] = "Converter entre negociações, faturas e orçamentos de CRM";
$MESS["LICENSE_CRM_FEATURE_14"] = "Pesquisa avançada duplicada";
$MESS["LICENSE_CRM_FEATURE_15"] = "Mudar histórico e restaurar";
$MESS["LICENSE_CRM_FEATURE_16"] = "Log de acesso de CRM";
$MESS["LICENSE_CRM_FEATURE_17"] = "Exibição de lista superior a 5000 registros";
$MESS["LICENSE_CURRENCY_RUR"] = "#NUM# RUB";
$MESS["LICENSE_CURRENCY_EUR"] = "#NUM#&euro;";
$MESS["LICENSE_CURRENCY_USD"] = "\$#NUM#.";
$MESS["DEMO_TITLE"] = "Avaliação gratuita de 30 dias ";
$MESS["CONFIG_LICENSE_TEST_NOT_ALLOWED"] = "Você já usou a versão de avaliação do plano Profissional ";
$MESS["DEMO_DESC"] = "Ative o período de avaliação gratuita de 30 dias para obter acesso a todos os recursos do plano Profissional Bitrix24 ";
$MESS["DEMO_START"] = "Ativar avaliação";
$MESS["DEMO_DESC2"] = "Você vai receber os seguintes recursos ativados:";
$MESS["DEMO_FEATURE_1"] = "Número ilimitado de usuários";
$MESS["DEMO_FEATURE_2"] = "Espaço de armazenamento ilimitado";
$MESS["DEMO_FEATURE_3"] = "CRM Avançado";
$MESS["DEMO_FEATURE_4"] = "Automação do fluxo de trabalho";
$MESS["DEMO_FEATURE_5"] = "Rastreamento de tempo";
$MESS["DEMO_FEATURE_6"] = "Relatórios de trabalho";
$MESS["DEMO_FEATURE_7"] = "Reuniões e briefings";
$MESS["DEMO_FEATURE_8"] = "e mais";
$MESS["DEMO_INFO_TITLE"] = "O que acontece quando a avaliação acabar?";
$MESS["DEMO_INFO_DESC"] = "Quando a avaliação acabar, sua conta voltará automaticamente para o plano gratuito. <b>Todos os dados serão mantidos</b> (para qualquer ferramenta avançada que você usou durante a avaliação), no entanto o acesso a ela <b>pode ser limitado</b>. O acesso a esses dados serão restaurados automaticamente quando você fizer o upgrade para um plano Bitrix24 adequado.";
$MESS["DEMO_INFO_1"] = "Usuários";
$MESS["DEMO_INFO_2"] = "Gravações de chamadas";
$MESS["DEMO_INFO_3"] = "Armazenamento na nuvem";
$MESS["DEMO_INFO_4"] = "Limite de visualização dos registros de CRM";
$MESS["DEMO_INFO_DESC_1"] = "Contas gratuitas fornecem acesso a ferramentas de negócios, como CRM, tarefas ou gerenciamento de documentos, apenas para 12 usuários de sua escolha. Se você deseja fornecer acesso a ferramentas de negócios para mais usuários, você <a class=\"link\" href=\"/settings/license_all.php\">deve atualizar sua conta</a>. ";
$MESS["DEMO_INFO_DESC_2"] = "O limite de gravações de chamadas telefônicas é aumentado durante a avaliação. O limite para o plano gratuito é de 100 gravações por mês, por isso se você gravou 40 chamadas durante a avaliação, o restante para o mês será 60 gravações. Para aumentar o limite, você precisa <a class=\"link\" href=\"/settings/license_all.php\">fazer o upgrade</a>.";
$MESS["DEMO_INFO_DESC_4"] = "Todos os planos Bitrix24 vêm com registros ilimitados de CRM. Porém o plano gratuito tem limite de visualização de 5000 registros da lista. Se você tiver adicionado mais de 5000 registros durante a avaliação, todos os registros estarão acessíveis, mas seu limite de visualização da lista será definido para não mais do que 5000 registros ao mesmo tempo. Para aumentar esse limite, você precisa <a class=\"link\" href=\"/settings/license_all.php\">fazer o upgrade</a>.";
$MESS["LICENSE_BILLING_CURRENCY"] = "Mostrar preços em:";
$MESS["LICENSE_BILLING_CURRENCY_RUR"] = "Rublo Russo";
$MESS["LICENSE_BILLING_CURRENCY_USD"] = "Dólar americano";
$MESS["LICENSE_BILLING_CURRENCY_EUR"] = "Euro";
$MESS["LICENSE_BILLING_CURRENCY_BRL"] = "Reais Brasileiros";
$MESS["LICENSE_BILLING_CURRENCY_CNY"] = "Iuan Renmimbi Chinês";
$MESS["LICENSE_BILLING_CURRENCY_INR"] = "Rupia Indiana";
$MESS["LICENSE_BILLING_CURRENCY_UAH"] = "Hryvnia Ucraniana";
$MESS["LICENSE_PRICE_AUTO"] = "#PRICE#/mês";
$MESS["LICENSE_CRM_SHOW_MORE"] = "Mostrar recursos de CRM para vários planos";
$MESS["LICENSE_TEL_SHOW_MORE"] = "Mostrar recursos de Telefonia para vários planos";
$MESS["LICENSE_TASKS_SHOW_MORE"] = "Mostrar recursos de Tarefa para vários planos";
$MESS["LICENSE_HIDE_MORE"] = "ocultar tabela";
$MESS["LICENSE_TARIFF_BACK"] = "voltar para planos";
$MESS["LICENSE_TARIFF_COMPARE"] = "comparar planos";
$MESS["LICENSE_EXTENDED_TOOLS"] = "Ferramentas avançadas";
$MESS["LICENSE_CHOOSE_TARIFF"] = "Selecionar plano";
$MESS["CONFIG_USERS_COUNT_BUSINESS_TOOLS_LIMIT"] = "Tem acesso a ferramentas de negócio:";
$MESS["CONFIG_USERS_BUSINESS_TOOLS_COUNT"] = "Efetivamente usa ferramentas de negócio:";
$MESS["DEMO_POPUP_TITLE"] = "Compartilhar as novidades com todos?";
$MESS["DEMO_POPUP_SHARE"] = "Compartilhar as novidades";
$MESS["DEMO_POPUP_CLOSE"] = "Não, obrigado";
$MESS["LICENSE_FEATURE_TIMEREPORT"] = "Folhas de ponto";
$MESS["LICENSE_FEATURE_SOON"] = "em breve";
$MESS["LICENSE_CRM_FEATURE_18"] = "Discador progressivo (em breve)";
$MESS["LICENSE_CRM_FEATURE_20"] = "Disparar e-mails (em breve)";
$MESS["LICENSE_CRM_FEATURE_21"] = "Documentos de CRM Avançado (em breve)";
$MESS["LICENSE_CRM_FEATURE_22"] = "e muito mais (em breve)";
$MESS["LICENSE_TEL_TITLE"] = "Telefonia Bitrix24 Avançada";
$MESS["LICENSE_TEL_FULL"] = "Telefonia Avançada";
$MESS["LICENSE_TEL_DESC"] = "Telefonia avançada permite gravar mais de 100 chamadas por mês, rastrear diversos canais de marketing e obter acesso a outras ferramentas";
$MESS["LICENSE_TEL_FEATURE_2"] = "Aluguel de número de telefone local e gratuito";
$MESS["LICENSE_TEL_FEATURE_3"] = "Número de telefone de retransmissão";
$MESS["LICENSE_TEL_FEATURE_4"] = "Linhas ilimitadas de recebimento de chamadas";
$MESS["LICENSE_TEL_FEATURE_5"] = "Roteamento de chamada";
$MESS["LICENSE_TEL_FEATURE_6"] = "Transferência de chamada";
$MESS["LICENSE_TEL_FEATURE_7"] = "Integração de CRM";
$MESS["LICENSE_TEL_FEATURE_8"] = "Integração de PBX";
$MESS["LICENSE_TEL_FEATURE_9"] = "Compatível com aparelho de telefone SIP/VoIP";
$MESS["LICENSE_TEL_FEATURE_11"] = "Ramais de funcionários";
$MESS["LICENSE_TEL_FEATURE_12"] = "Horas de telefone";
$MESS["LICENSE_TEL_FEATURE_13"] = "Saudações e correio de voz";
$MESS["LICENSE_TEL_FEATURE_14"] = "Gravação de chamada (até 100 por mês)";
$MESS["LICENSE_TEL_FEATURE_15"] = "Gravação ilimitada de chamadas";
$MESS["LICENSE_TEL_FEATURE_16"] = "Chamada simultânea para todos os funcionários disponíveis";
$MESS["LICENSE_TEL_FEATURE_17"] = "Avaliação da qualidade da chamada";
$MESS["LICENSE_TEL_FEATURE_18"] = "Rastreio da origem da chamada (CRM)";
$MESS["LICENSE_TEL_FEATURE_19"] = "Análises e relatórios de chamadas";
$MESS["LICENSE_TEL_FEATURE_20"] = "IVR (em breve)";
$MESS["LICENSE_TEL_FEATURE_21"] = "e muito mais (em breve)";
$MESS["LICENSE_ADD_FEATURE_1"] = "Extensão gratuita do domínio para e-mail";
$MESS["LICENSE_ADD_FEATURE_2"] = "Desativar OTP (Somente adminstrador)";
$MESS["LICENSE_ADD_FEATURE_3"] = "Transferência de domínio para e-mail";
$MESS["LICENSE_ADD_FEATURE_4"] = "Alterar nome da conta Bitrix24";
$MESS["LICENSE_ADD_FEATURE_5"] = "Mover conta Bitrix24 para outra zona de domínio";
$MESS["LICENSE_TASKS_TITLE"] = "Tarefas avançadas";
$MESS["LICENSE_TASKS_DESC"] = "Além do plano gratuito, experimente usar Tarefas avançadas para estabelecer dependências de tarefa ou mudar os prazos para todas as tarefas de uma só vez, disponível nos planos comerciais Bitrix24.";
$MESS["LICENSE_TASKS_FULL"] = "Tarefas avançadas";
$MESS["LICENSE_TASKS_FEATURE_1"] = "Controle de prazo";
$MESS["LICENSE_TASKS_FEATURE_2"] = "Listas de verificação";
$MESS["LICENSE_TASKS_FEATURE_3"] = "Lembretes";
$MESS["LICENSE_TASKS_FEATURE_4"] = "Delegação";
$MESS["LICENSE_TASKS_FEATURE_5"] = "Rastreamento de Tempo";
$MESS["LICENSE_TASKS_FEATURE_6"] = "Modelos de tarefa";
$MESS["LICENSE_TASKS_FEATURE_7"] = "Grupos de projeto";
$MESS["LICENSE_TASKS_FEATURE_8"] = "Avaliação da tarefa concluída";
$MESS["LICENSE_TASKS_FEATURE_9"] = "Relatórios";
$MESS["LICENSE_TASKS_FEATURE_10"] = "Integração com CRM, fluxos de trabalho, calendários e Bitrix24.Drive";
$MESS["LICENSE_TASKS_FEATURE_11"] = "E-mail para tarefa";
$MESS["LICENSE_TASKS_FEATURE_12"] = "Gráfico de Gantt";
$MESS["LICENSE_TASKS_FEATURE_13"] = "Restringir tarefas para dias/horas de trabalho";
$MESS["LICENSE_TASKS_FEATURE_14"] = "Quatro tipos de dependências de tarefa";
$MESS["LICENSE_PRICE_TF"] = "\$39/mês";
$MESS["LICENSE_FREE"] = "Grátis";
$MESS["LICENSE_FULL_COMPARE"] = "Comparar planos";
$MESS["LICENSE_FULL_COMPARE_LINK"] = "https://www.bitrix24.com/prices/";
$MESS["LICENSE_DEMO2"] = "Habilitar demonstrativo por 30 dias";
$MESS["LICENSE_MORE_FEATURES"] = "Adicionar ao meu Bitrix24";
$MESS["LICENSE_MORE_FEATURES_DESC"] = "Estes e outros recursos úteis para você &#8220;Bitrix24&#8221; estão disponíveis a partir do plano Plus por apenas #PRICE#/mês.";
$MESS["LICENSE_MORE_USERS"] = "Mais usuários";
$MESS["LICENSE_MORE_SPACE"] = "Mais espaço na nuvem";
$MESS["LICENSE_TEL_BALANCE"] = "Seu saldo";
$MESS["LICENSE_TEL_BALANCE_ADD"] = "Adicionar ao saldo";
$MESS["LICENSE_TEL_REMOVE_RESTR"] = "Remover limitações";
$MESS["LICENSE_TEL_RECORDS"] = "Gravação de chamada:  <b>#NUM# de #LIMIT# este mês</b>";
$MESS["LICENSE_TEL_BALANCE_UA"] = "Saldo atual disponível na conta pessoal para cada operador";
$MESS["DEMO_INFO_DESC_3"] = "Se você tiver usado mais de 5 GB de armazenamento em nuvem durante a avaliação, todos os dados serão mantidos, mas você não poderá adicionar novos documentos, apenas visualizar os existentes. Para adicionar mais documentos, você pode excluir arquivos indesejados para permanecer abaixo do limite de 5 GB ou você precisa <a class=\"link\" href=\"/settings/license_all.php\">fazer o upgrade</a>. 
";
$MESS["DEMO_POPUP_TEXT"] = "Obrigado por ativar a avaliação de 30 dias do Bitrix24 Professional<br/><br/>
Deixe seus colegas saberem para aproveitar ao máximo os novos empolgantes recursos.<br/><br/>Preparamos um esboço de mensagem para você que você pode enviar de imediato ou editar como quiser.";
$MESS["DEMO_IMPORTANT_TITLE"] = "A avaliação de 30 dias do Bitrix24 Professional foi ativada";
$MESS["DEMO_IMPORTANT_TEXT"] = "Caros amigos!

Acabamos de ativar a avaliação do Bitrix24 Profissional para testar todos os recursos disponíveis dentro do Bitrix24. Experimente as novas ferramentas de produtividade e deixe seus comentários no Fluxo de Atividade.

Dê uma olhada nos recursos premium:
[LIST=1]
 [*]Sem restrição no número de funcionários a usar Bitrix24; sem restrição de espaço em nuvem.
[*]O gerenciamento do tempo de trabalho está ativado: o que você vê no topo não é apenas o relógio, é um planejador:

[LEFT][IMG WIDTH=600 HEIGHT=350]/images/demo_license/en/timeman.png[/IMG]

[/LEFT]

[*][URL=https://www.bitrix24.com/pro/crm.php]CRM Ampliado[/URL], [URL=https://www.bitrix24.com/pro/call.php]Telefonia Ampliada[/URL] e [URL=https://www.bitrix24.com/pro/tasks.php]Tarefas Ampliadas[/URL] estão disponíveis agora. Todas as ferramentas que estavam fora dos limites agora estão prontas para uso: converter negociações, faturas e cotações; busca avançada duplicada; alterar log; avaliação da qualidade da chamada do cliente.
[*]Os chefes de departamentos agora podem solicitar relatórios de trabalho aos seus subordinados (compatível com relatórios obrigatórios programados). O planejador de reuniões e eventos agora está pronto para usar. Você irá encontrá-lo na área \"Empresa\".

[LEFT][IMG WIDTH=600 HEIGHT=350]/images/demo_license/en/work_report.png[/IMG][/LEFT]
 [/LIST]
 Esses são apenas alguns recursos premium disponíveis agora para você. Consulte a página \"Planos avançados\" para a lista completa.";
$MESS["LICENSE_EXPLODE"] = "Fazer upgrade";
$MESS["LICENSE_FEATURE_OPENLINES"] = "Canais Abertos";
$MESS["LICENSE_FEATURE_ONLINECHAT"] = "Bate-papo ao vivo no site";
$MESS["LICENSE_FEATURE_LINE_1"] = "1 canal";
$MESS["LICENSE_FEATURE_LINE_2"] = "2 canais";
$MESS["LICENSE_FEATURE_CHAT_1"] = "1 bate-papo";
$MESS["LICENSE_FEATURE_CHAT_2"] = "2 bate-papos";
$MESS["LICENSE_CRM_FEATURE_19"] = "Integração 1C";
$MESS["LICENSE_CRM_FEATURE_23"] = "Formulários de CRM ativos";
$MESS["LICENSE_CRM_FEATURE_24"] = "Ocultar link Bitrix24 no formulário público de CRM";
$MESS["LICENSE_CRM_FEATURE_25"] = "Faturas online";
$MESS["LICENSE_CRM_FEATURE_26"] = "Rastreador de e-mail";
$MESS["LICENSE_CRM_FEATURE_27"] = "Histórico de download";
$MESS["LICENSE_CRM_FEATURE_28"] = "Rastreador 1C";
$MESS["LICENSE_CRM_FEATURE_29"] = "Vários pipelines";
$MESS["LICENSE_BUSINESS_USERS_12"] = "12 usuários empresariais";
$MESS["LICENSE_BUSINESS_USERS_24"] = "24 usuários empresariais";
$MESS["LICENSE_DAYS_3"] = "3 dias";
$MESS["LICENSE_TEL_FEATURE_22"] = "Direitos de acesso de configurações de telefonia e chamada telefônica";
$MESS["LICENSE_TASKS_FEATURE_15"] = "Campos de tarefa personalizados";
$MESS["LICENSE_TASKS_FEATURE_16"] = "Participação em tarefas";
$MESS["LICENSE_DISK_TITLE"] = "Unidade Avançada";
$MESS["LICENSE_DISK_DESC"] = "Unidade Avançada torna possível bloquear um documento para edição, evitando assim que outros usuários o modifiquem enquanto você está fazendo alterações. Outro recurso útil possibilita que você permita ou não que seus funcionários criem links de documento para acesso externo.";
$MESS["LICENSE_DISK_FULL"] = "Unidade Avançada";
$MESS["LICENSE_DISK_FEATURE_1"] = "Unidade privada do funcionário";
$MESS["LICENSE_DISK_FEATURE_2"] = "Unidade compartilhada da empresa";
$MESS["LICENSE_DISK_FEATURE_3"] = "Sincronização com o computador local";
$MESS["LICENSE_DISK_FEATURE_4"] = "Integrar com One Drive, Google Drive e Drop Box";
$MESS["LICENSE_DISK_FEATURE_5"] = "Editar documentos online no GoogleDocs";
$MESS["LICENSE_DISK_FEATURE_6"] = "Editar documentos online no Microsoft Office Online";
$MESS["LICENSE_DISK_FEATURE_7"] = "Editar documentos no computador local (Microsoft Office, OpenOffice, LibreOffice etc)";
$MESS["LICENSE_DISK_FEATURE_8"] = "Pesquisar conteúdos de documentos";
$MESS["LICENSE_DISK_FEATURE_9"] = "Histórico de revisão";
$MESS["LICENSE_DISK_FEATURE_10"] = "Atribuir permissões de acesso";
$MESS["LICENSE_DISK_FEATURE_11"] = "Compartilhar documentos (usando links externos)";
$MESS["LICENSE_DISK_FEATURE_12"] = "Desativar links públicos";
$MESS["LICENSE_DISK_FEATURE_13"] = "Bloquear documentos";
$MESS["LICENSE_DISK_SHOW_MORE"] = "Mostrar recursos da Unidade em planos diferentes";
$MESS["LICENSE_ADMIN_NOTIFY_LICENSE_REQUEST"] = "Pedido de compra de plano <a href=\"#LICENSE_LINK#\">#LICENSE_NAME#</a>";
$MESS["LICENSE_FEATURE_SOCIAL_COMMUNICATION"] = "Comunicações Sociais";
$MESS["LICENSE_FEATURE_FILES"] = "Arquivos e Documentos";
$MESS["LICENSE_FEATURE_EXTRANET"] = "Extranet";
$MESS["LICENSE_PRICE"] = "Custo";
$MESS["LICENSE_PROJECT_FREE"] = "Custo zero!";
$MESS["CONFIG_CHANGE_LICENSE"] = "Selecionar Licensa";
$MESS["LICENCE_FEATURE_R_GENERAL"] = "Funcionalidades principais";
$MESS["LICENCE_FEATURE_R_EXTRANET"] = "Usuários Externos (Extranet)";
$MESS["LICENCE_FEATURE_R_CRM_1C"] = "Logo Próprio";
$MESS["LICENCE_FEATURE_R_OTCH"] = "Relatórios de trabalho";
$MESS["LICENCE_FEATURE_R_BP"] = "Fluxo de aprovações (Ativo, despesas, etc)";
$MESS["LICENCE_FEATURE_R_MORE"] = "Mais";
$MESS["CONFIG_LICENSE_DESCRIPTION_NEW"] = "Use cupons para: <br>
1) ativar uma assinatura paga; <br>
2) estender uma assinatura paga para 1, 6 ou 12 meses; <br>
3) mudar para outro plano tarifário; <br>
4) adicionar usuários; <br>
5) aumentar o espaço do disco na nuvem; <br>
6) recarregar (crédito) seu telefone.";
$MESS["LICENSE_FEATURE_LISTS"] = "Gestão de Registro";
$MESS["LICENSE_PRICE_CORPORATION"] = "a partir de US \$8 por usuário/mês";
$MESS["LICENSE_TITLE_CORPORATION"] = "Comprar Versão Enterprise";
$MESS["CONFIG_LICENSE_DESCRIPTION"] = "Usar cupons para:<br>1) ativar assinatura paga;<br>2) Renovar assinatura paga para 6 ou 12 meses (depende da sua licença);<br>3) mudar para outro plano tarifário .";
$MESS["CONFIG_CHANGE_LICENSE_INFO"] = "Estender ou<br> comprar uma nova licença<br> Para mais Detalhes de:<br><a href = target 'http://www.bitrix24.com/prices' = '_blank' > Bitrix24</a>";
$MESS["CONFIG_LICENSE_INVOICE_BUTTON"] = "Criar fatura";
$MESS["LICENSE_F_R_GENERAL_L"] = "http://www.bitrix24.com/prices/";
$MESS["LICENCE_FEATURE_R_EXTRANET_L"] = "http://www.bitrix24.com/features/tasks.php#extranet";
$MESS["LICENCE_FEATURE_R_OTCH_L"] = "http://www.bitrix24.com/features/company.php#work_reports";
$MESS["LICENSE_FEATURE_TIMEMAN_L"] = "http://www.bitrix24.com/features/company.php#time_management";
$MESS["LICENSE_FEATURE_MEETING_L"] = "http://www.bitrix24.com/features/calendars.php#meetings";
$MESS["LICENCE_FEATURE_R_BP_L"] = "http://www.bitrix24.com/features/company.php#lists";
$MESS["LICENSE_FEATURE_IP_L"] = "http://www.bitrix24.com/features/more.php#ip_limitation";
?>