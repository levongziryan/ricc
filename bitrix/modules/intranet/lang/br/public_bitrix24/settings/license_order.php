<?
$MESS["LICENSE_TITLE"] = "Comprar Licença";
$MESS["LICENSE_CHOOSE"] = "Selecione a opção de compra";
$MESS["LICENSE_ORDER_PARTNER_TITLE"] = "Comprar licença <br/><strong>via representante</strong>";
$MESS["LICENSE_ORDER_RESEIVE_TEXT"] = "<strong>Você terá:</strong>";
$MESS["LICENSE_ORDER_PARTNER_TEXT_1"] = "Atendimento ao cliente impecável fornecido pela Bitrix e apoiado pela comunidade através do fórum.";
$MESS["LICENSE_ORDER_PARTNER_TEXT_2"] = "Suporte telefônico pelo representante, que responderá todas as suas perguntas e o auxiliará passo a passo.";
$MESS["LICENSE_ORDER_PARTNER_TEXT_3"] = "5% a 10% de desconto (em vista do plano escolhido).";
$MESS["LICENSE_ORDER_PARTNER_NAME"] = "Seu representante:";
$MESS["LICENSE_ORDER_PARTNER_TEL"] = "Telefone:";
$MESS["LICENSE_ORDER_PARTNER_CHANGE"] = "alterar representante";
$MESS["LICENSE_ORDER_PARTNER_CHOOSE"] = "Selecionar representante";
$MESS["LICENSE_ORDER_PARTNER_BUY"] = "Comprar ";
$MESS["LICENSE_ORDER_OR"] = "OU";
$MESS["LICENSE_ORDER_MYSELF_TITLE"] = "Comprar licença <br><strong>diretamente</strong>";
$MESS["LICENSE_ORDER_MYSELF_TEXT_1"] = "Atendimento ao cliente impecável fornecido pela Bitrix e apoiado pela comunidade através do fórum.";
$MESS["LICENSE_ORDER_MYSELF_TEXT_2"] = "Uma variedade de opções de pagamento para obter a sua licença imediatamente.";
$MESS["LICENSE_ORDER_PARTNER_SEND"] = "A solicitação de compra foi enviada <br> para o representante";
?>