<?
$MESS["TELEPHONY_TITLE"] = "Configurações de telefonia";
$MESS["TELEPHONY_TITLE_2"] = "Telefonia";
$MESS["TELEPHONY_BALANCE"] = "Saldo da conta";
$MESS["TELEPHONY_REFRESH"] = "Atualizar";
$MESS["TELEPHONY_PAY"] = "Adicionar crédito";
$MESS["TELEPHONY_HISTORY"] = "Histórico da conta";
$MESS["TELEPHONY_RUR"] = "RUB";
$MESS["TELEPHONY_UAH"] = "UAH";
$MESS["TELEPHONY_USD"] = "\$";
$MESS["TELEPHONY_EUR"] = "&euro;";
$MESS["TELEPHONY_MIN"] = "min";
$MESS["TELEPHONY_STAT"] = "Estatísticas";
$MESS["TELEPHONY_PAY_DISABLE"] = "Nesse momento, você não pode colocar dinheiro na sua conta. Esta opção estará disponível em breve. Pedimos desculpas pela inconveniência.";
$MESS["TELEPHONY_TARIFFS"] = "Tarifas";
$MESS["TELEPHONY_DETAIL"] = "Fatura discriminada";
$MESS["TELEPHONY_DETAIL_NOTICE"] = "A discriminação está indisponível no momento. Verifique novamente mais tarde.";
$MESS["TELEPHONY_MONTH_1"] = "Janeiro";
$MESS["TELEPHONY_MONTH_2"] = "Fevereiro";
$MESS["TELEPHONY_MONTH_3"] = "Março";
$MESS["TELEPHONY_MONTH_4"] = "Abril";
$MESS["TELEPHONY_MONTH_5"] = "Maio";
$MESS["TELEPHONY_MONTH_6"] = "Junho";
$MESS["TELEPHONY_MONTH_7"] = "Julho";
$MESS["TELEPHONY_MONTH_8"] = "Agosto";
$MESS["TELEPHONY_MONTH_9"] = "Setembro";
$MESS["TELEPHONY_MONTH_10"] = "Outubro";
$MESS["TELEPHONY_MONTH_11"] = "Novembro";
$MESS["TELEPHONY_MONTH_12"] = "Dezembro";
$MESS["TELEPHONY_PUT_PHONE"] = "Digite o número de telefone da empresa";
$MESS["TELEPHONY_EMPTY_PHONE"] = "Não foi fornecido nenhum número de telefone";
$MESS["TELEPHONY_EMPTY_PHONE_DESC"] = "As pessoas chamadas irão ver um número de telefone retransmitido";
$MESS["TELEPHONY_VERIFY_CODE"] = "Será feita uma chamada automática para este número de telefone e o código de verificação será fornecido por comando de voz.";
$MESS["TELEPHONY_VERIFY_CODE_2"] = "Agora, será feita uma chamada automática para este número de telefone, o código de verificação será fornecido por comando de voz.";
$MESS["TELEPHONY_PUT_CODE"] = "Digite o código de verificação:";
$MESS["TELEPHONY_WRONG_CODE"] = "Código inválido";
$MESS["TELEPHONY_PHONE"] = "Este número será visível para a pessoa chamada.";
$MESS["TELEPHONY_CONFIRM_PHONE"] = "Seu número não foi comprovado";
$MESS["TELEPHONY_CONFIRM"] = "Confirmar";
$MESS["TELEPHONY_CANCEL"] = "Cancelar";
$MESS["TELEPHONY_RETRY"] = "Tente Novamente";
$MESS["TELEPHONY_EDIT"] = "Editar";
$MESS["TELEPHONY_OR"] = "ou";
$MESS["TELEPHONY_DELETE"] = "excluir";
$MESS["TELEPHONY_CONFIRM_DATE"] = "O número de telefone foi confirmado. Estará ativo até #DATA#. Depois desta data, será desativado e precisa ser verificado novamente.";
?>