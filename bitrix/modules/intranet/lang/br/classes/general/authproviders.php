<?
$MESS["authprov_group"] = "Departamento";
$MESS["authprov_check_d"] = "Todos os colaboradores do departamento";
$MESS["authprov_check_dr"] = "Todos os colaboradores do departamento e sub-departamentos";
$MESS["authprov_panel_last"] = "ltimo";
$MESS["authprov_panel_group"] = "Selecionar da estrutura";
$MESS["authprov_panel_search"] = "Buscar";
$MESS["authprov_panel_search_text"] = "Digite o login de usuário, ou nome ou sobrenome, ou o nome do departamento.";
$MESS["authprov_name_out_group"] = "Departamento ";
$MESS["authprov_name"] = "Departamentos";
$MESS["authprov_group_extranet"] = "Extranet";
$MESS["authprov_name_out_user1"] = "Colaborador e supervisores";
$MESS["authprov_name_out_user"] = "Colaborador";
?>