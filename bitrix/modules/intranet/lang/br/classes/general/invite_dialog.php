<?
$MESS["BX24_INVITE_TITLE_INVITE"] = "Convidar colaboradores";
$MESS["BX24_INVITE_TITLE_ADD"] = "Adicionar colaborador";
$MESS["BX24_INVITE_BUTTON"] = "Convidar";
$MESS["BX24_CLOSE_BUTTON"] = "Fechar";
$MESS["BX24_LOADING"] = "Carregando...";
$MESS["BX24_INVITE_DIALOG_EMAIL_ERROR"] = "Os endereço de e-mail está incorreto: <br/>";
$MESS["BX24_INVITE_DIALOG_EMAIL_LIMIT_EXCEEDED"] = "Número máximo de endereços de e-mail no convite excedido.";
$MESS["BX24_INVITE_DIALOG_USER_ID_NO_EXIST_ERROR"] = "O ID do usuário está incorreto.";
$MESS["BX24_INVITE_DIALOG_USER_EXIST_ERROR"] = "Usuários com esses endereços de e-mail já existem.";
$MESS["BX24_INVITE_DIALOG_MAX_COUNT_ERROR"] = "O número de convidados excedem as os termos da licença.";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT"] = "Junte-se a nós na nossa nova conta Bitrix24, onde todos podem colaborar em projetos, ter uma comunicação ideal, coordenar tarefas, gerenciar clientes e mais recursos.";
$MESS["BX24_INVITE_DIALOG_ERROR_EMPTY_EMAIL_LIST"] = "Os endereços de e-mail não foram especificados.";
$MESS["BX24_INVITE_DIALOG_ERROR_EMPTY_EMAIL"] = "O endereço de e-mail não foi especificado.";
$MESS["BX24_INVITE_DIALOG_ERROR_EMPTY_LAST_NAME"] = "O sobrenome é obrigatório.";
$MESS["BX24_INVITE_DIALOG_ERROR_EMPTY_PASSWORD"] = "A senha não é obrigatória.";
$MESS["BX24_INVITE_DIALOG_ERROR_WRONG_PASSWORD_CONFIRM"] = "Senha não coincide com a confirmação.";
$MESS["BX24_INVITE_DIALOG_ERROR_WRONG_USER"] = "Especificado usuário de e-mail incorreto.";
$MESS["BX24_INVITE_DIALOG_ERROR_USER_TRANSFER"] = "Não é possível converter o usuário de e-mail.";
$MESS["BX24_INVITE_TITLE"] = "Convidar usuário";
?>