<?
$MESS["INTRANET_RATING_NAME"] = "Intranet";
$MESS["INTRANET_RATING_USER_SUBORDINATE_NAME"] = "Extra votos com base na estrutura da empresa";
$MESS["INTRANET_RATING_USER_SUBORDINATE_DESC"] = "O valor é baseado em dados calculados pela regra \"Estrutura da Empresa\".";
$MESS["INTRANET_RATING_USER_SUBORDINATE_FORMULA_DESC"] = "ValorSubordinado - votos extra; K - razão definida or usuário.";
?>