<?
$MESS["I_NEW_USER_TITLE"] = "Possui novo colaborador no time";
$MESS["I_NEW_USER_TITLE_SETTINGS"] = "Novos colaboradores";
$MESS["I_NEW_USER_TITLE_LIST"] = "Novo colaborador";
$MESS["I_NEW_USER_MENTION"] = "mencionou você em um comentário sobre um colaborador recém-adicionado #title#.";
$MESS["I_NEW_USER_MENTION_M"] = "mencionou você em um comentário sobre um colaborador recém-adicionado #title#.";
$MESS["I_NEW_USER_MENTION_F"] = "mencionou você em um comentário sobre um colaborador recém-adicionado #title#.";
$MESS["I_NEW_USER_EXTERNAL_TITLE"] = "Adicionado novo usuário externo";
?>