<?
$MESS["PP_USER_CONDITION_SUBORDINATE_NAME"] = "Estrutura da Empresa";
$MESS["PP_USER_CONDITION_SUBORDINATE_TEXT"] = "Calcular votos extras para autoridade do usuário baseado na estrutura da empresa.";
$MESS["PP_USER_CONDITION_SUBORDINATE_T0"] = "Máximo de votos para Supervisor";
$MESS["PP_USER_CONDITION_SUBORDINATE_T1"] = "Votos para subordinados";
$MESS["PP_USER_CONDITION_SUBORDINATE_T2"] = "50% do máximo do supervisor";
$MESS["PP_USER_CONDITION_SUBORDINATE_T3"] = "75% do máximo do supervisor";
$MESS["PP_USER_CONDITION_SUBORDINATE_T4"] = "100% do máximo do supervisor";
?>