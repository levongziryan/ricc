<?
$MESS["INTR_EMP_CANCEL"] = "Cancelar";
$MESS["INTR_EMP_CANCEL_TITLE"] = "Cancelar Seleção de colaboradors";
$MESS["INTR_EMP_SUBMIT_TITLE"] = "Escolha o colaborador selecionado";
$MESS["INTR_EMP_WINDOW_CLOSE"] = "Fechar";
$MESS["INTR_EMP_WAIT"] = "Carregando...";
$MESS["INTR_EMP_SUBMIT"] = "Selecionar";
$MESS["INTR_EMP_WINDOW_TITLE"] = "Selecionar colaborador";
?>