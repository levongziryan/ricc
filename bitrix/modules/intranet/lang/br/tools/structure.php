<?
$MESS["INTR_STRUCTURE_IBLOCK_MODULE"] = "O módulo Blocos de Informação não está instalado.";
$MESS["INTR_EDIT_TITLE"] = "Editar departamento";
$MESS["INTR_ADD_TITLE"] = "Adicionar Departamento";
$MESS["INTR_STRUCTURE_NAME"] = "Nome do departamento";
$MESS["INTR_STRUCTURE_HEAD"] = "Supervisor";
$MESS["INTR_STRUCTURE_DEPARTMENT"] = "Departamento principal";
$MESS["INTR_STRUCTURE_SUCCESS"] = "O departamento foi adicionado.";
$MESS["INTR_STRUCTURE_ADD"] = "Adicionar";
$MESS["INTR_STRUCTURE_ADD_MORE"] = "Adicionar mais";
$MESS["INTR_STRUCTURE_EDIT"] = "Salvar";
$MESS["INTR_UF_HEAD_CHOOSE"] = "Selecionar da Estrutura";
$MESS["INTR_USER_ERR_NO_RIGHT"] = "Permissão insuficiente para realizar alterações.";
?>