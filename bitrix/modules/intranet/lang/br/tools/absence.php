<?
$MESS["INTR_ABSENCE_IBLOCK_MODULE"] = "O módulo Blocos de Informação não está instalado.";
$MESS["INTR_ADD_TITLE"] = "Adicionar Entrada";
$MESS["INTR_EDIT_TITLE"] = "Editar entrada de ausência";
$MESS["INTR_ABSENCE_NAME"] = "Razão da ausência *";
$MESS["INTR_ABSENCE_NO_TYPE"] = "(não especificado)";
$MESS["INTR_ABSENCE_USER"] = "Selecionar pessoa ausente *";
$MESS["INTR_ABSENCE_TYPE"] = "Tipo de ausência";
$MESS["INTR_ABSENCE_PERIOD"] = "Tepo de ausência";
$MESS["INTR_ABSENCE_ACTIVE_FROM"] = "Iniciar:";
$MESS["INTR_ABSENCE_ACTIVE_TO"] = "Fim:";
$MESS["INTR_ABSENCE_SUCCESS"] = "Entrada de ausência foi adicionada.";
$MESS["INTR_ABSENCE_ADD"] = "Adicionar";
$MESS["INTR_ABSENCE_EDIT"] = "Editar";
$MESS["INTR_ABSENCE_ADD_MORE"] = "Adicionar mais";
$MESS["INTR_USER_CHOOSE"] = "Selecionar da Estrutura";
$MESS["INTR_USER_ERR_NO_RIGHT"] = "Acesso para fazer alterações não concedido";
?>