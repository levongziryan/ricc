<?
$MESS["TITLE"] = "Bericht über die Arbeitszeit";
$MESS["TARIFF_RESTRICTION_TEXT"] = "Das Tool Arbeitszeitmanagement ist für jeden Vorgesetzten wichtig. Damit kann man sofort sehen, wann die Mitarbeiter ihren Arbeitstag gestartet und wann beendet und womit sie sich im Laufe des Tages beschäftigt haben. Das Tool funktioniert auf einem beliebigen Gerät, es liefert Ihnen einen detaillierten Arbeitszeitbericht für jeden Mitarbeiter. <a href=\" https://helpdesk.bitrix24.de/open/1343796/\">Mehr</a>";
$MESS["TARIFF_RESTRICTION_TEXT2"] = "Das Arbeitszeitmanagement ist nur im Tarif Professional verfügbar.";
?>