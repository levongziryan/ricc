<?
$MESS["TITLE"] = "Arbeitsberichte";
$MESS["TARIFF_RESTRICTION_TEXT"] = "Ein schneller Zugriff auf Arbeitsberichte Ihrer Mitarbeiter ermöglicht es Ihnen, die Gesamteffizient Ihres Teams einzuschätzen und ggf. zu erhöhen. Sie können für jeden Mitarbeiter individuell definieren, für welchen Zeitraum und zu welchem Zeitpunkt der Bericht eingereicht werden soll (täglich, wöchentlich usw.). Jeder Vorgesetzte kann den Bericht und damit die Leistungen des jeweiligen Mitarbeiters bewerten.<a href=\" https://helpdesk.bitrix24.de/open/1343802/\">Mehr</a>";
$MESS["TARIFF_RESTRICTION_TEXT2"] = "Arbeitsberichte sind nur im Tarif Professional verfügbar.";
?>