<?
$MESS["TITLE"] = "Meetings und Briefings";
$MESS["TARIFF_RESTRICTION_TEXT"] = "Bitrix24 kann Ihnen dabei helfen, Ihre internen Termine mithilfe von einem speziellen Planer namens Meetings and Briefings zu organisieren. Dabei können alle Diskussionen aufgezeichnet und die Aufgaben zugewiesen werden, und die Verantwortlichen können mit entsprechenden Berichten beauftragt werden.
<a href=\" https://helpdesk.bitrix24.de/open/2412969/\">Mehr</a>
";
$MESS["TARIFF_RESTRICTION_TEXT2"] = "Das Tool Meetings and Briefings ist nur im Tarif Professional verfügbar.";
?>