<?
$MESS["TITLE"] = "Einstellungen";
$MESS["STATUS"] = "Auswahllisten";
$MESS["PERMS"] = "Zugriffsberechtigungen";
$MESS["BP"] = "Geschäftsprozesse";
$MESS["FIELDS"] = "Benutzerdefinierte Felder";
$MESS["CONFIG"] = "Sonstige Einstellungen";
$MESS["SENDSAVE"] = "Integration mit Send&Save";
$MESS["CURRENCY"] = "Währungen";
$MESS["EXTERNAL_SALE"] = "E-Shops";
$MESS["EXCH1C"] = "1C-Integration";
$MESS["MAIL_TEMPLATES"] = "E-Mail-Vorlagen";
$MESS["TAX"] = "Steuern";
$MESS["PS"] = "Zahlungsoptionen und Rechnungen";
$MESS["LOCATIONS"] = "Standorte";
$MESS["MEASURE"] = "Maßeinheiten";
$MESS["PRODUCT_PROPS"] = "Produkteigenschaften";
$MESS["SLOT"] = "Analytische Berichte";
?>