<?
$MESS["MENU_TELEPHONY_BALANCE"] = "Guthaben und Statistik";
$MESS["MENU_TELEPHONY"] = "Telefonie-Einstellungen";
$MESS["MENU_TELEPHONY_PERMISSIONS"] = "Zugriffsrechte";
$MESS["MENU_TELEPHONY_LINES"] = "Telefonnummern";
$MESS["MENU_TELEPHONY_CONNECT"] = "Verbindung";
$MESS["MENU_TELEPHONY_USERS"] = "Telefonie-Nutzer";
$MESS["MENU_TELEPHONY_PHONES"] = "SIP-Telefone";
$MESS["MENU_TELEPHONY_GROUPS"] = "Warteschleifen";
$MESS["MENU_TELEPHONY_IVR"] = "IVR konfigurieren";
?>