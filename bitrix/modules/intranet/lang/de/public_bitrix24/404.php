<?
$MESS["ERROR_404_TITLE"] = "Fehler 404";
$MESS["ERROR_404_TEXT1"] = "Die aufgerufene Seite konnte leider<br>nicht gefunden werden.";
$MESS["ERROR_404_TEXT2"] = "Wenn Sie glauben, dass diese Meldung fehlerhaft ist,<br>kontaktieren Sie bitte den Systemadministrator.";
?>