<?
$MESS["TITLE"] = "Geschäftsprozesse";
$MESS["CONTENT"] = "<div> Inhalt:
  <ul>
    <li><a href=\"#bizproc\">Geschäftsprozesse</a></li>

    <li><a href=\"#tipical\">Standardgeschäftsprozesse</a></li>

    <li><a href=\"#work\">Erstellung der Geschäftsprozesse</a></li>

    <li><a href=\"#perfomance\">Initiierung und Ausführung der Geschäftsprozesse</a></li>
   </ul>
  <br />
  
  <h1><a name=\"bizproc\"></a>Geschäftsprozesse</h1>

  <p><b>Geschäftsprozesse</b> sind ein Werkzeug, mit dem Sie alle Informationsabläufe steuern können. </p>

  <p><i><b>Geschäftsprozesse</b> stellen Prozesse der Informations- und Dokumentenlenkung innerhalb eines vorgegebenen Algorithmus dar. 
  Der Algorithmus des Geschäftsprozesses kann dabei Folgendes enthalten:</i></p>

  <ul>
    <li><i>Einen oder mehrere Inputs und Outputs (Anfang, Ende);</i></li>
    <li><i>Reihenfolge von Aktionen (oder Schritten, Phasen, Funktionen), die nach einer vorgegebenen Ordnung und unter bestimmten Bedingungen durchgeführt werden.</i></li>
   </ul>

  <p>Die Prozesse des Informationsablaufs können in der wirklichen Geschäftstätigkeit sehr verschieden sein. Es können verschiedene Aktionsordnungen oder auch verschiedene Bearbeitungsbedingungen und Benachrichtigungsmodalitäten angegeben werden, damit das Dokument veröffentlicht werden kann.</p>

  <p>Geschäftsprozesse verfügen über ein universales Mittel, mit welchem sie auch von einfachen Mitarbeitern ohne 
  Programmierkenntnisse erstellt und bearbeitet werden können. Dabei muss man aber berücksichtigen, dass der Algorithmus der Geschäftsprozesse 
  eine bestimmte analytische Denkweise und detaillierte Kenntnisse über den realen Geschäftsprozess des Unternehmens verlangt.</p>

  <p>Dieses universale Mittel ist eine Technik zum visuellen Programmieren. <b>Drag&Drop</b> ist jedem Computernutzer bekannt und verständlich. 
  Die Geschäftsprozessvorlagen werden innerhalb eines speziellen visuellen Konstruktors erstellt. Die <b>Geschäftsprozesse</b> ermöglichen es den 
  Mitarbeitern, ein konkretes Ablaufschema bzgl. des Umgangs mit beispielsweise einem Dokument zu bestimmen und spezifische Bedingungen dieses Ablaufs mit Hilfe von einfachen graphischen Darstellungen anzugeben.</p>

  <p>Die Reihenfolge der Informationsflüsse im Unternehmen wird mit einer Geschäftsprozessvorlage definiert, welche wiederum aus einer Reihe von gewissen Aktionen besteht. Unter Aktion ist dabei alles Mögliche zu verstehen - angefangen bei der Erstellung eines Dokumentes über das Senden von E-Mails bis hin zur Erneuerung einer Zeile in der Datenbank und vieles mehr.</p>

  <p>Das System enthält eine ganze Reihe von integrierten Aktionen und Standardgeschäftsprozessen, welche bei üblichen Arbeitsprozessen benutzt 
  werden können. Es gibt einige Dutzende von verschiedenen Aktionen und einige vorgefertigte Standardgeschäftsprozesse.</p>

  <p>Mit den Geschäftsprozessen können zwei verschiedene Typen des Workflows erstellt werden:</p>

  <ul>
    <li>Ein regelmäßiger Geschäftsprozess: Aktionen werden hierbei nacheinander durchgeführt - vom Input bis zum Output; </li>

    <li>Ein Geschäftsprozess mit Status: Ein solcher Prozess hat weder einen Anfang noch ein Ende. Bei seiner Ausführung wird lediglich von einem Zustand (Status) zum anderen gewechselt, wobei der Prozess in einem beliebigen Punkt beendet werden kann.</li>
   </ul>

  <h2> <b>Regelmäßiger Geschäftsprozess</b></h2>

  <p>Ein sequentieller Algorithmus kann bei solchen Prozessen verwendet werden, die einen beschränkten Lebenszyklus haben (beispielsweise die 
  Erstellung, Genehmigung und Veröffentlichung eines Textdokumentes). Ein solcher Prozessalgorithmus besteht aus einigen Aktionen verschiedenen 
  Typs, die zwischen dem Anfangs- und Endpunkt des Prozesses liegen.</p>

  <p><img border=\"1\" alt=\"Example: simple linear process\" title=\"Example: simple linear process\" src=\"/images/bp/de/2.png\" /></p>

  <h2>Geschäftsprozess mit Status</h2>

  <p>Ein Prozess mit Status ist besonders praktisch bei Geschäftsprozessen, die keinen bestimmten oder vorgegebenen Zeitrahmen haben. 
  Solche Geschäftsprozesse können sich wiederholen und betriebsbedingt von einem Zustand zum anderen wechseln (beispielsweise eine stetige 
  Aktualisierung der technischen Dokumentation für die herzustellenden Produkte). Status sind nicht nur Vermerke darüber, inwiefern ein bestimmtes 
  Dokument fertig ist. Vielmehr ermöglichen die Status auch eine Beschreibung des Kreislaufs eines realen Prozesses im Unternehmen mit Mitteln der 
  <b>Geschäftsprozesse</b> und erleichtern eben diese Beschreibung auch wesentlich.</p>

  <p>Eine solche Vorlage zu erstellen ist schwieriger als die Erstellung einer sequentiellen, aber die Vorlage für Geschäftsprozesse mit 
  Status bietet zahlreiche Möglichkeiten für Automatisierungen in der Informationsverarbeitung. Der Prozessalgorithmus besteht aus einigen Status,
  welche bestimmte Aktionen und Bedingungen zum Statuswechsel beinhalten.</p>
 <img border=\"1\" alt=\"Example: process with statuses\" title=\"Example: process with statuses\" src=\"/images/bp/de/3.png\" />
  <p>Jede Aktion innerhalb des Status stellt in der Regel einen selbstständigen sequentiellen Prozess dar, welcher entsprechend dem jeweiligen Status der Informationsverarbeitung geplant wird. </p>
<br />

  <h1> <a name=\"tipical\"></a>Standardgeschäftsprozesse</h1>

  <p>Standardgeschäftsprozesse sind im System bereits enthalten, sie können mithilfe des visuellen Konstruktors geändert werden.</p>

  <h2>Sequentieller Prozess \"Einfache Genehmigung/Abstimmung\"</h2>

  <p>Dieser Prozess wird für Situationen empfohlen, in welchen eine Entscheidung durch einfache Stimmenmehrheit getroffen werden kann.</p>

  <h2>Sequentieller Prozess \"Erste Genehmigung\"</h2>

  <p>Dieser Prozess wird für Situationen empfohlen, in welchen eine Expertenmeinung aus einer bestimmten Community für die Entscheidung ausreicht.</p>

  <h2>Prozess mit Status \"Dokument mit Status genehmigen\"</h2>

  <p>Dieser Prozess wird immer dann empfohlen, wenn für die zu treffende Entscheidung ein gemeinsames Einvernehmen, also die Zustimmung mehrerer
  Personen erforderlich ist.</p>

  <h2>Sequentieller Prozess \"Zweistufige Genehmigung\"</h2>

  <p>Dieser Prozess wird für Situationen empfohlen, bei welchen vor der Genehmigung des Dokuments eine Expertenbewertung einzuholen ist. 
  Während der ersten Prozessstufe wird das Dokument durch die Experten bewertet. Wenn einer der Experten das Dokument ablehnt, geht das 
  Dokument an den Verfasser zurück. Wird es positiv bewertet, geht es zur endgültigen Abstimmung an die Gruppe der ausgewählten Mitarbeiter.
  Die Abstimmung erfolgt mit einfacher Mehrheit. Wenn die endgültige Abstimmung scheitert geht das Dokument wieder an den Verfasser zurück. </p>

  <h2>Sequentieller Prozess \"Expertenmeinung\"</h2>

  <p>Dieser Prozess wird für Situationen empfohlen, in denen eine Person ein Dokument bestätigen oder ablehnen soll und dafür vorher die Meinungen mehrerer Experten benötigt. Die Meinungen werden an die Person überreicht, die die endgültige Entscheidung trifft. </p>

  <h2>Sequentieller Prozess \"Dokument lesen\"</h2>

  <p>Dieser Prozess passt ideal für Situation, wenn Sie sicher gehen wollen, dass alle, die mit einem Dokument vertraut gemacht werden
  sollen, es auch wirklich gelesen haben.  </p>
<p>Alle Geschäftsprozesse (sowohl die Standardgeschäftsprozesse als auch die neu erstellten), die sich auf einen bestimmten Dokumenttyp beziehen, sind verfügbar mit dem Menüpunkt <b>Geschäftsprozesse</b> der Schaltfläche <b>Mehr</b>: </p>  <p><img border=\"1\" src=\"/images/bp/de/4.png\" alt=\"Schaltfläche Geschäftsprozesse\" title=\" Schaltfläche Geschäftsprozesse \" /></p> 
  
  <p>Klickt man also auf diese Schaltfläche, dann öffnet sich die Seite <b>Geschäftsprozessvorlage</b>.</p>

 <p><img border=\"1\" src=\"/images/bp/de/11.png\" alt=\"Business processes page\" title=\"Business processes page\" /></p>
<p>Auf dieser Seite können bereits existierende Geschäftsprozesse geändert, sowie neue Geschäftsprozesse erstellt werden.</p>
<br />
 
  <h1><a name=\"work\"></a>Erstellung von Geschäftsprozessen</h1>

  <p>Geschäftsprozesse können in einem speziellen visuellen Kunstruktor erstellt und bearbeitet werden. </p>

  <p>Bei der Erstellung eines Geschäftsprozesses wird zuerst sein Typ definiert. Ein Geschäftsprozess kann, wie bereits oben erwähnt 
  wurde, entweder sequentiell oder mit Status sein. Das Layout des Visuellen Konstruktors ist genau davon abhängig. Der Typ des 
  Geschäftsprozesses wird im Panel über der Vorlagenliste ausgewählt.</p>

  <p>Wenn ein Geschäftsprozess erstellt wird, müssen zuerst seine Parameter definiert werden. Die Prozessparameter sind Daten, welche bei 
  jedem Befehl oder in jeder Aktion benutzt werden können. Im nächsten Schritt erfolgen dann die Erstellung selbst sowie die Einstellung des Geschäftsprozesses.</p>
 <img border=\"1\" title=\"Setting process parameters\" alt=\"Setting process parameters\" src=\"/images/bp/de/6.png\" />
<br />
<br />

  <h2>Erstellung eines Geschäftsprozesses mit Status</h2>

  <p>Bei der Erstellung eines Geschäftsprozesses mit Status müssen zuerst die benötigten Status erstellt sowie derer Parameter 
  definiert werden. Danach werden die verschiedenen Befehlstypen für diesen Status vorgegeben. Dabei stellt jeder einzelne Befehl einen 
  einzelnen sequentiellen Prozess dar.</p>
   <img border=\"1\" src=\"/images/bp/de/7.png\" alt=\"Assigning actions in statuses\" title=\"Assigning actions in statuses\" />
<br />
<br />

  <h2>Erstellung eines sequentiellen Prozesses</h2>

  <p>Bei der Erstellung eines sequentiellen Prozesses wird im visuellen Konstruktor eine Liste der Aktionen angezeigt, die durch eine einfache Technik hinzugefügt und/oder geändert werden können.</p>

  <p>Das Hinzufügen der einzelnen Aktionen erfolgt im visuellen Konstruktor mit Hilfe der allen bekannten und verständlichen Technik 
  des <b>Drag&Drops</b>. Danach erfolgt die Parametereinstellung. Jedes Dialogfenster bei der Parametereinstellung hat sein eigenes Layout, 
  abhängig vom Aktionstyp.</p>
 <img border=\"1\" title=\"Adding actions in the visual editor\" alt=\"Adding actions in the visual editor\" src=\"/images/bp/de/8.png\" /><br /><br />
  <h1><a name=\"perfomance\"></a>Initiierung und Ausführung eines Geschäftsprozesses</h1>

  <p>Ein Geschäftsprozess kann sowohl manuell als auch automatisch initiiert werden, abhängig von seinen Einstellungen. Die Initiierungsart
  beeinflusst dabei nicht die Ausführung des Prozesses. Ein Geschäftsprozess kann außerdem mehrere Versionen haben, die alle unabhängig 
  voneinander funktionieren können.</p>

  <p>Um einen Geschäftsprozess für ein bestimmtes Dokument zu initiieren, wählen Sie in der Aktionsliste des Dokumentes <b>Neuer 
  Geschäftsprozess</b> und dann in der Auswahlliste den benötigten Geschäftsprozess aus.</p>
 <img border=\"1\" src=\"/images/bp/de/5.png\" alt=\"Launching a business process for a document\" title=\"Launching a business process for a document\" />

  <p>Jetzt müssen Sie in dem erschienenen Formular bestimmte Felder ausfüllen (diese werden für jeden Prozesstyp unterschiedlich sein). Anschließend klicken Sie auf <b>Start</b>.</p>
 <img border=\"1\" title=\"Setting up a business process\" alt=\"Setting up a business process\" src=\"/images/bp/de/9.png\" />
  <p>Sieht der Geschäftsprozess Benachrichtigungen vor, so wird der entsprechende Mitarbeiter genau dann benachrichtigt, 
  wenn der Geschäftsprozess sich auf einer Ausführungsstufe befindet, in welche dieser Mitarbeiter involviert ist. Wenn Sie für die Ausführung bestimmter Geschäftsprozesse verantwortlich sind, können Sie die Ausführung mit dem Befehl <b>Geschäftsprozesse</b> im Kontext-Menü des jeweiligen Dokumentes beginnen. </p>
</div>";
?>