<?
$MESS["INTR_STRUCTURE_IBLOCK_MODULE"] = "Das Modul Informationsblöcke ist nicht installiert.";
$MESS["INTR_EDIT_TITLE"] = "Abteilung bearbeiten";
$MESS["INTR_ADD_TITLE"] = "Neue Abteilung";
$MESS["INTR_STRUCTURE_NAME"] = "Name der Abteilung";
$MESS["INTR_STRUCTURE_HEAD"] = "Vorgesetzte(r)";
$MESS["INTR_STRUCTURE_DEPARTMENT"] = "Übergeordnete Abteilung";
$MESS["INTR_STRUCTURE_SUCCESS"] = "Abteilung wurde hinzugefügt.";
$MESS["INTR_STRUCTURE_ADD"] = "Hinzufügen";
$MESS["INTR_STRUCTURE_ADD_MORE"] = "Mehr hinzufügen";
$MESS["INTR_STRUCTURE_EDIT"] = "Bearbeiten";
$MESS["INTR_UF_HEAD_CHOOSE"] = "Aus der Struktur auswählen";
$MESS["INTR_USER_ERR_NO_RIGHT"] = "Sie haben nicht genügend Rechte, um Änderungen vorzunehmen.";
?>