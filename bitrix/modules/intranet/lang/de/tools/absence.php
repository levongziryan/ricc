<?
$MESS["INTR_ABSENCE_IBLOCK_MODULE"] = "Das Modul Informationsblöcke ist nicht installiert.";
$MESS["INTR_ADD_TITLE"] = "Neuer Eintrag";
$MESS["INTR_EDIT_TITLE"] = "Abwesenheitseintrag bearbeiten";
$MESS["INTR_ABSENCE_NAME"] = "Abwesenheitsgrund *";
$MESS["INTR_ABSENCE_NO_TYPE"] = "(nicht festgelegt)";
$MESS["INTR_ABSENCE_USER"] = "Abwesenden Mitarbeiter wählen *";
$MESS["INTR_ABSENCE_TYPE"] = "Abwesenheitstyp";
$MESS["INTR_ABSENCE_PERIOD"] = "Abwesenheitsdauer";
$MESS["INTR_ABSENCE_ACTIVE_FROM"] = "Beginndatum:";
$MESS["INTR_ABSENCE_ACTIVE_TO"] = "Enddatum:";
$MESS["INTR_ABSENCE_SUCCESS"] = "Abwesenheitseintrag wurde hinzugefügt.";
$MESS["INTR_ABSENCE_ADD"] = "Hinzufügen";
$MESS["INTR_ABSENCE_EDIT"] = "Bearbeiten";
$MESS["INTR_ABSENCE_ADD_MORE"] = "Mehr hinzufügen";
$MESS["INTR_USER_CHOOSE"] = "Aus der Struktur auswählen";
$MESS["INTR_USER_ERR_NO_RIGHT"] = "Sie haben nicht genügend Rechte, um Änderungen vorzunehmen";
$MESS["INTR_ABSENCE_FROM_TO_ERR"] = "Beginndatum muss früher sein als Enddatum.";
?>