<?
$MESS["INTR_MODULE_NAME"] = "Intranet";
$MESS["INTR_MODULE_DESCRIPTION"] = "Intranet-Portal";
$MESS["INTR_INSTALL_TITLE"] = "Modulinstallation";
$MESS["INTR_PHP_L439"] = "Sie verwenden PHP Version #VERS#, das Modul jedoch erfordert die Version 5.0.0 oder h�her. Bitte aktualisieren Sie Ihre PHP Installation oder wenden Sie sich an den technischen Support.";
$MESS["INTR_UNINSTALL_TITLE"] = "Deinstallation des Moduls Intranet";
$MESS["INTR_INSTALL_RATING_RULE"] = "Zus�tzliche Stimmen f�r eine Autorit�t aufgrund der Unternehmensstruktur errechnen";
?>