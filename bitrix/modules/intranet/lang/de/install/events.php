<?
$MESS["INTRANET_USER_INVITATION_NAME"] = "Mitarbeiter einladen";
$MESS["INTRANET_USER_INVITATION_DESC"] = "#EMAIL_TO# - die E-Mail-Adresse des eingeladenen Mitarbeiters
#LINK# - Link für Aktivierung eines neuen Mitarbeiters";
$MESS["INTRANET_USER_INVITATION_SUBJECT"] = "Willkommen im Bitrix24 Intranet";
$MESS["INTRANET_USER_INVITATION_MESSAGE"] = "#USER_TEXT#

#LINK#

Nutzen Sie Ihre E-Mail-Adresse als Login für Authentifizierung. Wenn Sie die Authentifizierung zum ersten Mal durchführen, müssen Sie Ihr persönliches Passwort definieren.";
$MESS["INTRANET_USER_ADD_NAME"] = "Mitarbeiter hinzufügen";
$MESS["INTRANET_USER_ADD_DESC"] = "#EMAIL_TO# - E-Mail-Adresse des neuen Mitarbeiters
#LINK# - Intranet-URL";
$MESS["INTRANET_USER_ADD_SUBJECT"] = "Sie wurden zum Intranet in Bitrix24 hinzugefügt";
$MESS["INTRANET_USER_ADD_MESSAGE"] = "#USER_TEXT#

#LINK#

Zum Einloggen benutzen Sie Ihre E-Mail als Login und dieses Passwort: #PASSWORD#";
$MESS["INTRANET_MAILDOMAIN_NOCOMPLETE_NAME"] = "Hinzufügen einer E-Mail-Domain wird abgeschlossen";
$MESS["INTRANET_MAILDOMAIN_NOCOMPLETE_DESC"] = "#EMAIL_TO# - E-Mail des Administrators 
#LEARNMORE_LINK# - Link zur Documentation
#SUPPORT_LINK# - Link zum Technischen Support
";
$MESS["INTRANET_MAILDOMAIN_NOCOMPLETE_SUBJECT"] = "Sie haben das Hinzufügen Ihrer Domain nicht abgeschlossen";
$MESS["INTRANET_MAILDOMAIN_NOCOMPLETE_MESSAGE"] = "<p>Sie haben versucht, eine Unternehmensdomain zu erstellen, haben aber die Registrierung nicht abgeschlossen.</p>

<p>Wenn Sie Hilfe benötigen, öffnen Sie die Sete mit E-Mail-Einstellungen, wo Sie eine detaillierte Anleitung finden, wie Sie Ihren E-Mail-Service konfigurieren und die Registrierung Ihrer Bitrix24-Domain abschließen.</p>

<p>Möchten Sie sich einige Beispiele anschauen, wie das in der Regel gemacht wird, habe wir für Sie einige vorbereitet. Die Beispiele erklären Ihnen, wie eine Domain registriert und ein Mail-Konto erstellt werden und wie das Ganze in Bitrix24 funktioniert.</p>

<p><a href=\"#LEARNMORE_LINK#\">Mehr erfahren</a></p>

<p>Wenn Sie immer noch Fragen haben, wenden Sie sich bitte an unseren <a href=\"#SUPPORT_LINK#\">Technischen Support</a>.</p>
";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX_NAME"] = "Mail-Konten in der Domain erstellen";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX_DESC"] = "#EMAIL_TO# - E-Mail des Administrators 
#LEARNMORE_LINK# - Link zur Dokumentation
#SUPPORT_LINK# - Link zum Technischen Support
";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX_SUBJECT"] = "Ihre Unternehmens-E-Mail in Bitrix24";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX_MESSAGE"] = "<p>Ihre Domain wurde zu Bitrix24 erfolgreich hinzugefügt, allerdings gibt es dort noch kein Mail-Konto.</p>
<p>Wir haben für Sie einige Beispiele vorbereitet, an denen Sie sehen können, wie die E-Mail-Domains genutzt werden können. Die Beispiele zeigen Ihnen, wie Sie ein Mail-Konto erstellen und es mit Bitrix24 nutzen können.</p>

<p><a href=\"#LEARNMORE_LINK#\">Mehr erfahren</a></p>

<p>Wenn Sie immer noch Fragen haben, wenden Sie sich bitte an unseren <a href=\"#SUPPORT_LINK#\">Technischen Support</a>.</p>
";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_NAME"] = "Mail-Konten der Mitarbeiter in der Domain erstellen";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_DESC"] = "#EMAIL_TO# - E-Mail des Administrators 
#LEARNMORE_LINK# - Link zur Dokumentation
#SUPPORT_LINK# - Link zum Technischen Support
";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_SUBJECT"] = "Unternehmens-E-Mail für Ihre Mitarbeiter";
$MESS["INTRANET_MAILDOMAIN_NOMAILBOX2_MESSAGE"] = "<p>Sie haben die Domain registriert und Ihr Mail-Konto erstellt. Aber Ihre Mitarbeiter wissen immer noch nicht davon, dass sie ihre eigenen Mail-Konten in der Unternehmensdomain erstellen können.</p>

<p>Teilen Sie Ihr Wissen mit Ihren Kollegen und erzählen Sie ihnen darüber, wie sie ihre eigenen Mail-Konten erstellen oder Mail-Konten auf jeweils ihrer Bitrix24-Seite registrieren können.</p>

<p><a href=\"#LEARNMORE_LINK#\">Wie können Mail-Konten Ihrer Mitarbeiter registriert werden</a></p>

<p>Wenn Sie immer noch Fragen haben, wenden Sie sich bitte an unseren <a href=\"#SUPPORT_LINK#\">Technischen Support</a>.</p>
";
$MESS["INTRANET_MAILDOMAIN_NOREG_NAME"] = "E-Mail-Domain registrieren";
$MESS["INTRANET_MAILDOMAIN_NOREG_DESC"] = "#EMAIL_TO# - E-Mail des Administrators 
#LEARNMORE_LINK# - Link zur Dokumentation
#SUPPORT_LINK# - Link zum Technischen Support
";
$MESS["INTRANET_MAILDOMAIN_NOREG_SUBJECT"] = "Sie haben das Hinzufügen Ihrer Domain nicht abgeschlossen";
$MESS["INTRANET_MAILDOMAIN_NOREG_MESSAGE"] = "<p>Sie wollten eine Domain für Ihr Unternehmen registrieren, scheinen aber noch keinen passenden Namen gefunden zu haben.</p>

<p>Wir haben für Sie einige Beispiele vorbereitet, an denen Sie sehen können, wie die E-Mail-Domains genutzt werden können. Die Beispiele zeigen Ihnen, wie Sie ein Mail-Konto erstellen und es mit Bitrix24 nutzen können.</p>

<p><a href=\"#LEARNMORE_LINK#\">Wie können Mail-Konten Ihrer Mitarbeiter registriert werden</a></p>

<p>Wenn Sie immer noch Fragen haben, wenden Sie sich bitte an unseren <a href=\"#SUPPORT_LINK#\">Technischen Support</a>.</p>
";
?>