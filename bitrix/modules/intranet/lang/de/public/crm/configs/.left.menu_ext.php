<?
$MESS["CRM_MENU_STATUS"] = "Auswahllisten";
$MESS["CRM_MENU_CURRENCY"] = "Währungen";
$MESS["CRM_MENU_TAX"] = "Steuern";
$MESS["CRM_MENU_LOCATIONS"] = "Standorte";
$MESS["CRM_MENU_PS"] = "Zahlungsoptionen";
$MESS["CRM_MENU_PERMS"] = "Zugriffsrechte";
$MESS["CRM_MENU_BP"] = "Geschäftsprozesse";
$MESS["CRM_MENU_FIELDS"] = "Benutzerdefinierte Felder";
$MESS["CRM_MENU_PRODUCT_PROPS"] = "Produkteigenschaften";
$MESS["CRM_MENU_CONFIG"] = "Sonstige Einstellungen";
$MESS["CRM_MENU_SENDSAVE"] = "Integration mit Send&Save";
$MESS["CRM_MENU_SALE"] = "E-Shops";
$MESS["CRM_MENU_MEASURE"] = "Maßeinheiten";
$MESS["CRM_MENU_MAILTEMPLATE"] = "E-Mail-Vorlagen";
$MESS["CRM_MENU_INFO"] = "Hilfe";
$MESS["CRM_MENU_PRODUCTPROPS"] = "Produkteigenschaften";
$MESS["CRM_MENU_PRESET"] = "Vorlagen der Informationen";
$MESS["CRM_MENU_EXCH1C"] = "&quot;1C:Enterprise&quot; Integration";
$MESS["CRM_MENU_MYCOMPANY"] = "Informationen zum Unternehmen";
?>