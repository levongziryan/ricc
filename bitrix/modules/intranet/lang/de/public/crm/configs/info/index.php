<?
$MESS["CRM_TITLE"] = "Hilfe";
$MESS["CRM_INFO"] = "<h3>Was ist CRM?</h3>
<p><b>Customer Relationship Management</b> (<b>CRM</b>) ist ein System, welches dazu dient, die Verkaufszahlen zu erhöhen, Marketingaktivitäten zu optimieren und den Kundenservice zu verbessern. CRM
enthält Kundendaten und die History der Kundenbeziehungen, welche ausgewertet werden können.</p>
<p>Mithilfe von CRM betreut ein Mitarbeiter einen potentiellen Kunden und wandelt ihn als Lead in einen erfolgreichen Auftrag um. </p>
<p>Bei der Arbeit mit CRM werden gewöhnlich folgende Begriffe verwendet.&nbsp;</p>
<ul>
<li><b>Kontakt</b>: ein Eintrag mit Daten und Informationen über eine Person, mit welcher der Mitarbeiter geschäftliche Beziehungen unterhält. </li>
<li><b>Unternehmen</b>: ein Eintrag mit Daten und Informationen über ein Unternehmen, welches geschäftliche Beziehungen mit Ihrem Unternehmen hat bzw. hatte. </li>
<li><b>Lead</b>: Informationen über jede Art der Kommunikation (Anruf, E-Mail, Treffen etc.), welche sich zu einem erfolgreichen Auftrag entwickeln kann. </li>
<li><b>Änderungen</b>: Beschreibung einer beliebigen Änderung, die bei einem Kontakt, Lead oder Unternehmen vorgenommen wurde. Z.B.: Beim Lead wurde eine neue E-Mail-Adresse hinzugefügt. </li>
<li><b>Auftrag</b>: ein Eintrag mit Daten und Informationen über Aktivitäten, die bezüglich eines Kunden unternommen wurden, um ein angestrebtes Geschäftsziel (also Verkauf) zu erreichen. </li>
</ul>
<h3>Wie funktioniert das?</h3>
<p>CRM kann genutzt werden:</p>
<ol>
<li>als eine <b>Datenbank</b> mit Kontakten und Unternehmen,</li>

<li>als klassisches <b>CRM</b>.</li>
</ol>
<h4>1. CRM als eine Datenbank</h4>
<p> CRM kann als eine Datenbank für Informationen über Kontakte und Unternehmen genutzt werden, in welcher unter anderem die History der Beziehungen abgespeichert wird. Die Hauptelemente sind dabei ein Kontakt und ein Unternehmen. Die Beziehung mit diesen werden durch Erstellung diverser Aktivitäten definiert. Auch wenn Sie Ihr CRM lediglich als eine Datenbank nutzen, können Sie einen Lead aus Ergebnissen Ihrer Aktivitäten ableiten und diesen Lead zu einem Auftrag konvertieren.</p>
<p><img height='430' border='0' width='900' src='/upload/crm/cim/01.png'  /></p>
<h4>2. Klassisches CRM</h4>
<p>In einem klassischen CRM-System ist das ursprüngliche Element ein Lead, welches manuell durch einen Mitarbeiter, automatisch vom Bitrix Site Manager oder auch aus einer anderen Quelle hinzugefügt werden kann. Nachdem hinzugefügt, kann der Lead zu einem Kontakt oder einem Unternehmen konvertiert werden, welche auch in einer allgemeinen Datenbank dargestellt werden. Wenn ein Lead alle Phasen des Sales Funnel durchläuft, wird er zu einem Auftrag.</p>
<p><img height='430' border='0' width='900' src='/upload/crm/cim/03.png'  /></p>
";
?>