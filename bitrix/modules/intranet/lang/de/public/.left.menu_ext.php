<?
$MESS["LEFT_MENU_LIVE_FEED"] = "Activity Stream";
$MESS["LEFT_MENU_IM_MESSENGER"] = "Chat und Anrufe";
$MESS["LEFT_MENU_TASKS"] = "Aufgaben";
$MESS["LEFT_MENU_CALENDAR"] = "Kalender";
$MESS["LEFT_MENU_DISC"] = "Drive";
$MESS["LEFT_MENU_PHOTO"] = "Fotos";
$MESS["LEFT_MENU_BLOG"] = "Kommunikation";
$MESS["LEFT_MENU_MAIL"] = "Webmail";
$MESS["LEFT_MENU_MAIL_SETTING"] = "Einstellungen bearbeiten";
$MESS["LEFT_MENU_BP"] = "Workflow";
$MESS["LEFT_MENU_CRM"] = "CRM";
$MESS["LEFT_MENU_MY_PROCESS"] = "Meine Anträge";
$MESS["TOP_MENU_GROUPS"] = "Gruppen";
$MESS["TOP_MENU_GROUPS_EXTRANET"] = "Extranet-Gruppen";
$MESS["TOP_MENU_DEPARTMENTS"] = "Abteilungen";
$MESS["TOP_MENU_TELEPHONY"] = "Telefonie";
$MESS["TOP_MENU_MARKETPLACE"] = "Anwendungen";
$MESS["TOP_MENU_OPENLINES"] = "Kommunikationskanäle";
$MESS["TOP_MENU_COMPANY"] = "Unternehmen";
$MESS["TOP_MENU_EMPLOYEES"] = "Mitarbeiter";
$MESS["TOP_MENU_DISCS"] = "Dokumente";
$MESS["TOP_MENU_SERVICES"] = "Services";
?>