<?
$MESS["SERVICES_TITLE"] = "Extranet";
$MESS["SERVICES_INFO"] = "<h3>Zusammenführung von zwei Informationsressourcen</h3>

<p>Das <b>'Extranet'</b> - Modul ist eine Erweiterung des Unternehmensportals zur Erstellung eines geschützten Bereiches für die Interaktion mit Lieferanten, Distributoren und anderen externen Nutzern.
</p>
<p><span style='FONT-WEIGHT: bold'>Extranet</span> ist ein <b>gesicherte</b> <b>Schnittstelle</b> für die Kollaboration mit der 'Außenwelt'. Dies ist eine Online-Plattform, die auf eine effektive Zusammenarbeit mit anderen Unternehmen ausgerichtet ist. Laden Sie in die Extranet-Arbeitsgruppen Ihre Kollegen aus 'befreundeten' Firmen ein (z.B. Lieferanten, Distributoren, Partner, etc.) und lösen Sie mit ihnen gemeinsame Aufgaben. Die Verbindung mit externen Nutzern bleibt dabei gesichert und die Sicherheit des Intranets wird nicht beeinträchtigt.
</p>

<div align='center'> <img width='302' height='217' src='#SITE#images/de/docs/cp/extranet_main-s300.png' complete='complete' />
  <br />
<i>Startseite des Extranets</i>   </div>

<h3>Zugriff und Sicherheit</h3>

<p>Für die Mitarbeiter Ihrer Firma erfolgt die Arbeit im Extranet genauso wie auch auf dem Unternehmensportal. Die eingeladenen Nutzer,
die keine Mitarbeiter Ihrer Firma sind, bekommen <b>beschränkte Zugriffsrechte</b> erteilt. Dabei gelten diese Zugriffsrechte <b>nur für das
Extranet</b> ohne Zugriffsmöglichkeiten auf die internen Intranet-Bereiche. Ihre Mitarbeiter können auch den Extranet-Gruppen per Einladung
 beitreten und mit den eingeladenen Nutzern an bestimmten Projekten zusammenarbeiten, sie können innerhalb von diesen Extranet-Gruppen
 Aufgaben nutzen, Termine planen, Berichte erstellen usw.</p>

<h3>Zusammenarbeit und Kollaboration</h3>

<p>Welche <b>Vorteile</b> bietet das Extranet, als ein 'neutrales Territorium' für die Zusammenarbeit? Diese Ressource funktioniert nach
 den gleichen Prinzipien wie das Intranet-Portal. Dadurch benötigen Ihre Mitarbeiter keine zusätzliche Schulung für neue Software.
 Die Zusammenarbeit im Extranet verläuft genauso, wie die Arbeit in den Arbeitsgruppen. Der einzige Unterschied sind die strikteren Sicherheitspolitiken.</p>

<h3>Installationsanweisungen</h3>

<p>Um die Arbeit mit Ihrem Extranet zu starten, müssen Sie das <a href='/bitrix/admin/module_admin.php' >Extranet-Modul installieren</a>. </p>

<div align='center'><img width='354' height='200' src='#SITE#images/de/docs/cp/extranet_setup.png' complete='complete' />
  <br />
<i>Installation des Extranet-Moduls</i> </div>

<p>Nachdem der Modul installiert wurde wird Ihnen angeboten <a href='/bitrix/admin/wizard_install.php?lang=de&amp;wizardName=bitrix:extranet&amp;<?=bitrix_sessid_get()?>' >den Extranet-Konfigurationsassistenten zu starten</a> mit dessen Hilfe Sie das Template und das Farbschema auswählen und andere Einstellungen vornehmen können.</p>

<p>Nachdem der Assistent abgeschlossen ist, können Sie Ihr Extranet sofort nutzen.</p>

<table width='100%' style='BORDER-COLLAPSE: collapse' border='0' cellspacing='0' cellpadding='0'>
  <tbody>
    <tr><td valign='top'>
        <br />
      </td><td valign='top'>         <span class='text'><b>Empfohlene Vorgehensweise:</b>
          <br />

          <ul>
            <li>Verpassen Sie Ihrem Extranet ein Aussehen, das sich von Ihrem Intranet unterscheidet. Verwenden Sie dazu eine fertige <b>Designvorlage</b> oder erstellen Sie Ihre eigene)</li>

            <li>verwenden Sie spezielle <b>Extranet-Tools</b>, um die Arbeit in Ihrem Extranet zu vereinfachen</li>

            <li>veröffentlichen Sie im Extranet die <b>allgemeinen Informationen</b> (Füllen Sie den Bereich 'Unternehmen' und die Ratgeber aus)</li>

            <li><span class='text'><b>Laden Sie Kollegen aus anderen Firmen ein</b>, um  an bestimmten Projekten gemeinsam zu arbeiten.</span> </li>
          </ul>
        </span></td></tr>
  </tbody>
</table>";
?>