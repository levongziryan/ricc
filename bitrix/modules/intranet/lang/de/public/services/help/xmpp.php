<?
$MESS["SERVICES_TITLE"] = "Kommunikationsmittel";
$MESS["SERVICES_INFO"] = "Das Portal verfügt über einen integrierten <b>JABBER/XMPP Server</b>. Stellen Sie eine Verbindung zu diesem Server her und <b>bleiben so immer online</b>! Sie können
Mitteilungen und Dateien austauschen, miteinander per Stimme oder Video-Chat kommunizieren und das alles softwareunabhängig via Mobilgerät. Kurzum: Egal wo
und wie - Ihre <b>Anwesenheit</b> auf dem Portal <b>ist gesichert</b>.<br />
<br />

<br />

<table cellspacing='1' cellpadding='1' border='0' width='100%'>
  <tbody>
    <tr><td valign='top'><img hspace='10' height='15' width='12' src='#SITE#images/de/docs/cp/bullet-n.gif' />Instant Messaging (IM);
        <br />
       <img hspace='10' height='15' width='12' src='#SITE#images/de/docs/cp/bullet-n.gif' />Dateiaustausch;
        <br />
       <img hspace='10' height='15' width='12' src='#SITE#images/de/docs/cp/bullet-n.gif' />Anwesenheit auf dem Portal;
        <br />
       <img hspace='10' height='15' width='12' src='#SITE#images/de/docs/cp/bullet-n.gif' />Spezielle Anwendung Miranda;
        <br />
       <img hspace='10' height='15' width='12' src='#SITE#images/de/docs/cp/bullet-n.gif' />Unterstützung für verbreitete Messenger;
        <br />
       <img hspace='10' height='15' width='12' src='#SITE#images/de/docs/cp/bullet-n.gif' />Integration mit VoIP, Video-Chat;
        <br />
       <img hspace='10' height='15' width='12' src='#SITE#images/de/docs/cp/bullet-n.gif' />'Mobiles' Portal;
        <br />
       <img hspace='10' height='15' width='12' src='#SITE#images/de/docs/cp/bullet-n.gif' />Fertige Kontaktlisten;
        <br />
       <img hspace='10' height='15' width='12' src='#SITE#images/de/docs/cp/bullet-n.gif' />Einheitliche Historie aller Nachrichten;
        <br />
       <img hspace='10' height='15' width='12' src='#SITE#images/de/docs/cp/bullet-n.gif' />SSL-Unterstützung.
        <br />
        <br />
       </td><td>
        <br />
       </td><td valign='top'>
        <br />
       <img height='63' width='130' src='#SITE#images/de/docs/cp/m.jpg' />
        <br /><br />
        <a href='http://www.bitrixsoft.com/download/Miranda_IM.zip'>Miranda herunterladen</a>
        <br />

        <br />
       </td></tr>
   </tbody>
 </table>

<br />

<h2>Integrierter XMPP-Server</h2>
Ein integrierter <b>XMPP-Server</b> übernimmt eine Aufgabe für das gesamte Geschäft äußerst wichtige Aufgabe: Er schafft ein <b>System zum sichern Nachrichtenaustausch</b>.
Ein eigener Server ermöglicht es Ihnen, auf die Nutzung von unsicheren Drittservices wie bspw. ICQ zu verzichten. Trotzdem verfügen Sie über Ihr gewohntes
Kommunikationsmittel. Der Portalserver funktioniert mit dem Protokoll <b>Jabber</b>, welches mit den populärsten Messengern kommunizieren kann. Für Sie bedeutet
das: Sie können Ihre Lieblingsanwendungen wie z.B. <b>Miranda, QIP Infium, Pidgin, Psi, Bria</b> o.ä. weiterhin auch mit dem Portal nutzen.
<br />

<br />
Dabei können Sie nicht nur Textnachrichten, sondern auch verschiedene Dateien inkl. Audio- und Videodateien senden und empfangen. Noch mehr: Sie können
mit Hilfe von Programmen kommunizieren, die auf den mobilen Geräten installiert sind. So wird das Portal für Sie wirklich zu einem universalen Kommunikationsmittel.
<br />

<br />

<h2>Wie funktioniert das alles?</h2>
<b>Ein JABBER/XMPP Server</b> ist ein komplettes Produkt, welches auf seine Funktionen und erfolgreiche Arbeitsweise getestet ist.
<br />

<br />

<table cellspacing='0' cellpadding='0' border='0' width='100%'>
  <tbody>
    <tr> </tr>

    <tr><td valign='top'>

        <br />
                                          </td><td>
        <br />
       </td><td valig='top'>Mit diesem Server funktioniert Ihr Portal einerseits als universale Technik für Sofortbenachrichtigungen und andererseits
	   als Nutzeranwesenheitsanzeige. Das heißt, Ihre Mitarbeiter sind jetzt immer online, unabhängig davon, wo sie sich befinden und welche Kommunikationsprogramme
	   sie benutzen, denn der XMPP-Server funktioniert auf verschiedenen Plattformen und unterstützt sowohl SSL-Protokoll als auch Dateiaustausch. Installieren Sie
	   auf Ihrem Mobiltelefon einen Client, welcher das Jabber-Protokoll unterstützt, z.B. QIP für Windows Mobile, und  <b>seien Sie auf dem Portal stets online</b>!
        <br />

        <br />
	  </td><td>
        <br />
       </td>
	   </tr>
   </tbody>
 </table>

<br />

<table cellspacing='1' cellpadding='1' border='0' width='100%'>
  <tbody>
    <tr><td valign='top'>Schauen Sie sich als Beispiel die üblichen Internet-Pager (Mirande, Psi und ähnliche) an. Hier brauchen Sie nicht einmal die
	Kontaktlisten des Unternehmens manuell zu erstellen. Bei der Installation des XMPP-Servers werden diese Listen automatisch eingelesen. Die gesamte
	Unternehmensstruktur mit der Mitarbeiterliste und den abteilungsspezifisch aufgeteilten Gruppen wird dabei in diese Kontaktlisten importiert. Wenn Ihr
	Kommunikationsprogramm außerdem die Erstellung angehängter Gruppen unterstützt, dann werden auch diese Gruppen automatisch erstellt.<br />
	Egal mit welchem Messenger Sie nun kommunizieren, ob über das Portal, QIP Infium oder auch Miranda, Ihre Kommunikationshistorie wird dabei immer auf
	dem Portal gespeichert und aufbewahrt. Öffnen Sie 'Meine Nachrichten' auf Ihrer persönlichen Seite, und schon sehen Sie Ihren kompletten
	Schriftverkehr mit Kollegen.<br />
				<h2>Welchen Messenger sollte man am besten benutzen?</h2>
				Welche Anwendungen sind für diese Art von Kommunikation am besten geeignet? Im Prinzip können das alle Programme sein, mit denen das
				Protokoll XMPP kommunizieren kann. Optimal wäre jedoch eine  <a href='http://www.bitrixsoft.com/download/Miranda_IM.zip'>spezielle Miranda-Version</a>
				verfügbar auf der Bitrix Website. Diese Version besonders gut geeignet für die Kommunikation innerhalb der Arbeits- bzw. Projektgruppen
				des Unternehmensportals, außerdem wurde sie speziell für den Einsatz mit einem konkreten XMPP-Server getestet. (Die XMPP-Serverkonfiguration
				für Miranda ist im <a href='http://www.bitrixsoft.com/support/training/course/lesson.php?COURSE_ID=27&ID=1641'>
				Nutzerhandbuch für die Arbeit mit dem Portal</a> ausführlich beschrieben).

          <br />

        <h2><a name='Miranda'></a>Miranda</h2>

        <p class='a'>Jetzt müssen Sie nur noch Ihren persönlichen Login und Passwort für Ihre Arbeit auf dem Portal und den Domainnamen des Portals
		wissen, und schon können Sie die Miranda-Version von Bitrix auf die Arbeit mit dem Portal einstellen. Fragen Sie im Zweifel Ihren Portaladministrator
		nach diesen Informationen. Zusätzlich könnten Sie noch fragen, welche Portnummer der XMPP-Server benutzt und ob SSL-Protokoll benutzt wird.
		All das geben Sie in den Miranda-Einstellungen an. Sie können sich dazu den Screenshot mit einem  Beispiel für die Miranda-Einstellungen
		anschauen (dieser kann bei Bedarf auch vergrößert werden) und somit sicherstellen, dass Sie alles korrekt gemacht haben.</p>
       </td><td>
        <br />
       </td><td valign='top'>
        <a href=\"javascript:ShowImg('#SITE#images/de/docs/cp/im2.png', 266, 405, 'Company contact list')\">
        <img src='#SITE#images/de/docs/cp/im2-s.png' width='100' height='152' border='0' alt='Company contact list' title='Company contact list' />
        </a>
        <br />

        <br />
       </td></tr>
   </tbody>
 </table>

<br />

<table cellspacing='0' cellpadding='0' border='0' width='100%' style='border-collapse: collapse;'>
  <tbody>


    <tr><td valign='top'>


    <a href=\"javascript:ShowImg('#SITE#images/de/docs/cp/cp8_miranda.png', 648, 472, 'Miranda settings')\">
    <img src='#SITE#images/de/docs/cp/cp8_miranda-s.png' border='0' width='100' height='73' alt='Miranda settings' title='Miranda settings' />
    </a>
        <br />
       </td><td valign='top'> <span class='text'>
          <ul>
            <li>In den Feldern <b>Nutzer</b> und <b>Passwort</b> der Jabber-Einstellungen geben Sie Login und Passwort für den Ihren üblichen Portalzugriff an;</li>

            <li>Im Feld Server geben Sie den Domainnamen Ihres Portalservers an;</li>

            <li>Fragen Sie bei Ihrem IT-Administrator nach, welcher Portalport für den XMPP-Server benutzt wird. Sollte er sich von dem Standardwert
			unterscheiden, geben Sie im Feld <b>Port</b> einen neuen Wert an;</li>

            <li>Fragen Sie bei Ihrem IT-Administrator, ob <b>SSL</b>-Protokoll benutzt wird. Wenn ja, markieren Sie das entsprechende Feld;</li>

			<li>Die anderen Felder nicht verändern;</li>

			<li>Speichern Sie die vorgenommenen Änderungen.</li>

           </ul>
         </span></td></tr>
   </tbody>
 </table>
<p>Sie müssen nicht unbedingt zu Miranda wechseln, wenn Sie sich bereits an einen anderen Messenger gewöhnt haben. Wenn das beispielsweise QIP Infium
 ist, können Sie auch da Jabber-Einstellungsfelder finden und diese analog zu Miranda ausfüllen. <i>Also bis zum nächsten Kontakt auf dem Portal!</i></p>
";
?>