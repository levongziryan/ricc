<?
$MESS["SERVICES_TITLE"] = "Hilfe";
$MESS["SERVICES_MENU_EXTRANET"] = "Extranet";
$MESS["SERVICES_MENU_ABSENCE_HELP"] = "Abwesenheitsplanung";
$MESS["SERVICES_MENU_OUTLOOK"] = "Integration mit MS Outlook";
$MESS["SERVICES_MENU_NOVICE"] = "Für neue Mitarbeiter";
$MESS["SERVICES_MENU_BP"] = "Geschäftsprozesse";
$MESS["SERVICES_MENU_XMPP"] = "Sofortnachrichten";
?>