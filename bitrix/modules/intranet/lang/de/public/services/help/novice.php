<?
$MESS["SERVICES_TITLE"] = "Für neue Mitarbeiter";
$MESS["SERVICES_INFO"] = "<img hspace='5' height='170' width='173' vspace='5' align='right' src='#SITE#images/de/new_sm.jpg' /><a href='#SITE#about/company/index.php'>Über uns</a>- &#1086;ffizielle und inoffizielle Informationen über unser Unternehmen. Hier erfahren Sie Wissenswertes über die Firmengeschichte und den Tätigkeitsbereich unseres Unternehmens. Dieser Bereich enthält auch wichtige Informationen wie z.B. Adress- und Telefondaten aller Abteilungen und Ihrer Vorgesetzten. 
<p><a href='#SITE#company/vis_structure.php'>Unternehmensstruktur</a> - anschauliche Darstellung der Hierarchie von Standorten und Abteilungen. Falls Sie eine für bestimmte Aufgaben zuständige Abteilung schnell finden möchten - suchen Sie hier. Auf dieser Seite sind die Kontaktdaten (E-Mail-Adresse/Telefonnummer) Ihrer Ansprechpartner hinterlegt. </p>

<p><a href='#SITE#company/index.php'>Mitarbeiterverzeichnis</a> - einfache/erweiterte Suchfunktion mit diversen Suchkriterien (Abteilung/Position/Name/E-Mail-Adresse/Schlüsselwörter); Anzeige der Suchergebnisse mit Information über die Anwesenheit am oder die Abwesenheit vom Arbeitsplatz.. Jeder Mitarbeiter hat eine persönliche Seite, wo er zusätzlich persönliche Angaben zur Verfügung stellen kann (z.B. Fotos oder Dokumente). Auf dieser Seite kann man zum Beispiel einen Mitarbeiter in seinem persönlichen Forum anschreiben und Kommentare in seinem persönlichen Blog hinterlassen.
  <br />
 </p>

<p><a href='#SITE#about/index.php'>Allgemeine Informationen</a> - über aktuelle Ereignisse im Unternehmen, neue Arbeitsanweisungen, Informationen von der Geschäftsführung und den Abteilungsleitern. </p>

<p><a href='#SITE#about/life.php'>Unser Leben</a> - informiert über Ereignisse und Geburtstage, Jubiläen und Festtage, gemeinsame Unternehmungen und interne Fortbildungen - also alles, was uns im Unternehmen verbindet.</p>

<p><a href='#SITE#about/calendar.php'>Kalender</a> - praktisches Mittel für die Planung und Übersicht von aktuellen und bevorstehenden Ereignissen im Unternehmen. </p>

<p><a href='#SITE#company/birthdays.php'>Geburtstage</a> - eine Seite für Mitarbeiter zur Erinnerung an aktuelle und bevorstehende Geburtstage von Freunden und Kollegen.</p>

<p><a href='#SITE#docs/'>Dokumentenbibliothek</a> - Datenbank der Dokumente, die öffentlich zur Ansicht oder Zusammenarbeit zur Verfügung stehen. Für die Zusammenarbeit der Mitarbeiter verschiedener Abteilungen sind auch geschlossene Bereiche der Bibliothek vorgesehen.</p>

<p>Tipp: Bevor Sie die Arbeit mit dem Intranet starten, lesen Sie bitte sorgfältig die Intranetanleitungen durch (<a target='_blank' href='#SITE#services/learning/'>Erste Schritte</a>). Sie werden über alle Funktionen des Portals, täglichen Einsatz und Teamwork-Fähigkeiten erfahren.</p>

<p>Wir wünschen Ihnen viel Erfolg! </p>";
?>