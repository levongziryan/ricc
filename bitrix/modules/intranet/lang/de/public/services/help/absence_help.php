<?
$MESS["SERVICES_TITLE"] = "Terminplanung für mehrere Beteiligte";
$MESS["SERVICES_INFO"] = "Sie möchten eine Sitzung ankündigen, sind sich dabei aber nicht sicher, ob die geplante Zeit auch allen Kollegen passt? Es ist wirklich einfach,
alle Teilnehmer versammelt zu bekommen! Wenn ein Mitarbeiter, den Sie einladen möchten, in seinem Terminkalender nicht schon den Status <b>Abwesend</b> hat,
 kann er eingeladen werden. Dafür:
<ul>
  <li>Erstellen Sie einen Termin im Kalender <b>über den Terminplaner</b>.
<p><img height='269' border='0' width='361' src='#SITE#images/de/docs/event_plan1.png' /></p>
  <p>Es öffnet sich dabei ein Formular, in dem ein neuer Termin hinzugefügt werden kann:</p>

<p><img height='479' border='0' width='704' src='#SITE#images/de/docs/event_plan.png' /></p>
  </li>
<li>Mit dem Link <b>Teilnehmer hinzufügen</b> wählen Sie nun erforderliche Mitarbeiter aus.</li>
</ul>
Das Portal wird dann automatisch die Verfügbarkeit der ausgewählten Mitarbeiter zum angegebenen Zeitpunkt prüfen und gegebenenfalls Sie über derer Nichtverfügbarkeit benachrichtigen.


<p>Wie können Sie anderen mitteilen, dass Sie selbst nicht verfügbar sind? Ganz einfach: Die Anzeige Ihrer Nichtverfügbarkeit erfolgt automatisch, und zwar sofort nachdem Sie einen Termin in Ihren Kalender eingetragen haben. Dieser Termin kann nicht nur in Ihrem Kalender, sondern auch in der allgemeinen <b>Abwesenheitsliste</b> angezeigt werden. Je nach dem, welchen Status Sie dabei angeben. Ihre Abwesenheit können Sie auch durch Bearbeiten eines bereits eingetragenen Termins im Nachhinein angeben, das Ergebnis würde dasselbe bleiben.</p>
<p><b>Probieren Sie das doch gleich mal aus</b></p>
<ul>
<li>Wechseln Sie in Ihren persönlichen Kalender auf Ihrer persönlichen Seite;</li>
<li>Tragen Sie einen neuen Termin ein, auf der Registerkarte <b>Neuer Termin</b> im Feld <b>Verfügbarkeit</b> wählen Sie <b>Abwesend (in die Abwesenheitsliste eintragen)</b> aus;
<p><img height='387' border='0' width='532' src='#SITE#images/de/docs/new_personal_absence.png' /></p></li>
<li>Speichern Sie die vorgenommenen Änderungen.</li>
</ul>

<p>Als Ergebnis erscheint in der Abwesenheitsliste der Eintrag über Ihre Abwesenheit.</p>

<p><img height='352' border='0' width='883' src='#SITE#images/de/docs/graph.png' /></p>

<p>Ob Sie eine Dienstreise planen, eine Veranstaltungsbesichtigung oder gar einen Besuch beim Arzt, <b>tragen Sie all das in Ihren Kalender ein</b>.
So können Sie unnötige Fragen vermeiden und Ihr Unternehmen kann einerseits <b>allgemeine Termine einfacher planen</b> und andererseits auch die Verfügbarkeit der
Mitarbeiter und derer Abwesenheit besser kontrollieren.</p>

<p>Bezüglich Ihres Urlaubs - dieser wird von den Mitarbeitern der Personalabteilung in den Unternehmenskalender eingetragen, nachdem der Urlaub von den Vorgesetzten genehmigt und an die Personalabteilung weitergeleitet wurde.</p>
";
?>