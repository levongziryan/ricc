<?
$MESS["SERVICES_TITLE"] = "Räume reservieren";
$MESS["SERVICES_INIT_DATE"] = "-Aktuelles Datum anzeigen-";
$MESS["SERVICES_INFO"] = "Wenn Sie einen Konferenzraum buchen wollen, wählen Sie bitte die passende Zeit im Kalender und markieren Sie sie im Reservierungsplan.";
$MESS["SERVICES_LINK"] = "Reservierungssystem als Tabelle anzeigen";
?>