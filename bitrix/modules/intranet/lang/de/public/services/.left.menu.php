<?
$MESS["SERVICES_MENU_MEETING_ROOM"] = "Konferenzraumbuchung";
$MESS["SERVICES_MENU_MEETING"] = "Meetings und Briefings";
$MESS["SERVICES_MENU_IDEA"] = "Ideen";
$MESS["SERVICES_MENU_PROCESSES"] = "Prozesse";
$MESS["SERVICES_MENU_LISTS"] = "Listen";
$MESS["SERVICES_MENU_BP"] = "Geschäftsprozesse";
$MESS["SERVICES_MENU_REQUESTS"] = "Anfrage einstellen";
$MESS["SERVICES_MENU_LEARNING"] = "Schulungszentrum";
$MESS["SERVICES_MENU_WIKI"] = "Wissensdatenbank (Wiki)";
$MESS["SERVICES_MENU_FAQ"] = "Häufig gestellte Fragen";
$MESS["SERVICES_MENU_VOTE"] = "Umfragen";
$MESS["SERVICES_MENU_SUPPORT"] = "Technischer Support";
$MESS["SERVICES_MENU_LINKS"] = "Linkverzeichnis";
$MESS["SERVICES_MENU_SUBSCR"] = "Abonnement";
$MESS["SERVICES_MENU_EVENTLIST"] = "Änderungsprotokoll";
$MESS["SERVICES_MENU_BOARD"] = "Schwarzes Brett";
$MESS["SERVICES_MENU_TELEPHONY"] = "Telefonie";
$MESS["SERVICES_MENU_OPENLINES"] = "Kommunikationskanäle";
$MESS["SERVICES_MENU_SALARY"] = "Lohnabrechnung und Urlaub";
?>