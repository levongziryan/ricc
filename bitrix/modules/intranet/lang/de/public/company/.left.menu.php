<?
$MESS["COMPANY_MENU_EMPLOYEES"] = "Mitarbeiterverzeichnis";
$MESS["COMPANY_MENU_TELEPHONES"] = "Telefonverzeichnis";
$MESS["COMPANY_MENU_STRUCTURE"] = "Unternehmensstruktur";
$MESS["COMPANY_MENU_EVENTS"] = "Personaländerungen";
$MESS["COMPANY_MENU_ABSENCE"] = "Abwesenheitsliste";
$MESS["COMPANY_MENU_TIMEMAN"] = "Arbeitszeit";
$MESS["COMPANY_MENU_WORKREPORT"] = "Arbeitsberichte";
$MESS["COMPANY_MENU_REPORT"] = "Leistung";
$MESS["COMPANY_MENU_LEADERS"] = "Beste Mitarbeiter";
$MESS["COMPANY_MENU_BIRTHDAYS"] = "Geburtstage";
$MESS["COMPANY_MENU_GALLERY"] = "Fotogalerie";
$MESS["COMPANY_MENU_MY_PROCESSES"] = "Meine Anträge";
?>