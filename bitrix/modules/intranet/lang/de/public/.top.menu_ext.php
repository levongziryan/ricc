<?
$MESS["TOP_MENU_FAVORITE"] = "Mein Arbeitsplatz";
$MESS["TOP_MENU_GROUPS"] = "Projektgruppen";
$MESS["TOP_MENU_GROUPS_EXTRANET"] = "Extranet-Projektgruppen";
$MESS["TOP_MENU_DEPARTMENTS"] = "Abteilungen";
$MESS["TOP_MENU_TELEPHONY"] = "Telefonie";
$MESS["TOP_MENU_MARKETPLACE"] = "Anwendungen";
$MESS["TOP_MENU_ONEC"] = "1C + Bitrix24 CRM";
$MESS["TOP_MENU_OPENLINES"] = "Kommunikationskanäle";
$MESS["TOP_MENU_LIVE_FEED"] = "Activity Stream";
$MESS["TOP_MENU_TASKS"] = "Aufgaben";
$MESS["TOP_MENU_CALENDAR"] = "Kalender";
$MESS["TOP_MENU_DISK"] = "Drive";
$MESS["TOP_MENU_PHOTO"] = "Fotos";
$MESS["TOP_MENU_BLOG"] = "Kommunikation";
$MESS["TOP_MENU_MAIL"] = "Webmail";
$MESS["TOP_MENU_CRM"] = "CRM";
$MESS["TOP_MENU_BIZPROC"] = "Workflows";
$MESS["TOP_MENU_TIMEMAN"] = "Zeit und Berichte";
$MESS["TOP_MENU_IM_MESSENGER"] = "Chat und Anrufe";
$MESS["TOP_MENU_COMPANY"] = "Mitarbeiter";
$MESS["TOP_MENU_CONFIGS"] = "Einstellungen";
$MESS["MENU_ONEC_SECTION"] = "1C + Bitrix24 CRM";
?>