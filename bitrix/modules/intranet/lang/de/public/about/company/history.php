<?
$MESS["ABOUT_TITLE"] = "Geschichte";
$MESS["ABOUT_INFO"] = "<img height=\"268\" width=\"400\" src=\"#SITE#images/de/company/about/cp7.jpg\" /><br />

<br />

<p><b>1990-1996</b></p>

<p>DEMO COMPANY&rsquo;s Geschichte begann im Jahre 1990, als die erste Geschäftsstelle in New York eröffnet wurde. Nur fünf Jahre später existierten bereits 24 DEMO COMPANY&#8217;s mit einem jährlichen Gesamtumsatz von 11,5 Millionen Dollar. Am ersten April selben Jahres ging DEMO COMPANY an die Börse und wurde 1996 zu den Top 20 der größten Firmen Amerikas gezählt. Zum selben Zeitpunkt begann eine Expandierung nach Deutschland und Frankreich. </p>

<p><b>1997-1999 </b></p>

<p>DEMO COMPANY expandierte weiter außerhalb der USA: Großhandel Eröffnungen in der Tschechischen Republik und auf dem polnischen Markt standen auf dem Programm. </p>

<p>1998 stieg der Umsatz um sage und schreibe 57,5 Prozent. Das Unternehmen wurde zu einer klar strukturierten Gesellschaft. Die Entwicklung außerhalb der USA nahm 35,2 Prozent des Gesamtumsatzes ein. Im Jahre 1999 wurden weitere 16 Geschäftsstellen im Ausland eröffnet. </p>

<p><b>2000-2004 </b></p>

<p>DEMO COMPANY trat in das neue Jahrtausend mit einer Publikation im Fortune Magazine auf Platz 5 der &quot;Weltweit meist bewunderten All-Stars&quot; ein. Gleichzeitig machte man weitere Fortschritte im Ausland. Im Jahre 2001 wurden weitere 80 Standorte hinzugefügt; einschließlich der ersten Geschäftsstelle in Russland. </p>

<p>Im Jahre 2004 operierte DEMO COMPANY bereits in acht Ländern. Die Geschäfte in Osteuropa und in Asien hatten dabei einen großen Anteil an der positiven Verkaufsentwicklung. Ebenfalls im Jahre 2004 eröffnete DEMO COMPANY die erste Filiale in der Ukraine. </p>

<p><b>2005-2006 </b></p>

<p>2005 war ein signifikantes Jahr für DEMO COMPANY. Durch den Beitrag zum Umweltschutz gewann das Unternehmen weiter an Popularität. Aber auch die Qualität der Produkte stieg stetig an. Durch die neue Warenwirtschafts-Software gelang eine weitere Steigerung des Umsatzes, sodass auch die Mitarbeiterzahl auf über 500 ansteigen konnte. </p>

<p><b>2007</b> </p>

<p>DEMO COMPANY erweiterte ihre Auslandsgeschäfte um zwei weitere Länder: In Pakistan und in der Türkei wurden die ersten Filialen eröffnet. Dadurch wird das Unternehmen zum Vorbild bezüglich der Balance zwischen Arbeit und Familie.</p>
";
?>