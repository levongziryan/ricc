<?
$MESS["ABOUT_TITLE"] = "Kontakt";
$MESS["ABOUT_INFO"] = "<p><b>E-Mail Adressen:</b></p>

<table width='100%' border='0'>
  <tbody>
    <tr>
      <td width='20%'>Allgemeine Informationen:</td>
      <td>info@company.com</td>
    </tr>
    <tr>
      <td>Verkauf:</td>
      <td>sales@company.com</td>
    </tr>
    <tr>
      <td>Technischer Support:</td>
      <td>support@company.com</td>
    </tr>
   </tbody>
 </table>

<p><b>Telefon:</b></p>

<table width='100%' border='0'>
  <tbody>
    <tr>
      <td width='20%'>Zentrale:</td>
      <td>+49 (1234) 567 891 0</td>
    </tr>
    <tr>
      <td>Bestellhotline:</td>
      <td>800-827-0685</td>
    </tr>
    <tr>
      <td>Internationale Bestellungen:</td>
      <td>+49 (1234) 567 891 65</td>
    </tr>
    <tr>
      <td>Technischer Support:</td>
      <td>+49 (1234) 567 891 25</td>
    </tr>
    <tr>
      <td>Fax:</td>
      <td>+49 (1234) 567 891 118</td>
    </tr>
   </tbody>
 </table>

<br />
<b>Niederlassung Berlin:</b> 12345 Musterort,  Musterlandstraße 1
<p><b>Öffnungszeiten:</b></p>
 Mo.-Do.: 9.00 - 18.00
<br />
 Fr.: 9.00 - 17.00
<p><strong>So finden Sie uns</strong><b>:</b></p>
<img height='300' width='535' src='#SITE#images/de/company/about/address_2.png' />";
?>