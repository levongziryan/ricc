<?
$MESS["ABOUT_TITLE"] = "Unternehmen heute";
$MESS["ABOUT_INFO"] = "<p>Mit der Gründung der DEMO COMPANY (die in den frühen 90er Jahren stattgefunden hat) haben wir das Fundament für eine beständige Software-Handelskette gelegt. Die Kunden können sich  bei uns sich sicher sein, stets die bestmöglichen Angebote zu bekommen.</p>

<p>Die Manager der Geschäftsstellen fällen ihre wirtschaftlichen Entscheidungen anhand des lokalen Angebotsbedarfs, der Auswahl an Produkten, des Preises und der Personalplanung und gehen flexibel auf die Bedürfnisse der Kunden ein. </p>

<p>Die DEMO COMPANY erlebt eine ständige Weiterentwicklung und einen regen Wachstum, sodass wir längst eine der führenden Handelsketten in Europa sind. Zur Zeit existieren mehr als 1.200 Geschäftsstellen in über 10 Ländern, die im Jahre 2007 einen Umsatz von 27,1 Millionen Euro erwirtschaftet haben. Verantwortlich für diesen Erfolg sind die 500 Angestellte, von denen über 150 in der Hauptgeschäftsstelle tätig sind. </p>

<p>Unsere Kunden können in unseren Filialen stets eine moderne und qualitativ hochwertige Produktauswahl erwarten. Abhängig von der Größe und vom Standort enthält unser Inventar über 20.000 verschiedene Produkte. Der Kunde genießt also eine schier unendlich große Angebotsvielfalt, sodass alle individuellen Wünsche erfüllt werden. </p>

<p>Die DEMO COMPANY ist nicht nur national ein angesehenes Unternehmen. Es existiert in zahlreichen Ländern mit drei unterschiedlichen Geschäftsformaten, sodass wir uns auch international einen Namen gemacht haben. Zu begründen ist dies sowohl mit der Produktauswahl als auch mit der Größe des Verkaufsraums, die in westlichen Regionen Europas zwischen 5.000 &ndash; 10.000 Quadratmeter beträgt. In Osteuropa sind die Verkaufsräume wiederum ein wenig kleiner angelegt und betragen zwischen 1.000 und 5.000 Quadratmeter. </p>

<p>Das Erfolgsrezept der DEMO COMPANY ist, dass die Produktauswahl auf die individuellen Bedürfnisse der lokalen Zielgruppen zugeschnitten ist. Die Ware in unseren Regalen wurde sorgfältig, gemäß des Kundenverhalten und der Kundenansprüche in der jeweiligen Region, ausgewählt. </p>

<p>Ebenso richten wir unsere Aufmerksamkeit auf die firmeninternen Bedingungen. Wir sind uns darüber im Klaren, dass ein wirtschaftlicher Erfolg nur mit einer gut funktionierenden Infrastruktur, einem hochwertigen Bildungssystem, einem angenehmen Betriebsklima und attraktiven Freizeitbeschäftigungen für die eigenen Mitarbeiter möglich ist. Mit der Beteiligung an Gemeinschaftsaktivitäten können Firmen dazu beitragen, die Effizienz und Attraktivität an der Umwelt zu stärken und gleichzeitig auf die Bereitwilligkeit zur Verantwortung für die Gesellschaft als Ganzes hinzuweisen.</p>

<p>Die Nähe zu den Kunden, den Lieferanten und den Mitarbeitern hat seit Geschäftsbeginn unser Verständnis von sozialer Verantwortung charakterisiert. Es ist unser ständiges Ziel, dass die Leute in den Geschäftsstellen, in denen wir tätig sind, einen gravierenden Vorteil von unseren Aktivitäten zu spüren bekommen. Wie unsere Geschäftstätigkeit dazu angetrieben wird, einen profitablen Wachstum zu erreichen, so zielt auch unser soziales Engagement darauf ab, eine tragbare Struktur rund um unsere Firmenstandorte zu erschaffen. Wir tragen mit unserer Erfahrung und Kompetenz dazu bei, Projekte und Initiativen, an denen wir bedeutungsvoll und glaubwürdig mit unserem Geschäftskern arbeiten, zu verwirklichen. </p>
";
?>