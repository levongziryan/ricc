<?
$MESS["ABOUT_TITLE"] = "Veranstaltungen";
$MESS["ABOUT_INFO1"] = "<p>All unsere Erfolge wären ohne unsere positive Firmenkultur nicht möglich gewesen. Sie ist die Quelle all unserer positiven Ergebnisse und genießt bei uns oberste Priorität. Es ist uns ein großes Anliegen, jeden einzelnen unserer Mitarbeiter zu inspirieren und zu hohen Leistungen zu befähigen. Nur so lassen sich gute Ergebnisse erzielen und wirtschaftliche Ziele realisieren. Da wir uns schon seit Geschäftsbeginn über diese Tatsache im Klaren sind, legen wir größten Wert auf eine positive Geschäftskultur, eine harmonische Zusammenarbeit und Verantwortung für die Organisation. </p>

<p>Jeder einzelne Mitarbeiter der DEMO COMPANY hat folgende drei Ziele: </p>

<p><b>Die gegenseitige Befähigung </b></p>

<p>Nur gemeinsam lassen sich Erfolge erzielen. Daraus ergibt sich, dass man sich im Team gegenseitig zu höheren Leistungen befähigt. Wir engagieren uns für den gemeinsamen Erfolg, indem wir uns gegenseitig inspirieren, stärken und motivieren. Im Verlaufe unserer fast 20 Geschäftsjahre wurde diese Art der gegenseitigen Befähigung zu einem wichtigen Stützpfeiler unserer wirtschaftlichen Erfolge. </p>

<p><b>Verantwortung übernehmen </b></p>

<p>Bei der Realisierung eines wirtschaftlichen Erfolges ist jeder einzelne Mitarbeiter gefordert. Unabhängig vom Rang und Titel müssen alle dazu bereit sein, Verantwortung für sich, die Kollegen und die Projekte zu übernehmen. Die Aufgaben verteilen sich bei uns gleichmäßig auf mehrere Schultern, sodass jeder einzelne Mitarbeiter gefordert ist. </p>

<p><b>Die Führung einer offenen und lückenlosen Kommunikation </b></p>

<p>Das Miteinander genießt bei uns oberste Priorität. Wir definieren uns als eine große Gemeinschaft, die gemeinsam an einem Strang zieht und dadurch auch die höchsten Ziele realisieren kann. Voraussetzung hierfür ist eine offene und lückenlose Kommunikation. Wir lassen keinerlei Hindernisse entstehen, die einem Erfolg im Wege stehen könnten. Stattdessen arbeiten wir eng zusammen, genießen gegenseitiges Vertrauen und führen eine ständige Kommunikation. </p>";
$MESS["ABOUT_INFO2"] = "<p><img height=\"231\" src=\"#SITE#images/de/company/about/birthday.jpg\" width=\"350\" /></p>";
?>