<?
$MESS["ABOUT_TITLE"] = "Ziele & Strategien";
$MESS["ABOUT_INFO"] = "<img height=\"268\" align=\"\" width=\"400\" src=\"#SITE#images/de/company/about/cp1.jpg\" />
<br /><br /><p>Bei der Entwicklung oder dem Updaten von Projekten sprechen wir mit unseren Kunden und Vertragspartnern alle notwendigen Schritte umfassend ab. Direkt vor Ort arbeiten wir mit den Verantwortlichen alle notwendigen Aufgaben Schritt für Schritt ab. Das zielorientierte und strategische Konzept ist auf eine Verbindung von Konzeption, Design und weiteren Anforderungen ausgelegt. </p>

<p>Ziele sind die entscheidenden Motivationen und Antriebe für menschliches Handeln. Antoine de Saint-Exupery formulierte dies äußerst prägnant: \"Wer ein Schiff bauen will, muss nicht Männer zusammen bringen, die Holz beschaffen oder weitere Aufgaben erledigen. Stattdessen muss man eine Idee von der Weite des Meeres und der Welt geben.\" </p>

<p>Ziele sind anspruchsvoll und herausfordernd zugleich. Sie sollten weder zu niedrig noch zu hoch angesetzt werden, damit sie erreicht werden können und zugleich eine Basis der Motivation darstellen. Darüber hinaus sollten die Ziele stets positiv formuliert werden und überprüfbar sein. Nur dann ist gewährleistet, dass man das Erreichen des Ziels mit einem Erfolgserlebnis verbinden kann. </p>

<p>Eine solche Zielintelligenz ist keine Selbstverständlichkeit. Möglich ist sie nur durch geeignete Maßnahmen mit klaren Aufgaben, genau geregelten Verantwortlichkeiten und zukunftssicherer Terminplanung. </p>

<p>Unsere Ziele für die kommenden Jahre sind auf die weitere Entwicklung der Bedürfnisbefriedigung unserer Kunden ausgerichtet. In diesem Zusammenhang arbeiten wir eng auf Basis einer sogenannten EKS-Strategie. Wir haben uns das Konzept einer engpassorientierten Kunden-Strategie angelegt, wie sie zum Markenzeichen von Wolfgang Mewes geworden ist, und haben zugleich neue Überlegungen, die für unser Handlungsfeld relevant sein können, in die Strategie mit einbezogen. Für einen kundenorientierten Ansatz spricht die Wichtigkeit der Formulierung persönlicher und geschäftlicher Strategien im Umfeld des Consultings. Demnach können sich die unterstützten Firmen in einer regional optimierten Art und Weise auf die effektive Nutzung der jeweiligen Ressourcen spezialisieren, um in den wichtigen Marktnischen einen Erfolg zu sichern. </p>

<p>Zum einen werden die tatsächlich dringenden Kundenbedürfnisse mit den Anforderungen des Marktes in Verbindung gesetzt. Jeder Kunde hat ein spezifisches und dringendes Problem, das ihm auf dem Herzen liegt und wo er Unterstützung erwartet. Diese findet er aber selten, weil die angesprochenen Dienstleister anders ticken oder andere Prioritäten setzen. Dieses Vorgehen werden wir in unserer Strategie keinesfalls reproduzieren; das dringende Problem des Kunden muss gelöst werden. Unsere Strategie wird sich intensiv mit den Kunden und den Mitarbeitern befassen, um das vorhandene Problem zu ermitteln und zu lösen. </p>
";
?>