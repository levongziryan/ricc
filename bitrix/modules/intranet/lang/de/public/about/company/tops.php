<?
$MESS["ABOUT_TITLE"] = "Ðóêîâîäñòâî";
$MESS["ABOUT_TOP1_IMG"] = "#SITE#images/de/company/about/management/bandurin.jpg";
$MESS["ABOUT_TOP2_IMG"] = "#SITE#images/de/company/about/management/pankrashev.jpg";
$MESS["ABOUT_TOP3_IMG"] = "#SITE#images/de/company/about/management/porshnev.jpg";
$MESS["ABOUT_TOP1_INFO"] = "<p><h2>Serge Clemons</h2></p>
<p><b>Generaldirektor</b></p><br />
<p>Serge Clemons wurde im Juni 2004 zum Generaldirektor ernannt. Zuvor war der 43-jährige in mehreren lokalen SOFTWARE DEMO COMPANY Geschäftsstellen, einschließlich der Industrie und Umwelt, tätig. Im Jahre 1999 ist er der damals noch existierenden ANOTHER DEMO COMPANY beigetreten und hat damit begonnen, alle lokalen Softwarebranchen in eine Globale umzuwandeln. Weiterhin arbeitete er an den Anstandsbelangen, Eigentum- und Qualitätsstandards von Softwares, bevor er im Jahre 2003 zu uns stieß und mit seiner Kompetenz und langjähriger Erfahrung uns bereicherte.</p><br/>";
$MESS["ABOUT_TOP2_INFO"] = "<p><h2>Dimitris Stumbles</h2></p>
<p><b>Investitionsdirektor</b></p><br />
<p>Dimitris Stumbles hat im Software-Produktionssektor und in einer Auswahl von Bezirken und zentralen Ämtern für Softwareentwicklung gearbeitet. Er war Betriebsdirektor und zwischen 1997 und 2004 Bezirksleiter, um den Zeitraum zwischen dem Pariser Börsenverlegung und der Verlegung vom Software-Managementfinanzierung zu überbrücken. Wir sind sehr glücklich darüber, dass er seit dem Jahre 2004 als Investitionsdirektor für uns aktiv ist.</p><br/>";
$MESS["ABOUT_TOP3_INFO"] = "<p><h2>Hermann Butterfield</h2></p>
<p> <b>Regelungs- &amp; Inspektionsdirektor</b></p><br />
<p>Hermann Butterfield wurde im Juni 2003 zum Regelungs- &amp; Inspektionsdirektor ernannt. Er hat in den letzten 10 Jahren in der Softwareindustrie gearbeitet, bis er seinen Schwerpunkt schließlich auf die Softwareentwicklung legte. In dieser Rolle entwickelt er mit uns ein neues Gerüst, um die Softwareleistung zu steigern.</p><br />";
?>