<?
$MESS["ABOUT_TITLE"] = "Stellenangebote";
$MESS["ABOUT_PAGE_TITLE"] = "Offene Stellen";
$MESS["ABOUT_DET_PAGE_TITLE"] = "Seite";
$MESS["ABOUT_INFO"] = "<p>Wenn Sie sich bei uns bewerben möchten, füllen Sie das <a href=\"resume.php\"><b> Online-Bewerbungsformular</b></a> aus oder senden Ihre aussagekräftigen Bewerbungsunterlagen per E-Mail an:
<a href=\"mailto:hr@example.de\">hr@example.com</a>. Das Unternehmen verpflichtet sich, Ihre Daten vertraulich zu behandeln.</p>

<p>Für weitere Auskünfte steht Ihnen der Leiter unserer Personalabteilung gerne zur Verfügung:</p>

<p> Max Mustermann, Leiter der Personalabteilung,
  <br />
Tel.: +49 (0)30 597-42-43, 597-00-00,
  <br />
Fax: +49 (0)30 466-73-11, <a href=\"mailto:hr@example.de\">hr@example.de</a></p>";
?>