<?
$MESS["PP_USER_CONDITION_SUBORDINATE_NAME"] = "Unternehmensstruktur";
$MESS["PP_USER_CONDITION_SUBORDINATE_TEXT"] = "Zus�tzliche Stimmen f�r eine Nutzerautorit�t aufgrund der Unternehmensstruktur errechnen.";
$MESS["PP_USER_CONDITION_SUBORDINATE_T0"] = "Maximale Stimmenanzahl f�r den Vorgesetzten";
$MESS["PP_USER_CONDITION_SUBORDINATE_T1"] = "Stimmenanzahl f�r Mitarbeiter";
$MESS["PP_USER_CONDITION_SUBORDINATE_T2"] = "50% von dem Vorgesetztenmaximum";
$MESS["PP_USER_CONDITION_SUBORDINATE_T3"] = "75% von dem Vorgesetztenmaximum";
$MESS["PP_USER_CONDITION_SUBORDINATE_T4"] = "100% von dem Vorgesetztenmaximum";
?>