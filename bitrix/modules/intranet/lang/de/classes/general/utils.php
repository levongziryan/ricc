<?
$MESS["INTR_MAIL_DOMAINREADY_NOTICE"] = "Die Domain [b]#DOMAIN#[/b] wurde angebunden. <a href=\"#SERVER#/company/personal/mail/manage/\">Mail-Konten verwalten</a>";
$MESS["INTR_MAIL_DOMAIN_SUPPORT_LINK"] = "http://www.bitrix.de/support/?utm_source=regru&utm_medium=email&utm_campaign=regru_email_support";
$MESS["INTR_MAIL_DOMAIN_SUPPORTB24_LINK"] = "http://www.bitrix24.de/support/helpdesk/?utm_source=regru&utm_medium=email&utm_campaign=regru_email_support";
$MESS["INTR_MAIL_DOMAIN_LEARNMORE_LINK"] = "http://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=55&CHAPTER_ID=06527?utm_source=regru&utm_medium=email&utm_campaign=regru_email_howto";
$MESS["INTR_MAIL_DOMAIN_LEARNMOREB24_LINK"] = "http://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=55&CHAPTER_ID=06527?utm_source=regru&utm_medium=email&utm_campaign=regru_email_howto";
$MESS["INTR_SYNC_OUTLOOK_NOWEBSERVICE"] = "Für eine Synchronisierung mit Outlook ist das Modul Web Services erforderlich. Wenden Sie sich bitte an Ihren Portal-Administrator.";
?>