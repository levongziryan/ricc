<?
$MESS["I_NEW_USER_TITLE"] = "Neuer Mitarbeiter wurde hinzugefügt";
$MESS["I_NEW_USER_TITLE_SETTINGS"] = "Neue Mitarbeiter";
$MESS["I_NEW_USER_TITLE_LIST"] = "Neuer Mitarbeiter";
$MESS["I_NEW_USER_MENTION"] = "hat Sie in einem Kommentar zu einer Nachricht darüber erwähnt, dass ein neuer Mitarbeiter #title# hinzugefügt wurde. ";
$MESS["I_NEW_USER_MENTION_M"] = "hat Sie in einem Kommentar zu einer Nachricht darüber erwähnt, dass ein neuer Mitarbeiter #title# hinzugefügt wurde. ";
$MESS["I_NEW_USER_MENTION_F"] = "hat Sie in einem Kommentar zu einer Nachricht darüber erwähnt, dass ein neuer Mitarbeiter #title# hinzugefügt wurde. ";
$MESS["I_NEW_USER_EXTERNAL_TITLE"] = "Ein neuer externer Nutzer wurde hinzugefügt";
?>