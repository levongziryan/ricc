<?
$MESS["BX24_INVITE_TITLE_INVITE"] = "Mitarbeiter einladen";
$MESS["BX24_INVITE_TITLE_ADD"] = "Mitarbeiter hinzufügen";
$MESS["BX24_INVITE_BUTTON"] = "Einladen";
$MESS["BX24_CLOSE_BUTTON"] = "Schließen";
$MESS["BX24_LOADING"] = "Wird geladen...";
$MESS["BX24_INVITE_DIALOG_EMAIL_ERROR"] = "Die E-Mail-Adressen sind  nicht korrekt:<br/>";
$MESS["BX24_INVITE_DIALOG_EMAIL_LIMIT_EXCEEDED"] = "Das Limit der E-Mail-Adressen in der Einladung wurde überschritten.";
$MESS["BX24_INVITE_DIALOG_USER_ID_NO_EXIST_ERROR"] = "Die Nutzer-ID ist nicht korrekt.";
$MESS["BX24_INVITE_DIALOG_USER_EXIST_ERROR"] = "Nutzer mit diesen E-Mail-Adressen existieren bereits.";
$MESS["BX24_INVITE_DIALOG_MAX_COUNT_ERROR"] = "Die Anzahl der Einladungen überschreitet das in Ihrer Lizenz festgelegte Limit.";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT"] = "Ich lade Sie in unser Bitrix24 Account ein. Hier können wir gemeinsam an Projekten und Aufgaben arbeiten, Termine und Besprechungen planen, Dokumente verwalten, miteinander kommunizieren, Kundendaten verwalten usw.";
$MESS["BX24_INVITE_DIALOG_ERROR_EMPTY_EMAIL_LIST"] = "E-Mail-Adressen sind nicht angegeben.";
$MESS["BX24_INVITE_DIALOG_ERROR_EMPTY_EMAIL"] = "E-Mail-Adresse ist nicht angegeben.";
$MESS["BX24_INVITE_DIALOG_ERROR_EMPTY_LAST_NAME"] = "Der Nachname ist erforderlich.";
$MESS["BX24_INVITE_DIALOG_ERROR_EMPTY_PASSWORD"] = "Passwort ist nicht angegeben.";
$MESS["BX24_INVITE_DIALOG_ERROR_WRONG_PASSWORD_CONFIRM"] = "Passwort und die Passwortbestätigung stimmen nicht überein.";
$MESS["BX24_INVITE_DIALOG_ERROR_WRONG_USER"] = "Der E-Mail-Nutzer ist nicht korrekt.";
$MESS["BX24_INVITE_DIALOG_ERROR_USER_TRANSFER"] = "Der E-Mail-Nutzer kann nicht konvertiert werden.";
?>