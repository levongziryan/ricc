<?
$MESS["authprov_group"] = "Abteilung";
$MESS["authprov_check_d"] = "Alle Abteilungsmitarbeiter";
$MESS["authprov_check_dr"] = "Alle Mitarbeiter der Abteilung und Unterabteilungen";
$MESS["authprov_panel_last"] = "Letzte";
$MESS["authprov_panel_group"] = "Aus der Struktur ausw�hlen";
$MESS["authprov_panel_search"] = "Suchen";
$MESS["authprov_panel_search_text"] = "Geben Sie den Nutzerlogin, oder den Vor- oder Nachnamen des Nutzers, oder den Abteilungsnamen ein.";
$MESS["authprov_name_out_group"] = "Abteilung";
$MESS["authprov_name"] = "Mitarbeiter und Abteilungen";
$MESS["authprov_group_extranet"] = "Extranet";
$MESS["authprov_name_out_user1"] = "Mitarbeiter und Vorgesetzten";
?>