<?
$MESS["INTASK_WF_TMPL_SNMA_M_SetResponsibleEvent"] = "Neue zust�ndige Person f�r die Aufgabe \"[url={=Template:PathTemplate}]{=Document:NAME}[/url]\"";
$MESS["INTASK_WF_TMPL_SNMA95_M_NotAccepted"] = "Neue Aufgabe \"[url={=Template:PathTemplate}]{=Document:NAME}[/url]\" wurde erstellt.";
$MESS["INTASK_WF_TMPL_HEEA_T_ApproveEvent"] = "Annehmen";
$MESS["INTASK_WF_TMPL_HEEA_T_CloseEvent"] = "Schlie�en";
$MESS["INTASK_WF_TMPL_Closed"] = "Geschlossen";
$MESS["INTASK_WF_TMPL_HEEA_T_CompleteEvent"] = "Abschlie�en";
$MESS["INTASK_WF_TMPL_Completed"] = "Abgeschlossen";
$MESS["INTASK_WF_TMPL_HEEA_T_DeferredEvent"] = "Zur�ckstellen";
$MESS["INTASK_WF_TMPL_Deferred"] = "Zur�ckgestellt";
$MESS["INTASK_WF_TMPL_InProgress"] = "In Bearbeitung";
$MESS["INTASK_WF_TMPL_SNMA95_MT_NotAccepted"] = "Benachrichtigung �ber die neue Aufgabe";
$MESS["INTASK_WF_TMPL_NotAccepted"] = "Nicht angenommen";
$MESS["INTASK_WF_TMPL_HEEA_T_NotStartedEvent"] = "Nicht angefangen";
$MESS["INTASK_WF_TMPL_NotStarted"] = "Nicht angefangen";
$MESS["INTASK_WF_TMPL_SNMA_MT_NotStartedEvent"] = "Benachrichtigung";
$MESS["INTASK_WF_TMPL_HEEA_T_WaitingEvent"] = "Unerledigt";
$MESS["INTASK_WF_TMPL_Waiting"] = "Unerledigt";
$MESS["INTASK_WF_TMPL_SLAX_LOGTEXT"] = "Priorit�t: {=Document:PROPERTY_TaskPriority}\\r\\nStart: {=Document:ACTIVE_FROM}\\r\\nEnde: {=Document:ACTIVE_TO}\\r\\nVerantwortlich: {=Document:PROPERTY_TaskAssignedTo_PRINTABLE}";
$MESS["INTASK_WF_TMPL_SNMA_MT_SetResponsibleEvent"] = "Benachrichtigung �ber die �nderung der verantwortlichen Person";
$MESS["INTASK_WF_TMPL_SSA_S_CloseEvent"] = "\"Geschlossen\"-Status setzen";
$MESS["INTASK_WF_TMPL_SSA_S_CompleteEvent"] = "\"Abgeschlossen\"-Status setzen";
$MESS["INTASK_WF_TMPL_SSA_S_DeferredEvent"] = "\"Zur�ckgenommen\"-Status setzen";
$MESS["INTASK_WF_TMPL_SSA_S_InProgressEvent"] = "\"In Bearbeitung\"-Status setzen";
$MESS["INTASK_WF_TMPL_SSA_S_SetResponsibleEvent"] = "\"Nicht akzeptiert\"-Status setzen";
$MESS["INTASK_WF_TMPL_SSA_S_ApproveEvent"] = "\"Nicht begonnen\"-Status setzen";
$MESS["INTASK_WF_TMPL_SSA_S_NotStartedEvent"] = "\"Nicht begonnen\"-Status setzen";
$MESS["INTASK_WF_TMPL_SSA_S_WaitingEvent"] = "\"Suspendiert\"-Status setzen";
$MESS["INTASK_WF_TMPL_HEEA_T_SetResponsibleEvent"] = "Verantwortliche Person bestimmen";
$MESS["INTASK_WF_TMPL_HEEA_T_InProgressEvent"] = "Ausf�hrung beginnen";
$MESS["INTASK_WF_TMPL_SNMA_MT_ApproveEvent"] = "Benachrichtigung �ber die Akzeptierung der Aufgabe";
$MESS["INTASK_WF_TMPL_BP_NAME"] = "Vorlage f�r die Gesch�ftsprozessaufgabe";
$MESS["INTASK_WF_TMPL_NAME"] = "Vorlage f�r die Gesch�ftsprozessaufgabe";
$MESS["INTASK_WF_TMPL_DESC"] = "Vorlage f�r die Gesch�ftsprozessaufgabe";
$MESS["INTASK_WF_TMPL_SNMA_MT_CloseEvent"] = "Benachrichtigung �ber das Schlie�en der Aufgabe";
$MESS["INTASK_WF_TMPL_SNMA_MT_CompleteEvent"] = "Benachrichtigung �ber die Erledigung der Aufgabe";
$MESS["INTASK_WF_TMPL_SNMA_MT_DeferredEvent"] = "Benachrichtigung �ber das Zur�cknehmen der Aufgabe";
$MESS["INTASK_WF_TMPL_SNMA_MT_InProgressEvent"] = "Benachrichtigung �ber den Beginn der Aufgabe";
$MESS["INTASK_WF_TMPL_SNMA_MT_WaitingEvent"] = "Benachrichtigung �ber die Suspendierung der Aufgabe";
$MESS["INTASK_WF_TMPL_SLAX_NotAccepted_LOGTITLE"] = "Die Aufgabe \"#TITLE# hat den \"Nicht akzeptiert\"-Status.";
$MESS["INTASK_WF_TMPL_SLAX_Closed_LOGTITLE"] = "Die Aufgabe \"#TITLE# hat den \"Geschlossen\"-Status.";
$MESS["INTASK_WF_TMPL_SLAX_Completed_LOGTITLE"] = "Die Aufgabe \"#TITLE# hat den \"Abgeschlossen\"-Status.";
$MESS["INTASK_WF_TMPL_SLAX_Deferred_LOGTITLE"] = "Die Aufgabe \"#TITLE# hat den \"Zur�ckgenommen\"-Status.";
$MESS["INTASK_WF_TMPL_SLAX_InProgress_LOGTITLE"] = "Die Aufgabe \"#TITLE# hat den \"In Bearbeitung\"-Status.";
$MESS["INTASK_WF_TMPL_SLAX_NotStarted_LOGTITLE"] = "Die Aufgabe \"#TITLE# hat den \"Nicht begonnen\"-Status.";
$MESS["INTASK_WF_TMPL_SLAX_Waiting_LOGTITLE"] = "Die Aufgabe \"#TITLE# hat den \"Wartet\"-Status.";
$MESS["INTASK_WF_TMPL_SLAXR_SetResponsibleEvent"] = "Die Aufgabe \"#TITLE# hat die zust�ndige Person ge�ndert.";
$MESS["INTASK_WF_TMPL_SNMA_M_CompleteEvent"] = "Die Aufgabe \"[url={=Template:PathTemplate}]{=Document:NAME}[/url]\" wurde erledigt.";
$MESS["INTASK_WF_TMPL_SNMA_M_DeferredEvent"] = "Die Aufgabe \"[url={=Template:PathTemplate}]{=Document:NAME}[/url]\" wurde zur�ckgenommen.";
$MESS["INTASK_WF_TMPL_SNMA_M_InProgressEvent"] = "Die Aufgabe \"[url={=Template:PathTemplate}]{=Document:NAME}[/url]\" hat begonnen.";
$MESS["INTASK_WF_TMPL_SNMA_M_WaitingEvent"] = "Die Aufgabe \"[url={=Template:PathTemplate}]{=Document:NAME}[/url]\" wurde suspendiert.";
$MESS["INTASK_WF_TMPL_SNMA_M_CloseEvent"] = "Die Aufgabe \"[url={=Template:PathTemplate}]{=Document:NAME}[/url]\" wurde geschlossen.";
$MESS["INTASK_WF_TMPL_SNMA_M_ApproveEvent"] = "Die Aufgabe \"[url={=Template:PathTemplate}]{=Document:NAME}[/url]\" wurde von der zust�ndigen Person akzeptiert.";
$MESS["INTASK_WF_TMPL_SNMA_M_NotStartedEvent"] = "Die Aufgabe \"[url={=Template:PathTemplate}]{=Document:NAME}[/url]\" hat nicht begonnen.";
$MESS["INTASK_WF_TMPL_FRAXR_NotAccepted_ONINIT"] = "Die Aufgabe hat den \"Nicht akzeptiert\"-Status.";
$MESS["INTASK_WF_TMPL_FRAXR_Closed_ONINIT"] = "Die Aufgabe hat den \"Geschlossen\"-Status.";
$MESS["INTASK_WF_TMPL_FRAXR_Completed_ONINIT"] = "Die Aufgabe hat den \"Abgeschlossen\"-Status.";
$MESS["INTASK_WF_TMPL_FRAXR_Deferred_ONINIT"] = "Die Aufgabe hat den \"Zur�ckgenommen\"-Status.";
$MESS["INTASK_WF_TMPL_FRAXR_InProgress_ONINIT"] = "Die Aufgabe hat den \"In Bearbeitung\"-Status.";
$MESS["INTASK_WF_TMPL_FRAXR_NotStarted_ONINIT"] = "Die Aufgabe hat den \"Nicht begonnen\"-Status.";
$MESS["INTASK_WF_TMPL_FRAXR_Waiting_ONINIT"] = "Die Aufgabe hat den \"Wartet\"-Status.";
$MESS["INTASK_WF_TMPL_FRAXR_SetResponsibleEvent"] = "Die zust�ndige Person f�r diese Aufgabe wurde ge�ndert. Neue zust�ndige Person ist {=Document:PROPERTY_TaskAssignedTo_PRINTABLE}";
?>