<?
$MESS["INTRANET_RATING_NAME"] = "Intranet";
$MESS["INTRANET_RATING_USER_SUBORDINATE_NAME"] = "Zusätzliche Stimmen aufgrund der Unternehmensstruktur";
$MESS["INTRANET_RATING_USER_SUBORDINATE_DESC"] = "Der Wert basiert auf Daten, welche mit der Regel \"Unternehmensstruktur\" errechnet wurden.";
$MESS["INTRANET_RATING_USER_SUBORDINATE_FORMULA_DESC"] = "SubordinateValue - zusätzliche Stimmen; K - nutzerdefinierter Faktor.";
?>