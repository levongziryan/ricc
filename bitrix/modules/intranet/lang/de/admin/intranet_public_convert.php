<?
$MESS["INTRANET_PUBLIC_CONVERT_TITLE"] = "Öffentliche Seite konvertieren";
$MESS["INTRANET_PUBLIC_CONVERT_DESC"] = "Die neue Bitrix24 Vorlage enthält aktualisierte und verbesserte Navigation und bietet zudem ein kompaktes personaliertes Linksmenü an. Nutzer können jetzt jeden Bereich anpassen. Der Arbeitsbereich ist jetzt um 20% breiter. Das Menü oben enthält ein einheitliches Navigationssystem.";
$MESS["INTRANET_PUBLIC_CONVERT_DESC_TITLE"] = "Konvertieren Sie Ihre öffentlichen Seiten, um die neuen Funktionen sofort zu testen:";
$MESS["INTRANET_PUBLIC_CONVERT_DESC2"] = "<ul> 
<li>Einfach anzupassendes linkes Menü</li>
<li>Wichtige Tools sind zur Hand</li>
<li>Personalisiertes Haupttool, das Sie besonders oft nutzen (CRM, Aufgaben oder Activity Stream), welches an konkrete Kunden angepasst werden kann</li>
<li>Der Arbeitsbereich wurde um 20% erweitert: das linke Menü kann jetzt ausgeblendet werden, wenn man es nicht benötigt</li>
<li>Einheitliche Navigation im oberen Menü: alle Menüs der zweiten Ebene haben nun die gleiche Struktur</li>
<li>Seiten können nach Namen aus dem Suchfeld schneller gefunden werden</li>
</ul>
";
$MESS["INTRANET_PUBLIC_CONVERT_OPTIONS_TITLE"] = "Konvertieren Sie öffentliche Seiten jetzt";
$MESS["INTRANET_PUBLIC_CONVERT_OPTIONS_DESC"] = "(es wird empfohlen, eine Sicherungskopie von Ihren öffentlichen Seiten zu erstellen, wenn Sie später wieder die vorherige Struktur nutzen möchten)";
$MESS["INTRANET_PUBLIC_CONVERT_SECTIONS"] = "Bereiche (\"Zeit und Berichte\", \"Kalender\", \"Workflows\" und \"Bitrix24.Drive\" werden erstellt)";
$MESS["INTRANET_PUBLIC_CONVERT_INDEX"] = "Sie können einen beliebigen Bereich zu Ihrer Startseite machen (indem die Datei index.php überschrieben und der Activity Stream in den Ordner /stream/ kopiert wird)";
$MESS["INTRANET_PUBLIC_CONVERT_TAB"] = "Konvertierung";
$MESS["INTRANET_PUBLIC_CONVERT_TAB_TITLE"] = "Konvertierungsparameter bearbeiten";
$MESS["INTRANET_PUBLIC_CONVERT_BUTTON"] = "Jetzt konvertieren";
$MESS["INTRANET_PUBLIC_CONVERT_FINISH"] = "Konvertierung wurde abgeschlossen.";
$MESS["INTRANET_PUBLIC_SKIP_CONVERT_BUTTON"] = "Nicht konvertieren";
$MESS["INTRANET_PUBLIC_SKIP_CONVERT_FINISH"] = "Konvertierung wurde abgebrochen";
?>