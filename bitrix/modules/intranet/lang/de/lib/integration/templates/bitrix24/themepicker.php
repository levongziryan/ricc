<?
$MESS["INTRANET_B24_INTEGRATION_CANT_CREATE_THEME"] = "Cannot create a theme using data provided";
$MESS["INTRANET_B24_INTEGRATION_UPLOAD_ERROR"] = "Fehler beim Hochladen der Datei.";
$MESS["INTRANET_B24_INTEGRATION_THEMES_LIMIT_EXCEEDED"] = "Maximale Anzahl eigener Themen überschritten (#NUM#).";
?>