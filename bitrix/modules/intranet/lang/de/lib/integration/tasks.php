<?
$MESS["SONET_TASK_DESCRIPTION"] = "Füllen Sie Ihr Profil aus";
$MESS["SONET_TASK_DESCRIPTION_V2"] = "Füllen Sie Ihr Profil aus, laden Sie Ihr Nutzerbild hoch und geben Sie Ihre persönlichen Informationen an.

#ANCHOR_EDIT_PROFILE#Profil ausfüllen#ANCHOR_END#
";
$MESS["SONET_INVITE_TASK_TITLE"] = "Kollegen einladen";
$MESS["SONET_INVITE_TASK_DESCRIPTION"] = "Laden Sie Ihre Kollegen in Ihr Bitrix24 ein.

#ANCHOR_INVITE#Einladen#ANCHOR_END#
";
$MESS["SONET_INSTALL_APP_TASK_TITLE"] = "Bitrix24 Anwendung herunterladen";
$MESS["SONET_INSTALL_APP_TASK_DESCRIPTION"] = "Installieren Sie die Bitrix24 App auf Ihrem mobilen Gerät und bleiben Sie so stets im Kontakt. 

Installieren Sie die Desktop Anwendung auf Ihrem Computer (Mac, Windows oder Linux): so können Sie mit Ihren Kollegen auch dann kommunizieren, wenn der Browser geschlossen ist.

[URL=https://www.bitrix24.de/features/mobile.php]Bitrix24 Anwendungen herunterladen[/URL]
";
$MESS["SONET_TASK_TITLE"] = "Profil ausfüllen";
?>