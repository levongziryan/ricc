<?
$MESS["WS_OUTLOOK_APP_TITLE"] = "Microsoft Outlook Services";
$MESS["WS_OUTLOOK_APP_OPTIONS_CAPTION"] = "Verbinden";
$MESS["WS_OUTLOOK_APP_TITLE_OPTION"] = "Kontakte, Kalender, Aufgaben";
$MESS["WS_OUTLOOK_APP_DESC"] = "Passwort für Microsoft Outlook anfordern, um Kalender, Kontakte oder Aufgaben zu synchronisieren.";
?>