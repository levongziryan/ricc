<?
$MESS["TITLE"] = "Налаштування";
$MESS["STATUS"] = "Довідники";
$MESS["CURRENCY"] = "Валюти";
$MESS["TAX"] = "Податки";
$MESS["PS"] = "Способи оплати";
$MESS["LOCATIONS"] = "Місця розташування";
$MESS["PERMS"] = "Права доступу";
$MESS["BP"] = "Бізнес-процеси";
$MESS["FIELDS"] = "Поля користувача ";
$MESS["CONFIG"] = "Інше";
$MESS["SENDSAVE"] = "Інтеграція з поштою";
$MESS["EXTERNAL_SALE"] = "Інтернет-магазини";
$MESS["EXCH1C"] = "Інтеграція з \"1С:Підприємтсво\"";
$MESS["MAIL_TEMPLATES"] = "Поштові шаблони";
$MESS["MEASURE"] = "Одиниці виміру";
$MESS["PRODUCT_PROPS"] = "Властивості товарів";
$MESS["SLOT"] = "Аналітичні звіти";
?>