<?
$MESS["MENU_LIVE_FEED"] = "Жива стрічка";
$MESS["MENU_TASKS"] = "Завдання";
$MESS["MENU_CALENDAR"] = "Календар";
$MESS["MENU_SITES"] = "Сайти";
$MESS["MENU_FILES"] = "Файли";
$MESS["MENU_PHOTO"] = "Фотографії";
$MESS["MENU_BLOG"] = "Повідомлення";
$MESS["MENU_MAIL"] = "Пошта";
$MESS["MENU_GROUP_SECTION"] = "Групи";
$MESS["MENU_CRM"] = "CRM";
$MESS["MENU_CRM_WEBFORM"] = "CRM-форми";
$MESS["MENU_COMPANY_SECTION"] = "Компанія";
$MESS["MENU_SETTINGS_SECTION"] = "Налаштування";
$MESS["MENU_TARIFF"] = "Мій тариф";
$MESS["MENU_TELEPHONY_SECTION"] = "Телефонія";
$MESS["MENU_OPENLINES_LINES_SINGLE"] = "Відкриті лінії";
$MESS["MENU_DISK_SECTION"] = "Диск";
$MESS["MENU_TIMEMAN_SECTION"] = "Час та звіти";
$MESS["MENU_FAVOURITE_TITLE"] = "ОБРАНЕ";
$MESS["MENU_IM_MESSENGER"] = "Чат і дзвінки";
$MESS["MENU_MAIL_CHANGE_SETTINGS"] = "Змінити налаштування";
$MESS["MENU_GROUPS"] = "ГРУПИ";
$MESS["MENU_ALL_GROUPS"] = "Всі групи";
$MESS["MENU_GROUPS_EXTRANET"] = "ГРУПИ ЕКСТРАНЕТУ";
$MESS["MENU_COMPANY"] = "КОМПАНІЯ";
$MESS["MENU_EMPLOYEE"] = "Співробітники";
$MESS["MENU_SHARED_DOCS"] = "Загальні документи";
$MESS["MENU_STRUCTURE"] = "Структура компанії";
$MESS["MENU_ABSENCE"] = "Графік відсутностей";
$MESS["MENU_TIMEMAN"] = "Робочий час";
$MESS["MENU_WORK_REPORT"] = "Робочі звіти";
$MESS["MENU_MEETING"] = "Збори і планерки";
$MESS["MENU_LISTS"] = "Списки";
$MESS["MENU_SETTINGS"] = "НАЛАШТУВАННЯ";
$MESS["MENU_CONFIGS"] = "Налаштування порталу";
$MESS["MENU_MAIL_MANAGE"] = "Управління поштою";
$MESS["MENU_LICENSE"] = "Ліцензія";
$MESS["MENU_BIZPROC"] = "Бізнес-процеси";
$MESS["MENU_MARKETPLACE_ALL"] = "Всі застосунки";
$MESS["MENU_MARKETPLACE_ADD"] = "Додати застосунок";
$MESS["MENU_MARKETPLACE_LOCAL"] = "Мої застосунки ";
$MESS["MENU_MARKETPLACE_APPS"] = "Застосунки";
$MESS["MENU_ONEC_SECTION"] = "1С + CRM Бітрікс24";
$MESS["MENU_TELEPHONY_CATEGORY"] = "ТЕЛЕФОНІЯ";
$MESS["MENU_TELEPHONY_BALANCE"] = "Баланс і статистика";
$MESS["MENU_TELEPHONY"] = "Налаштування телефонії";
$MESS["MENU_TELEPHONY_PERMISSIONS"] = "Права доступу";
$MESS["MENU_TELEPHONY_LINES"] = "Управління номерами";
$MESS["MENU_TELEPHONY_USERS"] = "Користувачі";
$MESS["MENU_TELEPHONY_PHONES"] = "Апарати";
$MESS["MENU_MY_PROCESS"] = "Мої процеси";
$MESS["MENU_PROCESS"] = "Процеси";
$MESS["MENU_PROCESS_STREAM"] = "Процеси в стрічці ";
$MESS["MENU_BIZPROC_TASKS"] = "Завдання бізнес-процесів ";
$MESS["MENU_BIZPROC_ACTIVE"] = "Всі активні";
$MESS["MENU_TARIFF_INFO"] = "Мій «Бітрікс24»";
$MESS["MENU_TARIFF_ALL"] = "Розширені тарифи";
$MESS["MENU_TARIFF_DEMO"] = "Демо-режим";
$MESS["MENU_TARIFF_ORDERS"] = "Історія замовлень";
$MESS["MENU_BUSINESS_TOOLS"] = "Бізнес-інструменти";
$MESS["MENU_COMPANY_CALENDAR"] = "Календар компанії";
$MESS["MENU_IMOL_CATEGORY"] = "ВІДКРИТІ ЛІНІЇ";
$MESS["MENU_IMOL_PERMISSIONS"] = "Права доступу";
$MESS["MENU_IMOL_BUTTON"] = "Віджет на сайт";
$MESS["MENU_IMOL_STATISTICS"] = "Статистика";
$MESS["MENU_IMOL_DETAILED_STATISTICS"] = "Детальна статистика";
$MESS["MENU_IMOL_LIST_LINES"] = "Список";
$MESS["MENU_TELEPHONY_NUMBER"] = "Налаштування номерів";
$MESS["MENU_CRM_BUTTON"] = "Віджет на сайт";
$MESS["MENU_CRM_MARKETING"] = "CRM-маркетинг";
?>