<?
$MESS["MENU_TELEPHONY_BALANCE"] = "Баланс та статистика";
$MESS["MENU_TELEPHONY"] = "Налаштування";
$MESS["MENU_TELEPHONY_PERMISSIONS"] = "Права доступу";
$MESS["MENU_TELEPHONY_LINES"] = "Номери";
$MESS["MENU_TELEPHONY_USERS"] = "Користувачі";
$MESS["MENU_TELEPHONY_PHONES"] = "Апарати";
$MESS["MENU_TELEPHONY_CONNECT"] = "Підключення";
$MESS["MENU_TELEPHONY_GROUPS"] = "Групи";
$MESS["MENU_TELEPHONY_IVR"] = "Налаштування IVR";
?>