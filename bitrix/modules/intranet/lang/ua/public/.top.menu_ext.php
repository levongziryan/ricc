<?
$MESS["TOP_MENU_FAVORITE"] = "Вибране";
$MESS["TOP_MENU_GROUPS"] = "Групи";
$MESS["TOP_MENU_GROUPS_EXTRANET"] = "Групи екстранет";
$MESS["TOP_MENU_DEPARTMENTS"] = "Підрозділи";
$MESS["TOP_MENU_TELEPHONY"] = "Телефонія";
$MESS["TOP_MENU_MARKETPLACE"] = "Застосунки";
$MESS["TOP_MENU_OPENLINES"] = "Відкриті лінії";
$MESS["TOP_MENU_LIVE_FEED"] = "Жива стрічка";
$MESS["TOP_MENU_TASKS"] = "Завдання";
$MESS["TOP_MENU_CALENDAR"] = "Календар";
$MESS["TOP_MENU_DISK"] = "Диск";
$MESS["TOP_MENU_PHOTO"] = "Фотографії";
$MESS["TOP_MENU_BLOG"] = "Повідомлення";
$MESS["TOP_MENU_MAIL"] = "Пошта";
$MESS["TOP_MENU_CRM"] = "CRM";
$MESS["TOP_MENU_BIZPROC"] = "Бізнес-процеси";
$MESS["TOP_MENU_TIMEMAN"] = "Час і звіти";
$MESS["TOP_MENU_IM_MESSENGER"] = "Чат та дзвінки";
$MESS["TOP_MENU_COMPANY"] = "Співробітники";
$MESS["TOP_MENU_CONFIGS"] = "Налаштування";
$MESS["TOP_MENU_ONEC"] = "1С + CRM Бітрікс24";
?>