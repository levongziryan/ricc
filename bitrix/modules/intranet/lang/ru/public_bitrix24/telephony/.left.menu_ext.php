<?
$MESS["MENU_TELEPHONY_BALANCE"] = "Баланс и статистика";
$MESS["MENU_TELEPHONY"] = "Настройки";
$MESS["MENU_TELEPHONY_PERMISSIONS"] = "Права доступа";
$MESS["MENU_TELEPHONY_LINES"] = "Номера";
$MESS["MENU_TELEPHONY_CONNECT"] = "Подключение";
$MESS["MENU_TELEPHONY_USERS"] = "Пользователи";
$MESS["MENU_TELEPHONY_PHONES"] = "Аппараты";
$MESS["MENU_TELEPHONY_GROUPS"] = "Группы";
$MESS["MENU_TELEPHONY_IVR"] = "Настройка IVR";
?>