<?
$MESS["BCMMP_ON"] = "Activé";
$MESS["BCMMP_OFF"] = "Désactivé";
$MESS["BCMMP_DOMAINS_TITLE"] = "Domaines";
$MESS["BCMMP_TITLE"] = "Inspecteur des sites";
$MESS["BCMMP_BACK"] = "Précédent";
$MESS["BCMMP_TITLE2"] = "Réglage des notifications poussées";
$MESS["BCMMP_NO_DOMAINS"] = "Pas de domaines réglés";
$MESS["BCMMP_JS_SAVE_ERROR"] = "Erreur de sauvegarde.";
$MESS["BCMMP_PUSH_RECIEVE"] = "Recevoir une notification";
$MESS["BCMMP_SAVE"] = "Sauvegarder";
$MESS["BCMMP_JS_SAVING"] = "Sauvegarde";
?>