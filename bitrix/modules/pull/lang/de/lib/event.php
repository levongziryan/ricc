<?
$MESS["PULL_EVENT_RECIPIENT_FORMAT_ERROR"] = "Fehler im Format des Event-Empfängers";
$MESS["PULL_EVENT_PARAMETERS_FORMAT_ERROR"] = "Fehler im Event-Parameter";
$MESS["PULL_EVENT_PUSH_PARAMETERS_FORMAT_ERROR"] = "Fehler im Format der Push-Benachrichtigung";
$MESS["PULL_EVENT_CALLBACK_FORMAT_ERROR"] = "Fehler im Format der Call-Methode der Push-Benachrichtigung";
$MESS["PULL_EVENT_PUSH_CALLBACK_FORMAT_ERROR"] = "Fehler im Format der Call-Methode der Push-Benachrichtigung";
$MESS["PULL_EVENT_PUSH_DISABLED_ERROR"] = "Push-Benachrichtigungen sind in den Moduleinstellungen deaktiviert";
?>