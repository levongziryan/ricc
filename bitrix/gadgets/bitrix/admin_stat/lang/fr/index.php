<?
$MESS["GD_STAT_TOTAL"] = "Total";
$MESS["GD_STAT_YESTERDAY"] = "Hier";
$MESS["GD_STAT_NOT_ENOUGH_DATA"] = "Données insuffisantes pour faire une abaque.";
$MESS["GD_STAT_B_YESTERDAY"] = "Avant-hier";
$MESS["GD_STAT_VISITORS"] = "Visiteurs";
$MESS["GD_STAT_TAB_COMMON"] = "Fréquentation";
$MESS["GD_STAT_TAB_ADV"] = "Adv. Campagne";
$MESS["GD_STAT_TAB_REF"] = "Liste de sites";
$MESS["GD_STAT_TODAY"] = "Aujourd'hui";
$MESS["GD_STAT_SESSIONS"] = "Accéder à la liste des sessions";
$MESS["GD_STAT_EVENTS"] = "Evénements";
$MESS["GD_STAT_TAB_EVENT"] = "Evénements";
$MESS["GD_STAT_TAB_PHRASE"] = "Liste des phrases";
$MESS["GD_STAT_HITS"] = "Hits";
$MESS["GD_STAT_HOSTS"] = "Hébergeurs";
?>