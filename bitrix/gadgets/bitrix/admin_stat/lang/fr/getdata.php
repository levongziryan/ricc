<?
$MESS["GD_STAT_TOTAL_1"] = "Total";
$MESS["GD_STAT_YESTERDAY"] = "Hier";
$MESS["GD_STAT_EVENT_GRAPH"] = "Planning du type d'événement par jours";
$MESS["GD_STAT_NO_DATA"] = "Pas de données.";
$MESS["GD_STAT_BEFORE_YESTERDAY"] = "Avant-hier";
$MESS["GD_STAT_ADV_NAME"] = "Campagne publicitaire";
$MESS["GD_STAT_SERVER"] = "Site web";
$MESS["GD_STAT_TODAY"] = "Aujourd'hui";
$MESS["GD_STAT_EVENT"] = "Evénements";
$MESS["GD_STAT_PHRASE"] = "Mot-clé";
?>