<?
$MESS["GD_STAT_P_SITE_ID_ALL"] = "Tous";
$MESS["GD_STAT_P_GRAPH_HEIGHT"] = "Hauteur";
$MESS["GD_STAT_P_GRAPH_PARAMS"] = "Paramètres pour le graphique";
$MESS["GD_STAT_P_GUEST"] = "Visiteurs";
$MESS["GD_STAT_P_SITE_ID"] = "Site web";
$MESS["GD_STAT_P_SESSION"] = "Accéder à la liste des sessions";
$MESS["GD_STAT_P_HIDE_GRAPH"] = "Cacher l'abaque";
$MESS["GD_STAT_P_EVENT"] = "Evénements";
$MESS["GD_STAT_P_GRAPH_DAYS"] = "Construire un graphe pour les N derniers jours";
$MESS["GD_STAT_P_HIT"] = "Hits";
$MESS["GD_STAT_P_HOST"] = "Hébergeurs";
$MESS["GD_STAT_P_GRAPH_WIDTH"] = "Largeur";
?>