<?
$MESS["GD_CRM_EVENT_TYPE"] = "Entités dans la liste";
$MESS["GD_CRM_EVENT_TYPE_LEAD"] = "Prospect";
$MESS["GD_CRM_EVENT_TYPE_CONTACT"] = "Client";
$MESS["GD_CRM_EVENT_TYPE_COMPANY"] = "Entreprise";
$MESS["GD_CRM_EVENT_TYPE_DEAL"] = "Affaire";
$MESS["GD_CRM_EVENT_LIST_EVENT_COUNT"] = "Nombre d'événements sur la page";
?>