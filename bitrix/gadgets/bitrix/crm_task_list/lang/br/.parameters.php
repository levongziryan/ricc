<?
$MESS["GD_CRM_TASK_ENTITY_TYPE"] = "Entidades na lista";
$MESS["GD_CRM_TASK_TYPE_LEAD"] = "Lead";
$MESS["GD_CRM_TASK_TYPE_CONTACT"] = "Contato";
$MESS["GD_CRM_TASK_TYPE_COMPANY"] = "Empresa";
$MESS["GD_CRM_TASK_TYPE_DEAL"] = "Negócio ";
$MESS["GD_CRM_TASK_LIST_EVENT_COUNT"] = "Tarefas por página";
$MESS["GD_CRM_ONLY_MY"] = "Meu apenas";
$MESS["GD_CRM_SORT"] = "Ordenando";
$MESS["GD_CRM_SORT_BY"] = "Por";
$MESS["GD_CRM_SORT_ASC"] = "Ascendente";
$MESS["GD_CRM_SORT_DESC"] = "Descendente";
$MESS["GD_CRM_COLUMN_CREATED_DATE"] = "Criado em";
$MESS["GD_CRM_COLUMN_CHANGED_DATE"] = "Modificada em";
$MESS["GD_CRM_COLUMN_CLOSED_DATE"] = "Encerrada em";
$MESS["GD_CRM_COLUMN_DATE_START"] = "Iniciado em";
?>