<?
$MESS["GD_CRM_TASK_ENTITY_TYPE"] = "Entidades de la Lista";
$MESS["GD_CRM_TASK_TYPE_LEAD"] = "Prospectos";
$MESS["GD_CRM_TASK_TYPE_CONTACT"] = "Contacto";
$MESS["GD_CRM_TASK_TYPE_COMPANY"] = "Compañía";
$MESS["GD_CRM_TASK_TYPE_DEAL"] = "Negociaciones";
$MESS["GD_CRM_TASK_LIST_EVENT_COUNT"] = "Tareas por Página";
$MESS["GD_CRM_ONLY_MY"] = "Sólo Míos";
$MESS["GD_CRM_SORT"] = "Clasificar";
$MESS["GD_CRM_SORT_BY"] = "Por";
$MESS["GD_CRM_SORT_ASC"] = "Ascendente";
$MESS["GD_CRM_SORT_DESC"] = "Decendente";
$MESS["GD_CRM_COLUMN_CREATED_DATE"] = "Creado El";
$MESS["GD_CRM_COLUMN_CHANGED_DATE"] = "Modificado El";
$MESS["GD_CRM_COLUMN_CLOSED_DATE"] = "Cerrado El";
$MESS["GD_CRM_COLUMN_DATE_START"] = "Iniciado El";
?>