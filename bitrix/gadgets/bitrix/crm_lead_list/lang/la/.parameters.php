<?
$MESS["GD_CRM_LEAD_LIST_LEAD_COUNT"] = "Propectos por Página";
$MESS["GD_CRM_ONLY_MY"] = "Sólo Míos";
$MESS["GD_CRM_SORT"] = "Clasificación";
$MESS["GD_CRM_SORT_BY"] = "Ordenar";
$MESS["GD_CRM_SORT_ASC"] = "Ascendente";
$MESS["GD_CRM_SORT_DESC"] = "Descendente";
$MESS["GD_CRM_COLUMN_STATUS"] = "Estado";
$MESS["GD_CRM_COLUMN_DATE_CREATE"] = "Creado";
$MESS["GD_CRM_COLUMN_DATE_MODIFY"] = "Modificado";
?>