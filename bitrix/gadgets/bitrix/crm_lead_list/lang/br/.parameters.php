<?
$MESS["GD_CRM_LEAD_LIST_LEAD_COUNT"] = "Leads por página";
$MESS["GD_CRM_ONLY_MY"] = "Meu apenas";
$MESS["GD_CRM_SORT"] = "Ordenando";
$MESS["GD_CRM_SORT_BY"] = "Pedido";
$MESS["GD_CRM_SORT_ASC"] = "Ascendente";
$MESS["GD_CRM_SORT_DESC"] = "Descendente";
$MESS["GD_CRM_COLUMN_STATUS"] = "Status";
$MESS["GD_CRM_COLUMN_DATE_CREATE"] = "Criado";
$MESS["GD_CRM_COLUMN_DATE_MODIFY"] = "Modificado";
?>