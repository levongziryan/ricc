<?
$MESS["GD_LOG_TITLE"] = "Activity Stream";
$MESS["GD_LOG_MORE"] = "Activity Stream";
$MESS["GD_LOG_ALL"] = "Alle";
$MESS["GD_LOG_BLOG_USER"] = "Blog";
$MESS["GD_LOG_BLOG"] = "Blog/Berichte";
$MESS["GD_LOG_CALENDAR"] = "Kalender";
$MESS["GD_LOG_FORUM_GROUP"] = "Diskussionen";
$MESS["GD_LOG_FILES"] = "Dateien";
$MESS["GD_LOG_FORUM_USER"] = "forums";
$MESS["GD_LOG_FORUM"] = "Forum/Diskussionen";
$MESS["GD_LOG_SYSTEM_FRIENDS"] = "Freunde";
$MESS["GD_LOG_GROUP"] = "Gruppen";
$MESS["GD_LOG_SYSTEM_GROUPS"] = "Gruppen";
$MESS["GD_LOG_PHOTO"] = "Foto";
$MESS["GD_LOG_BLOG_GROUP"] = "Berichte";
$MESS["GD_LOG_SYSTEM"] = "System";
$MESS["GD_LOG_TASKS"] = "Aufgaben";
$MESS["GD_LOG_USER"] = "Benutzer";
?>