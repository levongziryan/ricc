<?
$MESS["GD_LOG_P_URL"] = "URL da Página de Atualizações em Comunicação";
$MESS["GD_LOG_P_ENTITY_TYPE_VALUE_ALL"] = "Todos";
$MESS["GD_LOG_P_EVENT_ID_VALUE_ALL"] = "Todos";
$MESS["GD_LOG_P_EVENT_ID_VALUE_BLOG"] = "blogs/relatórios";
$MESS["GD_LOG_P_EVENT_ID_VALUE_CALENDAR"] = "calendários";
$MESS["GD_LOG_P_EVENT_ID_VALUE_FILES"] = "Arquivos";
$MESS["GD_LOG_P_EVENT_ID_VALUE_FORUM"] = "Fóruns/Discussões";
$MESS["GD_LOG_P_EVENT_ID_VALUE_SYSTEM_FRIENDS"] = "amigos (apenas usuários)";
$MESS["GD_LOG_P_ENTITY_TYPE_VALUE_GROUP"] = "grupos";
$MESS["GD_LOG_P_EVENT_ID_VALUE_SYSTEM_GROUPS"] = "grupos (apenas usuários)";
$MESS["GD_LOG_P_EVENT_ID"] = "Tipo de mensagem";
$MESS["GD_LOG_P_EVENT_ID_VALUE_PHOTO"] = "foto";
$MESS["GD_LOG_P_ENTITY_TYPE"] = "Tipo de Inscrição";
$MESS["GD_LOG_P_EVENT_ID_VALUE_SYSTEM"] = "sistema";
$MESS["GD_LOG_P_EVENT_ID_VALUE_TASKS"] = "tarefas";
$MESS["GD_LOG_P_ENTITY_TYPE_VALUE_USER"] = "usuários";
$MESS["GD_LOG_P_LOG_CNT"] = "Número de itens a serem exibidos";
$MESS["GD_LOG_AVATAR_SIZE"] = "Tamanho da imagem do avatar no Fluxo de Atividades (conversação)";
$MESS["GD_LOG_AVATAR_SIZE_COMMENT"] = "Tamanho do avatar do Fluxo de Atividade (para uso nos comentários)";
?>