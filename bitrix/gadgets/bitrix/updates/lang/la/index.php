<?
$MESS["GD_LOG_TITLE"] = "Actualizaciones";
$MESS["GD_LOG_MORE"] = "Todas las actualizaciones";
$MESS["GD_LOG_USER"] = "usuarios";
$MESS["GD_LOG_GROUP"] = "grupos";
$MESS["GD_LOG_ALL"] = "todo";
$MESS["GD_LOG_FORUM"] = "foros/discusiones";
$MESS["GD_LOG_FORUM_GROUP"] = "discusiones";
$MESS["GD_LOG_FORUM_USER"] = "foros";
$MESS["GD_LOG_BLOG"] = "blogs/reportes";
$MESS["GD_LOG_BLOG_GROUP"] = "reportes";
$MESS["GD_LOG_BLOG_USER"] = "blogs";
$MESS["GD_LOG_PHOTO"] = "foto";
$MESS["GD_LOG_FILES"] = "archivos";
$MESS["GD_LOG_CALENDAR"] = "calendarios";
$MESS["GD_LOG_TASKS"] = "tareas";
$MESS["GD_LOG_SYSTEM"] = "sistema";
$MESS["GD_LOG_SYSTEM_GROUPS"] = "grupos";
$MESS["GD_LOG_SYSTEM_FRIENDS"] = "amigos";
?>