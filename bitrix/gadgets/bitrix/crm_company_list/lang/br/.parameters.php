<?
$MESS["GD_CRM_COMPANY_LIST_COMPANY_COUNT"] = "Negócios por página";
$MESS["GD_CRM_ONLY_MY"] = "Meu apenas";
$MESS["GD_CRM_SORT"] = "Ordenando";
$MESS["GD_CRM_SORT_BY"] = "Pedido";
$MESS["GD_CRM_SORT_ASC"] = "Ascendente";
$MESS["GD_CRM_SORT_DESC"] = "Descendente";
$MESS["GD_CRM_COLUMN_COMPANY_TYPE"] = "Tipo da Empresa";
$MESS["GD_CRM_COLUMN_DATE_CREATE"] = "Criado";
$MESS["GD_CRM_COLUMN_DATE_MODIFY"] = "Modificado";
?>