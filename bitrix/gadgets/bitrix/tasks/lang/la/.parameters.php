<?
$MESS["GD_TASKS_P_ORDER_BY"] = "Organizar por";
$MESS["GD_TASKS_P_ORDER_BY_D1"] = "fecha de vencimiento";
$MESS["GD_TASKS_P_ORDER_BY_D2"] = "fecha de creación";
$MESS["GD_TASKS_P_ORDER_BY_D3"] = "prioridad";
$MESS["GD_TASKS_P_TYPE"] = "Mostrar Tareas";
$MESS["GD_TASKS_P_TYPE_Z"] = "asignado a mi";
$MESS["GD_TASKS_P_TYPE_U"] = "creado por mí";
$MESS["GD_TASKS_P_PATH_TO_TASK"] = "URL de la página de Tareas";
$MESS["GD_TASKS_P_PATH_TO_TASK_NEW"] = "URL de la página de Nueva Tarea";
?>