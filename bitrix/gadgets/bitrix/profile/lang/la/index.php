<?
$MESS["GD_PROFILE_GENERAL"] = "General";
$MESS["GD_PROFILE_CH_PROFILE"] = "Editar Perfil";
$MESS["GD_PROFILE_SUBSCR"] = "Mi Suscripción";
$MESS["GD_PROFILE_LOG"] = "Actualizaciones";
$MESS["GD_PROFILE_MSG"] = "Mis Mensajes";
$MESS["GD_PROFILE_GROUPS"] = "Grupos";
$MESS["GD_PROFILE_GROUP_NEW"] = "Crear Grupo";
$MESS["GD_PROFILE_BLOG"] = "Blog";
$MESS["GD_PROFILE_CAL"] = "Calendario";
$MESS["GD_PROFILE_TASKS"] = "Tareas";
$MESS["GD_PROFILE_TASK_NEW"] = "Nueva Tarea";
$MESS["GD_PROFILE_LIB"] = "Archivos";
$MESS["GD_PROFILE_PHOTO"] = "Foto";
$MESS["GD_PROFILE_PHOTO_NEW"] = "Cargar Fotos";
$MESS["GD_PROFILE_FORUM"] = "Foro";
$MESS["GD_PROFILE_BLOG_NEW"] = "Enviar al Blog";
?>