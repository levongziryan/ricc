<?
$MESS["GD_PROFILE_P_PATH_TO_GENERAL"] = "URL de la página de la información general";
$MESS["GD_PROFILE_P_PATH_TO_PROFILE_EDIT"] = "URL de la página de la edición del perfil";
$MESS["GD_PROFILE_P_PATH_TO_LOG"] = "URL de la página de actualizaciones";
$MESS["GD_PROFILE_P_PATH_TO_SUBSCR"] = "URL de la página de Mis suscripciones";
$MESS["GD_PROFILE_P_PATH_TO_MSG"] = "URL de la página de Mis Mensajes";
$MESS["GD_PROFILE_P_PATH_TO_GROUPS"] = "URL de la página de Mis Grupos";
$MESS["GD_PROFILE_P_PATH_TO_GROUP_NEW"] = "URL de la página de Creación de grupos";
$MESS["GD_PROFILE_P_PATH_TO_PHOTO"] = "URL de la página de mis Fotos";
$MESS["GD_PROFILE_P_PATH_TO_PHOTO_NEW"] = "URL de la página para agregar fotos";
$MESS["GD_PROFILE_P_PATH_TO_FORUM"] = "URL de la página del Foro";
$MESS["GD_PROFILE_P_PATH_TO_BLOG"] = "URL de la página de del Blog";
$MESS["GD_PROFILE_P_PATH_TO_BLOG_NEW"] = "URL de la página del Nuevo blog";
$MESS["GD_PROFILE_P_PATH_TO_CAL"] = "URL de la página del calendario";
$MESS["GD_PROFILE_P_PATH_TO_TASK"] = "URL de la página de las Tareas";
$MESS["GD_PROFILE_P_PATH_TO_TASK_NEW"] = "URL de la página de la nueva tarea";
$MESS["GD_PROFILE_P_PATH_TO_LIB"] = "URL de la página de Archivos";
$MESS["GD_PROFILE_P_SHOW_GENERAL"] = "Mostrar General";
$MESS["GD_PROFILE_P_SHOW_GROUPS"] = "Mostrar Grupos";
$MESS["GD_PROFILE_P_SHOW_PHOTO"] = "Mostrar fotos";
$MESS["GD_PROFILE_P_SHOW_FORUM"] = "Mostrar foro";
$MESS["GD_PROFILE_P_SHOW_CAL"] = "Mostrar Calendario";
$MESS["GD_PROFILE_P_SHOW_BLOG"] = "Mostrar Blog";
$MESS["GD_PROFILE_P_SHOW_TASK"] = "Mostrar Tareas";
$MESS["GD_PROFILE_P_SHOW_LIB"] = "Mostrar Archivos";
?>