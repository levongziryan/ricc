<?
$MESS["GD_PROFILE_P_PATH_TO_BLOG"] = "URL de la page Blog";
$MESS["GD_PROFILE_P_PATH_TO_TASK_NEW"] = "URL de création de la tâche";
$MESS["GD_PROFILE_P_PATH_TO_PHOTO_NEW"] = "Ajouter l'URL de la page des photos";
$MESS["GD_PROFILE_P_PATH_TO_BLOG_NEW"] = "URL de page d'ajout d'un nouveau message au Blog";
$MESS["GD_PROFILE_P_PATH_TO_TASK"] = "URL de la liste de tâches";
$MESS["GD_PROFILE_P_PATH_TO_PROFILE_EDIT"] = "URL de page pour modifier le profil";
$MESS["GD_PROFILE_P_PATH_TO_CAL"] = "URL de la page Calendrier";
$MESS["GD_PROFILE_P_PATH_TO_GROUPS"] = "URL de la page Mes groupes";
$MESS["GD_PROFILE_P_PATH_TO_SUBSCR"] = "L'URL de la page 'Mes abonnements'";
$MESS["GD_PROFILE_P_PATH_TO_MSG"] = "URL de la page 'Mes messages'";
$MESS["GD_PROFILE_P_PATH_TO_PHOTO"] = "URL de la page 'Mes Photos'";
$MESS["GD_PROFILE_P_PATH_TO_LOG"] = "URL de la page de Mises à jour";
$MESS["GD_PROFILE_P_PATH_TO_GENERAL"] = "URL de la page Essentiel";
$MESS["GD_PROFILE_P_PATH_TO_GROUP_NEW"] = "URL de la page Création d'un groupe";
$MESS["GD_PROFILE_P_PATH_TO_LIB"] = "Fichiers URL de la page";
$MESS["GD_PROFILE_P_PATH_TO_FORUM"] = "URL de la page Forum";
$MESS["GD_PROFILE_P_SHOW_BLOG"] = "Voir Blog";
$MESS["GD_PROFILE_P_SHOW_GROUPS"] = "Montrer les groupes";
$MESS["GD_PROFILE_P_SHOW_TASK"] = "Afficher les tâches";
$MESS["GD_PROFILE_P_SHOW_CAL"] = "Afficher Le calendrier";
$MESS["GD_PROFILE_P_SHOW_GENERAL"] = "Afficher l'Essentiel";
$MESS["GD_PROFILE_P_SHOW_LIB"] = "Afficher les Mon disque";
$MESS["GD_PROFILE_P_SHOW_FORUM"] = "Afficher le forum";
$MESS["GD_PROFILE_P_SHOW_PHOTO"] = "Afficher la Photo";
?>