<?
$MESS["GD_SONET_GROUP_DESC_ARCHIVE"] = "Groupe d'archive";
$MESS["GD_SONET_GROUP_DESC_REQUEST_SENT_MESSAGE_BY_GROUP"] = "L'invitation à ce groupe vous est déjà envoyée. Vous pouvez la confirmer à la page des <a href='#LINK#'>invitations</a>.";
$MESS["GD_SONET_GROUP_DESC_REQUEST_SENT_MESSAGE"] = "Vous avez déjà envoyé une demande à rejoindre ce groupe.";
$MESS["GD_SONET_GROUP_DESC_DESCRIPTION"] = "Description";
$MESS["GD_SONET_GROUP_DESC_NAME"] = "Groupe";
$MESS["GD_SONET_GROUP_DESC_CREATED"] = "Créé";
$MESS["GD_SONET_GROUP_DESC_SUBJECT_NAME"] = "d'après les sujets";
$MESS["GD_SONET_GROUP_DESC_TYPE"] = "Type du groupe";
$MESS["GD_SONET_GROUP_DESC_NMEM"] = "Participants";
$MESS["GD_SONET_GROUP_DESC_TYPE_V1"] = "C'est un groupe visible. Tous les utilisateurs voient sa présence.";
$MESS["GD_SONET_GROUP_DESC_TYPE_O2"] = "C'est un groupe fermé. On ne peut y faire son entrée qu'après avoir reçu l'approbation de l'administrateur.";
$MESS["GD_SONET_GROUP_DESC_TYPE_V2"] = "C'est un groupe invisible. Il n'est visible que pour les membres du groupe.";
$MESS["GD_SONET_GROUP_DESC_TYPE_O1"] = "C'est un groupe ouvert. Chacun peut y rejoindre.";
?>