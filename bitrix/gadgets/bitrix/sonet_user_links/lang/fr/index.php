<?
$MESS["GD_SONET_USER_LINKS_ONLINE"] = "En ligne";
$MESS["GD_SONET_USER_LINKS_BIRTHDAY"] = "Date de naissance";
$MESS["GD_SONET_USER_LINKS_HONOURED"] = "Honoré(e)s";
$MESS["GD_SONET_USER_LINKS_ABSENT"] = "Hors de l'office";
$MESS["GD_SONET_USER_LINKS_SEND_MESSAGE"] = "Écrire un message";
$MESS["GD_SONET_USER_LINKS_SHOW_MESSAGES"] = "Afficher la correspondance";
$MESS["GD_SONET_USER_LINKS_FR_DEL"] = "Exclure des amis";
$MESS["GD_SONET_USER_LINKS_FR_ADD"] = "Ajouter aux amis";
$MESS["GD_SONET_USER_LINKS_INV_GROUP"] = "Ceux qui peuvent inviter dans le groupe";
$MESS["GD_SONET_USER_LINKS_EDIT_PROFILE"] = "Modifier le profil";
$MESS["GD_SONET_USER_LINKS_EDIT_SETTINGS"] = "Changer le mode privé";
$MESS["GD_SONET_USER_LINKS_EDIT_FEATURES"] = "Changer les paramètres";
$MESS["GD_SONET_USER_LINKS_EDIT_EXTMAIL"] = "Réglage de l'intégration de la poste";
$MESS["GD_SONET_USER_LINKS_SUBSCR"] = "Abonnement";
$MESS["GD_SONET_USER_LINKS_SUBSCR1"] = "Gestion de ma souscription";
$MESS["GD_SONET_USER_VIDEOCALL"] = "Appel vidéo";
$MESS["GD_SONET_USER_SONET_ADMIN_ON"] = "Mode de l'administrateur";
$MESS["GD_SONET_USER_LINKS_EDIT_REQUESTS"] = "Invitations et demandes";
$MESS["GD_SONET_USER_LINKS_SECURITY"] = "Sécurité";
$MESS["GD_SONET_USER_LINKS_PASSWORDS"] = "Les mots de passe d'application";
$MESS["GD_SONET_USER_LINKS_CODES"] = "Codes de sauvegarde";
$MESS["GD_SONET_USER_LINKS_SYNCHRONIZE"] = "Synchronisation";
?>