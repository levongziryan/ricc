<?
$MESS["GD_SONET_GROUP_LINKS_SEND_MESSAGE_GROUP_TITLE"] = "Crire un message aux participants du groupe";
$MESS["GD_SONET_GROUP_LINKS_SEND_MESSAGE_GROUP"] = "Écrire un message";
$MESS["GD_SONET_GROUP_LINKS_ACT_EDIT"] = "Diter le groupe";
$MESS["GD_SONET_GROUP_LINKS_ACT_FEAT"] = "Changer les paramètres";
$MESS["GD_SONET_GROUP_LINKS_ACT_MOD"] = "Modifier Modérateurs";
$MESS["GD_SONET_GROUP_LINKS_ACT_MOD1"] = "Modérateur";
$MESS["GD_SONET_GROUP_LINKS_ACT_USER"] = "Diter la composition";
$MESS["GD_SONET_GROUP_LINKS_ACT_BAN"] = "Liste noire";
$MESS["GD_SONET_GROUP_LINKS_ACT_USER1"] = "Participants";
$MESS["GD_SONET_GROUP_LINKS_ACT_REQU"] = "Ceux qui peuvent inviter dans le groupe";
$MESS["GD_SONET_GROUP_LINKS_ACT_VREQU_OUT"] = "Invitation au groupe";
$MESS["GD_SONET_GROUP_LINKS_ACT_JOIN"] = "Se joindre au groupe";
$MESS["GD_SONET_GROUP_LINKS_ACT_EXIT"] = "Quitter le Groupe";
$MESS["GD_SONET_GROUP_LINKS_ACT_SUBSCRIBE"] = "Abonnement";
$MESS["GD_SONET_GROUP_LINKS_ACT_DELETE"] = "Suppression du Groupe";
$MESS["GD_SONET_GROUP_LINKS_ACT_VREQU"] = "Demandes et invitations de se joindre au groupe";
?>