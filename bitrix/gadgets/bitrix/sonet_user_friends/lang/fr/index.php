<?
$MESS["GD_SONET_USER_FRIENDS_ALL_FRIENDS"] = "Tous les amis";
$MESS["GD_SONET_USER_FRIENDS_FR_SEARCH"] = "Trouver des amis";
$MESS["GD_SONET_USER_FRIENDS_NO_FRIENDS"] = "Il n'y a pas d'amis.";
$MESS["GD_SONET_USER_FRIENDS_LOG_USERS"] = "Listes de mises à jours";
$MESS["GD_SONET_USER_FRIENDS_FR_UNAVAIL"] = "La liste d'amis n'est pas disponible.";
$MESS["GD_SONET_USER_FRIENDS_NOT_ALLOWED"] = "La fonctionnalité est désactivée.";
?>