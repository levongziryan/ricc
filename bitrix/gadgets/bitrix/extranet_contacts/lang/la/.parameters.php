<?
$MESS["GD_CONTACTS_P_DETAIL_URL"] = "Ruta de la plantilla de perfil de usuario:";
$MESS["GD_CONTACTS_P_MESSAGES_CHAT_URL"] = "Ruta de la plantilla de la página de charla:";
$MESS["GD_CONTACTS_P_WGU_COUNT"] = "Mostrar los contactos de mis grupos de trabajo";
$MESS["GD_CONTACTS_P_PU_COUNT"] = "Mostrar contactos públicos";
$MESS["GD_CONTACTS_P_FULLLIST_URL"] = "URL de la lista de todos los contactos:";
$MESS["GD_CONTACTS_P_EMPLOYEES_FULLLIST_URL"] = "URL de todos los empleados";
?>