<?
$MESS["GD_CONTACTS_P_FULLLIST_URL"] = "URL de la liste complète de contacts:";
$MESS["GD_CONTACTS_P_EMPLOYEES_FULLLIST_URL"] = "URL de la liste complète des employés:";
$MESS["GD_CONTACTS_P_WGU_COUNT"] = "Afficher les contacts de mes groupes de travail";
$MESS["GD_CONTACTS_P_PU_COUNT"] = "Afficher les contacts publics";
$MESS["GD_CONTACTS_P_DETAIL_URL"] = "Modèle de chemin d'accès à la page d'utilisateur:";
$MESS["GD_CONTACTS_P_MESSAGES_CHAT_URL"] = "Modèle de chemin d'accès à la page du chat:";
?>