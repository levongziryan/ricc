<?
$MESS["GD_CONTACTS_P_EMPLOYEES_FULLLIST_URL"] = "URL da lista de todos os empregados";
$MESS["GD_CONTACTS_P_MESSAGES_CHAT_URL"] = "Modelo de caminho da página de chat";
$MESS["GD_CONTACTS_P_FULLLIST_URL"] = "URL da lista completa de contatos:";
$MESS["GD_CONTACTS_P_WGU_COUNT"] = "Mostrar contatos do meu Grupo de Trabalho";
$MESS["GD_CONTACTS_P_PU_COUNT"] = "Mostrar contatos públicos";
$MESS["GD_CONTACTS_P_DETAIL_URL"] = "Modelo de caminho de perfil de usuário";
?>