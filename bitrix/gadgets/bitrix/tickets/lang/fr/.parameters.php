<?
$MESS["GD_TICKETS_P_ALL"] = "(partout)";
$MESS["GD_TICKETS_P_GREEN"] = "vert";
$MESS["GD_TICKETS_P_RED"] = "rouge";
$MESS["GD_TICKETS_P_PATH_TO_TICKET_EDIT"] = "Chemin d'accès à la page de modification de demande";
$MESS["GD_TICKETS_P_PATH_TO_TICKET_NEW"] = "Chemin vers la page de création d'un appel";
$MESS["GD_TICKETS_P_GREY"] = "gris";
$MESS["GD_TICKETS_P_LAMP"] = "Filtre selon le statut";
?>