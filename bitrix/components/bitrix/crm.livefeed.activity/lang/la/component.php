<?
$MESS["C_CRM_LFA_TASKS_TITLE_CREATE_M"] = "#USER_NAME# creación de una tarea #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_CREATE_F"] = "#USER_NAME# creación de una tarea #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_CREATE"] = "#USER_NAME# creación de una tarea #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_COMPLETE_M"] = "#USER_NAME# tarea completada #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_COMPLETE_F"] = "#USER_NAME# tarea completada #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_COMPLETE"] = "#USER_NAME# tarea completada #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_MODIFY_M"] = "#USER_NAME# cambiar tarea #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_MODIFY_F"] = "#USER_NAME# cambiar tarea #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_MODIFY"] = "#USER_NAME# cambiar tarea #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_STATUS_M"] = "#USER_NAME# cambiar estado de #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_STATUS_F"] = "#USER_NAME# cambiar estado de #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_STATUS"] = "#USER_NAME# cambiar estado de #TITLE#";
$MESS["C_CRM_LFA_TASKS_CHANGED_MESSAGE_24_2"] = "Modificado el";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_1_24"] = "La tarea se ha reanudado";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_2_24"] = "Tarea pendiente";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_3_24"] = "Tarea en progreso";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_4_24"] = "Tarea ha sido cerrada";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_4_24_2"] = "Razón";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_4_24_CHANGES"] = "requiere el control del autor";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_5_24"] = "Tarea ha sido cerrada";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_6_24"] = "Tarea se ha diferido";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_7_24"] = "Tarea se ha declinado";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_7_24_2"] = "Razón";
?>