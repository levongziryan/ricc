<?
$MESS["C_CRM_LFA_TASKS_TITLE_CREATE_M"] = "#USER_NAME# criou uma tarefa #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_CREATE_F"] = "#USER_NAME# criou uma tarefa #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_CREATE"] = "#USER_NAME# criou uma tarefa #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_COMPLETE_M"] = "#USER_NAME# completou a tarefa #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_COMPLETE_F"] = "#USER_NAME# completou a tarefa #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_COMPLETE"] = "#USER_NAME# completou a tarefa #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_MODIFY_M"] = "#USER_NAME# mudou a tarefa #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_MODIFY_F"] = "#USER_NAME# mudou a tarefa #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_MODIFY"] = "#USER_NAME# mudou a tarefa #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_STATUS_M"] = "#USER_NAME# alterou o status de #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_STATUS_F"] = "#USER_NAME# alterou o status de #TITLE#";
$MESS["C_CRM_LFA_TASKS_TITLE_STATUS"] = "#USER_NAME# alterou o status de #TITLE#";
$MESS["C_CRM_LFA_TASKS_CHANGED_MESSAGE_24_2"] = "Modificado em";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_1_24"] = "A tarefa foi retomada";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_2_24"] = "A tarefa está pendente";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_3_24"] = "A tarefa está em andamento";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_4_24"] = "A tarefa foi fechada";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_4_24_2"] = "Motivo";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_4_24_CHANGES"] = "Requer o controle do autor";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_5_24"] = "A tarefa foi fechada";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_6_24"] = "A tarefa foi adiada";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_7_24"] = "A tarefa foi recusada";
$MESS["C_CRM_LFA_TASK_STATUS_MESSAGE_7_24_2"] = "Motivo";
?>