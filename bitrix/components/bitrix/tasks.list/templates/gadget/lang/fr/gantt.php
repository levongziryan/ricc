<?
$MESS["INTASK_LIST_EMPTY"] = "Il n'y pas de tâches pour le moment.";
$MESS["INTASK_TASKPRIORITY"] = "Criticité";
$MESS["INTASK_TASKSTATUS"] = "Statut";
$MESS["INTASK_TO_DATE_TLP"] = "jusqu'à #DATE#";
$MESS["INTASK_FROM_DATE_TLP"] = "dès #DATE#";
$MESS["INTASK_NO_DATE_TLP"] = "la période d'exécution n'est pas définie";
$MESS["INTASK_TASKASSIGNEDTO"] = "Assigner à";
$MESS["TASKS_PRIORITY_0"] = "Bas";
$MESS["TASKS_PRIORITY_1"] = "Moyen";
$MESS["TASKS_PRIORITY_2"] = "Augmenté";
$MESS["TASKS_STATUS_1"] = "Créer";
$MESS["TASKS_STATUS_3"] = "En cours";
$MESS["TASKS_STATUS_4"] = "En attente du contrôle";
$MESS["TASKS_STATUS_5"] = "Achevé(e)s";
$MESS["TASKS_STATUS_6"] = "Différé";
$MESS["TASKS_STATUS_7"] = "Est rejetée";
$MESS["TASKS_DEADLINE"] = "Date limite";
$MESS["TASKS_STATUS_2"] = "Attend l'exécution";
?>