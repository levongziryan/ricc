<?
$MESS["TASKS_PRIORITY_V2"] = "Tarea importante";
$MESS["TASKS_MARK"] = "Calificación";
$MESS["TASKS_MARK_NONE"] = "Ninguno";
$MESS["TASKS_MARK_P"] = "Positivo";
$MESS["TASKS_MARK_N"] = "Negativo";
$MESS["TASKS_DOUBLE_CLICK"] = "Hacer doble click para ver";
$MESS["TASKS_APPLY"] = "Aplicar";
$MESS["TASKS_NO_TITLE"] = "El nombre de la tarea no está especificado.";
$MESS["TASKS_NO_RESPONSIBLE"] = "La persona responsable no está especificada.";
$MESS["TASKS_TITLE"] = "Tareas";
$MESS["TASKS_DEADLINE"] = "Fecha límite";
$MESS["TASKS_RESPONSIBLE"] = "Responsable";
$MESS["TASKS_CREATOR"] = "Creador";
$MESS["TASKS_ACCOMPLICE"] = "Participante";
$MESS["TASKS_AUDITOR"] = "Observador";
$MESS["TASKS_FINISHED"] = "Completada";
$MESS["TASKS_APPROVE_TASK"] = "Aceptar como completado";
$MESS["TASKS_QUICK_TITLE"] = "Nombre de la Tarea";
$MESS["TASKS_QUICK_DEADLINE"] = "Fecha límite";
$MESS["TASKS_QUICK_SAVE"] = "Guardar";
$MESS["TASKS_QUICK_CANCEL"] = "Cancelar";
$MESS["TASKS_TEMPLATES"] = "Plantillas";
$MESS["TASKS_REPORTS"] = "Reportes";
$MESS["TASKS_EXPORT_EXCEL"] = "Exportar a Excel";
$MESS["TASKS_EXPORT_OUTLOOK"] = "Exportar al Outlook";
$MESS["TASKS_ADD_TASK"] = "Nueva Tarea";
$MESS["TASKS_ADD_QUICK_TASK"] = "Crear Subtarea rápida";
$MESS["TASKS_ADD_TEMPLATE_TASK"] = "Formulario de creación de plantilla de tarea";
$MESS["TASKS_TEMPLATES_LIST"] = "Ver todas las plantillas de tareas";
$MESS["TASKS_DURATION"] = "Horas Invertidas";
$MESS["TASKS_OK"] = "OK";
$MESS["TASKS_CANCEL"] = "Cancelar";
$MESS["TASKS_DECLINE_REASON"] = "Razón del rechazo";
$MESS["TASKS_FILTER_BY_TAG"] = "Por Etiqueta";
$MESS["TASKS_FILTER_SELECT"] = "seleccionar";
$MESS["TASKS_FILTER_CREAT_DATE"] = "Creado En";
$MESS["TASKS_FILTER_PICK_DATE"] = "Seleccionar fecha en el calendario";
$MESS["TASKS_FILTER_SHOW_SUBORDINATE"] = "mostrar tareas suboordinadas";
$MESS["TASKS_FILTER_FIND"] = "Buscar";
$MESS["TASKS_TREE_LIST"] = "Vista de Tareas y Subtareas";
$MESS["TASKS_PLAIN_LIST"] = "Vista plana de tareas";
$MESS["TASKS_GANTT"] = "Gráfico de Gantt";
$MESS["TASKS_FILTER_NOT_ACCEPTED"] = "No aceptada";
$MESS["TASKS_FILTER_IN_CONTROL"] = "En espera de control";
$MESS["TASKS_DEFAULT_SORT"] = "Orden de Clasificación Predeterminada";
$MESS["TASKS_NO_TEMPLATES"] = "No hay plantillas disponibles";
$MESS["TASKS_NO_TASKS"] = "no hay tareas";
$MESS["TASKS_DECLINE_TASK"] = "Declinar";
$MESS["TASKS_QUICK_IN_GROUP"] = "tarea del proyecto";
$MESS["TASKS_QUICK_DESCRIPTION"] = "descripción";
$MESS["TASKS_DATE_START"] = "Fecha de inicio";
$MESS["TASKS_DATE_END"] = "Fecha final";
$MESS["TASKS_DATE_STARTED"] = "Iniciada el";
$MESS["TASKS_DATE_COMPLETED"] = "Finalizada el";
$MESS["TASKS_TASK_TITLE_LABEL"] = "Nro. de tarea";
$MESS["TASKS_STATUS"] = "Estado";
$MESS["TASKS_STATUS_OVERDUE"] = "Atrasado";
$MESS["TASKS_STATUS_NEW"] = "Nuevo";
$MESS["TASKS_STATUS_ACCEPTED"] = "Pendiente";
$MESS["TASKS_STATUS_IN_PROGRESS"] = "En progreso";
$MESS["TASKS_STATUS_WAITING"] = "En espera de control";
$MESS["TASKS_STATUS_COMPLETED"] = "Completada";
$MESS["TASKS_STATUS_DELAYED"] = "Diferida";
$MESS["TASKS_STATUS_DECLINED"] = "Declinada";
$MESS["TASKS_QUICK_INFO_DETAILS"] = "Detalles";
$MESS["TASKS_QUICK_INFO_EMPTY_DATE"] = "no";
$MESS["TASKS_TASK_FILES"] = "Archivos";
$MESS["TASKS_HELP"] = "Ayuda";
$MESS["TASKS_CONFIGURE_LIST"] = "Configurar lista";
$MESS["TASKS_LIST_COLUMN_1"] = "ID";
$MESS["TASKS_LIST_COLUMN_2"] = "Nombre";
$MESS["TASKS_LIST_COLUMN_3"] = "Creado por";
$MESS["TASKS_LIST_COLUMN_4"] = "Persona responsable";
$MESS["TASKS_LIST_COLUMN_5"] = "Fecha límite";
$MESS["TASKS_LIST_COLUMN_6"] = "Calificación";
$MESS["TASKS_LIST_COLUMN_7"] = "CRM";
$MESS["TASKS_LIST_COLUMN_8"] = "Prioridad";
$MESS["TASKS_LIST_COLUMN_9"] = "Estado";
$MESS["TASKS_LIST_COLUMN_10"] = "Grupo";
$MESS["TASKS_LIST_COLUMN_11"] = "Tiempo estimado requerido";
$MESS["TASKS_LIST_COLUMN_12"] = "Control de tiempo invertido";
$MESS["TASKS_LIST_COLUMN_13"] = "Tiempo invertido";
$MESS["TASKS_LIST_COLUMN_14"] = "Persona responsable puede cambiar la fecha límite";
$MESS["TASKS_LIST_COLUMN_15"] = "Creado el ";
$MESS["TASKS_LIST_COLUMN_16"] = "Modificado el ";
$MESS["TASKS_LIST_COLUMN_17"] = "Fecha de finalización";
$MESS["TASKS_LIST_COLUMN_SORTING"] = "Personalizado";
$MESS["TASKS_LIST_MENU_RESET_TO_DEFAULT_PRESET"] = "Vista predeterminada";
$MESS["TASKS_LIST_CONFIRM_ACTION_FOR_ALL_ITEMS"] = "¿Quieres aplicar la acción a todas las tareas y subtareas de la lista que incluye las siguientes páginas?";
$MESS["TASKS_LIST_TOOLTIP_FOR_ALL_ITEMS"] = "Seleccione las tareas que incluyen subtareas en todas las páginas";
$MESS["TASKS_LIST_SUBMIT"] = "Aplicar";
$MESS["TASKS_LIST_GROUP_ACTION_FOR_ALL"] = "Para todos";
$MESS["TASKS_LIST_GROUP_ACTION_REMOVE"] = "Eliminar";
$MESS["TASKS_LIST_GROUP_ACTION_COMPLETE"] = "Completo";
$MESS["TASKS_LIST_GROUP_ACTION_CHANGE_RESPONSIBLE"] = "Establecer persona responsable";
$MESS["TASKS_LIST_GROUP_ACTION_CHANGE_ORIGINATOR"] = "Cambiar creador";
$MESS["TASKS_LIST_GROUP_ACTION_ADD_AUDITOR"] = "Agregar observador";
$MESS["TASKS_LIST_GROUP_ACTION_ADD_ACCOMPLICE"] = "Agregar participante";
$MESS["TASKS_LIST_GROUP_ACTION_ADJUST_DEADLINE"] = "Adelantar el fecha limite ";
$MESS["TASKS_LIST_GROUP_ACTION_SET_DEADLINE"] = "Ingresar fecha limite ";
$MESS["TASKS_LIST_GROUP_ACTION_SET_GROUP"] = "Seleccione grupo de proyecto";
$MESS["TASKS_LIST_GROUP_ACTION_SUBSTRACT_DEADLINE"] = "Atrasar fecha limite";
$MESS["TASKS_LIST_GROUP_ACTION_DAYS_PLURAL_0"] = "día";
$MESS["TASKS_LIST_GROUP_ACTION_DAYS_PLURAL_1"] = "días";
$MESS["TASKS_LIST_GROUP_ACTION_DAYS_PLURAL_2"] = "días";
$MESS["TASKS_LIST_GROUP_ACTION_WEEKS_PLURAL_0"] = "semana";
$MESS["TASKS_LIST_GROUP_ACTION_WEEKS_PLURAL_1"] = "semanas";
$MESS["TASKS_LIST_GROUP_ACTION_WEEKS_PLURAL_2"] = "semanas";
$MESS["TASKS_LIST_GROUP_ACTION_MONTHES_PLURAL_0"] = "mes";
$MESS["TASKS_LIST_GROUP_ACTION_MONTHES_PLURAL_1"] = "meses";
$MESS["TASKS_LIST_GROUP_ACTION_MONTHES_PLURAL_2"] = "meses";
$MESS["TASKS_LIST_GROUP_ACTION_PLEASE_WAIT"] = "Por favor, espere...";
$MESS["TASKS_LIST_CONFIRM_REMOVE_FOR_SELECTED_ITEMS"] = "¿Quiere eliminar las tareas seleccionadas?";
$MESS["TASKS_LIST_CONFIRM_REMOVE_FOR_ALL_ITEMS"] = "¿Desea eliminar todas las tareas y subtareas, incluidas las de las páginas subsiguientes?";
$MESS["TASKS_LIST_GROUP_ACTION_ADD_FAVORITE"] = "Agregar a los favoritos";
$MESS["TASKS_LIST_GROUP_ACTION_DELETE_FAVORITE"] = "Eliminar de los favoritos";
$MESS["TASKS_LIST_TRIAL_EXPIRED_TITLE_V2"] = "Disponible sólo en tareas avanzadas de Bitrix 24";
$MESS["TASKS_LIST_TRIAL_EXPIRED_TEXT"] = "Plan gratuito le permite tener hasta 5 dependencias entre tareas. Actualizar a tareas avanzadas y gestión de proyectos, si usted quiere tener dependencias ilimitadas.

Cuatro tipos de dependencia son compatibles actualmente:
Finalizar para Iniciar (FI)
Inicio para Iniciar (II)
Finalizar para Finalizar (FF)
Iniciar para Finalizar (IF)

<a href=\"https://bitrix24.com/pro/tasks.php\" target=\"_blank\"> Conozca más </a>

Tareas avanzadas + Avanzado CRM + Telefonía Avanzada y otras características adicionales están disponibles en los planes comerciales, empezando con Bitrix24 Plus.
";
$MESS["TASKS_PRIORITY"] = "Prioridad";
$MESS["TASKS_PRIORITY_0"] = "Baja";
$MESS["TASKS_PRIORITY_1"] = "Normal";
$MESS["TASKS_PRIORITY_2"] = "Alta";
?>