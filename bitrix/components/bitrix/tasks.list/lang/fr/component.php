<?
$MESS["TASKS_ACCESS_DENIED"] = "Vous ne pouvez pas consulter la liste de tâches";
$MESS["TASKS_ACCESS_TO_GROUP_DENIED"] = "Vous ne pouvez pas revoir la liste des tâches dans ce groupe.";
$MESS["TASKS_TITLE_TASKS"] = "Tâches";
$MESS["TASKS_TITLE_GROUP_TASKS"] = "Tâches des groupes";
$MESS["SOCNET_MODULE_NOT_INSTALLED"] = "Module 'Réseau social' n'a pas été installé.";
$MESS["TASKS_MODULE_NOT_AVAILABLE_IN_THIS_EDITION"] = "Le module de gestion des tâches n'est pas disponible dans cette version du produit.";
$MESS["TASKS_MODULE_NOT_FOUND"] = "Le module de commande des tâches n'a pas été installé.";
$MESS["FORUM_MODULE_NOT_INSTALLED"] = "Le forum sera supprimé de façon irréversible. Continuer?";
$MESS["TASKS_TITLE_MY_TASKS"] = "Mes Tâches";
$MESS["TASKS_UNEXPECTED_ERROR"] = "Erreur imprévue";
?>