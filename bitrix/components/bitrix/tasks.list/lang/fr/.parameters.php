<?
$MESS["INTL_SET_NAVCHAIN"] = "Activer la chaîne de navigation";
$MESS["INTL_TASK_TYPE_GROUP"] = "Pour le groupe";
$MESS["INTL_TASK_TYPE_USER"] = "Pour l'utilisateur";
$MESS["INTL_GROUP_VAR"] = "Nom de la variable pour le code du groupe";
$MESS["INTL_ACTION_VAR"] = "Nom de la variable pour le code d'action";
$MESS["INTL_TASK_VAR"] = "Nom de la variable pour le code de la tâche";
$MESS["INTL_USER_VAR"] = "Nom de la variable pour le code d'utilisateur";
$MESS["INTL_VIEW_VAR"] = "Nom de la variable pour le code de présentation";
$MESS["INTL_PAGE_VAR"] = "Nom de la variable pour le code de la page";
$MESS["INTL_OWNER_ID"] = "Code de l'utilisateur";
$MESS["INTL_ITEM_COUNT"] = "Nombre de notes sur une page";
$MESS["INTL_NAME_TEMPLATE"] = "Affichage du nom";
$MESS["INTL_VARIABLE_ALIASES"] = "Alias des variables";
$MESS["INTL_PATH_TO_GROUP_TASKS_TASK"] = "Chemin vers la tâche du groupe";
$MESS["INTL_PATH_TO_USER_TASKS_TASK"] = "Chemin vers la tâche de l'utilisateur";
$MESS["INTL_PATH_TO_GROUP_TASKS_VIEW"] = "Chemin vers la présentation";
$MESS["INTL_PATH_TO_GROUP_TASKS"] = "Chemin vers la liste de tâches du groupe";
$MESS["INTL_PATH_TO_USER_TASKS"] = "Chemin vers la liste de tâches de l'utilisateur";
$MESS["INTL_TASKS_FIELDS_SHOW"] = "Propriétés de la tâche";
$MESS["INTL_TASK_TYPE"] = "Type de la tâche";
?>