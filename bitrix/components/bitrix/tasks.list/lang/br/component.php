<?
$MESS["TASKS_MODULE_NOT_AVAILABLE_IN_THIS_EDITION"] = "O módulo Tarefas não está disponível nesta edição.";
$MESS["TASKS_MODULE_NOT_FOUND"] = "O módulo de tarefas não está instalado.";
$MESS["SOCNET_MODULE_NOT_INSTALLED"] = "O módulo \"Rede Social\" não está instalado.";
$MESS["FORUM_MODULE_NOT_INSTALLED"] = "O módulo do Fórum não está instalado.";
$MESS["TASKS_UNEXPECTED_ERROR"] = "Erro indefinido";
$MESS["TASKS_ACCESS_TO_GROUP_DENIED"] = "Você não pode ver a lista de tarefas deste grupo.";
$MESS["TASKS_TITLE_TASKS"] = "Tarefas";
$MESS["TASKS_TITLE_GROUP_TASKS"] = "Tarefas do Grupo";
$MESS["TASKS_TITLE_MY_TASKS"] = "Minhas Tarefas";
$MESS["TASKS_ACCESS_DENIED"] = "Você não tem permissão para ver a lista de tarefas local ";
?>