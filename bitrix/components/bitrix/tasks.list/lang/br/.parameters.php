<?
$MESS["INTL_VARIABLE_ALIASES"] = "Aliases variáveis";
$MESS["INTL_TASKS_FIELDS_SHOW"] = "Propriedades da tarefa";
$MESS["INTL_TASK_TYPE"] = "Tipo de Tarefa";
$MESS["INTL_OWNER_ID"] = "ID do Proprietário";
$MESS["INTL_TASK_VAR"] = "Nome da Variável de ID da Tarefa";
$MESS["INTL_USER_VAR"] = "Nome da variável de ID de usuário";
$MESS["INTL_GROUP_VAR"] = "ID do Nome da variável do Grupo";
$MESS["INTL_VIEW_VAR"] = "Visualizar ID do Nome da variável";
$MESS["INTL_ACTION_VAR"] = "Nome da variável de ID de ação";
$MESS["INTL_PAGE_VAR"] = "Variável da página";
$MESS["INTL_PATH_TO_GROUP_TASKS"] = "Caminho para as Tarefas";
$MESS["INTL_PATH_TO_GROUP_TASKS_TASK"] = "Caminho para a Tarefa";
$MESS["INTL_PATH_TO_USER_TASKS"] = "Caminho para Tarefas do Usuário";
$MESS["INTL_PATH_TO_USER_TASKS_TASK"] = "Caminho para Tarefa do Usuário";
$MESS["INTL_PATH_TO_GROUP_TASKS_VIEW"] = "Caminho para a Visualização";
$MESS["INTL_SET_NAVCHAIN"] = "Definir Barras de Navegação";
$MESS["INTL_ITEM_COUNT"] = "Itens por página";
$MESS["INTL_TASK_TYPE_GROUP"] = "Para o Grupo";
$MESS["INTL_TASK_TYPE_USER"] = "Para o Usuário";
$MESS["INTL_NAME_TEMPLATE"] = "Formato do Nome";
?>