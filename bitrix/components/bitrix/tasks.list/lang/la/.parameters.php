<?
$MESS["INTL_VARIABLE_ALIASES"] = "Alias Variables ";
$MESS["INTL_TASK_TYPE"] = "Tipo de Tarea";
$MESS["INTL_OWNER_ID"] = "ID del Propietario";
$MESS["INTL_TASK_VAR"] = "Nombre del ID variable de la Tarea";
$MESS["INTL_USER_VAR"] = "Nombre del ID variable del Usuario";
$MESS["INTL_GROUP_VAR"] = "Nombre del ID variable del Grupo";
$MESS["INTL_VIEW_VAR"] = "Ver Nombre del ID variable del Usuario";
$MESS["INTL_ACTION_VAR"] = "Nombre del ID variable de la acción";
$MESS["INTL_PAGE_VAR"] = "Nombre del ID variable de la página";
$MESS["INTL_PATH_TO_GROUP_TASKS"] = "Ruta a las Tareas del Grupo";
$MESS["INTL_PATH_TO_GROUP_TASKS_TASK"] = "Ruta A Las Tareas del Grupo";
$MESS["INTL_PATH_TO_USER_TASKS"] = "Ruta A Las Tareas del Usuario";
$MESS["INTL_PATH_TO_USER_TASKS_TASK"] = "Ruta A Las Tarea del Usuario";
$MESS["INTL_ITEM_COUNT"] = "Items Por Página";
$MESS["INTL_TASK_TYPE_GROUP"] = "Para el Grupo";
$MESS["INTL_TASK_TYPE_USER"] = "Para el Usuario";
$MESS["INTL_PATH_TO_GROUP_TASKS_VIEW"] = "Ruta para ver la tarea";
$MESS["INTL_SET_NAVCHAIN"] = "Habilitar Breadcrumbs";
$MESS["INTL_NAME_TEMPLATE"] = "Plantilla de visualización de nombre";
?>