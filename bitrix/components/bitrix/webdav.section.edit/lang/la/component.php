<?
$MESS["W_IBLOCK_IS_NOT_INSTALLED"] = "El módulo de blocks de información no está instalado.";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "El módulo del WebDav no está instalado.";
$MESS["WD_ERROR_BAD_SESSID"] = "Su sesión ha expirado. Por favor repita la operación.";
$MESS["WD_ERROR_BAD_ACTION"] = "No se ha especificado la acción de la carpeta.";
$MESS["WD_ERROR_BAD_SECTION_NAME"] = "El nombre de la carpeta no debe contener los siguientes caracteres: /\\<>|\"':*?|+#.";
$MESS["WD_ERROR_EMPTY_SECTION_ID"] = "El ID de la carpeta ahora está especificado.";
$MESS["WD_ERROR_EMPTY_SECTION_NAME"] = "El nombre de la carpeta ahora está especificado.";
$MESS["WD_ERROR_DELETE"] = "La sección no fue borrada.";
$MESS["WD_NEW"] = "Nueva carpeta";
$MESS["WD_ERROR_ACCESS_DENIED"] = "Acceso denegado.";
$MESS["WD_ERROR_RECOVER"] = "No puede recuperar el documento.";
?>