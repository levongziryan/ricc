<?
$MESS["WD_IBLOCK_TYPE"] = "Tipo de block de información";
$MESS["WD_IBLOCK_ID"] = "Block de información";
$MESS["WD_SECTION_ID"] = "ID de la sección";
$MESS["T_IBLOCK_DESC_NEWS_PANEL"] = "Mostrar panel de botones para este componente";
$MESS["WD_ACTION"] = "Acción";
$MESS["WD_USER_VIEW_URL"] = "Página de información del usuario";
$MESS["WD_SECTION_EDIT_URL"] = "Página de edición del catálogo";
$MESS["WD_PERMISSION"] = "Permisos de acceso externo (usar en componentes compuestos)";
$MESS["WD_ROOT_SECTION_ID"] = "ID de la raíz de la sección (usar en componentes compuestos)";
$MESS["WD_REPLACE_SYMBOLS"] = "Reemplaza caracteres ilegales en el archivo y en los nombres de la carpeta";
$MESS["WD_SECTION_LIST_URL"] = "Página de vista del catálogo";
?>