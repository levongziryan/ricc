<?
$MESS["WD_ERROR_ACCESS_DENIED"] = "Acesso negado.";
$MESS["W_IBLOCK_IS_NOT_INSTALLED"] = "Módulo de blocos de informações não está instalado.";
$MESS["WD_NEW"] = "Nova Pasta";
$MESS["WD_ERROR_BAD_ACTION"] = "A ação da pasta não foi especificado.";
$MESS["WD_ERROR_EMPTY_SECTION_ID"] = "O ID de pasta foi especificado.";
$MESS["WD_ERROR_EMPTY_SECTION_NAME"] = "O nome da pasta foi especificado.";
$MESS["WD_ERROR_BAD_SECTION_NAME"] = "O nome da pasta não deve conter os seguintes caracteres: / \\ \\ <> | \"': * | + #?.";
$MESS["WD_ERROR_DELETE"] = "A seção não foi excluída.";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "O módulo WebDav não está instalado";
$MESS["WD_ERROR_BAD_SESSID"] = "Sua sessão expirou. Por favor, repita a operação.";
$MESS["WD_ERROR_RECOVER"] = "Não foi possivel recuperar o documento";
$MESS["W_TITLE_EXTERNAL_LOCK_BY"] = "Bloqueado por";
$MESS["RATING_TYPE_CONFIG"] = "Padrão";
$MESS["WD_START_BP_TITLE"] = "Executar novo processo de negócio";
$MESS["WD_DISK_NOTIFY_DIR_NUMERAL_2_4"] = "#COUNT# pastas";
$MESS["BPCGDOC_INVALID_TYPE"] = "O tipo de parâmetro é indefinido.";
$MESS["WD_ERROR_UPLOAD_BAD_FILE"] = "O arquivo foi enviado de forma incompleta.";
$MESS["WD_ATTENTION2_2"] = "Arquivos com \"#TITLE#\" títulos: #FILES#";
$MESS["WD_UPLOAD_VERSION_TITLE"] = "Substituir o documento para \"#NAME\"";
$MESS["Description"] = "DESCRIÇÃO";
$MESS["WD_UPLOAD_EXPAND_PROPS"] = "Definir propriedades <a documento #LINK#> </a>";
$MESS["PARENT_SECTION_TIP"] = "Especifica o ID da secção pai, que pode ser usado para limitar os elementos seleccionados para essa secção.";
?>