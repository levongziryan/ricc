<?
$MESS["WD_ACTION"] = "Ação";
$MESS["WD_SECTION_EDIT_URL"] = "Página de edição de catálogo";
$MESS["WD_SECTION_LIST_URL"] = "Página de visualização de catálogos";
$MESS["T_IBLOCK_DESC_NEWS_PANEL"] = "Mostrar botões do painel para este componente";
$MESS["WD_PERMISSION"] = "Permissão de acesso externo (uso em componentes compostos)";
$MESS["WD_IBLOCK_ID"] = "Bloco de informação";
$MESS["WD_REPLACE_SYMBOLS"] = "Substituir caracteres ilegais em nomes de arquivos e pastas";
$MESS["WD_ROOT_SECTION_ID"] = "ID da raiz de Seção (uso em componentes compostos)";
$MESS["WD_SECTION_ID"] = "ID de seção";
$MESS["WD_IBLOCK_TYPE"] = "Tipo de bloco de informação";
$MESS["WD_USER_VIEW_URL"] = "Página de Informação de usuário";
$MESS["RATING_TYPE_STANDART_GRAPHIC"] = "Curtir / Curtir(Desfazer) (Imagem)";
$MESS["WD_DOCUMENT_BP_ALT2"] = "Ver o histórico de processos de negócios nessa versão do documento";
$MESS["WD_AG_HELP4"] = "Forneça o caminho da biblioteca na pasta <i> </i> campo:";
$MESS["WD_AG_GO_BACK"] = "De Volta";
$MESS["WD_N"] = "Nao";
$MESS["WD_ERROR_SAME_NAME"] = "Um arquivo com este nome já existe.";
$MESS["WD_ERROR_BAD_ACTION"] = "A ação da pasta não é especificado.";
$MESS["WD_EV_TITLE"] = "Versões do Arquivo";
$MESS["WD_RATING"] = "Classificação";
$MESS["WD_MODULE"] = "Biblioteca de Documentos";
?>