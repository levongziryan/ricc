<?
$MESS["WD_ERROR_BAD_SESSID"] = "Votre session a expiré. Répétez cette opération encore une fois, s'il vous plaît.";
$MESS["WD_ERROR_ACCESS_DENIED"] = "Accès interdit.";
$MESS["WD_ERROR_BAD_SECTION_NAME"] = "Le nom du dossier ne doit pas contenir les symboles suivants: /\\:*?''<>|{}%&~";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "Le module de la bibliothèque des documents n'a pas été installé.";
$MESS["W_IBLOCK_IS_NOT_INSTALLED"] = "Le module blocs d'information n'a pas été installé";
$MESS["WD_ERROR_RECOVER"] = "La restitution du document n'était pas réussie.";
$MESS["WD_ERROR_EMPTY_SECTION_ID"] = "L'ID du dossier n'est pas spécifié.";
$MESS["WD_ERROR_BAD_ACTION"] = "L'action avec le dossier n'est pas indiquée.";
$MESS["WD_ERROR_EMPTY_SECTION_NAME"] = "Nom du dossier inconnu.";
$MESS["WD_NEW"] = "Nouveau dossier";
$MESS["WD_ERROR_DELETE"] = "La section n'a pas été supprimée.";
?>