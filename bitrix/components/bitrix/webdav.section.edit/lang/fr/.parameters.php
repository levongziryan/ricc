<?
$MESS["WD_ROOT_SECTION_ID"] = "ID de la section de racine (il est recommandé d'utiliser en ensemble avec un composant complexe)";
$MESS["WD_SECTION_ID"] = "ID de la section";
$MESS["WD_PERMISSION"] = "Droits d'accès externes (il est recommandé d'utiliser dans le cadre d'un composant intégré)";
$MESS["WD_ACTION"] = "Action";
$MESS["T_IBLOCK_DESC_NEWS_PANEL"] = "Ajouter les boutons au panneau admin. Pour ce composant";
$MESS["WD_REPLACE_SYMBOLS"] = "Remplacer des caractères interdits dans les noms de dossiers et de fichiers";
$MESS["WD_IBLOCK_ID"] = "Bloc d'information";
$MESS["WD_SECTION_EDIT_URL"] = "Page de changement du répertoire";
$MESS["WD_USER_VIEW_URL"] = "Page d'affichage de l'information sur l'utilisateur";
$MESS["WD_SECTION_LIST_URL"] = "Page d'examen du catalogue";
$MESS["WD_IBLOCK_TYPE"] = "Type du bloc d'information";
?>