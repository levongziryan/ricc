<?
$MESS["WD_EDIT_SECTION"] = "Modification du dossier #NAME#";
$MESS["WD_NAME"] = "Dénomination";
$MESS["WD_CANCEL"] = "Annuler";
$MESS["WD_ADD_SECTION"] = "Création d'un dossier";
$MESS["WD_SAVE"] = "Sauvegarder";
$MESS["WD_DROP_SECTION"] = "Suppression du Dossier #NAME#";
$MESS["WD_DROP"] = "Supprimer";
$MESS["WD_DROP_CONFIRM"] = "tes-vous sûr de supprimer le dossier #NAME# sans possibilité de sa récupération?";
?>