<?
$MESS["WD_DROP_CONFIRM"] = "Tem certeza de que deseja Excluir a pasta #NAME# irreversívelmente?";
$MESS["WD_CANCEL"] = "Cancelar";
$MESS["WD_ADD_SECTION"] = "Criar Pasta";
$MESS["WD_DROP_SECTION"] = "Excluir a pasta #NAME#";
$MESS["WD_DROP"] = "Excluir";
$MESS["WD_EDIT_SECTION"] = "Editar pasta #NAME#";
$MESS["WD_NAME"] = "Nome";
$MESS["WD_SAVE"] = "Salvar";
$MESS["WD_Y"] = "Sim";
$MESS["WD_FILE_NAME"] = "Arquivo";
$MESS["WD_IBLOCK_TYPE"] = "Tipo de bloco de informação";
$MESS["WD_BP"] = "Business Process";
?>