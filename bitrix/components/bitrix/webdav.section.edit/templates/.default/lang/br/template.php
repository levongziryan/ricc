<?
$MESS["WD_CREATED"] = "Criado";
$MESS["WD_DELETE_SECTION"] = "Excluir";
$MESS["WD_LAST_UPDATE"] = "ltima atualização";
$MESS["WD_NAME"] = "Gerenciar Elemento";
$MESS["WD_PARENT_SECTION"] = "Nível pai";
$MESS["WD_CONTENT"] = "Nível superior";
$MESS["WD_FOLDER"] = "Pasta";
$MESS["WD_SAVE"] = "Salvar alterações";
$MESS["WD_CANCEL"] = "Cancelar";
$MESS["WD_ACCESS_DENIED"] = "Acesso negado.";
$MESS["WD_DISK_NOTIFY_ACTION_DELETE_D"] = "Excluído: #DIR#";
$MESS["WD_LOCK_TITLE"] = "Evitar que o documento seja modificado por outros usuários.";
$MESS["WD_DELETE_CONFIRM"] = "Tem certeza de que deseja excluir o elemento? Esta operação não pode ser desfeita!";
$MESS["WD_FILE_COMMENTS"] = "Comentarios";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "O módulo WebDav não está instalado";
?>