<?
$MESS["WD_CREATED"] = "Creado ";
$MESS["WD_LAST_UPDATE"] = "Última actualización";
$MESS["WD_PARENT_SECTION"] = "Nivel principal";
$MESS["WD_CONTENT"] = "Nivel superior";
$MESS["WD_NAME"] = "Nombre";
$MESS["WD_DELETE_SECTION"] = "Borrar";
$MESS["WD_FOLDER"] = "Carpeta";
$MESS["WD_SAVE"] = "Guardar cambios";
$MESS["WD_CANCEL"] = "Cancelar";
?>