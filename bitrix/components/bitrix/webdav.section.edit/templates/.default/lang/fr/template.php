<?
$MESS["WD_CONTENT"] = "Niveau supérieur";
$MESS["WD_LAST_UPDATE"] = "Dernière mise à jour";
$MESS["WD_NAME"] = "Dénomination";
$MESS["WD_CANCEL"] = "Annuler";
$MESS["WD_FOLDER"] = "Dossier";
$MESS["WD_PARENT_SECTION"] = "Section de parent";
$MESS["WD_CREATED"] = "Créé";
$MESS["WD_SAVE"] = "Enregistrer les modifications";
$MESS["WD_DELETE_SECTION"] = "Supprimer";
?>