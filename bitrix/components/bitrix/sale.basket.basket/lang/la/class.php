<?
$MESS["SALE_EMPTY_BASKET"] = "Su carrito de compras está vacío";
$MESS["SBB_PRODUCT_NOT_AVAILABLE"] = "#PRODUCT# está agotado";
$MESS["SBB_PRODUCT_NOT_ENOUGH_QUANTITY"] = "El stock actual del \"#PRODUCT#\" es insuficiente (#NUMBER# es requerido)";
$MESS["SOA_TEMPL_ORDER_PS_ERROR"] = "El método de pago seleccionado ha fallado. Por favor, póngase en contacto con el administrador del sitio o seleccione otro método.";
$MESS["SBB_TITLE"] = "Mi carrito de compras";
$MESS["SALE_MODULE_NOT_INSTALL"] = "El módulo e-Store no está instalado.";
$MESS["SBB_PRODUCT_QUANTITY_CHANGED"] = "La cantidad del producto fue cambiada";
?>