<?
$MESS["CRM_COLUMN_DATE_CREATE"] = "Date";
$MESS["CRM_COLUMN_ENTITY_TYPE"] = "Entité";
$MESS["CRM_COLUMN_ENTITY_TITLE"] = "Titre";
$MESS["CRM_COLUMN_CREATED_BY"] = "Auteur";
$MESS["CRM_COLUMN_EVENT_NAME"] = "Type d'événement";
$MESS["CRM_COLUMN_CREATED_BY_ID"] = "Auteur";
$MESS["CRM_COLUMN_ASSIGNED_BY_ID"] = "Responsable";
$MESS["CRM_COLUMN_EVENT_DESC"] = "Description";
$MESS["CRM_ENTITY_TYPE_LEAD"] = "Prospect";
$MESS["CRM_ENTITY_TYPE_CONTACT"] = "Client";
$MESS["CRM_ENTITY_TYPE_COMPANY"] = "Entreprise";
$MESS["CRM_ENTITY_TYPE_DEAL"] = "Affaire";
$MESS["CRM_EVENT_DESC_MORE"] = "en savoir plus";
$MESS["CRM_PRESET_CREATE_TODAY"] = "Créés aujourd'hui";
$MESS["CRM_PRESET_CREATE_YESTERDAY"] = "Créés hier";
$MESS["CRM_PRESET_CREATE_MY"] = "Créés par moi";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_EVENT_TYPE_SNS"] = "Lettre";
$MESS["CRM_EVENT_DESC_AFTER"] = "Après";
$MESS["CRM_EVENT_DESC_BEFORE"] = "Avant";
$MESS["CRM_COLUMN_EVENT_TYPE"] = "Entité";
$MESS["CRM_EVENT_TYPE_USER"] = "Pour utilisateur";
$MESS["CRM_EVENT_TYPE_CHANGE"] = "Changements";
$MESS["CRM_COLUMN_ENTITY"] = "Elément CRM";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Le module 'Boutique en ligne' n'a pas été installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "Le module 'Catalogue de marchandises' n'a pas été installé.";
?>