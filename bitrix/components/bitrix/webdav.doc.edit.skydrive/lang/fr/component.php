<?
$MESS["WD_DOC_EDIT_UNKNOWN_ERROR"] = "Une erreur est survenue lors de l'édition du document. Essayez encore une fois.";
$MESS["WD_DOC_ATTEMPT_EDIT_LOCK_DOCUMENT"] = "Ce document est bloqué pour édition.";
$MESS["WD_DOC_EDIT_UPLOAD_DOC_TO_GOOGLE"] = "Chargement du document";
$MESS["WD_DOC_INSTALL_SOCSERV"] = "Le module de services sociaux n'a pas été installé.";
$MESS["WD_DOC_INSTALL_SOCSERV_SKYDRIVE"] = "Le service Live ID n'est pas configuré.";
?>