<?
$MESS["BLACKLIST_TITLE"] = "Schwarze Liste verwalten";
$MESS["BLACKLIST_DELETE_CONFIRM"] = "Möchten Sie diese Nummer wirklich aus der Liste entfernen?";
$MESS["BLACKLIST_DELETE_ERROR"] = "Entfernung der Telefonnummer ist fehlgeschlagen.";
$MESS["BLACKLIST_ENABLE"] = "Telefonnummern außerhalb der Bürozeiten automatisch sperren";
$MESS["BLACKLIST_ENABLE_TEXT"] = "Die Außerhalb-Büro-Zeit wird individuell für jede Telefonnummer auf der Seite der Einstellungen definiert.";
$MESS["BLACKLIST_TEXT1"] = "Wenn Anruf von dem Anrufer mit derselben ID kommt";
$MESS["BLACKLIST_TEXT2"] = "Mal in der Reihe innerhalb von";
$MESS["BLACKLIST_TEXT3"] = "Minuten";
$MESS["BLACKLIST_NUMBERS"] = "Nummern auf der schwarzen Liste";
$MESS["BLACKLIST_NUMBER_ADD"] = "Nummer hinzufügen";
$MESS["BLACKLIST_NUMBER_DELETE"] = "Löschen";
$MESS["BLACKLIST_SAVE"] = "Speichern";
$MESS["BLACKLIST_ABOUT_2"] = "Ein Anruf wird abgelehnt, wenn die Anrufer-ID in der Liste gefunden wird. Informationen zu diesem Anruf finden Sie hier: \"#LINK#\"";
$MESS["BLACKLIST_ABOUT_LINK"] = "Anrufprotokoll";
?>