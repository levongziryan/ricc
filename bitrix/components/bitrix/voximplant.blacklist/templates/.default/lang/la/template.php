<?
$MESS["BLACKLIST_DELETE_CONFIRM"] = "¿Seguro que desea eliminar el número de la lista?";
$MESS["BLACKLIST_DELETE_ERROR"] = "Error al eliminar el número de teléfono.";
$MESS["BLACKLIST_ENABLE"] = "Bloqueo automático de números de teléfono fuera del horario de oficina";
$MESS["BLACKLIST_TEXT1"] = "Si la llamada se origina desde el mismo ID de la llamada";
$MESS["BLACKLIST_TEXT3"] = "minutos";
$MESS["BLACKLIST_NUMBERS"] = "Números de la lista negra";
$MESS["BLACKLIST_NUMBER_ADD"] = "Agregar número";
$MESS["BLACKLIST_NUMBER_DELETE"] = "Eliminar";
$MESS["BLACKLIST_SAVE"] = "Guardar";
$MESS["BLACKLIST_ENABLE_TEXT"] = "Fuera del horario laboral se especifica individualmente para cada uno de los números de teléfono en la página de configuración.";
$MESS["BLACKLIST_TEXT2"] = "vences en ";
$MESS["BLACKLIST_TITLE"] = "Administrar Lista Negra";
$MESS["BLACKLIST_ABOUT_2"] = "Una llamada será rechazada si el ID de la llamada se encuentra en la lista. Usted encontrará más información sobre estas llamadas aquí: \"#LINK#\"";
$MESS["BLACKLIST_ABOUT_LINK"] = "Registro de Llamadas";
?>