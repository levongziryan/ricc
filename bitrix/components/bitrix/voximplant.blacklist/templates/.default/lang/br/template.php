<?
$MESS["BLACKLIST_DELETE_CONFIRM"] = "Você tem certeza que deseja remover o número da lista?";
$MESS["BLACKLIST_DELETE_ERROR"] = "Erro ao remover o número de telefone.";
$MESS["BLACKLIST_ENABLE"] = "Bloquear automaticamente os números de telefone fora do horário de expediente";
$MESS["BLACKLIST_TEXT1"] = "Se uma chamada se origina do mesmo ID de autor";
$MESS["BLACKLIST_TEXT2"] = "várias vezes em";
$MESS["BLACKLIST_TEXT3"] = "minutos";
$MESS["BLACKLIST_NUMBERS"] = "Números da lista negra";
$MESS["BLACKLIST_NUMBER_ADD"] = "Adicionar números";
$MESS["BLACKLIST_NUMBER_DELETE"] = "Excluir";
$MESS["BLACKLIST_SAVE"] = "Salvar";
$MESS["BLACKLIST_ENABLE_TEXT"] = "As horas fora do horário de expediente são especificadas individualmente para cada número de telefone na página de configuração.";
$MESS["BLACKLIST_ABOUT"] = "Uma chamada será rejeitada se o ID do autor for encontrado na lista. Você irá encontrar informações sobre esta chamada na página \"Saldo e estatísticas > Fatura Discriminada\".";
$MESS["BLACKLIST_TITLE"] = "Gerenciar Lista Negra";
$MESS["BLACKLIST_ABOUT_2"] = "Uma chamada será rejeitada se o ID do autor da chamada for encontrado na lista. Você irá encontrar as informações sobre esta chamada aqui: \"#LINK#\"";
$MESS["BLACKLIST_ABOUT_LINK"] = "Log de Chamada";
?>