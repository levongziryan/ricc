<?
$MESS["CRM_FIELDS_LIST_ACTION_MENU_DELETE_CONF"] = "tes-vous sûr de vouloir supprimer ce champ?";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_FIELDS_LIST_ACTION_MENU_EDIT"] = "Editer";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_FIELDS_LIST_TITLE_EDIT"] = "Liste des champs: #NAME#";
$MESS["CRM_FIELDS_ENTITY_LIST"] = "Liste de modèles";
$MESS["CRM_FIELDS_LIST_ACTION_MENU_DELETE"] = "Supprimer";
?>