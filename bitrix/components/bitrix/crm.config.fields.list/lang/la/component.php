<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_FIELDS_LIST_TITLE_EDIT"] = "Campos: #NAME#";
$MESS["CRM_FIELDS_LIST_ACTION_MENU_EDIT"] = "Editar";
$MESS["CRM_FIELDS_LIST_ACTION_MENU_DELETE"] = "Eliminar";
$MESS["CRM_FIELDS_LIST_ACTION_MENU_DELETE_CONF"] = "Está usted seguro que desea eliminar este campo?";
$MESS["CRM_FIELDS_ENTITY_LIST"] = "Tipos";
?>