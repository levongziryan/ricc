<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Das CRM-Modul ist nicht installiert.";
$MESS["CRM_PERMISSION_DENIED"] = "Zugriff verweigert";
$MESS["CRM_FIELDS_LIST_TITLE_EDIT"] = "Felder: #NAME#";
$MESS["CRM_FIELDS_LIST_ACTION_MENU_EDIT"] = "Bearbeiten";
$MESS["CRM_FIELDS_LIST_ACTION_MENU_DELETE"] = "Löschen";
$MESS["CRM_FIELDS_LIST_ACTION_MENU_DELETE_CONF"] = "Möchten Sie dieses Feld wirklich löschen?";
$MESS["CRM_FIELDS_ENTITY_LIST"] = "Typen";
?>