<?
$MESS["WD_ERROR_ACCESS_DENIED"] = "Acesso negado.";
$MESS["WD_NEW"] = "Nova Pasta";
$MESS["WD_ERROR_BAD_ACTION"] = "A ação da pasta não é especificada.";
$MESS["WD_ERROR_EMPTY_SECTION_ID"] = "O ID de pasta agora está especificada.";
$MESS["WD_ERROR_EMPTY_SECTION_NAME"] = "O nome da pasta agora é especificada.";
$MESS["WD_ERROR_BAD_SECTION_NAME"] = "O nome da pasta não deve conter os seguintes caracteres: / \\ \\ <> | \"': * | + #?.";
$MESS["WD_ERROR_DELETE"] = "A seção não foi excluída.";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "O módulo WebDav não está instalado.";
$MESS["WD_ERROR_BAD_SESSID"] = "Sua sessão expirou. Por favor, repita a operação.";
$MESS["WD_ERROR_RECOVER"] = "Não é possivel recuperar o documento.";
$MESS["WD_IBLOCK_ID"] = "Bloco de informação";
$MESS["RATING_TYPE"] = "Projeto dos botões de classificação";
$MESS["WD_LOCK"] = "Bloqueio de edição";
$MESS["WD_DELETE_FILE_ALT2"] = "Excluir versão";
$MESS["WD_ERROR_BAD_ELEMENT_NAME"] = "O nome do arquivo não deve conter os seguintes caracteres: / \\ \\: * \"'<> |% {} & ~?";
$MESS["ErrorNoData"] = "Não existem dados. Ã provável que você esteja tentando fazer o upload do arquivo que o tamanho excede #POST_MAX_SIZE# MB.";
$MESS["Title"] = "Nome";
$MESS["Send_Version"] = "Carregar nova versão";
$MESS["WD_UPLOAD_EXTENDED"] = "href=\"#LINK#\"> <a Carregar Vários Documentos </a>";
$MESS["PAGER_DESC_NUMBERING_CACHE_TIME_TIP"] = "Especifica o tempo de cache para páginas (em segundos) ao usar a navegação ao contrário.";
?>