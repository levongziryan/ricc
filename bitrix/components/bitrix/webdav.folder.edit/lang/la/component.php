<?
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "El módulo del Webdav no está instalado.";
$MESS["WD_ERROR_BAD_SESSID"] = "Su sesión ha expirado. Por favor repita la operación.";
$MESS["WD_ERROR_BAD_ACTION"] = "La acción de la carpeta no está especificada.";
$MESS["WD_ERROR_BAD_SECTION_NAME"] = "La carpeta no debe contener los siguientes caracteres: /\\<>|\"':*?|+#.";
$MESS["WD_ERROR_EMPTY_SECTION_ID"] = "El ID de la carpeta ahora está especificado.";
$MESS["WD_ERROR_EMPTY_SECTION_NAME"] = "El nombre de la carpeta ahora está especificado.";
$MESS["WD_ERROR_DELETE"] = "La sección no fue borrada.";
$MESS["WD_NEW"] = "Nueva Carpeta";
$MESS["WD_ERROR_ACCESS_DENIED"] = "Acceso Denegado.";
$MESS["WD_ERROR_RECOVER"] = "No puede recuperar el documento.";
?>