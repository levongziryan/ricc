<?
$MESS["WD_DROP_CONFIRM"] = "Tem certeza de que deseja Excluir a pasta #NAME# irreversívelmente?";
$MESS["WD_ADD_SECTION"] = "Criar Pasta";
$MESS["WD_DROP_SECTION"] = "Excluir a pasta #NAME#";
$MESS["WD_EDIT_SECTION"] = "Editar pasta #NAME#";
$MESS["WD_NAME"] = "Nome";
$MESS["WD_PARENT_SECTION"] = "Nível pai";
$MESS["WD_DROP_SECTION_DESCRIPTION"] = "Excluir <b> Pasta #PATH# </b>";
$MESS["WD_EDIT_SECTION_DESCRIPTION"] = "Edite <b> Pasta #PATH# </b>";
$MESS["WD_ADD_SECTION_DESCRIPTION"] = "Criar Pasta Em <b> #PATH# </b>";
$MESS["WD_BUTTON_1_ALT"] = "Edite os parâmetros de biblioteca";
$MESS["IBLIST_BP"] = "Processos de Negócios";
$MESS["WD_UNDELETE_TITLE"] = "Recuperar documento";
$MESS["WD_RESTORE_ELEMENT"] = "Restaurar";
$MESS["BPATT_HELP2_TEXT"] = "Sequential Business Process é um processo de negócio simples de executar uma série de ações consecutivas em um documento.";
?>