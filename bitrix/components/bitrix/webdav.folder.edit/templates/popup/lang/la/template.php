<?
$MESS["WD_NAME"] = "Nombre";
$MESS["WD_DROP_CONFIRM"] = "Está seguro que desea borrar la carpeta de #NAME# irreversiblemente?";
$MESS["WD_DROP_SECTION"] = "Borrar Carpeta #NAME#";
$MESS["WD_EDIT_SECTION"] = "Editar la carpeta #NAME#";
$MESS["WD_ADD_SECTION"] = "Crear Carpeta";
$MESS["WD_PARENT_SECTION"] = "Nivel Principal";
$MESS["WD_DROP_SECTION_DESCRIPTION"] = "Borrar Carpeta <b>#PATH#</b>";
$MESS["WD_EDIT_SECTION_DESCRIPTION"] = "Borrar Carpeta <b>#PATH#</b>";
$MESS["WD_ADD_SECTION_DESCRIPTION"] = "Crear Carpeta En <b>#PATH#</b> ";
?>