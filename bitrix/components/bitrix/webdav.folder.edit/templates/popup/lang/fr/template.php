<?
$MESS["WD_EDIT_SECTION"] = "Modification du dossier #NAME#";
$MESS["WD_EDIT_SECTION_DESCRIPTION"] = "Modification du dossier <b>#PATH#</b>";
$MESS["WD_NAME"] = "Dénomination";
$MESS["WD_PARENT_SECTION"] = "Section de parent";
$MESS["WD_ADD_SECTION"] = "Création d'un dossier";
$MESS["WD_ADD_SECTION_DESCRIPTION"] = "Création du dossier dans un catalogue <b>#PATH#</b>";
$MESS["WD_DROP_SECTION"] = "Suppression du Dossier #NAME#";
$MESS["WD_DROP_SECTION_DESCRIPTION"] = "Suppression du Dossier <b>#PATH#</b>";
$MESS["WD_DROP_CONFIRM"] = "tes-vous sûr de supprimer le dossier #NAME# sans possibilité de sa récupération?";
?>