<?
$MESS["WD_NAME"] = "Nombre";
$MESS["WD_DROP"] = "Borrar";
$MESS["WD_SAVE"] = "Guardar";
$MESS["WD_CANCEL"] = "Cancelar";
$MESS["WD_DROP_CONFIRM"] = "Está usted seguro que desea borrar la carpeta #NAME# irreversiblemente?";
$MESS["WD_DROP_SECTION"] = "Borrar Carpeta #NAME# ";
$MESS["WD_EDIT_SECTION"] = "Editar Carpeta #NAME#";
$MESS["WD_ADD_SECTION"] = "Crear Carpeta";
?>