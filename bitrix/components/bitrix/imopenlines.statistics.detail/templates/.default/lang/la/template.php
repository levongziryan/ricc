<?
$MESS["CT_BLL_SELECTED"] = "Número de registros";
$MESS["OL_STAT_BACK"] = "Volver";
$MESS["OL_STAT_BACK_TITLE"] = "Volver atrás";
$MESS["OL_STAT_USER_ID_CANCEL_TITLE"] = "Restablecer filtro de empleado";
$MESS["OL_STAT_FILTER_CANCEL"] = "Restablecer filtro";
$MESS["OL_STAT_FILTER_CANCEL_TITLE"] = "Restablecer filtro";
$MESS["OL_STAT_USER_ID_CANCEL"] = "Restablecer filto de empleado";
$MESS["OL_STAT_EXCEL"] = "Exportar a Microsoft Excel";
$MESS["OL_STAT_TITLE"] = "#LINE_NAME# - Estadística";
?>