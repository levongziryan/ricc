<?
$MESS["OL_COMPONENT_MODULE_NOT_INSTALLED"] = "El módulo Open Channels no está instalado.";
$MESS["OL_COMPONENT_TABLE_INPUT"] = "Entrante";
$MESS["OL_COMPONENT_TABLE_OUTPUT"] = "Saliente";
$MESS["OL_STATS_HEADER_STATUS"] = "Estado";
$MESS["OL_COMPONENT_TABLE_STATUS_CLOSED"] = "Conversación cerrada";
$MESS["OL_COMPONENT_TABLE_STATUS_PAUSE"] = "Operador detuvo la conversación";
$MESS["OL_COMPONENT_TABLE_STATUS_WAIT_ANSWER"] = "A la espera de la respuesta del operador";
$MESS["OL_COMPONENT_TABLE_STATUS_IN_PROCESS"] = "Conversación en curso.";
$MESS["OL_COMPONENT_TABLE_SOURCE_TELEGRAMBOT"] = "Telegrama";
$MESS["OL_COMPONENT_TABLE_SOURCE_VKGROUP"] = "VK";
$MESS["OL_COMPONENT_TABLE_SOURCE_FACEBOOK"] = "Facebook";
$MESS["OL_COMPONENT_TABLE_SOURCE_SKYPEBOT"] = "Skype";
$MESS["OL_COMPONENT_TABLE_YES"] = "Si";
$MESS["OL_COMPONENT_TABLE_NO"] = "No";
$MESS["OL_COMPONENT_TABLE_ACTION_HISTORY"] = "Historial";
$MESS["OL_COMPONENT_TABLE_ACTION_ANSWER"] = "Conectar";
$MESS["OL_COMPONENT_TABLE_ACTION_START"] = "Iniciar conversación";
$MESS["OL_STATS_HEADER_MODE_NAME"] = "Tipo";
$MESS["OL_STATS_HEADER_ACTION"] = "Acciones";
$MESS["OL_STATS_HEADER_SOURCE_TEXT"] = "Origen";
$MESS["OL_STATS_HEADER_USER_NAME"] = "Cliente";
$MESS["OL_STATS_HEADER_CRM_TEXT"] = "Registro del CRM";
$MESS["OL_STATS_HEADER_PAUSE_TEXT"] = "Asignado";
$MESS["OL_STATS_HEADER_WORKTIME_TEXT"] = "Horas de trabajo";
$MESS["OL_STATS_HEADER_OPERATOR_NAME"] = "Empleado";
$MESS["OL_STATS_HEADER_DATE_CREATE"] = "Creado el ";
$MESS["OL_STATS_HEADER_DATE_MODIFY"] = "Modificado el";
$MESS["OL_STATS_HEADER_MESSAGE_COUNT"] = "Comunicaciones";
$MESS["OL_STATS_FILTER_UNSET"] = "(ninguno)";
$MESS["OL_STATS_FILTER_Y"] = "Si";
$MESS["OL_STATS_FILTER_N"] = "No";
$MESS["OL_COMPONENT_TABLE_ACTION_ASSIGN"] = "Transferir la conversación para mí";
$MESS["OL_STATS_HEADER_DATE_CLOSE"] = "Cerrado el ";
$MESS["OL_STATS_HEADER_DATE_OPERATOR_ANSWER"] = "El operador respondió el";
$MESS["OL_STATS_HEADER_DATE_OPERATOR_CLOSE"] = "El operador cerró el";
$MESS["OL_STATS_HEADER_DATE_OPERATOR"] = "Solicitud enviada al operador el";
$MESS["OL_STATS_HEADER_TIME_ANSWER_WO_BOT"] = "Tiempo de respuesta del operador";
$MESS["OL_STATS_HEADER_TIME_CLOSE_WO_BOT"] = "Duración de la conversación";
$MESS["OL_STATS_HEADER_TIME_ANSWER"] = "El tiempo de respuesta del operador (incl. actividad de chat bot)";
$MESS["OL_STATS_HEADER_TIME_CLOSE"] = "Duración de la conversación (incl. actividad de chat bot)";
$MESS["OL_STATS_HEADER_CRM_LINK"] = "Enlace del CRM";
$MESS["OL_STATS_HEADER_DATE_LAST_MESSAGE"] = "Último mensaje publicado el";
$MESS["OL_STATS_HEADER_DATE_FIRST_ANSWER"] = "El operador respondió el";
$MESS["OL_STATS_HEADER_TIME_FIRST_ANSWER"] = "Esperando que el operador pueda responder";
$MESS["OL_STATS_HEADER_TIME_DIALOG_WO_BOT"] = "Duración de la conversación";
$MESS["OL_STATS_HEADER_TIME_DIALOG"] = "Duración de la conversación (incl. actividad de chat bot)";
$MESS["OL_STATS_HEADER_TIME_BOT"] = "Tiempo de chat de bot";
$MESS["OL_COMPONENT_ACCESS_DENIED"] = "No tiene permiso para ver estadísticas.";
$MESS["OL_STATS_HEADER_VOTE_CLIENT"] = "Clasificados por clientes";
$MESS["OL_STATS_HEADER_VOTE_CLIENT_LIKE"] = "Me gusta";
$MESS["OL_STATS_HEADER_VOTE_CLIENT_DISLIKE"] = "No me gusta";
$MESS["OL_STATS_HEADER_VOTE_HEAD"] = "Evaluado por el supervisor";
$MESS["OL_STATS_HEADER_MODE_ID"] = "#";
$MESS["OL_STATS_HEADER_EXTRA_REGISTER"] = "Registrado el";
$MESS["OL_STATS_HEADER_EXTRA_TARIFF"] = "Plan";
$MESS["OL_STATS_HEADER_SPAM"] = "Spam";
$MESS["OL_STATS_HEADER_VOTE_HEAD_WO"] = "No calificado";
$MESS["OL_STATS_VOTE_AS_HEAD_BUTTON"] = "Calificar";
$MESS["OL_STATS_HEADER_SEND_FORM"] = "Formulario presentado";
$MESS["OL_STATS_HEADER_EXTRA_DOMAIN"] = "Dominio de origen";
$MESS["OL_STATS_HEADER_EXTRA_URL"] = "Origen";
$MESS["OL_STATS_HEADER_STATUS_DETAIL"] = "Estado (detallado)";
$MESS["OL_COMPONENT_TABLE_STATUS_CLIENT"] = "Cliente en espera de respuesta del operador";
$MESS["OL_COMPONENT_TABLE_STATUS_OPERATOR"] = "El operador respondió";
$MESS["OL_COMPONENT_TABLE_STATUS_CLIENT_AFTER_OPERATOR"] = "Cliente en espera de la respuesta del operador (tiene otra pregunta)";
$MESS["OL_COMPONENT_TABLE_STATUS_WAIT_ACTION_2"] = "Conversación pendiente de cierre (en espera de calificación o después de la respuesta automática)";
$MESS["OL_COMPONENT_TABLE_STATUS_NEW"] = "Nueva conversación iniciada";
$MESS["OL_COMPONENT_TABLE_STATUS_OPERATOR_SKIP"] = "Conversación en la cola de espera del operador";
$MESS["OL_COMPONENT_TABLE_STATUS_OPERATOR_ANSWER"] = "El operador respondió a la conversación";
$MESS["OL_STATS_HEADER_SESSION_ID"] = "ID de la sesión";
$MESS["OL_STATS_HEADER_SOURCE_TEXT_2"] = "Canal";
$MESS["OL_STATS_HEADER_SEND_HISTORY"] = "Historial solicitado";
$MESS["OL_STATS_HEADER_CRM"] = "CRM";
$MESS["OL_STATS_HEADER_SPAM_2"] = "Spam / Detención Forzada";
$MESS["OL_STATS_HEADER_CONFIG_NAME"] = "Canal Abierto";
$MESS["OL_STATS_FILTER_MESSAGE_COUNT"] = "Recuento de mensajes";
?>