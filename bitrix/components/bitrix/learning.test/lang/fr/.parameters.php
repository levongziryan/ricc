<?
$MESS["LEARNING_DESC_YES"] = "Oui";
$MESS["T_LEARNING_PAGE_NUMBER_VARIABLE"] = "Identificateur de la question";
$MESS["LEARNING_COURSE_ID"] = "Identificateur du cours";
$MESS["T_LEARNING_DETAIL_ID"] = "Identificateur du texte";
$MESS["LEARNING_PAGE_WINDOW_NAME"] = "Quantité de questions dans une chaîne de navigation";
$MESS["LEARNING_DESC_NO"] = "Non";
$MESS["LEARNING_SHOW_TIME_LIMIT"] = "Afficher le compteur de la limite du temps";
$MESS["LEARNING_CHECK_PERMISSIONS"] = "Vérifier le droit d'accès";
$MESS["LEARNING_GRADEBOOK_TEMPLATE_NAME"] = "Les résultats des tests de la page URL";
?>