<?
$MESS["LEARNING_COURSES_TEST_DELETE_CONF"] = "Toutes les informations relatives à cette écriture seront effacées! Continuer?";
$MESS["LEARNING_TEST_TIME_INTERVAL_ERROR_D"] = "d.";
$MESS["LEARNING_TEST_DENIED_PREVIOUS"] = "L'accès au test est interdit parce que vous n'a avez pas passé le test #TEST_LINK#";
$MESS["LEARNING_COURSES_QUESTION_EDIT"] = "Modifier la question";
$MESS["LEARNING_COURSES_TEST_EDIT"] = "Modifier le test";
$MESS["LEARNING_TEST_TIME_INTERVAL_ERROR_M"] = "min.";
$MESS["LEARNING_NEW_TEXT_ANSWER"] = "Nouvelle réponse textuelle";
$MESS["LEARNING_TEST_TIME_INTERVAL_ERROR"] = "Vous pourrez repasser le test dans";
$MESS["LEARNING_ATTEMPT_FAILED"] = "Test échoué";
$MESS["LEARNING_COURSES_TEST_DELETE"] = "Eliminer le Test";
$MESS["LEARNING_TEST_TIME_INTERVAL_ERROR_H"] = "h.";
$MESS["LEARNING_ATTEMPT_NOT_FOUND_ERROR"] = "Tâche introuvable.";
$MESS["LEARNING_TIME_LIMIT"] = "Test de passe-temps expiré.";
$MESS["LEARNING_NO_AUTHORIZE"] = "Autorisation nécessaire pour visualiser cette page.";
$MESS["LEARNING_MODULE_NOT_FOUND"] = "Module des blogs non installé.";
$MESS["LEARNING_ATTEMPT_CREATE_ERROR"] = "Une erreur est survenue en essayant de créer la tâche.";
$MESS["LEARNING_RESPONSE_SAVE_ERROR"] = "Une erreur est survenue lors de la tentative pour sauver réponse.";
$MESS["LEARNING_LIMIT_ERROR"] = "Pas plus de tentatives.";
$MESS["LEARNING_TEST_DENIED"] = "Testez introuvable ou accès refusé.";
?>