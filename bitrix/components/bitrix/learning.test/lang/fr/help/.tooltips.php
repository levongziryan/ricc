<?
$MESS["TEST_ID_TIP"] = "Sélectionnez ici l'un des tests existants. Si vous sélectionnez <b><i>(autre)</i></b>, vous devrez spécifier l'ID d'essai dans le domaine de côté.";
$MESS["COURSE_ID_TIP"] = "Sélectionnez ici l'un des cours existants. Si vous sélectionnez <b><i>(autre)</i></b>, vous devrez spécifier l'ID de cours dans le domaine de côté.";
$MESS["SHOW_TIME_LIMIT_TIP"] = "Afficher le compteur de la limite du temps";
$MESS["PAGE_NUMBER_VARIABLE_TIP"] = "Identificateur de la question.";
$MESS["PAGE_WINDOW_TIP"] = "Quantité de questions affichées dans la chaîne de navigation.";
$MESS["SET_TITLE_TIP"] = "Si cette option est sélectionnée, en tant que titre de la page sera désigné le nom d'utilisateur.";
$MESS["GRADEBOOK_TEMPLATE_TIP"] = "Chemin vers la page avec les résultats du test (Chemin vers le journal).";
?>