<?
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_CONNECT_ACTIVE"] = "Conectar";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_CONNECTOR_ERROR_STATUS"] = "Se produjo un error durante el uso de chat en vivo. Por favor, compruebe los datos especificados y pruebe de nuevo.";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_INDEX_DESCRIPTION"] = "Conectar una página accesible públicamente con su compañía para el chat del Canal Abierto y utilice el enlace en la página de firmas de correo electrónico o simplemente compartirlo en las redes sociales.";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_DESCRIPTION_1"] = "La página de chat ya está disponible en el enlace que aparece a continuación.<br><br>Podrás moldear tu página chat como desee.
";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_LINK_TIP"] = "El texto del enlace sólo puede incluir caracteres latinos, números, un punto y un guión.";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_CONFIG"] = "Ajustes";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_PAGE_LINK"] = "Enlace público de chat";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_PAGE_NAME"] = "nombre";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_COPY"] = "Copiar";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_COPIED"] = "Copiado en el portapapeles";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_FINAL_LINK"] = "enlace completo";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_LINK_NOTICE"] = "los usuarios autenticados de su Bitrix24 no pueden utilizar el chat que está en la página pública";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_PAGE_CONFIG"] = "configuración de la página pública";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_PAGE_TYPE"] = "Diseño del chat";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_PAGE_TYPE_1"] = "Transparente";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_PAGE_TYPE_2"] = "Colorido";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_PAGE_IMAGE_LOAD"] = "Puede cargar una imagen que se utilizará como fondo de la página de chat";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_PAGE_IMAGE_LOAD_BUTTON"] = "Subir archivo";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_PAGE_IMAGE_DELETE"] = "Eliminar imagen";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_PAGE_CSS"] = "CSS personalizado";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_PAGE_CSS_2"] = "utilice CSS con estilos personalizados";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_PAGE_CSS_PATH"] = "Ruta del archivo CSS";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_PAGE_CSS_TEXT"] = "Código CSS";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_PAGE_SIGN"] = "Firma";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_PAGE_SIGN_2"] = "quitar firma";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_PAGE_SIGN_3"] = "Desarrollado por BItrix24";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_PAGE_SIGN_HINT_1"] = "Restricciones del plan";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_PAGE_SIGN_HINT_2"] = "La firma sólo puede ser removido en planes comerciales.";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_PAGE_WIDGET"] = "Código del widget";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_WIDGET_CONFIG"] = "Ajustes del widget";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_PAGE_BUTTON_TEXT"] = "Puede obtener el código y configurar el chat en vivo para su sitio web en \"#LINK#\"";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SF_PAGE_BUTTON_LINK"] = "Widget de chat en vivo";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_WO_PUBLUC"] = "enlace público no proporcionada";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_INDEX_DESCRIPTION_1"] = "Instalar chat en vivo en su sitio web y hablar con sus clientes en línea, donde quiera que esté. <br> Conectar la página Chat en vivo de un canal abierto y utilizar el enlace de la página de firmas de correo electrónico, o simplemente compartir el enlace de chat en vivo en las redes sociales .";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_PL_HEADER"] = "Página pública";
?>