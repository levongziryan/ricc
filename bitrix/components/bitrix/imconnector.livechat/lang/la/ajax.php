<?
$MESS["IMCONNECTOR_PERMISSION_DENIED"] = "Usted no tiene permiso para acceder a esta función.";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_MODULE_IMOPENLINES_NOT_INSTALLED"] = "El módulo \"Canal Abierto\" no está instalado.";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_NO_ACTIVE_CONNECTOR"] = "Este conector está inactivo.";
$MESS["IMCONNECTOR_COMPONENT_LIVECHAT_SESSION_HAS_EXPIRED"] = "Su sesión ha caducado. Envíe el formulario de nuevo.";
?>