<?
$MESS["CRM_DEDUPE_LIST_REBUILD_INDEX_COMPLETED_SUMMARY"] = "A lista de duplicados foi feita. Combinações encontradas: #PROCESSED_ITEMS#.";
$MESS["CRM_DEDUPE_LIST_REBUILD_INDEX_PROGRESS_SUMMARY"] = "Combinações encontradas: #PROCESSED_ITEMS#.";
$MESS["CRM_DEDUPE_LIST_MERGE_ERROR_READ_DENIED"] = "Nenhum acesso lido para \"#TITLE#\" [#ID#]";
$MESS["CRM_DEDUPE_LIST_MERGE_ERROR_UPDATE_DENIED"] = "Nenhum acesso atualizado para\"#TITLE#\" [#ID#]";
$MESS["CRM_DEDUPE_LIST_MERGE_ERROR_DELETE_DENIED"] = "Nenhum acesso excluído para\"#TITLE#\" [#ID#]";
$MESS["CRM_DEDUPE_LIST_MERGE_ERROR_NOT_FOUND"] = "Não foi possível encontrar o item [#ID#]";
$MESS["CRM_DEDUPE_LIST_MERGE_ERROR_UPDATE_FAILED"] = "Não foi possível salvar \"#TITLE#\" [#ID#]";
$MESS["CRM_DEDUPE_LIST_MERGE_ERROR_DELETE_FAILED"] = "Não foi possível excluir #TITLE#\" [#ID#]";
$MESS["CRM_DEDUPE_LIST_MERGE_GENERAL_ERROR"] = "Ocorreu um erro durante a fusão";
$MESS["CRM_DEDUPE_LIST_JUNK"] = "A informação está desatualizada. Atualize a lista.";
?>