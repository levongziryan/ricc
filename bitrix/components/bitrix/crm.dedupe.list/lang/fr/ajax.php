<?
$MESS["CRM_DEDUPE_LIST_REBUILD_INDEX_PROGRESS_SUMMARY"] = "Correspondances trouvées: #PROCESSED_ITEMS#";
$MESS["CRM_DEDUPE_LIST_MERGE_ERROR_NOT_FOUND"] = "Vous ne trouvez pas l'article [#ID#]";
$MESS["CRM_DEDUPE_LIST_MERGE_ERROR_UPDATE_FAILED"] = "Impossible d'enregistrer '#TITLE#' [#ID#]";
$MESS["CRM_DEDUPE_LIST_MERGE_ERROR_DELETE_FAILED"] = "Vous ne pouvez pas supprimer  '#TITLE#' [#ID#]";
$MESS["CRM_DEDUPE_LIST_MERGE_ERROR_UPDATE_DENIED"] = "Pas d'accès de mise à jour '#TITLE#' [#ID#]";
$MESS["CRM_DEDUPE_LIST_MERGE_ERROR_DELETE_DENIED"] = "Pas d'accès à la suppression '#TITLE#' [#ID#]";
$MESS["CRM_DEDUPE_LIST_MERGE_ERROR_READ_DENIED"] = "Pas d'accès en lecture à '#TITLE#' [#ID#]";
$MESS["CRM_DEDUPE_LIST_REBUILD_INDEX_COMPLETED_SUMMARY"] = "Liste des doublons a été construit. Correspondances trouvées:  #PROCESSED_ITEMS#.";
$MESS["CRM_DEDUPE_LIST_MERGE_GENERAL_ERROR"] = "Une erreur est survenue lors de la fusion";
$MESS["CRM_DEDUPE_LIST_JUNK"] = "Information est à jour. S'il vous plaît mettre à jour la liste.";
?>