<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_DEDUPE_LIST_INVALID_ENTITY_TYPE"] = "El tipo '#TYPE_NAME#' no se admite en este contexto.";
$MESS["CRM_DEDUPE_LIST_COL_TITLE"] = "Nombre";
$MESS["CRM_DEDUPE_LIST_COL_PERSON"] = "Nombre completo";
$MESS["CRM_DEDUPE_LIST_COL_ORGANIZATION"] = "Nombre de la compañía";
$MESS["CRM_DEDUPE_LIST_COL_PHONE"] = "Teléfono";
$MESS["CRM_DEDUPE_LIST_COL_EMAIL"] = "E-mail";
$MESS["CRM_DEDUPE_LIST_COL_RESPONSIBLE"] = "Persona responsable";
$MESS["CRM_DEDUPE_LIST_NOT_FOUND"] = "No puede obtener una lista de duplicados por campo '#NAME#'.Por favor, vuelva a crear la lista.";
$MESS["CRM_DEDUPE_LIST_NOT_FOUND_MSG"] = "No hay duplicados para \"#NAME#\" encontrados.";
$MESS["CRM_DEDUPE_LIST_NOT_FOUND_MSG_PLURAL"] = "No se han encontrado campos para duplicados #NAMES#";
$MESS["CRM_DEDUPE_LIST_DEFAUL_SCOPE_TITLE"] = "No seleccionado";
?>