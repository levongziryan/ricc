<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado. ";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "O módulo de e-Store não está instalado. ";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "O módulo de Catálogo Comercial não está instalado. ";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_INVOICE_NAV_TITLE_EDIT"] = "Fatura: #NAME#";
$MESS["CRM_INVOICE_NAV_TITLE_ADD"] = "Adicionar fatura";
$MESS["CRM_INVOICE_NAV_TITLE_LIST"] = "Faturas";
$MESS["CRM_SECTION_INVOICE"] = "Informação de faturas";
$MESS["CRM_SECTION_PRODUCT_ROWS"] = "Itens da fatura";
$MESS["CRM_FIELD_PRODUCT_ROWS"] = "Itens da fatura";
$MESS["RESPONSIBLE_NOT_ASSIGNED"] = "[não atribuído]";
$MESS["CRM_FIELD_COMPANY_TITLE"] = "Empresa";
$MESS["CRM_SECTION_EVENT_MAIN"] = "Histórico de alterações da fatura";
$MESS["CRM_FIELD_INVOICE_EVENT"] = "Histórico de alterações da fatura";
$MESS["CRM_EXT_SALE_CD_EDIT"] = "Editar";
$MESS["CRM_EXT_SALE_CD_VIEW"] = "Visualizar";
$MESS["CRM_EXT_SALE_CD_PRINT"] = "Imprimir";
$MESS["CRM_EXT_SALE_DEJ_PRINT"] = "Imprimir";
$MESS["CRM_EXT_SALE_DEJ_SAVE"] = "Salvar";
$MESS["CRM_EXT_SALE_DEJ_ORDER"] = "Pedido";
$MESS["CRM_EXT_SALE_DEJ_CLOSE"] = "Fechar";
$MESS["CRM_SECTION_INVOICE_INFO"] = "Informações de fatura";
$MESS["CRM_FIELD_ACCOUNT_NUMBER"] = "Nota Fiscal #";
$MESS["CRM_FIELD_ORDER_TOPIC"] = "Tópico";
$MESS["CRM_FIELD_STATUS_ID"] = "Status";
$MESS["CRM_FIELD_PAY_VOUCHER_NUM"] = "Nota Fiscal #";
$MESS["CRM_FIELD_PAY_VOUCHER_DATE"] = "Data de pagamento";
$MESS["CRM_FIELD_REASON_MARKED_SUCCESS"] = "Observação de pagamento";
$MESS["CRM_FIELD_DATE_MARKED"] = "Recusado em";
$MESS["CRM_FIELD_REASON_MARKED"] = "Motivo de recusa";
$MESS["CRM_FIELD_DATE_BILL"] = "Data de emissão da fatura";
$MESS["CRM_FIELD_DATE_PAY_BEFORE"] = "Pagar antes de";
$MESS["CRM_FIELD_RESPONSIBLE_ID"] = "Pessoa responsável";
$MESS["CRM_FIELD_CURRENCY_ID"] = "Moeda da fatura";
$MESS["CRM_FIELD_UF_QUOTE_ID"] = "Estimativa";
$MESS["CRM_FIELD_UF_DEAL_ID"] = "Negociação";
$MESS["CRM_FIELD_CLIENT_ID"] = "Contratante";
$MESS["CRM_INVOICE_DEAL_NOT_ASSIGNED"] = "não atribuido";
$MESS["CRM_INVOICE_CLIENT_NOT_ASSIGNED"] = "não atribuido";
$MESS["CRM_INVOICE_QUOTE_NOT_ASSIGNED"] = "não atribuido";
$MESS["CRM_SECTION_INVOICE_PAYER"] = "ID do Pagador";
$MESS["CRM_FIELD_CONTACT_PERSON_ID"] = "Pessoa de contato";
$MESS["CRM_INVOICE_CONTACT_PERSON_NOT_ASSIGNED"] = "não atribuido";
$MESS["CRM_FIELD_LOCATION"] = "Localização";
$MESS["CRM_SECTION_COMMENTS"] = "Observações";
$MESS["CRM_FIELD_COMMENTS"] = "Observação do representante de vendas";
$MESS["CRM_FIELD_USER_DESCRIPTION"] = "Notas da Fatura (aparece na fatura)";
$MESS["CRM_SECTION_ADDITIONAL"] = "Mais";
$MESS["CRM_INVOICE_SHOW_CONTACT_SELECTOR_HEADER"] = "Contatos mencionados na fatura";
$MESS["CRM_SECTION_PAY_SYSTEM"] = "Forma de pagamento";
$MESS["CRM_FIELD_PAY_SYSTEM_ID"] = "Forma de pagamento";
$MESS["CRM_INVOICE_FIELD_UF_MYCOMPANY_ID"] = "Informações da sua empresa";
$MESS["CRM_FIELD_ENTITY_TREE"] = "Dependências";
$MESS["CRM_FIELD_PAYER_INFO"] = "Detalhes bancários";
?>