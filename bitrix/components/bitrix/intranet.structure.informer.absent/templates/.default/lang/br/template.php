<?
$MESS["INTR_ISIA_TPL_SHOW"] = "Exibir:";
$MESS["INTR_ISIA_TPL_ALL"] = "Todos";
$MESS["INTR_ISIA_TPL_NOW"] = "Agora";
$MESS["INTR_ISIA_TPL_TODAY"] = "Hoje";
$MESS["INTR_ISIA_TPL_TOMORROW"] = "Amanhã";
$MESS["INTR_ISIA_TPL_THE_DAY_AFTER_TOMORROW"] = "Dia depois de amanhã";
$MESS["INTR_ISIA_TPL_IN"] = "em";
$MESS["INTR_ISIA_TPL_DAYS_1"] = "dia";
$MESS["INTR_ISIA_TPL_DAYS_2_4"] = "dia";
$MESS["INTR_ISIA_TPL_DAYS"] = "dias";
$MESS["INTR_ISIA_TPL_FROM"] = "de";
$MESS["INTR_ISIA_TPL_TILL"] = "até";
$MESS["INTR_ISIA_TPL_NO_ABSENCES"] = "Nenhuma ausência";
?>