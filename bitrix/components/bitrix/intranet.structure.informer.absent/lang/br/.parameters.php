<?
$MESS["INTR_PREDEF_DEPARTMENT"] = "Departmento/Escritório";
$MESS["INTR_ISIA_PARAM_DETAIL_URL"] = "Página de Visualização de Detalhe";
$MESS["INTR_ABSC_PARAM_CALENDAR_IBLOCK_ID"] = "Bloco de informação para Calendários Pessoais";
$MESS["INTR_ABSC_PARAM_CALENDAR_IBLOCK_TYPE"] = "Tipo de Bloco de Informação para Calendários Pessoais";
$MESS["INTR_ISIA_PARAM_NUM_USERS"] = "Exibir Registros";
$MESS["INTR_ISIA_PARAM_PM_URL"] = "Página de Mensagem Pessoal";
$MESS["INTR_ISIA_PARAM_PATH_TO_CONPANY_DEPARTMENT"] = "Caminho de Página de Modelo de Departamento";
$MESS["INTR_ISIA_PARAM_DATE_TIME_FORMAT"] = "Formato de Data e Hora";
$MESS["INTR_ISIA_PARAM_SHOW_YEAR"] = "Exibir Ano de Nascimento";
$MESS["INTR_ISIA_PARAM_SHOW_YEAR_VALUE_Y"] = "Todos";
$MESS["INTR_ISIA_PARAM_SHOW_YEAR_VALUE_M"] = "apenas homens";
$MESS["INTR_ISIA_PARAM_SHOW_YEAR_VALUE_N"] = "ninguém";
$MESS["INTR_ISIA_PARAM_NAME_TEMPLATE"] = "Formato de Nome";
$MESS["INTR_ISIA_PARAM_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["INTR_ISIA_PARAM_SHOW_LOGIN"] = "Exibir Nome de Login se nenhum campo de nome de usuário obrigatório está disponível";
$MESS["INTR_ISIA_PARAM_DATE_FORMAT"] = "Formato de Data";
?>