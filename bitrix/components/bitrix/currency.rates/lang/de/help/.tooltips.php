<?
$MESS ['CACHE_TYPE_TIP'] = "<i>Automatisch</i>: Der Cache ist gültig gemäß der Definition in den Cache-Einstellungen;<br /><i>Cache</i>: immer cachen für den Zeitraum, der im nächsten Feld definiert wird;<br /><i>Nicht cachen</i>: es wird kein Caching ausgeführt.";
$MESS ['CACHE_TIME_TIP'] = "Feld für die Eingabe der Cache-Laufzeit in Sekunden.";
$MESS ['RATE_DAY_TIP'] = "Hier wird das Datum angegeben, dem der Währungskurs entsprechen soll. Wenn keine Währungsrate für das angegebene Datum gibt, wird die Standard Währungsrate angezeigt.";
$MESS ['CURRENCY_BASE_TIP'] = "Hier wird eine der im System vorhandenen Währungen ausgewählt, nach der...";
$MESS ['arrCURRENCY_FROM_TIP'] = "Wählen Sie die im System vorhandenen Währungen, die in der Tabelle angezeigt werden sollen.";
$MESS ['SHOW_CB_TIP'] = "Wenn diese Option aktiv ist, werden Währungsraten relativ zu RUB der Russischen Bank angezeigt. Es ist nur effektiv, wenn das Feld <b>Währung in die konvertiert werden soll</b> auf RUB gesetzt ist.";
?>