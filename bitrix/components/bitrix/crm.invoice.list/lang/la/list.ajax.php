<?
$MESS["CRM_INVOICE_LIST_REBUILD_STATISTICS_NOT_REQUIRED_SUMMARY"] = "Datos estadísticos de las facturas fue actualizado.";
$MESS["CRM_INVOICE_LIST_REBUILD_STATISTICS_PROGRESS_SUMMARY"] = "Facturas procesadas: #PROCESSED_ITEMS# of #TOTAL_ITEMS#.";
$MESS["CRM_INVOICE_LIST_REBUILD_STATISTICS_COMPLETED_SUMMARY"] = "Procesamiento listo de datos estadísticos para las facturas. Facturas procesadas: #PROCESSED_ITEMS#.";
$MESS["CRM_INVOICE_LIST_ROW_COUNT"] = "Total: #ROW_COUNT#";
?>