<?
$MESS["CRM_COLUMN_PRODUCT_QUANTITY"] = "Quantité";
$MESS["CRM_COLUMN_COMPANY"] = "Entreprise";
$MESS["CRM_COLUMN_CONTACT"] = "Client";
$MESS["CRM_COLUMN_DEAL"] = "Affaire";
$MESS["ERROR_INVOICE_IS_EMPTY_2"] = "Il n'y a pas d'articles.";
$MESS["CRM_COLUMN_PRODUCT_NAME"] = "Article";
$MESS["CRM_COLUMN_PRODUCT_PRICE"] = "Prix";
?>