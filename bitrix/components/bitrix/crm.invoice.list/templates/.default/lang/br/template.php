<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_INVOICE_SHOW_TITLE"] = "Visualizar fatura";
$MESS["CRM_INVOICE_SHOW"] = "Visualizar";
$MESS["CRM_INVOICE_PAYMENT_HTML_TITLE"] = "Imprimir";
$MESS["CRM_INVOICE_PAYMENT_HTML"] = "Imprimir";
$MESS["CRM_INVOICE_PAYMENT_PDF_TITLE"] = "Visualizar fatura em formato PDF";
$MESS["CRM_INVOICE_PAYMENT_PDF"] = "Visualizar PDF";
$MESS["CRM_INVOICE_EDIT_TITLE"] = "Editar fatura";
$MESS["CRM_INVOICE_EDIT"] = "Editar";
$MESS["CRM_INVOICE_COPY_TITLE"] = "Copiar";
$MESS["CRM_INVOICE_COPY"] = "Copiar";
$MESS["CRM_INVOICE_DELETE_TITLE"] = "Deletar fatura";
$MESS["CRM_INVOICE_DELETE"] = "Deletar";
$MESS["CRM_INVOICE_DELETE_CONFIRM"] = "Você tem certeza que deseja deletar esta fatura?";
$MESS["CRM_INVOICE_LIST_ADD_SHORT"] = "Criar fatura";
$MESS["CRM_INVOICE_REBUILD_ACCESS_ATTRS"] = "As permissões de acesso atualizadas exigem que você atualize os atuais atributos de acesso usando a <a id=\"#ID#\" target=\"_blank\" href= \"#URL#\">página de gerenciamento de permissões</a>.";
$MESS["CRM_PS_RQ_TX_PROC_DLG_TITLE"] = "Transferir informações das opções de pagamento";
$MESS["CRM_PS_RQ_TX_PROC_DLG_DLG_SUMMARY"] = "Isto irá migrar as informações do vendedor das opções de pagamento para empresas.";
$MESS["CRM_PSRQ_LRP_DLG_BTN_START"] = "Executar";
$MESS["CRM_PSRQ_LRP_DLG_BTN_STOP"] = "Parar";
$MESS["CRM_PSRQ_LRP_DLG_BTN_CLOSE"] = "Fechar";
$MESS["CRM_PSRQ_LRP_DLG_REQUEST_ERR"] = "Erro ao processar a solicitação.";
$MESS["CRM_INVOICE_LIST_FILTER_NAV_BUTTON_LIST"] = "Lista";
$MESS["CRM_INVOICE_LIST_FILTER_NAV_BUTTON_WIDGET"] = "Relatórios";
$MESS["CRM_INVOICE_LIST_FILTER_NAV_BUTTON_KANBAN"] = "Kanban";
$MESS["CRM_INVOICE_LIST_CHOOSE_ACTION"] = "Selecionar ação";
$MESS["CRM_INVOICE_LIST_APPLY_BUTTON"] = "Aplicar";
$MESS["CRM_SHOW_ROW_COUNT"] = "Mostrar quantidade";
$MESS["CRM_INVOICE_LIST_ADD_ON_DEAL"] = "Criar fatura para o negócio";
$MESS["CRM_INVOICE_START_CALL_LIST"] = "Começar a discagem";
$MESS["CRM_INVOICE_CREATE_CALL_LIST"] = "Criar lista de chamada";
$MESS["CRM_INVOICE_UPDATE_CALL_LIST"] = "Adicionar à lista de chamada";
$MESS["CRM_PS_RQ_TX_PROC_DLG_DLG_SUMMARY1"] = "Isto irá migrar as informações do vendedor das opções de pagamento para as informações da sua empresa.";
?>