<?
$MESS["CRM_COLUMN_PRODUCT_NAME"] = "Produkt";
$MESS["CRM_COLUMN_PRODUCT_PRICE"] = "Preis";
$MESS["CRM_COLUMN_PRODUCT_QUANTITY"] = "Menge";
$MESS["ERROR_INVOICE_IS_EMPTY_2"] = "In der Liste gibt es keine Rechnungen.";
$MESS["CRM_COLUMN_DEAL"] = "Auftrag";
$MESS["CRM_COLUMN_COMPANY"] = "Unternehmen";
$MESS["CRM_COLUMN_CONTACT"] = "Kontakt";
?>