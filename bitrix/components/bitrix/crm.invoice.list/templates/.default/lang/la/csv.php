<?
$MESS["CRM_COLUMN_PRODUCT_NAME"] = "producto";
$MESS["CRM_COLUMN_PRODUCT_PRICE"] = "Precio";
$MESS["CRM_COLUMN_PRODUCT_QUANTITY"] = "Cantidad";
$MESS["ERROR_INVOICE_IS_EMPTY_2"] = "No hay facturas en la lista.";
$MESS["CRM_COLUMN_DEAL"] = "Negociación";
$MESS["CRM_COLUMN_COMPANY"] = "Compañía";
$MESS["CRM_COLUMN_CONTACT"] = "Contacto";
?>