<?
$MESS["ECL_T_ACCESS_DENIED"] = "Accès interdit";
$MESS["ECL_T_INACTIVE_FEATURE"] = "Le calendrier des événements de l'utilisateur n'est pas disponible.";
$MESS["ECL_T_NO_ITEMS"] = "Il n'y a pas d'événements dans un calendrier pour des jours prochains.";
?>