<?
$MESS["ECL_P_ALLOW_SUPERPOSE"] = "Permitir Calendários Favoritos";
$MESS["ECL_P_CACHE_TIME"] = "Vida útil do cache (seg.)";
$MESS["ECL_P_SHOW_CUR_DATE"] = "-Data atual-";
$MESS["ECL_P_DETAIL_URL"] = "URL da Página de Visualização Detalhada";
$MESS["ECL_P_EVENTS_COUNT"] = "Eventos na Lista";
$MESS["ECL_P_IBLOCK"] = "Bloco de informação";
$MESS["ECL_P_USER_IBLOCK_ID"] = "Bloco de informação para Calendários do Usuário";
$MESS["ECL_P_IBLOCK_SECTION_ID"] = "ID da Seção do Bloco de Informação";
$MESS["ECL_P_IBLOCK_TYPE"] = "Tipo de Bloco de Informação";
$MESS["ECL_P_INIT_DATE"] = "Inicializado";
$MESS["ECL_P_LOAD_MODE"] = "Carregar Eventos";
$MESS["ECL_GROUP_BASE_SETTINGS"] = "Configurações Principais";
$MESS["ECL_P_CUR_USER_EVENT_LIST"] = "Exibir eventos do usuário atual";
$MESS["ECL_P_FUTURE_MONTH_COUNT"] = "Exibir primeiros eventos para (meses)";
$MESS["ECL_P_EVENT_LIST_MODE"] = "Exibir Apenas Lista de Eventos";
?>