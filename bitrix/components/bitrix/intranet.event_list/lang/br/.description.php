<?
$MESS["EVENT_CALENDAR_LIST"] = "Eventos no calendário";
$MESS["EVENT_CALENDAR_LIST_DESCRIPTION"] = "Exibe os primeiros eventos existentes em um calendário.";
$MESS["EVENT_CALENDAR"] = "Calendários de Eventos";
$MESS["EC_DEPRECATED"] = "(não recomendado)";
$MESS["EC_USE_MODULE_CALENDAR"] = ". É recomendado usar um novo componente \"Event Calendar 2.0\".";
?>