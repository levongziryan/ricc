<?
$MESS["EC_DEPRECATED"] = "(non recommandé)";
$MESS["EC_USE_MODULE_CALENDAR"] = ". Il est recommandé d'utiliser un nouveau composant 'Calendrier des événements 2.0'.";
$MESS["EVENT_CALENDAR"] = "Calendrier d'événements";
$MESS["EVENT_CALENDAR_LIST_DESCRIPTION"] = "Composant pour l'affichage de la liste des événements à venir dans un calendrier.";
$MESS["EVENT_CALENDAR_LIST"] = "Liste des événements du calendrier";
?>