<?
$MESS["CRM_LEAD_PRODUCT_ROWS_SAVING_ERROR"] = "Une erreur est survenue pendant l'enregistrement des produits.";
$MESS["CRM_LEAD_DEAULT_TITLE"] = "Nouvelle piste";
$MESS["CRM_LEAD_NOT_FOUND"] = "Piste introuvable.";
$MESS["CRM_LEAD_ACCESS_DENIED"] = "Accès refusé.";
$MESS["CRM_LEAD_DELETION_ERROR"] = "Erreur lors de la suppression de la transaction.";
?>