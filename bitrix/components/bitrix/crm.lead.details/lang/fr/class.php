<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n’est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_LEAD_CREATE_ON_BASIS"] = "Générer";
$MESS["CRM_LEAD_FIELD_TITLE"] = "Nom du lead";
$MESS["CRM_LEAD_FIELD_STATUS_ID"] = "Statut";
$MESS["CRM_LEAD_FIELD_STATUS_DESCRIPTION"] = "Informations sur le statut";
$MESS["CRM_LEAD_FIELD_OPPORTUNITY_WITH_CURRENCY"] = "Montant et devise";
$MESS["CRM_LEAD_FIELD_SOURCE_ID"] = "Source";
$MESS["CRM_LEAD_FIELD_SOURCE_DESCRIPTION"] = "Informations sur la source";
$MESS["CRM_LEAD_FIELD_ASSIGNED_BY_ID"] = "Personne responsable";
$MESS["CRM_LEAD_CREATION_PAGE_TITLE"] = "Nouveau lead";
$MESS["CRM_LEAD_FIELD_OPENED"] = "Accessible pour tous";
$MESS["CRM_LEAD_FIELD_HONORIFIC"] = "Salutation";
$MESS["CRM_LEAD_FIELD_NAME"] = "Prénom";
$MESS["CRM_LEAD_FIELD_LAST_NAME"] = "Nom";
$MESS["CRM_LEAD_FIELD_SECOND_NAME"] = "Deuxième prénom";
$MESS["CRM_LEAD_FIELD_BIRTHDATE"] = "Date de naissance";
$MESS["CRM_LEAD_FIELD_POST"] = "Fonction";
$MESS["CRM_LEAD_FIELD_ADDRESS"] = "Adresse";
$MESS["CRM_LEAD_FIELD_COMMENTS"] = "Commentaire";
$MESS["CRM_LEAD_FIELD_PRODUCTS"] = "Produits";
$MESS["CRM_LEAD_SECTION_MAIN"] = "Information sur le lead";
$MESS["CRM_LEAD_SECTION_ADDITIONAL"] = "Plus";
$MESS["CRM_LEAD_SECTION_PRODUCTS"] = "Produits";
$MESS["CRM_LEAD_TAB_PRODUCTS"] = "Produits";
$MESS["CRM_LEAD_TAB_AUTOMATION"] = "Automatisation";
$MESS["CRM_LEAD_TAB_EVENT"] = "Historique";
$MESS["CRM_LEAD_TAB_QUOTE"] = "Offres";
$MESS["CRM_LEAD_HONORIFIC_NOT_SELECTED"] = "Non sélectionné";
$MESS["CRM_LEAD_COPY_PAGE_TITLE"] = "Copier le lead";
$MESS["CRM_LEAD_DUP_CTRL_COMPANY_TTL_SUMMARY_TITLE"] = "par nom de la société";
$MESS["CRM_LEAD_DUP_CTRL_EMAIL_SUMMARY_TITLE"] = "par adresse e-mail";
$MESS["CRM_LEAD_DUP_CTRL_FULL_NAME_SUMMARY_TITLE"] = "par nom";
$MESS["CRM_LEAD_DUP_CTRL_PHONE_SUMMARY_TITLE"] = "par téléphone";
$MESS["CRM_LEAD_FIELD_COMPANY_TITLE"] = "Nom de l’entreprise";
$MESS["CRM_LEAD_TAB_BIZPROC"] = "Flux de travail";
?>