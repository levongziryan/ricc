<?
$MESS["CRM_LEAD_DETAIL_HISTORY_STUB"] = "Vous ajoutez maintenant un lead...";
$MESS["CRM_LEAD_CONV_ACCESS_DENIED"] = "Vous avez besoin de permissions pour créer des contacts, sociétés et transactions afin de finaliser l'opération.";
$MESS["CRM_LEAD_CONV_GENERAL_ERROR"] = "Erreur de conversion générique.";
$MESS["CRM_LEAD_CONV_DIALOG_TITLE"] = "Convertir le lead";
$MESS["CRM_LEAD_CONV_DIALOG_SYNC_LEGEND"] = "Les entités sélectionnées n'ont pas de champs pouvant stocker des données de leads. Choisissez les entités dans lesquelles les champs manquants seront créés pour accueillir toutes les informations disponibles.";
$MESS["CRM_LEAD_CONV_DIALOG_SYNC_FILED_LIST_TITLE"] = "Ces champs seront créés";
$MESS["CRM_LEAD_CONV_DIALOG_SYNC_ENTITY_LIST_TITLE"] = "Sélectionnez les entités dans lesquelles les champs manquants seront créés";
$MESS["CRM_LEAD_CONV_OPEN_ENTITY_SEL"] = "Sélectionner dans la liste...";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_TITLE"] = "Sélectionner un contact et une société";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_CONTACT"] = "Contacts";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_COMPANY"] = "Entreprises";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_SEARCH"] = "Rechercher";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_SEARCH_NO_RESULT"] = "Aucune entrée n'a été trouvée.";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_LAST"] = "Dernier";
$MESS["CRM_LEAD_CONV_DEAL_CATEGORY_DLG_TITLE"] = "Préférences de la transaction";
$MESS["CRM_LEAD_CONV_DEAL_CATEGORY_DLG_FIELD"] = "Pipeline";
$MESS["CRM_LEAD_DETAIL_CONTINUE_BTN"] = "Continuer";
$MESS["CRM_LEAD_DETAIL_CANCEL_BTN"] = "Annuler";
$MESS["CRM_LEAD_CONV_ENTITY_SEL_BTN"] = "Sélectionner";
$MESS["CRM_LEAD_DETAIL_BUTTON_SAVE"] = "Enregistrer";
$MESS["CRM_LEAD_DETAIL_BUTTON_CANCEL"] = "Annuler";
?>