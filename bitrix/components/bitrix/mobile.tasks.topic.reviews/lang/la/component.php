<?
$MESS["MB_TASKS_TASK_TOPIC_REVIEWS_ERR_ACCESS_DENIED_TO_FORUM"] = "No tiene permisos suficientes para ver los comentarios.";
$MESS["MB_TASKS_TASK_TOPIC_REVIEWS_ERR_FORUM_NOT_AVAILABLE"] = "El foro de tarea comentarios #FORUM_ID# no está disponible.";
$MESS["MB_TASKS_TASK_TOPIC_REVIEWS_ERR_TASK_NOT_AVAILABLE"] = "La tarea #TASK_ID # no está disponible.";
$MESS["MB_TASKS_TASK_FORUM_TOPIC_REVIEWS_COMMENT_SONET_NEW_TASK_MESSAGE"] = "Tarea creada";
$MESS["MB_TASKS_TASK_FORUM_TOPIC_REVIEWS_COMMENT_MESSAGE_ADD"] = "Se ha agregado un comentario en la tarea \"#TASK_TITLE#\", texto del comentario: \"#TASK_COMMENT_TEXT#\"";
$MESS["MB_TASKS_TASK_FORUM_TOPIC_REVIEWS_COMMENT_MESSAGE_EDIT"] = "Cambiar comentario agregado a la tarea \" #TASK_TITLE#\" a: \" #TASK_COMMENT_TEXT#.";
?>