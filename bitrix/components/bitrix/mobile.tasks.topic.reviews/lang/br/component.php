<?
$MESS["MB_TASKS_TASK_TOPIC_REVIEWS_ERR_ACCESS_DENIED_TO_FORUM"] = "Permissões insuficientes para visualizar os comentários.";
$MESS["MB_TASKS_TASK_TOPIC_REVIEWS_ERR_FORUM_NOT_AVAILABLE"] = "Os comentários da tarefa #FORUM_ID# não estão disponíveis.";
$MESS["MB_TASKS_TASK_TOPIC_REVIEWS_ERR_TASK_NOT_AVAILABLE"] = "A tarefa ##TASK_ID# não está disponível.";
$MESS["MB_TASKS_TASK_FORUM_TOPIC_REVIEWS_COMMENT_MESSAGE_ADD"] = "Adicionar comentário na tarefa \"#TASK_TITLE#\", comentar o texto:  \"#TASK_COMMENT_TEXT#\"";
$MESS["MB_TASKS_TASK_FORUM_TOPIC_REVIEWS_COMMENT_MESSAGE_EDIT"] = "Alterar um comentário adicionado à tarefa \"#TASK_TITLE#\" to: \"#TASK_COMMENT_TEXT#\"";
$MESS["MB_TASKS_TASK_FORUM_TOPIC_REVIEWS_COMMENT_SONET_NEW_TASK_MESSAGE"] = "Tarefa criada";
?>