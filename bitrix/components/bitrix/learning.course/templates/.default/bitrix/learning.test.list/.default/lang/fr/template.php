<?
$MESS["LEARNING_BTN_START"] = "Commencer";
$MESS["LEARNING_BTN_CONTINUE"] = "Continuer";
$MESS["LEARNING_TEST_NAME"] = "Nom du test";
$MESS["LEARNING_TEST_ATTEMPT_LIMIT"] = "Nombre d'essais";
$MESS["LEARNING_TEST_ATTEMPT_UNLIMITED"] = "Nombre illimité";
$MESS["LEARNING_TEST_TIME_LIMIT"] = "Limite dans le temps";
$MESS["LEARNING_TEST_TIME_LIMIT_UNLIMITED"] = "Sans restrictions";
$MESS["LEARNING_TEST_TIME_LIMIT_MIN"] = "Min.";
$MESS["LEARNING_PASSAGE_TYPE"] = "Type de passation du test";
$MESS["LEARNING_PASSAGE_NO_FOLLOW_NO_EDIT"] = "Il est interdit de passer à la question suivante sans répondre à la question courante, <b>il est impossible</b> de modifier ses réponses.";
$MESS["LEARNING_PASSAGE_FOLLOW_NO_EDIT"] = "Le passage à la question suivante est autorisée sans réponse à la question courante, <b>il est interdit de</b> modifier vos réponses.";
$MESS["LEARNING_PASSAGE_FOLLOW_EDIT"] = "Le passage à la question suivante sans réponse à la question actuelle est permis, <b> il est possible</b> de modifier ses réponses.";
$MESS["LEARNING_PREV_TEST_REQUIRED"] = "Pour avoir l'accès au test il faut passer le test #TEST_LINK# et obtenir #TEST_SCORE#% au minimum du nombre total de points.";
?>