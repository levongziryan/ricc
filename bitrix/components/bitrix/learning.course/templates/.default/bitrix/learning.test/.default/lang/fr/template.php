<?
$MESS["LEARNING_QUESTION_TITLE"] = "Question";
$MESS["LEARNING_QUESTION_OF"] = "Depuis";
$MESS["LEARNING_QUESTION_DESCRIPTION"] = "Description";
$MESS["LEARNING_CHOOSE_ANSWER"] = "Choisissez une réponse";
$MESS["LEARNING_INPUT_ANSWER"] = "Entrer la réponse";
$MESS["LEARNING_BTN_START"] = "Commencer";
$MESS["LEARNING_BTN_NEXT"] = "Suivant";
$MESS["LEARNING_BTN_PREVIOUS"] = "Précédent";
$MESS["LEARNING_BTN_FINISH"] = "Terminer";
$MESS["LEARNING_BTN_CONTINUE"] = "Continuer";
$MESS["LEARNING_BTN_CONFIRM_FINISH"] = "Voulez-vous vraiment terminer ce test?";
$MESS["LEARNING_NO_RESPONSE_CONFIRM"] = "Vous n'avez pas choisir la réponse! ?? - passer à la question suivante. En cela la réponse est considérée incorrecte. Annuler - choisir la réponse à la question.";
$MESS["LEARNING_EMPTY_RESPONSE_CONFIRM"] = "Vous n'avez pas saisi de réponse! OK - passage à la question suivante. La réponse sera considérée comme incorrecte. Annuler - saisir la réponse à la question.";
$MESS["LEARNING_INVALID_SORT_CONFIRM"] = "Un des points n'est pas choisi ou est choisi plus d'une fois! OK - passer à la question suivante. Dans ce cas, la réponse est considérée incorrecte. Annuler - modifier la réponse à la question.";
$MESS["LEARNING_PROFILE"] = "Consulter les résultats du test";
$MESS["LEARNING_QBAR_CURRENT_TITLE"] = "Question courante";
$MESS["LEARNING_QBAR_ANSWERED_TITLE"] = "Question avec réponse";
$MESS["LEARNING_QBAR_NOANSWERED_TITLE"] = "Question sans réponse";
$MESS["LEARNING_QBAR_NEXT_TITLE"] = "Passer à la question suivante";
$MESS["LEARNING_QBAR_NEXT_NOANSWER_TITLE"] = "Passer à la question suivante laissée sans réponse";
$MESS["LEARNING_QBAR_PREVIOUS_TITLE"] = "Retour à la question précédente";
$MESS["LEARNING_QBAR_PREVIOUS_NOANSWER_TITLE"] = "Passer à la question précédente restée sans réponse";
$MESS["LEARNING_TEST_NAME"] = "Nom du test";
$MESS["LEARNING_TEST_ATTEMPT_LIMIT"] = "Nombre d'essais";
$MESS["LEARNING_TEST_ATTEMPT_UNLIMITED"] = "Nombre illimité";
$MESS["LEARNING_TEST_TIME_LIMIT"] = "Limite dans le temps";
$MESS["LEARNING_TEST_TIME_LIMIT_UNLIMITED"] = "Sans restrictions";
$MESS["LEARNING_TEST_TIME_LIMIT_MIN"] = "Min.";
$MESS["LEARNING_PASSAGE_TYPE"] = "Type de passation du test";
$MESS["LEARNING_PASSAGE_NO_FOLLOW_NO_EDIT"] = "Il est interdit de passer à la question suivante sans répondre à la question courante, <b>il est impossible</b> de modifier ses réponses.";
$MESS["LEARNING_PASSAGE_FOLLOW_NO_EDIT"] = "Le passage à la question suivante est autorisée sans réponse à la question courante, <b>il est interdit de</b> modifier vos réponses.";
$MESS["LEARNING_PASSAGE_FOLLOW_EDIT"] = "Le passage à la question suivante sans réponse à la question actuelle est permis, <b> il est possible</b> de modifier ses réponses.";
$MESS["LEARNING_TEST_FAILED"] = "Le test n'est pas passé";
$MESS["LEARNING_TEST_PASSED"] = "Le test est passé";
$MESS["LEARNING_CURRENT_RIGHT_COUNT"] = "Nombre de réponses correctes";
$MESS["LEARNING_CURRENT_MARK"] = "Note actuelle";
$MESS["LEARNING_RESULT_QUESTIONS_COUNT"] = "Nombre de questions";
$MESS["LEARNING_RESULT_RIGHT_COUNT"] = "Nombre de réponses correctes";
$MESS["LEARNING_RESULT_MAX_SCORE"] = "Nombre maximal de points";
$MESS["LEARNING_RESULT_SCORE"] = "Quantité de points reçus";
$MESS["LEARNING_RESULT_MARK"] = "Evaluation";
$MESS["LEARNING_RESULT_MESSAGE"] = "Message";
$MESS["INCORRECT_QUESTION_NAME"] = "Question";
$MESS["INCORRECT_QUESTION_MESSAGE"] = "Erreur";
$MESS["LEARNING_PREV_TEST_REQUIRED"] = "Pour avoir l'accès au test il faut passer le test #TEST_LINK# et obtenir #TEST_SCORE#% au minimum du nombre total de points.";
$MESS["LEARNING_COMPLETED"] = "Exécution du test accomplie.";
?>