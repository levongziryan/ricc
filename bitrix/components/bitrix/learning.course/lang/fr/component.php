<?
$MESS["LEARNING_COURSES_QUEST_M_ADD"] = "Créer  une question (choix multiple)";
$MESS["LEARNING_COURSES_QUEST_S_ADD"] = "Créer  une question (choix unique)";
$MESS["LEARNING_COURSES_QUEST_R_ADD"] = "Créer  une question (triage)";
$MESS["LEARNING_COURSES_QUEST_T_ADD"] = "Créer une question (réponse textuelle)";
$MESS["LEARNING_COURSES_CHAPTER_ADD"] = "Ajouter un nouveau chapitre";
$MESS["LEARNING_COURSES_COURSE_ADD"] = "Ajouter un nouveau cours";
$MESS["LEARNING_COURSES_TEST_ADD"] = "Ajouter un texte";
$MESS["LEARNING_COURSES_LESSON_ADD"] = "Ajouter une leçon";
$MESS["LEARNING_COURSES_CHAPTER_EDIT"] = "Modifier le chapitre";
$MESS["LEARNING_COURSES_COURSE_EDIT"] = "Modifier le cours";
$MESS["LEARNING_COURSES_TEST_EDIT"] = "Modifier le test";
$MESS["LEARNING_COURSES_LESSON_EDIT"] = "Changement de leçon";
$MESS["LEARNING_COURSE_DETAIL"] = "Cours en détails";
$MESS["LEARNING_COURSE_DENIED"] = "Le cours ne peut pas être trouvé ou l'accès à lui est refusé.";
$MESS["INCORRECT_QUESTION_MESSAGE"] = "Erreur";
$MESS["LEARNING_PANEL_CONTROL_PANEL"] = "Administration du système";
$MESS["LEARNING_PANEL_CONTROL_PANEL_ALT"] = "Accéder au panneau de commande";
$MESS["LEARNING_MODULE_NOT_FOUND"] = "Module des blogs non installé.";
?>