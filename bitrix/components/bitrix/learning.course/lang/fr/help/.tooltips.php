<?
$MESS["SEF_URL_TEMPLATES_gradebook_TIP"] = "URL qui mène à la page du journal.";
$MESS["SEF_URL_TEMPLATES_test_TIP"] = "URL, conduisant à la page de test";
$MESS["SEF_MODE_TIP"] = "Cocher cette option active le mode SEF et les champs de configuration d'URL.";
$MESS["CACHE_TIME_TIP"] = "Veuillez indiquer la période de temps durant laquelle la mémoire cache reste valide.";
$MESS["PAGE_NUMBER_VARIABLE_TIP"] = "Identificateur de la question.";
$MESS["VARIABLE_ALIASES_TYPE_TIP"] = "Identifiant de tous les supports de cours.";
$MESS["VARIABLE_ALIASES_CHAPTER_ID_TIP"] = "ID du chapitre.";
$MESS["VARIABLE_ALIASES_GRADEBOOK_TIP"] = "Identifiant du journal.";
$MESS["VARIABLE_ALIASES_INDEX_TIP"] = "L'identificateur de page d'index.";
$MESS["COURSE_ID_TIP"] = "Sélectionnez ici l'un des cours existants. Si vous sélectionnez <b><i>(autre)</i></b>, vous devrez spécifier l'ID de cours dans le domaine de côté.";
$MESS["VARIABLE_ALIASES_COURSE_ID_TIP"] = "Identificateur du cours.";
$MESS["VARIABLE_ALIASES_TEST_LIST_TIP"] = ".Identificateur de la liste de tests";
$MESS["VARIABLE_ALIASES_TEST_ID_TIP"] = "Identificateur du texte.";
$MESS["VARIABLE_ALIASES_FOR_TEST_ID_TIP"] = "Identificateur du test dans le journal.";
$MESS["VARIABLE_ALIASES_SELF_TEST_ID_TIP"] = "Identifiant du test pour l'auto-contrôle.";
$MESS["VARIABLE_ALIASES_LESSON_ID_TIP"] = "Identifiant de la leçon.";
$MESS["SEF_FOLDER_TIP"] = "Catalogue de la commande numérique (par rapport à la racine du site).";
$MESS["PAGE_WINDOW_TIP"] = "Quantité de questions affichées dans la chaîne de navigation.";
$MESS["TESTS_PER_PAGE_TIP"] = "Le nombre maximum de tests qui peuvent être affichés sur une page. D'autres tests seront disponibles en utilisant le fil d'ariane.";
$MESS["SHOW_TIME_LIMIT_TIP"] = "Afficher le compteur de la limite du temps";
$MESS["CHECK_PERMISSIONS_TIP"] = "Au cas où la valeur 'Oui' soit choisie, alors le droit d'accès sera vérifié.";
$MESS["CACHE_TYPE_TIP"] = "<i>Auto</i>: cache est valide pendant un temps prédéterminé dans les paramètres;<br /><i>Mettre en cache</i>: pour la mise en cache il faut déterminer seulement le temps de la mise en cache;<br /><i>Ne pas mettre en cache</i>: pas de mise en cache en tout cas.";
$MESS["SET_TITLE_TIP"] = "Installer l'en-tête de la page.";
?>