<?
$MESS["WDUF_PICKUP_ATTACHMENTS"] = "Seleccione el archivo de este computador";
$MESS["WDUF_ATTACHED_TO_MESSAGE"] = "El archivo se ha adjuntado al mensaje.";
$MESS["WD_SAVED_PATH"] = "Guardado";
$MESS["WD_LOCAL_COPY_ONLY"] = "En este mensaje";
$MESS["WD_MY_LIBRARY"] = "Mi drive";
$MESS["SONET_GROUP_PREFIX"] = "Grupo:";
$MESS["WDUF_FILE_DOWNLOAD"] = "Descargar";
$MESS["WDUF_FILE_PREVIEW"] = "Vista previa";
$MESS["WDUF_FILE_REVISION_HISTORY"] = "Revisión del historial";
$MESS["WDUF_FILE_ONLINE_EDIT_IN_SERVICE"] = "Actualmente está siendo editada en #SERVICE#";
$MESS["WDUF_FILE_EDIT"] = "Editar";
$MESS["WDUF_HISTORY_FILE"] = "Versión #NUMBER#:";
$MESS["WDUF_FILE_DOWNLOAD_ARCHIVE"] = "Descargar todos las carpetas como archivos";
?>