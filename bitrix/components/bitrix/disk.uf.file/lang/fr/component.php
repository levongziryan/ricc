<?
$MESS["WDUF_PICKUP_ATTACHMENTS"] = "Sélectionner un fichier de cet ordinateur";
$MESS["WDUF_ATTACHED_TO_MESSAGE"] = "Le fichier joint au message";
$MESS["WD_SAVED_PATH"] = "Enregistré";
$MESS["WD_LOCAL_COPY_ONLY"] = "Dans ce message";
$MESS["WD_MY_LIBRARY"] = "Mon Drive";
$MESS["SONET_GROUP_PREFIX"] = "Groupe:";
$MESS["WDUF_FILE_DOWNLOAD"] = "Télécharger";
$MESS["WDUF_FILE_PREVIEW"] = "Affichage";
$MESS["WDUF_FILE_REVISION_HISTORY"] = "Historique des modifications";
$MESS["WDUF_FILE_ONLINE_EDIT_IN_SERVICE"] = "en cours de modification dans #SERVICE#";
$MESS["WDUF_FILE_EDIT"] = "Editer";
$MESS["WDUF_HISTORY_FILE"] = "Version #NUMBER#:";
$MESS["WDUF_FILE_DOWNLOAD_ARCHIVE"] = "Télécharger tous les fichiers comme archive";
?>