<?
$MESS["WD_FILE_EXISTS"] = "Já existe um arquivo com este nome. Você ainda pode usar a pasta atual. Neste caso, a versão existente do documento será salva no histórico.";
$MESS["WDUF_SELECT_ATTACHMENTS"] = "Carregar arquivos e imagens";
$MESS["WDUF_DROP_ATTACHMENTS"] = "Arrastar os anexos aqui";
$MESS["WD_SELECT_FILE_LINK"] = "Selecionar documento do Bitrix24";
$MESS["WD_SELECT_FILE_LINK_ALT"] = "Bibliotecas disponíveis";
$MESS["WDUF_CREATE_DOCX"] = "Documento";
$MESS["WDUF_CREATE_XLSX"] = "Planilha";
$MESS["WDUF_CREATE_PPTX"] = "Apresentação";
$MESS["WDUF_CREATE_IN_SERVICE"] = "Criar usando #SERVICE#";
$MESS["WD_ACCESS_DENIED"] = "Acesso negado.";
$MESS["WD_FILE_LOADING"] = "Carregando";
$MESS["DISK_UF_FILE_CLOUD_IMPORT_TITLE"] = "Baixe da unidade externa";
$MESS["DISK_UF_FILE_CLOUD_IMPORT_TITLE_SERVICE_ONEDRIVE"] = "OneDrive";
$MESS["DISK_UF_FILE_CLOUD_IMPORT_TITLE_SERVICE_GDRIVE"] = "Google Drive";
$MESS["DISK_UF_FILE_CLOUD_IMPORT_TITLE_SERVICE_DROPBOX"] = "Dropbox";
$MESS["DISK_UF_FILE_CLOUD_IMPORT_TITLE_SERVICE_OFFICE365"] = "Office 365";
?>