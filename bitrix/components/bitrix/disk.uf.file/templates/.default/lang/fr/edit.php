<?
$MESS["WDUF_UPLOAD_DOCUMENT"] = "Charger les documents";
$MESS["WD_FILE_LOADING"] = "Chargement";
$MESS["WDUF_ATTACHMENTS"] = "Images et fichiers joints";
$MESS["WDUF_FILE_EDIT_BY_DESTINATION_USERS"] = "Autoriser l'édition des documents par les destinataires du message?";
?>