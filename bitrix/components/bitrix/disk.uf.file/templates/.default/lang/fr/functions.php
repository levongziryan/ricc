<?
$MESS["WD_FILE_EXISTS"] = "Le fichier du même nom existe déjà. Vous pouvez choisir le dossier en cours, dans ce cas l'ancienne version du fichier sera sauvegardée dans l'historique du document.";
$MESS["WDUF_SELECT_ATTACHMENTS"] = "Télécharger le fichier ou l'image";
$MESS["WDUF_DROP_ATTACHMENTS"] = "Déplacer à l'aide du drag'n'drop";
$MESS["WD_SELECT_FILE_LINK"] = "Trouver dans Bitrix24";
$MESS["WD_SELECT_FILE_LINK_ALT"] = "Bibliothèques disponibles";
$MESS["WDUF_CREATE_DOCX"] = "Document";
$MESS["WDUF_CREATE_XLSX"] = "Table";
$MESS["WDUF_CREATE_PPTX"] = "Présentation";
$MESS["WDUF_CREATE_IN_SERVICE"] = "Créer à l'aide de #SERVICE#";
$MESS["WD_ACCESS_DENIED"] = "Accès interdit.";
$MESS["WD_FILE_LOADING"] = "Chargement";
$MESS["DISK_UF_FILE_CLOUD_IMPORT_TITLE"] = "Télécharger depuis un lecteur externe";
$MESS["DISK_UF_FILE_CLOUD_IMPORT_TITLE_SERVICE_ONEDRIVE"] = "OneDrive";
$MESS["DISK_UF_FILE_CLOUD_IMPORT_TITLE_SERVICE_GDRIVE"] = "Google Drive";
$MESS["DISK_UF_FILE_CLOUD_IMPORT_TITLE_SERVICE_DROPBOX"] = "Dropbox";
$MESS["DISK_UF_FILE_CLOUD_IMPORT_TITLE_SERVICE_OFFICE365"] = "Office 365";
?>