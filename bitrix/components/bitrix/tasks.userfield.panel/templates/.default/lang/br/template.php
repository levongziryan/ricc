<?
$MESS["TASKS_TUFP_FIELD_MANDATORY"] = "Obrigatório";
$MESS["TASKS_TUFP_FIELD_MULTIPLE"] = "Vários";
$MESS["TASKS_TUFP_FIELD_MULTIPLE_HINT"] = "Esta propriedade pode ser selecionada apenas ao criar um novo campo";
$MESS["TASKS_TUFP_FIELD_ADD"] = "Adicionar campo";
$MESS["TASKS_TUFP_FIELD_UN_HIDE"] = "Mostrar campo";
$MESS["TASKS_TUFP_FIELD_HIDE"] = "Ocultar campo";
$MESS["TASKS_TUFP_FIELD_EDIT"] = "Editar campo";
$MESS["TASKS_TUFP_SHOW_DETAILS"] = "Informações";
$MESS["TASKS_TUFP_LICENSE_RESTRICTED_MANDATORY"] = "Os campos obrigatórios estão disponíveis no plano \"Professional\".";
$MESS["TASKS_TUFP_LICENSE_RESTRICTED"] = "Os campos personalizados estão disponíveis em Bitrix24 Standard e Professional.";
$MESS["TASKS_TUFP_NO_FIELDS_TO_SHOW"] = "Não há nenhum campo personalizado a exibir";
?>