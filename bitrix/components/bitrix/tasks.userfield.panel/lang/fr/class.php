<?
$MESS["TASKS_TUFE_EMPTY_LABEL"] = "Le nom du champ n'est pas spécifié";
$MESS["TASKS_TUFE_UF_USAGE_RESTRICTED"] = "Vous ne pouvez pas utiliser les champs personnalisés avec votre abonnement.";
$MESS["TASKS_TUFE_UF_MANAGING_RESTRICTED"] = "Vous ne pouvez pas gérer les champs personnalisés avec votre abonnement.";
$MESS["TASKS_TUFE_UF_USAGE_RESTRICTED_MANDATORY"] = "Impossible de créer un champ requis.";
?>