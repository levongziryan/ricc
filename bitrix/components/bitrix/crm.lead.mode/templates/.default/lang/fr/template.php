<?
$MESS["CRM_TYPE_TITLE"] = "Choisissez comment vous voulez travailler avec votre GRC";
$MESS["CRM_TYPE_SAVE"] = "Enregistrer";
$MESS["CRM_TYPE_CANCEL"] = "Annuler";
$MESS["CRM_TYPE_POPUP_TITLE"] = "Piste est le nom donné à quiconque pouvant potentiellement devenir un client.<br/>Il existe deux manières de travailler avec la GRC Bitrix24 :";
$MESS["CRM_TYPE_SIMPLE"] = "GRC simple";
$MESS["CRM_TYPE_SIMPLE_DESC"] = "Transactions + contacts (pas de piste)";
$MESS["CRM_TYPE_SIMPLE_DESC2"] = "Recommandé pour les services des ventes des petites entreprises. Tous les e-mails, appels, demandes et chats entrants deviennent immédiatement des transactions et/ou des contacts.";
$MESS["CRM_TYPE_CLASSIC"] = "GRC classique";
$MESS["CRM_TYPE_CLASSIC_DESC"] = "Pistes &rarr; transactions + contacts";
$MESS["CRM_TYPE_CLASSIC_DESC2"] = "Recommandé pour les entreprises de taille moyenne à grande. Tout d'abord, toutes les demandes reçues deviennent des pistes. Ensuite, les équipes de vente essaient de convertir les pistes en transactions et contacts.";
$MESS["CRM_TYPE_MORE"] = "En apprendre plus";
$MESS["CRM_TYPE_CHANGE"] = "Vous pouvez modifier votre préférence à tout moment à partir des paramètres GRC";
$MESS["CRM_TYPE_POPUP_TITLE_LEADS_DISABLED"] = "Vous utilisez actuellement le mode \"GRC simple\" qui permet de <br/>transformer immédiatement les requêtes des nouveaux clients en transactions.<br/>Deux options sont possibles :";
$MESS["CRM_RIGTHS_INFO"] = "Veuillez contacter votre administrateur pour changer de mode GRC";
?>