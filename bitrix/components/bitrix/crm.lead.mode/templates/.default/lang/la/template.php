<?
$MESS["CRM_TYPE_TITLE"] = "Elija la forma en que desea trabajar con su CRM";
$MESS["CRM_TYPE_SAVE"] = "Guardar";
$MESS["CRM_TYPE_CANCEL"] = "Cancelar";
$MESS["CRM_TYPE_POPUP_TITLE"] = "Un prospecto o cualquier persona que potencialmente pueda convertirse en un cliente.<br/>Hay dos formas de trabajar con Bitrix24 CRM:";
$MESS["CRM_TYPE_POPUP_TITLE_LEADS_DISABLED"] = "Actualmente está utilizando el modo \"CRM Simple\", donde<br/>las solicitudes de nuevos clientes se convierten inmediatamente en negociaciones.<br/>Dos opciones son posibles:";
$MESS["CRM_TYPE_SIMPLE"] = "CRM Simple";
$MESS["CRM_TYPE_SIMPLE_DESC"] = "Negociación + contactos (sin prospectos)";
$MESS["CRM_TYPE_SIMPLE_DESC2"] = "Recomendado para los departamentos de ventas de pequeñas empresas. Todos los correos electrónicos entrantes, llamadas, solicitudes y chats se convierten inmediatamente en negociaciones y/o contactos.";
$MESS["CRM_TYPE_CLASSIC"] = "CRM clásico";
$MESS["CRM_TYPE_CLASSIC_DESC"] = "Prospectos; negociaciones + contactos";
$MESS["CRM_TYPE_CLASSIC_DESC2"] = "Recomendado para empresas medianas y grandes. En primer lugar, todas las consultas entrantes se convierten en prospectos. Que su equipo de ventas trate de convertir prospectos a negociones y contactos.";
$MESS["CRM_TYPE_MORE"] = "Más información";
$MESS["CRM_TYPE_CHANGE"] = "Puedes cambiar tus preferencias en cualquier momento en la configuración del CRM";
$MESS["CRM_TYPE_HELPER_ARTICLE_ID"] = "5332541";
$MESS["CRM_RIGTHS_INFO"] = "Póngase en contacto con su administrador para cambiar el modo CRM";
?>