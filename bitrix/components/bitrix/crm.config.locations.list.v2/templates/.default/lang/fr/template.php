<?
$MESS["CRM_CLL2_EDIT_TITLE"] = "Passer sur la page d'édition de cet emplacement";
$MESS["CRM_CLL2_EDIT"] = "Editer la localisation";
$MESS["CRM_CLL2_DELETE_TITLE"] = "Supprimer ce lieu";
$MESS["CRM_CLL2_DELETE"] = "Annuler la localisation";
$MESS["CRM_CLL2_DELETE_CONFIRM"] = "Tes-vous sûr de vouloir supprimer '%s'?";
$MESS["CRM_CLL2_ALL"] = "Total";
$MESS["CRM_CLL2_VIEW_SUBTREE_TITLE"] = "Voir les emplacements de l'enfant";
$MESS["CRM_CLL2_VIEW_SUBTREE"] = "Voir les emplacements de l'enfant";
$MESS["CRM_CLL2_NOT_SELECTED"] = "Pas sélectionné";
?>