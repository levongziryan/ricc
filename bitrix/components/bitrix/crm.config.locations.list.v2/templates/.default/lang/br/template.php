<?
$MESS["CRM_CLL2_NOT_SELECTED"] = "Não selecionado";
$MESS["CRM_CLL2_EDIT_TITLE"] = "Abrir esta localização para edição";
$MESS["CRM_CLL2_EDIT"] = "Editar localização";
$MESS["CRM_CLL2_DELETE_TITLE"] = "Excluir esta localização";
$MESS["CRM_CLL2_DELETE"] = "Excluir localização";
$MESS["CRM_CLL2_DELETE_CONFIRM"] = "Você tem certeza de que deseja excluir '%s'?";
$MESS["CRM_CLL2_ALL"] = "Total";
$MESS["CRM_CLL2_VIEW_SUBTREE_TITLE"] = "Ver localizações secundárias";
$MESS["CRM_CLL2_VIEW_SUBTREE"] = "Ver localizações secundárias";
?>