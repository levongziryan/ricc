<?
$MESS["MB_CALENDAR_SAVE_BUT"] = "Salvar";
$MESS["MBCAL_EDEV_NEW"] = "Novo Evento";
$MESS["MBCAL_EDEV_EDIT"] = "Editar evento";
$MESS["MBCAL_EDEV_NAME"] = "Nome do evento";
$MESS["MBCAL_EDEV_NAME_DEF"] = "Novo evento";
$MESS["MBCAL_EDEV_ALLDAY"] = "Todos os dias";
$MESS["MBCAL_EDEV_FROM"] = "Começar";
$MESS["MBCAL_EDEV_TO"] = "Final";
$MESS["MBCAL_EDEV_DESC"] = "Descrição";
$MESS["MBCAL_EDEV_ADD_ATTENDEES_TITLE"] = "Participantes";
$MESS["MBCAL_EDEV_ADD_ATTENDEES"] = "Adicionar participantes";
$MESS["MBCAL_EDEV_PRIVATE"] = "Evento privado";
$MESS["MBCAL_EDEV_PRIV_NOTICE"] = "Só você vai ver os detalhes do evento";
$MESS["MBCAL_EDEV_YES"] = "Sim";
$MESS["MBCAL_EDEV_NO"] = "Não";
$MESS["MBCAL_EDEV_LOCATION"] = "Localização";
$MESS["MBCAL_EDEV_ACC"] = "Disponibilidade";
$MESS["MBCAL_EDEV_ACC_ABSENT"] = "Ausente";
$MESS["MBCAL_EDEV_ACC_BUSY"] = "Ocupado";
$MESS["MBCAL_EDEV_ACC_QUEST"] = "Inseguro";
$MESS["MBCAL_EDEV_ACC_FREE"] = "Disponível";
$MESS["MBCAL_EDEV_IMP"] = "Prioridade";
$MESS["MBCAL_EDEV_IMP_HIGH"] = "Alto";
$MESS["MBCAL_EDEV_IMP_NORMAL"] = "Normal";
$MESS["MBCAL_EDEV_IMP_LOW"] = "Baixo";
$MESS["MBCAL_EDEV_SECTION"] = "Calendário";
$MESS["MBCAL_EDEV_REMIMDER"] = "Lembrar";
$MESS["MBCAL_EDEV_REM_NO"] = "Não";
$MESS["MBCAL_EDEV_REM_MIN"] = "Em #N# min.";
$MESS["MBCAL_EDEV_REM_HOUR1"] = "Em 1 hora";
$MESS["MBCAL_EDEV_REM_HOURS"] = "Em #N# horas";
$MESS["MBCAL_EDEV_REM_DAY"] = "1 dia antes";
$MESS["MBCAL_EDEV_REM_DAYS"] = "#N# dias antes";
$MESS["MBCAL_EDEV_OK"] = "Ok";
$MESS["MBCAL_EDEV_SELECT"] = "Selecionar";
$MESS["MBCAL_EDEV_CANCEL"] = "Cancelar";
$MESS["MBCAL_EDEV_REMOVE"] = "Excluir";
$MESS["MBCAL_EDEV_NO_WORK_POS"] = "não especificados";
$MESS["MBCAL_EDEV_DEL_EVENT_CONFIRM_TITLE"] = "deletar evento";
$MESS["MBCAL_EDEV_DEL_EVENT_CONFIRM"] = "Tem certeza de que deseja excluir este evento?";
$MESS["PULL_TEXT"] = "Puxe para atualizar";
$MESS["DOWN_TEXT"] = "Solte para atualizar";
$MESS["LOAD_TEXT"] = "atualizando ...";
$MESS["MBCAL_REPEAT_CLEAR"] = "Cancelar tarefa recorrente";
$MESS["MBCAL_REPEAT_TITLE"] = "Repetir";
$MESS["MBCAL_VIEWEV_MO"] = "Mo";
$MESS["MBCAL_VIEWEV_TU"] = "Tu";
$MESS["MBCAL_VIEWEV_WE"] = "Nós";
$MESS["MBCAL_VIEWEV_TH"] = "Th";
$MESS["MBCAL_VIEWEV_FR"] = "sex";
$MESS["MBCAL_VIEWEV_SA"] = "Sa";
$MESS["MBCAL_VIEWEV_SU"] = "Su";
$MESS["EC_JS_TO_"] = "a";
$MESS["EC_JS_FROM_"] = "de";
$MESS["EC_JS_EVERY_M"] = "cada";
$MESS["EC_JS_EVERY_F"] = "cada";
$MESS["EC_JS_EVERY_M_"] = "cada";
$MESS["EC_JS_EVERY_F_"] = "cada";
$MESS["EC_JS_EVERY_N"] = "cada";
$MESS["EC_JS_EVERY_N_"] = "cada";
$MESS["EC_JS_WEEK_P"] = "semana";
$MESS["EC_JS_DAY_P"] = "dia";
$MESS["EC_JS_MONTH_P"] = "mês";
$MESS["EC_JS_YEAR_P"] = "ano";
$MESS["EC_JS_DATE_P_"] = "data";
$MESS["EC_JS_MONTH_P_"] = "mês";
$MESS["MB_CAL_EVENT_DATE_FORMAT"] = "D., M. d, Y";
$MESS["MB_CAL_EVENT_TIME_FORMAT_AMPM"] = "h: i uma";
$MESS["MB_CAL_EVENT_TIME_FORMAT"] = "G: i";
?>