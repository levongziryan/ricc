<?
$MESS["SONET_LOG_EXTRANET_SUFFIX"] = "(extranet)";
$MESS["SUBSCRIBE_CB_ALL_1"] = "auteur #TITLE#";
$MESS["SUBSCRIBE_CB_ALL_2"] = "Tous les événements de l'auteur #TITLE#";
$MESS["SUBSCRIBE_CB_ALL"] = "Tous les événements de cet utilisateur";
$MESS["SUBSCRIBE_TRANSPORT_DIGEST"] = "Digest journalier";
$MESS["SUBSCRIBE_TRANSPORT_DIGEST_WEEK"] = "Digest hebdomadaire";
$MESS["SUBSCRIBE_TRANSPORT_XMPP"] = "Message instantané";
$MESS["SUBSCRIBE_TRANSPORT_NONE"] = "Ne pas notifier";
$MESS["SUBSCRIBE_TRANSPORT_MAIL"] = "Informer par e-mail";
?>