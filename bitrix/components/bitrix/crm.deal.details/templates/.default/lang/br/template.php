<?
$MESS["CRM_DEAL_DETAIL_HISTORY_STUB"] = "Você está adicionando uma venda agora...";
$MESS["CRM_DEAL_CONV_ACCESS_DENIED"] = "Você precisa de permissão para criar contatos, empresas e vendas para concluir a operação.";
$MESS["CRM_DEAL_CONV_GENERAL_ERROR"] = "Erro de conversão genérico.";
$MESS["CRM_DEAL_CONV_DIALOG_TITLE"] = "Gerar entidade com base na venda";
$MESS["CRM_DEAL_CONV_DIALOG_CONTINUE_BTN"] = "Continuar";
$MESS["CRM_DEAL_CONV_DIALOG_CANCEL_BTN"] = "Cancelar";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_LEGEND"] = "As entidades selecionadas não têm campos que podem armazenar dados de venda. Selecione entidades em que campos faltantes serão criados para acomodar todas as informações disponíveis.";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_FILED_LIST_TITLE"] = "Estes campos serão criados";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_ENTITY_LIST_TITLE"] = "Selecionar as entidades nas quais os campos adicionais serão criados";
?>