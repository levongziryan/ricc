<?
$MESS["INAF_F_ID"] = "ID";
$MESS["INAF_F_NAME"] = "Nombre";
$MESS["INAF_F_DESCRIPTION"] = "Descripción";
$MESS["INAF_F_FLOOR"] = "Piso";
$MESS["INAF_F_PLACE"] = "Capacidad";
$MESS["INAF_F_PHONE"] = "Teléfono";
$MESS["INTASK_C29_UF_PERSONS"] = "Personas";
$MESS["INTASK_C29_UF_RES_TYPE"] = "Tipo de sesión";
$MESS["INTASK_C29_UF_RES_TYPEA"] = "Consulta";
$MESS["INTASK_C29_UF_RES_TYPEB"] = "Presentación";
$MESS["INTASK_C29_UF_RES_TYPEC"] = "Negociación";
$MESS["INTASK_C29_UF_RES_TYPED"] = "Otro";
$MESS["INTASK_C29_UF_PREPARE_ROOM"] = "Sala de reuniones de preparación";
$MESS["INTASK_C29_PERIOD_TYPE"] = "Tipo de periodo";
$MESS["INTASK_C29_PERIOD_COUNT"] = "Frecuencia";
$MESS["INTASK_C29_EVENT_LENGTH"] = "Duración";
$MESS["INTASK_C29_PERIOD_ADDITIONAL"] = "Adicional";
?>