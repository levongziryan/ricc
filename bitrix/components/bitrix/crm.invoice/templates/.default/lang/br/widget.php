<?
$MESS["CRM_INVOICE_WGT_PAGE_TITLE"] = "Relatórios analíticos da fatura";
$MESS["CRM_INVOICE_WGT_FUNNEL"] = "Canal da fatura";
$MESS["CRM_INVOICE_WGT_SUM_INVOICE_IN_WORK"] = "Valor total das faturas ativas";
$MESS["CRM_INVOICE_WGT_SUM_INVOICE_SUCCESSFUL"] = "Valor total das faturas pagas";
$MESS["CRM_INVOICE_WGT_SUM_INVOICE_OWED"] = "Valor total das faturas pendentes";
$MESS["CRM_INVOICE_WGT_QTY_INVOICE_OVERDUE"] = "Número de faturas vencidas";
$MESS["CRM_INVOICE_WGT_SUM_INVOICE_OVERDUE"] = "Valor total das faturas vencidas";
$MESS["CRM_INVOICE_WGT_DEAL_PAYMENT_CONTROL"] = "Controle de pagamento das vendas ganhas";
$MESS["CRM_INVOICE_WGT_SUM_DEAL_INVOICE_OVERALL"] = "Valor das faturas criadas";
$MESS["CRM_INVOICE_WGT_SUM_DEAL_INVOICE_OWED"] = "Valor das faturas incriadas";
$MESS["CRM_INVOICE_WGT_INVOCE_PAYMENT"] = "Débitos e pagamentos";
$MESS["CRM_INVOICE_WGT_RATING"] = "Classificação das faturas pagas";
$MESS["CRM_INVOICE_WGT_INVOCE_MANAGER"] = "Eficiência no manuseio da fatura dos empregados";
$MESS["CRM_INVOICE_WGT_DEMO_TITLE"] = "Este é um relatório de demonstração. Oculte-o para acessar a análise de suas faturas.";
$MESS["CRM_INVOICE_WGT_DEMO_CONTENT"] = "Se você ainda não tiver faturas, <a href=\"#URL#\" class=\"#CLASS_NAME#\">crie uma</a> agora mesmo!";
?>