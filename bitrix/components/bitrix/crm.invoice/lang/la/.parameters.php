<?
$MESS["CRM_INVOICE_VAR"] = "ID de la factura y nombre de la variable";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "Plantilla de ruta a la página principal";
$MESS["CRM_SEF_PATH_TO_LIST"] = "Plantilla de ruta a la página de la lista de facturas";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "Plantilla de ruta a la página de edición de facturas";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "Plantilla de ruta a la página de vista de facturas";
$MESS["CRM_SEF_PATH_TO_PAYMENT"] = "Plantilla de ruta a la página de pago de facturas";
$MESS["CRM_ELEMENT_ID"] = "ID de la factura";
$MESS["CRM_NAME_TEMPLATE"] = "Forrmato de nombre";
?>