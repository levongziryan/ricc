<?
$MESS["CRM_INVOICE_VAR"] = "Nome da variável ID da fatura";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "Caminho para o template da página de índice";
$MESS["CRM_SEF_PATH_TO_LIST"] = "Caminho para o template da página de lista de faturas";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "Caminho para o template da página de edição de faturas";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "Caminho para o template da página de visualização de faturas";
$MESS["CRM_SEF_PATH_TO_PAYMENT"] = "Caminho para o template da página de pagamento de faturas";
$MESS["CRM_ELEMENT_ID"] = "ID da fatura";
$MESS["CRM_NAME_TEMPLATE"] = "Formato do nome";
?>