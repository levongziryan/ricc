<?
$MESS["CRM_INVOICE_VAR"] = "Le nom de la variable de l'identificateur de la facture";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "Modèle de chemin d'accès à la page principale";
$MESS["CRM_SEF_PATH_TO_LIST"] = "Modèle de chemin d'accès à la page de la liste des factures";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "Modèle de chemin d'accès à la page d'édition d'une facture";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "Modèle de chemin d'accès à la page de visualisation de la facture";
$MESS["CRM_SEF_PATH_TO_PAYMENT"] = "Modèle de chemin d'accès à la page de règlement d'une facture";
$MESS["CRM_ELEMENT_ID"] = "ID du facture";
$MESS["CRM_NAME_TEMPLATE"] = "Format du nom";
?>