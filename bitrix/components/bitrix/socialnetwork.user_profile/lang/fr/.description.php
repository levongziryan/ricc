<?
$MESS["SONET_DEFAULT_TEMPLATE_DESCRIPTION"] = "Permet d'afficher et d'éditer le profil de l'utilisateur.";
$MESS["SONET_DEFAULT_TEMPLATE_NAME"] = "Profil de l'utilisateur";
$MESS["SONET_NAME"] = "Réseau social";
$MESS["SONET_UP_TEMPLATE_NAME"] = "Vue de profil utilisateur";
$MESS["SONET_UP_TEMPLATE_DESCRIPTION"] = "Un composant pour afficher un profil utilisateur";
?>