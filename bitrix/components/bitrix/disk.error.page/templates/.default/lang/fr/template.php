<?
$MESS["DISK_ERROR_PAGE_TITLE"] = "Rien n'a été trouvé.";
$MESS["DISK_ERROR_PAGE_BASE_DESCRIPTION"] = "Je n'ai pu obtenir l'information que vous avez demandée. Il est possible que vous ne disposiez pas des permissions d'accès ou que le lien soit cassé.";
$MESS["DISK_ERROR_PAGE_BASE_SOLUTION_1"] = "- Essayez de vérifier si le lien ne contient pas d'erreur, ou contactez l'auteur.";
$MESS["DISK_ERROR_PAGE_BASE_SOLUTION_2"] = "- Cliquez sur Retour dans votre navigateur pour retourner à la page précédente.";
?>