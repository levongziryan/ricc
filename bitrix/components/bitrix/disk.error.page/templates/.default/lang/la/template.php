<?
$MESS["DISK_ERROR_PAGE_TITLE"] = "No se encontró nada.";
$MESS["DISK_ERROR_PAGE_BASE_DESCRIPTION"] = "No puede obtener la información solicitada. Es posible que usted no tenga permisos de acceso o el enlace está dañado.";
$MESS["DISK_ERROR_PAGE_BASE_SOLUTION_1"] = "- Intente comprobar los errores del enlace o póngase en contacto con el autor.";
$MESS["DISK_ERROR_PAGE_BASE_SOLUTION_2"] = "- Haga clic Atrás en su navegador para volver a la página anterior.";
?>