<?
$MESS["TASKS_MODULE_NOT_AVAILABLE_IN_THIS_EDITION"] = "O módulo de Tarefas não está disponível nesta edição.";
$MESS["MB_TASKS_TASK_EDIT_TASK_NOT_ACCESSIBLE"] = "A tarefa ##TASK_ID# não existe ou você não tem permissões suficientes.";
?>