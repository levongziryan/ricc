<?
$MESS["TASKS_MODULE_NOT_AVAILABLE_IN_THIS_EDITION"] = "El módulo de tareas está disponible en esta edición.";
$MESS["MB_TASKS_TASK_EDIT_TASK_NOT_ACCESSIBLE"] = "La tarea ##TASK_ID#  no existe o no tiene permisos suficientes.";
?>