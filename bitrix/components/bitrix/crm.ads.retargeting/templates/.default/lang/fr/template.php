<?
$MESS["CRM_ADS_RTG_REFRESH"] = "Mettre à jour";
$MESS["CRM_ADS_RTG_REFRESH_TEXT"] = "Mettre à jour l'audience cible.";
$MESS["CRM_ADS_RTG_REFRESH_TEXT_YANDEX"] = "Mettre à jour les segments cible.";
$MESS["CRM_ADS_RTG_ERROR_ACTION"] = "L'action a été annulée à cause d'une erreur.";
$MESS["CRM_ADS_RTG_CLOSE"] = "Fermer";
$MESS["CRM_ADS_RTG_APPLY"] = "Exécuter";
$MESS["CRM_ADS_RTG_CANCEL"] = "Annuler";
$MESS["CRM_ADS_RTG_LOGIN"] = "Connecter";
$MESS["CRM_ADS_RTG_LOGOUT"] = "Déconnecter";
$MESS["CRM_ADS_RTG_CABINET_FACEBOOK"] = "Compte publicitaire Facebook";
$MESS["CRM_ADS_RTG_CABINET_YANDEX"] = "Compte publicitaire Yandex.Audience";
$MESS["CRM_ADS_RTG_CABINET_VKONTAKTE"] = "Compte publicitaire VK";
$MESS["CRM_ADS_RTG_CABINET_GOOGLE"] = "Compte publicitaire Google AdWords";
$MESS["CRM_ADS_RTG_TITLE"] = "Configurer l'audience cible";
$MESS["CRM_ADS_RTG_SELECT_ACCOUNT"] = "Sélectionner le compte publicitaire";
$MESS["CRM_ADS_RTG_SELECT_AUDIENCE"] = "Ajouter à l'audience";
$MESS["CRM_ADS_RTG_SELECT_CONTACT_DATA"] = "Ajouter le téléphone et l'e-mail aux segments";
$MESS["CRM_ADS_RTG_SELECT_CONTACT_DATA_EMAIL"] = "e-mail";
$MESS["CRM_ADS_RTG_SELECT_CONTACT_DATA_PHONE"] = "téléphone";
$MESS["CRM_ADS_RTG_ERROR_NO_AUDIENCES"] = "Aucune audience trouvée. Veuillez vous rendre sur %name% pour créer une audience.";
$MESS["CRM_ADS_RTG_AUTO_REMOVE_TITLE"] = "supprimer de l'audience dans";
$MESS["CRM_ADS_RTG_AUTO_REMOVE_TITLE_YANDEX"] = "supprimer des segments dans";
$MESS["CRM_ADS_RTG_AUTO_REMOVE_DAYS"] = "jours";
?>