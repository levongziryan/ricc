<?
$MESS["TASKS_SIDEBAR_TASK_REPEATS"] = "A tarefa se repete";
$MESS["TASKS_SIDEBAR_TEMPLATE_NOT_ACCESSIBLE"] = "Não é possível visualizar um modelo de tarefa recorrente";
$MESS["TASKS_TWRV_ENABLE_REPLICATION"] = "Retomar tarefa recorrente";
$MESS["TASKS_TWRV_DISABLE_REPLICATION"] = "Suspender tarefa recorrente";
?>