<?
$MESS["TASKS_SIDEBAR_TASK_REPEATS"] = "La tâche est répétée";
$MESS["TASKS_SIDEBAR_TEMPLATE_NOT_ACCESSIBLE"] = "Impossible d'afficher le modèle de tâche récurrente";
$MESS["TASKS_TWRV_ENABLE_REPLICATION"] = "Reprendre la tâche récurrente";
$MESS["TASKS_TWRV_DISABLE_REPLICATION"] = "Suspendre la tâche récurrente";
?>