<?
$MESS["CRM_TAB_CONFIG"] = "Envió de email";
$MESS["CRM_TAB_CONFIG_TITLE"] = "Configurar el email enviando parámetros";
$MESS["CRM_TAB_ACTIVITY_CONFIG"] = "Actividad";
$MESS["CRM_TAB_ACTIVITY_CONFIG_TITLE"] = "Parámetros de actividad";
$MESS["CRM_TAB_FORMAT"] = "Formato";
$MESS["CRM_TAB_FORMAT_TITLE"] = "Parámetros de formato";
$MESS["CRM_TAB_INVNUM"] = "Número de factura";
$MESS["CRM_TAB_INVNUM_TITLE"] = "Plantilla del número de factura";
$MESS["CRM_TAB_QUOTENUM"] = "ID's de las cotizaciones";
$MESS["CRM_TAB_QUOTENUM_TITLE"] = "ID de las cotizaciones y formato de numeración";
$MESS["CRM_TAB_DUPLICATE_CONTROL"] = "Control de Duplicados";
$MESS["CRM_TAB_DUPLICATE_CONTROL_TITLE"] = "Parámetros de control de duplicados";
$MESS["CRM_TAB_STATUS_CONFIG"] = "Diccionarios";
$MESS["CRM_TAB_STATUS_CONFIG_TITLE"] = "Configuración de diccionario";
$MESS["CRM_TAB_DEAL_CONFIG"] = "Negociaciones";
$MESS["CRM_TAB_DEAL_CONFIG_TITLE"] = "Parámetros de la negociación";
$MESS["CRM_TAB_GENERAL"] = "General";
$MESS["CRM_TAB_GENERAL_TITLE"] = "Parámetros comunes";
$MESS["CRM_TAB_HISTORY"] = "Historial";
$MESS["CRM_TAB_HISTORY_TITLE"] = "Configuración del historial";
$MESS["CRM_TAB_LIVEFEED"] = "Flujo de Actividad";
$MESS["CRM_TAB_LIVEFEED_TITLE"] = "Configuración del flujo de actividad";
$MESS["CRM_TAB_REST"] = "Aplicaciones";
$MESS["CRM_TAB_REST_TITLE"] = "Configuraciones de la aplicación";
?>