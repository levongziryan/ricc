<?
$MESS["WD_ACCESS_DENIED"] = "Acesso negado.";
$MESS["WD_ERROR_ELEMENT_NOT_FOUND"] = "Elemento não foi encontrado.";
$MESS["WD_TITLE_1"] = "Histórico de alterações de arquivo";
$MESS["WD_HISTORY"] = "História";
$MESS["W_IBLOCK_IS_NOT_INSTALLED"] = "Módulo de blocos de informações não está instalado.";
$MESS["WD_TITLE_2"] = "Versão do arquivo original";
$MESS["WD_TITLE"] = "Ver Elemento";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "Módulo WebDav não está instalado.";
$MESS["WD_ORIGINAL"] = "Original:";
$MESS["WD_VIEW_ELEMENT"] = "Ver";
$MESS["WD_USER"] = "Documentos do usuário";
$MESS["WD_USER_SECTION_FILES_NOT_FOUND"] = "O usuário secção de documentos não foi encontrado.";
$MESS["WD_DISK_NOTIFY_DIR_NUMERAL_21"] = "#COUNT# pastas";
$MESS["WD_SECTION_LIST_URL"] = "Página de visualização de catálogos";
$MESS["Send_Document"] = "Carregar novo documento";
$MESS["W_TITLE_ELS"] = "Elementos";
$MESS["W_TITLE_TAGS"] = "Tags";
$MESS["WD_SECTION_NAME"] = "Biblioteca";
$MESS["WD_INSTALL"] = "Próxima>";
$MESS["WD_REWRITE_PUBLIC"] = "Substituir os arquivos existentes";
?>