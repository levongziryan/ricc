<?
$MESS["WD_ERROR_ELEMENT_NOT_FOUND"] = "lément introuvable.";
$MESS["WD_ACCESS_DENIED"] = "Accès interdit.";
$MESS["WD_HISTORY"] = "Histoire de la compagnie";
$MESS["WD_TITLE_1"] = "Historique des modifications du document";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "Le module de la bibliothèque des documents n'a pas été installé.";
$MESS["W_IBLOCK_IS_NOT_INSTALLED"] = "Le module blocs d'information n'a pas été installé";
$MESS["WD_ORIGINAL"] = "Original:";
$MESS["WD_TITLE_2"] = "Version originale de document";
$MESS["WD_TITLE"] = "Afficher l'élément";
?>