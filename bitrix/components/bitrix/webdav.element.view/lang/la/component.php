<?
$MESS["W_IBLOCK_IS_NOT_INSTALLED"] = "El módulo del block de información no está instalado.";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "El módulo del WebDav no está instalado.";
$MESS["WD_ERROR_ELEMENT_NOT_FOUND"] = "El elemento no fue encontrado.";
$MESS["WD_TITLE"] = "Ver el elemento";
$MESS["WD_ACCESS_DENIED"] = "Acceso denegado.";
$MESS["WD_TITLE_1"] = "Historial de cambio del archivo";
$MESS["WD_TITLE_2"] = "Versión original del archivo";
$MESS["WD_HISTORY"] = "Historial";
$MESS["WD_ORIGINAL"] = "Original: ";
?>