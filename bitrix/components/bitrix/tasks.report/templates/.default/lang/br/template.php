<?
$MESS["TASKS_ADD_BACK_TO_TASKS_LIST"] = "Voltar para Tarefas";
$MESS["TASKS_THIS_MONTH"] = "este mês";
$MESS["TASKS_PREVIOUS_MONTH"] = "mês passado";
$MESS["TASKS_THIS_WEEK"] = "esta semana";
$MESS["TASKS_PREVIOUS_WEEK"] = "semana passada";
$MESS["TASKS_LAST_N_DAYS"] = "ltimo";
$MESS["TASKS_AFTER"] = "depois";
$MESS["TASKS_BEFORE"] = "antes";
$MESS["TASKS_DATE_INTERVAL"] = "intervalo de datas";
$MESS["TASKS_REPORT_EMPLOYEE"] = "Funcionário";
$MESS["TASKS_REPORT_NEW"] = "Novo";
$MESS["TASKS_REPORT_REPORT"] = "Relatório";
$MESS["TASKS_REPORT_ALL"] = "todos";
$MESS["TASKS_REPORT_OPEN"] = "Aberto";
$MESS["TASKS_REPORT_CLOSED"] = "Fechado";
$MESS["TASKS_REPORT_OVERDUE"] = "Atrasado";
$MESS["TASKS_REPORT_MARKED"] = "Avaliação";
$MESS["TASKS_REPORT_EFFICIENCY"] = "Eficiência";
$MESS["TASKS_REPORT_DEPARTMENT_SUMMARY"] = "Resumo de departamento";
$MESS["TASKS_REPORT_FILTER"] = "Filtrar";
$MESS["TASKS_REPORT_PERIOD"] = "Período do Relatório";
$MESS["TASKS_PICK_DATE"] = "Selecione a data no calendário";
$MESS["TASKS_REPORT_DAYS"] = "dias";
$MESS["TASKS_REPORT_STRUCTURE"] = "Estrutura da Empresa";
$MESS["TASKS_REPORT_NOT_SELECTED"] = "não selecionado";
$MESS["TASKS_REPORT_WORKGROUP"] = "Workgroup";
$MESS["TASKS_REPORT_FIND"] = "Pesquisar";
$MESS["TASKS_REPORT_CANCEL"] = "Cancelar";
$MESS["TASKS_REPORT_REPORTS"] = "Relatórios";
$MESS["TASKS_REPORT_TASKS"] = "Tarefas";
$MESS["TASKS_REPORT_TEMPLATES"] = "Modelos";
$MESS["TASKS_EXPORT_TO_EXCEL"] = "Exportar para Excel";
$MESS["TASKS_REPORT_SHOW_ALL"] = "mostrar relatório para todas as tarefas";
$MESS["TASKS_REPORT_HEAD"] = "chefe de departamento";
$MESS["TASKS_REPORT_SUBORDINATE"] = "Funcionário";
$MESS["TASKS_REPORT_WHOLE_COMPANY_EFFICIENCY"] = "Produtividade dos funcionários";
$MESS["TASKS_REPORT_EMPLOYEES_COUNT"] = "Total de funcionários";
$MESS["TASKS_REPORT_USE_TASKS"] = "Usando tarefas";
$MESS["TASKS_NO_DATA"] = "sem dados";
?>