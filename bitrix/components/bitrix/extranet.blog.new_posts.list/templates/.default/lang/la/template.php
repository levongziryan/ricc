<?
$MESS["BLOG_BLOG_BLOG_MORE"] = "Más...";
$MESS["BLOG_BLOG_BLOG_CATEGORY"] = "Etiquetas:";
$MESS["BLOG_BLOG_BLOG_PERMALINK"] = "Dirección habitual";
$MESS["BLOG_BLOG_BLOG_COMMENTS"] = "Comentarios:";
$MESS["BLOG_BLOG_BLOG_NO_AVAIBLE_MES"] = "Ningún mensaje habilitado";
$MESS["BLOG_MES_DELETE_POST_CONFIRM"] = "Esta usted seguro de querer eliminar esta publicación?";
$MESS["BLOG_BLOG_BLOG_VIEWS"] = "Revisiones:";
$MESS["BLOG_BLOG_BLOG_CATEGORY_FILTER"] = "<div class=\"extranet-blog-tag-title\"> Publicaciones etiquetadas: <div class=\"extranet-blog-tag\">#CATEGORY#</div></div>";
?>