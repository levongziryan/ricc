<?
$MESS["EBMNP_NAME_TEMPLATE"] = "Formato de nombre";
$MESS["EBMNP_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["EBMNP_SHOW_LOGIN"] = "Mostrar el login si ninguno de los campos de nombre de usuario requeridos esta disponibles.";
$MESS["EBMNP_PATH_TO_CONPANY_DEPARTMENT"] = "Ruta de la página del departamento de plantilla";
$MESS["EBMNP_PATH_TO_VIDEO_CALL"] = "Página de vídeo conferencia";
$MESS["EBMNP_PATH_TO_SONET_USER_PROFILE"] = "Ruta de la plantilla del perfil de usuario de redes sociales";
$MESS["EBMNP_PATH_TO_MESSAGES_CHAT"] = "Ruta de la plantilla del messenger de usuario";
?>