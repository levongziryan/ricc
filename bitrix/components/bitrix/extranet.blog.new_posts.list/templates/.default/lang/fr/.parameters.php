<?
$MESS["EBMNP_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["EBMNP_NAME_TEMPLATE"] = "Affichage du nom";
$MESS["EBMNP_SHOW_LOGIN"] = "Afficher le nom d'utilisateur si le nom n'est pas spécifié";
$MESS["EBMNP_PATH_TO_VIDEO_CALL"] = "Page de l'appel vidéo";
$MESS["EBMNP_PATH_TO_CONPANY_DEPARTMENT"] = "Modèle de chemin d'accès à la page d'une section";
$MESS["EBMNP_PATH_TO_SONET_USER_PROFILE"] = "Modèle de chemin d'accès à la page de l'utilisateur du réseau social";
$MESS["EBMNP_PATH_TO_MESSAGES_CHAT"] = "Modèle de chemin d'accès au chat";
?>