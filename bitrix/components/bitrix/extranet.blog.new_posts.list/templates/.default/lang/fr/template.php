<?
$MESS["BLOG_BLOG_BLOG_CATEGORY_FILTER"] = "<div class='extranet-blog-tag-title'>Message avec un tag <div class='extranet-blog-tag'>#CATEGORY#</div></div>";
$MESS["BLOG_MES_DELETE_POST_CONFIRM"] = "?tes-vous sûr de vouloir supprimer le message?";
$MESS["BLOG_BLOG_BLOG_NO_AVAIBLE_MES"] = "Message introuvable";
$MESS["BLOG_BLOG_BLOG_COMMENTS"] = "Nombre de commentaires:";
$MESS["BLOG_BLOG_BLOG_MORE"] = "Plus";
$MESS["BLOG_BLOG_BLOG_PERMALINK"] = "Adresse permanente";
$MESS["BLOG_BLOG_BLOG_VIEWS"] = "vues:";
$MESS["BLOG_BLOG_BLOG_CATEGORY"] = "Tags:";
?>