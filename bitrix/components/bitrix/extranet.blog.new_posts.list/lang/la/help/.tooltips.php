<?
$MESS["MESSAGE_COUNT_TIP"] = "Numero máximo de mensajes mostrados en una página.";
$MESS["MESSAGE_LENGTH_TIP"] = "El número de caracteres permitidos en una publicación simple";
$MESS["PATH_TO_BLOG_TIP"] = "La ruta a la página principal del blog. Ejemplo: blog_blog.php?page=blog&blog=#blog#.";
$MESS["PATH_TO_POST_TIP"] = "La ruta a una vista de página del blog publicado. Ejemplo: <nobr>blog_post.php?page=post&blog=#blog#&post_id=#post_id#.</nobr>";
$MESS["PATH_TO_USER_TIP"] = "La ruta a una página de perfil de usuario. Ejemplo: <nobr>blog_user.php?page=user&user_id=#user_id#.</nobr>";
$MESS["BLOG_VAR_TIP"] = "Especificar aquí el nombre de una variable a los cuales el ID del blog sera aprobado.";
$MESS["POST_VAR_TIP"] = "Especificar aquí el nombre de una variable a los cuales el ID del blog publicado sera aprobado.";
$MESS["USER_VAR_TIP"] = "Especificar aquí el nombre de una variable a los cuales el ID del usuario del blog sera aprobado.";
$MESS["PAGE_VAR_TIP"] = "Especificar aquí el nombre de una variable a los cuales el ID de la página del blog sera aprobada.";
$MESS["CACHE_TYPE_TIP"] = "<i>Auto</i>: el caché es valido por un periodo de tiempo predeterminado en la configuración del caché;<br /><i>Cache</i>: Siempre el periodo del caché sera especificado en el siguiente campo;<br /><i>Sin Caché</i>: no se almacena el caché.";
$MESS["CACHE_TIME_TIP"] = "Especificar aquí el periodo de tiempo durante el cual el caché es valido.";
$MESS["PATH_TO_SMILE_TIP"] = "La ruta a una carpeta contenedora de iconos, relacionada al sitio raíz.";
$MESS["MESSAGE_LENGHT_TIP"] = "El número de caracteres permintido en una publicación simple.";
?>