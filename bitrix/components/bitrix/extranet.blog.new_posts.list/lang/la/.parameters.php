<?
$MESS["EBMNP_MESSAGE_PER_PAGE"] = "Mensajes por página";
$MESS["EBMNP_MESSAGE_LENGTH"] = "Mostrar el máximo de mensajes del área";
$MESS["EBMNP_PATH_TO_BLOG"] = "Ruta de la plantilla del blog";
$MESS["EBMNP_PATH_TO_POST"] = "Plantilla del blog de la página de mensajes";
$MESS["EBMNP_PATH_TO_USER"] = "Ruta de la plantilla del blog de la página de usuario";
$MESS["EBMNP_PATH_TO_SMILE"] = "Ruta de acceso a la carpeta con iconos, en relación con el sitio raíz";
$MESS["EBMNP_BLOG_VAR"] = "Identificador de la variable del blog";
$MESS["EBMNP_POST_VAR"] = "Identificador de la variable del mensajes del blog";
$MESS["EBMNP_USER_VAR"] = "Identificador de la variable del blog de usuario";
$MESS["EBMNP_PAGE_VAR"] = "Pagina variable";
$MESS["B_VARIABLE_ALIASES"] = "Alias variables";
$MESS["EBMNP_PREVIEW_WIDTH"] = "Ancho de la imagen previa";
$MESS["EBMNP_PREVIEW_HEIGHT"] = "Altura de la imagen previa";
$MESS["BC_DATE_TIME_FORMAT"] = "Formato de fecha y hora ";
$MESS["BLG_GROUP_ID"] = "Grupos de Blogs";
$MESS["BLG_BLOG_URL"] = "URL del Blog";
$MESS["EBMNP_PATH_TO_GROUP_BLOG_POST"] = "Plantilla URL para página publicada en el grupo de blog";
$MESS["BB_PATH_TO_BLOG_CATEGORY"] = "Plantilla URL para página en el grupo de blog (filtrado por un etiqueta)";
$MESS["BB_NAV_TEMPLATE"] = "Paginando plantillas por nombre";
$MESS["EBMNP_PATH_TO_BLOG_CATEGORY"] = "Página de Categoría de Lista de Mensajes";
$MESS["EBMNP_CATEGORY_NAME_VAR"] = "Nombre de Variable de la Categoría";
?>