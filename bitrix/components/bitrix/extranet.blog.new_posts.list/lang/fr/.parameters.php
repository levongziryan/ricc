<?
$MESS["BLG_BLOG_URL"] = "Adresse du blogue à afficher";
$MESS["EBMNP_PREVIEW_HEIGHT"] = "Hauteur de l'image pour l'affichage préalable";
$MESS["BLG_GROUP_ID"] = "Groupe de blogues pour l'affichage";
$MESS["EBMNP_MESSAGE_LENGTH"] = "Longueur du texte du message affiché";
$MESS["B_VARIABLE_ALIASES"] = "Alias des variables";
$MESS["EBMNP_BLOG_VAR"] = "Nom de la variable pour l'identificateur de l'utilisateur";
$MESS["EBMNP_USER_VAR"] = "Nom de la variable pour l'identificateur de l'utilisateur";
$MESS["EBMNP_POST_VAR"] = "Le nom de la variable pour l'identificateur du message du blog";
$MESS["EBMNP_CATEGORY_NAME_VAR"] = "Nom de valeur variable pour la catégorie";
$MESS["EBMNP_PAGE_VAR"] = "Nom de la variable pour la page";
$MESS["BB_NAV_TEMPLATE"] = "Nom du modèle pour navigation page par page";
$MESS["EBMNP_MESSAGE_PER_PAGE"] = "Nombre de messages affichés";
$MESS["EBMNP_PATH_TO_SMILE"] = "Chemin vers le dossier avec les smileys par rapport à la racine du site";
$MESS["BC_DATE_TIME_FORMAT"] = "Format d'affichage de la date et de l'heure";
$MESS["EBMNP_PATH_TO_BLOG"] = "Modèle de chemin d'accès à la page des messages";
$MESS["BB_PATH_TO_BLOG_CATEGORY"] = "Modèle de chemin d'accès à la page d'un blog avec un filtre de tag";
$MESS["EBMNP_PATH_TO_BLOG_CATEGORY"] = "Liste des messages est vide";
$MESS["EBMNP_PATH_TO_USER"] = "Modèle de chemin d'accès à la page de l'utilisateur du blog";
$MESS["EBMNP_PATH_TO_POST"] = "Modèle de chemin d'accès à la page avec un message de blog";
$MESS["EBMNP_PATH_TO_GROUP_BLOG_POST"] = "Modèle de chemin d'accès à la page avec un message de blog du groupe";
$MESS["EBMNP_PREVIEW_WIDTH"] = "La largeur de l'image de consultation préalable";
?>