<?
$MESS["EBMNP_MESSAGE_PER_PAGE"] = "Mensagens por página";
$MESS["EBMNP_MESSAGE_LENGTH"] = "Comprimento de exibição máximo de mensagem";
$MESS["EBMNP_PATH_TO_BLOG"] = "Modelo de caminho da página de blog";
$MESS["EBMNP_PATH_TO_POST"] = "Modelo da página de mensagem do blog";
$MESS["EBMNP_PATH_TO_USER"] = "Modelo do caminho da página do blog de usuário";
$MESS["EBMNP_PATH_TO_BLOG_CATEGORY"] = "Página de categoria de lista de mensagens";
$MESS["EBMNP_PATH_TO_SMILE"] = "Caminho para a pasta com smileys, em relação à raiz do site";
$MESS["EBMNP_BLOG_VAR"] = "Variável identificadora de blog";
$MESS["EBMNP_POST_VAR"] = "Variável identificadora de mensagem de blog";
$MESS["EBMNP_USER_VAR"] = "Variável identificadora de usuário de blog";
$MESS["EBMNP_PAGE_VAR"] = "Variável de página";
$MESS["EBMNP_CATEGORY_NAME_VAR"] = "Categoria Nome Variável";
$MESS["B_VARIABLE_ALIASES"] = "Pseudônimos variáveis";
$MESS["EBMNP_PREVIEW_WIDTH"] = "Largura da imagem de visualização";
$MESS["EBMNP_PREVIEW_HEIGHT"] = "Altura da imagem de visualização";
$MESS["BC_DATE_TIME_FORMAT"] = "Formato de data e hora";
$MESS["BLG_GROUP_ID"] = "Grupo de blogs";
$MESS["BLG_BLOG_URL"] = "URL do Blog";
$MESS["EBMNP_PATH_TO_GROUP_BLOG_POST"] = "Modelo de URL da página de postagem do blog do grupo";
$MESS["BB_PATH_TO_BLOG_CATEGORY"] = "Modelo de URL da página de blog do grupo (filtrado por tag)";
$MESS["BB_NAV_TEMPLATE"] = "Nome do modelo de paginação";
?>