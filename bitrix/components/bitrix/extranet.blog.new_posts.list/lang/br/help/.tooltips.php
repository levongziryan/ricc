<?
$MESS["MESSAGE_COUNT_TIP"] = "O número máximo de mensagens exibidas em uma página.";
$MESS["MESSAGE_LENGTH_TIP"] = "O número de caracteres permitidos em um único post.";
$MESS["PATH_TO_BLOG_TIP"] = "O caminho para a página principal do blog. Exemplo: blog_blog.php?page=blog&blog=#blog#.";
$MESS["PATH_TO_POST_TIP"] = "O caminho para a visualilzação de um post do blog. Exemplo: <nobr>blog_post.php?page=post&blog=#blog#&post_id=#post_id#.</nobr>";
$MESS["PATH_TO_USER_TIP"] = "O caminho para uma página de perfil do usuário. Exemplo: <nobr>blog_user.php?page=user&user_id=#user_id#.</nobr>";
$MESS["BLOG_VAR_TIP"] = "Especifique aqui o nome de uma variável para a qual o ID do blog vai ser passado.";
$MESS["POST_VAR_TIP"] = "Especifique aqui o nome de uma variável para a qual o ID do blog vai ser passado.";
$MESS["USER_VAR_TIP"] = "Especifique aqui o nome de uma variável para a qual o ID do blog vai ser passado.";
$MESS["PAGE_VAR_TIP"] = "Especifique aqui o nome de uma variável para a qual o ID do blog vai ser passado.";
$MESS["CACHE_TYPE_TIP"] = "<br /><i>Cache</i>: always cache for the period specified in the next field";
$MESS["CACHE_TIME_TIP"] = "Especifique aqui o período de tempo durante o qual o cache é válido.";
$MESS["PATH_TO_SMILE_TIP"] = "O caminho para uma pasta contendo smileys, relativa à raiz do site.";
$MESS["MESSAGE_LENGHT_TIP"] = "O número de caracteres permitidos em um único post.";
?>