<?
$MESS["CALENDAR_MODULE_IS_NOT_INSTALLED"] = "Le module 'Calendrier des événements' n'a pas été installé.";
$MESS["EVENTS_GROUP_TODAY"] = "Aujourd'hui";
$MESS["EVENTS_GROUP_TOMORROW"] = "Demain";
$MESS["EVENTS_GROUP_LATE"] = "Plus tard";
$MESS["MB_CAL_NO_EVENTS"] = "Il n'y a pas d'événements à venir";
$MESS["MB_CAL_EVENTS_COUNT"] = "Evènements au total: #COUNT#";
$MESS["MB_CAL_EVENT_ALL_DAY"] = "toute la journée";
$MESS["MB_CAL_EVENT_DATE_FROM_TO"] = "A partir de #DATE_FROM# jusqu'à #DATE_TO#";
$MESS["MB_CAL_EVENT_TIME_FROM_TO_TIME"] = "de #TIME_FROM#  à #TIME_TO#";
$MESS["MB_CAL_EVENT_DATE_FORMAT"] = "D., M. d, Y";
$MESS["MB_CAL_EVENT_TIME_FORMAT_AMPM"] = "h:i a";
$MESS["MB_CAL_EVENT_TIME_FORMAT"] = "G:i";
?>