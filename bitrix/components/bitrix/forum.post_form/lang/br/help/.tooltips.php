<?
$MESS["FID_TIP"] = "Inscrever-se para novos posts";
$MESS["TID_TIP"] = "Increver o autor do elemento para novos comentários";
$MESS["MID_TIP"] = "Inscrever-se para novos posts neste fórum";
$MESS["PAGE_NAME_TIP"] = "Inscrever-se para novos posts neste fórum";
$MESS["MESSAGE_TYPE_TIP"] = "Inscrever-se para novos posts neste fórum";
$MESS["URL_TEMPLATES_MESSAGE_TIP"] = "Inscrever-se para novos posts neste fórum";
$MESS["URL_TEMPLATES_LIST_TIP"] = "Inscrever-se para novos posts neste fórum";
$MESS["URL_TEMPLATES_HELP_TIP"] = "Inscrever-se para novos posts neste tópico";
$MESS["URL_TEMPLATES_RULES_TIP"] = "Inscrever-se para novos posts neste tópico";
$MESS["PATH_TO_SMILE_TIP"] = "Inscrever-se para novos posts neste tópico";
$MESS["PATH_TO_ICON_TIP"] = "Inscrever-se para novos posts neste tópico";
$MESS["SMILE_TABLE_COLS_TIP"] = "Inscrever-se para novos posts neste tópico";
$MESS["AJAX_TYPE_TIP"] = "Inscrever-se para novos tópicos";
$MESS["CACHE_TYPE_TIP"] = "Inscrever-se para novos tópicos";
$MESS["CACHE_TIME_TIP"] = "Os inscritos podem receber meus posts:";
$MESS["SHOW_VOTE_TIP"] = "Inscrição";
?>