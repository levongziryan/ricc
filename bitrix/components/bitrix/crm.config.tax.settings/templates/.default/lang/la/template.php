<?
$MESS["CRM_TAX_SETTINGS_SAVE_BUTTON"] = "Guardar";
$MESS["CRM_TAX_SETTINGS_CHOOSE"] = "Seleccione el tipo de impuesto";
$MESS["CRM_TAX_TAX"] = "Función de la ubicación de impuesto (impuesto sobre las ventas)";
$MESS["CRM_TAX_TAX_HINT"] = "Este impuesto se aplica a la cantidad total del pedido. La tasa del impuesto depende de la ubicación del cliente.";
$MESS["CRM_TAX_VAT"] = "Elemento sujeto a impuesto (IVA)";
$MESS["CRM_TAX_VAT_HINT"] = "Este impuesto se especifica y se aplican a cada elemento de forma individual.";
$MESS["CRM_TAX_SETTINGS_TITLE"] = "Ajustes fiscales";
$MESS["CRM_TAX_TAX1"] = "Impuesto de la factura";
$MESS["CRM_TAX_TAX_HINT1"] = "El importe del impuesto se calcula sobre el total de la factura. La tasa del impuesto depende de la ubicación del cliente.";
$MESS["CRM_TAX_VAT_HINT1"] = "El importe del impuesto se calcula para cada elemento de la factura individualmente.";
?>