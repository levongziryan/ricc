<?
$MESS["CRM_TAX_DELETE_DLG_MESSAGE"] = "tes-vous sûr de vouloir supprimer cette taxe?";
$MESS["CRM_TAX_ADD"] = "Ajouter un împot";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "Le module 'Boutique en ligne' n'a pas été installé.";
$MESS["CRM_TAX_SETTINGS"] = "Paramètres";
$MESS["CRM_TAX_SETTINGS_TITLE"] = "Régler le système de taxes";
$MESS["CRM_TAX_SHOW_TITLE"] = "Passage à l'examen de la taxe";
$MESS["CRM_TAX_EDIT_TITLE"] = "Modifier l'impôt";
$MESS["CRM_TAX_ADD_TITLE"] = "Passage vers la création d'un nouvel impôt";
$MESS["CRM_TAX_LIST_TITLE"] = "Accéder à la liste des impôts";
$MESS["CRM_TAX_SHOW"] = "Affichage";
$MESS["CRM_TAX_EDIT"] = "Editer";
$MESS["CRM_TAX_LIST"] = "Impôts";
$MESS["CRM_TAX_DELETE_TITLE"] = "Eliminer l'impôt";
$MESS["CRM_TAX_DELETE_DLG_TITLE"] = "Eliminer l'impôt";
$MESS["CRM_TAX_DELETE"] = "Eliminer l'impôt";
$MESS["CRM_TAX_DELETE_DLG_BTNTITLE"] = "Eliminer l'impôt";
?>