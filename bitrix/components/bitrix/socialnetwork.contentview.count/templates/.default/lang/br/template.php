<?
$MESS["SCVC_TEMPLATE_LICENSE_TITLE"] = "Fluxo de Atividade Estendido";
$MESS["SCVC_TEMPLATE_LICENSE_TEXT"] = "O contador de exibição de postagem do fluxo de atividade está disponível no plano Plus e superior.<br><br>Para exibir o número de visualizações e leitores de um determinado lugar, passe o cursor do mouse sobre o ícone.";
?>