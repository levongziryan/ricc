<?
$MESS["SCVC_TEMPLATE_LICENSE_TITLE"] = "Flux d'activités étendu";
$MESS["SCVC_TEMPLATE_LICENSE_TEXT"] = "Le compteur de vues des publications du Flux d'activité est disponible dans l'abonnement Plus et dans les abonnements supérieurs.<br><br>Pour voir le nombre de vues et de lecteurs d'une publication spécifique, passez le pointeur de la souris au-dessus de l'icône.";
$MESS["SCVC_TEMPLATE_POPUP_TITLE"] = "Vues";
?>