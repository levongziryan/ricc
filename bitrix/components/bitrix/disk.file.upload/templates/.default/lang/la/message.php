<?
$MESS["DFU_UPLOAD_TITLE1"] = "Cargar Nuevo Documento";
$MESS["DFU_UPLOAD_TITLE2"] = "Cargar #NUMBER# archivos de #COUNT#";
$MESS["DFU_UPLOAD_CANCELED"] = "Carga cancelada";
$MESS["DFU_REPLACE"] = "Reemplazar";
$MESS["DFU_CANCEL"] = "Cancelar";
$MESS["DFU_CLOSE"] = "Cerrar";
$MESS["DFU_UPLOAD"] = "Cargar más";
$MESS["DFU_DUPLICATE"] = "Un archivo con este nombre ya existe.";
$MESS["DFU_DND1"] = "Subir archivo o imagen";
$MESS["DFU_DND2"] = "Agregar mediante la función de arrastrar y soltar";
$MESS["DFU_SAVE_BP"] = "Guardar parámetros";
$MESS["DFU_SAVE_BP_DIALOG"] = "Cargar";
?>