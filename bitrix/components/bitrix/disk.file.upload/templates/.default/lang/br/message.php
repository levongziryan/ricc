<?
$MESS["DFU_UPLOAD_TITLE1"] = "Carregar Novo Documento";
$MESS["DFU_UPLOAD_TITLE2"] = "Carregados #NUMBER# arquivos de #COUNT#";
$MESS["DFU_UPLOAD_CANCELED"] = "Carregamento cancelado";
$MESS["DFU_REPLACE"] = "Substituir";
$MESS["DFU_CANCEL"] = "Cancelar";
$MESS["DFU_CLOSE"] = "Fechar";
$MESS["DFU_UPLOAD"] = "Carregar mais";
$MESS["DFU_DUPLICATE"] = "Já existe um arquivo com este nome.";
$MESS["DFU_DND1"] = "Carregar arquivo ou imagem";
$MESS["DFU_DND2"] = "Adicionar usando arrastar e largar";
$MESS["DFU_SAVE_BP"] = "Salvar parâmetros";
$MESS["DFU_SAVE_BP_DIALOG"] = "Carregar";
?>