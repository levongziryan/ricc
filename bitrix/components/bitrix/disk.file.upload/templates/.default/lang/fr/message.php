<?
$MESS["DFU_UPLOAD_TITLE2"] = "#NUMBER# fichiers téléchargés sur #COUNT#";
$MESS["DFU_UPLOAD"] = "Télécharger plus";
$MESS["DFU_DND1"] = "Télécharger fichier ou image";
$MESS["DFU_UPLOAD_TITLE1"] = "Chargement d'un nouveau document";
$MESS["DFU_UPLOAD_CANCELED"] = "Téléchargement annulé";
$MESS["DFU_CLOSE"] = "Fermer";
$MESS["DFU_REPLACE"] = "Remplacer";
$MESS["DFU_CANCEL"] = "Annuler";
$MESS["DFU_DND2"] = "Déplacer à l'aide du drag'n'drop";
$MESS["DFU_SAVE_BP"] = "Sauvegarder les paramètres";
$MESS["DFU_DUPLICATE"] = "Le fichier du même nom existe déjà.";
$MESS["DFU_SAVE_BP_DIALOG"] = "Chargement";
?>