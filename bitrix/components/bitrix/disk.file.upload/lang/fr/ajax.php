<?
$MESS["DISK_FILE_UPLOAD_ERROR_COULD_NOT_FIND_FOLDER"] = "Dossier introuvable.";
$MESS["DISK_FILE_UPLOAD_ERROR_COULD_NOT_FIND_FILE"] = "Fichier introuvable.";
$MESS["DISK_FILE_UPLOAD_ERROR_COULD_UPLOAD_VERSION"] = "La mise à jour n'a pas été effectuée.";
$MESS["DISK_FILE_UPLOAD_ERROR_BAD_RIGHTS"] = "Permission insuffisante pour ajouter un fichier.";
$MESS["DISK_FILE_UPLOAD_ERROR_BAD_RIGHTS2"] = "Permission insuffisante pour procéder à la mise à jour du fichier.";
?>