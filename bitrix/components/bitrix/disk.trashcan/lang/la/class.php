<?
$MESS["DISK_TRASHCAN_ERROR_COULD_NOT_FIND_OBJECT"] = "No se pudo encontrar el objeto.";
$MESS["DISK_TRASHCAN_NAME"] = "Papelera De Reciclaje";
$MESS["DISK_TRASHCAN_COLUMN_ID"] = "ID";
$MESS["DISK_TRASHCAN_COLUMN_NAME"] = "Nombre";
$MESS["DISK_TRASHCAN_COLUMN_CREATE_TIME"] = "Creado el";
$MESS["DISK_TRASHCAN_COLUMN_UPDATE_TIME"] = "Modificado el";
$MESS["DISK_TRASHCAN_COLUMN_CREATE_USER"] = "Creado por";
$MESS["DISK_TRASHCAN_COLUMN_UPDATE_USER"] = "Modificado por";
$MESS["DISK_TRASHCAN_COLUMN_FORMATTED_SIZE"] = "Tamaño";
$MESS["DISK_TRASHCAN_ACT_DESTROY"] = "Eliminar";
$MESS["DISK_TRASHCAN_ACT_OPEN"] = "Abrir";
$MESS["DISK_TRASHCAN_ACT_DOWNLOAD"] = "Descargar";
$MESS["DISK_TRASHCAN_ACT_RESTORE"] = "Restaurar";
$MESS["DISK_TRASHCAN_TRASH_DELETE_TITLE"] = "Confirmar eliminación";
$MESS["DISK_TRASHCAN_TRASH_RESTORE_TITLE"] = "Confirmar restauración";
$MESS["DISK_TRASHCAN_TRASH_EMPTY_TITLE"] = "Vaciar la papelera de reciclaje";
$MESS["DISK_TRASHCAN_TRASH_DELETE_DESTROY_FILE_CONFIRM"] = "¿Seguro que quieres borrar '#NAME#' para siempre?";
$MESS["DISK_TRASHCAN_TRASH_DELETE_DESTROY_FOLDER_CONFIRM"] = "¿Seguro que quieres eliminar la carpeta '#NAME#' para siempre?";
$MESS["DISK_TRASHCAN_TRASH_RESTORE_FILE_CONFIRM"] = "¿Desea restaurar el documento '#NAME#'?";
$MESS["DISK_TRASHCAN_TRASH_RESTORE_FOLDER_CONFIRM"] = "¿Desea restaurar la carpeta '#NAME#'?";
$MESS["DISK_TRASHCAN_TRASH_DELETE_FOLDER_CONFIRM"] = "¿Desea mover la carpeta \"#NAME#\" a la papelera de reciclaje?";
$MESS["DISK_TRASHCAN_TRASH_DELETE_FILE_CONFIRM"] = "¿Quieres pasar \"#NAME#\" a la papelera de reciclaje?";
$MESS["DISK_TRASHCAN_TRASH_DELETE_GROUP_CONFIRM"] = "¿Seguro que desea eliminar los elementos seleccionados para siempre?";
$MESS["DISK_TRASHCAN_TRASH_RESTORE_GROUP_CONFIRM"] = "¿Quieres restaurar los elementos seleccionados?";
$MESS["DISK_TRASHCAN_TRASH_EMPTY_FOLDER_CONFIRM"] = "¿Seguro que desea vaciar la papelera de reciclaje?";
$MESS["DISK_TRASHCAN_TRASH_EMPTY_BUTTON"] = "Vacío";
$MESS["DISK_TRASHCAN_TRASH_DESTROY_BUTTON"] = "Eliminar irreversiblemente";
$MESS["DISK_TRASHCAN_TRASH_DELETE_BUTTON"] = "Mover a la papelera de reciclaje";
$MESS["DISK_TRASHCAN_TRASH_RESTORE_BUTTON"] = "Restaurar";
$MESS["DISK_TRASHCAN_TRASH_CANCEL_DELETE_BUTTON"] = "Cancelar";
$MESS["DISK_TRASHCAN_TRASH_CANCEL_STOP_BUTTON"] = "Parar";
$MESS["DISK_TRASHCAN_TRASH_COUNT_ELEMENTS"] = "Archivos y/o carpetas para eliminar:";
$MESS["DISK_TRASHCAN_COLUMN_DELETE_TIME"] = "Eliminar";
$MESS["DISK_TRASHCAN_COLUMN_DELETE_USER"] = "Eliminado por";
?>