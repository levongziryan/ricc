<?
$MESS["DISK_FOLDER_ACTION_MESSAGE_FOLDER_MARK_DELETED"] = "La carpeta se ha eliminado";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FILE_MARK_DELETED"] = "El archivo se ha eliminado";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FOLDER_DELETED"] = "La carpeta se ha eliminado";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FILE_DELETED"] = "El archivo se ha eliminado";
$MESS["DISK_TRASHCAN_ERROR_COULD_NOT_FIND_OBJECT"] = "No se pudo encontrar el objeto.";
$MESS["DISK_TRASHCAN_ERROR_COULD_NOT_RESTORE_OBJECT"] = "No se puede restaurar el objeto.";
$MESS["DISK_TRASHCAN_ERROR_COULD_NOT_COPY_OBJECT"] = "No se puede copiar el objeto.";
$MESS["DISK_TRASHCAN_ERROR_COULD_NOT_MOVE_OBJECT"] = "No se puede mover el objeto.";
$MESS["DISK_TRASHCAN_ERROR_COULD_NOT_CREATE_FIND_EXT_LINK"] = "No se pudo crear o encontrar un enlace público del objeto.";
?>