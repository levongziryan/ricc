<?
$MESS["DISK_BIZPROC_SERIAL_TEXT"] = "Crear un proceso de negocio secuencial";
$MESS["DISK_BIZPROC_BACK_TITLE"] = "Volver al Drive";
$MESS["DISK_BIZPROC_BACK_TEXT"] = "Drive";
$MESS["DISK_BIZPROC_STATUS_TITLE"] = "Un proceso de negocio impulsado por un estado es un proceso de negocio continuo con la distribución de permisos de acceso para manejar documentos en varios estados.";
$MESS["DISK_BIZPROC_SERIAL_TITLE"] = "Un proceso de negocio secuencial es un proceso de negocio fácil para realizar una serie de acciones consecutivas en un documento.";
$MESS["DISK_BIZPROC_STATUS_TEXT"] = "Crear un proceso de negocio impulsada por un estado";
?>