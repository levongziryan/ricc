<?
$MESS["WD_ERROR_ELEMENT_NOT_FOUND"] = "lément introuvable.";
$MESS["WD_ACCESS_DENIED"] = "Accès interdit.";
$MESS["WD_UPLOAD_DONE"] = "Le téléchargement est terminé avec succès.";
$MESS["WD_TITLE"] = "Dition";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "Le module de la bibliothèque des documents n'a pas été installé.";
$MESS["WD_ERROR_RECOVER"] = "La restitution du document n'était pas réussie.";
?>