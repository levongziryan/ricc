<?
$MESS["WD_ACCESS_DENIED"] = "Acesso negado.";
$MESS["WD_TITLE"] = "Editar Elemento";
$MESS["WD_ERROR_ELEMENT_NOT_FOUND"] = "Elemento não foi encontrado.";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "Módulo WebDav não está instalado.";
$MESS["WD_UPLOAD_DONE"] = "Envio foi concluído com êxito.";
$MESS["WD_ERROR_RECOVER"] = "Não é possivel recuperar o documento.";
$MESS["WD_LOCK_ELEMENT"] = "Bloquear documento para edição";
$MESS["WD_CHECK_CREATOR"] = "Verifique o criador do arquivo";
$MESS["WD_DELETE_FILE_ALT"] = "Excluir documento";
?>