<?
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "El módulo del WebDav no está instalado.";
$MESS["WD_ERROR_ELEMENT_NOT_FOUND"] = "El elemento no fu encontrado.";
$MESS["WD_TITLE"] = "Editar Elemento";
$MESS["WD_ACCESS_DENIED"] = "Acceso Denegado.";
$MESS["WD_UPLOAD_DONE"] = "La carga ha finalizado exitosamente.";
$MESS["WD_ERROR_RECOVER"] = "No se puede recuperar el documento.";
?>