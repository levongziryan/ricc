<?
$MESS["WD_NAME"] = "Nombre";
$MESS["WD_FILE"] = "Archivo";
$MESS["WD_FILE_REPLACE"] = "Reemplazar archivo";
$MESS["WD_FILE_MODIFIED"] = "Modificado";
$MESS["WD_FILE_SIZE"] = "Dimensión";
$MESS["WD_DOWNLOAD_FILE"] = "Descargar";
$MESS["WD_DOCUMENT"] = "Documento";
$MESS["WD_DOCUMENT_ALT"] = "Propiedades del documento";
$MESS["WD_DOWNLOAD_ELEMENT"] = "Descargar";
$MESS["WD_EDIT_MSOFFICE"] = "Editar el archivo en MS Office";
$MESS["WD_GO_BACK"] = "Atrás";
$MESS["WD_GO_BACK_ALT"] = "Regresar a los documentos de la biblioteca";
$MESS["WD_SAVE"] = "Guardar Cambios";
$MESS["WD_CANCEL"] = "Cancelar";
$MESS["WD_END_EDIT"] = "Reemplazar el documento actual";
$MESS["WD_DELETE_FILE"] = "Eliminar";
$MESS["WD_DELETE_FILE_ALT"] = "Eliminar el Documento";
?>