<?
$MESS["WD_DOCUMENT"] = "Document";
$MESS["WD_END_EDIT"] = "Charger la nouvelle copie";
$MESS["WD_FILE_REPLACE"] = "Remplacer le fichier";
$MESS["WD_FILE_MODIFIED"] = "Modifié";
$MESS["WD_GO_BACK"] = "Précédent";
$MESS["WD_GO_BACK_ALT"] = "Retour à la liste de documents de bibliothèque";
$MESS["WD_NAME"] = "Dénomination";
$MESS["WD_DOCUMENT_ALT"] = "Propriétés du document";
$MESS["WD_CANCEL"] = "Annuler";
$MESS["WD_FILE_SIZE"] = "Rempli";
$MESS["WD_EDIT_MSOFFICE"] = "Editer dans MS Office";
$MESS["WD_DOWNLOAD_FILE"] = "Télécharger";
$MESS["WD_DOWNLOAD_ELEMENT"] = "Télécharger";
$MESS["WD_SAVE"] = "Enregistrer les modifications";
$MESS["WD_DELETE_FILE"] = "Supprimer";
$MESS["WD_DELETE_FILE_ALT"] = "Supprimer le document";
$MESS["WD_FILE"] = "Fichier";
?>