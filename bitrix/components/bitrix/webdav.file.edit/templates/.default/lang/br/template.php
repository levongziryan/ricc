<?
$MESS["WD_NAME"] = "Nome";
$MESS["WD_FILE"] = "Arquivo";
$MESS["WD_FILE_REPLACE"] = "Substituir arquivo";
$MESS["WD_FILE_MODIFIED"] = "Modificado";
$MESS["WD_FILE_SIZE"] = "Tamanho";
$MESS["WD_DOWNLOAD_FILE"] = "Baixar";
$MESS["WD_DOCUMENT"] = "Documento";
$MESS["WD_DOCUMENT_ALT"] = "Propriedades do Documento";
$MESS["WD_EDIT_MSOFFICE"] = "Editar arquivo no MS Office";
$MESS["WD_DOWNLOAD_ELEMENT"] = "Baixar";
$MESS["WD_GO_BACK"] = "Voltar";
$MESS["WD_GO_BACK_ALT"] = "Voltar para documentos de biblioteca";
$MESS["WD_SAVE"] = "SALVAR alterações";
$MESS["WD_CANCEL"] = "Cancelar";
$MESS["WD_END_EDIT"] = "Substituir o documento atual";
$MESS["WD_DELETE_FILE"] = "Excluir";
$MESS["WD_DELETE_FILE_ALT"] = "Excluir documento";
$MESS["BPAT_COMMENT"] = "Comentários";
$MESS["BPATL_DOC_HISTORY"] = "Histórico de Alterações do documento";
$MESS["WD_DOWNLOAD"] = "Baixar";
$MESS["WD_ERROR_BAD_ACTION"] = "A ação da pasta não é especificada.";
$MESS["W_IBLOCK_IS_NOT_INSTALLED"] = "Módulo de blocos de informação não foi instalado";
$MESS["WD_USE_COMMENTS"] = "Permitir comentários";
$MESS["WD_PAGE_NAME"] = "Id de página de componente complexo";
$MESS["WD_ELEMENT_ADD_ALT"] = "Criar MS Word";
?>