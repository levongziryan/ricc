<?
$MESS["NM_MORE"] = "Detalhes";
$MESS["NM_EMPTY"] = "Não há novas notificações";
$MESS["NM_FORMAT_DATE"] = "g: i a, d F Y";
$MESS["NM_FORMAT_TIME"] = "g: i a";
$MESS["NM_PULLTEXT"] = "Puxe para atualizar";
$MESS["NM_DOWNTEXT"] = "Solte para atualizar";
$MESS["NM_LOADTEXT"] = "descarregando...";
$MESS["NM_TITLE"] = "Notificações";
?>