<?
$MESS["NM_FORMAT_TIME"] = "g:i a";
$MESS["NM_FORMAT_DATE"] = "g:i a, d F Y";
$MESS["NM_EMPTY"] = "Il n'y a pas de nouvelles notifications";
$MESS["NM_DOWNTEXT"] = "Relâcher pour mettre à jour";
$MESS["NM_LOADTEXT"] = "Mise à jour...";
$MESS["NM_MORE"] = "Détails";
$MESS["NM_PULLTEXT"] = "Tirer pour mettre à jour";
$MESS["NM_TITLE"] = "Alertes";
?>