<?
$MESS["NM_MORE"] = "Detalles";
$MESS["NM_EMPTY"] = "No hay nuevas notificaciones";
$MESS["NM_FORMAT_DATE"] = "g:i a, d F Y";
$MESS["NM_FORMAT_TIME"] = "g:i a";
$MESS["NM_PULLTEXT"] = "Tire para actualizar...";
$MESS["NM_DOWNTEXT"] = "Suelte para actualizar";
$MESS["NM_LOADTEXT"] = "Volver a cargar...";
$MESS["NM_TITLE"] = "Notificaciones";
$MESS["NM_TITLE_2"] = "Lista de actualizacion...";
?>