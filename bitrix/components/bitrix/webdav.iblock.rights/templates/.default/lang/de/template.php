<?
$MESS["WD_TAB_PERMISSIONS"] = "Zugriff";
$MESS["WD_TAB_PERMISSIONS_ELEMENT"] = "Zugriffsberechtigungen für ein Dokument verwalten";
$MESS["WD_TAB_PERMISSIONS_SECTION"] = "Zugriffsberechtigungen für einen Ordner verwalten";
$MESS["WD_TAB_PERMISSIONS_IBLOCK"] = "Zugriffsberechtigungen für einen Informationsblock";
$MESS["WD_SAVE"] = "Speichern";
$MESS["WD_CANCEL"] = "Abbrechen";
$MESS["WD_CHANGE"] = "Ändern";
$MESS["WD_ADD_PERMISSION"] = "Hinzufügen";
$MESS["WD_INHERITED_FROM_SECTION"] = "Vererbt vom Ordner \"#NAME#\"";
$MESS["WD_INHERITED_FROM_IBLOCK"] = "Vererbt vom Root von \"#NAME#\"";
?>