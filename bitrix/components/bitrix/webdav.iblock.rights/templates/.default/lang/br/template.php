<?
$MESS["WD_TAB_PERMISSIONS"] = "Acesso";
$MESS["WD_TAB_PERMISSIONS_ELEMENT"] = "Acesso a permissões de documentos";
$MESS["WD_TAB_PERMISSIONS_SECTION"] = "Acesso a permissões de pasta";
$MESS["WD_TAB_PERMISSIONS_IBLOCK"] = "Informações de permissões de acesso de bloco";
$MESS["WD_SAVE"] = "SALVAR";
$MESS["WD_CANCEL"] = "Cancelar";
$MESS["WD_CHANGE"] = "Editar";
$MESS["WD_ADD_PERMISSION"] = "Adicionar";
$MESS["WD_INHERITED_FROM_SECTION"] = "Herdado de pasta \"#NAME#\"";
$MESS["WD_INHERITED_FROM_IBLOCK"] = "Herdado da raiz de \"#NAME#\"";
$MESS["IB_MODULE_IS_NOT_INSTALLED"] = "Módulo de blocos de informações não está instalado.";
$MESS["WD_FOLDER_EMPTY"] = "Caminho para a pasta não foi especificado.";
$MESS["WD_ERROR_1"] = "O módulo mod_rewrite não está carregado.Â Solução: mudar a configuração do Apache.";
$MESS["WD_UPLOAD"] = "Substituir o documento atual ...";
$MESS["WD_IBLOCK_USER_ID"] = "Informação sobre bloqueio de documentos de usuários";
$MESS["WD_NAME_TEMPLATE"] = "Formato do nome";
$MESS["WD_DISK_JS_ERROR_NOT_EMPTY"] = "A pasta (#PATH#) já tem arquivos nele.";
$MESS["WD_BASE_URL"] = "URL da biblioteca para mapeamento de unidades de rede (caminho completo)";
?>