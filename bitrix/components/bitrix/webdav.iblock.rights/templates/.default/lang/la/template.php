<?
$MESS["WD_TAB_PERMISSIONS"] = "Acceso";
$MESS["WD_TAB_PERMISSIONS_ELEMENT"] = "Permisos de acceso para el documento";
$MESS["WD_TAB_PERMISSIONS_SECTION"] = "Permisos de acceso para la carpeta";
$MESS["WD_TAB_PERMISSIONS_IBLOCK"] = "Permisos de acceso para el block de información";
$MESS["WD_SAVE"] = "Guardar";
$MESS["WD_CANCEL"] = "Cancelar";
$MESS["WD_CHANGE"] = "Editar";
$MESS["WD_ADD_PERMISSION"] = "Agregar";
$MESS["WD_INHERITED_FROM_SECTION"] = "Se hereda de la carpeta \"#NAME#\"";
$MESS["WD_INHERITED_FROM_IBLOCK"] = "Hereda de la raíz de \"#NAME#\"";
?>