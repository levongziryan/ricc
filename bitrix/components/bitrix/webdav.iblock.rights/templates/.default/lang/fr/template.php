<?
$MESS["WD_ADD_PERMISSION"] = "Ajouter";
$MESS["WD_TAB_PERMISSIONS"] = "Accès";
$MESS["WD_CHANGE"] = "Editer";
$MESS["WD_INHERITED_FROM_IBLOCK"] = "Dossier initial:";
$MESS["WD_INHERITED_FROM_SECTION"] = "Hérité de dossier '#NAME#'";
$MESS["WD_CANCEL"] = "Annuler";
$MESS["WD_TAB_PERMISSIONS_ELEMENT"] = "Droits d'accès au document '#NAME#'";
$MESS["WD_TAB_PERMISSIONS_IBLOCK"] = "Droits d'accès au bloc d'information";
$MESS["WD_TAB_PERMISSIONS_SECTION"] = "Droits d'accès au dossier";
$MESS["WD_SAVE"] = "Sauvegarder";
?>