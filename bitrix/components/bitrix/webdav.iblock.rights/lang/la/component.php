<?
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "El módulo de la biblioteca del Documento no está instalado.";
$MESS["WD_PERMS_NO_IBLOCK_ID"] = "El block de Información no está especificado.";
$MESS["WD_PERMS_NO_ENTITY_TYPE"] = "El tipo de entidad no está especificado.";
$MESS["WD_PERMS_NO_ENTITY_ID"] = "El ID de entidad no está especificado.";
$MESS["WD_PERMS_TITLE"] = "Permisos de Acceso";
?>