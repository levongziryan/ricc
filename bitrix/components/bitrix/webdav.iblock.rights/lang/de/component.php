<?
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "Das Modul der Dokumentenbibliothek ist nicht installiert.";
$MESS["WD_PERMS_NO_IBLOCK_ID"] = "Der Informationsblock ist nicht angegeben.";
$MESS["WD_PERMS_NO_ENTITY_TYPE"] = "Der Einheitstyp ist nicht angegeben.";
$MESS["WD_PERMS_NO_ENTITY_ID"] = "Die Einheits-ID ist nicht angegeben.";
$MESS["WD_PERMS_TITLE"] = "Zugriffsberechtigungen";
?>