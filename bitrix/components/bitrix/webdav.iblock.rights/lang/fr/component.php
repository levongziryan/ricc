<?
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "Le document intitulé «# FILENAME # 'est déjà en cours d'utilisation.";
$MESS["WD_PERMS_NO_ENTITY_ID"] = "Identifiant de l'entité inconnu.";
$MESS["WD_PERMS_NO_IBLOCK_ID"] = "Bloc d'information non spécifié.";
$MESS["WD_PERMS_NO_ENTITY_TYPE"] = "Le type de l'entité n'est pas spécifié.";
$MESS["WD_PERMS_TITLE"] = "Droits d'accès";
?>