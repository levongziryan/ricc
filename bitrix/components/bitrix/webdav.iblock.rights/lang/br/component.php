<?
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "O módulo de biblioteca de documentos não está instalado.";
$MESS["WD_PERMS_NO_IBLOCK_ID"] = "O bloco de informação não é especificada.";
$MESS["WD_PERMS_NO_ENTITY_TYPE"] = "O tipo de entidade não é especificado.";
$MESS["WD_PERMS_NO_ENTITY_ID"] = "O ID da entidade não é especificado.";
$MESS["WD_PERMS_TITLE"] = "Como permissões de Acesso";
$MESS["WD_WF_ATTENTION1"] = "Atenção! Você não pode modificar os arquivos enviados.";
$MESS["IU_ATTENTION"] = "Atenção!";
$MESS["WD_EDIT_SECTION"] = "Editar pasta #NAME#";
$MESS["WD_WF_ATTENTION2"] = "Você pode editar arquivos no Estado Estado #STATUS#.";
?>