<?
$MESS["VOXIMPLANT_PERM_ROLE"] = "Rôle";
$MESS["VOXIMPLANT_PERM_ADD_ACCESS_CODE"] = "Ajouter une autorisation d'accès";
$MESS["VOXIMPLANT_PERM_ROLE_LIST"] = "Rôles";
$MESS["VOXIMPLANT_PERM_DELETE"] = "Supprimer";
$MESS["VOXIMPLANT_PERM_EDIT"] = "Modifier";
$MESS["VOXIMPLANT_PERM_ADD"] = "Ajouter";
$MESS["VOXIMPLANT_PERM_SAVE"] = "Enregistrer";
$MESS["VOXIMPLANT_PERM_ERROR"] = "Erreur";
$MESS["VOXIMPLANT_PERM_ROLE_DELETE_ERROR"] = "Erreur de suppression du rôle.";
$MESS["VOXIMPLANT_PERM_ROLE_DELETE"] = "Supprimer le rôle";
$MESS["VOXIMPLANT_PERM_ROLE_DELETE_CONFIRM"] = "Voulez-vous vraiment supprimer le rôle?";
$MESS["VOXIMPLANT_PERM_ROLE_OK"] = "OK";
$MESS["VOXIMPLANT_PERM_ROLE_CANCEL"] = "Annuler";
?>