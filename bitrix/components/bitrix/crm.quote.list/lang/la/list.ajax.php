<?
$MESS["CRM_QUOTE_LIST_ROW_COUNT"] = "Total: #ROW_COUNT#";
$MESS["CRM_QUOTE_LIST_REBUILD_SEARCH_CONTENT_NOT_REQUIRED_SUMMARY"] = "El índice de búsqueda de cotizaciones no necesita ser recreado.";
$MESS["CRM_QUOTE_LIST_REBUILD_SEARCH_CONTENT_PROGRESS_SUMMARY"] = "Cotizaciones procesadas: #PROCESSED_ITEMS# of #TOTAL_ITEMS#.";
$MESS["CRM_QUOTE_LIST_REBUILD_SEARCH_CONTENT_COMPLETED_SUMMARY"] = "El índice de búsqueda de cotizaciones se ha recreado.Cotizaciones procesadas: #PROCESSED_ITEMS#.";
?>