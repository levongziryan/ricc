<?
$MESS["TASKS_MODULE_NOT_AVAILABLE_IN_THIS_EDITION"] = "Das Modul Aufgaben ist in dieser Produktedition nicht verfügbar.";
$MESS["MB_TASKS_TASK_DETAIL_TASK_NOT_ACCESSIBLE"] = "Aufgabe ##TASK_ID# existiert nicht oder Sie haben nicht genügend Rechte diese Aufgabe anzuzeigen.";
$MESS["MB_TASKS_TASK_DETAIL_STATUS_NEW"] = "Neu";
$MESS["MB_TASKS_TASK_DETAIL_STATUS_ACCEPTED"] = "Angenommene";
$MESS["MB_TASKS_TASK_DETAIL_STATUS_IN_PROGRESS"] = "In Arbeit";
$MESS["MB_TASKS_TASK_DETAIL_STATUS_WAITING"] = "Muss kontrolliert werden";
$MESS["MB_TASKS_TASK_DETAIL_STATUS_COMPLETED"] = "Abgeschlossene";
$MESS["MB_TASKS_TASK_DETAIL_STATUS_DELAYED"] = "Verschobene";
$MESS["MB_TASKS_TASK_DETAIL_STATUS_DECLINED"] = "Abgelehnte";
$MESS["MB_TASKS_TASK_DETAIL_STATUS_OVERDUE"] = "Überfällige";
$MESS["MB_TASKS_TASK_DETAIL_STATUS_DATE_PREPOSITION"] = "seit";
?>