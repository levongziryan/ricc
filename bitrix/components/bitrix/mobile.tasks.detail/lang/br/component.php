<?
$MESS["TASKS_MODULE_NOT_AVAILABLE_IN_THIS_EDITION"] = "O módulo de Tarefas não está disponível nesta edição.";
$MESS["MB_TASKS_TASK_DETAIL_TASK_NOT_ACCESSIBLE"] = "A tarefa ##TASK_ID# não existe ou você não tem permissões suficientes.";
$MESS["MB_TASKS_TASK_DETAIL_STATUS_NEW"] = "Novo";
$MESS["MB_TASKS_TASK_DETAIL_STATUS_ACCEPTED"] = "Aceito";
$MESS["MB_TASKS_TASK_DETAIL_STATUS_IN_PROGRESS"] = "Em andamento";
$MESS["MB_TASKS_TASK_DETAIL_STATUS_WAITING"] = "Aguardando controle";
$MESS["MB_TASKS_TASK_DETAIL_STATUS_COMPLETED"] = "Concluído";
$MESS["MB_TASKS_TASK_DETAIL_STATUS_DELAYED"] = "Deferido";
$MESS["MB_TASKS_TASK_DETAIL_STATUS_DECLINED"] = "Recusado";
$MESS["MB_TASKS_TASK_DETAIL_STATUS_OVERDUE"] = "Atrasado";
$MESS["MB_TASKS_TASK_DETAIL_STATUS_DATE_PREPOSITION"] = "de";
?>