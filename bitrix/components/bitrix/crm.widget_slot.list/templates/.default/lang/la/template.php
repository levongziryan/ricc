<?
$MESS["CRM_WGT_SLST_LRP_DLG_BTN_START"] = "Empezar";
$MESS["CRM_WGT_SLST_LRP_DLG_BTN_STOP"] = "Parar";
$MESS["CRM_WGT_SLST_LRP_DLG_BTN_CLOSE"] = "Cerrar";
$MESS["CRM_WGT_SLST_DLG_WAIT"] = "Por favor, espere...";
$MESS["CRM_WGT_SLST_LRP_DLG_REQUEST_ERR"] = "Error al procesar la solicitud.";
$MESS["CRM_WGT_SLST_GENERAL_INTRO"] = "Seleccione en esta página los campos personalizados para incluir en los reportes analíticos del CRM.";
$MESS["CRM_WGT_SLST_LIMITS_INTRO"] = "Se pueden seleccionar hasta diez campos; hasta cinco del mismo tipo.";
$MESS["CRM_WGT_SLST_LIMITS_TOTALS"] = "Seleccionado: #TOTAL# de #OVERALL#";
$MESS["CRM_WGT_SLST_NOT_SELECTED"] = "No seleccionado";
$MESS["CRM_WGT_SLST_BY_DEFALT"] = "Por defecto";
$MESS["CRM_WGT_SLST_TOTAL_SUM"] = "Suma total";
$MESS["CRM_WGT_SLST_USER_FIELDS"] = "Campos personalizados";
$MESS["CRM_WGT_SLST_ADD_PRODUCT_SUM"] = "Agregar cantidad de productos";
$MESS["CRM_WGT_SLST_ADD"] = "Agregar";
$MESS["CRM_WGT_SLST_EDIT"] = "Editar";
$MESS["CRM_WGT_SLST_REMOVE"] = "Eliminar";
$MESS["CRM_WGT_SLST_SAVE"] = "Guardar";
$MESS["CRM_WGT_SLST_CALCEL"] = "Cancelar";
$MESS["CRM_WGT_SLST_RESET"] = "Reiniciar";
$MESS["CRM_WGT_SLST_ERR_SELECT_FIELD"] = "Por favor, seleccione un campo para procesar.";
$MESS["CRM_WGT_SLST_ERR_FIELD_ALREADY_EXISTS"] = "Por favor seleccione otro campo. El \"#FIELD#\" campo ya está en uso.";
$MESS["CRM_WGT_SLST_ERR_FIELD_LIMT_EXCEEDED"] = "No se puede agregar un nuevo campo, porque ya agregó el máximos de campos posibles.";
$MESS["CRM_WGT_SLST_ERR_NO_FREE_SLOTS"] = "No puede agregar un nuevo campo porque no hay ranuras libres.";
$MESS["CRM_WGT_SLST_NODE_TOOLTIP"] = "Si utiliza un campo personalizado para la cantidad total de la negociación, especifíquelo aquí. El total de los productos de la negociación se pueden agregar a este campo. De forma predeterminada, los reportes se crean utilizando el campo \"Total\" de negociaciones.";
?>