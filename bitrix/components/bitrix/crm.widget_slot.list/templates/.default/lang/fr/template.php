<?
$MESS["CRM_WGT_SLST_LRP_DLG_BTN_START"] = "Exécuter";
$MESS["CRM_WGT_SLST_LRP_DLG_BTN_STOP"] = "Arrêter";
$MESS["CRM_WGT_SLST_LRP_DLG_BTN_CLOSE"] = "Fermer";
$MESS["CRM_WGT_SLST_DLG_WAIT"] = "Veuillez patienter...";
$MESS["CRM_WGT_SLST_LRP_DLG_REQUEST_ERR"] = "Erreur lors du traitement de la requête.";
$MESS["CRM_WGT_SLST_GENERAL_INTRO"] = "Sélectionnez sur cette page les champs personnalisés à inclure dans les rapports analytiques CRM.";
$MESS["CRM_WGT_SLST_LIMITS_TOTALS"] = "Sélectionné : #TOTAL# sur #OVERALL#";
$MESS["CRM_WGT_SLST_NOT_SELECTED"] = "Pas sélectionné";
$MESS["CRM_WGT_SLST_BY_DEFALT"] = "Par défaut";
$MESS["CRM_WGT_SLST_TOTAL_SUM"] = "Somme totale";
$MESS["CRM_WGT_SLST_USER_FIELDS"] = "Champs personnalisés";
$MESS["CRM_WGT_SLST_ADD_PRODUCT_SUM"] = "Ajouter un montant de produit";
$MESS["CRM_WGT_SLST_ADD"] = "Ajouter";
$MESS["CRM_WGT_SLST_EDIT"] = "Éditer";
$MESS["CRM_WGT_SLST_REMOVE"] = "Supprimer";
$MESS["CRM_WGT_SLST_SAVE"] = "Enregistrer";
$MESS["CRM_WGT_SLST_CALCEL"] = "Annuler";
$MESS["CRM_WGT_SLST_RESET"] = "Réinitialiser";
$MESS["CRM_WGT_SLST_ERR_SELECT_FIELD"] = "Veuillez sélectionner un champ à traiter.";
$MESS["CRM_WGT_SLST_ERR_FIELD_ALREADY_EXISTS"] = "Veuillez sélectionner un autre champ Le champ \"#FIELD#\" est déjà en cours d'utilisation.";
$MESS["CRM_WGT_SLST_ERR_FIELD_LIMT_EXCEEDED"] = "Impossible d'ajouter un nouveau champ parce que vous atteint le maximum à ajouter.";
$MESS["CRM_WGT_SLST_ERR_NO_FREE_SLOTS"] = "Impossible d'ajouter un nouveau champ parce qu'il n'y a plus d'emplacement libre.";
$MESS["CRM_WGT_SLST_NODE_TOOLTIP"] = "Précisez ici si vous utilisez un champ personnalisé pour le montant total de la transaction. Le total des produits de la transaction peut être ajouté à ce champ. Par défaut, les rapports sont créés à partir du champ \"Total\" de la transaction.";
$MESS["CRM_WGT_SLST_LIMITS_INTRO"] = "Jusqu'à dix champs peuvent être sélectionnés (dont jusqu'à cinq du même type).";
?>