<?
$MESS["TASKS_PRIORITY"] = "(niveau d'importance)";
$MESS["TASKS_STATUS_W"] = "Attend l'exécution";
$MESS["TASKS_STATUS_I"] = "En cours";
$MESS["TASKS_PRIORITY_H"] = "Augmenté";
$MESS["TASKS_STATUS_C"] = "Achevé(e)s";
$MESS["SOCNET_MODULE_NOT_INSTALLED"] = "Module 'Réseau social' n'a pas été installé.";
$MESS["TASKS_MODULE_NOT_INSTALLED"] = "Le module 'Tâches' n'a pas été installé.";
$MESS["TASKS_STATUS_S"] = "N'a pas commencé";
$MESS["TASKS_STATUS_N"] = "Non accepté(e)";
$MESS["TASKS_PRIORITY_L"] = "Bas";
$MESS["TASKS_TITLE_CREATE_TEMPLATE"] = "Un nouveau modèle";
$MESS["TASKS_PRIORITY_N"] = "Moyen";
$MESS["TASKS_STATUS_D"] = "Différé";
$MESS["TASKS_TITLE_EDIT_TEMPLATE"] = "Modification du modèle ##TEMPLATE_ID#";
$MESS["TASKS_TEMPLATE_NOT_FOUND"] = "L'ID de test est pas spécifié.";
?>