<?
$MESS["TASKS_PRIORITY"] = "(prioridad)";
$MESS["TASKS_PRIORITY_L"] = "Bajo";
$MESS["TASKS_PRIORITY_N"] = "Normal";
$MESS["TASKS_PRIORITY_H"] = "Alto";
$MESS["TASKS_STATUS_N"] = "No es aceptado";
$MESS["TASKS_STATUS_S"] = "No ha Iniciado";
$MESS["TASKS_STATUS_I"] = "En Progreso";
$MESS["TASKS_STATUS_C"] = "Finalizado";
$MESS["TASKS_STATUS_W"] = "Pendiente";
$MESS["TASKS_STATUS_D"] = "Diferir";
$MESS["TASKS_MODULE_NOT_INSTALLED"] = "El módulo Task no está instalado.";
$MESS["SOCNET_MODULE_NOT_INSTALLED"] = "El módulo \"Social Network\" no está instalado.";
$MESS["TASKS_TEMPLATE_NOT_FOUND"] = "La plantilla no fue encontrada o el acceso está denegado.";
$MESS["TASKS_TITLE_EDIT_TEMPLATE"] = "Editar Plantilla ¹#TEMPLATE_ID#";
$MESS["TASKS_TITLE_CREATE_TEMPLATE"] = "Nueva Plantilla";
?>