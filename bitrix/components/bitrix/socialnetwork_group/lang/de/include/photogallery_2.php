<?
$MESS["SONET_PHOTO_LOG_1"] = "#AUTHOR_NAME# hat eine Datei hinzugefügt #TITLE# ";
$MESS["SONET_PHOTO_LOG_2"] = "Fotos (#COUNT#)";
$MESS["SONET_PHOTO_LOG_TEXT"] = "Neue Fotos: <div class='notificationlog'>#LINKS#</div> <a href=\"#HREF#\">Zum Album</a>.";
$MESS["SONET_PHOTO_LOG_MAIL_TEXT"] = "Neue Fotos: #LINKS# und andere.";
$MESS["SONET_LOG_GUEST"] = "Gast";
?>