<?
$MESS["CT_BLL_SELECTED"] = "Anzahl Einträge";
$MESS["TEL_STAT_BACK"] = "Zurück";
$MESS["TEL_STAT_BACK_TITLE"] = "Zurück";
$MESS["TEL_STAT_USER_ID_CANCEL"] = "Filter nach Mitarbeiter zurücksetzen";
$MESS["TEL_STAT_USER_ID_CANCEL_TITLE"] = "Filter nach Mitarbeiter zurücksetzen";
$MESS["TEL_STAT_FILTER_CANCEL"] = "Filter zurücksetzen";
$MESS["TEL_STAT_FILTER_CANCEL_TITLE"] = "Filter zurücksetzen";
$MESS["TEL_STAT_EXPORT_TO_EXCEL"] = "Nach Excel exportieren";
?>