<?
$MESS["CT_BLL_SELECTED"] = "Registros seleccionados";
$MESS["TEL_STAT_BACK"] = "Atrás";
$MESS["TEL_STAT_BACK_TITLE"] = "Atrás";
$MESS["TEL_STAT_USER_ID_CANCEL"] = "Reiniciar filtro empleado";
$MESS["TEL_STAT_USER_ID_CANCEL_TITLE"] = "Reiniciar filtro empleado";
$MESS["TEL_STAT_FILTER_CANCEL"] = "Restablecer filtro";
$MESS["TEL_STAT_FILTER_CANCEL_TITLE"] = "Restablecer filtro";
$MESS["TEL_STAT_EXPORT_TO_EXCEL"] = "Exportar a Excel";
?>