<?
$MESS["TEL_STAT_BACK_TITLE"] = "Précédent";
$MESS["TEL_STAT_BACK"] = "Précédent";
$MESS["TEL_STAT_USER_ID_CANCEL"] = "Filtre employé Réinitialiser";
$MESS["TEL_STAT_USER_ID_CANCEL_TITLE"] = "Filtre employé Réinitialiser";
$MESS["CT_BLL_SELECTED"] = "Le nombre d'enregistrements";
$MESS["TEL_STAT_FILTER_CANCEL"] = "Réinitialiser le filtre";
$MESS["TEL_STAT_FILTER_CANCEL_TITLE"] = "Réinitialiser le filtre";
$MESS["TEL_STAT_EXPORT_TO_EXCEL"] = "Exporter vers Excel";
?>