<?
$MESS["TELEPHONY_HEADER_USER"] = "Empleado";
$MESS["TELEPHONY_HEADER_PHONE"] = "Teléfono";
$MESS["TELEPHONY_HEADER_INCOMING"] = "Tipo de la llamada";
$MESS["TELEPHONY_HEADER_DURATION"] = "Duración de la llamada";
$MESS["TELEPHONY_HEADER_START_DATE"] = "Fecha de la llamada";
$MESS["TELEPHONY_HEADER_STATUS"] = "Estatus";
$MESS["TELEPHONY_HEADER_COST"] = "Costo";
$MESS["TELEPHONY_STATUS_1"] = "Crédito de bonificación de registro cargado. Para más detalles, póngase en contacto con soporte técnico.";
$MESS["TELEPHONY_STATUS_2"] = "Crédito de bonificación de registro cargado";
$MESS["TELEPHONY_STATUS_3"] = "Crédito top-up";
$MESS["TELEPHONY_STATUS_409"] = "Saldo corregido después de que fuera revisado por un especialista. Para más detalles, póngase en contacto con soporte técnico.";
$MESS["TELEPHONY_BILLING"] = "Facturación";
$MESS["TELEPHONY_HEADER_RECORD"] = "Registro";
$MESS["TELEPHONY_HEADER_PORTAL_PHONE"] = "Número de teléfono del portal";
$MESS["TELEPHONY_PORTAL_PHONE_EMPTY"] = "No identificado";
$MESS["TELEPHONY_HEADER_LOG"] = "Detalles";
$MESS["TELEPHONY_PORTAL_PHONE_SIP_OFFICE"] = "Oficina BPX (#ID#)";
$MESS["TELEPHONY_PORTAL_PHONE_SIP_CLOUD"] = "Hosted PBX en la nube (#ID#)";
$MESS["TELEPHONY_HEADER_VOTE"] = "Evaluación";
?>