<?
$MESS["TASKS_FILTER"] = "Filtrar";
$MESS["TASKS_FILTER_TITLE"] = "Minhas Tarefas";
$MESS["TASKS_FILTER_COMMON"] = "Normal";
$MESS["TASKS_FILTER_EXTENDED"] = "Extendido";
$MESS["TASKS_FILTER_STATUSES"] = "Status:";
$MESS["TASKS_FILTER_STATUS"] = "Status";
$MESS["TASKS_FILTER_BY_TAG"] = "Por Tag";
$MESS["TASKS_FILTER_SELECT"] = "selecionar";
$MESS["TASKS_FILTER_CREAT_DATE"] = "Criado em";
$MESS["TASKS_FILTER_PICK_DATE"] = "Selecione a data no calendário";
$MESS["TASKS_FILTER_SHOW_SUBORDINATE"] = "mostrar as tarefas dos subordinados";
$MESS["TASKS_FILTER_FIND"] = "Pesquisar";
$MESS["TASKS_FILTER_CLOSE_DATE"] = "Encerrada em";
$MESS["TASKS_FILTER_ACTIVE_DATE"] = "Estava ativo";
$MESS["TASKS_FILTER_MARKED"] = "avaliado";
$MESS["TASKS_FILTER_ADV_IN_REPORT"] = "em relatório";
$MESS["TASKS_FILTER_OVERDUED"] = "atrasado";
$MESS["TASKS_CANCEL"] = "Cancelar";
$MESS["TASKS_FILTER_WORKGROUP"] = "Workgroup";
$MESS["TASKS_FILTER_ID"] = "ID";
?>