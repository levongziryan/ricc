<?
$MESS["TASKS_FILTER_TITLE"] = "Mis tareas";
$MESS["TASKS_FILTER"] = "Filtro";
$MESS["TASKS_FILTER_COMMON"] = "Normal";
$MESS["TASKS_FILTER_EXTENDED"] = "Extendido";
$MESS["TASKS_FILTER_STATUSES"] = "Estados ";
$MESS["TASKS_FILTER_STATUS"] = "Estado";
$MESS["TASKS_FILTER_BY_TAG"] = "Por etiqueta";
$MESS["TASKS_FILTER_SELECT"] = "seleccionar";
$MESS["TASKS_FILTER_CREAT_DATE"] = "Creado el";
$MESS["TASKS_FILTER_PICK_DATE"] = "Seleccionar fecha en el calendario";
$MESS["TASKS_FILTER_SHOW_SUBORDINATE"] = "Mostrar las tareas de los subordinados";
$MESS["TASKS_FILTER_FIND"] = "Búsqueda";
$MESS["TASKS_FILTER_CLOSE_DATE"] = "Cerrado en";
$MESS["TASKS_FILTER_ACTIVE_DATE"] = "Estar activo";
$MESS["TASKS_FILTER_MARKED"] = "evaluar";
$MESS["TASKS_FILTER_ADV_IN_REPORT"] = "en el reporte";
$MESS["TASKS_FILTER_OVERDUED"] = "atrasado";
$MESS["TASKS_CANCEL"] = "Cancelar";
$MESS["TASKS_FILTER_WORKGROUP"] = "Grupo de trabajo";
$MESS["TASKS_FILTER_ID"] = "ID";
?>