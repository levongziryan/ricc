<?
$MESS["REST_CRM_FF_LEAD"] = "Pistes";
$MESS["REST_CRM_FF_CONTACT"] = "Contacts";
$MESS["REST_CRM_FF_COMPANY"] = "Sociétés";
$MESS["REST_CRM_FF_DEAL"] = "Transactions";
$MESS["REST_CRM_FF_OK"] = "Sélectionner";
$MESS["REST_CRM_FF_CANCEL"] = "Annuler";
$MESS["REST_CRM_FF_CLOSE"] = "Fermer";
$MESS["REST_CRM_FF_SEARCH"] = "Recherche";
$MESS["REST_CRM_FF_NO_RESULT"] = "Malheureusement, votre demande de recherche n'a donné aucun résultat.";
$MESS["REST_CRM_FF_CHOISE"] = "Sélectionner";
$MESS["REST_CRM_FF_CHANGE"] = "Editer";
$MESS["REST_CRM_FF_LAST"] = "Dernier";
$MESS["REST_CRM_FF_QUOTE"] = "Devis";
?>