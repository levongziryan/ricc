<?
$MESS["CRM_COMPANY_EDIT_EVENT_SUCCESSFULLY_CREATED"] = "La société <a href='#URL#'>#TITLE#</a> a bien été créée. Vous allez maintenant être redirigé vers la page précédente. Si vous êtes toujours sur cette page, veuillez la fermer manuellement.";
$MESS["CRM_COMPANY_EDIT_EVENT_CANCELED"] = "L'action a été annulée. Vous allez maintenant être redirigé vers la page précédente. Si vous êtes toujours sur cette page, veuillez la fermer manuellement.";
?>