<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["CRM_COUNTER_ENTITY_TYPE_NOT_DEFINED"] = "O tipo de entidade não está especificado..";
$MESS["CRM_COUNTER_DEAL_CAPTION"] = "Negociações";
$MESS["CRM_COUNTER_LEAD_CAPTION"] = "Prospectos";
$MESS["CRM_COUNTER_CONTACT_CAPTION"] = "Contatos";
$MESS["CRM_COUNTER_COMPANY_CAPTION"] = "Empresas";
$MESS["CRM_COUNTER_DEAL_STUB"] = "Não há nenhuma negociação que necessite de atenção imediata.";
$MESS["CRM_COUNTER_LEAD_STUB"] = "Não há nenhum cliente potencial que necessite de atenção imediata.";
$MESS["CRM_COUNTER_CONTACT_STUB"] = "Não há nenhum contato que necessite de atenção imediata.";
$MESS["CRM_COUNTER_COMPANY_STUB"] = "Não há nenhuma empresa que necessite de atenção imediata.";
$MESS["CRM_COUNTER_TYPE_PENDING"] = "#VALUE# atividades de hoje";
$MESS["CRM_COUNTER_TYPE_OVERDUE"] = "#VALUE# atrasadas";
$MESS["CRM_COUNTER_TYPE_IDLE"] = "#VALUE# sem atividades programadas";
?>