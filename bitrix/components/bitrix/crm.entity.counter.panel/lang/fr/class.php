<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_COUNTER_ENTITY_TYPE_NOT_DEFINED"] = "Le type d'entité n'est pas spécifié.";
$MESS["CRM_COUNTER_DEAL_CAPTION"] = "Affaires";
$MESS["CRM_COUNTER_LEAD_CAPTION"] = "Client potentiels";
$MESS["CRM_COUNTER_CONTACT_CAPTION"] = "Contacts";
$MESS["CRM_COUNTER_COMPANY_CAPTION"] = "Entreprises";
$MESS["CRM_COUNTER_DEAL_STUB"] = "Aucune affaire ne nécessite une attention immédiate.";
$MESS["CRM_COUNTER_LEAD_STUB"] = "Aucune client potentiel ne nécessite une attention immédiate.";
$MESS["CRM_COUNTER_CONTACT_STUB"] = "Aucun contact ne nécessite une attention immédiate.";
$MESS["CRM_COUNTER_COMPANY_STUB"] = "Aucune entreprise ne nécessite une attention immédiate.";
$MESS["CRM_COUNTER_TYPE_PENDING"] = "#VALUE# activités du jour";
$MESS["CRM_COUNTER_TYPE_OVERDUE"] = "#VALUE# en retard";
$MESS["CRM_COUNTER_TYPE_IDLE"] = "#VALUE# sans activités prévues";
?>