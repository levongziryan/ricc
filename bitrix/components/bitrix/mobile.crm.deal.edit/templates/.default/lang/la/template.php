<?
$MESS["M_CRM_DEAL_EDIT_CREATE_TITLE"] = "Nueva negociación";
$MESS["M_CRM_DEAL_EDIT_CANCEL_BTN"] = "Cancelar";
$MESS["M_CRM_DEAL_EDIT_SAVE_BTN"] = "Guardar";
$MESS["M_CRM_DEAL_EDIT_CONTINUE_BTN"] = "Continuar";
$MESS["M_CRM_DEAL_EDIT_EDIT_TITLE"] = "Editar";
$MESS["M_CRM_DEAL_EDIT_VIEW_TITLE"] = "Ver negociación";
$MESS["M_CRM_DEAL_EDIT_CONVERT_TITLE"] = "Negociación";
$MESS["M_DETAIL_PULL_TEXT"] = "Pulsar para catualizar...";
$MESS["M_DETAIL_DOWN_TEXT"] = "Soltar para actualizar...";
$MESS["M_DETAIL_LOAD_TEXT"] = "Actualizando...";
$MESS["M_CRM_DEAL_MENU_EDIT"] = "Editar";
$MESS["M_CRM_DEAL_MENU_DELETE"] = "Eliminar";
$MESS["M_CRM_DEAL_CONVERSION_NOTIFY"] = "Campos requeridos";
$MESS["M_CRM_DEAL_MENU_CREATE_ON_BASE"] = "Crear usando el origen";
$MESS["M_CRM_DEAL_MENU_HISTORY"] = "Historial";
$MESS["M_CRM_DEAL_MENU_ACTIVITY"] = "Actividades";
?>