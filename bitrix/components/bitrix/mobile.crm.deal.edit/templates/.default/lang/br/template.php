<?
$MESS["M_CRM_DEAL_EDIT_PULL_TEXT"] = "Puxe para baixo para atualizar...";
$MESS["M_CRM_DEAL_EDIT_DOWN_TEXT"] = "Solte para atualizar...";
$MESS["M_CRM_DEAL_EDIT_LOAD_TEXT"] = "Atualizando...";
$MESS["M_CRM_DEAL_EDIT_CREATE_TITLE"] = "Novo negócio";
$MESS["M_CRM_DEAL_EDIT_UPDATE_TITLE"] = "Editar negócio";
$MESS["M_CRM_DEAL_EDIT_SECTION_GENERAL"] = "Informações gerais";
$MESS["M_CRM_DEAL_EDIT_CURRENCY"] = "Moeda";
$MESS["M_CRM_DEAL_EDIT_OPPORTUNITY"] = "Total";
$MESS["M_CRM_DEAL_EDIT_FIELD_HINT_REQUIRED"] = "Campo obrigatório";
$MESS["M_CRM_DEAL_EDIT_FIELD_TITLE"] = "Nome";
$MESS["M_CRM_DEAL_EDIT_FIELD_TYPE"] = "Tipo";
$MESS["M_CRM_DEAL_EDIT_PROBABILITY"] = "Probabilidade,%";
$MESS["M_CRM_DEAL_EDIT_FIELD_RESPONSIBLE"] = "Responsável";
$MESS["M_CRM_DEAL_EDIT_FIELD_COMMENTS"] = "Comentar";
$MESS["M_CRM_DEAL_EDIT_FIELD_TYPE_NOT_SPECIFIED"] = "Não especificado";
$MESS["M_CRM_DEAL_EDIT_FIELD_RESPONSIBLE_NOT_SPECIFIED"] = "Não especificado";
$MESS["M_CRM_DEAL_EDIT_CREATE_BTN"] = "Criar";
$MESS["M_CRM_DEAL_EDIT_UPDATE_BTN"] = "Salvar";
$MESS["M_CRM_DEAL_EDIT_CANCEL_BTN"] = "Cancelar";
$MESS["M_CRM_DEAL_EDIT_USER_SELECTOR_CANCEL_BTN"] = "Cancelar";
$MESS["M_CRM_DEAL_EDIT_USER_SELECTOR_OK_BTN"] = "Selecionar";
$MESS["M_CRM_DEAL_EDIT_SUM_TOTAL"] = "Total";
$MESS["M_CRM_DEAL_EDIT_PRODUCT_ROW_TEMPLATE"] = "#PRODUCT_NAME#<br/><strong>- #QUANTITY# pcs.</strong>, <strong>#FORMATTED_PRICE#</strong> cada";
$MESS["M_CRM_DEAL_EDIT_CLIENT"] = "Cliente";
$MESS["M_CRM_DEAL_EDIT_FIELD_CLIENT_NOT_SPECIFIED"] = "Não especificado";
$MESS["M_CRM_DEAL_EDIT_ADD_PRODUCT"] = "Adicionar produto";
$MESS["M_CRM_DEAL_EDIT_SAVE_BTN"] = "Salvar";
$MESS["M_CRM_DEAL_EDIT_CONTINUE_BTN"] = "Continuar";
$MESS["M_CRM_DEAL_EDIT_EDIT_TITLE"] = "Editar";
$MESS["M_CRM_DEAL_EDIT_VIEW_TITLE"] = "Ver negociação";
$MESS["M_CRM_DEAL_EDIT_CONVERT_TITLE"] = "Negociação";
$MESS["M_DETAIL_PULL_TEXT"] = "Puxe para baixo para atualizar...";
$MESS["M_DETAIL_DOWN_TEXT"] = "Solte para atualizar...";
$MESS["M_DETAIL_LOAD_TEXT"] = "Atualizando...";
$MESS["M_CRM_DEAL_MENU_EDIT"] = "Editar";
$MESS["M_CRM_DEAL_MENU_DELETE"] = "Excluir";
$MESS["M_CRM_DEAL_CONVERSION_NOTIFY"] = "Campos obrigatórios";
$MESS["M_CRM_DEAL_MENU_CREATE_ON_BASE"] = "Criar usando fonte";
$MESS["M_CRM_DEAL_MENU_HISTORY"] = "Histórico";
$MESS["M_CRM_DEAL_MENU_ACTIVITY"] = "Atividades";
?>