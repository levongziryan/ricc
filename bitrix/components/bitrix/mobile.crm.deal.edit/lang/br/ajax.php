<?
$MESS["CRM_ACCESS_DENIED"] = "Acesso negado.";
$MESS["CRM_ENTITY_TYPE_NOT_SUPPORTED"] = "TYPE_NOT_SUPPORTED: Tipo '#ENTITY_TYPE#' não é compatível com o contexto atual.";
$MESS["CRM_ENTITY_DATA_NOT_FOUND"] = "DATA_NOT_FOUND: Não há dados para processar.";
$MESS["CRM_ENTITY_ID_NOT_FOUND"] = "ID_NOT_FOUND: A ID não pôde ser encontrado.";
$MESS["CRM_DEAL_STAGE_NOT_FOUND"] = "STAGE_NOT_FOUND: Estágio não pôde ser encontrado.";
$MESS["CRM_DEAL_TITLE_NOT_ASSIGNED"] = "Por favor, insira o nome do negócio.";
$MESS["CRM_DEAL_NOT_FOUND"] = "Não foi possível encontrar negócio ##ID#.";
$MESS["CRM_DEAL_COULD_NOT_DELETE"] = "O negócio não pôde ser excluído.";
?>