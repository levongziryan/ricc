<?
$MESS["CRM_ACCESS_DENIED"] = "Acceso denegado.";
$MESS["CRM_ENTITY_TYPE_NOT_SUPPORTED"] = "TYPE_NOT_SUPPORTED: Type '#ENTITY_TYPE#' no se admite en el contexto actual.";
$MESS["CRM_ENTITY_DATA_NOT_FOUND"] = "DATA_NOT_FOUND: No hay datos para procesar.";
$MESS["CRM_ENTITY_ID_NOT_FOUND"] = "ID_NOT_FOUND: El ID no se pudo encontrar.";
$MESS["CRM_DEAL_STAGE_NOT_FOUND"] = "STAGE_NOT_FOUND: No se puedo encontrar la etapa";
$MESS["CRM_DEAL_TITLE_NOT_ASSIGNED"] = "Por favor, introduzca el nombre de la negociación.";
$MESS["CRM_DEAL_NOT_FOUND"] = "No se puede encontrar la negociación ##ID#.";
$MESS["CRM_DEAL_COULD_NOT_DELETE"] = "La negociación no pudo ser eliminada.";
?>