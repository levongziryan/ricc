<?
$MESS["LOAD_MORE_USERS"] = "Carga más";
$MESS["LOAD_MORE_RESULT"] = "Mostrar más";
$MESS["SEARCH_LOADING"] = "Buscar...";
$MESS["RECENT_SEARCH"] = "Búsqueda reciente";
$MESS["SEARCH_EMPTY_RESULT"] = "Lamentablemente, su solicitud de búsqueda no arrojó resultados";
$MESS["USER_LOADING"] = "Cargando...";
$MESS["SEARCH_PLACEHOLDER"] = "Ingrese nombre o departamento";
$MESS["ACTION_DELETE"] = "Eliminar";
$MESS["INVITE_USERS"] = "Invitar usuarios";
?>