<?
$MESS["BIZPROC_WFEDIT_ERROR_TYPE"] = "Se requiere que el tipo de documento.";
$MESS["BIZPROC_WFEDIT_DEFAULT_TITLE"] = "Plantilla de Procesos de Negocios";
$MESS["BIZPROC_WFEDIT_SAVE_ERROR"] = "Se produjo un error al guardar un objeto:";
$MESS["BIZPROC_WFEDIT_CATEGORY_MAIN"] = "Principal";
$MESS["BIZPROC_WFEDIT_CATEGORY_DOC"] = "Procesamiento de documentos";
$MESS["BIZPROC_WFEDIT_CATEGORY_CONSTR"] = "Construcciones";
$MESS["BIZPROC_WFEDIT_CATEGORY_INTER"] = "Configuración interactiva";
$MESS["BIZPROC_WFEDIT_CATEGORY_OTHER"] = "Otro";
$MESS["BIZPROC_WFEDIT_TITLE_ADD"] = "Nueva Plantilla de Procesos de Negocio";
$MESS["BIZPROC_WFEDIT_TITLE_EDIT"] = "Editar Plantilla de Procesos de Negocio";
$MESS["BIZPROC_WFEDIT_IMPORT_ERROR"] = "Error al importar la plantilla de procesos de negocio.";
$MESS["BIZPROC_USER_PARAMS_SAVE_ERROR"] = "Una o más actividades en la barra \"Mis Actividades\" son demasiado grande. Los cambios no se guardarán.";
$MESS["BIZPROC_WFEDIT_CATEGORY_REST"] = "Acciones de la aplicación";
?>