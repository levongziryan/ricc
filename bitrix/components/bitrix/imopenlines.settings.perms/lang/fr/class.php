<?
$MESS["IMOL_PERM_UNKNOWN_ACCESS_CODE"] = "(ID d'accès inconnu)";
$MESS["IMOL_PERM_UNKNOWN_SAVE_ERROR"] = "Erreur d'enregistrement des données";
$MESS["IMOL_PERM_ACCESS_DENIED"] = "Autorisations d'accès insuffisantes";
$MESS["IMOL_PERM_LICENSING_ERROR"] = "Votre abonnement ne vous permet pas de gérer les autorisations d'accès pour les Canaux ouverts.";
?>