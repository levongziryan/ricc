<?
$MESS["IMOL_PERM_ROLE"] = "Rôle";
$MESS["IMOL_PERM_ADD_ACCESS_CODE"] = "Ajouter une autorisation d'accès";
$MESS["IMOL_PERM_ROLE_LIST"] = "Rôles";
$MESS["IMOL_PERM_DELETE"] = "Supprimer";
$MESS["IMOL_PERM_EDIT"] = "Modifier";
$MESS["IMOL_PERM_ADD"] = "Ajouter";
$MESS["IMOL_PERM_SAVE"] = "Enregistrer";
$MESS["IMOL_PERM_ERROR"] = "Erreur";
$MESS["IMOL_PERM_ROLE_DELETE_ERROR"] = "Erreur de suppression du rôle.";
$MESS["IMOL_PERM_ROLE_DELETE"] = "Supprimer le rôle";
$MESS["IMOL_PERM_ROLE_DELETE_CONFIRM"] = "Voulez-vous vraiment supprimer le rôle?";
$MESS["IMOL_PERM_ROLE_OK"] = "OK";
$MESS["IMOL_PERM_ROLE_CANCEL"] = "Annuler";
?>