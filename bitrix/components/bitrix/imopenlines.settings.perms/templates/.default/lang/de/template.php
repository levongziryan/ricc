<?
$MESS["IMOL_PERM_ROLE"] = "Rolle";
$MESS["IMOL_PERM_ADD_ACCESS_CODE"] = "Zugriffsrecht hinzufügen";
$MESS["IMOL_PERM_ROLE_LIST"] = "Rollen";
$MESS["IMOL_PERM_DELETE"] = "Löschen";
$MESS["IMOL_PERM_EDIT"] = "Bearbeiten";
$MESS["IMOL_PERM_ADD"] = "Hinzufügen";
$MESS["IMOL_PERM_SAVE"] = "Speichern";
$MESS["IMOL_PERM_ERROR"] = "Fehler";
$MESS["IMOL_PERM_ROLE_DELETE_ERROR"] = "Fehler beim Löschen der Rolle.";
$MESS["IMOL_PERM_ROLE_DELETE"] = "Rolle löschen";
$MESS["IMOL_PERM_ROLE_DELETE_CONFIRM"] = "Möchten Sie diese Rolle wirklich löschen?";
$MESS["IMOL_PERM_ROLE_OK"] = "OK";
$MESS["IMOL_PERM_ROLE_CANCEL"] = "Abbrechen";
?>