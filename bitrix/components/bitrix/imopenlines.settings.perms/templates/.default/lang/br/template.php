<?
$MESS["IMOL_PERM_ROLE"] = "Função";
$MESS["IMOL_PERM_ADD_ACCESS_CODE"] = "Adicionar permissão de acesso";
$MESS["IMOL_PERM_ROLE_LIST"] = "Funções";
$MESS["IMOL_PERM_DELETE"] = "Excluir";
$MESS["IMOL_PERM_EDIT"] = "Editar";
$MESS["IMOL_PERM_ADD"] = "Adicionar";
$MESS["IMOL_PERM_SAVE"] = "Salvar";
$MESS["IMOL_PERM_ERROR"] = "Erro";
$MESS["IMOL_PERM_ROLE_DELETE_ERROR"] = "Erro ao excluir a função.";
$MESS["IMOL_PERM_ROLE_DELETE"] = "Excluir função";
$MESS["IMOL_PERM_ROLE_DELETE_CONFIRM"] = "Tem certeza de que deseja excluir a função?";
$MESS["IMOL_PERM_ROLE_OK"] = "OK";
$MESS["IMOL_PERM_ROLE_CANCEL"] = "Cancelar";
?>