<?
$MESS["FSL_ALL_MESSAGES"] = "Tous les messages";
$MESS["JS_DEL_SUBSCRIBE"] = "tes-vous sûr de vouloir supprimer des souscriptions?";
$MESS["FSL_NOT_SUBCRIBED"] = "Vous n'êtes pas souscrit au forum. Pour la souscription, utilisez des liens ('SousEcrire') sur l'écran du forum ou du thème.";
$MESS["FSL_SUBSCR_DATE"] = "Date d'Abonnement";
$MESS["FSL_HERE"] = "Ici";
$MESS["FSL_TOPIC_NAME"] = "Sujet";
$MESS["FSL_FORUM_NAME"] = "Forum Sujets mobiles";
$MESS["JS_NO_SUBSCRIBE"] = "Pas d'abonnement sélectionné.";
$MESS["FSL_NEW_TOPICS"] = "Mises à jour";
$MESS["FSL_LAST_SENDED_MESSAGE"] = "Dernier message envoyé";
$MESS["F_DELETE_SUBSCRIBES"] = "Supprimer les abonnements";
$MESS["FSL_SUBSCR_MANAGE"] = "Abonnement";
?>