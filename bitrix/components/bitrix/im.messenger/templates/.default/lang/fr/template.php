<?
$MESS["IM_MESSENGER_OPEN_CALL"] = "Faire un appel";
$MESS["IM_MESSENGER_OPEN_MESSENGER"] = "Ouvrir le Chat d'Affaires";
$MESS["IM_MESSENGER_OPEN_MESSENGER_CP"] = "Ouvrir le Chat d'Affaires";
$MESS["IM_MESSENGER_OPEN_NOTIFY"] = "Ouvrir les avertissements";
$MESS["IM_MESSENGER_OPEN_EMAIL"] = "Accéder à la boîte aux lettres";
$MESS["IM_MESSENGER_OPEN_LF"] = "Accéder au flux d'activités";
$MESS["IM_MESSENGER_OPEN_NETWORK"] = "Accéder à Bitrix24 Network";
$MESS["IM_MESSENGER_OPEN_MESSENGER_2"] = "Ouvrir Chat et Appels";
?>