<?
$MESS["CRM_ACTIVITY_EDIT_RESPONSIBLE_NOT_ASSIGNED"] = "[non affecté]";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_ACTIVITY_EDIT_NOT_FOUND"] = "Chec de recherche du dossier à identificateur '#ID#'.";
$MESS["CRM_ACTIVITY_EDIT_OWNER_TYPE_IS_NOT_SUPPORTED"] = "Le type de propriétaire (#OWNER_TYPE#) n'est pas supporté dans le contexte actuel.";
$MESS["CRM_ACTIVITY_EDIT_OWNER_TYPE_IS_NOT_DIFINED"] = "Le type de propriétaire n'est pas déterminé.";
$MESS["CRM_ACTIVITY_EDIT_TYPE_IS_NOT_SUPPORTED"] = "Type d'affaire (#TYPE#) n'est pas supporté dans ce contexte";
?>