<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_ACTIVITY_EDIT_NOT_FOUND"] = "Não foi possível encontrar atividade ##ID#.";
$MESS["CRM_ACTIVITY_EDIT_OWNER_TYPE_IS_NOT_DIFINED"] = "O tipo de proprietário está indefinido.";
$MESS["CRM_ACTIVITY_EDIT_OWNER_TYPE_IS_NOT_SUPPORTED"] = "O tipo de proprietário (#OWNER_TYPE#) não é compatível com o contexto atual.";
$MESS["CRM_ACTIVITY_EDIT_TYPE_IS_NOT_SUPPORTED"] = "O tipo de atividade (#TYPE#) não é suportado no contexto atual";
$MESS["CRM_ACTIVITY_EDIT_RESPONSIBLE_NOT_ASSIGNED"] = "[Não atribuído]";
?>