<?
$MESS["TASKS_COUNTER_TOTAL_PLURAL_0"] = "tâche";
$MESS["TASKS_COUNTER_TOTAL_PLURAL_1"] = "tâches";
$MESS["TASKS_COUNTER_TOTAL_PLURAL_2"] = "tâches";
$MESS["TASKS_COUNTER_EXPIRED_PLURAL_0"] = "dépassé";
$MESS["TASKS_COUNTER_EXPIRED_PLURAL_1"] = "dépassé";
$MESS["TASKS_COUNTER_EXPIRED_PLURAL_2"] = "dépassé";
$MESS["TASKS_COUNTER_WO_DEADLINE_PLURAL_0"] = "aucune date limite";
$MESS["TASKS_COUNTER_WO_DEADLINE_PLURAL_1"] = "aucune date limite";
$MESS["TASKS_COUNTER_WO_DEADLINE_PLURAL_2"] = "aucune date limite";
$MESS["TASKS_COUNTER_EXPIRED_CANDIDATES_PLURAL_0"] = "presque dépassé";
$MESS["TASKS_COUNTER_EXPIRED_CANDIDATES_PLURAL_1"] = "presque dépassé";
$MESS["TASKS_COUNTER_EXPIRED_CANDIDATES_PLURAL_2"] = "presque dépassé";
$MESS["TASKS_COUNTER_NEW_PLURAL_0"] = "non visualisé";
$MESS["TASKS_COUNTER_NEW_PLURAL_1"] = "non visualisé";
$MESS["TASKS_COUNTER_NEW_PLURAL_2"] = "non visualisé";
$MESS["TASKS_COUNTER_WAIT_CTRL_PLURAL_0"] = "en attente de contrôle";
$MESS["TASKS_COUNTER_WAIT_CTRL_PLURAL_1"] = "en attente de contrôle";
$MESS["TASKS_COUNTER_WAIT_CTRL_PLURAL_2"] = "en attente de contrôle";
$MESS["TASKS_COUNTER_EMPTY"] = "Aucune tâche ne nécessite une attention immédiate";
$MESS["TASKS_SWITCHER_NAME"] = "Voir";
$MESS["TASKS_SWITCHER_ITEM_LIST"] = "Liste";
$MESS["TASKS_SWITCHER_ITEM_GANTT"] = "Gantt";
$MESS["TASKS_SWITCHER_ITEM_KANBAN"] = "Kanban";
$MESS["TASKS_SWITCHER_ITEM_REPORTS"] = "Rapports";
$MESS["TASKS_COUNTER_TOTAL"] = "Tâches:";
$MESS["TASKS_GROUP_COUNTERS_SOON"] = "Prochainement, compteurs de tâches du projet";
?>