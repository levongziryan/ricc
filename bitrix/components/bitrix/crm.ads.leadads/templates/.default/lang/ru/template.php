<?php

$MESS["CRM_ADS_LEADADS_ERROR_ACTION"] = "Произошла ошибка. Действие отменено.";
$MESS["CRM_ADS_LEADADS_CLOSE"] = "Закрыть";
$MESS["CRM_ADS_LEADADS_APPLY"] = "Выполнить";
$MESS["CRM_ADS_LEADADS_CANCEL"] = "Отменить";
$MESS["CRM_ADS_LEADADS_LOGIN"] = "Подключить";
$MESS["CRM_ADS_LEADADS_LOGOUT"] = "Отключить";
$MESS["CRM_ADS_LEADADS_REFRESH"] = "Обновить";
$MESS["CRM_ADS_LEADADS_REFRESH_TEXT"] = "Обновите доступные настройки.";
$MESS['CRM_ADS_LEADADS_CABINET_FACEBOOK'] = 'Список страниц в Facebook';
$MESS['CRM_ADS_LEADADS_TITLE'] = 'В рекламе Facebook можно выводить форму, добавленную из Битрикс24.';
$MESS['CRM_ADS_LEADADS_SELECT_ACCOUNT'] = 'Добавить форму для страницы Facebook';
$MESS['CRM_ADS_LEADADS_ERROR_NO_ACCOUNTS'] = 'Страницы Facebook не найдены, перейдите в %name% и создайте страницу.';
$MESS['CRM_ADS_LEADADS_BUTTON_EXPORT_FACEBOOK'] = 'Связать с Facebook';
$MESS['CRM_ADS_LEADADS_BUTTON_UNLINK_FACEBOOK'] = 'Отвязать от Facebook';
$MESS['CRM_ADS_LEADADS_BUTTON_EXPORTED_SUCCESS'] = 'Успешно выполнено';
$MESS['CRM_ADS_LEADADS_LINKS_ITEM_FACEBOOK'] = 'для страницы "%account%" как "%name%"';
$MESS['CRM_ADS_LEADADS_LINKS_TITLE'] = 'Форма "%name%" была уже добавлена';
$MESS['CRM_ADS_LEADADS_FORM_NAME'] = 'Название формы для показа в Facebook';
$MESS['CRM_ADS_LEADADS_FORM_SUCCESS_URL'] = 'Адрес для перенаправления пользователя после заполнения формы';
$MESS['CRM_ADS_LEADADS_AFTER_ENABLE_FACEBOOK'] = 'После связи с Facebook, форма будет заблокирована для редактирования. А данные будут передаваться в CRM.';
$MESS['CRM_ADS_LEADADS_AFTER_DISABLE_FACEBOOK'] = 'После отвязывания от Facebook, форма будет доступна для редактирования. А данные перестанут передаваться в CRM.';
$MESS['CRM_ADS_LEADADS_NOW'] = 'связана сейчас';
$MESS['CRM_ADS_LEADADS_IS_LINKED'] = 'связана';
$MESS['CRM_ADS_LEADADS_MORE'] = 'Подробнее';
$MESS['CRM_ADS_LEADADS_LIST'] = 'список';