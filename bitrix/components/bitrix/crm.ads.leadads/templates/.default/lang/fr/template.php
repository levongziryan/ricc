<?
$MESS["CRM_ADS_LEADADS_ERROR_ACTION"] = "L'action a été annulée à cause d'une erreur.";
$MESS["CRM_ADS_LEADADS_CLOSE"] = "Fermer";
$MESS["CRM_ADS_LEADADS_APPLY"] = "Exécuter";
$MESS["CRM_ADS_LEADADS_CANCEL"] = "Annuler";
$MESS["CRM_ADS_LEADADS_LOGIN"] = "Connecter";
$MESS["CRM_ADS_LEADADS_LOGOUT"] = "Déconnecter";
$MESS["CRM_ADS_LEADADS_REFRESH"] = "Actualiser";
$MESS["CRM_ADS_LEADADS_REFRESH_TEXT"] = "Actualiser les préférences disponibles.";
$MESS["CRM_ADS_LEADADS_CABINET_FACEBOOK"] = "Pages Facebook";
$MESS["CRM_ADS_LEADADS_TITLE"] = "Les publicités Facebook peuvent afficher les formulaires que vous ajoutez dans Bitrix24.";
$MESS["CRM_ADS_LEADADS_SELECT_ACCOUNT"] = "Ajouter un formulaire pour les pages Facebook";
$MESS["CRM_ADS_LEADADS_ERROR_NO_ACCOUNTS"] = "Aucun page Facebook trouvée. Veuillez vous rendre sur %name% et créer un page.";
$MESS["CRM_ADS_LEADADS_BUTTON_EXPORT_FACEBOOK"] = "Associer à Facebook";
$MESS["CRM_ADS_LEADADS_BUTTON_UNLINK_FACEBOOK"] = "Dissocier de Facebook";
$MESS["CRM_ADS_LEADADS_BUTTON_EXPORTED_SUCCESS"] = "Réussi";
$MESS["CRM_ADS_LEADADS_LINKS_ITEM_FACEBOOK"] = "pour la page \"%account%\" comme \"%name%\"";
$MESS["CRM_ADS_LEADADS_LINKS_TITLE"] = "Le formulaire \"%name%\" a déjà été ajouté";
$MESS["CRM_ADS_LEADADS_FORM_NAME"] = "Nom du formulaire Facebook";
$MESS["CRM_ADS_LEADADS_FORM_SUCCESS_URL"] = "URL de redirection après envoi du formulaire";
$MESS["CRM_ADS_LEADADS_AFTER_ENABLE_FACEBOOK"] = "Le formulaire ne peut être édité une fois associé à Facebook ; les données du formulaire seront envoyée à la GRC.";
$MESS["CRM_ADS_LEADADS_AFTER_DISABLE_FACEBOOK"] = "Le formulaire pourra être édité une fois dissocié de Facebook ; les données du formulaire ne seront pas envoyées à la GRC.";
$MESS["CRM_ADS_LEADADS_NOW"] = "associé maintenant";
$MESS["CRM_ADS_LEADADS_IS_LINKED"] = "associé";
$MESS["CRM_ADS_LEADADS_MORE"] = "Détails";
$MESS["CRM_ADS_LEADADS_LIST"] = "liste";
?>