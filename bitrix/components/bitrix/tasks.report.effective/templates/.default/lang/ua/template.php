<?
$MESS["TASKS_EFFECTIVE_TITLE_FULL"] = "Ефективність";
$MESS["TASKS_EFFECTIVE_TITLE_SHORT"] = "Ефективність";
$MESS["TASKS_MORE_LINK_TEXT"] = "Детальніше";
$MESS["TASKS_ADD_TASK"] = "Створити завдання";
$MESS["TASKS_TITLE_GRAPH_KPI"] = "Ефективність у %";
$MESS["TASKS_MY_EFFECTIVE"] = "Моя ефективність";
$MESS["TASKS_COMPLETED"] = "Завершено завдань";
$MESS["TASKS_VIOLATION"] = "Зауважень до завдань";
$MESS["TASKS_IN_PROGRESS"] = "Всього в роботі";
$MESS["TASKS_EFFECTIVE_HELP_URL"] = "https://helpdesk.bitrix24.ua/open/6844989/";
$MESS["TASKS_EFFECTIVE_HELP_TEXT"] = "Як це працює?";
?>