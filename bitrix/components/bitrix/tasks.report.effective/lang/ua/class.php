<?
$MESS["TASKS_COLUMN_ID"] = "ID";
$MESS["TASKS_COLUMN_USER"] = "Співробітник";
$MESS["TASKS_COLUMN_EFFECTIVE"] = "Ефективність";
$MESS["TASKS_COLUMN_DINAMIC"] = "Динаміка";
$MESS["TASKS_COLUMN_MORE"] = "Додатково";
$MESS["TASKS_COLUMN_CREATED_DATE"] = "Дата зауваження";
$MESS["TASKS_FILTER_COLUMN_GROUP_ID"] = "Проект/група";
$MESS["TASKS_FILTER_COLUMN_DATE"] = "Період";
$MESS["TASKS_FILTER_COLUMN_KPI"] = "Ефективність";
$MESS["TASKS_PRESET_CURRENT_DAY"] = "Поточний день";
$MESS["TASKS_PRESET_CURRENT_MONTH"] = "Поточний місяць";
$MESS["TASKS_PRESET_CURRENT_QUARTER"] = "Поточний квартал";
$MESS["TASKS_PRESET_CURRENT_YEAR"] = "Поточний рік";
?>