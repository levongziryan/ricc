<?
$MESS["CRM_TAB_1"] = "Importar";
$MESS["CRM_TAB_1_TITLE"] = "Importar compañías";
$MESS["CRM_TAB_2"] = "Campos";
$MESS["CRM_TAB_2_TITLE"] = "Configurar mapeo del campo";
$MESS["CRM_TAB_3"] = "Importar";
$MESS["CRM_TAB_3_TITLE"] = "Importar resultado";
$MESS["CRM_IMPORT_NEXT_STEP"] = "Siguiente >>";
$MESS["CRM_IMPORT_NEXT_STEP_TITLE"] = "Ir al siguiente paso";
$MESS["CRM_IMPORT_PREVIOUS_STEP"] = "<< Atrás";
$MESS["CRM_IMPORT_PREVIOUS_STEP_TITLE"] = "Ir al paso previo";
$MESS["CRM_IMPORT_DONE"] = "Hecho";
$MESS["CRM_IMPORT_DONE_TITLE"] = "Ver las compañías";
$MESS["CRM_IMPORT_CANCEL"] = "Cancelar";
$MESS["CRM_IMPORT_CANCEL_TITLE"] = "Abortar e ir a la lista de la compañía";
$MESS["CRM_IMPORT_AGAIN"] = "Importar otro archivo";
$MESS["CRM_IMPORT_AGAIN_TITLE"] = "Oprimir para importar otro archivo";
$MESS["CRM_TAB_DUP_CONTROL"] = "Control de duplicados";
$MESS["CRM_TAB_DUP_CONTROL_TITLE"] = "Configurar el control de duplicados";
?>