<?
$MESS["EVENT_CALENDAR"] = "Calendrier d'événements";
$MESS["EVENT_CALENDAR_VIEW_FORM"] = "Formulaire de visualisation d'un événement";
$MESS["EVENT_CALENDAR_VIEW_FORM_DESCRIPTION"] = "Le formulaire de visualisation d'un événement est un composant de service utilisé uniquement comme élément du composant composite.";
?>