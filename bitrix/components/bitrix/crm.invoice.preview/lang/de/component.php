<?
$MESS["CRM_FIELD_OPPORTUNITY"] = "Betrag";
$MESS["CRM_FIELD_STATUS"] = "Status";
$MESS["CRM_FIELD_ASSIGNED_BY"] = "Verantwortlich";
$MESS["CRM_FIELD_DATE_BILL"] = "Rechnungsdatum";
$MESS["DATE_PAY_BEFORE"] = "Zahlungsfrist";
$MESS["CRM_FIELD_DEAL"] = "Auftrag";
$MESS["CRM_FIELD_QUOTE"] = "Angebot";
$MESS["CRM_TITLE_INVOICE"] = "Rechnung";
?>