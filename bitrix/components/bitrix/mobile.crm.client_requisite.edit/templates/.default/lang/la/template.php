<?
$MESS["M_CRM_REQUISITE_EDIT_PULL_TEXT"] = "Tire hacia abajo para actualizar...";
$MESS["M_CRM_REQUISITE_EDIT_DOWN_TEXT"] = "Suelte para actualizar...";
$MESS["M_CRM_REQUISITE_EDIT_LOAD_TEXT"] = "Actualizando...";
$MESS["M_CRM_REQUISITE_EDIT_TITLE"] = "Editar detalles de pago";
$MESS["M_CRM_REQUISITE_EDIT_SAVE_BTN"] = "Guardar";
$MESS["M_CRM_REQUISITE_EDIT_CANCEL_BTN"] = "Cancelar";
?>