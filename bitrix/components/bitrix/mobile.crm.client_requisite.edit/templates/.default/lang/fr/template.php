<?
$MESS["M_CRM_REQUISITE_EDIT_LOAD_TEXT"] = "Mise à jour...";
$MESS["M_CRM_REQUISITE_EDIT_CANCEL_BTN"] = "Annuler";
$MESS["M_CRM_REQUISITE_EDIT_DOWN_TEXT"] = "Relâcher pour mettre à jour...";
$MESS["M_CRM_REQUISITE_EDIT_PULL_TEXT"] = "Tirer pour mettre à jour...";
$MESS["M_CRM_REQUISITE_EDIT_TITLE"] = "Rédaction de références";
$MESS["M_CRM_REQUISITE_EDIT_SAVE_BTN"] = "Sauvegarder";
?>