<?
$MESS["SONET_LOG_COMMENT_EMPTY"] = "El texto del mensaje está vacío.";
$MESS["SONET_LOG_COMMENT_NO_PERMISSIONS"] = "Usted no tiene permiso para añadir comentarios.";
$MESS["SONET_LOG_CREATED_BY_ANONYMOUS"] = "Visitante no autorizado";
$MESS["SONET_LOG_COMMENT_DELETE_NO_PERMISSIONS"] = "No tiene permisos para borrar comentarios";
$MESS["SONET_LOG_COMMENT_CANT_DELETE"] = "No se pudo eliminar el comentario";
$MESS["SONET_LOG_COMMENT_FORMAT_TIME"] = "g:i a";
$MESS["SONET_LOG_COMMENT_FORMAT_DATE"] = "d F, g:i a";
$MESS["SONET_LOG_COMMENT_FORMAT_DATE_YEAR"] = "d F Y, g:i a";
$MESS["SONET_LOG_COMMENT_EDIT_NO_PERMISSIONS"] = "No tiene permiso para editar el comentario";
?>