<?
$MESS["SONET_LOG_COMMENT_FORMAT_DATE_YEAR"] = "d F Y, g:i a";
$MESS["SONET_LOG_COMMENT_FORMAT_DATE"] = "d F, g:i a";
$MESS["SONET_LOG_COMMENT_EMPTY"] = "Le texte du message n'est pas indiqué.";
$MESS["SONET_LOG_CREATED_BY_ANONYMOUS"] = "Utilisateur pas autorisé";
$MESS["SONET_LOG_COMMENT_NO_PERMISSIONS"] = "Il n'y a pas de droits pour ajouter un commentaire.";
$MESS["SONET_LOG_COMMENT_FORMAT_TIME"] = "g:i a";
$MESS["SONET_LOG_COMMENT_CANT_DELETE"] = "Impossible de supprimer le commentaire";
$MESS["SONET_LOG_COMMENT_DELETE_NO_PERMISSIONS"] = "Pas de permission pour effacer le commentaire";
$MESS["SONET_LOG_COMMENT_EDIT_NO_PERMISSIONS"] = "Aucune permission pour éditer le commentaire";
?>