<?
$MESS["SONET_LOG_COMMENT_EMPTY"] = "O texto da mensagem está vazio.";
$MESS["SONET_LOG_COMMENT_NO_PERMISSIONS"] = "Você não tem permissão para adicionar comentários.";
$MESS["SONET_LOG_CREATED_BY_ANONYMOUS"] = "Visitante não autorizado";
$MESS["SONET_LOG_COMMENT_DELETE_NO_PERMISSIONS"] = "Sem permissão para excluir comentário";
$MESS["SONET_LOG_COMMENT_CANT_DELETE"] = "Não foi possível excluir o comentário";
$MESS["SONET_LOG_COMMENT_EDIT_NO_PERMISSIONS"] = "Sem permissão para editar comentário";
?>