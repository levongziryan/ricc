<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "El módulo Business Process no está instalado.";
$MESS["BIZPROCDESIGNER_MODULE_NOT_INSTALLED"] = "El módulo Business Process Editor no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_BP_LIST_TITLE_EDIT"] = "Plantillas: #NAME#";
$MESS["CRM_BP_ENTITY_LIST"] = "Tipos";
$MESS["CRM_BP_LEAD"] = "Prospecto";
$MESS["CRM_BP_DEAL"] = "Negociación";
$MESS["CRM_BP_CONTACT"] = "Contacto";
$MESS["CRM_BP_COMPANY"] = "Compañía";
?>