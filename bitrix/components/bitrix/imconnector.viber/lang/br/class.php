<?
$MESS["IMCONNECTOR_COMPONENT_VIBER_MODULE_NOT_INSTALLED"] = "O módulo \"Conectores IM Externos\" não está instalado.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_NO_ACTIVE_CONNECTOR"] = "Este conector está inativo.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_SESSION_HAS_EXPIRED"] = "Sua sessão expirou. Envie o formulário novamente.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_OK_SAVE"] = "A informação foi salva com sucesso.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_NO_DATA_SAVE"] = "Não há dados para salvar";
$MESS["IMCONNECTOR_COMPONENT_VIBER_NO_SAVE"] = "Erro ao salvar os dados. Verifique seus dados e tente novamente.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_OK_CONNECT"] = "A conexão de teste foi estabelecida com sucesso";
$MESS["IMCONNECTOR_COMPONENT_VIBER_NO_CONNECT"] = "Falha ao estabelecer a conexão de teste usando os parâmetros fornecidos";
$MESS["IMCONNECTOR_COMPONENT_VIBER_OK_REGISTER"] = "O registro foi bem sucedido";
$MESS["IMCONNECTOR_COMPONENT_VIBER_NO_REGISTER"] = "Erro de registro";
?>