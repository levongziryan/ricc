<?
$MESS["IMCONNECTOR_COMPONENT_VIBER_MODULE_NOT_INSTALLED"] = "El módulo \"External IM Connectors\" no está instalado.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_NO_ACTIVE_CONNECTOR"] = "Este conector está inactivo.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_SESSION_HAS_EXPIRED"] = "Su sesión ha caducado. Envíe el formulario nuevamente.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_OK_SAVE"] = "La información se ha guardado correctamente.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_NO_DATA_SAVE"] = "No hay datos para guardar";
$MESS["IMCONNECTOR_COMPONENT_VIBER_NO_SAVE"] = "Error al guardar los datos. Compruebe la entrada e inténtelo de nuevo.";
$MESS["IMCONNECTOR_COMPONENT_VIBER_OK_CONNECT"] = "La conexión de prueba se ha establecido satisfactoriamente";
$MESS["IMCONNECTOR_COMPONENT_VIBER_NO_CONNECT"] = "Error al establecer la conexión de prueba utilizando los parámetros proporcionados";
$MESS["IMCONNECTOR_COMPONENT_VIBER_OK_REGISTER"] = "El registro fue un éxito";
$MESS["IMCONNECTOR_COMPONENT_VIBER_NO_REGISTER"] = "Error de registro";
?>