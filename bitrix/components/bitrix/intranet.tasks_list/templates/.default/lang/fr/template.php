<?
$MESS["INTASK_TASKPRIORITY"] = "criticité";
$MESS["INTASK_TO_DATE_TLP"] = "jusqu'à #DATE#";
$MESS["INTASK_LIST_EMPTY"] = "Il n'y pas de tâches pour le moment.";
$MESS["INTASK_TASKASSIGNEDTO"] = "Assigner à";
$MESS["INTASK_NO_DATE_TLP"] = "la période d'exécution n'est pas définie";
$MESS["INTASK_FROM_DATE_TLP"] = "dès #DATE#";
$MESS["INTASK_TASKSTATUS"] = "Statut";
?>