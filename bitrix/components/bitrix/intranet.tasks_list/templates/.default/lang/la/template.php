<?
$MESS["INTASK_LIST_EMPTY"] = "No se asignaron tareas.";
$MESS["INTASK_TASKPRIORITY"] = "Prioridad";
$MESS["INTASK_TASKSTATUS"] = "Estado";
$MESS["INTASK_TO_DATE_TLP"] = "hasta #DATE#";
$MESS["INTASK_FROM_DATE_TLP"] = "desde #DATE#";
$MESS["INTASK_NO_DATE_TLP"] = "fecha de vencimiento no asignada";
$MESS["INTASK_TASKASSIGNEDTO"] = "Asignado a";
?>