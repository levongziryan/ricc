<?
$MESS["INTASK_TASKASSIGNEDTO"] = "Ausführend";
$MESS["INTASK_NO_DATE_TLP"] = "Der Zeitraum für die Abgabe wurde nicht angegeben";
$MESS["INTASK_FROM_DATE_TLP"] = "vom #DATE#";
$MESS["INTASK_LIST_EMPTY"] = "Keine Aufgaben";
$MESS["INTASK_TASKPRIORITY"] = "Priorität";
$MESS["INTASK_TASKSTATUS"] = "Status";
$MESS["INTASK_TO_DATE_TLP"] = "bis zum #DATE#";
?>