<?
$MESS["INTASK_TASKASSIGNEDTO"] = "Atribuído a";
$MESS["INTASK_NO_DATE_TLP"] = "data de vencimento não foi atribuída";
$MESS["INTASK_FROM_DATE_TLP"] = "de #DATE#";
$MESS["INTASK_LIST_EMPTY"] = "Nenhuma tarefa foi atribuída.";
$MESS["INTASK_TASKPRIORITY"] = "Prioridade";
$MESS["INTASK_TASKSTATUS"] = "Status";
$MESS["INTASK_TO_DATE_TLP"] = "até #DATE#";
?>