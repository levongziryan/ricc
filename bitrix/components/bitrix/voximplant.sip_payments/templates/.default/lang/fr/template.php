<?
$MESS["VI_SIP_TITLE"] = "SIP Connecteur";
$MESS["VI_SIP_PAID_BEFORE"] = "Module expirera le #DATE#";
$MESS["VI_SIP_PAID_NOTICE"] = "Les numéros de téléphone SIP seront déconnectés après l'expiration de l'abonnement de module.";
$MESS["VI_SIP_BUTTON"] = "tendre";
$MESS["VI_SIP_CONFIG"] = "Gérer numéros SIP";
$MESS["VI_SIP_PAID_FREE"] = "Vous avez #COUNT# minutes gratuites pour configurer et tester votre équipement.";
$MESS["VI_SIP_PAID_NOTICE_2"] = "Après les minutes gratuites ont expiré, les numéros de SIP seront déconnectés jusqu'à ce que vous payez pour SIP connecteur abonnement mensuel.";
$MESS["VI_SIP_BUTTON_BUY"] = "Connecter";
$MESS["VI_NOTICE_OLD_CONFIG_OFFICE_PBX"] = "Pour accélérer le travail du connecteur SIP, vous devez changer l'adresse du serveur des appels entrants<br> de <b>incoming.#ACCOUNT_NAME#</b><br> à <b>ip.#ACCOUNT_NAME#</b><br> dans les paramètres de connexion PBX.";
$MESS["VI_NOTICE_BUTTON_DONE"] = "Terminé";
?>