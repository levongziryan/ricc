<?
$MESS["VI_SIP_TITLE"] = "SIP Conector";
$MESS["VI_SIP_PAID_BEFORE"] = "El módulo expirará el #DATE#";
$MESS["VI_SIP_PAID_NOTICE"] = "Los números de teléfono SIP se desconectarán una vez finalizada la suscripción del módulo.";
$MESS["VI_SIP_BUTTON"] = "Extender";
$MESS["VI_SIP_CONFIG"] = "Administrar números SIP";
$MESS["VI_SIP_PAID_FREE"] = "Usted tiene #COUNT# minutos libres para configurar y poner a prueba su equipo.";
$MESS["VI_SIP_PAID_NOTICE_2"] = "Después de que los minutos libres hayan expirado, los números SIP se desconectarán hasta que pague la suscripción mensual del SIP Connector.";
$MESS["VI_SIP_BUTTON_BUY"] = "Conectar";
$MESS["VI_NOTICE_OLD_CONFIG_OFFICE_PBX"] = "Para acelerar el trabajo del SIP connector, debe cambiar la dirección del servidor de Llamadas entrantes<br> desde <b>entrante.#ACCOUNT_NAME#</b><br> to <b>ip.#ACCOUNT_NAME#</b><br> en los parámetros de conexión PBX.";
$MESS["VI_NOTICE_BUTTON_DONE"] = "Listo";
?>