<?
$MESS["VI_SIP_TITLE"] = "Conector SIP";
$MESS["VI_SIP_PAID_BEFORE"] = "O módulo irá expirar em #DATA#";
$MESS["VI_SIP_PAID_NOTICE"] = "Os números de telefone SIP serão desconectados depois que a assinatura do módulo expirar.";
$MESS["VI_SIP_BUTTON"] = "Ampliar";
$MESS["VI_SIP_CONFIG"] = "Gerenciar Números SIP";
$MESS["VI_SIP_PAID_FREE"] = "Você tem #COUNT# minutos grátis para configurar e testar seu equipamento.";
$MESS["VI_SIP_PAID_NOTICE_2"] = "Depois que os minutos grátis expirarem, os números SIP serão desconectados até que você pague pela assinatura mensal do Conector SIP.";
$MESS["VI_SIP_BUTTON_BUY"] = "Conectar";
$MESS["VI_NOTICE_OLD_CONFIG_OFFICE_PBX"] = "Para agilizar o trabalho do conector SIP, você precisa alterar o endereço do servidor de Chamadas Recebidas<br> de <b>recepção.#ACCOUNT_NAME#</b><br> para <b>ip.#ACCOUNT_NAME#</b><br> nos Parâmetros de Conexão PBX.";
$MESS["VI_NOTICE_BUTTON_DONE"] = "Feito";
?>