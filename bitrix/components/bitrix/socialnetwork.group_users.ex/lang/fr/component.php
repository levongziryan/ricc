<?
$MESS["SONET_GUE_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["SONET_P_USER_NO_GROUP"] = "Groupe introuvable.";
$MESS["SONET_GUE_MODS_NAV"] = "Modérateur";
$MESS["SONET_MODULE_NOT_INSTALL"] = "Le module du réseau social n'a pas été installé.";
$MESS["SONET_GUE_BAN_NAV"] = "Utilisateurs";
$MESS["SONET_GUE_NO_PERMS"] = "Vous n'avez pas de droits pour accéder à ce groupe.";
$MESS["SONET_GUE_USERS_NAV"] = "Participants";
$MESS["SONET_GUE_PAGE_TITLE"] = "Membres du groupe";
$MESS["SONET_GUE_NO_PERMS_PROJECT"] = "Vous n'êtes pas autorisé à supprimer ce projet.";
$MESS["SONET_GUE_PAGE_TITLE_PROJECT"] = "Membres du projet";
?>