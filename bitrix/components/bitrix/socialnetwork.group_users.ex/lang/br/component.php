<?
$MESS["SONET_MODULE_NOT_INSTALL"] = "O Módulo de Rede Social não está instalado";
$MESS["SONET_P_USER_NO_GROUP"] = "O grupo não foi encontrado";
$MESS["SONET_GUE_NO_PERMS"] = "Voê não tem permissão para visualizar esse grupo";
$MESS["SONET_GUE_PAGE_TITLE"] = "Membros do Grupo";
$MESS["SONET_GUE_USERS_NAV"] = "Membros";
$MESS["SONET_GUE_MODS_NAV"] = "Moderadores";
$MESS["SONET_GUE_BAN_NAV"] = "Usuários";
$MESS["SONET_GUE_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["SONET_GUE_NO_PERMS_PROJECT"] = "Você não tem permissão para excluir este projeto.";
$MESS["SONET_GUE_PAGE_TITLE_PROJECT"] = "Membros do Projeto";
?>