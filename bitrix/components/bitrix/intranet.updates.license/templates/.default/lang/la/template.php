<?
$MESS["UPDATES_ACTIVATE_SITE_TEXT"] = "Si no está registrado en <a href=\"http://www.bitrixsoft.com\" target=\"_blank\">www.bitrixsoft.com</a>, asegúrese de que la opción \"Crear usuario\"
 <br/>Está marcada e ingrese su información (nombre y apellidos, nombre de usuario y contraseña) 
<br/>en los campos de formulario. Habiéndose registrado en www.bitrixsoft.com le permite utilizar el
<br/><a href=\"http://www.bitrixsoft.com/support/\" target=\"_blank\">techsupport service</a> and the <a href=\"http://www.bitrixsoft.com/support/forum/\" target=\"_blank\">private forum</a> para resolver sus problemas y obtener respuestas
<br/>a sus preguntas.";
$MESS["UPDATES_ACTIVATE_GENERATE_USER"] = "Crear usuario en www.bitrixsoft.com";
$MESS["UPDATES_ACTIVATE_GENERATE_USER_NO"] = "Ya tengo una cuenta de usuario y quiero usarla para acceder a las secciones de sitio de Helpdesk y de Descargar;";
$MESS["SUP_REGISTERED"] = "Registrado para";
$MESS["SUP_LICENSE_KEY"] = "Clave de licencia";
$MESS["SUP_EDITION"] = "Edición";
$MESS["SUP_SITES"] = "Número de sitios";
$MESS["SUP_USERS"] = "Máximo de usuarios";
$MESS["SUP_CURRENT_NUMBER_OF_USERS"] = "; número de usuarios actuales: #NUM#";
$MESS["SUP_USERS_IS_NOT_LIMITED"] = "Su licencia no tiene límite de usuario máximo.";
$MESS["SUP_CURRENT_NUMBER_OF_USERS1"] = "Usuarios activos: #NUM#.";
$MESS["UPDATES_LICENSE_TITLE"] = "Bitrix24 Licencia";
$MESS["UPDATES_LICENSE_KEY"] = "Clave de licencia";
$MESS["UPDATES_LICENSE_SAVE"] = "Guardar";
$MESS["UPDATES_LICENSE_ACTIVATE"] = "Activar";
$MESS["UPDATES_ACTIVATE_LICENSE_TITLE"] = "Activación de Claves de Licencia";
$MESS["UPDATES_ACTIVATE_NAME"] = "Nombre completo del propietario (compañía o persona)";
$MESS["UPDATES_ACTIVATE_SITE_URL"] = "Todos los dominios que serán alimentados<br/>por esta instancia de Bitrix Site Manager, incluidos los dominios de prueba";
$MESS["UPDATES_ACTIVATE_PHONE"] = "Copia del producto y teléfono del propietario";
$MESS["UPDATES_ACTIVATE_EMAIL"] = "Licencias y uso de e-mail de contacto";
$MESS["UPDATES_ACTIVATE_CONTACT_PERSON"] = "Póngase en contacto con la persona encargada de la copia de este producto";
$MESS["UPDATES_ACTIVATE_CONTACT_EMAIL"] = "E-mail de la persona de contacto";
$MESS["UPDATES_ACTIVATE_CONTACT_PHONE"] = "Teléfono de la persona de contacto";
$MESS["UPDATES_ACTIVATE_CONTACT_INFO"] = "Otros contactos importantes";
$MESS["UPDATES_ACTIVATE_USER_NAME"] = "Nombre";
$MESS["UPDATES_ACTIVATE_USER_LAST_NAME"] = "Apellido";
$MESS["UPDATES_ACTIVATE_USER_LOGIN_A"] = "Login (3 o más caracteres)";
$MESS["UPDATES_ACTIVATE_USER_PASSWORD"] = "Contraseña (6 o más caracteres)";
$MESS["UPDATES_ACTIVATE_USER_PASSWORD_CONFIRM"] = "Confirmar contraseña";
$MESS["UPDATES_ACTIVATE_USER_EMAIL"] = "E-mail";
$MESS["UPDATES_COUPON_TITLE"] = "Activar cupón";
$MESS["UPDATES_COUPON_KEY"] = "Ingrese el código del cupón";
$MESS["UPDATES_COUPON_SUCCESS"] = "El cupón se ha aplicado correctamente";
$MESS["UPDATES_ACTIVATE_SUCCESS"] = "La clave se ha activado correctamente";
$MESS["SUP_ACTIVE_TITLE"] = "La clave de licencia es válida";
$MESS["SUP_ACTIVE_PERIOD_TO"] = "mediante #DATE_TO#";
$MESS["UPDATES_ACTIVATE_SITE_URL_NEW"] = "Todos los dominios que serán alimentados<br/>por esta instancia del sistema incluyendo dominios de prueba";
?>