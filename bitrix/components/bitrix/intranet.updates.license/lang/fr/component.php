<?
$MESS["SUPA_AERR_LOGIN1"] = "L'identifiant de <a href=\"http://www.bitrixsoft.ru\">www.bitrixsoft.ru</a> doit contenir au moins 3 symboles";
$MESS["SUPA_AERR_EMAIL1"] = "Veuillez vérifier la validité de l'adresse e-mail";
$MESS["SUPA_AERR_CONTACT_EMAIL1"] = "Veuillez vérifier la validité de l'adresse e-mail d'un contact";
$MESS["SUPA_AERR_URI"] = "Veuillez saisir l'adresse du site qui sera utilisé avec la clé";
$MESS["SUPA_AERR_EMAIL"] = "Veuillez saisir l'e-mail du contact";
$MESS["SUPA_AERR_FNAME"] = "Veuillez saisir le prénom de l'utilisateur dont le compte <a href=\"http://www.bitrixsoft.ru\">www.bitrixsoft.ru</a> sera créé";
$MESS["SUPA_AERR_LNAME"] = "Veuillez saisir le nom de famille de l'utilisateur dont le compte <a href=\"http://www.bitrixsoft.ru\">www.bitrixsoft.ru</a> sera créé";
$MESS["SUPA_AERR_LOGIN"] = "Veuillez saisir l'identifiant à utiliser sur <a href=\"http://www.bitrixsoft.ru\">www.bitrixsoft.ru</a>";
$MESS["SUPA_AERR_NAME"] = "Veuillez saisir le nom de la société qui possède la clé";
$MESS["SUPA_AERR_PASSW"] = "Veuillez saisir le mot de passe à utiliser sur <a href=\"http://www.bitrixsoft.ru\">www.bitrixsoft.ru</a>";
$MESS["SUPA_AERR_CONTACT_EMAIL"] = "Veuillez saisir l'adresse e-mail d'un contact";
$MESS["SUPA_AERR_CONTACT_PERSON"] = "Veuillez saisir les nom et prénoms d'un contact";
$MESS["SUPA_AERR_CONTACT_PHONE"] = "Veuillez saisir le numéro de téléphone d'un contact";
$MESS["SUPA_AERR_PHONE"] = "Veuillez saisir le numéro de téléphone du propriétaire de la copie du produit";
$MESS["SUPA_ACE_CPN"] = "Le code du coupon n'a pas été fourni";
$MESS["SUPA_ACE_ACT"] = "Erreur lors de la tentative d'activation du coupon";
$MESS["SUPA_AERR_PASSW_CONF"] = "Le mot de passe et la confirmation de mot de passe sont différents.";
?>