<?
$MESS["M_CRM_PRODUCT_EDIT_SAVE_BTN"] = "Salvar";
$MESS["M_CRM_PRODUCT_EDIT_CANCEL_BTN"] = "Cancelar";
$MESS["M_CRM_PRODUCT_EDIT_CREATE_TITLE"] = "Criar produto";
$MESS["M_CRM_PRODUCT_EDIT_EDIT_TITLE"] = "Editar";
$MESS["M_CRM_PRODUCT_EDIT_VIEW_TITLE"] = "Ver produto";
$MESS["M_DETAIL_PULL_TEXT"] = "Puxe para baixo para atualizar...";
$MESS["M_DETAIL_DOWN_TEXT"] = "Solte para atualizar...";
$MESS["M_DETAIL_LOAD_TEXT"] = "Atualizando...";
$MESS["M_CRM_PRODUCT_MENU_EDIT"] = "Editar";
$MESS["M_CRM_PRODUCT_MENU_DELETE"] = "Excluir";
?>