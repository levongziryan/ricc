<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_PRODUCT_NOT_FOUND"] = "O produto não foi encontrado.";
$MESS["CRM_SECTION_PRODUCT_INFO"] = "Informações do produto";
$MESS["CRM_SECTION_PRODUCT_SECTION"] = "Seção de produto";
$MESS["CRM_FIELD_PRODUCT_NAME"] = "Nome";
$MESS["CRM_FIELD_PRODUCT_DESCRIPTION"] = "Descrição";
$MESS["CRM_FIELD_ACTIVE"] = "Ativo";
$MESS["CRM_FIELD_PRICE"] = "Preço";
$MESS["CRM_FIELD_CURRENCY"] = "Moeda";
$MESS["CRM_FIELD_SORT"] = "Classificar";
$MESS["CRM_FIELD_SECTION"] = "Seção";
$MESS["CRM_PRODUCT_ADD_UNKNOWN_ERROR"] = "Erro ao criar o produto.";
$MESS["CRM_PRODUCT_UPDATE_UNKNOWN_ERROR"] = "Erro ao atualizar o produto.";
$MESS["CRM_PRODUCT_DELETE_UNKNOWN_ERROR"] = "Erro ao excluir o produto.";
$MESS["CRM_PRODUCT_NAV_TITLE_EDIT"] = "Produto: #NAME#";
$MESS["CRM_PRODUCT_NAV_TITLE_ADD"] = "Novo produto";
$MESS["CRM_PRODUCT_NAV_TITLE_LIST"] = "Produtos";
$MESS["CRM_FIELD_VAT_INCLUDED"] = "IVA incluído";
$MESS["CRM_FIELD_VAT_ID"] = "taxa de IVA";
$MESS["CRM_FIELD_MEASURE"] = "Unidade de medida";
$MESS["CRM_MEASURE_NOT_SELECTED"] = "[não selecionado]";
$MESS["CRM_PRODUCT_FIELD_PREVIEW_PICTURE"] = "Pré-visualizar imagem";
$MESS["CRM_PRODUCT_FIELD_DETAIL_PICTURE"] = "Imagem completa";
$MESS["CRM_PRODUCT_PROP_ENLARGE"] = "Ampliar";
$MESS["CRM_PRODUCT_PROP_DOWNLOAD"] = "Baixar";
?>