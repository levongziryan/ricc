<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
$MESS["CRM_PRODUCT_NOT_FOUND"] = "Le produit est introuvable.";
$MESS["CRM_SECTION_PRODUCT_INFO"] = "Informations du produit";
$MESS["CRM_SECTION_PRODUCT_SECTION"] = "Section du produit";
$MESS["CRM_FIELD_PRODUCT_NAME"] = "Prénom";
$MESS["CRM_FIELD_PRODUCT_DESCRIPTION"] = "Description";
$MESS["CRM_FIELD_ACTIVE"] = "Actif";
$MESS["CRM_FIELD_PRICE"] = "Prix";
$MESS["CRM_FIELD_CURRENCY"] = "Devise";
$MESS["CRM_FIELD_SORT"] = "Trier";
$MESS["CRM_FIELD_SECTION"] = "Section";
$MESS["CRM_PRODUCT_ADD_UNKNOWN_ERROR"] = "Erreur de création du produit.";
$MESS["CRM_PRODUCT_UPDATE_UNKNOWN_ERROR"] = "Erreur de mise à jour du produit.";
$MESS["CRM_PRODUCT_DELETE_UNKNOWN_ERROR"] = "Erreur de suppression du produit.";
$MESS["CRM_PRODUCT_NAV_TITLE_EDIT"] = "Produit: #NAME#";
$MESS["CRM_PRODUCT_NAV_TITLE_ADD"] = "Nouveau produit";
$MESS["CRM_PRODUCT_NAV_TITLE_LIST"] = "Produits";
$MESS["CRM_FIELD_VAT_INCLUDED"] = "TVA comprise";
$MESS["CRM_FIELD_VAT_ID"] = "Taux de TVA";
$MESS["CRM_FIELD_MEASURE"] = "Unité de mesure";
$MESS["CRM_MEASURE_NOT_SELECTED"] = "[non sélectionné]";
$MESS["CRM_PRODUCT_FIELD_PREVIEW_PICTURE"] = "Aperçu de l'image";
$MESS["CRM_PRODUCT_FIELD_DETAIL_PICTURE"] = "Image complète";
$MESS["CRM_PRODUCT_PROP_ENLARGE"] = "Élargir";
$MESS["CRM_PRODUCT_PROP_DOWNLOAD"] = "Télécharger";
?>