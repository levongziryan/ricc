<?
$MESS["CRM_CATALOG_ID"] = "Catalogue commercial";
$MESS["CRM_PRODUCT_ID"] = "ID du produit";
$MESS["CRM_PRODUCT_ID_PARAM"] = "Nom de variable d'ID du produit";
$MESS["CRM_PATH_TO_PRODUCT_LIST"] = "Template du chemin de page des produits";
$MESS["CRM_PATH_TO_PRODUCT_EDIT"] = "Template du chemin de page de modification du produit";
$MESS["CRM_PATH_TO_PRODUCT_SHOW"] = "Template du chemin de page de visualisation du produit";
$MESS["CRM_CATALOG_NOT_SELECTED"] = "[non sélectionné]";
?>