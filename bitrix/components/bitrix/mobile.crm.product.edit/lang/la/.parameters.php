<?
$MESS["CRM_CATALOG_ID"] = "Catálogo Comercial";
$MESS["CRM_PRODUCT_ID"] = "ID del producto";
$MESS["CRM_PRODUCT_ID_PARAM"] = "ID del nombre de la variable del producto";
$MESS["CRM_PATH_TO_PRODUCT_LIST"] = "Plantilla de ruta a la página de productos";
$MESS["CRM_PATH_TO_PRODUCT_EDIT"] = "Plantilla de ruta a la página de edición del producto";
$MESS["CRM_PATH_TO_PRODUCT_SHOW"] = "Plantilla de ruta a la página de vista del producto";
$MESS["CRM_CATALOG_NOT_SELECTED"] = "[no seleccionado]";
?>