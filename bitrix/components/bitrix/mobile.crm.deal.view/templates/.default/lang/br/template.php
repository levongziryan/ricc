<?
$MESS["M_CRM_DEAL_VIEW_PULL_TEXT"] = "Puxe para baixo para atualizar ...";
$MESS["M_CRM_DEAL_VIEW_DOWN_TEXT"] = "Solte para atualizar ...";
$MESS["M_CRM_DEAL_VIEW_LOAD_TEXT"] = "Atualizando ...";
$MESS["M_CRM_DEAL_VIEW_ID"] = "##ID#";
$MESS["M_CRM_DEAL_VIEW_STAGE"] = "Etapa";
$MESS["M_CRM_DEAL_VIEW_PROBABILITY"] = "Probabilidade";
$MESS["M_CRM_DEAL_VIEW_ACTION_CALL_TO_CLIENT"] = "Chamar";
$MESS["M_CRM_DEAL_VIEW_NO_TITLE"] = "[Sem título]";
$MESS["M_CRM_DEAL_VIEW_PRODUCT_ROWS"] = "Produtos";
$MESS["M_CRM_DEAL_VIEW_ACTIVITY_LIST"] = "Atividades";
$MESS["M_CRM_DEAL_VIEW_EVENT_LIST"] = "Histórico";
$MESS["M_CRM_DEAL_VIEW_RESPONSIBLE"] = "Responsável";
$MESS["M_CRM_DEAL_VIEW_COMMENT"] = "Comentar";
$MESS["M_CRM_DEAL_VIEW_COMMENT_CUT"] = "Leia mais ...";
$MESS["M_CRM_DEAL_BEGINDATE"] = "Iniciado em";
$MESS["M_CRM_DEAL_CLOSEDATE"] = "Encerrado em";
$MESS["M_CRM_DEAL_TYPE"] = "Tipo";
$MESS["M_CRM_DEAL_VIEW_EDIT"] = "Editar";
$MESS["M_CRM_DEAL_VIEW_DELETE"] = "Excluir";
$MESS["M_CRM_DEAL_VIEW_DELETION_CONFIRMATION"] = "Você tem certeza que deseja excluir o negócio?";
$MESS["M_CRM_DEAL_VIEW_DELETION_TITLE"] = "Excluir negócio";
$MESS["M_CRM_DEAL_VIEW_CREATE_INVOICE"] = "Criar fatura";
?>