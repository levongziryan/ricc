<?
$MESS["M_CRM_DEAL_VIEW_PULL_TEXT"] = "Pulsar hacia abajo para actualizar...";
$MESS["M_CRM_DEAL_VIEW_DOWN_TEXT"] = "Soltar para actualizar...";
$MESS["M_CRM_DEAL_VIEW_LOAD_TEXT"] = "Actualizando...";
$MESS["M_CRM_DEAL_VIEW_ID"] = "##ID#";
$MESS["M_CRM_DEAL_VIEW_STAGE"] = "Etapa";
$MESS["M_CRM_DEAL_VIEW_PROBABILITY"] = "Probabilidad";
$MESS["M_CRM_DEAL_VIEW_ACTION_CALL_TO_CLIENT"] = "Llamada";
$MESS["M_CRM_DEAL_VIEW_NO_TITLE"] = "[sin título]";
$MESS["M_CRM_DEAL_VIEW_PRODUCT_ROWS"] = "Productos";
$MESS["M_CRM_DEAL_VIEW_ACTIVITY_LIST"] = "Actividades";
$MESS["M_CRM_DEAL_VIEW_EVENT_LIST"] = "Historial";
$MESS["M_CRM_DEAL_VIEW_RESPONSIBLE"] = "Persona responsable";
$MESS["M_CRM_DEAL_VIEW_COMMENT"] = "Comentarios";
$MESS["M_CRM_DEAL_VIEW_COMMENT_CUT"] = "Leer más...";
$MESS["M_CRM_DEAL_BEGINDATE"] = "Iniciado el";
$MESS["M_CRM_DEAL_CLOSEDATE"] = "Cerrado el ";
$MESS["M_CRM_DEAL_TYPE"] = "Tipo";
$MESS["M_CRM_DEAL_VIEW_EDIT"] = "Editar";
$MESS["M_CRM_DEAL_VIEW_DELETE"] = "Eliminar";
$MESS["M_CRM_DEAL_VIEW_DELETION_CONFIRMATION"] = "Está seguro que desea eliminar el prospecto?";
$MESS["M_CRM_DEAL_VIEW_DELETION_TITLE"] = "Eliminar prospecto";
$MESS["M_CRM_DEAL_VIEW_CREATE_INVOICE"] = "Crear factura";
?>