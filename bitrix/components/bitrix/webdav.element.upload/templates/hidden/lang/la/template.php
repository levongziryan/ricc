<?
$MESS["WD_DROP_CONFIRM"] = "¿Está seguro que desea eliminar la carpeta #NAME# irreversiblemente?";
$MESS["WD_NAME"] = "Nombre";
$MESS["WD_DROP_SECTION"] = "Eliminar archivo #NAME#";
$MESS["WD_EDIT_SECTION"] = "Editar archivo #NAME#";
$MESS["WD_ADD_SECTION"] = "Crear archivo";
$MESS["WD_DROP"] = "Eliminar";
$MESS["WD_SAVE"] = "Crear";
$MESS["WD_CANCEL"] = "Cancelar";
$MESS["WD_CLOSE"] = "Cerrar";
$MESS["WD_UPLOAD_TITLE"] = "Cargar documento nuevo";
$MESS["WD_UPLOAD_VERSION_TITLE"] = "Reemplazar el documento para \"#NAME#\"";
$MESS["WD_UPLOAD_SUCCESS"] = "El archivo se ha cargado correctamente.";
$MESS["File"] = "Seleccione un archivo para cargar";
$MESS["Title"] = "Nombre";
$MESS["Tags"] = "Etiquetas";
$MESS["Send"] = "Subir";
$MESS["Send_Document"] = "Cargar documento nuevo";
$MESS["Send_Version"] = "Cargar una nueva versión";
$MESS["Description"] = "Descripción";
$MESS["WD_UPLOAD_EXTENDED"] = "<a href=\"#LINK#\">Cargar varios documentos</a>";
$MESS["WD_UPLOAD_EXPAND_PROPS"] = "Conjunto <a #LINK#>propiedades del documento</a>";
$MESS["WD_UPLOAD_COLLAPSE_PROPS"] = "Ocultar <a #LINK#>propiedades del documento</a>";
$MESS["WD_UPLOAD_INTERRUPT_BEWARE"] = "El documento está siendo cargado. El proceso de carga se anulará si usted sale de esta página.";
$MESS["WD_UPLOAD_INTERRUPT_CONFIRM"] = "El documento aún se está cargando. ¿Está seguro de que desea Cancelar?";
$MESS["WD_UPLOAD_NOT_DONE"] = "El documento aún no se ha cargado.";
$MESS["WD_UPLOAD_NOT_DONE_ASK"] = "El documento aún no se ha cargado. ¿Cierre el cuadro de diálogo de carga?";
$MESS["WD_WARNING_SAME_NAME"] = "Ya existe un archivo con ese nombre.Usted puede <a #LINK#>cambiar el nombre de archivo</a>, o cargar una nueva versión haciendo clic en \"Cargar\" (que conserva la versión anterior en el registro de cambios)";
$MESS["WD_ERROR_SAME_NAME"] = "Ya existe un archivo con este nombre.";
$MESS["WD_ERROR_BAD_STATUS"] = "No tienes permiso para guardar el archivo.";
$MESS["WD_WARNING_EXTENSIONS_DONT_MATCH"] = "El tipo de archivo que está intentando cargar no coincide con el tipo de archivo existente.";
$MESS["WD_UPLOAD_UNLOCK"] = "Desbloquear documento después de carga";
$MESS["WD_ERROR_BAD_SESSID"] = "Su sesión ha caducado. Por favor, inténtelo de nuevo.";
$MESS["WD_WARNING_FIRST_DOT"] = "El nombre comienza con un punto. El documento no es mostrado en la lista de documentos bajo parámetros actuales.";
$MESS["WD_UPLOAD_DROPHERE"] = "Puede arrastrar un archivo desde su equipo a esta ventana y suéltelo.";
?>