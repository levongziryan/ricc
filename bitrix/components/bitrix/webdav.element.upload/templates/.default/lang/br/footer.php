<?
$MESS["WD_WF_ATTENTION1"] = "Atenção! Você não pode modificar os arquivos enviados.";
$MESS["WD_BP"] = "Processos de negócios";
$MESS["IBEL_BIZPROC_DATE"] = "Data Estado atual:";
$MESS["IBEL_BIZPROC_STATE"] = "Estado atual:";
$MESS["IBEL_BIZPROC_NA"] = "Não existe processos de negócios sendo executados no momento";
$MESS["IBEL_BIZPROC_RUN_CMD_NO"] = "Nenhum";
$MESS["WD_OVERVIEW"] = "Substituir o arquivo existente (s)?";
$MESS["IBEL_BIZPROC_RUN_CMD"] = "Executar Ação:";
$MESS["WD_WF_STATUS"] = "Status para novos arquivos enviados";
$MESS["WD_WF_ATTENTION2"] = "Você pode editar arquivos no Estado Estado #STATUS#";
$MESS["WD_WF_ATTENTION3"] = "Você pode editar arquivos no Estado Estado #STATUS#";
$MESS["BPABL_RES_6"] = "Indefinido";
$MESS["WD_DOCUMENT_BP"] = "Processos de Negócios";
$MESS["WD_USER_VIEW_URL"] = "Página de Informação de usuário";
$MESS["WD_ADD_SECTION"] = "Criar Pasta";
$MESS["IBLOCK_ID_TIP"] = "Selecione aqui um dos blocos de informação existentes. Se você selecionar <b> <i> (outro) </i> </b>, você terá que especificar o ID do bloco de informações no campo ao lado. Por exemplo: <b> = {\$ _REQUEST [\"IBLOCK_ID\"]} </b>";
$MESS["WD_SECTION_LIST_URL"] = "Página de visualização de catálogos";
$MESS["WD_WHO"] = "Alteradas por";
$MESS["WD_WHEN"] = "ltima modificação em";
?>