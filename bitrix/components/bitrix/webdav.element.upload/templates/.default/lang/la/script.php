<?
$MESS["IU_ATTENTION"] = "¡atención!";
$MESS["File"] = "Archivo";
$MESS["Title"] = "Título";
$MESS["Tags"] = "Etiquetas";
$MESS["Description"] = "Descripción";
$MESS["IU_DISABLED_JAVA"] = "Java Virtual Machive no se instala o no es compatible con su navegador. Por favor use <a target=\"_self\" href=\"#HREF_SIMPLE#\">formulario simple de carga</a> o instale Java Virtual Machive desde el sitio web oficial: <a target=\"_blank\" href=\"http://java.com/\">java.com</a>.";
?>