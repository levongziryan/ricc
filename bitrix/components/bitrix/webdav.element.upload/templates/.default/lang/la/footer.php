<?
$MESS["WD_WF_STATUS"] = "Estado de los Nuevos archivos subidos";
$MESS["WD_WF_ATTENTION1"] = "¡Atención! Usted no puede modificar los archivos subidos.";
$MESS["WD_WF_ATTENTION2"] = "Puede editar los archivos en el estado #STATUS#.";
$MESS["WD_WF_ATTENTION3"] = "Puede editar los archivos en el estado #STATUS#.";
$MESS["IBEL_BIZPROC_DATE"] = "Fecha actual del estado:";
$MESS["IBEL_BIZPROC_STATE"] = "Estado actual:";
$MESS["IBEL_BIZPROC_RUN_CMD_NO"] = "Ninguno";
$MESS["WD_BP"] = "Procesos de negocio";
$MESS["IBEL_BIZPROC_RUN_CMD"] = "Ejecutar acción:";
$MESS["IBEL_BIZPROC_NA"] = "No ejecutar procesos de negocio.";
$MESS["WD_OVERVIEW"] = "¿Sobre escribir el(los) archivo(s) existente(s)?";
?>