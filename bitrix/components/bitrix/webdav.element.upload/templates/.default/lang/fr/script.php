<?
$MESS["IU_ATTENTION"] = "Attention!";
$MESS["Title"] = "Titre";
$MESS["Description"] = "Description";
$MESS["IU_DISABLED_JAVA"] = "Le logiciel Java n'a pas été installé sur votre PC ou il n'est pas soutenu par le navigateur. Pour charger les fichiers, vous pouvez utiliser <a target='_self' href='#HREF_SIMPLE#'> une forme simple de chargement</a> ou installer Java depuis le site officiel <a target='_blank' href='http://java.com/'>java.com</a>.";
$MESS["Tags"] = "Tags";
$MESS["File"] = "Fichier";
?>