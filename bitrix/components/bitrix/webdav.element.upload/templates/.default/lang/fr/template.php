<?
$MESS["WD_GOTO_LIBRARY"] = "Revenir à la bibliothèque de documents";
$MESS["WD_VIEW"] = "Affichage";
$MESS["IU_ATTENTION"] = "Attention!";
$MESS["AddFolders"] = "Ajouter un dossier";
$MESS["AddFiles"] = "Ajouter les fichiers";
$MESS["Send"] = "Chargement";
$MESS["WD_LOADING"] = "En cours de chargement...";
$MESS["WD_IU_CLASSIC_TITLE"] = "Utilisé dans les versions précédentes.";
$MESS["WD_IU_CLASSIC"] = "classique";
$MESS["Title"] = "Titre";
$MESS["NoFiles"] = "non";
$MESS["ErrorNoData"] = "Il n'y a pas de données. Vous essayez, peut-être, de charger un trop grand fichier dont la taille est supérieure à #POST_MAX_SIZE# Mo.";
$MESS["WD_IU_APPLET"] = "classique";
$MESS["WD_ATTENTION2_1"] = "Les fichiers portent des pseudonymes identiques:";
$MESS["Description"] = "Description";
$MESS["WD_IU_FORM_TITLE"] = "Permet de charger les fichiers sans installation d'un logiciel supplémentaire.";
$MESS["WD_IU_APPLET_TITLE"] = "Permet d'introduire facilement les données supplémentaires sur le fichier au démarrage.";
$MESS["WD_IU_FORM"] = "Ordinaire";
$MESS["WD_ATTENTION2_2"] = "Pseudonyme '#TITLE#' pour les fichiers: #FILES#";
$MESS["WD_ATTENTION_FILESIZE"] = "La taille du fichier chargé ne doit pas excéder <b>#FILE_SIZE# Mo</b>.";
$MESS["IU_DISABLED_JAVASCRIPT"] = "JavaScript est désactivé ou non supporté par votre navigateur. S'il vous plaît utiliser <a target='_self' href='#HREF_SIMPLE#'>forme simple de téléchargement</a> pour télécharger vos fichiers.";
$MESS["Tags"] = "Tags";
$MESS["RemoveAllFromUploadList"] = "Supprimer tous";
$MESS["File"] = "Fichier";
$MESS["Files"] = "Fichiers";
$MESS["WD_TOO_OLD_JAVA"] = "Vous avez trop ancienne version Java. S'il vous plaît aller à <a href='http://java.com/en/download/installed.jsp'>verify java</a> page, mettez à jour votre version de Java et de revenir.";
?>