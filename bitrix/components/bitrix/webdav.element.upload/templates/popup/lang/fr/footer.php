<?
$MESS["WD_BP"] = "De processus business";
$MESS["IBEL_BIZPROC_NA"] = "Les processus d'affaires ne sont pas en cours d'exécution.";
$MESS["WD_WF_STATUS"] = "Avec quel statut charger le document";
$MESS["WD_WF_ATTENTION1"] = "Attention! Vous ne pouvez pas modifier les fichiers téléchargés.";
$MESS["WD_WF_ATTENTION3"] = "Vous pouvez éditer les fichiers dont le staut est #STATUS#.";
$MESS["WD_WF_ATTENTION2"] = "Vous pouvez éditer les fichiers dont le staut est #STATUS#.";
$MESS["IBEL_BIZPROC_RUN_CMD"] = "Exécuter l'ordre:";
$MESS["IBEL_BIZPROC_DATE"] = "Date de l'état courant:";
$MESS["IBEL_BIZPROC_RUN_CMD_NO"] = "Ne pas exécuter";
$MESS["WD_OVERVIEW"] = "Remplacer les fichiers existants";
$MESS["WD_USER_FIELDS"] = "Champs d'utilisateur";
$MESS["IBEL_BIZPROC_STATE"] = "Statut courant:";
?>