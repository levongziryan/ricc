<?
$MESS["WD_WF_STATUS"] = "Status para novos arquivos enviados";
$MESS["WD_WF_ATTENTION1"] = "Atenção! Você não pode modificar os arquivos enviados.";
$MESS["WD_WF_ATTENTION2"] = "Você pode editar arquivos no Estado Estado #STATUS#.";
$MESS["WD_WF_ATTENTION3"] = "Você pode editar arquivos no Estado Estado #STATUS#.";
$MESS["WD_OVERVIEW"] = "Substituir os arquivos existentes";
$MESS["IBEL_BIZPROC_DATE"] = "Data Estado atual:";
$MESS["IBEL_BIZPROC_NA"] = "Não existem processos de negócios.";
$MESS["IBEL_BIZPROC_RUN_CMD"] = "Executar o comando:";
$MESS["IBEL_BIZPROC_RUN_CMD_NO"] = "Não Executar";
$MESS["IBEL_BIZPROC_STATE"] = "Estado atual:";
$MESS["WD_BP"] = "Processos de Negócios";
$MESS["WD_USER_FIELDS"] = "Campos personalizados";
$MESS["BPADH_AUTHOR"] = "Autor";
$MESS["WD_HISTORY_FILE_ALT2"] = "Ver log de alterações versão";
$MESS["WD_DOCUMENT_ALT"] = "Propriedades do Documento";
$MESS["WD_DROP_SECTION"] = "Excluir a pasta #NAME#";
$MESS["SORT_BY2_TIP"] = "Selecione aqui o campo pelo qual os itens que são documentos serão classificados na segunda passagem. Você pode selecionar <b> <i> (outro) </i> </b> e especificar o ID do campo no campo ao lado.";
$MESS["WD_ELEMENT_UPLOAD_URL"] = "Página de envio de arquivos";
$MESS["WD_DOCTYPE"] = "Tipo de documento";
$MESS["WD_DOCTYPE_DOCUMENTS"] = "Documentos";
?>