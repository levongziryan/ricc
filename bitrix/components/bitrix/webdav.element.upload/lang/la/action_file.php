<?
$MESS["WD_ERROR_BAD_SESSID"] = "Su sesión ha expirado. Por favor repita la operación.";
$MESS["WD_ERROR_BAD_POST"] = "No hay base de datos. Es como si usted estuviera intentando cargar muchos archivos que en total exceden los #SIZE# MB.";
$MESS["WD_ERROR_DOUBLE_NAME_SECTION"] = "La carpeta '#NAME#' ya existe.";
$MESS["WD_ERROR_DOUBLE_NAME_ELEMENT"] = "El archivo '#NAME#' ya existe.";
$MESS["WD_ERROR_UPLOAD_MAX_FILE_SIZE"] = "El tamaño del #NAME# está sobre #SIZE#MB";
$MESS["WD_ERROR_UPLOAD_BAD_FILE"] = "El archivo fue cargado en forma incompleta.";
$MESS["WD_ERROR_UPLOAD_FILE_NOT_LOAD"] = "El archivo falló al cargar.";
$MESS["WD_ERROR_BAD_ELEMENT_NAME"] = "El nombre del archivo no debe contener los siguientes caracteres: /\\:*?\"'<>|{}%&~";
$MESS["WD_ERROR_DOUBLE_NAME_TITLE"] = "Error al cargar el archivo \"#NAME#\" con el título \"#TITLE#\". Un archivo con este título \"#TITLE#\" ya existe.";
?>