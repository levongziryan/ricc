<?
$MESS["WD_ERROR_BAD_SESSID"] = "Su sesión ha expirado. Por favor repita la operación.";
$MESS["WD_ERROR_BAD_POST"] = "No hay datos. Es probable que usted está intentando cargar demasiados archivos cuyo tamaño total se excede #SIZE# MB.";
$MESS["WD_ERROR_DOUBLE_NAME_SECTION"] = "La carpeta '#NAME#' ya existe.";
$MESS["WD_ERROR_DOUBLE_NAME_ELEMENT"] = "El archivo '#NAME#' ya existe.";
$MESS["WD_ERROR_ADD"] = "Error al cargar el archivo.";
$MESS["WD_ERROR_UPDATE"] = "Error al cargar el archivo.";
$MESS["WD_ERROR_UPLOAD_MAX_FILE_SIZE"] = "El tamaño del #NAME# está sobre #SIZE#MB";
$MESS["WD_ERROR_UPLOAD_BAD_FILE"] = "El archivo se cargó incompleto.";
$MESS["WD_ERROR_UPLOAD_FILE_NOT_LOAD"] = "El archivo falló al cargar.";
$MESS["WD_ERROR_BAD_ELEMENT_NAME"] = "El nombre del archivo no contiene los siguientes caracteres: /\\:*?\"'<>|{}%&~";
$MESS["WD_FILE_MODIFIED"] = "Editar Archivo";
$MESS["WD_NEW_FILE"] = "Nuevo Archivo.";
$MESS["WD_ERROR_BAD_STATUS"] = "Usted no puede guardar el archivo en este estado.";
$MESS["WD_ERROR_DOUBLE_NAME_TITLE"] = "Error al cargar el archivo \"#NAME#\" con el título \"#TITLE#\". Un archivo con el título \"#TITLE#\" ya existe.";
$MESS["WD_ERROR_DOUBLE_NAME_ELEMENT_NOT_REWRITE"] = "El archivo '#NAME#' ya existe y es de propiedad de otro usuario. Usted no tiene permiso para escribir en este archivo.";
$MESS["WD_ERROR_EXTENSIONS_DONT_MATCH"] = "Error al actualizar el archivo: el tipo de archivo no coincide que el tipo de documento actual.";
?>