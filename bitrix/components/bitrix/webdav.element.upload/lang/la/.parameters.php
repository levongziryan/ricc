<?
$MESS["WD_IBLOCK_TYPE"] = "Tipo de block de información";
$MESS["WD_IBLOCK_ID"] = "Block de información";
$MESS["WD_SECTION_ID"] = "ID de la sección";
$MESS["T_IBLOCK_DESC_NEWS_PANEL"] = "Mostrar panel de botones para este componente";
$MESS["WD_SECTION_LIST_URL"] = "Página de vista de catálogo";
$MESS["WD_ELEMENT_UPLOAD_URL"] = "Página de carga del archivo";
$MESS["WD_NAME_FILE_PROPERTY"] = "Nombre de la propiedad del archivo almacenado. (las propiedad del block de información podrá ser creada si lo requiere)";
$MESS["WD_UPLOAD_MAX_FILE"] = "Máximo de archivos enviados por mensaje";
$MESS["WD_UPLOAD_MAX_FILESIZE"] = "Tamaño máximo del archivo (MB)";
$MESS["WD_PERMISSION"] = "Permisos de acceso externo (utilizar en componentes compuestos)";
$MESS["WD_ROOT_SECTION_ID"] = "ID de la raíz de la sección (usar en componentes compuestos)";
$MESS["WD_REPLACE_SYMBOLS"] = "Reemplazar símbolos prohibidos en la carpeta y en los nombres del archivo";
$MESS["WD_CHECK_CREATOR"] = "Verificar Creador del Archivo";
?>