<?
$MESS["WD_ROOT_SECTION_ID"] = "ID de la section de racine (il est recommandé d'utiliser en ensemble avec un composant complexe)";
$MESS["WD_SECTION_ID"] = "ID de la section";
$MESS["WD_PERMISSION"] = "Droits d'accès externes (il est recommandé d'utiliser dans le cadre d'un composant intégré)";
$MESS["T_IBLOCK_DESC_NEWS_PANEL"] = "Ajouter les boutons au panneau admin. Pour ce composant";
$MESS["WD_REPLACE_SYMBOLS"] = "Remplacer des caractères interdits dans les noms de dossiers et de fichiers";
$MESS["WD_IBLOCK_ID"] = "Bloc d'information";
$MESS["WD_UPLOAD_MAX_FILE"] = "Nombre maximal de fichiers envoyés par un post";
$MESS["WD_UPLOAD_MAX_FILESIZE"] = "Taille maximale du fichier (MB)";
$MESS["WD_NAME_FILE_PROPERTY"] = "Code de la qualité du bloc d'information pour le stockage du fichier (la qualité sera créée, si elle n'existe pas)";
$MESS["WD_CHECK_CREATOR"] = "Vérifier le propriétaire du document";
$MESS["WD_ELEMENT_UPLOAD_URL"] = "Page du chargement des fichiers";
$MESS["WD_SECTION_LIST_URL"] = "Page d'examen du catalogue";
$MESS["WD_IBLOCK_TYPE"] = "Type du bloc d'information";
?>