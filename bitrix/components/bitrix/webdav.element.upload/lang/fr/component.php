<?
$MESS["WD_ACCESS_DENIED"] = "Accès interdit.";
$MESS["WD_TITLE"] = "Chargement de fichiers";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "Le module de la bibliothèque des documents n'a pas été installé.";
$MESS["WD_NO_BP_AUTORUN"] = "Il n'y a pas de processus business avec l'autochargement ajusté.";
$MESS["WD_NO_BP_TEMPLATES"] = "Il n'y a pas de processus business.";
$MESS["WD_BP_ACTIVE_STATES"] = "Pas de processus d'affaires avec les statuts accessibles.";
$MESS["WD_UPLOAD_ERROR_TITLE"] = "Erreur lors du chargement du document.";
$MESS["WD_ERROR_BAD_SECTION"] = "Dossier n'est pas retrouvé";
$MESS["WD_NO_BP_TEMPLATES_ADMIN"] = "Accéder à <a hreg='#LINK#'>la liste des processus d'affaires </a>.";
?>