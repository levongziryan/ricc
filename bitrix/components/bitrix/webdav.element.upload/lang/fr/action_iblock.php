<?
$MESS["WD_ERROR_BAD_SESSID"] = "Votre session a expiré. Répétez cette opération encore une fois, s'il vous plaît.";
$MESS["WD_FILE_MODIFIED"] = "Edition du fichier";
$MESS["WD_ERROR_BAD_ELEMENT_NAME"] = "Le nom du fichier '#NAME#' comprend les symboles interdits: /\\:*?''<>|{}%&~";
$MESS["WD_ERROR_BAD_POST"] = "Pas de données. Peut-être vous chargez trop de fichiers dont la taille totale dépasse #SIZE#Mb.";
$MESS["WD_NEW_FILE"] = "Nouveau fichier.";
$MESS["WD_ERROR_DOUBLE_NAME_TITLE"] = "Erreur de téléchargement du fichier '#NAME#' avec le pseudonyme '#TITLE#'. Le fichier nommé '#TITLE#' existe déjà.";
$MESS["WD_ERROR_ADD"] = "Erreur de chargement du fichier.";
$MESS["WD_ERROR_UPDATE"] = "Erreur de rafraîchissement du fichier.";
$MESS["WD_ERROR_DOUBLE_NAME_SECTION"] = "Le dossier nommé '#NAME#' existe déjà.";
$MESS["WD_ERROR_EXTENSIONS_DONT_MATCH"] = "Une erreur est survenue lors du chargement du fichier. Le type de fichier ne correspond pas au type existant de document.";
$MESS["WD_ERROR_UPLOAD_MAX_FILE_SIZE"] = "La taille du fichier '#NAME#' dépasse #SIZE#Mb.";
$MESS["WD_ERROR_BAD_STATUS"] = "Vous n'avez pas de droits pour sauvegarder le document dans ce statut.";
$MESS["WD_ERROR_UPLOAD_BAD_FILE"] = "Fichier chargé au serveur non pas en entier.";
$MESS["WD_ERROR_UPLOAD_FILE_NOT_LOAD"] = "Le fichier n'a pas été téléchargé.";
$MESS["WD_ERROR_DOUBLE_NAME_ELEMENT_NOT_REWRITE"] = "Le fichier avec le nom '#NAME#' existe déjà et appartient à un autre utilisateur. Vous n'avez pas le droit d'écraser ce fichier.";
$MESS["WD_ERROR_DOUBLE_NAME_ELEMENT"] = "Le fichier avec le nom '#NAME#' existe déjà.";
?>