<?
$MESS["WD_ACCESS_DENIED"] = "Acesso negado.";
$MESS["WD_ERROR_BAD_SECTION"] = "A pasta não foi encontrada.";
$MESS["WD_TITLE"] = "Upload de arquivos";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "Módulo WebDav não está instalado.";
$MESS["WD_NO_BP_TEMPLATES"] = "Não há processos de negócios.";
$MESS["WD_NO_BP_TEMPLATES_ADMIN"] = "hreg=\"#LINK#\"> <a Ver todos os processos de negócios </a>";
$MESS["WD_UPLOAD_ERROR_TITLE"] = "Erro ao carregar o documento.";
$MESS["WD_NO_BP_AUTORUN"] = "Nenhum processo de negócio de execução automática.";
$MESS["WD_BP_ACTIVE_STATES"] = "Nenhuma empresa com status disponíveis.";
$MESS["WD_URL_ELEMENT_UPLOAD"] = "Carregar página";
$MESS["WD_DELETE_ELEMENT"] = "Excluir Elemento";
$MESS["DOCLIST_DOT_DEFAULT_DESC"] = "Modelo padrão";
$MESS["WD_NAME"] = "Gerenciar Elemento";
$MESS["WD_VERSION_ALT"] = "Propriedades da versão do Documento";
$MESS["PAGE_NAVIGATION_TEMPLATE"] = "Modelo de Navegação Breadcrumb";
$MESS["WD_ROOT_SECTION_ID"] = "ID raiz da Seção (uso em componentes compostos)";
$MESS["WD_EDIT_SECTION"] = "Editar pasta #NAME#";
?>