<?
$MESS["WD_SECTION_LIST_URL"] = "Página de visualização de catálogos";
$MESS["WD_CHECK_CREATOR"] = "Verifique o criador de arquivo";
$MESS["T_IBLOCK_DESC_NEWS_PANEL"] = "Mostrar botões do painel para este componente";
$MESS["WD_PERMISSION"] = "Permissão de acesso externo (uso em componentes compostos)";
$MESS["WD_NAME_FILE_PROPERTY"] = "Nome do arquivo de armazenamento de propriedade (a propriedade bloco de informações será criado se necessário)";
$MESS["WD_ELEMENT_UPLOAD_URL"] = "Página para envio de arquivos";
$MESS["WD_IBLOCK_ID"] = "Bloco de informação";
$MESS["WD_UPLOAD_MAX_FILESIZE"] = "Tamanho máximo (MB)";
$MESS["WD_UPLOAD_MAX_FILE"] = "Máximo de arquivos enviados por mensagem";
$MESS["WD_REPLACE_SYMBOLS"] = "Substitua símbolos não permitidos nos nomes de pastas e arquivos";
$MESS["WD_ROOT_SECTION_ID"] = "ID da raiz Seção (uso em componentes compostos)";
$MESS["WD_SECTION_ID"] = "Id da Seção";
$MESS["WD_IBLOCK_TYPE"] = "Tipo de bloco de informação";
$MESS["WD_HISTORY_FILE"] = "História";
$MESS["WD_AG_HELP6"] = "Para mais informações sobre como utilizar as bibliotecas em Windows e Mac OS X, consulte o #STARTLINK# Documento seção de ajuda Biblioteca #ENDLINK#..";
$MESS["WD_TITLE"] = "Ajudar";
$MESS["WD_DISK_NOTIFY_FILE_NUMERAL_1"] = "#COUNT# arquivo";
$MESS["IBEL_BIZPROC_TASKS"] = "Processo de tarefas de negócios";
$MESS["WD_DOCUMENTS"] = "Documentos";
$MESS["WD_HISTORY_FILE_ALT"] = "Ver versões do documento";
$MESS["T_IBLOCK_DESC_DOCS_NAME"] = "Mostrar título do elemento";
$MESS["WD_HELP_ALT"] = "Mostra ajuda sobre como usar biblioteca de documentos";
$MESS["WD_MENU_FF_EXTENSION_TEXT"] = "Extensão para o Firefox";
?>