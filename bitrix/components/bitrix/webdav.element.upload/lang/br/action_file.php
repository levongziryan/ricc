<?
$MESS["WD_ERROR_BAD_SESSID"] = "Sua sessão expirou. Por favor, tente Novamente.";
$MESS["WD_ERROR_BAD_POST"] = "Não existem dados. É provável que você está tentando carregar muitos arquivos cujo tamanho total exceda #SIZE# MB.";
$MESS["WD_ERROR_DOUBLE_NAME_SECTION"] = "A pasta '#NAME#' já existe.";
$MESS["WD_ERROR_DOUBLE_NAME_ELEMENT"] = "O arquivo '#NAME' já existe.";
$MESS["WD_ERROR_UPLOAD_MAX_FILE_SIZE"] = "O tamanho do '#NAME#' é sobre #SIZE# MB";
$MESS["WD_ERROR_UPLOAD_BAD_FILE"] = "O arquivo foi enviado de forma incompleta.";
$MESS["WD_ERROR_UPLOAD_FILE_NOT_LOAD"] = "Falha ao enviar o arquivo.";
$MESS["WD_ERROR_BAD_ELEMENT_NAME"] = "O nome do arquivo não deve conter os seguintes caracteres: / \\ \\: * \"'<> |% {} & ~?";
$MESS["WD_ERROR_DOUBLE_NAME_TITLE"] = "Erro ao enviar o arquivo  \"#NAME#\" com o título \"#TITLE#\". Um arquivo com esse título \"#TITLE#\" já existe.";
$MESS["WD_SHOW_TAGS"] = "Mostrar etiquetas";
$MESS["WD_DOCUMENT_BP_ALT"] = "Ver os processos de documentos de negócios";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "Módulo WebDav não está instalado.";
$MESS["WD_UPLOAD_NOT_DONE"] = "O documento ainda não foi enviado.";
$MESS["ACTIVE_DATE_FORMAT_TIP"] = "Selecione aqui o formato da data desejada. Se você selecionar <i><b>outro</b></i>, você pode criar seu próprio formato usando o <i><b>data</b></i> função PHP.";
?>