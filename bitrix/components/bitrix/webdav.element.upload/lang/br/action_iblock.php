<?
$MESS["WD_FILE_MODIFIED"] = "Editar Arquivo";
$MESS["WD_ERROR_DOUBLE_NAME_TITLE"] = "Arquivo de upload de erro \"#NAME#\" com o título \"# TITLE #\". Um arquivo com esse título \"#TITLE#' já existe.";
$MESS["WD_ERROR_UPDATE"] = "Erro na atualização do arquivo.";
$MESS["WD_ERROR_ADD"] = "Erro no envio do arquivo.";
$MESS["WD_NEW_FILE"] = "Novo arquivo.";
$MESS["WD_ERROR_BAD_POST"] = "Não existem dados. É provável que você está tentando carregar muitos arquivos cujo tamanho total exceda #SIZE# MB.";
$MESS["WD_ERROR_DOUBLE_NAME_ELEMENT_NOT_REWRITE"] = "#NAME#' O arquivo já existe e é de propriedade de outro usuário. Você não tem permissão para gravar este arquivo.";
$MESS["WD_ERROR_DOUBLE_NAME_ELEMENT"] = "#NAME#' O arquivo já existe.";
$MESS["WD_ERROR_UPLOAD_FILE_NOT_LOAD"] = "Falha ao enviar o arquivo.";
$MESS["WD_ERROR_BAD_ELEMENT_NAME"] = "O nome do arquivo não deve conter os seguintes caracteres: / \\ \\: * \"'<> |% {} & ~?";
$MESS["WD_ERROR_UPLOAD_BAD_FILE"] = "O arquivo foi enviado de forma incompleta.";
$MESS["WD_ERROR_DOUBLE_NAME_SECTION"] = "A pasta '#NAME#' já existe.";
$MESS["WD_ERROR_UPLOAD_MAX_FILE_SIZE"] = "O tamanho do '#NAME#' é sobre #SIZE# MB";
$MESS["WD_ERROR_BAD_STATUS"] = "Você não pode salvar o arquivo neste estado.";
$MESS["WD_ERROR_BAD_SESSID"] = "Sua sessão expirou. Por favor, repita a operação.";
$MESS["WD_ERROR_EXTENSIONS_DONT_MATCH"] = "Erro ao carregar o arquivo: o tipo de arquivo não corresponde ao tipo de documento atual.";
$MESS["WD_NAME"] = "Biblioteca";
$MESS["W_TITLE_ACTIVE"] = "Ativo";
$MESS["SEARCH_PERIOD"] = "Mostrar tags dentro (dias)";
$MESS["IBEL_BIZPROC_NEW"] = "Processo de Novos Negócios";
$MESS["WD_DOCUMENT_BP_ALT2"] = "Ver o histórico de processos de negócios nesta versão do documento";
$MESS["WD_NO_BP_TEMPLATES"] = "Não há processos de negócios.";
$MESS["WD_UPLOAD_NOT_DONE_ASK"] = "O documento ainda não foi enviado. Fechar a caixa de diálogo de upload?";
$MESS["WD_WARNING_FIRST_DOT"] = "O nome começa com um ponto. O documento não será mostrado na lista de documentos sob configurações atuais.";
$MESS["PROPERTY_CODE_TIP"] = "Selecione as propriedades de blocos de informações que você deseja exibir no filtro. Você também pode adicionar seus próprios campos nos campos abaixo.";
$MESS["WD_TITLE_NAME"] = "Nome do documento ou texto";
?>