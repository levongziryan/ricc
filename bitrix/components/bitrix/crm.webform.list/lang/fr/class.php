<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "Le module Processus d'entreprise n'a pas été installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "Le module Devise n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Le module e-Store n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "Le module Catalogue commercial n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
$MESS["CRM_WEBFORM_LIST_TITLE"] = "Formulaires CRM";
$MESS["CRM_WEBFORM_LIST_FILTER_ACTIVE_ALL"] = "tout";
$MESS["CRM_WEBFORM_LIST_FILTER_ACTIVE_Y"] = "actif";
$MESS["CRM_WEBFORM_LIST_FILTER_ACTIVE_N"] = "inactif";
?>