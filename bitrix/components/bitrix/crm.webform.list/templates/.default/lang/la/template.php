<?
$MESS["CRM_WEBFORM_LIST_DESC1"] = "Utilice formularios del CRM para aumentar la productividad. Todos los datos del formulario se guardan en el sistema del CRM; los administradores sólo tendrán que procesar la información obtenida en el interior del sistema del CRM.";
$MESS["CRM_WEBFORM_LIST_DESC2"] = "Utilice formularios en el chat, en las páginas de su sitio o enviar un enlace a una página pública para que sus clientes pueden completar el formulario cada vez que quieran.";
$MESS["CRM_WEBFORM_LIST_ADD_CAPTION"] = "Agregar un nuevo formulario";
$MESS["CRM_WEBFORM_LIST_ITEM_FILL_START"] = "total de intentos";
$MESS["CRM_WEBFORM_LIST_ITEM_FILL_END"] = "completado con éxito";
$MESS["CRM_WEBFORM_LIST_ITEM_DATE_CREATE"] = "Creado el ";
$MESS["CRM_WEBFORM_LIST_ITEM_PUBLIC_LINK"] = "Enlace público";
$MESS["CRM_WEBFORM_LIST_ITEM_PUBLIC_LINK_COPY"] = "Copiar al portapapeles";
$MESS["CRM_WEBFORM_LIST_ITEM_START_EDIT_BUT_STOPPED"] = "No finalizado";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_ON"] = "on";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_OFF"] = "off";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_ON_STATUS"] = "El formulario está activo";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_OFF_STATUS"] = "El formulario está inactivo";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_ON_NOW"] = "activado";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_OFF_NOW"] = "desactivado en este momento";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_ACTIVATED"] = "activado";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_DEACTIVATED"] = "desactivado";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_ACT_ON"] = "on";
$MESS["CRM_WEBFORM_LIST_ERROR_ACTION"] = "La acción ha sido cancelada debido a un error.";
$MESS["CRM_WEBFORM_LIST_DELETE_CONFIRM"] = "¿Desea eliminar este formulario?";
$MESS["CRM_WEBFORM_LIST_CLOSE"] = "Cerrar";
$MESS["CRM_WEBFORM_LIST_APPLY"] = "Ejecutar";
$MESS["CRM_WEBFORM_LIST_CANCEL"] = "Cancelar";
$MESS["CRM_WEBFORM_LIST_POPUP_LIMITED_TITLE"] = "Formularios extendidos del CRM";
$MESS["CRM_WEBFORM_LIST_POPUP_LIMITED_TEXT"] = "Su plan actual limita el número de formularios del CRM. Con el fin de añadir nuevos formularios, por favor, actualice su plan.
<br><br>
TIP: Bitrix24 Professional viene con formularios ilimitados del CRM.
<br><br>
Pruebe Bitrix24 Professional gratis durante 30 días.";
$MESS["CRM_WEBFORM_LIST_ADD_DESC1"] = "Haga clic aquí para crear un formulario";
$MESS["CRM_WEBFORM_LIST_INFO_TITLE"] = "Utilice formularios del CRM para aumentar la productividad.";
$MESS["CRM_WEBFORM_LIST_HIDE_DESC"] = "Ocultar descripción";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_BTN_ON"] = "habilitar";
$MESS["CRM_WEBFORM_LIST_ITEM_ACTIVE_BTN_OFF"] = "deshabilitar";
$MESS["CRM_WEBFORM_LIST_ITEM_BTN_GET_SCRIPT"] = "Código de inserción";
$MESS["CRM_WEBFORM_LIST_PLUGIN_TITLE"] = "Plugins de CMS";
$MESS["CRM_WEBFORM_LIST_PLUGIN_BTN_MORE"] = "más";
$MESS["CRM_WEBFORM_LIST_PLUGIN_DESC"] = "Instala el widget en tu sitio web y comuniquese con sus clientes dentro de su Bitrix24. Obtendrá una caja de herramientas de comunicación lista para usar: Live Chat, callbacks y formularios del CRM. Instalar el widget es extremadamente simple: simplemente selecciona la plataforma de su sitio y le diremos qué debe hacer a continuación.";
$MESS["CRM_WEBFORM_LIST_FILTER_SHOW"] = "Mostrar formulario";
$MESS["CRM_WEBFORM_LIST_NOTIFY_USER_CONSENT_TITLE"] = "-";
$MESS["CRM_WEBFORM_LIST_NOTIFY_USER_CONSENT_TEXT"] = "-";
$MESS["CRM_WEBFORM_LIST_NOTIFY_USER_CONSENT_ITEMS_1"] = "-";
$MESS["CRM_WEBFORM_LIST_NOTIFY_USER_CONSENT_ITEMS_2"] = "-";
$MESS["CRM_WEBFORM_LIST_NOTIFY_USER_CONSENT_ITEMS_3"] = "-";
$MESS["CRM_WEBFORM_LIST_NOTIFY_USER_CONSENT_BTN_OK"] = "-";
$MESS["CRM_WEBFORM_LIST_NOTIFY_USER_CONSENT_BTN_DETAIL"] = "-";
$MESS["CRM_WEBFORM_LIST_SUCCESS_ACTION"] = "La acción se completó exitosamente.";
$MESS["CRM_WEBFORM_LIST_ADS_FORM_STATUS_ON_FACEBOOK"] = "Vinculado a Facebook";
$MESS["CRM_WEBFORM_LIST_ADS_FORM_DESC_HINT"] = "El formulario está vinculado a Facebook.<br>Los formularios de anuncios de Facebook ahora se envían al CRM.<br>No puede editar el formulario mientras esté vinculado a Facebook.";
$MESS["CRM_WEBFORM_LIST_SUCCESS_ACTION_CACHE_CLEANED"] = "Consentimiento aplicado; formulario actualizado.";
?>