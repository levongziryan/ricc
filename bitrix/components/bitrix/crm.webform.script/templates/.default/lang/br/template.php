<?
$MESS["CRM_WEBFORM_SCRIPT_BTN_COPY"] = "copiar";
$MESS["CRM_WEBFORM_SCRIPT_BTN_EDIT"] = "configurar";
$MESS["CRM_WEBFORM_SCRIPT_TAB_ONPAGE"] = "Na página";
$MESS["CRM_WEBFORM_SCRIPT_TAB_WINDOW"] = "Janela própria";
$MESS["CRM_WEBFORM_SCRIPT_LINK"] = "Link";
$MESS["CRM_WEBFORM_SCRIPT_INLINE"] = "Na página";
$MESS["CRM_WEBFORM_SCRIPT_WINDOW_BUTTON"] = "Janela pop-up - clique no botão";
$MESS["CRM_WEBFORM_SCRIPT_WINDOW_LINK"] = "Janela pop-up - link";
$MESS["CRM_WEBFORM_SCRIPT_WINDOW_DELAY"] = "Janela pop-up - mostrar depois de atraso";
$MESS["CRM_WEBFORM_SCRIPT_SEND_TO_EMAIL"] = "enviar código por e-mail";
$MESS["CRM_WEBFORM_SCRIPT_WINDOW_INLINE"] = "O formulário será inserido na sua página";
$MESS["CRM_WEBFORM_SCRIPT_TAB_SCRIPT_INLINE"] = "Inserido";
$MESS["CRM_WEBFORM_SCRIPT_TAB_SCRIPT_BUTTON"] = "Botão";
$MESS["CRM_WEBFORM_SCRIPT_TAB_SCRIPT_LINK"] = "Link";
$MESS["CRM_WEBFORM_SCRIPT_TAB_SCRIPT_DELAY"] = "Tempo de atraso";
$MESS["CRM_WEBFORM_SCRIPT_COPY_ONE_LINE"] = "uma linha";
$MESS["CRM_WEBFORM_SCRIPT_SCRIPT_ON_SITE"] = "Código de inserção";
?>