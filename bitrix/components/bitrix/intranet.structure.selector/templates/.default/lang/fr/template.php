<?
$MESS["INTR_ISS_TPL_EMAIL"] = "Courrier électronique";
$MESS["INTR_ISS_TPL_POST"] = "Emplacement";
$MESS["INTR_ISS_TPL_SUBMIT"] = "Recherche";
$MESS["INTR_ISS_TPL_KEYWORDS"] = "Mots-clés";
$MESS["INTR_ISS_TPL_CANCEL"] = "Annuler";
$MESS["INTR_ISS_TPL_DEPARTMENT"] = "Département";
$MESS["INTR_ISS_TPL_DEPARTMENT_MINE"] = "Uniquement mon office";
$MESS["INTR_ISS_TPL_FIO"] = "Dénomination";
?>