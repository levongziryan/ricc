<?
$MESS["INTR_ISS_TPL_DEPARTMENT_MINE"] = "Sólo mi departamento";
$MESS["INTR_ISS_TPL_DEPARTMENT"] = "Departamento";
$MESS["INTR_ISS_TPL_POST"] = "Cargo";
$MESS["INTR_ISS_TPL_FIO"] = "Nombre";
$MESS["INTR_ISS_TPL_EMAIL"] = "E-mail";
$MESS["INTR_ISS_TPL_KEYWORDS"] = "Palabras clave";
$MESS["INTR_ISS_TPL_SUBMIT"] = "Buscar";
$MESS["INTR_ISS_TPL_CANCEL"] = "Cancelar";
?>