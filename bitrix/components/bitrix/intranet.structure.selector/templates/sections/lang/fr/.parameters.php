<?
$MESS["INTR_ISS_TPL_PARAM_SHOW_SECTION_INFO"] = "Voir l'information sur le département.";
$MESS["INTR_ISS_TPL_PARAM_COLUMNS"] = "Nombre de colonnes";
$MESS["INTR_ISS_TPL_PARAM_COLUMNS_FIRST"] = "Nombre de colonnes sur la première page";
$MESS["INTR_ISS_TPL_PARAM_MAX_DEPTH"] = "La profondeur maximum de l'arbre (0 - tous)";
$MESS["INTR_ISS_TPL_PARAM_MAX_DEPTH_FIRST"] = "La profondeur maximale de l'arbre sur la première page (0 - tous)";
?>