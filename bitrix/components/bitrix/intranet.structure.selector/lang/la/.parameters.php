<?
$MESS["INTR_ISS_GROUP_FILTER"] = "Parámetros del Filtro";
$MESS["INTR_ISS_PARAM_LIST_URL"] = "Página de la lista del empleado";
$MESS["INTR_ISS_PARAM_FILTER_NAME"] = "Filtrar Nombre";
$MESS["INTR_ISS_PARAM_FILTER_DEPARTMENT_SINGLE"] = "Filtrar en Departamentos";
$MESS["INTR_ISS_PARAM_FILTER_DEPARTMENT_SINGLE_VALUE_Y"] = "individual";
$MESS["INTR_ISS_PARAM_FILTER_DEPARTMENT_SINGLE_VALUE_N"] = "múltiple";
$MESS["INTR_ISS_PARAM_FILTER_SESSION"] = "Almacenar Filtro en la sesión";
?>