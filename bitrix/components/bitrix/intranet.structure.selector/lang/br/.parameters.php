<?
$MESS["INTR_ISS_PARAM_FILTER_DEPARTMENT_SINGLE"] = "Departmentos no Filtro";
$MESS["INTR_ISS_PARAM_LIST_URL"] = "Página da Lista de Funcionários";
$MESS["INTR_ISS_PARAM_FILTER_NAME"] = "Nome do Filtro";
$MESS["INTR_ISS_GROUP_FILTER"] = "Parâmetros do Filtro";
$MESS["INTR_ISS_PARAM_FILTER_DEPARTMENT_SINGLE_VALUE_N"] = "múltiplo";
$MESS["INTR_ISS_PARAM_FILTER_DEPARTMENT_SINGLE_VALUE_Y"] = "simples";
$MESS["INTR_ISS_PARAM_FILTER_SESSION"] = "Armazenar Filtro na Sessão";
?>