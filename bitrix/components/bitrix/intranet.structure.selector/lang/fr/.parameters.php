<?
$MESS["INTR_ISS_PARAM_FILTER_DEPARTMENT_SINGLE"] = "Choix de subdivision pour le Filtrage";
$MESS["INTR_ISS_PARAM_FILTER_SESSION"] = "Mémoriser le filtre dans la session";
$MESS["INTR_ISS_PARAM_FILTER_NAME"] = "Dénomination du filtre";
$MESS["INTR_ISS_PARAM_FILTER_DEPARTMENT_SINGLE_VALUE_N"] = "multiple";
$MESS["INTR_ISS_PARAM_FILTER_DEPARTMENT_SINGLE_VALUE_Y"] = "unique";
$MESS["INTR_ISS_GROUP_FILTER"] = "Paramètres du filtre";
$MESS["INTR_ISS_PARAM_LIST_URL"] = "Page de la liste d'utilisateurs";
?>