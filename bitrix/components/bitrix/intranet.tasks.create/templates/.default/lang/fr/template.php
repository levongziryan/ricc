<?
$MESS["INTET_NOT_SET"] = "(non installé)";
$MESS["INTET_VE_INS_SUBTASK1"] = "Insertion de la sous-tâche";
$MESS["INTET_SEND_COMMAND"] = "Effectuer une action";
$MESS["INTET_ADD"] = "Ajouter";
$MESS["INTET_VE_INS_SUBTASK"] = "Ajouter une subtâche";
$MESS["INTET_FINISH_BUTTON"] = "Achever";
$MESS["INTET_EDIT_TITLE"] = "Modification de la Tâche ##ID#";
$MESS["INTET_VE_INS_SUBTASK2"] = "Dénomination de la sous-tâche";
$MESS["INTET_RESPONSIBLE"] = "Responsable";
$MESS["INTET_REJECT"] = "Rejeter";
$MESS["INTET_CANCEL"] = "Annuler";
$MESS["INTET_APPLY"] = "Appliquer";
$MESS["INTET_CONFIRM"] = "Accepter";
$MESS["INTET_VIEW_TITLE"] = "Visualisation de tâche ##ID#";
$MESS["INTET_CREATE_TITLE"] = "Création d'une nouvelle tâche";
$MESS["INTET_SAVE"] = "Sauvegarder";
$MESS["INTET_CURRENT_STATUS"] = "Statut courant";
$MESS["INTET_U_DEL"] = "Supprimer";
?>