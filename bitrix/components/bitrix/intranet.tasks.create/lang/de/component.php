<?
$MESS["INTE_ADD_TASK_MESSAGE"] = "Es wurde eine neue Aufgabe erstellt \"#NAME#\"

[url=#URL_VIEW#]Details anzeigen[/url]
[url=#URL_EDIT#]Aufgabe ändern[/url]";
$MESS["INTE_ADD_TASK_MESSAGE1"] = "Es wurde eine neue Aufgabe erstellt \"#NAME#\"
Priorität: #TASK_PRIORITY#
Abgabetermin: #TASK_DATES#

[url=#URL_VIEW#]Details anzeigen[/url]
[url=#URL_EDIT#]Aufgabe ändern[/url]";
$MESS["INTE_EDIT_TITLE"] = "Aufgabe Nr. #ID# bearbeiten";
$MESS["INTE_ERROR_SAVE_TASK"] = "Beim Speichern der Aufgabe ist ein Fehler aufgetreten";
$MESS["INTASK_FROM_DATE_TLP"] = "vom #DATE#";
$MESS["INTE_CREATE_TITLE"] = "Neue Aufgabe erstellen";
$MESS["INTE_NO"] = "Nein";
$MESS["INTE_ADD_TASK_DATES_EMPTY"] = "nein";
$MESS["INTAST_T6654_LOG"] = "Priorität: #PRIORITY#
Abgabetermin: #TIME#
Ausführend: #RESP#";
$MESS["INTE_INTERNAL_ERROR"] = "Systemfehler";
$MESS["INTE_VIEW_TITLE"] = "Aufgabe Nr. #ID#";
$MESS["INTE_TASK_NOT_FOUND"] = "Die Aufgabe wurde nicht gefunden";
$MESS["INTE_EDIT_TASK_MESSAGE"] = "Einstellungen für die Aufgabe \"#NAME#\" wurden geändert

[url=#URL_VIEW#]Details anzeigen[/url]";
$MESS["INTE_NO_ASSIGNEDTO_PROP"] = "Die Eigenschaft \"Ausführend\" des Informationsblocks wurde nicht gefunden";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "Das Modul \"Informationsblöcke\" wurde nicht installiert.";
$MESS["EC_SONET_MODULE_NOT_INSTALLED"] = "Das Modul \"Soziales Netz\" wurde nicht installiert";
$MESS["INTE_WRONG_STATUS_VAL"] = "Werte der Eigenschaft 'Status' wurden gerändert";
$MESS["INTE_NO_STATUS_PROP"] = "Die Eigenschaft \"Status\" des Informationsblocks wurde nicht gefunden";
$MESS["INTE_APPLY_MESSAGE"] = "Die Aufgabe wurde angenommen \"#NAME#\"

[url=#URL_VIEW#]Details anzeigen[/url]";
$MESS["INTE_FINISH_MESSAGE"] = "Die Aufgabe \"#NAME#\" wurde abgeschlossen

[url=#URL_VIEW#]Details anzeigen[/url]";
$MESS["INTE_REJECT_MESSAGE"] = "Die Aufgabe wurde nicht angenommen \"#NAME#\"

[url=#URL_VIEW#]Details anzeigen[/url]";
$MESS["INTE_EDIT_TASK_MESSAGE1"] = "Einstellungen für die Aufgabe \"#NAME#\" wurden geändert.
Priorität: #TASK_PRIORITY#
Abgabetermin: #TASK_DATES#

[url=#URL_VIEW#]Details anzeigen[/url]";
$MESS["INTE_NO_ASSIGNEDTO_USER"] = "Der Nutzer für die Aufgabe wurde nicht angegeben";
$MESS["INTE_TASKS_OFF"] = "Die Funktion \"Aufgaben\" ist deaktiviert";
$MESS["INTASK_TO_DATE_TLP"] = "bis zum #DATE#";
$MESS["INTE_YES"] = "Ja";
$MESS["INTE_NO_APPLY_REJECT_PERMS"] = "Sie dürfen diese Aufgabe weder annehmen, noch ablehnen";
$MESS["INTE_WRONG_ASSIGNEDTO_USER"] = "Sie dürfen diesem Nutzer keine Aufgabe zuteilen";
$MESS["INTE_NO_REJECT_PERMS"] = "Sie dürfen diese Aufgabe nicht ablehnen";
$MESS["INTE_NO_CREATE_PERMS"] = "Sie haben nicht genügend Rechte, um eine Aufgabe zu erstellen";
$MESS["INTE_NO_SONET_PERMS"] = "Sie haben nicht genügend Rechte, Aufgaben anzusehen";
$MESS["INTE_NO_IBLOCK_PERMS"] = "Sie haben nicht genügend Rechte, Informationsblock für die Aufgaben anzuzeigen";
$MESS["INTE_NO_VIEW_PERMS"] = "Sie haben nicht genügend Rechte, diese Aufgabe anzusehen";
$MESS["INTE_TASKS_RESPONSIBLE"] = "Ausführend";
$MESS["INTE_TASKS_EMPTY_FIELD"] = "Das Feld \"#FIELD#\" ist leer.";
$MESS["INTE_TASKS_PERMS_EVENT"] = "Sie haben nicht genügend Rechte, diese Aktion durchzuführen.";
$MESS["INTE_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
?>