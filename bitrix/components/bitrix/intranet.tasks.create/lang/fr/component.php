<?
$MESS["INTE_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["INTAST_T6654_LOG"] = "Priorité: #PRIORITY#
Temps: #TIME#
Responsable: #RESP#";
$MESS["INTE_WRONG_ASSIGNEDTO_USER"] = "Vous ne pouvez pas poser une tâche à cet utilisateur.";
$MESS["INTE_NO_REJECT_PERMS"] = "Vous ne pouvez pas décliner cette tâche.";
$MESS["INTE_NO_APPLY_REJECT_PERMS"] = "Vous ne pouvez pas accepter ou refuser la tâche.";
$MESS["INTE_YES"] = "Oui";
$MESS["INTASK_TO_DATE_TLP"] = "jusqu'à #DATE#";
$MESS["INTE_ADD_TASK_MESSAGE"] = "Une tâche nouvelle '#NAME#' a été créé.\\r\\n\\r\\n[url=#URL_VIEW#]Voir la fiche de tâche[/url]\\r\\n[url=#URL_EDIT#]Modifier tâche[/url]";
$MESS["INTE_ADD_TASK_MESSAGE1"] = "Ajout d'une nouvelle tâche '#NAME#'
Importance: #TASK_PRIORITY#
Délai: #TASK_DATES#

[url=#URL_VIEW#]Voir les détails[/url]
[url=#URL_EDIT#]Modifier la tâche[/url]";
$MESS["INTE_FINISH_MESSAGE"] = "L'exécution de la tâche '#NAME#' est terminée. [url=#URL_VIEW#]Voir les détails[/url]";
$MESS["INTE_VIEW_TITLE"] = "Tâche  ##ID#";
$MESS["INTE_TASK_NOT_FOUND"] = "Tâche introuvable.";
$MESS["INTE_EDIT_TITLE"] = "Modification de la Tâche ##ID#";
$MESS["INTE_EDIT_TASK_MESSAGE"] = "Les paramètres de tâche '#NAME#'



[url=#URL_VIEW#]Voir les détails [/url] sont changés";
$MESS["INTE_EDIT_TASK_MESSAGE1"] = "Les paramètres de la tâche '#NAME#' sont modifiés.
Importance: #TASK_PRIORITY#
Durée: #TASK_DATES#

[url=#URL_VIEW#]Voir les détails[/url]";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "Le module des blocs d'informations n'a pas été installé.";
$MESS["EC_SONET_MODULE_NOT_INSTALLED"] = "Le module du réseau social n'a pas été installé.";
$MESS["INTE_TASKS_EMPTY_FIELD"] = "Champ '#FIELD#' non renseigné.";
$MESS["INTE_NO_ASSIGNEDTO_PROP"] = "Propriété 'Responsable' du bloc d'information introuvable.";
$MESS["INTE_NO_STATUS_PROP"] = "Propriété 'État' du bloc d'information introuvable.";
$MESS["INTE_REJECT_MESSAGE"] = "La tâche '#NAME#' n'est pas acceptée pour exécution.



[url=#URL_VIEW#]Voir les détails[/url]";
$MESS["INTE_NO_ASSIGNEDTO_USER"] = "Utilisateur à qui est assignée la tâche pas indiqué.";
$MESS["INTE_NO"] = "Non";
$MESS["INTE_ADD_TASK_DATES_EMPTY"] = "pas encore défini";
$MESS["INTE_TASKS_RESPONSIBLE"] = "Responsable";
$MESS["INTE_ERROR_SAVE_TASK"] = "Erreur d'enregistrement de la tâche.";
$MESS["INTE_APPLY_MESSAGE"] = "Reçu pour l'exécution la tâche '#NAME#'

[url=#URL_VIEW#]Voir les détails[/url]";
$MESS["INTASK_FROM_DATE_TLP"] = "dès #DATE#";
$MESS["INTE_INTERNAL_ERROR"] = "Erreur système.";
$MESS["INTE_CREATE_TITLE"] = "Création d'une nouvelle tâche";
$MESS["INTE_WRONG_STATUS_VAL"] = "La liste des valeurs de la qualité 'État' du bloc d'information est modifiée.";
$MESS["INTE_TASKS_PERMS_EVENT"] = "Vous n'avez pas l'autorisation d'effectuer cette action.";
$MESS["INTE_NO_SONET_PERMS"] = "Vous n'avez pas de droits pour accéder aux tâches.";
$MESS["INTE_NO_IBLOCK_PERMS"] = "Vous n'avez pas de droits d'accès à l'affichage du bloc d'information des tâches.";
$MESS["INTE_NO_VIEW_PERMS"] = "Vous n'avez pas de droits pour voir cette tâche.";
$MESS["INTE_NO_CREATE_PERMS"] = "Vous n'avez pas de droits pour la création des tâches.";
$MESS["INTE_TASKS_OFF"] = "La fonctionnalité des tâches est activée.";
?>