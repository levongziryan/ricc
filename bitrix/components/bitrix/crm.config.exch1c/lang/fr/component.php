<?
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_EXCH1C"] = "Intégration au programme '1C: Entreprise'";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "Le module catalog n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Module CRM non installé";
$MESS["CRM_MODULE_NOT_INSTALLED_IBLOCK"] = "Module iblock non installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Module SALE non installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "Le module Devise n'est pas installé.";
?>