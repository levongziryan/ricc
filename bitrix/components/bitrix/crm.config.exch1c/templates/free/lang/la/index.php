<?
$MESS["CRM_CONFIGS_LINK_TEXT"] = "Ver la configuración del CRM";
$MESS["CRM_CONFIGS_LINK_TITLE"] = "Abrir el formulario de configuración del CRM";
$MESS["CRM_EXCH1C_CONNECT_SECTION_TITLE"] = "1C: Conexión Enterprise";
$MESS["CRM_EXCH1C_COMMENT_FREE_P1"] = "Bitrix24 CRM implementa el intercambio de datos dúplex 1C automatizado";
$MESS["CRM_EXCH1C_COMMENT_FREE_P2"] = "Usted obtendrá en su CRM un catálogo de productos actualizado, listas de precios y stocks de almacén. Todos los datos se sincronizarán automáticamente con 1C. Mas información";
$MESS["CRM_EXCH1C_COMMENT_FREE_P3"] = "La integración requiere 1C: ediciones Enterprise Salespoint o Small Business";
$MESS["CRM_EXCH1C_COMMENT_FREE_P4"] = "Para habilitar la integración 1C, actualice a cualquier plan Bitrix24 comenzando con el Team.";
$MESS["CRM_EXCH1C_BITRIX24_LINK1"] = "aquí";
$MESS["CRM_EXCH1C_BITRIX24_LINK2"] = "Seleccionar plan";
?>