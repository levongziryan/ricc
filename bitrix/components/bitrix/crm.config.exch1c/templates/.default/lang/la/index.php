<?
$MESS["CRM_CONFIGS_LINK_TEXT"] = "Ver la configuración del CRM";
$MESS["CRM_CONFIGS_LINK_TITLE"] = "Abrir el formulario de configuración del CRM";
$MESS["CRM_EXCH1C_CONNECT_SECTION_TITLE"] = "1C: Conexión Enterprise";
$MESS["CRM_EXCH1C_SYNCSERV_SECTION_TITLE"] = "Servicios de sincronización";
$MESS["CRM_EXCH1C_CONNECT_COMMENT_P1"] = "1C: La integración empresarial está disponible en 1C: Salespoint, 1C: Small Business, 1C: Automation etc.";
$MESS["CRM_EXCH1C_INV_SETTINGS_LINK_TITLE"] = "Configurar la sincronización de facturas 1C";
$MESS["CRM_EXCH1C_CAT_SETTINGS_LINK_TITLE"] = "Configurar la sincronización del catálogo de productos 1C";
$MESS["CRM_EXCH1C_ENABLED_TITLE"] = "Activar 1C: intercambio de datos empresariales";
$MESS["CRM_EXCH1C_CONNECT_COMMENT_P3"] = "Obtenga más información sobre 1C: Enterprise";
$MESS["CRM_EXCH1C_CONNECT_COMMENT_P3_1"] = "en nuestro blog.";
$MESS["CRM_EXCH1C_CONNECT_COMMENT_P2"] = "En el cuadro de diálogo de perfil de intercambio 1C: Enterprise, especifique # URL # como dirección del sitio y el nombre de un usuario con permiso de configuración de CRM como inicio de sesión.";
?>