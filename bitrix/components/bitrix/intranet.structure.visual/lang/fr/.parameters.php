<?
$MESS["INTR_ISV_PARAM_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["INTR_ISV_PARAM_USE_USER_LINK"] = "Afficher les cartes d'information escamotées des utilisateurs";
$MESS["INTR_ISV_PARAM_NAME_TEMPLATE"] = "Affichage du nom";
$MESS["INTR_ISV_PARAM_SHOW_LOGIN"] = "Afficher le nom d'utilisateur si le nom n'est pas spécifié";
$MESS["INTR_ISV_PARAM_PATH_TO_VIDEO_CALL"] = "Page de l'appel vidéo";
$MESS["INTR_ISV_PARAM_PM_URL"] = "Page de l'envoi du message privé";
$MESS["INTR_ISV_PARAM_PROFILE_URL"] = "Page du profil de l'utilisateur";
$MESS["INTR_ISV_PARAM_DETAIL_URL"] = "Page de la structure de l'entreprise";
?>