<?
$MESS["ISV_set_department_head"] = "<b>#NAME#</b> ha sido nombrado como jefe de <b>#DEPARTMENT#</b> departamento.";
$MESS["ISV_change_department"] = "<b>#NAME#</b> se ha transferido a <b>#DEPARTMENT#</b>.";
$MESS["ISV_move_department"] = "El <b>#DEPARTMENT#</b> departamento está subordinado a <b>#DEPARTMENT_TO#</b>.";
$MESS["ISV_ERROR_dpt_not_empty"] = "Usted tiene que trasladar a todos los empleados de departamentos antes de eliminar el departamento.";
?>