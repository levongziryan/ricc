<?
$MESS["ISV_set_department_head"] = "<b>#NAME#</b> foi nomeado chefe do departamento <b>#DEPARTMENT#</b>.";
$MESS["ISV_change_department"] = "<b>#NAME#</b> foi transferido para <b>#DEPARTMENT#</b>.";
$MESS["ISV_move_department"] = "O departamento <b>#DEPARTMENT#</b> está subordinado a <b>#DEPARTMENT_TO#</b>.";
$MESS["ISV_ERROR_dpt_not_empty"] = "Você deve realocar todos os colaboradores do departamento antes de excluí-lo. ";
?>