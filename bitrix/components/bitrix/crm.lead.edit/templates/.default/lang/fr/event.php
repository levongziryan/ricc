<?
$MESS["CRM_LEAD_EDIT_EVENT_SUCCESSFULLY_CREATED"] = "La piste <a href='#URL#'>#TITLE#</a> a été créée. Vous allez être redirigé vers la page initiale. Si cette page est actuellement encore affichée, veuillez la fermer manuellemet.";
$MESS["CRM_LEAD_EDIT_EVENT_CANCELED"] = "Une action a été annulée. Vous êtes maintenant redirigé vers la page précédente. Si cette page est toujours affichée, veuillez la fermer manuellement.";
?>