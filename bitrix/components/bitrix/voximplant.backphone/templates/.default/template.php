<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetAdditionalCSS("/bitrix/components/bitrix/voximplant.main/templates/.default/telephony.css");

CJSCore::RegisterExt('voximplant_backphone', array(
	'js' => '/bitrix/components/bitrix/voximplant.backphone/templates/.default/template.js',
	'lang' => '/bitrix/components/bitrix/voximplant.backphone/templates/.default/lang/'.LANGUAGE_ID.'/template.php',
));
CJSCore::Init(array('voximplant_backphone'));
?>

<div class="tel-balance-ifo-wrap" id="backphone-placeholder"></div>
<script type="text/javascript">
	BX.VoxImplant.backPhone.init({
		'placeholder': BX('backphone-placeholder'),
		'number': "<?=$arResult['PHONE_NUMBER']?>",
		'verified': "<?=$arResult['VERIFIED']?>",
		'verifiedUntil': "<?=$arResult['VERIFIED_UNTIL']?>"
	})
</script>