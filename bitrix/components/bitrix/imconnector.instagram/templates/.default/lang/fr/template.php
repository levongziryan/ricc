<?
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_DEL_REFERENCE"] = "Déconnecter";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_STEP_1_OF_2_TITLE"] = "Se connecter";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_STEP_2_OF_2_TITLE"] = "Terminer";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_STEP_N_OF_2"] = "étape #STEP# de 2";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_INDEX_DESCRIPTION_NEW"] = "Connectez Instagram au Canal ouvert pour recevoir les commentaires laissés par vos clients dans le chat Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_USER"] = "Compte";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_ENTITY"] = "Connectez-vous à l'aide de votre compte Instagram pour recevoir les commentaires de vos clients";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_CONNECT"] = "Connecter";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_FINAL_DESCRIPTION_1"] = "Veuillez noter que le système récupère les commentaires Instagram toutes les 15 minutes et non de manière instantanée.<br>Vous trouverez plus de détails sur l'échange des données Instagram <a href=\"#URL#\" target=\"_blank\">dans cet article</a>.";
?>