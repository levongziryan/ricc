<?
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_DEL_REFERENCE"] = "Verknüpfung löschen";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_STEP_1_OF_2_TITLE"] = "Einloggen";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_STEP_2_OF_2_TITLE"] = "Abschließen";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_STEP_N_OF_2"] = "Schritt #STEP# von 2";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_INDEX_DESCRIPTION_NEW"] = "Verbinden Sie Instagram mit einem Kommunikationskanal, um Kommentare Ihrer Kunden im Bitrix24 Chat zu erhalten.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_USER"] = "Account";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_ENTITY"] = "Loggen Sie sich mit Ihren Instagram-Anmeldedaten ein, um Kommentare Ihrer Kunden zu erhalten";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_CONNECT"] = "Verbinden";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_FINAL_DESCRIPTION_1_URL"] = "https://helpdesk.bitrix24.de/open/4781483/#limitations";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_FINAL_DESCRIPTION_1"] = "Beachten Sie bitte, dass die Kommentare aus Instagram nicht ununterbrochen, sondern alle 15 Minuten geholt werden.<br>
Weitere Informationen über den Datenaustausch mit Instagram finden Sie <a href=\"#URL#\" target=\"_blank\">in diesem Artikel</a>.
";
?>