<?
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_MODULE_NOT_INSTALLED"] = "O módulo \"Conectores IM Externos\" não está instalado.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_NO_ACTIVE_CONNECTOR"] = "Este conector está inativo.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_SESSION_HAS_EXPIRED"] = "Sua sessão expirou. Envie o formulário novamente.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_REMOVED_REFERENCE_TO_ENTITY"] = "Este conector foi configurado para uso com grupo, página pública ou evento que você atualmente não tem acesso administrativo.<br>
Configure o conector novamente.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_OK_DEL_USER"] = "Sua conta de usuário foi desvinculada";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_NO_DEL_USER"] = "Não é possível desvincular sua conta de usuário";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_OK_DEL_ENTITY"] = "O grupo, página pública ou evento foram desvinculados.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_NO_DEL_ENTITY"] = "Não é possível desvincular grupo, página pública ou evento.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_CONNECTOR_ERROR_STATUS"] = "Houve um erro. Verifique suas preferências.";
$MESS["IMCONNECTOR_COMPONENT_INSTAGRAM_ERROR_REQUEST_INFORMATION_FROM_SERVER"] = "Erro de servidor.";
?>