<?
$MESS["SONET_GRE_T_SUBTITLE_IN"] = "Demande de se joindre au groupe";
$MESS["SONET_GRE_T_NO_REQUESTS_DESCR"] = "Ici sont affichées les demandes d'utilisateurs de devenir vos amis et les invitations d'adhérer au groupe.";
$MESS["SONET_GRE_T_NO_REQUESTS_OUT_DESCR"] = "Ici sont affichées des invitations aux utilisateurs de joindre le groupe.";
$MESS["SONET_GRE_T_NO_REQUESTS"] = "Il n'y a pas d'invitations.";
$MESS["SONET_GRE_T_NO_REQUESTS_OUT"] = "Il n'y a pas d'invitations.";
$MESS["SONET_GRE_T_DO_SAVE"] = "Approuver l'affiliation";
$MESS["SONET_GRE_T_REJECT"] = "Refuser";
$MESS["SONET_GRE_T_REJECT_OUT"] = "Annuler les invitations";
$MESS["SONET_GRE_T_CHECK_ALL"] = "Cocher/décocher tous";
$MESS["SONET_GRE_T_SENDER"] = "Expéditeur";
$MESS["SONET_GRE_T_RECIPIENT"] = "Adresse du destinataire";
$MESS["SONET_GRE_T_INVITE"] = "Inviter au groupe";
$MESS["SONET_GRE_T_SUBTITLE_OUT"] = "Invitations de se joindre au groupe";
$MESS["SONET_GRE_T_MESSAGE_IN"] = "messages";
$MESS["SONET_GRE_T_MESSAGE_OUT"] = "messages";
?>