<?
$MESS["SONET_GRE_T_DO_SAVE"] = "Aprovar participação";
$MESS["SONET_GRE_T_REJECT"] = "Recusar";
$MESS["SONET_GRE_T_REJECT_OUT"] = "Cancelar convites";
$MESS["SONET_GRE_T_CHECK_ALL"] = "Marcar todos / Desmarcar todos";
$MESS["SONET_GRE_T_SUBTITLE_IN"] = "Pedidos de participação do grupo de trabalho";
$MESS["SONET_GRE_T_SUBTITLE_OUT"] = "Convites de participação do grupo de trabalho";
$MESS["SONET_GRE_T_SENDER"] = "Remetente";
$MESS["SONET_GRE_T_RECIPIENT"] = "Destinatário";
$MESS["SONET_GRE_T_MESSAGE_IN"] = "Mensagens de usuários";
$MESS["SONET_GRE_T_MESSAGE_OUT"] = "Mensagens para usuários";
$MESS["SONET_GRE_T_NO_REQUESTS"] = "Sem pedidos.";
$MESS["SONET_GRE_T_NO_REQUESTS_DESCR"] = "Mostrar pedidos de usuários de participação no grupo";
$MESS["SONET_GRE_T_NO_REQUESTS_OUT"] = "Sem convites";
$MESS["SONET_GRE_T_NO_REQUESTS_OUT_DESCR"] = "Mostrar convites de participação do grupo de trabalho";
$MESS["SONET_GRE_T_INVITE"] = "Convidar para o grupo";
?>