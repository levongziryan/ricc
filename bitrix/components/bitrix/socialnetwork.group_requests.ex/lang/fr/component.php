<?
$MESS["SONET_GRE_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["SONET_GRE_NO_GROUP"] = "Groupe introuvable.";
$MESS["SONET_GRE_TITLE"] = "Demandes et invitations de se joindre au groupe";
$MESS["SONET_MODULE_NOT_INSTALL"] = "Le module du réseau social n'a pas été installé.";
$MESS["SONET_GRE_NOT_SELECTED"] = "Aucun utilisateur n'a été séléctionné.";
$MESS["SONET_GRE_CANT_DELETE_INVITATION"] = "Chec de la suppression d'invitation à l'utilisateur ID=#RELATION_ID#";
$MESS["SONET_GRE_NAV"] = "Utilisateurs";
$MESS["SONET_P_USER_NO_USER"] = "L'utilisateur n'est pas trouvé.";
$MESS["SONET_GRE_CANT_INVITE"] = "Vous n'êtes pas autorisé pour approuver les demandes des utilisateurs de se joindre à ce groupe.";
$MESS["SONET_GRE_NO_PERMS"] = "Vous n'avez pas de droits pour accéder à ce groupe.";
?>