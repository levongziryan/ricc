<?
$MESS["IMOL_CONFIG_EDIT_NAME"] = "Nombre de canal abierto";
$MESS["IMOL_CONFIG_EDIT_CONNECTED_SOURCE"] = "Canales de comunicación conectados";
$MESS["IMOL_CONFIG_EDIT_FLOW"] = "Enrutamiento de envío";
$MESS["IMOL_CONFIG_EDIT_CRM"] = "Verificar en la base de datos del CRM el cliente";
$MESS["IMOL_CONFIG_EDIT_CRM_DISABLED"] = "Por favor, activa el modulo de CRM en Bitrix24 para configurar y activar esta opción.";
$MESS["IMOL_CONFIG_EDIT_CRM_FORWARD"] = "Envió de transferencia a la persona responsable si el ID de cliente es reconocido";
$MESS["IMOL_CONFIG_EDIT_CRM_CREATE"] = "Si no se ha encontrado en el cliente del CRM";
$MESS["IMOL_CONFIG_EDIT_CRM_CREATE_NONE"] = "crear uno manualmente utilizando el formulario CRM";
$MESS["IMOL_CONFIG_EDIT_CRM_CREATE_IN_CHAT"] = "crear manualmente en la ventana de chat";
$MESS["IMOL_CONFIG_EDIT_CRM_CREATE_LEAD"] = "crear un nuevo prospecto";
$MESS["IMOL_CONFIG_EDIT_CRM_CREATE_LEAD_DESC"] = "sólo se creará un nuevo prospecto si está disponible la información de contacto del cliente
";
$MESS["IMOL_CONFIG_EDIT_CRM_SOURCE"] = "Origen del nuevo prospecto";
$MESS["IMOL_CONFIG_EDIT_CRM_SOURCE_TIP"] = "Si el <br>\"<b>Origen de Canal Abierto</b>\" está seleccionado, el prospecto nuevo especificará el origen de comunicación utilizado: Facebook, Skype, Telegrama, etc,.";
$MESS["IMOL_CONFIG_EDIT_CRM_TRANSFER_CHANGE_2"] = "Cambio automático de la presona responsable del prospecto al transferir la conversación a otro operador";
$MESS["IMOL_CONFIG_EDIT_QUEUE"] = "Cola";
$MESS["IMOL_CONFIG_EDIT_QUEUE_DESC"] = "Seleccionar las personas responsables de responder las contribuciones en este canal abierto. Especificar el modo de distribución de presentación y responsable de tiempo de espera de respuesta por persona requeuing automático.";
$MESS["IMOL_CONFIG_EDIT_QUEUE_TYPE"] = "Distribuir comunicaciones entre las personas responsables";
$MESS["IMOL_CONFIG_EDIT_QUEUE_TYPE_EVENLY"] = "igualmente";
$MESS["IMOL_CONFIG_EDIT_QUEUE_TYPE_STRICTLY"] = "Exactamente como en la cola de espera";
$MESS["IMOL_CONFIG_EDIT_QUEUE_TYPE_ALL"] = "a todos";
$MESS["IMOL_CONFIG_EDIT_QUEUE_TYPE_TIP"] = "<b>Igualdad</b> Significa que el orden actual de los empleados involucrados no tiene ninguna consecuencia; las comunicaciones se distribuirán de forma que todos los empleados reciben cargas de trabajo similares.<br><br><b>Poner en cola</b> Implica que el orden actual de los empleados tiene prioridad; una nueva comunicación será enviada a la primera persona disponible y, a continuación, al siguiente en cola si sigue sin respuesta.<br><i>Observe que este modo hace imposible la cola interminable.</i>";
$MESS["IMOL_CONFIG_EDIT_QUEUE_TYPE_TIP_2"] = "<b>A todos</b> - las solicitudes se enviarán a todos los empleados de la cola.";
$MESS["IMOL_CONFIG_EDIT_QUEUE_TYPE_TIP_ASTERISK_2"] = "Observe que este modo permite colas interminables.";
$MESS["IMOL_CONFIG_EDIT_NO_ANSWER_RULE_FORM"] = "Envíe el formulario de CRM";
$MESS["IMOL_CONFIG_EDIT_NO_ANSWER_RULE_FORM_ID"] = "Formulario del CRM";
$MESS["IMOL_CONFIG_EDIT_NO_ANSWER_RULE_TEXT"] = "Enviar texto";
$MESS["IMOL_CONFIG_EDIT_NO_ANSWER_RULE_BOT"] = "Conectar chat bot";
$MESS["IMOL_CONFIG_EDIT_NO_ANSWER_RULE_BOT_ID"] = "Chat bot";
$MESS["IMOL_CONFIG_EDIT_NO_ANSWER_RULE_QUEUE"] = "En cola de espera";
$MESS["IMOL_CONFIG_EDIT_NO_ANSWER_RULE_NONE"] = "No hacer nada";
$MESS["IMOL_CONFIG_EDIT_BOT_JOIN_TIP"] = "Chat bot es el primer nivel de asistencia. Todos los mensajes son procesados por bot primero, antes de que se encuentran en cola para asistencia en vivo.<br><br> Puede descargar nuevos robots desde marketplace Bitrix24.";
$MESS["IMOL_CONFIG_EDIT_BOT_EMPTY"] = "Actualmente no hay ningún canal abierto de chat bots instalado. Puede obtener chat bots en Bitrix Marketplace.";
$MESS["IMOL_CONFIG_EDIT_BOT_JOIN_2"] = "Asignar bot de chat cuando se recibe una solicitud del cliente";
$MESS["IMOL_CONFIG_EDIT_BOT_ID"] = "Seleccione un chat bot";
$MESS["IMOL_CONFIG_EDIT_BOT_TIME_TIP"] = "Periodo de tiempo tras el cual el cliente será transferido de un bot o un agente en vivo. El cliente puede conectar al agente antes enviando el 0.";
$MESS["IMOL_CONFIG_EDIT_BOT_TIME"] = "Despues de la cola del bot a un agente en persona";
$MESS["IMOL_CONFIG_EDIT_QUEUE_TIME"] = "Tiempo de espera de respuesta antes de estar en cola";
$MESS["IMOL_CONFIG_EDIT_NA_TIME"] = "Marcar solicitud como no contestada";
$MESS["IMOL_CONFIG_EDIT_QUEUE_TIME_1"] = "1 minuto";
$MESS["IMOL_CONFIG_EDIT_QUEUE_TIME_3"] = "3 minutos";
$MESS["IMOL_CONFIG_EDIT_QUEUE_TIME_5"] = "5 minutos";
$MESS["IMOL_CONFIG_EDIT_QUEUE_TIME_10"] = "10 minutos";
$MESS["IMOL_CONFIG_EDIT_QUEUE_TIME_15"] = "15 minutos";
$MESS["IMOL_CONFIG_EDIT_QUEUE_TIME_30"] = "30 minutos";
$MESS["IMOL_CONFIG_EDIT_QUEUE_TIME_0"] = "No transferir";
$MESS["IMOL_CONFIG_EDIT_TIMEMAN"] = "No enviar a la cola al empeado si no ha ingresado al trabajo o se encuentra fuera";
$MESS["IMOL_CONFIG_EDIT_LM_ADD1"] = "Agregar empleados";
$MESS["IMOL_CONFIG_EDIT_LM_ADD2"] = "agregar más";
$MESS["IMOL_CONFIG_EDIT_LM_ERROR_BUSINESS"] = "Usted puede seleccionar sólo usuarios empresariales con su tarifa";
$MESS["IMOL_CONFIG_NO_ANSWER"] = "Comunicación de envío sin respuesta";
$MESS["IMOL_CONFIG_NO_ANSWER_DESC"] = "Si ninguno de los empleados puede responder la comunicación, usted puede responder con texto, formulario web o ponerlo en cola de nuevo.";
$MESS["IMOL_CONFIG_NO_ANSWER_RULE"] = "Si los empleados no responde a una comunicación";
$MESS["IMOL_CONFIG_NO_ANSWER_FORM_ID"] = "Formulario del CRM";
$MESS["IMOL_CONFIG_NO_ANSWER_FORM_TEXT"] = "Si el ID de un cliente se reconoce, no se enviará el formulario web y el texto se enviarán de regreso en su lugar";
$MESS["IMOL_CONFIG_NO_ANSWER_TEXT"] = "respuesta de texto automática";
$MESS["IMOL_CONFIG_RECORDING"] = "Guardar mensaje de registro";
$MESS["IMOL_CONFIG_RECORDING_DESC"] = "todas las conversaciones estarán disponibles en las estadísticas del Canal Abierto";
$MESS["IMOL_CONFIG_EDIT_LANG"] = "Preferencias de idioma";
$MESS["IMOL_CONFIG_EDIT_LANG_EMAIL"] = "Idioma para las notificaciones por correo electrónico del cliente";
$MESS["IMOL_CONFIG_EDIT_LANG_EMAIL_TIP"] = "Este idioma se usará en los mensajes de correo electrónico enviados cuando un cliente recibe un registro de conversación solicitada o una notificación de mensaje nuevo.";
$MESS["IMOL_CONFIG_EDIT_WORKTIME"] = "Preferencias de tiempo de trabajo";
$MESS["IMOL_CONFIG_EDIT_WORKTIME_ENABLE"] = "Personalizar el tiempo de trabajo en el Canal Abierto";
$MESS["IMOL_CONFIG_EDIT_WORKTIME_TIMEZONE"] = "Seleccionar zona horaria";
$MESS["IMOL_CONFIG_EDIT_WORKTIME_TIME"] = "Horas de trabajo";
$MESS["IMOL_CONFIG_EDIT_WORKTIME_DAYOFF"] = "Fines de semana";
$MESS["IMOL_CONFIG_EDIT_WORKTIME_HOLIDAYS"] = "Vacaciones";
$MESS["IMOL_CONFIG_EDIT_WORKTIME_HOLIDAYS_EXAMPLE"] = "Ejemplo: 01.01,04.07,01.11,25.12";
$MESS["IMOL_CONFIG_EDIT_TIMEMAN_SUPPORT_B24"] = "No se puede utilizar esta opción porque la gestión del tiempo de trabajo no está disponible en su tarifa.";
$MESS["IMOL_CONFIG_EDIT_TIMEMAN_SUPPORT_CP"] = "No se puede utilizar esta opción ya que el módulo Working Time Management no está instalado.";
$MESS["IMOL_CONFIG_LOCK_ALT"] = "Se aplican restricciones, haga clic para ver más detalles.";
$MESS["IMOL_CONFIG_WEEK_MO"] = "Lunes";
$MESS["IMOL_CONFIG_WEEK_TU"] = "Martes";
$MESS["IMOL_CONFIG_WEEK_WE"] = "Miercoles";
$MESS["IMOL_CONFIG_WEEK_TH"] = "Jueves";
$MESS["IMOL_CONFIG_WEEK_FR"] = "Viernes";
$MESS["IMOL_CONFIG_WEEK_SA"] = "Sábado";
$MESS["IMOL_CONFIG_WEEK_SU"] = "Domingo";
$MESS["IMOL_CONFIG_EDIT_WORKTIME_DAYOFF_RULE"] = "Procesamiento de comunicaciones fuera de la hora de trabajo";
$MESS["IMOL_CONFIG_EDIT_WORKTIME_DAYOFF_FORM_ID"] = "Formulario del CRM";
$MESS["IMOL_CONFIG_EDIT_WORKTIME_DAYOFF_FORM_ID_NOTICE"] = "Si el ID de un clientese reconoce, no se enviará el formulario y el texto se enviarán de regreso en su lugar";
$MESS["IMOL_CONFIG_EDIT_WORKTIME_DAYOFF_TEXT"] = "Respuesta de texto automática";
$MESS["IMOL_CONFIG_EDIT_ACTIONS"] = "Configurar acciones";
$MESS["IMOL_CONFIG_EDIT_VOTE_MESSAGE"] = "Enviar solicitud de calificación de cliente";
$MESS["IMOL_CONFIG_EDIT_VOTE_MESSAGE_1_TITLE"] = "1. Personalizar el texto de la solicitud de calificación para su uso en Live Chat y Bitrix24.Network:";
$MESS["IMOL_CONFIG_EDIT_VOTE_MESSAGE_1_DESC"] = "* - cada texto no debe superar los 100 caracteres.";
$MESS["IMOL_CONFIG_EDIT_VOTE_MESSAGE_2_TITLE"] = "2. Personalizar el texto de la solicitud de calificación para su uso en otros canales:";
$MESS["IMOL_CONFIG_EDIT_VOTE_MESSAGE_2_DESC"] = "* - asegúrese de proporcionar descripciones concisas para las calificaciones \"1\" y \"0\". De lo contrario, sus clientes no podrán calificar su servicio.";
$MESS["IMOL_CONFIG_EDIT_VOTE_MESSAGE_TEXT"] = "Texto de la solicitud de calificación";
$MESS["IMOL_CONFIG_EDIT_VOTE_MESSAGE_LIKE"] = "Calificación positiva";
$MESS["IMOL_CONFIG_EDIT_VOTE_MESSAGE_DISLIKE"] = "Calificación negativa";
$MESS["IMOL_CONFIG_EDIT_VOTE_MESSAGE_EDIT"] = "Editar texto";
$MESS["IMOL_CONFIG_EDIT_WELCOME_MESSAGE"] = "Enviar saludos";
$MESS["IMOL_CONFIG_EDIT_WELCOME_MESSAGE_TIP"] = "Este texto será enviado inmediatamente después de que un cliente haya enviado un mensaje inicial.";
$MESS["IMOL_CONFIG_EDIT_WELCOME_MESSAGE_TEXT"] = "Mensaje de texto";
$MESS["IMOL_CONFIG_EDIT_WELCOME_BOT_JOIN"] = "Activar chat bot";
$MESS["IMOL_CONFIG_EDIT_WELCOME_BOT_JOIN_FIRST"] = "sólo la primera vez que un cliente inicia una conversación";
$MESS["IMOL_CONFIG_EDIT_WELCOME_BOT_JOIN_ALWAYS"] = "cada vez que un cliente inicia una conversación";
$MESS["IMOL_CONFIG_EDIT_WELCOME_BOT_LEFT"] = "Desconectar el chat bot";
$MESS["IMOL_CONFIG_EDIT_WELCOME_BOT_LEFT_QUEUE"] = "después de reenviar la conversación con un representante de ventas";
$MESS["IMOL_CONFIG_EDIT_WELCOME_BOT_LEFT_CLOSE"] = "después de completar la conversación";
$MESS["IMOL_CONFIG_EDIT_AGREEMENT_MESSAGE"] = "Enviar advertencia sobre la recopilación de datos personales";
$MESS["IMOL_CONFIG_EDIT_BUTTON"] = "cambiar";
$MESS["IMOL_CONFIG_EDIT_CLOSE_ACTION"] = "Acción de cierre de conversación";
$MESS["IMOL_CONFIG_EDIT_CLOSE_RULE"] = "Realizar una acción";
$MESS["IMOL_CONFIG_EDIT_CLOSE_FORM_ID"] = "Formulario del CRM";
$MESS["IMOL_CONFIG_EDIT_CLOSE_TEXT"] = "Mensaje de texto";
$MESS["IMOL_CONFIG_EDIT_AUTO_CLOSE_ACTION"] = "Acción a realizar cuando la conversación se cierre automáticamente";
$MESS["IMOL_CONFIG_EDIT_AUTO_CLOSE_TIME"] = "Tiempo de espera de la conversación";
$MESS["IMOL_CONFIG_EDIT_AUTO_CLOSE_TIME_1_H"] = "1 hora";
$MESS["IMOL_CONFIG_EDIT_AUTO_CLOSE_TIME_4_H"] = "4 horas";
$MESS["IMOL_CONFIG_EDIT_AUTO_CLOSE_TIME_8_H"] = "8 horas";
$MESS["IMOL_CONFIG_EDIT_AUTO_CLOSE_TIME_1_D"] = "1 día";
$MESS["IMOL_CONFIG_EDIT_AUTO_CLOSE_TIME_2_D"] = "2 días";
$MESS["IMOL_CONFIG_EDIT_AUTO_CLOSE_TIME_1_W"] = "1 semana";
$MESS["IMOL_CONFIG_EDIT_AUTO_CLOSE_TIME_1_M"] = "1 mes";
$MESS["IMOL_CONFIG_EDIT_AUTO_CLOSE_RULE"] = "Realizar una acción";
$MESS["IMOL_CONFIG_EDIT_AUTO_CLOSE_FORM_ID"] = "Formulario del CRM";
$MESS["IMOL_CONFIG_EDIT_AUTO_CLOSE_TEXT"] = "Mensaje de texto";
$MESS["IMOL_CONFIG_EDIT_CONNECTORS"] = "Conectar los canales de comunicación";
$MESS["IMOL_CONFIG_EDIT_SAVE"] = "Guardar";
$MESS["IMOL_CONFIG_EDIT_APPLY"] = "Aplicar";
$MESS["IMOL_CONFIG_EDIT_BACK"] = "Volver";
$MESS["IMOL_CONFIG_EDIT_POPUP_LIMITED_TITLE"] = "Canal Abierto Avanzado";
$MESS["IMOL_CONFIG_EDIT_POPUP_LIMITED_TEXT"] = "Su plan actual limita el número de usuarios que puede tener su compañía.
<br><br>
Sólo los usuarios de su compañía pueden ser incluidos en la cola de Canal Abierto
<br><br>
TIP: Bitrix24 Professional viene con un número ilimitado de Canales Abiertos y usuarios de negocios.
<br><br>
Prueba gratuita de Bitrix24 Professional durante 30 días.";
$MESS["IMOL_CONFIG_EDIT_POPUP_LIMITED_VOTE"] = "Los clientes son capaces de calificar las conversaciones en los siguientes planes: Plus, Standard, Professional.<br><br> Las calificaciones de los clientes permiten medir la efectividad de cada agente individual y ver cómo se compara con el resto.";
$MESS["IMOL_CONFIG_EDIT_POPUP_LIMITED_QUEUE_ALL"] = "Usted es capaz de distribuir los mensajes entrantes a todos simultáneamente en los siguientes planes: Plus, Standard, Professional.<br><br>Esta opción envía un mensaje entrante a todos en la cola y el primer agente para responder continúa con la conversación.";
$MESS["IMOL_CONFIG_EDIT_CATEGORY_TITLE"] = "Solicitud de categorías";
$MESS["IMOL_CONFIG_EDIT_CATEGORY_ENABLE"] = "Utilice las categorías para filtrar las solicitudes en canales abiertos";
$MESS["IMOL_CONFIG_EDIT_CATEGORY_ENABLE_TIP"] = "Categoría de solicitud puede ser seleccionado manualmente mientras está hablando con un cliente, o puede ser asignado por un chat bot automáticamente (sólo si el bot de chat está conectado al canal abierto)";
$MESS["IMOL_CONFIG_EDIT_CATEGORY_DESC"] = "Especificar las categorías de solicitudes para los clientes en este Canal Abierto.";
$MESS["IMOL_CONFIG_EDIT_CATEGORY_ADD"] = "agregar";
$MESS["IMOL_CONFIG_EDIT_CATEGORY_DELETE"] = "eliminar";
$MESS["IMOL_CONFIG_EDIT_CATEGORY_DEFAULT_ID"] = "valor por defecto";
$MESS["IMOL_CONFIG_EDIT_CATEGORY_DEFAULT_0"] = "no seleccionado";
$MESS["IMOL_CONFIG_EDIT_QUICK_ANSWERS_STORAGE"] = "Lista de respuestas predeterminadas";
$MESS["IMOL_CONFIG_QUICK_ANSWERS_DESC"] = "Use respuestas predieterminadas para ahorrar tiempo escribiendo respuestas a las preguntas más comunes de sus clientes. Guarda cualquier respuesta como una respuesta predeterminada y úsela mientras chatea en un Canal Abierto. <a href=\"https://helpdesk.bitrix24.com/open/6216707/&referer1=clman2lesson\">Detalles</a>";
$MESS["IMOL_CONFIG_QUICK_ANSWERS_LIST_MANAGE"] = "<a href=\"#LIST_URL#\">Administrar lista</a>";
$MESS["IMOL_CONFIG_QUICK_ANSWERS_CREATE_NEW"] = "<a href=\"#LIST_URL#\" target=\"_blank\">Crear</a>";
$MESS["IMOL_CONFIG_EDIT_LANG_SESSION_PRIORITY"] = "Prioridad de sesión";
$MESS["IMOL_CONFIG_EDIT_LANG_SESSION_PRIORITY_2"] = "segundos";
$MESS["IMOL_CONFIG_EDIT_LANG_SESSION_PRIORITY_TIP"] = "Este parámetro define la prioridad y el orden de los Canales Abiertos tal como aparecen en el chat del operador.";
?>