<?
$MESS["OL_COMPONENT_MODULE_NOT_INSTALLED"] = "El módulo Open Channels no está instalado.";
$MESS["OL_COMPONENT_LE_CRM_SOURCE_CREATE"] = "Origen de canal abierto";
$MESS["OL_COMPONENT_LE_BOT_LIST"] = "chat bot no está seleccionada";
$MESS["OL_COMPONENT_LE_OPTION_FORM"] = "Envíe el formulario del CRM";
$MESS["OL_COMPONENT_LE_OPTION_FORM_ID"] = "Formulario del CRM";
$MESS["OL_COMPONENT_LE_OPTION_TEXT"] = "Enviar texto";
$MESS["OL_COMPONENT_LE_OPTION_QUALITY"] = "Enviar texto y calificación de calidad";
$MESS["OL_COMPONENT_LE_OPTION_BOT"] = "Conectar chat bot";
$MESS["OL_COMPONENT_LE_OPTION_BOT_ID"] = "Chat bot";
$MESS["OL_COMPONENT_LE_OPTION_QUEUE"] = "En cola";
$MESS["OL_COMPONENT_LE_OPTION_NONE"] = "No hacer nada";
$MESS["OL_COMPONENT_LE_QUICK_ANSWERS_STORAGE_CREATE"] = "Crear automáticamente";
$MESS["OL_COMPONENT_LE_QUICK_ANSWERS_NO_ACCESS_CREATE"] = "No tiene permiso para editar este Canal Abierto";
$MESS["OL_COMPONENT_LE_QUICK_ANSWERS_STORAGE_CREATE_ERROR"] = "Error al crear la lista de Respuestas Predeterminadas. Por favor, póngase en contacto con el servicio de Helpdesk.";
$MESS["OL_COMPONENT_LE_QUICK_ANSWERS_STORAGE_CREATE_ERROR_UNIQUE"] = "Ya existe una lista de respuestas predeterminadas para este Canal Abierto.";
?>