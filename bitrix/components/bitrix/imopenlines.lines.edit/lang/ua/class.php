<?
$MESS["OL_COMPONENT_MODULE_NOT_INSTALLED"] = "Модуль відкритих ліній не встановлено";
$MESS["OL_COMPONENT_LE_CRM_SOURCE_CREATE"] = "Джерело відкритої лінії";
$MESS["OL_COMPONENT_LE_OPTION_FORM"] = "Відправити CRM-форму";
$MESS["OL_COMPONENT_LE_OPTION_TEXT"] = "Відправити текст";
$MESS["OL_COMPONENT_LE_OPTION_QUEUE"] = "Повернути назад в чергу";
$MESS["OL_COMPONENT_LE_OPTION_NONE"] = "Нічого не робити";
$MESS["OL_COMPONENT_LE_BOT_LIST"] = "чат-бот не вибрано";
$MESS["OL_COMPONENT_LE_OPTION_FORM_ID"] = "CRM-форма";
$MESS["OL_COMPONENT_LE_OPTION_BOT"] = "Підключити чат-бота";
$MESS["OL_COMPONENT_LE_OPTION_BOT_ID"] = "Чат-бот";
$MESS["OL_COMPONENT_LE_OPTION_QUALITY"] = "Відправити текст та оцінку якості";
$MESS["OL_COMPONENT_LE_QUICK_ANSWERS_STORAGE_CREATE"] = "Буде створено автоматично";
$MESS["OL_COMPONENT_LE_QUICK_ANSWERS_NO_ACCESS_CREATE"] = "У вас немає прав на редагування цієї відкритої лінії";
$MESS["OL_COMPONENT_LE_QUICK_ANSWERS_STORAGE_CREATE_ERROR"] = "Помилка при створенні списку швидких відповідей. Будь ласка, зверніться у техпідтримку";
$MESS["OL_COMPONENT_LE_QUICK_ANSWERS_STORAGE_CREATE_ERROR_UNIQUE"] = "До цієї відкритої лінії вже прив'язаний список швидких відповідей.";
?>