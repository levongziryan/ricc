<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_CURRENCY_NOT_FOUND"] = "No se encontró la moneda.";
$MESS["CRM_CURRENCY_SECTION_MAIN"] = "Moneda";
$MESS["CRM_CURRENCY_FIELD_ID"] = "Moneda";
$MESS["CRM_CURRENCY_FIELD_DEFAULT_EXCH_RATE"] = "Tipo de cambio (por defecto)";
$MESS["CRM_CURRENCY_FIELD_SORT"] = "Tipo de índice";
$MESS["CRM_CURRENCY_FULL_NAME"] = "Nombre";
$MESS["CRM_CURRENCY_FORMAT_STRING"] = "Formato de moneda";
$MESS["CRM_CURRENCY_DEC_POINT"] = "coma decimal";
$MESS["CRM_CURRENCY_THOUSANDS_VARIANT"] = "Separador de miles";
$MESS["CRM_CURRENCY_THOUSANDS_VARIANT_N"] = "Ninguno";
$MESS["CRM_CURRENCY_THOUSANDS_VARIANT_D"] = "Punto";
$MESS["CRM_CURRENCY_THOUSANDS_VARIANT_C"] = "Coma";
$MESS["CRM_CURRENCY_THOUSANDS_VARIANT_S"] = "Espacio";
$MESS["CRM_CURRENCY_THOUSANDS_VARIANT_B"] = "Espacio de no separación";
$MESS["CRM_CURRENCY_THOUSANDS_VARIANT_ANOTHER"] = "Otros";
$MESS["CRM_CURRENCY_THOUSANDS_SEP"] = "Separador de miles personalizado";
$MESS["CRM_CURRENCY_INVOICES_DEFAULT"] = "Moneda de facturación por defecto";
$MESS["CRM_CURRENCY_FIELD_AMOUNT_CNT"] = "Valor nominal";
$MESS["CRM_CURRENCY_SHOW_BASE"] = "Moneda de referencia";
?>