<?
$MESS["CRM_TAB_1"] = "Producto";
$MESS["CRM_TAB_1_TITLE"] = "Propiedades del producto";
$MESS["CRM_PRODUCT_PROP_ADD_BUTTON"] = "Agregar";
$MESS["CRM_PRODUCT_PROP_NO_VALUE"] = "(sin definir)";
$MESS["CRM_PRODUCT_PROP_CHOOSE_ELEMENT"] = "Seleccionar de la lista";
$MESS["CRM_PRODUCT_PROP_START_TEXT"] = "(ingresar nombre)";
$MESS["CRM_PRODUCT_PROP_NO_SEARCH_RESULT_TEXT"] = "no hay ningún resultado";
$MESS["CRM_PRODUCT_PROP_ENLARGE"] = "Ampliar ";
$MESS["CRM_PRODUCT_PROP_DOWNLOAD"] = "Descargar";
?>