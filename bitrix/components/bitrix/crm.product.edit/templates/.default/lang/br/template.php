<?
$MESS["CRM_TAB_1"] = "Produto";
$MESS["CRM_TAB_1_TITLE"] = "Propriedades do produto";
$MESS["CRM_PRODUCT_PROP_ADD_BUTTON"] = "Adicionar";
$MESS["CRM_PRODUCT_PROP_NO_VALUE"] = "(não definido)";
$MESS["CRM_PRODUCT_PROP_CHOOSE_ELEMENT"] = "Selecionar da lista";
$MESS["CRM_PRODUCT_PROP_START_TEXT"] = "(digite o nome)";
$MESS["CRM_PRODUCT_PROP_NO_SEARCH_RESULT_TEXT"] = "não foram encontrados resultados de busca";
$MESS["CRM_PRODUCT_PROP_ENLARGE"] = "Ampliar";
$MESS["CRM_PRODUCT_PROP_DOWNLOAD"] = "Download";
?>