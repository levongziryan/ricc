<?
$MESS["CRM_CATALOG_NOT_SELECTED"] = "[non indiqué]";
$MESS["CRM_PRODUCT_ID"] = "Code du produit";
$MESS["CRM_PRODUCT_ID_PARAM"] = "Nom de la variable de l'identificateur du produit";
$MESS["CRM_CATALOG_ID"] = "Catalogue de marchandises";
$MESS["CRM_PATH_TO_PRODUCT_SHOW"] = "Modèle de chemin d'accès vers la page de visualisation d'un produit";
$MESS["CRM_PATH_TO_PRODUCT_EDIT"] = "Modèle de chemin d'accès à la page d'édition d'un produit";
$MESS["CRM_PATH_TO_PRODUCT_LIST"] = "Modèle de chemin d'accès à la page de la liste des produits";
?>