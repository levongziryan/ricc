<?
$MESS["CRM_CATALOG_ID"] = "Catálogo comercial";
$MESS["CRM_PRODUCT_ID"] = "ID del producto";
$MESS["CRM_PRODUCT_ID_PARAM"] = "Nombre de variable de ID del producto";
$MESS["CRM_PATH_TO_PRODUCT_LIST"] = "Plantilla de ruta de la página de productos";
$MESS["CRM_PATH_TO_PRODUCT_EDIT"] = "Editar Plantilla de ruta de la página de productos";
$MESS["CRM_PATH_TO_PRODUCT_SHOW"] = "Ver Plantilla de ruta de la página de productos";
$MESS["CRM_CATALOG_NOT_SELECTED"] = "[no seleccionado]";
?>