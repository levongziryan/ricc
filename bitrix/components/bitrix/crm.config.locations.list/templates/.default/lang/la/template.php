<?
$MESS["CRM_LOC_EDIT_TITLE"] = "Abrir esta ubicación para la edición";
$MESS["CRM_LOC_EDIT"] = "Editar ubicación";
$MESS["CRM_LOC_DELETE_TITLE"] = "Eliminar esta ubicación";
$MESS["CRM_LOC_DELETE"] = "Eliminar ubicación";
$MESS["CRM_LOC_DELETE_CONFIRM"] = "Está seguro de que desea eliminar '% s'?";
$MESS["CRM_ALL"] = "Total";
?>