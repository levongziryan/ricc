<?
$MESS["CRM_COLUMN_CITY_NAME"] = "Ville";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_INTS_TASKS_NAV"] = "Inscriptions";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "Le module 'Boutique en ligne' n'a pas été installé.";
$MESS["CRM_LOC_UPDATE_GENERAL_ERROR"] = "Erreur de la mise à jour de l'emplacement.";
$MESS["CRM_LOC_DELETION_GENERAL_ERROR"] = "Erreur de suppression de l'ordre.";
$MESS["CRM_COLUMN_REGION_NAME"] = "District";
$MESS["CRM_COLUMN_SORT"] = "Trier";
$MESS["CRM_COLUMN_COUNTRY_NAME"] = "Pays";
?>