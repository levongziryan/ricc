<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "El módulo e-Store no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_COUNTRY_NAME"] = "País";
$MESS["CRM_COLUMN_REGION_NAME"] = "Región";
$MESS["CRM_COLUMN_CITY_NAME"] = "Ciudad";
$MESS["CRM_COLUMN_SORT"] = "Clasificar";
$MESS["CRM_LOC_DELETION_GENERAL_ERROR"] = "Error al eliminar la ubicación.";
$MESS["CRM_LOC_UPDATE_GENERAL_ERROR"] = "Error al actualizar la ubicación.";
$MESS["CRM_INTS_TASKS_NAV"] = "Registrar";
?>