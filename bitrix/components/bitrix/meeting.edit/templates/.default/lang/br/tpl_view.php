<?
$MESS["ME_LIST_TITLE"] = "Ver reuniões";
$MESS["ME_DESCR_TITLE"] = "Descrição da reunião";
$MESS["ME_EDIT_TITLE"] = "Editar";
$MESS["ME_FILES"] = "Arquivos";
$MESS["ME_CHANGE"] = "mudar";
$MESS["ME_OWNER"] = "Proprietário";
$MESS["ME_PLACE"] = "Localização";
$MESS["ME_DATE_START"] = "Começar";
$MESS["ME_CURRENT_STATE"] = "Status";
$MESS["ME_KEEPER"] = "Secretaria de Reuniões de minutos";
$MESS["ME_MEMBERS"] = "Participantes";
$MESS["ME_REFUSED"] = "Recusou";
$MESS["ME_ACTION"] = "Começar reunião";
$MESS["ME_CLOSE"] = "Concluir reunião";
$MESS["ME_COPY"] = "Criar próxima reunião";
$MESS["ME_PREPARE"] = "Retomar a reunião";
$MESS["ME_COMMENTS"] = "Comentários";
$MESS["ME_AGENDA"] = "Agenda";
$MESS["ME_GROUP"] = "Projeto";
?>