<?
$MESS["ME_MODULE_NOT_INSTALLED"] = "O módulo de \"Encontros e Eventos\" não está instalado.";
$MESS["ME_MEETING_NOT_FOUND"] = "A reunião não foi encontrada.";
$MESS["ME_MEETING_ACCESS_DENIED"] = "Acesso negado";
$MESS["ME_MEETING_EDIT"] = "Reunião número #ID# em #DATE#";
$MESS["ME_MEETING_EDIT_NO_DATE"] = "Reunião número #ID#";
$MESS["ME_MEETING_ADD"] = "Criar uma nova reunião";
$MESS["ME_MEETING_COPY"] = "Criar uma nova reunião";
$MESS["ME_MEETING_TITLE_DEFAULT"] = "(sem título)";
$MESS["ME_MEETING_TITLE_DEFAULT_1"] = "Tópico";
?>