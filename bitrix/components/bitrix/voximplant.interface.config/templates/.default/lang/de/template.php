<?
$MESS["VI_INTERFACE_TITLE"] = "Anrufbenutzeroberfläche konfigurieren";
$MESS["VI_INTERFACE_CHAT_TITLE"] = "Anrufe im Messenger anzeigen: ";
$MESS["VI_INTERFACE_CHAT_ADD"] = "Separaten Chat für jeden Anruf erstellen";
$MESS["VI_INTERFACE_CHAT_APPEND"] = "Einheitlichen Chat für alle Anrufe erstellen";
$MESS["VI_INTERFACE_CHAT_NONE"] = "Anrufe nicht anzeigen";
$MESS["VI_INTERFACE_SAVE"] = "Speichern";
?>