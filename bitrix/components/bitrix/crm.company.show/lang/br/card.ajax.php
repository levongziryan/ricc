<?
$MESS["CRM_COLUMN_COMPANY_TYPE"] = "Tipo da empresa";
$MESS["CRM_COLUMN_EMPLOYEES"] = "Empregados";
$MESS["CRM_COLUMN_PHONE"] = "Telefone";
$MESS["CRM_COLUMN_EMAIL"] = "E-mail";
$MESS["CRM_COLUMN_WEB"] = "Site";
$MESS["CRM_COLUMN_DATE_MODIFY"] = "Modificada em";
$MESS["CRM_SECTION_COMPANY_INFO"] = "Informações da empresa";
$MESS["CRM_SECTION_CONTACT_INFO"] = "Informações do contato";
$MESS["CRM_OPER_SHOW"] = "Visualizar";
$MESS["CRM_OPER_EDIT"] = "Editar";
?>