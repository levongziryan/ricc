<?
$MESS["MAIL_MODULE_NOT_INSTALLED"] = "O módulo Correspondência não está instalado.";
$MESS["INTR_MAIL_MAX_AGE_ERROR"] = "Especifique o intervalo de sincronização.";
$MESS["INTR_MAIL_IMAP_DIRS"] = "Selecione as pastas que deseja sincronizar";
$MESS["INTR_MAIL_CSRF"] = "Erro de segurança ao submeter o formulário.";
$MESS["INTR_MAIL_AUTH"] = "Erro de autenticação";
$MESS["INTR_MAIL_FORM_ERROR"] = "Erro ao processar o formulário.";
$MESS["INTR_MAIL_AJAX_ERROR"] = "Erro ao processar o pedido.";
$MESS["INTR_MAIL_SAVE_ERROR"] = "Erro ao salvar dados de conexão.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado.";
$MESS["INTR_MAIL_INP_EMAIL_BAD"] = "Endereço de e-mail inválido";
$MESS["INTR_MAIL_CRM_ALREADY"] = "Contas de e-mail CRM já configuradas";
$MESS["INTR_MAIL_CHECK_INTERVAL_2M"] = "2 minutos";
$MESS["INTR_MAIL_CHECK_INTERVAL_5M"] = "5 minutos";
$MESS["INTR_MAIL_CHECK_INTERVAL_10M"] = "10 minutos";
$MESS["INTR_MAIL_CHECK_INTERVAL_1H"] = "1 hora";
$MESS["INTR_MAIL_CHECK_INTERVAL_3H"] = "3 horas";
$MESS["INTR_MAIL_CHECK_INTERVAL_12H"] = "12 horas";
$MESS["INTR_MAIL_IMAP_AUTH_ERR_EXT"] = "Erro de autenticação. Verifique se o login e a senha estão corretos.<br>Observe que se você estiver usando as senhas do app e a autenticação em duas etapas estiver ativada, você tem que usar uma senha especial de integração.";
$MESS["INTR_MAIL_IMAP_OAUTH_ACC"] = "Erro ao obter dados da caixa de correio";
?>