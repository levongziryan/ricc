<?
$MESS["MAIL_MODULE_NOT_INSTALLED"] = "Le module Email n'est pas installé.";
$MESS["INTR_MAIL_MAX_AGE_ERROR"] = "Veuillez spécifier un intervalle de synchronisation.";
$MESS["INTR_MAIL_IMAP_DIRS"] = "Sélectionnez les dossiers à synchroniser";
$MESS["INTR_MAIL_CSRF"] = "Erreur de sécurité lors de l'envoi du formulaire.";
$MESS["INTR_MAIL_AUTH"] = "Erreur d'authentification";
$MESS["INTR_MAIL_FORM_ERROR"] = "Erreur de traitement du formulaire.";
$MESS["INTR_MAIL_AJAX_ERROR"] = "Erreur de traitement de la demande.";
$MESS["INTR_MAIL_SAVE_ERROR"] = "Erreur de sauvegarde des données.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé.";
$MESS["INTR_MAIL_INP_EMAIL_BAD"] = "Adresse e-mail invalide";
$MESS["INTR_MAIL_CRM_ALREADY"] = "Comptes e-mail CRM déjà configurés";
$MESS["INTR_MAIL_CHECK_INTERVAL_2M"] = "2 minutes";
$MESS["INTR_MAIL_CHECK_INTERVAL_5M"] = "5 minutes";
$MESS["INTR_MAIL_CHECK_INTERVAL_10M"] = "10 minutes";
$MESS["INTR_MAIL_CHECK_INTERVAL_1H"] = "1 heure";
$MESS["INTR_MAIL_CHECK_INTERVAL_3H"] = "3 heures";
$MESS["INTR_MAIL_CHECK_INTERVAL_12H"] = "12 heures";
$MESS["INTR_MAIL_IMAP_AUTH_ERR_EXT"] = "Erreur d’authentification. Veuillez vérifier que l'identifiant et le mot de passe sont corrects.<br>Notez que si vous utilisez les mots de passe de l'application et si la double authentification est activée, vous devez utiliser un mot de passe d'intégration spécial.";
$MESS["INTR_MAIL_IMAP_OAUTH_ACC"] = "Erreur lors de l'obtention des données de boîtes mail";
?>