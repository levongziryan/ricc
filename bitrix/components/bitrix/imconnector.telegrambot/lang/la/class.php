<?
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_MODULE_NOT_INSTALLED"] = "El módulo \"External IM Connectors\" no está instalado.";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_NO_ACTIVE_CONNECTOR"] = "Esta conexión está inactiva.";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_SESSION_HAS_EXPIRED"] = "Su sesión ha caducado. Por favor envíe el formulario de nuevo.";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_OK_SAVE"] = "La información se ha guardado correctamente.";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_NO_DATA_SAVE"] = "No hay datos para guardar";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_NO_SAVE"] = "Error al guardar los datos. Compruebe la entrada e inténtelo de nuevo.";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_OK_CONNECT"] = "La conexión de prueba se ha establecido con éxito";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_NO_CONNECT"] = "No se pudo establecer la conexión de prueba utilizando los parámetros proporcionados";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_OK_REGISTER"] = "El registro fue un éxito";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_NO_REGISTER"] = "Error de registro";
?>