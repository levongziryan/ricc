<?
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_API_TOKEN_NAME"] = "Token de acceso";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_REGISTER"] = "Bot de registro";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_TESTED"] = "Prueba de conexión";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_CONNECTOR_ERROR_STATUS"] = "Error de bot. Compruebe las credenciales y pruebe de nuevo la conexión";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_CREATE_NEW_BOT"] = "Crear nuevo bot";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_THERE_IS_A_BOT_TO_KNOW_THE_TOKEN"] = "Búsqueda de token del bot existente";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_I_KNOW_TOKEN"] = "Tengo el token";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_NAME_BOT"] = "Nombre del bot";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_LINK_BOT"] = "Enlace del bot";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_6_STEPS_TITLE"] = "Completar 6 pasos para crear bot de telegrama";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_1_OF_6"] = "paso 1 de 6";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_2_OF_6"] = "paso 2 de 6";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_3_OF_6"] = "paso 3 de 6";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_4_OF_6"] = "paso 4 de 6";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_5_OF_6"] = "paso 5 de 6";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_6_OF_6"] = "paso 6 de 6";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_1_OF_6_TITLE"] = "¿Por dónde empiezo?";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_1_OF_6_DESCRIPTION_1"] = "Siga el enlace y haga clic en Iniciar <span class=\"imconnector-link\"><a href=\"https://telegram.me/BotFather\" target=\"_blank\">https://telegram.me/BotFather</a></span>";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_2_OF_6_TITLE"] = "Creación de bot";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_2_OF_6_DESCRIPTION_1"] = "Elija un comando para la creación de un nuevo bot <span class=\"imconnector-link\">/newbot</span>";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_3_OF_6_TITLE"] = "Nombre del bot";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_3_OF_6_DESCRIPTION_1"] = "El nombre del bot que sus clientes van a utilizar para localizar e interactuar con su compañía a través del  telegrama. Este nombre se mostrará en la lista de contactos, así como los chats públicos de telegramas";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_4_OF_6_TITLE"] = "Bot de inicio de sesión";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_4_OF_6_DESCRIPTION_1"] = "Este es el nombre del telegrama bot. Los clientes serán capaces de encontrarlo en el telegrama y se utiliza para crear un enlace a su bot.";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_5_OF_6_TITLE"] = "Receptor de token";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_5_OF_6_DESCRIPTION_1"] = "Copiar token recibido";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_5_OF_6_DESCRIPTION_2"] = "Inserte el token en el siguiente campo y haga clic en \"Guardar\"";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_6_OF_6_TITLE"] = "Terminar";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_6_OF_6_DESCRIPTION_1"] = "El telegrama no está conectado a su canal abierto. Todos los mensajes enviados a través del robot va a terminar en Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_3_STEPS_TITLE"] = "Siga estos tres pasos para obtener un token para su telegrama bot";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_1_OF_3"] = "paso 1 de 3";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_2_OF_3"] = "paso 2 de 3";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_3_OF_3"] = "paso 3 de 3";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_1_OF_3_TITLE"] = "Recuperación de token";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_1_OF_3_DESCRIPTION_1"] = "Sigue el enlace <span class=\"imconnector-link\"><a href=\"https://telegram.me/BotFather\" target=\"_blank\">https://telegram.me/BotFather</a></span> y seleccione el comando <span class=\"imconnector-link\">/token</span><br><br>
Verá una lista de todos los robots que haya creado. Seleccione un bot para conectarse a Bitrix24 Línea Abierta.";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_2_OF_3_TITLE"] = "Conectar el bot";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_2_OF_3_DESCRIPTION_1"] = "Copiar el token recibido";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_2_OF_3_DESCRIPTION_2"] = "Inserte el token en el siguiente campo y haga clic en \"Guardar\"";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_3_OF_3_TITLE"] = "Terminar";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_STEP_3_OF_3_DESCRIPTION_1"] = "El telegrama está ahora conectado a su canal abierto. Todos los mensajes enviados a través del bot terminarán en Bitrix24
";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_SIMPLE_FORM_DESCRIPTION_1"] = "Si ya tiene un bot de telegramas y sabe el token, entre en el campo de abajo y haga clic en \"Guardar\".";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_SIMPLE_FORM_DESCRIPTION_TESTED"] = "Por favor, probar la conexión para asegurarse de que todos los datos se ha introducido correctamente.";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_SIMPLE_FORM_DESCRIPTION_REGISTER"] = "Ahora puede registrar su bot.";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_INDEX_DESCRIPTION"] = "Conectar el Telegrama al Canal Abierto para recibir mensajes a través del telegrama Bitrix24. Para ello, cree un nuevo bot telegrama o conecte una ya existente. <br> Si usted todavia no tiene bot de Telegrama , vamos a ayudarlo a crear y conectarse a Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_TELEGRAMBOT_FINAL_FORM_DESCRIPTION_1"] = "El telegrama no está conectado a su canal abierto. Todos los mensajes enviados a través del bot terminarán en Bitrix24";
?>