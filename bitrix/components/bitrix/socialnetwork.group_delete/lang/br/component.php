<?
$MESS["SONET_C9_TITLE"] = "Excluir grupo";
$MESS["SONET_P_USER_NO_GROUP"] = "O grupo não foi encontrado";
$MESS["SONET_MODULE_NOT_INSTALL"] = "O módulo de rede social não está instalado.";
$MESS["SONET_P_USER_NO_USER"] = "O usuário não foi encontrado.";
$MESS["SONET_C9_NO_PERMS"] = "Você não tem permissão para efetuar essa operação";
?>