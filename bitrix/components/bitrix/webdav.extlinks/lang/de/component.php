<?
$MESS["WD_MODULE_IS_NOT_INSTALLED"] = "Das Modul Dokumentenbibliothek ist nicht installiert.";
$MESS["WD_MODULE_IS_FILE_NOT_FOUND"] = "Datei wurde nicht gefunden.";
$MESS["WD_MODULE_IS_FILE_NOT_FOUND_DESCRIPTION"] = "Möglicherweise wurde diese Datei nicht veröffentlicht oder der Link ist nicht korrekt. Wenden Sie sich bitte an den Dateibesitzer.";
?>