<?
$MESS["WD_MODULE_IS_FILE_NOT_FOUND_DESCRIPTION"] = "Peut être le fichier a été retiré de la publication ou vous avez cliqué sur le mauvais lien. Reportez-vous au propriétaire du fichier.";
$MESS["WD_MODULE_IS_NOT_INSTALLED"] = "Le document intitulé «# FILENAME # 'est déjà en cours d'utilisation.";
$MESS["WD_MODULE_IS_FILE_NOT_FOUND"] = "Fichier introuvable";
?>