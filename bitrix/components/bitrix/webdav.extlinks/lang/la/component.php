<?
$MESS["WD_MODULE_IS_NOT_INSTALLED"] = "El módulo de biblioteca de documentos no está instalado.";
$MESS["WD_MODULE_IS_FILE_NOT_FOUND"] = "No se ha encontrado el archivo";
$MESS["WD_MODULE_IS_FILE_NOT_FOUND_DESCRIPTION"] = "Es posible que el archivo no fue publicado o el enlace no es válido. Por favor, póngase en contacto con el propietario del archivo.";
?>