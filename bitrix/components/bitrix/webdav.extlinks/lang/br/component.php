<?
$MESS["WD_MODULE_IS_NOT_INSTALLED"] = "O módulo de biblioteca de documentos não está instalado.";
$MESS["WD_MODULE_IS_FILE_NOT_FOUND"] = "Arquivo não encontrado";
$MESS["WD_MODULE_IS_FILE_NOT_FOUND_DESCRIPTION"] = "possível que o arquivo seja inédito ou o link é inválido. Por favor, entre em contato com o dono do arquivo.";
$MESS["IBEL_BIZPROC_APPLY"] = "Aplicar";
$MESS["WD_DROP_SECTION"] = "Excluir pasta #NAME#";
$MESS["WD_DOWNLOAD_FILE"] = "Baixar";
?>