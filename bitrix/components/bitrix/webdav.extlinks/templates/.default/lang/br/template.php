<?
$MESS["WD_EXT_LINK_COMP_LINK"] = "Baixar ARQUIVO";
$MESS["WD_EXT_LINK_COMP_REMOVED"] = "O link é inválido ou o arquivo foi apagado.";
$MESS["WD_EXT_LINK_COMP_T1"] = "Arquivo está disponível na Intranet.";
$MESS["WD_EXT_LINK_COMP_B"] = "Bitrix24";
$MESS["WD_EXT_LINK_COMP_LOGO_C"] = "Home";
$MESS["WD_EXT_LINKS_COMP_PASS_TITLE"] = "Este arquivo é protegido por senha.";
$MESS["WD_EXT_LINKS_COMP_PASS_TEXT"] = "Se você tiver uma senha, por favor digite-a abaixo.";
$MESS["WD_EXT_LINKS_COMP_PASS"] = "Senha";
$MESS["WD_EXT_LINKS_COMP_PASS_CONTINUE"] = "Continuar '";
$MESS["WD_EXT_LINKS_COMP_PASS_TITLE_WRONG"] = "Infelizmente, a senha que você forneceu está incorreta. Por favor, TENTE Novamente.";
$MESS["IBEL_BIZPROC_START"] = "Executar novo processo de negócio";
$MESS["WD_PATH"] = "Caminho:";
$MESS["WD_DOCUMENT"] = "Documento";
$MESS["WD_VERSIONS"] = "Versões";
$MESS["IBLOCK_TYPE"] = "Tipo de bloco de informações contendo bibliotecas de documentos";
$MESS["WD_DISK_JS_ERROR_OFFLINE"] = "IM está offline.";
$MESS["WD_ATTENTION2_1"] = "Os seguintes arquivos têm títulos idênticos:";
$MESS["WD_CHANGE"] = "Editar";
$MESS["T_IBLOCK_PROPERTY"] = "PROPRIEDADES";
$MESS["WD_EXT_LINK_COMP_PREVIEW"] = "Visualização";
?>