<?
$MESS["WD_EXT_LINK_COMP_B"] = "Bitrix24";
$MESS["WD_EXT_LINKS_COMP_PASS_TEXT"] = "Si vous avez le mot de passe, veuillez le saisir dans le champ au-dessous.";
$MESS["WD_EXT_LINKS_COMP_PASS_TITLE_WRONG"] = "Malheureusement le mot de passe ne convient pas. Essayer à nouveau.";
$MESS["WD_EXT_LINK_COMP_LOGO_C"] = "maison";
$MESS["WD_EXT_LINK_COMP_REMOVED"] = "Lien incorrect ou ce fichier a été supprimé par l'utilisateur.";
$MESS["WD_EXT_LINKS_COMP_PASS"] = "Mot de passe";
$MESS["WD_EXT_LINKS_COMP_PASS_CONTINUE"] = "Continuer";
$MESS["WD_EXT_LINK_COMP_PREVIEW"] = "Affichage";
$MESS["WD_EXT_LINK_COMP_LINK"] = "Télécharger le document";
$MESS["WD_EXT_LINK_COMP_T1"] = "Le fichier est disponible sur l'intranet social";
$MESS["WD_EXT_LINKS_COMP_PASS_TITLE"] = "Ce fichier est protégé par un mot de passe.";
?>