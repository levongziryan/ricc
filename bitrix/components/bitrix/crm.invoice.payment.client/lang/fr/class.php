<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module GRC n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Le module Boutique en ligne n'est pas installé.";
$MESS["CIPC_EMPTY_PAY_SYSTEM"] = "Aucun système de paiement sélectionné";
$MESS["CIPC_WRONG_ACCOUNT_NUMBER"] = "La facture dotée du numéro spécifié est introuvable";
$MESS["CIPC_WRONG_LINK"] = "Aucune entrée n'a été trouvée.";
$MESS["CIPC_ERROR_PAYMENT_EXECUTION"] = "Erreur lors de l'exécution du paiement";
$MESS["CIPC_TITLE_COMPONENT"] = "Paiement de la facture";
?>