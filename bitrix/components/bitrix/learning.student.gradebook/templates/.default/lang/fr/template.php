<?
$MESS["LEARNING_PROFILE_SCORE"] = "Evaluation";
$MESS["LEARNING_BACK_TO_GRADEBOOK"] = "Revenir vers la liste des tests";
$MESS["LEARNING_PROFILE_QUESTIONS"] = "Nombre de questions";
$MESS["LEARNING_PROFILE_YES"] = "Oui";
$MESS["LEARNING_PROFILE_DATE_END"] = "Date de mise en service";
$MESS["LEARNING_PROFILE_ACTION"] = "Actions";
$MESS["LEARNING_PROFILE_TIME_DURATION"] = "Temps dépensé";
$MESS["LEARNING_PROFILE_COURSE"] = "Taux";
$MESS["LEARNING_PROFILE_BEST_SCORE"] = "Meilleure résultat";
$MESS["LEARNING_PROFILE_NO"] = "Non";
$MESS["LEARNING_PROFILE_NO_DATA"] = "Pas de données";
$MESS["LEARNING_PROFILE_MARK"] = "Evaluation";
$MESS["LEARNING_ATTEMPT_NOT_FINISHED"] = "Un essai n'est pas terminé";
$MESS["LEARNING_ATTEMPTS_TITLE"] = "Essais";
$MESS["LEARNING_PROFILE_ATTEMPTS"] = "Essais";
$MESS["LEARNING_PROFILE_LAST_RESULT"] = "Le dernier test est passé.";
$MESS["LEARNING_PROFILE_RESULT"] = "A été passé";
$MESS["LEARNING_PROFILE_TRY"] = "Passer encore une fois";
$MESS["LEARNING_PROFILE_LAST_SCORE"] = "Résultat du dernier test";
$MESS["LEARNING_PROFILE_TEST_DETAIL"] = "Liste de tentatives";
$MESS["LEARNING_PROFILE_TEST"] = "Test";
$MESS["LEARNING_TEST_CHECKED_MANUALLY_SO_NOT_ALL_RESULTS_CAN_BE_ACTUAL"] = "Le test est vérifié manuellement et peut être encore non vérifié.";
?>