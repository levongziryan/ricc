<?
$MESS["TEST_ID_VARIABLE_TIP"] = "Identificateur du texte.";
$MESS["SET_TITLE_TIP"] = "Si cette option est sélectionnée, <b>Résultats de test</b> sera établi sous forme de titre de la page.";
$MESS["TEST_DETAIL_TEMPLATE_TIP"] = "Chemin vers la page du test.";
$MESS["COURSE_DETAIL_TEMPLATE_TIP"] = "Chemin vers la page avec la revue détaillée du cours.";
?>