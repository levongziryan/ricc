<?
$MESS["CRM_SUBSCRIBE_ADD_TITLE"] = "Écrire un message";
$MESS["CRM_SUBSCRIBE_ADD_BUTTON"] = "Envoyer";
$MESS["CRM_SUBSCRIBE_TITLE"] = "En-tête";
$MESS["CRM_SUBSCRIBE_SUBJECT"] = "Titre";
$MESS["CRM_SUBSCRIBE_FROM"] = "De qui";
$MESS["CRM_SUBSCRIBE_TO"] = "Dans";
$MESS["CRM_SUBSCRIBE_TO_SHOW"] = "Afficher";
$MESS["CRM_SUBSCRIBE_TO_HIDE"] = "Cacher";
$MESS["CRM_SUBSCRIBE_ADD_FILE"] = "Attacher le fichier";
$MESS["CRM_NEW_TITLE"] = "Écrire un message";
?>