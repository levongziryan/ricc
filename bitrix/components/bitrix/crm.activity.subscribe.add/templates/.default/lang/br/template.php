<?
$MESS["CRM_SUBSCRIBE_ADD_TITLE"] = "Enviar mensagem";
$MESS["CRM_SUBSCRIBE_ADD_BUTTON"] = "Enviar";
$MESS["CRM_SUBSCRIBE_TITLE"] = "Assunto";
$MESS["CRM_SUBSCRIBE_SUBJECT"] = "Título";
$MESS["CRM_SUBSCRIBE_FROM"] = "De";
$MESS["CRM_SUBSCRIBE_TO"] = "Para";
$MESS["CRM_SUBSCRIBE_TO_SHOW"] = "Mostrar";
$MESS["CRM_SUBSCRIBE_TO_HIDE"] = "Ocultar";
$MESS["CRM_SUBSCRIBE_ADD_FILE"] = "Anexar arquivo";
$MESS["CRM_NEW_TITLE"] = "Enviar mensagem";
?>