<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_PRESET_EDIT_TITLE"] = "Campos para modelo: #NAME# (#ENTITY_TYPE_NAME#)";
$MESS["CRM_PRESET_ENTITY_TYPE_INVALID"] = "Tipo de modelo incorreto";
$MESS["CRM_PRESET_NOT_FOUND"] = "O modelo não foi encontrado";
$MESS["CRM_PRESET_FIELD_ID"] = "ID";
$MESS["CRM_PRESET_FIELD_FIELD_NAME"] = "Nome";
$MESS["CRM_PRESET_FIELD_FIELD_ETITLE"] = "Nome";
$MESS["CRM_PRESET_FIELD_FIELD_TITLE"] = "Nome no modelo";
$MESS["CRM_PRESET_FIELD_SORT"] = "Tipo";
$MESS["CRM_PRESET_FIELD_IN_SHORT_LIST"] = "Mostrar no resumo";
$MESS["CRM_PRESET_EDIT_SELECT_FIELDS_NONE"] = "(Não selecionado)";
$MESS["CRM_PRESET_EDIT_SELECT_OTHER_FIELDS"] = "(Campos não vinculados)";
$MESS["CRM_REQUISITE_PRESET_NAME_EMPTY"] = "Modelo sem título";
?>