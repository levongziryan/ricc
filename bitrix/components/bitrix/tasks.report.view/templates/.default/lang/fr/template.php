<?
$MESS["TASKS_REPORT_MY_GROUPS_TASKS_ONLY_HINT"] = "On sort toutes les tâches des groupes auxquels vous adhérez";
$MESS["TASKS_REPORT_MY_TASKS_HINT"] = "Sont affichées toutes les tâches dont vous êtes exécuteur ou coexécuteur";
$MESS["TASKS_REPORT_MY_DEPTS_TASKS_ONLY_HINT"] = "Afficher les tâches de vos subordonnés et celles que vous avez données";
$MESS["TASKS_REPORT_DURATION_DAYS"] = "d.";
$MESS["TASKS_REPORT_MY_GROUPS_TASKS_ONLY"] = "Tâches des groupes";
$MESS["TASKS_REPORT_MY_DEPTS_TASKS_ONLY"] = "Tâches des subordonnés";
$MESS["TASKS_REPORT_MY_TASKS_ONLY"] = "Mes tâches";
$MESS["TASKS_REPORT_DURATION_HOURS"] = "h.";
?>