<?
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_TESTED"] = "Conexión de prueba";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CREATE_NEW_BOT"] = "Crear un nuevo bot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_FORM_SETTINGS"] = "Ya tengo un bot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECTOR_ERROR_STATUS"] = "Su bot ha encontrado un error. Por favor, compruebe las preferencias y probar el nuevo bot.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_GET_LINKS"] = "Obtener enlaces en sitio de Microsoft";
$MESS["IMCONNECTOR_COMPONENT_FINAL_FORM_DESCRIPTION_OK_1"] = "Microsoft Bot Framework ha sido conectado a su Canal Abierto.
A partir de ahora, todos los mensajes enviados al bot serán remitidos a su Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FINAL_FORM_DESCRIPTION_OK_2"] = "Puede conectar otras fuentes de comunicación para el bot o configurar las ya existentes para reenviar todos los mensajes al chat.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_10_STEPS_TITLE"] = "Siga estos pasos para conectar Microsoft Bot Framework al Canal Abierto";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_1_OF_10_TITLE"] = "Iniciar sesión en el sitio de Microsoft";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_2_OF_10_TITLE"] = "Registrar nuevo bot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_5_OF_10_TITLE"] = "Información adicional";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_6_OF_10_TITLE"] = "Contrato";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_7_OF_10_TITLE"] = "Publicar el bot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_8_OF_10_TITLE"] = "Opciones";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_9_OF_10_TITLE"] = "Se está revisando el bot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_10_OF_10_TITLE"] = "¡Listo!";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_1_OF_10"] = "paso 1 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_2_OF_10"] = "paso 2 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_3_OF_10"] = "paso 3 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_4_OF_10"] = "paso 4 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_5_OF_10"] = "paso 5 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_6_OF_10"] = "paso 6 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_7_OF_10"] = "paso 7 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_8_OF_10"] = "paso 8 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_9_OF_10"] = "paso 9 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_10_OF_10"] = "paso 10 de 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_1_OF_10_DESCRIPTION_1"] = "Vaya al sitio web de Microsoft e inicie sesión usando su cuenta:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_2_OF_10_DESCRIPTION_3"] = "Copie la dirección especificada a continuación y péguelo en el campo \"Punto final de la mensajería\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_BOT_HANDLE_NAME"] = "ID del bot (Bot handle):";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_APP_ID_NAME"] = "ID de la Aplicación (ID Microsoft App):";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_APP_SECRET_NAME"] = "Aplicación de contraseña secreta:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_2_OF_10_DESCRIPTION_2"] = "Especifique el nombre del bot (bot handle) que usted ha proporcionado:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_6_OF_10_DESCRIPTION_1"] = "Lea los términos de uso, o confirme su acuerdo al seleccionar la casilla de verificación y haga clic en \"Registrarse\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_7_OF_10_DESCRIPTION_1"] = "El bot ha sido creado. Ahora usted tiene que publicarlo.
Revisar la información proporcionada y, si todo es correcto, haga clic en \"Publicar\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_8_OF_10_DESCRIPTION_1"] = "Ahora el bot debe ser verificado. Marque la opción de publicación para su bot en la lista de bots para que sea visible para los demás, y haga clic en \"Enviar para revisión\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_9_OF_10_DESCRIPTION_1"] = "Asegúrese de que el bot ha sido enviado para su verificación: el estado debe decir \"En revisión\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_9_OF_10_DESCRIPTION_2"] = "Tenga en cuenta que sólo en esta página se puede obtener los enlaces a aplicaciones conectadas a su bot.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_9_OF_10_DESCRIPTION_3"] = "Skype y chat en vivo ya están conectados. No requieren configuración.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SIMPLE_FORM_DESCRIPTION_TESTED"] = "Usted puede comprobar la exactitud de los datos.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_COPY"] = "Copiar";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_1_OF_10_DESCRIPTION_2"] = "Registrar el bot seleccionando \"Registre un bot\" en el menú superior.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_3_OF_10_TITLE"] = "Creación de aplicaciones";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_4_OF_10_TITLE"] = "Obtener contraseña";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_3_OF_10_DESCRIPTION_1"] = "Oprima el botón \"Crear ID de Microsoft App y contraseña\"";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_3_OF_10_DESCRIPTION_2"] = "Aparecerá una nueva ventana. Copiar el ID de la aplicación e introducirla en el campo de abajo";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_4_OF_10_DESCRIPTION_1"] = "Oprima el botón \"Crear contraseña para continuar\"  ";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_4_OF_10_DESCRIPTION_2"] = "Copiar la contraseña en la nueva ventana que aparece";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_4_OF_10_DESCRIPTION_4"] = "Oprima el botón \"Finalizar la operación y volver Bot Framework\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_INDEX_DESCRIPTION"] = "Conecte Microsoft Bot Framework a Open Channel para recibir mensajes de sus clientes desde Skype y otras fuentes compatibles (Slack, Kik, GroupMe, SMS, correo electrónico, etc.) directamente a su Bitrix24.
Le ayudaremos a crear un bot y conectarlo a su Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_2_OF_10_DESCRIPTION_1"] = "Proporcione la información necesaria sobre su nuevo bot.<br><br>
                                                    - Subir una imagen del logotipo. Sólo los archivos PNG se aceptan no más de 30 kB<br>
                                                    - Introduzca el nombre de su bot<br>
                                                    - Proporcionar un ID de bot único<br>
                                                    - Introduzca la descripción del bot<br><br>
                                                    Estos datos estarán disponibles una vez que su bot haya sido publicado en el sitio web de Microsoft.<br>
                                                    Todos los campos son obligatorios.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_4_OF_10_DESCRIPTION_3"] = "¡Importante! La contraseña sólo se mostrará una vez. Recomendamos guardarlo en algún lugar seguro. <br><br> Introduzca la contraseña en el campo de abajo.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_5_OF_10_DESCRIPTION_1"] = "Proporcione información que estará disponible en el sitio web de Microsoft en la descripción de su bot. Los siguientes campos son obligatorios.<br><br>
                                                    Nombre del editor - su nombre<br>
                                                    Email del editor - su e-mail<br>
                                                    Declaración de privacidad - URL de su declaración de privacidad<br>
                                                    Términos de uso - URL a la página términos de uso";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SIMPLE_FORM_DESCRIPTION_1"] = "Si su bot ya se ha registrado, conéctelo a Bitrix24.<br><br>
Especifique esta dirección en el campo \"Messaging Webhook\" del sitio web de Microsoft:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TWILIO_PLACEHOLDER"] = "+15615155866";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_DESCRIPTION"] = "Puede especificar enlaces públicos a los canales que conectó con Microsoft para mostrarlos en el widget (chat en directo) del sitio.
<br><br>
Para obtener los enlaces, continúe con #LINK_BEGIN#su cuenta de Microsoft Bot Framework#LINK_END# haga clic en \"Get bot embed codes\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_TITLE"] = "especificar enlaces a canales de comunicación";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_CHANNELS_DESCRIPTION"] = "En su #LINK_BEGIN#Microsoft Bot Framework account#LINK_END#, copie los enlaces a los canales que desee mostrar a sus clientes en Live Chat y péguelos a continuación.
<br><br>
Observe que solo una parte de un enlace necesita ser copiada. Encontrará ejemplos de lo que tiene que copiar y pegar.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_DESCRIPTION_MASTER"] = "Puede especificar vínculos públicos con los canales que conectó con Microsoft para mostrarlos en el widget (chat en directo) del sitio en el #LINK_BEGIN#formato de configuración de canal#LINK_END#.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_SKYPEBOT_PLACEHOLDER"] = "https://join.skype.es/bot/0c237a9f-e52b-4bbd-92ed-d5a5a850c892";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_SKYPEBOT_EXAMPLE"] = "&lt;a href='<strong>https://join.skype.es/bot/0c237a9f-e52b-4bbd-92ed-d5a5a850c892</strong>'&gt;&lt;img src='https://dev.botframework.com/Client/Images/Add-To-Skype-Buttons.png'/&gt;&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_SLACK_PLACEHOLDER"] = "https://slack.es/oauth/authorize?scope=bot&client_id=58933734192.70978425654&redirect_uri=https%3a%2f%2fslack.botframework.com%2fHome%2fauth&state=bitrix";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_SLACK_EXAMPLE"] = "&lt;a href=\"<strong>https://slack.es/oauth/authorize?scope=bot&client_id=58933734192.70978425654 &redirect_uri=https%3a%2f%2fslack.botframework.com%2fHome%2fauth&state=bitrix</strong>\"&gt;&lt;img height=\"40\" width=\"139\" src=\"https://platform.slack-edge.com/img/add_to_slack.png\" srcset=\"https://platform.slack-edge.com/img/add_to_slack.png 1x, https://platform.slack-edge.com/img/add_to_slack@2x.png 2x\"&gt;&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_KIK_PLACEHOLDER"] = "https://bots.kik.es/#/testbitrix";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_KIK_EXAMPLE"] = "&lt;a href='<strong>https://bots.kik.es/#/bitrix</strong>'&gt;bitrix&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_GROUPME_PLACEHOLDER"] = "https://groupme.botframework.es/?botId=bitrix";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_GROUPME_EXAMPLE"] = "&lt;a href='<strong>https://groupme.botframework.es/?botId=bitrix</strong>'&gt;@bitrix&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TWILIO_EXAMPLE"] = "&lt;a href=\"tel:<strong>+15615155866</strong>\"&gt;(561) 515-5866&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_MSTEAMS_PLACEHOLDER"] = "https://teams.microsoft.es/l/chat/0/0?users=28:0c237a9f-e52b-4bbd-92ed-d5a5a850c892";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_MSTEAMS_EXAMPLE"] = "&lt;a href='<strong>https://teams.microsoft.es/l/chat/0/0?users=28:0c237a9f-e52b-4bbd-92ed-d5a5a850c892</strong>'&gt;&lt;img height=\"30\" width=\"113\" src='https://dev.botframework.com/Client/Images/Add-To-MSTeams-Buttons.png'&gt;&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_WEBCHAT_PLACEHOLDER"] = "https://webchat.botframework.es/embed/bitrix?s=SZfvj3Wdqrw.cwA.7RE.iiQcU8IlUJsYFASNYXusu3BmAsHitiyvVSH4A0xjTF0";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_WEBCHAT_EXAMPLE"] = "Get the link:<br>
									&lt;iframe src=\"<strong>https://webchat.botframework.es/embed/bitrix?s=</strong>YOUR_SECRET_HERE\" style=\"height: 502px; max-height: 502px;\"&gt;&lt;/iframe&gt;<br>Replace YOUR_SECRET_HERE with the contents of the \"Secret\" field.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_EMAILOFFICE365_PLACEHOLDER"] = "pokoev@bitrix.onmicrosoft.es";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_EMAILOFFICE365_EXAMPLE"] = "&lt;a href=\"mailto:<strong>pokoev@bitrix.onmicrosoft.es</strong>\"&gt;pokoev@bitrix.onmicrosoft.com&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TELEGRAM_PLACEHOLDER"] = "https://telegram.es/pokoevBot";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_TELEGRAM_EXAMPLE"] = "&lt;a href='<strong>https://telegram.es/pokoevBot</strong>'&gt;@pokoevBot&lt;/a&gt;";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_FACEBOOKMESSENGER_PLACEHOLDER"] = "https://www.messenger.es/t/1343073279053039";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_FACEBOOKMESSENGER_EXAMPLE"] = "&lt;a href='<strong>https://www.messenger.es/t/1343073279053039</strong>'&gt;&lt;img src='https://facebook.botframework.com/Content/MessageUs.png'&gt;&lt;/a&gt;";
?>