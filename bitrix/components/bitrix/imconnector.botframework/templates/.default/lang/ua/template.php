<?
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_BOT_HANDLE_NAME"] = "Ідентифікатор бота (Bot handle):";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_APP_ID_NAME"] = "ID програми (Microsoft App ID):";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_APP_SECRET_NAME"] = "Секретний пароль додатка:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_TESTED"] = "Тестування з'єднання";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_INDEX_DESCRIPTION"] = "Підключіть Microsoft Bot Framework до Відкритої лінії щоб приймати звернення ваших клієнтів зі Skype та інших каналів, які підтримує бот (Slack, Kik, GroupMe, SMS, email та ін) у вашому Бітрікс24.<br><br>
Ми допоможемо Вам створити бота в кілька кроків і підключити його до вашого Бітрікс24.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CREATE_NEW_BOT"] = "Створити нового бота";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_FORM_SETTINGS"] = "Є бот, заповнити форму";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_CONNECTOR_ERROR_STATUS"] = "У процесі роботи вашого бота виникла помилка. Вам необхідно перевірити зазначені дані та протестувати бота повторно.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_GET_LINKS"] = "Отримати посилання на сайті Microsoft";
$MESS["IMCONNECTOR_COMPONENT_FINAL_FORM_DESCRIPTION_OK_1"] = "Microsoft Bot Framework успішно підключений до вашої Відкритої лінії.
Тепер всі звернення до вашого боту будуть автоматично спрямовані на ваш Бітрікс24.";
$MESS["IMCONNECTOR_COMPONENT_FINAL_FORM_DESCRIPTION_OK_2"] = "Ви можете підключити до боту інші канали комунікацій або провести налаштування підключених, щоб збирати всі повідомлення в єдиний чат.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_10_STEPS_TITLE"] = "Щоб підключити Microsoft Bot Framework до Відкритої лінії, необхідно виконати кілька кроків";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_1_OF_10_TITLE"] = "Авторизація на сайті Microsoft";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_2_OF_10_TITLE"] = "Реєстрація нового бота";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_3_OF_10_TITLE"] = "Створення додатка";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_4_OF_10_TITLE"] = "Отримання секретного пароля";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_5_OF_10_TITLE"] = "Додаткова інформація";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_6_OF_10_TITLE"] = "Угоди";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_7_OF_10_TITLE"] = "Публікація бота";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_8_OF_10_TITLE"] = "Опції";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_9_OF_10_TITLE"] = "Бот на перевірці";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_10_OF_10_TITLE"] = "Все готово!";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_1_OF_10"] = "крок 1 з 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_2_OF_10"] = "крок 2 з 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_3_OF_10"] = "крок 3 з 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_4_OF_10"] = "крок 4 з 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_5_OF_10"] = "крок 5 з 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_6_OF_10"] = "крок 6 з 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_7_OF_10"] = "крок 7 з 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_8_OF_10"] = "крок 8 з 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_9_OF_10"] = "крок 9 з 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_10_OF_10"] = "крок 10 з 10";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_1_OF_10_DESCRIPTION_1"] = "Перейдіть на сайт Microsoft і авторизуйтесь під своїм обліковим записом: ";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_1_OF_10_DESCRIPTION_2"] = "Зареєструйте бота, вибравши пункт верхнього меню \"Register a bot\"";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_2_OF_10_DESCRIPTION_1"] = "Заповніть основну інформацію про вашого майбутнього бота.<br><br>
- Завантажте логотип. Зверніть увагу, що картинка повинна бути не більше 30К і тільки у форматі png (ви можете залишити запропоновану картинку)<br>
- Введіть ім'я вашого бота<br>
- Унікальний ідентифікатор бота латиницею<br>
- Опис<br><br>
Всі дані будуть доступні після публікації вашого бота, в списку, на сайті Microsoft.<br>
Всі поля обов'язкові до заповнення.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_2_OF_10_DESCRIPTION_2"] = "Ім'я бота (bot handle), введене вами, введіть в поле нижче:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_2_OF_10_DESCRIPTION_3"] = "Скопіюйте адресу, вказану нижче і вставте її в поле «Messaging endpoin».";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_3_OF_10_DESCRIPTION_1"] = "Натисніть кнопку «Create Microsoft App ID and password».";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_3_OF_10_DESCRIPTION_2"] = "У вікні, скопіюйте ідентифікатор програми і вставте його в поле нижче.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_4_OF_10_DESCRIPTION_1"] = "Натисніть кнопку «Створити пароль, щоб продовжити».";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_4_OF_10_DESCRIPTION_2"] = "У вікні, що відчинилося, скопіюйте пароль.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_4_OF_10_DESCRIPTION_3"] = "Зверніть увагу, що пароль буде показаний тільки один раз, тому
рекомендуємо скопіювати і зберегти його для себе.<br><br>
Вкажіть пароль у полі нижче.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_4_OF_10_DESCRIPTION_4"] = "Натисніть кнопку «Завершити операцію і повернутися до Bot Framework».";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_5_OF_10_DESCRIPTION_1"] = "Заповніть інформацію, яка буде доступна в описі вашого бота на сайті Microsoft. Достатньо тільки заповнити обов'язкові поля.<br><br>
Publisher name - ім'я<br>
Publisher Email - e-mail<br>
Privacy statement - посилання на сторінку з умовами конфіденційності<br>
Terms of Use - посилання на сторінку з угодою";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_6_OF_10_DESCRIPTION_1"] = "Ознайомтеся з умовами та угодою, встановіть галочку і натисніть кнопку Зареєструвати «Register».";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_7_OF_10_DESCRIPTION_1"] = "На стороні Microsoft створено бот, залишилося його опублікувати.
Перевірте всі дані, якщо всі зазначено коректно, натисніть кнопку Опублікувати (Publish)";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_8_OF_10_DESCRIPTION_1"] = "Залишилося відправити бота на перевірку. Активуйте опцію публікації
вашого бота в списку інших ботів Microsoft, якщо хочете щоб
він з'явився в загальному списку і натисніть кнопку Відправити на перевірку (Submit for review)";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_9_OF_10_DESCRIPTION_1"] = "Переконайтеся, що бот відправлений на перевірку, повинен бути статус In review";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_9_OF_10_DESCRIPTION_2"] = "Зверніть увагу, що посилання на підключені додатки до вашого боту, можна отримати лише на цій сторінці";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_STEP_9_OF_10_DESCRIPTION_3"] = "Skype і веб-чат від Microsoft вже підключені, додаткових налаштувань не вимагають.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SIMPLE_FORM_DESCRIPTION_1"] = "Якщо ви вже реєстрували бота, то залишилося підключити його до Бітрікс24<br><br>
На стороні налаштувань бота на сайті Microsoft в поле Messaging Webhook вкажіть наступну адресу:";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_SIMPLE_FORM_DESCRIPTION_TESTED"] = "Ви можете перевірити коректність введених даних.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_COPY"] = "Копіювати";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_DESCRIPTION"] = "Ви можете вказати публічні посилання на канали, які ви підключили на стороні Microsoft, щоб вони відображалися у віджеті (онлайн-чаті) на сайті.
<br><br>
Отримати посилання в #LINK_BEGIN#особистому кабінеті Microsoft Bot Framework#LINK_END# за посиланням \"Get bot embed codes\"";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_TITLE"] = "вказати посилання на канали комунікацій";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_CHANNELS_DESCRIPTION"] = "В #LINK_BEGIN#особистому кабінеті Microsoft Bot Framework#LINK_END# вам необхідно скопіювати посилання на ті канали, які ви хочете показати клієнтам в онлайн-чаті і вставити посилання в поля нижче.
<br><br>
Зверніть увагу, що потрібно копіювати тільки частину посилання. Для кожного каналу ми навели приклади що саме необхідно скопіювати і вставити в поле.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_LINKS_CHANNELS_COMMUNICATION_DESCRIPTION_MASTER"] = "Ви можете вказати публічні посилання на канали, які ви підключили на стороні Microsoft, щоб вони відображалися у віджеті (онлайн-чаті) на сайті в #LINK_BEGIN#формі налаштування каналу#LINK_END#.";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_WEBCHAT_EXAMPLE"] = "Потрібно взяти посилання:<br>
&lt;iframe src=\"<strong>https://webchat.botframework.com/embed/bitrix?s=</strong>YOUR_SECRET_HERE\" style=\"height: 502px; max-height: 502px;\"&gt;&lt;/iframe&gt;<br>А замість YOUR_SECRET_HERE вставити код з поля \"Secret\".";
$MESS["IMCONNECTOR_COMPONENT_BOTFRAMEWORK_URL_EMAILOFFICE365_PLACEHOLDER"] = "pokoev@bitrix.onmicrosoft.com";
?>