<?
$MESS["M_CRM_ACTIVITY_LIST_RUBRIC_LEGEND"] = "(Dossiers)";
$MESS["M_CRM_ACTIVITY_LIST_TIME_NOT_DEFINED"] = "[non indiqué]";
$MESS["M_CRM_ACTIVITY_LIST_IMPORTANT"] = "Important";
$MESS["M_CRM_ACTIVITY_LIST_RUBRIC_FILTER_NONE"] = "Tous";
$MESS["M_CRM_ACTIVITY_LIST_FILTER_NONE"] = "Toutes les affaires";
$MESS["M_CRM_ACTIVITY_LIST_NOTHING_FOUND"] = "Affaires introuvables.";
$MESS["M_CRM_ACTIVITY_LIST_LOAD_TEXT"] = "Mise à jour...";
$MESS["M_CRM_ACTIVITY_LIST_DOWN_TEXT"] = "Relâcher pour mettre à jour...";
$MESS["M_CRM_ACTIVITY_LIST_SEARCH_BUTTON"] = "Recherche";
$MESS["M_CRM_ACTIVITY_LIST_SEARCH_PLACEHOLDER"] = "Recherche par la dénomination";
$MESS["M_CRM_ACTIVITY_LIST_PULL_TEXT"] = "Tirer pour mettre à jour...";
$MESS["M_CRM_ACTIVITY_LIST_FILTER_CUSTOM"] = "Résultats de recherche";
$MESS["M_CRM_ACTIVITY_LIST_CREATE_MEETING"] = "Ajouter une rencontre";
$MESS["M_CRM_ACTIVITY_LIST_CREATE_CALL"] = "Ajouter un appel";
$MESS["M_CRM_ACTIVITY_LIST_CREATE_EMAIL"] = "Créer une lettre";
?>