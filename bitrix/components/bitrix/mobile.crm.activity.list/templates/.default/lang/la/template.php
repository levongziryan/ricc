<?
$MESS["M_CRM_ACTIVITY_LIST_PULL_TEXT"] = "Pulsar hacia abajo para actualizar...";
$MESS["M_CRM_ACTIVITY_LIST_DOWN_TEXT"] = "Soltar para actualizar...";
$MESS["M_CRM_ACTIVITY_LIST_LOAD_TEXT"] = "Actualizando...";
$MESS["M_CRM_ACTIVITY_LIST_SEARCH_PLACEHOLDER"] = "Búsqueda por nombre";
$MESS["M_CRM_ACTIVITY_LIST_SEARCH_BUTTON"] = "Búsqueda";
$MESS["M_CRM_ACTIVITY_LIST_FILTER_NONE"] = "Todas las actividades";
$MESS["M_CRM_ACTIVITY_LIST_FILTER_CUSTOM"] = "Resultados de búsqueda";
$MESS["M_CRM_ACTIVITY_LIST_IMPORTANT"] = "Importante";
$MESS["M_CRM_ACTIVITY_LIST_RUBRIC_FILTER_NONE"] = "Todos";
$MESS["M_CRM_ACTIVITY_LIST_RUBRIC_LEGEND"] = "(Actividades)";
$MESS["M_CRM_ACTIVITY_LIST_TIME_NOT_DEFINED"] = "[No especificado]";
$MESS["M_CRM_ACTIVITY_LIST_CREATE_CALL"] = "Nueva llamada";
$MESS["M_CRM_ACTIVITY_LIST_CREATE_MEETING"] = "Nueva reunión";
$MESS["M_CRM_ACTIVITY_LIST_CREATE_EMAIL"] = "Nuevo correo electrónico";
$MESS["M_CRM_ACTIVITY_LIST_NOTHING_FOUND"] = "No hay actividades encontradas.";
?>