<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_MY"] = "Minhas atividades";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_MY_NOT_COMPLETED"] = "Minhas atividades incompletas";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_MY_COMPLETED"] = "Minhas atividades concluídas";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_NOT_COMPLETED"] = "Incompleto";
$MESS["M_CRM_ACTIVITY_LIST_PRESET_COMPLETED"] = "Concluído";
?>