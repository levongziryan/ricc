<?
$MESS["CRM_COLUMN_STAGE_ID"] = "Fase do negócio";
$MESS["CRM_COLUMN_PRODUCT_ID"] = "Produto";
$MESS["CRM_COLUMN_OPPORTUNITY"] = "Montante";
$MESS["CRM_COLUMN_PROBABILITY"] = "Probabilidade";
$MESS["CRM_COLUMN_DATE_MODIFY"] = "Modificada em";
$MESS["CRM_COLUMN_COMPANY_TITLE"] = "Empresa";
$MESS["CRM_COLUMN_CONTACT_FULL_NAME"] = "Contato";
$MESS["CRM_OPER_SHOW"] = "Visualizar";
$MESS["CRM_OPER_EDIT"] = "Editar";
$MESS["CRM_COLUMN_PRODUCTS"] = "Produto";
?>