<?
$MESS["VOX_IVR_FILE_UPLOAD_ERROR"] = "Error de carga de archivos.";
$MESS["VOX_IVR_FILE_TOO_LARGE"] = "El tamaño del archivo excede el valor máximo posible.";
$MESS["VOX_IVR_EDIT_ERROR_IVR_NOT_AVAILABLE"] = "El menú de voz no está disponible en su plan actual.";
$MESS["VOX_IVR_EDIT_ERROR_IVR_DEPTH_TOO_LARGE"] = "El nivel IVR de anidación supera el límite.";
?>