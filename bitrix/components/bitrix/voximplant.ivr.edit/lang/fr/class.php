<?
$MESS["VOX_IVR_FILE_UPLOAD_ERROR"] = "Erreur de chargement du fichier.";
$MESS["VOX_IVR_FILE_TOO_LARGE"] = "La taille du fichier dépasse la valeur maximale possible.";
$MESS["VOX_IVR_EDIT_ERROR_IVR_NOT_AVAILABLE"] = "Le menu vocal n'est pas disponible dans votre abonnement actuel.";
$MESS["VOX_IVR_EDIT_ERROR_IVR_DEPTH_TOO_LARGE"] = "Le niveau d’imbrication SVI dépasse la limite.";
?>