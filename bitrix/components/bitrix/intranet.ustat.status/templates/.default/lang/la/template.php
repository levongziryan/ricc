<?
$MESS["INTRANET_USTAT_RATING_LOADING"] = "Cargando...";
$MESS["INTRANET_USTAT_RATING_COMMON_TAB"] = "Evaluación general";
$MESS["INTRANET_USTAT_RATING_INVOLVE_TAB"] = "No participa";
$MESS["INTRANET_USTAT_WIDGET_TITLE"] = "Pulso de la Compañía";
$MESS["INTRANET_USTAT_WIDGET_LOADING"] = "Cargando...";
$MESS["INTRANET_USTAT_WIDGET_ACTIVITY_HELP"] = "Nivel de actividad actual de la compañía (compuesto de todos los usuarios en la última hora)";
$MESS["INTRANET_USTAT_WIDGET_INVOLVEMENT_HELP"] = "Compromiso actual de los usuarios. Esto muestra el porcentaje de todos los usuarios de hoy en día que han utilizado al menos cuatro herramientas diferentes en la intranet.";
?>