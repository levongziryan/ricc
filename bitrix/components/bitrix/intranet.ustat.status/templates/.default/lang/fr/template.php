<?
$MESS["INTRANET_USTAT_RATING_LOADING"] = "En cours de chargement...";
$MESS["INTRANET_USTAT_RATING_COMMON_TAB"] = "Classement général";
$MESS["INTRANET_USTAT_RATING_INVOLVE_TAB"] = "Non concerné";
$MESS["INTRANET_USTAT_WIDGET_TITLE"] = "Pouls de la société";
$MESS["INTRANET_USTAT_WIDGET_LOADING"] = "En cours de chargement...";
$MESS["INTRANET_USTAT_WIDGET_ACTIVITY_HELP"] = "L'activité de la compagnie à l'heure actuelle (somme des actions de tous les employés durant la dernière heure)";
$MESS["INTRANET_USTAT_WIDGET_INVOLVEMENT_HELP"] = "L'implication courante des employés indique le pourcentage des employés du nombre total des employés qui ont utilisé au moins quatre outils de travail.";
?>