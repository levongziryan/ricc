<?
$MESS["M_FILTER_PULL_TEXT"] = "Pulse hacia abajo para actualizar...";
$MESS["M_FILTER_DOWN_TEXT"] = "Suéltelo para actualizar...";
$MESS["M_FILTER_LOAD_TEXT"] = "Actualización del filtro...";
$MESS["M_FILTER_TITLE"] = "Configurar el filtro";
$MESS["M_FILTER_BUTTON_APPLY"] = "Aplicar";
$MESS["M_FILTER_BUTTON_CANCEL"] = "Cancelar";
?>