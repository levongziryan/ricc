<?
$MESS["interface_filter_no_no_no_1"] = "(non)";
$MESS["interface_filter_today"] = "aujourd'hui";
$MESS["interface_filter_yesterday"] = "hier";
$MESS["interface_filter_week"] = "cette semaine";
$MESS["interface_filter_week_ago"] = "la semaine dernière";
$MESS["interface_filter_month"] = "ce mois-ci";
$MESS["interface_filter_month_ago"] = "le mois dernier";
$MESS["interface_filter_last"] = "récent";
$MESS["interface_filter_exact"] = "exactement";
$MESS["interface_filter_later"] = "après";
$MESS["interface_filter_earlier"] = "avant";
$MESS["interface_filter_interval"] = "plage de date";
?>