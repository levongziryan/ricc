<?
$MESS["WDUF_CREATE_DOCX"] = "Document";
$MESS["WDUF_SELECT_ATTACHMENTS"] = "Télécharger le fichier ou l'image";
$MESS["WD_SELECT_FILE_LINK"] = "Trouver dans Bitrix24";
$MESS["WD_SELECT_FILE_LINK_ALT"] = "Bibliothèques disponibles";
$MESS["WDUF_DROP_ATTACHMENTS"] = "Déplacer à l'aide du drag'n'drop";
$MESS["WDUF_CREATE_PPTX"] = "Présentation";
$MESS["WDUF_CREATE_IN_SERVICE"] = "Créer à l'aide de #SERVICE#";
$MESS["WDUF_CREATE_XLSX"] = "Table";
?>