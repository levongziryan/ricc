<?
$MESS["WD_FILE_EXISTS"] = "Le fichier du même nom existe déjà. Vous pouvez choisir le dossier en cours, dans ce cas l'ancienne version du fichier sera sauvegardée dans l'historique du document.";
$MESS["WDUF_UPLOAD_DOCUMENT"] = "Charger les documents";
$MESS["WD_FILE_LOADING"] = "Chargement";
$MESS["WDUF_ATTACHMENTS"] = "Images et fichiers joints";
$MESS["WDUF_FILE_EDIT_BY_DESTINATION_USERS"] = "Autoriser l'édition des documents par les destinataires du message?";
$MESS["WD_FILE_UPLOAD_ERROR"] = "Erreur inconnue lors du téléchargement.";
?>