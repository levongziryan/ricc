<?
$MESS["WD_FILE_LOADING"] = "Cargando";
$MESS["WD_FILE_EXISTS"] = "Un archivo con este nombre ya existe. Puede seguir utilizando la carpeta actual, en cuyo caso la versión existente del documento se guardará en el historial.";
$MESS["WDUF_UPLOAD_DOCUMENT"] = "Cargar documentos";
$MESS["WDUF_ATTACHMENTS"] = "Archivos adjuntos e imágenes";
$MESS["WDUF_FILE_EDIT_BY_DESTINATION_USERS"] = "¿Permitir a un destinatario editar los documentos?";
$MESS["WD_FILE_UPLOAD_ERROR"] = "Error desconocido durante el proceso de carga.";
?>