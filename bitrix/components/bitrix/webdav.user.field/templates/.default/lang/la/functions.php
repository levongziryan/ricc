<?
$MESS["WDUF_SELECT_ATTACHMENTS"] = "Subir archivos e imágenes";
$MESS["WDUF_DROP_ATTACHMENTS"] = "Arrastrar archivos adjuntos aquí";
$MESS["WD_SELECT_FILE_LINK"] = "Seleccione el documento de Bitrix24";
$MESS["WD_SELECT_FILE_LINK_ALT"] = "Bibliotecas disponibles";
$MESS["WDUF_CREATE_DOCX"] = "Documentos";
$MESS["WDUF_CREATE_XLSX"] = "Hoja de cálculo";
$MESS["WDUF_CREATE_PPTX"] = "Presentación";
$MESS["WDUF_CREATE_IN_SERVICE"] = "Crear utilizando #SERVICE#";
?>