<?
$MESS["WD_SAVED_PATH"] = "Guardado";
$MESS["WD_LOCAL_COPY_ONLY"] = "En este mensaje";
$MESS["WD_MY_LIBRARY"] = "My Drive";
$MESS["WDUF_ATTACHED_TO_MESSAGE"] = "El archivo se ha colocado en el mensaje.";
$MESS["SONET_GROUP_PREFIX"] = "Grupo: ";
$MESS["WDUF_PICKUP_ATTACHMENTS"] = "Seleccione el archivo desde el equipo local";
$MESS["WDUF_FILE_DOWNLOAD"] = "Descargar";
$MESS["WDUF_FILE_PREVIEW"] = "Ver";
$MESS["WDUF_FILE_REVISION_HISTORY"] = "Revisión del historial";
$MESS["WDUF_FILE_ONLINE_EDIT_IN_SERVICE"] = "Actualmente se está editando en #SERVICE#";
$MESS["WDUF_FILE_EDIT"] = "Editar";
$MESS["WDUF_HISTORY_FILE"] = "Versión #NUMBER#:";
?>