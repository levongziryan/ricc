<?
$MESS["INTRANET_OTP_MANDATORY_DESCR"] = "<p>Aujourd'hui, votre Bitrix24 est protégé par la technologie de cryptage de données et une paire de logins et mots de passe pour chaque utilisateur. Cependant, il
sont des outils qui un utilisateur malveillant peut utiliser pour pénétrer dans votre ordinateur et de voler ces données.</p>
<p>Votre administrateur système a activé l'option de sécurité supplémentaire et est maintenant demander
vous activez l'authentification en deux étape.</p>
<p>L'authentification à deux étapes signifie que vous aurez à passer deux niveaux de
la vérification lors de la connexion. D'abord, vous entrez votre mot de passe. Ensuite, vous devrez
Entrez un code de sécurité unique envoyé à votre appareil mobile.</p>
<p>Cela rendra vos affaires et de données plus sûres.</p>";
$MESS["INTRANET_OTP_MANDATORY_DESCR2"] = "<p>Vous avez #NUM#. Vous ne serez pas en mesure de vous connecter à votre Bitrix24 si vous ne pas activer l'authentification en deux étapes jusque-là.</p>";
$MESS["INTRANET_OTP_PASS"] = "La première ligne de défense: votre mot de passe";
$MESS["INTRANET_OTP_CODE"] = "La deuxième ligne de défense: le code de vérification";
$MESS["INTRANET_OTP_MANDATORY_TITLE"] = "Une sécurité supplémentaire pour les données de votre entreprise";
$MESS["INTRANET_OTP_CLOSE"] = "Rappele moi plus tard";
$MESS["INTRANET_OTP_GOTO"] = "Commencer";
$MESS["INTRANET_OTP_CLOSE_FOREVER"] = "Ne pas afficher de nouveau";
?>