<?
$MESS["INTRANET_OTP_PASS"] = "A primeira linha de defesa: sua senha";
$MESS["INTRANET_OTP_CODE"] = "A segunda linha de defesa: código de verificação";
$MESS["INTRANET_OTP_GOTO"] = "Iniciar";
$MESS["INTRANET_OTP_CLOSE"] = "Lembre-me depois";
$MESS["INTRANET_OTP_CLOSE_FOREVER"] = "Não mostrar novamente";
$MESS["INTRANET_OTP_MANDATORY_TITLE"] = "Segurança extra para seus dados de negócio";
$MESS["INTRANET_OTP_MANDATORY_DESCR"] = "<p>Hoje, seu Bitrix24 está protegido por tecnologia de criptografia de dados e vários logins e senhas para cada usuário. No entanto, há ferramentas que um usuário mal intencionado pode utilizar para entrar no seu computador e roubar esses dados</p>
<p>Seu administrador de sistema ativou opção de segurança extra e, agora, está pedindo a você para ativar a autenticação em dois passos.</p> <p>A autenticação em dois passos significa que você terá que passar por dois níveis de verificação quando fizer login. Primeiro, você vai digitar sua senha. Depois, você terá que digitar um código de segurança único enviado ao seu dispositivo móvel.</p> <p>Isso tornará seus dados de negócio mais seguro.</p>";
$MESS["INTRANET_OTP_MANDATORY_DESCR2"] = "<p>Você tem #NUM#. Você não poderá fazer login no seu Bitrix24 se você não ativar a autenticação de dois passos até lá.</p>";
?>