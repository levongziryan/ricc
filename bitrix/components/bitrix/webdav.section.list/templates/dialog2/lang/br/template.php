<?
$MESS["WD_TITLE_FILE_SIZE"] = "Tamanho";
$MESS["WD_TITLE_NAME"] = "Nome";
$MESS["WD_TITLE_MODIFIED_BY"] = "Modificado por";
$MESS["WD_TITLE_TIMESTAMP"] = "Modificado em'";
$MESS["WD_SAVE_DOCUMENT_TITLE"] = "Selecione uma pasta para guardar o documento";
$MESS["WD_SELECT_DOCUMENT_TITLE"] = "Selecione um ou mais documentos";
$MESS["WD_TITLE_SELECT"] = "Selecione documento";
$MESS["WD_SELECT_FOLDER"] = "Selecione a pasta atual";
$MESS["WD_SELECT_DOCUMENT"] = "Selecione documento";
$MESS["WD_CANCEL"] = "Cancelar";
$MESS["WD_MY_DOCUMENTS"] = "Meus documentos";
$MESS["WD_SHARED_DOCUMENTS"] = "Documentos compartilhados";
$MESS["WD_DESCR_DISABLE_ATTACH_NON_PUBLIC_FILE"] = "O documento não foi publicado. Execute a publicação de documentos de processos de negócios.";
$MESS["WD_MY_GROUPS"] = "Grupos";
$MESS["WD_IBLOCK_TYPE"] = "Tipo Bloco de Informações";
$MESS["RATING_TYPE_LIKE_TEXT"] = "Curtir (texto)";
$MESS["WD_FILE"] = "Arquivo";
$MESS["WD_FILE_COMMENTS"] = "Comentarios";
$MESS["WD_MODULE"] = "Biblioteca de Documentos";
$MESS["T_IBLOCK_DESC_ACTIVE_DATE_FORMAT"] = "Formato de exibição de data";
$MESS["T_IBLOCK_DESC_PAGER_DOCS"] = "Documentos";
$MESS["T_IBLOCK_DESC_LIST_CONT"] = "Documentos por página";
$MESS["T_IBLOCK_DESC_HIDE_LINK_WHEN_NO_DETAIL"] = "Ocultar link para a página de detalhes, somente se nenhuma descrição é detalhada";
$MESS["T_IBLOCK_DESC_INCLUDE_IBLOCK_INTO_CHAIN"] = "Incluir blocos de informações na cadeia de navegação";
$MESS["WD_GO_BACK_ALT"] = "Voltar";
?>