<?
$MESS["WD_TITLE_FILE_SIZE"] = "Rempli";
$MESS["WD_TITLE_NAME"] = "Dénomination";
$MESS["WD_TITLE_MODIFIED_BY"] = "Modifié par";
$MESS["WD_TITLE_TIMESTAMP"] = "Modifié";
$MESS["WD_SAVE_DOCUMENT_TITLE"] = "Sélectionnez un dossier pour enregistrer le document à";
$MESS["WD_SELECT_DOCUMENT_TITLE"] = "Choisissez un ou quelques documents";
$MESS["WD_TITLE_SELECT"] = "Choisissez le document";
$MESS["WD_SELECT_FOLDER"] = "Choisir le dossier courant";
$MESS["WD_SELECT_DOCUMENT"] = "Choisissez le document";
$MESS["WD_CANCEL"] = "Annuler";
$MESS["WD_MY_DOCUMENTS"] = "Mon Drive";
$MESS["WD_SHARED_DOCUMENTS"] = "Drive Entreprise";
$MESS["WD_DESCR_DISABLE_ATTACH_NON_PUBLIC_FILE"] = "Le document n'a pas été publié. Lancez le processus d'affaires de publication du document.";
$MESS["WD_MY_GROUPS"] = "Disques des groupes";
?>