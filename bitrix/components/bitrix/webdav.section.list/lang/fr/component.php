<?
$MESS["WD_VERSIONS_COUNT"] = "#NUM# #VERSIONS#";
$MESS["WD_NAME_VERSION"] = "(version)";
$MESS["WD_TITLE"] = "Libye";
$MESS["IBLIST_BP"] = "De processus business";
$MESS["WD_GO_BACK_ALT"] = "Précédent";
$MESS["WD_VERSIONS_MOD_2_4"] = "version";
$MESS["WD_VERSIONS_0"] = "de versions";
$MESS["WD_VERSIONS_10_20"] = "de versions";
$MESS["WD_VERSIONS_MOD_OTHER"] = "de versions";
$MESS["WD_VERSIONS_1"] = "version";
$MESS["WD_VERSIONS_MOD_1"] = "version";
$MESS["IBLOCK_RED_ALT"] = "bloquée temporairement (en édition à présent)";
$MESS["WD_TITLE_TIMESTAMP"] = "Date de modification";
$MESS["WD_TITLE_ADMIN_DCREATE"] = "Créé le";
$MESS["WD_DOCUMENTS"] = "Documents";
$MESS["WD_DOCTYPE_DOCUMENTS"] = "Documents";
$MESS["WD_ACCESS_DENIED"] = "Accès interdit.";
$MESS["IBLOCK_GREEN_ALT"] = "accessible pour la modification";
$MESS["IBLOCK_YELLOW_ALT"] = "bloquée par vous (n'oubliez pas de 'libérer')";
$MESS["WD_DOCTYPE_IMAGES"] = "Images";
$MESS["WD_WHEN"] = "Date de la dernière modification";
$MESS["WD_WHO"] = "Changé par";
$MESS["WD_TITLE_MODIFIED_BY"] = "Modifié(e)s par";
$MESS["WD_TITLE_ADMIN_WCREATE2"] = "Créé par";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "Le module de la bibliothèque des documents n'a pas été installé.";
$MESS["WD_DOCTYPE_MEDIA"] = "Contenu multimédia";
$MESS["WD_GO_BACK"] = "Précédent";
$MESS["WD_TITLE_NAME"] = "Titre ou texte du document";
$MESS["WD_TITLE_CONTENT"] = "Titre ou texte du document";
$MESS["WD_STATUS_NOT_PUBLISHED"] = "N'est pas publié";
$MESS["WD_DOCSTATUS_GREEN"] = "Non modifiable";
$MESS["WD_TITLE_DESCRIPTION"] = "Description";
$MESS["WD_STATUS_PUBLISHED"] = "Année de publication";
$MESS["WD_UNSUBSCRIBE"] = "Se désabonner";
$MESS["WD_UNSUBSCRIBE_ELEMENT"] = "Recevoir les commentaires sur ce document par e-mail";
$MESS["WD_SUBSCRIBE"] = "Abonnement";
$MESS["WD_SUBSCRIBE_ELEMENT"] = "Recevoir les commentaires sur ce document par e-mail";
$MESS["WD_TITLE_FILE_SIZE"] = "Rempli";
$MESS["WD_DOCSTATUS_RED"] = "En cours de rédaction par quelqu'un";
$MESS["WD_DOCSTATUS_YELLOW"] = "Dité par moi";
$MESS["WD_LOCK_STATUS"] = "Tat du document";
$MESS["WD_TITLE_TAGS"] = "Tags";
$MESS["WD_DOCTYPE"] = "Type du document";
?>