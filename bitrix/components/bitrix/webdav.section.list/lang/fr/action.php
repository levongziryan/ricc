<?
$MESS["WD_ERROR_BAD_SESSID"] = "Votre session a expiré. Répétez cette opération encore une fois, s'il vous plaît.";
$MESS["WD_ACCESS_DENIED"] = "Accès interdit.";
$MESS["WD_ERROR_EMPTY_DATA"] = "Aucun dossier/document sélectionné.";
$MESS["WD_ERROR_BAD_ACTION"] = "L'action avec le dossier n'est pas indiquée.";
$MESS["WD_ERROR_EMPTY_ACTION"] = "L'action disponible n'est pas indiquée.";
$MESS["WD_ERROR_EMPTY_NAME"] = "Le nouveau nom pour le document ou le dossier n'est pas indiqué.";
$MESS["WD_ERROR_UNDELETE"] = "Erreur de restauration.";
$MESS["WD_ERROR_DELETE"] = "Erreur lors de suppression.";
$MESS["WD_ERROR_EMPTY_TARGET_SECTION"] = "Le dossier de destination n'est pas précisé.";
?>