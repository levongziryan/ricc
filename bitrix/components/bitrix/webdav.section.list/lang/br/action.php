<?
$MESS["WD_ACCESS_DENIED"] = "Acesso negado.";
$MESS["WD_ERROR_DELETE"] = "Não é possível excluir.";
$MESS["WD_ERROR_EMPTY_ACTION"] = "Nenhuma ação especificada.";
$MESS["WD_ERROR_EMPTY_DATA"] = "Nenhum arquivo ou pasta selecionada.";
$MESS["WD_ERROR_EMPTY_TARGET_SECTION"] = "A pasta de destino não especificado.";
$MESS["WD_ERROR_BAD_ACTION"] = "A ação da pasta não especificado.";
$MESS["WD_ERROR_BAD_SESSID"] = "Sua sessão expirou. Por favor, repita a operação.";
$MESS["WD_ERROR_EMPTY_NAME"] = "Novo documento ou nome da pasta não especificado.";
$MESS["WD_ERROR_UNDELETE"] = "Recuperação de erros.";
$MESS["W_TITLE_EXTERNAL_LOCK_WHEN"] = "Bloquear data";
$MESS["W_TITLE_SORT"] = "Ordenar.";
$MESS["WD_HIST_ELEMENT"] = "História";
$MESS["WD_Y"] = "Sim";
$MESS["PAGE_ELEMENTS"] = "Elementos por página";
$MESS["WD_PERMISSION"] = "Permissão de acesso externo (uso em componentes compostos)";
$MESS["WD_ERROR_DOUBLE_NAME_TITLE"] = "Arquivo de upload de erro \"#NAME#\" com o título \"# TITLE #\". Um arquivo com esse título \"#TITLE#' já existe.";
$MESS["WD_TITLE_NAME"] = "Nome";
$MESS["AJAX_OPTION_STYLE_TIP"] = "Especificar para baixar e processar estilos CSS de componente em transição AJAX.";
?>