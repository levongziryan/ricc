<?
$MESS["NEXT_EXECUTION_DEAL_HINT"] = "La prochaine transaction sera créée le #DATE_EXECUTION#";
$MESS["NEXT_DEAL_EMPTY"] = "Rien n'est sélectionné";
$MESS["NEXT_BASED_ON_DEAL_ONCE"] = "Un modèle a été créé pour la transaction récurrente ##ID# ";
$MESS["SIGN"] = "#";
$MESS["NEXT_BASED_ON_DEAL_MULTY"] = "Un modèle a été créé pour des transactions récurrentes : #ID_LINE# ";
$MESS["SIGN_NUM_WITH_DEAL_ID"] = "##DEAL_ID#";
?>