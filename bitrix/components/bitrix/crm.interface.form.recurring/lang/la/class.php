<?
$MESS["CRM_RECURRING_FILTER_DAILY"] = "día";
$MESS["CRM_RECURRING_FILTER_WEEKLY"] = "semana";
$MESS["CRM_RECURRING_FILTER_MONTHLY"] = "mes";
$MESS["CRM_RECURRING_FILTER_YEARLY"] = "año";
$MESS["CRM_RECURRING_FILTER_END_CONSTRAINT_TIMES_PLURAL_0"] = "repetición";
$MESS["CRM_RECURRING_FILTER_END_CONSTRAINT_TIMES_PLURAL_1"] = "repetición";
$MESS["CRM_RECURRING_FILTER_END_CONSTRAINT_TIMES_PLURAL_2"] = "repetición";
$MESS["CRM_RECURRING_FILTER_DAILY_WORK"] = "día de trabajo";
$MESS["CRM_RECURRING_FILTER_DAILY_ANY"] = "calendario";
$MESS["CRM_RECURRING_FILTER_EACH_M"] = "cada";
$MESS["CRM_RECURRING_FILTER_EACH_M_ALT"] = "cada";
$MESS["CRM_RECURRING_FILTER_EACH_F"] = "cada";
$MESS["CRM_RECURRING_FILTER_EACH"] = "cada";
$MESS["CRM_RECURRING_FILTER_REPEAT_TYPE"] = "repetir tipo";
$MESS["CRM_RECURRING_FILTER_DAY_INTERVAL"] = "día";
$MESS["CRM_RECURRING_FILTER_MONTH_SHORT"] = "mes.";
$MESS["CRM_RECURRING_FILTER_WEEK_ALT"] = "semana";
$MESS["CRM_RECURRING_FILTER_MONTH_ALT"] = "meses";
$MESS["CRM_RECURRING_FILTER_NUMBER_OF_EACH_M_ALT"] = "día de cada";
$MESS["CRM_RECURRING_FILTER_DAY_OF_MONTH"] = "día";
$MESS["CRM_RECURRING_FILTER_REPEAT_START"] = "Hacer recurrentes después";
$MESS["CRM_RECURRING_FILTER_REPEAT_END"] = "Repetir hasta";
$MESS["CRM_RECURRING_FILTER_REPEAT_END_C_DATE"] = "fecha final";
$MESS["CRM_RECURRING_FILTER_REPEAT_END_C_NONE"] = "sin fecha de finalización";
$MESS["CRM_RECURRING_FILTER_REPEAT_END_C_TIMES"] = "completar despues";
$MESS["CRM_RECURRING_FILTER_REPEATS"] = "repetición";
$MESS["CRM_RECURRING_FILTER_WD_SH_1"] = "Lu";
$MESS["CRM_RECURRING_FILTER_WD_SH_2"] = "Ma";
$MESS["CRM_RECURRING_FILTER_WD_SH_3"] = "Mié";
$MESS["CRM_RECURRING_FILTER_WD_SH_4"] = "Ju";
$MESS["CRM_RECURRING_FILTER_WD_SH_5"] = "Vi";
$MESS["CRM_RECURRING_FILTER_WD_SH_6"] = "Sáb";
$MESS["CRM_RECURRING_FILTER_WD_SH_7"] = "Dom";
$MESS["CRM_RECURRING_FILTER_MONTH_1"] = "Enero";
$MESS["CRM_RECURRING_FILTER_MONTH_2"] = "Febrero";
$MESS["CRM_RECURRING_FILTER_MONTH_3"] = "Marzo";
$MESS["CRM_RECURRING_FILTER_MONTH_4"] = "Abril";
$MESS["CRM_RECURRING_FILTER_MONTH_5"] = "Mayo";
$MESS["CRM_RECURRING_FILTER_MONTH_6"] = "Junio";
$MESS["CRM_RECURRING_FILTER_MONTH_7"] = "Julio";
$MESS["CRM_RECURRING_FILTER_MONTH_8"] = "Agosto";
$MESS["CRM_RECURRING_FILTER_MONTH_9"] = "Septiembre";
$MESS["CRM_RECURRING_FILTER_MONTH_10"] = "Octubre";
$MESS["CRM_RECURRING_FILTER_MONTH_11"] = "Noviembre";
$MESS["CRM_RECURRING_FILTER_MONTH_12"] = "Diciembre";
$MESS["CRM_RECURRING_FILTER_WD_1"] = "Lunes";
$MESS["CRM_RECURRING_FILTER_WD_2"] = "Martes";
$MESS["CRM_RECURRING_FILTER_WD_3"] = "Miércoles";
$MESS["CRM_RECURRING_FILTER_WD_4"] = "Jueves";
$MESS["CRM_RECURRING_FILTER_WD_5"] = "Viernes";
$MESS["CRM_RECURRING_FILTER_WD_6"] = "Sábado";
$MESS["CRM_RECURRING_FILTER_WD_7"] = "Domingo";
$MESS["CRM_RECURRING_FILTER_NUMBER_0"] = "primero";
$MESS["CRM_RECURRING_FILTER_NUMBER_1"] = "segundo";
$MESS["CRM_RECURRING_FILTER_NUMBER_2"] = "tercero";
$MESS["CRM_RECURRING_FILTER_NUMBER_3"] = "cuarto";
$MESS["CRM_RECURRING_FILTER_NUMBER_4"] = "último";
$MESS["CRM_RECURRING_FILTER_NUMBER_0_M"] = "primero";
$MESS["CRM_RECURRING_FILTER_NUMBER_0_F"] = "primero";
$MESS["CRM_RECURRING_FILTER_NUMBER_1_M"] = "segundo";
$MESS["CRM_RECURRING_FILTER_NUMBER_1_F"] = "segundo";
$MESS["CRM_RECURRING_FILTER_NUMBER_2_M"] = "tercero";
$MESS["CRM_RECURRING_FILTER_NUMBER_2_F"] = "tercero";
$MESS["CRM_RECURRING_FILTER_NUMBER_3_M"] = "cuarto";
$MESS["CRM_RECURRING_FILTER_NUMBER_3_F"] = "cuarto";
$MESS["CRM_RECURRING_FILTER_NUMBER_4_M"] = "último";
$MESS["CRM_RECURRING_FILTER_NUMBER_4_F"] = "último";
$MESS["CRM_RECURRING_TEMPLATE_WILL_BE_CREATED"] = "¡Atención! Se creará una nueva plantilla basada en esta factura con los parámetros de repetición especificados.";
$MESS["CRM_RECURRING_SWITCHER_BLOCK"] = "Realice una factura recurrente";
$MESS["CRM_RECURRING_EMAIL_LABEL"] = "enviar factura al correo electrónico del cliente";
$MESS["CRM_RECURRING_EMAIL_TEMPLATE"] = "plantilla de mensaje";
$MESS["CRM_RECURRING_CREATE_NEW"] = "crear nuevo";
$MESS["CRM_RECURRING_HINT_BASE"] = "La factura se creará #ELEMENT##START##END#.";
$MESS["CRM_RECURRING_HINT_START_DATE"] = ", empezando desde <nobr>#DATETIME#</nobr>";
$MESS["CRM_RECURRING_HINT_START_EMPTY"] = "después de que se guarda";
$MESS["CRM_RECURRING_HINT_END"] = " terminando en <nobr>#DATETIME#</nobr>";
$MESS["CRM_RECURRING_HINT_END_NONE"] = ", sin fecha de finalización";
$MESS["CRM_RECURRING_HINT_END_TIMES"] = ", y terminará en <nobr>#TIMES# #TIMES_PLURAL#</nobr>";
$MESS["CRM_RECURRING_HINT_END_CONSTRAINT_TIMES_PLURAL_0"] = "repetición";
$MESS["CRM_RECURRING_HINT_END_CONSTRAINT_TIMES_PLURAL_1"] = "repetición";
$MESS["CRM_RECURRING_HINT_END_CONSTRAINT_TIMES_PLURAL_2"] = "repetición";
$MESS["CRM_RECURRING_HINT_ELEMENT_DAY_MASK"] = "cada #DAY_NUMBER#día";
$MESS["CRM_RECURRING_HINT_ELEMENT_DAY_MASK_WORK"] = "cada #DAY_NUMBER#día de trabajo";
$MESS["CRM_RECURRING_HINT_ELEMENT_WEEKDAY_MASK"] = "cada #WEEK_NUMBER#semana (#LIST_WEEKDAY_NAMES#)";
$MESS["CRM_RECURRING_HINT_WEEKDAY_EVERY_DAY"] = "diariamente";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_1"] = "Lunes";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_2"] = "Martes";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_3"] = "Miércoles";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_4"] = "Jueves";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_5"] = "Viernes";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_6"] = "Sábado";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_7"] = "Domingo";
$MESS["CRM_RECURRING_HINT_ELEMENT_MONTH_MASK_1"] = "cada #DAY_NUMBER# día de cada #MONTH_NUMBER#mes";
$MESS["CRM_RECURRING_HINT_ELEMENT_MONTH_MASK_1_WORK"] = "cada #DAY_NUMBER# día laboral de cada #MONTH_NUMBER#mes";
$MESS["CRM_RECURRING_HINT_MONTHLY_EXT_TYPE_2"] = "#EACH# #WEEKDAY_NUMBER# #WEEKDAY_NAME# de cada #MONTH_NUMBER#mes";
$MESS["CRM_RECURRING_HINT_YEARLY_EXT_TYPE_1"] = "cada #DAY_NUMBER# día de #MONTH_NAME#";
$MESS["CRM_RECURRING_HINT_YEARLY_EXT_TYPE_1_WORKDAY"] = "cada #DAY_NUMBER# día laboral de #MONTH_NAME#";
$MESS["CRM_RECURRING_HINT_YEARLY_EXT_TYPE_2"] = "#EACH# #WEEKDAY_NUMBER# #WEEKDAY_NAME# de #MONTH_NAME#";
$MESS["CRM_RECURRING_HINT_EACH"] = "cada";
$MESS["CRM_RECURRING_HINT_EACH_M"] = "cada";
$MESS["CRM_RECURRING_HINT_EACH_M_ALT"] = "cada";
$MESS["CRM_RECURRING_HINT_EACH_F"] = "cada";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_0"] = "primero";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_1"] = "segundo";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_2"] = "tercero";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_3"] = "cuarto";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_4"] = "último";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_0_M"] = "primero";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_0_F"] = "primero";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_1_M"] = "segundo";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_1_F"] = "segundo";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_2_M"] = "tercero";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_2_F"] = "tercero";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_3_M"] = "cuarto";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_3_F"] = "cuarto";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_4_M"] = "último";
$MESS["CRM_RECURRING_HINT_WEEKDAY_NUMBER_4_F"] = "último";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_3_ALT"] = "Miércoles";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_5_ALT"] = "Viernes";
$MESS["CRM_RECURRING_HINT_WEEKDAY_WD_6_ALT"] = "Sábado";
$MESS["CRM_RECURRING_HINT_MONTH_1"] = "Enero";
$MESS["CRM_RECURRING_HINT_MONTH_2"] = "Febrero";
$MESS["CRM_RECURRING_HINT_MONTH_3"] = "Marzo";
$MESS["CRM_RECURRING_HINT_MONTH_4"] = "Abril";
$MESS["CRM_RECURRING_HINT_MONTH_5"] = "Mayo";
$MESS["CRM_RECURRING_HINT_MONTH_6"] = "Junio";
$MESS["CRM_RECURRING_HINT_MONTH_7"] = "Julio";
$MESS["CRM_RECURRING_HINT_MONTH_8"] = "Agosto";
$MESS["CRM_RECURRING_HINT_MONTH_9"] = "Septiembre";
$MESS["CRM_RECURRING_HINT_MONTH_10"] = "Octubre";
$MESS["CRM_RECURRING_HINT_MONTH_11"] = "Noviembre";
$MESS["CRM_RECURRING_HINT_MONTH_12"] = "Diciembre";
$MESS["CRM_RECURRING_INVOICE_B24_BLOCK_TEXT"] = "Use la característica de facturación automática para facturar a sus clientes regularmente y ahorrarle tiempo.
	Cree una factura y especifique con qué frecuencia desea que se envíe en el futuro. El sistema creará y enviará una nueva factura al cliente de acuerdo con sus preferencias de tiempo.
		<ul class=\"hide-features-list\">
			<li class=\"hide-features-list-item\">Ahorre tiempo con la facturación automática</li>
			<li class=\"hide-features-list-item\">Acceso rápido a todas las plantillas de facturación automática</li>
			<li class=\"hide-features-list-item\">Enviar factura al correo electrónico del cliente <a href=\"#LINK#\" target=\"_blank\" class=\"ocultar-funciones-más\">Más información</a></li>
		</ul>		
		<strong>La facturación automática está disponible en planes Standard y Professional.</strong>";
$MESS["CRM_RECURRING_INVOICE_B24_BLOCK_TITLE"] = "La facturación automática está disponible en planes Standard y Professional";
$MESS["CRM_RECURRING_EMPTY_OWNER_EMAIL"] = "Correo electrónico de su compañía en los detalles de su compañía (CRM - Configuración - Detalles de mi compañía) está vacío.";
$MESS["NEXT_EXECUTION_HINT"] = "Se creará la siguiente factura #DATE_EXECUTION#";
$MESS["CRM_RECURRING_FILTER_DAYS"] = "días";
$MESS["CRM_RECURRING_FILTER_WEEKS"] = "semanas";
$MESS["CRM_RECURRING_FILTER_MONTHS"] = "meses";
$MESS["CRM_RECURRING_FILTER_DO_REPEAT"] = "repetir";
$MESS["CRM_RECURRING_FILTER_EVERY_DAY"] = "diariamente";
$MESS["CRM_RECURRING_FILTER_EVERY_WEEK"] = "semanal";
$MESS["CRM_RECURRING_FILTER_EVERY_MONTH"] = "mensual";
$MESS["CRM_RECURRING_FILTER_EVERY_YEAR"] = "anual";
$MESS["CRM_RECURRING_FILTER_DEAL_CREATE_BY"] = "crear negociación";
$MESS["CRM_RECURRING_FILTER_BEFORE_DATE"] = "antes de";
$MESS["CRM_RECURRING_INVOICE_SWITCHER_BLOCK"] = "Realice una factura recurrente";
$MESS["CRM_RECURRING_DEAL_SWITCHER_BLOCK"] = "Hacer negociación recurrente";
$MESS["CRM_RECURRING_INVOICE_HINT_BASE"] = "La factura se creará #ELEMENT##START##END#.";
$MESS["CRM_RECURRING_DEAL_HINT_BASE"] = "La negociación se creará #ELEMENT##START##END#.";
$MESS["CRM_RECURRING_HINT_EVERY_DEAL_1"] = "diariamente";
$MESS["CRM_RECURRING_HINT_EVERY_DEAL_2"] = "semanal";
$MESS["CRM_RECURRING_HINT_EVERY_DEAL_3"] = "mensual";
$MESS["CRM_RECURRING_HINT_EVERY_DEAL_4"] = "anual";
$MESS["CRM_RECURRING_HINT_1_PLURAL_2"] = "días";
$MESS["CRM_RECURRING_HINT_1_PLURAL_0"] = "día";
$MESS["CRM_RECURRING_HINT_1_PLURAL_1"] = "días";
$MESS["CRM_RECURRING_HINT_2_PLURAL_2"] = "semanas";
$MESS["CRM_RECURRING_HINT_2_PLURAL_0"] = "semana";
$MESS["CRM_RECURRING_HINT_2_PLURAL_1"] = "semanas";
$MESS["CRM_RECURRING_HINT_3_PLURAL_2"] = "meses";
$MESS["CRM_RECURRING_HINT_3_PLURAL_0"] = "mes";
$MESS["CRM_RECURRING_HINT_3_PLURAL_1"] = "meses";
$MESS["CRM_RECURRING_HINT_BEFORE_DATE"] = "hasta #DATE#";
$MESS["CRM_RECURRING_HINT_TODAY"] = "hoy";
$MESS["CRM_RECURRING_HINT_A_FEW_DAYS_BEFORE_DATE"] = "para #COUNT_ELEMENT# #TYPE_ELEMENT##BEFORE_DATE#";
$MESS["NEXT_EXECUTION_INVOICE_HINT"] = "Siguiente fecha de factura #DATE_EXECUTION#";
$MESS["NEXT_EXECUTION_DEAL_HINT"] = "La próxima negociación se creará en #DATE_EXECUTION#";
$MESS["CRM_RECURRING_FILTER_DEAL_CATEGORISES"] = "Negociación pipeline";
$MESS["NEXT_DEAL_EMPTY"] = "La negociación está inactiva";
$MESS["NEXT_INVOICE_EMPTY"] = "La plantilla de factura está inactiva";
$MESS["CRM_RECURRING_DEAL_TEMPLATE_WILL_BE_CREATED"] = "¡Atención! Se creará una nueva plantilla con los parámetros de tiempo de ejecución especificados en función de la negociación.";
$MESS["CRM_RECURRING_EMPTY_OWNER_EMAIL1"] = "El E-mail falta en los detalles de su compañía.";
$MESS["CRM_RECURRING_FILTER_CREATE_DATE_PAYMENT"] = "Establecer la fecha de vencimiento del pago";
$MESS["CRM_RECURRING_FILTER_UNSET_DATE_PAYMENT"] = "no establecer";
$MESS["CRM_RECURRING_FILTER_DATE_PAYMENT_AFTER_EXPOSING"] = "días después de la fecha de emisión de la factura";
$MESS["CRM_RECURRING_FILTER_DATE_PAYMENT_IN_DATE"] = "en";
$MESS["CRM_RECURRING_DEAL_B24_BLOCK_TEXT"] = "Deje que Bitrix24 cree negociaciones para usted.
	Las negociaciones recurrentes le ahorrarán tiempo y recursos en varias ocasiones. Por ejemplo, si su plan es tener contratos semanales durante el próximo año, las negociaciones recurrentes serán su liberación con certeza.
	Crea una negociación recurrente y establece el cronograma y una canalización. Se crearán nuevas negociaciones a la hora especificada, y no tendrá que mover un dedo.
		<ul class=\"hide-features-list\">
			<li class=\"hide-features-list-item\">Creación automática de negociaciones</li>
			<li class=\"hide-features-list-item\">Mejore el rendimiento y la eficiencia de sus empleados</li>
			<li class=\"hide-features-list-item\">Acceso rápido a las negociaciones recurrentes actuales- <a href=\"#LINK#\" target=\"_blank\" class=\"ocultar-funciones-más\">Más información</a></li>
		</ul>		
		<strong>Las negociaciones recurrentes están disponibles en planes \"standard\" y \"Professional\"</strong>";
$MESS["CRM_RECURRING_DEAL_B24_BLOCK_TITLE"] = "Las negociaciones recurrentes están disponibles en planes \"Standard\" y \"Professional\"";
?>