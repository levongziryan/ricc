<?
$MESS["CRM_VAT_SHOW_TITLE"] = "Ver este impuesto";
$MESS["CRM_VAT_SHOW"] = "Ver impuesto";
$MESS["CRM_VAT_EDIT_TITLE"] = "Abrir este impuesto para la edición";
$MESS["CRM_VAT_EDIT"] = "Editar IVA";
$MESS["CRM_VAT_DELETE_TITLE"] = "Eliminar está tasa del IVA";
$MESS["CRM_VAT_DELETE"] = "Eliminar tasa del IVA";
$MESS["CRM_VAT_DELETE_CONFIRM"] = "Está seguro de que desea eliminar '% s'?";
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_PRODUCT_ROW_TAX_UNIFORM_TITLE"] = "Los impuesto sobre los bienes pueden ser aplicados a toda la lista, es decir, para cada elemento en el documento, o para ninguno de los elementos. Dentro de un mismo documento, no puede haber elementos que no se aplique el impuesto, así como aquellos a los que se aplica el impuesto.";
$MESS["CRM_PRODUCT_ROW_TAX_UNIFORM_ALERT"] = "Si es necesario, puede desactivar esta opción. Asegúrese de que la desactivación de esta opción no viola ninguna ley o regulación con el que usted está obligado a cumplir.";
?>