<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_CATALOG_MODULE_NOT_INSTALLED"] = "El módulo Commercial Catalog no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_C_SORT"] = "Clasificar";
$MESS["CRM_COLUMN_ACTIVE"] = "Activo";
$MESS["CRM_COLUMN_NAME"] = "Nombre";
$MESS["CRM_COLUMN_RATE"] = "Tasa";
$MESS["CRM_VAT_DELETION_GENERAL_ERROR"] = "Error al eliminar el impuesto.";
$MESS["CRM_VAT_UPDATE_GENERAL_ERROR"] = "Error al actualizar el impuesto.";
?>