<?
$MESS["LEARNING_DESC_YES"] = "Oui";
$MESS["LEARNING_DESC_FACT"] = "Date de l'activation";
$MESS["LEARNING_DESC_FTSAMP"] = "Dernière mise à jour";
$MESS["LEARNING_DESC_FID"] = "Identificateur du cours";
$MESS["LEARNING_DESC_FSORT"] = "Classification";
$MESS["LEARNING_DESC_FNAME"] = "Dénomination";
$MESS["LEARNING_DESC_NO"] = "Non";
$MESS["LEARNING_DESC_ASC"] = "Croissant";
$MESS["LEARNING_DESC_DESC"] = "Décroissant";
$MESS["LEARNING_DESC_SORTBY"] = "Tri liste des règles";
$MESS["LEARNING_CHECK_PERMISSIONS"] = "Vérifier le droit d'accès";
$MESS["LEARNING_DESC_SORTORDER"] = "Classification";
$MESS["LEARNING_COURSE_URL_NAME"] = "Cours détail URL de la page";
$MESS["LEARNING_COURSES_PER_PAGE"] = "Nombre de cours par page";
?>