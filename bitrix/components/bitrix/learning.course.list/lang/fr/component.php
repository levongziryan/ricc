<?
$MESS["LEARNING_COURSES_COURSE_ADD"] = "Ajouter un nouveau cours";
$MESS["comp_course_list_toolbar_add"] = "Ajouter un nouveau cours";
$MESS["comp_course_list_toolbar_add_title"] = "Ajouter un nouveau cours dans le panneau de contrôle";
$MESS["LEARNING_PANEL_CONTROL_PANEL"] = "Administration du système";
$MESS["LEARNING_PANEL_CONTROL_PANEL_ALT"] = "Accéder au panneau de commande";
$MESS["comp_course_list_toolbar_list_title"] = "Liste de cours dans le Panneau de Configuration";
$MESS["comp_course_list_toolbar_list"] = "Gestion des cours";
$MESS["LEARNING_COURSE_LIST"] = "Liste de leçons";
$MESS["LEARNING_COURSES_NAV"] = "Cours";
$MESS["LEARNING_MODULE_NOT_FOUND"] = "Module des blogs non installé.";
?>