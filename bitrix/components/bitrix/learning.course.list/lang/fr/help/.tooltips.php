<?
$MESS["SORORDER_TIP"] = "Dans quel ordre les cours seront triés: dans ordre croissant ou décroissant.";
$MESS["COURSES_PER_PAGE_TIP"] = "Le nombre de cours affichés sur la page. d'autres le seront par la méthode de navigation page par page.";
$MESS["CHECK_PERMISSIONS_TIP"] = "Au cas où la valeur 'Oui' soit choisie, alors le droit d'accès sera vérifié.";
$MESS["SET_TITLE_TIP"] = "Avec l'option marquée comme le titre de la page il sera mis <b>Liste des cours</b>.";
$MESS["COURSE_DETAIL_TEMPLATE_TIP"] = "Chemin vers la page avec la revue détaillée du cours.";
$MESS["SORBY_TIP"] = "Indique le champ permettant de trier les taux sur la page.";
?>