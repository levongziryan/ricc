<?
$MESS["MB_LIVE_FEED"] = "Fluxo de Atividades";
$MESS["MB_MESSAGES"] = "Mensagens";
$MESS["MB_COMPANY"] = "Funcionários";
$MESS["MB_SEC_FAVORITE"] = "Meu Site";
$MESS["MB_SEC_GROUPS"] = "Grupos";
$MESS["MB_SEC_EXTRANET"] = "Grupos Extranet";
$MESS["MB_EXIT"] = "Sair";
$MESS["MB_HELP"] = "Ajudar";
$MESS["MB_TASKS_MAIN_MENU_ITEM"] = "Tarefas";
$MESS["MB_CALENDAR_LIST"] = "Calendário";
$MESS["MB_CURRENT_USER_FILES_MAIN_MENU_ITEM"] = "Arquivos";
$MESS["MB_SHARED_FILES_MAIN_MENU_ITEM"] = "Os arquivos compartilhados";
$MESS["PULL_TEXT"] = "Puxe para baixo para atualizar";
$MESS["DOWN_TEXT"] = "Solte para atualizar";
$MESS["LOAD_TEXT"] = "Atualizando o menu ...";
$MESS["MB_CRM_ACTIVITY"] = "Minhas atividades";
$MESS["MB_CRM_CONTACT"] = "Contactos";
$MESS["MB_CRM_COMPANY"] = "Empresas";
$MESS["MB_CRM_DEAL"] = "Negócios";
$MESS["MB_CRM_LEAD"] = "Leads";
$MESS["MB_CRM_INVOICE"] = "Faturas";
$MESS["MB_CRM_PRODUCT"] = "Produtos";
$MESS["MB_CURRENT_USER_FILES_MAIN_MENU_ITEM_NEW"] = "Minha Unidade";
$MESS["MB_SHARED_FILES_MAIN_MENU_ITEM_NEW"] = "Unidade da Empresa";
?>