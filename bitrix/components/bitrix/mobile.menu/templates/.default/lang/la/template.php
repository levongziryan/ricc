<?
$MESS["MB_LIVE_FEED"] = "Flujo de Actividad";
$MESS["MB_MESSAGES"] = "Mensajes";
$MESS["MB_COMPANY"] = "Empleados";
$MESS["MB_SEC_FAVORITE"] = "Mi Área de Trabajo";
$MESS["MB_SEC_GROUPS"] = "Grupos";
$MESS["MB_SEC_EXTRANET"] = "Grupos de Extranet";
$MESS["MB_EXIT"] = "Cerrar Sesión";
$MESS["MB_HELP"] = "Ayuda";
$MESS["MB_TASKS_MAIN_MENU_ITEM"] = "Tareas";
$MESS["MB_CALENDAR_LIST"] = "Calendario";
$MESS["MB_CURRENT_USER_FILES_MAIN_MENU_ITEM"] = "My Drive";
$MESS["MB_SHARED_FILES_MAIN_MENU_ITEM"] = "Drive de la Compañía";
$MESS["PULL_TEXT"] = "Deslizar hacia abajo para actualizar";
$MESS["DOWN_TEXT"] = "Versión para actualizar";
$MESS["LOAD_TEXT"] = "Actualizando el menú...";
$MESS["MB_CRM_ACTIVITY"] = "Mis actividades";
$MESS["MB_CRM_CONTACT"] = "Contactos";
$MESS["MB_CRM_COMPANY"] = "Compañías";
$MESS["MB_CRM_DEAL"] = "Negociaciones";
$MESS["MB_CRM_LEAD"] = "Prospectos";
$MESS["MB_CRM_INVOICE"] = "Facturas";
$MESS["MB_CRM_PRODUCT"] = "Productos";
$MESS["MB_CURRENT_USER_FILES_MAIN_MENU_ITEM_NEW"] = "Mi Drive";
$MESS["MB_SHARED_FILES_MAIN_MENU_ITEM_NEW"] = "Drive de la Compañía";
?>