<?
$MESS["CRM_CONTACT_VAR"] = "Nome variável do ID de contato";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "Modelo de Caminho da Página de Índice";
$MESS["CRM_SEF_PATH_TO_LIST"] = "Modelo de Caminho da Página de Contatos";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "Modelo de Caminho da Página do Editor de Contato";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "Modelo de Caminho da Página de Visualização do Contato";
$MESS["CRM_SEF_PATH_TO_SERVICE"] = "Modelo de Caminho da Página de Serviço Web";
$MESS["CRM_SEF_PATH_TO_EXPORT"] = "Modelo de Caminho da Página de Exportação";
$MESS["CRM_ELEMENT_ID"] = "ID do contato";
$MESS["CRM_SEF_PATH_TO_IMPORT"] = "Modelo  de Caminho da Página Importação";
$MESS["CRM_NAME_TEMPLATE"] = "Formato do nome";
?>