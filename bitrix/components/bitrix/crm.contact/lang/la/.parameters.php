<?
$MESS["CRM_CONTACT_VAR"] = "Nombre del nombre de la variable";
$MESS["CRM_ELEMENT_ID"] = "ID del Contacto";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "Plantilla de la Ruta a la página del índice";
$MESS["CRM_SEF_PATH_TO_LIST"] = "Plantilla de la ruta a la página de contactos";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "Plantilla de la ruta a la página del editor";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "Plantilla de la ruta da la página de vista de contacto";
$MESS["CRM_SEF_PATH_TO_SERVICE"] = "Plantilla de la ruta a la página del servicio web";
$MESS["CRM_SEF_PATH_TO_EXPORT"] = "Plantilla de la ruta a la página de exportación";
$MESS["CRM_SEF_PATH_TO_IMPORT"] = "Importar la plantilla de la ruta a la página";
$MESS["CRM_NAME_TEMPLATE"] = "Formato de nombre";
?>