<?
$MESS["CRM_FF_LEAD"] = "Leads";
$MESS["CRM_FF_CONTACT"] = "Contatos";
$MESS["CRM_FF_COMPANY"] = "Empresas";
$MESS["CRM_FF_DEAL"] = "Negócios";
$MESS["CRM_FF_OK"] = "Selecionar";
$MESS["CRM_FF_CANCEL"] = "Cancelar";
$MESS["CRM_FF_CLOSE"] = "Fechar";
$MESS["CRM_FF_SEARCH"] = "Pesquisar";
$MESS["CRM_FF_NO_RESULT"] = "Infelizmente, seu pedido de busca não retornou resultados.";
$MESS["CRM_FF_CHOISE"] = "Selecionar";
$MESS["CRM_FF_CHANGE"] = "Editar";
$MESS["CRM_FF_LAST"] = "ltimo";
$MESS["CRM_REPORT_CURRENCY_INFO"] = "Moeda do relatório: #CURRENCY#";
$MESS["CRM_REPORT_INCLUDE_ALL"] = "Todos";
?>