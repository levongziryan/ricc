<?
$MESS["CRM_FF_LEAD"] = "Prospectos";
$MESS["CRM_FF_CONTACT"] = "Contactos";
$MESS["CRM_FF_COMPANY"] = "Compañías";
$MESS["CRM_FF_DEAL"] = "Negociaciones";
$MESS["CRM_FF_OK"] = "Seleccione";
$MESS["CRM_FF_CANCEL"] = "Cancelar";
$MESS["CRM_FF_CLOSE"] = "Cerrar";
$MESS["CRM_FF_SEARCH"] = "Búsqueda";
$MESS["CRM_FF_NO_RESULT"] = "Lamentablemente, su solicitud de búsqueda no ha obtenido resultados.";
$MESS["CRM_FF_CHOISE"] = "Seleccione";
$MESS["CRM_FF_CHANGE"] = "Editar";
$MESS["CRM_FF_LAST"] = "Último";
$MESS["CRM_REPORT_CURRENCY_INFO"] = "Reporte de moneda: #CURRENCY#";
$MESS["CRM_REPORT_INCLUDE_ALL"] = "Todos";
?>