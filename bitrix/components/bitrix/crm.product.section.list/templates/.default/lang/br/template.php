<?
$MESS["CRM_SECTION_NAME"] = "Nome";
$MESS["CRM_SECTION_ADD_DIALOG_TITLE"] = "Adicionar seção";
$MESS["CRM_SECTION_RENAME_DIALOG_TITLE"] = "Renomear Seção";
$MESS["CRM_SECTION_NAME_FIELD_TITLE"] = "Nome";
$MESS["CRM_SECTION_DEFAULT_NAME"] = "Nova Seção";
$MESS["CRM_SECTION_ADD_BTN_TEXT"] = "Adicionar";
$MESS["CRM_SECTION_RENAME_BTN_TEXT"] = "Renomear";
$MESS["CRM_SECTION_CANCEL_BTN_TEXT"] = "Cancelar";
$MESS["CRM_PRODUCT_SECTION_ACTION_RENAME"] = "Renomear";
$MESS["CRM_PRODUCT_SECTION_ACTION_DELETE"] = "Excluir";
$MESS["CRM_PRODUCT_SECTION_ACTION_DELETE_PROPMT"] = "Isto irá apagar todas as subseções e os sub elementos. Tem certeza de que deseja apagar a seção?";
$MESS["CRM_SECTION_EMPTY_NAME_ERROR"] = "Por favor insira o nome da seção.";
?>