<?
$MESS["CRM_SECTION_NAME"] = "Nombre";
$MESS["CRM_SECTION_ADD_DIALOG_TITLE"] = "Agregar sección";
$MESS["CRM_SECTION_RENAME_DIALOG_TITLE"] = "Cambiar el nombre de sección";
$MESS["CRM_SECTION_NAME_FIELD_TITLE"] = "Nombre";
$MESS["CRM_SECTION_DEFAULT_NAME"] = "Nueva sección";
$MESS["CRM_SECTION_ADD_BTN_TEXT"] = "Agregar";
$MESS["CRM_SECTION_RENAME_BTN_TEXT"] = "Cambiar el nombre";
$MESS["CRM_SECTION_CANCEL_BTN_TEXT"] = "Cancelar";
$MESS["CRM_PRODUCT_SECTION_ACTION_RENAME"] = "Cambiar el nombre";
$MESS["CRM_PRODUCT_SECTION_ACTION_DELETE"] = "Eliminar";
$MESS["CRM_PRODUCT_SECTION_ACTION_DELETE_PROPMT"] = "Esto eliminará todas las subsecciones y los elementos secundarios. ¿Está seguro que desea eliminar la sección?";
$MESS["CRM_SECTION_EMPTY_NAME_ERROR"] = "Por favor, introduzca el nombre de la sección.";
?>