<?
$MESS["CRM_PRODUCT_SECTION_ACTION_DELETE_PROPMT"] = "Toutes les subdivisions avec leurs éléments seront supprimées. Êtes-vous sûr de vouloir supprimer la section?";
$MESS["CRM_SECTION_ADD_BTN_TEXT"] = "Ajouter";
$MESS["CRM_SECTION_ADD_DIALOG_TITLE"] = "Ajouter une section";
$MESS["CRM_SECTION_NAME"] = "Dénomination";
$MESS["CRM_SECTION_NAME_FIELD_TITLE"] = "Dénomination";
$MESS["CRM_SECTION_DEFAULT_NAME"] = "Une nouvelle rubrique";
$MESS["CRM_SECTION_CANCEL_BTN_TEXT"] = "Annuler";
$MESS["CRM_SECTION_RENAME_BTN_TEXT"] = "Renommer";
$MESS["CRM_PRODUCT_SECTION_ACTION_RENAME"] = "Renommer";
$MESS["CRM_SECTION_RENAME_DIALOG_TITLE"] = "Renommer la section";
$MESS["CRM_SECTION_EMPTY_NAME_ERROR"] = "Veuillez saisir le nom de la section.";
$MESS["CRM_PRODUCT_SECTION_ACTION_DELETE"] = "Supprimer";
?>