<?
$MESS ['P_SHOW_PAGE_NAVIGATION_BOTTOM'] = "unten";
$MESS ['P_SHOW_PAGE_NAVIGATION'] = "Navigation anzeigen";
$MESS ['P_SHOW_PAGE_NAVIGATION_NONE'] = "verbergen";
$MESS ['P_THUMBS_SIZE'] = "Größe des Detailfotos (px)";
$MESS ['P_SQUARE_PERCENT'] = "Größenänderung in Prozent (bei der Größenberechnung für rechteckige Bilder 'Größe des Detailbildes' * 'Größenänderung in Prozent')";
$MESS ['P_SQUARE'] = "Quadratische Darstellung (Fotos, deren Größe die voreingestellte Größe übersteigt, werden im Beschnitt angezeigt)";
$MESS ['P_SHOW_PAGE_NAVIGATION_TOP'] = "oben";
$MESS ['P_SHOW_PAGE_NAVIGATION_BOTH'] = "oben und unten";
?>