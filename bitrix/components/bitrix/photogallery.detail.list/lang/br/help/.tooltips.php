<?
$MESS["IBLOCK_TYPE_TIP"] = "Selecione aqui um dos tipos existentes de blocos de informação. Clique em <b><i>OK</i></b>  para carregar os blocos de informação do tipo selecionado. ";
$MESS["CACHE_TYPE_TIP"] = "<i>Auto</i>: o cache é válido durante o período pré-definido nas configurações de cache;<br /><i>Cache</i>: sempre utilizar cache para o período especificado no próximo campo;<br /><i>Não utilizar cache</i>: nenhum cache é executado.";
$MESS["CACHE_TIME_TIP"] = "Especifique aqui o período durante o qual o cache é válido. 
";
$MESS["SEARCH_URL_TIP"] = "Caminho para a página de pesquisa ";
$MESS["IBLOCK_ID_TIP"] = "Especifique aqui o bloco de informações que irá armazenar as fotos. Alternativamente, você pode selecionar <b>(outro)-></b> e especificar a ID do bloco de informações no campo ao lado.
";
$MESS["SECTION_ID_TIP"] = "Este campo contém a expressão que avalia a ID da seção (álbum).";
$MESS["ELEMENT_LAST_TYPE_TIP"] = "Especifique o modo com que as fotos serão selecionadas para exibição. ";
$MESS["USE_DESC_PAGE_TIP"] = "Especifica o uso de caminho de navegação inverso na página de fotos. 
";
$MESS["ELEMENT_SORT_FIELD_TIP"] = "Especifique aqui o campo pelo qual as fotos serão classificadas. ";
$MESS["ELEMENT_SORT_ORDER_TIP"] = "Especifica a ordem de classificação.";
$MESS["ADDITIONAL_SIGHTS_TIP"] = "Selecione aqui os tamanhos de miniaturas que estarão disponíveis.";
$MESS["PICTURES_SIGHT_TIP"] = "Selecione aqui o tamanho inicial das miniaturas que serão exibidas na página do álbum. ";
$MESS["DETAIL_URL_TIP"] = "Especifique o endereço da página de visualização de detalhes do álbum. ";
$MESS["PAGE_ELEMENTS_TIP"] = "Especifica o número de álbuns por página. Álbuns que não estiverem na página estarão disponíveis via mapa de navegação. ";
$MESS["PAGE_NAVIGATION_TEMPLATE_TIP"] = "Especifica o template de caminho de navegação. O  <b>padrão</b> será utilizado se nenhum template for selecionado. ";
$MESS["USE_PERMISSIONS_TIP"] = "Restringe o acesso ao álbum.";
$MESS["GROUP_PERMISSIONS_TIP"] = "Especifique aqui os grupos de usuários cujos membros podem ver o álbum.";
$MESS["COMMENTS_TYPE_TIP"] = "Selecione aqui o módulo cujas funções serão utilizadas para comentários. ";
$MESS["SET_TITLE_TIP"] = "Ative esta opção para ajustar o título da página pra \"Fotos\".";
$MESS["DATE_TIME_FORMAT_TIP"] = "Especifique aqui o formato de exibição de data.";
$MESS["ELEMENTS_LAST_COUNT_TIP"] = "Especifica o número de fotos recentes a serem exibidas. ";
$MESS["ELEMENTS_LAST_TIME_TIP"] = "Especifica o número de dias recentes durante os quais serão exibidas as fotos. ";
$MESS["ELEMENTS_LAST_TIME_FROM_TIP"] = "Especifique aqui o primeiro dia do período de seleção.";
$MESS["ELEMENTS_LAST_TIME_TO_TIP"] = "Especifique aqui o último dia do período de seleção. ";
?>