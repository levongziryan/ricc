<?
$MESS["CATALOG_SET_IBLOCK_ELEMENT_ID"] = "Identifiant de l'élément";
$MESS["IBLOCK_VAT_INCLUDE"] = "Inclure la TVA au prix";
$MESS["CP_BCT_CACHE_GROUPS"] = "Prendre en compte les droits d'accès";
$MESS["IBLOCK_BASKET_URL"] = "URL de la page avec le panier de l'acheteur";
$MESS["CP_BCT_CURRENCY_ID"] = "Convertir les prix en devise unique:";
$MESS["CATALOG_SET_IBLOCK_ID"] = "Bloc d'information";
$MESS["CP_BCT_CONVERT_CURRENCY"] = "Afficher les prix en devise unique";
$MESS["CP_BCT_OFFERS_CART_PROPERTIES"] = "Qualités des offres à ajouter au panier";
$MESS["CATALOG_SET_IBLOCK_TYPE"] = "Type de bloc d'information";
$MESS["CATALOG_SET_IBLOCK_PRICE_CODE"] = "Type de prix";
$MESS["IBLOCK_PRICES"] = "Prix";
$MESS["CATALOG_SET_PRODUCT_PROPERTIES"] = "Caractéristiques des marchandises";
?>