<?
$MESS["WIKI_VERSION_FROM"] = "Version de";
$MESS["WIKI_RESTORE_TO_CURRENT"] = "Restaurer jusqu'à courant(e)";
$MESS["WIKI_PREV_VERSION"] = "Comparer avec la version précédente";
$MESS["WIKI_NEXT_VERSION"] = "En avant";
$MESS["WIKI_TAGS"] = "Tags";
$MESS["WIKI_CURR_VERSION"] = "Version Courante";
?>