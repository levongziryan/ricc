<?
$MESS["LEARNING_COUNTRY_NONE"] = "(inconnu(e))";
$MESS["LEARNING_USER_EMAIL"] = "Courrier électronique";
$MESS["LEARNING_USER_PERSONAL_WWW"] = "Site web";
$MESS["LEARNING_USER_PERSONAL_CITY"] = "Ville";
$MESS["LEARNING_USER_NAME"] = "Prénom";
$MESS["LEARNING_PERSONAL_DATA"] = "Données personnelles";
$MESS["LEARNING_USER_PERSONAL_STATE"] = "District";
$MESS["LEARNING_USER_ADDRESS"] = "Adresse postale";
$MESS["LEARNING_USER_PERSONAL_ZIP"] = "Code Postal";
$MESS["LEARNING_RESUME"] = "Curriculum vitae";
$MESS["LEARNING_SAVE"] = "Sauvegarder";
$MESS["LEARNING_USER_PERSONAL_COUNTRY"] = "Pays";
$MESS["LEARNING_DELETE_FILE"] = "Suppression du fichier";
$MESS["LEARNING_USER_PERSONAL_STREET"] = "Rue";
$MESS["LEARNING_USER_LAST_NAME"] = "Nom";
$MESS["LEARNING_USER_PHOTO"] = "Photo";
$MESS["LEARNING_USER_PERSONAL_ICQ"] = "Numéro ICQ";
$MESS["LEARNING_EDIT_PROFILE"] = "Les paramètres de profil";
$MESS["LEARNING_PUBLIC_PROFILE"] = "Autoriser l'accès du public au profil de l'étudiant";
$MESS["LEARNING_TRANSCRIPT"] = "Lien vers la page de certification";
?>