<?
$MESS["LEARNING_PROFILE_TITLE"] = "Perfil del estudiante ";
$MESS["LEARNING_MODULE_NOT_FOUND"] = "El módulo e-Learning no está instalado.";
$MESS["LEARNING_NO_AUTHORIZE"] = "Autorización requerida para ver esta página.";
$MESS["LEARNING_NO_MAIL"] = "El e-mail no está especificado";
$MESS["LEARNING_BAD_MAIL"] = "El e-mail es incorrecto ";
?>