<?
$MESS["USER_PROPERTY_TIP"] = "Sélectionnez ici propriétés supplémentaires qui seront présentées dans le profil utilisateur.";
$MESS["SET_TITLE_TIP"] = "Avec une option marquée, il sera mis comme titre de la page <b>Questionnaire d'un spécialiste</b>.";
$MESS["TRANSCRIPT_DETAIL_TEMPLATE_TIP"] = "Chemin vers la page avec les résultats de certification.";
?>