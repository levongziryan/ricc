<?
$MESS["LEARNING_PROFILE_TITLE"] = "Enquête du spécialiste";
$MESS["LEARNING_NO_MAIL"] = "Adresse email est pas spécifié";
$MESS["LEARNING_BAD_MAIL"] = "Adresse email est incorrecte";
$MESS["LEARNING_NO_AUTHORIZE"] = "Autorisation nécessaire pour visualiser cette page.";
$MESS["LEARNING_MODULE_NOT_FOUND"] = "Module des blogs non installé.";
?>