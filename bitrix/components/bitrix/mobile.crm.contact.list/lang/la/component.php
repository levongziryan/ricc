<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["M_CRM_CONTACT_LIST_PRESET_CHANGE_MY"] = "Modificado por mí";
$MESS["M_CRM_CONTACT_LIST_PRESET_MY"] = "Mis contactos";
$MESS["M_CRM_CONTACT_LIST_FILTER_CUSTOM"] = "Resustado de búsqueda";
$MESS["M_CRM_CONTACT_LIST_FILTER_NONE"] = "Todos mis contactos";
$MESS["M_CRM_CONTACT_LIST_PRESET_USER"] = "Filtro personalizado";
$MESS["M_CRM_CONTACT_DEALS"] = "Actividades";
$MESS["M_CRM_CONTACT_CALLS"] = "Llamadas";
$MESS["M_CRM_CONTACT_LIST_MORE"] = "Más...";
$MESS["M_CRM_CONTACT_LIST_EDIT"] = "Editar";
$MESS["M_CRM_CONTACT_LIST_DELETE"] = "Eliminar";
?>