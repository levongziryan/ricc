<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["M_CRM_CONTACT_LIST_PRESET_CHANGE_MY"] = "Modificado por mim";
$MESS["M_CRM_CONTACT_LIST_PRESET_MY"] = "Meus contatos";
$MESS["M_CRM_CONTACT_LIST_FILTER_CUSTOM"] = "Resultados da Pesquisa";
$MESS["M_CRM_CONTACT_LIST_FILTER_NONE"] = "Todos os contatos";
$MESS["M_CRM_CONTACT_LIST_PRESET_USER"] = "Filtro personalizado";
$MESS["M_CRM_CONTACT_DEALS"] = "Atividades";
$MESS["M_CRM_CONTACT_CALLS"] = "Ligações";
$MESS["M_CRM_CONTACT_LIST_MORE"] = "Mais...";
$MESS["M_CRM_CONTACT_LIST_EDIT"] = "Editar";
$MESS["M_CRM_CONTACT_LIST_DELETE"] = "Excluir";
?>