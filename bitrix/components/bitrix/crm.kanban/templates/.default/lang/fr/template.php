<?
$MESS["CRM_KANBAN_POPUP_DATE"] = "Date";
$MESS["CRM_KANBAN_POPUP_DOC_NUM"] = "Facture #";
$MESS["CRM_KANBAN_POPUP_COMMENT"] = "Commentaire";
$MESS["CRM_KANBAN_POPUP_PARAMS_SAVE"] = "Enregistrer";
$MESS["CRM_KANBAN_POPUP_PARAMS_CANCEL"] = "Annuler";
$MESS["CRM_KANBAN_POPUP_INVOICE"] = "Paramètres finaux de facture :";
$MESS["CRM_KANBAN_POPUP_LEAD"] = "Créer sur base du lead :";
$MESS["CRM_KANBAN_POPUP_LEAD_SELECT"] = "Sélectionner";
?>