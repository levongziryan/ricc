<?
$MESS["CRM_KANBAN_POPUP_DATE"] = "Fecha";
$MESS["CRM_KANBAN_POPUP_DOC_NUM"] = "Factura #";
$MESS["CRM_KANBAN_POPUP_COMMENT"] = "Comentario";
$MESS["CRM_KANBAN_POPUP_PARAMS_SAVE"] = "Guardar";
$MESS["CRM_KANBAN_POPUP_PARAMS_CANCEL"] = "Cancelar";
$MESS["CRM_KANBAN_POPUP_INVOICE"] = "Parámetros de cierre de factura:";
$MESS["CRM_KANBAN_POPUP_LEAD"] = "Crear basado en el prospecto:";
$MESS["CRM_KANBAN_POPUP_LEAD_SELECT"] = "Seleccionar";
?>