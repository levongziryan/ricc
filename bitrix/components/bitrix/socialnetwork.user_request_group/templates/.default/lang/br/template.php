<?
$MESS["SONET_C39_T_PROMT"] = "Um pedido de participação foi enviado para o grupo. Assim que o administrador do grupo aprovar a solicitação, você vai visualizar o grupo na lista";
$MESS["SONET_C39_T_GROUP"] = "Grupo";
$MESS["SONET_C39_T_MESSAGE"] = "Mensagem";
$MESS["SONET_C39_T_SEND"] = "Enviar Mensagem";
$MESS["SONET_C39_T_SUCCESS"] = "Um pedido de participação foi enviado. Assim que o administrador do grupo aprovar a solicitação, você vai visualizar o grupo na lista";
$MESS["SONET_C39_T_SUCCESS_ALT"] = "Você entrou no grupo, se você deseja deixar o grupo, clique no link \"Deixar o grupo\" na página do perfil";
$MESS["SONET_C39_T_SENDER"] = "Remetente";
$MESS["SONET_C39_T_ACTIONS"] = "Ações";
$MESS["SONET_C39_T_FRIEND_REQUEST"] = "O usuário quer adicionar você como amigo";
$MESS["SONET_C39_T_USER"] = "Usuário";
$MESS["SONET_C39_T_INVITE"] = "Convidou você para o grupo";
$MESS["SONET_C39_T_DO_FRIEND"] = "Aceitar";
$MESS["SONET_C39_T_DO_DENY"] = "Recusar";
$MESS["SONET_C39_T_DO_AGREE"] = "Aceitar";
$MESS["SONET_C39_T_REJECTED"] = "Você rejeitou a participação no grupo";
?>