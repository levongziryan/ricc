<?
$MESS["INTR_PREDEF_DEPARTMENT"] = "Departmento/Escritório";
$MESS["INTR_ISIA_PARAM_DETAIL_URL"] = "Página de Visualização de Detalhe";
$MESS["INTR_ISIN_PARAM_NUM_USERS"] = "Exibir Registros";
$MESS["INTR_ISIN_PARAM_PM_URL"] = "Página de Mensagem Pessoal";
$MESS["INTR_ISIN_PARAM_PATH_TO_CONPANY_DEPARTMENT"] = "Caminho de Página de Modelo de Departamento";
$MESS["INTR_ISIN_PARAM_DATE_TIME_FORMAT"] = "Formato de Data e Hora";
$MESS["INTR_ISIN_PARAM_SHOW_YEAR"] = "Exibir Ano de Nascimento";
$MESS["INTR_ISIN_PARAM_SHOW_YEAR_VALUE_Y"] = "Todos";
$MESS["INTR_ISIN_PARAM_SHOW_YEAR_VALUE_M"] = "apenas homens";
$MESS["INTR_ISIN_PARAM_SHOW_YEAR_VALUE_N"] = "ninguém";
$MESS["INTR_ISIN_PARAM_NAME_TEMPLATE"] = "Formato de Nome";
$MESS["INTR_ISIN_PARAM_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["INTR_ISIN_PARAM_SHOW_LOGIN"] = "Exibir Nome de Login se nenhum campo de nome de usuário obrigatório está disponível";
$MESS["INTR_ISIN_PARAM_DATE_FORMAT"] = "Formato de Data";
?>