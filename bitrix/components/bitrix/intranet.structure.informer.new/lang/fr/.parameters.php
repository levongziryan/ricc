<?
$MESS["INTR_ISIN_PARAM_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["INTR_ISIN_PARAM_SHOW_YEAR_VALUE_Y"] = "tous";
$MESS["INTR_ISIN_PARAM_NUM_USERS"] = "Nombre des enregistrements affichés";
$MESS["INTR_ISIN_PARAM_SHOW_YEAR_VALUE_N"] = "personne";
$MESS["INTR_PREDEF_DEPARTMENT"] = "Département/bureau";
$MESS["INTR_ISIN_PARAM_NAME_TEMPLATE"] = "Affichage du nom";
$MESS["INTR_ISIN_PARAM_SHOW_YEAR"] = "Afficher l'année de naissance";
$MESS["INTR_ISIN_PARAM_SHOW_LOGIN"] = "Afficher le nom d'utilisateur si le nom n'est pas spécifié";
$MESS["INTR_ISIA_PARAM_DETAIL_URL"] = "Page de l'examen détaillé";
$MESS["INTR_ISIN_PARAM_PM_URL"] = "Page de l'envoi du message privé";
$MESS["INTR_ISIN_PARAM_SHOW_YEAR_VALUE_M"] = "seulement aux hommes";
$MESS["INTR_ISIN_PARAM_DATE_FORMAT"] = "Format d'affichage de la date";
$MESS["INTR_ISIN_PARAM_DATE_TIME_FORMAT"] = "Format d'affichage de la date et de l'heure";
$MESS["INTR_ISIN_PARAM_PATH_TO_CONPANY_DEPARTMENT"] = "Modèle de chemin d'accès à la page d'une section";
?>