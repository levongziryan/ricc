<?
$MESS["DISK_EXT_LINK_INVALID"] = "Le lien public est invalide";
$MESS["DISK_EXT_LINK_TITLE"] = "Fichier introuvable";
$MESS["DISK_EXT_LINK_TEXT"] = "Fichier introuvable";
$MESS["DISK_EXT_LINK_B24"] = "Bitrix<span>24</span>";
$MESS["DISK_EXT_LINK_DESCRIPTION"] = "Ce fichier a certainement été rétiré de la publication, ou vous avez été utilisé un lien invalide.";
$MESS["DISK_EXT_LINK_B24_ADV_TEXT"] = "Espace multifonctionnel unifié";
$MESS["DISK_EXT_LINK_B24_ADV_1"] = "Tâches & Projets";
$MESS["DISK_EXT_LINK_B24_ADV_2"] = "CRM";
$MESS["DISK_EXT_LINK_B24_ADV_3"] = "Chat collaboratif";
$MESS["DISK_EXT_LINK_B24_ADV_4"] = "Documents";
$MESS["DISK_EXT_LINK_B24_ADV_5"] = "Drive de groupe";
$MESS["DISK_EXT_LINK_B24_ADV_6"] = "Calendrier";
$MESS["DISK_EXT_LINK_B24_ADV_CREATE_LINK_TEXT"] = "Créez votre compte Bitrix24";
$MESS["DISK_EXT_LINK_B24_ADV_CREATE_LINK_HREF"] = "http://www.bitrix24.com/?utm_source=fileshare_button&utm_medium=referral&utm_campaign=fileshare_button";
?>