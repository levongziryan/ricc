<?
$MESS["DISK_EXT_LINK_INVALID"] = "O link público é inválido.";
$MESS["DISK_EXT_LINK_TITLE"] = "Arquivo não encontrado";
$MESS["DISK_EXT_LINK_TEXT"] = "Arquivo não encontrado";
$MESS["DISK_EXT_LINK_B24"] = "Bitrix<span>24</span>";
$MESS["DISK_EXT_LINK_DESCRIPTION"] = "O arquivo pode não ter sido publicado, ou você clicou em um link inválido. <br/>Entre em contato com o proprietário do arquivo.";
$MESS["DISK_EXT_LINK_B24_ADV_TEXT"] = "Conjunto de aplicações de colaboração unificado";
$MESS["DISK_EXT_LINK_B24_ADV_1"] = "Tarefas e projetos";
$MESS["DISK_EXT_LINK_B24_ADV_2"] = "CRM";
$MESS["DISK_EXT_LINK_B24_ADV_3"] = "IM&Group Chat";
$MESS["DISK_EXT_LINK_B24_ADV_4"] = "Documentos";
$MESS["DISK_EXT_LINK_B24_ADV_5"] = "Unidade do Grupo";
$MESS["DISK_EXT_LINK_B24_ADV_6"] = "Calendário";
$MESS["DISK_EXT_LINK_B24_ADV_CREATE_LINK_TEXT"] = "Crie sua conta Bitrix24 agora";
?>