<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado. ";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_TAX_NOT_FOUND"] = "O imposto não foi encontrado";
$MESS["CRM_TAX_SECTION_MAIN"] = "Imposto";
$MESS["CRM_TAX_FIELD_ID"] = "ID";
$MESS["CRM_TAX_FIELD_TIMESTAMP_X"] = "Modificado em";
$MESS["CRM_TAX_FIELD_NAME"] = "Nome";
$MESS["CRM_TAX_FIELD_LID"] = "Website";
$MESS["CRM_TAX_FIELD_CODE"] = "Código Mnemônico";
$MESS["CRM_TAX_FIELD_DESCRIPTION"] = "Descrição";
$MESS["CRM_TAX_RATE_LIST"] = "Taxas de impostos";
?>