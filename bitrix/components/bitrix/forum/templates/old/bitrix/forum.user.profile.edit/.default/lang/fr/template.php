<?
$MESS["FP_REQUIED_FILEDS"] = "- Veuillez vous assurer que tous les champs obligatoires soient remplis.";
$MESS["FP_CHANGE_PROFILE"] = "Profil";
$MESS["FP_NAME"] = "Nom :";
$MESS["FP_LAST_NAME"] = "Nom de famille :";
$MESS["FP_LOGIN"] = "Identifiant :";
$MESS["FP_NEW_PASSWORD"] = "Saisissez un nouveau mot de passe si vous voulez en changer :";
$MESS["FP_PASSWORD_CONFIRM"] = "Confirmation du mot de passe :";
$MESS["FP_PRIVATE_INFO"] = "Détails personnels";
$MESS["FP_PROFESSION"] = "Profession :";
$MESS["FP_WWW_PAGE"] = "Site internet :";
$MESS["FP_SEX"] = "Sexe :";
$MESS["FP_SEX_NONE"] = "inconnu";
$MESS["FP_BIRTHDATE"] = "Date de naissance (";
$MESS["FP_PHOTO"] = "Photo";
$MESS["FP_LOCATION"] = "Localisation";
$MESS["FP_COUNTRY"] = "Pays :";
$MESS["FP_COUNTRY_NONE"] = "inconnu";
$MESS["FP_REGION"] = "État :";
$MESS["FP_CITY"] = "Ville :";
$MESS["FP_WORK_INFO"] = "Information professionnelle";
$MESS["FP_COMPANY_NAME"] = "Société :";
$MESS["FP_COMPANY_DEPARTMENT"] = "Service :";
$MESS["FP_COMPANY_ROLE"] = "Position :";
$MESS["FP_COMPANY_ACT"] = "Profil :";
$MESS["FP_COMPANY_LOCATION"] = "Localisation";
$MESS["FP_FORUM_PROFILE"] = "Profil de forum";
$MESS["FP_ALLOW_POST"] = "Permettre de poster des messages :";
$MESS["FP_SHOW_NAME"] = "Afficher le nom :";
$MESS["FP_NOT_SHOW_IN_LIST"] = "&nbsp;Masquer de";
$MESS["FP_SUBSC_GET_MY_MESSAGE"] = "Inclure ses propres messages dans l'e-mail ";
$MESS["FP_NOW_ONLINE"] = "Utilisateur actif";
$MESS["FP_DESCR"] = "Description :";
$MESS["FP_INTERESTS"] = "Centres d'intérêt :";
$MESS["FP_SIGNATURE"] = "Signature :";
$MESS["FP_AVATAR"] = "Avatar :";
$MESS["FP_SAVE"] = "Enregistrer";
$MESS["FP_CANCEL"] = "Annuler";
$MESS["FILE_DELETE"] = "Supprimer fichier";
$MESS["USER_TYPE_EDIT_TAB"] = "Propriété utilisateur";
$MESS["F_KB"] = " KB";
$MESS["F_MB"] = " MB";
$MESS["F_B"] = " octet";
$MESS["FP_SIZE_AVATAR"] = "La taille de l'avatar ne doit pas dépasser #SIZE# pixels et #SIZE_BITE#";
?>