<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_CATALOG_MODULE_NOT_INSTALLED"] = "El módulo Commercial Catalog no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_VAT_NOT_FOUND"] = "No se encontró el tipo de IVA.";
$MESS["CRM_VAT_SECTION_MAIN"] = "Tasa del IVA";
$MESS["CRM_VAT_FIELD_ID"] = "ID";
$MESS["CRM_VAT_FIELD_C_SORT"] = "Clasificar";
$MESS["CRM_VAT_FIELD_NAME"] = "Nombre";
$MESS["CRM_VAT_FIELD_ACTIVE"] = "Activo";
$MESS["CRM_VAT_FIELD_RATE"] = "Tasa";
$MESS["CRM_VAT_ADD_UNKNOWN_ERROR"] = "Error al crear un tipo de IVA.";
$MESS["CRM_VAT_UPDATE_UNKNOWN_ERROR"] = "Error al actualizar un tipo de IVA.";
$MESS["CRM_VAT_DELETE_UNKNOWN_ERROR"] = "Error al eliminar un tipo de IVA.";
?>