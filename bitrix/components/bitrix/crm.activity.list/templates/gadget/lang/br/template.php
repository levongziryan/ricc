<?
$MESS["CRM_ACTION_TYPE_MEETING"] = "Reunião com um cliente";
$MESS["CRM_ACTION_TYPE_CALL_OUTGOING"] = "Telefonar um cliente";
$MESS["CRM_ACTION_TYPE_CALL_INCOMING"] = "Telefonema de um cliente";
$MESS["CRM_ACTION_TYPE_EMAIL_OUTGOING"] = "E-mail para um cliente";
$MESS["CRM_ACTION_TYPE_EMAIL_INCOMING"] = "E-mail de um cliente";
$MESS["CRM_ACTION_TYPE_TASK"] = "Tarefa";
$MESS["CRM_ACTION_END_TIME"] = "Prazo";
$MESS["CRM_ACTION_REFERENCE_LEAD"] = "Lead";
$MESS["CRM_ACTION_REFERENCE_DEAL"] = "Negócio";
$MESS["CRM_ACTION_CUSTOMER"] = "Cliente";
$MESS["CRM_ACTION_EXPIRED"] = "atrasado";
$MESS["CRM_ACTION_IMPORTANT"] = "Coisas importantes";
$MESS["CRM_ACTION_GO_TO_FULL_VIEW"] = "Visualizar lista completa";
$MESS["CRM_ACTION_COMPLETED"] = "concluído";
?>