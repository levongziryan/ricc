<?
$MESS["CRM_ACTION_DEFAULT_SUBJECT"] = "Nova atividade (#DATE#)";
$MESS["CRM_CALL_ACTION_DEFAULT_SUBJECT"] = "Novo telefonema (#DATE#)";
$MESS["CRM_MEETING_ACTION_DEFAULT_SUBJECT"] = "Nova reunião (#DATE#)";
$MESS["CRM_EMAIL_ACTION_DEFAULT_SUBJECT"] = "Novo e-mail (#DATE#)";
$MESS["CRM_TITLE_EMAIL_SUBJECT"] = "Assunto";
$MESS["CRM_TITLE_EMAIL_FROM"] = "De";
$MESS["CRM_TITLE_EMAIL_TO"] = "Para";
$MESS["CRM_COMMUNICATION_TAB_LEAD"] = "Lead";
$MESS["CRM_COMMUNICATION_TAB_DEAL"] = "Negócio";
$MESS["CRM_COMMUNICATION_TAB_COMPANY"] = "Empresa";
$MESS["CRM_COMMUNICATION_TAB_CONTACT"] = "Contato";
$MESS["CRM_ACTIVITY_EMAIL_EMPTY_FROM_FIELD"] = "Por favor, informe o remetente da mensagem.";
$MESS["CRM_ACTIVITY_EMAIL_EMPTY_TO_FIELD"] = "Por favor, informe o destinatário da mensagem.";
$MESS["CRM_ACTIVITY_INVALID_EMAIL"] = "#VALUE#' não é um endereço de e-mail válido.";
$MESS["CRM_ACTIVITY_RESPONSIBLE_NOT_FOUND"] = "Não foi possível encontrar o usuário responsável por esta atividade.";
?>