<?
$MESS["CRM_ACTIVITY_LIST_ROW_COUNT"] = "Total : #ROW_COUNT#";
$MESS["CRM_ACTIVITY_LIST_REBUILD_ACT_STATISTICS_NOT_REQUIRED_SUMMARY"] = "Aucune statistique d'activité n'a besoin d'être mise à jour.";
$MESS["CRM_ACTIVITY_LIST_REBUILD_ACT_STATISTICS_PROGRESS_SUMMARY"] = "Activités traitées : #PROCESSED_ITEMS# sur #TOTAL_ITEMS#.";
$MESS["CRM_ACTIVITY_LIST_REBUILD_ACT_STATISTICS_COMPLETED_SUMMARY"] = "Traitement des données statistiques pour les activités terminé. Activités traitées : #PROCESSED_ITEMS#.";
$MESS["CRM_ACTIVITY_LIST_REBUILD_SEARCH_CONTENT_NOT_REQUIRED_SUMMARY"] = "L'index de recherche des activités n'a pas besoin d'être recréé.";
$MESS["CRM_ACTIVITY_LIST_REBUILD_SEARCH_CONTENT_PROGRESS_SUMMARY"] = "Activités traitées : #PROCESSED_ITEMS# sur #TOTAL_ITEMS#.";
$MESS["CRM_ACTIVITY_LIST_REBUILD_SEARCH_CONTENT_COMPLETED_SUMMARY"] = "L'index de recherche des activités a été recréé. Activités traitées : #PROCESSED_ITEMS#.";
?>