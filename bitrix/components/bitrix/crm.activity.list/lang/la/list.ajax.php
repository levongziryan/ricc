<?
$MESS["CRM_ACTIVITY_LIST_ROW_COUNT"] = "Total: #ROW_COUNT#";
$MESS["CRM_ACTIVITY_LIST_REBUILD_ACT_STATISTICS_NOT_REQUIRED_SUMMARY"] = "No hay estadísticas de actividades deben ser actualizados";
$MESS["CRM_ACTIVITY_LIST_REBUILD_ACT_STATISTICS_PROGRESS_SUMMARY"] = "Actividades procesadas: #PROCESSED_ITEMS# of #TOTAL_ITEMS#.";
$MESS["CRM_ACTIVITY_LIST_REBUILD_ACT_STATISTICS_COMPLETED_SUMMARY"] = "Realizar procesamiento de datos estadísticos para los actividades. Actividades procesadas: #PROCESSED_ITEMS#.";
$MESS["CRM_ACTIVITY_LIST_REBUILD_SEARCH_CONTENT_NOT_REQUIRED_SUMMARY"] = "No es necesario recrear el índice de búsqueda de actividad.";
$MESS["CRM_ACTIVITY_LIST_REBUILD_SEARCH_CONTENT_PROGRESS_SUMMARY"] = "Actividades procesadas: #PROCESSED_ITEMS# of #TOTAL_ITEMS#.";
$MESS["CRM_ACTIVITY_LIST_REBUILD_SEARCH_CONTENT_COMPLETED_SUMMARY"] = "Se ha recreado el índice de búsqueda de actividad. Actividades procesadas: #PROCESSED_ITEMS#.";
?>