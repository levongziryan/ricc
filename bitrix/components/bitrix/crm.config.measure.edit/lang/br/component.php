<?
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_SECTION_MEASURE_INFO"] = "Unidade de medida";
$MESS["CRM_MEASURE_FIELD_ID"] = "ID";
$MESS["CRM_MEASURE_FIELD_IS_DEFAULT"] = "Padrão";
$MESS["CRM_MEASURE_FIELD_CODE"] = "Código";
$MESS["CRM_MEASURE_FIELD_MEASURE_TITLE"] = "Nome da unidade";
$MESS["CRM_MEASURE_FIELD_SYMBOL_RUS"] = "Símbolo da unidade";
$MESS["CRM_MEASURE_FIELD_SYMBOL_INTL"] = "Símbolo da unidade (internacional)";
$MESS["CRM_MEASURE_FIELD_SYMBOL_LETTER_INTL"] = "Nome do código (intl.)";
$MESS["CRM_MEASURE_ERR_CODE_EMPTY"] = "Por favor, especifique um código para a unidade de medida. O código deve ser um número inteiro positivo.";
$MESS["CRM_MEASURE_ERR_TITLE_EMPTY"] = "Por favor, forneça o nome para a unidade de medida.";
$MESS["CRM_MEASURE_ERR_CREATE"] = "Erro desconhecido ao criar unidade de medida.";
$MESS["CRM_MEASURE_ERR_UPDATE"] = "Erro desconhecido ao atualizar unidade de medida.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["CATALOG_MODULE_NOT_INSTALLED"] = "O módulo de Catálogo Comercial não está instalado.";
$MESS["CRM_MEASURE_ERR_CODE_INVALID"] = "O ID da unidade de medida pode incluir apenas números.";
$MESS["CRM_MEASURE_ERR_ALREADY_EXISTS"] = "Já existe uma unidade de medida com o ID \"#CODE#\".";
?>