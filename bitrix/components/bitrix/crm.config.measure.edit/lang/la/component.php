<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no esta instalado ";
$MESS["CATALOG_MODULE_NOT_INSTALLED"] = "El módulo Commercial Catalog no esta instalado ";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_SECTION_MEASURE_INFO"] = "Unidad de medida";
$MESS["CRM_MEASURE_FIELD_ID"] = "ID";
$MESS["CRM_MEASURE_FIELD_IS_DEFAULT"] = "Predeterminado";
$MESS["CRM_MEASURE_FIELD_CODE"] = "Código";
$MESS["CRM_MEASURE_FIELD_MEASURE_TITLE"] = "Nombre de la unidad";
$MESS["CRM_MEASURE_FIELD_SYMBOL_RUS"] = "Símbolo de la unidad";
$MESS["CRM_MEASURE_FIELD_SYMBOL_INTL"] = "Símbolo de la unidad (internacional)";
$MESS["CRM_MEASURE_FIELD_SYMBOL_LETTER_INTL"] = "Nombre de código (Intl.)";
$MESS["CRM_MEASURE_ERR_CODE_EMPTY"] = "Por favor, indique un código para la unidad de medida. El código debe ser un número entero positivo.";
$MESS["CRM_MEASURE_ERR_TITLE_EMPTY"] = "Por favor, indique el nombre de la unidad de medida.";
$MESS["CRM_MEASURE_ERR_CREATE"] = "Error desconocido al crear la unidad de medición.";
$MESS["CRM_MEASURE_ERR_UPDATE"] = "Error desconocido en la actualización de la unidad de medida.";
$MESS["CRM_MEASURE_ERR_CODE_INVALID"] = "El ID de unidad de medida puede incluir sólo números.";
$MESS["CRM_MEASURE_ERR_ALREADY_EXISTS"] = "Ya existe una unidad de medida con este ID \"#CODE#\".";
?>