<?
$MESS["CRM_LEAD_WGT_PAGE_TITLE"] = "Rapports analytiques sur les prospects";
$MESS["CRM_LEAD_WGT_FUNNEL"] = "Entonnoir de prospects";
$MESS["CRM_LEAD_WGT_QTY_LEAD_NEW"] = "Nombre de nouvelles prospects";
$MESS["CRM_LEAD_WGT_QTY_LEAD_IN_WORK"] = "Nombre de prospects actives";
$MESS["CRM_LEAD_WGT_QTY_LEAD_SUCCESSFUL"] = "Nombre de prospects converties";
$MESS["CRM_LEAD_WGT_QTY_LEAD_FAILED"] = "Nombre de mauvaises prospects";
$MESS["CRM_LEAD_WGT_QTY_LEAD_IDLE"] = "Nombre de prospects oisives";
$MESS["CRM_LEAD_WGT_EMPLOYEE_LEAD_PROC"] = "Efficacité du traitement des prospects par les employés";
$MESS["CRM_LEAD_WGT_QTY_ACTIVITY"] = "Nombre d'activités";
$MESS["CRM_LEAD_WGT_QTY_CALL"] = "Nombre d'appels";
$MESS["CRM_LEAD_WGT_DATE_NEW_LEAD"] = "Dynamiques de traitement des nouvelles prospects";
$MESS["CRM_LEAD_WGT_RATING"] = "Classement des prospects converties";
$MESS["CRM_LEAD_WGT_SOURCE"] = "Sources des prospects";
$MESS["CRM_LEAD_WGT_CONVERSION_SUCCESS"] = "Conversion";
$MESS["CRM_LEAD_WGT_CONVERSION_FAIL"] = "Perdu";
$MESS["CRM_LEAD_WGT_DEMO_TITLE"] = "Ceci est un affichage de démonstration. Masquez-le pour accéder à l'analytique de vos prospects.";
$MESS["CRM_LEAD_WGT_DEMO_CONTENT"] = "Vous n'avez pas encore de prospect. <a href=\"#URL#\" class=\"#CLASS_NAME#\">Créez-en une</a> maintenant !";
$MESS["CRM_LEAD_WGT_PAGE_TITLE_SHORT"] = "Rapport des pistes";
?>