<?
$MESS["CRM_LEAD_VAR"] = "Le nom de la variable de l'identifiant du prospect";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "Modèle de chemin d'accès à la page principale";
$MESS["CRM_SEF_PATH_TO_LIST"] = "Modèle de chemin d'accès à la page de la liste des prospects";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "Modèle de chemin d'accès à la la page d'édition d'un prospect";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "Modèle de chemin d'accès à la page de visualisation du prospect";
$MESS["CRM_SEF_PATH_TO_CONVERT"] = "Modèle de chemin d'accès à la page de conversion des prospects";
$MESS["CRM_SEF_PATH_TO_IMPORT"] = "Modèle de chemin d'accès à la page d'importation des prospects";
$MESS["CRM_SEF_PATH_TO_SERVICE"] = "Modèle de chemin d'accès à la page du service WEB";
$MESS["CRM_ELEMENT_ID"] = "ID du prospect";
$MESS["CRM_NAME_TEMPLATE"] = "Format du nom";
?>