<?
$MESS["VI_REGULAR_TITLE"] = "Pagos recurrentes";
$MESS["VI_REGULAR_TABLE_NUMBER"] = "Número";
$MESS["VI_REGULAR_TABLE_PAID_BEFORE"] = "Pagar antes del";
$MESS["VI_REGULAR_TABLE_FEE"] = "Cuota mensual";
$MESS["VI_REGULAR_TABLE_STATUS_Y"] = "Permitir";
$MESS["VI_REGULAR_TABLE_STATUS_N"] = "Deshabilitado, se requiere pago";
$MESS["VI_REGULAR_FEE_RUR"] = "RUB #MONEY#";
$MESS["VI_REGULAR_FEE_EUR"] = "EUR #MONEY#";
$MESS["VI_REGULAR_FEE_USD"] = "\$#MONEY#";
$MESS["VI_REGULAR_NO_MONEY"] = "Usted no tiene suficiente crédito para pagos recurrentes automáticos. Usted necesita recargar su saldo hasta #DATE#.";
$MESS["VI_REGULAR_CONFIG_RENT"] = "Gestión de números alquilados";
$MESS["VI_REGULAR_NOTICE"] = "Atención: la prolongación automática de números de teléfono está ocupada.";
$MESS["VI_REGULAR_FEE_UAH"] = "UAH #MONEY#.";
$MESS["VI_REGULAR_NO_VERIFY"] = "La ley requiere que se proporcione documentación legal para utilizar los números alquilados.<br><br>Usted tiene que cargar documentos hasta #DATE#, o su número será desconectado. <br><br>#URL_START#Cargar documentos ahora#URL_END#";
?>