<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "O módulo Processos de Negócio não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_BP_ENTITY_LIST"] = "Tipos";
$MESS["CRM_BP_LEAD"] = "Lead";
$MESS["CRM_BP_LEAD_DESC"] = "Modelos de processos de negócio do lead";
$MESS["CRM_BP_DEAL"] = "Negócio";
$MESS["CRM_BP_DEAL_DESC"] = "Modelos de processos de negócios do negócio";
$MESS["CRM_BP_CONTACT"] = "Contato";
$MESS["CRM_BP_CONTACT_DESC"] = "Modelos de processos de negócios para \"Contatos\"";
$MESS["CRM_BP_COMPANY"] = "Empresa";
$MESS["CRM_BP_COMPANY_DESC"] = "Modelos de processos de negócios para \"Empresas\"";
?>