<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "Le module bizproc n'a pas été installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_BP_ENTITY_LIST"] = "Liste de modèles";
$MESS["CRM_BP_LEAD"] = "Prospect";
$MESS["CRM_BP_LEAD_DESC"] = "Modèles des processus business pour les 'Prospects'";
$MESS["CRM_BP_DEAL"] = "Affaire";
$MESS["CRM_BP_DEAL_DESC"] = "Modèles des processus business pour les 'Transactions'";
$MESS["CRM_BP_CONTACT"] = "Client";
$MESS["CRM_BP_CONTACT_DESC"] = "Modèles de processus d'affaires pour 'Contacts'";
$MESS["CRM_BP_COMPANY"] = "Entreprise";
$MESS["CRM_BP_COMPANY_DESC"] = "Modèles de procédures d'entreprise pour 'Entreprises'";
?>