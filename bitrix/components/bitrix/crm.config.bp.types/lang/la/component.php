<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "El módulo Business Processes no está instalados.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_BP_ENTITY_LIST"] = "Lista";
$MESS["CRM_BP_LEAD"] = "Prospecto";
$MESS["CRM_BP_LEAD_DESC"] = "Plantillas del business process de prospectos";
$MESS["CRM_BP_DEAL"] = "Negociación";
$MESS["CRM_BP_DEAL_DESC"] = "Plantillas del business process de la negociación";
$MESS["CRM_BP_CONTACT"] = "Contacto";
$MESS["CRM_BP_CONTACT_DESC"] = "Plantillas de business process para los \"Contactos\"";
$MESS["CRM_BP_COMPANY"] = "Compañía";
$MESS["CRM_BP_COMPANY_DESC"] = "Plantillas de business process para los \"Contactos\"";
?>