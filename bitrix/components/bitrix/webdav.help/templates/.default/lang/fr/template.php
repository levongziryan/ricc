<?
$MESS["WD_HELP_BPHELP_TEXT"] = "<p><b>Annotation</b>: pour l'information détaillée sur les processus business veuillez vous adresser à la page<a href='#LINK#' target='_blank'>Processus business</a>.</p>";
$MESS["WD_HELP_FULL_TEXT"] = "On peut travailler avec la bibliothèque de documents de deux façons: via le navigateur (Internet Explorer, Opera, FireFox etc..) ou via WebDAV- clients du système d'exploitation (pour le système d'exploitation Windows: composant de dossiers WEB, connexion du disque). <br><br>
<ul>
<li><b><a href='#iewebfolder'>Travail avec la bibliothèque de documents dans le navigateur WEB</a></b></li>
<li><b><a href='#ostable'>Table des comparaisons des applications WebDAV </a></b></li>
<li><b><a href='#oswindows'>Connexion de la bibliothèque de documents dans le système d'exploitation Windows</a></b></li>
<ul>
<li><a href='#oswindowsnoties'>Limitations du système d'exploitation Windows</a></li>
<li><a href='#oswindowsreg'>Autorisation de s'autoriser sans https</a></li>
<li><a href='#oswindowswebclient'>Relancement du service client WEB/a></li>
<li><a href='#oswindowsfolders'>Connexion via le composant des dossiers WEB</a></li>
<li><a href='#oswindowsmapdrive'>Connexion du disque réseau </a></li>

</ul>
<li><b><a href='#osmacos'>Connexion dans la bibliothèque dans le système d'exploitation Mac, Mac OS X</a></b></li>
<li><b><a href='#maxfilesize'>Agrandissement de la taille minimale des fichiers à charger</a></b></li>
</ul>


<h2><a name='browser'></a>Travail avec la bibliothèque de documents dans le navigateur WEB </h2>
<h4><a name='upload'></a>Chargement de documents</h4>
<p>Pour effectuer le chargement, passez dans le dossier où il faut charger les documents. Cliquez sur la touche <b>Charger</b> sur la Barre contextuelle:</p>
<p><img src='#TEMPLATEFOLDER#/images/load_contex_panel.png' border='0'/></p>
<p>On verra s'ouvrir une forme pour charger les fichiers qui a plusieurs représentations:</p>
<ul>
<li><b>Un chargement isolé </b> - permet de charger facilement et rapidement un document;</li>
<li><b>ordinaire</b> - permet d'effectuer le chargement par fichier de documents depuis différents répertoire (touche <b>Ajouter les fichiers</b>) ou charger des documents d'un dossier (touche <b>Ajouter le dossier</b>);</li>
<li><b>classique</b> - sert à charger les documents depuis un répertoire bien déterminé;</li>
<li><b>simple</b> - sert au chargement individuel de chaque document.</li>
</ul>

<p>Sélectionnez le type de forme qui vous convient et indiquez les documents à charger.</p>
<p><a href='javascript: ShowImg('#TEMPLATEFOLDER#/images/load_form.png',661,575,'Forme pour le chargement des documents');'>
<img src='#TEMPLATEFOLDER#/images/load_form_sm.png' style='CURSOR: pointer' width='300' height='261' alt='Cliquez sur l'image pour agrandir' border='0'/></a></p>

<p>Cliquez sur la touche <b>Charger</b>.</p>

<br/>
<h4><a name='bizproc'></a>Lancement du processus d'affaires</h4>

<p>Dans certains cas, il est nécessaire d'exécuter telle ou telle opération avec un document chargé. Par exemple, valider ou concerter le document. On utilise à cette fin des processus d'affaires</p>

<p>Pour lancer un processus d'affaires, sélectionnez dans le menu contextuel le point <b>Nouveau processus d'affaires</b>:</p>
<p><a href='javascript:ShowImg('#TEMPLATEFOLDER#/images/new_bizproc.png',622,459,'Lancement du processus d'affaires ');'>
<img src='#TEMPLATEFOLDER#/images/new_bizproc_sm.png' style='CURSOR: pointer' alt='Cliquez sur l'image pour agrandir ' border='0'/></a></p>
#BPHELP#
<p>Pour passer à la gestion des modèles de processus d'affaires, cliquez sur la touche <b>Processus d'affaires</b> sur la barre contextuelle:</p>

<br/>
<h4><a name='delete'></a>Modification, élimination des documents</h4>
<p>La gestion des documents est réalisée soit au moyen du menu contextuel:
<p><a href='javascript:ShowImg('#TEMPLATEFOLDER#/images/delete_file.png',399,516,'Modification, élimination des documents');'>
<img src='#TEMPLATEFOLDER#/images/delete_file_sm.png' style='CURSOR: pointer' alt='Cliquez sur l'image pour agrandir ' border='0'/></a></p>
soit au moyen de la barre d'actions en groupe situé sous la liste des documents.
<br/><br/>

<br>
<h2><a name='ostable'></a>Table des comparaisons des applications WebDAV client</h2>

<p>
<div style='border:1px solid #ffc34f; background: #fffdbe; padding:1em;'>
<table cellpadding='0' cellspacing='0' border='0'>
<tr>
<td style='border-right:1px solid #FFDD9D; padding-right:1em;'>
<img src='/bitrix/components/bitrix/webdav.help/templates/.default/images/help.png' width='20' height='18' border='0'/>
</td>
<td style='padding-left:1em;'>
Si vous utilisez une application WebDAV client pour gérer la bibliothèque en cas de <b>traitement de documents</b> ou de <b>les processus d'affaires</b>, certaines limitations s'imposent: <br/><br/>
1. on ne doit pas lancer un processus d'affaires pour ce document; <br/>
2. on ne doit pas charger, modifier les documents, si au lancement automatique sont destinés des processus d'affaires avec paramètres de lancement automatique sans valeurs par défaut; <br/>
3. suivre l'historique du document.
</td>
</tr>
</table>
</div>
</p>


<table cellpadding='0' cellspacing='0' border='0' width='100%' class='wd-main data-table'>
<thead>
<tr class='wd-row'>
<th class='wd-cell'>WebDAV-client</th>
<th class='wd-cell'>Autorisation<br />Windows (IWA)</th>
<th class='wd-cell'>Digest<br /> autorisation<br />(Digest)</th>
<th class='wd-cell'>Autorisation<br />de base (Basic)</th>
<th class='wd-cell'>SSL</th>
<th class='wd-cell'>port</th>
</tr>
</thead>
<tbody>
<tr>
<td><a href='#oswindowsfolders'><u>Web&ndash;dossier</u></a>, Windows 7</td>
<td>+</td>
<td>+</td>
<td>+&nbsp;<sup><small><a title='Il faut apporter des modifications au registre' href='#osnote2'>[2]</a></small></sup></td>
<td>&ndash;&nbsp;<sup><small><a title='Non soutenu par le système d'exploitation' href='#osnote1'>[1]</a></small></sup></td>
<td>???</td>
</tr>
<tr>
<td><a href='#oswindowsfolders'><u>???&ndash;dossier</u></a>, Vista SP1</td>
<td>+</td>
<td>+</td>
<td>+&nbsp;<sup><small><a title='Il faut apporter des modifications au registre' href='#osnote2'>[2]</a></small></sup></td>
<td>+</td>
<td>???</td>
</tr>
<tr>
<td><a href='#oswindowsfolders'><u>WEB&ndash; dossier</u></a>, Windows XP</td>
<td>+</td>
<td>&ndash;&nbsp;<sup><small><a title='Non soutenu par le système d'exploitation' href='#osnote1'>[1]</a></small></sup></td>
<td>+&nbsp;<sup><small><a title='Il faut apporter des modifications au registre' href='#osnote2'>[2]</a></small></sup></td>
<td>+</td>
<td>???</td>
</tr>
<tr>
<td><a href='#oswindowsfolders'><u>Web&ndash;?????</u></a>, Windows 2003/2000
<sup><small><a title= 'Non installé par défaut dans le système d'exploitation' href='#osnote3'>[3]</a></small></sup></td>
<td>+</td>
<td>&ndash;&nbsp;<sup><small><a title='Non soutenu par le système d'exploitation' href='#osnote1'>[1]</a></small></sup></td>
<td>+&nbsp;<sup><small><a title=''Il faut apporter des modifications au registre'  href='#osnote2'>[2]</a></small></sup></td>
<td>+</td>
<td>???</td>
</tr>
<tr>
<td><a href='#oswindowsfolders'><u>???&ndash;?????</u></a>, Windows Server 2008
<sup><small><a title= ''Non installé par défaut dans le système d'exploitation' href='#osnote3'>[3]</a></small></sup></td>
<td>+</td>
<td>+</td>
<td>+&nbsp;<sup><small><a title='Il faut apporter des modifications au registre'  href='#osnote2'>[2]</a></small></sup></td>
<td>+</td>
<td>???</td>
</tr>
<tr>
<td><a href='#oswindowsmapdrive'><u>Disque réseau</u></a>, Windows 7</td>
<td>+</td>
<td>+</td>
<td>+&nbsp;<sup><small><a title='Il faut apporter des modifications au registre'  href='#osnote2'>[2]</a></small></sup></td>
<td>+</td>
<td>???</td>
</tr>
<tr>
<td><a href='#oswindowsmapdrive'><u>Disque réseau</u></a>, Vista SP1</td>
<td>+</td>
<td>+</td>
<td>+&nbsp;<sup><small><a title='Il faut apporter des modifications au registre'  href='#osnote2'>[2]</a></small></sup></td>
<td>+</td>
<td>???</td>
</tr>
<tr>
<td><a href='#oswindowsmapdrive'><u>Disque réseau</u></a>, Windows XP</td>
<td>+</td>
<td>&ndash;&nbsp;<sup><small><a title='Non soutenu par le système d'exploitation'  href='#osnote1'>[1]</a></small></sup></td>
<td>&ndash;&nbsp;<sup><small><a title='Non soutenu par le système d'exploitation'  href='#osnote1'>[1]</a></small></sup></td>
<td>&ndash;&nbsp;<sup><small><a title='Non soutenu par le système d'exploitation'  href='#osnote1'>[1]</a></small></sup></td>
<td>80</td>
</tr>
<tr>
<td><a href='#oswindowsmapdrive'><u>Disque réseau/u></a>, Windows 2003/2000</td>
<td>+</td>
<td>&ndash;&nbsp;<sup><small><a title='Non soutenu par le système d'exploitation'  href='#osnote1'>[1]</a></small></sup></td>
<td>&ndash;&nbsp;<sup><small><a title='Non soutenu par le système d'exploitation' href='#osnote1'>[1]</a></small></sup></td>
<td>&ndash;&nbsp;<sup><small><a title='Non soutenu par le système d'exploitation'  href='#osnote1'>[1]</a></small></sup></td>
<td>80</td>
</tr>
<tr>
<td>MS Office 2007/2003/XP</td>
<td>+</td>
<td>+</td>
<td>+</td>
<td>+</td>
<td>tous</td>
</tr>
<tr>
<td>MS Office 2010</td>
<td>+</td>
<td>+</td>
<td>+&nbsp;<sup><small><a title='Il faut apporter des modifications au registre'   href='#osnote2'>[2]</a></small></sup></td>
<td>+</td>
<td>tous</td>
</tr>
<tr>
<td><a href='#osmacos'><u>MAC OS X</u></a></td>
<td>&ndash;&nbsp;<sup><a title='Non soutenu par le système d'exploitation'  href='#osnote1'>[1]</a></sup></td>
<td>+</td>
<td>+</td>
<td>+</td>
<td>tous</td>
</tr>
</tbody>
</table>
<br />
<p>
<b>Remarques:</b>
<ol>
<li><a name='osnote1'></a>'Non soutenu par le système d'exploitation'.</li>
<li><a name='osnote2'></a>Pour activer le soutien dans le système d'opération, il faut <a href='#oswindowsreg'>apporter des modifications au registre</a>.</li>
<li><a name='osnote3'></a>Le service <i>Client WEB </i> (<i>WebClient</i>) n'est pas installé par défaut dans le système d'exploitation.
Il faut installer <i>les Suppléments</i> (<i>Features</i>) de la façon suivante:
<ul>
<li><i>Start -> Administrative Tools -> Server Manager -> Features</i></li>
<li>Cliquer à droite, en haut sur la touche <i>Add Features</i></li>
<li>Sélectionner<i>Desktop Experience</i>, installer</li>
</ul>
</li>
</ol>
</p>
<br>
<h2><a name='oswindows'></a>Connexion de la bibliothèque de documents dans le système d'exploitation Windows</h2>
<h4><a name='oswindowsnoties'></a>Limitations du système d'exploitation Windows</h4>
<div style='border:1px solid #ffc34f; background: #fffdbe; padding:1em;'>
<table cellpadding='0' cellspacing='0' border='0'>
<tr>
<td style='border-right:1px solid #FFDD9D; padding-right:1em;'>
<img src='/bitrix/components/bitrix/webdav.help/templates/.default/images/help.png' width='20' height='18' border='0'/>
</td>
<td style='padding-left:1em;'>
<p>Dans <b>Windows 7</b> le composant dossiers WEB ne fonctionne pas selon un protocole protégé. Pour travailler avec la bibliothèque depuis Windows 7, vous devez travailler selon le protocole HTTP. </p>
<p>Dans <b>Windows XP</b> il est toujours nécessaire d'indiquer le numéro de port, même si l'on utilise le port 80 (http://servername:80/).</p>
<p><b>Avant de connecter la bibliothèque de documents, assurez-vous que le service client WEB (WebClient) est lancé.</b></p>
</td>
</tr>
</table>
</div>

<h4><a name='oswindowsreg'>Autorisation de s'autoriser sans https</a></h4>
<p><b>Premièrement</b>, il faut modifier le paramètre <b>Basic authentification</b> dans le registre du système d'exploitation Windows: </p>
<ul>
 <li><a href='/bitrix/webdav/xp.reg'>adapter le registre </a> à <b>Windows XP, Windows 2003 Server</b></li>
 <li><a href='/bitrix/webdav/vista.reg'> adapter le registre </a> à <b>Windows 7, Vista, Windows 2008 Server</b></li>
 <li><a href='/bitrix/webdav/office14.reg'> adapter le registre </a> à <b>Microsoft Office 2010</b></li>
</ul>
<p>Cliquez sur la touche <b>Lancer</b> dans la fenêtre de chargement du fichier, cliquez sur <b>Oui</b>: dans le dialogue <b>de l'éditeur de fichier</b> avec avertissement sur l'infidélité de la source
Cliquez sur 
<p><a href='javascript: ShowImg('#TEMPLATEFOLDER#/images/vista_reg_2.png',572,212,'Dialogue du système d'exploitation <img src='#TEMPLATEFOLDER#/images/vista_reg_2_sm.png' style='CURSOR: pointer' width='250' height='93' alt='Cliquez sur l'image pour agrandir' border='0'/></a></p>
<p>Si vous utilisez un navigateur qui ne permet pas de lancer les fichiers.reg, il faut télécharger le fichier et lancer ou modifier le registre à la main au moyen du<b>Editeur de base de registre</b>.</p>
<p><b>Modification des paramètres au moyen de l'Editeur de base de registre </b></p>
<p>Exécutez l'instruction: <b>Lancement &gt; Exécuter</b>. On verra s'ouvrir la fenêtre <b>Lancement du programme</b>:</p>

<p><img src='#TEMPLATEFOLDER#/images/regedit.png' width='347' height='179' border='0'/></a></p>

<p>Dans le champ <b>Ouvrir</b> saisissez<b>regedit</b> et cliquez sur la touche <b>??</b>.</p>
<p>Pour <b>Windows XP, Windows 2003 Server</b> il faut modifier la valeur du paramètre pour:</p>
<p></p>
 <table cellspacing='0' cellpadding='0' border='1'>
 <tbody>
 <tr><td width='638' valign='top'>
 <p>[HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\WebClient\\Parameters] &quot;UseBasicAuth&quot;=dword:00000001</p>
 </td></tr>
 </tbody>
 </table>
<p></p>
<p>pour <b>Windows 7, Vista, Windows 2008 Server</b> il faut modifier la valeur du paramètre ou créer une inscription dans le registre:</p>
<table cellspacing='0' cellpadding='0' border='1'>
<tbody>
<tr><td width='638' valign='top'>
<p>[HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\WebClient\\Parameters]
<br />
&quot;BasicAuthLevel&quot;=dword:00000002</p>
</td></tr>
</tbody>
</table>

<p><b>Deuxièmement</b>, il faut relancer le service<a href='#oswindowswebclient'><b>Client WEB (WebClient)</b></a>.</p>
<h4><a name='oswindowswebclient'></a><b>Relancement du service client WEB</b></h4>
<p>Pour relacer, suivez: <b>???? &gt; Barre de commande &gt; Management &gt; Services</b>. On verra s'ouvrir le dialogue <b>Services</b>:
<p><a href='javascript: ShowImg('#TEMPLATEFOLDER#/images/web_client.png',638,450,'Services');'>
<img src='#TEMPLATEFOLDER#/images/web_client_sm.png' style='CURSOR: pointer' width='300' height='212' alt='Cliquez sur l'image pour agrandir' border='0'/></a></p>
<p>Trouvez sur la liste générale de services la ligne <b>Client WE? (WebClient)</b>, relacez (lancez). Pour que le service démarre ultérieurement au démarrage du système d'exploitation, il faut entrer dans les propriété du service la valeur du paramètre <b>Type de lancement</b> dans <b>?utomatique</b>:
<p><a href='javascript: ShowImg ('#TEMPLATEFOLDER#/images/web_client_prop.png',410,461,'Propriété du service client WEB');'>
<img src='#TEMPLATEFOLDER#/images/web_client_prop_sm.png' style='CURSOR: pointer' width='205' height='230' alt=' Cliquez sur l'image pour agrandir ' border='0'/></a></p></li>
<p>On peut procéder en direct à la connexion du dossier.</p>

<h4><a name='oswindowsfolders'>Connexion via le composant dossiers WEB (web-folders)</a></h4>
<div style='border:1px solid #ffc34f; background: #fffdbe;padding:1em;'>
<table cellpadding='0' cellspacing='0' border='0'>
<tr>
<td style='border-right:1px solid #FFDD9D; padding-right:1em;'>
<img src='/bitrix/components/bitrix/webdav.help/templates/.default/images/help.png' width='20' height='18' border='0'/>
</td>
<td style='padding-left:1em;'>
Dans <b>Windows 7</b> la connexion selon le protocole protégé HTTPS/SSL.<br> ne fonctionne pas.
Dans <b>Windows 2003 Server</b> le composant dossiers WEB n'est pas installé. Il faut installer le composant dossiers WEB ( <a href='http://www.microsoft.com/downloads/details.aspx?displaylang=ru&FamilyID=17c36612-632e-4c04-9382-987622ed1d64' target='_blank'>passer sur le site Microsoft</a> ).
</td>
</tr>
</table>
</div>
<p>Avant de connecter la bibliothèque de documents, assurez-vous que <a href='#oswindowsreg'> des modifications sont apportées au registre</a> et que <a href='#oswindowswebclient'> le service client WEB (WebClient) est lancé.</a></p>
<p>Pour la connexion à la bibliothèque de documents par ce procédé, on a besoin du composant dossiers WEB. Il est souhaitable d'installer la version la plus récente de logiciel pour les dossiers WEB ( <a href='http://www.microsoft.com/downloads/details.aspx?displaylang=ru&FamilyID=17c36612-632e-4c04-9382-987622ed1d64' target='_blank'>passer sur le site Microsoft</a> ) sur l'ordinateur client. </p>
<p>Cliquez sur la touche <b>Connecter</b>, située sur la barre d'outils. </p>
<p><img border='0' src='#TEMPLATEFOLDER#/images/load_contex_panel.png' /></p>
<p>Dans le dialogue qui s'ouvrira vous verrez les procédés de connexion accessibles pour votre navigateur et votre système d'exploitation.</p>
<p><a href='javascript:ShowImg('#TEMPLATEFOLDER#/images/connection.png',719,490,'connexion');'>
<img src='#TEMPLATEFOLDER#/images/connection_sm.png' style='CURSOR: pointer' alt='Cliquez sur l'image pour agrandir' border='0'/></a></p>
<p>Si vous n'utilisez pas Si vous n'utilisez pas <b>Internet Explorer</b> ou si la bibliothèque n'a pas été ouverte comme dossier WEB une fois la touche cliquée dans la fenêtre de dialogue, effectuer les actions ci-dessous:</p>
<ul>
<li>Lancez <b>Explorateur</b></li>
<li>Sélectionnez dans le menu le point <b>Service &gt; Connecter le disque réseau</b></li>
<li>En suivant le lien<b>s'abonner au stockage sur Internet ou se connecter au serveur réseau</b> lancez <b>Assistant à l'ajout d'un emplacement réseau</b>:</p>
<p><a href='javascript: ShowImg ('#TEMPLATEFOLDER#/images/network_add_1.png',447,322,'Connexion du disque réseau');'>
<img width='250' height='180' border='0' src='#TEMPLATEFOLDER#/images/network_add_1_sm.png' style='cursor: pointer;' alt='Cliquez sur l'image pour agrandir' /></a></li>
<li>Cliquez sur la touche <b>Suivre</b>, on verra s'ouvrir une deuxième fenêtre <b>Assistants</b></li>
<li>Dans cette fenêtre activez la position <b>Choisissez un autre emplacement réseau</b> et cliquez sur la touche <b>Suivre</b>. On verra s'ouvrir le pas suivant <b>Assistants</b>:
<p><a href='javascript: ShowImg ('#TEMPLATEFOLDER#/images/network_add_4.png',563,459,'Ajout d'un emplacement réseau: Pas 3');'>
<img width='250' height='204' border='0' src='#TEMPLATEFOLDER#/images/network_add_4_sm.png' style='cursor: pointer;' alt='Cliquez sur l'image pour agrandir' /></a></li>
<li>Dans le champ <b>Adresse dans le réseau ou adresse sur Internet</b> entrez l'URL du dossier à connecter du type: <i>http://<votre serveur>/docs/shared/</i>.</li>
<li>Cliquez sur la touche <b>Suivre </b>. Si la fenêtre pour autorisation apparaît, entrez y les données pour l'autorisation sur le serveur.</li>
</ul>

<p>Pour ouvrir le dossier ultérieurement, exécutez l'instruction: <b>Lancement > Environnement réseau > Nom de dossier</b>.</p>

<br />
<br />

<h4><a name='oswindowsmapdrive'></a>Connexion du disque réseau </h4>
<div style='border:1px solid #ffc34f; background: #fffdbe; padding:1em;'>
<table cellpadding='0' cellspacing='0' border='0'>
<tr>
<td style='border-right:1px solid #FFDD9D; padding-right:1em;'>
<img src='/bitrix/components/bitrix/webdav.help/templates/.default/images/help.png' width='20' height='18' border='0'/>
</td>
<td style='padding-left:1em;'>
<b>Attention !</b> dans le système d'exploitation <b>Windows XP and Windows Server 2003</b> la connexion au protocole protégé HTTPS/SSL. ne fonctionne pas 
</td>
</tr>
</table>
</div>
<p>Pour connecter la bibliothèque comme disque réseau dans <b>Windows 7</b> selon le protocole protégé <b>HTTPS/SSL</b>: ex& Exécutez l'instruction <b>Lancement &gt; Exécuter &gt; cmd</b>. Dans la ligne de commande entrez:<br>
<table cellspacing='0' cellpadding='0' border='1'>
<tbody>
<tr><td width='638' valign='top'>
<p>net use z: https://&lt; votre _ serveur &gt;/docs/shared/ /user:&lt;userlogin&gt; *</p>
</td></tr>
</tbody>
</table>
<br>

<p>Pour connecter la bibliothèque comme disque réseau au moyen <b>de l'explorateur</b>:
<ul>
<li>Lancez<b>Explorateur.</b></li>
<li>Sélectionnez dans le menu le point <i>Service > Connecter le disque réseau</i>. On verra s'ouvrir le dialogue pour la connexion du disque réseau:
<br><a href='javascript: ShowImg('#TEMPLATEFOLDER#/images/network_storage.png',628,465,'Connexion du disque réseau;'>
<img width='250' height='185' border='0' src='#TEMPLATEFOLDER#/images/network_storage_sm.png' style='cursor: pointer;' alt='Cliquez sur l'image pour agrandir' /></a></li>
<li>Dans le champ <b>Disque</b> désignez la lettre pour le dossier à connecter.</li>
<li>Dans le champ <b>Dossier</b>entrez le chemin vers la bibliothèque: <i>http://&lt;votre_serveur&gt;/docs/shared/</i>. S'il est nécessaire que le dossier puisse être connecté pour consultation à chaque lancement du système, installez le drapeau <b>Restaurer à l'entrée dans le système </b>.</li>
<li>Cliquez sur <b>C'est fait</b>. Si le dialogue du système d'exploitation pour autorisation s'ouvre, entrez y des données pour l'autorisation sur le serveur.</li>
</ul>
</p>
<p>Les ouvertures ultérieures du dossier pourront être réalisées soit via <b>Explorateur Windows</b>, où le dossier est visualisé sous forme d'un disque à part, soit via n'importe quel gestionnaire de fichier.</p>

<h2><a name='osmacos'></a>Connexion à la bibliothèque dans le système d'exploitation Mac, Mac OS X</h2>

<p>Pour la connexion:</p>

<ul>
<li>Ouvrez <i>Finder Go->Connect to Server command</i>.</li>
<li>Entrez l'adresse de la bibliothèque dans le champ <b>Server Address</b>:</p>
<p><a href='javascript:ShowImg('#TEMPLATEFOLDER#/images/macos.png',465,550,'Mac OS X');'>
<img width='235' height='278' border='0' src='#TEMPLATEFOLDER#/images/macos_sm.png' style='cursor: pointer;' alt='Cliquez sur l'image pour agrandir' /></a></li>
</ul>
<br />

<h2><a name='maxfilesize'></a>Agrandissement de la taille minimale des fichiers à charger</h2>

<p>La taille maximale du fichier à charger ce sont les valeurs minimales des variables PHP (<b>upload_max_filesize</b> ? <b>post_max_size</b>) et les paramètres de réglage des composants.</p>
<p>Si vous voulez augmenter le quota qui excède les valeurs recommandées, apportez les modifications suivantes <b>php.ini</b>:</p>

<table cellspacing='0' cellpadding='0' border='1'>
 <tbody>
 <tr><td width='638' valign='top'>
<p>upload_max_filesize = valeur_ souhaitée;
<br/>post_max_size = excède _la taille_upload_max_filesize;</p>
 </td></tr>
 </tbody>
</table>

<p>Si vous louez la plateforme (hébergement virtuel), apportez les modifications au fichier <b>.htaccess</b>:</p>

<table cellspacing='0' cellpadding='0' border='1'>
 <tbody>
 <tr><td width='638' valign='top'>
<p>php_value upload_max_filesize valeur_souhaitée <br/>
php_value post_max_size excède_la taille_upload_max_filesize</p>
 </td></tr>
 </tbody>
</table>

<p>Il est possible que vous soyez obligé de demander à l'hébergeur d'augmenter les valeurs minimales des variables PHP (<b>upload_max_filesize</b> ? <b>post_max_size</b>).</p>
<p>Une fois les quotas PHP augmentés, il faudra apporter des modifications aux réglages des composants.</p>";
?>