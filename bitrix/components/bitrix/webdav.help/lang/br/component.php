<?
$MESS["WD_TITLE"] = "Ajudar";
$MESS["W_IBLOCK_IS_NOT_INSTALLED"] = "Módulo de blocos de informação não foi instalado";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "O módulo WebDav não está instalado";
$MESS["WD_DESCRIPTION"] = "Descrição";
$MESS["WD_DISK_NOTIFY_FILE_NUMERAL_2_4"] = "#COUNT# arquivos";
$MESS["WD_CLOSE"] = "Fechar";
?>