<?
$MESS["T_IBLOCK_DESC_NEWS_PANEL"] = "Adicionar botões para controlar Toolbar Painel";
$MESS["WD_IBLOCK_ID"] = "Bloco de informação";
$MESS["WD_BASE_URL"] = "URL da biblioteca para mapeamento de unidades de rede (caminho completo)";
$MESS["WD_SECTION_ID"] = "ID da seção";
$MESS["WD_IBLOCK_TYPE"] = "Tipo de bloco de informação";
$MESS["WD_VERSIONS"] = "Versões";
$MESS["WD_DELETE_FILE_ALT2"] = "Excluir versão";
$MESS["WD_NO_LIBRARIES"] = "Bibliotecas de documentos não disponíveis";
$MESS["WD_MENU_EDIT_IN"] = "Editar em";
$MESS["W_TITLE_ACTIVE"] = "Ativo";
?>