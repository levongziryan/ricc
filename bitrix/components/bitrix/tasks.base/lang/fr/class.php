<?
$MESS["TASKS_TB_TASKS_MODULE_NOT_INSTALLED"] = "Le module Tasks n'est pas installé.";
$MESS["TASKS_TB_TASKS_MODULE_NOT_AVAILABLE"] = "Le module Tasks n'est pas disponible dans cette édition.";
$MESS["TASKS_TB_USER_NOT_AUTHORIZED"] = "Vous ne disposez pas des permissions nécessaires pour voir les tâches";
?>