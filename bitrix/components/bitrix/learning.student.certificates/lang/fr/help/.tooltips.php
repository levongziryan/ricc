<?
$MESS["SET_TITLE_TIP"] = "Lorsque cette option est sélectionnée, <b>Rapport sur les cours</b> sera utilisée comme en-tête de la page.";
$MESS["COURSE_DETAIL_TEMPLATE_TIP"] = "Chemin vers la page avec la revue détaillée du cours.";
$MESS["TESTS_LIST_TEMPLATE_TIP"] = "Chemin vers la page d'une liste des tests du cours.";
?>