<?
$MESS["CRM_EVENT_ADD_TITLE"] = "Registro de llamadas";
$MESS["CRM_PHONE_LIST"] = "Teléfono";
$MESS["CRM_EVENT_ADD_BUTTON"] = "Agregar";
$MESS["CRM_EVENT_DESC_TITLE"] = "Descripción de la conversación";
$MESS["CRM_EVENT_ADD_FILE"] = "Adjuntar archivo";
$MESS["CRM_EVENT_TITLE_COMPANY"] = "Compañía";
$MESS["CRM_EVENT_TITLE_DEAL"] = "Negociación";
$MESS["CRM_EVENT_TITLE_CONTACT"] = "Contacto";
$MESS["CRM_EVENT_TITLE_LEAD"] = "Prospecto";
$MESS["CRM_EVENT_STAGE_ID"] = "Fase de una negociación";
$MESS["CRM_EVENT_STATUS_ID"] = "Estados del prospecto";
$MESS["CRM_EVENT_DATE"] = "Fecha de la llamada";
$MESS["CRM_NO_PHONES"] = "no hay teléfonos encontrados";
$MESS["CRM_CALL_DESCR"] = "Contacto #NAME# #PHONE#";
$MESS["CRM_EVENT_TITLE_QUOTE"] = "Cotización";
$MESS["CRM_EVENT_QUOTE_STATUS_ID"] = "Estado de la cotización";
?>