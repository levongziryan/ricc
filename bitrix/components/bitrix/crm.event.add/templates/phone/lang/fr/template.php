<?
$MESS["CRM_EVENT_ADD_TITLE"] = "Enregistrer un nouvel appel";
$MESS["CRM_PHONE_LIST"] = "Téléphones";
$MESS["CRM_EVENT_ADD_BUTTON"] = "Ajouter";
$MESS["CRM_EVENT_DESC_TITLE"] = "Veuillez saisir la description de la conversation";
$MESS["CRM_EVENT_ADD_FILE"] = "Attacher le fichier";
$MESS["CRM_EVENT_TITLE_COMPANY"] = "Entreprise";
$MESS["CRM_EVENT_TITLE_DEAL"] = "Affaire";
$MESS["CRM_EVENT_TITLE_CONTACT"] = "Client";
$MESS["CRM_EVENT_TITLE_LEAD"] = "Prospect";
$MESS["CRM_EVENT_STAGE_ID"] = "Tape de la transaction";
$MESS["CRM_EVENT_STATUS_ID"] = "Statut du prospect";
$MESS["CRM_EVENT_DATE"] = "Date de l'appel";
$MESS["CRM_NO_PHONES"] = "les téléphones ne sont pas trouvées";
$MESS["CRM_CALL_DESCR"] = "Interlocuteur #NAME# #PHONE#";
$MESS["CRM_EVENT_TITLE_QUOTE"] = "Offre";
$MESS["CRM_EVENT_QUOTE_STATUS_ID"] = "Statut de l'offre";
?>