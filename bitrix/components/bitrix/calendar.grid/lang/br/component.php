<?
$MESS["EC_INTRANET_MODULE_NOT_INSTALLED"] = "O módulo \"Portal Intranet\" não está instalado.";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "O módulo \"blocos de informação\" não está instalado.";
$MESS["EC_IBLOCK_ACCESS_DENIED"] = "Acesso negado";
$MESS["EC_USER_NOT_FOUND"] = "O usuário não foi encontrado.";
$MESS["EC_GROUP_NOT_FOUND"] = "Grupo não foi encontrado.";
$MESS["EC_USER_ID_NOT_FOUND"] = "Não é possível mostrar o calendário do usuário, pois o ID do usuário não foi especificado.";
$MESS["EC_GROUP_ID_NOT_FOUND"] = "Não é possível mostrar o calendário do grupo porque o ID do grupo não foi especificado.";
?>