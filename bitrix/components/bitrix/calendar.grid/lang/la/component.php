<?
$MESS["EC_INTRANET_MODULE_NOT_INSTALLED"] = "No está instalado el módulo de \"Portal de Intranet\".";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "No está instalado el módulo de \"Bloques de información\".";
$MESS["EC_IBLOCK_ACCESS_DENIED"] = "Acceso denegado";
$MESS["EC_USER_NOT_FOUND"] = "No se encontró el usuario.";
$MESS["EC_GROUP_NOT_FOUND"] = "No se encontró el grupo.";
$MESS["EC_USER_ID_NOT_FOUND"] = "No se puede mostrar el calendario de usuario porque no se ha especificado el ID del usuario.";
$MESS["EC_GROUP_ID_NOT_FOUND"] = "No se puede mostrar el calendario de grupo porque no se ha especificado el ID de grupo.";
$MESS["EC_CALENDAR_INDEX"] = "Ïndice de eventos del calendario ";
$MESS["EC_CALENDAR_SPOTLIGHT_SYNC"] = "Sincronice sus calendarios con otros servicios y dispositivos móviles automáticamente. La sincronización funciona en ambos sentidos.";
$MESS["EC_CALENDAR_SPOTLIGHT_LIST"] = "Cambie entre varias vistas el calendario para su conveniencia. Pruebe nuestra nueva vista Programada creada para profesionales ocupados que necesitan una vista de lista de todas las reuniones y citas.";
?>