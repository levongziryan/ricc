<?
$MESS["CRM_TAB_INVOICE_EXPORT"] = "Factures - l'exportation";
$MESS["CRM_TAB_INVOICE_EXPORT_TITLE"] = "Configuration d'exportation les factures dans '1C: Enterprise'";
$MESS["CRM_TAB_INVOICE_PROF_COM"] = "Factures - le profil de la compagnie";
$MESS["CRM_TAB_INVOICE_PROF_COM_TITLE"] = "Réglage des champs à exporter vers '1C:Entreprise'";
$MESS["CRM_TAB_INVOICE_PROF_CON"] = "Factures - profil du contact";
$MESS["CRM_TAB_INVOICE_PROF_CON_TITLE"] = "Réglage des champs à exporter vers '1C:Entreprise'";
$MESS["CRM_BUTTON_SAVE"] = "Sauvegarder";
$MESS["CRM_BUTTON_SAVE_TITLE"] = "Sauvegarder les réglages d'intégration avec '1C:Entreprise'";
$MESS["CRM_BUTTON_CANCEL"] = "Annuler";
$MESS["CRM_BUTTON_CANCEL_TITLE"] = "Ne pas enregistrer les paramètres courants de l'intégration avec '1C:Entreprise'";
$MESS["CRM_CONFIGS_EXCH1C_LINK_TEXT"] = "Vers les paramètres de l'intégration avec 1C";
$MESS["CRM_CONFIGS_EXCH1C_LINK_TITLE"] = "Passage aux réglages de l'intégration avec 1C";
?>