<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_ACTIVITY_PLANNER_NO_ACTIVITY"] = "No se encontró actividad.";
$MESS["CRM_ACTIVITY_PLANNER_NO_UPDATE_PERMISSION"] = "Permisos insuficientes para editar datos de la actividad.";
$MESS["CRM_ACTIVITY_PLANNER_NO_READ_PERMISSION"] = "Permisos insuficientes para ver datos de la actividad.";
$MESS["CRM_ACTIVITY_PLANNER_NO_PROVIDER"] = "No se encontro actividad de la compañía.";
$MESS["CALENDAR_MODULE_NOT_INSTALLED"] = "El módulo \"Event Calendar\" no está instalado.";
$MESS["CRM_ACTIVITY_PLANNER_IMPORTANT_SLIDER"] = "Importante";
$MESS["CRM_ACT_EMAIL_REPLY_SET_DOCS"] = "Seleccione la negociación";
$MESS["CRM_ACT_EMAIL_REPLY_ADD_DOCS"] = "Cambiar la negociación";
?>