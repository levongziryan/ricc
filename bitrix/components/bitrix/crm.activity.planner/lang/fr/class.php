<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module GRC n'est pas installé.";
$MESS["CRM_ACTIVITY_PLANNER_NO_ACTIVITY"] = "Activité introuvable.";
$MESS["CRM_ACTIVITY_PLANNER_NO_UPDATE_PERMISSION"] = "Permissions insuffisantes pour éditer les données d'activité.";
$MESS["CRM_ACTIVITY_PLANNER_NO_READ_PERMISSION"] = "Permissions insuffisantes pour afficher les données d'activité.";
$MESS["CRM_ACTIVITY_PLANNER_NO_PROVIDER"] = "Fournisseur d'activité introuvable.";
$MESS["CALENDAR_MODULE_NOT_INSTALLED"] = "Le module \"Calendrier des événements\" n'est pas installé.";
$MESS["CRM_ACTIVITY_PLANNER_IMPORTANT_SLIDER"] = "Important";
$MESS["CRM_ACT_EMAIL_REPLY_SET_DOCS"] = "Sélectionnez la transaction";
$MESS["CRM_ACT_EMAIL_REPLY_ADD_DOCS"] = "Changer de transaction";
?>