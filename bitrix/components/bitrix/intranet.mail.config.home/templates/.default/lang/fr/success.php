<?
$MESS["INTR_MAIL_SUCCESS_MESSAGE_TITLE"] = "Félicitations ! Vos boîtes de réception ont bien été associées.";
$MESS["INTR_MAIL_SUCCESS_MESSAGE_TEXT"] = "Vous pouvez commencer à parcourir votre boîte de réception ou retourner à la page de configuration de la messagerie.";
$MESS["INTR_MAIL_SUCCESS_MAIL_GO"] = "Afficher les e-mails";
$MESS["INTR_MAIL_SUCCESS_MAIL_HOME"] = "Retourner à la configuration";
?>