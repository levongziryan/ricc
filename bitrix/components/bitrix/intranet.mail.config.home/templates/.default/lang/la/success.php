<?
$MESS["INTR_MAIL_SUCCESS_MESSAGE_TITLE"] = "¡Felicitaciones! Sus buzones de correo se han conectado correctamente.";
$MESS["INTR_MAIL_SUCCESS_MESSAGE_TEXT"] = "Ahora puede empezar a buscar en su bandeja de entrada o volver a la página de configuración de correo electrónico.";
$MESS["INTR_MAIL_SUCCESS_MAIL_GO"] = "Ver correos electrónicos";
$MESS["INTR_MAIL_SUCCESS_MAIL_HOME"] = "Volver a la configuración";
?>