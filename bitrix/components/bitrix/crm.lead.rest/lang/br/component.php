<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_REST_ERROR_BAD_REQUEST"] = "Erro de solicitação";
$MESS["CRM_REST_ERROR_BAD_AUTH"] = "Erro de autorização";
$MESS["CRM_REST_OK"] = "O lead foi adiciondo.";
$MESS["UNKNOWN_ERROR"] = "Erro desconhecido.";
?>