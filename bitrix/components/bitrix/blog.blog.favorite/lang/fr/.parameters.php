<?
$MESS["BMNP_PREVIEW_HEIGHT"] = "Hauteur de l'image pour l'affichage préalable";
$MESS["BMNP_MESSAGE_LENGTH"] = "Longueur du texte du message affiché";
$MESS["B_VARIABLE_ALIASES"] = "Alias des variables";
$MESS["BB_BLOG_VAR"] = "Nom de la variable pour l'identificateur de l'utilisateur";
$MESS["BB_POST_VAR"] = "Le nom de la variable pour l'identificateur du message du blog";
$MESS["BB_PAGE_VAR"] = "Nom de la variable pour la page";
$MESS["BMNP_MESSAGE_COUNT"] = "Nombre de messages affichés";
$MESS["BB_BLOG_URL"] = "Adresse du blogue à afficher";
$MESS["BB_PATH_TO_SMILE"] = "Chemin vers le dossier avec les smileys par rapport à la racine du site";
$MESS["BC_DATE_TIME_FORMAT"] = "Format d'affichage de la date et de l'heure";
$MESS["BB_PATH_TO_POST"] = "Modèle de chemin d'accès à la page avec un message";
$MESS["BMNP_PREVIEW_WIDTH"] = "La largeur de l'image de consultation préalable";
?>