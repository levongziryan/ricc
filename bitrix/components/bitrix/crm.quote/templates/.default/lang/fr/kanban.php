<?
$MESS["CRM_KANBAN_SUPERVISOR_Y"] = "Activer le mode superviseur";
$MESS["CRM_KANBAN_SUPERVISOR_TITLE"] = "Utilisez le mode superviseur pour afficher le travail des autres employés en temps réel. Le mode superviseur est désactivé lorsque vous êtes absent.";
$MESS["CRM_KANBAN_SUPERVISOR_N"] = "Désactiver le mode superviseur";
?>