<?
$MESS["CRM_ELEMENT_ID"] = "ID de l'offre";
$MESS["CRM_QUOTE_VAR"] = "Nom de la variable de l'identificateur de la proposition";
$MESS["CRM_NAME_TEMPLATE"] = "Format du nom";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "Modèle de chemin d'accès à la page principale";
$MESS["CRM_SEF_PATH_TO_IMPORT"] = "Modèle de chemin d'accès à la page d'importation";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "Modèle de chemin d'accès à la fiche de l'offre";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "Modèle de chemin d'accès à la page d'édition d'une offre";
$MESS["CRM_SEF_PATH_TO_LIST"] = "Modèle de chemin d'accès à la page de la liste des offres";
?>