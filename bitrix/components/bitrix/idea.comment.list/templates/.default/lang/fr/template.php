<?
$MESS["BPC_HIDDEN_COMMENT"] = "(Commentaire caché)";
$MESS["B_B_MS_NAME"] = "Prénom";
$MESS["FPF_VIDEO"] = "Intégrer la vidéo";
$MESS["BLOG_P_IMAGE_LINK"] = "Insérer le lien de l'image";
$MESS["BPC_MES_DELETE_POST_CONFIRM"] = "tes-vous sûr de vouloir supprimer le commentaire?";
$MESS["B_B_MS_ADD_COMMENT"] = "Ajouter un commentaire";
$MESS["BPC_MES_BIND"] = "Attacher";
$MESS["BPC_BIND_BITRIX_ANSWER"] = "Fixer comme une réponse officielle";
$MESS["B_B_MS_CAPTCHA_SYM"] = "Code de confirmation (caractères sur l'image)";
$MESS["BPC_IMAGE_SIZE_NOTICE"] = "Taille maximale admissible de l'image chargée <b>#SIZE#Mb</b>.";
$MESS["BPC_VIDEO_PATH_EXAMPLE"] = "Par exemple: <i>http://www.youtube.com/watch?v=j8YcLyzJOEg</i> <br/> ou <i>www.mysite.com/video/my_video.mp4</i>";
$MESS["B_B_MS_SEND"] = "Envoyer";
$MESS["BPC_MES_SHOW"] = "Afficher";
$MESS["B_B_MS_PREVIEW_TITLE"] = "Aperçu du commentaire";
$MESS["BPC_VIDEO_P"] = "Chemin vers le vidéo";
$MESS["BPC_MES_EDIT"] = "Editer";
$MESS["IDEA_RATING_TITLE"] = "Classement";
$MESS["BPC_MES_HIDE"] = "Cacher";
$MESS["B_B_MS_SAVE"] = "Sauvegarder";
$MESS["BPC_PAGE"] = "Pages:";
$MESS["BPC_MES_DELETE"] = "Supprimer";
?>