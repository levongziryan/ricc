<?
$MESS["BPC_TEXT_ENTER_URL"] = "Ingrese la dirección completa (URL).";
$MESS["BPC_TEXT_ENTER_URL_NAME"] = "Ingrese el texto del hipervínculo";
$MESS["BPC_TEXT_ENTER_IMAGE"] = "Ingresar el URL de la imagen";
$MESS["BPC_LIST_PROMPT"] = "Tipee el texto de la lista del item. Oprimir 'Cancelar' o tipear un espacio para cerrar la lista.";
$MESS["BPC_ERROR_NO_URL"] = "El URL no está especificado.";
$MESS["BPC_ERROR_NO_TITLE"] = "El título no está especificado.";
?>