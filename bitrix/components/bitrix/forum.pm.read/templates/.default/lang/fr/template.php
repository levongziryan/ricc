<?
$MESS["PM_IN"] = "dans";
$MESS["PM_DATA"] = "Données";
$MESS["PM_POST_FULLY"] = "Rempissage de la boîte:";
$MESS["PM_TO"] = "Dans";
$MESS["PM_ACT_COPY"] = "Copie";
$MESS["PM_FROM"] = "De qui";
$MESS["PM_ACT_REPLY"] = "Répondre";
$MESS["PM_REQUEST_NOTIF"] = "Envoyeur de la lettre a demandé la confirmation de la lecture de la lettre.";
$MESS["PM_SEND_NOTIF"] = "Envoyer la confirmation";
$MESS["PM_ACT_MOVE"] = "Déplacement";
$MESS["P_PREV"] = "Comparer avec la version précédente";
$MESS["PM_ACT_EDIT"] = "Editer";
$MESS["P_NEXT"] = "En avant";
$MESS["PM_ACT_DELETE"] = "Supprimer";
?>