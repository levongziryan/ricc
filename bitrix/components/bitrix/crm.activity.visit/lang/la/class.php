<?
$MESS["CRM_ACTIVITY_VISIT_LEAD_TITLE"] = "Nuevo visitante (#DATE#)";
$MESS["CRM_ACTIVITY_VISIT_LEAD_CREATE_ERROR"] = "Error al crear un nuevo prospecto";
$MESS["CRM_ACTIVITY_VISIT_SUBJECT"] = "Grabación de eventos";
$MESS["CRM_ACTIVITY_VISIT_DESCRIPTION"] = "La grabación del evento ocurrió el #DATE#";
$MESS["CRM_ACTIVITY_CREATE_ERROR"] = "Error al crear actividad";
$MESS["CRM_ACTIVITY_NO_RECORD_ERROR"] = "Error al guardar la grabación.";
$MESS["CRM_ACTIVITY_FILE_ERROR"] = "Error de escritura de archivo";
$MESS["CRM_ACTIVITY_VISIT_NO_FACEID"] = "El módulo Face recognition no está instalado.";
$MESS["CRM_ACTIVITY_VISIT_NO_PICTURE"] = "Error al enviar la imagen";
$MESS["CRM_ACTIVITY_VISIT_FILE_TOO_LARGE"] = "No se puede enviar la imagen porque el tamaño del archivo excede el límite.";
$MESS["CRM_ACTIVITY_VISIT_FACE_SERVER_ERROR"] = "Error de reconocimiento";
$MESS["CRM_ACTIVITY_VISIT_FACE_NOTHING_FOUND"] = "No se han encontrado entradas.";
?>