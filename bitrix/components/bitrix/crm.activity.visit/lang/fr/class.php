<?
$MESS["CRM_ACTIVITY_VISIT_LEAD_TITLE"] = "Nouveau visiteur (#DATE#)";
$MESS["CRM_ACTIVITY_VISIT_LEAD_CREATE_ERROR"] = "Erreur lors de la création d'une nouvelle piste";
$MESS["CRM_ACTIVITY_VISIT_SUBJECT"] = "Enregistrement de l'événement";
$MESS["CRM_ACTIVITY_VISIT_DESCRIPTION"] = "L'enregistrement de l'événement a eu lieu le #DATE#";
$MESS["CRM_ACTIVITY_CREATE_ERROR"] = "Erreur lors de la création de l'activité";
$MESS["CRM_ACTIVITY_NO_RECORD_ERROR"] = "Erreur lors de la sauvegarde de l'enregistrement.";
$MESS["CRM_ACTIVITY_FILE_ERROR"] = "Erreur d'écriture de fichier";
$MESS["CRM_ACTIVITY_VISIT_NO_FACEID"] = "Le module de reconnaissance faciale n'est pas installé.";
$MESS["CRM_ACTIVITY_VISIT_NO_PICTURE"] = "Erreur lors de l'envoi de l'image";
$MESS["CRM_ACTIVITY_VISIT_FILE_TOO_LARGE"] = "Envoi de l'image impossible parce que la taille du fichier dépasse la limite.";
$MESS["CRM_ACTIVITY_VISIT_FACE_SERVER_ERROR"] = "Erreur de reconnaissance";
$MESS["CRM_ACTIVITY_VISIT_FACE_NOTHING_FOUND"] = "Aucune entrée n'a été trouvée.";
?>