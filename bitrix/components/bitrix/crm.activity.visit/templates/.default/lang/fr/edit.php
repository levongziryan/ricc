<?
$MESS["CRM_ACTIVITY_VISIT_OWNER"] = "Avec :";
$MESS["CRM_ACTIVITY_VISIT_FINISH"] = "Terminer";
$MESS["CRM_ACTIVITY_VISIT_RECORDING"] = "enregistrement en cours";
$MESS["CRM_ACTIVITY_VISIT_MINUTES"] = "mn";
$MESS["CRM_ACTIVITY_VISIT_CREATE"] = "Créer :";
$MESS["CRM_ACTIVITY_VISIT_LEAD"] = "Piste";
$MESS["CRM_ACTIVITY_VISIT_CONTACT"] = "Contact";
$MESS["CRM_ACTIVITY_VISIT_SELECT"] = "Sélectionner :";
$MESS["CRM_ACTIVITY_VISIT_CONTACT_OR_COMPANY"] = "Contact ou société";
$MESS["CRM_ACTIVITY_VISIT_ACTIVITY"] = "Activité";
$MESS["CRM_ACTIVITY_VISIT_DEAL"] = "Transaction";
$MESS["CRM_ACTIVITY_VISIT_INVOICE"] = "Facture";
$MESS["CRM_ACTIVITY_VISIT_CAMERA"] = "Caméra :";
$MESS["CRM_ACTIVITY_VISIT_VK"] = "VK";
$MESS["CRM_ACTIVITY_BROWSER_ERROR"] = "Malheureusement, votre navigateur ne prend pas en charge l'enregistrement audio. <br> Les navigateurs suivants sont recommandés : Mozilla Firefox et Google Chrome.";
$MESS["CRM_ACTIVITY_VISIT_SEARCH_VK"] = "Trouver un profil VK";
$MESS["CRM_ACTIVITY_VISIT_SEARCH_IN_PROGRESS"] = "Recherche...";
$MESS["CRM_ACTIVITY_VISIT_TAB_VISIT"] = "Enregistrement en direct";
$MESS["CRM_ACTIVITY_VISIT_TAB_SAVE_PHOTO"] = "Photo de profil";
?>