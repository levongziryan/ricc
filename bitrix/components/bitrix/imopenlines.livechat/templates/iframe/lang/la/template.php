<?
$MESS["IMOL_LIVECHAT_FORM_INPUT_EMAIL"] = "E-mail";
$MESS["IMOL_LIVECHAT_FORM_INPUT_EMAIL_ERROR"] = "Por favor, indique su dirección de correo electrónico para recibir las respuestas.";
$MESS["IMOL_LIVECHAT_FORM_INPUT_EMAIL_ERROR_2"] = "Por favor proporcione su dirección de e-mail para recibir una copia de la conversación.";
$MESS["IMOL_LIVECHAT_FORM_INPUT_PHONE"] = "Teléfono";
$MESS["IMOL_LIVECHAT_FORM_INPUT_NAME"] = "Nombre";
$MESS["IMOL_LIVECHAT_FORM_BUTTON_SEND"] = "Enviar";
$MESS["IMOL_LIVECHAT_FORM_BUTTON_YES"] = "Enviar";
$MESS["IMOL_LIVECHAT_FORM_BUTTON_NO"] = "No, gracias";
$MESS["IMOL_LIVECHAT_FORM_TITLE_HISTORY"] = "¿Desea que se envíe una copia de la conversación a su e-mail?";
$MESS["IMOL_LIVECHAT_FORM_TITLE_OFFLINE"] = "Deje por favor su email para ser notificado cuando usted recibe una contestación";
$MESS["IMOL_LIVECHAT_FORM_TITLE_WELCOME"] = "Por favor, escriba su nombre";
$MESS["IMOL_LIVECHAT_FORM_RESULT_HISTORY"] = "Hemos enviado una copia de la conversación a su e-mail.";
$MESS["IMOL_LIVECHAT_FORM_RESULT_OFFLINE"] = "Gracias!";
$MESS["IMOL_LIVECHAT_FORM_RESULT_WELCOME"] = "Encantada de conocerlo!";
?>