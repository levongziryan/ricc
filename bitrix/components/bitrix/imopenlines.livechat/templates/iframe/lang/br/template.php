<?
$MESS["IMOL_LIVECHAT_FORM_INPUT_EMAIL"] = "E-mail";
$MESS["IMOL_LIVECHAT_FORM_INPUT_EMAIL_ERROR"] = "Forneça o seu endereço de e-mail para receber respostas.";
$MESS["IMOL_LIVECHAT_FORM_INPUT_EMAIL_ERROR_2"] = "Forneça o seu endereço de e-mail para receber uma cópia da conversa.";
$MESS["IMOL_LIVECHAT_FORM_INPUT_PHONE"] = "Telefone";
$MESS["IMOL_LIVECHAT_FORM_INPUT_NAME"] = "Nome";
$MESS["IMOL_LIVECHAT_FORM_BUTTON_SEND"] = "Enviar";
$MESS["IMOL_LIVECHAT_FORM_BUTTON_YES"] = "Enviar";
$MESS["IMOL_LIVECHAT_FORM_BUTTON_NO"] = "Não, obrigado";
$MESS["IMOL_LIVECHAT_FORM_TITLE_HISTORY"] = "Você gostaria de ter uma cópia da conversa enviada para o seu e-mail?";
$MESS["IMOL_LIVECHAT_FORM_TITLE_OFFLINE"] = "Deixe seu e-mail para ser notificado quando você receber uma resposta";
$MESS["IMOL_LIVECHAT_FORM_TITLE_WELCOME"] = "Digite seu nome";
$MESS["IMOL_LIVECHAT_FORM_RESULT_HISTORY"] = "Enviamos uma cópia da conversa para o seu e-mail.";
$MESS["IMOL_LIVECHAT_FORM_RESULT_OFFLINE"] = "Obrigado!";
$MESS["IMOL_LIVECHAT_FORM_RESULT_WELCOME"] = "Muito prazer!";
?>