<?
$MESS["IMOL_LIVECHAT_FORM_INPUT_EMAIL"] = "Електронна пошта";
$MESS["IMOL_LIVECHAT_FORM_INPUT_EMAIL_ERROR"] = "Вкажіть адресу електронної пошти, щоб ми могли надіслати вам відповідь.";
$MESS["IMOL_LIVECHAT_FORM_INPUT_EMAIL_ERROR_2"] = "Вкажіть адресу електронної пошти, щоб ми могли надіслати вам копію розмови.";
$MESS["IMOL_LIVECHAT_FORM_INPUT_PHONE"] = "Телефон";
$MESS["IMOL_LIVECHAT_FORM_INPUT_NAME"] = "Ім'я";
$MESS["IMOL_LIVECHAT_FORM_BUTTON_SEND"] = "Відправити";
$MESS["IMOL_LIVECHAT_FORM_BUTTON_YES"] = "Відправити";
$MESS["IMOL_LIVECHAT_FORM_BUTTON_NO"] = "Ні, спасибі";
$MESS["IMOL_LIVECHAT_FORM_TITLE_HISTORY"] = "Відправити копію розмови на електронну пошту?";
$MESS["IMOL_LIVECHAT_FORM_TITLE_OFFLINE"] = "Залиште контакти і ми сповістимо вас про відповідь";
$MESS["IMOL_LIVECHAT_FORM_TITLE_WELCOME"] = "Назвіться, будь ласка";
$MESS["IMOL_LIVECHAT_FORM_RESULT_HISTORY"] = "Ми надіслали копію розмови на вашу пошту.";
$MESS["IMOL_LIVECHAT_FORM_RESULT_OFFLINE"] = "Спасибі!";
$MESS["IMOL_LIVECHAT_FORM_RESULT_WELCOME"] = "Приємно познайомитися!";
?>