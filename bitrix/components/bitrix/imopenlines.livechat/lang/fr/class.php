<?
$MESS["OL_COMPONENT_MODULE_NOT_INSTALLED"] = "Le module \"Canaux ouverts\" n'est pas installé.";
$MESS["OL_COMPONENT_LIVECHAT_DESCRIPTION"] = "Canal ouvert pour communiquer avec les employés de l'entreprise.";
$MESS["OL_COMPONENT_MODULE_IM_NOT_INSTALLED"] = "Le module Messagerie web n'est pas installée.";
$MESS["OL_COMPONENT_MODULE_IMCONNECTOR_NOT_INSTALLED"] = "Le module \"Connecteurs de messagerie instantanée externe\" n'est pas installé.";
?>