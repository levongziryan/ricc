<?
$MESS["OL_COMPONENT_MODULE_NOT_INSTALLED"] = "O módulo Canais Abertos não está instalado.";
$MESS["OL_COMPONENT_LIVECHAT_DESCRIPTION"] = "Canal Aberto para se comunicar com os funcionários da empresa.";
$MESS["OL_COMPONENT_MODULE_IM_NOT_INSTALLED"] = "O módulo Web Messenger não está instalado.";
$MESS["OL_COMPONENT_MODULE_IMCONNECTOR_NOT_INSTALLED"] = "O módulo \"Conectores IM Externos\" não está instalado.";
?>