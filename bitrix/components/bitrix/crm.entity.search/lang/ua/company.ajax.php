<?
$MESS["CRM_ES_WINDOW_TITLE"] = "Вибір компанії";
$MESS["CRM_ES_SUBMIT"] = "Вибрати";
$MESS["CRM_ES_SUBMIT_TITLE"] = "Вибрати виділену компанію";
$MESS["CRM_ES_WINDOW_CLOSE"] = "Закрити";
$MESS["CRM_ES_NOTHING_FOUND"] = "нічого не знайдено";
$MESS["CRM_ES_CANCEL"] = "Скасувати";
$MESS["CRM_ES_CANCEL_TITLE"] = "Скасувати вибір компанії";
$MESS["CRM_ES_WAIT"] = "Зачекайте, триває завантаження списку...";
$MESS["CRM_ES_SEARCH"] = "пошук компанії";
$MESS["CRM_ES_LAST"] = "Останні вибрані";
$MESS["CRM_ES_LIST"] = "Список компаній";
?>