<?
$MESS["CRM_ES_WINDOW_TITLE"] = "Select Contact";
$MESS["CRM_ES_SUBMIT_TITLE"] = "Select A Contact";
$MESS["CRM_ES_CANCEL_TITLE"] = "Cancel Selection";
$MESS["CRM_ES_SEARCH"] = "search contacts";
$MESS["CRM_ES_LIST"] = "Contacts";
$MESS["CRM_ES_COMPANY_NONE"] = "The company is not specified.";
$MESS["CRM_ES_WINDOW_CLOSE"] = "Close";
$MESS["CRM_ES_WAIT"] = "Please wait, the list is being loaded...";
$MESS["CRM_ES_SUBMIT"] = "Select";
$MESS["CRM_ES_CANCEL"] = "Cancel";
$MESS["CRM_ES_LAST"] = "Recent Items";
$MESS["CRM_ES_NOTHING_FOUND"] = "search returned no results";
?>