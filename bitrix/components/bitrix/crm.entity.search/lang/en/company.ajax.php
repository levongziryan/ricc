<?
$MESS["CRM_ES_WINDOW_TITLE"] = "Select Company";
$MESS["CRM_ES_SUBMIT_TITLE"] = "Select A Company";
$MESS["CRM_ES_CANCEL_TITLE"] = "Cancel Selection";
$MESS["CRM_ES_SEARCH"] = "search companies";
$MESS["CRM_ES_LIST"] = "Companies";
$MESS["CRM_ES_WINDOW_CLOSE"] = "Close";
$MESS["CRM_ES_WAIT"] = "Please wait, the list is being loaded...";
$MESS["CRM_ES_SUBMIT"] = "Select";
$MESS["CRM_ES_CANCEL"] = "Cancel";
$MESS["CRM_ES_LAST"] = "Recent Items";
$MESS["CRM_ES_NOTHING_FOUND"] = "search returned no results";
?>