<?
$MESS["CRM_STATUS_LIST_RECOVERY_NAME"] = "Revertir Nombre Original";
$MESS["CRM_STATUS_BUTTONS_SAVE"] = "Guardar";
$MESS["CRM_STATUS_BUTTONS_CANCEL"] = "Cancelar";
$MESS["CRM_STATUS_FIX_STATUSES"] = "Por favor <a id=\"#ID#\" href=\"#URL#\">actualice ID's</a>para elementos afectados. De lo contrario, los diccionarios no están actualizados y podría fallar.";
$MESS["CRM_STATUS_SUCCESSFUL"] = "Éxito";
$MESS["CRM_STATUS_UNSUCCESSFUL"] = "Falló";
$MESS["CRM_STATUS_FINAL_TITLE"] = "final";
$MESS["CRM_STATUS_VIEW_SCALE"] = "Vista previa de escala";
$MESS["CRM_STATUS_VIEW_FUNNEL"] = "Embudo de vista previa";
$MESS["CRM_STATUS_ADD"] = "Agregar campo";
$MESS["CRM_STATUS_NEW"] = "Nuevo campo";
$MESS["CRM_STATUS_EDIT_COLOR"] = "Editar color";
$MESS["CRM_STATUS_EDIT_NAME"] = "Editar nombre";
$MESS["CRM_STATUS_DELETE_FIELD"] = "Eliminar";
$MESS["CRM_STATUS_TITLE_INITIAL_DEAL_STAGE"] = "Estado inicial";
$MESS["CRM_STATUS_TITLE_EXTRA_DEAL_STAGE"] = "Etapas adicionales";
$MESS["CRM_STATUS_SUCCESSFUL_DEAL_STAGE"] = "Etapa exitosa";
$MESS["CRM_STATUS_UNSUCCESSFUL_DEAL_STAGE"] = "Etapas sin éxito";
$MESS["CRM_STATUS_FUNNEL_SUCCESSFUL_DEAL_STAGE"] = "Etapas exitosas";
$MESS["CRM_STATUS_FUNNEL_UNSUCCESSFUL_DEAL_STAGE"] = "Etapas sin éxito";
$MESS["CRM_STATUS_TITLE_INITIAL_STATUS"] = "Estado inicial";
$MESS["CRM_STATUS_TITLE_EXTRA_STATUS"] = "Etapas adicionales";
$MESS["CRM_STATUS_SUCCESSFUL_STATUS"] = "Etapas exitosas";
$MESS["CRM_STATUS_UNSUCCESSFUL_STATUS"] = "Etapas sin éxito";
$MESS["CRM_STATUS_FUNNEL_SUCCESSFUL_STATUS"] = "Etapas exitosas";
$MESS["CRM_STATUS_FUNNEL_UNSUCCESSFUL_STATUS"] = "Etapas sin éxito";
$MESS["CRM_STATUS_TITLE_INITIAL_QUOTE_STATUS"] = "Estado inicial";
$MESS["CRM_STATUS_TITLE_EXTRA_QUOTE_STATUS"] = "Etapas adicionales";
$MESS["CRM_STATUS_SUCCESSFUL_QUOTE_STATUS"] = "Etapas exitosas";
$MESS["CRM_STATUS_UNSUCCESSFUL_QUOTE_STATUS"] = "Etapas sin éxito";
$MESS["CRM_STATUS_FUNNEL_SUCCESSFUL_QUOTE_STATUS"] = "Etapas exitosas";
$MESS["CRM_STATUS_FUNNEL_UNSUCCESSFUL_QUOTE_STATUS"] = "Etapas sin éxito";
$MESS["CRM_STATUS_TITLE_INITIAL_INVOICE_STATUS"] = "Estado inicial";
$MESS["CRM_STATUS_TITLE_EXTRA_INVOICE_STATUS"] = "Etapas adicionales";
$MESS["CRM_STATUS_SUCCESSFUL_INVOICE_STATUS"] = "Etapas exitosas";
$MESS["CRM_STATUS_UNSUCCESSFUL_INVOICE_STATUS"] = "Etapas sin éxito";
$MESS["CRM_STATUS_FUNNEL_SUCCESSFUL_INVOICE_STATUS"] = "Etapas exitosas";
$MESS["CRM_STATUS_FUNNEL_UNSUCCESSFUL_INVOICE_STATUS"] = "Etapas sin éxito";
$MESS["CRM_STATUS_CONFIRMATION_DELETE_TITLE"] = "Eliminar";
$MESS["CRM_STATUS_CONFIRMATION_DELETE_SAVE_BUTTON"] = "Si, eliminar";
$MESS["CRM_STATUS_CONFIRMATION_DELETE_CANCEL_BUTTON"] = "No, cancelar";
$MESS["CRM_STATUS_REMOVE_ERROR"] = "Error de eliminación";
$MESS["CRM_STATUS_CLOSE_POPUP_REMOVE_ERROR"] = "Cerrar";
$MESS["CRM_STATUS_DELETE_FIELD_QUESTION"] = "¿Está seguro de que desea eliminar la etapa? Puede estar siendo utilizado por algunos de los objetos CRM.";
$MESS["CRM_STATUS_CHECK_CHANGES"] = "Los cambios no se han guardado.";
$MESS["CRM_STATUS_FOOTER_PIN_ON"] = "Anclar barra";
$MESS["CRM_STATUS_FOOTER_PIN_OFF"] = "Desanclar barra";
?>