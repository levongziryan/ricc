<?
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_FIELDS_ENTITY_LIST"] = "Liste de modèles";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "Le module Devise n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Le module Vente n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "Le module Catalogue n'a pas été installé.";
$MESS["CRM_MODULE_ERROR_REMOVE_FIELD"] = "Impossible de supprimer le champ #field# parce qu'il est utilisé par des entités actives.";
$MESS["CRM_MODULE_ERROR_REMOVE_FIELD_MANY"] = "Impossible de supprimer les champs #field# parce qu'ils sont utilisés par des entités actives.";
$MESS["CRM_STATUS_ADD_DEAL_STAGE"] = "Ajouter une étape";
$MESS["CRM_STATUS_ADD_STATUS"] = "Ajouter un statut";
$MESS["CRM_STATUS_ADD_QUOTE_STATUS"] = "Ajouter un statut";
$MESS["CRM_STATUS_ADD_INVOICE_STATUS"] = "Ajouter un statut";
$MESS["CRM_STATUS_DEFAULT_NAME_DEAL_STAGE"] = "Nouvelle étape";
$MESS["CRM_STATUS_DEFAULT_NAME_STATUS"] = "Nouveau statut";
$MESS["CRM_STATUS_DEFAULT_NAME_QUOTE_STATUS"] = "Nouveau statut";
$MESS["CRM_STATUS_DEFAULT_NAME_INVOICE_STATUS"] = "Nouveau statut";
$MESS["CRM_STATUS_DELETION_CONFIRMATION_DEAL_STAGE"] = "Voulez-vous vraiment supprimer l’étape? Elle peut être utilisée par certaines des entités actives.";
$MESS["CRM_STATUS_DELETION_CONFIRMATION_STATUS"] = "Voulez-vous vraiment supprimer le statut? Il peut être utilisé par certaines des entités actives.";
$MESS["CRM_STATUS_DELETION_CONFIRMATION_QUOTE_STATUS"] = "Voulez-vous vraiment supprimer le statut? Il peut être utilisé par certaines des entités actives.";
$MESS["CRM_STATUS_DELETION_CONFIRMATION_INVOICE_STATUS"] = "Voulez-vous vraiment supprimer le statut? Il peut être utilisé par certaines des entités actives.";
?>