<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_FIELDS_ENTITY_LIST"] = "Tipos";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "O módulo de Moeda não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "O módulo de Venda não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "O módulo de Catálogo não está instalado.";
$MESS["CRM_MODULE_ERROR_REMOVE_FIELD"] = "Não é possível excluir o campo #field# porque ele é usado nas atuais entidades.";
$MESS["CRM_MODULE_ERROR_REMOVE_FIELD_MANY"] = "Não é possível excluir os campos #field# porque eles são usados nas atuais entidades.";
$MESS["CRM_STATUS_ADD_DEAL_STAGE"] = "Adicionar fase";
$MESS["CRM_STATUS_ADD_STATUS"] = "Adicionar status";
$MESS["CRM_STATUS_ADD_QUOTE_STATUS"] = "Adicionar status";
$MESS["CRM_STATUS_ADD_INVOICE_STATUS"] = "Adicionar status";
$MESS["CRM_STATUS_DEFAULT_NAME_DEAL_STAGE"] = "Nova fase";
$MESS["CRM_STATUS_DEFAULT_NAME_STATUS"] = "Novo status";
$MESS["CRM_STATUS_DEFAULT_NAME_QUOTE_STATUS"] = "Novo status";
$MESS["CRM_STATUS_DEFAULT_NAME_INVOICE_STATUS"] = "Novo status";
$MESS["CRM_STATUS_DELETION_CONFIRMATION_DEAL_STAGE"] = "Tem certeza de que deseja excluir a fase? Ela pode estar em uso por algumas das entidades ativas.";
$MESS["CRM_STATUS_DELETION_CONFIRMATION_STATUS"] = "Tem certeza de que deseja excluir o status? Ele pode estar em uso por algumas das entidades ativas.";
$MESS["CRM_STATUS_DELETION_CONFIRMATION_QUOTE_STATUS"] = "Tem certeza de que deseja excluir o status? Ele pode estar em uso por algumas das entidades ativas.";
$MESS["CRM_STATUS_DELETION_CONFIRMATION_INVOICE_STATUS"] = "Tem certeza de que deseja excluir o status? Ele pode estar em uso por algumas das entidades ativas.";
?>