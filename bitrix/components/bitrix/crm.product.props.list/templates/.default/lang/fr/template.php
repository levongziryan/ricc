<?
$MESS["CRM_PRODUCTPROP_EDIT"] = "Editer";
$MESS["CRM_PRODUCTPROP_DELETE"] = "Supprimer";
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_PRODUCTPROP_DELETE_CONFIRM"] = "Tes-vous sûr de vouloir supprimer '%s'?";
$MESS["CRM_PRODUCTPROP_EDIT_TITLE"] = "Ouvrez cette propriété pour l'édition";
$MESS["CRM_PRODUCTPROP_DELETE_TITLE"] = "Supprimer cette propriété";
?>