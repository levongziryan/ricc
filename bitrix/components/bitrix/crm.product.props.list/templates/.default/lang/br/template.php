<?
$MESS["CRM_PRODUCTPROP_EDIT_TITLE"] = "Abrir esta propriedade para edição";
$MESS["CRM_PRODUCTPROP_EDIT"] = "Editar";
$MESS["CRM_PRODUCTPROP_DELETE_TITLE"] = "Excluir esta propriedade";
$MESS["CRM_PRODUCTPROP_DELETE"] = "Excluir";
$MESS["CRM_PRODUCTPROP_DELETE_CONFIRM"] = "Você tem certeza que deseja excluir '%s'?";
$MESS["CRM_ALL"] = "Total";
?>