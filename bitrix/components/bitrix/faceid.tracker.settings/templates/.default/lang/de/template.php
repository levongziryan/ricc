<?
$MESS["FACEID_FTS_AUTO_LEAD_SETTING_A"] = "Einen neuen Lead automatisch erstellen";
$MESS["FACEID_FTS_LEAD_SOURCE_SETTING"] = "Quelle des neuen Leads";
$MESS["FACEID_FTS_CONFIG_EDIT_SAVE"] = "Speichern";
$MESS["FACEID_FTS_AUTO_LEAD_SETTING"] = "Wenn Besucher im CRM nicht gefunden wurde";
$MESS["FACEID_FTS_AUTO_LEAD_SETTING_M"] = "manuell erstellen";
$MESS["FACEID_FTS_SOCNET_ENABLED"] = "Suche in sozialen Netzwerken aktivieren";
$MESS["FACEID_FTS_TO_FTRACKER"] = "Face Tracker öffnen";
$MESS["FACEID_FTS_DELETE_ALL"] = "Alle Besucher aus dem Face Tracker entfernen";
$MESS["FACEID_FTS_DELETE_ALL_CONFIRM"] = "Möchten Sie die History komplett löschen und alle Besucher entfernen?";
?>