<?
$MESS["T_IBLOCK_DESC_DOCS_DATE"] = "Afficher la date de l'élément";
$MESS["T_IBLOCK_DESC_DOCS_PICTURE"] = "Afficher l'image pour l'annonce";
$MESS["T_IBLOCK_DESC_DOCS_NAME"] = "Afficher la dénomination de l'élément";
$MESS["T_IBLOCK_DESC_DOCS_TEXT"] = "Afficher le texte de l'annonce";
?>