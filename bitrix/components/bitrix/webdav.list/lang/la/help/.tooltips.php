<?
$MESS["DOCS_COUNT_TIP"] = "Especificar el número de elementos por página.";
$MESS["SORT_ORDER1_TIP"] = "Ascendente o descendente.";
$MESS["SORT_ORDER2_TIP"] = "Ascendente o descendente.";
$MESS["AJAX_OPTION_HISTORY_TIP"] = "Permite los botones del navegador \"Atrás\" y \"Adelante\" para transiciones AJAX.";
$MESS["IBLOCK_TYPE_TIP"] = "Seleccione uno de los tipos de blocks de información existentes en la lista y haga click en <b>OK</b>. Esto podrá cargar los tipos de blocks de información seleccionados. En todo caso el componente de este parámetro es opcional.";
$MESS["IBLOCK_ID_TIP"] = "Seleccione aquí uno de los blocks de información existentes. Si usted selecciona <b><i>(other)</i></b>, usted tendrá que especificar el ID del block de información en el campo de a lado. Por ejemplo: <b>={\$_REQUEST[\"IBLOCK_ID\"]}</b>";
$MESS["SORT_BY1_TIP"] = "Seleccione aquí el campo por el cual los productos de los documentos están clasificados. Usted puede seleccionar <b><i>(other)</i></b> y especificar el ID del campo en el campo del lado.";
$MESS["SORT_BY2_TIP"] = "Seleccione aquí el campo por el cual los documentos de los productos serán ordenados en el segundo pase. Usted puede seleccionar <b><i> y especificar el ID en el campo de al lado.  ";
$MESS["FILTER_NAME_TIP"] = "El nombre de la variable en la cual las configuraciones son filtradas y podrán pasar. Usted puede dejar el campo vacío para usar un nombre predeterminado.";
$MESS["FIELD_CODE_TIP"] = "Awuí usted puede elegir los campos del elemento del block de información si lo requiere, por el cual el filtro podrá ser aplicado. Usted puede agregar los campos del cliente como un código en los campos de entrada de abajo.";
$MESS["PROPERTY_CODE_TIP"] = "Seleccione las propiedades del block de información que usted desea visualizar en el filtro. Usted también puede agregar sus propios campos en los campos de abajo.";
$MESS["ACTIVE_DATE_FORMAT_TIP"] = "Seleccione aquí la fecha de formato requerida. Si usted selecciona <i><b>other</b></i>, usted puede crear su propio formato usando la función PHP <i><b>date</b></i>.";
$MESS["DISPLAY_PANEL_TIP"] = "Si revisa, los botones del editor podrán mostrarse en la barra del panel de contro en el modo de Edición del sitio, y en el componente del área de la caja de herramientas.";
$MESS["SET_TITLE_TIP"] = "Eligiendo esta opción podrá fijar el título para el nombre del block de información actual.";
$MESS["INCLUDE_IBLOCK_INTO_CHAIN_TIP"] = "Si elige, el nombre del block de información podrá ser agregado a la cadena de navegación.";
$MESS["ADD_SECTIONS_CHAIN_TIP"] = "Si la opción está activa y el block de información contiene secciones, sus nombres podrán ser mostrados en la cadena de navegación.";
$MESS["HIDE_LINK_WHEN_NO_DETAIL_TIP"] = "Especificar para ocultar el vínculo si los detalles del producto no están disponibles, o si un usuario no tiene suficientes permisos.";
$MESS["PARENT_SECTION_TIP"] = "Especificar el ID de la sección principal el cual podrá usarse para el límite de los elementos de la sección seleccionada.";
$MESS["CACHE_TIME_TIP"] = "Especificar aquí el periodo de tiempo por el cual el caché es válido.";
$MESS["DISPLAY_TOP_PAGER_TIP"] = "Si se elige, los vínculos del elemento de la navegación pódrán ser mostrados en la parte superior de la página.";
$MESS["DISPLAY_BOTTOM_PAGER_TIP"] = "Si se selecciona, los vínculos del elemento de navegación podrán ser mostrados en la parte inferior de la página.";
$MESS["PAGER_TITLE_TIP"] = "El nombre de la unidad del producto para la navegación. Por ejemplo: página, capítulo, etc.";
$MESS["PAGER_SHOW_ALWAYS_TIP"] = "Si no se selecciona, los vínculos de la cadena de navegación no podrán ser presentados en una sóla página. De otro modo, los vínculos de navegación podrán mostrarse siempre.";
$MESS["PAGER_DESC_NUMBERING_TIP"] = "Usar esta opción si desea nuevos elementos para ser ubicados en la parte superior. Si no, sólo la última página del breadcrumb será modificada. Todas las demás páginas podrán ser válidas por un tiempo considerablemente largo.";
$MESS["PAGER_DESC_NUMBERING_CACHE_TIME_TIP"] = "Especificar el tiempo del caché para las páginas (en segundos) cuando use la navegación inversa.";
$MESS["AJAX_MODE_TIP"] = "Habilita AJAX en el componente.";
$MESS["DETAIL_URL_TIP"] = "La URL de la vista de los detalles de la página. Por ejemplo: <b>doc_detail.php?ID=#ELEMENT_ID#</b>";
$MESS["PREVIEW_TRUNCATE_LEN_TIP"] = "Si el block de información es un fragmento de texto, puede especificar el número máximo de símbolos aquí. Cualquier texto que supere este límite se truncará.";
$MESS["CACHE_TYPE_TIP"] = "<i>Auto</i>: el caché es válido durante el tiempo predefinido en la configuración del caché
;<br /><i>Cache</i>: siempre utilizar el caché para el período especificado en el siguiente campo
;<br /><i>No almacenar en caché</i>: no se realiza el almacenamiento en caché.";
$MESS["CACHE_FILTER_TIP"] = "Si se selecciona, el sistema de caché filtrará todos los resultados. Esto puede ser útil si muchos visitantes utilizan con frecuencia la configuración del filtro mismo.";
$MESS["PAGER_TEMPLATE_TIP"] = "El nombre de la plantilla de ruta de navegación. Usted puede dejar el campo vacío para usar la plantilla por defecto (<b><i>.default</i></b>). (El sistema proporciona una plantilla alternativa: <i>orange</i>.)";
$MESS["AJAX_OPTION_SHADOW_TIP"] = "Especifica a la zona de sombra modificables en la transición de AJAX.";
$MESS["AJAX_OPTION_JUMP_TIP"] = "Especifica para desplazarse a los componentes AJAX de transición cuando se complete.";
$MESS["AJAX_OPTION_STYLE_TIP"] = "Especifica para descargar y procesar los estilos CSS del componente en AJAX de transición.";
?>