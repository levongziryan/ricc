<?
$MESS["IBLOCK_CACHE_FILTER"] = "Caché si el filtro está activo";
$MESS["T_IBLOCK_DESC_ASC"] = "Ascendente";
$MESS["T_IBLOCK_DESC_DESC"] = "Descendente";
$MESS["T_IBLOCK_DESC_FID"] = "ID";
$MESS["T_IBLOCK_DESC_FNAME"] = "Nombre";
$MESS["T_IBLOCK_DESC_FACT"] = "Fecha de activación";
$MESS["T_IBLOCK_DESC_FSORT"] = "Clasificar";
$MESS["T_IBLOCK_DESC_FTSAMP"] = "Fecha del último cambio";
$MESS["T_IBLOCK_DESC_IBORD1"] = "Campo para los documentos del primer pase";
$MESS["T_IBLOCK_DESC_IBBY1"] = "Direción para los documentos del primer pase de clasificación";
$MESS["T_IBLOCK_DESC_LIST_ID"] = "Código del block de información";
$MESS["T_IBLOCK_DESC_LIST_TYPE"] = "Tipo del block de información (usado para verificación solamente)";
$MESS["T_IBLOCK_DESC_LIST_CONT"] = "Documentos por pàgina";
$MESS["T_IBLOCK_DESC_DETAIL_PAGE_URL"] = "URL de la página de detalles (desde las configuraciones del block de información predeterminado)";
$MESS["T_IBLOCK_DESC_INCLUDE_IBLOCK_INTO_CHAIN"] = "Incluir el block de información en la cadena de navegación";
$MESS["T_IBLOCK_PROPERTY"] = "Propiedades";
$MESS["T_IBLOCK_FILTER"] = "Filtro";
$MESS["T_IBLOCK_DESC_DOCS_PANEL"] = "Mostrar el panel de botones para este componente";
$MESS["IBLOCK_FIELD"] = "Campos";
$MESS["T_IBLOCK_DESC_ACTIVE_DATE_FORMAT"] = "Formato de visualización de fecha";
$MESS["T_IBLOCK_DESC_PAGER_DOCS"] = "Documentos";
$MESS["T_IBLOCK_DESC_PREVIEW_TRUNCATE_LEN"] = "Extensión máxima del texto previo (para tipear texto solamente)";
$MESS["T_IBLOCK_DESC_HIDE_LINK_WHEN_NO_DETAIL"] = "Ocultar vínculp para detalles de la página si no ha brindado detalles de la descripción";
$MESS["IBLOCK_SECTION_ID"] = "ID de la sección";
$MESS["T_IBLOCK_DESC_ADD_SECTIONS_CHAIN"] = "Agregar el nombre de la sección al breadcrumb de navegación";
$MESS["T_IBLOCK_DESC_IBORD2"] = "Campo para el segundo paso de clasificación documentos";
$MESS["T_IBLOCK_DESC_IBBY2"] = "Dirección para el segundo paso de clasificación documentos";
?>