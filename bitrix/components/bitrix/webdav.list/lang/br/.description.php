<?
$MESS["T_IBLOCK_DESC_DOCS"] = "Biblioteca de Documentos";
$MESS["T_IBLOCK_DESC_LIST"] = "Lista de documentos";
$MESS["T_IBLOCK_DESC_LIST_DESC"] = "Lista de documentos de um bloco de informação";
$MESS["IBEL_E_TAB_BIZPROC"] = "Processos de Negócios";
$MESS["WD_DISK_ERROR_NOT_INSTALLED_PULL"] = "Bitrix24.Drive requer o módulo Pull and Push. Por favor, contate o administrador do site.";
$MESS["WD_DISK_NOTIFY_ACTION_ADD_F_D"] = "Adicionado: #FILE# e #DIR#";
?>