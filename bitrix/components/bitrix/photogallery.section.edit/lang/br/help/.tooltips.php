<?
$MESS["IBLOCK_TYPE_TIP"] = "Selecione aqui um dos tipos existentes de blocos de informação. Clique em <b><i>OK</i></b>  para carregar os blocos de informação do tipo selecionado. ";
$MESS["DISPLAY_PANEL_TIP"] = "Se selecionado, os botões do editor irão ser exibidos no modo de edição do Site na barra de ferramentas do Painel de Controle e na área da caixa de ferramentas do componente. 
";
$MESS["IBLOCK_ID_TIP"] = "Especifique aqui o bloco de informações que irá armazenar as fotos. Alternativamente, você pode selecionar <b>(outro)-></b> e especificar a ID do bloco de informações no campo ao lado.
";
$MESS["SECTION_ID_TIP"] = "Este campo contém a expressão que avalia a ID da seção (álbum).
";
$MESS["SECTION_CODE_TIP"] = "Especifique aqui o código mnemônico da seção (álbum)
";
$MESS["INDEX_URL_TIP"] = "Especifica o endereço da página de álbuns.";
$MESS["SECTION_URL_TIP"] = "Especifica o endereço da página de visualização do álbum.";
$MESS["USE_PERMISSIONS_TIP"] = "Restringe o acesso ao álbum.";
$MESS["GROUP_PERMISSIONS_TIP"] = "Especifique aqui os grupos de usuários cujos membros podem visualizar um álbum";
$MESS["SET_TITLE_TIP"] = "Ative esta opção para ajustar o título da página para o nome do álbum ou para \"Novo álbum\".";
$MESS["DATE_TIME_FORMAT_TIP"] = "Especifique aqui o formato de exibição da data.";
?>