<?
$MESS["IBLOCK_SECTION_ID"] = "ID de la section";
$MESS["IBLOCK_INDEX_URL"] = "Liste d'albums";
$MESS["P_ACTION"] = "Action";
$MESS["T_IBLOCK_DESC_NEWS_PANEL"] = "Ajouter les boutons au panneau admin. Pour ce composant";
$MESS["IBLOCK_IBLOCK"] = "Bloc d'information";
$MESS["IBLOCK_SECTION_CODE"] = "Code de la section";
$MESS["P_USER_ALIAS"] = "Code Gallery";
$MESS["IBLOCK_GALLERY_URL"] = "Contenu de la galerie";
$MESS["IBLOCK_TYPE"] = "Type du bloc d'information";
$MESS["P_SET_STATUS_404"] = "Tablir le statut 404 si l'élément ou la section ne sont pas trouvés";
$MESS["T_DATE_TIME_FORMAT"] = "Format de la date";
$MESS["IBLOCK_SECTION_URL"] = "URL menant vers la page avec le contenu de la section";
$MESS["P_BEHAVIOUR"] = "Mode Galerie";
?>