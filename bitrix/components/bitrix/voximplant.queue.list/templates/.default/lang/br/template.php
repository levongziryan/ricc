<?
$MESS["VOX_QUEUE_LIST_SELECTED"] = "Total de filas";
$MESS["VOX_QUEUE_LIST_ADD"] = "Adicionar grupo";
$MESS["VOX_QUEUE_DELETE_ERROR"] = "Erro ao excluir o grupo.";
$MESS["VOX_QUEUE_IS_USED"] = "Este grupo está atualmente em uso em:";
$MESS["VOX_QUEUE_NUMBER"] = "Número";
$MESS["VOX_QUEUE_CLOSE"] = "Fechar";
$MESS["VOX_QUEUE_IVR"] = "IVR";
?>