<?
$MESS["EC_INTRANET_MODULE_NOT_INSTALLED"] = "El módulo de Intranet no está instalado.";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "El módulo Information Blocks no está instalado.";
$MESS["INTS_NO_IBLOCK_PERMS"] = "Usted no tiene permisos para ver las tareas del bloque informativo.";
$MESS["INTASK_C36_PAGE_TITLE"] = "Horario de la sala de Reuniones";
$MESS["INTASK_C36_PAGE_TITLE1"] = "Reserva de la Sala de Reuniones";
$MESS["INAF_F_ID"] = "ID";
$MESS["INAF_F_NAME"] = "Titulo";
$MESS["INAF_F_DESCRIPTION"] = "Descripción";
$MESS["INAF_F_FLOOR"] = "Piso";
$MESS["INAF_F_PLACE"] = "Asientos";
$MESS["INAF_F_PHONE"] = "Teléfono";
$MESS["INAF_MEETING_NOT_FOUND"] = "La sala de reuniones no fue encontrada.";
$MESS["INTASK_C25_CONFLICT1"] = "Tiempo del conflicto durante #TIME# entre \"#RES1#\" y \"#RES2#\"";
$MESS["INTASK_C25_CONFLICT2"] = "Tiempo del conflicto durante #TIME# entre \"#RES1#\" y \"#RES2#\"";
$MESS["INTR_IRMM_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
?>