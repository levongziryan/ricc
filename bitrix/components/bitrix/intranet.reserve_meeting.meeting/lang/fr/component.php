<?
$MESS["INTR_IRMM_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["INAF_F_ID"] = "ID";
$MESS["INTASK_C36_PAGE_TITLE"] = "Calendrier de la salle de réunion";
$MESS["INTASK_C25_CONFLICT1"] = "Conflit du temps en période #TIME# entre '#RES1#' et '#RES2#'";
$MESS["INTASK_C25_CONFLICT2"] = "Conflit du temps en période #TIME# entre '#RES1#' et '#RES2#'";
$MESS["INAF_F_PLACE"] = "Nombre de places";
$MESS["EC_INTRANET_MODULE_NOT_INSTALLED"] = "Le module 'Intranet' n'a pas été installé.";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "Le module des blocs d'informations n'a pas été installé.";
$MESS["INAF_F_NAME"] = "Titre";
$MESS["INAF_F_DESCRIPTION"] = "Description";
$MESS["INAF_MEETING_NOT_FOUND"] = "Une salle de réunion n'est pas retrouvée.";
$MESS["INTASK_C36_PAGE_TITLE1"] = "Points de négociations";
$MESS["INAF_F_PHONE"] = "Numéro de téléphone";
$MESS["INTS_NO_IBLOCK_PERMS"] = "Vous n'avez pas la permission d'afficher le bloc d'information de la salle de réunion.";
$MESS["INAF_F_FLOOR"] = "Etage";
?>