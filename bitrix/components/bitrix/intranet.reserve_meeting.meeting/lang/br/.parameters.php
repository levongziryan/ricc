<?
$MESS["INTL_VARIABLE_ALIASES"] = "Apelidos variáveis";
$MESS["INTL_IBLOCK_TYPE"] = "Tipo de Bloco de Informação";
$MESS["INTL_IBLOCK"] = "Bloco de informação";
$MESS["INTL_MEETING_VAR"] = "Variável para ID de sala de reunião";
$MESS["INTL_ITEM_VAR"] = "Variável de ID da Reserva";
$MESS["INTL_PAGE_VAR"] = "Página Variável";
$MESS["INTL_MEETING_ID"] = "ID da Sala de Reunião";
$MESS["INTL_PATH_TO_MEETING"] = "Página de Agendamento de Reserva da Sala";
$MESS["INTL_PATH_TO_MEETING_LIST"] = "Página Principal para Reserva da Sala de Reunião";
$MESS["INTL_PATH_TO_RESERVE_MEETING"] = "Página para Reserva da Sala de Reunião";
$MESS["INTL_PATH_TO_VIEW_ITEM"] = "Página da visualização de reserva da sala de reunião";
$MESS["INTL_PATH_TO_MODIFY_MEETING"] = "Página do Editor de Parâmetros da Sala de Reunião";
$MESS["INTL_SET_NAVCHAIN"] = "Definir Trilha de Navegação";
$MESS["INTL_USERGROUPS_CLEAR"] = "Grupos de usuários autorizados a cancelarem reservas de salas de reunião";
$MESS["INTL_P_WEEK_HOLIDAYS"] = "Fim de semana";
$MESS["INTL_P_MON_F"] = "Segunda-feira";
$MESS["INTL_P_TUE_F"] = "Terça-feira";
$MESS["INTL_P_WEN_F"] = "Quarta-feira";
$MESS["INTL_P_THU_F"] = "Quinta-feira";
$MESS["INTL_P_FRI_F"] = "Sexta-feira";
$MESS["INTL_P_SAT_F"] = "Sábado";
$MESS["INTL_P_SAN_F"] = "Domingo";
?>