<?
$MESS["INTASK_C25T_RESERVED_BY"] = "Reservado por";
$MESS["INTASK_C25T_CLEAR"] = "Cancelar Reserva";
$MESS["INTASK_C25T_DBL_CLICK"] = "Duplo clique para reservar a sala de reunião";
$MESS["INTASK_C25T_EDIT"] = "Editar";
$MESS["INTASK_C25T_FLOOR"] = "Andar";
$MESS["INTASK_C25T_D5"] = "Sexta-feira";
$MESS["INTASK_C25T_D1"] = "Segunda-feira";
$MESS["INTASK_C25T_NEXT_WEEK"] = "Próxima Semana";
$MESS["INTASK_C25T_PHONE"] = "Telefone";
$MESS["INTASK_C25T_PRIOR_WEEK"] = "Semana Anterior";
$MESS["INTASK_C25T_D6"] = "Sábado";
$MESS["INTASK_C25T_PLACE"] = "Lugares";
$MESS["INTASK_C25T_SET"] = "Definir";
$MESS["INTASK_C25T_D7"] = "Domingo";
$MESS["INTASK_C25T_CLEAR_CONF"] = "A reserva será cancelada! Continuar?";
$MESS["INTASK_C25T_D4"] = "Quinta-feira";
$MESS["INTASK_C25T_TIME"] = "Tempo";
$MESS["INTASK_C25T_D2"] = "Terça-feira";
$MESS["INTASK_C25T_D3"] = "Quarta-feira";
?>