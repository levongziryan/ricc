<?
$MESS["INTASK_C25T_CLEAR_CONF"] = "La réservation sera enlevée! Continuer?";
$MESS["INTASK_C25T_D7"] = "Dimanche";
$MESS["INTASK_C25T_TIME"] = "Temps";
$MESS["INTASK_C25T_D2"] = "Mardi";
$MESS["INTASK_C25T_DBL_CLICK"] = "Double-cliquez sur le bouton gauche de la souris pour réserver la salle de réunion";
$MESS["INTASK_C25T_RESERVED_BY"] = "A réservé";
$MESS["INTASK_C25T_EDIT"] = "Editer";
$MESS["INTASK_C25T_PLACE"] = "Nombre de places";
$MESS["INTASK_C25T_CLEAR"] = "Annuler de la réservation";
$MESS["INTASK_C25T_D1"] = "Lundi";
$MESS["INTASK_C25T_PRIOR_WEEK"] = "Semaine précédente";
$MESS["INTASK_C25T_D5"] = "Vendredi";
$MESS["INTASK_C25T_NEXT_WEEK"] = "La semaine prochaine";
$MESS["INTASK_C25T_D3"] = "Mercredi";
$MESS["INTASK_C25T_D6"] = "Samedi";
$MESS["INTASK_C25T_PHONE"] = "Numéro de téléphone";
$MESS["INTASK_C25T_SET"] = "Ensemble";
$MESS["INTASK_C25T_D4"] = "Jeudi";
$MESS["INTASK_C25T_FLOOR"] = "Etage";
?>