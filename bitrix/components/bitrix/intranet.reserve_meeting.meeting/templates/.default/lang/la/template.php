<?
$MESS["INTASK_C25T_FLOOR"] = "Piso";
$MESS["INTASK_C25T_PLACE"] = "Asientos";
$MESS["INTASK_C25T_PHONE"] = "Teléfono";
$MESS["INTASK_C25T_PRIOR_WEEK"] = "Semana anterior";
$MESS["INTASK_C25T_SET"] = "Establecer";
$MESS["INTASK_C25T_NEXT_WEEK"] = "Semana siguiente";
$MESS["INTASK_C25T_D1"] = "Lunes";
$MESS["INTASK_C25T_D2"] = "Martes";
$MESS["INTASK_C25T_D3"] = "Miércoles";
$MESS["INTASK_C25T_D4"] = "Jueves";
$MESS["INTASK_C25T_D5"] = "Viernes";
$MESS["INTASK_C25T_D6"] = "Sábado";
$MESS["INTASK_C25T_D7"] = "Domingo";
$MESS["INTASK_C25T_TIME"] = "Tiempo";
$MESS["INTASK_C25T_RESERVED_BY"] = "Reservado por";
$MESS["INTASK_C25T_CLEAR_CONF"] = "La reservación será cancelada! Desea Continuar?";
$MESS["INTASK_C25T_EDIT"] = "Editar";
$MESS["INTASK_C25T_CLEAR"] = "Cancelar reservación";
$MESS["INTASK_C25T_DBL_CLICK"] = "Haga doble clic para reservar la sala de reunión";
?>