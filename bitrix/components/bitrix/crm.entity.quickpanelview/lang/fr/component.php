<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_ENTITY_QPV_CLIENT_NOT_ASSIGNED"] = "non défini";
$MESS["CRM_ENTITY_QPV_DEAL_NOT_ASSIGNED"] = "non défini";
$MESS["CRM_ENTITY_QPV_PAY_SYSTEM_NOT_ASSIGNED"] = "non défini";
$MESS["CRM_ENTITY_QPV_QUOTE_NOT_ASSIGNED"] = "non défini";
$MESS["CRM_ENTITY_QPV_LOCATION_NOT_ASSIGNED"] = "non défini";
$MESS["CRM_ENTITY_QPV_ENTITY_ID_NOT_DEFINED"] = "Identifiant de l'entité inconnu.";
$MESS["CRM_ENTITY_QPV_ENTITY_TYPE_NAME_NOT_SUPPORTED"] = "Divulgué type d'entité est pas pris en charge.";
$MESS["CRM_ENTITY_QPV_NOT_SELECTED"] = "valeur indéfinie";
$MESS["CRM_ENTITY_QPV_ENTITY_TYPE_NAME_NOT_DEFINED"] = "Les points d'entrée ne sont pas définis";
$MESS["CRM_ENTITY_QPV_ENTITY_FIELDS_NOT_FOUND"] = "Vous ne trouvez pas l'entité.";
$MESS["CRM_ENTITY_QPV_NOT_AUTHORIZED"] = "Utilisateur non connecté";
$MESS["CRM_ENTITY_QPV_MULTI_FIELD_NOT_ASSIGNED"] = "non spécifié";
$MESS["CRM_ENTITY_QPV_ACCESS_DENIED"] = "Accès refusé";
$MESS["CRM_ENTITY_QPV_HIDDEN_CONTACT"] = "Contact masqué";
$MESS["CRM_ENTITY_QPV_HIDDEN_COMPANY"] = "Entreprise masquée";
?>