<?
$MESS["CRM_ENTITY_QPV_COMPANY_HEADER"] = "Empresa";
$MESS["CRM_ENTITY_QPV_CONTACT_HEADER"] = "Contato";
$MESS["CRM_ENTITY_QPV_RESET_MENU_ITEM"] = "Redefinir configurações";
$MESS["CRM_ENTITY_QPV_SAVE_FOR_ALL_MENU_ITEM"] = "Salvar preferências para todos os usuários";
$MESS["CRM_ENTITY_QPV_RESET_FOR_ALL_MENU_ITEM"] = "Redefinir preferências para todos os usuários";
$MESS["CRM_ENTITY_QPV_SUM_HEADER"] = "Total";
$MESS["CRM_ENTITY_QPV_SIP_MGR_UNKNOWN_RECIPIENT"] = "Número não reconhecido";
$MESS["CRM_ENTITY_QPV_SIP_MGR_ENABLE_CALL_RECORDING"] = "Gravar conversa";
$MESS["CRM_ENTITY_QPV__SIP_MGR_MAKE_CALL"] = "Chamar";
$MESS["CRM_ENTITY_QPV_INFO_DLG_BTN_CONTINUE"] = "Continuar";
$MESS["CRM_ENTITY_QPV_DRAG_DROP_ERROR_TITLE"] = "Erro ao mover o campo";
$MESS["CRM_ENTITY_QPV_DRAG_DROP_ERROR_FIELD_NOT_SUPPORTED"] = "Desculpe, ainda não há suporte para mover este campo para o painel superior.  ";
$MESS["CRM_ENTITY_QPV_DRAG_DROP_ERROR_FIELD_ALREADY_EXISTS"] = "Este campo já está contido numa coluna.";
$MESS["CRM_ENTITY_QPV_RESPONSIBLE_CHANGE"] = "alteração";
$MESS["CRM_ENTITY_QPV_CONTROL_FIELD_DATA_NOT_SAVED"] = "O valor do \"#FIELD#\" não foi salvo.";
$MESS["CRM_ENTITY_QPV_NOT_SELECTED"] = "nenhum selecionado";
$MESS["CRM_ENTITY_QPV_DELETION_CONFIRMATION"] = "Você tem certeza de que deseja ocultar este campo?";
$MESS["CRM_ENTITY_QPV_EDIT_CONTEXT_MENU_ITEM"] = "Editar";
$MESS["CRM_ENTITY_QPV_DELETE_CONTEXT_MENU_ITEM"] = "Excluir";
$MESS["CRM_ENTITY_QPV_DD_BIN_PROMPTING"] = "Para ocultar um campo, arraste-o para <a id=\"#DEMO_BTN_ID#\" href=\"#\">a lixeira à direita do formulário</a> ou clique no ícone ao lado do nome do campo para abrir um menu de contexto. <a id=\"#CLOSE_BTN_ID#\" href=\"#\">Fechar</a>";
$MESS["CRM_ENTITY_QPV_CONTACT_NOT_SELECTED"] = "Nenhum contato selecionado";
$MESS["CRM_ENTITY_QPV_COMPANY_NOT_SELECTED"] = "Nenhuma empresa selecionada";
?>