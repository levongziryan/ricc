<?
$MESS["INTR_IAC_COMPONENT_NAME"] = "Abwesenheitskalender";
$MESS["INTR_ABSENCE_GROUP_NAME"] = "Grafik: Abwesenheitsliste";
$MESS["INTR_HR_GROUP_NAME"] = "Personal";
$MESS["INTR_GROUP_NAME"] = "Intranet-Portal";
$MESS["INTR_IAC_COMPONENT_DESCR"] = "Grafische Darstellung der Abwesenheitsliste aller Mitarbeiter";
?>