<?
$MESS["SONET_DEFAULT_TEMPLATE_DESCRIPTION"] = "Permet d'afficher et d'éditer le profil de l'utilisateur.";
$MESS["SONET_DEFAULT_TEMPLATE_NAME"] = "Profil de l'utilisateur";
$MESS["SONET_NAME"] = "Réseau social";
$MESS["SONET_UPE_TEMPLATE_NAME"] = "Modifier profil utilisateur";
$MESS["SONET_UPE_TEMPLATE_DESCRIPTION"] = "Un composant pour modifier un profil utilisateur";
?>