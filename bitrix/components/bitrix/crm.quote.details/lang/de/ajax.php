<?
$MESS["CRM_QUOTE_PRODUCT_ROWS_SAVING_ERROR"] = "Beim Speichern von Produkten ist ein Fehler aufgetreten.";
$MESS["CRM_QUOTE_DEAULT_TITLE"] = "Neues Angebot";
$MESS["CRM_QUOTE_NOT_FOUND"] = "Angebot wurde nicht gefunden.";
$MESS["CRM_QUOTE_ACCESS_DENIED"] = "Zugriff verweigert.";
$MESS["CRM_QUOTE_DELETION_ERROR"] = "Fehler beim Löschen des Angebots.";
?>