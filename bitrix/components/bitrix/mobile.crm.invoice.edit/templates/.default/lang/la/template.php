<?
$MESS["M_CRM_INVOICE_EDIT_CREATE_TITLE"] = "Crear factura";
$MESS["M_CRM_INVOICE_EDIT_CANCEL_BTN"] = "Cancelar";
$MESS["M_CRM_INVOICE_EDIT_SAVE_BTN"] = "Guardar";
$MESS["M_CRM_INVOICE_EDIT_CONTINUE_BTN"] = "Continuar";
$MESS["M_CRM_INVOICE_EDIT_EDIT_TITLE"] = "Editar";
$MESS["M_CRM_INVOICE_EDIT_VIEW_TITLE"] = "Ver factura";
$MESS["M_CRM_INVOICE_EDIT_CONVERT_TITLE"] = "Factura";
$MESS["M_DETAIL_DOWN_TEXT"] = "Soltar para actualizar...";
$MESS["M_DETAIL_LOAD_TEXT"] = "Actualizando...";
$MESS["M_CRM_INVOICE_MENU_EDIT"] = "Editar";
$MESS["M_CRM_INVOICE_MENU_DELETE"] = "Eliminar";
$MESS["M_CRM_INVOICE_CONVERSION_NOTIFY"] = "Campos requeridos";
$MESS["M_CRM_INVOICE_MENU_CREATE_ON_BASE"] = "Crear usando el origen";
$MESS["M_CRM_INVOICE_SELECT"] = "Seleccionar";
$MESS["M_CRM_INVOICE_MENU_HISTORY"] = "Historial";
$MESS["M_CRM_INVOICE_MENU_SEND_EMAIL"] = "Enviar e-mail";
$MESS["M_DETAIL_PULL_TEXT"] = "Pulsar hacia abajo para actualizar...";
?>