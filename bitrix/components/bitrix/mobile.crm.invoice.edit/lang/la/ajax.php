<?
$MESS["CRM_ACCESS_DENIED"] = "Acceso denegado";
$MESS["CRM_ENTITY_TYPE_NOT_SUPPORTED"] = "TYPE_NOT_SUPPORTED: Type '#ENTITY_TYPE#' is not supported in the current context.";
$MESS["CRM_ENTITY_DATA_NOT_FOUND"] = "DATA_NOT_FOUND: No hay datos para procesar.";
$MESS["CRM_ENTITY_ID_NOT_FOUND"] = "ID_NOT_FOUND: El ID no se pudo encontrar.";
$MESS["CRM_INVOICE_STATUS_NOT_FOUND"] = "STATUS_NOT_FOUND: No se pudo encontrar el estado.";
$MESS["CRM_INVOICE_COULD_NOT_SAVE_STATUS"] = "No se puede guardar el estado de la factura.";
$MESS["CRM_INVOICE_PRODUCT_ROWS_NOT_FOUND"] = "El SKU's no fueron encontrados";
$MESS["CRM_INVOICE_COULD_NOT_RECALCULATE"] = "No se pudo calcular SKU's.";
$MESS["CRM_INVOICE_TOPIC_IS_NOT_ASSIGNED"] = "Por favor, especifique el tema de la factura.";
$MESS["CRM_INVOICE_CLIENT_IS_NOT_ASSIGNED"] = "Por favor, especifique el cliente.";
$MESS["CRM_INVOICE_STATUS_IS_NOT_ASSIGNED"] = "Por favor especificar el estado de la factura.";
$MESS["CRM_INVOICE_PRODUCT_ROWS_ARE_EMPTY"] = "Por favor, seleccione al menos un producto.";
$MESS["CRM_INVOICE_EXISTING_ACCOUNT_NUMBER"] = "Una factura con este número ya existe";
$MESS["CRM_INVOICE_FIELD_CHECK_GENERAL_ERROR"] = "No se pudo guardar la factura porque los campos no son validos.";
$MESS["CRM_INVOICE_SAVING_GENERAL_ERROR"] = "No se pudo guardar la factura debido a un error desconocido.";
$MESS["CRM_INVOICE_NOT_FOUND"] = "El ID de la factura ##ID# no se pudo encontrar.";
$MESS["CRM_INVOICE_COULD_NOT_DELETE"] = "No se puede eliminar la factura.";
$MESS["CRM_INVOICE_COULD_NOT_LOAD_SALE_MODULE"] = "El módulo e-Store no está instalado.";
$MESS["CRM_INVOICE_COULD_NOT_FIND_SALE_ORDER"] = "El pedido no fue encontrado.";
$MESS["CRM_INVOICE_COULD_NOT_FIND_SALE_ORDER_PAY_SYSTEM"] = "No se ha encontrado la forma de pago.";
$MESS["CRM_INVOICE_COULD_NOT_FIND_SALE_ORDER_PERSON_TYPE"] = "No se encontró el tipo de cliente.";
$MESS["CRM_INVOICE_COULD_NOT_FIND_PAY_SYSTEM_HANDLER"] = "No se encontró el controlador de sistema de pago.";
$MESS["CRM_INVOICE_COULD_NOT_CREATE_FILE"] = "No se puede crear un archivo.";
$MESS["CRM_INVOICE_NO_PDF_CONTENT"] = "No se puede crear un documento PDF.";
$MESS["CRM_INVOICE_COULD_NOT_CREATE_WEBDAV_ELEMENT"] = "No se puede guardar el documento en la biblioteca de documentos.";
?>