<?
$MESS["CRM_ENTITY_DATA_NOT_FOUND"] = "DATA_NOT_FOUND: Impossible de trouver les données pour le traitement.";
$MESS["CRM_ENTITY_ID_NOT_FOUND"] = "ID_NOT_FOUND: ID n'est pas trouvé.";
$MESS["CRM_INVOICE_STATUS_NOT_FOUND"] = "STATUS_NOT_FOUND: Impossible de déterminer le statut.";
$MESS["CRM_ENTITY_TYPE_NOT_SUPPORTED"] = "TYPE_NOT_SUPPORTED: Le type '#ENTITY_TYPE#' n'est pas soutenu dans le contexte courant.";
$MESS["CRM_ACCESS_DENIED"] = "Accès interdit.";
$MESS["CRM_INVOICE_COULD_NOT_RECALCULATE"] = "Chec d'exécution de la révision des positions du produit.";
$MESS["CRM_INVOICE_COULD_NOT_LOAD_SALE_MODULE"] = "Impossible de charger le module 'sale'.";
$MESS["CRM_INVOICE_COULD_NOT_FIND_SALE_ORDER"] = "La recherche de la commande n'était pas réussie.";
$MESS["CRM_INVOICE_COULD_NOT_FIND_PAY_SYSTEM_HANDLER"] = "La tentative de trouver le traiteur du système de règlement a échoué.";
$MESS["CRM_INVOICE_COULD_NOT_FIND_SALE_ORDER_PAY_SYSTEM"] = "La recherche du mode de règlement a échoué";
$MESS["CRM_INVOICE_NOT_FOUND"] = "Impossible de trouver le compte avec l'identificateur '#ID#'.";
$MESS["CRM_INVOICE_COULD_NOT_FIND_SALE_ORDER_PERSON_TYPE"] = "La tentative de trouver le type de client a échoué.";
$MESS["CRM_INVOICE_PRODUCT_ROWS_NOT_FOUND"] = "Les positions de marchandises n'ont pas été trouvées.";
$MESS["CRM_INVOICE_NO_PDF_CONTENT"] = "On n'a pas réussi à créer un document PDF.";
$MESS["CRM_INVOICE_COULD_NOT_CREATE_FILE"] = "Impossible de créer le fichier.";
$MESS["CRM_INVOICE_COULD_NOT_CREATE_WEBDAV_ELEMENT"] = "La sauvegarde du document dans la bibliothèque des documents a échoué.";
$MESS["CRM_INVOICE_COULD_NOT_SAVE_STATUS"] = "Impossible de sauvegarder le statut de la facture.";
$MESS["CRM_INVOICE_FIELD_CHECK_GENERAL_ERROR"] = "Impossible de sauvegarder le compte: erreur générale de validation des champs.";
$MESS["CRM_INVOICE_SAVING_GENERAL_ERROR"] = "Chec de la sauvegarde d'une facture: erreur générale de la sauvegarde.";
$MESS["CRM_INVOICE_COULD_NOT_DELETE"] = "Impossible de supprimer le compte.";
$MESS["CRM_INVOICE_PRODUCT_ROWS_ARE_EMPTY"] = "Veuillez choisir au moins un produit.";
$MESS["CRM_INVOICE_CLIENT_IS_NOT_ASSIGNED"] = "Veuillez choisir un client.";
$MESS["CRM_INVOICE_STATUS_IS_NOT_ASSIGNED"] = "Veuillez indiquer le statut de la facture.";
$MESS["CRM_INVOICE_TOPIC_IS_NOT_ASSIGNED"] = "Indiquez le sujet de la facture, s'il vous plaît.";
$MESS["CRM_INVOICE_EXISTING_ACCOUNT_NUMBER"] = "La facture avec ce numéro existe déjà..";
?>