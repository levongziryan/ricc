<?
$MESS["INTR_ISS_PARAM_EMAIL"] = "Courrier électronique";
$MESS["INTR_ISS_PARAM_PHONE_INNER"] = "Recherche interne";
$MESS["INTR_ISS_PARAM_POST"] = "Emplacement";
$MESS["INTR_ISS_PARAM_KEYWORDS"] = "Mots-clés";
$MESS["INTR_ISS_PARAM_WORK_COMPANY"] = "Entreprise";
$MESS["INTR_ISS_BUTTON_SUBMIT"] = "Recherche";
$MESS["INTR_ISS_BUTTON_CANCEL"] = "Annuler";
$MESS["INTR_ISS_PARAM_DEPARTMENT"] = "Département";
$MESS["INTR_ISS_PARAM_DEPARTMENT_MINE"] = "Uniquement mon office";
$MESS["INTR_ISS_PARAM_FIO"] = "Dénomination";
?>