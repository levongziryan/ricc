<?
$MESS["DISK_EXTERNAL_LINK_LIST_LABEL_GRID_TOTAL"] = "Total";
$MESS["DISK_EXTERNAL_LINK_LIST_DELETE_GROUP_CONFIRM"] = "Voulez-vous désactiver les liens publiques sélectionnés?";
$MESS["DISK_EXTERNAL_LINK_LIST_SELECTED_OBJECT_1"] = "#COUNT# éléments sélectionnés";
$MESS["DISK_EXTERNAL_LINK_LIST_SELECTED_OBJECT_21"] = "#COUNT# éléments sélectionnés";
$MESS["DISK_EXTERNAL_LINK_LIST_SELECTED_OBJECT_2_4"] = "#COUNT# éléments sélectionnés";
$MESS["DISK_EXTERNAL_LINK_LIST_SELECTED_OBJECT_5_20"] = "#COUNT# éléments sélectionnés";
$MESS["DISK_EXTERNAL_LINK_LIST_DELETE_BUTTON"] = "Désactivé";
$MESS["DISK_EXTERNAL_LINK_LIST_CANCEL_DELETE_BUTTON"] = "Annuler";
$MESS["DISK_EXTERNAL_LINK_LIST_DELETE_TITLE"] = "Confirmation de suppression";
?>