<?
$MESS["TASKS_TASK_TEMPLATE_SET_NAVCHAIN"] = "Activer les fils d'Ariane";
$MESS["TASKS_TASK_TEMPLATE_PATH_TO_USER_TASKS"] = "Chemin d'accès aux tâches d'utilisateur";
$MESS["TASKS_TASK_TEMPLATE_PATH_TO_GROUP_TASKS"] = "Chemin d'accès à la liste des tâches";
$MESS["TASKS_TASK_TEMPLATE_PATH_TO_GROUP_TASKS_TASK"] = "Chemin d'accès à la tâche";
$MESS["TASKS_TASK_TEMPLATE_PATH_TO_USER_PROFILE"] = "Chemin d'accès au profil utilisateur";
$MESS["TASKS_TASK_TEMPLATE_PATH_TO_GROUP"] = "Chemin d'accès au groupe";
$MESS["TASKS_TASK_TEMPLATE_SHOW_RATING"] = "Activer les évaluations";
$MESS["TASKS_TASK_TEMPLATE_RATING_TYPE"] = "Apparence des boutons d'évaluation";
$MESS["TASKS_TASK_TEMPLATE_RATING_TYPE_CONFIG"] = "par défaut";
$MESS["TASKS_TASK_TEMPLATE_RATING_TYPE_LIKE_TEXT"] = "J'aime/Je n'aime pas (texte)";
$MESS["TASKS_TASK_TEMPLATE_RATING_TYPE_LIKE_GRAPHIC"] = "J'aime/Je n'aime pas (image)";
$MESS["TASKS_TASK_TEMPLATE_RATING_TYPE_STANDART_TEXT"] = "J'aime (texte)";
$MESS["TASKS_TASK_TEMPLATE_RATING_TYPE_STANDART_GRAPHIC"] = "J'aime (image)";
$MESS["TASKS_TASK_TEMPLATE_REDIRECT_ON_SUCCESS"] = "Rediriger vers l'URL de renvoi en cas de copie réussie";
?>