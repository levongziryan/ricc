<?
$MESS["TASKS_TT_TASKS_MODULE_NOT_INSTALLED"] = "O módulo de Tarefas não está instalado.";
$MESS["TASKS_TT_SOCIALNETWORK_MODULE_NOT_INSTALLED"] = "O módulo de Rede Social não está instalado.";
$MESS["TASKS_TT_FORUM_MODULE_NOT_INSTALLED"] = "O módulo de Fórum não está instalado.";
$MESS["TASKS_TT_NOT_FOUND_OR_NOT_ACCESSIBLE"] = "A tarefa não foi encontrada ou o acesso é negado.";
$MESS["TASKS_TT_NOT_FOUND_OR_NOT_ACCESSIBLE_COPY"] = "A tarefa a ser copiada não foi encontrada ou o acesso foi negado.";
$MESS["TASKS_TT_COPY_READ_ERROR"] = "Erro ao ler o objeto a ser copiado";
?>