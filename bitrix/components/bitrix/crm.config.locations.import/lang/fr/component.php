<?
$MESS["CRM_LOC_IMP_STEP_LENGTH_ERROR"] = "Vous devez indiquer la longueur non nulle du pas.";
$MESS["CRM_LOC_IMP_LOAD_ACCESS_DENIED"] = "Accès interdit";
$MESS["CRM_LOC_IMP_GFILE_ERROR"] = "Le fichier à charger n'est pas indiqué.";
$MESS["CRM_LOC_IMP_NO_LOC_FILE"] = "Le fichier des emplacements n'a pas été téléchargé.";
?>