<?
$MESS["CRM_LOC_IMP_STEP_CHECK"] = "A duração desta etapa não pode ser zero ou negativa.";
$MESS["CRM_LOC_IMP_JS_ERROR"] = "Erro";
$MESS["CRM_LOC_IMP_JS_IMPORT_SUCESS"] = "A importação de dados foi concluída com sucesso.";
$MESS["CRM_LOC_IMP_JS_IMPORT_PROCESS"] = "Importando dados...";
$MESS["CRM_LOC_IMP_JS_FILE_PROCESS"] = "Fazendo upload do arquivo...";
$MESS["CRM_LOC_IMP_TITLE"] = "Importar Locais";
$MESS["CRM_IMPORT_BUTTON"] = "Importar";
$MESS["CRM_LOC_IMP_CHOOSE_FILE"] = "Selecionar o arquivo de definição de local";
$MESS["CRM_LOC_IMP_FILE_RS"] = "Rússia e ex-URSS (cidades)";
$MESS["CRM_LOC_IMP_FILE_USA"] = "EUA (cidades)";
$MESS["CRM_LOC_IMP_FILE_CNTR"] = "Mundo (países)";
$MESS["CRM_LOC_IMP_FILE_FFILE"] = "fazer upload de arquivos";
$MESS["CRM_LOC_IMP_FILE_NONE"] = "nenhum";
$MESS["CRM_LOC_IMP_LOAD_ZIP"] = "upload de dados de código postal";
$MESS["CRM_LOC_IMP_SYNC"] = "Sincronização";
$MESS["CRM_LOC_IMP_SYNC_Y"] = "tentar sincronizar dados existentes e novos";
$MESS["CRM_LOC_IMP_SYNC_N"] = "excluir dados antigos";
$MESS["CRM_LOC_IMP_STEP_LENGTH"] = "Duração do passo (seg)";
$MESS["CRM_LOC_IMP_STEP_LENGTH_HINT"] = "Especifique a duração necessária do passo, em segundos. Não altere este parâmetro se você não tem certeza absoluta de que o valor que você está prestes a entrar é suficiente e que o script não vai acabar prematuramente.";
$MESS["CRM_CLOSE_BUTTON"] = "Fechar";
$MESS["CRM_CANCEL_BUTTON"] = "Cancelar";
$MESS["CRM_LOC_IMP_SYNC_NO_ZIP"] = "Apenas os códigos postais russos serão sincronizados.";
?>