<?
$MESS["F_NO_MODULE"] = "El módulo del foro no está instalado";
$MESS["F_ERR_FID_EMPTY"] = "La revisión del foro no se ha fijado";
$MESS["F_ERR_FID_IS_NOT_EXIST"] = "Revisiones para el foro #FORUM# no existen";
$MESS["COMM_COMMENT_OK"] = "El comentario fue guardado exitosamente";
$MESS["COMM_COMMENT_OK_AND_NOT_APPROVED"] = "El comentario ha sido agregado exitosamente. Esto se mostrará despúes que el moderador lo apruebe.";
$MESS["NAV_OPINIONS"] = "Revisiones";
$MESS["F_NO_MODULE_TASKS"] = "El módulo de las tareas no está instalado.";
$MESS["F_ERR_TID_EMPTY"] = "La tarea no se ha especificado.";
$MESS["F_ERR_TID_IS_NOT_EXIST"] = "La tarea #TASK_ID# no fue encontrada.";
$MESS["F_ERR_FORUM_NO_ACCESS"] = "No tienes permiso para ver los comentarios.";
?>