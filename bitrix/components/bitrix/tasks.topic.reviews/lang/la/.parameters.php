<?
$MESS["F_MESSAGES_PER_PAGE"] = "Número de mensajes por página";
$MESS["F_PATH_TO_SMILE"] = "Ruta a la carpeta Smiles (relativo a la raíz)";
$MESS["F_FORUM_ID"] = "ID del Foro";
$MESS["F_PROFILE_VIEW_TEMPLATE"] = "Página de perfil";
$MESS["F_USE_CAPTCHA"] = "Usar CAPTCHA";
$MESS["F_READ_TEMPLATE"] = "Página de lectura de tema";
$MESS["F_PREORDER"] = "Visualizar los mensajes en orden de adelante hacia atrás";
$MESS["F_DATE_TIME_FORMAT"] = "Formato de fecha y hora ";
$MESS["F_DISPLAY_PANEL"] = "Muestra los botones del panel de este componente";
$MESS["F_PAGE_NAVIGATION_TEMPLATE"] = "Nombre de la plantilla del buscador";
$MESS["F_DETAIL_TEMPLATE"] = "Página del elemento del block de información";
$MESS["F_POST_FIRST_MESSAGE"] = "Utilice el texto de este elemento para iniciar un tema";
$MESS["F_POST_FIRST_MESSAGE_TEMPLATE"] = "Plantilla del primer post en el tema";
$MESS["F_TASK_ID"] = "ID de la tarea";
$MESS["SHOW_RATING"] = "Habilitar calificación";
$MESS["SHOW_RATING_CONFIG"] = "predeterminado";
$MESS["RATING_TYPE"] = "Diseño de los botones de calificación";
$MESS["RATING_TYPE_CONFIG"] = "predeterminado";
$MESS["RATING_TYPE_STANDART_TEXT"] = "Me gusta/No me gusta (texto)";
$MESS["RATING_TYPE_STANDART_GRAPHIC"] = "Me gusta/No me gusta (imagen)";
$MESS["RATING_TYPE_LIKE_TEXT"] = "Me gusta (texto)";
$MESS["RATING_TYPE_LIKE_GRAPHIC"] = "Me gusta (imagen)";
$MESS["F_NAME_TEMPLATE"] = "Formato de nombre";
?>