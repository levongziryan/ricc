<?
$MESS["F_ERR_SESSION_TIME_IS_UP"] = "Su sesión ha expirado. Por favor reenvíe su mensaje.";
$MESS["F_ERR_NO_REVIEW_TEXT"] = "Por favor ingrese sus comentarios.";
$MESS["COMM_COMMENT_OK"] = "Su comentario fue guardado exitosamente.";
$MESS["POSTM_CAPTCHA"] = "El código CAPTCHA es incorrecto.";
$MESS["F_ERR_NOT_RIGHT_FOR_ADD"] = "Permisos insuficientes para agregar los comentarios.";
$MESS["F_ERR_ADD_TOPIC"] = "Error al crear el tema.";
$MESS["F_ERR_ADD_MESSAGE"] = "Error al crear el post.";
$MESS["F_FORUM_TOPIC_ID"] = "Tema del Foro";
$MESS["F_FORUM_MESSAGE_CNT"] = "Comentarios";
$MESS["TASKS_COMMENT_SONET_NEW_TASK_MESSAGE"] = "Tarea creada";
$MESS["TASKS_COMMENT_MESSAGE_ADD"] = "Comentó \"#TASK_TITLE#\": \"#TASK_COMMENT_TEXT#\"";
$MESS["TASKS_COMMENT_MESSAGE_EDIT"] = "Modificar el comentario \"#TASK_TITLE#\", new text: \"#TASK_COMMENT_TEXT#\"";
$MESS["F_ERR_REMOVE_COMMENT"] = "Error al intentar eliminar comentario.";
$MESS["TASKS_COMMENT_MESSAGE_ADD_F"] = "Añadir un comentario para \"#TASK_TITLE#\". El texto del comentario es: \"#TASK_COMMENT_TEXT#\".";
$MESS["TASKS_COMMENT_MESSAGE_EDIT_F"] = "Comentario actualizado al \"#TASK_TITLE#\". El nuevo texto es: \"#TASK_COMMENT_TEXT#\".";
$MESS["TASKS_COMMENT_MESSAGE_ADD_M"] = "Agregar un comentario a la tarea \"#TASK_TITLE#\", texto del comentario: \"#TASK_COMMENT_TEXT#\"";
$MESS["TASKS_COMMENT_MESSAGE_EDIT_M"] = "Un comentario a cambiado \"#TASK_TITLE#\". El texto nuevo\"#TASK_COMMENT_TEXT#\".";
?>