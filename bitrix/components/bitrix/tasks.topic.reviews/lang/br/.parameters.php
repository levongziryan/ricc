<?
$MESS["F_DATE_TIME_FORMAT"] = "Formato de data e hora";
$MESS["F_PREORDER"] = "Mostrar mensagens em ordem crescente";
$MESS["F_DISPLAY_PANEL"] = "Exibir botões do painel para este componente";
$MESS["F_FORUM_ID"] = "ID do Fórum";
$MESS["F_DETAIL_TEMPLATE"] = "Página de elementos do Bloco de Informações";
$MESS["F_PAGE_NAVIGATION_TEMPLATE"] = "Nome do modelo de pager";
$MESS["F_MESSAGES_PER_PAGE"] = "Mensagens por página";
$MESS["F_PATH_TO_SMILE"] = "Caminho para a pasta com as imagens Smilyes";
$MESS["F_PROFILE_VIEW_TEMPLATE"] = "Página do Perfil do Usuário";
$MESS["F_POST_FIRST_MESSAGE_TEMPLATE"] = "Texto para o primeiro post do tópico";
$MESS["F_READ_TEMPLATE"] = "Página de Leitura do Tópico (- valor vazio - Começa a partir das configurações do fórum)";
$MESS["F_USE_CAPTCHA"] = "Usar CAPTCHA";
$MESS["F_POST_FIRST_MESSAGE"] = "Usar o texto deste elemento para iniciar um tópico";
$MESS["F_TASK_ID"] = "ID da Tarefa";
$MESS["SHOW_RATING"] = "Habilitar classificação";
$MESS["SHOW_RATING_CONFIG"] = "Padrão";
$MESS["RATING_TYPE"] = "Desing dos botões de Classificação";
$MESS["RATING_TYPE_CONFIG"] = "Padrão";
$MESS["RATING_TYPE_STANDART_TEXT"] = "Curtir/Curtir (Desfazer) (Texto)";
$MESS["RATING_TYPE_STANDART_GRAPHIC"] = "Curtir/Curtir (Desfazer) (imagem)";
$MESS["RATING_TYPE_LIKE_TEXT"] = "Curtir (texto)";
$MESS["RATING_TYPE_LIKE_GRAPHIC"] = "Curtir (imagem)";
$MESS["F_NAME_TEMPLATE"] = "Formato do Nome";
?>