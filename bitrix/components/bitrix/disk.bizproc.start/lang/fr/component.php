<?
$MESS["BPABS_TITLE"] = "Démarrer un processus business";
$MESS["BPABS_EMPTY_DOC_ID"] = "Aucun document ID pour lequel la procédure d'entreprise doit être créée n'a été spécifiée.";
$MESS["BPATT_NO_MODULE_ID"] = "Le module des identifiants est requis.";
$MESS["BPABS_EMPTY_DOC_TYPE"] = "Le type de document n'a pas été indiqué.";
$MESS["BPABS_NO_PERMS"] = "Accès interdit.";
?>