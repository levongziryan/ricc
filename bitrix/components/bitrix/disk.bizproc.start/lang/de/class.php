<?
$MESS["BPABS_EMPTY_DOC_ID"] = "Keine Dokument-ID angegeben, für die der Geschäftsprozess erstellt werden soll.";
$MESS["BPABS_TITLE"] = "Geschäftsprozess ausführen";
$MESS["BPABS_EMPTY_DOC_TYPE"] = "Dokumenttyp erforderlich";
$MESS["BPATT_NO_MODULE_ID"] = "Die Modul-ID ist erforderlich.";
$MESS["BPABS_NO_PERMS"] = "Sie haben nicht genügend Rechte um einen Geschäftsprozess für dieses Dokument zu starten.";
?>