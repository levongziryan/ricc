<?
$MESS["BPABS_EMPTY_DOC_ID"] = "No hay ningún ID del documento especificado para el proceso de negocio que va a crear.";
$MESS["BPABS_TITLE"] = "Ejecutar Procesos de Negocio";
$MESS["BPABS_EMPTY_DOC_TYPE"] = "Es necesario el tipo de documento.";
$MESS["BPATT_NO_MODULE_ID"] = "El ID del módulo es necesario.";
$MESS["BPABS_NO_PERMS"] = "Usted no tiene permiso para ejecutar un proceso de negocio para este documento.";
?>