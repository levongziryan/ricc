<?
$MESS["BPABS_MESSAGE_SUCCESS"] = "O processo comercial '#TEMPLATE#' foi iniciado com sucesso.";
$MESS["BPABS_TAB"] = "Processo Comercial";
$MESS["BPABS_TAB_TITLE"] = "Parâmetros de Execução de Processo Comercial ";
$MESS["BPABS_DESCRIPTION"] = "Descrição do Modelo de Processo Comercial ";
$MESS["BPABS_NAME"] = "Nome do Modelo de Processo Comercial ";
$MESS["BPABS_DO_CANCEL"] = "Cancelar";
$MESS["BPABS_MESSAGE_ERROR"] = "Não foi possível iniciar um processo comercial do tipo'#TEMPLATE#'.";
$MESS["BPABS_NO"] = "Não";
$MESS["BPABS_NO_TEMPLATES"] = "Não foi encontrado nenhum modelo de processo comercial para este tipo de documento.";
$MESS["BPABS_DO_START"] = "Iniciar";
$MESS["BPABS_INVALID_TYPE"] = "O tipo de parâmetro está indefinido.";
$MESS["BPABS_YES"] = "Sim";
$MESS["DISK_TITLE_TEMPLATES_OLD"] = "Modelos antigos criados antes da conversão para a nova unidade!";
?>