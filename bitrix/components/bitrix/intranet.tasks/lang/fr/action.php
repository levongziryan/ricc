<?
$MESS["INTL_TASK_INTERNAL_ERROR"] = "Erreur intérieure.";
$MESS["INTL_CAN_NOT_FINISH"] = "Vous ne pouvez pas effectuer cette tâche.";
$MESS["INTL_CAN_NOT_REJECT_OWN"] = "Vous ne pouvez pas rejeter votre tâche.";
$MESS["INTL_CAN_NOT_REJECT"] = "Vous ne pouvez pas décliner cette tâche.";
$MESS["INTL_CAN_NOT_APPLY"] = "Vous ne pouvez pas prendre cette tâche.";
$MESS["INTL_FINISH_MESSAGE"] = "Tâche '#NAME#' terminée

[url=#URL_VIEW#]Voir les détails[/url].";
$MESS["INTL_TASK_NOT_FOUND"] = "Tâche introuvable.";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "Le module des blocs d'informations n'a pas été installé.";
$MESS["EC_SONET_MODULE_NOT_INSTALLED"] = "Le module du réseau social n'a pas été installé.";
$MESS["INTL_NO_FOLDER_ID"] = "Code du dossier non renseigné";
$MESS["INTL_EMPTY_FOLDER_NAME"] = "Nom du dossier non renseigné.";
$MESS["INTL_REJECT_MESSAGE"] = "La page n'est pas accepté à l'exécution '#NAME#'.

[url=#URL_VIEW#]Voir les détails[/url]";
$MESS["INTL_SECURITY_ERROR"] = "Erreur de sécurité";
$MESS["INTL_ERROR_DELETE_TASK"] = "Impossible de supprimer la tâche.";
$MESS["INTL_FOLDER_DELETE_ERROR"] = "Erreur de la suppression du dossier.";
$MESS["INTL_FOLDER_NOT_FOUND"] = "Dossier n'est pas retrouvé";
$MESS["INTL_VIEW_NOT_FOUND"] = "Affichage.";
$MESS["INTL_WRONG_VIEW"] = "Affichage incorrecte.";
$MESS["INTL_APPLY_MESSAGE"] = "La tâche '#NAME#'

[url=#URL_VIEW#]Voir les détails[/url] est acceptée pour exécution.";
$MESS["INTL_NO_VIEW_PERMS"] = "Vous n'avez pas de droits pour changer des représentations communes.";
$MESS["INTL_NO_FOLDER_PERMS"] = "Vous n'avez pas le droits de modifier les dossiers.";
?>