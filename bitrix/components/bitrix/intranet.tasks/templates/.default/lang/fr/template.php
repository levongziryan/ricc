<?
$MESS["INTST_OUTLOOK_WARNING"] = "<small><b>Attention !</b> Avant la mise en uvre de la synchronisation des tâches avec Microsoft Outlook il est recommandé de synchroniser les utilisateurs du système.</small>";
$MESS["INTST_CLOSE"] = "Fermer";
$MESS["INTST_FOLDER_NAME"] = "Dénomination du dossier";
$MESS["INTST_CANCEL"] = "Annuler";
$MESS["INTST_SAVE"] = "Sauvegarder";
$MESS["INTST_DELETE"] = "Supprimer";
?>