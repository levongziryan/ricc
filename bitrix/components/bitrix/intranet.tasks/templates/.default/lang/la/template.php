<?
$MESS["INTST_CLOSE"] = "Cerrar";
$MESS["INTST_FOLDER_NAME"] = "Nombre de la Carpeta";
$MESS["INTST_DELETE"] = "Borrar";
$MESS["INTST_SAVE"] = "Guardar";
$MESS["INTST_CANCEL"] = "Cancelar";
$MESS["INTST_OUTLOOK_WARNING"] = "<small><b>Atención!</b> Se recomienda sincronizar los contactos antes de comenzar la tarea de sincronización con Microsoft Outlook.</small>";
?>