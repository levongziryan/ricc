<?
$MESS["DISK_FILE_VIEW_FILE_PROPERTIES"] = "Propriedades do Documento";
$MESS["DISK_FILE_VIEW_FILE_SIZE"] = "Tamanho";
$MESS["DISK_FILE_VIEW_FILE_UPDATE_TIME"] = "Modificado em";
$MESS["DISK_FILE_VIEW_FILE_DOWNLOAD"] = "Baixar";
$MESS["DISK_FILE_VIEW_FILE_OWNER"] = "Proprietário";
$MESS["DISK_FILE_VIEW_FILE_UPLOAD_VERSION"] = "Carregar nova versão";
$MESS["DISK_FILE_VIEW_FILE_EDIT"] = "Editar";
$MESS["DISK_FILE_VIEW_EXTERNAL_LINK"] = "Link público";
$MESS["DISK_FILE_VIEW_EXT_LINK_ON"] = "Ligado";
$MESS["DISK_FILE_VIEW_EXT_LINK_OFF"] = "Desligado";
$MESS["DISK_FILE_VIEW_USAGE"] = "Usado em";
$MESS["DISK_FILE_VIEW_COPY_INTERNAL_LINK"] = "Copiar link interno";
$MESS["DISK_FILE_VIEW_BTN_CLOSE"] = "Fechar";
$MESS["DISK_FILE_VIEW_ENTITY_MEMBERS"] = "Participantes";
$MESS["DISK_FILE_VIEW_ENTITY_MEMBERS_NEXT_PAGE"] = "Mais...";
$MESS["DISK_FILE_VIEW_VERSION_LABEL_GRID_TOTAL"] = "Total";
$MESS["DISK_FILE_VIEW_VERSION_DELETE_TITLE"] = "Confirmar exclusão";
$MESS["DISK_FILE_VIEW_VERSION_DELETE_GROUP_CONFIRM"] = "Você tem certeza que deseja excluir as versões selecionadas?";
$MESS["DISK_FILE_VIEW_VERSION_DELETE_BUTTON"] = "Excluir";
$MESS["DISK_FILE_VIEW_VERSION_CANCEL_DELETE_BUTTON"] = "Cancelar";
$MESS["DISK_FILE_VIEW_VERSION_RESTORE_CONFIRM"] = "Você deseja restaurar a versão deste documento?";
$MESS["DISK_FILE_VIEW_VERSION_RESTORE_BUTTON"] = "Restaurar";
$MESS["DISK_FILE_VIEW_VERSION_CANCEL_BUTTON"] = "Cancelar";
$MESS["DISK_FILE_VIEW_VERSION_RESTORE_TITLE"] = "Restaurar versão";
$MESS["DISK_FILE_VIEW_VERSION_DELETE_VERSION_CONFIRM"] = "Você tem certeza que deseja excluir esta versão?";
$MESS["DISK_FILE_VIEW_VERSION_DELETE_VERSION_BUTTON"] = "Excluir";
$MESS["DISK_FILE_VIEW_VERSION_DELETE_VERSION_TITLE"] = "Excluir versão";
$MESS["DISK_FILE_VIEW_BIZPROC"] = "Processos comerciais";
$MESS["DISK_FILE_VIEW_BIZPROC_STATE_MODIFIED"] = "Data de status atual:";
$MESS["DISK_FILE_VIEW_BIZPROC_STATE_TITLE"] = "Status atual:";
$MESS["DISK_FILE_VIEW_BIZPROC_DELETE_BUTTON"] = "Excluir";
$MESS["DISK_FILE_VIEW_BIZPROC_STOP_BUTTON"] = "Parar";
$MESS["DISK_FILE_VIEW_BIZPROC_LOG_BUTTON"] = "Histórico";
$MESS["DISK_FILE_VIEW_BIZPROC_NEW_BIZPROC_TITLE"] = "Novo processo comercial";
$MESS["DISK_FILE_VIEW_BIZPROC_NEW_BIZPROC_START"] = "Executar novo processo comercial:";
$MESS["DISK_FILE_VIEW_BIZPROC_LOG_BIZPROC"] = "Log de processo comercial";
$MESS["DISK_FILE_VIEW_BIZPROC_LIST_BIZPROC"] = "Processos comerciais";
$MESS["DISK_FILE_VIEW_BIZPROC_MODIFICATION"] = "Alterações recentes:";
$MESS["DISK_FILE_VIEW_BIZPROC_TASKS_BIZPROC"] = "Tarefa de processo comercial:";
$MESS["DISK_FILE_VIEW_BIZPROC_RUN_CMD"] = "Executar comando";
$MESS["DISK_FILE_VIEW_BIZPROC_RUN_CMD_NO"] = "Não executar";
$MESS["DISK_FILE_VIEW_BIZPROC_APPLY"] = "Aplicar";
$MESS["DISK_FILE_VIEW_BIZPROC_LABEL_START"] = "Fornecer parâmetros de processo comercial";
$MESS["DISK_FILE_VIEW_BIZPROC_LABEL_START_DIALOG"] = "Fornecer parâmetros de processo comercial";
$MESS["DISK_FILE_VIEW_TAB_PROPERTIES"] = "Propriedades";
$MESS["DISK_FILE_VIEW_TAB_HISTORY"] = "Histórico de revisão";
$MESS["DISK_FILE_VIEW_TAB_SHARING"] = "Compartilhando";
$MESS["DISK_FILE_VIEW_FF_EXTENSION_NAME"] = "Integração Bitrix24";
$MESS["DISK_FILE_VIEW_FF_EXTENSION_UPDATE"] = "Atualize o completo de suporte do navegador do documento do office <a href=\"#LINK#\">\"#NAME#\"</a>.";
$MESS["DISK_FILE_VIEW_FF_EXTENSION_INSTALL"] = "Instale o <a href=\"#LINK#\">\"#NAME#\"</a> complemento para Firefox.";
$MESS["DISK_FILE_VIEW_FF_EXTENSION_TITLE"] = "Complemento Firefox";
$MESS["DISK_FILE_VIEW_FF_EXTENSION_HELP"] = "Esta extensão oferece suporte para editar documentos do office sem baixá-los do computador local. É necessário um dos seguintes softwares do office: Microsoft Office, OpenOffice.org ou LibreOffice.";
$MESS["DISK_FILE_VIEW_FF_EXTENSION_DISABLE"] = "<input type=\"checkbox\" name=\"ff_extension_disable\" value=\"true\">&nbsp;Nunca me lembre de instalar este complemento novamente";
$MESS["DISK_FILE_VIEW_BTN_INSTALL"] = "Instalar Complemento";
$MESS["DISK_FILE_VIEW_BTN_UPDATE"] = "Atualizar Complemento";
$MESS["DISK_FILE_VIEW_BTN_INSTALL_CANCEL"] = "Fechar";
$MESS["DISK_FILE_VIEW_BTN_OPEN"] = "Não Instalar";
$MESS["DISK_FILE_VIEW_MENU_FF_EXTENSION_TEXT"] = "Complemento Firefox";
$MESS["DISK_FILE_VIEW_MENU_FF_EXTENSION_TITLE"] = "Integração Bitrix24";
$MESS["DISK_FILE_VIEW_TAB_USER_FIELDS"] = "Campos personalizados";
$MESS["DISK_FILE_VIEW_TITLE_USER_FIELDS"] = "Campos personalizados do documento";
$MESS["DISK_FILE_VIEW_LINK_EDIT_USER_FIELDS"] = "Editar";
$MESS["DISK_FILE_VIEW_BTN_EDIT_USER_FIELDS"] = "Salvar";
$MESS["DISK_FILE_VIEW_BTN_DISCARD_USER_FIELDS"] = "Cancelar";
?>