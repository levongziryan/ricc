<?
$MESS["DISK_FILE_VIEW_ERROR_COULD_NOT_FIND_OBJECT"] = "O objeto não pôde ser encontrado.";
$MESS["DISK_FILE_VIEW_ERROR_COULD_NOT_CREATE_FIND_EXT_LINK"] = "Não é possível criar ou encontrar um link público para o objeto.";
$MESS["DISK_FILE_VIEW_ERROR_COULD_NOT_FIND_VERSION"] = "A versão não pôde ser encontrada.";
$MESS["DISK_FILE_VIEW_BIZPROC_LOAD"] = "Não é possível incluir o módulo de Processos Comerciais.";
?>