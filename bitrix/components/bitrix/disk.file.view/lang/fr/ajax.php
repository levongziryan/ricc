<?
$MESS["DISK_FILE_VIEW_ERROR_COULD_NOT_FIND_VERSION"] = "Version introuvable";
$MESS["DISK_FILE_VIEW_ERROR_COULD_NOT_FIND_OBJECT"] = "Impossible de  trouver l'objet.";
$MESS["DISK_FILE_VIEW_BIZPROC_LOAD"] = "Impossible de connecter le module des procédures d'entreprises.";
$MESS["DISK_FILE_VIEW_ERROR_COULD_NOT_CREATE_FIND_EXT_LINK"] = "Impossible de créer ou trouver le lien public vers l'objet.";
?>