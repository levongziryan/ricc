<?
$MESS["CRM_LEAD_LIST_REBUILD_DUPLICATE_INDEX_COMPLETED_SUMMARY"] = "Los Prospectos duplicados han sido re-indexados. Elementos procesados: #PROCESSED_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_DUPLICATE_INDEX_PROGRESS_SUMMARY"] = "Prospectos procesados: #PROCESSED_ITEMS# de #TOTAL_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_DUPLICATE_INDEX_NOT_REQUIRED_SUMMARY"] = "Índice de prospectos duplicados no se debe volver a crear.";
$MESS["CRM_LEAD_LIST_ROW_COUNT"] = "Total: #ROW_COUNT#";
$MESS["CRM_LEAD_LIST_REBUILD_STATISTICS_NOT_REQUIRED_SUMMARY"] = "Datos estadísticos de prospectos están actualizados.";
$MESS["CRM_LEAD_LIST_REBUILD_STATISTICS_PROGRESS_SUMMARY"] = "Porspectos procesados:#PROCESSED_ITEMS# of #TOTAL_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_STATISTICS_COMPLETED_SUMMARY"] = "Procesamiento listo de datos estadísticos para los prospectos. Prospectos procesados: #PROCESSED_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_SEMANTICS_NOT_REQUIRED_SUMMARY"] = "El campos servicio del prospecto no requieren actualización.";
$MESS["CRM_LEAD_LIST_REBUILD_SEMANTICS_PROGRESS_SUMMARY"] = "Prospectos procesados: #PROCESSED_ITEMS# of #TOTAL_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_SEMANTICS_COMPLETED_SUMMARY"] = "Realizada la actualización del campos servicio del prospecto. Prospectos procesados: #PROCESSED_ITEMS#.";
$MESS["CRM_LEAD_LIST_DELETION_COMPLETED_SUMMARY"] = "Se han eliminado los prospectos. Prospectos procesados: #PROCESSED_ITEMS#.";
$MESS["CRM_LEAD_LIST_DELETION_PROGRESS_SUMMARY"] = "Eliminar prospecto: #PROCESSED_ITEMS# of #TOTAL_ITEMS#.";
$MESS["CRM_LEAD_LIST_DELETION_PARAM_ERROR"] = "La operación de eliminación ha sido anulada porque existen parámetros no válidos.";
$MESS["CRM_LEAD_LIST_DELETION_ACCESS_ERROR"] = "La operación de eliminación ha sido anulada porque no había suficientes permisos.";
$MESS["CRM_LEAD_LIST_DELETION_FILTER_NOT_FOUND_ERROR"] = "La operación de eliminación ha sido anulada porque no se encontraron las preferencias de filtro. Por favor, actualice la página y vuelva a intentarlo.";
$MESS["CRM_LEAD_LIST_DELETION_FILTER_OUTDATED_ERROR"] = "La operación de eliminación se ha cancelado porque las preferencias de filtro estan obsoletas. Por favor, actualice la página y vuelva a intentarlo.";
$MESS["CRM_LEAD_LIST_REBUILD_SEARCH_CONTENT_NOT_REQUIRED_SUMMARY"] = "El índice de búsqueda del prospecto no necesita ser recreado.";
$MESS["CRM_LEAD_LIST_REBUILD_SEARCH_CONTENT_PROGRESS_SUMMARY"] = "Prospectos procesados: #PROCESSED_ITEMS# of #TOTAL_ITEMS#.";
$MESS["CRM_LEAD_LIST_REBUILD_SEARCH_CONTENT_COMPLETED_SUMMARY"] = "Se ha recreado el índice de búsqueda del prospecto. Prospectos procesados: #PROCESSED_ITEMS#.";
?>