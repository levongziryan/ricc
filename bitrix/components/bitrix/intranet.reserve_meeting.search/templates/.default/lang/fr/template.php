<?
$MESS["INTASK_C31T_ANY"] = "(n'importe lesquel(el)s)";
$MESS["INTASK_C31T_STIME"] = "Temps";
$MESS["INTASK_C31T_SDATE"] = "Date";
$MESS["INTASK_C31T_FDATE"] = "Date";
$MESS["INTASK_C31T_FRESERVE"] = "Faire la réservation";
$MESS["INTASK_C31T_SEARCH"] = "Recherche";
$MESS["INTASK_C31T_FPLACE"] = "Nombre de places";
$MESS["INTASK_C31T_SPLACE"] = "Nombre de places nécessaires";
$MESS["INTASK_C31T_DURATION"] = "Nombre d'heures nécessaire";
$MESS["INTASK_C31T_ROOM"] = "Chambre";
$MESS["INTASK_C31T_FROOM"] = "Chambre";
$MESS["INTDT_NO_TASKS"] = "Il n'y a pas de salles de réunion";
$MESS["INTASK_C31T_TO"] = "jusqu'à";
$MESS["INTASK_C31T_FROM"] = "de";
$MESS["INTASK_C31T_FFREE"] = "Loisirs";
$MESS["INTASK_C31T_FFLOOR"] = "Etage";
?>