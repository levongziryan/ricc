<?
$MESS["INAF_F_ID"] = "ID";
$MESS["INAF_F_PLACE"] = "Nombre de places";
$MESS["EC_INTRANET_MODULE_NOT_INSTALLED"] = "Le module 'Intranet' n'a pas été installé.";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "Le module des blocs d'informations n'a pas été installé.";
$MESS["INAF_F_NAME"] = "Titre";
$MESS["INAF_F_DESCRIPTION"] = "Description";
$MESS["INTASK_C36_PAGE_TITLE"] = "Recherche des salles de réunion";
$MESS["INAF_F_PHONE"] = "Numéro de téléphone";
$MESS["INTS_NO_IBLOCK_PERMS"] = "Vous n'avez pas la permission d'afficher le bloc d'information de la salle de réunion.";
$MESS["INAF_F_FLOOR"] = "Etage";
?>