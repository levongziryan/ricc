<?
$MESS["INTL_VARIABLE_ALIASES"] = "Apelidos variáveis";
$MESS["INTL_IBLOCK_TYPE"] = "Tipo de Bloco de Informação";
$MESS["INTL_IBLOCK"] = "Bloco de informação";
$MESS["INTL_MEETING_VAR"] = "Variável para ID de sala de reunião";
$MESS["INTL_PAGE_VAR"] = "Página Variável";
$MESS["INTL_PATH_TO_MEETING"] = "Página de Agendamento de Reserva da Sala";
$MESS["INTL_PATH_TO_MEETING_LIST"] = "Página Principal para Reserva da Sala de Reunião";
$MESS["INTL_PATH_TO_RESERVE_MEETING"] = "Página para Reserva da Sala de Reunião";
$MESS["INTL_PATH_TO_MODIFY_MEETING"] = "Página do Editor de Parâmetros da Sala de Reunião";
$MESS["INTL_SET_NAVCHAIN"] = "Definir Trilha de Navegação";
$MESS["INTL_USERGROUPS_MODIFY"] = "Grupos de usuários autorizados a editarem a agenda da sala de reunião";
$MESS["INTL_USERGROUPS_RESERVE"] = "Grupos de Usuários autorizados a reservarem salas de reunião";
$MESS["INTL_USERGROUPS_CLEAR"] = "Grupos de usuários autorizados a cancelarem reservas de salas de reunião";
$MESS["INTL_MEETING_ID"] = "ID da Sala de Reunião";
?>