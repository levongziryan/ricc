<?
$MESS["CRM_CALL_LIST_ENTITY_NAME"] = "Prénom";
$MESS["CRM_CALL_LIST_ITEM_STATUS"] = "Statut";
$MESS["CRM_CALL_LIST_ITEM_CALL_RECORD"] = "Enregistrer";
$MESS["CRM_CALL_LIST_ITEM_WEBFORM_ACTIVITY"] = "Activité du formulaire GRC";
$MESS["CRM_CALL_LIST_ITEM_CREATED"] = "Date de création";
$MESS["CRM_CALL_LIST_ERROR_WRONG_ITEM_TYPE"] = "La liste des appels ne peut inclure d'entités de types différents";
$MESS["CRM_CALL_LIST_ENTITIES_ADDED"] = "#ENTITIES# ajoutés à la liste des appels";
$MESS["CRM_CALL_LIST_ITEM_FORM"] = "Formulaire ##ID#";
$MESS["VOXIMPLANT_MODULE_NOT_INSTALLED"] = "Le module Téléphonie n'est pas installé.";
$MESS["CRM_CALL_LIST_HAND_PICKED"] = "#ENTITIES# ont été manuellement sélectionnées";
$MESS["CRM_CALL_LIST_FILTERED"] = "#ENTITIES# ont été sélectionnées selon";
$MESS["CRM_CALL_LIST_DELETE"] = "Supprimer";
$MESS["CRM_CALL_LIST_ADD_MORE"] = "ajouter plus";
?>