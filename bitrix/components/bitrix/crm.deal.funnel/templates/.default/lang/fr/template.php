<?
$MESS["CRM_DEAL_FUNNEL_TYPE_SELECTOR_TITLE"] = "Type d'entonnoir";
$MESS["DEAL_STAGES_LOSE"] = "inactif";
$MESS["FUNNEL_CHART_NO_DATA"] = "Pas de données pour construire un diagramme";
$MESS["FUNNEL_CHART_SHOW"] = "Afficher le diagramme";
$MESS["CRM_DEAL_FUNNEL_SHOW_FILTER"] = "Voir la section de menu / masquer";
$MESS["DEAL_STAGES_WON"] = "Transactions en cours";
$MESS["FUNNEL_CHART_HIDE"] = "Cacher le diagramme";
$MESS["CRM_DEAL_FUNNEL_SHOW_FILTER_SHORT"] = "Filtre";
$MESS["CRM_DEAL_CATEGORY_SELECTOR_TITLE"] = "Pipeline";
?>