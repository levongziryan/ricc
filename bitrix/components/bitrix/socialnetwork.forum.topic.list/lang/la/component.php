<?
$MESS["F_FID_IS_EMPTY"] = "El foro de la red social no está especificado.";
$MESS["F_FID_IS_LOST"] = "El foro de la red social no fue encontrado.";
$MESS["F_TOPIC_LIST"] = "Temas";
$MESS["F_ERR_SESS_FINISH"] = "Su sesión ha expirado. Por favor repita la operación";
$MESS["F_NO_MODULE"] = "El módulo Forum no está instalado";
$MESS["FORUM_SONET_MODULE_NOT_AVAIBLE"] = "El foro no está disponible para este usuario.";
$MESS["SONET_MODULE_NOT_INSTALL"] = "El módulo Social Network no está instalada.";
$MESS["FORUM_SONET_NO_ACCESS"] = "Usted no cuenta con permiso para ver el foro.";
$MESS["FL_FORUM_CHAIN"] = "Foro";
$MESS["F_ERR_EMPTY_ACTION"] = "No ha seleccionado una acción.";
$MESS["F_ERR_EMPTY_TOPICS"] = "Ningún tema fue seleccionado.";
$MESS["F_ERR_TOPICS_NOT_MODERATION"] = "Los siguientes temas no pueden ser moderados:#TOPICS#.";
$MESS["SOCNET_FORUM_TL_EMAIL_RULE"] = "Agregar publicaciones a los foros de redes sociales";
?>