<?
$MESS["IM_VI_CALL"] = "Chamada telefônica";
$MESS["IM_DIALOG_LOADING"] = "Carregando... Aguarde.";
$MESS["IM_MESSENGER_NOT_DELIVERED"] = "a mensagem não foi entregue";
$MESS["IM_MESSENGER_DELIVERED"] = "a mensagem está sendo entregue";
$MESS["IM_MESSENGER_MESSAGE_EMPTY"] = "Não há mensagens";
$MESS["IM_MESSENGER_MESSAGE_PULLTEXT"] = "Puxe para carregar o histórico";
$MESS["IM_MESSENGER_MESSAGE_DOWNTEXT"] = "Solte para carregar o histórico";
$MESS["IM_MESSENGER_MESSAGE_LOADTEXT"] = "Carregando histórico ...";
$MESS["IM_MESSENGER_MESSAGE_NEW"] = "Enviar mensagem ...";
$MESS["IM_MESSENGER_MESSAGE_SEND"] = "Enviar";
$MESS["IM_MESSENGER_FORMAT_DATE"] = "g: i a, d F Y";
$MESS["IM_MESSENGER_FORMAT_TIME"] = "g: i a";
$MESS["IM_MESSENGER_WRITING"] = "#USER_NAME# está digitando uma mensagem ...";
$MESS["IM_MESSENGER_MENU_USERS"] = "Participantes";
$MESS["IM_MESSENGER_MENU_LEAVE"] = "Deixar o Chat";
$MESS["IM_MESSENGER_ND_RETRY"] = "Tentar Novamente";
$MESS["IM_MESSENGER_READED"] = "Mensagem lida em #DATE#";
$MESS["IM_MESSENGER_MENU_USER"] = "perfil";
$MESS["IM_MESSENGER_MENU_ADD"] = "Adicionar participante";
$MESS["IM_MESSENGER_EXTEND"] = "adicionar chat";
$MESS["IM_MESSENGER_MENU_RELOAD"] = "recarregar";
?>