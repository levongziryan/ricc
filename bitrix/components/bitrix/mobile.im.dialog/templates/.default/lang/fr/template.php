<?
$MESS["IM_VI_CALL"] = "Appel téléphonique";
$MESS["IM_DIALOG_LOADING"] = "Chargement ... Veuillez patienter.";
$MESS["IM_MESSENGER_NOT_DELIVERED"] = "Le message n'a pas été délivré";
$MESS["IM_MESSENGER_DELIVERED"] = "Réception du message";
$MESS["IM_MESSENGER_MESSAGE_EMPTY"] = "Il n'y a pas de messages disponibles";
$MESS["IM_MESSENGER_MESSAGE_PULLTEXT"] = "Tirez pour le chargement de l'historique";
$MESS["IM_MESSENGER_MESSAGE_DOWNTEXT"] = "Relâchez pour charger l'historique";
$MESS["IM_MESSENGER_MESSAGE_LOADTEXT"] = "Chargement de l'historique...";
$MESS["IM_MESSENGER_MESSAGE_NEW"] = "Écrire un message";
$MESS["IM_MESSENGER_MESSAGE_SEND"] = "Envoyer";
$MESS["IM_MESSENGER_FORMAT_DATE"] = "H:i, d F Y";
$MESS["IM_MESSENGER_FORMAT_TIME"] = "H:i";
$MESS["IM_MESSENGER_WRITING"] = "#USER_NAME# écrit un message...";
$MESS["IM_MESSENGER_MENU_USERS"] = "Participants";
$MESS["IM_MESSENGER_MENU_LEAVE"] = "Quitter le chat";
$MESS["IM_MESSENGER_ND_RETRY"] = "Envoyer encore une fois";
$MESS["IM_MESSENGER_READED"] = "Le message a été consulté le #DATE#";
$MESS["IM_MESSENGER_MENU_USER"] = "Profil";
$MESS["IM_MESSENGER_MENU_ADD"] = "Ajouter un participant";
$MESS["IM_MESSENGER_EXTEND"] = "Ajouter";
$MESS["IM_MESSENGER_MENU_RELOAD"] = "Redémarrer";
$MESS["IM_VIDEO_CALL"] = "Vidéo";
$MESS["IM_PHONE_CALL"] = "Téléphoner";
?>