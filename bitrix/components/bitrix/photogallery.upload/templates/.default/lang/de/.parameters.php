<?
$MESS ['P_USE_LIGHT_TEMPLATE'] = "Senkrechte Vorlage zum zahlreichen Hochladen benutzen";
$MESS ['P_WATERMARK_COLORS'] = "Farben für das Copyright Zeichen";
$MESS ['P_WATERMARK'] = "Copyright anzeigen";
$MESS ['P_SHOW_TAGS'] = "Tags anzeigen";
$MESS ['P_COLOR_FF0000'] = "Rot";
$MESS ['P_COLOR_FFA500'] = "Orange";
$MESS ['P_COLOR_FFFF00'] = "Gelb";
$MESS ['P_COLOR_008000'] = "Grün";
$MESS ['P_COLOR_00FFFF'] = "Cyan";
$MESS ['P_COLOR_800080'] = "Violett";
$MESS ['P_COLOR_FFFFFF'] = "Weiß";
$MESS ['P_COLOR_000000'] = "Schwarz";
?>