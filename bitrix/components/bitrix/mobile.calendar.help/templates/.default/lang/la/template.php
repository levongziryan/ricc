<?
$MESS["MB_CALENDAR_HELP_TITLE"] = "Sincronizar sus calendarios!";
$MESS["MB_CALENDAR_HELP_PHRASE_2"] = "Optimice su calendario para la comodidad!";
$MESS["MB_CALENDAR_HELP_HELP_NAME"] = "Ayuda";
$MESS["MB_CALENDAR_HELP_HOW"] = "Sí, mostrar cómo!";
$MESS["MB_CALENDAR_HELP_WEEK_DAY"] = "Tu";
$MESS["MB_CALENDAR_HELP_PHRASE_3"] = "<p>Trabaje en un ambiente familiar: con Bitrix24 puede ver el calendario de eventos en su calendario iOS. </ p> Crear eventos, ya sea en el mismo calendario y que aparezcan en ambos.</p>";
$MESS["MB_CALENDAR_HELP_GO_TO_LIST_BUT"] = "No gracias, sólo tomar el calendario de Bitrix24";
?>