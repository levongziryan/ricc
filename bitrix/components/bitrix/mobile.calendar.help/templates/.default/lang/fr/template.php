<?
$MESS["MB_CALENDAR_HELP_PHRASE_3"] = "<p> Travailler dans l'ambiance familière: vous pourrez voir tous les événements de Bitrix24 dans votre calendrier Apple.</p><p> Créer les événements dans tout calendrier - ils apparaîtront immédiatement dans les deux.</p>";
$MESS["MB_CALENDAR_HELP_WEEK_DAY"] = "Mar";
$MESS["MB_CALENDAR_HELP_HOW"] = "Comment le faire!";
$MESS["MB_CALENDAR_HELP_TITLE"] = "Fusionnez vos calendriers!";
$MESS["MB_CALENDAR_HELP_GO_TO_LIST_BUT"] = "Accéder au calendrier Bitrix24";
$MESS["MB_CALENDAR_HELP_HELP_NAME"] = "Aide";
$MESS["MB_CALENDAR_HELP_PHRASE_2"] = "Faites en sorte que votre calendrier soit encore plus commode!";
?>