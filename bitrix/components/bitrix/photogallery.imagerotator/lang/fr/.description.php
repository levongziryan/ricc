<?
$MESS["IBLOCK_ELEMENT_TEMPLATE_NAME"] = "Slideshow flash";
$MESS["T_IBLOCK_DESC_PHOTO"] = "Galerie photos 2.0";
$MESS["IBLOCK_ELEMENT_TEMPLATE_DESCRIPTION"] = "Affichage cyclique de photos de la galerie de photos.";
$MESS["IMAGEROTATOR_DEPRECATED"] = "(ne pas utiliser)";
$MESS["IMAGEROTATOR_DEPRECATED_2"] = "Ce composant est obsolète. Il ne fonctionne pas comme prévu dans les navigateurs modernes.";
?>