<?
$MESS["WD_ACCESS_DENIED"] = "Acesso negado.";
$MESS["WD_DOCUMENTS"] = "Documentos";
$MESS["WD_ERROR_ELEMENT_NOT_FOUND"] = "Elemento não foi encontrado.";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "Módulo WebDav não está instalado.";
$MESS["WD_EV_TITLE"] = "Versões do Arquivo";
$MESS["WD_VIEW"] = "Propriedades e descrição";
$MESS["WD_ACTION"] = "Ação no Elemento";
$MESS["IBLIST_BP"] = "Processos de Negócios";
?>