<?
$MESS["WD_ACCESS_DENIED"] = "Acesso negado.";
$MESS["WD_ERROR_DELETE"] = "Não é possível excluir.";
$MESS["WD_ERROR_EMPTY_ACTION"] = "Nenhuma ação especificada.";
$MESS["WD_ERROR_EMPTY_DATA"] = "Nenhum arquivo ou pasta selecionada.";
$MESS["WD_ERROR_EMPTY_TARGET_SECTION"] = "A pasta de destino não é especificado.";
$MESS["WD_ERROR_BAD_ACTION"] = "A ação da pasta não é especificado.";
$MESS["WD_ERROR_BAD_SESSID"] = "Sua sessão expirou. Por favor, repita a operação.";
$MESS["SEARCH_COLOR_OLD"] = "Ultima cor de tag (ex. \"FEFEFE\")";
$MESS["SEARCH_FONT_MIN"] = "Menor tamanho de fonte (px)";
$MESS["WD_DELETE_SECTION"] = "Excluir catálogo";
$MESS["IBEL_BIZPROC_TASKS"] = "Processo de tarefas de negócios";
$MESS["WD_ELEMENT_HISTORY_URL"] = "Página de edição de elemento do Histórico";
$MESS["WD_ELEMENT_URL"] = "Página de visualização de elementos (Para busca)";
$MESS["IBLOCK_TYPE_TIP"] = "Escolha um dos tipos de informação existentes bloco na lista e clique em OK <b> </b>. Isso irá carregar blocos de informação do tipo selecionado. No entanto, neste componente este parâmetro é opcional.";
?>