<?
$MESS["WD_EV_TITLE"] = "Versions du document";
$MESS["WD_ERROR_ELEMENT_NOT_FOUND"] = "lément introuvable.";
$MESS["WD_DOCUMENTS"] = "Documents";
$MESS["WD_ACCESS_DENIED"] = "Accès interdit.";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "Le module de la bibliothèque des documents n'a pas été installé.";
?>