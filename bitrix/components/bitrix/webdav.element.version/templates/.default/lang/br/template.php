<?
$MESS["WD_FILE_NAME"] = "Arquivo";
$MESS["WD_CHANGE_DATE"] = "Modificado";
$MESS["WD_MODIFIED_BY"] = "Modificado por";
$MESS["IBLIST_A_BP_H"] = "Business Process";
$MESS["WD_ALL"] = "Total";
$MESS["WD_CREATE_VERSION"] = "Criar versão";
$MESS["WD_CREATE_VERSION_ALT"] = "Criar versão do documento";
$MESS["WD_PREVIEW_ELEMENT"] = "Visualização do documento";
$MESS["WD_FILE"] = "Arquivo";
$MESS["WD_COMMENTS_NAME"] = "Discussão";
$MESS["WD_COMMENTS_NAME_JS"] = "Discussão";
$MESS["WD_FILE_MODIFIED"] = "Modificado";
$MESS["WD_ERROR_BAD_SECTION_NAME"] = "O nome da pasta não deve conter os seguintes caracteres: / \\ \\ <> | \"': * | + #?.";
?>