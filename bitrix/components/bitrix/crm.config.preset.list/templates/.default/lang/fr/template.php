<?
$MESS["CRM_PRESET_TOOLBAR_ADD"] = "Ajouter un modèle";
$MESS["CRM_PRESET_TOOLBAR_ADD_TITLE"] = "Créer un nouveau modèle";
$MESS["CRM_PRESET_TOOLBAR_EDIT"] = "Éditer le modèle";
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_PRESET_LIST_ACTION_MENU_DELETE"] = "Supprimer";
$MESS["CRM_PRESET_LIST_ACTION_MENU_DELETE_CONF"] = "Voulez-vous vraiment supprimer ce modèle ?";
$MESS["CRM_PRESET_LIST_ACTION_MENU_FIELD_LIST"] = "Champs";
$MESS["CRM_PRESET_LIST_ACTION_MENU_EDIT"] = "Éditer";
$MESS["CRM_ADD_PRESET_DIALOG_ERR_EMPTY_NAME"] = "Veuillez saisir le nom du modèle";
$MESS["CRM_ADD_PRESET_DIALOG_ERR_LONG_NAME"] = "Le nom du modèle est trop long.";
$MESS["CRM_ADD_PRESET_DIALOG_CREATE_NEW_TITLE"] = "Créer un nouveau";
$MESS["CRM_ADD_PRESET_DIALOG_FIXED_PRESET_TITLE"] = "Sélectionner un échantillon";
$MESS["CRM_ADD_PRESET_DIALOG_CREATE_SELECTED_TITLE"] = "Sélectionner depuis la liste";
$MESS["CRM_ADD_PRESET_DIALOG_NAME_TITLE"] = "Nom";
$MESS["CRM_ADD_PRESET_DIALOG_ACTIVE_TITLE"] = "Actif";
$MESS["CRM_ADD_PRESET_DIALOG_SORT_TITLE"] = "Trier";
$MESS["CRM_ADD_PRESET_DIALOG_BTN_ADD_TEXT"] = "Ajouter";
$MESS["CRM_ADD_PRESET_DIALOG_BTN_EDIT_TEXT"] = "Appliquer";
$MESS["CRM_ADD_PRESET_DIALOG_BTN_CANCEL_TEXT"] = "Annuler";
$MESS["CRM_PRESET_LIST_ACTION_MENU_SET_DEF_FOR_COMPANY"] = "Rendre principal pour entreprises";
$MESS["CRM_PRESET_LIST_ACTION_MENU_SET_DEF_FOR_CONTACT"] = "Rendre principal pour contacts";
?>