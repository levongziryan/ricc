<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_PRESET_LIST_TITLE_EDIT"] = "Modelos: #NAME#";
$MESS["CRM_PRESET_ENTITY_TYPE_INVALID"] = "Tipo de modelo incorreto";
$MESS["CRM_PRESET_COUNTRY_EMPTY"] = "(não selecionado)";
$MESS["CRM_PRESET_ERR_DELETE"] = "Erro ao excluir o modelo (ID = #ID#)";
$MESS["CRM_PRESET_NOT_SELECTED"] = "(não selecionado)";
$MESS["CRM_PRESET_DEF_FOR_REQUISITE_OF_COMPANY"] = "Primário para empresas";
$MESS["CRM_PRESET_DEF_FOR_REQUISITE_OF_CONTACT"] = "Primário para contatos";
?>