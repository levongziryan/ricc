<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
$MESS["CRM_PRESET_LIST_TITLE_EDIT"] = "Modèles: #NAME#";
$MESS["CRM_PRESET_ENTITY_TYPE_INVALID"] = "Type de modèle incorrect";
$MESS["CRM_PRESET_COUNTRY_EMPTY"] = "(non sélectionné)";
$MESS["CRM_PRESET_ERR_DELETE"] = "Erreur lors de la suppression du modèle (ID = #ID#)";
$MESS["CRM_PRESET_NOT_SELECTED"] = "(non sélectionné)";
$MESS["CRM_PRESET_DEF_FOR_REQUISITE_OF_COMPANY"] = "Principal pour entreprises";
$MESS["CRM_PRESET_DEF_FOR_REQUISITE_OF_CONTACT"] = "Principal pour contacts";
?>