<?
$MESS["FACEID_TRACKER_CMP_TITLE"] = "Observe a las personas que visitan sus ubicaciones de forma fisica.";
$MESS["FACEID_TRACKER_CMP_DESCR"] = "Face Tracker registrará y contará los visitantes en su tienda u oficina: total de visitantes hoy; nuevos visitantes; visitantes de vuelta; visitantes que ya existen en su CRM.";
$MESS["FACEID_TRACKER_CMP_DESCR_P1"] = "Búsqueda instantánea por foto entre personas registradas en su CRM.";
$MESS["FACEID_TRACKER_CMP_DESCR_P1_NEW"] = "Face-tracker utiliza la tecnología de reconocimiento facial para identificar a sus clientes. Puede contar con todos los visitantes de su tienda o su oficina, contar cuántos visitantes son nuevos y cuántos están regresando, y cuántos están ya en su CRM.";
$MESS["FACEID_TRACKER_CMP_DESCR_P2"] = "Bitrix24 encontrará un perfil social por foto y agregará el perfil a su CRM.";
$MESS["FACEID_TRACKER_CMP_DESCR_P2_NEW"] = "Face-tracker puede identificar el perfil social de VK por foto tomada y agregar el perfil a su CRM.";
$MESS["FACEID_TRACKER_CMP_AUTO"] = "Automático";
$MESS["FACEID_TRACKER_CMP_AUTO_PHOTO"] = "tomar una foto";
$MESS["FACEID_TRACKER_CMP_AUTO_LEAD"] = "crear un prospecto";
$MESS["FACEID_TRACKER_CMP_CAMERA"] = "Cámara de video";
$MESS["FACEID_TRACKER_CMP_STATS"] = "Estadísticas de visitantes";
$MESS["FACEID_TRACKER_CMP_STATS_NEW"] = "Nuevo";
$MESS["FACEID_TRACKER_CMP_STATS_OLD"] = "Regresando";
$MESS["FACEID_TRACKER_CMP_STATS_ALL"] = "Total";
$MESS["FACEID_TRACKER_CMP_STATS_CRM"] = "Estadísticas del CRM";
$MESS["FACEID_TRACKER_CMP_STATS_CRM_SAVE"] = "Guardado en el CRM";
$MESS["FACEID_TRACKER_CMP_STATS_DETAILED"] = "Detalles de las estadísticas";
$MESS["FACEID_TRACKER_CMP_HEAD_VISITORS"] = "Visitantes";
$MESS["FACEID_TRACKER_CMP_HEAD_CREDITS"] = "Crédito de reconocimiento";
$MESS["FACEID_TRACKER_CMP_HEAD_CREDITS_ADD"] = "Agregar credito";
$MESS["FACEID_TRACKER_CMP_FIRST_TIME_VISIT"] = "Tome una foto de su cliente para agregarla al sistema. Bitrix24 reconocerá al cliente en el próximo encuentro por lo que sabrá que es un cliente que regresa. <br><br> Instale una cámara en su tienda u oficina para registrar a sus visitantes y mantener estadísticas.";
$MESS["FACEID_TRACKER_CMP_FIRST_TIME_VISIT_NEW"] = "<div class=\"faceid-tracker-first-visit-desc\">
	<div class=\"faceid-tracker-first-visit-desc-inner\">
		<div class=\"faceid-tracker-first-visit-desc-title\">¿Como empiezo?</div>
		<div class=\"faceid-tracker-first-visit-desc-block\">
			<span class=\"faceid-tracker-first-visit-desc-item\">Tome una primera foto de su cliente con face-tracker
'recordarme' en. La próxima vez que esta persona regrese, el sistema lo identificará como un visitante repetido. </span>
		</div>
	</div>
        <div class=\"faceid-tracker-first-visit-desc-inner\">
		<div class=\"faceid-tracker-first-visit-desc-title\">Cómo utilizar el rastreador de rostro como contador de tráfico minorista</div>
		<div class=\"faceid-tracker-first-visit-desc-block\">
			<span class=\"faceid-tracker-first-visit-desc-item\">Instale la cámara cerca de la entrada de su oficina o tienda</span>
			<span class=\"faceid-tracker-first-visit-desc-item\">Conecte la cámara a cualquier computadora</span>
			<span class=\"faceid-tracker-first-visit-desc-item\">Conecte la cámara a cualquier computadora</span>
			<span class=\"faceid-tracker-first-visit-desc-item\">Habilitar la toma automática de imágenes</span>
		</div>
	</div>
</div>
";
$MESS["FACEID_TRACKER_CMP_LIST_MORE"] = "Mostrar más";
$MESS["FACEID_TRACKER_CMP_VK_TITLE"] = "Buscar en el perfil de VK";
$MESS["FACEID_TRACKER_CMP_VK_PROGRESS"] = "Buscando...";
$MESS["FACEID_TRACKER_CMP_VK_FOUND_COUNT"] = "Personas encontradas:";
$MESS["FACEID_TRACKER_CMP_AGR_TITLE"] = "Términos de Uso";
$MESS["FACEID_TRACKER_CMP_AGR_TEXT"] = "<div class=\"tracker-agreement-popup-content\">
   <ol class=\"tracker-agreement-popup-list\">
    <li class=\"tracker-agreement-popup-list-item\">
     El Servicio FindFace (el Servicio) es proporcionado por N-TECH.LAB LTD de acuerdo con lo siguiente <a href=\"https://saas.findface.pro/files/docs/license_us.pdf\" class=\"tracker-link\">Agreement</a>. Al pulsar \"Acepto los Términos\", acepta y garantiza que cumplirá los siguientes requisitos:
     <ul class=\"tracker-agreement-popup-inner-list\">
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Seguir estrictamente todas las leyes y reglamentos locales que rigen la privacidad y el uso de datos personales en su país durante toda la duración del uso del Servicio.
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Para obtener el permiso necesario (escrito o no, de acuerdo con las leyes locales de cada país en el que esté operando) de cada persona cuyos datos personales procesará usando el Servicio, incluyendo sus fotografías e imágenes. 
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
      Buscar asesoramiento jurídico ANTES de empezar a utilizar el Servicio, si no está seguro, qué permisos necesita obtener y de qué forma.
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Para garantizar que cualquier información que suba al Servicio ha sido obtenida por usted en plena conformidad con las leyes y reglamentos locales
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Para confirmar que está utilizando el Servicio a su propio riesgo sin garantía de ningún tipo.
      </li>
     </ul>
    </li>
    <li class=\"tracker-agreement-popup-list-item\">
     Bitrix, Inc, no es responsable del Servicio y no hace reclamaciones o garantías con respecto a la exactitud del Servicio o la disponibilidad, ni proporciona ningún soporte técnico o consultas sobre el Servicio.
    </li>
   </ol>
   <div class=\"tracker-agreement-popup-description\">
    Al aceptar el contrato de Servicio, usted acepta y confirma que Bitrix, Inc. no recopila ni procesa ningún dato cargado por usted al Servicio y que Bitrix, Inc. no conoce y no debe conocer las circunstancias bajo las cuales estos datos han sido recopilados .
   </div>
  </div>";
$MESS["FACEID_TRACKER_CMP_AGR_BUTTON"] = "Acepto los términos";
$MESS["FACEID_TRACKER_CMP_JS_CAMERA_DEFAULT"] = "Cámara";
$MESS["FACEID_TRACKER_CMP_JS_CAMERA_NOT_FOUND"] = "La cámara no fue encontrada";
$MESS["FACEID_TRACKER_CMP_JS_CAMERA_NO_SUPPORT"] = "Su navegador no admite cámara";
$MESS["FACEID_TRACKER_CMP_JS_CAMERA_ERROR"] = "Su navegador no admite grabación de video o la cámara no está conectada.";
$MESS["FACEID_TRACKER_CMP_JS_FACE_NOT_FOUND"] = "Lamentablemente no pudimos reconocer a la persona. Intente tomar otra foto.";
$MESS["FACEID_TRACKER_CMP_JS_FACE_ORIGINAL"] = "Ejemplo de foto";
$MESS["FACEID_TRACKER_CMP_JS_SAVE_CRM"] = "Guardar en el CRM";
$MESS["FACEID_TRACKER_CMP_JS_SAVE_CRM_DONE"] = "El prospecto ha sido guardado";
$MESS["FACEID_TRACKER_CMP_JS_VK_LINK"] = "VK";
$MESS["FACEID_TRACKER_CMP_JS_VK_LINK_ACTION"] = "Encuentra el perfil de VK";
$MESS["FACEID_TRACKER_CMP_JS_VK_FOUND_PEOPLE"] = "personas";
$MESS["FACEID_TRACKER_CMP_JS_VK_SELECT"] = "Seleccionar";
$MESS["FACEID_TRACKER_CMP_AUTH_ONLY"] = "Este componente requiere autenticación.";
$MESS["FACEID_TRACKER_CMP_AUTO_PHOTO_NEW"] = "toma fotos automáticamente";
$MESS["FACEID_TRACKER_CMP_SETTINGS"] = "Parámetros extendidos";
?>