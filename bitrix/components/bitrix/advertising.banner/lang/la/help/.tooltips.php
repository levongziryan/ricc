<?
$MESS["TYPE_TIP"] = "Tipo de Banner";
$MESS["CACHE_TYPE_TIP"] = "Tipo de caché";
$MESS["CACHE_TIME_TIP"] = "Tiempo de caché (sec.)";
$MESS["KEYBOARD_TIP"] = "Utilice las flecha de las teclas para controlar los slides";
$MESS["BS_KEYBOARD_TIP"] = "Utilice las flecha de las teclas para controlar los slides";
$MESS["BS_EFFECT_TIP"] = "Slides con imágenes fijas pueden representar de forma incorrecta al utilizar el efecto de slide.";
$MESS["JQUERY_TIP"] = "Este slider requiere jQuery.";
$MESS["BS_HIDE_FOR_TABLETS_TIP"] = "Compatible sólo con estilos Bootstrap";
$MESS["BS_HIDE_FOR_PHONES_TIP"] = "Compatible sólo con estilos Bootstrap";
$MESS["QUANTITY_TIP"] = "Numero máximo de banners que la plantilla del componente multipagina puede mostrar";
$MESS["DEFAULT_TEMPLATE_TIP"] = "La plantilla seleccionada será utilizada únicamente con los banners de la imagen";
?>