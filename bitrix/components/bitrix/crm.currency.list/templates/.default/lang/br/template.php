<?
$MESS["CRM_CURRENCY_SHOW_TITLE"] = "Visualizar esta moeda";
$MESS["CRM_CURRENCY_SHOW"] = "Visualizar Moeda";
$MESS["CRM_CURRENCY_EDIT_TITLE"] = "Editar esta moeda";
$MESS["CRM_CURRENCY_EDIT"] = "Editar moeda";
$MESS["CRM_CURRENCY_DELETE_TITLE"] = "Excluir esta moeda";
$MESS["CRM_CURRENCY_DELETE"] = "Excluuir moeda";
$MESS["CRM_CURRENCY_DELETE_CONFIRM"] = "Tem certeza de que deseja excluir os '%'?";
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_CURRENCY_SET_AS_BASE_TITLE"] = "Fazer moeda base";
$MESS["CRM_CURRENCY_SET_AS_BASE"] = "Fazer moeda base";
?>