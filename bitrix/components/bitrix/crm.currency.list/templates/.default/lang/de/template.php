<?
$MESS["CRM_CURRENCY_SHOW_TITLE"] = "Diese Währung anzeigen";
$MESS["CRM_CURRENCY_SHOW"] = "Währung anzeigen";
$MESS["CRM_CURRENCY_EDIT_TITLE"] = "Diese Währung bearbeiten";
$MESS["CRM_CURRENCY_EDIT"] = "Währung bearbeiten";
$MESS["CRM_CURRENCY_DELETE_TITLE"] = "Diese Währung löschen";
$MESS["CRM_CURRENCY_DELETE"] = "Währung löschen";
$MESS["CRM_CURRENCY_DELETE_CONFIRM"] = "Möchten Sie '%s' wirklich löschen?";
$MESS["CRM_ALL"] = "Gesamt";
$MESS["CRM_CURRENCY_SET_AS_BASE_TITLE"] = "Als Basiswährung festlegen";
$MESS["CRM_CURRENCY_SET_AS_BASE"] = "Als Basiswährung festlegen";
?>