<?
$MESS["IMOL_ROLE_LABEL"] = "Rolle";
$MESS["IMOL_ROLE_SAVE"] = "Speichern";
$MESS["IMOL_ROLE_CANCEL"] = "Abbrechen";
$MESS["IMOL_ROLE_ENTITY"] = "Einheit";
$MESS["IMOL_ROLE_ACTION"] = "Aktion";
$MESS["IMOL_ROLE_PERMISSION"] = "Zugriffsrecht";
$MESS["IMOL_ROLE_POPUP_LIMITED_TITLE"] = "Erweiterte Kommunikationskanäle";
$MESS["IMOL_ROLE_POPUP_LIMITED_VOTE_HEAD"] = "Vorgesetzte können Konversationen in folgenden Tarifen bewerten: Plus, Standard, Professional.<br><br> Die Bewertung durch Vorgesetzte ermöglicht es, das Feedback jedem einzelnen Mitarbeiter zu vermitteln und so die Arbeitsfähigkeit zu erhöhen.";
$MESS["IMOL_ROLE_LOCK_ALT"] = "Es gibt Einschränkungen. Klicken Sie hier, um mehr zu erfahren.";
?>