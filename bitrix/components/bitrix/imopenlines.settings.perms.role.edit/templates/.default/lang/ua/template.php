<?
$MESS["IMOL_ROLE_LABEL"] = "Роль";
$MESS["IMOL_ROLE_SAVE"] = "Зберегти";
$MESS["IMOL_ROLE_CANCEL"] = "Скасування";
$MESS["IMOL_ROLE_ENTITY"] = "Сутність";
$MESS["IMOL_ROLE_ACTION"] = "Дія";
$MESS["IMOL_ROLE_PERMISSION"] = "Право";
$MESS["IMOL_ROLE_POPUP_LIMITED_TITLE"] = "Розширені відкриті лінії";
$MESS["IMOL_ROLE_POPUP_LIMITED_VOTE_HEAD"] = "Оцінка діалогу керівником доступна в тарифах: \"Команда\" і \"Компанія\".<br><br>Оцінка діалогу керівником дозволяє отримати співробітникові зворотний зв'язок щодо ефективності і якості його роботи.";
$MESS["IMOL_ROLE_LOCK_ALT"] = "Є обмеження, клікніть щоб дізнатися більше.";
?>