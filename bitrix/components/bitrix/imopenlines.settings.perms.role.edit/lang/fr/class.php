<?
$MESS["IMOL_ROLE_NOT_FOUND"] = "Rôle introuvable. Remplissez le formulaire pour créer un nouveau rôle.";
$MESS["IMOL_ROLE_SAVE_ERROR"] = "Erreur d'enregistrement du rôle.";
$MESS["IMOL_ROLE_ERROR_EMPTY_NAME"] = "Le nom du rôle n'est pas spécifié";
$MESS["IMOL_ROLE_ERROR_INSUFFICIENT_RIGHTS"] = "Autorisations d'accès insuffisantes";
$MESS["IMOL_ROLE_LICENSE_ERROR"] = "Erreur d'enregistrement du rôle.";
?>