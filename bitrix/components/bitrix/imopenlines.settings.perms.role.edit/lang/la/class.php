<?
$MESS["IMOL_ROLE_NOT_FOUND"] = "El rol no fue encontrado. Complete el formulario para crear un nuevo rol.";
$MESS["IMOL_ROLE_SAVE_ERROR"] = "Error al guardar el rol.";
$MESS["IMOL_ROLE_ERROR_EMPTY_NAME"] = "El nombre del rol no se ha especificado";
$MESS["IMOL_ROLE_ERROR_INSUFFICIENT_RIGHTS"] = "Permisos de acceso insuficientes";
$MESS["IMOL_ROLE_LICENSE_ERROR"] = "Error al guardar el rol.";
?>