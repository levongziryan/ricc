<?
$MESS["LEARNING_COURSES_CHAPTER_DELETE_CONF"] = "Toutes les informations relatives à cette écriture seront effacées! Continuer?";
$MESS["LEARNING_COURSES_CHAPTER_ADD"] = "Ajouter un nouveau chapitre";
$MESS["LEARNING_COURSES_LESSON_ADD"] = "Ajouter une leçon";
$MESS["LEARNING_COURSES_CHAPTER_EDIT"] = "Modifier le chapitre";
$MESS["LEARNING_COURSE_DENIED"] = "Le cours ne peut pas être trouvé ou l'accès à lui est refusé.";
$MESS["LEARNING_COURSES_CHAPTER_DELETE"] = "Supprimer le Chapitre";
$MESS["LEARNING_CHAPTER_DENIED"] = "Chapitre introuvable ou accès refusé.";
$MESS["LEARNING_MODULE_NOT_FOUND"] = "Module des blogs non installé.";
?>