<?
$MESS["LEARNING_PATH_TO_USER_PROFILE"] = "Chemin vers la page de l'utilisateur";
$MESS["LEARNING_DESC_YES"] = "Oui";
$MESS["LEARNING_CHAPTER_ID_NAME"] = "ID du chapitre";
$MESS["LEARNING_COURSE_ID"] = "Identificateur du cours";
$MESS["LEARNING_DESC_NO"] = "Non";
$MESS["LEARNING_CHECK_PERMISSIONS"] = "Vérifier le droit d'accès";
$MESS["LEARNING_CHAPTER_DETAIL_TEMPLATE_NAME"] = "Page Chapitre URL";
$MESS["LEARNING_LESSON_DETAIL_TEMPLATE_NAME"] = "Page Leçon URL";
?>