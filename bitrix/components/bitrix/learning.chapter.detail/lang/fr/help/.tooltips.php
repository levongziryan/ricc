<?
$MESS["CACHE_TYPE_TIP"] = "<i>Auto</i>: cache est valide pendant un temps prédéterminé dans les paramètres;<br /><i>Mettre en cache</i>: pour la mise en cache il faut déterminer seulement le temps de la mise en cache;<br /><i>Ne pas mettre en cache</i>: pas de mise en cache en tout cas.";
$MESS["COURSE_ID_TIP"] = "Sélectionnez ici l'un des cours existants. Si vous sélectionnez <b><i>(autre)</i></b>, vous devrez spécifier l'ID de cours dans le domaine de côté.";
$MESS["CACHE_TIME_TIP"] = "Veuillez indiquer la période de temps durant laquelle la mémoire cache reste valide.";
$MESS["CHAPTER_ID_TIP"] = "Le champ contient le code qui donne l'ID du chapitre.";
$MESS["CHECK_PERMISSIONS_TIP"] = "Au cas où la valeur 'Oui' soit choisie, alors le droit d'accès sera vérifié.";
$MESS["SET_TITLE_TIP"] = "Avec une option marquée, au titre de la page sera mis le nom du chapitre.";
$MESS["CHAPTER_DETAIL_TEMPLATE_TIP"] = "Chemin vers la page avec le chapitre du cours.";
$MESS["LESSON_DETAIL_TEMPLATE_TIP"] = "Chemin vers la page de la leçon du cours.";
?>