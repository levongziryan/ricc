<?
$MESS["F_IN"] = "Dans";
$MESS["FM_LEAVE_LINK"] = "Gardez liens vers les rubriques déménagé dans l'ancien forum";
$MESS["FL_TITLE"] = "Transférer les sujets";
$MESS["FM_MOVE_TOPIC"] = "Transférer les sujets choisis";
$MESS["F_EMPTY_TOPIC_LIST"] = "Les thèmes choisis pour déplacement n'ont pas été trouvés ou se rapportent à un autre forum.";
?>