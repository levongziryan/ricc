<?
$MESS["F_INDEX_TEMPLATE"] = "Página de lista de foros";
$MESS["F_READ_TEMPLATE"] = "Página de tema leído";
$MESS["F_LIST_TEMPLATE"] = "Página de lista de temas";
$MESS["F_MOVE_TEMPLATE"] = "Página de temas movibles";
$MESS["F_PROFILE_VIEW_TEMPLATE"] = "Página del perfil";
$MESS["F_SET_NAVIGATION"] = "Muestra Breadcrumb de navegación";
$MESS["F_DEFAULT_FID"] = "ID del foro";
$MESS["F_DEFAULT_TID"] = "ID del tema";
$MESS["F_MESSAGE_TEMPLATE"] = "Página de mensajes vistos";
?>