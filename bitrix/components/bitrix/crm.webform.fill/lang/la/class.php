<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado";
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "El módulo Business Processes no está instalado";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "El módulo Currency no está instalado";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "El módulo e-Store no está instalado";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "El módulo Commercial Catalog no está instalado";
$MESS["CRM_MODULE_NOT_INSTALLED_REST"] = "El módulo Rest no está instalado";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_WEBFORM_ERROR_NOT_FOUND"] = "El formulario no se ha encontrado.";
$MESS["CRM_WEBFORM_ERROR_SECURITY"] = "La dirección URL del formulario posiblemente ha cambiado.";
$MESS["CRM_WEBFORM_ERROR_DEACTIVATED"] = "El formulario se encuentra inactivo.";
?>