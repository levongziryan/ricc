<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "O módulo Processos de Negócio não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "O módulo Moeda não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "O módulo e-Store não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "O módulo Catálogo Comercial não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_REST"] = "O módulo Restante não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_WEBFORM_ERROR_NOT_FOUND"] = "O formulário não foi encontrado.";
$MESS["CRM_WEBFORM_ERROR_SECURITY"] = "O formulário URL foi possivelmente alterado.";
$MESS["CRM_WEBFORM_ERROR_DEACTIVATED"] = "O formulário está inativo.";
?>