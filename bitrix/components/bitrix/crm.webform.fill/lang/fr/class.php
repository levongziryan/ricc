<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "Le module Processus opérationnels n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "Le module Devise n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Le module e-Store n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "Le module Catalogue commercial n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_REST"] = "Le module Rest n'est pas installé";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
$MESS["CRM_WEBFORM_ERROR_NOT_FOUND"] = "Le formulaire est introuvable.";
$MESS["CRM_WEBFORM_ERROR_SECURITY"] = "L'URL du formulaire a peut-être changé.";
$MESS["CRM_WEBFORM_ERROR_DEACTIVATED"] = "Le formulaire est inactif.";
?>