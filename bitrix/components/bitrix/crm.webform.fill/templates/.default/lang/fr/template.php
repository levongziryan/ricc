<?
$MESS["CRM_WEBFORM_FILL_ERROR_FIELD_EMPTY"] = "Veuillez compléter tous les champs obligatoires.";
$MESS["CRM_WEBFORM_FILL_ERROR_TITLE"] = "Attention !";
$MESS["CRM_WEBFORM_FILL_NOT_SELECTED"] = "Pas sélectionné";
$MESS["CRM_WEBFORM_FILL_FILE_SELECT"] = "Sélectionner";
$MESS["CRM_WEBFORM_FILL_FILE_NOT_SELECTED"] = "Aucun fichier sélectionné";
$MESS["CRM_WEBFORM_FILL_FIELD_ADD_OTHER"] = "Ajouter plus de champs";
$MESS["CRM_WEBFORM_FILL_PRODUCT_TITLE"] = "Produits sélectionnés";
$MESS["CRM_WEBFORM_FILL_PRODUCT_SUMMARY"] = "Total";
$MESS["CRM_WEBFORM_FILL_COPYRIGHT_CHARGED_BY"] = "Fourni par";
$MESS["CRM_WEBFORM_FILL_FILL_AGAIN"] = "Remplir à nouveau le formulaire";
$MESS["CRM_WEBFORM_FILL_LICENCE_PROMPT"] = "Conditions d'utilisation de la licence:";
$MESS["CRM_WEBFORM_FILL_LICENCE_ACCEPT"] = "J'accepte";
$MESS["CRM_WEBFORM_FILL_LICENCE_DECLINE"] = "Je refuse";
$MESS["CRM_WEBFORM_FILL_REDIRECT_DESC"] = "Vous serez redirigé dans";
$MESS["CRM_WEBFORM_FILL_REDIRECT_SECONDS"] = "sec";
$MESS["CRM_WEBFORM_FILL_REDIRECT_GO_NOW"] = "Ouvrir maintenant";
$MESS["CRM_WEBFORM_FILL_RESULT_SENT"] = "Le formulaire a bien été rempli.";
$MESS["CRM_WEBFORM_FILL_RESULT_ERROR"] = "Impossible d'envoyer le formulaire.";
$MESS["CRM_WEBFORM_FILL_BUTTON_DEFAULT"] = "Envoyer";
$MESS["CRM_WEBFORM_FILL_CALLBACK_FREE"] = "Gratuit";
$MESS["CRM_WEBFORM_FILL_COPYRIGHT_BITRIX"] = "Bitrix";
$MESS["CRM_WEBFORM_FILL_LICENCE_PROMPT1"] = "Termes du contrat :";
?>