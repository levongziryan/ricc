<?
$MESS["TASK_LIST_TASK_NOT_INSTALLED"] = "Le module 'Tâches' n'a pas été installé.";
$MESS["TASK_LIST_SOCNET_NOT_INSTALLED"] = "Le module Réseau social n'est pas installé.";
$MESS["TASK_LIST_NOT_AVAILABLE_IN_THIS_EDITION"] = "Le module Tâches n'est pas disponible dans cette version du produit.";
$MESS["TASK_LIST_ACCESS_DENIED"] = "Vous ne pouvez pas voir la liste des tâches car l’accès a été refusé.";
$MESS["TASK_LIST_ACCESS_TO_GROUP_DENIED"] = "Vous ne pouvez pas voir la liste des tâches pour ce groupe car l’accès a été refusé.";
$MESS["TASK_LIST_USER_NOT_FOUND"] = "Utilisateur introuvable.";
$MESS["TASK_LIST_GROUP_NOT_FOUND"] = "Le groupe de travail n'a pas été trouvé.";
$MESS["TASK_LIST_TASK_ACTION_DENIED"] = "Impossible d'exécuter cette commande car l’accès a été refusé.";
$MESS["TASK_LIST_TASK_CREATE_DENIED"] = "Vous ne pouvez pas créer de tâches car l’accès a été refusé.";
$MESS["TASK_LIST_TITLE"] = "Tâches";
$MESS["TASK_LIST_TITLE_MY"] = "Mes tâches";
$MESS["TASK_LIST_TITLE_GROUP"] = "Tâches du groupe de travail";
$MESS["TASK_LIST_COLUMN_TITLE_EMPTY"] = "Le nom de l'étape ne peut pas être vide.";
$MESS["TASK_LIST_UNKNOWN_ACTION"] = "Action inconnue.";
$MESS["TASK_LIST_COLUMN_NOT_EMPTY"] = "Des tâches se trouvent dans cette étape. Déplacez-les avant de supprimer l'étape.";
$MESS["TASK_LIST_SESS_EXPIRED"] = "Votre session a expiré.";
$MESS["TASK_LIST_ERROR_CHANGE_DEADLINE"] = "La date limite de la tâche ne peut pas être modifiée.";
$MESS["TASK_ACCESS_NOTIFY_MESSAGE"] = "Je vous prie de bien vouloir <a href=\"#URL#\">configurer les étapes de la tâche du projet kanban</a> pour moi, ou de me donner l'autorisation requise afin de pouvoir le faire moi-même.";
?>