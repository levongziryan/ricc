<?
$MESS["KANBAN_SORT_TITLE"] = "Nouvelles tâches :";
$MESS["KANBAN_SORT_DESC"] = "Top";
$MESS["KANBAN_SORT_ASC"] = "Au-dessous";
$MESS["KANBAN_WO_GROUP_1"] = "Cette zone contiendra le kanban des tâches de votre projet";
$MESS["KANBAN_WO_GROUP_2"] = "Créez votre premier projet pour commencer à utiliser les tâches";
$MESS["KANBAN_WO_GROUP_BUTTON"] = "Créer un projet";
$MESS["KANBAN_POPUP_K_TITLE"] = "Kanban";
$MESS["KANBAN_POPUP_K_TEXT_1"] = "Cette affichage présente les tâches de votre projet (groupe de travail) sous forme de tableau Kanban. Ce mode de gestion des tâches présente toutes les étapes allant de \"En attente\" à \"Terminé\".";
$MESS["KANBAN_POPUP_K_TEXT_2"] = "Configurer des étapes personnalisées pour chaque projet (cette option est disponible pour le propriétaire d’un groupe de travail et les modérateurs).";
$MESS["KANBAN_POPUP_K_TEXT_3"] = "Déplacez des tâches à travers les étapes et regardez la progression de votre projet !";
$MESS["KANBAN_POPUP_K_TEXT_4"] = "Le groupe sélectionné est celui dans lequel vous avez récemment travaillé.";
$MESS["KANBAN_POPUP_P_TITLE"] = "Planificateur";
$MESS["KANBAN_POPUP_P_TEXT_1"] = "Le planificateur est votre tableau kaban personnel. Les tâches sont groupées par étape, et vous contrôlez totalement les étapes possibles.";
$MESS["KANBAN_POPUP_P_TEXT_2"] = "Déplacez des tâches à travers les étapes sans changer leurs dates limites ou sans envoyer des notifications redondantes.";
$MESS["KANBAN_POPUP_P_TEXT_3"] = "Visualisez vos tâches et gardez un oeil sur les dates limites !";
$MESS["KANBAN_POPUP_DETAIL"] = "Détails";
$MESS["KANBAN_POPUP_CLOSE"] = "Fermer";
$MESS["KANBAN_TEMPLATE_LIST"] = "Tous les modèles";
$MESS["KANBAN_ITEM_TITLE_PLACEHOLDER"] = "Intitulé #tag";
$MESS["KANBAN_COLUMN_TITLE_PLACEHOLDER"] = "Nom";
?>