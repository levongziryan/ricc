<?
$MESS["KANBAN_SORT_TITLE"] = "Novas tarefas:";
$MESS["KANBAN_SORT_DESC"] = "Topo";
$MESS["KANBAN_SORT_ASC"] = "Inferior";
$MESS["KANBAN_WO_GROUP_1"] = "Esta área irá conter seu kanban de tarefa de projeto";
$MESS["KANBAN_WO_GROUP_2"] = "Crie seu primeiro projeto para começar a utilizar tarefas";
$MESS["KANBAN_WO_GROUP_BUTTON"] = "Criar Projeto";
$MESS["KANBAN_POPUP_K_TITLE"] = "Kanban";
$MESS["KANBAN_POPUP_K_TEXT_1"] = "Esta visualização mostra suas tarefas de projeto (grupo de trabalho) como um quadro Kanban. Este modo de gerenciamento de tarefa mostra todas as etapas de \"Pendente\" até \"Concluída\".";
$MESS["KANBAN_POPUP_K_TEXT_2"] = "Configure etapas personalizadas para cada projeto (esta opção está disponível para o proprietário do grupo de trabalho e moderadores).";
$MESS["KANBAN_POPUP_K_TEXT_3"] = "Mova tarefas entre as etapas e veja o progresso do seu projeto!";
$MESS["KANBAN_POPUP_K_TEXT_4"] = "O grupo selecionado é o que você trabalhou mais recentemente.";
$MESS["KANBAN_POPUP_P_TITLE"] = "Planejador";
$MESS["KANBAN_POPUP_P_TEXT_1"] = "O planejador é o seu quadro kanban pessoal. As tarefas são agrupadas por etapa, e você tem controle total das etapas possíveis.";
$MESS["KANBAN_POPUP_P_TEXT_2"] = "Mova tarefas entre as etapas sem alterar seus prazos ou enviar notificações redundantes.";
$MESS["KANBAN_POPUP_P_TEXT_3"] = "Visualize suas tarefas e mantenha os prazos verificados!";
$MESS["KANBAN_POPUP_DETAIL"] = "Informações";
$MESS["KANBAN_POPUP_CLOSE"] = "Fechar";
$MESS["KANBAN_TEMPLATE_LIST"] = "Todos os modelos";
$MESS["KANBAN_ITEM_TITLE_PLACEHOLDER"] = "Nome #tag";
$MESS["KANBAN_COLUMN_TITLE_PLACEHOLDER"] = "Nome";
?>