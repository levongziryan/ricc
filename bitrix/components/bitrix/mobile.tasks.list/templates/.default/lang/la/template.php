<?
$MESS["MB_TASKS_TASKS_LIST_PULLDOWN_PULL"] = "Deslizar hacia abajo para actualizar";
$MESS["MB_TASKS_TASKS_LIST_PULLDOWN_DOWN"] = "Suelte para actualizar";
$MESS["MB_TASKS_TASKS_LIST_PULLDOWN_LOADING"] = "Actualización de datos...";
$MESS["MB_TASKS_TASKS_LIST_NO_TASKS"] = "No hay tareas";
$MESS["MB_TASKS_TASKS_LIST_MENU_CREATE_NEW_TASK"] = "Nueva tarea";
$MESS["MB_TASKS_TASKS_LIST_MENU_GOTO_FILTER"] = "Configurar filtro";
$MESS["MB_TASKS_GENERAL_TITLE"] = "Tareas";
?>