<?
$MESS["TASKS_MODULE_NOT_AVAILABLE_IN_THIS_EDITION"] = "Das Modul Aufgaben ist in dieser Produktedition nicht verfügbar.";
$MESS["MB_TASKS_TASKS_LIST_PSEUDO_GROUP_NAME"] = "Aufgabe ohne Gruppe";
$MESS["MB_TASKS_TASKS_LIST_STATUS_NEW"] = "Neu";
$MESS["MB_TASKS_TASKS_LIST_STATUS_ACCEPTED"] = "Angenommene";
$MESS["MB_TASKS_TASKS_LIST_STATUS_IN_PROGRESS"] = "In Arbeit";
$MESS["MB_TASKS_TASKS_LIST_STATUS_WAITING"] = "Muss kontrolliert werden";
$MESS["MB_TASKS_TASKS_LIST_STATUS_COMPLETED"] = "Abgeschlossene";
$MESS["MB_TASKS_TASKS_LIST_STATUS_DELAYED"] = "Verschobene";
$MESS["MB_TASKS_TASKS_LIST_STATUS_DECLINED"] = "Abgelehnte";
$MESS["MB_TASKS_TASKS_LIST_STATUS_OVERDUE"] = "Überfällige";
$MESS["MB_TASKS_TASKS_LIST_STATUS_DATE_PREPOSITION"] = "seit";
?>