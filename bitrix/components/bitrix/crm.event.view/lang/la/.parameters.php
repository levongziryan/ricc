<?
$MESS["CRM_ENTITY_TYPE"] = "Tipo de evento";
$MESS["CRM_ENTITY_ID"] = "ID de la entidad";
$MESS["CRM_EVENT_COUNT"] = "Eventos Por Página";
$MESS["CRM_EVENT_ENTITY_LINK"] = "Mostrar Título de la entidad";
$MESS["CRM_ENTITY_TYPE_LEAD"] = "Prospecto";
$MESS["CRM_ENTITY_TYPE_CONTACT"] = "Contactos";
$MESS["CRM_ENTITY_TYPE_COMPANY"] = "Compañía";
$MESS["CRM_ENTITY_TYPE_DEAL"] = "Negociación";
$MESS["CRM_NAME_TEMPLATE"] = "Formato de nombre";
$MESS["CRM_ENTITY_TYPE_QUOTE"] = "Cotización";
?>