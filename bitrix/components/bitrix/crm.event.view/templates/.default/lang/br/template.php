<?
$MESS["CRM_EVENT_TABLE_EMPTY"] = "- Não há dados -";
$MESS["CRM_EVENT_TABLE_FILES"] = "Arquivos";
$MESS["CRM_EVENT_ENTITY_TYPE_DEAL"] = "Negócio";
$MESS["CRM_EVENT_ENTITY_TYPE_QUOTE"] = "Orçamento";
$MESS["CRM_EVENT_ENTITY_TYPE_COMPANY"] = "Empresa";
$MESS["CRM_EVENT_ENTITY_TYPE_CONTACT"] = "Contato";
$MESS["CRM_EVENT_ENTITY_TYPE_LEAD"] = "Lead";
$MESS["CRM_EVENT_DELETE"] = "Excluir";
$MESS["CRM_EVENT_DELETE_TITLE"] = "Excluir evento";
$MESS["CRM_EVENT_DELETE_CONFIRM"] = "Tem certeza de que deseja excluí-lo?";
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_IMPORT_EVENT"] = "Se o contato de e-mail está especificado no registro de CRM você pode salvar automaticamente toda a correspondência de e-mail relacionada como um registro de Evento. Você precisa encaminhar a mensagem recebida para o sistema de endereços de e-mail <b>%EMAIL%</b> e todos os arquivos de texto e anexos serão adicionados como Eventos para este Contato.";
$MESS["CRM_EVENT_VIEW_ADD_SHORT"] = "Evento";
$MESS["CRM_EVENT_VIEW_ADD"] = "Adicionar Eventos";
$MESS["CRM_EVENT_VIEW_SHOW_FILTER_SHORT"] = "Filtro";
$MESS["CRM_EVENT_VIEW_SHOW_FILTER"] = "Mostrar/ocultar filtro";
$MESS["CRM_SHOW_ROW_COUNT"] = "Mostrar quantidade";
?>