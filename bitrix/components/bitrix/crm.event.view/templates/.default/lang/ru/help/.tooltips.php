<?php

$MESS['PATH_TO_LEAD_SHOW_TIP'] = "Указывается путь к странице просмотра лида. Например: lead_show.php?lead_id=#lead_id#.";
$MESS['PATH_TO_LEAD_EDIT_TIP'] = "Указывается путь к странице редактирования лида. Например: lead_edit.php?lead_id=#lead_id#.";
$MESS['PATH_TO_LEAD_CONVERT_TIP'] = "Указывается путь к странице конвертации лида. Например: lead_convert.php?lead_id=#lead_id#.";
$MESS['ELEMENT_ID_TIP'] = "Поле содержит код, в котором передается идентификатор сделки.";
$MESS['PATH_TO_CONTACT_SHOW_TIP'] = "Указывается путь к странице просмотра контакта. Например: contact_show.php?contact_id=#contact_id#.";
$MESS['PATH_TO_CONTACT_EDIT_TIP'] = "Указывается путь к странице редактирования контакта. Например: contact_edit.php?contact_id=#contact_id#.";
$MESS['PATH_TO_COMPANY_SHOW_TIP'] = "Указывается путь к странице просмотра компании. Например: company_show.php?company_id=#company_id#.";
$MESS['PATH_TO_COMPANY_EDIT_TIP'] = "Указывается путь к странице редактирования компании. Например: company_edit.php?company_id=#company_id#.";
$MESS['PATH_TO_DEAL_SHOW_TIP'] = "Указывается путь к странице просмотра сделки. Например: deal_show.php?deal_id=#deal_id#.";
$MESS['PATH_TO_DEAL_EDIT_TIP'] = "Указывается путь к странице редактирования сделки. Например: deal_edit.php?deal_id=#deal_id#.";
$MESS['PATH_TO_DEAL_LIST_TIP'] = "Указывается путь к странице списка сделок. Например: deal.php.";

?>