<?
$MESS["CRM_EVENT_TABLE_EMPTY"] = "- Ningun dato -";
$MESS["CRM_EVENT_TABLE_FILES"] = "Archivos";
$MESS["CRM_EVENT_DELETE"] = "Eliminar";
$MESS["CRM_EVENT_DELETE_TITLE"] = "Eliminar Evento";
$MESS["CRM_EVENT_DELETE_CONFIRM"] = "¿Está seguro que desea eliminarlo?";
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_IMPORT_EVENT"] = "Si el correo del contacto está especificado en el registro del CRM usted puede guardar automáticamente todos los mails relacionados, correspondientes como registro del evento. Para responder el mensaje recibido usted necesita la dirección del mail <b>%EMAIL%</b> asi como todo el texto y archivos adjuntos que serán agregados como Eventos para este contacto.";
$MESS["CRM_EVENT_VIEW_ADD_SHORT"] = "Evento";
$MESS["CRM_EVENT_VIEW_ADD"] = "Agregar evento";
$MESS["CRM_EVENT_VIEW_SHOW_FILTER_SHORT"] = "Filtro";
$MESS["CRM_EVENT_VIEW_SHOW_FILTER"] = "Mostrar/ocultar filtro";
$MESS["CRM_SHOW_ROW_COUNT"] = "Mostrar la cantidad";
?>