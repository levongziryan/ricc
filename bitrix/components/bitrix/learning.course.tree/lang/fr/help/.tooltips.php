<?
$MESS["COURSE_ID_TIP"] = "Sélectionnez ici l'un des cours existants. Si vous sélectionnez <b><i>(autre)</i></b>, vous devrez spécifier l'ID de cours dans le domaine de côté.";
$MESS["CHECK_PERMISSIONS_TIP"] = "Au cas où la valeur 'Oui' soit choisie, alors le droit d'accès sera vérifié.";
$MESS["SET_TITLE_TIP"] = "Lorsque l'option est cochée, la dénomination du chapitre du cours sera installée comme l'en-tête de la page.";
$MESS["CHAPTER_DETAIL_TEMPLATE_TIP"] = "Chemin vers la page du chapitre du cours.";
$MESS["TEST_DETAIL_TEMPLATE_TIP"] = "Chemin vers la page du test.";
$MESS["COURSE_DETAIL_TEMPLATE_TIP"] = "Chemin vers la page avec la revue détaillée du cours.";
$MESS["SELF_TEST_TEMPLATE_TIP"] = "Chemin vers la page avec un test pour l'autocontrôle.";
$MESS["TESTS_LIST_TEMPLATE_TIP"] = "Chemin vers la page d'une liste des tests du cours.";
$MESS["LESSON_DETAIL_TEMPLATE_TIP"] = "Chemin vers la page de la leçon du cours.";
?>