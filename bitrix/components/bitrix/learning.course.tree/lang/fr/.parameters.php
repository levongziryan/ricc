<?
$MESS["LEARNING_DESC_YES"] = "Oui";
$MESS["LEARNING_COURSE_ID_NAME"] = "Identificateur du cours";
$MESS["LEARNING_DESC_NO"] = "Non";
$MESS["LEARNING_CHECK_PERMISSIONS"] = "Vérifier le droit d'accès";
$MESS["LEARNING_URL_TEMPLATE_GROUP"] = "Gestion des adresses de pages";
$MESS["LEARNING_TEST_DETAIL_TEMPLATE_NAME"] = "URL, conduisant à la page de test";
$MESS["LEARNING_CHAPTER_DETAIL_TEMPLATE_NAME"] = "Page Chapitre URL";
$MESS["LEARNING_COURSE_DETAIL_TEMPLATE_NAME"] = "Cours détail URL de la page";
$MESS["LEARNING_SELF_TEST_TEMPLATE_NAME"] = "Page d'auto-teste URL";
$MESS["LEARNING_LESSON_DETAIL_TEMPLATE_NAME"] = "Page Leçon URL";
$MESS["LEARNING_TESTS_LIST_TEMPLATE_NAME"] = "Test de la liste URL de la page";
?>