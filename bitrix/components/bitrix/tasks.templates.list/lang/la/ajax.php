<?
$MESS["TASKS_PRIORITY"] = "Prioridad";
$MESS["TASKS_PRIORITY_0"] = "Baja";
$MESS["TASKS_PRIORITY_1"] = "Normal";
$MESS["TASKS_PRIORITY_2"] = "Alta";
$MESS["TASKS_MARK"] = "Puntaje";
$MESS["TASKS_MARK_NONE"] = "Ninguno";
$MESS["TASKS_MARK_P"] = "Positivo";
$MESS["TASKS_MARK_N"] = "Negativo";
$MESS["TASKS_DOUBLE_CLICK"] = "Doble click para ver";
?>