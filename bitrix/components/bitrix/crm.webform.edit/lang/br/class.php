<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "O módulo Processos de Negócio não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "O módulo Moeda não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "O módulo e-Store não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "O módulo Catálogo Comercial não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_REST"] = "O módulo Restante não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_WEBFORM_EDIT_TITLE"] = "Editar formulário de CRM";
$MESS["CRM_WEBFORM_EDIT_"] = "Pesquisar";
$MESS["CRM_WEBFORM_EDIT_TITLE_ADD"] = "Criar Formulário de CRM";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_DOMAIN"] = "Nome do domínio";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_DOMAIN_DESC"] = "Nome do domínio a partir do qual o formulário é enviado";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_URL"] = "Endereço da página";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_URL_DESC"] = "Endereço da página a partir da qual o formulário é enviado";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_PARAM"] = "Parâmetro";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_PARAM_DESC"] = "Nome do parâmetro URL que você irá usar para reconhecer o formulário";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_RESULT_ID"] = "ID do resultado";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_RESULT_ID_DESC"] = "Especifica o número atribuído ao resultado do formulário.";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_FORM_ID"] = "ID do formulário";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_FORM_ID_DESC"] = "ID do formulário";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_FORM_NAME"] = "Nome do Formulário";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_FORM_NAME_DESC"] = "Nome do Formulário";
$MESS["CRM_WEBFORM_EDIT_SECOND_SHORT"] = "seg";
$MESS["CRM_WEBFORM_EDIT_TITLE_VIEW"] = "Visualizar Formulário de CRM";
?>