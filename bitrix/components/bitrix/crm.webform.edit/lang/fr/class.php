<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "Le module Processus opérationnels n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "Le module Devise n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "Le module e-Store n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "Le module Catalogue commercial n'est pas installé.";
$MESS["CRM_MODULE_NOT_INSTALLED_REST"] = "Le module Rest n'est pas installé";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
$MESS["CRM_WEBFORM_EDIT_TITLE"] = "Modifier le formulaire CRM";
$MESS["CRM_WEBFORM_EDIT_"] = "Rechercher";
$MESS["CRM_WEBFORM_EDIT_TITLE_ADD"] = "Créer un formulaire CRM";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_DOMAIN"] = "Nom du domaine";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_DOMAIN_DESC"] = "Nom du domaine depuis lequel le formulaire est envoyé";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_URL"] = "Adresse de la page";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_URL_DESC"] = "Adresse de la page depuis laquelle le formulaire est envoyé ";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_PARAM"] = "Paramètre";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_PARAM_DESC"] = "Nom du paramètre de l'URL que vous utiliserez pour reconnaître le formulaire";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_RESULT_ID"] = "ID du résultat";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_RESULT_ID_DESC"] = "Spécifie le numéro assigné au résultat du formulaire.";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_FORM_ID"] = "ID du formulaire";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_FORM_ID_DESC"] = "ID du formulaire";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_FORM_NAME"] = "Nom du formulaire";
$MESS["CRM_WEBFORM_EDIT_PRESET_MACROS_FORM_NAME_DESC"] = "Nom du formulaire";
$MESS["CRM_WEBFORM_EDIT_SECOND_SHORT"] = "sec";
$MESS["CRM_WEBFORM_EDIT_TITLE_VIEW"] = "Afficher le formulaire CRM";
?>