<?
$MESS["M_SORT_PULL_TEXT"] = "Tirez vers le bas pour rafraîchir...";
$MESS["M_SORT_DOWN_TEXT"] = "Lâchez pour rafraîchir...";
$MESS["M_SORT_LOAD_TEXT"] = "Mise à jour...";
$MESS["M_SORT_TITLE"] = "Trier selon";
$MESS["M_SORT_FIELDS"] = "Trier les champs";
$MESS["M_SORT_ASC"] = "Ascendant";
$MESS["M_SORT_DESC"] = "Descendant";
$MESS["M_SORT_NO_FIELDS"] = "Champs de tri non spécifiés";
$MESS["M_SORT_BUTTON"] = "Trier";
$MESS["M_SORT_CANCEL"] = "Annuler";
?>