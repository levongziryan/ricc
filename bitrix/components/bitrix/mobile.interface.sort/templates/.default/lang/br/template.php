<?
$MESS["M_SORT_PULL_TEXT"] = "Puxe para baixo para atualizar...";
$MESS["M_SORT_DOWN_TEXT"] = "Solte para atualizar...";
$MESS["M_SORT_LOAD_TEXT"] = "Atualizando...";
$MESS["M_SORT_TITLE"] = "Classificar por";
$MESS["M_SORT_FIELDS"] = "Campos de classificação";
$MESS["M_SORT_ASC"] = "Ascendente";
$MESS["M_SORT_DESC"] = "Descendente";
$MESS["M_SORT_NO_FIELDS"] = "Campos de classificação não especificados";
$MESS["M_SORT_BUTTON"] = "Classificar";
$MESS["M_SORT_CANCEL"] = "Cancelar";
?>