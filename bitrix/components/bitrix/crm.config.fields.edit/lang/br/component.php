<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_FIELDS_EDIT_TITLE_EDIT"] = "Parâmetros do campo: #NAME#";
$MESS["CRM_FIELDS_EDIT_WRONG_FIELD"] = "O campo ID está inválido.";
$MESS["CC_BLFE_BAD_FIELD_NAME"] = "O nome do campo é obrigatório.";
$MESS["CC_BLFE_WRONG_LINK_IBLOCK"] = "Uma lista incorreta está especificada para uma propriedade tipo \"Amarrar\"";
$MESS["CC_BLFE_FIELD_NAME_DEFAULT"] = "Novo Campo";
$MESS["CC_BLFE_TITLE_EDIT"] = "Parâmetros do campo: #NAME#";
$MESS["CC_BLFE_TITLE_NEW"] = "Novo Campo";
$MESS["CC_BLFE_CHAIN_FIELDS"] = "Mapeamento do campo";
$MESS["CC_BLFE_CHAIN_LIST_EDIT"] = "Configurações de grade";
$MESS["CRM_FIELDS_ENTITY_LIST"] = "Tipos";
$MESS["CRM_FIELDS_EDIT_NAME_DEFAULT"] = "Novo Campo";
$MESS["CC_BLFE_BAD_FIELD_NAME_LANG"] = "Especifique o campo nome em #LANG_NAME#.";
?>