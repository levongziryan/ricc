<?
$MESS["CRM_PERMISSION_DENIED"] = "Acceso Denegado";
$MESS["CC_BLFE_BAD_FIELD_NAME"] = "El nombre del campo es requerido.";
$MESS["CC_BLFE_FIELD_NAME_DEFAULT"] = "Nuevo Campo";
$MESS["CC_BLFE_TITLE_NEW"] = "Nuevo Campo";
$MESS["CRM_FIELDS_ENTITY_LIST"] = "Tipos";
$MESS["CRM_FIELDS_EDIT_NAME_DEFAULT"] = "Nuevo Campo";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_FIELDS_EDIT_TITLE_EDIT"] = "Parámetros del Campo: #NAME#";
$MESS["CRM_FIELDS_EDIT_WRONG_FIELD"] = "El ID del campo es inválido.";
$MESS["CC_BLFE_WRONG_LINK_IBLOCK"] = "Un lista incorrecta está especificada para \"Bind\" el tipo de propiedad";
$MESS["CC_BLFE_TITLE_EDIT"] = "Parámetros del campo: #NAME#";
$MESS["CC_BLFE_CHAIN_FIELDS"] = "Mapeo del campo";
$MESS["CC_BLFE_CHAIN_LIST_EDIT"] = "Configuraciones del grid";
$MESS["CC_BLFE_BAD_FIELD_NAME_LANG"] = "Por favor, especifique el nombre del campo en #LANG_NAME#.";
?>