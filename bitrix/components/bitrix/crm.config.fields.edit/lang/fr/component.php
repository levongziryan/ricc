<?
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CC_BLFE_CHAIN_FIELDS"] = "Réglage des champs";
$MESS["CRM_FIELDS_EDIT_TITLE_EDIT"] = "Réglage du champ: #NAME#";
$MESS["CC_BLFE_TITLE_EDIT"] = "Réglage du champ: #NAME#";
$MESS["CC_BLFE_CHAIN_LIST_EDIT"] = "Réglage de la liste";
$MESS["CC_BLFE_FIELD_NAME_DEFAULT"] = "Ajouter un nouveau champ";
$MESS["CC_BLFE_TITLE_NEW"] = "Ajouter un nouveau champ";
$MESS["CRM_FIELDS_EDIT_NAME_DEFAULT"] = "Ajouter un nouveau champ";
$MESS["CC_BLFE_BAD_FIELD_NAME_LANG"] = "Veuillez indiquer le nom du champ en langue #LANG_NAME#.";
$MESS["CC_BLFE_BAD_FIELD_NAME"] = "Veuillez renseigner le nom de champ.";
$MESS["CRM_FIELDS_ENTITY_LIST"] = "Liste de modèles";
$MESS["CRM_FIELDS_EDIT_WRONG_FIELD"] = "ID incorrect du champ est indiqué.";
$MESS["CC_BLFE_WRONG_LINK_IBLOCK"] = "Une liste incorrecte est indiquée pour la propriété du type 'Rattachement'.";
?>