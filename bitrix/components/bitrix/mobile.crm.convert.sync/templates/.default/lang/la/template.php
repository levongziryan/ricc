<?
$MESS["M_CRM_CONVERT_SYNC_TITLE"] = "Crear usando el origen";
$MESS["M_CRM_CONVERT_SYNC_PULL_TEXT"] = "Pulsar para actualizar...";
$MESS["M_CRM_CONVERT_SYNC_DOWN_TEXT"] = "Soltar para actualizar...";
$MESS["M_CRM_CONVERT_SYNC_LOAD_TEXT"] = "Actualizando...";
$MESS["M_CRM_CONVERT_SYNC_FIELDS"] = "Estos campos serán creados";
$MESS["M_CRM_CONVERT_SYNC_CONTINUE"] = "Continuar";
$MESS["M_CRM_CONVERT_SYNC_MORE"] = "Más";
$MESS["M_CRM_CONVERT_SYNC_ENTITIES"] = "Seleccione los documentos que serán repuestos con los campos que faltan";
$MESS["M_CRM_CONVERT_SYNC_LEGEND_LEAD"] = "Entidades seleccionadas no tienen campos que pueden almacenar datos del prospecto. Por favor, seleccione las entidades en las cuales se crearán campos que faltan para dar cabida a toda la información disponible.";
$MESS["M_CRM_CONVERT_SYNC_LEGEND_DEAL"] = "Entidades seleccionadas no tienen campos que pueden almacenar los datos de la negociación. Por favor, seleccione las entidades en las cuales se crearán campos que faltan para dar cabida a toda la información disponible.";
$MESS["M_CRM_CONVERT_SYNC_LEGEND_QUOTE"] = "Entidades seleccionadas no tienen campos que pueden almacenar datos de las cotizaciones. Por favor, seleccione las entidades en las cuales se crearán campos que faltan para dar cabida a toda la información disponible.";
?>