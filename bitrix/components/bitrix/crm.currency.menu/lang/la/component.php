<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_CURRENCY_ADD"] = "Agregar tipo de cambio";
$MESS["CRM_CURRENCY_ADD_TITLE"] = "Crear un nuevo registro de tipo de cambio";
$MESS["CRM_CURRENCY_EDIT"] = "Editar";
$MESS["CRM_CURRENCY_EDIT_TITLE"] = "Editar el registro de este tipo de cambio";
$MESS["CRM_CURRENCY_DELETE"] = "Eliminar tipo de cambio";
$MESS["CRM_CURRENCY_DELETE_TITLE"] = "Eliminar tipo de cambio";
$MESS["CRM_CURRENCY_DELETE_DLG_TITLE"] = "Eliminar tipo de cambio";
$MESS["CRM_CURRENCY_DELETE_DLG_MESSAGE"] = "¿Está seguro que desea eliminar el tipo de cambio?";
$MESS["CRM_CURRENCY_DELETE_DLG_BTNTITLE"] = "Eliminar tipo de cambio";
$MESS["CRM_CURRENCY_SHOW"] = "Ver";
$MESS["CRM_CURRENCY_SHOW_TITLE"] = "Mostrar tipo de cambio";
$MESS["CRM_CURRENCY_LIST"] = "Todas las monedas";
$MESS["CRM_CURRENCY_LIST_TITLE"] = "Ver  lista de todas las monedas";
?>