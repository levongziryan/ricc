<?
$MESS["BPWC_NO_CRM_MODULE"] = "O módulo CRM não está instalado.";
$MESS["CRM_EXT_SALE_DEJ_TITLE1"] = "Assistente de Integração";
$MESS["CRM_MODULE_NOT_INSTALLED_IBLOCK"] = "O módulo Blocos de Informações não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CURRENCY"] = "O módulo Moeda não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "O módulo Venda não está instalado.";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "O módulo Catálogo não está instalado.";
?>