<?
$MESS["SONET_C4_FUNC_TITLE"] = "Fonctionnalité '#NAME#' activée.";
$MESS["SONET_C4_FUNC_TITLE_ON"] = "Fonctionnalité '#NAME#' activée.";
$MESS["SONET_C4_FUNC_TITLE_OFF"] = "La fonctionnalité '#NAME#' est désactivée.";
$MESS["SONET_C4_SUBMIT"] = "Modifier les paramètres";
$MESS["SONET_C4_GR_SUCCESS"] = "Les paramètres du groupe sont modifiés avec succès.";
$MESS["SONET_C4_US_SUCCESS"] = "Les paramètres de l'utilisateur sont modifiés avec succès.";
$MESS["SONET_C4_T_CANCEL"] = "Annuler";
$MESS["SONET_FEATURES_NAME"] = "Dénomination";
$MESS["SONET_C4_NO_FEATURES"] = "La fonctionnalité dans les groupes est désactivée, vous pouvez l'activer sur la page d'édition du groupe.";
?>