<?
$MESS["INTL_USERGROUPS_MODIFY"] = "Groupes d'utilisateurs qui peuvent changer les salles de réunion";
$MESS["INTL_MEETING_VAR"] = "Nom de la variable pour l'identificateur de la salle de réunion";
$MESS["INTL_PAGE_VAR"] = "Nom de la variable pour la page";
$MESS["INTL_IBLOCK"] = "Bloc d'information";
$MESS["INTL_MEETING_ID"] = "Code de la salle de réunions";
$MESS["INTL_VARIABLE_ALIASES"] = "Alias des variables";
$MESS["INTL_PATH_TO_MEETING_LIST"] = "Chemin vers la page principale de réservation des salles de réunion";
$MESS["INTL_PATH_TO_MEETING"] = "Chemin vers le graphique de la salle de réunion";
$MESS["INTL_IBLOCK_TYPE"] = "Type de bloc d'information";
$MESS["INTL_SET_NAVCHAIN"] = "Configurer le fil d'Ariane";
?>