<?
$MESS["INAF_F_ID"] = "ID";
$MESS["INTASK_C36_SHOULD_AUTH"] = "Pour créer la discussion vous devez vous connecter.";
$MESS["INTASK_C36_PAGE_TITLE2"] = "Changement de la salle des Réunions";
$MESS["INAF_F_PLACE"] = "Nombre de places";
$MESS["EC_INTRANET_MODULE_NOT_INSTALLED"] = "Le module 'Intranet' n'a pas été installé.";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "Le module des blocs d'informations n'a pas été installé.";
$MESS["INAF_F_NAME"] = "Titre";
$MESS["INTASK_C36_EMPTY_NAME"] = "Le nom de la salle de réunion n'est pas indiqué.";
$MESS["INAF_F_DESCRIPTION"] = "Description";
$MESS["INAF_MEETING_NOT_FOUND"] = "Une salle de réunion n'est pas retrouvée.";
$MESS["INTASK_C36_PAGE_TITLE1"] = "Points de négociations";
$MESS["INTASK_C36_PAGE_TITLE"] = "Création d'une Salle de Réunion";
$MESS["INAF_F_PHONE"] = "Numéro de téléphone";
$MESS["INTS_NO_IBLOCK_PERMS"] = "Vous n'avez pas la permission d'afficher le bloc d'information de la salle de réunion.";
$MESS["INTASK_C36_NO_PERMS2CREATE"] = "Vous n'avez pas le droit de créer une négociation.";
$MESS["INAF_F_FLOOR"] = "Etage";
?>