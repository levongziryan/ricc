<?
$MESS["EC_INTRANET_MODULE_NOT_INSTALLED"] = "El módulo de Intranet no está instalado.";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "El modulo Information Blocks no está instalado.";
$MESS["INTS_NO_IBLOCK_PERMS"] = "Usted no tiene permiso para ver las tareas del bloque informativo.";
$MESS["INTASK_C36_PAGE_TITLE"] = "Crear reserva de la sala de reuniones";
$MESS["INTASK_C36_PAGE_TITLE2"] = "Editar Sala de Reuniones";
$MESS["INTASK_C36_PAGE_TITLE1"] = "Reserva de la sala de reuniones";
$MESS["INAF_F_ID"] = "ID";
$MESS["INAF_F_NAME"] = "Titulo";
$MESS["INAF_F_DESCRIPTION"] = "Descripción";
$MESS["INAF_F_FLOOR"] = "Piso";
$MESS["INAF_F_PLACE"] = "Asientos";
$MESS["INAF_F_PHONE"] = "Teléfono";
$MESS["INAF_MEETING_NOT_FOUND"] = "La sala de reuniones no fue encontrada.";
$MESS["INTASK_C36_SHOULD_AUTH"] = "Por favor autorizar antes de crear una nueva sala de reuniones.";
$MESS["INTASK_C36_NO_PERMS2CREATE"] = "Usted no tiene permiso para crear una nueva sala de reuniones";
$MESS["INTASK_C36_EMPTY_NAME"] = "El titulo de la sala de reuniones esta vacía.";
?>