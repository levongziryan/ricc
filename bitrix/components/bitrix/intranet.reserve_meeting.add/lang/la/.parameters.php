<?
$MESS["INTL_VARIABLE_ALIASES"] = "Variable del alias";
$MESS["INTL_IBLOCK_TYPE"] = "Tipo de Bloque Informativo";
$MESS["INTL_IBLOCK"] = "Bloque Informativo";
$MESS["INTL_MEETING_VAR"] = "Variable para el ID de la Sala de Reuniones";
$MESS["INTL_PAGE_VAR"] = "Variable de la página";
$MESS["INTL_MEETING_ID"] = "ID de la Sala de Reuniones";
$MESS["INTL_PATH_TO_MEETING"] = "Página de Horarios de las Reservas de la Sala de Reuniones";
$MESS["INTL_PATH_TO_MEETING_LIST"] = "Página Principal de Reserva de la Sala de Reuniones";
$MESS["INTL_SET_NAVCHAIN"] = "Establecer atajos";
$MESS["INTL_USERGROUPS_MODIFY"] = "Grupos de Usuarios Permitidos para Editar Horarios para la Sala de Reuniones";
?>