<?
$MESS["OL_PERMISSION_DENIED"] = "Accès refusé";
$MESS["OL_PERMISSION_CREATE_LINE"] = "Vous ne disposez pas des permissions pour créer un nouveau Canal ouvert.";
$MESS["OL_PERMISSION_MODIFY_LINE"] = "Vous ne disposez pas des permissions pour éditer ce Canal ouvert.";
?>