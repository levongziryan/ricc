<?
$MESS["BM_TO_USER_LIST"] = "Revenir vers la liste";
$MESS["BM_WRITE"] = "Écrire";
$MESS["BM_DEPARTMENT"] = "Département";
$MESS["BM_DIRECTOR"] = "A qui est soumis";
$MESS["BM_DIRECTOR_OF"] = "Qui administrer";
$MESS["BM_USR_CNT"] = "Quantité d'employés";
$MESS["BM_NO_USERS"] = "Pas d'utilisateurs";
$MESS["BM_PHONE"] = "Numéro de téléphone";
$MESS["BM_PHONE_MOB"] = "Portable";
$MESS["BM_PHONE_INT"] = "Intérieur";
$MESS["BM_BIRTHDAY"] = "Date de naissance";
?>