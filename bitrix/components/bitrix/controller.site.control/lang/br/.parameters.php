<?
$MESS["CP_BCSC_SITE_URL"] = "URL do site";
$MESS["CP_BCSC_COMMAND"] = "Comando";
$MESS["CP_BCSC_ACTION"] = "Ação";
$MESS["CP_BCSC_ACCESS_RESTRICTION"] = "Restringir o acesso";
$MESS["CP_BCSC_BY_GROUP"] = "Por grupo";
$MESS["CP_BCSC_BY_IP"] = "Por IP";
$MESS["CP_BCSC_IP_PERMISSIONS"] = "Permitir conexão de gerenciamento do IP";
$MESS["CP_BCSC_GROUP_PERMISSIONS"] = "Permitir a conexão de gestão para grupos de usuários";
$MESS["CP_BCSC_SEPARATOR"] = "Separador de campo";
?>