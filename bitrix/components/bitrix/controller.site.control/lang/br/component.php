<?
$MESS["CC_BCSC_ERROR_MODULE"] = "O módulo controlador não está instalado.";
$MESS["CC_BCSC_UPDATE_ERROR"] = "Erro ao atualizar o site ##ID#: #MESSAGE#";
$MESS["CC_BCSC_DELETE_ERROR"] = "Erro ao excluir o site #ID#.";
$MESS["CC_BCSC_UNKNOWN_GROUP"] = "Grupo desconhecido.";
$MESS["CC_BCSC_EMAIL_ERROR"] = "Erro ao alterar o #ID# site administrador de e-mail: #MESSAGE#";
$MESS["CC_BCSC_PASSWORD_ERROR"] = "Erro ao alterar a #ID# senha do site: #MESSAGE#";
$MESS["CC_BCSC_TITLE"] = "Componente de Gerenciamento do Site";
$MESS["CC_BCSC_RESERVE_ERROR_ALREADY"] = "Um site com esse endereço já existe.";
$MESS["CC_BCSC_RESERVE_ERROR_SITE_URL"] = "A URL do site está faltando ou está incorreta.";
$MESS["CC_BCSC_RESERVE_ERROR"] = "Erro ao reservar o site: #MESSAGE#.";
?>