<?
$MESS["CC_BCSC_RESERVE_ERROR_SITE_URL"] = "URL du site n'est pas indiqué ou est indiqué inexactement.";
$MESS["CC_BCSC_TITLE"] = "Composant de la gestion de sites.";
$MESS["CC_BCSC_ERROR_MODULE"] = "Le module du contrôleur non installé.";
$MESS["CC_BCSC_UNKNOWN_GROUP"] = "Groupe inconnu.";
$MESS["CC_BCSC_UPDATE_ERROR"] = "Mise à jour de l'état d'erreur #ID#: #MESSAGE#";
$MESS["CC_BCSC_RESERVE_ERROR"] = "Erreur réservant le site: #MESSAGE#.";
$MESS["CC_BCSC_EMAIL_ERROR"] = "Erreur de changement d'Adresse emaild'administrateur du site #ID#: #MESSAGE#";
$MESS["CC_BCSC_PASSWORD_ERROR"] = "Erreur de changement de mot de passe de l'administrateur du site #ID#: #MESSAGE#";
$MESS["CC_BCSC_DELETE_ERROR"] = "Erreur de la suppression du site #ID#.";
$MESS["CC_BCSC_RESERVE_ERROR_ALREADY"] = "Le site avec cette adresse existe déjà.";
?>