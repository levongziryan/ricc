<?
$MESS["CP_BCSC_IP_PERMISSIONS"] = "Adresse IP depuis laquelle la gestion est autorisée";
$MESS["CP_BCSC_SITE_URL"] = "Adresse du site";
$MESS["CP_BCSC_GROUP_PERMISSIONS"] = "Les groupes dont les utilisateurs sont autorisés à se charger de la gestion";
$MESS["CP_BCSC_ACTION"] = "Action";
$MESS["CP_BCSC_COMMAND"] = "Commande";
$MESS["CP_BCSC_ACCESS_RESTRICTION"] = "Limiter l'accès";
$MESS["CP_BCSC_BY_IP"] = "Par IP";
$MESS["CP_BCSC_BY_GROUP"] = "Selon le groupe";
$MESS["CP_BCSC_SEPARATOR"] = "Diviseur de champs";
?>