<?
$MESS["SITE_URL_TIP"] = "URL del sitio";
$MESS["COMMAND_TIP"] = "Comando";
$MESS["ACTION_TIP"] = "Acción";
$MESS["SEPARATOR_TIP"] = "Separador del campo";
$MESS["ACCESS_RESTRICTION_TIP"] = "Acceso restringido";
$MESS["GROUP_PERMISSIONS_TIP"] = "Especificar los grupos de usuarios con permisos de administración";
?>