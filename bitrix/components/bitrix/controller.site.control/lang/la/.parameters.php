<?
$MESS["CP_BCSC_SITE_URL"] = "URL del sitio";
$MESS["CP_BCSC_COMMAND"] = "Comando";
$MESS["CP_BCSC_ACTION"] = "Acción";
$MESS["CP_BCSC_ACCESS_RESTRICTION"] = "Restringir acceso";
$MESS["CP_BCSC_BY_GROUP"] = "por grupo";
$MESS["CP_BCSC_BY_IP"] = "por IP";
$MESS["CP_BCSC_IP_PERMISSIONS"] = "Permitir la conexión de administración desde el IP";
$MESS["CP_BCSC_GROUP_PERMISSIONS"] = "Permitir la conexión de administración para los grupos de usuarios";
$MESS["CP_BCSC_SEPARATOR"] = "Separador del campo";
?>