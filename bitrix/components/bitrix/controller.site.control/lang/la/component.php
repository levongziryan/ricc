<?
$MESS["CC_BCSC_ERROR_MODULE"] = "El modulo de Controlador del sitio no esta instalado";
$MESS["CC_BCSC_UPDATE_ERROR"] = "Error actualizando el sitio #ID#: #MESSAGE# ";
$MESS["CC_BCSC_DELETE_ERROR"] = "Error eliminando el sitio #ID#.";
$MESS["CC_BCSC_UNKNOWN_GROUP"] = "Grupo desconocido";
$MESS["CC_BCSC_EMAIL_ERROR"] = "Error cambiando el e-mail del administrador del sitio #ID#: #MESSAGE#";
$MESS["CC_BCSC_PASSWORD_ERROR"] = "Error cambiando la contraseña del sitio #ID#: #MESSAGE#";
$MESS["CC_BCSC_TITLE"] = "Componente de Administración del sitio.";
$MESS["CC_BCSC_RESERVE_ERROR_ALREADY"] = "Un sitio con esta dirección ya existe.";
$MESS["CC_BCSC_RESERVE_ERROR_SITE_URL"] = "El URL del sitio esta desaparecido o es incorrecto.";
$MESS["CC_BCSC_RESERVE_ERROR"] = "Error reservando el sitio: #MESSAGE#.";
?>