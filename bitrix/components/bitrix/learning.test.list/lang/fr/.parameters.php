<?
$MESS["LEARNING_DESC_YES"] = "Oui";
$MESS["LEARNING_COURSE_ID"] = "Identificateur du cours";
$MESS["LEARNING_TESTS_PER_PAGE"] = "Nombre de tests par page";
$MESS["LEARNING_DESC_NO"] = "Non";
$MESS["LEARNING_CHECK_PERMISSIONS"] = "Vérifier le droit d'accès";
$MESS["LEARNING_TEST_TEMPLATE_NAME"] = "URL de la page de test en ligne";
?>