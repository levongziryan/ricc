<?
$MESS["LEARNING_TEST_DENIED_PREVIOUS"] = "L'accès au test est interdit parce que vous n'a avez pas passé le test #TEST_LINK#";
$MESS["LEARNING_TESTS_NAV"] = "De tests";
$MESS["LEARNING_BAD_TEST_LIST"] = "Ce cours ne fournit pas de certification.";
$MESS["LEARNING_MODULE_NOT_FOUND"] = "Module des blogs non installé.";
$MESS["LEARNING_TESTS_LIST"] = "Liste des tests";
$MESS["LEARNING_COURSE_DENIED"] = "Cours introuvable ou accès refusé.";
?>