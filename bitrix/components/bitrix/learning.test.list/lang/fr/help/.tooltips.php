<?
$MESS["COURSE_ID_TIP"] = "Sélectionnez ici l'un des cours existants. Si vous sélectionnez <b><i>(autre)</i></b>, vous devrez spécifier l'ID de cours dans le domaine de côté.";
$MESS["TESTS_PER_PAGE_TIP"] = "Le nombre maximum de tests qui peuvent être affichés sur une page. D'autres tests seront disponibles en utilisant le fil d'ariane.";
$MESS["CHECK_PERMISSIONS_TIP"] = "Au cas où la valeur 'Oui' soit choisie, alors le droit d'accès sera vérifié.";
$MESS["SET_TITLE_TIP"] = "Lorsque l'option est cochée, <b>Liste de textes</b> sera affichée comme l'en-tête de la page.";
$MESS["TEST_DETAIL_TEMPLATE_TIP"] = "Chemin vers la page du test.";
?>