<?
$MESS["LEARNING_TEST_TIME_LIMIT_UNLIMITED"] = "sans restrictions";
$MESS["LEARNING_PREV_TEST_REQUIRED"] = "Pour avoir l'accès au test il faut passer le test #TEST_LINK# et obtenir #TEST_SCORE#% au minimum du nombre total de points.";
$MESS["LEARNING_PASSAGE_NO_FOLLOW_NO_EDIT"] = "il est interdit de passer à la question suivante sans répondre à la question courante, <b>Non autorisée</b> de modifier ses réponses.";
$MESS["LEARNING_TEST_ATTEMPT_LIMIT"] = "Nombre d'essais";
$MESS["LEARNING_TEST_TIME_LIMIT_MIN"] = "min.";
$MESS["LEARNING_TEST_NAME"] = "Nom du test";
$MESS["LEARNING_BTN_START"] = "Commencer";
$MESS["LEARNING_TEST_ATTEMPT_UNLIMITED"] = "nombre illimité";
$MESS["LEARNING_TEST_TIME_LIMIT"] = "Limite dans le temps";
$MESS["LEARNING_BTN_CONTINUE"] = "Continuer";
$MESS["LEARNING_PASSAGE_FOLLOW_EDIT"] = "le passage à la question suivante sans réponse à la question actuelle est permis, <b>il est possible</b> de modifier ses réponses.";
$MESS["LEARNING_PASSAGE_FOLLOW_NO_EDIT"] = "le passage à la question suivante est autorisée sans réponse à la question courante, <b>il est interdit de</b> modifier vos réponses.";
$MESS["LEARNING_PASSAGE_TYPE"] = "Type de passation du test";
?>