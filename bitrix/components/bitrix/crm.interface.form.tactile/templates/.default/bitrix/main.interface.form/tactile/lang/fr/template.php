<?
$MESS["interface_form_show_all"] = "Ouvrir tous les onglets sur une page";
$MESS["interface_form_save"] = "Sauvegarder";
$MESS["interface_form_save_title"] = "Sauvegarder et revenir";
$MESS["interface_form_apply"] = "Appliquer";
$MESS["interface_form_apply_title"] = "Sauvegarder et rester dans le formulaire";
$MESS["interface_form_cancel"] = "Annuler";
$MESS["interface_form_cancel_title"] = "Ne pas sauvegarder et revenir";
$MESS["interface_form_close_all"] = "Minimiser les onglets";
$MESS["interface_form_required"] = "Champs obligatoires à remplir.";
$MESS["interface_form_colors"] = "Couleur du thème";
$MESS["interface_form_colors_title"] = "Choisir le schéma de couleur du formulaire";
$MESS["interface_form_default"] = "(par défaut)";
$MESS["interface_form_settings"] = "Réglages de la forme";
$MESS["interface_form_tabs"] = "Onglets";
$MESS["intarface_form_up"] = "Déplacer vers le haut";
$MESS["intarface_form_up_title"] = "Déplacer plus haut sur la liste";
$MESS["intarface_form_up_down"] = "Déplacer vers le bas";
$MESS["intarface_form_down_title"] = "Déplacer vers le bas";
$MESS["intarface_form_add"] = "Ajouter";
$MESS["intarface_form_add_title"] = "Ajouter un nouveau onglet";
$MESS["intarface_form_edit"] = "Editer";
$MESS["intarface_form_edit_title"] = "Modifier la dénomination de l'onglet";
$MESS["intarface_form_del"] = "Supprimer";
$MESS["intarface_form_del_title"] = "Supprimer un onglet";
$MESS["intarface_form_fields"] = "Champs sur l'onglet";
$MESS["intarface_form_fields_available"] = "Champs accessibles:";
$MESS["intarface_form_add_field"] = "Ajouter le champ";
$MESS["intarface_form_del_field"] = "Supprimer le Champ";
$MESS["intarface_form_fields_on_tab"] = "Champs sur l'onglet:";
$MESS["intarface_form_add_sect"] = "Créer une nouvelle section";
$MESS["intarface_form_edit_field"] = "Changer le nom du champ";
$MESS["intarface_form_settings"] = "Réglages de la forme";
$MESS["intarface_form_tab"] = "Onglet";
$MESS["intarface_form_tab_name"] = "Nom d'onglet:";
$MESS["intarface_form_tab_title"] = "Titre de l'onglet:";
$MESS["intarface_form_field"] = "Champ";
$MESS["intarface_form_field_name"] = "Nom du champ:";
$MESS["intarface_form_sect"] = "Section de séparation";
$MESS["intarface_form_sect_name"] = "Dénomination de la section:";
$MESS["intarface_form_mnu_settings"] = "Réglages de la forme";
$MESS["intarface_form_mnu_settings_title"] = "Régler les onglets et les champs de la forme";
$MESS["intarface_form_mnu_on"] = "Activer les Paramètres";
$MESS["intarface_form_mnu_on_title"] = "Afficher la forme configurée";
$MESS["intarface_form_mnu_off"] = "Affichage par défaut";
$MESS["intarface_form_mnu_off_title"] = "Afficher le formulaire par défaut";
$MESS["intarface_form_show_additional_info"] = "Information complémentaire";
$MESS["interface_form_save_and_view"] = "Sauvegarder";
$MESS["interface_form_save_and_view_title"] = "Sauvegarder et passer à l'affichage";
$MESS["interface_form_save_and_add"] = "Sauvegarder et ajouter d'autres";
$MESS["interface_form_save_and_add_title"] = "Sauvegarder et passer à la création d'un nouvel élément";
$MESS["interface_form_add_new_entity"] = "Ajouter";
$MESS["interface_form_add_contact_dlg_title"] = "Création rapide d'un contact";
$MESS["interface_form_add_contact_fld_last_name"] = "Nom";
$MESS["interface_form_add_contact_fld_name"] = "Prénom";
$MESS["interface_form_add_contact_fld_second_name"] = "Prénom";
$MESS["interface_form_add_contact_fld_email"] = "Courrier électronique";
$MESS["interface_form_add_contact_fld_phone"] = "Numéro de téléphone";
$MESS["interface_form_add_dialog_btn_add"] = "Ajouter";
$MESS["interface_form_add_company_dlg_title"] = "Création rapide de l'entreprise";
$MESS["interface_form_add_company_fld_title_name"] = "Dénomination";
$MESS["interface_form_add_conpany_fld_company_type"] = "Entité";
$MESS["interface_form_add_company_fld_industry"] = "Sphère de l'activité";
$MESS["interface_form_add_conpany_fld_email"] = "Courrier électronique";
$MESS["interface_form_add_company_fld_phone"] = "Numéro de téléphone";
$MESS["interface_form_add_company_fld_address_legal"] = "Adresse juridique";
$MESS["intarface_form_select"] = "Sélectionner";
$MESS["interface_form_ajax_unknown_error"] = "Une erreur a eu lieu lors de l'affichage du mode de serveur.";
$MESS["interface_form_set_datetime"] = "Ensemble";
$MESS["interface_form_add_btn_company"] = "entreprise";
$MESS["interface_form_add_btn_or"] = "ou";
$MESS["interface_form_add_btn_contact"] = "contact";
$MESS["interface_form_add_contact_fld_export"] = "Inclure dans le test";
$MESS["interface_form_add_btn_add_field"] = "Ajouter le champ";
$MESS["interface_form_add_btn_restore_field"] = "Montrer le champ";
$MESS["interface_form_add_string_field_menu_item"] = "Ligne";
$MESS["interface_form_add_double_field_menu_item"] = "Chiffre";
$MESS["interface_form_add_boolean_field_menu_item"] = "Oui/Non";
$MESS["interface_form_add_datetime_field_menu_item"] = "Date";
$MESS["interface_form_add_section_menu_item"] = "Créer une section";
$MESS["interface_form_new_field_name"] = "Ajouter un nouveau champ";
$MESS["interface_form_new_section_name"] = "Nouvelle section";
$MESS["interface_form_field_name_placeholder"] = "Nom de champ";
$MESS["interface_form_section_ttl_placeholder"] = "Dénomination de la section";
$MESS["interface_form_field_delete_dlg_title"] = "Supprimer le Champ";
$MESS["interface_form_field_delete_dlg_content"] = "tes-vous sûr de vouloir supprimer ce champ?";
$MESS["interface_form_save_for_all_menu_item"] = "Sauvegarder les réglages pour tous les utilisateurs";
$MESS["interface_form_section_delete_dlg_title"] = "Suppression de Section";
$MESS["interface_form_section_delete_dlg_content"] = "tes-vous sûr devouloir supprimer cette section?";
$MESS["interface_form_reset_menu_item"] = "Effacer les paramètres";
$MESS["interface_form_settings_saved"] = "Les paramètres ont été sauvegardés avec succès.";
$MESS["interface_form_settings_undo_change"] = "Annuler des modifications";
$MESS["interface_form_field_hide_dlg_title"] = "Cacher le champ";
$MESS["interface_form_field_hide_dlg_content"] = "Voulez-vous vraiment cacher ce champ?";
$MESS["interface_form_hide"] = "Cacher";
$MESS["interface_form_section_has_required_fields"] = "La section ne peut pas être supprimé car il contient des champs obligatoires ou statiques.";
$MESS["interface_form_add_btn_add_section"] = "Ajouter une section";
$MESS["interface_form_field_field_edit_menu_item"] = "Modifier nom";
$MESS["interface_form_field_field_hide_menu_item"] = "Cacher";
$MESS["interface_form_entity_selector_prefContactType"] = "Entité";
$MESS["interface_form_entity_selector_prefPhone"] = "Numéro de téléphone";
$MESS["interface_form_entity_selector_prefEmail"] = "Courrier électronique";
$MESS["interface_form_continue"] = "Continuer";
$MESS["interface_form_continue_title"] = "Enregistrer et continuer";
$MESS["CRM_SIP_MGR_UNKNOWN_RECIPIENT"] = "Appelant inconnu";
$MESS["CRM_SIP_MGR_MAKE_CALL"] = "Appel";
$MESS["interface_form_in_short_list_option_title"] = "Afficher en vue détaillée";
$MESS["interface_form_bank_details_ttl_placeholder"] = "Nom des données bancaires";
$MESS["interface_form_entity_selector_prefPhoneLong"] = "Téléphone";
$MESS["interface_form_entity_selector_tabTitleAbout"] = "Information";
$MESS["interface_form_entity_selector_contactTabTitleAbout"] = "Sur le contact";
$MESS["interface_form_entity_selector_companyTabTitleAbout"] = "Au sujet de la société";
$MESS["interface_form_entity_selector_bankDetailsTitle"] = "Données bancaires";
$MESS["CRM_ADRESS_DELETE_CONFIRMATION"] = "Voulez-vous vraiment supprimer cette adresse ?";
$MESS["CRM_ADDRESS_ALREADY_EXISTS"] = "#TYPE_NAME# existe déjà. Veuillez sélectionner un autre type d'adresse.";
$MESS["CRM_BANK_DETAILS_ADD_BTN_TEXT"] = "Ajouter";
$MESS["interface_form_entity_selector_tabTitleContactRequisites"] = "Données de contact";
$MESS["interface_form_entity_selector_tabTitleCompanyRequisites"] = "Données de la société";
$MESS["CRM_ADDRESS_COPY_CONFIRMATION"] = "Il existe déjà une adresse de ce type. Voulez-vous déplacer ces champs d'adresse dans la nouvelle adresse ?";
$MESS["CRM_SIP_MGR_ENABLE_CALL_RECORDING"] = "Enregistrer la conversation";
$MESS["CRM_ADDRESS_CREATE"] = "Ajouter #TYPE_NAME#";
?>