<?
$MESS["TASKS_PARAM_VARIABLE_ALIASES"] = "Alias Variables";
$MESS["TASKS_PARAM_USER_ID"] = "ID del Usuario";
$MESS["TASKS_PARAM_GROUP_ID"] = "ID del Grupo";
$MESS["TASKS_PARAM_TASK_VAR"] = "Nombre variable del ID de la tarea";
$MESS["TASKS_PARAM_USER_VAR"] = "Nombre variable del ID del usuario";
$MESS["TASKS_PARAM_GROUP_VAR"] = "Nombre variable del ID del grupo";
$MESS["TASKS_PARAM_ACTION_VAR"] = "Nombre variable del ID de la acción";
$MESS["TASKS_PARAM_PAGE_VAR"] = "Nombre variable del ID de la página";
$MESS["TASKS_PARAM_PATH_TO_USER_TASKS"] = "Ruta de las tareas del Usuario";
$MESS["TASKS_PARAM_PATH_TO_USER_TASKS_TASK"] = "Ruta a la Tarea del Usuario";
$MESS["TASKS_PARAM_PATH_TO_GROUP_TASKS"] = "Ruta a las Tareas del grupo";
$MESS["TASKS_PARAM_PATH_TO_GROUP_TASKS_TASK"] = "Ruta A La Tarea Del Grupo";
$MESS["TASKS_PARAM_TASK_ID"] = "ID";
?>