<?
$MESS["TASKS_PARAM_GROUP_VAR"] = "Nom de la variable pour le code du groupe";
$MESS["TASKS_PARAM_ACTION_VAR"] = "Nom de la variable pour le code d'action";
$MESS["TASKS_PARAM_TASK_VAR"] = "Nom de la variable pour le code de la tâche";
$MESS["TASKS_PARAM_USER_VAR"] = "Nom de la variable pour le code d'utilisateur";
$MESS["TASKS_PARAM_PAGE_VAR"] = "Nom de la variable pour le code de la page";
$MESS["TASKS_PARAM_TASK_ID"] = "ID";
$MESS["TASKS_PARAM_GROUP_ID"] = "Identifiant du groupe";
$MESS["TASKS_PARAM_USER_ID"] = "Identifiant de l'utilisateur";
$MESS["TASKS_PARAM_VARIABLE_ALIASES"] = "Alias des variables";
$MESS["TASKS_PARAM_PATH_TO_GROUP_TASKS_TASK"] = "Chemin vers la tâche du groupe";
$MESS["TASKS_PARAM_PATH_TO_USER_TASKS_TASK"] = "Chemin vers la tâche de l'utilisateur";
$MESS["TASKS_PARAM_PATH_TO_GROUP_TASKS"] = "Chemin vers la liste de tâches du groupe";
$MESS["TASKS_PARAM_PATH_TO_USER_TASKS"] = "Chemin vers la liste de tâches de l'utilisateur";
?>