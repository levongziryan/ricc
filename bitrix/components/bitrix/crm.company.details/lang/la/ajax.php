<?
$MESS["CRM_COMPANY_NOT_FOUND"] = "La compañía no fue encontrada.";
$MESS["CRM_COMPANY_ACCESS_DENIED"] = "Acceso denegado.";
$MESS["CRM_COMPANY_DELETION_ERROR"] = "Error al eliminar la compañía.";
?>