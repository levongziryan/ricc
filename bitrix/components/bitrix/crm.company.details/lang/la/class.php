<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módelo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Accesos denegado";
$MESS["CRM_COMPANY_CREATION_PAGE_TITLE"] = "Agregar Compañía";
$MESS["CRM_COMPANY_FIELD_TITLE"] = "Nombre de la compañía";
$MESS["CRM_COMPANY_FIELD_ASSIGNED_BY_ID"] = "Persona responsable";
$MESS["CRM_COMPANY_FIELD_LOGO"] = "Logotipo";
$MESS["CRM_COMPANY_FIELD_COMPANY_TYPE"] = "Tipo de compañía";
$MESS["CRM_COMPANY_FIELD_INDUSTRY"] = "Industrial";
$MESS["CRM_COMPANY_FIELD_EMPLOYEES"] = "Empleados";
$MESS["CRM_COMPANY_FIELD_REVENUE"] = "Ingresos anuales";
$MESS["CRM_COMPANY_FIELD_COMMENTS"] = "Comentario";
$MESS["CRM_COMPANY_FIELD_BANKING_DETAILS"] = "Datos bancarios";
$MESS["CRM_COMPANY_FIELD_OPENED"] = "Disponible para todos";
$MESS["CRM_COMPANY_FIELD_IS_MY_COMPANY"] = "Vendedor";
$MESS["CRM_COMPANY_SECTION_MAIN"] = "Acerca de la compañía";
$MESS["CRM_COMPANY_SECTION_ADDITIONAL"] = "Más";
$MESS["CRM_COMPANY_TAB_INVOICES"] = "Facturas";
$MESS["CRM_COMPANY_TAB_EVENT"] = "Historial";
$MESS["CRM_COMPANY_TAB_QUOTE"] = "Cotización";
$MESS["CRM_COMPANY_TAB_DEAL"] = "Negociación";
$MESS["CRM_COMPANY_COPY_PAGE_TITLE"] = "Copiar compañía";
$MESS["CRM_COMPANY_DUP_CTRL_EMAIL_SUMMARY_TITLE"] = "por e-mail";
$MESS["CRM_COMPANY_DUP_CTRL_PHONE_SUMMARY_TITLE"] = "por teléfono";
$MESS["CRM_COMPANY_DUP_CTRL_TTL_SUMMARY_TITLE"] = "por nombre";
$MESS["CRM_COMPANY_FIELD_CONTACT"] = "Contacto";
$MESS["CRM_COMPANY_FIELD_CONTACT_LEGEND"] = "Contacto de la compañía";
$MESS["CRM_COMPANY_FIELD_REQUISITES"] = "Detalles";
$MESS["CRM_COMPANY_TAB_BIZPROC"] = "Flujo de trabajo";
$MESS["CRM_COMPANY_TAB_PORTRAIT"] = "Perfil";
$MESS["CRM_COMPANY_FIELD_ADDRESS"] = "Dirección";
$MESS["CRM_COMPANY_FIELD_ADDRESS_LEGAL"] = "Dirección legal";
$MESS["CRM_COMPANY_NOT_FOUND"] = "La compañía no fue encontrada.";
$MESS["CRM_COMPANY_FIELD_ID"] = "ID";
$MESS["CRM_COMPANY_TAB_TREE"] = "Dependencias";
?>