<?
$MESS["CRM_COMPANY_NOT_FOUND"] = "Société introuvable.";
$MESS["CRM_COMPANY_ACCESS_DENIED"] = "Accès refusé.";
$MESS["CRM_COMPANY_DELETION_ERROR"] = "Erreur lors de la suppression de la société.";
?>