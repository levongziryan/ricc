<?
$MESS["VI_AUTOPAY_TITLE"] = "Automatisch aufladen";
$MESS["VI_AUTOPAY_LABEL_2"] = "Telefonie-Guthaben automatisch aufladen, wenn es weniger als \$3.00 (3,00&euro;) aufweist oder einen niedrigeren Betrag hat, als es für Verlängerung der Nummernmiete erforderlich ist.";
?>