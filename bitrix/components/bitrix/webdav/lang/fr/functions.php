<?
$MESS["WD_CHANGE"] = "Date de modification";
$MESS["WD_DELETE"] = "Supprimer";
$MESS["WD_DELETE_TITLE"] = "Confirmer suppression";
$MESS["WD_CHANGE_ELEMENT"] = "Dition";
$MESS["WD_CHANGE_SECTION"] = "Modifier le Catalogue";
$MESS["WD_DELETE_ELEMENT"] = "Suppression d'un élément";
$MESS["WD_DELETE_SECTION"] = "Supprimer le Catalogue";
$MESS["WD_VIEW"] = "Caractéristiques et description";
$MESS["WD_VIEW_ELEMENT"] = "Affichage";
$MESS["WD_LOCK_ELEMENT"] = "Bloquer le document pour la rédaction";
$MESS["WD_LOCK"] = "verrouillé par vous (ne pas oublier de déverrouiller)";
$MESS["WD_UNLOCK_ELEMENT"] = "Terminer l'édition du document";
$MESS["WD_UNLOCK"] = "Débloquer";
$MESS["WD_START_BP"] = "Création d'un nouveau Processus d'Affaires";
$MESS["WD_START_BP_TITLE"] = "Exécuter un nouveau processus d'affaires";
$MESS["WD_HIST_ELEMENT"] = "Histoire de la compagnie";
$MESS["WD_HIST_ELEMENT_ALT"] = "Historique des modifications";
$MESS["IBLOCK_YELLOW_ALT"] = "bloquée par vous (n'oubliez pas de 'libérer')";
$MESS["IBLOCK_RED_ALT"] = "bloquée temporairement (en édition à présent)";
$MESS["WD_DELETE_CONFIRM"] = "Supprimer le document '#NAME# 'dans la corbeille?";
$MESS["WD_DESTROY_CONFIRM"] = "tes-vous sûr de supprimer le document '#NAME#' sans possibilité de récupération?";
$MESS["WD_DELETE_SECTION_CONFIRM"] = "Supprimez le dossier '#NAME# 'dans la corbeille?";
$MESS["WD_DESTROY_SECTION_CONFIRM"] = "tes-vous sûr de supprimer le dossier '#NAME#' sans possibilité de sa reconstitution?";
$MESS["WD_DOWNLOAD"] = "Télécharger";
$MESS["WD_DOWNLOAD_ELEMENT"] = "Télécharger";
$MESS["WD_PULL"] = "Editer";
$MESS["WD_PULL_ELEMENT"] = "Télécharger le document pour l'édition";
$MESS["WD_UPLOAD"] = "Charger la nouvelle copie";
$MESS["WD_UPLOAD_ELEMENT"] = "Charger la nouvelle copie";
$MESS["WD_CREATE_VERSION"] = "Créer une Version";
$MESS["WD_CREATE_VERSION_ALT"] = "Créer la version du document";
$MESS["WD_EDIT_MSOFFICE"] = "Editer le document dans l'éditeur de bureau";
$MESS["WD_EDIT_MSOFFICE_MENU"] = "Editer dans MS Office";
$MESS["WD_VERSIONS"] = "De versions";
$MESS["WD_VERSIONS_ALT"] = "Versions du document";
$MESS["WD_COMMENTS_FOR_DOCUMENT"] = "Commentaires au document:";
$MESS["IBLIST_A_BP_H"] = "les processus d'affaires";
$MESS["IBLIST_BP"] = "De processus business";
$MESS["WD_BP_R_P"] = "De processus business";
$MESS["WD_BP_R_P_TITLE"] = "Accéder au procédures d'entreprise du document";
$MESS["WD_TASKS"] = "Tâches";
$MESS["WD_TASKS_TITLE"] = "Accéder aux objectifs";
$MESS["WD_Y"] = "Oui";
$MESS["WD_N"] = "Non";
$MESS["WD_COPY_LINK_TITLE"] = "Copier le lien vers le document";
$MESS["WD_COPY_LINK"] = "Copiez le lien...";
$MESS["WD_OPEN"] = "Aller";
$MESS["WD_OPEN_SECTION"] = "Accéder au Dossier";
$MESS["WD_OPEN_DOCUMENT"] = "Ouvrir le document";
$MESS["IBLOCK_YELLOW_MSG"] = "<nobr>édité(e) par vous</nobr>";
$MESS["IBLOCK_RED_MSG"] = "<nobr>est en cours d'édition par #NAME# </nobr>";
$MESS["IBLOCK_RED_MSG_OTHER"] = "<nobr>bloqué(e)</nobr>";
$MESS["WD_DESCRIPTION"] = "Description";
$MESS["WD_TAGS"] = "Tags";
$MESS["WD_PROPERTIES"] = "Propriétés";
$MESS["WD_ELEMENT_COMMENT_NAME"] = "Discussion du document";
$MESS["WD_ELEMENT_COMMENT_TITLE"] = "Discussion";
$MESS["WD_RENAME_NAME"] = "Renommer...";
$MESS["WD_RENAME_TITLE"] = "Renommer le document";
$MESS["WD_COPY_NAME"] = "Copier...";
$MESS["WD_COPY_TITLE"] = "Copier le document";
$MESS["WD_MOVE_NAME"] = "Transférer...";
$MESS["WD_MOVE_TITLE"] = "Déplacer le Document";
$MESS["WD_UPLOAD_DONE"] = "Le téléchargement du document est terminé avec succès.";
$MESS["WD_COPY_LINK_HELP"] = "Copiez le lien dans le Presse-papiers.";
$MESS["WD_UNDELETE_ELEMENT"] = "Récupérer le document";
$MESS["WD_UNDELETE_SECTION"] = "Restituer le dossier";
$MESS["WD_UNDELETE"] = "Restaurer";
$MESS["WD_NAME_VERSION"] = "(version)";
$MESS["WD_RENAME_SECTION_TITLE"] = "Renommer un catalogue";
$MESS["WD_COPY_SECTION_TITLE"] = "Copier le catalogue";
$MESS["WD_MOVE_SECTION_TITLE"] = "Déplacer le répertoire";
$MESS["WD_PERMISSIONS"] = "Droits d'accès";
$MESS["WD_ELEMENT_PERMISSIONS"] = "Gérer l'import / export";
$MESS["WD_SECTION_PERMISSIONS"] = "Gérer les dossiers";
$MESS["WD_COPY_EXT_LINK_TITLE"] = "Ouvrir l'accès commun";
$MESS["WD_COPY_EXT_LINK"] = "Ouvrir l'accès commun";
$MESS["WD_PREVIEW_ELEMENT_TITLE"] = "Affichage préalable du journal";
$MESS["WD_PREVIEW_ELEMENT"] = "Affichage préalable du journal";
$MESS["WD_SHARE_NAME"] = "Faire commune";
$MESS["WD_SHARE_TITLE"] = "Faire commune";
$MESS["WD_MANAGE_SHARE_NAME"] = "Paramètres du dossier commun";
$MESS["WD_MANAGE_SHARE_TITLE"] = "Paramètres du dossier commun";
$MESS["WD_UNSHARE_SECTION"] = "Désactiver le dossier";
$MESS["WD_UNSHARE"] = "Supprimer";
$MESS["WD_UNSHARE_TITLE"] = "Confirmation de suppression";
$MESS["WD_UNSHARE_SECTION_CONFIRM"] = "Est-ce qu'il faut supprimer le dossier '#NAME#' sans possibilité de sa reconstitution?";
$MESS["WD_MAKE_SHARE_SECTION"] = "Faire commune";
$MESS["WD_ALREADY_SHARE_SECTION"] = "Dossier partagé";
$MESS["WD_DELETE_OWN_SHARE_SECTION_TITLE"] = "Confirmez la suppression du dossier commun";
$MESS["WD_DELETE_OWN_SHARE_SECTION_CONFIRM"] = "La suppression du dossier partagé conduira à la suppression des copies du dossier et de ses fichiers chez tous les utilisateurs qui ont l'accès partagé à ce dossier.";
$MESS["WD_SHARE_SECTION_CONNECT_NAME"] = "Brancher à Mon Drive";
$MESS["WD_SHARE_SECTION_CONNECT_TITLE"] = "Brancher à Mon Drive";
$MESS["WD_SHARE_SECTION_CONNECT_IN_GRID"] = "Conncter";
$MESS["WD_DESTROY_ELEMENT_CHECKBOX"] = "Directement, sans passer à la corbeille";
$MESS["WD_DESTROY_SECTION_CHECKBOX"] = "Directement, sans passer à la corbeille";
$MESS["WD_TRASH_DELETE_DESTROY_ELEMENT_CONFIRM"] = "Voulez-vous déplacer le dossier '#NAME#' dans la corbeille, ou le supprimer définitivement?";
$MESS["WD_TRASH_DELETE_DESTROY_SECTION_CONFIRM"] = "Voulez-vous déplacer '#NAME#' dans la corbeille, ou le supprimer définitivement?";
$MESS["WD_TRASH_DESTROY_BUTTON"] = "Supprimer définitivement";
$MESS["WD_TRASH_DELETE_BUTTON"] = "Déplacer dans corbeille";
$MESS["WD_TRASH_CANCEL_DELETE_BUTTON"] = "Annuler";
$MESS["WD_SHARE_NAME_2"] = "Accès commun";
$MESS["WD_SHARE_TITLE_2"] = "Accès commun";
?>