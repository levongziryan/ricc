<?
$MESS["WD_ERROR_HEADER"] = "<b> Attention!</ b> Le composant ne pourra pas fonctionner correctement selon le protocole WebDAV.";
$MESS["WD_ACCESS_DENIED"] = "Accès interdit.";
$MESS["WD_ERROR_2"] = "Le composant fonctionne sans support CNC. Solution: dans les paramètres du composant activer le support CNC.";
$MESS["WD_MODULE_IS_NOT_INSTALLED"] = "Le module de la bibliothèque des documents n'a pas été installé.";
$MESS["IB_MODULE_IS_NOT_INSTALLED"] = "Le module blocs d'information n'a pas été installé";
$MESS["WD_ERROR_1"] = "Le module mod_rewrite n'a pas été téléchargé. Solution: Veuillez apporter des modifications à la configuration du serveur Web Apache pour le chargement du module.";
$MESS["WD_IBLOCK_ID_EMPTY"] = "Bloc d'information non spécifié.";
$MESS["WD_FOLDER_EMPTY"] = "Le chemin vers le dossier n'est pas indiqué.";
?>