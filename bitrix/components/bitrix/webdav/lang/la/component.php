<?
$MESS["WD_MODULE_IS_NOT_INSTALLED"] = "El módulo WebDav no está instalado.";
$MESS["IB_MODULE_IS_NOT_INSTALLED"] = "El módulo Information blocks no está instalado.";
$MESS["WD_IBLOCK_ID_EMPTY"] = "El block de información no está especificado.";
$MESS["WD_FOLDER_EMPTY"] = "No se especifica la ruta de la carpeta.";
$MESS["WD_ERROR_1"] = "El módulo mod_rewrite no está cargado. Solución: Cambier la configuración del Apache.";
$MESS["WD_ERROR_2"] = "El componente trabaja en el modo non-SEF. Solución: habilitar soporte SEF en las configuraciones del componente.";
$MESS["WD_ERROR_HEADER"] = "<b>Cuidado!</b> El componente será habilitado para usar el WebDAV apropiadamente.";
$MESS["WD_ACCESS_DENIED"] = "Acceso denegado.";
?>