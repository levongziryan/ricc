<?
$MESS["NOTE_2"] = "Você não pode mapear uma unidade de rede, se as alterações em um documento do Microsoft Office não são salvos, consulte a seção de ajuda sobre como configurar <a href=\"#HREF##troubles\"> MS Windows </a> configurações.";
$MESS["NOTE_3"] = "Se você precisa aumentar o tamanho máximo de upload de arquivos, clique em <a href=\"#HREF##maxfilesize\"> aqui </a>.";
$MESS["WD_NEXT_ADVICE"] = "Próxima Dica ";
$MESS["WD_PREV_ADVICE"] = "Dica anterior";
$MESS["WD_BANNER_CLOSE"] = "Não mostrar dicas";
$MESS["WD_HELP_TEXT"] = "Para lidar com documentos por unidade de rede usar este nome:#BASE_URL#.Veja <a seção de ajuda href=\"#HELP_URL#\"> </a> para mais detalhes.";
$MESS["WD_ANCHOR_TITLE"] = "Caminho:";
$MESS["WD_COLLAPSE_FIELDS"] = "Mostrar mais propriedades";
$MESS["WD_TITLE_1"] = "Histórico de alterações de arquivo";
$MESS["WD_GO_BACK_ALT"] = "Voltar para documentos de biblioteca";
$MESS["WD_CANCEL"] = "Cancelar";
$MESS["WD_VERSIONS_MOD_2_4"] = "versões";
?>