<?
$MESS["WD_COMMENTS_NAME"] = "Discussão";
$MESS["WD_COMMENTS_TITLE"] = "Documento de discussão";
$MESS["WD_TEMPLATE_MESSAGE"] = "Novo documento na biblioteca de documentos: [url=#LINK#]#TITLE#[/url]. ";
$MESS["WD_COMMENTS_NAME_JS"] = "Discussão";
$MESS["WD_ERROR_BAD_SESSID"] = "Sua sessão expirou. Por favor, repita a operação.";
?>