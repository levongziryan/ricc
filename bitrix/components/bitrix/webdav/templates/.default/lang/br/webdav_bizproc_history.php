<?
$MESS["WD_ACCESS_DENIED"] = "Acesso negado.";
$MESS["WD_VERSIONS"] = "Versões";
$MESS["WD_VERSIONS_ALT"] = "Abrir versões do document";
$MESS["WD_ORIGINAL"] = "Original";
$MESS["WD_EDIT_FILE"] = "Editar";
$MESS["WD_EDIT_FILE_ALT"] = "Editar documento";
$MESS["WD_EDIT_FILE_ALT2"] = "Editar versão do documento";
$MESS["WD_HISTORY_DOCUMENT_TITLE"] = "História do Documento Para #NAME#";
$MESS["WD_HISTORY_VERSION_TITLE"] = "Histórico de versão para #NAME#";
$MESS["WD_DELETE_TITLE"] = "Confirmar a exclusão";
$MESS["WD_IB_GROUP_IS_NOT_FOUND"] = "O bloco de informação de documentos do grupo de trabalho  não foi encontrado.";
$MESS["WD_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["WD_UPLOAD_VERSION_TITLE"] = "Substituir o documento para \"#NAME#\"";
$MESS["WD_ERROR_EMPTY_DATA"] = "Nenhum arquivo ou pasta selecionada.";
?>