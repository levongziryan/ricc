<?
$MESS["WD_BP"] = "Business Process";
$MESS["WD_TASK"] = "Tarefas";
$MESS["WD_BUTTON_1"] = "Configurações";
$MESS["WD_BUTTON_1_ALT"] = "Edite os parâmetros de biblioteca";
$MESS["WD_BUTTON_2"] = "Acesso";
$MESS["WD_BUTTON_2_ALT"] = "Editar acesso à biblioteca";
$MESS["WD_MODULE_IS_NOT_INSTALLED"] = "Módulo WebDav não está instalado.";
$MESS["WD_FILE_ORIGINAL"] = "Original";
$MESS["WD_HOW_TO_INCREASE_QUOTA"] = "href=\"#HREF##maxfilesize\"> <a Como aumentar o tamanho máximo de arquivos carregados? </a>";
?>