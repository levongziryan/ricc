<?
$MESS["WD_TEMPLATE_MESSAGE"] = "Um novo arquivo na biblioteca de documentos: [url = # LINK #] # TITLE # [/ url]. ";
$MESS["WD_NOTE_EL"] = "O arquivo não foi publicado. Você não pode postar comentários para arquivos não publicados.";
$MESS["WD_VERSIONS"] = "Versões";
$MESS["WD_EV_TITLE"] = "Versões de documentos";
$MESS["WD_NO_BP_TEMPLATES_ADMIN"] = "hreg=\"#LINK#\"> <a Ver todos os processos de negócios </a>";
$MESS["WD_DROP_SECTION"] = "Excluir a pasta #NAME#";
$MESS["WD_ACCESS_DENIED"] = "Acesso negado.";
$MESS["WD_SUBSCRIBE_ELEMENT"] = "Acompanhar os comentários sobre documentos";
?>