<?
$MESS["NOTE_2"] = "Si usted no puede mapear la unidad de la red, o si sus cambios en un documento MS Office no se ha guardado, por favor vea la sección de ayuda en cómo configurar <a href=\"#HREF##troubles\">MS Windows</a> configuraciones.";
$MESS["NOTE_3"] = "Si usted necesita incrementar al máximo el tamaño del archivo cargado, por favor oprima <a href=\"#HREF##maxfilesize\">aquí</a>.";
$MESS["WD_NEXT_ADVICE"] = "Siguiente consejo";
$MESS["WD_PREV_ADVICE"] = "Consejo anterior";
$MESS["WD_BANNER_CLOSE"] = "No mostrar consejos";
$MESS["WD_HELP_TEXT"] = "Para manejar los documentos vía la unidad de la red usar este nombre: #BASE_URL#. See <a href=\"#HELP_URL#\">help section</a> para detalles.";
$MESS["WD_ANCHOR_TITLE"] = "Ruta:";
?>