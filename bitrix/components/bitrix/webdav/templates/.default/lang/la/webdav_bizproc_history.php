<?
$MESS["WD_ACCESS_DENIED"] = "Acceso denegado.";
$MESS["WD_VERSIONS"] = "Versiones";
$MESS["WD_VERSIONS_ALT"] = "Abrir versiones del documento";
$MESS["WD_ORIGINAL"] = "Original";
$MESS["WD_EDIT_FILE"] = "Editar";
$MESS["WD_EDIT_FILE_ALT"] = "Editar documento";
$MESS["WD_EDIT_FILE_ALT2"] = "Editar version del documento";
$MESS["WD_HISTORY_DOCUMENT_TITLE"] = "Historial del documento para #NAME#";
$MESS["WD_HISTORY_VERSION_TITLE"] = "Historial del documento para #NAME#";
?>