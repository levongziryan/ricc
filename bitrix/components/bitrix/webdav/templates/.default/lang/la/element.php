<?
$MESS["WD_TEMPLATE_MESSAGE"] = "Un nuevo archivo en la biblioteca del documento: [url=#LINK#]#TITLE#[/url].

#BODY#";
$MESS["WD_NOTE_EL"] = "El archivo no ha sido publicado. Usted no puede poner comentarios para archivos sin publicar.";
$MESS["WD_VERSIONS"] = "Versiones";
$MESS["WD_EV_TITLE"] = "Versiones del Documento";
?>