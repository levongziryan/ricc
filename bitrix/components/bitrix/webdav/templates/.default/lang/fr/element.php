<?
$MESS["WD_VERSIONS"] = "De versions";
$MESS["WD_EV_TITLE"] = "Versions du document";
$MESS["WD_NOTE_EL"] = "Le document n'est pas publié. Il est impossible de commenter un document qui n'est pas publié.";
$MESS["WD_TEMPLATE_MESSAGE"] = "Un nouveau document dans la bibliothèque des documents: [url=#LINK#]#TITLE#[/url].

#BODY#";
?>