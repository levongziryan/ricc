<?
$MESS["WD_HELP_TEXT"] = "Pour travailler avec les documents ainsi qu'avec le disque réseau, utilisez le nom: #BASE_URL#. Plus d'information dans la section <a href='#HELP_URL#'>d'aide</a>.";
$MESS["NOTE_3"] = "Si vous voulez augmenter la taille maximale du fichier téléchargé, il vous faut <a href='#HREF##maxfilesize'>ici</a>.";
$MESS["NOTE_2"] = "Si vous ne réussissez pas à connecter le dossier web ou l'enregistrement des modifications ne se fait pas lors de l'édition des documents dans MS Word, vous devriez prêter attention à la section d'aide consacrée au réglage <a href='#HREF##troubles'> du système d'exploitation Windows</a>.";
$MESS["WD_BANNER_CLOSE"] = "Fermer des invites";
$MESS["WD_PREV_ADVICE"] = "Conseil précédant";
$MESS["WD_ANCHOR_TITLE"] = "Chemin vers le disque réseau";
$MESS["WD_NEXT_ADVICE"] = "Conseil suivant";
?>