<?
$MESS["WD_DOCUMENT"] = "Document";
$MESS["WD_WF_COMMENTS1"] = "il y a des version plus récentes";
$MESS["WD_FILE_MODIFIED"] = "Modifié";
$MESS["WD_EDIT_FILE"] = "Date de modification";
$MESS["WD_HISTORY_FILE"] = "Histoire de la compagnie";
$MESS["WD_FILE_COMMENTS"] = "Commentaire";
$MESS["WD_DESCRIPTION"] = "Description";
$MESS["WD_FILE_ORIGINAL"] = "Original";
$MESS["WD_WF_PARAMS"] = "Paramètres de la gestion des documents";
$MESS["WD_VERSIONS_ALT"] = "Accéder à la liste des version du document #NAME#";
$MESS["WD_EDIT_FILE_ALT"] = "Accéder à la page de la modification du document";
$MESS["WD_HISTORY_FILE_ALT"] = "Retour à la page d'historique de versions du document";
$MESS["WD_WF_COMMENTS2"] = "voir l'original";
$MESS["WD_VIEW_FILE"] = "Affichage";
$MESS["WD_UNLOCK_FILE"] = "débloquer";
$MESS["WD_FILE_SIZE"] = "Rempli";
$MESS["WD_DOCUMENT_ALT"] = "Propriétés du document";
$MESS["WD_DOWNLOAD_FILE"] = "télécharger";
$MESS["WD_FILE_CREATED"] = "Créé";
$MESS["WD_VERSIONS"] = "De versions";
$MESS["WD_FILE_STATUS"] = "Statut";
$MESS["WD_TAGS"] = "Tags";
$MESS["WD_DELETE_FILE"] = "Supprimer";
$MESS["WD_DELETE_FILE_ALT"] = "Supprimer le document";
$MESS["WD_FILE"] = "Fichier";
?>