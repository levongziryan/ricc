<?
$MESS["WD_TAGS"] = "Etiquetas";
$MESS["WD_FILE_SIZE"] = "Tamaño";
$MESS["WD_DESCRIPTION"] = "Descripción";
$MESS["WD_FILE_CREATED"] = "Creado";
$MESS["WD_FILE_MODIFIED"] = "Modificado";
$MESS["WD_FILE_STATUS"] = "Estado";
$MESS["WD_FILE_COMMENTS"] = "Comentarios";
$MESS["WD_FILE_ORIGINAL"] = "Original";
$MESS["WD_FILE"] = "Archivo";
$MESS["WD_VIEW_FILE"] = "Ver";
$MESS["WD_DOWNLOAD_FILE"] = "descargo";
$MESS["WD_EDIT_FILE"] = "Modificar";
$MESS["WD_DELETE_FILE"] = "Borrar";
$MESS["WD_UNLOCK_FILE"] = "desbloquear";
$MESS["WD_HISTORY_FILE"] = "Historial";
$MESS["WD_WF_PARAMS"] = "Parámetros del flujo de trabajo";
$MESS["WD_WF_COMMENTS1"] = "existen versiones posteriores";
$MESS["WD_WF_COMMENTS2"] = "ver el original";
$MESS["WD_EDIT_FILE_ALT"] = "Editar el documento";
$MESS["WD_DELETE_FILE_ALT"] = "Borrar el documento";
$MESS["WD_HISTORY_FILE_ALT"] = "Ver las versiones del documento";
$MESS["WD_DOCUMENT"] = "Documento";
$MESS["WD_DOCUMENT_ALT"] = "Propiedades del documento";
$MESS["WD_VERSIONS"] = "Versiones";
$MESS["WD_VERSIONS_ALT"] = "Abrir versiones de #NAME#";
?>