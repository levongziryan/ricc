<?
$MESS["BPADH_AUTHOR"] = "Autor";
$MESS["BPADH_MODIFIED"] = "Modificado";
$MESS["BPADH_NAME"] = "Nome";
$MESS["BPADH_ALL"] = "Total";
$MESS["BPADH_SIZE"] = "Tamanho";
$MESS["BPADH_TAGS"] = "Tags";
$MESS["BPADH_DESCRIPTION"] = "Descrição";
$MESS["BPABL_STATUS_3"] = "Sendo Cancelado";
$MESS["W_IBLOCK_IS_NOT_INSTALLED"] = "Módulo de blocos de informações não está instalado.";
$MESS["WD_DOCUMENTS"] = "Documentos";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "O módulo WebDav não está instalado";
$MESS["WD_ERROR_BAD_ACTION"] = "A ação da pasta não foi especificado.";
?>