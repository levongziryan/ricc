<?
$MESS["pub_struct_modified"] = "Modificado:";
$MESS["pub_struct_sections"] = "Seções";
$MESS["pub_struct_close"] = "Fechar";
$MESS["pub_struct_folder_confirm_copy"] = "Pasta de cópia \"#FROM#\" para a pasta \"#TO#\"?";
$MESS["pub_struct_file_confirm_copy"] = "Copie o arquivo \"#FROM#\" para a pasta \"#TO#\"?";
$MESS["pub_struct_folder_confirm_move"] = "Mover pasta \"#FROM\" para a pasta \"#TO#\"?";
$MESS["pub_struct_file_confirm_move"] = "Mover o arquivo \"#FROM#\" pasta \"#TO#\"?";
$MESS["pub_struct_desc_copy"] = "Copiar \"#NAME#\" para a pasta";
$MESS["pub_struct_desc_move"] = "Mover \"#NAME\" para a pasta";
$MESS["pub_struct_settings_title"] = "Menu de Configurações";
$MESS["pub_struct_settings"] = "Configurações";
$MESS["pub_struct_folders_title"] = "Mostrar pastas na janela individual";
$MESS["pub_struct_folders_button"] = "Seções";
$MESS["pub_struct_loading"] = "Carregando ...";
$MESS["pub_struct_folder_del_confirm"] = "Todos os arquivos desta seção serão eliminado de forma irreversível. Tem certeza de que deseja apagar esta seção?";
$MESS["pub_struct_add_page"] = "Adicionar Página";
$MESS["pub_struct_add_page_title"] = "Criar uma nova página nesta seção";
$MESS["pub_struct_add_sect"] = "Adicionar Subseção";
$MESS["pub_struct_add_sect_title"] = "Criar uma nova seção nesta seção";
$MESS["pub_struct_folder_prop"] = "Propriedades da seção";
$MESS["pub_struct_folder_prop_title"] = "Editar título da seção, palavras-chave e propriedades";
$MESS["pub_struct_folder_access"] = "Acesso a pasta";
$MESS["pub_struct_folder_access_title"] = "Definir as permissões de acesso para seção";
$MESS["pub_struct_folder_del"] = "Excluir Seção";
$MESS["pub_struct_folder_del_title"] = "Apagar esta seção";
$MESS["pub_struct_cp"] = "Painel de controle";
$MESS["pub_struct_cp_title"] = "Abra o Painel de Controle";
$MESS["pub_struct_file_edit"] = "Editar página";
$MESS["pub_struct_file_edit_title"] = "Editar página no editor visual";
$MESS["pub_struct_file_edit_html"] = "Como editar HTML";
$MESS["pub_struct_file_edit_html_title"] = "Página Editar HTML código";
$MESS["pub_struct_file_prop"] = "Título da página e propriedades";
$MESS["pub_struct_file_prop_title"] = "Editar título da página, palavras-chave e propriedades";
$MESS["pub_struct_file_access"] = "Acesso página";
?>