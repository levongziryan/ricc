<?
$MESS["WD_DROP_CONFIRM"] = "Tem certeza que deseja deletar a pasta #NAME# irreversivelmete";
$MESS["WD_CANCEL"] = "Cancelar";
$MESS["WD_ADD_SECTION"] = "Criar Pasta";
$MESS["WD_DROP_SECTION"] = "Escluir a pasta #NAME#";
$MESS["WD_DROP"] = "Excluir";
$MESS["WD_EDIT_SECTION"] = "Editar a pásta #NAME#";
$MESS["WD_NAME"] = "Nome";
$MESS["WD_SAVE"] = "Salvar";
$MESS["WD_BP"] = "Business Process";
$MESS["WD_ERROR_ELEMENT_NOT_FOUND"] = "Elemento não encontrado.";
$MESS["T_IBLOCK_DESC_NEWS_PANEL"] = "Adicionar botões para controlar Toolbar Painel";
$MESS["WD_N"] = "Não";
?>