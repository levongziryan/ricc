<?
$MESS["WD_MOVE_CONFIRM"] = "Este documento ou pasta já existe. Você quer substituí-lo?";
$MESS["pub_struct_title"] = "Estrutura da Biblioteca de Documentos";
$MESS["pub_struct_folder"] = "Pasta";
$MESS["pub_struct_file"] = "Arquivo";
$MESS["pub_struct_name"] = "Título:";
$MESS["pub_struct_size"] = "Tamanho:";
$MESS["pub_struct_byte"] = "Byte (s)";
?>