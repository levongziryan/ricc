<?
$MESS["BPATL_SUCCESS"] = "A atribuição foi concluída.";
$MESS["BPAT_COMMENT"] = "Comentários";
$MESS["BPATL_DOC_HISTORY"] = "Histórico de Alterações do documento";
$MESS["BPAT_GOTO_DOC"] = "Open Document";
$MESS["WD_UPLOAD_MAX_FILE"] = "Max. Contagem de arquivos por upload (recommended: #upload_max_file#)";
$MESS["WD_USER_NOT_FOUND"] = "O usuário não foi encontrado.";
$MESS["WD_DROP_SECTION"] = "Excluir a pasta #NAME#";
?>