<?
$MESS["WD_PULL_HELP"] = "<b>Vous avez téléchargé cet élément pour la modification et avez bloqué la possibilité de sa modification par d'autres utilisateurs.<br> Après avoir terminé l'édition du document chargez sa version rafraîchissante  sur cette page.<br> Après avoir cliqué sur un bouton  &laquo;Sauvegarder&raquo; le document sera débloqué.</b>";
$MESS["WD_FILE_CREATED"] = "Créé";
$MESS["IBEL_E_TAB_BIZPROC"] = "De processus business";
$MESS["WD_DOCUMENT_BP"] = "Processus d'affaires";
$MESS["WD_VERSION"] = "Version";
$MESS["WD_CONTENT"] = "Niveau supérieur";
$MESS["IBEL_BIZPROC_RUN_CMD"] = "Exécuter l'ordre";
$MESS["WD_Y"] = "Y";
$MESS["WD_WF"] = "Flux de documents";
$MESS["WD_HISTORY"] = "Histoire de la compagnie";
$MESS["IBEL_BIZPROC_TASKS"] = "Tâches d'après le processus business";
$MESS["WD_FILE_REPLACE"] = "Remplacer le fichier";
$MESS["WD_FILE_MODIFIED"] = "Modifié";
$MESS["WD_DOCUMENT"] = "Document";
$MESS["WD_HISTORY_FILE"] = "Histoire de la compagnie";
$MESS["WD_FILE_COMMENTS"] = "Commentaire";
$MESS["WD_NAME"] = "Dénomination";
$MESS["IBEL_BIZPROC_RUN_CMD_NO"] = "Absent";
$MESS["WD_N"] = "Non";
$MESS["WD_DESCRIPTION"] = "Description";
$MESS["IBEL_E_PUBLISHED"] = "Document a été publié";
$MESS["WD_ORIGINAL"] = "Original";
$MESS["WD_VERSION_ALT"] = "Propriétés principales de la version du document";
$MESS["WD_DOCUMENT_ALT"] = "Propriétés du document";
$MESS["IBEL_BIZPROC_STOP"] = "Arrêter";
$MESS["WD_WF_PARAMS"] = "Paramètres de la gestion des documents";
$MESS["WD_VERSIONS_ALT"] = "Accéder à la liste des version du document #NAME#";
$MESS["WD_COLLAPSE_FIELDS"] = "Afficher des propriétés supplémentaires";
$MESS["WD_FILE_SIZE"] = "Rempli";
$MESS["WD_PARENT_SECTION"] = "Section de parent";
$MESS["WD_COLLAPSE_NAME"] = "Propriétés";
$MESS["WD_DOWNLOAD_FILE"] = "Télécharger";
$MESS["WD_DOCUMENT_BP_ALT2"] = "Consulter l'historique des processus d'affaires de la version du document";
$MESS["WD_DOCUMENT_BP_ALT"] = "Examiner l'historique des processus d'affaires du document";
$MESS["WD_HISTORY_FILE_ALT2"] = "Voir l'historique des modifications de la version de document";
$MESS["WD_HISTORY_FILE_ALT"] = "Retour à la page d'historique de versions du document";
$MESS["WD_VERSIONS"] = "De versions";
$MESS["WD_FILE_STATUS"] = "Statut";
$MESS["WD_TAGS"] = "Tags";
$MESS["IBEL_BIZPROC_STATE"] = "Statut courant";
$MESS["BPCGDOC_INVALID_TYPE"] = "Le type de paramètre n'a pas été défini.";
$MESS["WD_DELETE_FILE"] = "Supprimer";
$MESS["WD_DELETE_FILE_ALT2"] = "Supprimer la version";
$MESS["WD_DELETE_FILE_ALT"] = "Supprimer le document";
$MESS["WD_FILE"] = "Fichier";
?>