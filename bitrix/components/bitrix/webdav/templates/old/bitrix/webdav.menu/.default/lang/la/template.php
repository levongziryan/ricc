<?
$MESS["WD_SECTION_ADD"] = "Crear Carpeta";
$MESS["WD_SECTION_ADD_ALT"] = "Crea una carpeta anidada";
$MESS["WD_ELEMENT_ADD"] = "Crear";
$MESS["WD_ELEMENT_ADD_ALT"] = "Crear documento de MS Word";
$MESS["WD_UPLOAD"] = "Subir";
$MESS["WD_UPLOAD_ALT"] = "Carga nuevos archivos en esta carpeta";
$MESS["WD_UPLOAD_ROOT_ALT"] = "Carga nuevos archivos en la raíz de la biblioteca";
$MESS["WD_GO_BACK"] = "Documentos";
$MESS["WD_GO_BACK_ALT"] = "Volver a la lista de documentos";
$MESS["WD_ELEMENT_DELETE"] = "Eliminar";
$MESS["WD_ELEMENT_DELETE_ALT"] = "Eliminar Documentos";
$MESS["WD_ELEMENT_DELETE_CONFIRM"] = "¿Seguro que quiere eliminar el elemento? ¡Esta operación no se puede deshacer!";
$MESS["WD_ELEMENT_EDIT"] = "Editar";
$MESS["WD_ELEMENT_EDIT_ALT"] = "Abre el editor de propiedades del elemento";
$MESS["WD_HELP"] = "Ayuda";
$MESS["WD_HELP_ALT"] = "Muestra ayuda sobre el uso de la Biblioteca de Documentos";
$MESS["WD_MAPING"] = "Unidad del Drive";
$MESS["WD_MAPING_ALT"] = "Utiliza Windows Explorer para ver documentos.";
$MESS["WD_EMPTY_PATH"] = "La ruta de red no está especificada.";
$MESS["WD_PATH"] = "Ruta:";
$MESS["WD_HELP_TEXT"] = "<ul class='wd-content' style='color:#052635;font-family:Verdana; tamaño de letra:14px;'>
<li>Asegúrate de tener el <a target=\"_blank\" href='http://www.microsoft.com/downloads/details.aspx?FamilyId=43109663-3627-4949-BD43-F2247570B9EC&displaylang=en'>Carpetas Web</a> software instalado</li>
<li>Ejecutar Windows Explorer</li>
<li>Seleccionar <b>Servicios</b> > <b>Conectar al drive de red</b></li>
<li>Click<b>Regístrese para el almacenamiento en línea o conéctese a un servidor de red</b>; ejecutar el Asistente para agregar espacio de red. La pantalla inicial del asistente aparecerá.</li>
<li>Click <b>Siguiente</b>.</li>
<li>Seleccione la opción Elegir otra ubicación de red y haga clic<b>Siguiente</b>.</li>
<li>Escriba la URL de la carpeta web para conectarse (#BASE_URL#) en <b> Internet o dirección de red </b>.</li>
<li>Click <b>Siguiente</b> para abrir el cuadro de diálogo de autorización.</li>
<li>Ingrese el nombre de usuario y contraseña. Click <b>Ok</b>. El sistema autorizará, y el asistente abrirá el siguiente paso.</li>
<li>Aquí puede cambiar el nombre de la carpeta web a cualquier cadena deseada. Si es necesario, cambie el nombre del recurso de red a cualquier cosa de su elección. Click <b>Siguiente</b> para abrir el último paso del asistente.</li>
<li>El último paso del asistente le informa de la conexión exitosa y sugiere abrir la carpeta al cerrar la ventana del asistente. Click <b>Finalizar</b>.</li>
<li>El sistema conectará la carpeta y abrirá Windows Explorer mostrando la carpeta que acaba de conectar (si ha elegido).</li>
</ul>";
$MESS["WD_ERROR_1"] = "El documento no pudo ser creado.
La aplicación requerida puede no instalarse correctamente o la plantilla para esta biblioteca de documentos no se puede abrir.";
$MESS["WD_ERROR_2"] = "'Crear' requiere un Microsoft Internet Explorer 6.0 o superior. Para agregar un documento a esta biblioteca de documentos, haga clic en el botón 'Cargar'.";
$MESS["WD_SUBSCRIBE"] = "Suscribir";
$MESS["WD_SUBSCRIBE_TO_FORUM"] = "Suscríbete a comentarios en documentos";
$MESS["WD_UNSUBSCRIBE"] = "Darse de baja";
$MESS["WD_UNSUBSCRIBE_FROM_FORUM"] = "Darse de baja de los comentarios en los documentos";
$MESS["WD_SUBSCRIBE_DELETE_CONFIRM"] = "¿Desea darse de baja de los comentarios?";
$MESS["BPATT_HELP1_TEXT"] = "El proceso de negocios impulsado por el estado es un proceso de negocios continuo con distribución de permisos de acceso para manejar documentos en diferentes estados.";
$MESS["BPATT_HELP2_TEXT"] = "El proceso de negocios secuencial es un proceso de negocios simple para realizar una serie de acciones consecutivas en un documento.";
$MESS["BPATT_HELP1"] = "Proceso de negocios impulsado por el estado";
$MESS["BPATT_HELP2"] = "Proceso de negocios secuencial";
$MESS["WD_BP"] = "Proceso de negocios";
?>