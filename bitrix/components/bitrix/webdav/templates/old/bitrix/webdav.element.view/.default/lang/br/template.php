<?
$MESS["WD_EXT_LINKS_DIALOG_DELETE_COMENT"] = "Excluir comentário";
$MESS["WD_EXT_LINKS_DIALOG_DELETE_ALL_LINKS"] = "Eliminar todas as ligações";
$MESS["WD_EXT_LINKS_DIALOG_USER_NAME"] = "Usuário";
$MESS["WD_EXT_LINKS_DIALOG_CHANGE_TIME"] = "Modificada em";
$MESS["WD_EXT_LINKS_DEMO_DOCUMENT_DESCRIPTION"] = "Esta é a descrição de arquivo. Você pode adicionar uma na caixa de diálogo \"Compartilhar este arquivo\".";
$MESS["WD_EXT_LINKS_DIALOG_PASS_WRONG"] = "As senhas não coincidem.";
$MESS["WD_EXT_LINKS_DIALOG_PASS_EMPTY"] = "As senhas estão vazias.";
$MESS["WD_FILE_ERROR1"] = "O diretório físico não foi encontrado.";
$MESS["WD_FILE_ERROR2"] = "O caminho de destino está incorreto.";
$MESS["WD_FILE_ERROR3"] = "A fonte não foi encontrado.";
$MESS["WD_FILE_ERROR4"] = "O arquivo de destino ou pasta já existe.";
$MESS["WD_FILE_ERROR5"] = "Uma pasta #FOLDER# já existe.";
$MESS["WD_FILE_ERROR6"] = "O arquivo #FILE# não foi copiado";
$MESS["WD_FILE_ERROR7"] = "A pasta #FOLDER# falhou em ser criada";
$MESS["WD_FILE_ERROR8"] = "O arquivo #FILE# já existe";
$MESS["WD_FILE_ERROR9"] = "A pasta pai não foi encontrado.";
$MESS["WD_FILE_ERROR10"] = "A pasta não pode ser criado em um arquivo.";
$MESS["WD_FILE_ERROR11"] = "A pasta não pode ser criado porque um arquivo com o mesmo nome já existe.";
$MESS["WD_FILE_ERROR12"] = "A pasta não foi criada.";
$MESS["WD_FILE_ERROR13"] = "Extensão de arquivo inválido.";
$MESS["WD_FILE_ERROR14"] = "Nome de arquivo inválido.";
$MESS["WD_FILE_ERROR15"] = "A pasta \"Bitrix\" não pode ser usado como o destino.";
$MESS["WD_FILE_ERROR16"] = "Erro ao excluir arquivo ou pasta.";
?>