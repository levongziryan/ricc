<?
$MESS["IBLOCK_YELLOW_ALT"] = "usted lo ha bloqueado (recuerda desbloquear)";
$MESS["IBLOCK_RED_ALT"] = "bloqueado temporalmente (editado en este momento)";
$MESS["WD_DELETE_CONFIRM"] = "¿Seguro que quiere eliminar el elemento? ¡Esta operación no se puede deshacer!";
$MESS["WD_TAGS"] = "Etiquetas";
$MESS["WD_FILE_SIZE"] = "Tamaño";
$MESS["WD_DESCRIPTION"] = "Descripción";
$MESS["WD_FILE_CREATED"] = "Creado";
$MESS["WD_FILE_MODIFIED"] = "Modificado";
$MESS["WD_FILE_STATUS"] = "Estados";
$MESS["WD_FILE_COMMENTS"] = "Comentarios";
$MESS["WD_FILE_ORIGINAL"] = "Original";
$MESS["WD_FILE"] = "Archivo";
$MESS["WD_OPEN_FILE"] = "Abrir Archivo";
$MESS["WD_VIEW_FILE"] = "Ver";
$MESS["WD_DOWNLOAD_FILE"] = "descargar";
$MESS["WD_DOWNLOAD_FILE_TITLE"] = "descargar";
$MESS["WD_EDIT_FILE"] = "modificar";
$MESS["WD_DELETE_FILE"] = "eliminar";
$MESS["WD_UNLOCK_FILE"] = "desbloquear";
$MESS["WD_HISTORY_FILE"] = "historial";
$MESS["WD_WF_PARAMS"] = "Parámetros de Flujo de Trabajo";
$MESS["WD_WF_COMMENTS1"] = "versiones posteriores existen";
$MESS["WD_WF_COMMENTS2"] = "ver original";
?>