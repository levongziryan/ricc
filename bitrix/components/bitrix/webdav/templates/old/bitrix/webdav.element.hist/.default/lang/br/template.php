<?
$MESS["LOG_WEBDAV_SECTION_MOVE"] = "Pasta #NAME# movida para \"#MOVE_TO#\"";
$MESS["LOG_WEBDAV_ELEMENT_RENAME"] = "Arquivo #NAME# renomeado para #NAME#";
$MESS["LOG_WEBDAV_SECTION_RENAME"] = "Pasta #NAME# renomeada para #NAME#";
$MESS["LOG_WEBDAV_ELEMENT_UPDATE"] = "Arquivo #NAME# atualizado";
$MESS["WD_EXT_LINKS_DIALOG_TITLE"] = "Compartilhar este arquivo";
$MESS["WD_EXT_LINKS_DIALOG_CLOSE_BUTTON"] = "Fechar";
$MESS["WD_EXT_LINKS_DIALOG_LOADING"] = "Carregando ...";
$MESS["WD_EXT_LINKS_DIALOG_ERROR"] = "Erro ao criar o link pÃºblico!";
$MESS["WD_EXT_LINKS_DIALOG_GET"] = "Obter link pÃºblico";
$MESS["WD_EXT_LINKS_DIALOG_FILE_ACCESS_TIME_TITLE"] = "Use restrições de tempo";
$MESS["WD_EXT_LINKS_DIALOG_FILE_ACCESS_TIME_DAY"] = "Dias";
$MESS["WD_EXT_LINKS_DIALOG_FILE_ACCESS_TIME_HOUR"] = "Horas";
$MESS["WD_EXT_LINKS_DIALOG_FILE_ACCESS_TIME_MIN"] = "Minutos";
$MESS["WD_EXT_LINKS_DIALOG_FILE_ACCESS_PASS_TITLE"] = "Usar senha";
$MESS["WD_EXT_LINKS_DIALOG_FILE_ACCESS_PASS1"] = "Digite a senha";
$MESS["WD_EXT_LINKS_DIALOG_FILE_ACCESS_PASS2"] = "Confirmar senha";
$MESS["WD_EXT_LINKS_DIALOG_SPOILER_TITLE"] = "Há #n# links para este ficheiro:";
$MESS["WD_EXT_LINKS_DIALOG_TIME_LEFT"] = "O arquivo estará disponível até";
$MESS["WD_EXT_LINKS_DIALOG_PASSWORD"] = "Protegido por senha.";
$MESS["WD_EXT_LINKS_DIALOG_GREEN_WINDOW_TITLE"] = "O compartilhamento de arquivos nunca foi tão fácil!";
$MESS["WD_EXT_LINKS_DIALOG_GREEN_WINDOW_TEXT"] = "Enviar um link externo para o seu cliente, e eles estão apenas um clique de distância de seu arquivo.";
$MESS["WD_EXT_LINKS_DIALOG_GREEN_WINDOW_LINK"] = "O que parece pra você?";
$MESS["WD_EXT_LINKS_DIALOG_ADD_COMENT"] = "Adicionar comentário";
?>