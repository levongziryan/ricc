<?
$MESS["WD_FILE_NAME"] = "Archivo";
$MESS["WD_STATUS"] = "Estados";
$MESS["WD_MODIFIED_BY"] = "Modificado Por";
$MESS["WD_CHANGE_DATE"] = "Modificado";
$MESS["WD_DELETE"] = "Eliminar";
$MESS["WD_DELETE_CONFIRM"] = "¿Seguro que quiere eliminar el elemento? ¡Esta operación no se puede deshacer!";
$MESS["WD_CHANGE"] = "Editar";
$MESS["WD_CHANGE_ELEMENT"] = "Editar Elemento";
$MESS["WD_CURRENT_VERSION"] = "Versión actual:";
$MESS["WD_ARCHIVE"] = "Archivo:";
$MESS["WD_ACTIONS"] = "Acciones";
$MESS["WD_DELETE_ELEMENT"] = "Eliminar Elemento";
$MESS["WD_RESTORE"] = "Restaurar";
$MESS["WD_RESTORE_ELEMENT"] = "Restaurar";
$MESS["WD_DELETE_CONFIRM_FILE"] = "¿Desea eliminar el archivo irreversiblemente?";
$MESS["WD_DELETE_CONFIRMS"] = "¿Desea eliminar el archivo de forma irreversible?";
$MESS["WD_VIEW"] = "Ver";
$MESS["WD_VIEW_ELEMENT"] = "Ver Propiedades del Archivo";
$MESS["WD_DOWNLOAD"] = "Descargar";
$MESS["WD_DOWNLOAD_ELEMENT"] = "Descargar archivo";
$MESS["WD_COMMENTS"] = "Comentarios";
$MESS["WD_UNLOCK"] = "Desbloquear";
$MESS["WD_UNLOCK_ELEMENT"] = "Desbloquear Archivo";
?>