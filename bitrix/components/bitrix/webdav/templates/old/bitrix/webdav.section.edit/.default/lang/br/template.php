<?
$MESS["WD_FILE_ERROR9"] = "A pasta pai não foi encontrado.";
$MESS["WD_FILE_ERROR10"] = "A pasta não pode ser criado em um arquivo.";
$MESS["WD_FILE_ERROR11"] = "A pasta não pode ser criado porque um arquivo com o mesmo nome já existe.";
$MESS["WD_FILE_ERROR12"] = "A pasta não foi criada.";
$MESS["WD_FILE_ERROR14"] = "Nome de arquivo inválido.";
$MESS["WD_FILE_ERROR16"] = "Erro ao excluir arquivo ou pasta.";
$MESS["WD_FILE_ERROR100"] = "A pasta não pode ser movido para si.";
$MESS["WD_FILE_ERROR101"] = "Um arquivo ou nome da pasta contém caracteres inválidos.";
$MESS["WD_FILE_ERROR102"] = "A pasta não foi movida.";
$MESS["WD_FILE_ERROR103"] = "A pasta #FOLDER# não foi movida #TEXT_ERROR#";
$MESS["WD_FILE_ERROR104"] = "A pasta #FOLDER# não foi movida #TEXT_ERROR#";
$MESS["WD_FILE_ERROR105"] = "O documento não foi especificado.";
$MESS["WD_FILE_ERROR106"] = "A biblioteca não está no modo de processo de negócio.";
$MESS["WD_FILE_ERROR107"] = "O documento está bloqueado por um processo de negócio.";
$MESS["WD_FILE_ERROR108"] = "O documento pertence a outro proprietário.";
?>