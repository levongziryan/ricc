<?
$MESS["WD_CREATED"] = "Creado";
$MESS["WD_LAST_UPDATE"] = "Última actualización";
$MESS["WD_ACTIVE"] = "Registro activo";
$MESS["WD_PARENT_SECTION"] = "Nivel Principal";
$MESS["WD_CONTENT"] = "Nivel superior";
$MESS["WD_NAME"] = "Nombre";
$MESS["WD_SORT"] = "Índice de clasificación";
$MESS["WD_PICTURE"] = "Imagen";
$MESS["WD_DROP"] = "Soltar";
$MESS["WD_SAVE"] = "Guardar";
$MESS["WD_APPLY"] = "Aplicar";
$MESS["WD_CANCEL"] = "Cancelar";
$MESS["WD_YES"] = "Si";
$MESS["WD_NO"] = "No";
$MESS["WD_ACTIVE_FOLDER"] = "activo";
?>