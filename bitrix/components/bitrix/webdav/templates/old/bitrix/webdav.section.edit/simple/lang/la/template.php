<?
$MESS["WD_NAME"] = "Nombre";
$MESS["WD_DROP"] = "Soltar";
$MESS["WD_SAVE"] = "Guardar";
$MESS["WD_CANCEL"] = "Cancelar";
$MESS["WD_DROP_CONFIRM"] = "Are you sure you want to delete folder #NAME# irreversibly?";
$MESS["WD_DROP_SECTION"] = "Eliminar Carpeta #NAME#";
$MESS["WD_EDIT_SECTION"] = "Editar Carpeta #NAME#";
$MESS["WD_ADD_SECTION"] = "Crear Carpeta";
?>