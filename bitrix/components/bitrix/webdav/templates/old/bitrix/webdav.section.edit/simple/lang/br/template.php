<?
$MESS["WD_FILE_ERROR109"] = "Você não pode editar o documento neste estado.";
$MESS["WD_FILE_ERROR110"] = "Você não pode criar um documento neste estado.";
$MESS["WD_ROOT_SECTION_NOT_FOUND"] = "O diretório raiz não foi encontrado.";
$MESS["WD_FILE_ERROR_EMPTY_STATUSES"] = "Não há estados em que um documento pode ser carregado.";
$MESS["WD_ERR_DELETE_FOLDER_NO_PERMS"] = "Erro: Não permissões suficientes para excluir a pasta \"#NAME#\".";
$MESS["WD_TRASH"] = "Lixeira";
$MESS["WD_FILE_ERROR111"] = "Erro ao salvar o documento.";
$MESS["WD_OPTIONS_ALLOW_DOC_PREVIEW"] = "Permitir a visualização de documentos via Google Viewer";
?>