<?
$MESS["WEBDAV_SONET_EVENT_TITLE_FILE_24"] = "acrescentou um documento compartilhado:";
$MESS["SONET_FILES_UPDATE_LOG"] = "#AUTHOR_NAME# arquivo atualizado (s) #TITLE#";
$MESS["SONET_FILES_UPDATE_LOG_TEXT"] = "O arquivo #TITLE# em #URL# foi modificado.";
$MESS["WEBDAV_SONET_EVENT_TITLE_FILE_24_MOBILE"] = "Documento compartilhado";
$MESS["USER_TYPE_WEBDAV_FILE_DESCRIPTION"] = "Documento da Biblioteca";
$MESS["USER_TYPE_WEBDAV_FILE_IBLOCK_ID"] = "Documento bloco de informação Biblioteca";
$MESS["USER_TYPE_WEBDAV_FILE_SECTION_ID"] = "Pasta Biblioteca de Documentos";
$MESS["IBLOCK_VALUE_ANY"] = "Qualquer valor";
$MESS["WD_ERR_SECTION404"] = "A pasta não foi encontrado.";
$MESS["WD_ERR_IBLOCK404"] = "O bloco de informações não foi encontrado.";
$MESS["WD_ERR_SECTION403"] = "Permissão insuficiente para ler a pasta.";
$MESS["WD_ERR_MODULES"] = "Os módulos de Blocos WebDAV e informações não foram encontrados.";
$MESS["WD_ERR_PARSE_FILE"] = "Erro ao salvar o arquivo.";
$MESS["WD_ERR_FILE_EXISTS"] = "Um arquivo com este nome já existe.";
$MESS["WEBDAV_OPTIONS_FILETYPE_IMAGES"] = "Imagens";
?>