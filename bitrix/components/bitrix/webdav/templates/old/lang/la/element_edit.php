<?
$MESS["WD_NAME"] = "Nombre";
$MESS["WD_FILE"] = "Archivo";
$MESS["WD_SAVE"] = "Guardar";
$MESS["WD_APPLY"] = "Aplicar";
$MESS["WD_CANCEL"] = "Cancelar";
$MESS["WD_FILE_MODIFIED"] = "Modificar";
$MESS["WD_FILE_SIZE"] = "Tamaño";
$MESS["WD_OPEN_FILE"] = "Abrir Documento";
$MESS["WD_KB"] = "KB";
$MESS["WD_MB"] = "MB";
$MESS["WD_B"] = "byte";
$MESS["WD_ACCESS_DENIED"] = "Acceso denegado.";
$MESS["WD_DOWNLOAD_FILE"] = "Descargar";
$MESS["WD_FILE_NOT_FOUND"] = "Archivo no fue encontrado.";
$MESS["WD_TITLE"] = "Editar elemento";
?>