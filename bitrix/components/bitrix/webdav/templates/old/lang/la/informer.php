<?
$MESS["NOTE_2"] = "Si no puede asignar una unidad de red, o si sus cambios en un documento de MS Office no se guardan, consulte la sección de ayuda sobre cómo configurar <a href=\"#HREF##troubles\">MS Windows</a> configuración.";
$MESS["NOTE_3"] = "Si necesita aumentar el tamaño máximo de los archivos cargados, haga clic <a href=\"#HREF##maxfilesize\">aquí</a>.";
$MESS["WD_NEXT_ADVICE"] = "Siguiente Sugerencia";
$MESS["WD_PREV_ADVICE"] = "Sugerencia Anterior ";
$MESS["WD_BANNER_CLOSE"] = "No mostras sugerencias";
$MESS["WD_HELP_TEXT"] = "Para manejar documentos a través de una unidad de red use este nombre: #BASE_URL#. Ver <a href=\"#HELP_URL#\">sección de ayuda</a> para detalles.";
$MESS["WD_ANCHOR_TITLE"] = "Ruta:";
?>