<?
$MESS["CRM_CONFIG_PERMS_REBUILD_COMPANY_ATTRS"] = "Por favor <a id=\"#ID#\" href=\"#URL#\">atualização</a> atributos de acesso para as empresas existentes antes de utilizar as permissões de acesso estendidas.";
$MESS["CRM_CONFIG_PERMS_LRP_DLG_BTN_START"] = "Executar";
$MESS["CRM_CONFIG_PERMS_LRP_DLG_BTN_STOP"] = "Parar";
$MESS["CRM_CONFIG_PERMS_LRP_DLG_BTN_CLOSE"] = "Fechar";
$MESS["CRM_CONFIG_PERMS_LRP_DLG_WAIT"] = "Por favor espere...";
$MESS["CRM_CONFIG_PERMS_LRP_DLG_REQUEST_ERR"] = "Erro ao processar o pedido.";
$MESS["CRM_CONFIG_PERMS_REBUILD_COMPANY_ATTR_DLG_TITLE"] = "Atualize atributos de acesso da empresa";
$MESS["CRM_CONFIG_PERMS_REBUILD_COMPANY_ATTR_DLG_SUMMARY"] = "Atributos de acesso da empresa vai agora para ser recriada. Esta operação pode levar algum tempo.";
$MESS["CRM_CONFIG_PERMS_REBUILD_CONTACT_ATTRS"] = "As permissões de acesso atualizadas de contato exigem que você <a id=\"#ID#\"href=\"#URL#\">atualize</a>os atributos de acesso para contatos criados anteriormente.";
$MESS["CRM_CONFIG_PERMS_REBUILD_CONTACT_ATTR_DLG_TITLE"] = "Atualizar os Atributos de Acesso de Contato";
$MESS["CRM_CONFIG_PERMS_REBUILD_CONTACT_ATTR_DLG_SUMMARY"] = "Os atributos de acesso não serão refeitos para todos os contatos. Este procedimento pode demorar algum tempo.";
$MESS["CRM_CONFIG_PERMS_REBUILD_DEAL_ATTRS"] = "As permissões de acesso atualizadas de negociações exigem que você <a id=\" #ID#\"href=\" #URL#\">atualize</a>os atributos de acesso para negociações criadas anteriormente.";
$MESS["CRM_CONFIG_PERMS_REBUILD_DEAL_ATTR_DLG_TITLE"] = "Atualizar os Atributos de Acesso de Negociações";
$MESS["CRM_CONFIG_PERMS_REBUILD_DEAL_ATTR_DLG_SUMMARY"] = "Os atributos de acesso não serão refeitos para todas as negociações. Este procedimento pode demorar algum tempo.";
$MESS["CRM_CONFIG_PERMS_REBUILD_LEAD_ATTRS"] = "As permissões de acesso atualizadas de clientes potenciais exigem que você <a id=\" #ID#\"href=\" #URL#\">atualize</a>os atributos de acesso para clientes potenciais criados anteriormente.";
$MESS["CRM_CONFIG_PERMS_REBUILD_LEAD_ATTR_DLG_TITLE"] = "Atualizar os Atributos de Acesso de Liderança";
$MESS["CRM_CONFIG_PERMS_REBUILD_LEAD_ATTR_DLG_SUMMARY"] = "Os atributos de acesso não serão refeitos para todos os clientes potenciais. Este procedimento pode demorar algum tempo.";
$MESS["CRM_CONFIG_PERMS_REBUILD_QUOTE_ATTRS"] = "As permissões de acesso atualizadas de cotas exigem que você <a id=\"#ID#\"href=\"#URL#\">atualize</a>os atributos de acesso para as cotas criadas anteriormente.";
$MESS["CRM_CONFIG_PERMS_REBUILD_QUOTE_ATTR_DLG_TITLE"] = "Atualizar os Atributos de Acesso de Cotas";
$MESS["CRM_CONFIG_PERMS_REBUILD_QUOTE_ATTR_DLG_SUMMARY"] = "Os atributos de acesso não serão refeitos para todas as cotas. Este procedimento pode demorar algum tempo.";
$MESS["CRM_CONFIG_PERMS_REBUILD_INVOICE_ATTRS"] = "As permissões de acesso atualizadas de faturas exigem que você <a id=\"#ID#\"href=\"#URL#\">atualize</a>os atributos de acesso para as faturas criadas anteriormente.";
$MESS["CRM_CONFIG_PERMS_REBUILD_INVOICE_ATTR_DLG_TITLE"] = "Atualizar os Atributos de Acesso de Faturas";
$MESS["CRM_CONFIG_PERMS_REBUILD_INVOICE_ATTR_DLG_SUMMARY"] = "Os atributos de acesso não serão refeitos para todas as faturas. Este procedimento pode demorar algum tempo.";
?>