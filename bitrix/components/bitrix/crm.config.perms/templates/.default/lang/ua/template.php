<?
$MESS["CRM_PERMS_TYPE_ALL"] = "Всі";
$MESS["CRM_PERMS_ADD_LINK"] = "Додати";
$MESS["CRM_PERMS_TYPE_EDIT"] = "Зміни";
$MESS["CRM_PERMS_COMPANY"] = "Компанії";
$MESS["CRM_PERMS_CONTACT"] = "Контакти";
$MESS["CRM_PERMS_LEAD"] = "Ліди";
$MESS["CRM_PERMS_CONFIG"] = "Налаштування";
$MESS["CRM_PERMS_TYPE_NONE"] = "Немає";
$MESS["CRM_PERMS_BUTTONS_CANCEL"] = "Скасувати";
$MESS["CRM_PERMS_LIST_EMPTY"] = "Права доступу не задані";
$MESS["CRM_PERMS_TYPE_SELF"] = "Свої";
$MESS["CRM_PERMS_DEAL"] = "Угоди";
$MESS["CRM_PERMS_EVENT"] = "Події";
$MESS["CRM_PERMS_BUTTONS_SAVE"] = "Зберегти";
$MESS["CRM_PERMS_ALERT"] = "Після зміни прав доступу необхідно виконати повну переіндексацію";
?>