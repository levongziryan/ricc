<?
$MESS["CRM_CONFIG_PERMS_REBUILD_COMPANY_ATTR_DLG_SUMMARY"] = "Les attributs d'accès seront reconstruits pour toutes les entreprises. L'exécution de cette opération peut prendre un long temps.";
$MESS["CRM_CONFIG_PERMS_LRP_DLG_BTN_START"] = "Exécuter";
$MESS["CRM_CONFIG_PERMS_REBUILD_COMPANY_ATTRS"] = "Pour le fonctionnement correct des droits élargis d'accès aux entreprises il faut <a id='#ID#' href='#URL#'>mettre à jour</a> les attributs d'accès des entreprises créées avant.";
$MESS["CRM_CONFIG_PERMS_LRP_DLG_BTN_CLOSE"] = "Fermer";
$MESS["CRM_CONFIG_PERMS_REBUILD_COMPANY_ATTR_DLG_TITLE"] = "Mise à jour des attributs de l'accès pour les compagnies";
$MESS["CRM_CONFIG_PERMS_LRP_DLG_BTN_STOP"] = "Arrêter";
$MESS["CRM_CONFIG_PERMS_LRP_DLG_WAIT"] = "Attendez...";
$MESS["CRM_CONFIG_PERMS_LRP_DLG_REQUEST_ERR"] = "Erreur lors du traitement de la requête.";
$MESS["CRM_CONFIG_PERMS_REBUILD_CONTACT_ATTR_DLG_SUMMARY"] = "Attributs d'accès vont maintenant être reconstruits pour tous les contacts. Cette procédure peut prendre un certain temps.";
$MESS["CRM_CONFIG_PERMS_REBUILD_LEAD_ATTR_DLG_SUMMARY"] = "Attributs d'accès vont maintenant être reconstruits pour tous les conducteurs. Cette procédure peut prendre un certain temps.";
$MESS["CRM_CONFIG_PERMS_REBUILD_QUOTE_ATTR_DLG_SUMMARY"] = "Attributs d'accès vont maintenant être reconstruits pour toutes les citations. Cette procédure peut prendre un certain temps.";
$MESS["CRM_CONFIG_PERMS_REBUILD_DEAL_ATTR_DLG_SUMMARY"] = "Attributs d'accès vont maintenant être reconstruits pour toutes les offres. Cette procédure peut prendre un certain temps.";
$MESS["CRM_CONFIG_PERMS_REBUILD_INVOICE_ATTR_DLG_SUMMARY"] = "Attributs d'accès vont maintenant être reconstruits pour toutes les factures. Cette procédure peut prendre un certain temps.";
$MESS["CRM_CONFIG_PERMS_REBUILD_CONTACT_ATTRS"] = "Les autorisations d'accès de contact mises à jour exigent que vous <a id='#ID#' href='#URL#'>mis à jour</a> attributs d'accès pour les contacts créés précédemment.";
$MESS["CRM_CONFIG_PERMS_REBUILD_LEAD_ATTRS"] = "Les autorisations mises à jour d'accès plomb exigent que vous<a id='#ID#' href='#URL#'>mis à jour</a> attributs d'accès pour les prospects créés précédemment.";
$MESS["CRM_CONFIG_PERMS_REBUILD_QUOTE_ATTRS"] = "Les autorisations d'accès mises à jour de cotation exigent que vous <a id='#ID#' href='#URL#'>mis à jour</a> attributs d'accès pour les devis créés précédemment.";
$MESS["CRM_CONFIG_PERMS_REBUILD_DEAL_ATTRS"] = "Les autorisations mises à jour d'accès deal exigent que vous <a id='#ID#' href='#URL#'>mis à jour</a> attributs d'accès pour les bonnes affaires précédemment créés.";
$MESS["CRM_CONFIG_PERMS_REBUILD_INVOICE_ATTRS"] = "Les autorisations d'accès mises à jour de facturation nécessitent que vous <a id='#ID#' href='#URL#'> jour </a> attributs d'accès pour les factures précédemment créés.";
$MESS["CRM_CONFIG_PERMS_REBUILD_CONTACT_ATTR_DLG_TITLE"] = "Mise à jour Contact Accès Attributs";
$MESS["CRM_CONFIG_PERMS_REBUILD_LEAD_ATTR_DLG_TITLE"] = "Mettre à jour les attributs d'accès plomb";
$MESS["CRM_CONFIG_PERMS_REBUILD_QUOTE_ATTR_DLG_TITLE"] = "Attributs d'accès Mise à jour Quote";
$MESS["CRM_CONFIG_PERMS_REBUILD_DEAL_ATTR_DLG_TITLE"] = "Mise à jour offre l'accès Attributs";
$MESS["CRM_CONFIG_PERMS_REBUILD_INVOICE_ATTR_DLG_TITLE"] = "Mise à jour facture accès Attributs";
?>