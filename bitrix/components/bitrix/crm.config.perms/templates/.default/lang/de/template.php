<?
$MESS["CRM_PERMS_LEAD"] = "Leads";
$MESS["CRM_PERMS_CONTACT"] = "Kontakte";
$MESS["CRM_PERMS_COMPANY"] = "Unternehmen";
$MESS["CRM_PERMS_DEAL"] = "Aufträge";
$MESS["CRM_PERMS_EVENT"] = "Aktivitäten";
$MESS["CRM_PERMS_CONFIG"] = "Einstellungen";
$MESS["CRM_PERMS_TYPE_ALL"] = "Alle";
$MESS["CRM_PERMS_TYPE_SELF"] = "Eigene";
$MESS["CRM_PERMS_TYPE_NONE"] = "Keine";
$MESS["CRM_PERMS_TYPE_EDIT"] = "Bearbeiten";
$MESS["CRM_PERMS_LIST_EMPTY"] = "Zugriffsberechtigungen wurden nicht bestimmt.";
$MESS["CRM_PERMS_ADD_LINK"] = "Hinzufügen";
$MESS["CRM_PERMS_BUTTONS_SAVE"] = "Speichern";
$MESS["CRM_PERMS_BUTTONS_CANCEL"] = "Abbrechen";
?>