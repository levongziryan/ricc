<?
$MESS["CRM_CONFIG_PERMS_REBUILD_ATTR_PROGRESS_SUMMARY"] = "Itens processados: #PROCESSED_ITEMS# of #TOTAL_ITEMS#.";
$MESS["CRM_CONFIG_PERMS_REBUILD_ATTR_COMPLETED_SUMMARY"] = "Atributos de acesso foram atualizados. Itens processados: #PROCESSED_ITEMS#.";
$MESS["CRM_CONFIG_PERMS_REBUILD_ATTR_NOT_REQUIRED_SUMMARY"] = "Atributos de acesso não precisa ser atualizado.";
?>