<?
$MESS["CRM_CONFIG_PERMS_REBUILD_ATTR_COMPLETED_SUMMARY"] = "La mise à jour d'attributs d'accès est terminée. Eléments traités: #PROCESSED_ITEMS#.";
$MESS["CRM_CONFIG_PERMS_REBUILD_ATTR_NOT_REQUIRED_SUMMARY"] = "La mise à jour d'attributs d'accès n'est pas nécessaire.";
$MESS["CRM_CONFIG_PERMS_REBUILD_ATTR_PROGRESS_SUMMARY"] = "Nombre d'éléments traités: #PROCESSED_ITEMS# de #TOTAL_ITEMS#.";
?>