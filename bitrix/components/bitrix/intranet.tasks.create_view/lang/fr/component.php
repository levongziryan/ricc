<?
$MESS["INTV_EDIT_TITLE"] = "Modification d'affichage '#NAME#'";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "Le module des blocs d'informations n'a pas été installé.";
$MESS["EC_SONET_MODULE_NOT_INSTALLED"] = "Le module du réseau social n'a pas été installé.";
$MESS["INTV_INTERNAL_ERROR"] = "Erreur système.";
$MESS["INTV_CREATE_TITLE"] = "Création d'une nouvelle représentation";
$MESS["INTV_NO_SONET_PERMS"] = "Vous n'avez pas de droits pour accéder aux tâches.";
$MESS["INTV_NO_IBLOCK_PERMS"] = "Vous n'avez pas de droits d'accès à l'affichage du bloc d'information des tâches.";
$MESS["INTV_TASKS_OFF"] = "La fonctionnalité des tâches est activée.";
?>