<?
$MESS["INTV_EDIT_TITLE"] = "Editar Visualização \"#NAME#\"";
$MESS["INTV_CREATE_TITLE"] = "Nova Visualização";
$MESS["INTV_INTERNAL_ERROR"] = "Ocorreu um erro de sistema.";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "O módulo Blocos de Informação não está instalado.";
$MESS["EC_SONET_MODULE_NOT_INSTALLED"] = "O módulo Rede Social não está instalado.";
$MESS["INTV_TASKS_OFF"] = "O recurso Tarefas está desabilitado.";
$MESS["INTV_NO_SONET_PERMS"] = "Você não tem permissão para visualizar tarefas.";
$MESS["INTV_NO_IBLOCK_PERMS"] = "Você não tem permissão para visualizar o bloco de informações da tarefa.";
?>