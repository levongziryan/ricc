<?
$MESS["M_CRM_QUOTE_EDIT_SAVE_BTN"] = "Guardar";
$MESS["M_CRM_QUOTE_EDIT_CONTINUE_BTN"] = "Continuar";
$MESS["M_CRM_QUOTE_EDIT_CANCEL_BTN"] = "Cancelar";
$MESS["M_CRM_QUOTE_EDIT_CREATE_TITLE"] = "Crear cotización";
$MESS["M_CRM_QUOTE_EDIT_EDIT_TITLE"] = "Editar";
$MESS["M_CRM_QUOTE_EDIT_VIEW_TITLE"] = "Editar cotización";
$MESS["M_CRM_QUOTE_EDIT_CONVERT_TITLE"] = "Cotización";
$MESS["M_DETAIL_PULL_TEXT"] = "Pulsar para actualizar...";
$MESS["M_DETAIL_DOWN_TEXT"] = "Soltar para actualizar...";
$MESS["M_DETAIL_LOAD_TEXT"] = "Actualizando...";
$MESS["M_CRM_QUOTE_MENU_EDIT"] = "Editar";
$MESS["M_CRM_QUOTE_MENU_DELETE"] = "Eliminar";
$MESS["M_CRM_QUOTE_CONVERSION_NOTIFY"] = "Campo requerido";
$MESS["M_CRM_QUOTE_MENU_CREATE_ON_BASE"] = "Crear usando el origen";
$MESS["M_CRM_QUOTE_MENU_HISTORY"] = "Historial";
?>