<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
$MESS["CRM_QUOTE_EDIT_NOT_FOUND"] = "Impossible de trouver l'ID du devis # '#ID#'.";
$MESS["CRM_QUOTE_FIELD_TITLE_QUOTE"] = "Sujet";
$MESS["CRM_QUOTE_FIELD_OPENED"] = "Disponible pour tous";
$MESS["CRM_QUOTE_FIELD_FILES"] = "Fichiers";
$MESS["CRM_QUOTE_FIELD_COMMENTS"] = "Commentaire (non visible dans le devis)";
$MESS["CRM_QUOTE_FIELD_OPPORTUNITY"] = "Montant";
$MESS["CRM_QUOTE_FIELD_COMPANY_TITLE"] = "Entreprise";
$MESS["CRM_QUOTE_FIELD_CONTACT_TITLE"] = "Contact";
$MESS["CRM_QUOTE_FIELD_STATUS_ID"] = "Statut du devis";
$MESS["CRM_QUOTE_FIELD_CLOSED"] = "Devis fermé";
$MESS["CRM_QUOTE_FIELD_CURRENCY_ID"] = "Devise";
$MESS["CRM_QUOTE_FIELD_CLOSEDATE"] = "Date limite";
$MESS["CRM_QUOTE_FIELD_BEGINDATE"] = "Date de la facture";
$MESS["CRM_QUOTE_FIELD_ASSIGNED_BY_ID"] = "Personne responsable";
$MESS["CRM_QUOTE_FIELD_CREATED_BY_ID"] = "Créé par";
$MESS["CRM_QUOTE_FIELD_MODIFY_BY_ID"] = "Modifié par";
$MESS["CRM_QUOTE_FIELD_DATE_CREATE"] = "Créé le";
$MESS["CRM_QUOTE_FIELD_DATE_MODIFY"] = "Modifié le";
$MESS["CRM_QUOTE_FIELD_QUOTE_EVENT"] = "Historique de devis";
$MESS["CRM_QUOTE_FIELD_PRODUCT_ROWS"] = "Produits";
$MESS["CRM_QUOTE_FIELD_SALE_ORDER1"] = "Commande e-Store";
$MESS["CRM_QUOTE_FIELD_CONTACT_PHONE"] = "Téléphones du contact";
$MESS["CRM_QUOTE_FIELD_CONTACT_EMAIL"] = "E-mails du contact";
$MESS["CRM_QUOTE_FIELD_CONTACT_POST"] = "Position";
$MESS["CRM_QUOTE_FIELD_CONTACT_ADDRESS"] = "Adresse";
$MESS["CRM_QUOTE_FIELD_CONTACT_IM"] = "Messagerie instantanée du contact";
$MESS["CRM_QUOTE_FIELD_CONTACT_SOURCE"] = "Source du contact";
$MESS["CRM_QUOTE_FIELD_CONTACT_TYPE"] = "Type de contact";
$MESS["CRM_QUOTE_FIELD_COMPANY_INDUSTRY"] = "Industrie";
$MESS["CRM_QUOTE_FIELD_COMPANY_PHONE"] = "Téléphones de l'entreprise";
$MESS["CRM_QUOTE_FIELD_COMPANY_EMAIL"] = "E-mails de l'entreprise";
$MESS["CRM_QUOTE_FIELD_COMPANY_EMPLOYEES"] = "Employés";
$MESS["CRM_QUOTE_FIELD_COMPANY_REVENUE"] = "Revenu annuel";
$MESS["CRM_QUOTE_FIELD_COMPANY_TYPE"] = "Type d'entreprise";
$MESS["CRM_QUOTE_FIELD_COMPANY_WEB"] = "Sites web de l'entreprise";
$MESS["CRM_QUOTE_FIELD_COMPANY_ADDRESS"] = "Adresse postale";
$MESS["CRM_QUOTE_FIELD_COMPANY_ADDRESS_LEGAL"] = "Adresse légale";
$MESS["RESPONSIBLE_NOT_ASSIGNED"] = "[non assigné]";
$MESS["CRM_QUOTE_FIELD_QUOTE_INVOICE"] = "Factures de devis";
$MESS["CRM_QUOTE_ADD_INVOICE_TITLE"] = "Créer une facture sur le devis";
$MESS["CRM_QUOTE_FIELD_QUOTE_NUMBER"] = "Devis #";
$MESS["CRM_QUOTE_FIELD_LEAD_ID"] = "Client potentiel";
$MESS["CRM_QUOTE_FIELD_DEAL_ID"] = "Affaire";
$MESS["CRM_QUOTE_FIELD_LOCATION_ID"] = "Lieu";
$MESS["CRM_FIELD_LOCATION"] = "Lieu";
$MESS["CRM_FIELD_LOCATION_DESCRIPTION"] = "Entrer un nom de ville ou de pays";
$MESS["CRM_QUOTE_FIELD_CONTENT"] = "Contenus";
$MESS["CRM_QUOTE_FIELD_TERMS"] = "Conditions";
$MESS["CRM_BUTTON_SELECT"] = "Sélectionner";
$MESS["CRM_MOBILE_MODULE_NOT_INSTALLED"] = "Le module “Mobile” n'est pas installé.";
?>