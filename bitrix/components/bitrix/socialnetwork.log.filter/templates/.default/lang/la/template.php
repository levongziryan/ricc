<?
$MESS["SONET_C30_T_FILTER_TITLE"] = "Buscar";
$MESS["SONET_C30_T_FILTER_CREATED_BY"] = "Autor";
$MESS["SONET_C30_T_FILTER_GROUP"] = "Grupo";
$MESS["SONET_C30_T_FILTER_USER"] = "Receptor";
$MESS["SONET_C30_T_FILTER_DATE"] = "Fecha ";
$MESS["SONET_C30_T_SHOW_HIDDEN"] = "Mostrar categorías ocultas";
$MESS["SONET_C30_T_SUBMIT"] = "Seleccionar";
$MESS["SONET_C30_T_RESET"] = "Resetear";
$MESS["SONET_C30_PRESET_FILTER_ALL"] = "Todos los eventos";
$MESS["SONET_C30_SMART_EXPERT_MODE"] = "Ocultar tareas";
$MESS["SONET_C30_F_EXPERT_MODE_IMAGENAME"] = "en";
$MESS["SONET_C30_F_EXPERT_MODE_POPUP_TITLE"] = "Modo de tareas ocultas";
$MESS["SONET_C30_F_EXPERT_MODE_POPUP_TEXT1"] = "Hemos habilitado el modo de tareas ocultas, debido a que usted las utiliza constantemente.<br/>Sus tareas no serán visibles en el Activity Stream, más bien, usted deberá ingresar directamente a la sección Tareas.";
$MESS["SONET_C30_F_EXPERT_MODE_POPUP_TEXT2"] = "Usted puede desactivarlo desde el Flujo de Actividad.";
$MESS["SONET_C30_SMART_FOLLOW_HINT"] = "Modo de seguimiento inteligente está activado. Sólo los mensajes de los que es autor, destinatario o en los cuales se le menciona en el texto serán trasladados a la parte superior. Usted automáticamente sigue cualquier mensaje sobre el que comente.";
$MESS["SONET_C30_T_FILTER_COMMENTS"] = "Buscar también en comentarios.";
$MESS["SONET_C30_F_DIALOG_CLOSE_BUTTON"] = "Cerrar";
$MESS["SONET_C30_EXPERT_MODE_HINT"] = "Modo \"Ocultar Tareas\" está ahora activado. Sus tareas no serán visibles en el Flujo de Actividad; más bien, se accede a ellas directamente desde la sección Tareas.<br /><br />Usted seguirá recibiendo notificaciones de tareas en el sistema de mensajería instantánea.";
$MESS["SONET_C30_EXTRANET_ROOT"] = "Extranet";
$MESS["SONET_C30_T_FILTER_TO"] = "Para";
$MESS["SONET_C30_SMART_FOLLOW"] = "Modo de seguimiento inteligente";
$MESS["SONET_C30_F_DIALOG_READ_BUTTON"] = "Yo leí esto";
?>