<?
$MESS["P_SELECT_PHOTO"] = "Escolher foto de capa";
$MESS["P_SUBMIT"] = "Salvar";
$MESS["P_CANCEL"] = "Cancelar";
$MESS["P_UP"] = "Acima";
$MESS["P_UP_TITLE"] = "Um nível acima";
$MESS["P_BACK_UP"] = "Voltar";
$MESS["P_BACK_UP_TITLE"] = "Voltar";
$MESS["P_EMPTY_PHOTO"] = "Este álbum está vazio";
?>