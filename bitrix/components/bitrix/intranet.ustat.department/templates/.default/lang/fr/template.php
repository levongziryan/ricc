<?
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_TOTAL"] = "Utilisation d'entité";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_SOCNET"] = "Utilisation d'entité: Réseau social";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_LIKES"] = "Utilisation d'entité: Likes";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_TASKS"] = "Utilisation d'entité: Tâches";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_IM"] = "Utilisation d'entité: Chat";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_DISK"] = "Utilisation d'entité: Drive";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_MOBILE"] = "Utilisation d'entité: Mobile App";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_CRM"] = "Utilisation d'entité: CRM";
$MESS["INTRANET_USTAT_COMPANY_GRAPH_TITLE"] = "Pouls de la société";
$MESS["INTRANET_USTAT_COMPANY_GRAPH_ACTIVITY"] = "Actif(ve)";
$MESS["INTRANET_USTAT_COMPANY_GRAPH_INVOLVEMENT"] = "Utilisation d'entité";
$MESS["INTRANET_USTAT_COMPANY_RATING_TITLE"] = "Classement général";
$MESS["INTRANET_USTAT_COMPANY_ACTIVITY_TITLE"] = "Index <br> de l'activité";
$MESS["INTRANET_USTAT_COMPANY_SECTION_INVOLVEMENT"] = "Utilisation d'entité";
$MESS["INTRANET_USTAT_COMPANY_SECTION_ACTIVITY"] = "Activité";
$MESS["INTRANET_USTAT_COMPANY_SECTION_TELL_ABOUT"] = "Dire";
$MESS["INTRANET_USTAT_COMPANY_HELP_GENERAL"] = "Pouls de la société est un indicateur global de l'activité de l'utilisateur dans le portail à l'heure actuelle (composite pour tous les utilisateurs dans l'heure précédente).";
$MESS["INTRANET_USTAT_COMPANY_HELP_ACTIVITY"] = "Index d'activité est la somme des actions différentes effectuées par les utilisateurs sur le portail en utilisant l'un des outils pour une période de temps défini. L'index permet de déterminer le niveau d'utilisation des outils du service.";
$MESS["INTRANET_USTAT_COMPANY_HELP_INVOLVEMENT"] = "Niveau d'engagement: ceci est un indicateur clé qui montre comment les utilisateurs sont devenus familiers avec les capacités de Bitrix24. Il indique le pourcentage de l'entreprise l'utilisation d'au moins un quart des outils fournis.";
$MESS["INTRANET_USTAT_COMPANY_HELP_RATING"] = "Le b>Classement</b> est construit selon l'indice d'activité personnel parmi tous les collaborateurs qui ont utilisé ne serai-ce qu'une seule fois les outils de 'Bitrix24' au cours de la période de temps sélectionnée.";
?>