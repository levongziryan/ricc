<?
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_TOTAL"] = "Uso de funciones";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_SOCNET"] = "Uso de funciones: Red Social";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_LIKES"] = "Uso de función: Likes";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_TASKS"] = "Uso de función: Tareas";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_IM"] = "Uso de función: Chat";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_DISK"] = "Uso de función: Drive";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_MOBILE"] = "Uso de función: App Movil";
$MESS["INTRANET_USTAT_COMPANY_INV_BAR_CRM"] = "Uso de función: CRM";
$MESS["INTRANET_USTAT_COMPANY_GRAPH_TITLE"] = "Pulso de la Compañía";
$MESS["INTRANET_USTAT_COMPANY_GRAPH_ACTIVITY"] = "Actividad";
$MESS["INTRANET_USTAT_COMPANY_GRAPH_INVOLVEMENT"] = "Uso de función";
$MESS["INTRANET_USTAT_COMPANY_RATING_TITLE"] = "Raiting general";
$MESS["INTRANET_USTAT_COMPANY_ACTIVITY_TITLE"] = "Promedio<br>de actividad";
$MESS["INTRANET_USTAT_COMPANY_SECTION_INVOLVEMENT"] = "Uso de funciones";
$MESS["INTRANET_USTAT_COMPANY_SECTION_ACTIVITY"] = "Actividad";
$MESS["INTRANET_USTAT_COMPANY_SECTION_TELL_ABOUT"] = "Informar";
$MESS["INTRANET_USTAT_COMPANY_HELP_GENERAL"] = "Pulso de la Compañía es un indicador global de la actividad actual del usuario en el portal (se elabora basado en la actividad previa de todos los usuarios).";
$MESS["INTRANET_USTAT_COMPANY_HELP_ACTIVITY"] = "Índice de actividad: compuesto por toda la actividad de los usuarios sobre todas las funciones de la intranet para un perido dado de tiempo. El índice muestra a los usuarios que están trabajando activamente en las diferentes herramientas.";
$MESS["INTRANET_USTAT_COMPANY_HELP_INVOLVEMENT"] = "Nivel de compromiso: se trata de un indicador clave que muestra cómo los usuarios se han familiarizado con las funciones y características de Bitrix24. Muestra qué porcentaje de la empresa usa por lo menos una cuarta parte de las herramientas proporcionadas.";
$MESS["INTRANET_USTAT_COMPANY_HELP_RATING"] = "El raiting es determinado por indice promedio de la actividad indivicual de todos los usuarios que han realizado por lo menos una acción dentro del portal, para un periodo determinado.";
?>