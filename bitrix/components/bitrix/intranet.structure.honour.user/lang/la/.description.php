<?
$MESS["INTR_GROUP_NAME"] = "Portal de Intranet";
$MESS["INTR_ISHU_COMPONENT_NAME"] = "Usuario en la lista de empleados honorarios";
$MESS["INTR_ISHU_COMPONENT_DESCR"] = "Muestra la historia de la presencia del empleado en la lista de empleados honorarios";
$MESS["INTR_HR_GROUP_NAME"] = "RRHH";
$MESS["INTR_HONOUR_GROUP_NAME"] = "Empleados Honorarios";
?>