<?
$MESS["CRM_PERM_GROUP"] = "Groupe";
$MESS["CRM_PERM_DEPARTAMENT"] = "Département";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_PERM_USER"] = "Utilisateur";
$MESS["CRM_PERMS_ENTITY_LIST"] = "Droits d'accès";
?>