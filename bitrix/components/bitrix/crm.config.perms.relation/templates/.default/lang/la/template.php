<?
$MESS["CRM_PERMS_TYPE_NONE"] = "Acceso denegado.";
$MESS["CRM_PERMS_PERM_ADD"] = "Agregar permisos de acceso ";
$MESS["CRM_PERMS_PERM_DELETE"] = "Eliminar";
$MESS["CRM_PERMS_ROLE_ADD"] = "Agregar";
$MESS["CRM_PERMS_ROLE_EDIT"] = "Editar";
$MESS["CRM_PERMS_ROLE_DELETE"] = "Eliminar";
$MESS["CRM_PERMS_ROLE_LIST"] = "Roles";
$MESS["CRM_PERMS_PERM_ROLE"] = "Rol del CRM";
$MESS["CRM_PERMS_PERM_ENTITY"] = "Usuario";
$MESS["CRM_PERMS_BUTTONS_SAVE"] = "Guardar";
$MESS["CRM_PERMS_BUTTONS_CANCEL"] = "Cancelar";
$MESS["CRM_PERMS_DLG_MESSAGE"] = "¿Usted está seguro que desea eliminarlo?";
$MESS["CRM_PERMS_DLG_TITLE"] = "Eliminar rol";
$MESS["CRM_PERMS_DLG_BTN"] = "Eliminar";
?>