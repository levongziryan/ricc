<?
$MESS["CRM_PERMS_DLG_MESSAGE"] = "Etes-vous sûr de vouloir supprimer?";
$MESS["CRM_PERMS_ROLE_ADD"] = "Ajouter";
$MESS["CRM_PERMS_PERM_ADD"] = "Ajouter le droit d'accès";
$MESS["CRM_PERMS_TYPE_NONE"] = "Accès interdit.";
$MESS["CRM_PERMS_BUTTONS_CANCEL"] = "Annuler";
$MESS["CRM_PERMS_PERM_ENTITY"] = "Utilisateur";
$MESS["CRM_PERMS_ROLE_EDIT"] = "Editer";
$MESS["CRM_PERMS_PERM_ROLE"] = "Rôle de CRM";
$MESS["CRM_PERMS_BUTTONS_SAVE"] = "Sauvegarder";
$MESS["CRM_PERMS_ROLE_LIST"] = "Listes des rôles";
$MESS["CRM_PERMS_DLG_TITLE"] = "Suppression du rôle";
$MESS["CRM_PERMS_PERM_DELETE"] = "Supprimer";
$MESS["CRM_PERMS_ROLE_DELETE"] = "Supprimer";
$MESS["CRM_PERMS_DLG_BTN"] = "Supprimer";
?>