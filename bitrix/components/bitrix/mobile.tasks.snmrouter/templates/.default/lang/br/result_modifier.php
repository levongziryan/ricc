<?
$MESS["MB_TASKS_TASKS_LIST_PULLDOWN_PULL"] = "Puxe para baixo para atualizar";
$MESS["MB_TASKS_TASKS_LIST_PULLDOWN_DOWN"] = "Solte para atualizar";
$MESS["MB_TASKS_TASKS_LIST_PULLDOWN_LOADING"] = "Atualizando dados ...";
$MESS["MB_TASKS_TASK_SNMROUTER_TASK_WAS_REMOVED"] = "A tarefa foi excluída.";
$MESS["MB_TASKS_TASKS_LIST_MENU_CREATE_NEW_TASK"] = "Nova tarefa";
$MESS["MB_TASKS_TASKS_LIST_MENU_GOTO_FILTER"] = "Configurar filtro";
$MESS["MB_TASKS_PANEL_TAB_ALL"] = "Todos";
$MESS["MB_TASKS_PANEL_TAB_PROJECTS"] = "Projetos";
$MESS["MB_TASKS_GENERAL_TITLE"] = "Tarefas";
$MESS["MB_TASKS_ROLES_TASK_ADD"] = "Nova tarefa";
$MESS["MB_TASKS_TASKS_LIST_NO_TASKS"] = "Sem tarefas";
$MESS["MB_TASKS_TASK_ERROR_TITLE"] = "Erro";
?>