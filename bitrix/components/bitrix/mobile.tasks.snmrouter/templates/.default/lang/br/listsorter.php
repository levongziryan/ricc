<?
$MESS["TASK_COLUMN_TITLE"] = "Nome da tarefa";
$MESS["TASK_COLUMN_DEADLINE"] = "Prazo";
$MESS["TASK_COLUMN_CREATED_BY"] = "Criado por";
$MESS["TASK_COLUMN_RESPONSIBLE_ID"] = "Pessoa responsável";
$MESS["TASK_COLUMN_PRIORITY"] = "Prioridade";
$MESS["TASK_COLUMN_MARK"] = "Avaliação";
$MESS["TASK_COLUMN_TIME_ESTIMATE"] = "Tempo estimado obrigatório";
$MESS["TASK_COLUMN_ALLOW_TIME_TRACKING"] = "Gerenciar tempo gasto";
$MESS["TASK_COLUMN_CREATED_DATE"] = "Criado em";
$MESS["TASK_COLUMN_CHANGED_DATE"] = "Modificado em";
$MESS["TASK_COLUMN_CLOSED_DATE"] = "Concluído em";
$MESS["TASK_COLUMN_SORTING"] = "Minha classificação";
?>