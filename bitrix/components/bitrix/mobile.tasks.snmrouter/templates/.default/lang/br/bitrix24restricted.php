<?
$MESS["TASK_RESTRICTED_USER1"] = "<h1>Seu plano limita o acesso a esta opção</h1><p>Ferramentas de negócio no seu plano estão disponíveis para até 24 usuários.</p><p>Se você precisa de acesso a ferramentas de negócio, entre em contato com seu administrador Bitrix24.</p>";
$MESS["TASK_RESTRICTED_USER2"] = "Enviar pedido";
$MESS["TASK_RESTRICTED_USER3"] = "O pedido foi enviado";
$MESS["TASK_RESTRICTED_ADMIN1"] = "<h1>Seu plano limita o acesso a esta opção</h1><p>Ferramentas de negócio no seu plano estão disponíveis para até 24 usuários.</p><p>Você pode conceder ou revogar o acesso de um usuário a ferramentas de negócio apenas na versão completa do Bitrix24.</p>";
$MESS["TASK_RESTRICTED_ADMIN2"] = "Abrir versão completa";
?>