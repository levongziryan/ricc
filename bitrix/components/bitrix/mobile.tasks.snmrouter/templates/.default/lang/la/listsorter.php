<?
$MESS["TASK_COLUMN_TITLE"] = "Nombre de la tarea";
$MESS["TASK_COLUMN_DEADLINE"] = "Declinado";
$MESS["TASK_COLUMN_CREATED_BY"] = "Creado por ";
$MESS["TASK_COLUMN_RESPONSIBLE_ID"] = "Persona responsable";
$MESS["TASK_COLUMN_PRIORITY"] = "Prioridad";
$MESS["TASK_COLUMN_MARK"] = "Valoración";
$MESS["TASK_COLUMN_TIME_ESTIMATE"] = "Tiempo estimado requerido";
$MESS["TASK_COLUMN_ALLOW_TIME_TRACKING"] = "Seguimiento de tiempo empleado";
$MESS["TASK_COLUMN_CREATED_DATE"] = "Creado el ";
$MESS["TASK_COLUMN_CHANGED_DATE"] = "Modificado el ";
$MESS["TASK_COLUMN_CLOSED_DATE"] = "Completado el ";
$MESS["TASK_COLUMN_SORTING"] = "Mi clasificación";
?>