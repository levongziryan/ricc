<?
$MESS["MB_TASKS_TASKS_LIST_PULLDOWN_PULL"] = "Tirez vers le bas pour rafraîchir";
$MESS["MB_TASKS_TASKS_LIST_PULLDOWN_DOWN"] = "Lâchez pour rafraîchir";
$MESS["MB_TASKS_TASKS_LIST_PULLDOWN_LOADING"] = "Mise à jour des données...";
$MESS["MB_TASKS_TASK_SNMROUTER_TASK_WAS_REMOVED"] = "La tâche a été supprimée.";
$MESS["MB_TASKS_TASKS_LIST_MENU_CREATE_NEW_TASK"] = "Nouvelle tâche";
$MESS["MB_TASKS_TASKS_LIST_MENU_GOTO_FILTER"] = "Configurer le filtre";
$MESS["MB_TASKS_PANEL_TAB_ALL"] = "Tous";
$MESS["MB_TASKS_PANEL_TAB_PROJECTS"] = "Projets";
$MESS["MB_TASKS_GENERAL_TITLE"] = "Tâches";
$MESS["MB_TASKS_ROLES_TASK_ADD"] = "Nouvelle tâche";
$MESS["MB_TASKS_TASKS_LIST_NO_TASKS"] = "Aucune tâche";
$MESS["MB_TASKS_TASK_ERROR_TITLE"] = "Erreur";
?>