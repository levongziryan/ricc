<?
$MESS["INTMT_BACK2LIST_DESCR"] = "Regresar a la Lista de Tarea";
$MESS["INTMT_BACK2LIST"] = "Tareas";
$MESS["INTMT_EDIT_TASK_DESCR"] = "Editar Tarea Actual";
$MESS["INTMT_EDIT_TASK"] = "Editar Tarea";
$MESS["INTMT_VIEW_TASK_DESCR"] = "Ver tarea actual";
$MESS["INTMT_VIEW_TASK"] = "Ver Tarea";
$MESS["INTMT_CREATE_TASK_DESCR"] = "Crear Nueva Tarea";
$MESS["INTMT_CREATE_TASK"] = "Crear Tarea";
$MESS["INTMT_CREATE_FOLDER_DESCR"] = "Crear nuevas carpetas";
$MESS["INTMT_CREATE_FOLDER"] = "Crear Carpeta";
$MESS["INTMT_VIEW"] = "Ver";
$MESS["INTMT_DEFAULT"] = "Predeterminado";
$MESS["INTMT_CREATE_VIEW"] = "Crear Vista";
$MESS["INTMT_EDIT_VIEW"] = "Editar Vista";
$MESS["INTMT_DELETE_VIEW_CONF"] = "Usted está seguro que desea borrar esta vista?";
$MESS["INTMT_DELETE_VIEW"] = "Borrar Vista";
$MESS["INTMT_OUTLOOK"] = "Conectar al Outlook";
$MESS["INTMT_OUTLOOK_TITLE"] = "Sincronización de la tarea Outlook";
?>