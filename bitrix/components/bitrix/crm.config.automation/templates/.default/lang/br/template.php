<?
$MESS["CRM_CONFIG_AUTOMATION_HELP_TITLE"] = "Automatize as vendas e o gerenciamento de clientes com o CRM Bitrix24";
$MESS["CRM_CONFIG_AUTOMATION_HELP_1_1"] = "Integração rápida de novos representantes de vendas. Regras de automação e gatilhos definem quem precisa fazer o quê e quando.";
$MESS["CRM_CONFIG_AUTOMATION_HELP_1_2"] = "Livre seus representantes de vendas da rotina.";
$MESS["CRM_CONFIG_AUTOMATION_HELP_1_3"] = "Controle cada etapa do processo de vendas. O CRM Bitrix24 notifica imediatamente os supervisores sobre quaisquer problemas com representantes, documentos ou gargalos.";
$MESS["CRM_CONFIG_AUTOMATION_HELP_TITLE_2"] = "Defina facilmente regras e gatilhos";
$MESS["CRM_CONFIG_AUTOMATION_HELP_2_1"] = "Use guia de Automação em todas as entidades aplicáveis de CRM.";
$MESS["CRM_CONFIG_AUTOMATION_HELP_2_3"] = "Teste e veja como as regras de automação e gatilhos funcionam em tempo real.";
$MESS["CRM_CONFIG_AUTOMATION_HELP_2_2"] = "Defina regras de automação para ajudar o seu negócio a crescer.";
?>