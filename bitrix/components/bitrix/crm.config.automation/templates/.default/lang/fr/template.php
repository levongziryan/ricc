<?
$MESS["CRM_CONFIG_AUTOMATION_HELP_TITLE"] = "Automatisez les ventes et la gestion des clients avec le CRM de Bitrix24";
$MESS["CRM_CONFIG_AUTOMATION_HELP_1_1"] = "Intégration rapide pour les nouveaux agents commerciaux. Les règles d'automatisation et les déclencheurs définissent qui doit faire quoi, et quand.";
$MESS["CRM_CONFIG_AUTOMATION_HELP_1_2"] = "Libérez vos agents commerciaux de leur routine.";
$MESS["CRM_CONFIG_AUTOMATION_HELP_1_3"] = "Contrôlez chaque étape du processus de vente. Le CRM de Bitrix24 notifie les superviseurs de tout problème avec les agents, documents ou blocages.";
$MESS["CRM_CONFIG_AUTOMATION_HELP_TITLE_2"] = "Définissez des règles et déclencheurs facilement";
$MESS["CRM_CONFIG_AUTOMATION_HELP_2_1"] = "Utilisez l'onglet Automatisation dans toutes les entités CRM applicables.";
$MESS["CRM_CONFIG_AUTOMATION_HELP_2_3"] = "Effectuez des tests et voyez comment fonctionnent les règles d'automatisation et les déclencheurs en temps réel.";
$MESS["CRM_CONFIG_AUTOMATION_HELP_2_2"] = "Définissez des règles d'automatisation pour aider votre entreprise à grandir.";
?>