<?
$MESS["CRM_TAB_1"] = "Configurações de importação";
$MESS["CRM_TAB_1_TITLE"] = "Editar configurações de importação";
$MESS["CRM_TAB_2"] = "Controle de duplicados";
$MESS["CRM_TAB_2_TITLE"] = "Configurar controle de duplicados";
$MESS["CRM_TAB_3"] = "Importar";
$MESS["CRM_TAB_3_TITLE"] = "Resultado da importação";
$MESS["CRM_IMPORT_NEXT_STEP"] = "Avançar >>";
$MESS["CRM_IMPORT_NEXT_STEP_TITLE"] = "Avance para o próximo passo";
$MESS["CRM_IMPORT_PREVIOUS_STEP"] = "<< Voltar";
$MESS["CRM_IMPORT_PREVIOUS_STEP_TITLE"] = "Volte para o passo anterior";
$MESS["CRM_IMPORT_DONE"] = "Pronto";
$MESS["CRM_IMPORT_DONE_TITLE"] = "Visualizar contatos";
$MESS["CRM_IMPORT_AGAIN"] = "Importar outro arquivo";
$MESS["CRM_IMPORT_AGAIN_TITLE"] = "Clique para importar outro arquivo";
$MESS["CRM_IMPORT_CANCEL"] = "Cancelar";
$MESS["CRM_IMPORT_CANCEL_TITLE"] = "Abortar e voltar para a lista de contatos";
?>