<?
$MESS["CRM_TAB_1"] = "Importar configuración";
$MESS["CRM_TAB_1_TITLE"] = "Editar configuración de importación";
$MESS["CRM_TAB_2"] = "Control de duplicados";
$MESS["CRM_TAB_2_TITLE"] = "Configurar el control de duplicados";
$MESS["CRM_TAB_3"] = "Importar";
$MESS["CRM_TAB_3_TITLE"] = "Resultado de la importación";
$MESS["CRM_IMPORT_NEXT_STEP"] = "Siguiente>>";
$MESS["CRM_IMPORT_NEXT_STEP_TITLE"] = "Ir al siguiente paso";
$MESS["CRM_IMPORT_PREVIOUS_STEP"] = "<< Atrás";
$MESS["CRM_IMPORT_PREVIOUS_STEP_TITLE"] = "Ir al paso anterior";
$MESS["CRM_IMPORT_DONE"] = "Listo";
$MESS["CRM_IMPORT_DONE_TITLE"] = "Ver contactos";
$MESS["CRM_IMPORT_AGAIN"] = "Importar otro archivo";
$MESS["CRM_IMPORT_AGAIN_TITLE"] = "Haga clic para importar otro archivo";
$MESS["CRM_IMPORT_CANCEL"] = "Cancelar";
$MESS["CRM_IMPORT_CANCEL_TITLE"] = "Cancelar y volver a la lista de contactos";
?>