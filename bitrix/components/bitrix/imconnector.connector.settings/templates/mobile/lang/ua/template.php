<?
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CONNECT"] = "Налаштування каналу";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CONFIGURE_CHANNEL"] = "Налаштування каналу";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_OPEN_LINE"] = "Відкрита лінія:";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CREATE_OPEN_LINE"] = "Створити відкриту лінію";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_NO_OPEN_LINE"] = "У вас немає жодної відкритої лінії, у якій ви можете налаштувати канали комунікацій. Будь ласка, створіть нову відкриту лінію.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_NO_OPEN_LINE_AND_NOT_ADD_OPEN_LINE"] = "У вас немає жодної відкритої лінії, у якій ви можете налаштувати канали комунікацій, і у вас немає прав на створення нової відкритої лінії.<br>Зверніться до адміністратора порталу.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_POPUP_LIMITED_TITLE"] = "Розширені відкриті лінії";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_POPUP_LIMITED_TEXT"] = "На вашому тарифному плані є обмеження по кількості Відкритих ліній. Щоб додати ще одну лінію (наприклад, для створення декількох рівнів підтримки клієнтів), перейдіть на інший тарифний план.
<br><br>
У верхньому тариф \"Компанія\" число активних відкритих ліній не обмежене.";
$MESS["IIMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_ERROR_ACTION"] = "Виникла помилка. Дію скасовано.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CLOSE"] = "Закрити";
?>