<?
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CONNECT"] = "Configuração do canal";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_LIVECHAT"] = "Caixa do bate-papo ao vivo do site";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_LIVECHAT"] = "<p class=\"im-connector-settings-header-description\">Instale o widget gratuito do bate-papo ao vivo no seu site e comunique-se com seus clientes em tempo real de qualquer lugar.</p>

    <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">funciona para qualquer site, código de inserção fornecido</li>

     <li class=\"im-connector-settings-header-list-item\">fácil de personalizar e configurar as condições do monitor </li>

     <li class=\"im-connector-settings-header-list-item\">mensagens recebidas distribuídas entre os agentes de acordo com suas regras de fila</li>

     <li class=\"im-connector-settings-header-list-item\">todas as conversas salvas no histórico de CRM</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_NETWORK"] = "Comunique-se facilmente com seus clientes, colegas e parceiros de negócio";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_NETWORK"] = "<p class=\"im-connector-settings-header-description\">Conecte Bitrix24.Network aos Canais Abertos e comunique-se facilmente com seus clientes, parceiros e colegas que usam Bitrix24. </p>

    <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">distribuição automática de mensagem recebida de acordo com suas regras de fila </li>

     <li class=\"im-connector-settings-header-list-item\">comunicações em tempo real</li>

     <li class=\"im-connector-settings-header-list-item\">interface familiar com o bate-papo Bitrix24</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_VIBER"] = "Gerencie sua conta pública Viber";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_VIBER"] = "<p class=\"im-connector-settings-header-description\"> Conecte sua conta pública Viber aos Canais Abertos e seus funcionários poderão se comunicar com os usuários do Viber diretamente a partir do Bitrix24. </p>

    <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">distribuição automática de mensagem recebida de acordo com regras de fila </li>

     <li class=\"im-connector-settings-header-list-item\">interface familiar com o bate-papo Bitrix24</li>

     <li class=\"im-connector-settings-header-list-item\">todas as conversas salvas no histórico de CRM</li>

</ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_TELEGRAMBOT"] = "Bitrix24 ama Telegram";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_TELEGRAMBOT"] = "<p class=\"im-connector-settings-header-description\">Conecte sua conta Telegram ao Canais Abertos e comunique-os a partir de sua conta Bitrix24.</p>

<ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">distribuição automática de mensagem recebida de acordo com as regras de fila </li>

     <li class=\"im-connector-settings-header-list-item\">distribuição automática de mensagem recebida de acordo com as regras de fila </li>

     <li class=\"im-connector-settings-header-list-item\">interface familiar com o bate-papo Bitrix24</li>

     <li class=\"im-connector-settings-header-list-item\">todas as conversas salvas no histórico de CRM</li>

</ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_VKGROUP"] = "Comunique-se com VK via Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_VKGROUP"] = "<p class=\"im-connector-settings-header-description\">Conecte seu grupo VK aos Canais Abertos e comunique-os a partir de sua conta Bitrix24.</p>

    <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">distribuição automática de mensagem recebida de acordo com as regras de fila </li>

     <li class=\"im-connector-settings-header-list-item\">interface familiar com o bate-papo Bitrix24</li>

     <li class=\"im-connector-settings-header-list-item\">todas as conversas salvas no histórico de CRM</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_FACEBOOK"] = "Comunique-se com usuários do Facebook via Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_FACEBOOK"] = "<p class=\"im-connector-settings-header-description\">Conecte a página do Facebook de sua empresa aos Canais Abertos e comunique-se com usuários do Facebook via Bitrix24.</p>

     <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">distribuição automática de mensagem recebida de acordo com as regras de fila </li>

     <li class=\"im-connector-settings-header-list-item\">interface familiar com o bate-papo Bitrix24</li>

     <li class=\"im-connector-settings-header-list-item\">todas as conversas salvas no histórico de CRM</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_FACEBOOKCOMMENTS"] = "Gerencie sua página do Facebook a partir do Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_FACEBOOKCOMMENTS"] = "<p class=\"im-connector-settings-header-description\">Conecte sua página do Facebook aos Canais Abertos e gerencie comentários das suas postagens, fotos e vídeos no Facebook pela sua conta Bitrix24.</p>

     <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">comunicações em tempo real</li>

     <li class=\"im-connector-settings-header-list-item\">distribuição automática de mensagem recebida de acordo com as regras de fila </li>

     <li class=\"im-connector-settings-header-list-item\">interface familiar com o bate-papo Bitrix24</li>

     <li class=\"im-connector-settings-header-list-item\">todas as conversas salvas no histórico de CRM</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_INSTAGRAM"] = "Bitrix24 ama Instagram";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_INSTAGRAM"] = "<p class=\"im-connector-settings-header-description\">Conecte sua conta do Instagram aos Canais Abertos e gerencie comentários do Instagram a partir da conta Bitrix24.</p>

    <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">Cometários do Instagram mostrados nos messenger Bitrix24</li>

     <li class=\"im-connector-settings-header-list-item\">distribuição automática de mensagem recebida de acordo com as regras de fila </li>

     <li class=\"im-connector-settings-header-list-item\">interface familiar com o bate-papo Bitrix24</li>

     <li class=\"im-connector-settings-header-list-item\">todas as conversas salvas no histórico de CRM</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_BOTFRAMEWORK"] = "Bitrix24 ama Microsoft Bot Framework";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_BOTFRAMEWORK"] = "<p class=\"im-connector-settings-header-description\">Conecte serviços suportados pelo Microsoft Bot Framework como Skype, Slack, Kik, GroupMe, SMS e Canais Abertos. Cada mensagem será encaminhada para a sua conta Bitrix24.</p>

    <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">distribuição automática de mensagem recebida de acordo com as regras de fila </li>

     <li class=\"im-connector-settings-header-list-item\">interface familiar com o bate-papo Bitrix24</li>

     <li class=\"im-connector-settings-header-list-item\">todas as conversas salvas no histórico de CRM</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CONFIGURE_CHANNEL"] = "Preferências do Canal Aberto";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_OPEN_LINE"] = "Canal Aberto:";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CREATE_OPEN_LINE"] = "Criar Canal Aberto";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CONFIGURE"] = "configurar";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_NO_OPEN_LINE"] = "Você não tem nenhum Canal Aberto cujas comunicações possam ser configuradas. Crie um novo Canal Aberto.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_NO_OPEN_LINE_AND_NOT_ADD_OPEN_LINE"] = "Você não tem nenhum Canal Aberto cujas comunicações possam ser configuradas. Você não tem permissão suficiente para criar Canais Abertos.<br>Entre em contato com o administrador do portal.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_POPUP_LIMITED_TITLE"] = "Canais Abertos Estendidos";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_POPUP_LIMITED_TEXT"] = "Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.Seu plano atual restringe o número de Canais Abertos disponíveis. Para adicionar outro Canal Aberto, faça um upgrade para um plano maior.
<br><br>
O plano \"Profissional\" não tem nenhuma restrição no número de Canais Abertos.";
$MESS["IIMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_ERROR_ACTION"] = "A ação foi cancelada porque ocorreu um erro.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CLOSE"] = "Fechar";
?>