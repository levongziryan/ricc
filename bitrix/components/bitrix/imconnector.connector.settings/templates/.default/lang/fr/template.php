<?
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CONNECT"] = "Configuration du Canal";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_LIVECHAT"] = "Boîte de chat live du site";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_LIVECHAT"] = "<p class=\"im-connector-settings-header-description\">Installez un widget de chat live gratuit sur votre site et communiquez avec vos clients en temps réel depuis n'importe où.</p>
<ul class=\"im-connector-settings-header-list\">
<li class=\"im-connector-settings-header-list-item\">fonctionne pour n'importe quel site, code embarqué fourni</li>
<li class=\"im-connector-settings-header-list-item\">personnalisation aisée et configuration facile des conditions d'affichage</li>
<li class=\"im-connector-settings-header-list-item\">messages entrants distribués parmi les agents selon les règles de votre file d'attente</li>
<li class=\"im-connector-settings-header-list-item\">toutes les conversations sont enregistrées dans l'historique du CRM</li>
</ul>";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_NETWORK"] = "Communiquez facilement avec vos clients, collègues et partenaires professionnels";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_NETWORK"] = "<p class=\"im-connector-settings-header-description\">Connectez Bitrix24.Network aux Canaux ouverts et communiquez facilement avec vos clients, partenaires et collègues qui utilisent Bitrix24. </p>
<ul class=\"im-connector-settings-header-list\">
<li class=\"im-connector-settings-header-list-item\">distribution automatique des messages entrants selon les règles de file d'attente </li>
<li class=\"im-connector-settings-header-list-item\">communications en temps réel</li>
<li class=\"im-connector-settings-header-list-item\">interface du chat Bitrix24 familière</li>
</ul>";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_VIBER"] = "Gérez votre compte Viber public";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_VIBER"] = "<p class=\"im-connector-settings-header-description\"> Connectez votre compte public Viber aux Canaux ouverts et vos employés pourront communiquer avec les utilisateurs Viber directement depuis Bitrix24. </p>
<ul class=\"im-connector-settings-header-list\">
<li class=\"im-connector-settings-header-list-item\">distribution automatique des messages entrants selon les règles de file d'attente </li>
<li class=\"im-connector-settings-header-list-item\">interface du chat Bitrix24 familière</li>
<li class=\"im-connector-settings-header-list-item\">toutes les conversations sont enregistrées dans l'historique du CRM</li>
</ul>";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_TELEGRAMBOT"] = "Bitrix24 aime Telegram";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_TELEGRAMBOT"] = "<p class=\"im-connector-settings-header-description\">Connectez votre compte Telegram aux Canaux ouverts et communiquez depuis votre compte Bitrix24.</p>
<ul class=\"im-connector-settings-header-list\">
<li class=\"im-connector-settings-header-list-item\">distribution automatique des messages entrants selon les règles de file d'attente </li>
<li class=\"im-connector-settings-header-list-item\">distribution automatique des messages entrants selon les règles de file d'attente </li>
<li class=\"im-connector-settings-header-list-item\">interface du chat Bitrix24 familière</li>
<li class=\"im-connector-settings-header-list-item\">toutes les conversations sont enregistrées dans l'historique du CRM</li> </ul>";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_VKGROUP"] = "Communiquez avec VK via Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_VKGROUP"] = "<p class=\"im-connector-settings-header-description\">Connectez votre groupe VK aux Canaux ouverts et communiquez depuis votre compte Bitrix24.</p>

    <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">distribution automatique des messages entrants selon les règles de file d'attente </li>

     <li class=\"im-connector-settings-header-list-item\">interface du chat Bitrix24 familière</li>

     <li class=\"im-connector-settings-header-list-item\">toutes les conversations sont enregistrées dans l'historique du CRM</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_FACEBOOK"] = "Communiquez avec les utilisateurs Facebook via Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_FACEBOOK"] = "<p class=\"im-connector-settings-header-description\">Connectez la page Facebook de votre entreprise aux Canaux ouverts et communiquez avec les utilisateurs Facebook via Bitrix24.</p>

     <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">distribution automatique des messages entrants selon les règles de file d'attente </li>

     <li class=\"im-connector-settings-header-list-item\">interface du chat Bitrix24 familière</li>

     <li class=\"im-connector-settings-header-list-item\">toutes les conversations sont enregistrées dans l'historique du CRM</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_FACEBOOKCOMMENTS"] = "Gérez votre page Facebook depuis Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_FACEBOOKCOMMENTS"] = "<p class=\"im-connector-settings-header-description\">Connectez votre page Facebook aux Canaux ouverts et gérez les commentaires de vos publications, photos et vidéos Facebook via votre compte Bitrix24.</p>

     <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">communications en temps réel</li>

     <li class=\"im-connector-settings-header-list-item\">distribution automatique des messages entrants selon les règles de file d'attente </li>

     <li class=\"im-connector-settings-header-list-item\">interface du chat Bitrix24 familière</li>

     <li class=\"im-connector-settings-header-list-item\">toutes les conversations sont enregistrées dans l'historique du CRM</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_INSTAGRAM"] = "Bitrix24 aime Instagram";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_INSTAGRAM"] = "<p class=\"im-connector-settings-header-description\">Connectez votre compte Instagram aux Canaux ouverts et gérez les commentaires Instagram depuis votre compte Bitrix24.</p>

    <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">commentaires Instagram affichés dans la messagerie Bitrix24</li>

     <li class=\"im-connector-settings-header-list-item\">distribution automatique des messages entrants selon les règles de file d'attente </li>

     <li class=\"im-connector-settings-header-list-item\">interface du chat Bitrix24 familière</li>

     <li class=\"im-connector-settings-header-list-item\">toutes les conversations sont enregistrées dans l'historique du CRM</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_BOTFRAMEWORK"] = "Bitrix24 aime Microsoft Bot Framework";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_BOTFRAMEWORK"] = "<p class=\"im-connector-settings-header-description\">Connectez les services supportés par Microsoft Bot Framework tels que Skype, Slack, Kik, GroupMe, SMS aux Canaux ouverts. Chaque message sera transféré vers votre compte Bitrix24.</p>

    <ul class=\"im-connector-settings-header-list\">

     <li class=\"im-connector-settings-header-list-item\">distribution automatique des messages entrants selon les règles de file d'attente </li>

     <li class=\"im-connector-settings-header-list-item\">interface du chat Bitrix24 familière</li>

     <li class=\"im-connector-settings-header-list-item\">toutes les conversations sont enregistrées dans l'historique du CRM</li>

    </ul>
";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CONFIGURE_CHANNEL"] = "Ouvrir les préférences du Canal ouvert";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_OPEN_LINE"] = "Canal ouvert:";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CREATE_OPEN_LINE"] = "Créer un Canal ouvert";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CONFIGURE"] = "configurer";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_NO_OPEN_LINE"] = "Vous n'avez aucun Canal ouvert dont les communications peuvent être configurées. Veuillez créer un nouveau Canal ouvert.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_NO_OPEN_LINE_AND_NOT_ADD_OPEN_LINE"] = "Vous n'avez aucun Canal ouvert dont les communications peuvent être configurées. Vous n'avez pas les autorisations suffisantes pour créer des Canaux ouverts.<br>Veuillez contacter l'administrateur de votre portail.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_POPUP_LIMITED_TITLE"] = "Canaux ouverts étendus";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_POPUP_LIMITED_TEXT"] = "Votre plan actuel limite le nombre de Canaux ouverts disponibles. Pour ajouter un autre Canal ouvert, veuillez passer à un plan supérieur. <br><br>Le plan \"Professionnal\" ne comprend aucune restriction du nombre de Canaux ouverts.";
$MESS["IIMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_ERROR_ACTION"] = "L'action a été annulée à cause d'une erreur.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CLOSE"] = "Fermer";
?>