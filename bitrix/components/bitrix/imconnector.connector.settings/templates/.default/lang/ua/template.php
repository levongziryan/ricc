<?
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CONNECT"] = "Налаштування каналу";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_LIVECHAT"] = "Класичні онлайн-консультації в чаті на сайті";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_LIVECHAT"] = "<p class=\"im-connector-settings-header-description\">Поставте безкоштовний Онлайн-чат на ваш сайт і консультує своїх клієнтів онлайн, де б ви не знаходилися.</p>
<ul class=\"im-connector-settings-header-list\">
<li class=\"im-connector-settings-header-list-item\">чат легко поставити на будь-який сайт (HTML код) </li>
<li class=\"im-connector-settings-header-list-item\">дуже просто налаштувати зовнішній вигляд і параметри показу на сайті</li>
<li class=\"im-connector-settings-header-list-item\">повідомлення від клієнтів розподіляються за правилами черги між співробітниками </li>
 <li class=\"im-connector-settings-header-list-item\">історія спілкування зберігається в CRM</li>
</ul>";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_NETWORK"] = "Об'єднує вас з клієнтами, партнерами, колегами по бізнесу";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_NETWORK"] = "<p class=\"im-connector-settings-header-description\">Підключіть Бітрікс24.Network до Відкритої лінії щоб спілкуватися з клієнтами, партнерами та колегами з інших компаній, які працюють з Бітрікс24. </p>
				<ul class=\"im-connector-settings-header-list\">
					<li class=\"im-connector-settings-header-list-item\">автоматичний розподіл звернень за правилами черги </li>
					<li class=\"im-connector-settings-header-list-item\">миттєві комунікації з клієнтами</li>
					<li class=\"im-connector-settings-header-list-item\">спілкування в звичному робочому чаті Бітрікс24</li>
				</ul>";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_VIBER"] = "Керуйте публічним чатом Viber вашій компанії";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_VIBER"] = "<p class=\"im-connector-settings-header-description\">Підключіть публічний чат Viber вашої компанії до Відкритої лінії. Ваші співробітники зможуть спілкуватися з клієнтами прямо в Бітрікс24, в одному робочому чаті. </p>
<ul class=\"im-connector-settings-header-list\">
<li class=\"im-connector-settings-header-list-item\">автоматичний розподіл звернень за правилами черги </li>
<li class=\"im-connector-settings-header-list-item\">спілкування в звичному робочому чаті Бітрікс24 </li>
<li class=\"im-connector-settings-header-list-item\">кожен клієнт зафіксований в CRM </li>
 <li class=\"im-connector-settings-header-list-item\">все листування збережене в справу</li>";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_TELEGRAMBOT"] = "Підключіть бота своєї компанії та керуйте комунікаціями всередині Бітрікс24";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_TELEGRAMBOT"] = "<p class=\"im-connector-settings-header-description\">Підключіть Telegram вашої компанії до Відкритої лінії щоб приймати звернення ваших клієнтів в робочому чаті Бітрікс24.</p>
<ul class=\"im-connector-settings-header-list\">
<li class=\"im-connector-settings-header-list-item\">миттєві комунікації з клієнтами</li>
<li class=\"im-connector-settings-header-list-item\">автоматичний розподіл звернень за правилами черги</li>
<li class=\"im-connector-settings-header-list-item\">спілкування в звичному робочому чаті Бітрікс24</li>
 <li class=\"im-connector-settings-header-list-item\">автоматичне збереження клієнтів в CRM</li>
</ul>";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_VKGROUP"] = "Відповідайте на звернення клієнтів з групи Вконтакті в робочому чаті Бітрікс24";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_VKGROUP"] = "<p class=\"im-connector-settings-header-description\">Підключіть групу вашої компанії до Відкритої лінії щоб приймати звернення клієнтів у робочому чаті Бітрікс24.</p>
<ul class=\"im-connector-settings-header-list\">
<li class=\"im-connector-settings-header-list-item\">миттєві комунікації з відвідувачами групи</li>
<li class=\"im-connector-settings-header-list-item\">спілкування в звичному робочому чаті Бітрікс24</li>
<li class=\"im-connector-settings-header-list-item\">автоматичне збереження клієнтів в CRM </li>
 <li class=\"im-connector-settings-header-list-item\">збереження всього листування у справи CRM</li>
</ul>";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_FACEBOOK"] = "Відповідайте на звернення клієнтів Facebook в робочому чаті Бітрікс24";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_FACEBOOK"] = "<p class=\"im-connector-settings-header-description\">Підключіть публічну сторінку Facebook вашої компанії до відкритої лінії та спілкуйтеся з клієнтами в звичному чаті Бітрікс24.</p>
<ul class=\"im-connector-settings-header-list\">
<li class=\"im-connector-settings-header-list-item\">миттєві комунікації з відвідувачами вашої сторінки</li>
<li class=\"im-connector-settings-header-list-item\">автоматичний розподіл звернень за правилами черги</li>
<li class=\"im-connector-settings-header-list-item\">спілкування в звичному робочому чаті Бітрікс24</li>
 <li class=\"im-connector-settings-header-list-item\">автоматичне збереження клієнтів в CRM</li>
</ul>";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_FACEBOOKCOMMENTS"] = "Керуйте публічною сторінкою Facebook в Бітрікс24";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_FACEBOOKCOMMENTS"] = "<p class=\"im-connector-settings-header-description\">Підключіть публічну сторінку Facebook вашої компанії до відкритої лінії та керуйте коментарями до записів на стіні, до фотографій, відео, не йдучи з Бітрікс24.</p>
<ul class=\"im-connector-settings-header-list\">
<li class=\"im-connector-settings-header-list-item\">миттєві комунікації з відвідувачами вашої сторінки</li>
<li class=\"im-connector-settings-header-list-item\">автоматичний розподіл звернень за правилами черги</li>
 <li class=\"im-connector-settings-header-list-item\">спілкування в звичному робочому чаті Бітрікс24</li>
<li class=\"im-connector-settings-header-list-item\">автоматичне збереження клієнтів в CRM</li>
</ul>";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_INSTAGRAM"] = "Керуйте комунікаціями з клієнтами всередині Бітрікс24";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_INSTAGRAM"] = "<p class=\"im-connector-settings-header-description\">Підключіть Instagram вашої компанії до Відкритої лінії та керуйте коментарями до фотографій, відео, не йдучи з Бітрікс24.</p>
<ul class=\"im-connector-settings-header-list\">
<li class=\"im-connector-settings-header-list-item\">кожен коментар приходить в чат Бітрікс24</li>
<li class=\"im-connector-settings-header-list-item\">всі звернення розподіляються за правилами черги</li>
<li class=\"im-connector-settings-header-list-item\">кожен клієнт зафіксований в CRM</li>
 <li class=\"im-connector-settings-header-list-item\">все листування збережене в справу</li>
</ul>";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_TITLE_BOTFRAMEWORK"] = "Об'єднайте онлайн комунікації з клієнтами в одному чаті";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_DESCRIPTION_BOTFRAMEWORK"] = "<p class=\"im-connector-settings-header-description\">Підключіть відразу кілька каналів комунікацій з клієнтами і обробляйте їх в одному чаті Бітрікс24. Skype, Slack, Kik, GroupMe, SMS, email та інші. Кожне звернення клієнта в один з каналів буде направлено в ваш Бітрікс24.</p>
<ul class=\"im-connector-settings-header-list\">
<li class=\"im-connector-settings-header-list-item\">миттєві комунікації з клієнтами</li>
<li class=\"im-connector-settings-header-list-item\">автоматичний розподіл звернень за правилами черги</li>
 <li class=\"im-connector-settings-header-list-item\">спілкування в звичному робочому чаті Бітрікс24</li>
<li class=\"im-connector-settings-header-list-item\">автоматичне збереження клієнтів в CRM</li>
</ul>";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CONFIGURE_CHANNEL"] = "Налаштування каналу";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_OPEN_LINE"] = "Відкрита лінія:";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CREATE_OPEN_LINE"] = "Створити відкриту лінію";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CONFIGURE"] = "налаштувати";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_NO_OPEN_LINE"] = "У вас немає жодної відкритої лінії, у якій ви можете налаштувати канали комунікацій. Будь ласка, створіть нову відкриту лінію.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_NO_OPEN_LINE_AND_NOT_ADD_OPEN_LINE"] = "У вас немає жодної відкритої лінії, у якій ви можете налаштувати канали комунікацій, і у вас немає прав на створення нової відкритої лінії.<br>Зверніться до адміністратора порталу.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_POPUP_LIMITED_TITLE"] = "Розширені відкриті лінії";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_POPUP_LIMITED_TEXT"] = "На вашому тарифному плані є обмеження по кількості Відкритих ліній. Щоб додати ще одну лінію (наприклад, для створення декількох рівнів підтримки клієнтів), перейдіть на інший тарифний план.
<br><br>
У верхньому тарифі \"Компанія\" число активних відкритих ліній необмежене.";
$MESS["IIMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_ERROR_ACTION"] = "Виникла помилка. Дію скасовано.";
$MESS["IMCONNECTOR_COMPONENT_CONNECTOR_SETTINGS_CLOSE"] = "Закрити";
?>