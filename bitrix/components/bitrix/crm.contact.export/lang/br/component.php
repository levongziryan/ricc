<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O Módulo de CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso Negado";
$MESS["CRM_COLUMN_NAME"] = "Nome";
$MESS["CRM_COLUMN_LAST_NAME"] = "Sobrenome";
$MESS["CRM_COLUMN_SECOND_NAME"] = "Segundo Nome";
$MESS["CRM_COLUMN_BIRTHDATE"] = "Aniversário";
$MESS["CRM_COLUMN_TITLE"] = "Título";
$MESS["CRM_COLUMN_PHONE"] = "Telefone";
$MESS["CRM_COLUMN_WEB"] = "Site";
$MESS["CRM_COLUMN_MESSENGER"] = "Messenger";
$MESS["CRM_COLUMN_POST"] = "Cargo";
$MESS["CRM_COLUMN_ADDRESS"] = "Endereço";
$MESS["CRM_COLUMN_COMMENT"] = "Observação";
$MESS["CRM_COLUMN_TYPE"] = "Tipo de Contato";
$MESS["CRM_COLUMN_SOURCE"] = "Fonte";
$MESS["CRM_COLUMN_ASSIGNED_BY"] = "Responsável";
$MESS["CRM_COLUMN_DATE_CREATE"] = "Criado em";
$MESS["CRM_COLUMN_DATE_MODIFY"] = "Modificado em";
?>