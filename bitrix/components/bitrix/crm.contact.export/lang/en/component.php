<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "The CRM module is not installed.";
$MESS["CRM_PERMISSION_DENIED"] = "Access Denied";
$MESS["CRM_COLUMN_NAME"] = "First Name";
$MESS["CRM_COLUMN_LAST_NAME"] = "Last Name";
$MESS["CRM_COLUMN_SECOND_NAME"] = "Second Name";
$MESS["CRM_COLUMN_BIRTHDATE"] = "Birthday";
$MESS["CRM_COLUMN_TITLE"] = "Title";
$MESS["CRM_COLUMN_PHONE"] = "Phone";
$MESS["CRM_COLUMN_WEB"] = "Site";
$MESS["CRM_COLUMN_MESSENGER"] = "Messenger";
$MESS["CRM_COLUMN_POST"] = "Position";
$MESS["CRM_COLUMN_ADDRESS"] = "Address";
$MESS["CRM_COLUMN_COMMENT"] = "Comment";
$MESS["CRM_COLUMN_TYPE"] = "Contact Type";
$MESS["CRM_COLUMN_SOURCE"] = "Source";
$MESS["CRM_COLUMN_ASSIGNED_BY"] = "Responsible";
$MESS["CRM_COLUMN_DATE_CREATE"] = "Created On";
$MESS["CRM_COLUMN_DATE_MODIFY"] = "Modified On";
?>