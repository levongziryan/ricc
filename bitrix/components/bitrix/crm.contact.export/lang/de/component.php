<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Das CRM-Modul ist nicht installiert.";
$MESS["CRM_PERMISSION_DENIED"] = "Zugriff verweigert";
$MESS["CRM_COLUMN_NAME"] = "Vorname";
$MESS["CRM_COLUMN_LAST_NAME"] = "Nachname";
$MESS["CRM_COLUMN_SECOND_NAME"] = "Zweiter Vorname";
$MESS["CRM_COLUMN_BIRTHDATE"] = "Geburtstag";
$MESS["CRM_COLUMN_TITLE"] = "Überschrift";
$MESS["CRM_COLUMN_PHONE"] = "Telefon";
$MESS["CRM_COLUMN_EMAIL"] = "E-Mail";
$MESS["CRM_COLUMN_WEB"] = "Website";
$MESS["CRM_COLUMN_MESSENGER"] = "Messenger";
$MESS["CRM_COLUMN_POST"] = "Position";
$MESS["CRM_COLUMN_ADDRESS"] = "Adresse";
$MESS["CRM_COLUMN_COMMENT"] = "Kommentar";
$MESS["CRM_COLUMN_TYPE"] = "Kontakttyp";
$MESS["CRM_COLUMN_SOURCE"] = "Quelle";
$MESS["CRM_COLUMN_ASSIGNED_BY"] = "Verantwortlich";
$MESS["CRM_COLUMN_DATE_CREATE"] = "Erstellt am";
$MESS["CRM_COLUMN_DATE_MODIFY"] = "Geändert am";
?>