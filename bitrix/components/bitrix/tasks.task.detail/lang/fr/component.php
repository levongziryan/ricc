<?
$MESS["TASKS_DATE_MUST_BE_IN_FUTURE"] = "La date du rappel doit être dans le futur.";
$MESS["TASKS_TASK_NUM"] = "Tâche ##TASK_NUM#";
$MESS["TASKS_TASK_NOT_FOUND"] = "La tâche n'est pas trouvée et l'accès à lui est interdit.";
$MESS["SOCNET_MODULE_NOT_INSTALLED"] = "Le module du réseau social n'a pas été installé.";
$MESS["TASKS_MODULE_NOT_INSTALLED"] = "Le module 'Tâches' n'a pas été installé.";
$MESS["FORUM_MODULE_NOT_INSTALLED"] = "Le forum sera supprimé de façon irréversible. Continuer?";
$MESS["TASKS_BAD_TASK_ID"] = "Identificateur de tâche non valide.";
$MESS["TASKS_TASK_UNABLE_TO_DELETE"] = "Chec de suppression de la tâche";
?>