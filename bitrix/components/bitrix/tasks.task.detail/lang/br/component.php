<?
$MESS["TASKS_MODULE_NOT_INSTALLED"] = "O módulo Tarefas não está instalado.";
$MESS["FORUM_MODULE_NOT_INSTALLED"] = "O módulo do Fórum não está instalado.";
$MESS["SOCNET_MODULE_NOT_INSTALLED"] = "O módulo \"Rede Social\" não está instalado.";
$MESS["TASKS_BAD_TASK_ID"] = "ID da tarefa invalida";
$MESS["TASKS_TASK_NOT_FOUND"] = "A tarefa não foi encontrada, ou o acesso foi negado.";
$MESS["TASKS_TASK_NUM"] = "Tarefa ##TASK_NUM#";
$MESS["TASKS_DATE_MUST_BE_IN_FUTURE"] = "A data do lembrete deve ser posterior a data presente.";
$MESS["TASKS_TASK_UNABLE_TO_DELETE"] = "Não foi possível excluir a tarefa";
?>