<?
$MESS["P_ALBUM_WIDTH"] = "Largeur de la photo d'album (px)";
$MESS["P_DATE_FORMAT"] = "Format d'affichage de la date";
$MESS["T_IBLOCK_DESC_NEWS_PANEL"] = "Boutons du panneau d'affichage pour ce composant";
$MESS["IBLOCK_IBLOCK"] = "Bloc d'information";
$MESS["IBLOCK_SECTION_EDIT_URL"] = "URL de la page d'édition de section";
$MESS["IBLOCK_TYPE"] = "Type de bloc d'information";
$MESS["IBLOCK_DETAIL_URL"] = "URL de la page des contenus de détail";
$MESS["IBLOCK_SECTION_URL"] = "URL de la page des contenus de section";
$MESS["T_IBLOCK_DESC_USE_PERMISSIONS"] = "Utiliser les restrictions d'accès supplémentaires";
$MESS["T_IBLOCK_DESC_GROUP_PERMISSIONS"] = "Les groupes d'utilisateur sont autorisés à voir la description détaillée";
?>