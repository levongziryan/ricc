<?
$MESS["BPWC_WNCT_2LIST"] = "Lojas";
$MESS["BPWC_WNCT_TAB1"] = "Loja";
$MESS["BPWC_WNCT_TAB1T"] = "Editar parâmetros";
$MESS["BPWC_WNCT_DATE_CREATE"] = "Criado em";
$MESS["BPWC_WNCT_DATE_UPDATE"] = "ltima importação em";
$MESS["BPWC_WNCT_NAME"] = "Nome";
$MESS["BPWC_WNCT_ACTIVE"] = "Ativo";
$MESS["BPWC_WNCT_URL"] = "URL";
$MESS["BPWC_WNCT_LOGIN"] = "Login";
$MESS["BPWC_WNCT_PASSWORD"] = "Senha";
$MESS["BPWC_WNCT_SIZE"] = "Número máximo de pedidos por upload";
$MESS["BPWC_WNCT_IMPORT_PERIOD"] = "Carregar período (dias)";
$MESS["BPWC_WNCT_STATUS"] = "Status de última sincronização";
$MESS["BPWC_WNCT_LABEL"] = "Marca de modificação";
$MESS["BPWC_WNCT_NOTE_HINT"] = "Especifique o login e a senha de um usuário da loja com permissão para editar pedidos em todos os status e para importar e exportar.";
$MESS["BPWC_WNCT_DATA_SYNC_PERIOD"] = "Período de sincronização (minutos, coloque 0 para desativar)";
$MESS["BPWC_WNCT_2SYNC"] = "Importar";
$MESS["BPWC_WNCT_TAB2"] = "Configurações";
$MESS["BPWC_WNCT_TAB2T"] = "Configurações adicionais";
$MESS["BPWC_WNCT_IMPORT_PROBABILITY"] = "Probabilidade padrão de negócio (%)";
$MESS["BPWC_WNCT_IMPORT_RESPONSIBLE"] = "Padrão de responsável";
$MESS["BPWC_WNCT_IMPORT_PUBLIC"] = "O negócio é público por padrão";
$MESS["BPWC_WNCT_IMPORT_PREFIX"] = "Prefixo do nome do negócio";
$MESS["BPWC_WLC_NEED_FIRST_SYNC1"] = "Importação precisa ser executado manualmente pela primeira vez.";
$MESS["BPWC_WLC_NEED_FIRST_SYNC1_DO"] = "Iniciar importação";
$MESS["BPWC_WLC_NEED_FIRST_SYNC2"] = "Em andamento";
$MESS["BPWC_WLC_NEED_FIRST_SYNC3"] = "Erro de importação:";
$MESS["BPWC_WNCT_LAST_STATUS_DATE"] = "ltima sincronização em";
$MESS["BPWC_WNCT_IMPORT_GROUP_ID"] = "Notificar grupo";
$MESS["BPWC_WNCT_DO_SELECT"] = "Selecionar";
?>