<?
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_DEL_REFERENCE"] = "Dissocier";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_STEP_1_OF_3_TITLE"] = "Se connecter";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_STEP_2_OF_3_TITLE"] = "Choisir la page";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_STEP_3_OF_3_TITLE"] = "Terminer";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_STEP_N_OF_3"] = "étape #STEP# de 3";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_PAGE"] = "Page";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_DESCRIPTION"] = "Connectez la page Facebook de votre entreprise à un Canal ouvert Bitrix24 et commencez à communiquer avec vos clients via le chat Bitrix24.   Vous devez créer la page Facebook de votre entreprise ou en utiliser une qui existe déjà. Vous devez être un administrateur de cette page.   ";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECT_PAGE"] = "Connecter la page";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_PAGE"] = "Veuillez procéder à l'autorisation via un compte Facebook qui gère vos pages pour recevoir les messages de vos clients Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_EXPIRED_ACCOUNT_TOKEN"] = "Vous devez être autorisé à modifier les paramètres avec votre compte Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_EXPIRED_ACCOUNT_TOKEN_WARNING"] = "Veuillez noter que lorsque vous autorisez un compte Facebook qui n'est pas l'administrateur de la page actuelle, celui-ci sera déconnecté de votre Canal ouvert Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_THERE_IS_NO_PAGE_WHERE_THE_ADMINISTRATOR"] = "Vous n'êtes administrateur d'aucune page Facebook.<br>Vous pouvez dès maintenant créer votre page Facebook. ";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TO_CREATE_A_PAGE"] = "Créer";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECTED_PAGE"] = "Page connectée";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CHANGE_PAGE"] = "Modifier";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_MY_OTHER_PAGES"] = "Mes autres pages Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_SELECT_THE_PAGE"] = "Veuillez choisir la page Facebook devant être connectée à votre Canal ouvert Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OTHER_PAGES"] = "Autres pages";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_PAGE_IM"] = "Messagerie";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_REPEATING_ERROR"] = "Si le problème persiste, envisagez de débrancher le canal et de le configurer une nouvelle fois.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECTOR_ERROR_STATUS"] = "Une erreur est survenue. Veuillez vérifier vos paramètres.";
?>