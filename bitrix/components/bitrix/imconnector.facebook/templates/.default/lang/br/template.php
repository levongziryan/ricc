<?
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_DEL_REFERENCE"] = "Desvincular";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_STEP_1_OF_3_TITLE"] = "Fazer login";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_STEP_2_OF_3_TITLE"] = "Selecionar página";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_STEP_3_OF_3_TITLE"] = "Concluir";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_STEP_N_OF_3"] = "etapa #STEP# de 3";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_PAGE"] = "Página";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INDEX_DESCRIPTION"] = "Conecte a página do Facebook de sua empresa ao Canal Aberto Bitrix24 e comece a se comunicar com seus clientes pelo bate-papo Bitrix24.   Você precisa para criar a página do Facebook de sua empresa ou utilizar a existente. Você precisa ser o administrador desta página.   ";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECT_PAGE"] = "Conectar página";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_PAGE"] = "Autorize a conta do Facebook que gerencia suas páginas para receber mensagens dos seus clientes Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_EXPIRED_ACCOUNT_TOKEN"] = "Você precisa para ser autorizado por sua conta do Facebook para modificar as configurações";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_EXPIRED_ACCOUNT_TOKEN_WARNING"] = "Observe que, quando você autorizar com uma conta do Facebook a qual não administra a página atual, ela será desconectada do seu Canal Aberto Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_THERE_IS_NO_PAGE_WHERE_THE_ADMINISTRATOR"] = "Você não administra nenhuma página do Facebook.<br>Você pode criar sua página do Facebook agora mesmo. ";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_TO_CREATE_A_PAGE"] = "Criar";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECTED_PAGE"] = "Página conectada";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CHANGE_PAGE"] = "Alterar";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_MY_OTHER_PAGES"] = "Minhas outras páginas do Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_SELECT_THE_PAGE"] = "Escolha a página do Facebook que deve ser conectada ao seu Canal Aberto Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OTHER_PAGES"] = "Outras páginas";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_PAGE_IM"] = "Messenger";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_REPEATING_ERROR"] = "Se o problema persistir, talvez você precise desconectar o canal e configurá-lo novamente.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECTOR_ERROR_STATUS"] = "Ocorreu um erro. Verifique suas configurações.";
?>