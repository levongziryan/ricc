<?
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_MODULE_NOT_INSTALLED"] = "Le module \"Connecteurs de messagerie instantanée externe\" n'est pas installé.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_NO_ACTIVE_CONNECTOR"] = "Ce connecteur est inactif.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_ERROR_REQUEST_INFORMATION_FROM_SERVER"] = "Erreur d'inscription";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_SESSION_HAS_EXPIRED"] = "Votre session a expiré. Veuillez renvoyer le formulaire.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_REMOVED_REFERENCE_TO_PAGE"] = "Ce connecteur a été configuré pour être utilisé avec un groupe de travail auquel vous n'avez actuellement pas d'accès administrateur.
Veuillez reconfigurer le connecteur.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OK_DEL_USER"] = "Votre compte utilisateur a été dissocié";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_NO_DEL_USER"] = "Impossible de dissocier votre compte utilisateur";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OK_DEL_PAGE"] = "La page a été dissociée";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_NO_DEL_PAGE"] = "Impossible de dissocier la page";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_OK_AUTHORIZATION_PAGE"] = "La page a été liée";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_NO_AUTHORIZATION_PAGE"] = "Impossible de lier la page ";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_CONNECTOR_ERROR_STATUS"] = "Une erreur est survenue. Veuillez vérifier vos paramètres.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_INVALID_OAUTH_ACCESS_TOKEN"] = "Impossible de gérer la page publique, l'accès a été perdu. Vous devez vous reconnecter à votre page publique.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_REPEATING_ERROR"] = "Si le problème persiste, envisagez de déconnecter le canal et de le configurer une nouvelle fois.";
?>