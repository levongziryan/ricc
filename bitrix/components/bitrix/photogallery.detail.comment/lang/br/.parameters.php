<?
$MESS["IBLOCK_TYPE"] = "Tipo de bloco de informações";
$MESS["IBLOCK_IBLOCK"] = "Bloco de informações";
$MESS["IBLOCK_DETAIL_URL"] = "Página de visualização detalhada";
$MESS["IBLOCK_ELEMENT_ID"] = "ID do elemento";
$MESS["P_COMMENTS_TYPE"] = "Componente de comentários";
$MESS["P_COMMENTS_TYPE_BLOG"] = "Blog";
$MESS["P_COMMENTS_TYPE_FORUM"] = "Fórum";
$MESS["F_COMMENTS_COUNT"] = "Comentários por página";
$MESS["F_BLOG_URL"] = "Comentários ";
$MESS["P_PATH_TO_USER"] = "Caminho para o perfil do usuário";
$MESS["P_PATH_TO_BLOG"] = "Caminho para o blog";
$MESS["F_PATH_TO_SMILE"] = "Caminho para a pasta de Smiles (relativo à raiz)";
$MESS["F_FORUM_ID"] = "ID do fórum";
$MESS["F_USE_CAPTCHA"] = "Utilizar CAPTCHA";
$MESS["F_READ_TEMPLATE"] = "Página de leitura de tópicos";
$MESS["F_PREORDER"] = "Exibir mensagens na ordem cronológica";
?>