<?
$MESS["CRM_WEBFORM_EDIT_HINT_ANY"] = "* - l'astérisque correspond à n'importe quel nombre de n'importe quel type de caractères.
Exemples:
- Toutes les pages dans /catalogue/ répertoire sur www.example.com
http://www.example.com/catalogue/*

- N'importe quelle page sur n'importe quel site ayant /panier/ dans le chemin d'accès
*/panier/*

- N'importe quelle page sur n'importe quel site ayant les sous-liens \"utm_source\" et \"utm_company\" dans le chemin d'accès
*utm_source*utm_company*

- Toutes les pages dans /catalogue/ répertoire sur tous les sous-domaines de example.com
http://*.example.com/catalogue/*";
$MESS["CRM_WEBFORM_EDIT_DESC"] = "Choisissez les canaux pour communiquer avec vos clients. Vous pouvez activer ou désactiver les canaux si nécessaire.
Personnalisez le design et la disposition du widget.
Le code du widget sera disponible après avoir enregistré le formulaire.";
$MESS["CRM_WEBFORM_EDIT_CHANNELS"] = "Canaux connectés";
$MESS["CRM_WEBFORM_EDIT_SELECT_BACK_TO_LIST"] = "Retour";
$MESS["CRM_WEBFORM_EDIT_COPY_TO_CLIPBOARD"] = "Copier dans le presse-papiers";
$MESS["CRM_WEBFORM_EDIT_SITE_SCRIPT"] = "Code du widget";
$MESS["CRM_WEBFORM_EDIT_SHOW_DELAY"] = "Affficher le widget";
$MESS["CRM_WEBFORM_EDIT_SHOW_DELAY_AT_ONCE"] = "immédiatement après le chargement de la page";
$MESS["CRM_WEBFORM_EDIT_SHOW_DELAY_DELAY"] = "introduire un délai";
$MESS["CRM_WEBFORM_EDIT_SHOW_CHOOSE_LOCATION"] = "Choisir la position";
$MESS["CRM_WEBFORM_EDIT_SHOW_VIEW"] = "Afficher";
$MESS["CRM_BUTTON_EDIT_COLOR_BG"] = "Couleur d'arrière-plan";
$MESS["CRM_BUTTON_EDIT_COLOR_ICON"] = "Couleur d'icône";
$MESS["CRM_WEBFORM_EDIT_SELECT_BACK_TO_BUTTON_LIST"] = "Retour aux widgets";
$MESS["CRM_WEBFORM_EDIT_SELECT_BACK_TO_BUTTON_LIST1"] = "retour à la liste";
$MESS["CRM_WEBFORM_EDIT_SHOW_ON_ALL_PAGES"] = "sur toutes les pages sauf";
$MESS["CRM_WEBFORM_EDIT_SHOW_ONLY_PAGES"] = "sur les pages sélectionnées uniquement";
$MESS["CRM_WEBFORM_EDIT_PAGE_SETTINGS_USER"] = "paramètres personnalisés";
$MESS["CRM_WEBFORM_EDIT_PAGE_SETTINGS_DEFAULT"] = "sur toutes les pages par défaut";
$MESS["CRM_WEBFORM_EDIT_PAGE_SETTINGS_SETUP"] = "configurer";
$MESS["CRM_WEBFORM_EDIT_PAGE_SETTINGS"] = "Paramètres d'affichage du widget";
$MESS["CRM_WEBFORM_EDIT_CHANNEL_EDIT"] = "configurer";
$MESS["CRM_WEBFORM_EDIT_CHANNEL_ADD"] = "créer";
$MESS["CRM_WEBFORM_EDIT_CHANNEL_SETUP"] = "Paramètres";
$MESS["CRM_WEBFORM_EDIT_CHANNEL_ADD_DESC"] = "Aucun canal n'est actif pour le moment. Ouvrez les préférences pour activer les canaux.";
$MESS["CRM_WEBFORM_EDIT_ON"] = "on";
$MESS["CRM_WEBFORM_EDIT_OFF"] = "off";
$MESS["CRM_WEBFORM_EDIT_NAME_PLACEHOLDER"] = "Nom du widget";
$MESS["CRM_WEBFORM_EDIT_SAVE"] = "Enregistrer";
$MESS["CRM_WEBFORM_EDIT_APPLY"] = "Appliquer";
$MESS["CRM_WEBFORM_EDIT_SITE_SCRIPT_TIP"] = "Copiez le code et collez-le dans le code du template de votre site avant la balise de fermeture &lt;/body&gt;.";
$MESS["CRM_WEBFORM_EDIT_SITE_HELLO_TITLE"] = "Message de bienvenue automatique";
$MESS["CRM_WEBFORM_EDIT_EDIT"] = "Modifier";
$MESS["CRM_WEBFORM_EDIT_HELLO_TITLE"] = "Message de bienvenue automatique";
$MESS["CRM_WEBFORM_EDIT_HELLO_DESC"] = "Sélectionnez les pages qui afficheront le message de bienvenue à vos clients. Téléchargez une image et entrez le texte afin d'attirer plus de clients.";
$MESS["CRM_WEBFORM_EDIT_HELLO_TUNE"] = "Configurer un message de bienvenue";
$MESS["CRM_WEBFORM_EDIT_HELLO_TIME_DELAY"] = "Afficher le délai";
$MESS["CRM_WEBFORM_EDIT_HELLO_TIME_DELAY_TIP"] = "Indique le temps mis par le message de bienvenue pour apparaître une fois que le widget est visible.";
$MESS["CRM_WEBFORM_EDIT_HELLO_TIME_DELAY_NO"] = "afficher immédiatement";
$MESS["CRM_WEBFORM_EDIT_HELLO_PAGES_LIST"] = "Afficher le message de bienvenue sur ces pages";
$MESS["CRM_WEBFORM_EDIT_HELLO_PAGES_EXCLUDE"] = "Excepté ces pages";
$MESS["CRM_WEBFORM_EDIT_HELLO_PAGES_EXCLUDE_ADDITIONAL"] = "et les pages avec des messages configurés individuellement";
$MESS["CRM_WEBFORM_EDIT_HELLO_MODE"] = "Mode d'affichage du message de bienvenue";
$MESS["CRM_WEBFORM_EDIT_HELLO_MODE_EXCLUDE"] = "Sur toutes les pages";
$MESS["CRM_WEBFORM_EDIT_HELLO_MODE_INCLUDE"] = "Sur les pages sélectionnées uniquement";
$MESS["CRM_WEBFORM_EDIT_SECTION_ALL"] = "Message de bienvenue par défaut pour toutes les pages";
$MESS["CRM_WEBFORM_EDIT_SECTION_CUSTOM"] = "Messages de bienvenue personnalisés";
$MESS["CRM_WEBFORM_EDIT_HELLO_ADD"] = "ajouter un message de bienvenue";
$MESS["CRM_WEBFORM_EDIT_HELLO_CHANGE"] = "modifier";
$MESS["CRM_WEBFORM_EDIT_HELLO_DEF_NAME"] = "Lisa Smith";
$MESS["CRM_WEBFORM_EDIT_HELLO_DEF_TEXT"] = "Bonjour! Je suis votre assistant personnel. Écrivez-moi si vous avez des questions.";
$MESS["CRM_WEBFORM_EDIT_REMOVE_LOGO"] = "enlever la signature";
$MESS["CRM_WEBFORM_EDIT_REMOVE_LOGO_BX"] = "Fourni par Bitrix24";
$MESS["CRM_WEBFORM_EDIT_POPUP_LIMITED_TITLE"] = "CRM étendu";
$MESS["CRM_WEBFORM_EDIT_POPUP_LIMITED_TEXT"] = "La signature peut être enlevée uniquement dans les abonnements commerciaux.";
$MESS["CRM_BUTTON_EDIT_CLOSE"] = "Fermer";
$MESS["CRM_BUTTON_EDIT_ERROR_ACTION"] = "L'action a été annulée à cause d'une erreur.";
$MESS["CRM_BUTTON_EDIT_DELETE_CONFIRM"] = "Voulez-vous supprimer cette photo ?";
$MESS["CRM_BUTTON_EDIT_AVATAR_LOADED"] = "Photos téléchargées";
$MESS["CRM_BUTTON_EDIT_AVATAR_PRESET"] = "Classique";
$MESS["CRM_BUTTON_EDIT_WORK_TIME"] = "Préférences du temps de travail";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_DISABLED"] = "non configuré";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_ENABLED"] = "Afficher uniquement pendant les heures de travail";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_TIME_ZONE"] = "Choisir le fuseau horaire";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_TIME"] = "Heures de travail";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_DAY_OFF"] = "Week-ends";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_HOLIDAYS"] = "Jours fériés";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_HOLIDAYS_EXAMPLE"] = "Exemple : 01.01,04.07,01.11,25.12";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_ACTION_RULE"] = "Traitement des appels en dehors des heures de travail";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_ACTION_TEXT"] = "Message";
$MESS["CRM_BUTTON_EDIT_DETAIL_OPENLINE_CHANNELS"] = "canaux connectés";
$MESS["CRM_BUTTON_EDIT_DETAIL_CRMFORM_FIELDS"] = "champs du formulaire";
$MESS["CRM_BUTTON_EDIT_DETAIL_CALLBACK_PHONE_NUMBER"] = "numéro de téléphone";
$MESS["CRM_BUTTON_EDIT_LANG_CHOOSE"] = "Sélectionner la langue";
$MESS["CRM_BUTTON_EDIT_LANG_CHOOSE_TIP"] = "Attention ! Changer la langue ne traduira pas le texte personnalisé et les étiquettes que vous avez entrés manuellement en créant le widget. Vous pouvez modifier ce texte dans le formulaire CRM, le Canal ouvert ou sur les pages de paramètres respectives de Rappel.";
$MESS["CRM_BUTTON_EDIT_MOBILE_DEVICES"] = "Appareils mobiles";
$MESS["CRM_BUTTON_EDIT_DO_NOT_SHOW"] = "masquer sur mobile";
$MESS["CRM_BUTTON_EDIT_OPENLINE_CHOISE_CHANNEL_HINT"] = "sélectionnez les sources que vous souhaitez voir dans le widget. Les widgets ne peuvent afficher qu’une seule source d’un type donné : par exemple, une source Facebook, une source Viber etc.";
$MESS["CRM_BUTTON_EDIT_OPENLINE_ANOTHER_CHANNELS"] = "Canaux supplémentaires";
$MESS["CRM_BUTTON_EDIT_OPENLINE_USE_MULTI_LINES"] = "connecter les sources d'autres Canaux ouverts";
$MESS["CRM_BUTTON_EDIT_OPENLINE_ADD"] = "ajouter un Canal ouvert";
$MESS["CRM_BUTTON_EDIT_OPENLINE_REMOVE"] = "déconnecter";
$MESS["CRM_BUTTON_EDIT_OPENLINE_ANOTHER_CHANNELS_EMPTY"] = "Vous pouvez créer des Canaux ouverts et connecter les sources existantes de Canaux ouverts .";
$MESS["CRM_BUTTON_EDIT_OPENLINE_MULTI_POPUP_LIMITED_TEXT"] = "Des Canaux ouverts supplémentaires sont disponibles uniquement dans les abonnements commerciaux.";
?>