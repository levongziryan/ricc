<?
$MESS["CRM_WEBFORM_EDIT_HINT_ANY"] = "* - asterisco corresponde a qualquer número de qualquer caracter.
Exemplos:
- Todas as páginas no diretório /catalog/ em www.example.com
http://www.example.com/catalog/*

- Qualquer página em qualquer site que tenha /cesta/ no caminho
 */basket/*

- Qualquer página em qualquer site que tenha a sequência \"utm_source\" e \"utm_company\" no caminho *utm_source*utm_company*

 - Todas as páginas no diretório /catalog/ em todos os subdomínios de exemplo.com
 http://*.example.com/catalog/*";
$MESS["CRM_WEBFORM_EDIT_DESC"] = "Escolha canais para se comunicar com seus clientes. Você pode ativar ou desativar os canais conforme necessário.
Personalizar o widget de design e layout.
O código do widget estará disponível depois que você salvar o formulário.";
$MESS["CRM_WEBFORM_EDIT_CHANNELS"] = "Canais conectados";
$MESS["CRM_WEBFORM_EDIT_SELECT_BACK_TO_LIST"] = "Voltar";
$MESS["CRM_WEBFORM_EDIT_COPY_TO_CLIPBOARD"] = "Copiar para Transferência";
$MESS["CRM_WEBFORM_EDIT_SITE_SCRIPT"] = "Código do widget";
$MESS["CRM_WEBFORM_EDIT_SHOW_DELAY"] = "Mostrar widget";
$MESS["CRM_WEBFORM_EDIT_SHOW_DELAY_AT_ONCE"] = "imediatamente após carregar a página";
$MESS["CRM_WEBFORM_EDIT_SHOW_DELAY_DELAY"] = "introduzir atraso";
$MESS["CRM_WEBFORM_EDIT_SHOW_CHOOSE_LOCATION"] = "Escolher posição";
$MESS["CRM_WEBFORM_EDIT_SHOW_VIEW"] = "Visualizar";
$MESS["CRM_BUTTON_EDIT_COLOR_BG"] = "Cor de fundo";
$MESS["CRM_BUTTON_EDIT_COLOR_ICON"] = "Cor do ícone";
$MESS["CRM_WEBFORM_EDIT_SELECT_BACK_TO_BUTTON_LIST"] = "Voltar aos widgets";
$MESS["CRM_WEBFORM_EDIT_SELECT_BACK_TO_BUTTON_LIST1"] = "voltar para a lista";
$MESS["CRM_WEBFORM_EDIT_SHOW_ON_ALL_PAGES"] = "em todas as páginas exceto";
$MESS["CRM_WEBFORM_EDIT_SHOW_ONLY_PAGES"] = "apenas nas páginas selecionadas";
$MESS["CRM_WEBFORM_EDIT_PAGE_SETTINGS_USER"] = "configurações personalizadas";
$MESS["CRM_WEBFORM_EDIT_PAGE_SETTINGS_DEFAULT"] = "em todas as páginas por padrão";
$MESS["CRM_WEBFORM_EDIT_PAGE_SETTINGS_SETUP"] = "configurar";
$MESS["CRM_WEBFORM_EDIT_PAGE_SETTINGS"] = "Configurações de exibição do widget";
$MESS["CRM_WEBFORM_EDIT_CHANNEL_EDIT"] = "configurar";
$MESS["CRM_WEBFORM_EDIT_CHANNEL_ADD"] = "criar";
$MESS["CRM_WEBFORM_EDIT_CHANNEL_SETUP"] = "Configurações";
$MESS["CRM_WEBFORM_EDIT_CHANNEL_ADD_DESC"] = "Atualmente não há canais ativos. Abrir preferências para ativar canais.";
$MESS["CRM_WEBFORM_EDIT_ON"] = "ativado";
$MESS["CRM_WEBFORM_EDIT_OFF"] = "desativado";
$MESS["CRM_WEBFORM_EDIT_NAME_PLACEHOLDER"] = "Nome do widget";
$MESS["CRM_WEBFORM_EDIT_SAVE"] = "Salvar";
$MESS["CRM_WEBFORM_EDIT_APPLY"] = "Aplicar";
$MESS["CRM_WEBFORM_EDIT_SITE_SCRIPT_TIP"] = "Copie o código e cole-o no seu código de modelo de site antes de &lt;/body&gt; fechar a etiqueta.";
$MESS["CRM_WEBFORM_EDIT_SITE_HELLO_TITLE"] = "Mensagem automática de boas-vindas";
$MESS["CRM_WEBFORM_EDIT_EDIT"] = "Editar";
$MESS["CRM_WEBFORM_EDIT_HELLO_TITLE"] = "Mensagem automática de boas-vindas";
$MESS["CRM_WEBFORM_EDIT_HELLO_DESC"] = "Selecionar as páginas que irão mostrar a mensagem de boas-vindas aos seus clientes. Faça o upload de uma imagem e digite o texto para atrair mais clientes.";
$MESS["CRM_WEBFORM_EDIT_HELLO_TUNE"] = "Configurar mensagem de boas-vindas";
$MESS["CRM_WEBFORM_EDIT_HELLO_TIME_DELAY"] = "Mostrar adiamento";
$MESS["CRM_WEBFORM_EDIT_HELLO_TIME_DELAY_TIP"] = "Especificar o tempo de espera antes de exibir a mensagem de boas-vindas após o widget se tornar visível.";
$MESS["CRM_WEBFORM_EDIT_HELLO_TIME_DELAY_NO"] = "mostrar imediatamente";
$MESS["CRM_WEBFORM_EDIT_HELLO_PAGES_LIST"] = "Mostrar mensagem de boas-vindas nestas páginas";
$MESS["CRM_WEBFORM_EDIT_HELLO_PAGES_EXCLUDE"] = "Com exceção destas páginas";
$MESS["CRM_WEBFORM_EDIT_HELLO_PAGES_EXCLUDE_ADDITIONAL"] = "e páginas com mensagens configuradas individualmente";
$MESS["CRM_WEBFORM_EDIT_HELLO_MODE"] = "Modo de exibição de mensagem de boas-vindas";
$MESS["CRM_WEBFORM_EDIT_HELLO_MODE_EXCLUDE"] = "Em todas as páginas";
$MESS["CRM_WEBFORM_EDIT_HELLO_MODE_INCLUDE"] = "Apenas nas páginas selecionadas";
$MESS["CRM_WEBFORM_EDIT_SECTION_ALL"] = "Mensagem de boas-vindas padrão para todas as páginas";
$MESS["CRM_WEBFORM_EDIT_SECTION_CUSTOM"] = "Mensagens de boas-vindas personalizadas";
$MESS["CRM_WEBFORM_EDIT_HELLO_ADD"] = "adicionar adicionar mensagem de boas-vindas";
$MESS["CRM_WEBFORM_EDIT_HELLO_CHANGE"] = "alterar";
$MESS["CRM_WEBFORM_EDIT_HELLO_DEF_NAME"] = "Lisa Smith";
$MESS["CRM_WEBFORM_EDIT_HELLO_DEF_TEXT"] = "Olá! Sou seu ajudante pessoal. Escreva-me sempre que você tiver dúvidas.";
$MESS["CRM_WEBFORM_EDIT_REMOVE_LOGO"] = "remover assinatura";
$MESS["CRM_WEBFORM_EDIT_REMOVE_LOGO_BX"] = "Fornecido por Bitrix24";
$MESS["CRM_WEBFORM_EDIT_POPUP_LIMITED_TITLE"] = "CRM estendido";
$MESS["CRM_WEBFORM_EDIT_POPUP_LIMITED_TEXT"] = "A assinatura pode ser removida somente em planos comerciais.";
$MESS["CRM_BUTTON_EDIT_CLOSE"] = "Fechar";
$MESS["CRM_BUTTON_EDIT_ERROR_ACTION"] = "A ação foi cancelada porque ocorreu um erro.";
$MESS["CRM_BUTTON_EDIT_DELETE_CONFIRM"] = "Você deseja excluir esta foto?";
$MESS["CRM_BUTTON_EDIT_AVATAR_LOADED"] = "Fotos carregadas";
$MESS["CRM_BUTTON_EDIT_AVATAR_PRESET"] = "Padrão";
$MESS["CRM_BUTTON_EDIT_WORK_TIME"] = "Preferências de horário de trabalho";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_DISABLED"] = "não configurado";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_ENABLED"] = "Mostrar somente durante o horário de trabalho";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_TIME_ZONE"] = "Selecionar fuso horário";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_TIME"] = "Horários de trabalho";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_DAY_OFF"] = "Finais de semana";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_HOLIDAYS"] = "Feriados";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_HOLIDAYS_EXAMPLE"] = "Exemplo: 01.01,04.07,01.11,25.12";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_ACTION_RULE"] = "Processando chamadas fora do horário de expediente";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_ACTION_TEXT"] = "Mensagem";
$MESS["CRM_BUTTON_EDIT_DETAIL_OPENLINE_CHANNELS"] = "canais conectados";
$MESS["CRM_BUTTON_EDIT_DETAIL_CRMFORM_FIELDS"] = "campos do formulário";
$MESS["CRM_BUTTON_EDIT_DETAIL_CALLBACK_PHONE_NUMBER"] = "número de telefone";
$MESS["CRM_BUTTON_EDIT_LANG_CHOOSE"] = "Selecionar idioma";
$MESS["CRM_BUTTON_EDIT_LANG_CHOOSE_TIP"] = "Atenção! Alterar o idioma não irá traduzir o texto personalizado e etiquetas que você digitou manualmente ao criar o widget. Você pode editar este texto no formulário de CRM, Canal Aberto ou nas respectivas páginas de configurações de Retorno de Chamada.";
$MESS["CRM_BUTTON_EDIT_MOBILE_DEVICES"] = "Dispositivos móveis";
$MESS["CRM_BUTTON_EDIT_DO_NOT_SHOW"] = "ocultar no celular";
$MESS["CRM_BUTTON_EDIT_OPENLINE_CHOISE_CHANNEL_HINT"] = "selecione as fontes que você quer ver no widget. Widgets podem mostrar apenas uma fonte de um determinado tipo: por exemplo, uma fonte Facebook, uma fonte Viber etc.";
$MESS["CRM_BUTTON_EDIT_OPENLINE_ANOTHER_CHANNELS"] = "Canais adicionais";
$MESS["CRM_BUTTON_EDIT_OPENLINE_USE_MULTI_LINES"] = "conectar fontes de outros Canais Abertos";
$MESS["CRM_BUTTON_EDIT_OPENLINE_ADD"] = "adicionar Canal Aberto";
$MESS["CRM_BUTTON_EDIT_OPENLINE_REMOVE"] = "desconectar";
$MESS["CRM_BUTTON_EDIT_OPENLINE_ANOTHER_CHANNELS_EMPTY"] = "Você pode criar Canais Abertos e conectar fontes existentes de Canais Abertos.";
$MESS["CRM_BUTTON_EDIT_OPENLINE_MULTI_POPUP_LIMITED_TEXT"] = "Canais Abertos extras estão disponíveis somente nos planos comerciais.";
?>