<?
$MESS["CRM_WEBFORM_EDIT_HINT_ANY"] = "*- El asterisco coincide con cualquier número de caracteres.
Ejemplos:
- Todas las páginas del /catálogo/ en el directorio www.example.com
http://www.example.com/catalog/*

- Cualquier página en cualquier sitio tiene /cesta/ en la ruta
*/basket/*

- Cualquier página en cualquier sitio tiene el \"utm_source\" y \"utm_company\" en subseries de la ruta
*utm_source*utm_company*

- Todas las páginas del /catálogo/ directorio en todos los subdominios de example.com
http: //*.example.com/catalog/*";
$MESS["CRM_WEBFORM_EDIT_DESC"] = "Escoja canales para comunicarse con sus clientes. Puede activar o desactivar lo que estime oportuno.
Personalizar el diseño y el diseño de widgets.
El código del widget estará disponible después de haber guardado el formulario.";
$MESS["CRM_WEBFORM_EDIT_CHANNELS"] = "Canales conectados";
$MESS["CRM_WEBFORM_EDIT_SELECT_BACK_TO_LIST"] = "Volver";
$MESS["CRM_WEBFORM_EDIT_COPY_TO_CLIPBOARD"] = "Copiar al portapapeles";
$MESS["CRM_WEBFORM_EDIT_SITE_SCRIPT"] = "Código del icono";
$MESS["CRM_WEBFORM_EDIT_SHOW_DELAY"] = "Mostrar el icono";
$MESS["CRM_WEBFORM_EDIT_SHOW_DELAY_AT_ONCE"] = "Inmediatamente después de que la página se ha cargado";
$MESS["CRM_WEBFORM_EDIT_SHOW_DELAY_DELAY"] = "Introducir delay";
$MESS["CRM_WEBFORM_EDIT_SHOW_CHOOSE_LOCATION"] = "Escoja la posición";
$MESS["CRM_WEBFORM_EDIT_SHOW_VIEW"] = "Ver";
$MESS["CRM_BUTTON_EDIT_COLOR_BG"] = "Color de fondo";
$MESS["CRM_BUTTON_EDIT_COLOR_ICON"] = "Color del icono";
$MESS["CRM_WEBFORM_EDIT_SELECT_BACK_TO_BUTTON_LIST"] = "Volver al icono";
$MESS["CRM_WEBFORM_EDIT_SELECT_BACK_TO_BUTTON_LIST1"] = "volver a la lista";
$MESS["CRM_WEBFORM_EDIT_SHOW_ON_ALL_PAGES"] = "en todas las páginas excepto";
$MESS["CRM_WEBFORM_EDIT_SHOW_ONLY_PAGES"] = "sólo en las páginas seleccionadas";
$MESS["CRM_WEBFORM_EDIT_PAGE_SETTINGS_USER"] = "ajustes personalizados";
$MESS["CRM_WEBFORM_EDIT_PAGE_SETTINGS_DEFAULT"] = "en todas las páginas por defecto";
$MESS["CRM_WEBFORM_EDIT_PAGE_SETTINGS_SETUP"] = "configurar";
$MESS["CRM_WEBFORM_EDIT_PAGE_SETTINGS"] = "Configuración de la pantalla de widgets";
$MESS["CRM_WEBFORM_EDIT_CHANNEL_EDIT"] = "configurar";
$MESS["CRM_WEBFORM_EDIT_CHANNEL_ADD"] = "crear";
$MESS["CRM_WEBFORM_EDIT_CHANNEL_SETUP"] = "Ajustes";
$MESS["CRM_WEBFORM_EDIT_CHANNEL_ADD_DESC"] = "Actualmente no hay canales activos. Abrir las preferencias para activar los canales.";
$MESS["CRM_WEBFORM_EDIT_ON"] = "encendido";
$MESS["CRM_WEBFORM_EDIT_OFF"] = "apagado";
$MESS["CRM_WEBFORM_EDIT_NAME_PLACEHOLDER"] = "Nombre del icono";
$MESS["CRM_WEBFORM_EDIT_SAVE"] = "Guardar";
$MESS["CRM_WEBFORM_EDIT_APPLY"] = "Aplicar";
$MESS["CRM_WEBFORM_EDIT_SITE_SCRIPT_TIP"] = "Copiar el código y pégarlo en tu sitio web de la plantilla del código antes de &lt;/body&gt; close tag.";
$MESS["CRM_WEBFORM_EDIT_SITE_HELLO_TITLE"] = "Mensaje de bienvenida automático";
$MESS["CRM_WEBFORM_EDIT_EDIT"] = "Editar";
$MESS["CRM_WEBFORM_EDIT_HELLO_TITLE"] = "Mensaje de bienvenida automático";
$MESS["CRM_WEBFORM_EDIT_HELLO_DESC"] = "Seleccione las páginas que mostrarán el mensaje de bienvenida a sus clientes. Cargue una imagen y escriba un texto para atraer a más clientes.";
$MESS["CRM_WEBFORM_EDIT_HELLO_TUNE"] = "Configurar mensaje de bienvenida";
$MESS["CRM_WEBFORM_EDIT_HELLO_TIME_DELAY"] = "Mostrar retraso";
$MESS["CRM_WEBFORM_EDIT_HELLO_TIME_DELAY_TIP"] = "Especificar el tiempo de espera antes de mostrar el mensaje de bienvenida después de que el widget se vuelve visible.";
$MESS["CRM_WEBFORM_EDIT_HELLO_TIME_DELAY_NO"] = "mostrar inmediatamente";
$MESS["CRM_WEBFORM_EDIT_HELLO_PAGES_LIST"] = "Mostrar mensaje de bienvenida en estas páginas";
$MESS["CRM_WEBFORM_EDIT_HELLO_PAGES_EXCLUDE"] = "Excepto estas páginas";
$MESS["CRM_WEBFORM_EDIT_HELLO_PAGES_EXCLUDE_ADDITIONAL"] = "y páginas con mensajes configurados individualmente";
$MESS["CRM_WEBFORM_EDIT_HELLO_MODE"] = "Mostrar modo de mensaje de bienvenida";
$MESS["CRM_WEBFORM_EDIT_HELLO_MODE_EXCLUDE"] = "En todas las páginas";
$MESS["CRM_WEBFORM_EDIT_HELLO_MODE_INCLUDE"] = "Sólo en las páginas seleccionadas";
$MESS["CRM_WEBFORM_EDIT_SECTION_ALL"] = "Mensaje de bienvenida predeterminado para todas las páginas";
$MESS["CRM_WEBFORM_EDIT_SECTION_CUSTOM"] = "Mensajes personalizados de bienvenida";
$MESS["CRM_WEBFORM_EDIT_HELLO_ADD"] = "agregar mensaje de bienvenida";
$MESS["CRM_WEBFORM_EDIT_HELLO_CHANGE"] = "cambiar";
$MESS["CRM_WEBFORM_EDIT_HELLO_DEF_NAME"] = "Lisa Smith";
$MESS["CRM_WEBFORM_EDIT_HELLO_DEF_TEXT"] = "¡Hola! Soy tu ayuda personal. Dejame tu consulta siempre que tengas una duda.";
$MESS["CRM_WEBFORM_EDIT_REMOVE_LOGO"] = "eliminar firma";
$MESS["CRM_WEBFORM_EDIT_REMOVE_LOGO_BX"] = "Desarrollado por Bitrix24";
$MESS["CRM_WEBFORM_EDIT_POPUP_LIMITED_TITLE"] = "CRM extendido";
$MESS["CRM_WEBFORM_EDIT_POPUP_LIMITED_TEXT"] = "La firma se puede quitar solamente en planes comerciales.";
$MESS["CRM_BUTTON_EDIT_CLOSE"] = "Cerrar";
$MESS["CRM_BUTTON_EDIT_ERROR_ACTION"] = "Se ha cancelado la acción porque se ha producido un error";
$MESS["CRM_BUTTON_EDIT_DELETE_CONFIRM"] = "¿Quiere eliminar esta foto?";
$MESS["CRM_BUTTON_EDIT_AVATAR_LOADED"] = "Fotos subidas";
$MESS["CRM_BUTTON_EDIT_AVATAR_PRESET"] = "Standard";
$MESS["CRM_BUTTON_EDIT_WORK_TIME"] = "Preferencias de tiempo de trabajo";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_DISABLED"] = "no configurado";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_ENABLED"] = "Mostrar sólo durante las horas de trabajo";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_TIME_ZONE"] = "Selecciona la zona horaria";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_TIME"] = "Horas laborales";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_DAY_OFF"] = "Fines de semana";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_HOLIDAYS"] = "Vacaciones";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_HOLIDAYS_EXAMPLE"] = "Ejemplo: 01.01,04.07,01.11,25.12";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_ACTION_RULE"] = "Procesamiento de llamadas fuera de horas";
$MESS["CRM_BUTTON_EDIT_WORK_TIME_FIELD_ACTION_TEXT"] = "Mensaje";
$MESS["CRM_BUTTON_EDIT_DETAIL_OPENLINE_CHANNELS"] = "canales conectados";
$MESS["CRM_BUTTON_EDIT_DETAIL_CRMFORM_FIELDS"] = "campos del formulario";
$MESS["CRM_BUTTON_EDIT_DETAIL_CALLBACK_PHONE_NUMBER"] = "número de teléfono";
$MESS["CRM_BUTTON_EDIT_LANG_CHOOSE"] = "Seleccione el idioma";
$MESS["CRM_BUTTON_EDIT_LANG_CHOOSE_TIP"] = "¡Atención! Si cambia el idioma, no traducirá el texto personalizado ni las etiquetas que haya introducido manualmente al crear el widget. Puede editar este texto en las páginas de configuración respectivas de formulario de CRM, de canal abierto o de devolución de llamada.";
$MESS["CRM_BUTTON_EDIT_MOBILE_DEVICES"] = "Dispositivos móviles";
$MESS["CRM_BUTTON_EDIT_DO_NOT_SHOW"] = "no mostrar";
$MESS["CRM_BUTTON_EDIT_OPENLINE_CHOISE_CHANNEL_HINT"] = "Seleccione las fuentes que desea ver en el widget. Los widgets pueden mostrar sólo una fuente de un tipo dado: por ejemplo, una fuente de Facebook, una fuente de Viber, etc.";
$MESS["CRM_BUTTON_EDIT_OPENLINE_ANOTHER_CHANNELS"] = "Canales adicionales";
$MESS["CRM_BUTTON_EDIT_OPENLINE_USE_MULTI_LINES"] = "conectar fuentes de otros Canales Abiertos";
$MESS["CRM_BUTTON_EDIT_OPENLINE_ADD"] = "agregar Canales Abiertos";
$MESS["CRM_BUTTON_EDIT_OPENLINE_REMOVE"] = "desconectar";
$MESS["CRM_BUTTON_EDIT_OPENLINE_ANOTHER_CHANNELS_EMPTY"] = "Puede crear Canales Abiertos y conectar fuentes de Canales Abiertos existentes.";
$MESS["CRM_BUTTON_EDIT_OPENLINE_MULTI_POPUP_LIMITED_TEXT"] = "Los canales adicionales abiertos sólo están disponibles en los planes comerciales.";
?>