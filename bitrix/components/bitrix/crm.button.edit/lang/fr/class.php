<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
$MESS["CRM_BUTTON_EDIT_TITLE_EDIT"] = "Modifier le widget";
$MESS["CRM_BUTTON_EDIT_TITLE_ADD"] = "Créer un widget";
$MESS["CRM_BUTTON_EDIT_UNIT_SECOND"] = "sec";
$MESS["CRM_BUTTON_EDIT_UNIT_MINUTE"] = "min";
$MESS["CRM_BUTTON_EDIT_ERROR_FILE"] = "Impossible de générer le code du widget. Veuillez enregistrer le formulaire une nouvelle fois.";
?>