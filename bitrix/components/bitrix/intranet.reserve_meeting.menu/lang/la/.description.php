<?
$MESS["ITSRM_NAME"] = "Menú";
$MESS["ITSRM_DESCRIPTION"] = "Mostrar el componente del menú.";
$MESS["INTR_GROUP_NAME"] = "Intranet Portal";
$MESS["INTRANET_RESMIT"] = "Reserva de la sala de reuniones";
$MESS["INTRANET_RESMIT_LIST"] = "Sala de Reuniones";
$MESS["INTRANET_RESMIT_LIST_DESCRIPTION"] = "Mostrar la lista de las salas de reuniones existentes.";
?>