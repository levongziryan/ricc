<?
$MESS["ITSRM1_MEETING_LIST_DESCR"] = "Ver todas las salas de reuniones";
$MESS["ITSRM1_MEETING_LIST"] = "Salas de reuniones";
$MESS["ITSRM1_MEETING_SEARCH_DESCR"] = "Buscar salas de reuniones";
$MESS["ITSRM1_MEETING_SEARCH"] = "Buscar salas de reuniones";
$MESS["INTASK_C27T_RESERVE_TITLE"] = "Reserva una sala de reunión";
$MESS["INTASK_C27T_RESERVE"] = "Reservar";
$MESS["INTASK_C27T_GRAPH_TITLE"] = "Programar reservación de la sala de reunión";
$MESS["INTASK_C27T_GRAPH"] = "Programar";
$MESS["INTASK_C27T_EDIT_TITLE"] = "Editar parámetros de la sala de reunión";
$MESS["INTASK_C27T_EDIT"] = "Editar sala de reunión";
$MESS["INTASK_C27T_CRAETE_TITLE"] = "Crear sala de reunión";
$MESS["INTASK_C27T_CREATE"] = "Crear sala de reunión";
?>