<?
$MESS["INTASK_C27T_RESERVE"] = "Reservar";
$MESS["INTASK_C27T_RESERVE_TITLE"] = "Reservar uma sala da reunião";
$MESS["INTASK_C27T_CRAETE_TITLE"] = "Criar Sala de Reunião";
$MESS["INTASK_C27T_CREATE"] = "Create Meeting Room";
$MESS["INTASK_C27T_EDIT"] = "Editar Sala de Reunião";
$MESS["INTASK_C27T_EDIT_TITLE"] = "Editar parâmetros da sala de reunião";
$MESS["INTASK_C27T_GRAPH_TITLE"] = "Agenda de reserva da sala de reunião";
$MESS["ITSRM1_MEETING_LIST"] = "Salas de Reunião";
$MESS["INTASK_C27T_GRAPH"] = "Horários";
$MESS["ITSRM1_MEETING_SEARCH_DESCR"] = "Pesquisar Salas de Reunião";
$MESS["ITSRM1_MEETING_SEARCH"] = "Pesquisar Salas de Reunião";
$MESS["ITSRM1_MEETING_LIST_DESCR"] = "Ver todas as salas de reunião";
?>