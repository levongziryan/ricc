<?
$MESS["MAIN_DUMP_PARTS"] = "parties : ";
$MESS["MAIN_DUMP_LOCAL"] = "localement";
$MESS["BACKUP_ACTION_DOWNLOAD"] = "Télécharger";
$MESS["BACKUP_ACTION_DELETE"] = "Supprimer";
$MESS["BACKUP_ACTION_LINK"] = "Obtenir le lien de sauvegarde";
$MESS["BACKUP_ACTION_RESTORE"] = "Restaurer";
$MESS["BACKUP_ACTION_RENAME"] = "Renommer";
$MESS["DUMP_DELETE_ERROR"] = "Suppression du fichier #FILE# impossible";
$MESS["MAIN_DUMP_ERROR"] = "Erreur";
$MESS["MAIN_DUMP_ERR_COPY_FILE"] = "Erreur ! Impossible de copier le fichier : ";
$MESS["MAIN_DUMP_USE_THIS_LINK"] = "Utilisez ce lien pour migrer vers un autre serveur en utilisant";
$MESS["MAIN_DUMP_ERR_FILE_RENAME"] = "Erreur au moment de renommer le fichier : ";
$MESS["MAIN_DUMP_ERR_NAME"] = "Le nom du fichier ne peut contenir que des caractères latins, des chiffres, des traits d'union et des points.";
?>