<?
$MESS["BACKUP_PROGRESS_HINT"] = "La copia de seguridad ahora se está creando. Por favor, no cierre la página.";
$MESS["BACKUP_PROGRESS_TEXT"] = "Creando copia de seguridad";
$MESS["BACKUP_BUTTON"] = "Crear copia de seguridad";
$MESS["BACKUP_SUCCESS"] = "La copia de seguridad se ha creado con éxito.";
$MESS["BACKUP_ERROR"] = "Error al crear la copia de seguridad";
$MESS["BACKUP_SYSTEM_ERROR"] = "Error del sistema:";
$MESS["BACKUP_HEADER_NAME"] = "Nombre";
$MESS["BACKUP_HEADER_SIZE"] = "Tamaño de archivo";
$MESS["BACKUP_HEADER_DATE"] = "Modificado el";
$MESS["BACKUP_HEADER_PLACE"] = "Ubicación";
$MESS["BACKUP_RESTORE_CONFIRM"] = "¡Atención! Restaurar la copia de seguridad en un sitio en funcionamiento puede dañar el sitio. ¿Desea continuar?";
$MESS["BACKUP_DELETE_CONFIRM"] = "¿Estás seguro que quiere eliminar el archivo?";
$MESS["BACKUP_RENAME_TITLE"] = "Renombrar archivo";
$MESS["BACKUP_CANCEL_BUTTON"] = "Cancelar";
$MESS["BACKUP_SAVE_BUTTON"] = "Guardar";
$MESS["BACKUP_STOP_BUTTON"] = "Cancelar";
?>