<?
$MESS["BACKUP_PROGRESS_HINT"] = "La copie de sauvegarde est en cours de création. Veuillez ne pas fermer la page.";
$MESS["BACKUP_PROGRESS_TEXT"] = "Création d'une sauvegarde";
$MESS["BACKUP_BUTTON"] = "Créer une sauvegarde";
$MESS["BACKUP_SUCCESS"] = "La sauvegarde a bien été créée.";
$MESS["BACKUP_ERROR"] = "Erreur lors de la création de la sauvegarde";
$MESS["BACKUP_SYSTEM_ERROR"] = "Erreur système : ";
$MESS["BACKUP_HEADER_NAME"] = "Nom";
$MESS["BACKUP_HEADER_SIZE"] = "Taille de l'archive";
$MESS["BACKUP_HEADER_DATE"] = "Date de modification";
$MESS["BACKUP_HEADER_PLACE"] = "Localisation";
$MESS["BACKUP_RESTORE_CONFIRM"] = "Attention ! Restaurer une sauvegarde sur un site fonctionnel peut endommager le site ! Continuer ?";
$MESS["BACKUP_DELETE_CONFIRM"] = "Voulez-vous vraiment supprimer le fichier ?";
$MESS["BACKUP_RENAME_TITLE"] = "Renommer le fichier";
$MESS["BACKUP_CANCEL_BUTTON"] = "Annuler";
$MESS["BACKUP_SAVE_BUTTON"] = "Enregistrer";
$MESS["BACKUP_STOP_BUTTON"] = "Annuler";
?>