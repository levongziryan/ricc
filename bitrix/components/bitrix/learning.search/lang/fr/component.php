<?
$MESS["LEARNING_MAIN_SEARCH_SEARCH_CHAPTER"] = "aux chapitres";
$MESS["LEARNING_MAIN_SEARCH_SEARCH_COURSE"] = "dans les cours";
$MESS["LEARNING_MAIN_SEARCH_SEARCH_LESSON"] = "en leçons";
$MESS["LEARNING_MODULE_NOT_INSTALL"] = "Module des blogs non installé.";
$MESS["SEARCH_MODULE_NOT_INSTALL"] = "Module de recherche non installé.";
$MESS["LEARNING_MAIN_SEARCH_NOTHING_FOUND"] = "Rien n'a été trouvé.";
$MESS["LEARNING_MAIN_SEARCH_ERROR"] = "Erreur de la demande de recherche:";
$MESS["LEARNING_MAIN_SEARCH_TITLE"] = "Recherche par cours";
$MESS["LEARNING_MAIN_SEARCH_COURSE_TITLE"] = "Recherche selon le cours";
$MESS["LEARNING_RESULT_PAGES"] = "Résultat";
?>