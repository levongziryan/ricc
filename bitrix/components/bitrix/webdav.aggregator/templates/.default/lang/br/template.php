<?
$MESS["WD_AG_MSGTITLE"] = "Para conectar-se a biblioteca como uma unidade de rede:";
$MESS["WD_AG_HELP1"] = "Executar <i> Windows Explorer </i>.";
$MESS["WD_AG_HELP2"] = "Selecione \"Ferramentas\" -> \"Mapear Unidade de Rede ...» no menu Explorer.";
$MESS["WD_AG_HELP3"] = "Selecione uma letra de unidade para a pasta a ser conectada.";
$MESS["WD_AG_HELP4"] = "Forneça o caminho da biblioteca na pasta <i> </i> campo:";
$MESS["WD_AG_HELP5"] = "Clique em Concluir <i> </i>.";
$MESS["WD_AG_HELP6"] = "Para mais informações sobre como utilizar as bibliotecas em Windows e Mac OS X, consulte o #STARTLINK# Documento seção de ajuda Biblioteca #ENDLINK#..";
$MESS["WD_NO_LIBRARIES"] = "Bibliotecas de documentos não disponíveis";
$MESS["WD_AG_ADD_LIBRARY"] = "Adicionar biblioteca";
$MESS["WD_AG_ADD_STORAGE"] = "Adicionar armazenamento";
$MESS["WD_AG_MAP_DRIVE"] = "Mapear Unidade de Rede";
$MESS["WD_AG_GO_BACK"] = "De Volta";
$MESS["WD_AG_DOCUMENTS"] = "Documentos: #N#";
$MESS["WD_AG_PUBLISHED_DOCUMENTS"] = "Documentos publicados: #N#";
$MESS["WD_UPLOAD_ELEMENT"] = "Substitua o documento atual";
$MESS["WD_EDIT_MSOFFICE"] = "Editar documento em aplicação externa";
$MESS["WD_DELETE_FILE_ALT"] = "Excluir documento";
$MESS["WD_IBLOCK_ID"] = "Bloco de informação";
$MESS["WD_ERROR_ELEMENT_NOT_FOUND"] = "Elemento não foi encontrado.";
$MESS["WD_HISTORY"] = "História";
$MESS["IBLOCK_CACHE_FILTER"] = "Cache se o filtro está ativo";
$MESS["T_IBLOCK_DESC_FID"] = "ID";
?>