<?
$MESS["WD_AG_MSGTITLE"] = "Para conectar a la biblioteca como unidad de la red de trabajo:";
$MESS["WD_AG_HELP1"] = "Activar <i>Windows Explorer</i>.";
$MESS["WD_AG_HELP2"] = "Seleccione &laquo;Tools&laquo; -> &laquo;Map Network Drive...&raquo; en el Explorador del menú.";
$MESS["WD_AG_HELP3"] = "Selecione una unidad del letra para que la carpeta sea conectada.";
$MESS["WD_AG_HELP4"] = "Proporcione a la biblioteca la ruta en el campo <i>Folder</i>:";
$MESS["WD_AG_HELP5"] = "Oprima <i>Finish</i>.";
$MESS["WD_NO_LIBRARIES"] = "No hay documentos disponibles en la biblioteca";
$MESS["WD_AG_HELP6"] = "Para más información sobre el uso de bibliotecas en Windows and Mac OS X, por favor remitase a la sección de ayuda del documento de la biblioteca #STARTLINK# #ENDLINK#.";
$MESS["WD_AG_ADD_LIBRARY"] = "Agregar Biblioteca";
$MESS["WD_AG_ADD_STORAGE"] = "Agregar Almacenaje";
$MESS["WD_AG_GO_BACK"] = "Atrás";
$MESS["WD_AG_DOCUMENTS"] = "Documentos: #N#";
$MESS["WD_AG_PUBLISHED_DOCUMENTS"] = "Documentos publicados: #N#";
$MESS["WD_AG_MAP_DRIVE"] = "Mapa de la Unidad de la red";
?>