<?
$MESS["IBLOCK_TYPE"] = "Tipo de block de información que contiene las bibliotecas de los documentos";
$MESS["IBLOCK_OTHER_IDS"] = "Block de información de las bibliotecas del documento";
$MESS["IBLOCK_GROUP_ID"] = "Block de información de los documentos del grupo de trabajo";
$MESS["IBLOCK_USER_ID"] = "Block de información de los documentos del usuario";
$MESS["NAME_TEMPLATE_TIP"] = "Marcos posibles: #NAME# - nombre; #LAST_NAME# - apellido, #SECOND_NAME# - segundo nombre.";
?>