<?
$MESS["WD_WD_MODULE_IS_NOT_INSTALLED"] = "El módulo Document Library no está instalado.";
$MESS["WD_IB_MODULE_IS_NOT_INSTALLED"] = "El módulo \"Information Blocks\" no está instalado.";
$MESS["WD_SN_MODULE_IS_NOT_INSTALLED"] = "El módulo Social Network no está instalado.";
$MESS["WD_GROUP"] = "Drive del grupo de trabajo";
$MESS["WD_PRIVATE"] = "Mi Drive";
$MESS["WD_USER"] = "Documentos del Drives";
$MESS["WD_ROOT"] = "Root";
$MESS["SONET_GROUP"] = "Grupo";
$MESS["SONET_GROUP_PREFIX"] = "Grupo: ";
$MESS["WD_IB_GROUP_IS_NOT_FOUND"] = "El block de información de los documentos del grupo de trabajo no fue encontrado.";
$MESS["WD_IB_USER_IS_NOT_FOUND"] = "El block de información de los documentos del usuario no fue encontrado..";
$MESS["WD_DAV_INSUFFICIENT_RIGHTS"] = "Usted no tiene permiso para esta acción.";
$MESS["WD_DAV_UNSUPORTED_METHOD"] = "Este método no está soportado.";
$MESS["WD_GROUP_SECTION_FILES_NOT_FOUND"] = "La sección de los documentos del grupo no se encontró.";
$MESS["WD_USER_SECTION_FILES_NOT_FOUND"] = "La sección de los documentos del usuario no se encontró.";
$MESS["WD_USER_NOT_FOUND"] = "El usuario no fue encontrado.";
$MESS["WD_NOT_SEF_MODE"] = "Buscar un motor amigable el URL del(SEF) no permite parámetros en el componente.";
$MESS["WD_SOCNET_LANG_NOT_FOUND"] = "Los componentes de la red social faltan. Verifique la ruta root del servidor web en la configuración del sitio.";
$MESS["WD_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["WD_SHARED"] = "Drive de la Compañía";
?>