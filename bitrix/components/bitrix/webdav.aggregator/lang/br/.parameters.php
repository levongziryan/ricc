<?
$MESS["WD_USER_FILE_PATH"] = "Página de documentos do usuário";
$MESS["WD_GROUP_FILE_PATH"] = "Página de documentos do grupo de trabalho";
$MESS["WD_IBLOCK_TYPE"] = "Tipo Bloco de Informações";
$MESS["WD_IBLOCK_OTHER_ID"] = "Documentos Information Block";
$MESS["WD_IBLOCK_GROUP_ID"] = "Bloco de documentos de informações sobre grupos de trabalho";
$MESS["WD_IBLOCK_USER_ID"] = "Informação sobre bloqueio de documentos de usuários";
$MESS["WD_NAME_TEMPLATE"] = "Formato do nome";
$MESS["WD_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["WD_URL_USER_VIEW"] = "URL da página do usuário (deve abrir um perfil de usuário existente e conter #USER_ID#)";
$MESS["W_TITLE_ACTFROM"] = "Ativo de";
$MESS["WD_DOCUMENT_ALT"] = "Propriedades do Documento";
$MESS["WD_DISK_JS_ERROR_WRONG_TOKEN"] = "Símbolo JS inválido.";
$MESS["WD_UPLOAD_NOT_DONE"] = "O documento ainda não foi enviado.";
$MESS["WD_UPLOAD_NOT_DONE_ASK"] = "O documento ainda não foi enviado. Fechar a caixa de diálogo de upload?";
$MESS["WD_BTN_INSTALL"] = "Instale Extensão";
$MESS["W_TITLE_EXTERNAL_COM"] = "Comentário do Administrador";
?>