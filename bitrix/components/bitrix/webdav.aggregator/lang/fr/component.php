<?
$MESS["WD_WD_MODULE_IS_NOT_INSTALLED"] = "Le document intitulé «# FILENAME # 'est déjà en cours d'utilisation.";
$MESS["WD_IB_MODULE_IS_NOT_INSTALLED"] = "Le module des blocs d'information n'a pas été installé.";
$MESS["WD_SN_MODULE_IS_NOT_INSTALLED"] = "Le module du réseau social n'a pas été installé.";
$MESS["WD_GROUP"] = "Groupes de travail";
$MESS["WD_PRIVATE"] = "Mon Drive";
$MESS["WD_USER"] = "Documents des employés";
$MESS["WD_ROOT"] = "Racine";
$MESS["SONET_GROUP"] = "Groupe";
$MESS["SONET_GROUP_PREFIX"] = "Groupe:";
$MESS["WD_IB_GROUP_IS_NOT_FOUND"] = "Le bloc d'information des documents de travail des groupes n'est pas trouvé.";
$MESS["WD_IB_USER_IS_NOT_FOUND"] = "Le bloc d'information de documents d'utilisateurs n'a pas été retrouvé.";
$MESS["WD_DAV_INSUFFICIENT_RIGHTS"] = "Vous n'êtes pas autorisé pour accomplir cette action.";
$MESS["WD_DAV_UNSUPORTED_METHOD"] = "La méthode n'est pas soutenue.";
$MESS["WD_GROUP_SECTION_FILES_NOT_FOUND"] = "La section documente du groupe n'a pas été trouvé.";
$MESS["WD_USER_SECTION_FILES_NOT_FOUND"] = "La rubrique avec les documents utilisateur n'a pas été trouvée";
$MESS["WD_USER_NOT_FOUND"] = "L'utilisateur n'est pas trouvé.";
$MESS["WD_NOT_SEF_MODE"] = "Dans les paramètres du composant le soutien de la commande numérique n'est pas activé.";
$MESS["WD_SOCNET_LANG_NOT_FOUND"] = "Composants du module de réseau social introuvables. Veuillez vérifier les paramètres du chemin d'accès au dossier racine du serveur Web dans les paramètres du site.";
$MESS["WD_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["WD_SHARED"] = "Drive Entreprise";
?>