<?
$MESS["NAME_TEMPLATE_TIP"] = "Modèles valides: #NAME# - prénom, #LAST_NAME# - nom de famille, #SECOND_NAME# - patronyme";
$MESS["IBLOCK_OTHER_IDS"] = "Blocs information avec les bibliothèques de documents";
$MESS["IBLOCK_USER_ID"] = "Bloc d'information des documents des utilisateurs";
$MESS["IBLOCK_GROUP_ID"] = "Le bloc d'information des documents des groupes de travail";
$MESS["IBLOCK_TYPE"] = "Type du bloc d'information avec les bibliothèques de documents";
?>