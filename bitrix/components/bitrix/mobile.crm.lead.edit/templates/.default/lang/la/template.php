<?
$MESS["M_CRM_LEAD_EDIT_CREATE_TITLE"] = "Nuevo prospecto";
$MESS["M_CRM_LEAD_EDIT_CANCEL_BTN"] = "Cancelar";
$MESS["M_CRM_LEAD_EDIT_SAVE_BTN"] = "Guardar";
$MESS["M_CRM_LEAD_EDIT_EDIT_TITLE"] = "Editar";
$MESS["M_CRM_LEAD_EDIT_VIEW_TITLE"] = "Ver prospecto";
$MESS["M_DETAIL_PULL_TEXT"] = "Pulsar para actualizar...";
$MESS["M_DETAIL_DOWN_TEXT"] = "Soltar para actualizar...";
$MESS["M_DETAIL_LOAD_TEXT"] = "Actualizando...";
$MESS["M_CRM_LEAD_MENU_EDIT"] = "Editar";
$MESS["M_CRM_LEAD_MENU_DELETE"] = "Eliminar";
$MESS["M_CRM_LEAD_MENU_CREATE_ON_BASE"] = "Crear usando el origen";
$MESS["M_CRM_LEAD_CONV_OPEN_ENTITY_SEL_CONTACT"] = "Seleccionar el contacto de la lista...";
$MESS["M_CRM_LEAD_CONV_OPEN_ENTITY_SEL_COMPANY"] = "Seleccionar la compañía de la lista...";
$MESS["M_CRM_LEAD_MENU_HISTORY"] = "Historial";
$MESS["M_CRM_LEAD_MENU_ACTIVITY"] = "Actividades";
?>