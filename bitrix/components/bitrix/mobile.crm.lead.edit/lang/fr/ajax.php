<?
$MESS["CRM_ENTITY_DATA_NOT_FOUND"] = "DATA_NOT_FOUND: Impossible de trouver les données pour le traitement.";
$MESS["CRM_ENTITY_ID_NOT_FOUND"] = "ID_NOT_FOUND: ID n'est pas trouvé.";
$MESS["CRM_LEAD_STATUS_NOT_FOUND"] = "STATUS_NOT_FOUND: Impossible de déterminer le statut.";
$MESS["CRM_ENTITY_TYPE_NOT_SUPPORTED"] = "TYPE_NOT_SUPPORTED: Le type '#ENTITY_TYPE#' n'est pas soutenu dans le contexte courant.";
$MESS["CRM_ACCESS_DENIED"] = "Accès interdit.";
$MESS["CRM_LEAD_NOT_FOUND"] = "Impossible de trouver le lead avec l'ID  ##ID#.";
$MESS["CRM_LEAD_COULD_NOT_DELETE"] = "Chec de la suppression du lead.";
$MESS["CRM_LEAD_TITLE_NOT_ASSIGNED"] = "Veuillez indiquer le nom de prospect.";
?>