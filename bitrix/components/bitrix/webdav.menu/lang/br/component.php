<?
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "O módulo WebDav não está instalado";
$MESS["WD_TITLE"] = "Biblioteca";
$MESS["WD_ACCESS_DENIED"] = "Acesso negado.";
$MESS["WD_SHARE_CONNECT_GROUP_TITLE"] = "Grupo acessível via My Drive";
$MESS["WD_SHARE_CONNECT_GROUP_DESCR"] = "Agora você não pode trabalhar com o grupo de unidades usando Bitrix24.Drive em seu computador local.";
$MESS["WD_SHARE_CONNECT_GROUP_BTN_INSTALL_DESKTOP"] = "instalar Bitrix24.Drive";
$MESS["WD_SHARE_CONNECT_GROUP_BTN_ENABLE_DESKTOP"] = "conectar Bitrix24.Drive";
$MESS["WD_SHARE_CONNECT_GROUP_BTN_OPEN_FOLDER_DESKTOP"] = "Mostrar a pasta";
$MESS["WD_SHARE_CONNECT_SHARED_SECTION_TITLE"] = "Pasta compartilhada conectada";
$MESS["WD_SHARE_CONNECT_SHARED_SECTION_DESCR"] = "Agora você pode abrir e salvar documentos em \"#NAME#\" diretamente do seu computador.";
$MESS["WD_START_BP"] = "Processo de Novos Negócios";
$MESS["WD_FILE_REPLACE"] = "Substituir arquivo";
$MESS["WD_UPLOAD_TITLE"] = "Carregar novo Documento";
?>