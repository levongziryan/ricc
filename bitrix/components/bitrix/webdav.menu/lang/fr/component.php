<?
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "Le module WebDav n'a pas été installé";
$MESS["WD_TITLE"] = "Libye";
$MESS["WD_ACCESS_DENIED"] = "Accès interdit.";
$MESS["WD_SHARE_CONNECT_GROUP_TITLE"] = "Connecté à Mon Drive";
$MESS["WD_SHARE_CONNECT_GROUP_DESCR"] = "Maintenant vous pouvez travailler avec les documents du groupe directement sur votre ordinateur.";
$MESS["WD_SHARE_CONNECT_GROUP_BTN_INSTALL_DESKTOP"] = "Installer l'application Bitrix24.Drive";
$MESS["WD_SHARE_CONNECT_GROUP_BTN_ENABLE_DESKTOP"] = "Connecter Bitrix24.Drive";
$MESS["WD_SHARE_CONNECT_GROUP_BTN_OPEN_FOLDER_DESKTOP"] = "Afficher le dossier";
$MESS["WD_SHARE_CONNECT_SHARED_SECTION_TITLE"] = "Le dossier partagé est connecté";
$MESS["WD_SHARE_CONNECT_SHARED_SECTION_DESCR"] = "Maintenant, vous pouvez travailler avec des documents du dossier '#NAME#' directement sur votre ordinateur.";
?>