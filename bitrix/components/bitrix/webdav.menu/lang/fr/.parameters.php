<?
$MESS["WD_SECTION_ID"] = "ID de la section";
$MESS["WD_ROOT_SECTION_ID"] = "ID de la section de racine (il est recommandé d'utiliser en ensemble avec un composant complexe)";
$MESS["WD_PAGE_NAME"] = "ID de la Page du Composant complexe";
$MESS["WD_FORUM_ID"] = "Forum ID";
$MESS["WD_ELEMENT_ID"] = "Identifiant de l'élément";
$MESS["WD_BASE_URL"] = "Adresse de la bibliothèque pour la connexion du disque réseau (adresse complète)";
$MESS["WD_PERMISSION"] = "Droits d'accès externes (il est recommandé d'utiliser dans le cadre d'un composant intégré)";
$MESS["WD_IBLOCK_ID"] = "Bloc d'information";
$MESS["WD_STR_TITLE"] = "Nom de la section de racine";
$MESS["WD_CHECK_CREATOR"] = "Vérifier le propriétaire du document";
$MESS["WD_USE_COMMENTS"] = "Autoriser des avis";
$MESS["WD_ELEMENT_UPLOAD_URL"] = "Page du chargement des fichiers";
$MESS["WD_SECTION_EDIT_URL"] = "Page de changement du répertoire";
$MESS["WD_ELEMENT_EDIT_URL"] = "Page de modification de l' élément";
$MESS["WD_SECTION_LIST_URL"] = "Page d'examen du catalogue";
$MESS["WD_IBLOCK_TYPE"] = "Type du bloc d'information";
?>