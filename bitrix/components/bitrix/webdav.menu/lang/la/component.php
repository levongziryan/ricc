<?
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "El módulo de WebDav no está instalado";
$MESS["WD_TITLE"] = "Biblioteca";
$MESS["WD_ACCESS_DENIED"] = "Acceso denegado.";
$MESS["WD_SHARE_CONNECT_GROUP_TITLE"] = "Unidad de grupo accesible a través de My Drive";
$MESS["WD_SHARE_CONNECT_GROUP_DESCR"] = "Ahora usted puede trabajar con la unidad de grupo utilizando Bitrix24.Drive en el equipo local.";
$MESS["WD_SHARE_CONNECT_GROUP_BTN_INSTALL_DESKTOP"] = "Instalación Bitrix24.Drive";
$MESS["WD_SHARE_CONNECT_GROUP_BTN_ENABLE_DESKTOP"] = "Conectar Bitrix24.Drive";
$MESS["WD_SHARE_CONNECT_GROUP_BTN_OPEN_FOLDER_DESKTOP"] = "Mostrar carpeta";
$MESS["WD_SHARE_CONNECT_SHARED_SECTION_TITLE"] = "Carpeta compartida conectada";
$MESS["WD_SHARE_CONNECT_SHARED_SECTION_DESCR"] = "Ahora ya puede abrir y guardar documentos en \"#NAME#\" directamente desde su computadora.";
?>