<?
$MESS["WD_IBLOCK_TYPE"] = "Tipo del block de información";
$MESS["WD_IBLOCK_ID"] = "Block de información";
$MESS["WD_SECTION_ID"] = "ID de la sección";
$MESS["WD_ELEMENT_ID"] = "ID del Elemento";
$MESS["WD_USE_COMMENTS"] = "Permitir los comentarios";
$MESS["WD_FORUM_ID"] = "ID del Foro";
$MESS["WD_CHECK_CREATOR"] = "Comprobar creador del archivo";
$MESS["WD_PAGE_NAME"] = "ID de la página del compomente complejo";
$MESS["WD_SECTION_LIST_URL"] = "Página de la vista del catálogo";
$MESS["WD_SECTION_EDIT_URL"] = "Página de edición del catálogo";
$MESS["WD_ELEMENT_EDIT_URL"] = "Página de edición del elemento";
$MESS["WD_ELEMENT_UPLOAD_URL"] = "Página del archivo cargado";
$MESS["WD_STR_TITLE"] = "Nombre de la raíz de la sección";
$MESS["WD_BASE_URL"] = "URL de la biblioteca para el mapeo de la unidada de la red (ruta completa)";
$MESS["WD_PERMISSION"] = "acceso externo de Permiso (usar en componentes compuestos)";
$MESS["WD_ROOT_SECTION_ID"] = "ID de la raíz de la sección (usar en componentes compuestos)";
?>