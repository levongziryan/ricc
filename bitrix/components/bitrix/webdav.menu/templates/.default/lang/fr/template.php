<?
$MESS["WD_SECTION_ADD"] = "Créer le dossier";
$MESS["WD_SECTION_ADD_ALT"] = "Créez un sous-dossier";
$MESS["WD_ELEMENT_ADD"] = "Ajouter";
$MESS["WD_ELEMENT_ADD_ALT"] = "Créer un document MS Word";
$MESS["WD_UPLOAD"] = "Chargement";
$MESS["WD_UPLOAD_ALT"] = "Télécharger de nouveaux documents dans ce dossier";
$MESS["WD_UPLOAD_ROOT_ALT"] = "Chargez de nouveaux documents dans la racine de bibliothèque";
$MESS["WD_GO_BACK"] = "Précédent";
$MESS["WD_GO_BACK_ALT"] = "Précédent";
$MESS["WD_DELETE_CONFIRM"] = "Supprimer dans la corbeille?";
$MESS["WD_HELP"] = "Aide";
$MESS["WD_HELP_ALT"] = "Aide pendant le travail avec la bibliothèque de documents";
$MESS["WD_MAPING"] = "Disque réseau";
$MESS["WD_MAPING_ALT"] = "Travailler avec la bibliothèque avec l'aide du gestionnaire de fichiers";
$MESS["WD_EMPTY_PATH"] = "Le chemin pour la connexion n'est pas indiqué.";
$MESS["WD_HELP_TEXT"] = "<ul class='wd-content' style='color:#052635;font-family:Verdana;font-size:14px;'>
<li> Assurez-vous que le logiciel nécessaire est installé <a target='_blank' href='http://www.microsoft.com/downloads/details.aspx?FamilyId=43109663-3627-4949-BD43-F2247570B9EC&displaylang=fr'>Web Folders</a></li>
<Ii> Lancer l'Explorateur</li>
<li>Dans le menu, cliquez sur <b>Service</b> - <b>Mettre en route un disque réseau</b></li>
<li>Depuis le lien <b> SousEcrire au stockage en ligne ou se connecter à un disque réseau </b>; Lancez l'Assistant d'ajout à un emplacement réseau. La première fenêtre (d'information) de l'Assistant apparaît </li>
<li>Cliquer ensuite sur <b>Suivant</b>. </li>
<li>Dans cette fenêtre, sélectionnez Choisir un autre emplacement réseau et cliquez sur <b>Suivant</b>. </li>
<li>Dans le champ <b>Adresse URL ou adresse IP</b>entrez l'URL du fichier à connecter #BASE_URL#.</li>
<li>Cliquer ensuite sur <b>Suivant</b>. La boîte de dialogue d'authentification du système d'exploitation apparaît. </Ii>
<li>Entrez le login et le mot de passe de l'utilisateur et cliquez ensuite sur <b>??</b>.La connexion est autorisée, l'Assistant affiche l'étape suivante. </li>
<li>Sur cette étape, vous pouvez donner un nom aléatoire au dossier. Par défaut, le dossier est nommé dossier. Si nécessaire, renommez le réseau partagé sur lequel il s'affichera dans votre ordinateur. Cliquez sur <b>Suivant</b>. La dernière étape de l'Assistant s'affiche. </li>
<li>Sur cette étape, l'Assistant va notifier que la connexion a été établie avec succès et va proposer d'ouvrir le dossier après la fermeture de la boîte de dialogue. Cliquez sur <b>Prêt</b>. </li>
<li>Le système effectue la connexion du dossier et (si vous aviez accepté l'ouverture après la fermeture de l'Assistant) ouvrira la fenêtre de l'Explorateur du dossier. </li>
</ul>";
$MESS["WD_ERROR_1"] = "Le document n'a pas pu être créé.
L'application nécessaire ne peut pas être installé correctement, ou le modèle pour cette bibliothèque de documents ne peut pas être ouvert.";
$MESS["WD_ERROR_2"] = "Pour qu'on puisse créer un document en cliquant sur la touche 'Créer', il faut avoir une version de Miscrosot Internet Explorer 6.00 ou plus récente. Pour ajouter un document à la bibliothèque, utilisez le chargement des documents par la touche 'Charger'";
$MESS["WD_SUBSCRIBE"] = "Abonnement";
$MESS["WD_SUBSCRIBE_TO_FORUM"] = "Recevez par email tous les commentaires aux documents de la bibliothèque";
$MESS["WD_UNSUBSCRIBE"] = "Se désabonner";
$MESS["WD_UNSUBSCRIBE_FROM_FORUM"] = "Ne recevoir aucun commentaire aux documents par email";
$MESS["BPATT_HELP1_TEXT"] = "Le processus d'affaires avec les statuts est une longue procédure d'entrepris avec le partage des droits d'accès lors du travail avec les documents dans de différents statuts";
$MESS["BPATT_HELP2_TEXT"] = "Processus d'affaires successif - c'est la simple Processus d'affaires qui permet d'envoyer le document pour son traitement successif.";
$MESS["BPATT_HELP1"] = "Créer le processus d'affaires avec les statuts";
$MESS["BPATT_HELP2"] = "Créer un processus d'affaires consécutif";
$MESS["WD_BP"] = "les processus d'affaires";
$MESS["WD_CLEAN_TRASH"] = "Vider la corbeille";
$MESS["WD_CONFIRM_CLEAN_TRASH"] = "Etes-vous sur de vouloir vider la corbeille ?";
$MESS["WD_DELETE_TITLE"] = "Confirmation de suppression";
$MESS["WD_Y"] = "Oui";
$MESS["WD_N"] = "Non";
$MESS["WD_FF_EXTENSION_NAME"] = "Intégration avec Bitrix Portail Intranet";
$MESS["WD_FF_EXTENSION_UPDATE"] = "Veuillez mettre à jour l'extension de votre navigateur <a href='#LINK#'>'#NAME#'</a> afin de modifier des documents office.";
$MESS["WD_FF_EXTENSION_INSTALL"] = "Installer l'extension <a href='#LINK#'>'#NAME#'</a> pour le navigateur Firefox.";
$MESS["WD_FF_EXTENSION_TITLE"] = "Extension pour Firefox";
$MESS["WD_FF_EXTENSION_HELP"] = "Cette extension permet d'éditer plus facilement et plus vite des documents textuels de la bibliothèque de documents sans les charger sur l'ordinateur local. Il est nécessaire que vous ayez sur votre ordinateur un des logiciels suivants installé: Microsoft Office, OpenOffice.org ou LibreOffice.";
$MESS["WD_FF_EXTENSION_DISABLE"] = "<input type='checkbox' name='ff_extension_disable' value='true'>&nbsp;Ne plus proposer l'installation de l'extension.";
$MESS["WD_BTN_INSTALL"] = "Installer l'Extension";
$MESS["WD_BTN_UPDATE"] = "Mettre à jour l'extension";
$MESS["WD_BTN_INSTALL_CANCEL"] = "Fermer";
$MESS["WD_BTN_OPEN"] = "ne pas installer";
$MESS["WD_MENU_FF_EXTENSION_TEXT"] = "Extension pour Firefox";
$MESS["WD_MENU_FF_EXTENSION_TITLE"] = "Intégration avec Bitrix Portail Intranet";
$MESS["WD_MENU_EDIT_IN"] = "Editer dans";
$MESS["WD_MENU_EDIT_IN_OTHER"] = "Editer";
$MESS["WD_MENU_GROUP_DISK_DISCONNECTED"] = "Brancher à Mon Drive";
$MESS["WD_MENU_GROUP_DISK_CONNECTED"] = "Connecté à Mon Drive";
$MESS["WD_STOP_DROP_TRASH"] = "Arrêter";
$MESS["WD_DROP_TRASH_COUNT_ELEMENTS"] = "Des fichiers à supprimer:";
?>