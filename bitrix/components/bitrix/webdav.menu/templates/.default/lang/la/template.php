<?
$MESS["WD_SECTION_ADD"] = "Crear la carpeta";
$MESS["WD_SECTION_ADD_ALT"] = "Crear carpeta anidada";
$MESS["WD_ELEMENT_ADD"] = "Crear ";
$MESS["WD_ELEMENT_ADD_ALT"] = "Crear un documento MS Word";
$MESS["WD_UPLOAD"] = "Cargar";
$MESS["WD_UPLOAD_ALT"] = "Carga nuevos archivos para esta carpeta ";
$MESS["WD_UPLOAD_ROOT_ALT"] = "Carga nuevos archivos para la raíz de la biblioteca";
$MESS["WD_GO_BACK"] = "Documentos";
$MESS["WD_GO_BACK_ALT"] = "Regresar a la lista de los documentos";
$MESS["WD_DELETE_CONFIRM"] = "¿Está usted seguro que desea borrar el elemento (la sección)? Esta operación no podrá deshacerse! ";
$MESS["WD_HELP"] = "Ayuda";
$MESS["WD_HELP_ALT"] = "Muestra ayuda en el uso del documento de la biblioteca ";
$MESS["WD_MAPING"] = "Unidad de la red ";
$MESS["WD_MAPING_ALT"] = "Utiliza el Explorador de windows para ver los documentos.";
$MESS["WD_EMPTY_PATH"] = "La ruta de la red no está especificada.";
$MESS["WD_SUBSCRIBE"] = "Suscribirse";
$MESS["WD_SUBSCRIBE_TO_FORUM"] = "Suscribirse a los comentarios en los documentos";
$MESS["WD_UNSUBSCRIBE"] = "Desafiliarse";
$MESS["WD_UNSUBSCRIBE_FROM_FORUM"] = "Desafiliarse de los comentarios en los documentos";
$MESS["BPATT_HELP2"] = "Secuencia del proceso de negocio";
$MESS["WD_BP"] = "Proceso de negocio";
$MESS["WD_ERROR_1"] = "El documento no se pudo crear.
La solicitud requerida no se puede instalar correctamente, o la plantilla de esta biblioteca de documentos no se puede abrir";
$MESS["WD_ERROR_2"] = "'Crear' requiere una aplicación de Microsoft Internet Explorer 6.0 o superior. Para agregar un documento a esta biblioteca de documentos, haga clic en el botón 'Subir'.";
$MESS["BPATT_HELP1_TEXT"] = "El estado de la unidad del proceso de negocio es un proceso continuo de negocios con la distribución de permisos de acceso para manejar documentos en diferentes estados.";
$MESS["BPATT_HELP2_TEXT"] = "Un proceso de negocio secuencial es un  proceso de negocio simple  usado para llevar a cabo una serie de acciones consecutivas sobre un documento.
";
$MESS["BPATT_HELP1"] = "El estado de la unidad del proceso de negocio";
$MESS["WD_HELP_TEXT"] = "<ul class='wd-content' style='color:#052635;font-family:Verdana;font-size:14px;'>
<li>asegúrese que tiene <a target=\"_blank\" href='http://www.microsoft.com/downloads/details.aspx?FamilyId=43109663-3627-4949-BD43-F2247570B9EC&displaylang=en'>Web Carpetas</a> software instalado</li>
<li>Activar Windows Explorer</li>
<li>Seleccionar <b>Service</b> > <b>mapa de la unidad de la red</b></li>
<li>Click<b>Regístrese en almacenamiento en línea o conéctese a un servidor de red
</b>; para ejecutar el Asistente agregar sitio de red. La pantalla del asistente inicial se abrirá
.</li>
<li>Click <b>siguiente</b>.</li>
<li>Seleccione la opción de elegir otra ubicación de red y haga clic en
<b>siguiente</b>.</li>
<li>Escriba la dirección URL de la carpeta web para conectarse
 (#BASE_URL#) in <b> dirección de internet o de la red </b>.</li>
<li>Click <b>siguiente</b> para abrir el diálogo.</li>
<li>Ingresar el usuario y contraseña.  Haga click <b>Ok</b>. El sistema lo autoriza, y el Asistente abre el siguiente
 paso.</li>
<li>Aquí puede cambiar el nombre de la carpeta web, en cualquier cadena que desee. Si es necesario, cambiar el nombre del recurso de red a cualquier cosa de su elección.
 Haga click <b>Siguiente</b> para abrir el último paso del asistente.</li>
<li>El último paso del asistente le informa de la conexión correcta y sugiere abrir la carpeta al cerrar la ventana del asistente.
 Click <b>terminar</b>.</li>
<li>El sistema se conectará la carpeta y abrir el Explorador de Windows que muestra la carpeta acaba de conectarse (si ha elegido así).
.</li>
</ul>";
$MESS["WD_CLEAN_TRASH"] = "Vaciar papelera de reciclaje";
$MESS["WD_CONFIRM_CLEAN_TRASH"] = "¿Está usted seguro que desea vaciar la papelera de reciclaje?";
$MESS["WD_DELETE_TITLE"] = "Confirmar eliminación";
$MESS["WD_Y"] = "Si";
$MESS["WD_N"] = "No";
$MESS["WD_FF_EXTENSION_NAME"] = "Integración de la Intranet de Bitrix";
$MESS["WD_FF_EXTENSION_TITLE"] = "Extensión de Firefox";
$MESS["WD_BTN_INSTALL"] = "Instalar la extensión";
$MESS["WD_BTN_UPDATE"] = "Actualizar Extensión";
$MESS["WD_BTN_INSTALL_CANCEL"] = "Cerrar";
$MESS["WD_BTN_OPEN"] = "No instalar";
$MESS["WD_MENU_FF_EXTENSION_TEXT"] = "Extensión del Firefox";
$MESS["WD_MENU_FF_EXTENSION_TITLE"] = "Integración de la Intranet de Bitrix";
$MESS["WD_MENU_EDIT_IN"] = "Editar en";
$MESS["WD_MENU_EDIT_IN_OTHER"] = "Editar";
$MESS["WD_FF_EXTENSION_UPDATE"] = "Por favor actualizar el <a href=\"#LINK#\">\"#NAME#\"</a>el soporte de navegador del documento de ofice agregado en..";
$MESS["WD_FF_EXTENSION_INSTALL"] = "Por favor instale el navegador agregado en<a href=\"#LINK#\">\"#NAME#\"</a> para Firefox.";
$MESS["WD_FF_EXTENSION_DISABLE"] = "<input type=\"checkbox\" name=\"ff_extension_disable\" value=\"true\">&nbsp;No volverme a recordar la instalación de la extensión";
$MESS["WD_FF_EXTENSION_HELP"] = "Esta extensión le brinda soporte para la edición del documento de office sin descargarlos a una computadora local. Uno de los paquetes del software es requerido: Microsoft Office, OpenOffice.org o LibreOffice.";
$MESS["WD_MENU_GROUP_DISK_DISCONNECTED"] = "Hacer al grupo accesible a través My Drive";
$MESS["WD_MENU_GROUP_DISK_CONNECTED"] = "accesible a través de la unidad de grupo vía My Drive";
$MESS["WD_STOP_DROP_TRASH"] = "Stop";
$MESS["WD_DROP_TRASH_COUNT_ELEMENTS"] = "Archivos que desea eliminar:";
?>