<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["CRM_PRESET_ENTITY_TYPE_INVALID"] = "Modelos de detalhes de contato ou empresa incorretos";
$MESS["CRM_REQUISITE_ENTITY_TYPE_INVALID"] = "Tipo de entidade de detalhes de contato ou empresa incorreto";
$MESS["CRM_REQUISITE_FORM_ID_INVALID"] = "ID de formulário de detalhes de contato ou empresa incorreto";
$MESS["CRM_REQUISITE_PRESET_NAME_EMPTY"] = "Modelo sem título";
?>