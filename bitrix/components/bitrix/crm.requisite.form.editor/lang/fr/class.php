<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PRESET_ENTITY_TYPE_INVALID"] = "Modèles de données de contact ou de la société incorrects";
$MESS["CRM_REQUISITE_ENTITY_TYPE_INVALID"] = "Type d'entité de données de contact ou de la société incorrect";
$MESS["CRM_REQUISITE_FORM_ID_INVALID"] = "ID du formulaire des données de contact ou de la société incorrecte";
$MESS["CRM_REQUISITE_PRESET_NAME_EMPTY"] = "Template sans titre";
?>