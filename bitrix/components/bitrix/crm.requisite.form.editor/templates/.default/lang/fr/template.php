<?
$MESS["CRM_JS_STATUS_ACTION_SUCCESS"] = "Réussi";
$MESS["CRM_JS_STATUS_ACTION_ERROR"] = "C'est une erreur.";
$MESS["CRM_REQUISITE_PRESET_SELECTOR_TEXT"] = "Ajouter un nouveau";
$MESS["CRM_REQUISITE_POPUP_TITLE_CONTACT"] = "Données de contact";
$MESS["CRM_REQUISITE_POPUP_TITLE_COMPANY"] = "Données de la société";
$MESS["CRM_REQUISITE_SERCH_RESULT_NOT_FOUND"] = "Votre recherche n'a donné aucun résultat.";
$MESS["CRM_REQUISITE_PRESET_SELECTOR_TITLE_COMPANY"] = "Ajouter des données de la société via un modèle";
$MESS["CRM_REQUISITE_PRESET_SELECTOR_TITLE_CONTACT"] = "Ajouter des données de contact via un modèle";
$MESS["CRM_REQUISITE_POPUP_ERR_PRESET_NOT_SELECTED_COMPANY"] = "Aucun modèle existant n'est disponible.";
$MESS["CRM_REQUISITE_POPUP_ERR_PRESET_NOT_SELECTED_CONTACT"] = "Aucun modèle existant n'est disponible.";
?>