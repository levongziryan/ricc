<?
$MESS["CRM_JS_STATUS_ACTION_SUCCESS"] = "Sucesso";
$MESS["CRM_JS_STATUS_ACTION_ERROR"] = "Isso é um erro.";
$MESS["CRM_REQUISITE_PRESET_SELECTOR_TEXT"] = "Adicionar novo";
$MESS["CRM_REQUISITE_POPUP_TITLE_CONTACT"] = "Detalhes do contato";
$MESS["CRM_REQUISITE_POPUP_TITLE_COMPANY"] = "Detalhes da empresa";
$MESS["CRM_REQUISITE_SERCH_RESULT_NOT_FOUND"] = "Sua solicitação de pesquisa não deu resultados.";
$MESS["CRM_REQUISITE_PRESET_SELECTOR_TITLE_COMPANY"] = "Adicionar detalhes da empresa usando modelo";
$MESS["CRM_REQUISITE_PRESET_SELECTOR_TITLE_CONTACT"] = "Adicionar detalhes de contato usando modelo";
$MESS["CRM_REQUISITE_POPUP_ERR_PRESET_NOT_SELECTED_COMPANY"] = "Não existem modelos existentes disponíveis.";
$MESS["CRM_REQUISITE_POPUP_ERR_PRESET_NOT_SELECTED_CONTACT"] = "Não existem modelos existentes disponíveis.";
?>