<?
$MESS["CRM_JS_STATUS_ACTION_SUCCESS"] = "Éxitoso";
$MESS["CRM_JS_STATUS_ACTION_ERROR"] = "Eso es un error.";
$MESS["CRM_REQUISITE_PRESET_SELECTOR_TEXT"] = "Agregar nuevo";
$MESS["CRM_REQUISITE_POPUP_TITLE_CONTACT"] = "Datos del contacto";
$MESS["CRM_REQUISITE_POPUP_TITLE_COMPANY"] = "Datos de la compañía";
$MESS["CRM_REQUISITE_SERCH_RESULT_NOT_FOUND"] = "Su solicitud de busqueda no tiene ningún resultado.";
$MESS["CRM_REQUISITE_PRESET_SELECTOR_TITLE_COMPANY"] = "Agrear datos de la compañía utilizando la plantilla";
$MESS["CRM_REQUISITE_PRESET_SELECTOR_TITLE_CONTACT"] = "Agrear datos del contacto utilizando la plantilla";
$MESS["CRM_REQUISITE_POPUP_ERR_PRESET_NOT_SELECTED_COMPANY"] = "No hay plantillas existentes disponibles.";
$MESS["CRM_REQUISITE_POPUP_ERR_PRESET_NOT_SELECTED_CONTACT"] = "No hay plantillas existentes disponibles.";
?>