<?
$MESS["CRM_BP_TOOLBAR_ADD"] = "Agregar plantilla";
$MESS["CRM_BP_TOOLBAR_ADD_TITLE"] = "Agregar nueva plantilla";
$MESS["CRM_BP_TOOLBAR_TYPES"] = "Tipos";
$MESS["CRM_BP_TOOLBAR_TYPES_TITLE"] = "Tipos disponibles";
$MESS["CRM_BP_LIST_NAME"] = "Nombre";
$MESS["CRM_BP_LIST_DATE_MODIFY"] = "Modificado en";
$MESS["CRM_BP_LIST_MODIFIED_BY"] = "Modificado por";
$MESS["CRM_BP_LIST_AUTOSTART"] = "Autoejecución";
?>