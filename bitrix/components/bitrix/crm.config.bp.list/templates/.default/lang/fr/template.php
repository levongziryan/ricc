<?
$MESS["CRM_BP_TOOLBAR_ADD"] = "Ajouter un modèle";
$MESS["CRM_BP_TOOLBAR_ADD_TITLE"] = "Créer un nouveau modèle";
$MESS["CRM_BP_TOOLBAR_TYPES"] = "Liste de modèles";
$MESS["CRM_BP_TOOLBAR_TYPES_TITLE"] = "Liste des processus d'affaires";
$MESS["CRM_BP_LIST_NAME"] = "Dénomination";
$MESS["CRM_BP_LIST_DATE_MODIFY"] = "Date de modification";
$MESS["CRM_BP_LIST_MODIFIED_BY"] = "Modifié(e)s par";
$MESS["CRM_BP_LIST_AUTOSTART"] = "Autochargement";
?>