<?
$MESS["M_CRM_CONTACT_VIEW_PULL_TEXT"] = "Pulsar hacia abajo para actualizar...";
$MESS["M_CRM_CONTACT_VIEW_DOWN_TEXT"] = "Soltar para actualizar...";
$MESS["M_CRM_CONTACT_VIEW_LOAD_TEXT"] = "Actualizando...";
$MESS["M_CRM_CONTACT_VIEW_ACTION_CALL_TO"] = "Llamada";
$MESS["M_CRM_CONTACT_VIEW_ACTIVITY_LIST"] = "Actividades";
$MESS["M_CRM_CONTACT_VIEW_DEAL_LIST"] = "Negociaciones";
$MESS["M_CRM_CONTACT_VIEW_EVENT_LIST"] = "Historial";
$MESS["M_CRM_CONTACT_VIEW_RESPONSIBLE"] = "Persona responsable";
$MESS["M_CRM_CONTACT_VIEW_COMMENT"] = "Comentarios";
$MESS["M_CRM_CONTACT_VIEW_COMMENT_CUT"] = "Leer más...";
$MESS["M_CRM_CONTACT_VIEW_ADDRESS"] = "Dirección";
$MESS["M_CRM_CONTACT_VIEW_PHONE"] = "Teléfono";
$MESS["M_CRM_CONTACT_VIEW_EMAIL"] = "Correo electrónico";
$MESS["M_CRM_CONTACT_VIEW_WEB"] = "Sitio Web";
$MESS["M_CRM_CONTACT_VIEW_IM"] = "Messenger";
$MESS["M_CRM_CONTACT_VIEW_TYPE"] = "Tipo";
$MESS["M_CRM_CONTACT_VIEW_SOURCE"] = "Origen
";
$MESS["M_CRM_CONTACT_VIEW_EDIT"] = "Editar";
$MESS["M_CRM_CONTACT_VIEW_DELETE"] = "Eliminar";
$MESS["M_CRM_CONTACT_VIEW_DELETION_CONFIRMATION"] = "¿Está seguro que desea eliminar el contacto?";
$MESS["M_CRM_CONTACT_VIEW_DELETION_TITLE"] = "Eliminar contacto";
$MESS["M_CRM_CONTACT_VIEW_CREATE_INVOICE"] = "Crear factura";
?>