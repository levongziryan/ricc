<?
$MESS["CRM_DEAL_CONV_ACCESS_DENIED"] = "Você precisa de permissão para criar contatos, empresas e vendas para concluir a operação.";
$MESS["CRM_DEAL_CONV_GENERAL_ERROR"] = "Erro de conversão genérico.";
$MESS["CRM_DEAL_CONV_DIALOG_TITLE"] = "Gerar entidade com base na venda";
$MESS["CRM_DEAL_CONV_DIALOG_CONTINUE_BTN"] = "Continuar";
$MESS["CRM_DEAL_CONV_DIALOG_CANCEL_BTN"] = "Cancelar";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_LEGEND"] = "As entidades selecionadas não têm campos que podem armazenar dados de venda. Selecione entidades em que campos faltantes serão criados para acomodar todas as informações disponíveis.";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_FILED_LIST_TITLE"] = "Estes campos serão criados";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_ENTITY_LIST_TITLE"] = "Selecionar as entidades nas quais os campos adicionais serão criados";
$MESS["CRM_DEAL_CATEGORY_SELECT_DLG_TITLE"] = "Preferências de venda";
$MESS["CRM_DEAL_CATEGORY_SELECT_DLG_FIELD"] = "Pipeline";
$MESS["CRM_BUTTON_SAVE"] = "Salvar";
$MESS["CRM_BUTTON_CANCEL"] = "Cancelar";
$MESS["CRM_DEAL_MOVE_TO_CATEGORY_DLG_TITLE"] = "Transferir para novo canal";
$MESS["CRM_DEAL_MOVE_TO_CATEGORY_DLG_SUMMARY"] = "Vendas atualmente em andamento serão transferidas para a etapa inicial do novo canal; vendas realizadas irão para a etapa \"Venda concluída\"; vendas perdidas - para a etapa \"Venda perdida\". Observe que os registros atualizados da etapa pertencentes ao canal de origem serão removidos do histórico. Campos personalizados que foram utilizados para calcular números apresentados serão reiniciados. Eles irão coletar dados a partir do zero assim que as vendas forem transferidas para o novo canal.";
?>