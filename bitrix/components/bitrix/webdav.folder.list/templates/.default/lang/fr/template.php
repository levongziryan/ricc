<?
$MESS["WD_CONFIRM_RESTORE_TITLE"] = "Récupération de documents";
$MESS["WD_RESTORE_DONE"] = "La restauration est achevée.";
$MESS["WD_ALL"] = "Total";
$MESS["WD_CONFIRM_RESTORE"] = "Etes-vous sûr de vouloir restaurer les documents sélectionnés dans le panier?";
$MESS["WD_DOC_RESTORE_DONE"] = "Le document est restauré.";
$MESS["WD_DELETE_DONE"] = "Document supprimé.";
$MESS["WD_TITLE_TIMESTAMP"] = "Modifié(e)s le";
$MESS["WD_EMPTY_TRASH_DONE"] = "Corbeille vidée.";
$MESS["WD_TITLE_NAME"] = "Dénomination";
$MESS["WD_SEC_RESTORE_DONE"] = "Dossier restauré.";
$MESS["WD_SECTION_DELETE_DONE"] = "Le dossier est supprimé.";
$MESS["WD_MOVE_TO"] = "Déplacer le message dans";
$MESS["WD_TITLE_FILE_SIZE"] = "Rempli";
?>