<?
$MESS["WD_TITLE_TIMESTAMP"] = "Modif.";
$MESS["WD_TITLE_NAME"] = "Nombre";
$MESS["WD_TITLE_FILE_SIZE"] = "Tamaño";
$MESS["WD_MOVE_TO"] = "Quitar a";
$MESS["WD_ALL"] = "Total";
$MESS["WD_DELETE_DONE"] = "El documento ha sido eliminado.";
$MESS["WD_SECTION_DELETE_DONE"] = "La carpeta ha sido eliminada.";
$MESS["WD_EMPTY_TRASH_DONE"] = "La papelera de reciclaje ha sido vaciada.";
$MESS["WD_CONFIRM_RESTORE"] = "¿Usted desea recuperar los documentos seleccionados?";
$MESS["WD_CONFIRM_RESTORE_TITLE"] = "Recuperar el Documento";
$MESS["WD_DOC_RESTORE_DONE"] = "El documento ha sido recuperado.";
$MESS["WD_SEC_RESTORE_DONE"] = "La carpeta ha sido recuperada.";
$MESS["WD_RESTORE_DONE"] = "La recuperación ha finalizado.";
?>