<?
$MESS["WD_TITLE_TIMESTAMP"] = "Modif.";
$MESS["WD_TITLE_NAME"] = "Nome";
$MESS["WD_TITLE_FILE_SIZE"] = "Tamanho";
$MESS["WD_MOVE_TO"] = "Mover para";
$MESS["WD_ALL"] = "Total";
$MESS["WD_DELETE_DONE"] = "O documento foi apagado.";
$MESS["WD_SECTION_DELETE_DONE"] = "A pasta foi excluída.";
$MESS["WD_EMPTY_TRASH_DONE"] = "A lixeira foi limpa.";
$MESS["WD_CONFIRM_RESTORE"] = "Você quer recuperar os documentos selecionados?";
$MESS["WD_CONFIRM_RESTORE_TITLE"] = "Recuperação de documentos";
$MESS["WD_DOC_RESTORE_DONE"] = "O documento foi recuperado.";
$MESS["WD_SEC_RESTORE_DONE"] = "A pasta foi recuperada.";
$MESS["WD_RESTORE_DONE"] = "A recuperação foi completa.";
$MESS["W_TITLE_EXTERNAL_WFNEW"] = "Não publicado";
$MESS["W_TITLE_STATUS"] = "Estado";
$MESS["W_TITLE_CODE"] = "Código simbólico";
$MESS["SHOW_RATING"] = "Ativar classificação";
$MESS["WD_ERROR_RECOVER"] = "Não é possivel recuperar o documento.";
$MESS["WD_FILE_CREATED"] = "CRIADO POR";
$MESS["WD_VIEW"] = "Ver.";
$MESS["Send"] = "Carregar";
$MESS["WD_LOADING"] = "Carregando ...";
$MESS["TP_BST_CONTAINER_ID"] = "ID do layout de Container (para confinar os resultados de pesquisa largura)";
$MESS["WD_UPLOAD"] = "Carregar";
$MESS["WD_ERROR_DELETE"] = "A seção não foi excluída.";
$MESS["WD_DOCSTATUS_RED"] = "Sendo editado por outra pessoa";
?>