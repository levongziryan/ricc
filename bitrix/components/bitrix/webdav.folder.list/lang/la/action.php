<?
$MESS["WD_ERROR_BAD_SESSID"] = "Su sesión ha expirado. Por favor repita la operación.";
$MESS["WD_ERROR_BAD_ACTION"] = "La acción carpeta no se ha especificado.";
$MESS["WD_ACCESS_DENIED"] = "Acceso Denegado.";
$MESS["WD_ERROR_EMPTY_ACTION"] = "No se especificó la acción.";
$MESS["WD_ERROR_EMPTY_TARGET_SECTION"] = "El destio de la carpeta no está especificada.";
$MESS["WD_ERROR_DELETE"] = "No se puede borrar.";
$MESS["WD_ERROR_EMPTY_DATA"] = "Ningún archivo o carpeta ha sido seleccionada.";
$MESS["WD_ERROR_EMPTY_NAME"] = "El nuevo documento o el nombre de la carpeta no están especificados.";
$MESS["WD_ERROR_UNDELETE"] = "Error de recuperación.";
?>