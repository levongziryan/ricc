<?
$MESS["WD_ACCESS_DENIED"] = "Acesso negado.";
$MESS["WD_TITLE"] = "Biblioteca";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "Módulo WebDav não está instalado.";
$MESS["WD_DOCUMENTS"] = "Documentos";
$MESS["WD_TITLE_CONTENT"] = "Nome do Documento";
$MESS["WD_UNLOCK"] = "Desbloquear";
$MESS["WD_FILE"] = "Arquivo";
$MESS["T_IBLOCK_DESC_NEWS_PANEL"] = "Mostrar botões do painel para este componente";
$MESS["WD_ERROR_DOUBLE_NAME_TITLE"] = "Erro ao enviar o arquivo  \"#NAME#\" com o título \"#TITLE#\". Um arquivo com esse título \"#TITLE#\" já existe.";
?>