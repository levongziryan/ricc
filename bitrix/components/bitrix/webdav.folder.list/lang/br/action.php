<?
$MESS["WD_ACCESS_DENIED"] = "Acesso negado.";
$MESS["WD_ERROR_DELETE"] = "Não é possível excluir.";
$MESS["WD_ERROR_EMPTY_ACTION"] = "Nenhuma ação especificada.";
$MESS["WD_ERROR_EMPTY_DATA"] = "Nenhum arquivo ou pasta selecionada.";
$MESS["WD_ERROR_EMPTY_TARGET_SECTION"] = "A pasta de destino não é especificada.";
$MESS["WD_ERROR_BAD_ACTION"] = "A ação da pasta não é especificada.";
$MESS["WD_ERROR_BAD_SESSID"] = "Sua sessão expirou. Por favor, repita a operação.";
$MESS["WD_ERROR_EMPTY_NAME"] = "Novo documento ou nome da pasta não é especificado.";
$MESS["WD_ERROR_UNDELETE"] = "Recuperação de erros.";
$MESS["SEARCH_FONT_MAX"] = "Maior tamanho de fonte (px)";
$MESS["W_TITLE_EXTERNAL_SHOWS"] = "Shows";
$MESS["WD_UNLOCK_ELEMENT"] = "Fechar e Desbloquear documento";
$MESS["IBEL_E_PUBLISHED"] = "Publicado";
$MESS["WD_ELEMENT_HISTORY_GET_URL"] = "Histórico";
$MESS["WD_ELEMENT_EDIT_URL"] = "Página de edição de elementos";
$MESS["WD_FILE_MODIFIED"] = "Editar Arquivo";
$MESS["WD_TITLE_TIMESTAMP"] = "Modif.";
$MESS["DOCS_COUNT_TIP"] = "Especifica o nÃºmero de elementos por página.";
?>