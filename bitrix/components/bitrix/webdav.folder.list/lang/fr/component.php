<?
$MESS["WD_TITLE"] = "Libye";
$MESS["WD_DOCUMENTS"] = "Documents";
$MESS["WD_ACCESS_DENIED"] = "Accès interdit.";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "Le module de la bibliothèque des documents n'a pas été installé.";
$MESS["WD_TITLE_CONTENT"] = "Titre ou texte du document";
?>