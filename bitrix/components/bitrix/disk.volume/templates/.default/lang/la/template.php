<?
$MESS["DISK_VOLUME_START_TITLE"] = "Espacio Libre";
$MESS["DISK_VOLUME_START_COMMENT"] = "Bitrix24 escaneará sus documentos y mostrará un resumen de los archivos que pueden eliminarse de forma segura para liberar espacio.";
$MESS["DISK_VOLUME_START_COMMENT_EXPERT"] = "Puede activar el modo Experto para analizar todos los archivos en su drive.";
$MESS["DISK_VOLUME_MEASURE_DATA"] = "Iniciar escaneo";
$MESS["DISK_VOLUME_MEASURE_TITLE"] = "Buscando archivos para una limpieza segura...";
$MESS["DISK_VOLUME_MEASURE_PROCESS"] = "El escaneo del drive puede llevar algo de tiempo.";
$MESS["DISK_VOLUME_MAY_DROP_TITLE"] = "Limpieza segura";
$MESS["DISK_VOLUME_MAY_DROP"] = "Los elementos encontrados se pueden eliminar de forma segura.";
$MESS["DISK_VOLUME_MAY_DROP_TRASHCAN"] = "Papelera de Reciclaje";
$MESS["DISK_VOLUME_MAY_DROP_TRASHCAN_NOTE"] = "Archivos eliminados";
$MESS["DISK_VOLUME_MAY_DROP_UNNECESSARY_VERSION"] = "No usado";
$MESS["DISK_VOLUME_MAY_DROP_UNNECESSARY_VERSION_NOTE"] = "Versiones de archivos heredados";
$MESS["DISK_VOLUME_RUN_CLEANER"] = "Ejecutar limpieza";
$MESS["DISK_VOLUME_AVAILABLE_SPACE"] = "será liberado después de la limpieza";
$MESS["DISK_VOLUME_CANNT_DROP"] = "No se encontraron archivos para una limpieza segura.";
$MESS["DISK_VOLUME_CANCEL"] = "Cancelar";
$MESS["DISK_VOLUME_START_FILES_NONE"] = "No hay archivos en tu drive en este momento. Tan pronto como empiece a usar su drive, Bitrix24 la escaneará continuamente y le notificará si hay archivos que se pueden eliminar de forma segura para liberar espacio.";
?>