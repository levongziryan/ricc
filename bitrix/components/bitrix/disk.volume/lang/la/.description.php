<?
$MESS["DISK_SERVICE"] = "Drive";
$MESS["DISK_VOLUME_NAME"] = "Calcule el espacio utilizado del disco";
$MESS["DISK_VOLUME_DESC"] = "Calcula el espacio ocupado en el disco y solicita usar una herramienta limpiadora de disco si es necesario.";
?>