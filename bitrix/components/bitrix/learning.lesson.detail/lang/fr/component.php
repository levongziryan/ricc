<?
$MESS["LEARNING_COURSES_LESSON_DELETE_CONF"] = "Toutes les informations relatives à cette écriture seront effacées! Continuer?";
$MESS["LEARNING_COURSES_QUEST_M_ADD"] = "Créer  une question (choix multiple)";
$MESS["LEARNING_COURSES_QUEST_S_ADD"] = "Créer  une question (choix unique)";
$MESS["LEARNING_COURSES_LESSON_EDIT"] = "Changement de leçon";
$MESS["LEARNING_COURSE_DENIED"] = "Le cours ne peut pas être trouvé ou l'accès à lui est refusé.";
$MESS["LEARNING_COURSES_LESSON_VIEW"] = "Afficher la leçon";
$MESS["LEARNING_COURSES_LESSON_DELETE"] = "Eliminer la Leçon";
$MESS["LEARNING_MODULE_NOT_FOUND"] = "Module des blogs non installé.";
$MESS["LEARNING_LESSON_DENIED"] = "Leçon introuvable ou accès refusé.";
?>