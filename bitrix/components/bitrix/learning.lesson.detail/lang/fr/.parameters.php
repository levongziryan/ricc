<?
$MESS["LEARNING_PATH_TO_USER_PROFILE"] = "Chemin vers la page de l'utilisateur";
$MESS["LEARNING_DESC_YES"] = "Oui";
$MESS["LEARNING_COURSE_ID"] = "Identificateur du cours";
$MESS["LEARNING_LESSON_ID_NAME"] = "Identifiant de la leçon";
$MESS["LEARNING_DESC_NO"] = "Non";
$MESS["LEARNING_CHECK_PERMISSIONS"] = "Vérifier le droit d'accès";
$MESS["LEARNING_SELF_TEST_TEMPLATE_NAME"] = "Page d'auto-teste URL";
?>