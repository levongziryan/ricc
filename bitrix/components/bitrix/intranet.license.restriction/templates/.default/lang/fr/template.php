<?
$MESS["LICENSE_RESTR_TITLE"] = "Violation des conditions du CLUF";
$MESS["LICENSE_RESTR_TEXT"] = "Malheureusement, vous avez dépassé le maximum d'utilisateurs autorisés pour votre Bitrix24.CRM.";
$MESS["LICENSE_RESTR_TEXT2"] = "D'après le <a href=\"https://www.bitrix24.com/eula/\" target=\"_blank\" class=\"intranet-license-restriction-link\">CLUF</a>, vous pouvez ajouter <span class=\"intranet-license-restriction-text-bold\">jusqu'à #NUM# utilisateurs actifs</span>.";
$MESS["LICENSE_RESTR_TEXT3"] = "Votre Bitrix24.CRM contient actuellement <span class=\"intranet-license-restriction-count\">#NUM#</span> utilisateurs";
$MESS["LICENSE_RESTR_TEXT4"] = "Contactez votre administrateur Bitrix24 pour masquer cette notification.";
?>