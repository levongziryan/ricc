<?
$MESS["WD_COMMENTS"] = "Comentarios";
$MESS["WD_FILE_NAME"] = "Arquivo";
$MESS["WD_CHANGE_DATE"] = "Modificado";
$MESS["WD_MODIFIED_BY"] = "Modificado por";
$MESS["WD_STATUS"] = "Estado";
$MESS["WD_ALL"] = "Total";
$MESS["WD_COPY_EXT_LINK_TITLE"] = "Partilhe este item";
$MESS["WD_FILE_CREATED"] = "CRIADO";
$MESS["WD_ERROR_EMPTY_SECTION_NAME"] = "O nome da pasta foi especificado.";
?>