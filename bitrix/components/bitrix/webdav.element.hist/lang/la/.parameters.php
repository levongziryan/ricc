<?
$MESS["WD_IBLOCK_TYPE"] = "Tipo del block de información";
$MESS["WD_IBLOCK_ID"] = "Block de información";
$MESS["WD_ELEMENT_ID"] = "ID del elemento";
$MESS["T_IBLOCK_DESC_NEWS_PANEL"] = "Mostrar botones del panel para este componente";
$MESS["WD_SECTION_LIST_URL"] = "Página de vista del catálogo";
$MESS["WD_USER_VIEW_URL"] = "Página de información del usuario";
$MESS["WD_ELEMENT_HISTORY_URL"] = "Página del Historial de edición del elemento";
$MESS["WD_ELEMENT_HISTORY_GET_URL"] = "Página del Historial";
$MESS["PAGE_ELEMENTS"] = "Elementos por página";
$MESS["PAGE_NAVIGATION_TEMPLATE"] = "Plantilla de navegación del breadcrumb";
$MESS["WD_ELEMENT_URL"] = "Página del vista del elemento (para búsqueda)";
$MESS["WD_ELEMENT_EDIT_URL"] = "Página de edición del elemento ";
$MESS["WD_PERMISSION"] = "Permisos de acceso externo (utilizar en componentes compuestos)";
$MESS["WD_ROOT_SECTION_ID"] = "ID de la raíz de la sección (usar en componentes compuestos)";
$MESS["WD_CHECK_CREATOR"] = "Comprobar el creador del archivo";
$MESS["WD_NAME_TEMPLATE"] = "Nombre de formato";
?>