<?
$MESS["W_IBLOCK_IS_NOT_INSTALLED"] = "El módulo de los blocks de información no están instalados.";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "El módulo del WebDav no está instalado.";
$MESS["W_WORKFLOW_IS_NOT_INSTALLED"] = "El módulo del flujo de trabajo no está instalado.";
$MESS["WD_ERROR_ELEMENT_NOT_FOUND"] = "El elementos no fue encontrado.";
$MESS["WD_TITLE"] = "Historial de modificación del elemento";
$MESS["WD_DOCUMENTS"] = "Documentos";
$MESS["WD_ACCESS_DENIED"] = "Acceso Denegado.";
$MESS["WD_DELETE"] = "Borrar";
$MESS["WD_DELETE_CONFIRM"] = "Usted está seguro que desea borrar el elemento?Esta operación no puede ser deshecha!";
$MESS["WD_RESTORE"] = "Restaurar";
$MESS["WD_RESTORE_ELEMENT"] = "Restaurar";
$MESS["WD_VIEW"] = "Ver";
$MESS["WD_VIEW_ELEMENT"] = "Ver propiedades del archivo";
$MESS["WD_DOWNLOAD"] = "Descargar";
$MESS["WD_DOWNLOAD_ELEMENT"] = "Descargar archivo";
$MESS["WD_DELETE_ELEMENT"] = "Borrar el elemento";
$MESS["WD_IBLOCK_WF_IS_NOT_ACTIVE"] = "El block de información del flujo de trabajo está habilitado.";
?>