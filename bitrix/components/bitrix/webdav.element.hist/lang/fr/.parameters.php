<?
$MESS["WD_ROOT_SECTION_ID"] = "ID de la section de racine (il est recommandé d'utiliser en ensemble avec un composant complexe)";
$MESS["WD_ELEMENT_ID"] = "Identifiant de l'élément";
$MESS["WD_PERMISSION"] = "Droits d'accès externes (il est recommandé d'utiliser dans le cadre d'un composant intégré)";
$MESS["T_IBLOCK_DESC_NEWS_PANEL"] = "Ajouter les boutons au panneau admin. Pour ce composant";
$MESS["WD_IBLOCK_ID"] = "Bloc d'information";
$MESS["PAGE_ELEMENTS"] = "Nombre d'éléments par page";
$MESS["WD_CHECK_CREATOR"] = "Vérifier le propriétaire du document";
$MESS["WD_ELEMENT_EDIT_URL"] = "Page de modification de l' élément";
$MESS["WD_ELEMENT_HISTORY_GET_URL"] = "Page de l'histoirique";
$MESS["WD_ELEMENT_HISTORY_URL"] = "Page de l'historique des modifications de l'élément";
$MESS["WD_USER_VIEW_URL"] = "Page d'affichage de l'information sur l'utilisateur";
$MESS["WD_SECTION_LIST_URL"] = "Page d'examen du catalogue";
$MESS["WD_ELEMENT_URL"] = "Page de la revue de l'élément (pour la recherche)";
$MESS["WD_IBLOCK_TYPE"] = "Type du bloc d'information";
$MESS["WD_NAME_TEMPLATE"] = "Format du nom";
$MESS["PAGE_NAVIGATION_TEMPLATE"] = "Modèle de pagination";
?>