<?
$MESS["WD_RESTORE"] = "Agrandir";
$MESS["WD_RESTORE_ELEMENT"] = "Agrandir";
$MESS["WD_ERROR_ELEMENT_NOT_FOUND"] = "lément introuvable.";
$MESS["WD_DOCUMENTS"] = "Documents";
$MESS["WD_ACCESS_DENIED"] = "Accès interdit.";
$MESS["WD_IBLOCK_WF_IS_NOT_ACTIVE"] = "Le bloc d'information ne prend pas part à l'échange de documents.";
$MESS["WD_TITLE"] = "Historique de modifications du document";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "Le module de la bibliothèque des documents n'a pas été installé.";
$MESS["W_WORKFLOW_IS_NOT_INSTALLED"] = "Le module d'échange de documents n'a pas été installé.";
$MESS["W_IBLOCK_IS_NOT_INSTALLED"] = "Le module blocs d'information n'a pas été installé";
$MESS["WD_VIEW"] = "Affichage";
$MESS["WD_DOWNLOAD"] = "Télécharger";
$MESS["WD_DOWNLOAD_ELEMENT"] = "Télécharger le document";
$MESS["WD_VIEW_ELEMENT"] = "Voir les propriétés du document";
$MESS["WD_DELETE"] = "Supprimer";
$MESS["WD_DELETE_CONFIRM"] = "tes-vous sûr de supprimer l'enregistrement archivé définitivement?";
$MESS["WD_DELETE_ELEMENT"] = "Suppression d'un élément";
?>