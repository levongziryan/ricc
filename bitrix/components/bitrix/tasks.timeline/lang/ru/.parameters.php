<?php
$MESS ['INTL_OWNER_ID'] = 'Код владельца';
$MESS ['INTL_PATH_TO_USER_PROFILE'] = 'Путь к профилю пользователя';
$MESS ['INTL_PATH_TO_GROUP_TASKS'] = 'Путь к списку задач группы';
$MESS ['INTL_PATH_TO_GROUP_TASKS_TASK'] = 'Путь к задаче группы';
$MESS ['INTL_PATH_TO_USER_TASKS'] = 'Путь к списку задач пользователя';
$MESS ['INTL_PATH_TO_USER_TASKS_TASK'] = 'Путь к задаче пользователя';
$MESS ['INTL_ITEM_COUNT'] = 'Количество записей на странице';
$MESS ['INTL_NAME_TEMPLATE'] = 'Отображение имени';
$MESS ['INTL_USER_ID'] = 'Идентификатор пользователя';
$MESS ['INTL_GROUP_ID'] = 'Идентификатор группы задач';