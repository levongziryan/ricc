<?
$MESS["TASK_LIST_TASK_NOT_INSTALLED"] = "Модуль керування завданнями не встановлено.";
$MESS["TASK_LIST_SOCNET_NOT_INSTALLED"] = "Модуль соціальної мережі не встановлено.";
$MESS["TASK_LIST_NOT_AVAILABLE_IN_THIS_EDITION"] = "Модуль керування завданнями не доступний в даній редакції продукту.";
$MESS["TASK_LIST_ACCESS_DENIED"] = "Ви не можете переглядати список завдань.";
$MESS["TASK_LIST_ACCESS_TO_GROUP_DENIED"] = "Ви не можете переглядати список завдань у цій групі.";
$MESS["TASK_LIST_USER_NOT_FOUND"] = "Користувача не знайдено.";
$MESS["TASK_LIST_GROUP_NOT_FOUND"] = "Групу не знайдено.";
$MESS["TASK_LIST_TASK_ACTION_DENIED"] = "Заборонено здійснювати дану дію.";
$MESS["TASK_LIST_TASK_CREATE_DENIED"] = "Ви не можете створювати завдання.";
$MESS["TASK_LIST_TITLE"] = "Завдання";
$MESS["TASK_LIST_TITLE_MY"] = "Мої завдання";
$MESS["TASK_LIST_COLUMN_TITLE_EMPTY"] = "Назва стадії не може бути порожньою.";
$MESS["TASK_LIST_UNKNOWN_ACTION"] = "Дію не визначено.";
$MESS["TASK_LIST_COLUMN_NOT_EMPTY"] = "У цій стадії є завдання. Перемістіть їх, щоб видалити стадію.";
$MESS["TASK_LIST_SESS_EXPIRED"] = "Ваша сесія закінчилася.";
$MESS["TASK_LIST_ERROR_CHANGE_DEADLINE"] = "Заборонено змінювати крайній термін завдання.";
$MESS["TASK_LIST_TASK_TL_STAGE_FAR"] = "Більш ніж 7 днів";
$MESS["TASK_LIST_TASK_TL_STAGE_NEAR"] = "7 днів";
$MESS["TASK_LIST_TASK_TL_STAGE_TOMORROW"] = "Завтра";
$MESS["TASK_LIST_TASK_TL_STAGE_TODAY"] = "Сьогодні";
$MESS["TASK_LIST_TASK_TL_STAGE_OVERDUE"] = "Прострочені";
$MESS["TASK_LIST_TASK_TL_STAGE_COMPLETE"] = "Завершені";
$MESS["TASK_LIST_TITLE_GROUP"] = "Завдання групи";
?>