<?
$MESS["TASK_LIST_TASK_NOT_INSTALLED"] = "Das Modul Aufgaben ist nicht installiert.";
$MESS["TASK_LIST_SOCNET_NOT_INSTALLED"] = "Das Modul Soziales Netzwerk ist nicht installiert.";
$MESS["TASK_LIST_NOT_AVAILABLE_IN_THIS_EDITION"] = "Das Modul Aufgaben ist in dieser Edition nicht verfügbar.";
$MESS["TASK_LIST_ACCESS_DENIED"] = "Sie können die Aufgabenliste nicht anzeigen, weil Sie nicht genügend Rechte haben.";
$MESS["TASK_LIST_ACCESS_TO_GROUP_DENIED"] = "Sie können die Aufgabenliste für diese Gruppe nicht anzeigen, weil Sie nicht genügend Rechte haben.";
$MESS["TASK_LIST_USER_NOT_FOUND"] = "Der Nutzer wurde nicht gefunden.";
$MESS["TASK_LIST_GROUP_NOT_FOUND"] = "Projektgruppe wurde nicht gefunden.";
$MESS["TASK_LIST_TASK_ACTION_DENIED"] = "Diese Aktion kann nicht ausgeführt werden, weil Zugriff verweigert wurde.";
$MESS["TASK_LIST_TASK_CREATE_DENIED"] = "Sie können keine Aufgaben erstellen, weil Sie nicht genügend Rechte haben.";
$MESS["TASK_LIST_TITLE"] = "Aufgaben";
$MESS["TASK_LIST_TITLE_MY"] = "Meine Aufgaben";
$MESS["TASK_LIST_COLUMN_TITLE_EMPTY"] = "Phasenname kann nicht leer sein.";
$MESS["TASK_LIST_UNKNOWN_ACTION"] = "Unbekannte Aktion.";
$MESS["TASK_LIST_COLUMN_NOT_EMPTY"] = "Es gibt Aufgaben in dieser Phase. Sie sollen diese Aufgaben verschieben, bevor Sie die Phase löschen.";
$MESS["TASK_LIST_SESS_EXPIRED"] = "Ihre Sitzung ist abgelaufen.";
$MESS["TASK_LIST_ERROR_CHANGE_DEADLINE"] = "Aufgabenfrist kann nicht geändert werden.";
$MESS["TASK_LIST_TASK_TL_STAGE_FAR"] = "Über 7 Tage";
$MESS["TASK_LIST_TASK_TL_STAGE_NEAR"] = "7 Tage";
$MESS["TASK_LIST_TASK_TL_STAGE_TOMORROW"] = "Morgen";
$MESS["TASK_LIST_TASK_TL_STAGE_TODAY"] = "Heute";
$MESS["TASK_LIST_TASK_TL_STAGE_OVERDUE"] = "Überfällig";
$MESS["TASK_LIST_TASK_TL_STAGE_COMPLETE"] = "Abgeschlossen";
$MESS["TASK_LIST_TITLE_GROUP"] = "Aufgaben der Projektgruppe";
?>