<?
$MESS["INTL_OWNER_ID"] = "Besitzer-ID";
$MESS["INTL_PATH_TO_USER_PROFILE"] = "Pfad zum Nutzerprofil";
$MESS["INTL_PATH_TO_GROUP_TASKS"] = "Pfad zur Seite der Aufgaben der Projektgruppe";
$MESS["INTL_PATH_TO_GROUP_TASKS_TASK"] = "Pfad zur Aufgabe der Projektgruppe";
$MESS["INTL_PATH_TO_USER_TASKS"] = "Pfad zur Seite der Aufgaben des Nutzers";
$MESS["INTL_PATH_TO_USER_TASKS_TASK"] = "Pfad zur Aufgabe des Nutzers";
$MESS["INTL_ITEM_COUNT"] = "Elemente pro Seite";
$MESS["INTL_NAME_TEMPLATE"] = "Namenansicht";
$MESS["INTL_USER_ID"] = "Nutzer-ID";
$MESS["INTL_GROUP_ID"] = "ID der Aufgabengruppe";
?>