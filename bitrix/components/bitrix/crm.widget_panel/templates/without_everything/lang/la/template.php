<?
$MESS["CRM_WGT_RATING_LEGEND"] = "total #LEGEND#";
$MESS["CRM_WGT_RATING_NOMINEE_POSITION"] = "Su clasificación ##POSITION#";
$MESS["CRM_WGT_RATING_POSITION"] = "##POSITION#";
$MESS["CRM_WGT_CONFIG_DLG_TITLE"] = "Parámetros de widget";
$MESS["CRM_WGT_CONFIG_DLG_SAVE_BTN"] = "Guardar";
$MESS["CRM_WGT_CONFIG_DLG_CANCEL_BTN"] = "Cancelar";
$MESS["CRM_WGT_PRESET_NOT_SELECTED"] = "no seleccionado";
$MESS["CRM_WGT_PERIOD_CAPTION"] = "Período de información";
$MESS["CRM_WGT_REMOVAL_CONFIRMATION"] = "¿Usted está seguro que quiere eliminar el widget? \"#TITLE#\"?";
$MESS["CRM_WGT_MENU_ITEM_CONFIGURE"] = "Configuraciones";
$MESS["CRM_WGT_MENU_ITEM_REMOVE"] = "Eliminar";
$MESS["CRM_WGT_PERIOD_DESCR_YEAR"] = "#YEAR#";
$MESS["CRM_WGT_PERIOD_DESCR_QUARTER"] = "#FIRST_MONTH# &#8211; #LAST_MONTH# de #YEAR#";
$MESS["CRM_WGT_PERIOD_DESCR_MONTH"] = "#MONTH# #YEAR#";
$MESS["CRM_WGT_PERIOD_DESCR_LAST_DAYS"] = "últimos #DAYS# días";
$MESS["CRM_WGT_FIELD_TITLE_PLACEHOLDER"] = "Ingrese su nombre";
$MESS["CRM_WGT_PRESET_NAME"] = "Estadísticas para recolectar";
$MESS["CRM_WGT_GROUPING_CAPTION"] = "Agrupamiento";
$MESS["CRM_WGT_EXPR_LEGEND_DIFF"] = "El tercer número es el primer número menos el segundo";
$MESS["CRM_WGT_EXPR_LEGEND_SUM"] = "El tercer número es el primer número más el segundo";
$MESS["CRM_WGT_EXPR_LEGEND_PERCENT"] = "El tercer número es la relación entre el primer número y el segundo como un porcentaje";
$MESS["CRM_WGT_EXPR_HINT"] = "Seleccione la acción";
$MESS["CRM_WGT_COLOR_RED"] = "rojo";
$MESS["CRM_WGT_COLOR_GREEN"] = "verde";
$MESS["CRM_WGT_COLOR_BLUE"] = "azul";
$MESS["CRM_WGT_COLOR_CYAN"] = "cyan";
$MESS["CRM_WGT_COLOR_YELLOW"] = "amarillo";
$MESS["CRM_WGT_LIST_HINT_TITLE"] = "Alternar vista";
$MESS["CRM_WGT_DISABLE_LIST_HINT"] = "No mostrar de nuevo";
$MESS["CRM_WGT_PERIOD_ACCORDING_TO_FILTER"] = "Por filtro";
$MESS["CRM_WGT_MENU_ITEM_RESET"] = "Restaurar la configuración predeterminada del panel";
$MESS["CRM_WGT_REBUILD_DEAL_STATISTICS"] = "Los informes requieren que <a id=\"#ID#\" href=\"#URL#\">actualice las estadísticas de las negociaciiones</a> para que se ejecuten correctamente.";
$MESS["CRM_WGT_REBUILD_DEAL_STATISTICS_DLG_TITLE"] = "Actualizar estadísticas de las negociaciones";
$MESS["CRM_WGT_REBUILD_DEAL_STATISTICS_DLG_SUMMARY"] = "Esto actualizará las estadísticas de la negociación. Esto puede llevar mucho tiempo.";
$MESS["CRM_WGT_LRP_DLG_BTN_START"] = "Ejecutar";
$MESS["CRM_WGT_LRP_DLG_BTN_STOP"] = "Pausar";
$MESS["CRM_WGT_LRP_DLG_BTN_CLOSE"] = "Cerrar";
$MESS["CRM_WGT_LRP_DLG_WAIT"] = "Espere por favor...";
$MESS["CRM_WGT_LRP_DLG_REQUEST_ERR"] = "Error al procesar la solicitud.";
$MESS["CRM_WGT_DISABLE_DEMO"] = "Ocultar datos de demo";
$MESS["CRM_WGT_MENU_ITEM_ENABLE_DEMO_MODE"] = "Vista con datos de demo";
$MESS["CRM_WGT_TYPE_SELECTOR_DLG_TITLE"] = "Nuevo reporte";
$MESS["CRM_WGT_MENU_ITEM_ADD"] = "Agregar reporte";
$MESS["CRM_WGT_UNTITLED"] = "Sin título";
$MESS["CRM_WGT_SELECTOR_TYPE_NUMBER"] = "Número";
$MESS["CRM_WGT_SELECTOR_TYPE_NUMBER_BLOCK"] = "Block de números";
$MESS["CRM_WGT_SELECTOR_TYPE_NUMBER_BLOCK_EXPR"] = "Expresión numérica";
$MESS["CRM_WGT_SELECTOR_TYPE_RATING"] = "Clasificación";
$MESS["CRM_WGT_SELECTOR_TYPE_FUNNEL"] = "Embudo";
$MESS["CRM_WGT_SELECTOR_TYPE_BAR_CLUSTERED"] = "Gráfico de columnas";
$MESS["CRM_WGT_SELECTOR_TYPE_BAR_STACKED"] = "Stacked column chart";
$MESS["CRM_WGT_SELECTOR_TYPE_GRAPH"] = "Grafico";
$MESS["CRM_WGT_CONFIG_ADD_GRAPH"] = "+ Agregar gráfico";
$MESS["CRM_WGT_LAYOUT_TYPE_SELECTOR_DLG_TITLE"] = "Seleccionar diseño";
$MESS["CRM_WGT_MENU_CHANGE_LAYOUT"] = "Seleccionar diseño";
$MESS["CRM_WGT_CONFIG_ERROR_MAX_GRAPH_COUNT"] = "No se puede agregar el gráfico porque su informe ha alcanzado un máximo de #MAX_GRAPH_COUNT# gráficos.";
$MESS["CRM_WGT_CONFIG_ERROR_MAX_WIDGET_COUNT"] = "No se puede agregar el informe porque ha alcanzado un máximo de #MAX_WIDGET_COUNT# reportes.";
$MESS["CRM_WGT_PRESET_SEMANTICS"] = "Seleccione etapas y/o estados";
$MESS["CRM_WGT_CATEGORY_NOT_SELECTED"] = "no seleccionado";
$MESS["CRM_WGT_PRESET_CATEGORY"] = "Selecciona una categoría";
$MESS["CRM_WGT_CATEGORY_GROUP_TITLE"] = "Categoría";
$MESS["CRM_WGT_SELECTOR_TYPE_PIE"] = "Gráfico circular";
$MESS["CRM_WGT_LIST_HINT_CONTENT"] = "Use este botón para cambiar la vista";
$MESS["CRM_WGT_FILTER_NAV_BUTTON_LIST"] = "Lista";
$MESS["CRM_WGT_FILTER_NAV_BUTTON_WIDGET"] = "Reportes";
$MESS["CRM_WGT_FILTER_NAV_BUTTON_KANBAN"] = "Kanban";
$MESS["CRM_WGT_SELECTOR_TYPE_BAR_STACKED_WITH_AVATARS"] = "Gráfico apilado con los avatares";
$MESS["CRM_WGT_SELECTOR_TYPE_GRAPH_AREA"] = "Zoom de gráfico";
?>