<?
$MESS["BX24_INVITE_DIALOG_EMAIL_ERROR"] = "Die E-Mail-Adressen sind nicht korrekt.";
$MESS["BX24_INVITE_DIALOG_USER_EXIST_ERROR"] = "Die Nutzer mit diesen E-Mail-Adressen existieren bereits.";
$MESS["BX24_INVITE_DIALOG_MAX_COUNT_ERROR"] = "Die Anzahl der eingeladenen Personen überschreitet das Limit Ihres Tarifs.";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT"] = "Ich lade Sie in unser Intranet ein. Hier können wir gemeinsam an Projekten und Aufgaben arbeiten, Termine und Besprechungen planen, Dokumente verwalten, miteinander kommunizieren usw.";
?>