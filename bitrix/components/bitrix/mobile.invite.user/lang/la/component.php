<?
$MESS["BX24_INVITE_DIALOG_EMAIL_ERROR"] = "Las direcciones de correo electrónico son incorrectas.";
$MESS["BX24_INVITE_DIALOG_USER_EXIST_ERROR"] = "Los usuarios con las direcciones de correo electrónico ya existe.";
$MESS["BX24_INVITE_DIALOG_MAX_COUNT_ERROR"] = "El número de personas invitadas excede los términos de licencia.";
$MESS["BX24_INVITE_DIALOG_INVITE_MESSAGE_TEXT"] = "Únase a nosotros en nuestra nueva cuenta de Bitrix24. Este es un lugar donde todos pueden comunicarse, colaborar en tareas y proyectos, administrar clientes y hacer mucho más.";
?>