<?
$MESS["INTRANET_USTAT_TELLABOUT_SOCNET_TITLE"] = "Unissons l'entreprise";
$MESS["INTRANET_USTAT_TELLABOUT_SOCNET_TEXT"] = "Chers collègues, nous voyons tous que les technologies évoluent à une vitesse folle. Les réseaux sociaux ont devancé beaucoup d'autres moyens de communication avec les amis et les connaissances grâce au confort et la simplicité d'échange de messages et de photos, l'accessibilité à partir même des périphériques mobiles. Il nous faut alors utiliser le meilleur de ce qu'il y a dans les réseaux sociaux dans notre travail.

[IMG WIDTH=500 HEIGHT=338]https://www.bitrix24.com/images/ustat/en/socnet1.png[/IMG]

[B]Pourquoi avons-nous besoin d'un réseau social interne?[/B]

[LIST] 
[*] premièrement, nous unirons l'information, les documents, les connaissances accumulées, les contacts, dans un même système; 
[*] cela simplifiera et accélérera la résolution des questions liées au travail (vous recevez plus rapidement un message de Rétroaction des collègues dans Bitrix24, que, par exemple, en amassant de nombreux Re: et Fw: par courrier électronique); 
[*] vous ne manquerez aucun message important; 
[*] vous pourrez proposer des idées, partager des opinions, de l'information utile, votre message sera vu par tous (ou seulement par ceux que vous ajouterez aux 'destinataires'); 
[*] et, enfin, les 'Like' des collègues sont motivants et euphorisants:)
[/LIST]

Je propose de commencer l'usage des fonctionnalités de notre intranet social dès maintenant. Je raconte comment ça fonctionne.

[VIDEO WIDTH=400 HEIGHT=300]https://www.youtube.com/watch?v=fN6BsyjOOtY[/VIDEO]

[B]Comme envoyer un message[/B]
Sur notre portail on peut partager n'importe quelle information qui sera utile dans le travail. On peut envoyer des messages à tous les membres du personnel, à un groupe définie ou à une personne personnellement. Les destinataires verront votre message dans le 'volet' commun.

[IMG WIDTH=500 HEIGHT=313]https://www.bitrix24.com/images/ustat/en/socnet2.png[/IMG]

A un message on peut joindre un fichier, une photo, ajouter un lien, un tag, s'adresser à un collègue en le marquant:

[IMG WIDTH=500 HEIGHT=330]https://www.bitrix24.com/images/ustat/en/socnet3.png[/IMG]

[B]Comment rejoindre un groupe et comment créer son propre groupe[/B]
Les groupes sont un outil commode pour travailler sur des projets. Pour créer un groupe et inviter des participants, servez-vous du menu 'Ajouter' (le bouton vert à gauche de l'angle supérieur). Vous pouvez rejoindre un groupe existant en choisissant le groupe souhaité dans la section 'Groupe'.

[B]Comment exprimer sa reconnaissance aux collègues[/B]
N'oubliez pas de marquer les réalisations de vos collègues, de vos subordonnés et même des chefs: appuyez sur le bouton 'Plus' dans le menu Messages, choisissez 'Remercier', indiquez qui vous voulez remercier et choisissez l'icône correspondante sous le texte du message.

[IMG WIDTH=500 HEIGHT=350]https://www.bitrix24.com/images/ustat/en/socnet4.png[/IMG]

[B]Estimez le travail avec Bitrix24[/B]

Par ailleurs, vous pouvez voir à quel point vous avez maîtrisé tels ou tels outils, c'est à cela que sert le système des réalisations 'Pouls d'activité'.

[IMG WIDTH=500 HEIGHT=271]https://www.bitrix24.com/images/ustat/en/socnet5.png[/IMG]

Le pouls vous aidera à être au courant pour savoir à quel point vous avez maîtrisé le réseau social et à quelle fréquence vous l'utilisez. Vos résultats, vous pouvez les comparés avec la moyenne de votre service ou de toute la compagnie.

 [B]Des notification [/B] sur le portail vous aideront à être au courant de tous les changements.

[B]Si vous voulez en apprendre plus[/B]

Ceux qui souhaitent prendre connaissance des possibilités du portail plus en détail, je vous propose de lire [url=http://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=55&LESSON_ID=5179] la documentation [/url].

[B]Commençons dès maintenant![/B]

Vous pouvez commencer à utiliser notre intranet social en discutant ce post, écrivez un commentaire, donnez-moi un 'like', cela me fera plaisir:) et n'oubliez pas d'ajouter ce post à vos favoris;)
";
$MESS["INTRANET_USTAT_TELLABOUT_LIKES_TITLE"] = "J'aime, j'aime, j'aime!";
$MESS["INTRANET_USTAT_TELLABOUT_LIKES_TEXT"] = "Mes amis! Cessons d'être trop timide et commencer à parler tous les trucs cool qui est happing dans l'entreprise. Bien sûr, notre intranet est pas un réseau assez sociale, mais il a «aime» et est un excellent moyen de vous assurer que vous savez ce qui se passe.

Découvrez les messages, commentaires, photos et fichiers et «comme» certains d'entre eux.

[IMG WIDTH=500 HEIGHT=341]https://www.bitrix24.com/images/ustat/en/likes1.png[/IMG]

[B]Comment montrer que vous aimez quelque chose[/B]

Si un poste ou un message d'un collègue est dans l'activité Stream, vous pouvez «comme» en utilisant le bouton comme dans ce poste. Aime prendre seulement une seconde, mais ils ont laissé la personne qui écrit, il sait que la contribution était précieuse à quelqu'un et il permet aux autres savent qu'il pourrait être utile pour eux.

[B]Qui aime ce[/B]

Vous pouvez voir une liste des utilisateurs qui ont «aimé» un poste ou commentaire en planant sur l'étoile à côté du mot comme. Ces goûts sont importants parce que la fonction de recherche donnera la priorité au contenu bien-aimé qui est pertinent pour une recherche donnée.

[IMG WIDTH=500 HEIGHT=255]https://www.bitrix24.com/images/ustat/en/likes2.png[/IMG]

[B]Pourquoi est-il un système de notation avec goûts?[/B]

Social Search - résultats de la recherche signifie qui sont influencés par les semblables, aident les utilisateurs à trouver des documents et des discussions qui ont été particulièrement utiles.

[IMG WIDTH=500 HEIGHT=426]https://www.bitrix24.com/images/ustat/en/likes3.png[/IMG]

Les messages avec le plus 'aime' se affichés dans la section «Messages populaires» sur la première page.

[B]Que pouvez-vous? [/B]

Les messages et les commentaires ne sont pas les seules choses qui peuvent être goûts. Vous pouvez aimer tâches quand ils seront terminés à motiver ses collaborateurs à continuer de travailler dur. Fichiers et des photos peuvent également être aimé.

[IMG WIDTH=500 HEIGHT=322]https://www.bitrix24.com/images/ustat/en/likes4.png[/IMG]

 [B]L'évaluation de votre activité goût [/B]

Pour afficher le niveau d'activité dont vous utilisez la fonction «comme», utiliser le pouls Société et comparer votre score avec celui de votre département ou de l'ensemble de l'entreprise.

[IMG WIDTH=500 HEIGHT=271]https://www.bitrix24.com/images/ustat/en/likes5.png[/IMG]

 [B]Lancer l'aimer! [/B]

Vous pouvez commencer à utiliser le bouton «like» maintenant dans ce poste très. Ne pas oublier de mettre aime sur les fichiers et les tâches que vous jugez importants ou que vous avez utilisé comme référence. Encouragez vos collègues et de travailler un peu plus comme une équipe - le bouton comme il est simple!
Ne pas oublier d'aimer ce message :)
";
$MESS["INTRANET_USTAT_TELLABOUT_TASKS_TITLE"] = "Travaillons d'arrache-pied!";
$MESS["INTRANET_USTAT_TELLABOUT_TASKS_TEXT"] = "Regardez attentivement votre bureau de travail: il est fort probable que de petites notes avec des affaires courantes soient partout;) Ou bien les notes dans votre agenda, des 'rappels' dans votre calendrier, 'signes' dans la correspondance postale?  Comment procéder pour ne pas oublier toutes les tâches à faire et avoir assez de temps

Aujourd'hui je vous propose d'essayer un nouvel instrument  Tâches au Bitrix24 qui permettra d'assigner les tâches aux collègues et à vous-même d'une façon très simple et rapide. Bitrix24 va tout garder dans sa mémoire et, ce qui est le plus important - il va vous faire des rappels à temps.

[VIDEO WIDTH=400 HEIGHT=300]https://www.youtube.com/watch?v=WyDI_eZzRb4[/VIDEO]

[IMG WIDTH=500 HEIGHT=330]https://www.bitrix24.com/images/ustat/en/task1.png[/IMG]

Si quelqu'un n'a pas encore pris connaissance des Tâches, je vais expliquer comment on peut travailler avec elles:

[B]Comment Ajouter une tâche à vous-même ou à votre collègue?[/B]

Il est facile de créer une tâche. Cliquez sur Ajouter > Tâches, dans une fenêtre apparue indiquez l'essentiel de la tâche, assignez le responsable, définissez un délai d'exécution. Décrivez ce qu'il faut faire. Si c'est nécessaire, choisissez des coexécuteurs, ajoutez les fichiers. Sauvegardez des changements. Vous pouvez mettre la tâche à l'intérieur du groupe de travail pour assurer le travail collectif sur le projet.

[IMG WIDTH=500 HEIGHT=359]https://www.bitrix24.com/images/ustat/en/task2.png[/IMG]

[B]Comment travailer sur la tâche avec vos collèges[/B]

Discutez les variantes de solution avec vos collègues, ajoutez le commentaire directement dans la Tâche ou dans la bande défilante recevez la réponse instantanément. Cette procédure est beaucoup plus commode et rapide que la discussion par email ou au cours d'un entretien ordinaire. Vous aurez toujours sous la main le suivi complet de votre travail: les commentaires et les fichiers sont sauvegardés dans la tâche.

[IMG WIDTH=500 HEIGHT=398]https://www.bitrix24.com/images/ustat/en/task3.png[/IMG]

[B]Comment contrôler les délais pour ne pas manquer une tâche[/B]

Veillez à l'accomplissement de vos tâches à temps. Des compteurs vont vous aider (ils se trouvent dans le menu 'Mes instruments' et sur un onglet 'Tâches').

[IMG WIDTH=500 HEIGHT=340]https://www.bitrix24.com/images/ustat/en/task4.png[/IMG]

Ces compteurs indiquent des tâches non accomplies: périmées, non examinées ou bien des tâches aux délais d'exécution indéfinis. Essayez de baisser le compteur au minimum: définissez des délais réels, décalez-les au cas échéant, fermez des tâches terminées. L'état idéal - c'est l'absence de ces compteurs (cela signifie que vous faites toutes les tâches à temps:)

[B]Comment faire le compte du temps nécessaire pour une tâche[/B]

Faites le contrôle du temps que vous dépensez pour une tâche. Ajoutez de nouvelles tâches dans un planning de votre journée de travail pour avoir sous les yeux la liste des activités et ne pas manquer quelque chose d'important. Fixez le temps dont vous avez besoin pour trouver la solution à une tâche donnée.

Au cours de la création d'une tâche marquez 'Contrôler le temps'.

[IMG WIDTH=500 HEIGHT=446]https://www.bitrix24.com/images/ustat/en/task5.png[/IMG]

Maintenant, après un clic sur 'Commencer l'exécution', le temps passé sera compté.

[IMG]https://www.bitrix24.com/images/ustat/en/task6.png[/IMG]

Le temps est compté jusqu'a ce que vous ne cliquiez sur la pause. Le compteur du temps d'arrête aussi quand vous avez une pause marquée sur votre calendrier et à la fin de votre journée de travail.

[IMG WIDTH=500 HEIGHT=399]https://www.bitrix24.com/images/ustat/en/task7.png[/IMG]

[B]Pour savoir plus[/B]

Pour ceux qui souhaitent avoir plus de détails sur les possibilités assurées par cet instrument je reccommande [URL=http://bitrixsoft.com/learning/course/index.php?COURSE_ID=52&CHAPTER_ID=05018&LESSON_PATH=3922.5010.5018]un cours de formation[/URL] destiné aux utilisateurs du Bitrix24.

[B]Evaluer votre travail avec les tâches[/B]

'Le pouls d'activité' va évaluer vos aptitudes de travail avec les tâches et va permettre de suivre le progrès de vos collègues:

[IMG WIDTH=500 HEIGHT=269]https://www.bitrix24.com/images/ustat/en/task8.png[/IMG]

Dans les commentaires à ce message vous pouvez laisser toutes vos idées, souhaits, remarques et impressions.

Bonne chance à tous! N'oubliez pas d'ajouter ce post aux favoris:)

P.S. Il est intéressant de savoir quelle sera votre première tâche au Bitrix24?:)
";
$MESS["INTRANET_USTAT_TELLABOUT_IM_TITLE"] = "Communiquer avec plaisir!";
$MESS["INTRANET_USTAT_TELLABOUT_IM_TEXT"] = "Chers collègues, quels outils utilisez-vous pour discuter entre vous de questions urgentes ? L'email, le téléphone, les SMS, le skype, ICQ, VKontakte...? Vu cette diversité d'outils, il devient parfois assez difficile de trouver l'information nécessaire. L'historique de la correspondance est stocké en divers endroits, s'il a été réellement sauvegardé du tout. Nous ne pouvons pas organiser n'importe quand un chat groupé, et les appels ne sont pas parfois accessibles à tout le monde...

[B]Chat[/B]

[IMG WIDTH=500 HEIGHT=339]https://www.bitrix24.com/images/ustat/en/im1.png[/IMG]

Prêtez attention à ce qu'il existe déjà sur notre portail les contacts de tous les collaborateurs de la compagnie et qu'il ne faut pas ajouter personne ' à la main '. Le chat conserve tout l'historique des communications, il est possible de contacter par chat ' un à un ', de réunir plusieurs collègues pour discussions, faire des appels ordinaires et vidéo !

[B]Les contacts des collègues sont déjà sur la liste[/B]

Il est facile de commencer à chater: il suffit d'ouvrir la fenêtre messages. Il ne faut ajouter personne aux contacts: tous les collaborateurs figurent déjà sur la liste. On peut rapidement trouver dans la liste tout collègue d'après son nom ou son nom de famille au moyen de la fonction recherche. Les contacts de ceux avec qui vous chatez le plus souvent s'affichent sous l'onglet ' Les plus récents '.

[IMG WIDTH=500 HEIGHT=339]https://www.bitrix24.com/images/ustat/en/im2.png[/IMG]

Attention: si votre collègue est en ligne (c'est-à-dire, prêt à être contacté au moment donné), le témoin lumineux de la présence sera vert et s'il est absent, le témoin est rouge.

Toute communication se passe en temps réel. Contactant par chat, vous verrez que votre message a été lu et le collègue écrit la réponse.

[IMG]https://www.bitrix24.ru/images/ustat/ru/im3.png[/IMG]

[B]Chat en groupe[/B]

S'il devient nécessaire de discuter d'urgence telle ou telle question avec plusieurs collègues à la fois, il n'est pas obligatoire de convoquer une réunion et d'occuper une salle de conférences. On peut tout simplement contacter les collaborateurs par chat en groupe.

[IMG WIDTH=500 HEIGHT=349]https://www.bitrix24.com/images/ustat/en/im4.png[/IMG]

[B]Historique de la correspondance[/B]

Tout le courrier est stocké sur le portail, et vous pouvez à tout moment facilement trouver les messages nécessaires. Cliquez sur l'icône ' Historique des messages ' au coin droit en haut de l'écran chat et introduisez la phrase nécessaire dans la ligne recherche.

[IMG WIDTH=500 HEIGHT=345]https://www.bitrix24.com/images/ustat/en/im5.png[/IMG]

[B]Chat pour mobiles[/B]

A propos, le chat est accessible aussi sous forme d'application mobile  vous pouvez recevoir non seulement des messages, mais aussi des avertissements sur les invitations au rendez-vous, les ' like ', les tâches et les commentaires.
Il est facile d'installer l'application mobile Bitrix24 sur un smartphone ou une tablette à partir de [URL=https://itunes.apple.com/app/bitrix24/id561683423]Apple AppStore[/URL] and [URL=https://play.google.com/store/apps/details?id=com.bitrix24.android]Google Play[/URL].

[IMG WIDTH=500 HEIGHT=461]https://www.bitrix24.com/images/ustat/en/im6.jpg[/IMG]

[B]Appels vidéo[/B]

Parfois le message écrit dans le chat s'avère insuffisant, car certaines questions exigent un examen plus détaillé. Alors on a recours au chat audio/vidéo auquel, on peut connecter aussi plusieurs utilisateurs et cela gratuitement!

[IMG WIDTH=500 HEIGHT=305]https://www.bitrix24.com/images/ustat/en/im7.png[/IMG]

Vous pouvez, au besoin, désactiver le micro.

On peut faire des appels vidéo dans le WEB-messenger à partir du navigateur Chrome. Si vous utilisez un autre navigateur et voulez profiter de la communication visuelle, appelez via l'application desktop.

[B] Application desktop [/B]

Pour rester toujours en ligne, même quand votre navigateur est fermé, installez l'application desktop Bitrix24. Vous obtenez ainsi un chat d'affaires que vous pourrez l'utiliser comme une application ordinaire.

[IMG WIDTH=500 HEIGHT=313]https://www.bitrix24.com/images/ustat/en/im8.jpg[/IMG]

[LIST]
[*]For MacOS [URL]http://dl.bitrix24.com/b24/bitrix24_desktop.dmg[/URL]
[*]For Windows [URL]http://dl.bitrix24.com/b24/bitrix24_desktop.exe[/URL]
[/LIST]

Résumons ce qui a été dit plus haut: nous avons un outil capable de remplacer et de réunir des messengers différents, la téléphonie intérieure et même le service SMS que nous utilisions jusqu'ici. Au moyen du chat et des appels vidéo à Bitrix24, nous pouvons contacter nos collègues par différents moyens: un à un, par groupe, au moyen de messages textuels ou par la voix.
 
[B]Evaluez votre travail avec le chat et les appels vidéo [/B]

Il est assez facile d'apprendre à utiliser le chat, et vous pouvez comparer le niveau de votre activité sur le chat avec celui de vos collègues dans le ' Pouls de l'activité '.

[IMG WIDTH=500 HEIGHT=270]https://www.bitrix24.com/images/ustat/en/im9.png[/IMG]

[B]Si vous voulez en savor plus[/B]

Si vous voulez des précisions concernant l'utilisation du chat et des appels vidéo, vous pouvez lire la documentation:

[LIST]
[*][URL=https://bitrixsoft.com/learning/course/index.php?COURSE_ID=52&LESSON_ID=5121] pour le travail avec le chat [/URL]
[*][URL=https://bitrixsoft.com/learning/course/index.php?COURSE_ID=52&LESSON_ID=5374] pour installation du desktop client du chat [/URL]
[*][URL=https://bitrixsoft.com/learning/course/index.php?COURSE_ID=52&LESSON_ID=5389] pour les appels vidéo [/URL]
[*][URL=https://bitrixsoft.com/learning/course/index.php?COURSE_ID=52&LESSON_ID=5340] pour la version mobile de chat [/URL]
[/LIST]

S'il reste encore des questions à poser, je propose de les examiner par chat et, ce qui serait encore mieux, par appel vidéo:)

Et n'oubliez pas d'ajouter ce poste aux favoris:)

Prenons plaisir à notre dialogue!";
$MESS["INTRANET_USTAT_TELLABOUT_DISK_TITLE"] = "Faire la rédaction du document... en 60 secondes";
$MESS["INTRANET_USTAT_TELLABOUT_DISK_TEXT"] = "Comment la co-édition des documents est-elle effectuée maintenant dans notre entreprise ? Nous envoyons les modifications l'un à l'autre par e-mail, ce qui produit un grand nombre de versions du même document qui sont stockées sous de différentes versions par de différentes personnes. Il est facile de s'y perdre - probablement vous avez rencontré des problèmes pareils. En sauvegardant les documents corrigés dans le réseau local, souvent nous ne pouvons pas déterminer celui qui les a corrigés et la cause de ces corrections. Mais ce qui est le plus important - c'est que sans avoir accès au réseau local nous n'avons pas d'accès aux documents et nous ne pouvons pas être sûrs que la version sauvegardée sur notre ordinateur reste toujours actuelle.

Nous proposons d'utiliser les capacités de notre intranet social et de simplifier considérablement la co-édition des documents.

[VIDEO WIDTH=400 HEIGHT=300]https://www.youtube.com/watch?v=-yepvqmabFw[/VIDEO]

[B]Comment envoyer un fichier[/B]

Il n'y a rien de compliqué, vous pouvez même le faire sans votre éditeur de texte habituel. Pour partager un fichier avec vos collègues et permettre à tous les participants de la discussion d'apporter des modifications à un texte, il vous suffit de créer un message dans 'Activités Récentes' et y attacher le document nécessaire.

[IMG WIDTH=500 HEIGHT=579]https://www.bitrix24.com/images/ustat/en/disk1.png[/IMG]

Maintenant tous les destinataires du message le verront dans le Activités Récentes et pourront afficher, télécharger ou même éEditer le document directement dans la fenêtre de vue.

[IMG WIDTH=500 HEIGHT=562]https://www.bitrix24.com/images/ustat/en/disk2.png[/IMG]

[B] Comment modifier un document en ligne [/B]

Pour modifier les documents vous ne devez plus les sauvegarder sur votre ordinateur  utilisez pour cela les services de cloud: Google Docs ou MS Office Online. Vous pouvez choisir l'un d'eux après avoir cliquer sur le bouton 'Modifier'.

[IMG WIDTH=500 HEIGHT=331]https://www.bitrix24.com/images/ustat/en/disk3.png[/IMG]

Pour faire afficher et éEditer un document vous devrez vous autoriser à votre compte Microsoft ou Google. Faites attention, dans le coin supérieur droit de la fenêtre il y a le bouton Entrée. Si vous n'avez pas de compte, vous pouvez facilement le créer. Cliquez sur le bouton Entrée et suivez le lien ' Enregistrer'. Tout est gratuit. 
Maintenant, vous pouvez éEditer le texte et enregistrer les modifications.
Après la sauvegarde de votre version du fichier, Activités Récentes affiche un nouveau commentaire, qui comprend déjà le document avec vos corrections.

[IMG WIDTH=500 HEIGHT=428]https://www.bitrix24.com/images/ustat/en/disk4.png[/IMG]

[B] Où stocker les documents et comment échanger des fichiers [/B]

Pour que vos documents soient toujours disponibles pour vous personnellement ou pour vos collègues nous recommandons d'utiliser Bitrix24.Disque. Il vous permet de synchroniser des fichiers avec vos collaborateurs, entre votre ordinateur et le stockage de fichiers sur le portail. 
Entrez dans le menu Outils> Mon disque et installez l'application Bitrix24 sur votre ordinateur, connectez Bitrix24.Disque.

[IMG WIDTH=500 HEIGHT=194]https://www.bitrix24.com/images/ustat/en/disk5.png[/IMG]

L'application desktop peut être téléchargée à partir du site bitrix24.ru:
[LIST]
[*]Pour MacOS [URL]http://dl.bitrix24.com/b24/bitrix24_desktop.dmg[/URL] 
[*]Pour Windows [URL]http://dl.bitrix24.com/b24/bitrix24_desktop.exe[/URL]
[/LIST]

Vous avez juste à désigner un dossier ordinaire pour les documents sur votre ordinateur comme le dossier documents pour Bitrix24. Le programme fera tout le reste: il téléchargera les fichiers sur le portail et assurera le suivi de changements à des fichiers et remplacera des fichiers si nécessaire.

[IMG WIDTH=500 HEIGHT=397]https://www.bitrix24.com/images/ustat/en/disk6.png[/IMG]

Si vous éditez un fichier dans ce dossier et enregistrez des modifications, exactement le même fichier apparaîtra dans votre dossier personnel sur le portail Bitrix24. La même chose se produit si vous modifiez un fichier sur le portail et puis ouvrez ce fichier sur votre ordinateur.

[IMG WIDTH=500 HEIGHT=358]https://www.bitrix24.com/images/ustat/en/disk7.png[/IMG]

[B] Comment permettre ou interdire l'affichage de vos fichiers[/B]

Il est toujours possible de distinguer les fichiers personnels (ils seront stockés dans les dossiers personnels), publics (ils sont savegardés dans les dossiers communs de la entreprise) et ceux qui ne sont accessible qu'à des employés définis (dans le dossier du groupe correspondant).

C'est à vous de décider qui peut afficher ou éEditer votre document, par exemple lorsque vous devez envoyer une présentation et donner un lien à quelqu'un de l'extérieur de l'entreprise. Si nécessaire vous pouvez donner accès à un employé particulier pour une période strictement définie, et aussi avec un accès par mot de passe.

[IMG WIDTH=500 HEIGHT=319]https://www.bitrix24.com/images/ustat/en/disk8.png[/IMG]

[B]Evaluer votre travail avec le Disque et les documents [/B]

Afin d'évaluer le degré d'activité de votre travail avec les documents sur le portail et d'utilisation des possibilités de Bitrix24.Disque, consultez 'Pouls de l'activité.' Comparez vos progrès avec les résultats de vos collègues.

[IMG WIDTH=500 HEIGHT=271]https://www.bitrix24.com/images/ustat/en/disk9.png[/IMG]

[B] Si vous souhaitez en savoir plus [/B]

Dans la documentation proposée par les développeurs il est possible d'apprendre plus sur:

[LIST]
[*][URL=https://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=55&CHAPTER_ID=05754]Working with files[/URL]
[*][URL=https://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=55&LESSON_ID=5760] Connexion du Disque [/URL]
[/LIST]
[/LIST]

Bitrix24.Disque et la possibilité d'édition des documents directement sur le portail nous permettent de passer moins de temps en co-éditant des documents. Commençons à utiliser ces outils dès maintenant!

[B] Commençons dès maintenant![/B]

Pour l'entraînement nous proposons à tous qui ont lu ce post de volus identifier dans le document ci-joint: ouvrez, entrez votre nom et enregistrez les modifications. Je suis sûr que vous apprendrez facilement à le faire!

N'oubliez pas d'ajouter cet article à vos favoris:)
";
$MESS["INTRANET_USTAT_TELLABOUT_MOBILE_TITLE"] = "Mobilisation urgente!";
$MESS["INTRANET_USTAT_TELLABOUT_MOBILE_TEXT"] = "Chers amis! Souvent nous devons travailler en dehors du bureau: sur le chemin vers le bureau, au cours des réunions avec les clients, pendant les vols, les voyages d'affaires. Il n'est pas très commode de prendre toujours et partout un ordinateur portable avec vous, tandis qu'il est important de rester tout le temps en contact avec ses collègues. Donc, je vous suggère d'utiliser l'application Bitrix24 portable qui peut être facilement installée sur votre smartphone ou tablette à partir de  url=https://itunes.apple.com/app/bitrix24/id561683423]Apple AppStore[/url] ou [url=https://play.google.com/store/apps/details?id=com.bitrix24.android]Google Play[/url].

[IMG]https://www.bitrix24.com/images/ustat/en/mob1.gif[/IMG]

Maintenant vous avez la possibilité de recevoir les informations de travail, de lire le Activités Récentes, de commenter les messages, d'être informé et de réagir rapidement, où que vous soyez.

[B]Contacts des collègues - toujours à portée de la main[/B]

La liste la plus complète et actuelle des contacts des collègues sera toujours à portée de la main. Dans la version portable Bitrix24 il est aussi facile de trouver des informations sur l'employé en question que sur le portail.

[IMG]https://www.bitrix24.com/images/ustat/en/mob3.png[/IMG] [IMG]https://www.bitrix24.com/images/ustat/en/mob4.png[/IMG]

Ecrivez un message à votre collègue - et il le verra immédiatement sur notre site ou reçevra la notification sur son smartphone s'il est hors du bureau.

[IMG]https://www.bitrix24.com/images/ustat/en/mob5.png[/IMG] [IMG]https://www.bitrix24.com/images/ustat/en/mob6.png[/IMG]

[B]Contenu de l'application portable?[/B]

Dans l'application portable vous avez l'accès à tous les outils principaux existants: 'Actions Récentes' avec les commentaires et J'aime, calendrier avec une liste des réunions, séances d'information et événements pertinents, recherche et examen de documents. Malgré l'absence dans le bureau, vous serez au courant de toutes les affaires et continuerez à communiquer avec les collègues et à discuter des questions de travail en temps réel.

[IMG]https://www.bitrix24.com/images/ustat/en/mob7.png[/IMG] [IMG]https://www.bitrix24.com/images/ustat/en/mob8.png[/IMG] [IMG]https://www.bitrix24.com/images/ustat/en/mob9.png[/IMG]

Si vous êtes loin du bureau, vous pouvez toujours définir les tâches, les commenter, et de surveiller les progrès en eux.

[IMG]https://www.bitrix24.com/images/ustat/en/mob10.png[/IMG] [IMG]https://www.bitrix24.com/images/ustat/en/mob11.png[/IMG]

[B]CRM portable[/B]
Selon toute vraisemblance, la meilleure partie - travaillant dans le CRM de l'application mobile? L'application est entièrement fonctionnel de sorte que lorsque vous êtes en déplacement, vous pouvez obtenir des informations sur les clients, faire des appels à eux, consultez le catalogue de produits, modifier les transactions et même créer des factures. Imaginez combien pratique qui est - changer le statut de plomb ou de traiter juste là au bureau du client!

[IMG WIDTH=500 HEIGHT=320]https://www.bitrix24.com/images/ustat/en/mob12.jpg[/IMG]

[B]Evaluez votre travail dans l'application mobile[/B]

Vous pouvez surveiller la façon dont vous utilisez activement l'application mobile et comparez cela avec votre entreprise ou un service dans le Pulse Société.

[IMG WIDTH=500 HEIGHT=269]https://www.bitrix24.com/images/ustat/en/mob13.png[/IMG]

Des renseignements supplémentaires sur l'application mobile est disponible sur
[URL=https://www.bitrix24.com/features/mobile-and-desktop-apps.php]Bitrix24 website[/URL].

Ne pas oublier d'ajouter cet article à vos favoris :)

Je souhaite que l'aide de l'application mobile que nous allons devenir plus efficace et que même quand nous sommes hors du bureau, nous allons être en mesure de garder tout aller de l'avant.
";
$MESS["INTRANET_USTAT_TELLABOUT_CRM_TITLE"] = "CRM: Service Clients";
$MESS["INTRANET_USTAT_TELLABOUT_CRM_TEXT"] = "Les clients sont tout ce que nous avons. Les potentiels, les existants, les permanents - beaucoup d'entre eux. Notre tâche - pour apprendre à lire et à analyser l'efficacité de chaque client, rappeler-vous chaque transaction sur chaque vente.

Nous avons un excellent outil qui nous permettra de travailler avec des clients dans une manière complètement nouvelle: ensemble, facilement et efficacement. C'est notre CRM (Customer Relationship Management - système de gestion des relations clients) dans Bitrix24 - la meilleure solution pour mener clientèle de contacts et la gestion des transactions et de rapports.

[VIDEO WIDTH=400 HEIGHT=300]https://www.youhit.com/watch?v=zuqeb38dH68[/VIDEO]

[IMG]https://www.bitrix24.fr/images/ustat/fr/crm1.png[/IMG]

[B]Base de clientèle et contacts[/B]

Dans CRM, nous pouvons stocker des informations complètes sur nos clients - entreprises et les travailleurs, avec qui nous travaillons, ainsi que toute l'histoire de la coopération (transactions, de la correspondance, des informations sur les réunions) avec chacun de vos clients.

[IMG]https://www.bitrix24.fr/images/ustat/fr/crm2.png[/IMG]

Si vous avez trouvé un client potentiel avec qui envisagent de développer la coopération, ne soyez pas paresseux [B] pour mettre les données à ce sujet dans CRM[/B]. Ils peuvent être ajoutés manuellement, importés d'autres systèmes ou de notre site (par exemple, des formulaires de rétroaction) les contacts d'éventuels clients - prospects.

[IMG]https://www.bitrix24.fr/images/ustat/fr/crm3.png[/IMG]

[B]Service à la clientèle[/B]

Rappelez-vous, CRM n'est pas seulement la base de données des contacts. Il est facile de transformer le client potentiel en acheteur avec. Planification d'une variété d'activités (appels, lettres, réunions), vous serez en mesure de travailler plus efficacement avec le client, d'avoir en main toutes les informations et rester connecté. Cela vous aidera à conclure rapidement la transaction et effectuer la vente.

[IMG]https://www.bitrix24.fr/images/ustat/fr/crm4.png[/IMG]

[B]Répartition des contacts entre les gestionnaires[/B]

Si il ya un grand nombre de clients potentiels, ils peuvent être répartis entre les gestionnaires. Pour ce faire, créez un processus d'affaires qui décrit un état dans lequel le prospect est transféré à un employé spécifique (par exemple, transactions de plus de 500.000 roubles, vous pouvez envoyer automatiquement un gestionnaire pour travailler avec des clients VIP).

Une fois que le client est prêt à faire un achat  [B]transaction sécurisée directement dans la CRM [/B].

[IMG]https://www.bitrix24.fr/images/ustat/fr/crm5.png[/IMG]

[B]Planification des travaux[/B]

Les informations recueillies vous aidera à travailler avec les clients à l'avenir. Planifier les travaux avec les clients et les transactions pour obtenir le maximum de retour sur votre clientèle. Capacités du système permettent d'assigner des tâches, des rendez-vous du calendrier, envoyer des emails. Par ailleurs, CRM lui-même vous rappelle ce genre de choses que vous devez faire - [B]prêtez attention aux compteurs [/B]!

[IMG]https://www.bitrix24.fr/images/ustat/fr/crm6.png[/IMG]

Directement à partir du système, vous pouvez effectuer toutes les actions nécessaires - projet de loi et l'envoyer au client par e-mail, envoyer des e-mails à un client ou d'un groupe de entreprises, d'appeler les clients, etc.

[IMG]https://www.bitrix24.fr/images/ustat/fr/crm7.png[/IMG]

N'oubliez pas d'analyser les résultats  [B] pour surveiller l'entonnoir des ventes [/B] vous pouvez utiliser des rapports standards en CRM.

[IMG]https://www.bitrix24.fr/images/ustat/fr/crm8.png[/IMG]

[B]Version portable de CRM[/B]

Par ailleurs, surtout pour ceux qui vont souvent aux réunions avec les clients, il existe une version mobile de la CRM. En dehors du bureau, vous pouvez facilement mettre à jour l'ensemble des informations sur l'affaire en cours, fermer et même en créer un nouveau.

[IMG]https://www.bitrix24.fr/images/ustat/fr/crm9_1.png[/IMG] [IMG]https://www.bitrix24.fr/images/ustat/fr/crm9_2.png[/IMG]

[B]Evaluez votre travail avec la CRM[/B]

Utilisez la CRM si activement que possible. Pour suivre la façon dont vous avez maîtrisé ses possibilités, regardez le 'pouls de l'activité' et comparez vos résultats avec ceux de vos collègues.

[IMG]https://www.bitrix24.fr/images/ustat/fr/crm10.png[/IMG]

[B]Si vous voulez en savoir plus[/B]

Des informations complémentaires sont disponibles dans le Tutoriel [URL=https://bitrixsoft.com/learning/course/index.php?COURSE_ID=53&LESSON_ID=4784]CRM dans Bitrix24[/URL].

Ne pas oublier d'ajouter ce site à vos favoris:)

[B]À partir de maintenant ![/B]

Commençons à utiliser cet outil pratique: les contacts des clients, des magasins, des partenaires et contreparties avec lesquelles vous exécutez CRM. Vérifions comment nous pouvons améliorer l'efficacité des ventes et le niveau de service à nos clients !

";
?>