<?
$MESS["INTRANET_USTAT_PERIOD_BUTTON_TODAY"] = "Aujourd'hui";
$MESS["INTRANET_USTAT_PERIOD_BUTTON_WEEK"] = "Semaine";
$MESS["INTRANET_USTAT_PERIOD_BUTTON_MONTH"] = "Mois";
$MESS["INTRANET_USTAT_PERIOD_BUTTON_YEAR"] = "An";
$MESS["INTRANET_USTAT_PERIOD_TITLE"] = "Période de temps:";
$MESS["INTRANET_USTAT_TOGGLE_PEOPLE"] = "Personnes";
$MESS["INTRANET_USTAT_TOGGLE_COMPANY"] = "Société";
$MESS["INTRANET_USTAT_SECTION_SOCNET_HELP_GENERAL"] = "Nombre total des messages et des commentaires publiés pour une période de temps donnée.";
$MESS["INTRANET_USTAT_SECTION_SOCNET_HELP_INVOLVEMENT"] = "Affiche le pourcentage d'employés qui utilisent le réseau social pour la période choisie sur le nombre total d'employés.";
$MESS["INTRANET_USTAT_SECTION_LIKES_HELP_GENERAL"] = "Nombre total des marques 'J'apprécie' pour des messages, commentaires, tâches et d'autres éléments pendant une période sélectionnée.";
$MESS["INTRANET_USTAT_SECTION_LIKES_HELP_INVOLVEMENT"] = "Montre le taux d'employés de la compagnie qui mettent des marques 'J'apprécie' pendant une période sélectionnée.";
$MESS["INTRANET_USTAT_SECTION_TASKS_HELP_GENERAL"] = "Quantité totale de modifications dans les tâches (création d'une nouvelle tâche, commentaires, changement de délais etc.) pendant une période sélectionnée du temps.";
$MESS["INTRANET_USTAT_SECTION_TASKS_HELP_INVOLVEMENT"] = "Montre le taux d'employés utilisant des tâches pendant une période sélectionné.";
$MESS["INTRANET_USTAT_SECTION_IM_HELP_GENERAL"] = "Nombre total de messages envoyés et d'appels sortants pendant la période sélectionnée.";
$MESS["INTRANET_USTAT_SECTION_IM_HELP_INVOLVEMENT"] = "Montre le taux des employés de la compagnie qui utilisent des messages instantanés et des appels-video pendant la période sélectionnée.";
$MESS["INTRANET_USTAT_SECTION_DISK_HELP_GENERAL"] = "Nombre total de fichiers téléchargés ou modifiés dans Bitrix24.Drive pour une période de temps choisie.";
$MESS["INTRANET_USTAT_SECTION_DISK_HELP_INVOLVEMENT"] = "Affiche le pourcentage d'employés utilisant les fichiers et Bitrix24.Drive.";
$MESS["INTRANET_USTAT_SECTION_MOBILE_HELP_GENERAL"] = "Nombre total d'actions dans l'application mobile 'Bitrix24' pour une période temporelle choisie (tout est pris en compte: messages, modifications des tâches, CRM et etc.)";
$MESS["INTRANET_USTAT_SECTION_MOBILE_HELP_INVOLVEMENT"] = "Cela montre la quantité (pourcentage) d'employés de l'entreprise qui utilisent l'application mobile pour une période choisie.";
$MESS["INTRANET_USTAT_SECTION_CRM_HELP_GENERAL"] = "Le nombre total d'actions dans la CRM (changements sur des affaires, des transactions, des prospects, etc) pour la période de temps sélectionnée.";
$MESS["INTRANET_USTAT_SECTION_CRM_HELP_INVOLVEMENT"] = "Afficher le pourcentage de tous les collaborateurs de entreprise qui utilise CRM pour une période donnée.";
?>