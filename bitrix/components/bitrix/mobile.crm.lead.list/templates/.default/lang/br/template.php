<?
$MESS["M_CRM_LEAD_LIST_PULL_TEXT"] = "Puxe para baixo para atualizar ...";
$MESS["M_CRM_LEAD_LIST_DOWN_TEXT"] = "Solte para atualizar ...";
$MESS["M_CRM_LEAD_LIST_LOAD_TEXT"] = "Atualizando ...";
$MESS["M_CRM_LEAD_LIST_SEARCH_BUTTON"] = "Pesquisar";
$MESS["M_CRM_LEAD_LIST_FILTER_NONE"] = "Todos os Leads";
$MESS["M_CRM_LEAD_LIST_FILTER_CUSTOM"] = "Resultados da Pesquisa";
$MESS["M_CRM_LEAD_LIST_SEARCH_PLACEHOLDER"] = "Pesquisa por nome";
$MESS["M_CRM_LEAD_LIST_TITLE"] = "Todos os clientes potenciais";
$MESS["M_CRM_LEAD_ADD"] = "Criar cliente potencial";
$MESS["M_CRM_LEAD_CONV_OPEN_ENTITY_SEL_CONTACT"] = "Selecionar contato da lista...";
$MESS["M_CRM_LEAD_CONV_OPEN_ENTITY_SEL_COMPANY"] = "Selecionar empresa da lista...";
?>