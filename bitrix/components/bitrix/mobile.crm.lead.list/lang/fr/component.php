<?
$MESS["M_CRM_LEAD_LIST_FILTER_NONE"] = "Tous les leads";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["M_CRM_LEAD_LIST_PRESET_MY"] = "Mes leads";
$MESS["M_CRM_LEAD_LIST_PRESET_NEW"] = "Nouveaux leads";
$MESS["M_CRM_LEAD_LIST_FILTER_CUSTOM"] = "Résultats de recherche";
$MESS["M_CRM_LEAD_LIST_EDIT"] = "Modifier";
$MESS["M_CRM_LEAD_LIST_DELETE"] = "Supprimer";
$MESS["M_CRM_LEAD_LIST_JUNK"] = "Indésirable";
$MESS["M_CRM_LEAD_LIST_DECLINE"] = "Rejeter";
$MESS["M_CRM_LEAD_LIST_CREATE_BASE"] = "Créer en utilisant la source";
$MESS["M_CRM_LEAD_LIST_MORE"] = "Plus";
$MESS["M_CRM_LEAD_LIST_CREATE_DEAL"] = "Affaire";
$MESS["M_CRM_LEAD_LIST_ADD_DEAL"] = "Ajouter une activité";
$MESS["M_CRM_LEAD_LIST_CALL"] = "Passer un appel";
$MESS["M_CRM_LEAD_LIST_MEETING"] = "Organiser une réunion";
$MESS["M_CRM_LEAD_LIST_MAIL"] = "Envoyer un message";
$MESS["M_CRM_LEAD_LIST_NO_FILTER"] = "Tous les clients potentiels";
$MESS["M_CRM_LEAD_LIST_PRESET_USER"] = "Filtre personnalisé";
?>