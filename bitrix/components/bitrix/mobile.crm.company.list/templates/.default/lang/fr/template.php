<?
$MESS["M_CRM_COMPANY_LIST_FILTER_NONE"] = "Toutes les entreprises";
$MESS["M_CRM_COMPANY_LIST_LOAD_TEXT"] = "Mise à jour...";
$MESS["M_CRM_COMPANY_LIST_DOWN_TEXT"] = "Relâcher pour mettre à jour...";
$MESS["M_CRM_COMPANY_LIST_SEARCH_BUTTON"] = "Recherche";
$MESS["M_CRM_COMPANY_LIST_SEARCH_PLACEHOLDER"] = "Recherche par la dénomination";
$MESS["M_CRM_COMPANY_LIST_PULL_TEXT"] = "Tirer pour mettre à jour...";
$MESS["M_CRM_COMPANY_LIST_FILTER_CUSTOM"] = "Résultats de recherche";
$MESS["M_CRM_COMPANY_LIST_INDUSTRY"] = "Sphère de l'activité";
$MESS["M_CRM_COMPANY_LIST_TYPE"] = "Entité";
$MESS["M_CRM_COMPANY_LIST_TITLE"] = "Toutes les entreprises";
$MESS["M_CRM_COMPANY_ADD"] = "Ajouter une entreprise";
?>