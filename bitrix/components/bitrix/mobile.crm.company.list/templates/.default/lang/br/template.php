<?
$MESS["M_CRM_COMPANY_LIST_PULL_TEXT"] = "Puxe para baixo para atualizar ...";
$MESS["M_CRM_COMPANY_LIST_DOWN_TEXT"] = "Solte para atualizar ...";
$MESS["M_CRM_COMPANY_LIST_LOAD_TEXT"] = "Atualizando ...";
$MESS["M_CRM_COMPANY_LIST_SEARCH_PLACEHOLDER"] = "Pesquisa por nome";
$MESS["M_CRM_COMPANY_LIST_SEARCH_BUTTON"] = "Pesquisar";
$MESS["M_CRM_COMPANY_LIST_FILTER_NONE"] = "Todas as empresas";
$MESS["M_CRM_COMPANY_LIST_FILTER_CUSTOM"] = "Resultados da Pesquisa";
$MESS["M_CRM_COMPANY_LIST_TYPE"] = "Tipo";
$MESS["M_CRM_COMPANY_LIST_INDUSTRY"] = "Indústria";
$MESS["M_CRM_COMPANY_LIST_TITLE"] = "Todas as empresas";
$MESS["M_CRM_COMPANY_ADD"] = "Adicionar empresa";
?>