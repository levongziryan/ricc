<?
$MESS["M_CRM_COMPANY_LIST_FILTER_NONE"] = "Toutes les entreprises";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["M_CRM_COMPANY_LIST_PRESET_CHANGE_MY"] = "Modifiés par moi";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["M_CRM_COMPANY_LIST_PRESET_MY"] = "Mes entreprises";
$MESS["M_CRM_COMPANY_LIST_FILTER_CUSTOM"] = "Résultats de recherche";
$MESS["M_CRM_COMPANY_LIST_PRESET_USER"] = "Filtre personnalisé";
$MESS["M_CRM_COMPANY_DEALS"] = "Activités";
$MESS["M_CRM_COMPANY_CALLS"] = "Appels";
$MESS["M_CRM_COMPANY_LIST_MORE"] = "Plus...";
$MESS["M_CRM_COMPANY_LIST_EDIT"] = "Modifier";
$MESS["M_CRM_COMPANY_LIST_DELETE"] = "Supprimer";
?>