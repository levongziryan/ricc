<?
$MESS["TMR_SET_TODAY"] = "Ir para hoje";
$MESS["TMR_FILTER_TITLE"] = "Filtrar";
$MESS["TMR_FILTER_DEPT_0"] = "Selecione a partir de estrutura";
$MESS["TMR_EMPLOYEE"] = "Colaborador";
$MESS["TMR_OVERALL"] = "Trabalhando<br />Horas";
$MESS["TMR_OVERALL_DAYS"] = "Trabalhando<br />Dias";
$MESS["TMR_OVERALL_VIOL_GOOD"] = "Taxa de<br />pontuação<br />positiva";
$MESS["TMR_ARRIVAL"] = "Ponto (entrada)";
$MESS["TMR_DEPARTURE"] = "Ponto (saida)";
$MESS["TMR_EXP"] = "expirado";
$MESS["TMR_CAPTION_ARRIVAL"] = "Ponto (Entrada)";
$MESS["TMR_CAPTION_DEPARTURE"] = "Ponto (Saída)";
$MESS["TMR_CAPTION_DURATION"] = "Tempo de Trabalho";
$MESS["TMR_FIXED"] = "entrou";
$MESS["TMR_FIXED_AT"] = "entrou em";
$MESS["TMR_FIXED_CAPTION"] = "Razão";
$MESS["TMR_FIXED_APPROVER"] = "Confirmado por";
$MESS["TMR_APPROVE"] = "Confirmar";
$MESS["TMR_FILTER_LABEL_ADD"] = "Campos adicionais";
$MESS["TMR_FILTER_ARRIVAL"] = "Ponto (entrada)";
$MESS["TMR_FILTER_DEPARTURE"] = "Ponto (saida)";
$MESS["TMR_FILTER_LABEL_DPT"] = "Estrutura da Empresa";
$MESS["TMR_FILTER_SHOW_ALL"] = "Mostrar funcionários";
$MESS["TMR_FILTER_SHOW_ALL_Y"] = "Mostrar todos os funcionários";
$MESS["TMR_FILTER_SHOW_ALL_N"] = "Não mostrar registros vazios";
$MESS["TMR_FILTER_ONLY_DIRECT"] = "Apenas subordinados diretos";
$MESS["TMR_HINT_MAX_START"] = "Ultimo registro de ponto (entrada)";
$MESS["TMR_HINT_MIN_FINISH"] = "Registro mais recente de ponto (saida)";
$MESS["TMR_HINT_MIN_DURATION"] = "Duração mínima";
$MESS["TMR_LEAKS_CAPTION"] = "Intervalos";
$MESS["TMR_STATS"] = "Estatística";
$MESS["TMR_ADDITONAL"] = "Mostrar os horários de início e fim";
$MESS["TMR_CLOSE"] = "Fechar";
$MESS["TMR_MONTH_1"] = "Janeiro";
$MESS["TMR_MONTH_2"] = "Fevereiro";
$MESS["TMR_MONTH_3"] = "Março";
$MESS["TMR_MONTH_4"] = "Abril";
$MESS["TMR_MONTH_5"] = "Maio";
$MESS["TMR_MONTH_6"] = "Junho";
$MESS["TMR_MONTH_7"] = "Julho";
$MESS["TMR_MONTH_8"] = "Agosto";
$MESS["TMR_MONTH_9"] = "Setembro";
$MESS["TMR_MONTH_10"] = "Outubro";
$MESS["TMR_MONTH_11"] = "Novembro";
$MESS["TMR_MONTH_12"] = "Dezembro";
$MESS["TMR_MONTH_R_1"] = "Janeiro";
$MESS["TMR_MONTH_R_2"] = "Fevereiro";
$MESS["TMR_MONTH_R_3"] = "Março";
$MESS["TMR_MONTH_R_4"] = "Abril";
$MESS["TMR_MONTH_R_5"] = "Maio";
$MESS["TMR_MONTH_R_6"] = "Junho";
$MESS["TMR_MONTH_R_7"] = "Julho";
$MESS["TMR_MONTH_R_8"] = "Agosto";
$MESS["TMR_MONTH_R_9"] = "Setembro";
$MESS["TMR_MONTH_R_10"] = "Outubro";
$MESS["TMR_MONTH_R_11"] = "Novembro";
$MESS["TMR_MONTH_R_12"] = "Dezembro";
$MESS["TMR_DAY_1"] = "Do";
$MESS["TMR_DAY_2"] = "Seg";
$MESS["TMR_DAY_3"] = "Ter";
$MESS["TMR_DAY_4"] = "Quar";
$MESS["TMR_DAY_5"] = "Qui";
$MESS["TMR_DAY_6"] = "Sex";
$MESS["TMR_DAY_7"] = "Sab";
$MESS["TMR_DAY_FULL_1"] = "Domingo";
$MESS["TMR_DAY_FULL_2"] = "Segunda-feira";
$MESS["TMR_DAY_FULL_3"] = "Terça-feira";
$MESS["TMR_DAY_FULL_4"] = "Quarta-feira";
$MESS["TMR_DAY_FULL_5"] = "Quinta-feira";
$MESS["TMR_DAY_FULL_6"] = "Sexta-feira";
$MESS["TMR_DAY_FULL_7"] = "Sábado";
$MESS["TMR_INDAY_FULL_1"] = "no Domingo";
$MESS["TMR_INDAY_FULL_2"] = "na Segunda-feira";
$MESS["TMR_INDAY_FULL_3"] = "na Terça-feira";
$MESS["TMR_INDAY_FULL_4"] = "na Quarta-feira";
$MESS["TMR_INDAY_FULL_5"] = "na Quinta-feira";
$MESS["TMR_INDAY_FULL_6"] = "na Sexta-feira";
$MESS["TMR_INDAY_FULL_7"] = "no Sábado";
$MESS["TMR_INDATE"] = "dia";
$MESS["TMR_DAY_WEEK"] = "Dia da semana";
$MESS["TMR_DATE_MONTH"] = "Dia do mês";
$MESS["TMR_HEAD"] = "supervisor";
$MESS["TMR_HINT_DISABLED"] = "A gestão do tempo foi desativada para este colaborador.";
$MESS["TMR_HINT_FREE"] = "Horário de trabalho flexível";
$MESS["TMR_LEGEND_TITLE"] = "Resumo do tempo trabalhado";
$MESS["TMR_DAY_NOT_FINISHED"] = "A duração do dia de trabalho não pode ser alterada até que seja temporizada.";
$MESS["TMR_NO_MARK"] = "nenhuma pontuação";
$MESS["TMR_GOOD_MARK"] = "positivo";
$MESS["TMR_BAD_MARK"] = "negativo";
$MESS["TMR_DAY"] = "Dia";
$MESS["TMR_NONE"] = "Relatório não é necessário";
$MESS["TMR_WEEK"] = "Semana";
$MESS["TMR_MONTH"] = "Mês";
$MESS["TMR_TIME"] = "Tempo";
$MESS["TMR_DATE"] = "Reportar data";
$MESS["TMR_PERIOD"] = "Relatar período";
$MESS["TMR_DESC"] = "Colaborador deve criar um relatório #PERIOD#.";
$MESS["TMR_EVERY_DAY"] = "todo dia";
$MESS["TMR_EVERY_WEEK"] = "toda semana";
$MESS["TMR_EVERY_MONTH"] = "todo mês";
$MESS["TMR_BEGIN_DATE"] = "A data de início";
$MESS["TMR_MARK"] = "Relatório de pontuação";
$MESS["TMR_REPORT_APPROVE"] = "Relatório confirmado";
$MESS["TMR_REPORT_APPROVER"] = "Confirmado por";
$MESS["TMR_ACCEPT_DATE"] = "Confirmado em";
$MESS["TMR_NOT_ACCEPT"] = "Relatório não confirmado";
$MESS["TMR_REPORT_COUNT"] = "Relatório<br>contagem";
$MESS["TMR_REPORT_COUNT_MARK"] = "Taxa de<br />relatórios<br />com a pontuação";
$MESS["TMR_REPORT"] = "Reportar";
$MESS["TMR_PARENT"] = "(herdar dept. configurações)";
$MESS["TMR_SUCCESS"] = "As configurações foram salvas.";
$MESS["TM_SETTINGS"] = "Configurações";
$MESS["TM_SETTINGS_REPORT"] = "Configurações do relatório";
$MESS["TMR_OVERALL_VIOL"] = "Fora do padrão<br />Dias, %";
$MESS["TMR_HINT_TITLE"] = "Parâmetros de Dia de Trabalho";
$MESS["TMR_LEGEND_HTML"] = "<p class=\"bx-head bx-tm-report-legend\">Chefe do departamento.</p>
<p class=\"bx-tm-report-inactive bx-tm-report-legend\">O registro precisar ser confirmado pelo supervisor.</p>
<p class=\"bx-tm-report-activated bx-tm-report-legend\">O registro foi confirmado pelo supervisor.</p>
<p class=\"bx-tm-report-info bx-tm-report-legend\">Os parâmetros de dia de trabalho podem ser especificados para uma empresa, um departamento ou um funcionário individual.</p>
<p><i>&quot;Dias fora do padrão, %&quot</i> é uma relação de dias fora do padrão de trabalho do total de dias de trabalho. <i>&quot;Dias de Trabalho&quot;</i>, <i>&quot; Horas de Trabalho&quot;</i> e <i>&quot;Dias fora do padrão, %&quot;</i> as colunas não incluem dias não confirmados.</p>";
?>