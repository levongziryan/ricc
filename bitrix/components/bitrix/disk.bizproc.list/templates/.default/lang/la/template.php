<?
$MESS["BPATT_NAME"] = "Nombre";
$MESS["BPATT_MODIFIED"] = "Modificado";
$MESS["BPATT_USER"] = "Modificado por";
$MESS["WD_EMPTY"] = "No hay plantillas de procesos de negocio. Crear estandar <a href=#HREF#>Procesos de Negocio</a>.";
$MESS["WD_EMPTY_NEW"] = "No hay nuevas plantillas de la versión de Procesos de Negocio. <a href=#HREF#>Create</a>.";
$MESS["BPATT_ALL"] = "Total";
$MESS["BPATT_AUTO_EXECUTE"] = "Ejecución automática";
$MESS["BPATT_HELP1_TEXT"] = "Un proceso de negocio impulsado por el Estado es un proceso de negocio continuo con la distribución de permisos de acceso para manejar documentos en varios estados.";
$MESS["BPATT_HELP2_TEXT"] = "Un proceso de negocio secuencial es un proceso de negocio simple que lleva a cabo una serie de acciones consecutivas en un documento.";
$MESS["PROMPT_OLD_TEMPLATE"] = "La actualización reciente del Drive ha hecho algunas de las plantillas de procesos de negocio obsoletas. Usted todavía puede usarlos. Las nuevas formas de edición de plantilla será un poco diferente, sin embargo. Destacan las plantillas existentes.";
?>