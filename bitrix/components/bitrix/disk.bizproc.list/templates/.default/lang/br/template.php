<?
$MESS["BPATT_AUTO_EXECUTE"] = "Autoexecução";
$MESS["BPATT_HELP1_TEXT"] = "Um processo comercial conduzido pelo status é um processo comercial contínuo com distribuição de permissão de acesso para lidar com documentos em diferentes status.";
$MESS["BPATT_HELP2_TEXT"] = "Um processo comercial sequencial é um processo comercial simples que executa uma série de ações consecutivas em um documento.";
$MESS["BPATT_NAME"] = "Nome";
$MESS["BPATT_MODIFIED"] = "Modificado";
$MESS["BPATT_USER"] = "Modificado por";
$MESS["WD_EMPTY"] = "Não há nenhum modelo de Processo Comercial. Criar padrão <a href= #HREF# >de Processos Comerciais</a>.";
$MESS["WD_EMPTY_NEW"] = "Não há nova versão de modelos de Processo Comercial. <A href= #HREF# >Criar</a>.";
$MESS["BPATT_ALL"] = "Total";
$MESS["PROMPT_OLD_TEMPLATE"] = "A recente atualização da Unidade tornou alguns dos modelos de processo comercial obsoletos. Você ainda pode usá-los. No entanto, os novos formulários de edição de modelo serão um pouco diferentes. Os modelos herdados estão destacados.";
?>