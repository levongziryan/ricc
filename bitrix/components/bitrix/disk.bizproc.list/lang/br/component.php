<?
$MESS["BPATT_NAV"] = "Modelos de Processo Comercial";
$MESS["BPATT_TITLE"] = "Modelos de Processo Comercial";
$MESS["BPATT_AE_CREATE"] = "Criar";
$MESS["BPATT_AE_DELETE"] = "Excluir";
$MESS["BPATT_AE_NONE"] = "Não";
$MESS["BPATT_NO_ENTITY"] = "Não foi especificada nenhuma entidade para a qual o processo comercial deve ser criado.";
$MESS["BPATT_NO_DOCUMENT_TYPE"] = "O tipo de documento é obrigatório.";
$MESS["BPATT_NO_MODULE_ID"] = "O módulo ID é obrigatório.";
$MESS["BPATT_AE_EDIT"] = "Atualização";
$MESS["BPATT_NO_PERMS"] = "Você não tem permissão para executar um processo comercial para este documento.";
$MESS["BPATT_DO_DELETE1_CONFIRM"] = "Você tem certeza que deseja excluir este modelo?";
$MESS["BPATT_DO_DELETE1"] = "Excluir";
$MESS["BPATT_DO_N_LOAD_CREATE_TITLE"] = "Não execute um processo comercial ao criar um documento";
$MESS["BPATT_DO_N_LOAD_EDIT_TITLE"] = "Não execute um processo comercial ao modificar um documento";
$MESS["BPATT_DO_N_LOAD_CREATE"] = "Não execute durante a criação";
$MESS["BPATT_DO_N_LOAD_EDIT"] = "Não execute durante a modificação";
$MESS["BPATT_DO_EDIT1"] = "Editar";
$MESS["BPATT_DO_LOAD_CREATE_TITLE"] = "Executar um processo comercial ao criar um documento";
$MESS["BPATT_DO_LOAD_EDIT_TITLE"] = "Executar um processo comerical ao modificar um documento";
$MESS["BPATT_DO_LOAD_CREATE"] = "Executar durante a criação";
$MESS["BPATT_DO_LOAD_EDIT"] = "Executar durante a modificação";
$MESS["BPATT_DO_EDIT_VARS"] = "Alterar os valores das variáveis iniciais";
$MESS["BPATT_DO_EDIT_VARS1"] = "Variáveis";
$MESS["BPATT_DO_OLD_TEMPLATE"] = "(versão antiga)";
?>