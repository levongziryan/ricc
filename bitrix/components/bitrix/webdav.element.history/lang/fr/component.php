<?
$MESS["WD_ELEMENT_HISTORY_ONLINE_SERVICE_EDIT"] = "Actuellement, le document est modifié dans #SERVICE#";
$MESS["WD_ELEMENT_HISTORY_FIRST_UPLOAD_M"] = "#USER# a chargé";
$MESS["WD_ELEMENT_HISTORY_FIRST_UPLOAD_F"] = "#USER# a chargé";
$MESS["WD_ELEMENT_HISTORY_AND_REVISION_COUNT_1"] = "et encore #COUNT# changements";
$MESS["WD_ELEMENT_HISTORY_AND_REVISION_COUNT_21"] = "et encore #COUNT# changements";
$MESS["WD_ELEMENT_HISTORY_AND_REVISION_COUNT_5_20"] = "et encore #COUNT# changements";
$MESS["WD_ELEMENT_HISTORY_AND_REVISION_COUNT_2_4"] = "et encore #COUNT# changements";
$MESS["WD_ELEMENT_HISTORY_AND_EDITOR_COUNT_1"] = "et encore #COUNT# hommes";
$MESS["WD_ELEMENT_HISTORY_AND_EDITOR_COUNT_21"] = "et encore #COUNT# hommes";
$MESS["WD_ELEMENT_HISTORY_AND_EDITOR_COUNT_5_20"] = "et encore #COUNT# hommes";
$MESS["WD_ELEMENT_HISTORY_AND_EDITOR_COUNT_2_4"] = "et encore #COUNT# hommes";
$MESS["WD_ELEMENT_HISTORY_LAST_EDIT_M"] = "Dernière édition par #USER#";
$MESS["WD_ELEMENT_HISTORY_LAST_EDIT_F"] = "Dernière édition par #USER#";
$MESS["WD_ELEMENT_HISTORY_LIST_TITLE"] = "Modifications précédentes";
$MESS["WD_ELEMENT_HISTORY_DOWNLOAD_ORIGIN"] = "Télécharger l'original";
?>