<?
$MESS["WD_ELEMENT_HISTORY_LIST_TITLE"] = "Histórico da Revisão";
$MESS["WD_ELEMENT_HISTORY_DOWNLOAD_ORIGIN"] = "Baixar original";
$MESS["WD_ELEMENT_HISTORY_ONLINE_SERVICE_EDIT"] = "Este documento está sendo editado em #SERVICE#";
$MESS["WD_ELEMENT_HISTORY_LAST_EDIT_F"] = "ltima edição por #USER#";
$MESS["WD_ELEMENT_HISTORY_LAST_EDIT_M"] = "ltima edição por #USER#";
$MESS["WD_ELEMENT_HISTORY_FIRST_UPLOAD_F"] = "Enviado por #USER#";
$MESS["WD_ELEMENT_HISTORY_FIRST_UPLOAD_M"] = "Enviado por #USER#";
$MESS["WD_ELEMENT_HISTORY_AND_REVISION_COUNT_1"] = "e #COUNT# mais mudanças";
$MESS["WD_ELEMENT_HISTORY_AND_REVISION_COUNT_21"] = "e #COUNT# mais mudanças";
$MESS["WD_ELEMENT_HISTORY_AND_REVISION_COUNT_2_4"] = "e #COUNT# mais mudanças";
$MESS["WD_ELEMENT_HISTORY_AND_REVISION_COUNT_5_20"] = "e #COUNT# mais mudanças";
$MESS["WD_ELEMENT_HISTORY_AND_EDITOR_COUNT_1"] = "e #COUNT# mais pessoas";
$MESS["WD_ELEMENT_HISTORY_AND_EDITOR_COUNT_21"] = "e #COUNT# mais pessoas";
$MESS["WD_ELEMENT_HISTORY_AND_EDITOR_COUNT_2_4"] = "e #COUNT# mais pessoas";
$MESS["WD_ELEMENT_HISTORY_AND_EDITOR_COUNT_5_20"] = "e #COUNT# mais pessoas";
?>