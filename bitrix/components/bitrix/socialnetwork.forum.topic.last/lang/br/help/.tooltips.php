<?
$MESS["CACHE_TIME_TIP"] = "Tempo de cache (seg.)";
$MESS["CACHE_TYPE_TIP"] = "Tipo de cache";
$MESS["PAGER_TITLE_TIP"] = "Nomes das categorias";
$MESS["DISPLAY_PANEL_TIP"] = "Painel de controle mostrará botões para este componente";
$MESS["DATE_TIME_FORMAT_TIP"] = "Formato de data e hora";
$MESS["SET_NAVIGATION_TIP"] = "Mostrar controles de navegação";
$MESS["FID_TIP"] = "Fórum ID";
$MESS["URL_TEMPLATES_INDEX_TIP"] = "Página de Fóruns";
$MESS["TOPICS_PER_PAGE_TIP"] = "Número de tópicos por página";
$MESS["SET_TITLE_TIP"] = "Definir Título da página";
$MESS["PAGER_SHOW_ALWAYS_TIP"] = "Mostrar sempre";
$MESS["SHOW_FORUM_ANOTHER_SITE_TIP"] = "Mostrar Fóruns de outros sites";
$MESS["SORT_BY_TIP"] = "Campo de classificação";
$MESS["SORT_ORDER_TIP"] = "Ordem";
$MESS["PAGER_TEMPLATE_TIP"] = "Título do modelo";
$MESS["URL_TEMPLATES_READ_TIP"] = "Exibir Tópico";
$MESS["URL_TEMPLATES_LIST_TIP"] = "Página de Tópicos";
$MESS["PAGER_DESC_NUMBERING_TIP"] = "Usar barra de navegação inversa";
$MESS["URL_TEMPLATES_PROFILE_VIEW_TIP"] = "Página do Perfil de usuário";
?>