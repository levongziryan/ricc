<?
$MESS ['CACHE_TYPE_TIP'] = "<i>Automatisch</i>: Der Cache ist gültig gemäß Definition in den Cache-Einstellungen;<br /><i>Cache</i>: immer cachen für den Zeitraum, der im nächsten Feld definiert wird;<br /><i>Nicht cachen</i>: es wird kein Caching ausgeführt.";
$MESS ['LINK_IBLOCK_ID_TIP'] = "Hier wird der Informationsblock angegeben, Elemente dessen mit dem angezeigten Informationsblock verbunden sind.";
$MESS ['SET_TITLE_TIP'] = "Wenn diese Option aktiv ist, wird als Seitenüberschrift die eingestellte Elementbezeichnung angezeigt.";
$MESS ['PRICE_VAT_INCLUDE_TIP'] = "Wenn diese Option aktiv ist, werden die Preise inklusive der MwSt. angezeigt.";
$MESS ['USE_PRICE_COUNT_TIP'] = "Wenn diese Option aktiv ist, werden die Preise aller Produkttypen angezeigt.";
$MESS ['DISPLAY_PANEL_TIP'] = "Wenn diese Option aktiv ist, werden Buttons im Modus \"Inhalte\" auf dem administrativen Pannel und in der Zusammenstellung des Bearbeitungsbereichs der Komponente angezeigt.";
$MESS ['LINK_IBLOCK_TYPE_TIP'] = "Wenn die Elemente des Informationsblocks mit den Elementen des ausgegebenen Informationsblocks verbunden sind, wird hier der Typ des verbundenen Informationsblocks angegeben.";
$MESS ['ADD_SECTIONS_CHAIN_TIP'] = "Wenn diese Option aktiv ist und im Informationsblock Bereiche erstellt sind, werden bei einem Bereichsübergang ihre Namen zur Navigationskette hinzugefügt.";
$MESS ['LINK_PROPERTY_SID_TIP'] = "Eigenschaft, nach der die Verbindungselemente aus der Liste gewählt oder als Code im Feld daneben eingegeben werden.";
$MESS ['PROPERTY_CODE_TIP'] = "Zwischen den Informationsblock-Eigenschaften kann man die wählen, die bei der Anzeige der Elemente angezeigt werden. Beim Auswählen des Punktes (nicht gewählt)-> und ohne die Angabe der Feld ID's in den unteren Zeilen, werden die Eigenschaften nicht angezeigt.";
$MESS ['IBLOCK_TYPE_TIP'] = "Wählen aus der angezeigten Liste einen Informationsblocktyp. Nachdem Sie <b><i>ok</i></b> gedrückt haben, werden Informationsblöcke vom ausgewählten Typ geladen.";
$MESS ['IBLOCK_ID_TIP'] = "Wählen Sie ein Informationsblock aus. Wenn der Punkt (andere)-> gewählt ist, müssen Sie im Feld daneben die ID des Informationsblocks eingeben.";
$MESS ['PRICE_VAT_SHOW_VALUE_TIP'] = "Diese Option zeigt die MwSt. an.";
$MESS ['BASKET_URL_TIP'] = "Hier wird der Pfad zur Warenkorbseite des Käufers angegeben.";
$MESS ['DETAIL_URL_TIP'] = "Hier wird der Pfad zur Seite mit der detailierten Beschreibung des Informationsblock-Elements angegeben.";
$MESS ['SECTION_URL_TIP'] = "Hier wird der Pfad zur Seite mit der Bereichsbeschreibung des Informationsblocks angegeben.";
$MESS ['CACHE_TIME_TIP'] = "Geben Sie die Cache-Laufzeit in Sekunden an.";
$MESS ['PRICE_CODE_TIP'] = "Es wird eingestellt, welcher der Preistypen im Katalog angezeigt wird. Wenn keiner der Typen gewählt ist, werden der Preis und die Buttons <i>Kaufen</i> und <i>Zum Warenkorb</i> nicht angezeigt.";
$MESS ['SHOW_PRICE_COUNT_TIP'] = "Hier können Sie die Menge festlegen, für die der Preis ausgegeben wird, z.B. 1 oder 10, abhängig von der Produktspezifikation.";
$MESS ['ELEMENT_ID_TIP'] = "Das Feld enthält den Code in dem die Element ID übergeben wird. Als Standard enthält das Feld ={\$_REQUEST[\"ID\"]}";
$MESS ['SECTION_ID_TIP'] = "Das Feld enthält den Code in dem die Bereichs ID übergeben wird. Als Standard enthält das Feld ={\$_REQUEST[\"SECTION_ID\"]}.";
$MESS ['SECTION_ID_VARIABLE_TIP'] = "Variable, in der die ID des Informationsblock-Bereichs übergeben wird.";
$MESS ['PRODUCT_ID_VARIABLE_TIP'] = "Variable, in der die Produkt ID übergeben wird.";
$MESS ['ACTION_VARIABLE_TIP'] = "Geben Sie die Variable an, in der die Aktionen: ADD_TO_COMPARE_LIST, ADD2BASKET usw. übergeben werden. Als Standard enthält das Feld <i>action</i>.";
$MESS ['LINK_ELEMENTS_URL_TIP'] = "Hier wird der Pfad zur Seite angegeben, wo die Liste der verlinkten Elemente angezeigt wird.";
$MESS ['META_KEYWORDS_TIP'] = "In der Dropdown-Liste sind alle Eigenschaften aufgeführt, die für den Informationsblock bestimmt sind. Von denen werden die ausgewählt, die Schlüsselwörter enthalten.";
$MESS ['META_DESCRIPTION_TIP'] = "In der Dropdown-Liste sind alle Eigenschaften aufgeführt, die für den Informationsblock bestimmt sind. Von denen werden die ausgewählt, die Beschreibungen enthalten.";
?>