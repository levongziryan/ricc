<?
$MESS["BLOG_BLOG_BLOG_NO_AVAIBLE_MES"] = "Pas de messages disponibles";
$MESS["BLOG_MES_DELETE_POST_CONFIRM"] = "Voulez-vous vraiment supprimer ce message ?";
$MESS["BLOG_BLOG_BLOG_COMMENTS"] = "Commentaires:";
$MESS["BLOG_MES_DELETE"] = "Supprimer";
$MESS["BLOG_MES_EDIT"] = "Éditer";
$MESS["BLOG_BLOG_BLOG_MORE"] = "Lire plus...";
$MESS["BLOG_BLOG_BLOG_CATEGORY"] = "Mots-clés:";
$MESS["BLOG_BLOG_BLOG_VIEWS"] = "Vues:";
?>