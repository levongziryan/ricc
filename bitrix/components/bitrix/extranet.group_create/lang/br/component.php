<?
$MESS["SONET_PHOTO_LOG_1"] = "#AUTHOR_NAME# adicionou o arquivo #TITLE#";
$MESS["SONET_FILES_LOG"] = "#AUTHOR_NAME# adicionou o arquivo #TITLE#";
$MESS["SONET_ACCESS_DENIED"] = "Acesso negado.";
$MESS["SONET_CREATE_WEBDAV"] = "Criar Biblioteca";
$MESS["IBLOCK_DEFAULT_UF"] = "Galeria padrão";
$MESS["SONET_FILES"] = "Arquivos";
$MESS["FL_FORUM_CHAIN"] = "Fórum";
$MESS["SONET_GROUP"] = "Grupo";
$MESS["SONET_GROUP_NOT_EXISTS"] = "Grupo não existe.";
$MESS["SONET_GROUP_PREFIX"] = "Grupo:";
$MESS["SONET_LOADING"] = "Carregando...";
$MESS["SONET_FILES_LOG_TEXT"] = "Novo arquivo #TITLE# em #URL#.";
$MESS["SONET_PHOTO_LOG_MAIL_TEXT"] = "Novas fotos: #LINKS# e outros.";
$MESS["SONET_PHOTO_LOG_TEXT"] = "Novas fotos: <div class='notificationlog'>#LINKS#</div> <a href=#HREF#>Abrir álbum</a>.";
$MESS["SONET_WEBDAV_NOT_EXISTS"] = "Nenhum documento.";
$MESS["SONET_GALLERY_NOT_FOUND"] = "Não há fotos.";
$MESS["SONET_PHOTO"] = "Foto";
$MESS["SONET_PHOTO_LOG_2"] = "Fotos (#COUNT#)";
$MESS["SONET_FILES_IS_NOT_ACTIVE"] = "O  recurso Arquivos não está habilitado.";
$MESS["SONET_PHOTO_IS_NOT_ACTIVE"] = "O recurso Fotos não está habilitado.";
$MESS["SONET_WD_MODULE_IS_NOT_INSTALLED"] = "O módulo de biblioteca de documentos não está instalado.";
$MESS["SONET_IBLOCK_ID_EMPTY"] = "O bloco de informação não está especificado.";
$MESS["SONET_IB_MODULE_IS_NOT_INSTALLED"] = "O módulo Blocos de Informação não está instalado.";
$MESS["SONET_P_MODULE_IS_NOT_INSTALLED"] = "O módulo Galeria de Fotos não está instalado.";
$MESS["SONET_MODULE_NOT_INSTALL"] = "O módulo de rede social não está instalado.";
$MESS["SONET_GALLERIES_IS_NOT_ACTIVE"] = "Suas galerias de fotos estão atualmente inativas. Por favor, contate o administrador.";
$MESS["SONET_GALLERY_IS_NOT_ACTIVE"] = "Sua galeria de fotos está atualmente inativa. Por favor, contate o administrador.";
?>