<?
$MESS["PATH_TO_USER_TIP"] = "O caminho para uma página de perfil do usuário. Exemplo: sonet_user.php?page=user&user_id=#user_id#.";
$MESS["PATH_TO_USER_EDIT_TIP"] = "O caminho para um editor de páginas do perfil de usuário. Exemplo: sonet_user_edit.php?page=user&user_id=#user_id#&mode=edit.";
$MESS["PAGE_VAR_TIP"] = "Especifique o nome da variável a que a página da rede social será passado aqui.";
$MESS["USER_VAR_TIP"] = "Especifique o nome de uma variável para que o ID do usuário de rede social será passado aqui.";
$MESS["ID_TIP"] = "Especifica o código que corresponde a um ID de usuário.";
$MESS["SET_TITLE_TIP"] = "Ao marcar esta opção irá definir o título da página para <i> nome de usuário </i> <b> Perfil de Usuário </b>.";
$MESS["USER_PROPERTY_TIP"] = "Selecione propriedades adicionais que serão mostradas no perfil de usuário aqui.";
?>