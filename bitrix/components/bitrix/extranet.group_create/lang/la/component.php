<?
$MESS["SONET_MODULE_NOT_INSTALL"] = "El módulo Social network no está instalado.";
$MESS["SONET_LOADING"] = "Cargando...";
$MESS["SONET_GROUP"] = "Grupo";
$MESS["SONET_FILES"] = "Archivos";
$MESS["SONET_PHOTO"] = "Foto";
$MESS["SONET_FILES_IS_NOT_ACTIVE"] = "Las características de los \"Archivos\" no esta habilitada ";
$MESS["SONET_PHOTO_IS_NOT_ACTIVE"] = "Las características de las \"Fotos\" no esta habilitada ";
$MESS["SONET_WD_MODULE_IS_NOT_INSTALLED"] = "El modulo de la librería de documentos no esta instalado";
$MESS["SONET_IB_MODULE_IS_NOT_INSTALLED"] = "El modulo de bloques de información no esta instalado";
$MESS["SONET_P_MODULE_IS_NOT_INSTALLED"] = "El modulo de galería de foto no esta instalado";
$MESS["SONET_IBLOCK_ID_EMPTY"] = "El bloque de información no esta especificado";
$MESS["SONET_ACCESS_DENIED"] = "Acceso denegado";
$MESS["SONET_GROUP_NOT_EXISTS"] = "El grupo no existe";
$MESS["SONET_GROUP_PREFIX"] = "Grupo:";
$MESS["SONET_CREATE_WEBDAV"] = "Crear Librería";
$MESS["SONET_WEBDAV_NOT_EXISTS"] = "Sin documentos";
$MESS["IBLOCK_DEFAULT_UF"] = "Galería por defecto";
$MESS["SONET_GALLERY_IS_NOT_ACTIVE"] = "Su galería de fotos esta actualmente inactiva. Por favor contactar al administrador.";
$MESS["SONET_GALLERIES_IS_NOT_ACTIVE"] = "Su galería de fotos esta actualmente inactiva. Por favor contactar al administrador.";
$MESS["SONET_GALLERY_NOT_FOUND"] = "Sin fotos.";
$MESS["SONET_FILES_LOG"] = "#AUTHOR_NAME# archivo agregado #TITLE#.";
$MESS["SONET_FILES_LOG_TEXT"] = "Nuevo archivo #TITLE# en #URL#.";
$MESS["SONET_PHOTO_LOG_1"] = "#AUTHOR_NAME# archivo agregado #TITLE#.";
$MESS["SONET_PHOTO_LOG_2"] = "Fotos (#COUNT#)";
$MESS["SONET_PHOTO_LOG_TEXT"] = "Nuevas fotos: <div class='notificationlog'>#LINKS#</div> <a href=\"#HREF#\">Abrir Álbum</a>.";
$MESS["SONET_PHOTO_LOG_MAIL_TEXT"] = "Nuevas fotos: #LINKS# y otro.";
$MESS["FL_FORUM_CHAIN"] = "Foro";
?>