<?
$MESS["SONET_GROUP_VAR"] = "Nom de la variable pour le groupe";
$MESS["SONET_USER_VAR"] = "Nom de la variable pour l'identificateur de l'utilisateur";
$MESS["SONET_PAGE_VAR"] = "Nom de la variable pour la page";
$MESS["SONET_GROUP_PAGE_PATH"] = "Modèle de chemin d'accès à la page d'un groupe";
$MESS["SONET_SEF_PATH_INVITE"] = "Page d'invitation des utilisateurs";
$MESS["SONET_SEF_PATH_INDEX"] = "Création d'un groupe";
?>