<?
$MESS["SONET_PHOTO_LOG_1"] = "#AUTHOR_NAME# a ajouté des images #TITLE#";
$MESS["SONET_FILES_LOG"] = "#AUTHOR_NAME# a ajouté un fichier #TITLE#";
$MESS["SONET_GALLERY_IS_NOT_ACTIVE"] = "Votre galerie photos est bloquée. Veuillez contacter l'administrateur.";
$MESS["SONET_GALLERIES_IS_NOT_ACTIVE"] = "Vos galeries photo sont bloquées. Contactez l'administrateur.";
$MESS["IBLOCK_DEFAULT_UF"] = "Galerie par défaut";
$MESS["SONET_GROUP"] = "Groupe";
$MESS["SONET_GROUP_NOT_EXISTS"] = "Groupe inexistant.";
$MESS["SONET_GROUP_PREFIX"] = "Groupe:";
$MESS["SONET_WEBDAV_NOT_EXISTS"] = "Pas de documents.";
$MESS["SONET_ACCESS_DENIED"] = "Accès interdit.";
$MESS["SONET_LOADING"] = "En cours de chargement...";
$MESS["SONET_WD_MODULE_IS_NOT_INSTALLED"] = "Le document intitulé «# FILENAME # 'est déjà en cours d'utilisation.";
$MESS["SONET_IB_MODULE_IS_NOT_INSTALLED"] = "Le module des blocs d'informations n'a pas été installé.";
$MESS["SONET_MODULE_NOT_INSTALL"] = "Le module du réseau social n'a pas été installé.";
$MESS["SONET_P_MODULE_IS_NOT_INSTALLED"] = "Le module de la galerie photo n'a pas été installé.";
$MESS["SONET_IBLOCK_ID_EMPTY"] = "Bloc d'information non spécifié.";
$MESS["SONET_PHOTO_LOG_MAIL_TEXT"] = "Des nouvelles photographies ont été chargées dans l'album '#TITLE#'";
$MESS["SONET_PHOTO_LOG_TEXT"] = "Nouvelles photos: <div class='notificationlog'>#LINKS#</div> <a href='#HREF#'>Passer dans l'album</a>.";
$MESS["SONET_FILES_LOG_TEXT"] = "Le nouveau fichier #TITLE# à l'adresse #URL#.";
$MESS["SONET_CREATE_WEBDAV"] = "Créer une Bibliothèque";
$MESS["SONET_FILES"] = "Fichiers";
$MESS["FL_FORUM_CHAIN"] = "Forum";
$MESS["SONET_PHOTO"] = "Photo";
$MESS["SONET_PHOTO_LOG_2"] = "Photos (#COUNT#)";
$MESS["SONET_GALLERY_NOT_FOUND"] = "Il n'y pas de photos.";
$MESS["SONET_FILES_IS_NOT_ACTIVE"] = "La fonction 'Mon disque' est désactivée";
$MESS["SONET_PHOTO_IS_NOT_ACTIVE"] = "La fonction 'Photo' est désactivée.";
?>