<?
$MESS["SONET_C8_SUBTITLE"] = "Parámetros del grupo";
$MESS["SONET_C8_NAME"] = "Nombre del grupo";
$MESS["SONET_C8_DESCR"] = "Descripción del grupo";
$MESS["SONET_C8_IMAGE"] = "Imagen del grupo";
$MESS["SONET_C8_IMAGE_DEL"] = "Eliminar imagen";
$MESS["SONET_C8_SUBJECT"] = "Tema del grupo";
$MESS["SONET_C8_TO_SELECT"] = "-No seleccionado-";
$MESS["SONET_C8_PARAMS"] = "Parámetros del grupo";
$MESS["SONET_C8_PARAMS_VIS"] = "Visible para todos";
$MESS["SONET_C8_PARAMS_OPEN"] = "Grupo publico, cualquiera puede unirse";
$MESS["SONET_C8_KEYWORDS"] = "palabras claves";
$MESS["SONET_C8_INVITE"] = "Quien puede invitar al grupo";
$MESS["SONET_C8_DO_EDIT"] = "Parámetros de las actualizaciones";
$MESS["SONET_C8_DO_CREATE"] = "Crear grupo";
$MESS["SONET_C8_SUCCESS_EDIT"] = "Los parámetros del grupo han sido cargados satisfactoriamente";
$MESS["SONET_C8_SUCCESS_CREATE"] = "El grupo ha sido creado satisfactoriamente";
$MESS["SONET_C8_T_CANCEL"] = "Cancelar";
$MESS["SONET_C8_PARAMS_CLOSED"] = "Crear un Archivo";
$MESS["SONET_C8_SPAM_PERMS"] = "Puede enviar mensajes a miembros del grupo";
?>