<?
$MESS["SONET_C11_SUBTITLE"] = "Un mensaje sera enviado, invitando a los usuarios del grupo. Los usuarios serán enlistados aquí después de que hayan aceptado la invitación.";
$MESS["SONET_C11_USER"] = "Usuarios";
$MESS["SONET_C11_GROUP"] = "Grupo";
$MESS["SONET_C11_MESSAGE"] = "Su mensaje";
$MESS["SONET_C11_DO_ACT"] = "Enviar Invitación";
$MESS["SONET_C11_DO_SKIP"] = "Omitir";
$MESS["SONET_C11_SUCCESS"] = "Su invitación ha sido enviada. Los usuarios serán enlistados aquí después que hayan confirmado sus membresias";
$MESS["SONET_C33_T_FRIENDS"] = "Amigos";
$MESS["SONET_C33_T_NO_FRIENDS"] = "Ningún Amigo";
$MESS["SONET_C33_T_ADD_FRIEND1"] = "Encontrar Usuarios";
$MESS["SONET_C33_T_UNOTSET"] = "Los usuarios no están especificados";
$MESS["SONET_C33_T_SUCCESS_LIST"] = "La invitación ha sido enviada a los siguientes usuarios:";
$MESS["SONET_C33_T_ERROR_LIST"] = "El envio de la invitación ha fallado para los siguientes usuarios:";
$MESS["SONET_C11_EMAIL"] = "Usted puede, también, enviar la invitación por correo electrónico. Usar la siguiente dirección de formato:<br>xxx@site.com<br>\"name lastname\" &lt;xxx@site.com&gt;<br> separado por una nueva linea o coma";
$MESS["SONET_C11_USER_INTRANET"] = "Empleados de la empresa:";
$MESS["SONET_C11_USER_EXTRANET"] = "Usuarios externos";
$MESS["SONET_C11_MESSAGE_DEFAULT"] = "Permitirme que invite a los grupos de trabajo '#NAME#'";
$MESS["SONET_C11_MESSAGE_GROUP_LINK"] = "Abrir grupo de trabajo";
$MESS["SONET_C11_USER_INTRANET_STRUCTURE"] = "Agregar de la estructura de la empresa";
?>