<?
$MESS["SONET_C11_SUBTITLE"] = "Uma mensagem será enviada convidando usuários para o grupo. Os usuários serão listados aqui depois de terem aceito o convite.";
$MESS["SONET_C11_USER_INTRANET_STRUCTURE"] = "Adicionar da Estrutura da Empresa";
$MESS["SONET_C11_USER_INTRANET"] = "Empregados da Empresa:";
$MESS["SONET_C11_USER_EXTRANET"] = "Usuários Externos:";
$MESS["SONET_C33_T_ADD_FRIEND1"] = "Encontrar Usuários";
$MESS["SONET_C33_T_FRIENDS"] = "Amigos";
$MESS["SONET_C11_GROUP"] = "Grupo";
$MESS["SONET_C11_MESSAGE_DEFAULT"] = "Deixe-me convidá-lo para o grupo de trabalho '#NAME#'";
$MESS["SONET_C33_T_NO_FRIENDS"] = "Nenhum amigo.";
$MESS["SONET_C11_MESSAGE_GROUP_LINK"] = "Abrir Grupo de Trabalho";
$MESS["SONET_C11_DO_ACT"] = "Enviar convite";
$MESS["SONET_C33_T_ERROR_LIST"] = "Envio do convite falhou para os seguintes usuários:";
$MESS["SONET_C11_DO_SKIP"] = "Pular";
$MESS["SONET_C33_T_SUCCESS_LIST"] = "O convite foi enviado para os seguintes usuários:";
$MESS["SONET_C11_USER"] = "Usuários";
$MESS["SONET_C33_T_UNOTSET"] = "Os usuários não estão especificados.";
$MESS["SONET_C11_EMAIL"] = "xxx@site.com&gt";
$MESS["SONET_C11_SUCCESS"] = "Seu convite foi enviado. Os usuários serão listados aqui depois de terem confirmado adesão.";
$MESS["SONET_C11_MESSAGE"] = "Sua mensagem";
?>