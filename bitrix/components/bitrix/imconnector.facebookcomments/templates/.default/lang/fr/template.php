<?
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_DEL_REFERENCE"] = "Dissocier";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_STEP_1_OF_3_TITLE"] = "Authentification";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_STEP_2_OF_3_TITLE"] = "Choisir la page";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_STEP_3_OF_3_TITLE"] = "Terminer";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_STEP_N_OF_3"] = "étape #STEP# de 3";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_PAGE"] = "Page";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_INDEX_DESCRIPTION"] = "Connectez la page Facebook de votre entreprise au Canal ouvert pour gérer les commentaires laissés sur les publications, photos et vidéos depuis votre Bitrix24. Pour effectuer la connexion, vous devez posséder une page Facebook qui existe déjà ou en créer une. Vous devez être un administrateur de cette page.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CONNECT_PAGE"] = "Connecter la page";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_LOG_IN_UNDER_AN_ADMINISTRATOR_ACCOUNT_PAGE"] = "Connectez-vous en utilisant le compte administrateur de la page Facebook pour gérer les commentaires directement depuis votre Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_EXPIRED_ACCOUNT_TOKEN"] = "Connectez-vous avec votre compte Facebook pour offrir une réparation.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_EXPIRED_ACCOUNT_TOKEN_WARNING"] = "Notez que la page sera déconnectée de votre Bitrix24 si vous vous connectez en utilisant un compte qui n'est pas lié à cette page.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_THERE_IS_NO_PAGE_WHERE_THE_ADMINISTRATOR"] = "Vous n'avez aucune page Facebook pour laquelle vous êtes un administrateur. <br>Créez-en un(e) dès maintenant.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_TO_CREATE_A_PAGE"] = "Créer";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CONNECTED_PAGE"] = "Page connectée";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CHANGE_PAGE"] = "Modifier";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_MY_OTHER_PAGES"] = "Mes autres pages Facebook";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_SELECT_THE_PAGE"] = "Sélectionnez une page Facebook publique pour la connecter au Canal ouvert Bitrix24.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_OTHER_PAGES"] = "Autres pages";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_REPEATING_ERROR"] = "Si le problème persiste, envisagez de débrancher le canal et de le configurer une nouvelle fois.";
$MESS["IMCONNECTOR_COMPONENT_FACEBOOK_COMMENTS_CONNECTOR_ERROR_STATUS"] = "Une erreur est survenue. Veuillez vérifier vos paramètres.";
?>