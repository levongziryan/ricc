<?
$MESS["REPORT1C_REPORTLIST"] = "Lista de relatórios";
$MESS["REPORT1C_SHORTREPORT"] = "Relatório sucinto";
$MESS["REPORT1C_MENU"] = "Menu";
$MESS["REPORT1C_URL"] = "URL";
$MESS["REPORT1C_PATH"] = "Caminho no servidor";
$MESS["REPORT1C_PORT"] = "Porta";
$MESS["REPORT1C_INITR"] = "Página Padrão";
$MESS["REPORT1C_LOGIN"] = "Login";
$MESS["REPORT1C_PASSWORD"] = "Senha";
$MESS["REPORT1C_UNIQUEID"] = "ID única. Deve ser diferente se mais que um componente é colocado na página.";
$MESS["REPORT1C_CONNTIMEOUT"] = "Tempo limite de conexão de rede";
$MESS["REPORT1C_CACHE_ERR"] = "Também fazer cache de resultados de erro";
?>