<?
$MESS["INTR_EMP_WINDOW_TITLE"] = "Seleccionar Empleado";
$MESS["INTR_EMP_WINDOW_CLOSE"] = "Cerrar";
$MESS["INTR_EMP_WAIT"] = "Cargando...";
$MESS["INTR_EMP_SUBMIT"] = "Seleccionar";
$MESS["INTR_EMP_SUBMIT_TITLE"] = "Elegir el empleado seleccionado";
$MESS["INTR_EMP_CANCEL"] = "Cancelar";
$MESS["INTR_EMP_CANCEL_TITLE"] = "Cancelar selección del empleado";
$MESS["INTR_EMP_HEAD"] = "Jefe del departamento";
$MESS["INTR_EMP_LAST"] = "Temas recientes";
$MESS["INTR_EMP_NOTHING_FOUND"] = "Búsqueda no retornó resultados";
$MESS["INTR_EMP_SEARCH"] = "búsqueda del empleado";
$MESS["INTR_EMP_EXTRANET"] = "Extranet";
?>