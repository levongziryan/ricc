<?
$MESS["INTR_EMP_WINDOW_TITLE"] = "Choix d'un employé";
$MESS["INTR_EMP_SUBMIT"] = "Choisir";
$MESS["INTR_EMP_SUBMIT_TITLE"] = "Choisir un employé sélectionné";
$MESS["INTR_EMP_WINDOW_CLOSE"] = "Fermer";
$MESS["INTR_EMP_HEAD"] = "chef";
$MESS["INTR_EMP_NOTHING_FOUND"] = "rien n'a été trouvé";
$MESS["INTR_EMP_CANCEL"] = "Annuler";
$MESS["INTR_EMP_CANCEL_TITLE"] = "Annuler le choix de l'employé";
$MESS["INTR_EMP_WAIT"] = "En cours de chargement...";
$MESS["INTR_EMP_SEARCH"] = "recherche des employés";
$MESS["INTR_EMP_LAST"] = "Les derniers choisis";
$MESS["INTR_EMP_EXTRANET"] = "Extranet";
?>