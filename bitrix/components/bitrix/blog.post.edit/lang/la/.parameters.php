<?
$MESS["BPE_BLOG_URL"] = "URL del blog";
$MESS["BPE_ID"] = "ID del mensaje";
$MESS["BPE_PATH_TO_BLOG"] = "Plantilla de la ruta a la página de Blog ";
$MESS["BPE_PATH_TO_POST"] = "Plantilla de la trayectoria a la página del mensaje del blog ";
$MESS["BPE_PATH_TO_USER"] = "Plantilla de la ruta a la página del usuario del blog ";
$MESS["BPE_PATH_TO_POST_EDIT"] = "Plantilla de la ruta a la página edición de mensaje del blog ";
$MESS["BH_PATH_TO_DRAFT"] = "Plantilla de la ruta a la carpeta del borrador de mensajes ";
$MESS["BPE_BLOG_VAR"] = "Variable del identificador de Blog";
$MESS["BPE_POST_VAR"] = "Variable del identificador de mensaje de Blog";
$MESS["BPE_USER_VAR"] = "Variable del identificador del usuario de Blog ";
$MESS["BPE_PAGE_VAR"] = "Variable de la página";
$MESS["POST_PROPERTY"] = "Visualizar propiedades adicionales del envío";
$MESS["BB_PATH_TO_SMILE"] = "Ruta a carpeta con emoticons, relativo a la raíz del sitio";
$MESS["B_VARIABLE_ALIASES"] = "Alias varaible";
$MESS["BC_DATE_TIME_FORMAT"] = "Formato de fecha y hora";
$MESS["BPC_SMILES_COLS"] = "Número de columnas de los smileys";
$MESS["BPC_SMILES_COUNT"] = "Número visible de los smileys";
$MESS["BPE_ALLOW_POST_MOVE"] = "Habilitar movimiento de envíos al lado de los blogs";
$MESS["BPE_PATH_TO_BLOG_POST"] = "Página de envío del blog";
$MESS["BPE_PATH_TO_BLOG_POST_EDIT"] = "Página del editor de envío del blog";
$MESS["BPE_PATH_TO_BLOG_DRAFT"] = "Página de borradores del blog";
$MESS["BPE_PATH_TO_BLOG_BLOG"] = "Ruta al blog en los blogs";
$MESS["BPE_PATH_TO_USER_POST"] = "Página de Publicación del Blog de Usuario de Red Social";
$MESS["BPE_PATH_TO_USER_POST_EDIT"] = "Página de Editor de Publicación del Blog de usuario de Red Social";
$MESS["BPE_PATH_TO_USER_DRAFT"] = "Página de Borradores de Blog de Usuario de Red Social";
$MESS["BPE_PATH_TO_USER_BLOG"] = "Página de blog de usuario de red social";
$MESS["BPE_PATH_TO_GROUP_POST"] = "Página de publicación del blog del grupo de redes sociales";
$MESS["BPE_PATH_TO_GROUP_POST_EDIT"] = "Página de editor de blog de grupo de red social";
$MESS["BPE_PATH_TO_GROUP_DRAFT"] = "Página de borradores del blog del grupo de redes sociales";
$MESS["BPE_PATH_TO_GROUP_BLOG"] = "Página del blog del grupo de redes sociales";
$MESS["BPC_IMAGE_MAX_WIDTH"] = "Ancho Máx. de la Imagen";
$MESS["BPC_IMAGE_MAX_HEIGHT"] = "Alto Máx. de la Imagen";
$MESS["BPC_EDITOR_RESIZABLE"] = "Rediseñar Editor Visual";
$MESS["BPC_EDITOR_DEFAULT_HEIGHT"] = "Alto predeterminado del Editor Visual (píx)";
$MESS["BPC_EDITOR_CODE_DEFAULT"] = "Predeterminado para el Modo de editor de Texto";
$MESS["BPC_ALLOW_POST_CODE"] = "Usar Código Simbólico de Mensajes Como el ID";
$MESS["BPE_USE_GOOGLE_CODE"] = "Obtener el Código Simbólico Válido Usando el Traductor del Google";
$MESS["BC_SEO_USE"] = "Utilice campos separados para la descpción de la cuenta SEO";
?>