<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["M_CRM_DEAL_LIST_PRESET_NEW"] = "Novos negócios";
$MESS["M_CRM_DEAL_LIST_PRESET_MY"] = "Meus negócios";
$MESS["M_CRM_DEAL_LIST_PRESET_MY_NOT_COMPLETED"] = "Meus negócios incompletos";
$MESS["M_CRM_DEAL_LIST_PRESET_NOT_COMPLETED"] = "Incompleto";
$MESS["M_CRM_DEAL_LIST_PRESET_COMPLETED"] = "Concluído";
$MESS["M_CRM_DEAL_LIST_PRESET_WON"] = "Ganhou";
$MESS["M_CRM_DEAL_LIST_PRESET_FAILED"] = "Fracassado";
$MESS["M_CRM_DEAL_LIST_FILTER_CUSTOM"] = "Resultados da Pesquisa";
$MESS["M_CRM_DEAL_LIST_FILTER_NONE"] = "Todas as ofertas";
$MESS["M_CRM_LEAD_LIST_PRESET_USER"] = "Filtro personalizado";
$MESS["M_CRM_DEAL_LIST_EDIT"] = "Editar";
$MESS["M_CRM_DEAL_LIST_DELETE"] = "Excluir";
$MESS["M_CRM_DEAL_LIST_CHANGE_STAGE"] = "Fase de alteração";
$MESS["M_CRM_DEAL_LIST_BILL"] = "Criar fatura";
$MESS["M_CRM_DEAL_LIST_MORE"] = "Mais...";
$MESS["M_CRM_DEAL_LIST_CREATE_DEAL"] = "Negociação";
$MESS["M_CRM_DEAL_LIST_ADD_DEAL"] = "Adicionar atividade";
$MESS["M_CRM_DEAL_LIST_CALL"] = "Fazer uma ligação";
$MESS["M_CRM_DEAL_LIST_MEETING"] = "Marcar uma reunião";
$MESS["M_CRM_DEAL_LIST_MAIL"] = "Enviar mensagem";
$MESS["M_CRM_DEAL_LIST_CREATE_BASE"] = "Criar usando fonte";
?>