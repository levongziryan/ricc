<?
$MESS["SBP_DETAIL_URL"] = "URL da página de conteúdo do elemento";
$MESS["SBP_SHOW"] = "Exibir mais vendidos por";
$MESS["SBP_DAYS"] = "dias";
$MESS["SBP_ORDER_FILTER_NAME"] = "Título do filtro de parâmetros do pedido";
$MESS["SBP_FILTER_NAME"] = "Título do filtro de propriedades do produto";
$MESS["SBP_ITEM_COUNT"] = "Exibir elementos";
$MESS["SBP_QUANTITY"] = "Produtos vendidos";
$MESS["SBP_AMOUNT"] = "Total de vendas";
$MESS["SBP_PERIOD"] = "Período";
?>