<?
$MESS["CRM_TAB_1"] = "Importar Configurações";
$MESS["CRM_TAB_1_TITLE"] = "Editar importação de configurações";
$MESS["CRM_TAB_2"] = "Campos";
$MESS["CRM_TAB_2_TITLE"] = "Configurar mapeamento de campo";
$MESS["CRM_TAB_3"] = "Importar";
$MESS["CRM_TAB_3_TITLE"] = "Resultado da importação";
$MESS["CRM_IMPORT_NEXT_STEP"] = "Próximo >>";
$MESS["CRM_IMPORT_NEXT_STEP_TITLE"] = "Ir para a próxima etapa";
$MESS["CRM_IMPORT_PREVIOUS_STEP"] = "<< Voltar";
$MESS["CRM_IMPORT_PREVIOUS_STEP_TITLE"] = "Voltar à etapa anterior";
$MESS["CRM_IMPORT_DONE"] = "Pronto";
$MESS["CRM_IMPORT_CANCEL"] = "Cancelar";
$MESS["CRM_IMPORT_AGAIN"] = "Importar outro arquivo";
$MESS["CRM_IMPORT_AGAIN_TITLE"] = "Clicar para importar outro arquivo";
$MESS["CRM_IMPORT_DONE_TITLE"] = "Ir para produtos";
$MESS["CRM_IMPORT_CANCEL_TITLE"] = "Cancelar e voltar para produtos";
?>