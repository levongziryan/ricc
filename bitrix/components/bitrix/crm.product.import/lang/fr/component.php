<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_PRODUCT_IMP_COL_NAME"] = "Dénomination";
$MESS["CRM_PRODUCT_IMP_COL_DESCRIPTION"] = "Description";
$MESS["CRM_PRODUCT_IMP_COL_ACTIVE"] = "Actif(ve)";
$MESS["CRM_PRODUCT_IMP_COL_CURRENCY_ID"] = "Devises";
$MESS["CRM_PRODUCT_IMP_COL_PRICE"] = "Prix";
$MESS["CRM_PRODUCT_IMP_COL_VAT_ID"] = "Taux de TVA";
$MESS["CRM_PRODUCT_IMP_COL_VAT_INCLUDED"] = "La TVA est incluse dans le prix";
$MESS["CRM_PRODUCT_IMP_COL_MEASURE"] = "Unité de mesure";
$MESS["CRM_PRODUCT_IMP_COL_SECTION_ID"] = "Créer une section";
$MESS["CRM_PRODUCT_IMP_COL_SORT"] = "Trier";
$MESS["CRM_PRODUCT_IMP_COL_DETAIL_PICTURE"] = "Image détaillée";
$MESS["CRM_COLUMN_HEADER"] = "Colonne";
$MESS["CRM_PRODUCT_IMP_CSV_NF_ERROR"] = "Il faut choisir un fichier.";
$MESS["CRM_CSV_SEPARATOR_ERROR"] = "Dans un fichier de données il est utilisé un séparateur différent du séparateur choisi.";
$MESS["CRM_PRODUCT_IMP_FIELD_FILE"] = "Fichier de données (format CSV)";
$MESS["CRM_PRODUCT_IMP_FIELD_FILE_EXAMPLE"] = "Modèle de fichier à importer";
$MESS["CRM_SECTION_IMPORT_FILE_FORMAT"] = "Format du fichier";
$MESS["CRM_PRODUCT_IMP_FIELD_FILE_FIRST_HEADER"] = "La première ligne comprend les en-têtes";
$MESS["CRM_PRODUCT_IMP_FIELD_FILE_SKIP_EMPTY"] = "Faire passer des colonnes vides";
$MESS["CRM_PRODUCT_IMP_FIELD_FILE_SEPARATOR"] = "Séparateur des colonnes";
$MESS["CRM_PRODUCT_IMP_FIELD_FILE_SEPARATOR_SEMICOLON"] = "Point-virgule";
$MESS["CRM_PRODUCT_IMP_FIELD_FILE_SEPARATOR_COMMA"] = "Virgule";
$MESS["CRM_PRODUCT_IMP_FIELD_FILE_SEPARATOR_TAB"] = "Onglet";
$MESS["CRM_PRODUCT_IMP_FIELD_FILE_SEPARATOR_SPACE"] = "Blanc";
$MESS["CRM_SECTION_IMPORT_ASSOC_EXAMPLE"] = "Exemple de données d'importation";
$MESS["CRM_REQUIRE_FIELDS"] = "Pour l'importation réussie il est nécessaire de choisir les champs obligatoires suivants";
$MESS["CRM_DOWNLOAD"] = "Télécharger";
$MESS["CRM_PRODUCT_IMP_FIELD_FILE_ENCODING"] = "Codage du fichier de données";
$MESS["CRM_PRODUCT_IMP_FIELD_DEFAULT_FILE_ENCODING"] = "par défaut";
$MESS["CRM_PRODUCT_ADD_UNKNOWN_ERROR"] = "Une erreur est intervenue pendant la création de la marchandise";
$MESS["CRM_MEASURE_NOT_SELECTED"] = "[non indiqué]";
$MESS["CRM_PRODUCT_IMP_COL_PREVIEW_PICTURE"] = "Image de l'annonce";
$MESS["CRM_PRODUCT_NAV_TITLE_LIST"] = "L'importation de produit";
$MESS["CRM_PRODUCT_IMP_FIELD_FILE_SECTION_LEVELS"] = "Nombre de sections imbriquées";
$MESS["CRM_PRODUCT_IMP_SECTION_HEADER"] = "Section (niveau  #LEVEL_NUM#)";
$MESS["CRM_IMPORT_FINISH"] = "Prosucts importés avec succès";
$MESS["CRM_IMPORT_ERROR"] = "Non les produits importés";
$MESS["CRM_PRODUCT_IMP_SAMPLE_NAME"] = "Produit importé";
$MESS["CRM_PRODUCT_IMP_SAMPLE_DESCRIPTION"] = "Un produit pour la démo importation";
$MESS["CRM_PRODUCT_IMP_SAMPLE_SECTION_ID"] = "Les produits importés";
$MESS["CRM_PRODUCT_IMP_SAMPLE_STRING_VALUE"] = "Valeur Démo";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
?>