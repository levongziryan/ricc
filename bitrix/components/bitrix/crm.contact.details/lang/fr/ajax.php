<?
$MESS["CRM_CONTACT_NOT_FOUND"] = "Contact introuvable.";
$MESS["CRM_CONTACT_ACCESS_DENIED"] = "Accès refusé.";
$MESS["CRM_CONTACT_DELETION_ERROR"] = "Erreur lors de la suppression du contact.";
?>