<?
$MESS["SL_LINK_EDIT"] = "Paramètres";
$MESS["SL_ERROR_NO_IBLOCK"] = "Bloc d'information non spécifié.";
$MESS["SL_ERROR_WRONG_URL"] = "Adresse incorrecte du serveur.";
$MESS["SL_ERROR_ACCESS_DENIED"] = "Accès interdit.";
$MESS["SL_LINK_SYNC"] = "Synchronisation";
$MESS["SL_LINK_ADD"] = "Créer un Lien";
?>