<?
$MESS["CRM_ENT_DETAIL_MAIN_TAB"] = "General";
$MESS["CRM_ENT_DETAIL_COMPANY_URL_COPIED"] = "El enlace de la compañía ha sido copiado al Portapapeles";
$MESS["CRM_ENT_DETAIL_CONTACT_URL_COPIED"] = "El enlace de contacto ha sido copiado al portapapeles";
$MESS["CRM_ENT_DETAIL_COPY_COMPANY_URL"] = "El enlace de la compañía ha sido copiado al portapapeles";
$MESS["CRM_ENT_DETAIL_COPY_CONTACT_URL"] = "El enlace de contacto ha sido copiado al portapapeles";
$MESS["CRM_ENT_DETAIL_COPY_DEAL_URL"] = "El enlace de la negociación ha sido copiado al portapapeles";
$MESS["CRM_ENT_DETAIL_COPY_LEAD_URL"] = "El enlace del prospecto ha sido copiado al portapapeles";
$MESS["CRM_ENT_DETAIL_DEAL_URL_COPIED"] = "El enlace de la negociación se ha copiado en el portapapeles";
$MESS["CRM_ENT_DETAIL_LEAD_URL_COPIED"] = "El enlace del prospecto se ha copiado en el portapapeles";
$MESS["CRM_ENT_DETAIL_COPY_DEAL_RECURRING_URL"] = "Copiar URL de la negociación recurrente en el Portapapeles";
$MESS["CRM_ENT_DETAIL_DEAL_RECURRING_URL_COPIED"] = "La URL de la negociación recurrente se ha copiado en el portapapeles";
$MESS["CRM_ENT_DETAIL_DEAL_DELETE_DIALOG_TITLE"] = "Eliminar Negociación";
$MESS["CRM_ENT_DETAIL_LEAD_DELETE_DIALOG_TITLE"] = "Eliminar Prospecto";
$MESS["CRM_ENT_DETAIL_CONTACT_DELETE_DIALOG_TITLE"] = "Eliminar Contacto";
$MESS["CRM_ENT_DETAIL_COMPANY_DELETE_DIALOG_TITLE"] = "Eliminar Compañía";
$MESS["CRM_ENT_DETAIL_DEAL_DELETE_DIALOG_MESSAGE"] = "¿Usted está seguro que quiere eliminar esta negociación?";
$MESS["CRM_ENT_DETAIL_LEAD_DELETE_DIALOG_MESSAGE"] = "¿Usted está seguro que quiere eliminar este prospecto?";
$MESS["CRM_ENT_DETAIL_CONTACT_DELETE_DIALOG_MESSAGE"] = "¿Usted está seguro que quiere eliminar este contacto?";
$MESS["CRM_ENT_DETAIL_COMPANY_DELETE_DIALOG_MESSAGE"] = "¿Usted está seguro que quiere eliminar esta compañía?";
$MESS["CRM_ENT_DETAIL_REST_BUTTON"] = "Aplicaciones";
?>