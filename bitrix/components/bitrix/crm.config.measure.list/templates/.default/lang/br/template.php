<?
$MESS["CRM_MEASURE_SHOW_TITLE"] = "Ver detalhes para esta unidade de medida";
$MESS["CRM_MEASURE_SHOW"] = "Ver unidade";
$MESS["CRM_MEASURE_EDIT_TITLE"] = "Editar parâmetros para esta unidade de medida";
$MESS["CRM_MEASURE_EDIT"] = "Editar unidade";
$MESS["CRM_MEASURE_DELETE_TITLE"] = "Apagar esta unidade de medida";
$MESS["CRM_MEASURE_DELETE"] = "Excluir unidade";
$MESS["CRM_MEASURE_DELETE_CONFIRM"] = "Você tem certeza que deseja excluir '#MEASURE_TITLE#'?";
$MESS["CRM_ALL"] = "Total";
?>