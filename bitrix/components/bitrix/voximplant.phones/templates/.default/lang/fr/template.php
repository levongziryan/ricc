<?
$MESS["VI_PHONES_TITLE"] = "Connectez téléphones SIP";
$MESS["VI_PHONES_HELP_1"] = "<p>Connecter SIP téléphones à votre Bitrix24 pour faire et recevoir des appels maintenant.</p>
<p>Améliorez votre environnement d'affaires classique en permettant à vos employés pour afficher les informations d'un appelant sur l'écran en utilisant les téléphones de bureau communs. </p>";
$MESS["VI_PHONES_HELP_2"] = "<h3>Votre Desktop Phone: aussi facile que 1-2-3!</h3>
<p>Bitrix24 attribue une adresse du serveur, le login et mot de passe pour chaque utilisateur. Utilise les
pour configurer votre téléphone SIP.</p>
<p>Une fois que vous avez enregistré les préférences, votre téléphone est prêt pour une utilisation avec Bitrix24.</p>
<p>Pour connecter un téléphone, ouvrez les #LINK_USERS_START# Utilisateurs de téléphonie #LINK_END#
cette page, choisissez un utilisateur et activer la téléphonie pour eux.<p>
#IMAGE_CONNECT#
<p>Comme une alternative à des téléphones SIP, vous pouvez utiliser un téléphone DECT fixe commun relié à
une passerelle SIP qui convertit le signal de format SIP.</p>
<p>Un téléphone mobile est une autre option pour ceux toujours sur la route. Utilisez votre Windows Phone,
mobiles iOS ou Android basés en installant une application de téléphone logiciel sur elle.</p>
<p>S'il vous plaît se référer à la #LINK_COURSE_1_START#training course#LINK_END# pour la liste
des appareils vérifiés et les instructions de connexion.</p>
<h3>Utilisation de votre téléphonie</h3>
<p><b>Faire un appel à partir Bitrix24 CRM</b></p>
<p>Ne perdez pas vos boutons de temps poussant précieux. Il suffit de cliquer Appel en CRM et,
entendre votre téléphone carillon, le ramasser. Le téléphone sera momentanément commencer à composer
l'appelé. Avec votre ordinateur montrant encore la fenêtre d'appel, vous pouvez toujours voir
le profil du client ou de visualiser les informations détaillées Vous pouvez toujours le faire vieille école si vous voulez: composer le numéro sur le téléphone et
voir l'écran d'ordinateur montrant la fenêtre d'appel, comme si vous avez initié l'appel
de CRM.</p>
<P><b>Recevoir un appel</b></p> 
<p>Un appel entrant atteint votre navigateur (ou l'application de bureau) et la
téléphone simultanément. Vous pouvez répondre sur chaque dispositif; la conversation sera
enregistrée indépendamment de la manière vous avez répondu.</p>
<p>Lorsque vous êtes en conversation via le téléphone, tous les boutons de contrôle d'appel (Attente,
Muet, renvoi d'appel) sont disponibles dans la fenêtre Messenger (ou application de bureau).</p>
<p>Notez que si votre téléphone de bureau est connecté, tous les appels sortants sont à
être faite à travers elle. Vous pouvez déconnecter votre téléphone pendant que vous êtes loin de faire
les appels de l'application de votre navigateur ou de bureau.</p>
#IMAGE_CALL_WITHOUT_BROWER#
<p>S'il vous plaît se référer à la #LINK_COURSE_2_START# cours de formation #LINK_END# pour
détails sur la réalisation et la réception d'appels, et les instructions de connexion.</p>
";
?>