<?
$MESS["VI_PHONES_TITLE"] = "Conecte teléfonos SIP";
$MESS["VI_PHONES_HELP_1"] = "<p>Conectar los teléfonos SIP a su Bitrix24 y hacer y recibir llamadas ahora.</p>
<p>Mejorar su entorno empresarial clásico al permitir a sus empleados ver la información de una persona que llama en la pantalla durante el uso de teléfonos de escritorio comunes.</p>";
$MESS["VI_PHONES_HELP_2"] = "<h3>Su Escritorio Telefónico: tan fácil como 1-2-3!</h3>
<p>Bitrix24 asigna una dirección del servidor, nombre de usuario y contraseña para cada usuario. Usa estos para configurar el teléfono SIP.</p>
<p>Una vez que haya guardado las preferencias, su teléfono estará listo para su uso con Bitrix24.</p>
<p>Para conectar un teléfono, abra las #LINK_USERS_START#página de usuarios de telefonía #LINK_END# elegir un usuario y habilitar la telefonía para ellos.<p>
#IMAGE_CONNECT#
<p>Como alternativa a los teléfonos SIP, puede utilizar un teléfono DECT teléfono fijo común conectado a
una pasarela SIP que convierte la señal a formato SIP.</p>
<p>Un teléfono móvil es otra opción para aquellos que siempre está en movimiento. Utilice su Windows Phone,
Los móviles iOS o Android se basan mediante la instalación de cualquier aplicación softphone en él.</p>
<p>Por favor, consulte el #LINK_COURSE_1_START# curso de capacitación#LINK_END# para la lista
de dispositivos verificados y las instrucciones de conexión.</p>
<h3>Uso de Telefonía </h3>
<p><b>Realizar una llamada desde Bitrix24 CRM </b></p>
<p>No pierdas tu valioso tiempo pulsando botones. Simplemente haga clic en llamadas en el CRM, y escuche su repique de teléfono, y cojalo. El teléfono momentáneamente comenzará a marcar
el destinatario de la llamada. Con el ordenador sigue mostrando la ventana de llamada, siempre se puede ver
el perfil de cliente o ver la información detallada.</p>
<p>Siempre puedes hacerlo de la vieja escuela si quieres: marcar el número en el teléfono y
ver la pantalla del ordenador que muestra la ventana de llamada, como si usted haya iniciado la llamada
de CRM.</p>
<p><b> Recibir una llamada</b></p>
<p>Una llamada entrante llega a su navegador (o la aplicación de escritorio) y al
teléfono simultáneamente. Puede responder en cualquiera de los dispositivos; la conversación será
registrada con independencia de la forma en que respondió.</p>
<p>Cuando usted está conversando a través del teléfono, todos los botones de control de llamada (En espera,
Silencio, desvío de llamadas) están disponibles en la ventana de mensajería (o aplicación de escritorio).</P>
<p>Tenga en cuenta que si su teléfono de escritorio está conectado, todas las llamadas salientes son
hacerse a través de él. Puede desconectar su teléfono mientras está lejos de hacer llamadas desde la aplicación del navegador o de escritorio. </p>
#IMAGE_CALL_WITHOUT_BROWER#
<p>Por favor, consulte el #LINK_COURSE_2_START# el curso de capacitación#LINK_END# para
detalles sobre la realización y recepción de llamadas, y las instrucciones de conexión.</p>";
?>