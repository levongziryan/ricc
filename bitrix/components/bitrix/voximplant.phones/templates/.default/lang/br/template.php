<?
$MESS["VI_PHONES_TITLE"] = "Conectar telefones SIP";
$MESS["VI_PHONES_HELP_1"] = "<p>Conectar telefones SIP ao seu Bitrix24 para fazer e receber chamadas agora.</p> 
<p>Melhore seu clássico ambiente de negócios ao permitir que seus funcionários vejam as informações do autor da chamada na tela enquanto utilizam telefones comuns de mesa.</p>";
$MESS["VI_PHONES_HELP_2"] = "<h3>Seu Telefone de Mesa: Tão fácil como 1-2-3!</h3> 
<p>O Bitrix24 atribui um endereço de servidor, login e senha para cada usuário. Use-os 
para configurar seu telefone SIP.</p> 
<p>Uma vez que você tenha salvado as preferências, o telefone está pronto para uso com o Bitrix24.</p> 
<p>Para conectar um telefone, abra a página #LINK_USERS_START#Usuários de Telefonia#LINK_END#, escolha um usuário e ative a telefonia para eles.<p> 
#IMAGE_CONNECT# 
<p>Como alternativa aos telefones SIP, você pode usar um telefone fixo comum telefone DECT conectado a
 um gateway SIP que converte o sinal para formato SIP.</p> 
<p>Um telefone celular é outra opção para quem está sempre em movimento. Use seu celular Windows, 
iOS ou Android, instalando qualquer aplicativo de softphone nele.</p> 
<p>Consulte o #LINK_COURSE_1_START#curso de formação#LINK_END# para a lista
 de dispositivos testados e as instruções de conexão.</p> 
<h3>Usando Sua Telefonia</h3> 
<p><b>Fazendo uma chamada do CRM Bitrix24</b><p> 
<p>Não desperdice seu precioso tempo apertando botões. Apenas clique em Chamada no CRM e, ao ouvir o som do telefone, pegue-o. O telefone irá iniciar instantaneamente a discagem da chamada solicitada. Com seu computador ainda mostrando a janela da chamada, você sempre pode ver
 o perfil do cliente ou visualizar as informações detalhadas.</p> 
<p>Você pode sempre fazer do modo antigo se você desejar: disque o número do telefone e
 veja a tela do computador mostrando a janela da chamada como se você iniciasse a chamada
 do CRM.</p> 
<p><b>Recebendo uma chamada</b><p> 
<p>Uma chamada recebida chega ao seu navegador (ou ao aplicativo desktop) e ao
 telefone ao mesmo tempo. Você pode responder em qualquer um dos dispositivos; a conversa vai ser
 gravada independentemente da maneira que você respondeu.</p> 
<p>Quando você está conversando por telefone, todos os botões de controle de chamada (Espera, 
Mudo, Reencaminhamento de chamada) estão disponíveis na janela messenger (ou aplicativo para desktop).</p> 
<p>Observe que se seu telefone de mesa está conectado, todas as chamadas realizadas devem ser feitas através dele. Você pode desconectar seu telefone enquanto você está fora para fazer chamadas a partir do seu navegador ou aplicativo para desktop.</p> 
#IMAGE_CALL_WITHOUT_BROWER# 
<p>Consulte o #LINK_COURSE_2_START#curso de formação#LINK_END# para
 obter detalhes sobre como fazer e receber chamadas, e instruções de conexão.</p>";
?>