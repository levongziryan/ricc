<?
$MESS["WD_ERROR_BAD_SESSID"] = "Votre session a expiré. Répétez cette opération encore une fois, s'il vous plaît.";
$MESS["WD_ERROR_ELEMENT_ALREADY_EXISTS"] = "Un document de même nom existe déjà.";
$MESS["WD_ERROR_DELETE"] = "Le document n'est pas été supprimé.";
$MESS["WD_ERROR_BAD_ELEMENT_NAME"] = "Le nom du document ne peut pas contenir les caractères suivants: /\\<>|'':*?|+#";
$MESS["WD_ERROR_RECOVER"] = "La restitution du document n'était pas réussie.";
$MESS["WD_ERROR_BAD_ACTION"] = "L'action avec le dossier n'est pas indiquée.";
$MESS["WD_ERROR_EMPTY_ELEMENT_NAME"] = "Le nom du document n'est pas indiqué.";
$MESS["WD_ERROR_EXTENSION_MISTMATCH"] = "Le nouveau nom du document ne correspond pas au type du fichier.";
$MESS["WD_ERROR_BAD_STATUS"] = "Vous n'avez pas de droits pour sauvegarder le document dans ce statut.";
$MESS["WD_ERROR_WF_DELETE"] = "Vous n'avez pas de droits pour supprimer cette inscription.";
?>