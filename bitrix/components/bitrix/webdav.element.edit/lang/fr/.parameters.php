<?
$MESS["WD_ROOT_SECTION_ID"] = "ID de la section de racine (il est recommandé d'utiliser en ensemble avec un composant complexe)";
$MESS["WD_ELEMENT_ID"] = "Identifiant de l'élément";
$MESS["RATING_TYPE"] = "Vue des boutons de rating";
$MESS["SHOW_RATING"] = "Activer le classement";
$MESS["WD_PERMISSION"] = "Droits d'accès externes (il est recommandé d'utiliser dans le cadre d'un composant intégré)";
$MESS["WD_ACTION"] = "Action avec un élément";
$MESS["T_IBLOCK_DESC_NEWS_PANEL"] = "Ajouter les boutons au panneau admin. Pour ce composant";
$MESS["WD_REPLACE_SYMBOLS"] = "Remplacer des caractères interdits dans les noms de dossiers et de fichiers";
$MESS["WD_IBLOCK_ID"] = "Bloc d'information";
$MESS["RATING_TYPE_LIKE_GRAPHIC"] = "J'aime (graphique)";
$MESS["RATING_TYPE_LIKE_TEXT"] = "J'aime (textuel)";
$MESS["WD_NAME_FILE_PROPERTY"] = "Code de la qualité du bloc d'information pour le stockage du fichier (la qualité sera créée, si elle n'existe pas)";
$MESS["RATING_TYPE_STANDART_GRAPHIC"] = "J'aime / Je n'aime pas (graphique)";
$MESS["RATING_TYPE_STANDART_TEXT"] = "J'aime / Je n'aime pas (textuel)";
$MESS["SHOW_RATING_CONFIG"] = "ordinaire";
$MESS["RATING_TYPE_CONFIG"] = "ordinaire";
$MESS["WD_CHECK_CREATOR"] = "Vérifier le propriétaire du document";
$MESS["WD_ELEMENT_EDIT_URL"] = "Page de modification de l' élément";
$MESS["WD_USER_VIEW_URL"] = "Page d'affichage de l'information sur l'utilisateur";
$MESS["WD_SECTION_LIST_URL"] = "Page d'examen du catalogue";
$MESS["WD_ELEMENT_URL"] = "Page de la revue de l'élément (pour la recherche)";
$MESS["WD_ELEMENT_HISTORY_GET_URL"] = "Page de téléchargement du document (historique des modifications)";
$MESS["WD_IBLOCK_TYPE"] = "Type du bloc d'information";
?>