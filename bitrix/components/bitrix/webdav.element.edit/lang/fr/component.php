<?
$MESS["WD_ERROR_ELEMENT_NOT_FOUND"] = "lément introuvable.";
$MESS["WD_ACCESS_DENIED"] = "Accès interdit.";
$MESS["WD_ERROR_ELEMENT_LOCKED"] = "L'inscription est provisoirement bloquée par l'utilisateur #ID# (#DATE#).";
$MESS["WD_TITLE"] = "Dition";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "Le module de la bibliothèque des documents n'a pas été installé.";
$MESS["W_IBLOCK_IS_NOT_INSTALLED"] = "Le module blocs d'information n'a pas été installé";
$MESS["WD_ORIGINAL"] = "Original:";
$MESS["WD_TITLE_CLONE"] = "La création d'une version du document";
?>