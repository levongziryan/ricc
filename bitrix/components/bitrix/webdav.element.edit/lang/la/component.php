<?
$MESS["W_IBLOCK_IS_NOT_INSTALLED"] = "El módulo Blocks de Información no está instalado.";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "El módulo del WebDav no está instalado.";
$MESS["WD_ERROR_ELEMENT_NOT_FOUND"] = "El elemento no se encontró.";
$MESS["WD_TITLE"] = "Editar el elemento";
$MESS["WD_ERROR_ELEMENT_LOCKED"] = "El registro está temporalmente bloqueado por el usuario # #ID# (#DATE#).";
$MESS["WD_ACCESS_DENIED"] = "Acceso denegado.";
$MESS["WD_ORIGINAL"] = "Original: ";
$MESS["WD_TITLE_CLONE"] = "Crear la versión del archivo";
?>