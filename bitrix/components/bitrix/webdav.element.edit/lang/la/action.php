<?
$MESS["WD_ERROR_BAD_SESSID"] = "Su sesión ha expirado. Por favor repita la operación.";
$MESS["WD_ERROR_BAD_ACTION"] = "La acción de la carpeta no está especificada.";
$MESS["WD_ERROR_BAD_ELEMENT_NAME"] = "El nombre del archivo no debe contener los siguientes caracteres: /\\<>|\"':*?|+#";
$MESS["WD_ERROR_EMPTY_ELEMENT_NAME"] = "El nombre del archivo no está especificado.";
$MESS["WD_ERROR_ELEMENT_ALREADY_EXISTS"] = "Un elemento con este nombre ya existe.";
$MESS["WD_ERROR_DELETE"] = "La sección no fue borrada.";
$MESS["WD_ERROR_WF_DELETE"] = "Usted no tiene permiso para borrar este registro.";
$MESS["WD_ERROR_BAD_STATUS"] = "Usted no puede guardar el archivo en este estado.";
$MESS["WD_ERROR_RECOVER"] = "No se puede recuperar el documento.";
$MESS["WD_ERROR_EXTENSION_MISTMATCH"] = "El nombre del nuevo documento no coincide con el tipo de archivo.";
?>