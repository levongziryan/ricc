<?
$MESS["WD_ACCESS_DENIED"] = "Acesso negado.";
$MESS["WD_TITLE"] = "Editar Elemento";
$MESS["WD_ERROR_ELEMENT_NOT_FOUND"] = "Elemento não foi encontrado.";
$MESS["WD_ERROR_ELEMENT_LOCKED"] = "O registro está temporariamente bloqueado por usuário # #ID# (#DATE#).";
$MESS["W_IBLOCK_IS_NOT_INSTALLED"] = "Módulo de blocos de informações não está instalado.";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "Módulo WebDav não está instalado.";
$MESS["WD_ORIGINAL"] = "Original:";
$MESS["WD_TITLE_CLONE"] = "Criar versão do arquivo";
$MESS["WD_CHANGE_ELEMENT"] = "Editar Elemento";
$MESS["WD_COMMENTS_TITLE"] = "Documento de discussão";
$MESS["WD_DISK_NOTIFY_DIR_NUMERAL_1"] = "#COUNT# pasta";
$MESS["WD_DESCRIPTION"] = "Editar e excluir propriedades do elemento";
$MESS["WD_UPLOAD_UNLOCK"] = "Desbloquear documento após envio";
$MESS["Send"] = "Carregar";
$MESS["WD_NAME_LATIN"] = "Nome (apenas símbolos latinos)";
$MESS["WD_CREATE_NEW_IBLOCK"] = "Bloco de Nova Informação";
$MESS["WD_INSTALL"] = "Próxima>";
$MESS["WD_SELECT"] = "Selecionar";
?>