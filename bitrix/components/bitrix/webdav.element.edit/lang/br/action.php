<?
$MESS["WD_ERROR_ELEMENT_ALREADY_EXISTS"] = "Um elemento com este nome já existe.";
$MESS["WD_ERROR_EMPTY_ELEMENT_NAME"] = "O nome do arquivo não foi especificado.";
$MESS["WD_ERROR_BAD_ELEMENT_NAME"] = "O nome do arquivo não deve conter os seguintes caracteres: / \\ \\ <> | \":? * | + #";
$MESS["WD_ERROR_BAD_ACTION"] = "A ação da pasta não foi especificado.";
$MESS["WD_ERROR_DELETE"] = "O documento não foi excluído.";
$MESS["WD_ERROR_BAD_STATUS"] = "Você não tem permissão para salvar o arquivo.";
$MESS["WD_ERROR_WF_DELETE"] = "Você não tem permissão para excluir este registro.";
$MESS["WD_ERROR_BAD_SESSID"] = "Sua sessão expirou. Por favor, repita a operação.";
$MESS["WD_ERROR_RECOVER"] = "Não é possivel recuperar o documento.";
$MESS["WD_ERROR_EXTENSION_MISTMATCH"] = "O novo nome do documento não corresponde ao tipo de arquivo.";
$MESS["W_TITLE_EXTERNAL_LOCK"] = "Bloqueado";
$MESS["WD_DELETE_FILE"] = "Excluir";
$MESS["Title"] = "Título";
$MESS["WD_UPLOAD_INTERRUPT_CONFIRM"] = "O documento ainda está sendo carregado. Tem certeza de que quer cancelar?";
$MESS["WD_WARNING_SAME_NAME"] = "Um arquivo com este nome já existe. Você pode <a #LINK#>mudar o nome do arquivo</a>, ou fazer upload de uma nova versão, clicando em \"Upload\" (que preserva a versão anterior no changelog).";
$MESS["SORT_BY1_TIP"] = "Selecione aqui o campo pelo qual os itens que são documentos a serão classificados. Você pode selecionar <b> <i> (outro) </i> </b> e especificar o ID do campo no campo ao lado.";
$MESS["WD_ADD_SECTION"] = "Criar Pasta";
$MESS["WD_VERSIONS_MOD_OTHER"] = "versões";
$MESS["WD_TITLE_DESCRIPTION"] = "Descrição";
?>