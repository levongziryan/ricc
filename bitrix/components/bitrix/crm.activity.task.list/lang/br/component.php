<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["TASKS_MODULE_NOT_INSTALLED"] = "O módulo de Tarefas não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_TITLE"] = "Nome";
$MESS["CRM_COLUMN_ENTITY_TYPE"] = "Tipo";
$MESS["CRM_COLUMN_ENTITY_TITLE"] = "Título";
$MESS["CRM_COLUMN_REAL_STATUS"] = "Status";
$MESS["CRM_COLUMN_CHANGED_BY"] = "Modificado por";
$MESS["CRM_COLUMN_RESPONSIBLE_BY"] = "Pessoa responsável";
$MESS["CRM_COLUMN_DATE_CREATE"] = "Criado em";
$MESS["CRM_COLUMN_DATE_MODIFY"] = "Modificada em";
$MESS["CRM_COLUMN_CLOSED_DATE"] = "Encerrada em";
$MESS["CRM_COLUMN_DATE_START"] = "Data de início";
$MESS["CRM_COLUMN_PRIORITY"] = "Prioridade";
$MESS["CRM_PRESET_NEW"] = "Novas Tarefas";
$MESS["CRM_PRESET_CHANGE_TODAY"] = "Modificado Hoje";
$MESS["CRM_PRESET_CHANGE_YESTERDAY"] = "Modificado Ontem";
$MESS["CRM_PRESET_CHANGE_MY"] = "Modificado por mim";
$MESS["CRM_PRESET_MY"] = "Minhas Tarefas";
$MESS["TASKS_STATUS_1"] = "Novo";
$MESS["TASKS_STATUS_2"] = "Pendente";
$MESS["TASKS_STATUS_3"] = "Em andamento";
$MESS["TASKS_STATUS_4"] = "Supostamente concluído";
$MESS["TASKS_STATUS_5"] = "Concluído";
$MESS["TASKS_STATUS_6"] = "Deferido";
$MESS["TASKS_STATUS_7"] = "Recusado";
$MESS["TASKS_PRIORITY_0"] = "Baixo";
$MESS["TASKS_PRIORITY_1"] = "Normal";
$MESS["TASKS_PRIORITY_2"] = "Alto";
$MESS["CRM_ENTITY_TYPE_LEAD"] = "Lead";
$MESS["CRM_ENTITY_TYPE_CONTACT"] = "Contato";
$MESS["CRM_ENTITY_TYPE_COMPANY"] = "Empresa";
$MESS["CRM_ENTITY_TYPE_DEAL"] = "Negócio";
$MESS["CRM_COLUMN_UF_CRM_TASK"] = "Item CRM";
?>