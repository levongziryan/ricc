<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_TASK_SHOW_TITLE"] = "Visualizar tarefa";
$MESS["CRM_TASK_SHOW"] = "Visualizar tarefa";
$MESS["CRM_TASK_EDIT_TITLE"] = "Editar tarefa";
$MESS["CRM_TASK_EDIT"] = "Editar tarefa";
$MESS["CRM_TASK_DELETE_TITLE"] = "Excluir Tarefa";
$MESS["CRM_TASK_DELETE"] = "Excluir Tarefa";
$MESS["CRM_TASK_DELETE_CONFIRM"] = "Tem certeza de que deseja excluí-lo?";
?>