<?
$MESS["CRM_ACCOUNT_NUMBER_TEMPLATE_EXAMPLE"] = "Ejemplo:";
$MESS["CRM_ACCOUNT_NUMBER_NUMBER"] = "Número inicial:";
$MESS["CRM_ACCOUNT_NUMBER_NUMBER_DESC"] = "1 A 7 caracteres. El nuevo valor debe ser mayor que el anterior.";
$MESS["CRM_ACCOUNT_NUMBER_PREFIX"] = "Prefijo:";
$MESS["CRM_ACCOUNT_NUMBER_PREFIX_DESC"] = "1 a 7 caracteres (letras latinas, números, guiones, guiones bajos). Ejemplo: TEST_1234";
$MESS["CRM_ACCOUNT_NUMBER_DATE"] = "Período:";
$MESS["CRM_ACCOUNT_NUMBER_DATE_1"] = "Diario";
$MESS["CRM_ACCOUNT_NUMBER_DATE_2"] = "Mensual";
$MESS["CRM_ACCOUNT_NUMBER_DATE_3"] = "Anual";
$MESS["CRM_ACCOUNT_NUMBER_RANDOM"] = "Número de caracteres:";
$MESS["CRM_ACCOUNT_NUMBER_WARNING"] = "Prefijo:";
$MESS["CRM_ACCOUNT_NUMBER_TEMPL"] = "Plantilla:";
?>