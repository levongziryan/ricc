<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado. ";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_ACCOUNT_NUMBER_TEMPLATE_0"] = "Não utilizado";
$MESS["CRM_ACCOUNT_NUMBER_TEMPLATE_1"] = "Iniciar numeração a partir de";
$MESS["CRM_ACCOUNT_NUMBER_TEMPLATE_2"] = "Utilizar prefixo";
$MESS["CRM_ACCOUNT_NUMBER_TEMPLATE_3"] = "Número aleatório";
$MESS["CRM_ACCOUNT_NUMBER_TEMPLATE_4"] = "ID's de usuário e pedido";
$MESS["CRM_ACCOUNT_NUMBER_TEMPLATE_5"] = "Reiniciar numeração periodicamente";
?>