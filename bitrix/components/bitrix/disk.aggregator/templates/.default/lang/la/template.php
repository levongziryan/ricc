<?
$MESS["DISK_AGGREGATOR_DESCRIPTION"] = "Los enlaces a todas las bibliotecas de documentos de un empleado tienen acceso a: documentos privados; otros usuarios; documentos personales; páginas de documentos; los documentos de extranet e intranet.";
$MESS["DISK_AGGREGATOR_NETWORK_DRIVE"] = "Para tener acceso a todas las bibliotecas disponibles en Bitrix24 y manejarlas como carpetas y archivos, conecte esta página como una unidad de red.";
$MESS["DISK_AGGREGATOR_ND"] = "Conectar a unidad de red";
$MESS["DISK_AGGREGATOR_NETWORK_DRIVE_URL_TITLE"] = "Utilice esta dirección para asignar una unidad de red";
$MESS["DISK_AGGREGATOR_TITLE_NETWORK_DRIVE"] = "Unidad de Red";
$MESS["DISK_AGGREGATOR_TITLE_NETWORK_DRIVE_DESCR_MODAL"] = "Utilice esta dirección para conectarse";
$MESS["DISK_AGGREGATOR_BTN_CLOSE"] = "Cerrar";
$MESS["DISK_AGGREGATOR_CREATE_STORAGE"] = "Agregar almacenamiento";
?>