<?
$MESS["DISK_AGGREGATOR_DESCRIPTION"] = "A cet endroit vous trouverez les liens vers toutes les bibliothèques de documents auquel un utilisateur a accès: documents privés; autres documents personnels;  les pages de documents; les documents des groupes extranet et intranet.";
$MESS["DISK_AGGREGATOR_NETWORK_DRIVE_URL_TITLE"] = "Utilisez l'adresse suivante pour la connection à l'disque réseau";
$MESS["DISK_AGGREGATOR_ND"] = "Connecter le disque réseau";
$MESS["DISK_AGGREGATOR_NETWORK_DRIVE"] = "Pour avoir accès directement à toutes les bibliothèques de documents et  pouvoir commencer les utiliser, veuillez connecter cette page à l'disque du réseau.";
$MESS["DISK_AGGREGATOR_BTN_CLOSE"] = "Fermer";
$MESS["DISK_AGGREGATOR_TITLE_NETWORK_DRIVE_DESCR_MODAL"] = "Veuillez utiliser cette adresse pour la connection";
$MESS["DISK_AGGREGATOR_TITLE_NETWORK_DRIVE"] = "Disque réseau";
$MESS["DISK_AGGREGATOR_CREATE_STORAGE"] = "Ajouter un stockage";
?>