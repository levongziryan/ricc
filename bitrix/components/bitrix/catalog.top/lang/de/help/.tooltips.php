<?
$MESS ['CACHE_TYPE_TIP'] = "<i>Automatisch</i>: Der Cache ist gültig gemäß Definition in den Cache-Einstellungen;<br /><i>Cache</i>: immer cachen für den Zeitraum, der im nächsten Feld definiert wird;<br /><i>Nicht cachen</i>: es wird kein Caching ausgeführt.";
$MESS ['PRICE_VAT_INCLUDE_TIP'] = "Wenn diese Option aktiv ist, werden die Preise inklusive der MwSt. angezeigt.";
$MESS ['DISPLAY_COMPARE_TIP'] = "Wenn diese Option aktiv ist, wird der Button <i>Vergleichen</i>, mit dessen Hilfe Elemente zur Vergleichsliste hinzugefügt werden, angezeigt.";
$MESS ['USE_PRICE_COUNT_TIP'] = "Wenn diese Option aktiv ist, werden die Preise aller Produkttypen angezeigt.";
$MESS ['ELEMENT_SORT_FIELD_TIP'] = "Hier wird das Feld angegeben, nach dem die Sortierung der Elemente erfolgt.";
$MESS ['PROPERTY_CODE_TIP'] = "Zwischen den Informationsblock-Eigenschaften kann man die wählen, die bei der Anzeige der Elemente in der Tabelle angezeigt werden. Beim Auswählen des Punktes (nicht gewählt)-> und ohne die Angabe der Feld ID's in den unteren Zeilen, werden die Eigenschaften nicht angezeigt.";
$MESS ['IBLOCK_TYPE_TIP'] = "Wählen aus der angezeigten Liste einen Informationsblocktyp. Nachdem Sie <b><i>ok</i></b> gedrückt haben, werden Informationsblöcke vom ausgewählten Typ geladen.";
$MESS ['IBLOCK_ID_TIP'] = "Wählen Sie ein Informationsblock aus. Wenn der Punkt (andere)-> gewählt ist, müssen Sie im Feld daneben die ID des Informationsblocks eingeben.";
$MESS ['ELEMENT_COUNT_TIP'] = "Diese Zahl gibt die Anzahl der Elemente pro Seite an";
$MESS ['LINE_ELEMENT_COUNT_TIP'] = "Die Zahl bestimmt die Menge der Elemente in einer Zeile bei der Anzeige der Elemente.";
$MESS ['PRICE_CODE_TIP'] = "Es wird eingestellt, welcher der Preistypen für die Elemente angezeigt wird. Wenn keiner der Typen gewählt ist, werden der Preis und die Buttons <i>Kaufen</i> und <i>Zum Warenkorb</i> nicht angezeigt.";
$MESS ['BASKET_URL_TIP'] = "Hier wird der Pfad zur Warenkorbseite des Käufers angegeben.";
$MESS ['DETAIL_URL_TIP'] = "Hier wird der Pfad zur Seite mit der detailierten Beschreibung des Informationsblock-Elements angegeben.";
$MESS ['SECTION_URL_TIP'] = "Hier wird der Pfad zur Seite mit der Bereichsbeschreibung des Informationsblocks angegeben.";
$MESS ['CACHE_TIME_TIP'] = "Geben Sie die Cache-Laufzeit in Sekunden an.";
$MESS ['SHOW_PRICE_COUNT_TIP'] = "Hier können Sie die Menge festlegen, für die der Preis ausgegeben wird, z.B. 1 oder 10, abhängig von der Produktspezifikation.";
$MESS ['ELEMENT_SORT_ORDER_TIP'] = "In welcher Reihenfolge werden Elemente sortiert: aufsteigend oder absteigend.";
$MESS ['SECTION_ID_VARIABLE_TIP'] = "Variable, in der die ID des Informationsblock-Bereichs übergeben wird.";
$MESS ['PRODUCT_ID_VARIABLE_TIP'] = "Variable, in der die Produkt ID übergeben wird.";
$MESS ['ACTION_VARIABLE_TIP'] = "Geben Sie die Variable an, in der die Aktionen: ADD_TO_COMPARE_LIST, ADD2BASKET usw. übergeben werden. Als Standard enthält das Feld <i>action</i>.";
?>