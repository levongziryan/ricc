<?
$MESS["P_DATE_UPDATE"] = "Date de la mise à jour";
$MESS["SALE_ACTION"] = "Actions";
$MESS["SALE_DETAIL"] = "Date de modification";
$MESS["SALE_DETAIL_DESCR"] = "Modifier le profil";
$MESS["P_ID"] = "Code";
$MESS["P_NAME"] = "Dénomination";
$MESS["P_PERSON_TYPE"] = "Type de payeur";
$MESS["SALE_DELETE"] = "Supprimer";
$MESS["STPPL_DELETE_CONFIRM"] = "tes-vous sûr de vouloir supprimer ce profil?";
$MESS["SALE_DELETE_DESCR"] = "Supprimer le profil";
$MESS["PERSON_TYPE_ID"] = "Type de payeur";
$MESS["P_PERSON_TYPE_ID"] = "Type de payeur";
$MESS["STPPL_EMPTY_PROFILE_LIST"] = "Il n'y a aucun profil";
?>