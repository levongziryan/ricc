<?
$MESS["P_ID"] = "Código";
$MESS["P_NAME"] = "Nome";
$MESS["P_PERSON_TYPE"] = "Tipo de cliente";
$MESS["P_DATE_UPDATE"] = "Data de atualização";
$MESS["SALE_ACTION"] = "Ações";
$MESS["SALE_DETAIL"] = "Modificar";
$MESS["SALE_DELETE"] = "Deletar";
$MESS["SALE_DETAIL_DESCR"] = "Editar perfil";
$MESS["SALE_DELETE_DESCR"] = "Deletar perfil";
$MESS["STPPL_DELETE_CONFIRM"] = "Você tem certeza que deseja deletar este perfil?";
?>