<?
$MESS["FITWD_SERVICES_MAIN_SECTION"] = "Portail intranet";
$MESS["FITWD_SERVICES_PARENT_SECTION"] = "Gestion du temps de travail";
$MESS["FITWD_SECTION_TEMPLATE_NAME"] = "Traqueur facial pour la gestion de temps de travail";
$MESS["FITWD_SECTION_TEMPLATE_DESCRIPTION"] = "Permet aux employés de pointer en utilisant la reconnaissance faciale.";
?>