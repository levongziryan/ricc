<?
$MESS["FACEID_TRACKERWD_CMP_DESCR"] = "Face Tracker registrará y contará los visitantes en su tienda u oficina: total de visitantes para hoy; nuevos visitantes; Visitantes de vuelta; Visitantes que ya existen en su CRM.";
$MESS["FACEID_TRACKERWD_CMP_DESCR_P1"] = "Búsqueda instantánea por foto entre personas registradas en su CRM.";
$MESS["FACEID_TRACKERWD_CMP_DESCR_P1_NEW"] = "Face-tracker utiliza la tecnología de reconocimiento facial para identificar a sus clientes. Puede contar con todos los visitantes de su tienda o su oficina, contar cuántos visitantes son nuevos y cuántos están regresando, y cuántos ya están en su CRM.";
$MESS["FACEID_TRACKERWD_CMP_DESCR_P2"] = "Bitrix24 encontrará un perfil social por foto y agregará el perfil a su CRM.";
$MESS["FACEID_TRACKERWD_CMP_DESCR_P2_NEW"] = "Face-tracker puede identificar VK perfil social por foto tomada y agregar el perfil a su CRM.";
$MESS["FACEID_TRACKERWD_CMP_AUTO"] = "Auto";
$MESS["FACEID_TRACKERWD_CMP_AUTO_PHOTO"] = "toma una foto";
$MESS["FACEID_TRACKERWD_CMP_AUTO_LEAD"] = "crear un prospecto";
$MESS["FACEID_TRACKERWD_CMP_CAMERA"] = "Camara de video";
$MESS["FACEID_TRACKERWD_CMP_STATS"] = "Estadísticas de los visitantes";
$MESS["FACEID_TRACKERWD_CMP_STATS_NEW"] = "Nuevo";
$MESS["FACEID_TRACKERWD_CMP_STATS_OLD"] = "Regresando";
$MESS["FACEID_TRACKERWD_CMP_STATS_ALL"] = "Total";
$MESS["FACEID_TRACKERWD_CMP_STATS_CRM"] = "Estadísticas de CRM";
$MESS["FACEID_TRACKERWD_CMP_STATS_CRM_SAVE"] = "Guardado en el CRM";
$MESS["FACEID_TRACKERWD_CMP_STATS_DETAILED"] = "Detalles de las estadísticas";
$MESS["FACEID_TRACKERWD_CMP_HEAD_VISITORS"] = "Visitantes";
$MESS["FACEID_TRACKERWD_CMP_HEAD_CREDITS"] = "Reconocimiento de crédito:";
$MESS["FACEID_TRACKERWD_CMP_HEAD_CREDITS_ADD"] = "Agrear crédito";
$MESS["FACEID_TRACKERWD_CMP_FIRST_TIME_VISIT"] = "Tome una foto de su cliente para agregarla al sistema. Bitrix24 reconocerá al cliente en el próximo encuentro por lo que sabrá que es un cliente que regresa.<br> <br>Instale una cámara en su tienda u oficina para registrar a sus visitantes y mantener las estadísticas.";
$MESS["FACEID_TRACKERWD_CMP_FIRST_TIME_VISIT_NEW"] = "<div class=\"faceid-tracker-first-visit-desc\">
	<div class=\"faceid-tracker-first-visit-desc-inner\">
		<div class=\"faceid-tracker-first-visit-desc-title\">How do I start?</div>
		<div class=\"faceid-tracker-first-visit-desc-block\">
			<span class=\"faceid-tracker-first-visit-desc-item\">Tome una primera foto de su cliente y el face-tracker lo \"recordará\". La próxima vez que esta persona regrese, el sistema lo identificará como un visitante repetido.</span>
		</div>
	</div>
	<div class=\"faceid-tracker-first-visit-desc-inner\">
		<div class=\"faceid-tracker-first-visit-desc-title\">Cómo utilizar face tracker como contador de tráfico minorista</div>
		<div class=\"faceid-tracker-first-visit-desc-block\">
			<span class=\"faceid-tracker-first-visit-desc-item\">Instale la cámara cerca de la entrada de su oficina o tienda</span>
			<span class=\"faceid-tracker-first-visit-desc-item\">Conecte la cámara a cualquier computadora</span>
			<span class=\"faceid-tracker-first-visit-desc-item\">Iniciar face-tracker en este equipo</span>
			<span class=\"faceid-tracker-first-visit-desc-item\">Habilitar la toma automática de imágenes</span>
		</div>
	</div>
</div>
";
$MESS["FACEID_TRACKERWD_CMP_LIST_MORE"] = "Mostrar más";
$MESS["FACEID_TRACKERWD_CMP_VK_TITLE"] = "Buscar en el perfil de VK";
$MESS["FACEID_TRACKERWD_CMP_VK_PROGRESS"] = "Buscando...";
$MESS["FACEID_TRACKERWD_CMP_VK_FOUND_COUNT"] = "Personas encontradas:";
$MESS["FACEID_TRACKERWD_CMP_AGR_TITLE"] = "Términos de Uso";
$MESS["FACEID_TRACKERWD_CMP_AGR_TEXT"] = "<div class=\"tracker-agreement-popup-content\">
   <ol class=\"tracker-agreement-popup-list\">
    <li class=\"tracker-agreement-popup-list-item\">
	El Servicio FindFace (denominado en adelante \"Servicio FindFace\") es proporcionado por N-TECH.LAB LTD. 
	Los usuarios del Servicio FindFace están obligados a firmar un acuerdo con el Servicio FindFace y deben <a href=\"https://saas.findface.pro/files/docs/license_us.pdf\" class=\"tracker-link\">siguientes términos y condiciones</a> seleccionando \"Acepto\". 
	<br>
	Al seleccionar \"Acepto\", el Usuario acepta y garantiza que cumplirá los siguientes requisitos: 
     <ul class=\"tracker-agreement-popup-inner-list\">
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Seguir estrictamente todas las leyes y reglamentos locales que rigen la privacidad y el uso de datos personales para el país de residencia del Usuario.
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Seguir estrictamente todas las leyes y reglamentos locales. Este requisito permanecerá vigente durante toda la duración de la utilización del Servicio FindFace.
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Para obtener autorización por escrito, o según lo prescrito por las leyes locales de cada país en el que el Usuario esté operando
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       El Usuario está obligado a informar diligentemente a todas las personas cuyos datos personales, incluidas las fotografías e imágenes que el Usuario pretende procesar mediante la utilización del Servicio FindFace.
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
       Buscar asesoría legal ANTES de utilizar el Servicio FindFace.
      </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
	   Garantizar que cualquier información cargada por el Usuario ha sido autorizada por el individuo y cumple con las leyes y reglamentos de la jurisdicción del Usuario.
	  </li>
      <li class=\"tracker-agreement-popup-inner-list-item\">
	   Para confirmar que el Usuario está utilizando el Servicio de FindFace a su propio riesgo y sin garantía de ningún tipo.
	  </li>	  
     </ul>
    </li>
    <li class=\"tracker-agreement-popup-list-item\">
    Bitrix, Inc., no es responsable del Servicio y no hace reclamaciones ni garantías con respecto a la exactitud o disponibilidad del Servicio, ni proporciona ningún soporte técnico o consultas sobre el Servicio.
    </li>
   </ol>
   <div class=\"tracker-agreement-popup-description\">
    Bitrix, Inc, no es responsable del Servicio de FindFace y no hace reclamaciones ni garantías para el Servicio de FindFace.
	Al aceptar los términos y condiciones de este acuerdo de servicio, el Usuario acepta y confirma que el Servicio FindFace, ni Bitrix, Inc. recopila o procesa los datos cargados por el Usuario.
	El usuario confirma que el Servicio FindFace, ni Bitrix, Inc. tiene conocimiento de las circunstancias bajo las cuales el Usuario ha recopilado los datos, fotografías o imágenes.. 
	El usuario se compromete a mantener el servicio FindFace y Bitrix inofensivos por todas y cada una de las acciones e inacciones del usuario.
	<br><br>
	Resolución de conflictos; Arbitraje Vinculante: Cualquier disputa, controversia, interpretación o reclamación incluyendo reclamaciones por, pero no limitado a incumplimiento de contrato, cualquier forma de negligencia, fraude o declaraciones falsas que surjan de o se relacionen con este acuerdo serán sometidas a arbitraje final y vinculante. 
   </div>
  </div>";
$MESS["FACEID_TRACKERWD_CMP_AGR_BUTTON"] = "Acepto los términos";
$MESS["FACEID_TRACKERWD_CMP_JS_CAMERA_DEFAULT"] = "Cámara";
$MESS["FACEID_TRACKERWD_CMP_JS_CAMERA_NOT_FOUND"] = "No se encontró la cámara";
$MESS["FACEID_TRACKERWD_CMP_JS_CAMERA_NO_SUPPORT"] = "Su navegador no admite cámara";
$MESS["FACEID_TRACKERWD_CMP_JS_CAMERA_ERROR"] = "Su navegador no admite la grabación de vídeo o la cámara no está conectada.";
$MESS["FACEID_TRACKERWD_CMP_JS_FACE_NOT_FOUND"] = "Lamentablemente no pudimos reconocer a la persona. Intente tomar otra foto.";
$MESS["FACEID_TRACKERWD_CMP_JS_FACE_ORIGINAL"] = "Ejemplo de foto";
$MESS["FACEID_TRACKERWD_CMP_JS_SAVE_CRM"] = "Guardar en el CRM";
$MESS["FACEID_TRACKERWD_CMP_JS_SAVE_CRM_DONE"] = "Guardar en CRM";
$MESS["FACEID_TRACKERWD_CMP_JS_VK_LINK"] = "VK";
$MESS["FACEID_TRACKERWD_CMP_JS_VK_LINK_ACTION"] = "Encontrar el perfil de VK";
$MESS["FACEID_TRACKERWD_CMP_JS_VK_FOUND_PEOPLE"] = "gente";
$MESS["FACEID_TRACKERWD_CMP_JS_VK_SELECT"] = "Seleccionar";
$MESS["FACEID_TRACKERWD_CMP_AUTH_ONLY"] = "Este componente requiere autenticación.";
$MESS["FACEID_TRACKERWD_CMP_STATUS_STARTED"] = "Ingreso";
$MESS["FACEID_TRACKERWD_CMP_STATUS_ENDED"] = "Salida";
$MESS["FACEID_TRACKERWD_CMP_ACTION_END"] = "descanso";
$MESS["FACEID_TRACKERWD_CMP_TITLE"] = "Vea a las personas visitando sus lugares de ladrillo y mortero.";
?>