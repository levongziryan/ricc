<?
$MESS["TASKS_COPY_TASK"] = "Copy";
$MESS["TASKS_ADD_TASK_TO_TIMEMAN"] = "Agregar al plan de día de trabajo";
$MESS["TASKS_COPY_TASK_EX"] = "Tarea duplicada";
$MESS["TASKS_ADD_TASK_TO_TIMEMAN_EX"] = "Agregar al plan de día de trabajo";
$MESS["TASKS_ADD_SUBTASK"] = "Crear subtarea";
$MESS["TASKS_DELETE_TASK"] = "Eliminar";
$MESS["TASKS_DELETE_CONFIRM"] = "¿Realmente desea eliminarlo?";
$MESS["TASKS_DEFER_TASK"] = "Posponer";
$MESS["TASKS_RENEW_TASK"] = "Reanudar";
$MESS["TASKS_DELEGATE_TASK"] = "Delegar";
$MESS["TASKS_TASK_CONFIRM_START_TIMER_TITLE"] = "El temporizador se está utilizando ahora con otra tarea.";
$MESS["TASKS_TASK_CONFIRM_START_TIMER"] = "Ya está utilizando el temporizador para\"{{TITLE}}\". Esta tarea se detendrá. ¿Desea continuar?";
$MESS["TASKS_UNKNOWN"] = "Desconocido";
?>