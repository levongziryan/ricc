<?
$MESS["TASKS_COPY_TASK"] = "Copiar";
$MESS["TASKS_ADD_TASK_TO_TIMEMAN"] = "Add to working day plan";
$MESS["TASKS_COPY_TASK_EX"] = "Duplicate task";
$MESS["TASKS_ADD_TASK_TO_TIMEMAN_EX"] = "Add to working day plan";
$MESS["TASKS_ADD_SUBTASK"] = "Criar subtarefa";
$MESS["TASKS_DELETE_TASK"] = "Excluir";
$MESS["TASKS_DELETE_CONFIRM"] = "Do you really want to delete it?";
$MESS["TASKS_DEFER_TASK"] = "Adiar";
$MESS["TASKS_RENEW_TASK"] = "Retornar";
$MESS["TASKS_DELEGATE_TASK"] = "Delegar";
$MESS["TASKS_TASK_CONFIRM_START_TIMER_TITLE"] = "O gerenciador de tempo está sendo usado agora com outra tarefa.";
$MESS["TASKS_TASK_CONFIRM_START_TIMER"] = "Você já está usando o controlador de hora para \"{{TITLE}}\". Esta tarefa será pausada. Continuar?";
$MESS["TASKS_UNKNOWN"] = "Desconhecido";
?>