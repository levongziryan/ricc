<?
$MESS["CRM_ACTIVITY_WGT_PAGE_TITLE"] = "Rapport sommaire des activités";
$MESS["CRM_ACTIVITY_WGT_DEMO_TITLE"] = "Ceci est un affichage démo. Fermez-le pour obtenir les données analytiques de vos activités.";
$MESS["CRM_ACTIVITY_WGT_DEMO_CONTENT"] = "Si vous n'avez toujours aucune activité, <a href=\"#URL#\" class=\"#CLASS_NAME#\">créez-en une</a> maintenant!";
?>