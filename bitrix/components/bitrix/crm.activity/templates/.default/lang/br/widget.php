<?
$MESS["CRM_ACTIVITY_WGT_PAGE_TITLE"] = "Relatório de resumo de atividades";
$MESS["CRM_ACTIVITY_WGT_DEMO_TITLE"] = "Esta é uma visualização de demonstração. Feche para obter a análise de suas atividades.";
$MESS["CRM_ACTIVITY_WGT_DEMO_CONTENT"] = "Se você ainda não tiver nenhuma atividade, <a href=\"#URL#\" class=\"#CLASS_NAME#\">crie uma</a> agora!";
?>