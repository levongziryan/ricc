<?
$MESS["main_app_pass_del"] = "Eliminar";
$MESS["main_app_pass_title"] = "¿Qué es esto?";
$MESS["main_app_pass_text1"] = "Para sincronizar sus datos con aplicaciones externas de forma segura, use una contraseña especial que puede obtener en esta página.";
$MESS["main_app_pass_text2"] = "Cada una de estas herramientas soporta una amplia gama de aplicaciones que soportan la sincronización. Seleccione una aplicación para la que desea configurar la transferencia de datos.";
$MESS["main_app_pass_created"] = "Creada el ";
$MESS["main_app_pass_last"] = "Último acceso";
$MESS["main_app_pass_last_ip"] = "Ultima IP";
$MESS["main_app_pass_manage"] = "Gestionar";
$MESS["main_app_pass_link"] = "Conectar";
$MESS["main_app_pass_comment"] = "Comentario";
$MESS["main_app_pass_other"] = "Otros";
$MESS["main_app_pass_comment_ph"] = "Agregar descripción";
$MESS["main_app_pass_get_pass"] = "Obtener la contraseña";
$MESS["main_app_pass_create_pass"] = "Crear contraseña";
$MESS["main_app_pass_create_pass_text"] = "Utilice esta contraseña para sincronizar con la aplicación seleccionada.
	La contraseña no se mantendrá en forma abierta; Por lo tanto, sólo está disponible cuando se hayan obtenido.
	No cierre la ventana hasta que haya copiado o introducido su contraseña.";
$MESS["main_app_pass_create_pass_close"] = "Cerrar";
$MESS["main_app_pass_del_pass"] = "¿Está seguro de que desea eliminar la contraseña?";
$MESS["main_app_pass_del_pass_text"] = "La sincronización se cancelará porque la aplicación no podrá acceder a los datos debido al error de autenticación.";
$MESS["DAV_BUTTON_SAVE"] = "Guardar";
$MESS["DAV_ACCOUNTS_SECTION"] = "Usuarios del sistema";
$MESS["DAV_ENABLE"] = "Habilitar";
$MESS["DAV_ACCOUNTS_EXPORT_DEPARTMENT"] = "Departamento";
$MESS["DAV_CONTACTS"] = "Contactos del CRM";
$MESS["DAV_COMPANIES"] = "Compañías del CRM";
$MESS["DAV_EXTRANET_ACCOUNTS"] = "Usuarios de Extranet";
$MESS["DAV_MAX_COUNT"] = "Límite";
$MESS["DAV_EXPORT_FILTER"] = "Filtro";
$MESS["DAV_CARDDAV_SETTINGS_HELP"] = "El módulo DAV proporciona facilidad para sincronizar los contactos del sitio con
Aplicaciones y dispositivos compatibles con el protocolo CardDAV. Por ejemplo, iPhone y
IPad ofrecen soporte completo CardDAV.
<br><br>

<h3>Habilitación de la conexión CardDAV en su dispositivo Apple</h3>

Para configurar el soporte CardDAV en dispositivos Apple, siga estos pasos:
<ol>
<li>En el dispositivo Apple, abra <b>Ajustes</b> ingrese a <b>Cuentas</b> tab.</li>
<li>Tap <b>Agregar Cuenta</b>.</li>
<li>Seleccionar <b>CardDAV</b> como el tipo de cuenta.</li>
<li>En las preferencias de la cuenta, ingrese la dirección del sitio (#SERVER#) como servidor, y
   proporcione su nombre de usuario y contraseña.</li>
<li>Seleccionar <b>Autorización básica</b> como tipo de autenticación.</li>
<li>Para especificar el número de puerto opcional, guarde la cuenta y la 
   edición.</li>
</ol>

Todos los contactos aparecerán en la aplicación Contactos. Para activar o desactivar la 
Sincronización para los contactos seleccionados, utilice los siguientes ajustes.<br>";
$MESS["DAV_COMMON_SECTION"] = "Configuración general";
$MESS["DAV_DEFAULT_COLLECTION_TO_SYNC"] = "Sincronizar por defecto";
?>