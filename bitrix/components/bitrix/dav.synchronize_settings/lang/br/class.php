<?
$MESS["DAV_SYNCHRONIZE_TITLE"] = "Parâmetros de sincronização";
$MESS["dav_app_synchronize_auth"] = "Você precisa estar logado antes de utilizar parâmetros de sincronização.";
$MESS["DAV_ACCOUNTS"] = "Usuários do Sistema";
$MESS["DAV_CONTACTS"] = "Contatos do CRM";
$MESS["DAV_COMPANIES"] = "Empresas do CRM";
$MESS["DAV_EXTRANET_ACCOUNTS"] = "Usuários da Extranet";
?>