<?
$MESS["CRM_DEAL_VAR"] = "Nome variável do ID de negócio";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "Modelo de Caminho da Página de Índice";
$MESS["CRM_SEF_PATH_TO_LIST"] = "Modelo de Caminho de Página de Negócios";
$MESS["CRM_SEF_PATH_TO_FUNNEL"] = "Modelo de caminho de página de funil de vendas";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "Modelo de Caminho da Página do Editor de Negócio";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "Modelo de Caminho da Página de Visualização do Negócio";
$MESS["CRM_SEF_PATH_TO_IMPORT"] = "Modelo  de Caminho da Página Importação";
$MESS["CRM_ELEMENT_ID"] = "ID do negócio";
$MESS["CRM_NAME_TEMPLATE"] = "Formato do nome";
?>