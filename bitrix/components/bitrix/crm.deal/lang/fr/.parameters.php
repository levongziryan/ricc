<?
$MESS["CRM_DEAL_VAR"] = "Nom de valeur variable d'identifiant de transaction";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "Modèle de chemin d'accès à la page principale";
$MESS["CRM_SEF_PATH_TO_LIST"] = "Modèle de chemin d'accès à la page de la liste des transactions";
$MESS["CRM_SEF_PATH_TO_FUNNEL"] = "Modèle de chemin d'accès à la page de l'entonnoir des ventes";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "Modèle de chemin d'accès à la page de modification d'une opportunité";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "Modèle de chemin d'accès à la Page de Visualisation de l'opportunité";
$MESS["CRM_SEF_PATH_TO_IMPORT"] = "Importer la page Chemin Template";
$MESS["CRM_ELEMENT_ID"] = "ID de l'opportunité";
$MESS["CRM_NAME_TEMPLATE"] = "Format du nom";
?>