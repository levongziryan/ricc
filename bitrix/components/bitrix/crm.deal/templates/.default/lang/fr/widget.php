<?
$MESS["CRM_DEAL_WGT_FUNNEL"] = "Entonnoir des ventes pour les transactions";
$MESS["CRM_DEAL_WGT_SUM_DEAL_OVERALL"] = "Valeur total des transactions";
$MESS["CRM_DEAL_WGT_SUM_DEAL_WON"] = "Total des transactions gagnées";
$MESS["CRM_DEAL_WGT_SUM_DEAL_IN_WORK"] = "Total des transactions en cours";
$MESS["CRM_DEAL_WGT_RATING"] = "Classement des transactions gagnées";
$MESS["CRM_DEAL_WGT_SUM_INVOICE_OVERALL"] = "Total des ventes facturées";
$MESS["CRM_DEAL_WGT_SUM_INVOICE_OWED"] = "Totale des ventes non facturées";
$MESS["CRM_DEAL_WGT_PAYMENT_CONTROL"] = "Contrôle des paiements pour les transactions gagnées";
$MESS["CRM_DEAL_WGT_DEAL_IN_WORK"] = "Transactions en cours";
$MESS["CRM_DEAL_WGT_QTY_DEAL_IN_WORK"] = "Nombre de transactions en cours";
$MESS["CRM_DEAL_WGT_QTY_CALL"] = "Nombre d'appels";
$MESS["CRM_DEAL_WGT_QTY_ACTIVITY"] = "Décompte des activités";
$MESS["CRM_DEAL_WGT_QTY_DEAL_IDLE"] = "Nombre de transactions en attente";
$MESS["CRM_DEAL_WGT_PAGE_TITLE"] = "Analytique des transactions";
$MESS["CRM_DEAL_WGT_EMPLOYEE_DEAL_IN_WORK"] = "Transactions en cours (par employé)";
$MESS["CRM_DEAL_WGT_QTY_MEETING"] = "Nombre de réunion";
$MESS["CRM_DEAL_WGT_QTY_EMAIL"] = "Nombre d'e-mails";
$MESS["CRM_DEAL_WGT_QTY_DEAL_WON"] = "Nombre de transactions gagnées";
$MESS["CRM_DEAL_WGT_DEMO_TITLE"] = "Voici une démonstration du tableau de bord. Vous pouvez masquer les données de la démo et utiliser les données de vos transactions.";
$MESS["CRM_DEAL_WGT_DEMO_CONTENT"] = "Si vous n'avez pas de transaction, ajoutez votre <a href=\"#URL#\" class=\"#CLASS_NAME#\">première</a> dès maintenant !";
$MESS["CRM_DEAL_WGT_DEAL_CATEGORY"] = "Pipeline";
$MESS["CRM_DEAL_WGT_NO_SELECTED"] = "non sélectionné";
$MESS["CRM_DEAL_WGT_CURRENT"] = "[pipeline actuel]";
$MESS["CRM_DEAL_WGT_PAGE_TITLE_SHORT"] = "Rapports des transactions";
?>