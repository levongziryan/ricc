<?
$MESS["MD_EMPLOYEES_ALL"] = "A tous les employés";
$MESS["MD_GROUPS_TITLE"] = "Groupe de travail";
$MESS["MD_EMPLOYEES_TITLE"] = "Employés";
$MESS["MD_EXTRANET"] = "Extranet";
$MESS["MD_MOBILE_APPLICATION"] = "Application mobile";
$MESS["MD_DISK_TABLE_FOLDERS_FILES"] = "Dossiers: #FOLDERS# Mon disque: #FILES#";
$MESS["MD_GENERATE_BY_MOBILE"] = "Généré par l'application";
?>