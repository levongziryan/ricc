<?
$MESS["MD_EMPLOYEES_TITLE"] = "Empleados";
$MESS["MD_EMPLOYEES_ALL"] = "Todos los empleados";
$MESS["MD_GROUPS_TITLE"] = "Grupos de trabajo";
$MESS["MD_EXTRANET"] = "Extranet";
$MESS["MD_DISK_TABLE_FOLDERS_FILES"] = "Carpetas: #FOLDERS#  Archivos: #FILES#";
$MESS["MD_MOBILE_APPLICATION"] = "Aplicación móvil";
$MESS["MD_GENERATE_BY_MOBILE"] = "Generada por la aplicación";
$MESS["MD_COMPONENT_IM_RECENT"] = "Chats";
$MESS["MD_COMPONENT_IM_OPENLINES"] = "Canales Abiertos";
$MESS["MD_COMPONENT_MORE"] = "Menú";
?>