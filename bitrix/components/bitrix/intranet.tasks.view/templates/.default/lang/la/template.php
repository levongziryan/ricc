<?
$MESS["INTDT_SHOW"] = "Mostrar";
$MESS["INTDT_ALL_TASKS"] = "Todas las tareas";
$MESS["INTDT_ASSIGNED_TASKS"] = "Tareas que me fueron asignadas";
$MESS["INTDT_CREATED_TASKS"] = "Tareas que fueron creadas por mí";
$MESS["INTDT_PERSONAL_TASKS"] = "Tareas Personales";
$MESS["INTDT_DC_UP"] = "Doble click para navegar un nivel más";
$MESS["INTDT_DC_FOLDER"] = "Doble click para abrir la carpeta";
$MESS["INTDT_ACTIONS"] = "Acciones";
$MESS["INTDT_DC_TASK"] = "Doble Click para ver los detalles de la tarea";
$MESS["INTDT_NO_TASKS"] = "No se encontró tarea.";
?>