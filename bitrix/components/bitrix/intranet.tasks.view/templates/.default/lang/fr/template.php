<?
$MESS["INTDT_ALL_TASKS"] = "Toutes les tâches";
$MESS["INTDT_DC_FOLDER"] = "Double clic: passer à l'intérieur du dossier";
$MESS["INTDT_DC_UP"] = "Double-clique - remonter d'un niveau";
$MESS["INTDT_DC_TASK"] = "Double-clique - voir les détails de tâche";
$MESS["INTDT_ACTIONS"] = "Actions";
$MESS["INTDT_NO_TASKS"] = "Les tâches sont introuvables.";
$MESS["INTDT_ASSIGNED_TASKS"] = "Mes tâches";
$MESS["INTDT_PERSONAL_TASKS"] = "Tâches personnelles";
$MESS["INTDT_SHOW"] = "Afficher";
$MESS["INTDT_CREATED_TASKS"] = "Tâches créées par moi";
?>