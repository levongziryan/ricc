<?
$MESS["INTDT_ACTIONS"] = "Ações";
$MESS["INTDT_ALL_TASKS"] = "Todas as tarefas";
$MESS["INTDT_DC_UP"] = "Duplo clique para navegar um nível acima";
$MESS["INTDT_DC_FOLDER"] = "Duplo clique para abrir pasta";
$MESS["INTDT_DC_TASK"] = "Duplo clique para visualizar detalhes da tarefa";
$MESS["INTDT_NO_TASKS"] = "Nenhuma tarefa foi encontrada.";
$MESS["INTDT_PERSONAL_TASKS"] = "Tarefas pessoais";
$MESS["INTDT_SHOW"] = "Exibir";
$MESS["INTDT_ASSIGNED_TASKS"] = "Tarefas atribuídas a mim";
$MESS["INTDT_CREATED_TASKS"] = "Tarefas criadas por mim";
?>