<?
$MESS["P_USER_ID"] = "Identifiant de l'utilisateur";
$MESS["P_INDEX_URL"] = "Page de la liste commune";
$MESS["IBLOCK_SORT_DATE"] = "date";
$MESS["T_IBLOCK_DESC_NEWS_PANEL"] = "Ajouter les boutons au panneau admin. Pour ce composant";
$MESS["P_UPLOAD_URL"] = "Charger une photo";
$MESS["IBLOCK_SORT_SORT"] = "index de tri";
$MESS["IBLOCK_IBLOCK"] = "Bloc d'information";
$MESS["IBLOCK_SECTION_PAGE_ELEMENT_COUNT"] = "Nombre de photos sur une page";
$MESS["IBLOCK_SORT_NAME"] = "dénomination";
$MESS["IBLOCK_SORT_ASC"] = "en ordre ascendant";
$MESS["IBLOCK_SECTION_SORT_FIELD"] = "Par quel champ trions-nous les rubriques?";
$MESS["IBLOCK_SORT_DESC"] = "par ordre descendant";
$MESS["IBLOCK_SECTION_SORT_ORDER"] = "Ordre de triage des sections";
$MESS["P_GALLERY_AVATAR_SIZE"] = "Taille d'avatar de Galerie (px)";
$MESS["IBLOCK_TYPE"] = "Type du bloc d'information";
$MESS["P_SET_STATUS_404"] = "Tablir le statut 404 si l'élément ou la section ne sont pas trouvés";
$MESS["T_DATE_TIME_FORMAT"] = "Format de la date";
$MESS["IBLOCK_PAGE_NAVIGATION_TEMPLATE"] = "Modèle de pagination";
$MESS["P_GALLERY_URL"] = "Galerie de photos";
$MESS["P_GALLERY_GROUPS"] = "Les groupes d'utilisateurs dont les membres peuvent créer des galeries";
$MESS["P_GALLERY_SIZE"] = "Galerie taille (MB)";
$MESS["P_ONLY_ONE_GALLERY"] = "Les utilisateurs enregistrés peuvent créer une seule galerie";
$MESS["P_GALLERY_EDIT_URL"] = "Galerie de photos (d'édition)";
?>