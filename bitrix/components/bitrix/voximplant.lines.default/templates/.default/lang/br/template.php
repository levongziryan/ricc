<?
$MESS["VI_NUMBERS_TITLE_2"] = "Configurar números padrão";
$MESS["VI_NUMBERS_CONFIG"] = "Configurar";
$MESS["VI_NUMBERS_CONFIG_BACKPHONE"] = "Número padrão para chamadas realizadas";
$MESS["VI_NUMBERS_CONFIG_BACKPHONE_TITLE"] = "Suas contrapartes verão esse número quando você ligar para elas usando o Bitrix24";
$MESS["VI_NUMBERS_EDIT"] = "Editar";
$MESS["VI_NUMBERS_SAVE"] = "Salvar";
$MESS["VI_NUMBERS_APPLY"] = "Aplicar";
$MESS["VI_NUMBERS_CANCEL"] = "Cancelar";
?>