<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Модуль CRM не встановлено.";
$MESS["CRM_PERMISSION_DENIED"] = "Доступ заборонений";
$MESS["CRM_PRESET_ENTITY_TYPE_INVALID"] = "Некоректний тип шаблонів";
$MESS["CRM_PRESET_UFIELD_ID"] = "ID";
$MESS["CRM_PRESET_UFIELD_FIELD_NAME"] = "Ім'я";
$MESS["CRM_PRESET_UFIELD_FIELD_TITLE"] = "Назва";
?>