<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Das Modul CRM ist nicht installiert.";
$MESS["CRM_PERMISSION_DENIED"] = "Zugriff verweigert";
$MESS["CRM_PRESET_ENTITY_TYPE_INVALID"] = "Vorlagentyp ist nicht korrekt";
$MESS["CRM_PRESET_UFIELD_ID"] = "ID";
$MESS["CRM_PRESET_UFIELD_FIELD_NAME"] = "Name";
$MESS["CRM_PRESET_UFIELDS_TITLE"] = "Ungenutzte benutzerdefinierte Felder";
$MESS["CRM_PRESET_UFIELD_FIELD_TITLE"] = "Name";
$MESS["CRM_PRESET_UFIELD_FIELD_TYPE"] = "Typ";
?>