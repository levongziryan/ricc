<?
$MESS["VI_DOCS_TITLE"] = "Cargar Documentación";
$MESS["VI_DOCS_BODY"] = "Es un requisito de ley en algunos países que un proveedor de telefonía debe cumplir con proporcionar documentación legal con el fin de utilizar los números de alquiler.<br>Usted tiene que proporcionar documentación para reservar el número por la duración del contrato de arrendamiento.";
$MESS["VI_DOCS_UPDATE_BTN"] = "Cargar nuevos documentos";
$MESS["VI_DOCS_UPLOAD_BTN"] = "Cargar documentos";
$MESS["VI_DOCS_WAIT"] = "Cargando... Espere por favor";
$MESS["VI_DOCS_COUNTRY_RU"] = "Rusia";
$MESS["VI_DOCS_TABLE_LINK"] = "Historial de carga de documentación";
$MESS["VI_DOCS_TABLE_UPLOAD"] = "Cargado el";
$MESS["VI_DOCS_TABLE_STATUS"] = "Comprobación del estado";
$MESS["VI_DOCS_TABLE_TYPE"] = "Entidad legal";
$MESS["VI_DOCS_TABLE_COMMENT"] = "Comentario";
$MESS["VI_DOCS_STATUS"] = "Estado:";
$MESS["VI_DOCS_UNTIL_DATE"] = "Usted podrá cargar la documentación hasta #DATE#";
$MESS["VI_DOCS_UNTIL_DATE_NOTICE"] = "Después de la fecha indicada, los números reservados serán desconectados, y los fondos devueltos a su cuenta.<br><br>Los números de alquiler estarán activos hasta el final del plazo del arrendamiento.";
$MESS["VI_DOCS_UPLOAD_NOTICE"] = "Tenga en cuenta que su documentación es cargada directamente de Fastcom LLC destinada a la transformación según la legislación de los países respectivos. Bitrix Inc. no recopila, transferir o proceso alguno de sus datos personales en cualquier forma.";
$MESS["VI_DOCS_SERVICE_ERROR"] = "Error al enviar la solicitud al servicio de carga de documentos";
?>