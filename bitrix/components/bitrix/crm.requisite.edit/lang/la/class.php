<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_REQUISITE_EDIT_ERR_NOT_FOUND"] = "No se puede obtener los detalles de contacto o de la compañía (ID = #ID#)";
$MESS["CRM_REQUISITE_EDIT_ERR_PRESET_NOT_SELECTED"] = "No hay detalles de contacto o de la compañía para editar la plantilla especificada.";
$MESS["CRM_REQUISITE_EDIT_ERR_INVALID_DATA"] = "Datos incorrectos proporcionados para el contacto o información de la compañía.";
$MESS["CRM_REQUISITE_EDIT_ERR_ENTITY_TYPE_ID"] = "Tipo de entidad no válida proporcionada para su edición.";
$MESS["CRM_REQUISITE_EDIT_ERR_CONTACT_NOT_FOUND"] = "No se encontraron datos de la compañía o contacto para editar.";
$MESS["CRM_REQUISITE_EDIT_ERR_COMPANY_EDIT_DENIED"] = "El acceso a editar los datos de la compañía o contacto fue denegadao.";
$MESS["CRM_REQUISITE_EDIT_ERR_CONTACT_EDIT_DENIED"] = "El acceso a editar los datos de la compañía o contacto fue denegadao.";
$MESS["CRM_REQUISITE_EDIT_ENTITY_BINDING"] = "Obligatorio";
$MESS["CRM_REQUISITE_EDIT_PRESET_NAME"] = "Plantilla";
$MESS["CRM_REQUISITE_EDIT_PRESET_NOT_SELECTED"] = "(no seleccionado)";
$MESS["CRM_REQUISITE_EDIT_POPUP_RESPONSE"] = "Datos internos";
$MESS["CRM_REQUISITE_EDIT_CUSTOM_FORM_HTML"] = "Para datos adicionales";
$MESS["CRM_REQUISITE_BANK_DETAILS_EDITOR_FIELD"] = "Datos bancarios";
$MESS["CRM_SECTION_CONTACT_REQUISITE_INFO"] = "General";
$MESS["CRM_SECTION_COMPANY_REQUISITE_INFO"] = "General";
$MESS["CRM_SECTION_CONTACT_REQUISITE_VALUES"] = "Valores de datos de contacto";
$MESS["CRM_SECTION_COMPANY_REQUISITE_VALUES"] = "Valores de datos de la compañía";
$MESS["CRM_REQUISITE_EDIT_ERR_COMPANY_NOT_FOUND"] = "No se encontró la compañía para editar detalles de contacto de la compañía. ";
?>