<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_REQUISITE_EDIT_ERR_NOT_FOUND"] = "Impossible de récupérer les données de contact ou de la société (ID = #ID#)";
$MESS["CRM_REQUISITE_EDIT_ERR_PRESET_NOT_SELECTED"] = "Aucun modèle d'édition des données de contact ou de la société n'a été spécifié.";
$MESS["CRM_REQUISITE_EDIT_ERR_INVALID_DATA"] = "Des données incorrectes ont été fournies pour les données de contact ou de la société.";
$MESS["CRM_REQUISITE_EDIT_ERR_ENTITY_TYPE_ID"] = "Un type d'entité invalide a été fourni pour l'édition.";
$MESS["CRM_REQUISITE_EDIT_ERR_COMPANY_NOT_FOUND"] = "Aucune société n'a été trouvée pour éditer les données de contact ou de la société.";
$MESS["CRM_REQUISITE_EDIT_ERR_CONTACT_NOT_FOUND"] = "Aucun contact n'a été trouvé pour éditer les données de contact ou de la société.";
$MESS["CRM_REQUISITE_EDIT_ERR_COMPANY_EDIT_DENIED"] = "L'édition de l'accès aux détails de contact ou de la société a été refusé.";
$MESS["CRM_REQUISITE_EDIT_ERR_CONTACT_EDIT_DENIED"] = "L'édition de l'accès aux détails de contact ou de la société a été refusé.";
$MESS["CRM_REQUISITE_EDIT_ENTITY_BINDING"] = "Lien";
$MESS["CRM_REQUISITE_EDIT_PRESET_NAME"] = "Modèle";
$MESS["CRM_REQUISITE_EDIT_PRESET_NOT_SELECTED"] = "(non sélectionné)";
$MESS["CRM_REQUISITE_EDIT_POPUP_RESPONSE"] = "Donnée interne";
$MESS["CRM_REQUISITE_EDIT_CUSTOM_FORM_HTML"] = "Supplément pour donnée";
$MESS["CRM_REQUISITE_BANK_DETAILS_EDITOR_FIELD"] = "Attributs";
$MESS["CRM_SECTION_CONTACT_REQUISITE_INFO"] = "Général";
$MESS["CRM_SECTION_COMPANY_REQUISITE_INFO"] = "Général";
$MESS["CRM_SECTION_CONTACT_REQUISITE_VALUES"] = "Valeurs des données de contact";
$MESS["CRM_SECTION_COMPANY_REQUISITE_VALUES"] = "Valeurs des données de la société";
?>