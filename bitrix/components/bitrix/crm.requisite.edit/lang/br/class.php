<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["CRM_REQUISITE_EDIT_ERR_NOT_FOUND"] = "Não é possível obter contato ou informações da empresa (ID = #ID#)";
$MESS["CRM_REQUISITE_EDIT_ERR_PRESET_NOT_SELECTED"] = "Nenhum modelo especificado de edição de informações de contato ou empresa.";
$MESS["CRM_REQUISITE_EDIT_ERR_INVALID_DATA"] = "Dados incorretos fornecidos de contato ou informações da empresa.";
$MESS["CRM_REQUISITE_EDIT_ERR_ENTITY_TYPE_ID"] = "Tipo de entidade inválido fornecido para edição.";
$MESS["CRM_REQUISITE_EDIT_ERR_COMPANY_NOT_FOUND"] = "Nenhuma empresa encontrada para editar contato ou informações da empresa.";
$MESS["CRM_REQUISITE_EDIT_ERR_CONTACT_NOT_FOUND"] = "Nenhum contato encontrado para editar contato ou informações da empresa.";
$MESS["CRM_REQUISITE_EDIT_ERR_COMPANY_EDIT_DENIED"] = "O acesso de edição do contato ou informações da empresa foi negado.";
$MESS["CRM_REQUISITE_EDIT_ERR_CONTACT_EDIT_DENIED"] = "O acesso de edição do contato ou informações da empresa foi negado.";
$MESS["CRM_REQUISITE_EDIT_ENTITY_BINDING"] = "Obrigatório";
$MESS["CRM_REQUISITE_EDIT_PRESET_NAME"] = "Modelo";
$MESS["CRM_REQUISITE_EDIT_PRESET_NOT_SELECTED"] = "(não selecionado)";
$MESS["CRM_REQUISITE_EDIT_POPUP_RESPONSE"] = "Dados internos";
$MESS["CRM_REQUISITE_EDIT_CUSTOM_FORM_HTML"] = "Adicionais para dados";
$MESS["CRM_REQUISITE_BANK_DETAILS_EDITOR_FIELD"] = "Informações bancárias";
$MESS["CRM_SECTION_CONTACT_REQUISITE_INFO"] = "Geral";
$MESS["CRM_SECTION_COMPANY_REQUISITE_INFO"] = "Geral";
$MESS["CRM_SECTION_CONTACT_REQUISITE_VALUES"] = "Valores de informações de contato";
$MESS["CRM_SECTION_COMPANY_REQUISITE_VALUES"] = "Valores de informações da empresa";
?>