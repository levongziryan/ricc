<?
$MESS["CRM_REQUISITE_CUSTOM_SAVE_BUTTON_TITLE"] = "Guardar y Regresar";
$MESS["CRM_TAB_1_CONTACT"] = "Datos del contacto";
$MESS["CRM_TAB_1_COMPANY"] = "Datos de la compañía";
$MESS["CRM_TAB_1_TITLE_CONTACT"] = "Información de contacto";
$MESS["CRM_TAB_1_TITLE_COMPANY"] = "Información de la compañía";
$MESS["CRM_REQUISITE_SHOW_TITLE_COMPANY"] = "Datos de la compañía";
$MESS["CRM_REQUISITE_SHOW_TITLE_CONTACT"] = "Datos del contacto";
$MESS["CRM_REQUISITE_SHOW_NEW_TITLE_COMPANY"] = "Datos de la nueva compañía";
$MESS["CRM_REQUISITE_SHOW_NEW_TITLE_CONTACT"] = "Datos del nuevo contacto";
$MESS["CRM_REQUISITE_SERCH_RESULT_NOT_FOUND"] = "Su solicitud de busqueda no tiene ningún resultado.";
$MESS["CRM_REQUISITE_EDIT_DUP_CTRL_SHORT_SUMMARY_TITLE"] = "encontrado";
$MESS["CRM_REQUISITE_EDIT_DUP_CTRL_WARNING_DLG_TITLE"] = "Posibles clones";
$MESS["CRM_REQUISITE_EDIT_DUP_CTRL_WARNING_ACCEPT_BTN_TITLE"] = "Ignorar y guardar";
$MESS["CRM_REQUISITE_EDIT_DUP_CTRL_WARNING_CANCEL_BTN_TITLE"] = "Cancelar";
$MESS["CRM_REQUISITE_EDIT_DUP_CTRL_REQUISITE_SUMMARY_TITLE"] = "por contacto/detalles de la compañía";
$MESS["CRM_REQUISITE_EDIT_DUP_CTRL_BANK_DETAIL_SUMMARY_TITLE"] = "por detalles bancarios";
?>