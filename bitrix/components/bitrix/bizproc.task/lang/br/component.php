<?
$MESS["BPAT_EMPTY_TASK"] = "A atribuição da ID não foi especificada.";
$MESS["BPAT_TITLE"] = "Atribuições";
$MESS["BPAT_NO_TASK"] = "A atribuição não foi encontrada.";
$MESS["BPAT_NO_ACCESS"] = "Você não pode visualizar a tarefa selecionada.";
$MESS["BPAT_DELEGATE_LABEL"] = "Delegar";
$MESS["BPAT_NO_STATE"] = "O processo empresarial não foi encontrado.";
$MESS["BPAT_DELEGATE_SELECT"] = "Selecionar";
$MESS["BPAT_DELEGATE_CANCEL"] = "Cancelar";
?>