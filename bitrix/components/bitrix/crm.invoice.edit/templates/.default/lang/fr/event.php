<?
$MESS["CRM_INVOICE_EDIT_EVENT_SUCCESSFULLY_CREATED"] = "La facture <a href='#URL#'>#TITLE#</a> a été créée. Vous allez être maintenant redirigé vers la page précédente. Si la page actuelle est toujours affichée, veuillez la fermer manuellement.";
$MESS["CRM_INVOICE_EDIT_EVENT_CANCELED"] = "Une action a été annulée. Vous êtes maintenant redirigé vers la page précédente. Si la page actuelle est toujours affichée, veuillez la fermer manuellement.";
?>