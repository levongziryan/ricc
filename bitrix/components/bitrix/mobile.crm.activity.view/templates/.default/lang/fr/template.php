<?
$MESS["M_CRM_ACTIVITY_VIEW_EMAIL_IN_LEGEND"] = "(Réception)";
$MESS["M_CRM_ACTIVITY_VIEW_CALL_IN_LEGEND"] = "(Réception)";
$MESS["M_CRM_ACTIVITY_VIEW_EMAIL_OUT_LEGEND"] = "(Envoyés)";
$MESS["M_CRM_ACTIVITY_VIEW_CALL_OUT_LEGEND"] = "(Envoyés)";
$MESS["M_CRM_ACTIVITY_VIEW_NO_TITLE"] = "[sans nom]";
$MESS["M_CRM_ACTIVITY_VIEW_SET_NOT_COMPLETED"] = "Curriculum vitae";
$MESS["M_CRM_ACTIVITY_VIEW_DELETION_CONFIRMATION"] = "Souhaitez-vous supprimer l'affaire?";
$MESS["M_CRM_ACTIVITY_VIEW_STATUS_COMPLETED"] = "Achevé(e)s";
$MESS["M_CRM_ACTIVITY_VIEW_SET_COMPLETED"] = "Achever";
$MESS["M_CRM_ACTIVITY_VIEW_TIME"] = "Quand";
$MESS["M_CRM_ACTIVITY_VIEW_COMMUNICATION_OUT"] = "Dans";
$MESS["M_CRM_ACTIVITY_VIEW_LOCATION"] = "Adresse domicile";
$MESS["M_CRM_ACTIVITY_VIEW_LOAD_TEXT"] = "Mise à jour...";
$MESS["M_CRM_ACTIVITY_VIEW_STATUS_NOT_COMPLETED"] = "Attend l'exécution";
$MESS["M_CRM_ACTIVITY_VIEW_DESCRIPTION"] = "Description";
$MESS["M_CRM_ACTIVITY_VIEW_COMMUNICATION_IN"] = "De qui";
$MESS["M_CRM_ACTIVITY_VIEW_RESPONSIBLE"] = "Responsable";
$MESS["M_CRM_ACTIVITY_VIEW_DOWN_TEXT"] = "Relâcher pour mettre à jour...";
$MESS["M_CRM_ACTIVITY_VIEW_ACTION_CALL_TO"] = "Appel";
$MESS["M_CRM_ACTIVITY_VIEW_PULL_TEXT"] = "Tirer pour mettre à jour...";
$MESS["M_CRM_ACTIVITY_VIEW_EDIT"] = "Editer";
$MESS["M_CRM_ACTIVITY_VIEW_DEAL_OWNER"] = "Affaire";
$MESS["M_CRM_ACTIVITY_VIEW_STATUS"] = "Statut";
$MESS["M_CRM_ACTIVITY_VIEW_DELETION_TITLE"] = "Suppression du dossier";
$MESS["M_CRM_ACTIVITY_VIEW_DELETE"] = "Supprimer";
$MESS["M_CRM_ACTIVITY_VIEW_CUT"] = "Continuer la lecture...";
?>