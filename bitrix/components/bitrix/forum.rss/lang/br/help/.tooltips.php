<?
$MESS["CACHE_TYPE_TIP"] = "Os tópicos foram fechadas com sucesso.";
$MESS["CACHE_TIME_TIP"] = "Os tópicos foram excluídos com sucesso.";
$MESS["RSS_TYPE_TIP"] = "Os tópicos foram abertos com sucesso.";
$MESS["FID_RANGE_TIP"] = "Os tópicos foram fixados com sucesso.";
$MESS["COUNT_TIP"] = "Os tópicos foram desfixados com sucesso.";
$MESS["TN_TITLE_TIP"] = "Os temas selecionados para serem movidos não existem ou são parte de outro fórum.";
$MESS["TN_DESCRIPTION_TIP"] = "Os temas selecionados para serem movidos não existem ou são parte de outro fórum.";
$MESS["PATH_TO_SMILE_TIP"] = "O tipo de RSS não está especificado";
$MESS["URL_TEMPLATES_LIST_TIP"] = "O usuário não pode acessar o fórum.";
$MESS["URL_TEMPLATES_READ_TIP"] = "O usuário não tem permissão para criar novos tópicos.";
$MESS["URL_TEMPLATES_PROFILE_VIEW_TIP"] = "O ID de usuário não foi especificado";
$MESS["URL_TEMPLATES_RSS_TIP"] = "O ID do usuário não está especificado.";
$MESS["TYPE_RANGE_TIP"] = "O ID do usuário não está especificado.";
$MESS["TYPE_TIP"] = "O usuário não foi encontrado.";
?>