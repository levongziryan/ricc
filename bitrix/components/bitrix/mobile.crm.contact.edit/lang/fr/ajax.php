<?
$MESS["CRM_ENTITY_DATA_NOT_FOUND"] = "DATA_NOT_FOUND: Impossible de trouver les données pour le traitement.";
$MESS["CRM_ENTITY_ID_NOT_FOUND"] = "ID_NOT_FOUND: ID n'est pas trouvé.";
$MESS["CRM_ENTITY_TYPE_NOT_SUPPORTED"] = "TYPE_NOT_SUPPORTED: Le type '#ENTITY_TYPE#' n'est pas soutenu dans le contexte courant.";
$MESS["CRM_ACCESS_DENIED"] = "Accès interdit.";
$MESS["CRM_CONTACT_NOT_FOUND"] = "Chec d'établissement du contact avec l'identificateur #ID#.";
$MESS["CRM_CONTACT_COULD_NOT_DELETE"] = "Impossible de supprimer le contact.";
$MESS["CRM_CONTACT_NAME_NOT_ASSIGNED"] = "Veuillez renseigner le nom et (ou) le nom de famille du contact.";
?>