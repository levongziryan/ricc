<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["M_CRM_INVOICE_LIST_PRESET_MY_UNPAID"] = "Minhas faturas pendentes";
$MESS["M_CRM_INVOICE_LIST_PRESET_MY_PAID"] = "Minhas faturas pagas";
$MESS["M_CRM_INVOICE_LIST_FILTER_CUSTOM"] = "Resultados da pesquisa";
$MESS["M_CRM_INVOICE_LIST_FILTER_NONE"] = "Todas as faturas";
$MESS["M_CRM_LEAD_LIST_PRESET_USER"] = "Filtro personalizado";
$MESS["M_CRM_INVOICE_LIST_SEND"] = "Enviar";
$MESS["M_CRM_INVOICE_LIST_CHANGE_STATUS"] = "Alterar status";
$MESS["M_CRM_INVOICE_LIST_MORE"] = "Mais...";
$MESS["M_CRM_INVOICE_LIST_EDIT"] = "Editar";
$MESS["M_CRM_INVOICE_LIST_DELETE"] = "Excluir";
$MESS["M_CRM_INVOICE_LIST_COMPANY"] = "Vinculado à empresa";
$MESS["M_CRM_INVOICE_LIST_CONTACT"] = "Vinculado ao contato";
$MESS["M_CRM_INVOICE_LIST_DEAL"] = "Vinculado à negociação";
$MESS["M_CRM_INVOICE_LIST_QUOTE"] = "Vinculado à cotação";
?>