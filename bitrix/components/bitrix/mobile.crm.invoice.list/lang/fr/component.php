<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["M_CRM_INVOICE_LIST_PRESET_MY_UNPAID"] = "Mes exposés";
$MESS["M_CRM_INVOICE_LIST_PRESET_MY_PAID"] = "Payés par moi";
$MESS["M_CRM_INVOICE_LIST_FILTER_CUSTOM"] = "Résultats de la recherche";
$MESS["M_CRM_INVOICE_LIST_FILTER_NONE"] = "Toutes les factures";
$MESS["M_CRM_LEAD_LIST_PRESET_USER"] = "Filtre personnalisé";
$MESS["M_CRM_INVOICE_LIST_SEND"] = "Envoyer";
$MESS["M_CRM_INVOICE_LIST_CHANGE_STATUS"] = "Modifier le statut";
$MESS["M_CRM_INVOICE_LIST_MORE"] = "Plus...";
$MESS["M_CRM_INVOICE_LIST_EDIT"] = "Modifier";
$MESS["M_CRM_INVOICE_LIST_DELETE"] = "Supprimer";
$MESS["M_CRM_INVOICE_LIST_COMPANY"] = "Lié à l'entreprise";
$MESS["M_CRM_INVOICE_LIST_CONTACT"] = "Lié au contact";
$MESS["M_CRM_INVOICE_LIST_DEAL"] = "Lié à l'affaire";
$MESS["M_CRM_INVOICE_LIST_QUOTE"] = "Lié au devis";
?>