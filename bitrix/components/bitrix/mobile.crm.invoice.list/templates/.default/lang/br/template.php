<?
$MESS["M_CRM_INVOICE_LIST_PULL_TEXT"] = "Puxe para baixo para atualizar...";
$MESS["M_CRM_INVOICE_LIST_DOWN_TEXT"] = "Solte para atualizar...";
$MESS["M_CRM_INVOICE_LIST_LOAD_TEXT"] = "Atualizando...";
$MESS["M_CRM_INVOICE_LIST_SEARCH_PLACEHOLDER"] = "Pesquisar";
$MESS["M_CRM_INVOICE_LIST_SEARCH_BUTTON"] = "Pesquisar";
$MESS["M_CRM_INVOICE_LIST_FILTER_NONE"] = "Todas as faturas";
$MESS["M_CRM_INVOICE_LIST_FILTER_CUSTOM"] = "Resultados da pesquisa";
$MESS["M_CRM_INVOICE_LIST_NUMBER"] = "##NUM#";
$MESS["M_CRM_INVOICE_LIST_CLIENT_CAPTION"] = "enviar para";
$MESS["M_CRM_INVOICE_LIST_DEAL_CAPTION"] = "para";
$MESS["M_CRM_INVOICE_LIST_TITLE"] = "Todas as faturas";
$MESS["M_CRM_INVOICE_ADD"] = "Criar fatura";
?>