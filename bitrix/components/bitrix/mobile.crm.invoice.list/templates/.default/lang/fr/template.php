<?
$MESS["M_CRM_INVOICE_LIST_NUMBER"] = "##NUM#";
$MESS["M_CRM_INVOICE_LIST_FILTER_NONE"] = "Tous les comptes";
$MESS["M_CRM_INVOICE_LIST_LOAD_TEXT"] = "Mise à jour...";
$MESS["M_CRM_INVOICE_LIST_DOWN_TEXT"] = "Relâcher pour mettre à jour...";
$MESS["M_CRM_INVOICE_LIST_DEAL_CAPTION"] = "Pour";
$MESS["M_CRM_INVOICE_LIST_SEARCH_BUTTON"] = "Recherche";
$MESS["M_CRM_INVOICE_LIST_SEARCH_PLACEHOLDER"] = "Recherche";
$MESS["M_CRM_INVOICE_LIST_PULL_TEXT"] = "Tirer pour mettre à jour...";
$MESS["M_CRM_INVOICE_LIST_FILTER_CUSTOM"] = "Résultats de la recherche";
$MESS["M_CRM_INVOICE_LIST_CLIENT_CAPTION"] = "dès";
$MESS["M_CRM_INVOICE_LIST_TITLE"] = "Toutes les factures";
$MESS["M_CRM_INVOICE_ADD"] = "Créer une facture";
?>