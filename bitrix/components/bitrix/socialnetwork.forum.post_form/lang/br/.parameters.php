<?
$MESS["F_DEFAULT_PAGE_NAME"] = "Componente de caller ID";
$MESS["F_DEFAULT_MESSAGE_TYPE"] = "Forma de exibição  do modo de edição(resposta, editar novo tópico)";
$MESS["F_DEFAULT_FID"] = "ID do Fórum";
$MESS["F_SOCNET_GROUP_ID"] = "ID do grupo";
$MESS["F_DEFAULT_MID"] = "ID da mensagem";
$MESS["F_MESSAGE_TEMPLATE"] = "Página de exibição de mensagem";
$MESS["F_DEFAULT_PATH_TO_SMILE"] = "Caminho para a pasta Smiles (relativo raiz)";
$MESS["F_DEFAULT_PATH_TO_ICON"] = "Caminho para a pasta de ícones do tema (relativo raiz)";
$MESS["F_SMILE_TABLE_COLS"] = "Smileys por linha";
$MESS["F_DEFAULT_TID"] = "ID do tópico";
$MESS["F_LIST_TEMPLATE"] = "Página de lista de tópicos";
$MESS["F_BVARSFROMFORM"] = "Usar formulário de data";
$MESS["F_USER_ID"] = "ID de Usuário";
?>