<?
$MESS["F_TID_IS_LINK"] = "Apenas um tópico do link pode ser apagado.";
$MESS["FPF_CREATE"] = "Criar novo tópico";
$MESS["FPF_CREATE_IN_FORUM"] = "Criar novo tópico no fórum";
$MESS["FPF_EDIT"] = "Editar";
$MESS["F_NO_MODULE"] = "O módulo do fórum não foi instalado";
$MESS["F_GROUPS"] = "Grupos";
$MESS["FPF_GUEST"] = "Convidado";
$MESS["FPF_EDIT_FORM"] = "Modificar mensagem";
$MESS["FPF_NO_ICON"] = "nenhum ícone";
$MESS["FPF_SEND"] = "Novo tópico";
$MESS["FPF_REPLY"] = "Responder";
$MESS["FPF_REPLY_FORM"] = "Formulário de resposta";
$MESS["FORUM_SONET_MODULE_NOT_AVAIBLE"] = "O fórum não está disponível para esse usuário.";
$MESS["F_MID_IS_LOST_IN_OBJECT"] = "O post #SOCNET_OBJECT# não foi encontrado.";
$MESS["F_MID_IS_LOST"] = "O post não foi encontrado.";
$MESS["F_MID_IS_LOST_IN_FORUM"] = "O post não foi encontrado no fórum especificado.";
$MESS["F_FID_IS_EMPTY"] = "O fórum da rede social não foi especificado.";
$MESS["F_FID_IS_LOST"] = "O fórum da rede social não foi encontrado.";
$MESS["SONET_MODULE_NOT_INSTALL"] = "O módulo de rede social não está instalado.";
$MESS["F_TID_IS_LOST_IN_OBJECT"] = "O tópico #SOCNET_OBJECT# não foi encontrado.";
$MESS["F_TID_IS_LOST"] = "O tópico não foi encontrado.";
$MESS["F_TID_IS_LOST_IN_FORUM"] = "O tópico não foi encontrado no fórum especificado.";
$MESS["F_TID_IS_NOT_APPROVED"] = "O tema será visível depois que o moderador aprovar.";
$MESS["F_USERS"] = "Usuário";
$MESS["F_NO_NPERMS"] = "Você não tem permissões suficientes para criar um novo tópico no fórum.";
$MESS["F_NO_EPERMS"] = "Você não tem permissões suficientes para modificar esta mensagem.";
?>