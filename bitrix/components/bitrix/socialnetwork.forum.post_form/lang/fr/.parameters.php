<?
$MESS["F_DEFAULT_PAGE_NAME"] = "ID du composant d'appel";
$MESS["F_SOCNET_GROUP_ID"] = "Identifiant du groupe";
$MESS["F_USER_ID"] = "Identifiant de l'utilisateur";
$MESS["F_DEFAULT_MID"] = "Identifiant du message";
$MESS["F_DEFAULT_TID"] = "Code du sujet";
$MESS["F_DEFAULT_FID"] = "Forum ID";
$MESS["F_BVARSFROMFORM"] = "Utilisez avant chapelure";
$MESS["F_SMILE_TABLE_COLS"] = "Nombre de smileys dans une ligne";
$MESS["F_DEFAULT_PATH_TO_ICON"] = "Chemin par rapport à la racine du site vers le dossier avec les icônes et thèmes";
$MESS["F_DEFAULT_PATH_TO_SMILE"] = "Chemin par rapport à la racine du site vers le dossier avec des smileys";
$MESS["F_LIST_TEMPLATE"] = "Page de la liste des sujets";
$MESS["F_MESSAGE_TEMPLATE"] = "Page de lecture du message";
$MESS["F_DEFAULT_MESSAGE_TYPE"] = "Type de visualisation de la forme d'édition (réponse, édition, nouveau thème)";
?>