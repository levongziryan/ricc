<?
$MESS["CRM_CONFIG_PLG_TITLE"] = "Plug-ins CMS";
$MESS["CRM_CONFIG_PLG_DESC"] = "Connectez votre boutique en ligne à Bitrix24 pour qu'une nouvelle activité soit créée dès qu'une nouvelle commande apparaît dans la GRC. Transformez votre boutique en ligne en nouvelle source d'informations sur les ventes.";
$MESS["CRM_CONFIG_PLG_DESC2"] = "Utilisez Bitrix24 pour augmenter votre taux de conversion et gérer vos clients !";
$MESS["CRM_CONFIG_PLG_STEP1"] = "Cliquez #A1#ici#A2# pour télécharger et installer l'application sur votre site.";
$MESS["CRM_CONFIG_PLG_STEP2_1"] = "Créer et copier le lien";
$MESS["CRM_CONFIG_PLG_STEP2_2"] = "créer le lien";
$MESS["CRM_CONFIG_PLG_STEP2_3"] = "créer un nouveau lien";
$MESS["CRM_CONFIG_PLG_STEP2_4"] = "Vous aurez besoin du lien pour configurer votre boutique en ligne.";
$MESS["CRM_CONFIG_PLG_STEP3"] = "Collez le lien dans le champ de configuration de l'application de votre boutique en ligne et enregistrez les modifications.";
$MESS["CRM_CONFIG_PLG_STEP4"] = "Votre boutique en ligne est à présent connectée !";
$MESS["CRM_CONFIG_PLG_STEP4_2"] = "Chaque nouvelle commande sera maintenant enregistrée dans Bitrix24 en tant que nouvelle activité. Le client correspondant sera enregistré dans la GRC en tant que client.";
?>