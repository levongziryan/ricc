<?
$MESS["CRM_CONFIG_PLG_TITLE"] = "Web Store Tracker";
$MESS["CRM_CONFIG_PLG_DESC1"] = "Exporte la base de datos del cliente de su tienda web a Bitrix24 y disfrute de numerosas funciones útiles de administración de clientes.";
$MESS["CRM_CONFIG_PLG_DESC2"] = "Convierta las órdenes de la tienda Web en actividades Bitrix24 y cree perfiles de clientes.";
$MESS["CRM_CONFIG_PLG_DESC3"] = "Reactivar órdenes abandonadas utilizando herramientas de automatización Bitrix24.";
$MESS["CRM_CONFIG_PLG_DESC4"] = "Un cliente visitó su sitio pero nunca envió una orden? Bitrix24 capturará el evento, creará un prospecto y hará una llamada telefónica automatizada al cliente.";
$MESS["CRM_CONFIG_PLG_SELECT_CMS"] = "Seleccione la plataforma de su tienda web para conectarse a Bitrix24";
$MESS["CRM_CONFIG_PLG_SOON"] = "¡próximamente!";
?>