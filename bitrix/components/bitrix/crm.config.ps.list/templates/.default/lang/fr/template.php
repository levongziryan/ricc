<?
$MESS["CRM_ALL"] = "Total";
$MESS["CRM_PS_DELETE_CONFIRM"] = "Tes-vous sûr de vouloir supprimer '%s'?";
$MESS["CRM_PS_EDIT_TITLE"] = "Retour à la page d'édition du système de paiement";
$MESS["CRM_PS_EDIT"] = "Editer";
$MESS["CRM_PS_DELETE"] = "Supprimer";
$MESS["CRM_PS_DELETE_TITLE"] = "Elimination du système de paiement";
$MESS["CRM_PS_LIST_TITLE"] = "Systèmes de paiement";
$MESS["CRM_PS_LIST_PS_CREATE"] = "Créer un système de paiement";
$MESS["CRM_PS_LIST_PS_CREATE_DESC"] = "Cliquez sur la boîte pour créer un nouveau système de paiement";
$MESS["CRM_PS_LIST_PS_ACTIVE_ON"] = "Le système de paiement est actif";
$MESS["CRM_PS_LIST_PS_ACTIVE_OFF"] = "Le système de paiement est inactif";
$MESS["CRM_PS_LIST_PS_ACTIVE_BTN_ON"] = "on";
$MESS["CRM_PS_LIST_PS_ACTIVE_BTN_OFF"] = "off";
$MESS["CRM_PS_DELETE_CONFIRM_TITLE"] = "Voulez-vous vraiment supprimer ce système de paiement?";
?>