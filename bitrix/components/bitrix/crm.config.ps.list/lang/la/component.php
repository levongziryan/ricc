<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "El módulo e-Store no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_COLUMN_NAME"] = "Nombre";
$MESS["CRM_COLUMN_ACTIVE"] = "Activo";
$MESS["CRM_COLUMN_SORT"] = "Clasificar";
$MESS["CRM_COLUMN_PERSON_TYPE_NAME"] = "Tipo de cliente";
$MESS["CRM_PS_DELETION_GENERAL_ERROR"] = "Error al eliminar el sistema de pago.";
$MESS["CRM_PS_UPDATE_GENERAL_ERROR"] = "Error al actualizar el sistema de pago.";
$MESS["CRM_COMPANY_PT"] = "Compañía";
$MESS["CRM_CONTACT_PT"] = "Contacto";
$MESS["CRM_PS_DESC_COMMISSION_DEFAULT"] = "Sin comisión";
$MESS["CRM_PS_DESC_RESTRICTION_DEFAULT"] = "No hay restricción de cantidad de pago";
$MESS["CRM_PS_DESCRIPTION_COMMISSION_DEFAULT"] = "Utilizar para la factura";
$MESS["CRM_PS_DESCRIPTION_RESTRICTION_DEFAULT"] = "No hay límites para la cantidad de pago";
$MESS["CRM_PS_DESCRIPTION_RETURN_DEFAULT"] = "Límite de devolución dependen del método de pago";
$MESS["CRM_PS_DESCRIPTION_COMMISSION_DEFAULT_QUOTE"] = "Se utiliza para el formulario de impresión de cotización";
?>