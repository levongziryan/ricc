<?
$MESS["CRM_PS_REQUISITES_TRANSFER_NOT_REQUIRED_SUMMARY"] = "No se requieren detalles del vendedor para ser migrado de la opciones de pago a las compañías.";
$MESS["CRM_PS_REQUISITES_TRANSFER_COMPLETED_SUMMARY"] = "Migración lista de opciones de pago a las compañías.";
$MESS["CRM_PS_REQUISITES_TRANSFER_PROGRESS_SUMMARY1"] = "Agregado plantillas de detalles...";
$MESS["CRM_PS_REQUISITES_TRANSFER_PROGRESS_SUMMARY2"] = "Compañías y detalles creados...";
$MESS["CRM_PS_REQUISITES_TRANSFER_PROGRESS_SUMMARY3"] = "Facturas procesadas: #PROCESSED_ITEMS# of #TOTAL_ITEMS#...";
?>