<?
$MESS["CRM_PS_REQUISITES_TRANSFER_NOT_REQUIRED_SUMMARY"] = "Les informations du vendeur ne sont pas requises pour la migration des options de paiement vers les entreprises.";
$MESS["CRM_PS_REQUISITES_TRANSFER_COMPLETED_SUMMARY"] = "Fin de la migration des options de paiement vers les entreprises.";
$MESS["CRM_PS_REQUISITES_TRANSFER_PROGRESS_SUMMARY1"] = "Templates d'informations ajoutés...";
$MESS["CRM_PS_REQUISITES_TRANSFER_PROGRESS_SUMMARY2"] = "Entreprises et informations créées...";
$MESS["CRM_PS_REQUISITES_TRANSFER_PROGRESS_SUMMARY3"] = "Factures traitées: #PROCESSED_ITEMS# de #TOTAL_ITEMS#...";
?>