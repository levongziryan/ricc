<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado. ";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "O módulo de e-Store não está instalado. ";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_COLUMN_NAME"] = "Nome";
$MESS["CRM_COLUMN_ACTIVE"] = "Ativo";
$MESS["CRM_COLUMN_SORT"] = "Classificação";
$MESS["CRM_COLUMN_PERSON_TYPE_NAME"] = "Tipo de cliente";
$MESS["CRM_PS_DELETION_GENERAL_ERROR"] = "Erro ao deletar o sistema de pagamento.";
$MESS["CRM_PS_UPDATE_GENERAL_ERROR"] = "Erro ao atualizar o sistema de pagamento.";
$MESS["CRM_COMPANY_PT"] = "Empresa";
$MESS["CRM_CONTACT_PT"] = "Contato";
$MESS["CRM_PS_DESC_RETURN_DEFAULT"] = "Usado com formulário de fatura para impressão";
$MESS["CRM_PS_DESC_COMMISSION_DEFAULT"] = "Sem comissão";
$MESS["CRM_PS_DESC_RESTRICTION_DEFAULT"] = "Sem restrição de valor do pagamento";
$MESS["CRM_PS_DESCRIPTION_RETURN_DEFAULT"] = "Limitações de retorno dependem do método de pagamento";
$MESS["CRM_PS_DESCRIPTION_COMMISSION_DEFAULT"] = "Utilizado para fatura";
$MESS["CRM_PS_DESCRIPTION_RESTRICTION_DEFAULT"] = "Sem limites para valor do pagamento";
$MESS["CRM_PS_DESCRIPTION_COMMISSION_DEFAULT_QUOTE"] = "Usado para formulário de impressão de cotação";
?>