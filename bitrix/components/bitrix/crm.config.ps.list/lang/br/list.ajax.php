<?
$MESS["CRM_PS_REQUISITES_TRANSFER_NOT_REQUIRED_SUMMARY"] = "As informações do vendedor não precisam ser migradas das opções de pagamento para empresas.";
$MESS["CRM_PS_REQUISITES_TRANSFER_COMPLETED_SUMMARY"] = "Opções de pagamento migradas realizadas para empresas.";
$MESS["CRM_PS_REQUISITES_TRANSFER_PROGRESS_SUMMARY1"] = "Modelos de informações adicionados...";
$MESS["CRM_PS_REQUISITES_TRANSFER_PROGRESS_SUMMARY2"] = "Empresas e informações criadas...";
$MESS["CRM_PS_REQUISITES_TRANSFER_PROGRESS_SUMMARY3"] = "Faturas processadas: #PROCESSED_ITEMS# de #TOTAL_ITEMS#...";
?>