<?
$MESS["CRM_CALENDAR_TOPIC"] = "Asunto";
$MESS["CRM_CALENDAR_DATE"] = "Fecha y hora";
$MESS["CRM_CALENDAR_ADD_BUTTON"] = "Agregar";
$MESS["CRM_CALENDAR_ADD_TITLE"] = "Agregar llamada o evento al calendario";
$MESS["CRM_CALENDAR_DESC_TITLE"] = "Ingresar comentario";
$MESS["CRM_CALENDAR_DESC"] = "Descripción";
$MESS["CRM_CALENDAR_REMIND"] = "Recordar el evento";
$MESS["CRM_CALENDAR_REMIND_FROM"] = "en";
$MESS["BX_CRM_CACA_REM_MIN"] = "minutos";
$MESS["BX_CRM_CACA_REM_HOUR"] = "horas";
$MESS["BX_CRM_CACA_REM_DAY"] = "días";
$MESS["BX_CRM_CACA_PRIORITY"] = "Prioridad de evento";
$MESS["BX_CRM_CACA_PRIORITY_HIGH"] = "Alta";
$MESS["BX_CRM_CACA_PRIORITY_NORMAL"] = "Normal";
$MESS["BX_CRM_CACA_PRIORITY_LOW"] = "baja";
?>