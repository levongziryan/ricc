<?
$MESS["CRM_CALENDAR_TOPIC"] = "Assunto";
$MESS["CRM_CALENDAR_DATE"] = "Data e hora";
$MESS["CRM_CALENDAR_ADD_BUTTON"] = "Adicionar";
$MESS["CRM_CALENDAR_ADD_TITLE"] = "Adicionar chamada ou evento ao calendário";
$MESS["CRM_CALENDAR_DESC_TITLE"] = "Insira o seu comentário";
$MESS["CRM_CALENDAR_DESC"] = "Descrição";
$MESS["CRM_CALENDAR_REMIND"] = "Lembre de evento";
$MESS["CRM_CALENDAR_REMIND_FROM"] = "em";
$MESS["BX_CRM_CACA_REM_MIN"] = "minutos";
$MESS["BX_CRM_CACA_REM_HOUR"] = "horas";
$MESS["BX_CRM_CACA_REM_DAY"] = "dias";
$MESS["BX_CRM_CACA_PRIORITY"] = "Prioridade do evento";
$MESS["BX_CRM_CACA_PRIORITY_HIGH"] = "Alto";
$MESS["BX_CRM_CACA_PRIORITY_NORMAL"] = "Normal";
$MESS["BX_CRM_CACA_PRIORITY_LOW"] = "Baixo";
?>