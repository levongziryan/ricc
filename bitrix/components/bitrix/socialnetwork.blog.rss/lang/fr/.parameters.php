<?
$MESS["BR_GROUP_ID"] = "Identificateur du groupe du blogue";
$MESS["B_VARIABLE_ALIASES"] = "Alias des variables";
$MESS["BR_BLOG_VAR"] = "Nom de la variable pour l'identificateur de l'utilisateur";
$MESS["BR_USER_VAR"] = "Nom de la variable pour l'identificateur de l'utilisateur";
$MESS["BR_POST_VAR"] = "Le nom de la variable pour l'identificateur du message du blog";
$MESS["BR_PAGE_VAR"] = "Nom de la variable pour la page";
$MESS["BR_NUM_POSTS"] = "Quantité de messages";
$MESS["BR_TYPE"] = "Format RSS";
$MESS["BR_PATH_TO_USER"] = "Modèle de chemin d'accès à la page de l'utilisateur du blog";
$MESS["BR_PATH_TO_POST"] = "Modèle de chemin d'accès à la page avec un message";
$MESS["BR_PATH_TO_GROUP_POST"] = "Modèle de chemin d'accès à la page avec un message de blog du groupe";
$MESS["BR_PATH_TO_BLOG"] = "Modèle de chemin d'accès à la page du blog";
?>