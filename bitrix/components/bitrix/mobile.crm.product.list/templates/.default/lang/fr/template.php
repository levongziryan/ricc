<?
$MESS["M_CRM_PRODUCT_LIST_BUTTON_UP"] = "Déplacer vers le haut";
$MESS["M_CRM_PRODUCT_LIST_SHOW_SECTION"] = "Tous les produits de la catégorie";
$MESS["M_CRM_PRODUCT_LIST_SELECTOR_LEGEND"] = "Choisir le produit";
$MESS["M_CRM_PRODUCT_LIST_TAB_SECTION"] = "Catégories";
$MESS["M_CRM_PRODUCT_LIST_ROOT_SECTION_LEGEND"] = "Catégorie racine";
$MESS["M_CRM_PRODUCT_LIST_LOAD_TEXT"] = "Mise à jour...";
$MESS["M_CRM_PRODUCT_LIST_DOWN_TEXT"] = "Relâcher pour mettre à jour...";
$MESS["M_CRM_PRODUCT_LIST_SEARCH_BUTTON"] = "Recherche";
$MESS["M_CRM_PRODUCT_LIST_SEARCH_PLACEHOLDER"] = "Recherche par la dénomination";
$MESS["M_CRM_PRODUCT_LIST_PULL_TEXT"] = "Tirer pour mettre à jour...";
$MESS["M_CRM_PRODUCT_LIST_TAB_PRODUCT"] = "investissements dans les marchandises";
$MESS["M_CRM_PRODUCT_LIST_TITLE"] = "Tous les produits";
$MESS["M_CRM_PRODUCT_ADD"] = "Créer un produit";
?>