<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["M_CRM_PRODUCT_LIST_PRESET_USER"] = "Filtro personalizado";
$MESS["M_CRM_PRODUCT_LIST_FILTER_NONE"] = "Todos os produtos";
$MESS["M_CRM_PRODUCT_LIST_EDIT"] = "Editar";
$MESS["M_CRM_PRODUCT_LIST_DELETE"] = "Excluir";
?>