<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["M_CRM_PRODUCT_LIST_PRESET_USER"] = "Filtro personalizado";
$MESS["M_CRM_PRODUCT_LIST_FILTER_NONE"] = "Todos los productos";
$MESS["M_CRM_PRODUCT_LIST_EDIT"] = "Editar";
$MESS["M_CRM_PRODUCT_LIST_DELETE"] = "Eliminar";
?>