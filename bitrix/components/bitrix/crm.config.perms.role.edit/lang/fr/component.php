<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_ENTITY_TYPE_LEAD"] = "Prospect";
$MESS["CRM_ENTITY_TYPE_CONTACT"] = "Client";
$MESS["CRM_ENTITY_TYPE_COMPANY"] = "Entreprise";
$MESS["CRM_ENTITY_TYPE_DEAL"] = "Affaire";
$MESS["CRM_PERMS_TYPE_"] = "Accès interdit.";
$MESS["CRM_PERMS_TYPE_X"] = "Tous";
$MESS["CRM_PERMS_TYPE_A"] = "Personel";
$MESS["CRM_PERMS_TYPE_D"] = "Personnel et service";
$MESS["CRM_PERMS_TYPE_F"] = "Personnel, service et sous-départements";
$MESS["CRM_PERMS_TYPE_O"] = "Tous ouverts";
$MESS["CRM_PERMS_ENTITY_LIST"] = "Droits d'accès";
$MESS["CRM_PERMS_ROLE_EDIT"] = "Gestion du rôle";
$MESS["CRM_ENTITY_TYPE_INVOICE"] = "Facture";
$MESS["CRM_ENTITY_TYPE_QUOTE"] = "Offre";
$MESS["CRM_ENTITY_TYPE_WEBFORM"] = "Formulaire CRM";
$MESS["CRM_ENTITY_TYPE_BUTTON"] = "Widget du site";
?>