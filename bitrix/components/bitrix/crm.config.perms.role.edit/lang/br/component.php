<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_ENTITY_TYPE_LEAD"] = "Lead";
$MESS["CRM_ENTITY_TYPE_CONTACT"] = "Contato";
$MESS["CRM_ENTITY_TYPE_COMPANY"] = "Empresa";
$MESS["CRM_ENTITY_TYPE_DEAL"] = "Negócio";
$MESS["CRM_PERMS_TYPE_"] = "Acesso negado.";
$MESS["CRM_PERMS_TYPE_X"] = "Todos";
$MESS["CRM_PERMS_TYPE_A"] = "Pessoal";
$MESS["CRM_PERMS_TYPE_D"] = "Pessoal e departamento";
$MESS["CRM_PERMS_TYPE_F"] = "Pessoal, departamento e subdepartamentos";
$MESS["CRM_PERMS_TYPE_O"] = "Tudo aberto";
$MESS["CRM_PERMS_ENTITY_LIST"] = "Permissões de acesso";
$MESS["CRM_PERMS_ROLE_EDIT"] = "Gerenciar funções";
$MESS["CRM_ENTITY_TYPE_INVOICE"] = "Fatura";
$MESS["CRM_ENTITY_TYPE_QUOTE"] = "Orçamento";
$MESS["CRM_ENTITY_TYPE_WEBFORM"] = "Formulário de CRM";
$MESS["CRM_ENTITY_TYPE_BUTTON"] = "Widget do site";
?>