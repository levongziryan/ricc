<?
$MESS["TASKS_TT_TASKS_MODULE_NOT_INSTALLED"] = "O módulo de Tarefas não está instalado. ";
$MESS["TASKS_TT_SOCIALNETWORK_MODULE_NOT_INSTALLED"] = "O módulo Rede Social não está instalado.";
$MESS["TASKS_TT_FORUM_MODULE_NOT_INSTALLED"] = "O módulo de Fórum não está instalado.";
$MESS["TASKS_TL_SOCIALNETWORK_MODULE_NOT_INSTALLED"] = "O módulo \"Rede Social\" não está instalado.";
$MESS["TASKS_TL_FORUM_MODULE_NOT_INSTALLED"] = "O módulo \"Fórum\" não está instalado.";
$MESS["TASKS_TL_ACCESS_TO_GROUP_DENIED"] = "Você não pode ver a lista de tarefas para este grupo.";
$MESS["TASKS_TL_TITLE_TASKS"] = "Tarefas";
$MESS["TASKS_GROUP_ACTION_DAYS_NUM_INVALID_TITLE"] = "Entrada inválida";
$MESS["TASKS_GROUP_ACTION_DAYS_NUM_INVALID_TEXT"] = "Este campo pode conter apenas números.";
?>