<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado. ";
$MESS["CRM_MODULE_NOT_INSTALLED_SALE"] = "O módulo de e-Store não está instalado. ";
$MESS["CRM_MODULE_NOT_INSTALLED_CATALOG"] = "O módulo de Catálogo Comercial não está instalado. ";
$MESS["CRM_UNSUPPORTED_OWNER_TYPE"] = "Tipo de proprietário não suportado: \"#OWNER_TYPE#\".";
$MESS["CRM_CURRENCY_IS_NOT_FOUND"] = "Não foi possível encontrar  ID da moeda = #CURRENCY_ID#.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado.";
$MESS["CRM_FIELD_NAME"] = "Nome";
$MESS["CRM_FIELD_DESCRIPTION"] = "Descrição";
$MESS["CRM_FIELD_ACTIVE"] = "Ativo";
$MESS["CRM_FIELD_CURRENCY"] = "Moeda";
$MESS["CRM_FIELD_PRICE"] = "Preço";
$MESS["CRM_FIELD_VAT_ID"] = "Taxa de IVA";
$MESS["CRM_FIELD_VAT_INCLUDED"] = "IVA incluído";
$MESS["CRM_FIELD_SECTION"] = "Seção";
$MESS["CRM_FIELD_SORT"] = "Tipo";
$MESS["CRM_PRODUCT_CREATE"] = "Criar novos produtos";
$MESS["CRM_PRODUCT_CREATE_WAIT"] = "Criando o produto ...";
$MESS["CRM_PRODUCT_CREATE_AJAX_ERR"] = "Erro ao processar o novo pedido do produto <br>";
$MESS["CRM_BUTTON_CREATE_TITLE"] = "Criar";
$MESS["CRM_BUTTON_CANCEL_TITLE"] = "Cancelar";
?>