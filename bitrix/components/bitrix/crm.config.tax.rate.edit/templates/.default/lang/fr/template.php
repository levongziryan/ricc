<?
$MESS["CRM_TAXRATE_FIELDS_ACTIVE"] = "Actif(ve)";
$MESS["CRM_ANY"] = "Tous";
$MESS["CRM_TAXRATE_FIELDS_IS_IN_PRICE"] = "Inclure la TVA au prix";
$MESS["CRM_TAXRATE_FIELDS_LOCATION2"] = "Groupe de sites";
$MESS["CRM_TAXRATE_TITLE"] = "Ajouter le taux d'imposition";
$MESS["CRM_TAXRATE_TITLE_EDIT"] = "Changement du Taux d'impôt";
$MESS["CRM_TAXRATE_FIELDS_LOCATION1"] = "Emplacements";
$MESS["CRM_TAXRATE_FIELDS_TAX"] = "Impôt";
$MESS["CRM_TAXRATE_FIELDS_APPLY_ORDER"] = "Modalités d'application";
$MESS["CRM_TAXRATE_SAVE_BUTTON"] = "Sauvegarder";
$MESS["CRM_TAXRATE_FIELDS_VALUE"] = "Coefficient";
$MESS["CRM_TAXRATE_FIELDS_VALUE_CHECK"] = "Le taux d'impôt peut être uniquement en chiffres";
$MESS["CRM_TAXRATE_FIELDS_PERSON_TYPE_ID"] = "Type du client";
?>