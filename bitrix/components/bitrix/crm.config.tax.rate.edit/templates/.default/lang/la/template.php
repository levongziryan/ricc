<?
$MESS["CRM_TAXRATE_SAVE_BUTTON"] = "Guardar";
$MESS["CRM_TAXRATE_TITLE"] = "Agregar tasa de impuestos";
$MESS["CRM_TAXRATE_FIELDS_TAX"] = "Impuesto";
$MESS["CRM_TAXRATE_FIELDS_ACTIVE"] = "Activo";
$MESS["CRM_TAXRATE_FIELDS_PERSON_TYPE_ID"] = "Tipo de cliente";
$MESS["CRM_ANY"] = "Todo";
$MESS["CRM_TAXRATE_FIELDS_VALUE"] = "Tasa";
$MESS["CRM_TAXRATE_FIELDS_IS_IN_PRICE"] = "Incluir impuesto en el precio";
$MESS["CRM_TAXRATE_FIELDS_APPLY_ORDER"] = "Orden de aplicación";
$MESS["CRM_TAXRATE_FIELDS_LOCATION1"] = "Ubicación";
$MESS["CRM_TAXRATE_FIELDS_LOCATION2"] = "Grupos de ubicación";
$MESS["CRM_TAXRATE_FIELDS_VALUE_CHECK"] = "La tasa de impuesto sólo puede ser un número.";
$MESS["CRM_TAXRATE_TITLE_EDIT"] = "Editar las tasas de impuestos";
?>