<?
$MESS["CRM_EVENT_DEFAULT_TITLE"] = "Não há dados.";
$MESS["CRM_ERROR_EDIT_TAX_RATE"] = "Erro ao atualizar a taxa de imposto.";
$MESS["CRM_ERROR_ADD_TAX_RATE"] = "Erro ao adicionar a taxa de imposto.";
$MESS["CRM_ERROR_NO_LOCATION"] = "Erro: não existem locais.";
?>