<?
$MESS["M_PARAM_list"] = "Plantilla a la ruta de la página de reuniones";
$MESS["M_PARAM_meeting"] = "Plantilla a la ruta de la página de vista de reuniones";
$MESS["M_PARAM_meeting_edit"] = "Plantilla a la ruta de la edición de reuniones";
$MESS["M_PARAM_meeting_copy"] = "Plantilla a la ruta de la página de creación de reuniones";
$MESS["M_PARAM_meeting_item"] = "Plantilla a la ruta de la página de detalles de temas de reuniones";
$MESS["INTL_IBLOCK_TYPE"] = "Tipo de block de información del salón de reuniones";
$MESS["INTL_IBLOCK"] = "Block de información del salón de reuniones";
$MESS["INTL_IBLOCK_TYPE_V"] = "Tipo de block de información del salón de video de las reuniones";
$MESS["INTL_IBLOCK_V"] = "Block de información del salón de video de reuniones";
$MESS["M_MEETINGS_COUNT"] = "Reuniones por página";
$MESS["M_NAME_TEMPLATE"] = "Mostrar nombre de plantilla";
?>