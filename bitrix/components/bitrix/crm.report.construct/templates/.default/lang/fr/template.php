<?
$MESS["REPORT_POPUP_COLUMN_TITLE"] = "Affaire";
$MESS["REPORT_CHOOSE"] = "Choisir";
$MESS["CRM_FF_LEAD"] = "Prospects";
$MESS["CRM_FF_CONTACT"] = "Contacts";
$MESS["CRM_FF_COMPANY"] = "Entreprises";
$MESS["CRM_FF_DEAL"] = "Transactions";
$MESS["CRM_FF_OK"] = "Choisir";
$MESS["CRM_FF_CANCEL"] = "Annuler";
$MESS["CRM_FF_CLOSE"] = "Fermer";
$MESS["CRM_FF_SEARCH"] = "Recherche";
$MESS["CRM_FF_NO_RESULT"] = "Malheureusement, il n'y a pas d'éléments retrouvés d'après votre demande de recherche.";
$MESS["CRM_FF_CHOISE"] = "Choisir";
$MESS["CRM_FF_CHANGE"] = "Editer";
$MESS["CRM_FF_LAST"] = "Dernier";
$MESS["REPORT_IGNORE_FILTER_VALUE"] = "Ignorer";
$MESS["CRM_REPORT_INCLUDE_ALL"] = "Tous";
$MESS["REPORT_POPUP_COLUMN_TITLE_CRM"] = "Affaire";
$MESS["REPORT_POPUP_COLUMN_TITLE_CRM_PRODUCT_ROW"] = "Article";
$MESS["CRM_REPORT_SELECT_OWNER"] = "Veuillez sélectionner le jeu de champs pour le nouveau rapport";
$MESS["CRM_REPORT_CONSTRUCT_BUTTON_CANCEL"] = "Annuler";
$MESS["CRM_REPORT_CONSTRUCT_BUTTON_CONTINUE"] = "Plus loin...";
?>