<?
$MESS["REQUISITE_ADD"] = "Ajouter détails";
$MESS["REQUISITE_ADD_TITLE"] = "Ajouter détails";
$MESS["REQUISITE_COPY"] = "Copier";
$MESS["REQUISITE_COPY_TITLE"] = "Copier détails";
$MESS["REQUISITE_DELETE"] = "Supprimer détails";
$MESS["REQUISITE_DELETE_TITLE"] = "Supprimer détails";
$MESS["REQUISITE_DELETE_DLG_TITLE"] = "Supprimer détails";
$MESS["REQUISITE_DELETE_DLG_MESSAGE"] = "Voulez-vous vraiment supprimer cet élément ?";
$MESS["REQUISITE_DELETE_DLG_BTNTITLE"] = "Supprimer";
?>