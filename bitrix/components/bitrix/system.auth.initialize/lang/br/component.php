<?
$MESS["CC_MAIN_REG_INIT_MESSAGE_NAME_EMPTY"] = "Primeiro nome está vazio.";
$MESS["CC_MAIN_REG_INIT_MESSAGE_LAST_NAME_EMPTY"] = "Sobrenome está vazio.";
$MESS["CC_MAIN_REG_INIT_MESSAGE_AUTHORIZED"] = "Como você já está autorizado, você não pode confirmar o seu convite, por favor use a sua autorização existente.";
$MESS["CC_MAIN_REG_INIT_MESSAGE_CHECKWORD_WRONG"] = "A palavra-chave está incorreta.";
$MESS["CC_MAIN_REG_INIT_MESSAGE_CHECKWORD_EMPTY"] = "A palavra-chave é necessária.";
$MESS["CC_MAIN_REG_INIT_MESSAGE_PASSWORD_NOT_CONFIRMED"] = "A confirmação de senha não corresponde à senha.";
$MESS["CC_MAIN_REG_INIT_MESSAGE_PASSWORD_EMPTY"] = "A senha não está especificado.";
$MESS["CC_MAIN_REG_INIT_MESSAGE_AUTH_SUCCESS"] = "O registro do perfil já foi confirmado.";
$MESS["CC_MAIN_REG_INIT_MESSAGE_INACTIVE"] = "O usuário está inativo.";
$MESS["CC_MAIN_REG_INIT_MESSAGE_NO_USER"] = "O usuário não foi encontrado.";
?>