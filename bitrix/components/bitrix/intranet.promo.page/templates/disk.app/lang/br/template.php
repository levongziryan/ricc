<?
$MESS["INTRANET_DISK_PROMO_TITLE_MACOS"] = "App Desktop Bitrix24 para MacOS";
$MESS["INTRANET_DISK_PROMO_TITLE_WINDOWS"] = "App Desktop Bitrix24 para Windows";
$MESS["INTRANET_DISK_PROMO_HEADER"] = "Equipe seu Bitrix24 com ainda mais recursos de gerenciamento de arquivo.";
$MESS["INTRANET_DISK_PROMO_DESC"] = "Hoje, podemos acessar a Internet em praticamente qualquer lugar, a qualquer hora. E, ainda, podemos encontrar situações em que o acesso ao armazenamento de arquivos online está indisponível: por exemplo, em um avião. Isso não significa que você tenha que interromper o seu trabalho! O aplicativo da área de trabalho Bitrix24.Drive irá sincronizar todos os arquivos com cópias locais no seu computador. Edite seus arquivos offline - todas as alterações serão sincronizadas automaticamente com a Nuvem assim que a conexão à Internet estiver disponível.";
$MESS["INTRANET_DISK_PROMO_DESC_SUB"] = "Instale o aplicativo no seu computador e continue seu trabalho a qualquer momento, em qualquer lugar!";
$MESS["INTRANET_DISK_PROMO_STEP1_TITLE"] = "Baixe e instale o aplicativo #LINK_START#Bitrix24#LINK_END#";
$MESS["INTRANET_DISK_PROMO_STEP2_TITLE"] = "Conectar Bitrix24.Drive";
$MESS["INTRANET_DISK_PROMO_STEP2_DESC"] = "Execute o aplicativo para sincronizar seus arquivos em modo autônomo. Você pode especificamente selecionar as pastas que estarão disponíveis localmente ou no navegador.";
?>