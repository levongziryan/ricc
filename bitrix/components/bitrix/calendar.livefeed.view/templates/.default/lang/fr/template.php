<?
$MESS["ECLFV_CRM"] = "CRM";
$MESS["ECLFV_EVENT_DECLINED"] = "Vous ne participez pas à cet événement";
$MESS["ECLFV_EVENT_ACCEPTED"] = "Vous participez à cet événement";
$MESS["ECLFV_EVENT_LOCATION"] = "Adresse domicile";
$MESS["ECLFV_EVENT_NAME"] = "Le nom de l'événement";
$MESS["ECLFV_EVENT_START"] = "Date et l'heure du début";
$MESS["ECLFV_DESCRIPTION"] = "Description de l'événement";
$MESS["ECLFV_EVENT_ATTENDEES_DES"] = "Refusé(e)s";
$MESS["ECLFV_INVITE_DECLINE"] = "Refuser";
$MESS["ECLFV_INVITE_DECLINE2"] = "Refuser";
$MESS["ECLFV_EVENT_ATTENDEES"] = "Ils ont confirmé la participation";
$MESS["ECLFV_INVITE_ACCEPT"] = "Confirmer la participation";
$MESS["ECLFV_EVENT"] = "Evènement";
$MESS["ECLFV_INVITE_ACCEPT2"] = "Confirmer la participation";
$MESS["ECLFV_REC_DECLINE"] = "Voulez-vous refuser de participer à l’événement périodique ?";
$MESS["ECLFV_REC_DECLINE_THIS"] = "Seulement cet événement";
$MESS["ECLFV_REC_DECLINE_NEXT"] = "Cet évènement et tous les évènements suivants";
$MESS["ECLFV_REC_DECLINE_ALL"] = "Tous les évènements";
?>