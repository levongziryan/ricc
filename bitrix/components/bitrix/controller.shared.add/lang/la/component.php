<?
$MESS["CSA_TITLE"] = "Agregando un sitio";
$MESS["CSA_MODULE_NOT_INSTALLED"] = "El modulo de Controlador del sitio no esta instalado";
$MESS["CSA_ERROR_DOMAIN_NAME"] = "Los nombre de dominios pueden incluir solo letras, digitos, puntos y guiones.";
$MESS["CSA_ERROR_DIR_PERMISSIONS"] = "Permisos incorrectos asignados a los directorios de los sitios de alquiler.";
$MESS["CSA_ERROR_FILE_PERMISSIONS"] = "Permisos incorrectos asignados a los archivos de los sitios rentados.";
$MESS["CSA_ERROR_MEMORY_LIMIT"] = "Incorrecto limite de memoria asignado a los sitios rentados.";
$MESS["CSA_ERROR_NAME_EXISTS"] = "El nombre especificado ya esta en uso. Por favor elija un nombre diferente.";
$MESS["CSA_ERROR_BAD_MYSQL_PATH"] = "Parámetro incorrecto: La Ruta al archivo ejecutable del MySQL.";
$MESS["CSA_ERROR_DB_FILEDUMP"] = "La base de datos inicial de almacenamiento de archivos no existe o no puede ser leída debido a la falta de permisos de lectura.  ";
$MESS["CSA_ERROR_DB_CONNECT"] = "Error conectando a la base de datos: #ERROR#";
$MESS["CSA_ERROR_DB_CREATE"] = "Error creando la base de datos (#DB_NAME#): #ERROR#";
$MESS["CSA_ERROR_DB_IMPORT"] = "Error importando la base de datos (#DB_NAME#).";
$MESS["CSA_ERROR_ADD_SITE"] = "Error registrando el sitio en el controlador.";
$MESS["CSA_ERROR_ADD_SITE2"] = "Error registrando el sitio en el controlador: #ERROR#";
$MESS["CSA_LOG_ADD_CLIENT"] = "Agregando clientes a través de la creación directa";
$MESS["CSA_ERROR_CREATE_DIR"] = "Error creando el directorio del sitio.";
$MESS["CSA_ERROR_FILE_WRITE"] = "Error escribiendo a #FILE#.";
$MESS["CSA_ERROR_NOT_FOUND_APACHE_VHOST_DIR"] = "El catalogo con los archivos de configuración de apache no existe o no puedes ser escrito.";
$MESS["CSA_ERROR_APACHE_TEMPLATE_NOT_FOUND"] = "La plantilla de configuración de Apache no puede ser encontrada (#FILE#).";
$MESS["CSA_ERROR_WRITE_APACHE_CONFIG"] = "Error escribiendo al archivo de configuración de apache.";
$MESS["CSA_ERROR_NGINX_TEMPLATE_NOT_FOUND"] = "La plantilla de configuración de nginx no puede ser encontrada (#FILE#).";
$MESS["CSA_ERROR_NOT_FOUND_NGINX_VHOST_DIR"] = "El catalogo con los archivos de configuración de nginx no existe o no puede ser escrito.";
$MESS["CSA_ERROR_WRITE_NGINX_CONFIG"] = "Error escribiendo al archivo de configuración de nginx.";
?>