<?
$MESS["CSA_PARAM_CONTROLLER_URL"] = "Dirección URL del controlador";
$MESS["CSA_PARAM_URL_SUBDOMAIN"] = "dominio de nivel superior";
$MESS["CSA_PARAM_PATH_VHOST"] = "Ruta completa a la carpeta con sitios alquilados";
$MESS["CSA_PARAM_MYSQL_PATH"] = "Ruta al archivo ejecutable del MySQL";
$MESS["CSA_PARAM_MYSQL_USER"] = "Nombre de usuario de MySQL con privilegios de creación de base de datos";
$MESS["CSA_PARAM_MYSQL_PASSWORD"] = "Contraseña de usuario de MySQL con privilegios de creación de base de datos";
$MESS["CSA_PARAM_MYSQL_DB_PATH"] = "Ruta a la base de datos inicial de almacenamiento de archivos";
$MESS["CSA_PARAM_DIR_PERMISSIONS"] = "Permisos para carpetas que contienen sitios alquilados";
$MESS["CSA_PARAM_FILE_PERMISSIONS"] = "Permisos para carpetas de sitios rentados";
$MESS["CSA_PARAM_PATH_PUBLIC"] = "Publicar archivos de sitios rentados";
$MESS["CSA_PARAM_MEMORY_LIMIT"] = "Limite de memoria para sitios rentados";
$MESS["CSA_PARAM_APACHE_ROOT"] = "Catálogo que contiene los archivos de configuración de Apache de los sitios virtuales";
$MESS["CSA_PARAM_NGINX_ROOT"] = "Catalogo que contiene los archivos de configuración de nginx de los sitios virtuales";
$MESS["CSA_PARAM_RELOAD_FILE"] = "Marcar archivo para provocar el reinicio del servidor web";
$MESS["CSA_PARAM_REGISTER_IMMEDIATE"] = "Registro del sitio sobre la creación";
?>