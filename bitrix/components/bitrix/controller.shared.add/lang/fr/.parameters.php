<?
$MESS["CSA_PARAM_CONTROLLER_URL"] = "URL du contrôleur des sites";
$MESS["CSA_PARAM_URL_SUBDOMAIN"] = "Domaine du niveau haut";
$MESS["CSA_PARAM_APACHE_ROOT"] = "Répertoire des fichiers de configuration des sites virtuels pour Apache";
$MESS["CSA_PARAM_NGINX_ROOT"] = "La catalogue des fichiers de configuration des sites virtuels pour nginx";
$MESS["CSA_PARAM_MYSQL_USER"] = "Nom de l'utilisateur MySQL avec privilège de création des bases de données";
$MESS["CSA_PARAM_MEMORY_LIMIT"] = "Limite de mémoire pour les sites loués";
$MESS["CSA_PARAM_MYSQL_PASSWORD"] = "Mot de passe de l'utilisateur MySQL avec privilège de création des bases de données";
$MESS["CSA_PARAM_PATH_VHOST"] = "Taille réelle";
$MESS["CSA_PARAM_DIR_PERMISSIONS"] = "Droits pour les répertoires des sites loués";
$MESS["CSA_PARAM_FILE_PERMISSIONS"] = "Droits des fichiers pour des sites en collocation";
$MESS["CSA_PARAM_MYSQL_PATH"] = "Chemin vers le fichier MySQL à exécuter";
$MESS["CSA_PARAM_MYSQL_DB_PATH"] = "Chemin vers le fichier avec un vidage initial de la base de données";
$MESS["CSA_PARAM_REGISTER_IMMEDIATE"] = "Enregistrer le site aussitôt au moment de sa création";
$MESS["CSA_PARAM_RELOAD_FILE"] = "Fichier drapeau pour rechargement du serveur WEB";
$MESS["CSA_PARAM_PATH_PUBLIC"] = "Fichiers de la partie publique des sites en location";
?>