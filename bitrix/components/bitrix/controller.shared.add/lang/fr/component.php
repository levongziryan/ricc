<?
$MESS["CSA_ERROR_NAME_EXISTS"] = "Le nom saisi existe déjà,  veuillez en choisir un autre, s'il vous plaît.";
$MESS["CSA_TITLE"] = "Ajout d'un site web";
$MESS["CSA_ERROR_DOMAIN_NAME"] = "Le nom du domaine peut contenir uniquement des caractères de l'alphabet latin, des chiffres et des traits d'union.";
$MESS["CSA_ERROR_NOT_FOUND_APACHE_VHOST_DIR"] = "Le catalogue des fichiers de configuration de sites virtuels Apache n'existe pas ou n'est pas disponible pour la sauvegarde.";
$MESS["CSA_ERROR_NOT_FOUND_NGINX_VHOST_DIR"] = "Le catalogue de fichiers configurés pour les sites virtuels nginx n'existe pas ou n'est pas disponible pour la sauvegarde.";
$MESS["CSA_MODULE_NOT_INSTALLED"] = "Le module du contrôleur non installé";
$MESS["CSA_ERROR_APACHE_TEMPLATE_NOT_FOUND"] = "Modèle de configuration apache (# FILE#) introuvable.";
$MESS["CSA_ERROR_NGINX_TEMPLATE_NOT_FOUND"] = "Modèle de configuration nginx (#FILE#) est introuvable.";
$MESS["CSA_ERROR_MEMORY_LIMIT"] = "Restriction de mémoire pour des sites en location est incorrect.";
$MESS["CSA_ERROR_DIR_PERMISSIONS"] = "Les droits pour les catalogues des sites en location sont indiqués incorrectement.";
$MESS["CSA_ERROR_FILE_PERMISSIONS"] = "Droits incorrects pour les fichiers de sites loués.";
$MESS["CSA_ERROR_BAD_MYSQL_PATH"] = "Paramètre incorrect: Path to MySQL fichier exécutable.";
$MESS["CSA_ERROR_WRITE_APACHE_CONFIG"] = "Erreur d'enregistrement du fichier de configuration apache.";
$MESS["CSA_ERROR_WRITE_NGINX_CONFIG"] = "Erreur d'enregistrement du fichier de configuration nginx.";
$MESS["CSA_ERROR_FILE_WRITE"] = "Erreur d'enregistrement du fichier #FILE#.";
$MESS["CSA_ERROR_DB_CONNECT"] = "Erreur de connexion à la base de données: #ERROR#";
$MESS["CSA_ERROR_DB_IMPORT"] = "Erreur lors de l'importation de la base de données (#DB_NAME#).";
$MESS["CSA_ERROR_ADD_SITE"] = "Erreur d'inscription du site dans le contrôleur.";
$MESS["CSA_ERROR_ADD_SITE2"] = "Erreur d'enrégistrement du site dans le contrôleur: #ERROR#";
$MESS["CSA_ERROR_DB_CREATE"] = "Erreur de création de la base de données (#DB_NAME#): #ERROR#";
$MESS["CSA_ERROR_CREATE_DIR"] = "Erreur de création d'un catalogue pour le site.";
$MESS["CSA_LOG_ADD_CLIENT"] = "Connexion du client par création directe";
$MESS["CSA_ERROR_DB_FILEDUMP"] = "Le fichier avec le vidage initial des bases de données n'existe pas ou vous ne disposez pas des droits pour le lire.";
?>