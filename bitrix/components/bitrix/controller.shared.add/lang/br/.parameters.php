<?
$MESS["CSA_PARAM_CONTROLLER_URL"] = "URL controlador";
$MESS["CSA_PARAM_URL_SUBDOMAIN"] = "Domínio de nível superior";
$MESS["CSA_PARAM_PATH_VHOST"] = "Caminho completo para a pasta com os sites alugados";
$MESS["CSA_PARAM_MYSQL_PATH"] = "Caminho para o arquivo executável mysql";
$MESS["CSA_PARAM_MYSQL_USER"] = "Login do usuário do MySQL com o privilégio de criação de banco de dados";
$MESS["CSA_PARAM_MYSQL_PASSWORD"] = "Senha de usuário do MySQL com privilégio de criação de banco de dados";
$MESS["CSA_PARAM_MYSQL_DB_PATH"] = "Caminho para arquivo de dump do banco de dados inicial";
$MESS["CSA_PARAM_DIR_PERMISSIONS"] = "Permissões para pastas contendo sites alugados";
$MESS["CSA_PARAM_FILE_PERMISSIONS"] = "Permissões para os arquivos de sites alugados";
$MESS["CSA_PARAM_PATH_PUBLIC"] = "Arquivos públicos dos sites alugados";
$MESS["CSA_PARAM_MEMORY_LIMIT"] = "Limite de memória para sites alugados";
$MESS["CSA_PARAM_APACHE_ROOT"] = "Catálogo contendo arquivos de configuração Apache de sites virtuais";
$MESS["CSA_PARAM_NGINX_ROOT"] = "Catálogo contendo arquivos de configuração nginx de sites virtuais";
$MESS["CSA_PARAM_RELOAD_FILE"] = "Sinalizar arquivo para acionar reinicialização do servidor web";
$MESS["CSA_PARAM_REGISTER_IMMEDIATE"] = "Registrar site após criação";
?>