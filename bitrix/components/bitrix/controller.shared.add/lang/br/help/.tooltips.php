<?
$MESS["URL_SUBDOMAIN_TIP"] = "Especifica o nível superior do subdomínio";
$MESS["PATH_VHOST_TIP"] = "Especifica o caminho completo para uma pasta contendo sites alugados";
$MESS["MYSQL_PATH_TIP"] = "O caminho para o arquivo executável mysql";
$MESS["MYSQL_USER_TIP"] = "Login de um usuário do MySQL que tem permissão de criação de banco de dados";
$MESS["MYSQL_PASSWORD_TIP"] = "Senha de um usuário do MySQL que tem permissão de criação de banco de dados";
$MESS["SET_TITLE_TIP"] = "Define o título da página.";
$MESS["CONTROLLER_URL_TIP"] = "Especifica a URL do controlador de site";
$MESS["MYSQL_DB_PATH_TIP"] = "O caminho para um arquivo contendo o dump do banco de dados inicial";
$MESS["DIR_PERMISSIONS_TIP"] = "Especifica permissões para uma pasta contendo sites alugados";
$MESS["FILE_PERMISSIONS_TIP"] = "Especifica permissões para os arquivos de sites alugados";
$MESS["PATH_PUBLIC_TIP"] = "Arquivos públicos dos sites alugados";
$MESS["MEMORY_LIMIT_TIP"] = "Limite de memória para sites alugados";
$MESS["APACHE_ROOT_TIP"] = "O catálogo que contém arquivos de configuração virtuais do site para o Apache";
$MESS["NGINX_ROOT_TIP"] = "O catálogo que contém arquivos de configuração virtuais do site para nginx";
$MESS["RELOAD_FILE_TIP"] = "O arquivo de sinalização para acionar reinicialização do servidor web";
?>