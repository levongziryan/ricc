<?
$MESS["CSA_TITLE"] = "Adicionando um site";
$MESS["CSA_MODULE_NOT_INSTALLED"] = "O módulo controlador não está instalado";
$MESS["CSA_ERROR_DOMAIN_NAME"] = "Os nomes de domínio podem incluir apenas letras, dígitos, pontos e hífens.";
$MESS["CSA_ERROR_DIR_PERMISSIONS"] = "Permissões incorretas atribuídas aos diretórios de sites alugados.";
$MESS["CSA_ERROR_FILE_PERMISSIONS"] = "Permissões incorretas atribuídas a arquivos de sites alugados.";
$MESS["CSA_ERROR_MEMORY_LIMIT"] = "Limite de memória atribuído a sites alugados incorreto.";
$MESS["CSA_ERROR_NAME_EXISTS"] = "O nome especificado já está em uso. Por favor escolha um nome diferente.";
$MESS["CSA_ERROR_BAD_MYSQL_PATH"] = "Parâmetro incorreto: Caminho para o arquivo executável mysql.";
$MESS["CSA_ERROR_DB_FILEDUMP"] = "O arquivo de dump inicial do banco de dados não existe ou não pode ser lido devido à falta de permissões para leitura.";
$MESS["CSA_ERROR_DB_CONNECT"] = "Erro ao conectar ao banco de dados: #ERROR#";
$MESS["CSA_ERROR_DB_CREATE"] = "Erro ao criar banco de dados (#DB_NAME#): #ERROR#";
$MESS["CSA_ERROR_DB_IMPORT"] = "Erro ao importar o banco de dados (#DB_NAME#).";
$MESS["CSA_ERROR_ADD_SITE"] = "Erro ao registrar o site no controlador.";
$MESS["CSA_ERROR_ADD_SITE2"] = "Erro ao registrar o site no controlador: #ERROR#";
$MESS["CSA_LOG_ADD_CLIENT"] = "Adicionando cliente por criação direta";
$MESS["CSA_ERROR_CREATE_DIR"] = "Erro ao criar o diretório do site.";
$MESS["CSA_ERROR_FILE_WRITE"] = "Erro ao gravar para #FILE#.";
$MESS["CSA_ERROR_NOT_FOUND_APACHE_VHOST_DIR"] = "O catálogo com os arquivos de configuração do Apache não existe ou não pode ser escrito.";
$MESS["CSA_ERROR_APACHE_TEMPLATE_NOT_FOUND"] = "O modelo de configuração do Apache não foi encontrado (#FILE#).";
$MESS["CSA_ERROR_WRITE_APACHE_CONFIG"] = "Erro ao gravar no arquivo de configuração do Apache.";
$MESS["CSA_ERROR_NGINX_TEMPLATE_NOT_FOUND"] = "O modelo de configuração nginx não foi encontrado (#FILE#).";
$MESS["CSA_ERROR_NOT_FOUND_NGINX_VHOST_DIR"] = "O catálogo com os arquivos de configuração nginx não existe ou não pode ser escrito.";
$MESS["CSA_ERROR_WRITE_NGINX_CONFIG"] = "Erro ao gravar no arquivo de configuração do nginx.";
?>