<?
$MESS["CT_BCSA_SITE_NAME"] = "Veuillez choisir un nom de domaine pour le nouveau site";
$MESS["CT_BCSA_SITE_CREATED"] = "Un nouvel site a été créé avec succès. Pour y entrer et accéder aux réglages veuillez passer par le lien, s'il vous plaît:";
$MESS["CT_BCSA_CREATE_BUTTON"] = "Créer mon Site à moi";
?>