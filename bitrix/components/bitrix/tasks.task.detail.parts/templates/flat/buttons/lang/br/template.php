<?
$MESS["TASKS_START_TASK_TIMER"] = "Iniciar controlador de hora";
$MESS["TASKS_PAUSE_TASK_TIMER"] = "Pausar";
$MESS["TASKS_MORE"] = "Mais...";
$MESS["TASKS_COPY_TASK"] = "Copiar";
$MESS["TASKS_ADD_TASK_TO_TIMEMAN"] = "Adicionar ao plano diário de trabalho";
$MESS["TASKS_COPY_TASK_EX"] = "Duplicar tarefa";
$MESS["TASKS_ADD_TASK_TO_TIMEMAN_EX"] = "Adicionar ao plano diário de trabalho";
$MESS["TASKS_ADD_SUBTASK"] = "Criar subtarefa";
$MESS["TASKS_DELETE_TASK"] = "Excluir";
$MESS["TASKS_DELETE_CONFIRM"] = "Você realmente deseja excluir?";
$MESS["TASKS_DEFER_TASK"] = "Pausar";
$MESS["TASKS_RENEW_TASK"] = "Retomar";
$MESS["TASKS_DELEGATE_TASK"] = "Delegar";
$MESS["TASKS_TASK_CONFIRM_START_TIMER_TITLE"] = "O controlador de hora está sendo usado agora com outra tarefa.";
$MESS["TASKS_TASK_CONFIRM_START_TIMER"] = "Você já está usando o controlador de hora para \"{{TITLE}}\". Esta tarefa será pausada. Continuar?";
$MESS["TASKS_UNKNOWN"] = "Desconhecido";
?>