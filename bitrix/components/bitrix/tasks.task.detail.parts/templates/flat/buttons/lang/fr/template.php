<?
$MESS["TASKS_START_TASK_TIMER"] = "Démarrer le suivi du temps";
$MESS["TASKS_PAUSE_TASK_TIMER"] = "Pause";
$MESS["TASKS_MORE"] = "Plus...";
$MESS["TASKS_COPY_TASK"] = "Copier";
$MESS["TASKS_ADD_TASK_TO_TIMEMAN"] = "Ajouter un plan de jours de travail";
$MESS["TASKS_COPY_TASK_EX"] = "Dupliquer la tâche";
$MESS["TASKS_ADD_TASK_TO_TIMEMAN_EX"] = "Ajouter un plan de jours de travail";
$MESS["TASKS_ADD_SUBTASK"] = "Créer une sous-tâche";
$MESS["TASKS_DELETE_TASK"] = "Supprimer";
$MESS["TASKS_DELETE_CONFIRM"] = "Voulez-vous vraiment supprimer ?";
$MESS["TASKS_DEFER_TASK"] = "Pause";
$MESS["TASKS_RENEW_TASK"] = "Reprendre";
$MESS["TASKS_DELEGATE_TASK"] = "Déléguer";
$MESS["TASKS_TASK_CONFIRM_START_TIMER_TITLE"] = "Le suivi du temps est maintenant utilisé sur une autre tâche.";
$MESS["TASKS_TASK_CONFIRM_START_TIMER"] = "Vous utilisez maintenant le suivi du temps pour \"{{TITLE}}\". Cette tâche va être mise en pause. Continuer ?";
$MESS["TASKS_UNKNOWN"] = "Inconnu";
?>