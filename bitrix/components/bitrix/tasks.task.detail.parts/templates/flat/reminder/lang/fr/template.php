<?
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_DELETE"] = "Supprimer";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_ADD_ALT"] = "Ajouter";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_UPDATE"] = "Mettre à jour";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_ADD"] = "Ajouter un rappel";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_ADD_REMARK"] = "via messagerie instantanée ou e-mail";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_TRANSPORT_J"] = "via messagerie instantanée";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_TRANSPORT_E"] = "via e-mail";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_TRANSPORT_J_EX"] = "Envoyer un message instantané";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_TRANSPORT_E_EX"] = "Envoyer un message par e-mail";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_RECEPIENT_TYPE_O"] = "au créateur";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_RECEPIENT_TYPE_R"] = "au responsable";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_RECEPIENT_TYPE_S"] = "à soit";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_RECEPIENT_TYPE_O_EX"] = "Le message sera envoyé au créateur actuel";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_RECEPIENT_TYPE_R_EX"] = "Le message sera envoyé au responsable actuel";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_RECEPIENT_TYPE_S_EX"] = "Le message sera envoyé à l'utilisateur actuel";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_TYPE_A"] = "date";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_TYPE_A_EX"] = "Le message sera envoyé à une date en particulier";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_TYPE_D"] = "date limite";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_TYPE_D_EX"] = "Le message sera envoyé à une heure en particulier avant la date limite";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_NO_DEADLINE"] = "Pour configurer un rappel de date limite, précisez la date limite.";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_DAYS"] = "jours";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_HOURS"] = "heures";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_TO_RESPONSIBLE"] = "au responsable";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_TO_CREATOR"] = "au créateur";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_TO_SELF"] = "à soit";
$MESS["TASKS_TTDP_TEMPLATE_REMINDER_REMIND_BY"] = "rappeler d'utiliser";
?>