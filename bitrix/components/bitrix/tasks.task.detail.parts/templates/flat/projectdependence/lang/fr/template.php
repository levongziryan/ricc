<?
$MESS["TASKS_TTDP_TEMPLATE_PROJDEP_DELETE"] = "Supprimer";
$MESS["TASKS_TTDP_TEMPLATE_PROJDEP_COL_RELATED"] = "Tâches précédentes";
$MESS["TASKS_TTDP_TEMPLATE_PROJDEP_COL_REL_TYPE"] = "Action";
$MESS["TASKS_TTDP_TEMPLATE_PROJDEP_WHEN_START"] = "va démarrer";
$MESS["TASKS_TTDP_TEMPLATE_PROJDEP_WHEN_END"] = "va finir";
$MESS["TASKS_TTDP_TEMPLATE_PROJDEP_CURRENT_TASK"] = "Tâche en cours";
$MESS["TASKS_TTDP_TEMPLATE_PROJDEP_WHEN"] = "quand";
$MESS["TASKS_TTDP_TEMPLATE_PROJDEP_ADD"] = "Ajouter la tâche précédente";
$MESS["TASKS_TTDP_LICENSE_TITLE"] = "Lier les tâches";
$MESS["TASKS_TTDP_LICENSE_BODY"] = "Les liens de tâches sont disponibles dans les abonnements supérieurs.";
?>