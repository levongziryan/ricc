<?
$MESS["CRM_ENTITY_LF_PLACEHOLDER_PARAGRAPH_1"] = "Este feed muestra todo lo que está pasando en su CRM: llamadas recientes; Negociaciones, prospectos y actualizaciones de facturas. La lista de la derecha muestra tareas urgentes.";
$MESS["CRM_ENTITY_LF_PLACEHOLDER_PARAGRAPH_2"] = "Puede enviar un correo electrónico a su cliente directamente desde el feed del CRM. Verá su respuesta en el feed tan pronto como esté disponible.";
$MESS["CRM_ENTITY_LF_PLACEHOLDER_IMAGE_HINT"] = "Flujo de Actividad del CRM";
?>