<?
$MESS["CRM_ENTITY_LF_PLACEHOLDER_PARAGRAPH_1"] = "Ce flux affiche tout ce qui se passe dans votre GRC : appels récents ; mises à jour de transaction, piste et facture. Une liste affiche à droite les tâches urgentes.";
$MESS["CRM_ENTITY_LF_PLACEHOLDER_PARAGRAPH_2"] = "Vous pouvez envoyer un e-mail directement à votre client depuis le flux GRC. Sa réponse sera affichée dans le flux dès qu'elle sera disponible.";
$MESS["CRM_ENTITY_LF_PLACEHOLDER_IMAGE_HINT"] = "Flux d'activités GRC";
?>