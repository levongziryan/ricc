<?
$MESS["CRM_TAB_1"] = "Configuraciones de la Importación";
$MESS["CRM_TAB_1_TITLE"] = "Editar configuraciones de la importación";
$MESS["CRM_TAB_2"] = "Campos";
$MESS["CRM_TAB_2_TITLE"] = "Configurar mapeo del campo";
$MESS["CRM_TAB_3"] = "Importar";
$MESS["CRM_TAB_3_TITLE"] = "Importar resultado";
$MESS["CRM_IMPORT_NEXT_STEP"] = "Siguiente >>";
$MESS["CRM_IMPORT_NEXT_STEP_TITLE"] = "Ir a siguiente paso";
$MESS["CRM_IMPORT_PREVIOUS_STEP"] = "<< Regresar";
$MESS["CRM_IMPORT_PREVIOUS_STEP_TITLE"] = "Ir al paso anterior";
$MESS["CRM_IMPORT_DONE"] = "Hecho";
$MESS["CRM_IMPORT_DONE_TITLE"] = "Ver prospectos";
$MESS["CRM_IMPORT_CANCEL"] = "Cancelar";
$MESS["CRM_IMPORT_CANCEL_TITLE"] = "Abandonar y regresar a la lista de prospectos";
$MESS["CRM_IMPORT_AGAIN"] = "Importar otro archivo";
$MESS["CRM_IMPORT_AGAIN_TITLE"] = "Oprimir para importar otro archivo";
$MESS["CRM_TAB_DUP_CONTROL"] = "Control de duplicados";
$MESS["CRM_TAB_DUP_CONTROL_TITLE"] = "Configurar el control de duplicados";
?>