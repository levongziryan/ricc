<?
$MESS["CRM_CURRENCY_CLASSIFIER_MODULE_NOT_INSTALLED_CRM"] = "El módulo CRM no está instalado.";
$MESS["CRM_CURRENCY_CLASSIFIER_MODULE_NOT_INSTALLED_CURRENCY"] = "El módulo Currenci no está instalado.";
$MESS["CRM_CURRENCY_CLASSIFIER_CURRENCY_NOT_FOUND"] = "La moneda #currency# no fue encontrada";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_INCORRECT_VALUE_ERROR"] = "El valor es incorrecto o demasiado grande";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_MAX_LENGTH_ERROR"] = "El valor no debe exceder #max_length# caracteres";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_SYM_CODE_ERROR"] = "El código simbólico debe ser una abreviación de tres letras latinas";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_FULL_NAME_ERROR"] = "El nombre de la moneda no debe estar vacío o superar los 50 caracteres";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_THOUSANDS_VARIANT_OWN"] = "Otro valor";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_SIGN_POSITION_BEFORE"] = "Antes de la cantidad";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_SIGN_POSITION_AFTER"] = "Después de la cantidad";
$MESS["CRM_CURRENCY_CLASSIFIER_ADD_UNKNOWN_ERROR"] = "Error desconocido al crear una moneda.";
$MESS["CRM_CURRENCY_CLASSIFIER_UPDATE_UNKNOWN_ERROR"] = "Error desconocido al actualizar una moneda.";
?>