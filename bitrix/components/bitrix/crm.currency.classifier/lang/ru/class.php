<?
$MESS["CRM_CURRENCY_CLASSIFIER_MODULE_NOT_INSTALLED_CRM"] = "Модуль CRM не установлен.";
$MESS["CRM_CURRENCY_CLASSIFIER_MODULE_NOT_INSTALLED_CURRENCY"] = "Модуль Валюты не установлен.";
$MESS["CRM_CURRENCY_CLASSIFIER_CURRENCY_NOT_FOUND"] = "Валюта #currency# не найдена";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_INCORRECT_VALUE_ERROR"] = "Значение не корректно или слишком большое";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_MAX_LENGTH_ERROR"] = "Значение не должно превышать #max_length# символов";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_SYM_CODE_ERROR"] = "Символьный код должен состоять из 3-х символов латинского алфавита";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_FULL_NAME_ERROR"] = "Название валюты не должно превышать 50 символов";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_THOUSANDS_VARIANT_OWN"] = "Другое значение";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_SIGN_POSITION_BEFORE"] = "Перед суммой";
$MESS["CRM_CURRENCY_CLASSIFIER_FIELD_SIGN_POSITION_AFTER"] = "После суммы";
$MESS["CRM_CURRENCY_CLASSIFIER_ADD_UNKNOWN_ERROR"] = "Во время создания валюты произошла неизвестная ошибка";
$MESS["CRM_CURRENCY_CLASSIFIER_UPDATE_UNKNOWN_ERROR"] = "Во время обновления валюты произошла неизвестная ошибка";