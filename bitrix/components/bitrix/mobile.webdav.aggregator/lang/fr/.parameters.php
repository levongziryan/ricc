<?
$MESS["WD_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["WD_IBLOCK_OTHER_ID"] = "Blocs d'information avec les documents";
$MESS["WD_IBLOCK_USER_ID"] = "Bloc d'information des documents des utilisateurs";
$MESS["WD_IBLOCK_GROUP_ID"] = "Le bloc d'information des documents des groupes de travail";
$MESS["WD_NAME_TEMPLATE"] = "Affichage du nom";
$MESS["WD_USER_FILE_PATH"] = "Page de documents d'utilisateurs";
$MESS["WD_GROUP_FILE_PATH"] = "Page pour documents de groupes de travail";
$MESS["WD_IBLOCK_TYPE"] = "Type de bloc d'information";
?>