<?
$MESS["WD_USER_FILE_PATH"] = "Página de Documentos do Usuário";
$MESS["WD_GROUP_FILE_PATH"] = "Página de Documentos do Grupo de Trabalho";
$MESS["WD_IBLOCK_TYPE"] = "Tipo de Bloco de Informação";
$MESS["WD_IBLOCK_OTHER_ID"] = "Bloco de Informação de Documentos";
$MESS["WD_IBLOCK_GROUP_ID"] = "Bloco de Informação de Documentos do Grupo de Trabalho";
$MESS["WD_IBLOCK_USER_ID"] = "Bloco de Informação de Documentos do Usuário";
$MESS["WD_NAME_TEMPLATE"] = "Formato de Nome";
$MESS["WD_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
?>