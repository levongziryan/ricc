<?
$MESS["WD_WD_MODULE_IS_NOT_INSTALLED"] = "El módulo de la Biblioteca no está instalado.";
$MESS["WD_IB_MODULE_IS_NOT_INSTALLED"] = "El módulo de \"Los blocks de información\" no están instalados.";
$MESS["WD_SN_MODULE_IS_NOT_INSTALLED"] = "El módulo Social Network no está instalada.";
$MESS["WD_SHARED"] = "Documentos compartidos";
$MESS["WD_GROUP"] = "Grupos de Trabajo";
$MESS["WD_PRIVATE"] = "Documentos Privados";
$MESS["WD_USER"] = "Usar Documentos";
$MESS["WD_ROOT"] = "Raíz";
$MESS["WD_IB_GROUP_IS_NOT_FOUND"] = "El block de información de los documentos del grupo de trabajo no fue encontrado.";
$MESS["WD_IB_USER_IS_NOT_FOUND"] = "El block de información de los documentos del usuario de trabajo no fue encontrado.";
$MESS["WD_DAV_INSUFFICIENT_RIGHTS"] = "Usted no tiene permiso para esta acción.";
$MESS["WD_DAV_UNSUPORTED_METHOD"] = "Este método no está soportado.";
$MESS["WD_GROUP_SECTION_FILES_NOT_FOUND"] = "La sección de documentos del grupo no fue encontrada.";
$MESS["WD_USER_SECTION_FILES_NOT_FOUND"] = "La sección de documentos del usuario no fue encontrada.";
$MESS["WD_USER_NOT_FOUND"] = "El usuario no fue encontrado.";
$MESS["WD_NOT_SEF_MODE"] = "Los URL del Motor de búsqueda amigable (SEF) no están habilitados en los parámetros del componente.";
$MESS["WD_SOCNET_LANG_NOT_FOUND"] = "Los componentes de la red social faltan. Verifique la ruta del servidor web en la configuración del sitio.";
$MESS["WD_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
?>