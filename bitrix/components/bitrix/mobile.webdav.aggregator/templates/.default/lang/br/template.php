<?
$MESS["WD_AG_MSGTITLE"] = "Para conectar a biblioteca como um disco de rede:";
$MESS["WD_AG_HELP1"] = "Executar <i>Windows Explorer</i>.";
$MESS["WD_AG_HELP2"] = "Selecione \"Ferramentas\" -> \"Mapear Unidade de Rede...\" no menu do Explorer.";
$MESS["WD_AG_HELP3"] = "Selecione uma letra de driver para a pasta a ser conectado.";
$MESS["WD_AG_HELP4"] = "Fornecer o caminho da biblioteca no campo <i>Pasta</i>";
$MESS["WD_AG_HELP5"] = "Clique em <i>Finalizar</i>.";
$MESS["WD_AG_HELP6"] = "Para mais informações no uso de bibliotecas no Windows e Mac OS X, por favor consulte a seção de ajuda na #STARTLINK# Biblioteca de Documentos #ENDLINK#.";
$MESS["WD_NO_LIBRARIES"] = "Nenhuma biblioteca de documentos disponível";
?>