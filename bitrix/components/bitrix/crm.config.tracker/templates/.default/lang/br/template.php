<?
$MESS["CRM_CONFIGS_LINK_TEXT"] = "Exibir configurações de CRM";
$MESS["CRM_CONFIGS_LINK_TITLE"] = "Abre formulário de configurações de CRM";
$MESS["CRM_TRACKER_SECTION_TITLE"] = "Predefinições";
$MESS["CRM_TRACKER_SECTION_NAME"] = "Configurar rastreador";
$MESS["CRM_BUTTON_SAVE_TITLE"] = "Salvar";
$MESS["CRM_BUTTON_CANCEL_TITLE"] = "Cancelar";
$MESS["CRM_TRACKER_SETTINGS_COMPANY"] = "Para empresa";
$MESS["CRM_TRACKER_SETTINGS_CONTACT"] = "Para contato";
?>