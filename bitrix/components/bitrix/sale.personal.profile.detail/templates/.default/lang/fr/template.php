<?
$MESS["SALE_PNAME"] = "Dénomination";
$MESS["SALE_RESET"] = "Annuler";
$MESS["SALE_APPLY"] = "Appliquer";
$MESS["SALE_SAVE"] = "Sauvegarder";
$MESS["SALE_PERS_TYPE"] = "Type de payeur";
$MESS["SPPD_RECORDS_LIST"] = "Pour la liste des profils";
$MESS["SPPD_PROFILE_NO"] = "Profil N° #ID#";
$MESS["SPPD_DOWNLOAD_FILE"] = "Télécharger le fichier #FILE_NAME#";
$MESS["SPPD_DELETE_FILE"] = "Supprimer le fichier";
$MESS["SPPD_SELECT"] = "Sélectionner";
$MESS["SPPD_FILE_NOT_SELECTED"] = "Aucun fichier n'est sélectionné";
$MESS["SPPD_FILE_COUNT"] = "Fichiers sélectionnés : ";
$MESS["SPPD_ADD"] = "Ajouter";
?>