<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["SONET_MODULE_NOT_INSTALLED"] = "O módulo de rede social não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado.";
$MESS["CRM_SL_EVENT_EDIT_ENTITY_TYPE_NOT_DEFINED"] = "CRM tipo de entidade não é especificado.";
$MESS["CRM_SL_EVENT_EDIT_ENTITY_ID_NOT_DEFINED"] = "A entidade ID CRM não é especificado.";
$MESS["CRM_SL_EVENT_NOT_AVAIBLE"] = "Os eventos não estão disponíveis para o usuário atual.";
$MESS["CRM_SL_EVENT_EDIT_HIDDEN_GROUP"] = "Grupo oculto";
$MESS["CRM_SL_EVENT_EDIT_ENTITY_NOT_DEFINED"] = "Por favor, escolha pelo menos uma ligação, contato, empresa ou negócio.";
$MESS["CRM_SL_EVENT_EDIT_PERMISSION_DENIED"] = "Você não tem acesso para criar uma mensagem em '#TITLE#'.";
$MESS["CRM_SL_EVENT_EDIT_EMPTY_MESSAGE"] = "Por favor insira o texto da mensagem.";
$MESS["CRM_SL_EVENT_IM_MENTION_POST"] = "você mencionou no post \"#title#\"";
?>