<?
$MESS["INTL_VARIABLE_ALIASES"] = "Variables del alias";
$MESS["INTL_IBLOCK_TYPE"] = "Tipo de Bloque Informativo";
$MESS["INTL_IBLOCK"] = "Bloque Informativo";
$MESS["INTL_MEETING_VAR"] = "Variable para el ID de la sala de reuniones";
$MESS["INTL_PAGE_VAR"] = "Variable de la página";
$MESS["INTL_PATH_TO_MEETING"] = "Página de horarios de la reserva de la sala de reuniones";
$MESS["INTL_PATH_TO_MEETING_LIST"] = "Página principal de reserva de sala de reuniones";
$MESS["INTL_PATH_TO_RESERVE_MEETING"] = "Página de reserva de sala de reuniones";
$MESS["INTL_PATH_TO_MODIFY_MEETING"] = "Página del editor de parámetros de la reserva de sala de reuniones";
$MESS["INTL_SET_NAVCHAIN"] = "Establecer atajos";
$MESS["INTL_USERGROUPS_MODIFY"] = "Grupos de usuarios permitidos para editar horarios para la reserva de la sala de reuniones";
$MESS["INTL_USERGROUPS_RESERVE"] = "Grupos de usuarios permitidos para reserva de la sala de reuniones";
?>