<?
$MESS["EC_INTRANET_MODULE_NOT_INSTALLED"] = "El modulo de Intranet no esta instalado.";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "El modulo de los bloques informativos no esta instalado.";
$MESS["INTS_NO_IBLOCK_PERMS"] = "Usted no tiene permiso para ver las tareas del bloque informativo.";
$MESS["INTASK_C36_PAGE_TITLE"] = "Sala de reuiniones";
$MESS["INAF_F_ID"] = "ID";
$MESS["INAF_F_NAME"] = "Titulo";
$MESS["INAF_F_DESCRIPTION"] = "Descripción";
$MESS["INAF_F_FLOOR"] = "Piso";
$MESS["INAF_F_PLACE"] = "Asientos";
$MESS["INAF_F_PHONE"] = "Teléfono";
$MESS["INTASK_C23_GRAPH"] = "Horarios";
$MESS["INTASK_C23_GRAPH_DESCR"] = "Horario de la sala de reuniones";
$MESS["INTASK_C23_RESERV_DESCR"] = "Reservar una sala de reunión";
$MESS["INTASK_C23_EDIT"] = "Editar";
$MESS["INTASK_C23_EDIT_DESCR"] = "Editor de la sala de reunión";
$MESS["INTASK_C23_DELETE"] = "Eliminar";
$MESS["INTASK_C23_DELETE_DESCR"] = "Eliminar sala de reunión";
$MESS["INTASK_C23_DELETE_CONF"] = "Esta usted seguro de querer eliminar esta sala de reunión?";
$MESS["INTASK_C23_RESERV"] = "Libro";
?>