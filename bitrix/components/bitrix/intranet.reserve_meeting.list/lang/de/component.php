<?
$MESS["INTASK_C23_DELETE_CONF"] = "Wollen Sie diesen Raum wirklich löschen?";
$MESS["INTASK_C23_RESERV"] = "Reservieren";
$MESS["INTASK_C23_RESERV_DESCR"] = "Raum reservieren";
$MESS["INTASK_C23_DELETE"] = "Löschen";
$MESS["INTASK_C23_DELETE_DESCR"] = "Raum löschen";
$MESS["INAF_F_DESCRIPTION"] = "Beschreibung";
$MESS["INTASK_C23_EDIT"] = "Bearbeiten";
$MESS["INAF_F_FLOOR"] = "Stockwerk";
$MESS["INAF_F_ID"] = "ID";
$MESS["INTASK_C23_EDIT_DESCR"] = "Raum bearbeiten";
$MESS["INTASK_C23_GRAPH_DESCR"] = "Grafik: Räume reservieren";
$MESS["INTASK_C36_PAGE_TITLE"] = "Räume";
$MESS["INAF_F_PHONE"] = "Telefon";
$MESS["INTASK_C23_GRAPH"] = "Grafik";
$MESS["INAF_F_PLACE"] = "Plätze";
$MESS["EC_IBLOCK_MODULE_NOT_INSTALLED"] = "Das Modul \"Informationsblöcke\" wurde nicht installiert.";
$MESS["EC_INTRANET_MODULE_NOT_INSTALLED"] = "Das Modul \"Intranet\" wurde nicht installiert";
$MESS["INAF_F_NAME"] = "Überschrift";
$MESS["INTS_NO_IBLOCK_PERMS"] = "Sie haben nicht genügend Rechte, den Informationsblock der Besprechungsräume anzuzeigen.";
?>