<?
$MESS["DISK_FOLDER_LIST_MESSAGE_CONNECT_GROUP_DISK"] = "L'disque du groupe est connecté à votre disque Bitrix24";
$MESS["DISK_FOLDER_LIST_ERROR_VALIDATE_BIZPROC"] = "Veuillez compléter tous les champs obligatoires.";
$MESS["DISK_FOLDER_LIST_ERROR_COULD_NOT_FIND_OBJECT"] = "Impossible de  trouver l'objet.";
$MESS["DISK_FOLDER_LIST_ERROR_COULD_NOT_MOVE_OBJECT"] = "Impossible de déplacer l'objet.";
$MESS["DISK_FOLDER_LIST_ERROR_COULD_NOT_COPY_OBJECT"] = "Impossible de copier l'objet.";
$MESS["DISK_FOLDER_LIST_ERROR_COULD_NOT_CREATE_FIND_EXT_LINK"] = "Impossible de créer ou trouver le lien public vers l'objet.";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FOLDER_DETACH"] = "Le dossier est déconnecté";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FOLDER_MARK_DELETED_2"] = "Le dossier a été déplacé dans la corbeille";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FOLDER_MARK_DELETED"] = "Le dossier a été supprimé";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FOLDER_DELETED"] = "Le dossier a été supprimé";
$MESS["DISK_FOLDER_LIST_ERROR_COULD_NOT_SAVE"] = "Une erreur est survenue lors de la sauvegarde.";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FILE_DETACH"] = "Le fichier est déconnecté";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FILE_MARK_DELETED_2"] = "Le fichier a été déplacé dans la corbeille";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FILE_MARK_DELETED"] = "La fichier a été supprimé";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FILE_DELETED"] = "La fichier a été supprimé";
$MESS["DISK_FOLDER_LIST_LOCK_IS_DISABLED"] = "Le verrouillage de document est désactivé. Contactez votre administrateur Bitrix24.";
$MESS["DISK_FOLDER_LIST_ERROR_DISK_QUOTA"] = "Espace disque insuffisant pour finaliser l'action";
?>