<?
$MESS["DISK_FOLDER_ACTION_MESSAGE_FOLDER_MARK_DELETED"] = "La carpeta se ha eliminado";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FILE_MARK_DELETED"] = "La carpeta se ha eliminado";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FOLDER_DELETED"] = "La carpeta se ha eliminado";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FILE_DELETED"] = "La carpeta se ha eliminado";
$MESS["DISK_FOLDER_LIST_ERROR_COULD_NOT_FIND_OBJECT"] = "No se pudo encontrar el objeto.";
$MESS["DISK_FOLDER_LIST_ERROR_COULD_NOT_COPY_OBJECT"] = "No se pudo copiar el objeto.";
$MESS["DISK_FOLDER_LIST_ERROR_COULD_NOT_MOVE_OBJECT"] = "No se pudo mover el objeto.";
$MESS["DISK_FOLDER_LIST_ERROR_COULD_NOT_CREATE_FIND_EXT_LINK"] = "No se ha podido crear o encontrar un enlace público al objeto.";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FOLDER_DETACH"] = "Carpeta desconectado";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FILE_DETACH"] = "Carpeta desconectado";
$MESS["DISK_FOLDER_LIST_ERROR_COULD_NOT_SAVE"] = "Guardar error.";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FOLDER_MARK_DELETED_2"] = "La carpeta se ha movido a la papelera de reciclaje";
$MESS["DISK_FOLDER_ACTION_MESSAGE_FILE_MARK_DELETED_2"] = "El archivo se ha movido a la papelera de reciclaje";
$MESS["DISK_FOLDER_LIST_ERROR_VALIDATE_BIZPROC"] = "Por favor, complete todos los campos obligatorios.";
$MESS["DISK_FOLDER_LIST_MESSAGE_CONNECT_GROUP_DISK"] = "Grupo drive fue conectado con éxito a tu Bitrix24.Drive";
$MESS["DISK_FOLDER_LIST_LOCK_IS_DISABLED"] = "Bloqueo de documento está desactivado. Póngase en contacto con su administrador de Bitrix24.";
$MESS["DISK_FOLDER_LIST_ERROR_DISK_QUOTA"] = "El espacio del disco es insuficiente para completar la acción";
?>