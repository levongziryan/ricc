<?
$MESS["CRM_COMPANY_WGT_PAGE_TITLE"] = "Compañías: Resumen de informe";
$MESS["CRM_COMPANY_WGT_DEMO_TITLE"] = "Esta es una vista de demo. Cerrarla para obtener el análisis de sus compañías.";
$MESS["CRM_COMPANY_WGT_DEMO_CONTENT"] = "Si todavía no tiene ninguna compañía, <a href=\"#URL#\" class=\"#CLASS_NAME#\">crear una</a> ahora!";
?>