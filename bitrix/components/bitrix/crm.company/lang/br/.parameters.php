<?
$MESS["CRM_COMPANY_VAR"] = "Nome variável da ID da empresa";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "Modelo de Caminho da Página de Índice";
$MESS["CRM_SEF_PATH_TO_LIST"] = "Modelo de Caminho da Página de empresas";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "Modelo de Caminho da Página do Editor de empresa";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "Modelo de Caminho da Página de Visualização da empresa";
$MESS["CRM_SEF_PATH_TO_IMPORT"] = "Modelo  de Caminho da Página Importação";
$MESS["CRM_ELEMENT_ID"] = "Identificação da empresa";
$MESS["CRM_NAME_TEMPLATE"] = "Formato do nome";
?>