<?
$MESS["CRM_COMPANY_VAR"] = "Name der Variablen der Unternehmen-ID";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "Vorlage des Pfads zur Hauptseite";
$MESS["CRM_SEF_PATH_TO_LIST"] = "Vorlage des Pfads zur Seite mit der Unternehmensliste";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "Vorlage des Pfads zur Bearbeitungsseite des Unternehmens";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "Vorlage des Pfads zur Ansichtsseite des Unternehmens";
$MESS["CRM_ELEMENT_ID"] = "Unternehmen-ID";
$MESS["CRM_SEF_PATH_TO_IMPORT"] = "Vorlage des Pfads zur Importseite";
$MESS["CRM_NAME_TEMPLATE"] = "Namenformat";
?>