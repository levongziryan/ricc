<?
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_PRODUCT_LIST_OWNER_TYPE_NOT_SUPPORTED"] = "Le type donné du propriétaire n'est pas supporté.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_PRODUCT_LIST_OWNER_ID_NOT_DEFINED"] = "ID du propriétaire non renseigné.";
$MESS["CRM_PRODUCT_LIST_OWNER_TYPE_NOT_DEFINED"] = "Type de propriétaire non renseigné..";
?>