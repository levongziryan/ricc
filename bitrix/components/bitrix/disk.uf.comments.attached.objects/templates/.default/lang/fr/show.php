<?
$MESS["WDUF_FILES"] = "Fichiers :";
$MESS["WDUF_MORE_ACTIONS"] = "Plus...";
$MESS["WDUF_FILE_EDIT"] = "Éditer";
$MESS["WDUF_PHOTO"] = "Photo :";
$MESS["DISK_UF_FILE_SETTINGS_DOCS"] = "Configurations pour travailler avec les documents";
$MESS["DISK_UF_FILE_RUN_FILE_IMPORT"] = "Obtenir la dernière version";
$MESS["DISK_UF_FILE_STATUS_PROCESS_LOADING"] = "Chargement";
$MESS["DISK_UF_FILE_STATUS_SUCCESS_LOADING"] = "La toute dernière version a bien été chargée.";
$MESS["DISK_UF_FILE_STATUS_HAS_LAST_VERSION"] = "Vous avez la dernière version.";
$MESS["DISK_UF_FILE_STATUS_FAIL_LOADING"] = "Erreur lors du chargement de la toute dernière version.";
$MESS["DISK_UF_FILE_DISABLE_AUTO_COMMENT"] = "Désactiver les commentaires auto";
$MESS["DISK_UF_FILE_ENABLE_AUTO_COMMENT"] = "Activer les commentaires auto";
$MESS["DISK_UF_FILE_DOWNLOAD_ALL_FILES_BY_ARCHIVE"] = "Télécharger tous les fichiers comme archive";
?>