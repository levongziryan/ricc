<?
$MESS["CONFIG_HEADER_SETTINGS"] = "Paramètres";
$MESS["CONFIG_COMPANY_NAME"] = "Nom de l'entreprise";
$MESS["CONFIG_COMPANY_TITLE_NAME"] = "Nom de l'entreprise à afficher en en-tête";
$MESS["CONFIG_SAVE_SUCCESSFULLY"] = "La mise à jour des paramètres a été effectuée avec succès.";
$MESS["CONFIG_SAVE"] = "Sauvegarder";
$MESS["config_rating_label_likeY"] = "Texte du bouton 'j'aime' avant le vote";
$MESS["config_rating_label_likeN"] = "Texte du bouton 'j'aime' après le vote";
$MESS["CONFIG_EMAIL_FROM"] = "E-mail de l'administrateur du site <br>(adresse de l'expéditeur par défaut)";
$MESS["CONFIG_LOGO_24"] = "Ajouter '24' au logo de l'entreprise";
$MESS["CONFIG_WEBDAV_SERVICES_GLOBAL"] = "Activer l'édition des documents via les services<br/>externes (Google Docs, MS Office Online et autres)";
$MESS["CONFIG_WEBDAV_SERVICES_LOCAL"] = "Autoriser les utilisateurs individuels ou les groupes à activer<br/>l'édition des documents via des services externes";
$MESS["CONFIG_WEBDAV_ALLOW_AUTOCONNECT_SHARE_GROUP_FOLDER"] = "Connecter automatiquement l'disque du groupe lorsque<br/>l'utilisateur rejoint le groupe";
$MESS["CONFIG_DISK_ALLOW_EDIT_OBJECT_IN_UF"] = "Tous les utilisateurs impliqués peuvent éditer les documents attachés aux discussions, tâches, <br/>commentaires et autres événements. Ce comportement par défaut peut être changé <br/>individuellement dans n'importe quel événement";
$MESS["CONFIG_DATE_FORMAT"] = "Format de la date";
$MESS["CONFIG_TIME_FORMAT"] = "Format de l'heure";
$MESS["CONFIG_TIME_FORMAT_12"] = "12 heures";
$MESS["CONFIG_TIME_FORMAT_24"] = "24 heures";
$MESS["CONFIG_WEEK_START"] = "Premier jour de la semaine";
$MESS["DAY_OF_WEEK_0"] = "Dimanche";
$MESS["DAY_OF_WEEK_1"] = "Lundi";
$MESS["DAY_OF_WEEK_2"] = "Mardi";
$MESS["DAY_OF_WEEK_3"] = "Mercredi";
$MESS["DAY_OF_WEEK_4"] = "Jeudi";
$MESS["DAY_OF_WEEK_5"] = "Vendredi";
$MESS["DAY_OF_WEEK_6"] = "Samedi";
$MESS["CONFIG_CLIENT_LOGO"] = "Logo de l'entreprise";
$MESS["CONFIG_CLIENT_LOGO_DESCR"] = "Votre logo doit être au format <b>PNG</b> fichier.<br/>Les dimensions ne doivent pas excéder 222px X 55px.";
$MESS["CONFIG_ADD_LOGO_BUTTON"] = "Télécharger un logo";
$MESS["CONFIG_ADD_LOGO_DELETE"] = "Supprimer le logo";
$MESS["CONFIG_ADD_LOGO_DELETE_CONFIRM"] = "Etes-vous sûr de vouloir supprimer le logo?";
$MESS["CONFIG_ALLOW_TOALL"] = "Autoriser l'envoi de messages à 'Tous les employés' dans le flux d'activités";
$MESS["CONFIG_IM_CHAT_RIGHTS"] = "Permet aux utilisateurs d'envoyer des messages dans le Chat général";
$MESS["CONFIG_DEFAULT_TOALL"] = "Choisir 'Tous les employés' comme destinataire par défaut";
$MESS["CONFIG_TOALL_RIGHTS_ADD"] = "Ajouter";
$MESS["CONFIG_TOALL_RIGHTS"] = "Paramètres de l'option  'Tous les employés'";
$MESS["CONFIG_TOALL_DEL"] = "Supprimer";
$MESS["CONFIG_FEATURES_TITLE"] = "Services";
$MESS["CONFIG_FEATURES_EXTRANET"] = "Extranet";
$MESS["CONFIG_FEATURES_TIMEMAN"] = "Gestion du temps et rapport d'activité";
$MESS["CONFIG_FEATURES_MEETING"] = "Réunions et briefings";
$MESS["CONFIG_FEATURES_LISTS"] = "Listes";
$MESS["CONFIG_FEATURES_CRM"] = "CRM";
$MESS["CONFIG_FEATURES_PROCESSES"] = "Flux de travail administratifs";
$MESS["CONFIG_IP_TITLE"] = "Restrictions IP (permettre l'accès uniquement à partir  d'adresses IP spécifiées ou de domaines d'accès. Exemple: 192.168.0.7; 192.168.0.1-192.168.0.100)";
$MESS["CONFIG_WORK_TIME"] = "Paramètres de temps de travail";
$MESS["CONFIG_WEEK_HOLIDAYS"] = "Weekend";
$MESS["CAL_OPTION_FIRSTDAY_MO"] = "Lundi";
$MESS["CAL_OPTION_FIRSTDAY_TU"] = "Mardi";
$MESS["CAL_OPTION_FIRSTDAY_WE"] = "Mercredi";
$MESS["CAL_OPTION_FIRSTDAY_TH"] = "Jeudi";
$MESS["CAL_OPTION_FIRSTDAY_FR"] = "Vendredi";
$MESS["CAL_OPTION_FIRSTDAY_SA"] = "Samedi";
$MESS["CAL_OPTION_FIRSTDAY_SU"] = "Dimanche";
$MESS["CONFIG_YEAR_HOLIDAYS"] = "Weekends et jours fériés";
$MESS["CONFIG_HEADER_SECUTIRY"] = "Paramètres de sécurité";
$MESS["CONFIG_OTP_SECURITY"] = "Activer la validation en deux étapes pour tous les utilisateurs";
$MESS["CONFIG_OTP_SECURITY2"] = "Faire basculer tous les utilisateurs vers la validation en deux étapes";
$MESS["CONFIG_OTP_SECURITY_SWITCH_OFF_INFO"] = "Attention ! Le fait de décocher cette option ne désactivera pas l'utilisation des codes à usage unique pour les utilisateurs ayant déjà activé la validation en deux étapes.                                                                                                                                                                                                                Ils devront continuer à saisir le code pour se connecter à Bitrix24.                                                                                                                                                                                                                                                                                                                                                                                                                              La validation en deux étapes peut être désactivée à partir du profil de l'utilisateur concerné.";
$MESS["CONFIG_OTP_SECURITY_DAYS"] = "Veuillez indiquer la période durant laquelle tous les employés devront activer la validation en deux étapes";
$MESS["CONFIG_OTP_SECURITY_INFO"] = "Une procédure d'accès en deux étapes de validation a été mise au point de manière à ce qu'elle soit accessible pour chaque utilisateur.<br/><br/> Chaque collaborateur reçoit un message lui indiquant qu'il dispose d'une période de temps limitée pour activer la validation en 2 étapes. Si au cours de cette période il ne configure pas la validation, il ne pourra plus s'identifier ensuite.";
$MESS["CONFIG_OTP_SECURITY_INFO2"] = "Pour activer la validation en 2 étapes, l'utilisateur doit installer l'application Bitrix24 OTP sur son téléphone mobile. Cette application est téléchargeable sur Appstore ou GooglePlay.<br/><br/>  Les utilisateurs dont les téléphones ne peuvent accéder aux applications peuvent se procurer un dispositif eToken Pass. Ces authentificateurs sont disponibles sur internet, par exemple: <a target=_blank href='http://www.safenet-inc.com/multi-factor-authentication/authenticators/pki-usb-authentication/etoken-pro/'>www.safenet-inc.com</a>, <a target=_blank href='http://www.authguard.com/eToken-PASS.asp'>www.authguard.com</a>. Ou pour en savoir plus:  <a target=_blank href='https://www.google.com/?q=buy+eToken+PASS&spell=1#safe=off&q=buy+eToken+PASS'>Google</a>. <br/><br/>  Il reste possible de désactiver l'authentification en 2 étapes pour certains utilisateurs, cependant il existera alors une probabilité d'accès frauduleux au Bitrix24 de l'entreprise si le mot de passe et l'identifiant de ces utilisateurs devaient être interceptés par une personne extérieure. La validation en deux étapes pour un utilisateur peut être désactivée par l'administrateur à partir de son profil.";
$MESS["CONFIG_MORE"] = "Lire la suite";
$MESS["CONFIG_OTP_POPUP_TITLE"] = "Pourquoi ne pas publier ce message dans le flux d'activité ?";
$MESS["CONFIG_OTP_POPUP_TEXT"] = "Vous pouvez publier le message suivant dans le flux d'activités<br/> pour communiquer à l'ensemble des utilisateurs.<br/><br/> Informez vos collègues au sujet de la validation en 2 étapes,<br/>afin qu'ils sachent comment paramétrer leur accès<br/> et qu'ils se familiarisent avec le nouveau mode de connexion à Bitrix24";
$MESS["CONFIG_OTP_POPUP_SHARE"] = "Partager";
$MESS["CONFIG_OTP_POPUP_CLOSE"] = "Non, merci";
$MESS["CONFIG_OTP_ADMIN_IS_REQUIRED_INFO"] = "Avant de faire passer vos utilisateurs au système de validation en deux étapes, vous devrez préalablement configurer la connexion à votre propre compte. <br/><br/>L'activation se fait à partir de la page 'sécurité' de votre profil.";
$MESS["CONFIG_OTP_IMPORTANT_TITLE"] = "Validation en 2 étapes";
$MESS["CONFIG_OTP_IMPORTANT_TEXT"] = "Aujourd'hui, pour vous connecter à Bitrix24, vous utilisez votre identifiant et votre mot de passe. Les données sont, en outre, protégées par la technologie du cryptage. Cependant il existe des menaces telles que les programmes espions, tout à fait capables de s'introduire dans votre système et de voler vos identifiants et mots de passe et de les utiliser pour accéder au Bitrix24 de votre entreprise.                                                                                                                                                                                                                                                                                   C'est pour cette raison qu'il est fortement recommandé d'activer la protection supplémentaire proposée par Bitrix24: la validation en deux étapes.                                                                                                                                                                                                                                                                              La validation en deux étapes est une méthode de protection contre les logiciels espions. Lors de chaque connexion, vous devrez saisir votre email de référence (votre identifiant), votre mot de passe, ainsi qu'un code à usage unique qui vous sera envoyé sur votre téléphone mobile via une application.[IMG]/images/otp/ru/info.png[/IMG]                                                                                                                                                                                                                                                                                                                                                                                                                  Cela créé une protection supplémentaire qui interdit l'accès à vos données, quand bien même des malfaiteurs seraient en possession du mot de passe et de l'identifiant de l'un de vos collaborateurs.                                                                                                                                                                 Vous disposez de 5 jours pour activer cette fonction.                                                                                                                                                                                                                                                                                                                                                                                                                                                          Pour configurer la nouvelle procédure de validation, il suffit de vous rendre sur la page 'paramètres de sécurité' de votre profil.  Si vous n'avez pas la possibilité de télécharger l'application sur votre mobile, veuillez nous l'indiquer en commentaire à ce message. N'hésitez pas, également à nous faire part de difficultés rencontrées  lors de l'activation de la validation en deux étapes.";
$MESS["CONFIG_MARKETPLACE_TITLE"] = "Migrer les données d'autres systèmes vers Bitrix24";
$MESS["CONFIG_MARKETPLACE_MORE"] = "En savoir plus";
$MESS["CONFIG_NAME_FORMAT"] = "Format du nom";
$MESS["CONFIG_CULTURE_OTHER"] = "Autre";
$MESS["CONFIG_ORGANIZATION"] = "Type d'entreprise";
$MESS["CONFIG_ORGANIZATION_DEF"] = "Société et employés";
$MESS["CONFIG_ORGANIZATION_PUBLIC"] = "Entreprise et utilisateurs";
$MESS["CONFIG_ORGANIZATION_GOV"] = "Entreprise et employés";
$MESS["CONFIG_URL_PREVIEW_ENABLE"] = "Activer les liens multimédias riches";
$MESS["CONFIG_DISK_VIEWER_SERVICE"] = "Afficher les documents avec";
$MESS["CONFIG_ALLOW_SELF_REGISTER"] = "Autoriser les inscriptions rapides";
$MESS["CONFIG_ALLOW_INVITE_USERS"] = "Permettre à tout le monde d'inviter de nouveaux utilisateurs à ce compte Bitrix24";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_JOIN"] = "Informer des nouvelles recrues dans le Chat général";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_LEAVE"] = "Informer des renvois d'employés/utilisateurs dans le Chat général";
$MESS["CONFIG_ALLOW_NEW_USER_LF"] = "Poster les nouveaux événements de recrutement d'employés dans le Flux d'activités";
$MESS["CONFIG_DISK_VERSION_LIMIT_PER_FILE"] = "Entrées max. dans l'historique de document";
$MESS["CONFIG_DISK_ALLOW_USE_EXTERNAL_LINK"] = "Autoriser les liens publics";
$MESS["CONFIG_DISK_OBJECT_LOCK_ENABLED"] = "Autoriser le verrouillage de document";
$MESS["CONFIG_DISK_LOCK_POPUP_TEXT"] = "Fonctionnalités avancées de Drive:

<br/><br/>
- Verrouillage des fichiers pour empêcher d'autres personnes d'éditer les documents sur lesquels vous travailler.
<br/><br/>
- Désactivation des liens publics pour empêcher d'autres personnes de partager publiquement les fichiers avec des utilisateurs extérieurs à votre compte Bitrix24
<br/><br/>
<a href=\"https://www.bitrix24.com/pro/drive.php\" target='_blank'>En savoir plus</a>
<br/><br/>
Les fonctionnalités avancées de Bitrix24.Drive sont disponibles pour les abonnements commerciaux à partir de l'offre Bitrix24 Standard.
";
$MESS["CONFIG_NETWORK_AVAILABLE"] = "Tous les utilisateurs de mon Bitrix24 peuvent communiquer dans un réseau global";
$MESS["CONFIG_NETWORK_AVAILABLE_NOT_CONFIRMED"] = "Cette fonctionnalité sera disponible lorsque l’administrateur aura confirmé le compte.";
$MESS["CONFIG_NETWORK_AVAILABLE_TITLE"] = "Disponible uniquement dans les abonnements commerciaux de Bitrix24";
$MESS["CONFIG_NETWORK_AVAILABLE_TEXT"] = "Vous pouvez désactiver la communication de vos employés dans Bitrix24.Network en passant à l'un des abonnements commerciaux.<br/><br/>
Désactiver Bitrix24.Network amène les changements suivants:<br/>
<ul>
<li>Les autres utilisateurs ne peuvent trouver aucun de vos employés dans Bitrix24.Network, ni les ajouter pour discuter.</li>
<li>Vos employés ne peuvent pas envoyer de message vers un autre Bitrix24.</li>
<li>Personne ne pourra voir les recommendations d'autres personnes utilisant Bitrix24.</li>
<li>Vos contacts ne pourront pas voir les recommendations sur le fait que vos employés utilisent Bitrix24.</li>
</ul>
<b>Pour désactiver Bitrix24.Network et utiliser d'autres fonctionnalités utiles, il vous suffit de passer à l'abonnement \"Plus\" pour seulement #PRICE#.</b>";
$MESS["CONFIG_NETWORK_AVAILABLE_TEXT_NEW"] = "La communication dans Bitrix24.Network est disponible pour les utilisateurs commerciaux de Bitrix24 uniquement.

Les avantages de connecter Bitrix24.Network sont les suivants : 

1.    Tous vos contacts et partenaires professionnels sont connectés dans un réseau 

2.    Communications rapides et pratiques avec les clients et les utilisateurs externes 

3.    Communications continues entre les utilisateurs de différents comptes Bitrix24 L'ensemble des outils de Bitrix24.

Network est disponible dès Bitrix24 Plus pour \$39 par mois.";
$MESS["CONFIG_DISK_LIMIT_LOCK_POPUP_TITLE"] = "Disponible uniquement dans la version étendue de Bitrix24.Drive";
$MESS["CONFIG_DISK_LIMIT_LOCK_POPUP_TEXT"] = "Profitez d'encore plus de fonctionnalités utiles Bitrix24 avec Advanced Drive:<br/><br/>
+ Historique de mise à jour des documents (modifié quand et par qui)<br/>
+ Récupération de toute version précédente d'un document depuis l'historique<br/><br/>
<a href=\"https://www.bitrix24.com/pro/drive.php\" target='_blank'>En savoir plus</a><br/><br/>
Advanced Drive est disponible dans l'abonnement \"Standard\" et dans les abonnements supérieurs.";
$MESS["CONFIG_NAME_CHANGE_SECTION"] = "Modifier l’adresse de votre Bitrix24";
$MESS["CONFIG_NAME_CHANGE_ACTION"] = "Modifier";
$MESS["CONFIG_NAME_CHANGE_INFO"] = "Remarque : vous ne pouvez modifier l'adresse de votre Bitrix24 qu’une seule fois.";
$MESS["CONFIG_DISK_LOCK_POPUP_TITLE"] = "Disponible uniquement dans la version avancée de Bitrix24 Drive";
$MESS["CONFIG_SMTP_TITLE"] = "Configuration e-mail";
$MESS["CONFIG_SMTP_HOST"] = "Serveur SMTP";
$MESS["CONFIG_SMTP_PORT"] = "Port SMTP";
$MESS["CONFIG_SMTP_USER"] = "Nom d’utilisateur SMTP";
$MESS["CONFIG_SMTP_PASSWORD"] = "Mot de passe de l'utilisateur";
$MESS["CONFIG_SMTP_REPEAT_PASSWORD"] = "Confirmer le mot de passe";
$MESS["CONFIG_SMTP_EMAIL"] = "Adresse e-mail";
$MESS["CONFIG_SMTP_TLS"] = "Utiliser TLS";
$MESS["CONFIG_SMTP_ERROR"] = "Erreur de configuration e-mail";
$MESS["CONFIG_DISC_ALLOW_EDIT_OBJECT_IN_UTF"] = "Permettre à tous les utilisateurs par défaut<br/> de pouvoir modifier les documents attachés aux discussions<br/>, tâches, commentaires ou autres événements. (Cette fonction peut être désactivée manuellement dans chacun des évenements)";
$MESS["CONFIG_SMTP_AUTH"] = "Utiliser l’authentification";
$MESS["CONFIG_BXENV_UPDATE"] = "La Machine virtuelle doit être mise à jour.";
$MESS["CONFIG_DISK_ALLOW_DOCUMENT_TRANSFORMATION"] = "Générer automatiquement des fichiers PDF et JPG pour documents";
$MESS["CONFIG_DISK_ALLOW_VIDEO_TRANSFORMATION"] = "Générer automatiquement des fichiers MP4 et JPG pour supports vidéo";
$MESS["CONFIG_DISK_TRANSFORM_FILES_ON_OPEN"] = "Convertir le fichier dès qu’il est ouvert";
$MESS["CONFIG_NAME_GOOGLE_API_KEY"] = "Préférences d'intégration de l'API Google";
$MESS["CONFIG_NAME_GOOGLE_API_KEY_FIELD"] = "API Google Maps pour clé d'intégration Bitrix24";
$MESS["CONFIG_NAME_GOOGLE_API_HOST_HINT"] = "La clé a été obtenue pour le domaine <b>#domain#</b>. Si vous n'arrivez pas à faire fonctionner Google Maps, changez les paramètres de la clé ou <a href=\"https://developers.google.com/maps/documentation/javascript/get-api-key\" target=\"_blank\"> obtenez une nouvelle clé</a>.";
$MESS["CONFIG_NAME_GOOGLE_API_KEY_HINT"] = "Une clé API Google est nécessaire pour utiliser les cartes. Pour obtenir votre clé, veuillez <a href=\"https://developers.google.com/maps/documentation/javascript/get-api-key\" target=\"_blank\">utiliser ce formulaire</a>.";
$MESS["CONFIG_PHONE_NUMBER_DEFAULT_COUNTRY"] = "Format du numéro de téléphone : pays par défaut";
?>