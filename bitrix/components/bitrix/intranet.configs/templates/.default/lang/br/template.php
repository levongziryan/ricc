<?
$MESS["CONFIG_HEADER_SETTINGS"] = "Configurações";
$MESS["CONFIG_COMPANY_NAME"] = "Nome da empresa";
$MESS["CONFIG_COMPANY_TITLE_NAME"] = "Nome da empresa para exibir no cabeçalho";
$MESS["CONFIG_SAVE_SUCCESSFULLY"] = "As configurações foram atualizadas com sucesso";
$MESS["CONFIG_SAVE"] = "Salvar";
$MESS["config_rating_label_likeY"] = "Não votado texto do botão \"Curtir\"";
$MESS["config_rating_label_likeN"] = "Votado texto do botão \"Curtir\"";
$MESS["CONFIG_EMAIL_FROM"] = "E-mail do site do administrador<br>(endereço padrão do remetente)";
$MESS["CONFIG_LOGO_24"] = "Adicionar \"24\" ao logotipo da empresa";
$MESS["CONFIG_WEBDAV_SERVICES_GLOBAL"] = "Ativar a edição de documentos por via externa<br/>serviços (Google Docs, MS Office Online e outros)";
$MESS["CONFIG_WEBDAV_SERVICES_LOCAL"] = "Permitir aos usuários individuais e grupos ativarem<br/>a edição de documentos via serviços externos";
$MESS["CONFIG_WEBDAV_ALLOW_AUTOCONNECT_SHARE_GROUP_FOLDER"] = "Conectar automaticamente o disco do grupo quando<br/>o usuário ingressa no grupo";
$MESS["CONFIG_DISK_ALLOW_EDIT_OBJECT_IN_UF"] = "Todos os usuários envolvidos podem editar documentos anexos a discussões, tarefas,
<br>
comentários ou outros eventos. Este comportamento padrão pode ser alterado
<br>
em quaisquer dos eventos individualmente.";
$MESS["CONFIG_DATE_FORMAT"] = "Formato da data";
$MESS["CONFIG_TIME_FORMAT"] = "Formato da hora";
$MESS["CONFIG_TIME_FORMAT_12"] = "12 horas";
$MESS["CONFIG_TIME_FORMAT_24"] = "24 horas";
$MESS["CONFIG_WEEK_START"] = "Primeiro dia da semana";
$MESS["DAY_OF_WEEK_0"] = "Domingo";
$MESS["DAY_OF_WEEK_1"] = "Segunda-feira";
$MESS["DAY_OF_WEEK_2"] = "Terça-feira";
$MESS["DAY_OF_WEEK_3"] = "Quarta-feira";
$MESS["DAY_OF_WEEK_4"] = "Quinta-feira";
$MESS["DAY_OF_WEEK_5"] = "Sexta-feira";
$MESS["DAY_OF_WEEK_6"] = "Sábado";
$MESS["CONFIG_CLIENT_LOGO"] = "Logotipo da empresa";
$MESS["CONFIG_CLIENT_LOGO_DESCR"] = "Seu logotipo deve ser umarquivo <b>PNG</b>.<br/>As dimensões máximas são 222px por 55px.";
$MESS["CONFIG_ADD_LOGO_BUTTON"] = "Carregar Logotipo";
$MESS["CONFIG_ADD_LOGO_DELETE"] = "Excluir Logotipo";
$MESS["CONFIG_ADD_LOGO_DELETE_CONFIRM"] = "Você tem certeza de que deseja excluir o logotipo?";
$MESS["CONFIG_ALLOW_TOALL"] = "Permitir \"Todos os funcionários\" como opção no Fluxo de Atividade";
$MESS["CONFIG_IM_CHAT_RIGHTS"] = "Permitir que os usuários enviem mensagens para o Bate-papo Geral";
$MESS["CONFIG_DEFAULT_TOALL"] = "Usar \"Todos os funcionários\" como destinatário padrão";
$MESS["CONFIG_TOALL_RIGHTS_ADD"] = "Adicionar";
$MESS["CONFIG_TOALL_RIGHTS"] = "Opção de configurações para \"Todos os funcionários\"";
$MESS["CONFIG_TOALL_DEL"] = "excluir";
$MESS["CONFIG_FEATURES_TITLE"] = "Serviços";
$MESS["CONFIG_FEATURES_EXTRANET"] = "Extranet";
$MESS["CONFIG_FEATURES_TIMEMAN"] = "Gerenciamento do tempo e relatórios de trabalho";
$MESS["CONFIG_FEATURES_MEETING"] = "Reuniões e informações";
$MESS["CONFIG_FEATURES_LISTS"] = "Listas";
$MESS["CONFIG_FEATURES_CRM"] = "Fluxos de Trabalho Administrativos";
$MESS["CONFIG_FEATURES_PROCESSES"] = "Fluxos de Trabalho Administrativos";
$MESS["CONFIG_IP_TITLE"] = "Restrições de IP (permitir acesso apenas de determinados endereços IP ou intervalos de endereços. Exemplo: 192.168.0,7; 192.168.0,1-192.168.0.100)";
$MESS["CONFIG_WORK_TIME"] = "Parâmetros de tempo de trabalho";
$MESS["CONFIG_WEEK_HOLIDAYS"] = "Dias de fim de semana";
$MESS["CAL_OPTION_FIRSTDAY_MO"] = "Segunda-feira";
$MESS["CAL_OPTION_FIRSTDAY_TU"] = "Terça-feira";
$MESS["CAL_OPTION_FIRSTDAY_WE"] = "Quarta-feira";
$MESS["CAL_OPTION_FIRSTDAY_TH"] = "Quinta-feira";
$MESS["CAL_OPTION_FIRSTDAY_FR"] = "Sexta-feira";
$MESS["CAL_OPTION_FIRSTDAY_SA"] = "Sábado";
$MESS["CAL_OPTION_FIRSTDAY_SU"] = "Domingo";
$MESS["CONFIG_YEAR_HOLIDAYS"] = "Fins de semana e feriados";
$MESS["CONFIG_HEADER_SECUTIRY"] = "Configurações de segurança";
$MESS["CONFIG_OTP_SECURITY"] = "Ativar autenticação de dois passos para todos os usuários";
$MESS["CONFIG_OTP_SECURITY2"] = "Torne a autorização de dois passos obrigatória para todos os usuários";
$MESS["CONFIG_OTP_SECURITY_SWITCH_OFF_INFO"] = "Observe que desmarcar esta opção não irá interromper o uso de senhas únicas pelos usuários que já possuem autenticação de dois passos ativada. 
<br/><br/>Eles ainda terão que usar OTP ao fazer login no Bitrix24.
<br/><br/>Você pode desativar a autenticação de dois passos num respectivo perfil de usuário.";
$MESS["CONFIG_OTP_SECURITY_DAYS"] = "Especificar o período de tempo dentro do qual todos os funcionários<br/>terão que ativar a autenticação de dois passos";
$MESS["CONFIG_OTP_SECURITY_INFO"] = "Desenvolvemos um procedimento de ativação da autenticação de dois passos fácil de usar que qualquer funcionário pode fazer sem o auxílio de especialistas.<br/><br/>Será enviada uma mensagem para cada funcionário, informando-os que eles terão que ativar a autenticação de dois passos dentro do período de tempo inserido. Os usuários que não fizerem não poderão mais assinar.";
$MESS["CONFIG_OTP_SECURITY_INFO2"] = "<br/>Para ativar a autenticação de duas etapas, o usuário deverá instalar um aplicativo de OTP Bitrix24 para seu celular. Este aplicativo pode ser baixado da AppStore ou GooglePlay.<br/><br/>
Os empregados que não tenham um dispositivo móvel adequado devem ser equipados com um hardware especial - eToken Pass. Existe uma gama de fornecedores dos quais você pode obtê-lo, por exemplo:
<a target=_blank href=\"http://www.safenet-inc.com/multi-factor-authentication/authenticators/pki-usb-authentication/etoken-pro/\">www.safenet-inc.com</a>,
<a target=_blank href=\"http://www.authguard.com/eToken-PASS.asp\">www.authguard.com</a>.
Encontrar outros fornecedores em <a target=_blank href=\"https://www.google.com/?q=buy+eToken+PASS&spell=1#safe=off&q=buy+eToken+PASS\">Google</a>.
<br/><br/>
Como opção, você pode desabilitar a autenticação em duas etapas para alguns dos empregados. No entanto, isso irá aumentar o risco de acesso não autorizado ao seu Bitrix24 se o login e a senha de um desses empregados forem roubados. Como administrador, você pode desabilitar a autenticação em dois passos para um empregado no perfil do usuário.";
$MESS["CONFIG_MORE"] = "Leia mais";
$MESS["CONFIG_OTP_POPUP_TITLE"] = "Que tal postar este aqui num Fluxo de Atividades?";
$MESS["CONFIG_OTP_POPUP_TEXT"] = "Aqui está o texto que você pode postar num Fluxo de Atividade<br/>para seus funcionários lerem.<br/><br/>Informe aos seus colegas sobre a autenticação de dois passos,<br/>procedimento de configuração e o novo método de autenticação<br/>que eles terão que usar para fazer login";
$MESS["CONFIG_OTP_POPUP_SHARE"] = "Compartilhar";
$MESS["CONFIG_OTP_POPUP_CLOSE"] = "Não, obrigado";
$MESS["CONFIG_OTP_ADMIN_IS_REQUIRED_INFO"] = "Antes de você migrar seus funcionários para o sistema de autenticação de dois passos, configure-o para sua conta primeiro.<br/><br/>Prossiga, ativando-o na Página de segurança do seu perfil.";
$MESS["CONFIG_OTP_IMPORTANT_TITLE"] = "Autenticação de dois passos";
$MESS["CONFIG_OTP_IMPORTANT_TEXT"] = "Hoje, você utiliza seu login e senha para entrar no seu Bitrix24. Os dados pessoais e de negócios estão protegidos por tecnologia de criptografia de dados. No entanto, existem ferramentas que uma pessoa mal-intencionada pode empregar para entrar no seu computador e roubar suas credenciais de login. Procurando proteger contra possíveis ameaças, nós ativamos uma função extra de segurança para o Bitrix24: autenticação em dois passos";
$MESS["CONFIG_MARKETPLACE_TITLE"] = "Migrar dados para Bitrix24 a partir de outros sistemas";
$MESS["CONFIG_MARKETPLACE_MORE"] = "Saiba mais";
$MESS["CONFIG_NAME_FORMAT"] = "Formato do nome";
$MESS["CONFIG_CULTURE_OTHER"] = "Outros";
$MESS["CONFIG_ORGANIZATION"] = "Tipo da sua organização";
$MESS["CONFIG_ORGANIZATION_DEF"] = "Empresa e empregados";
$MESS["CONFIG_ORGANIZATION_PUBLIC"] = "Organização e usuários";
$MESS["CONFIG_ORGANIZATION_GOV"] = "Organização e empregados";
$MESS["CONFIG_URL_PREVIEW_ENABLE"] = "Permitir links rich media";
$MESS["CONFIG_DISK_VIEWER_SERVICE"] = "Exibir documentos usando";
$MESS["CONFIG_ALLOW_SELF_REGISTER"] = "Permitir cadastro rápido";
$MESS["CONFIG_ALLOW_INVITE_USERS"] = "Permitir a todos convidar novos usuários para esta conta Bitrix24";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_JOIN"] = "Notificar novas contratações no bate-papo geral";
$MESS["CONFIG_IM_GENERSL_CHAT_MESSAGE_LEAVE"] = "Notificar dispensa de empregado/usuário no bate-papo geral";
$MESS["CONFIG_ALLOW_NEW_USER_LF"] = "Postar eventos de contratação de novo empregado em Fluxo de Atividade";
$MESS["CONFIG_DISK_VERSION_LIMIT_PER_FILE"] = "Máx. de entradas no histórico do documento";
$MESS["CONFIG_DISK_ALLOW_USE_EXTERNAL_LINK"] = "Permitir links públicos";
$MESS["CONFIG_DISK_OBJECT_LOCK_ENABLED"] = "Permitir bloqueio de documento";
$MESS["CONFIG_DISK_LOCK_POPUP_TEXT"] = "Recursos do Drive avançado:

<br/><br/>
- Bloquear arquivos para impedir que outros possam editar documentos que você está trabalhando no momento.
<br/><br/>
- Desabilitar links públicos para evitar que outros possam compartilhar arquivos publicamente com usuários fora da sua conta Bitrix24
<br/><br/>
<a href=\"Https://www.bitrix24.com/pro/drive.php\" target=\"_blank\">Saiba mais</a>
<br/><br/>
Recursos avançados Bitrix24.Drive estão disponíveis em assinaturas comerciais a partir do plano padrão Bitrix24.
";
$MESS["CONFIG_NETWORK_AVAILABLE"] = "Todos os usuários do meu Bitrix24 podem se comunicar na rede global";
$MESS["CONFIG_NETWORK_AVAILABLE_NOT_CONFIRMED"] = "Esse recurso estará disponível uma vez que o administrador confirmar a conta.";
$MESS["CONFIG_NETWORK_AVAILABLE_TITLE"] = "Disponível apenas em planos comerciais do Bitrix24";
$MESS["CONFIG_NETWORK_AVAILABLE_TEXT"] = "Você pode desativar a comunicação de seus funcionários em Bitrix24.Network ao migrar para um dos planos comerciais.<br/><br/>
Desativar Bitrix24.Network traz as seguintes alterações:<br/>
 <ul>
 <li>Outros usuários não podem encontrar qualquer um de seus funcionários no Bitrix24.Network e adicioná-los ao bate-papo.</li>
 <li>Seus funcionários não podem enviar mensagens para outro Bitrix24.</li>
 <li>Ninguém vai ver recomendações de outras pessoas usando Bitrix24.</li>
 <li>Seus contatos não vão ver recomendações dizendo que seus funcionários estão usando Bitrix24.</li>
</ul>
 <b>Para desativar Bitrix24.Network e usar outros recursos úteis, faça o upgrade para o plano \"Plus\" por apenas #PRICE#.</b>";
$MESS["CONFIG_NETWORK_AVAILABLE_TEXT_NEW"] = "A comunicação na Bitrix24.Network está disponível apenas para usuários comerciais Bitrix24.

Aqui estão as vantagens de conectar a Bitrix24.Network:

1.    Todos os seus contatos e parceiros de negócios estão conectados em uma única rede

2.    Comunicação rápida e conveniente com clientes e usuários externos

3.    Comunicações contínuas entre usuários em diferentes contas Bitrix24

Todas as ferramentas Bitrix24.Network estão disponíveis a partir do Bitrix24 Plus por \$39 ao mês.";
$MESS["CONFIG_DISK_LIMIT_LOCK_POPUP_TITLE"] = "Disponível apenas em Bitrix24.Drive prolongado";
$MESS["CONFIG_DISK_LIMIT_LOCK_POPUP_TEXT"] = "Desfrute de recursos Bitrix24 ainda mais úteis com Unidade Avançada:<br/><br/>
 + Histórico de atualização de documentos (modificado quando e por quem)<br/>
 + Recuperação de qualquer versão de documento anterior do histórico<br/><br/>
 <a href=\"https://www.bitrix24.com/pro/drive.php\" target=\"_blank\">Saiba mais</a><br/><br/>Unidade Avançada está disponível no plano \"Standard\" e acima.";
$MESS["CONFIG_NAME_CHANGE_SECTION"] = "Altere seu endereço Bitrix24";
$MESS["CONFIG_NAME_CHANGE_ACTION"] = "Alterar";
$MESS["CONFIG_NAME_CHANGE_INFO"] = "Nota: você pode alterar seu endereço Bitrix24 apenas uma vez.";
$MESS["CONFIG_DISK_LOCK_POPUP_TITLE"] = "Disponível apenas no Bitrix24 Drive avançado";
$MESS["CONFIG_SMTP_TITLE"] = "Confirmação de e-mail";
$MESS["CONFIG_SMTP_HOST"] = "Servidor SMTP";
$MESS["CONFIG_SMTP_PORT"] = "Porta SMTP";
$MESS["CONFIG_SMTP_USER"] = "Login de usuário SMTP";
$MESS["CONFIG_SMTP_PASSWORD"] = "Senha do usuário";
$MESS["CONFIG_SMTP_REPEAT_PASSWORD"] = "Confirmar senha";
$MESS["CONFIG_SMTP_EMAIL"] = "Endereço de e-mail";
$MESS["CONFIG_SMTP_TLS"] = "Usar TLS";
$MESS["CONFIG_SMTP_ERROR"] = "Erro de configuração de e-mail";
$MESS["CONFIG_DISC_ALLOW_EDIT_OBJECT_IN_UTF"] = "Todos os usuários envolvidos podem editar documentos anexos a discussões, tarefas,<br/>comentários ou outros eventos. Este comportamento padrão pode ser alterado<br/>em quaisquer dos eventos individualmente.";
$MESS["CONFIG_SMTP_AUTH"] = "Usar autenticação";
$MESS["CONFIG_BXENV_UPDATE"] = "O Aparelho Virtual precisa ser atualizado.";
$MESS["CONFIG_DISK_ALLOW_DOCUMENT_TRANSFORMATION"] = "Gerar automaticamente arquivos PDF e JPG para documentos";
$MESS["CONFIG_DISK_ALLOW_VIDEO_TRANSFORMATION"] = "Gerar automaticamente arquivos MP4 e JPG para mídia de vídeo";
$MESS["CONFIG_DISK_TRANSFORM_FILES_ON_OPEN"] = "Converter arquivo assim que aberto";
$MESS["CONFIG_NAME_GOOGLE_API_KEY"] = "Preferências de integração API Google";
$MESS["CONFIG_NAME_GOOGLE_API_KEY_FIELD"] = "API Google Maps para chave de integração Bitrix24";
$MESS["CONFIG_NAME_GOOGLE_API_HOST_HINT"] = "A chave foi obtida para o domínio <b>#domain#</b>. Se você não conseguir fazer o Google Maps funcionar, altere as configurações da chave ou <a href=\"https://developers.google.com/maps/documentation/javascript/get-api-key\" target=\"_blank\">pegue uma nova</a>.";
$MESS["CONFIG_NAME_GOOGLE_API_KEY_HINT"] = "É necessária uma chave API Google para usar mapas. Para obter a sua chave, <a href=\"https://developers.google.com/maps/documentation/javascript/get-api-key\" target=\"_blank\">utilize este formulário</a>.";
?>