<?
$MESS["CONFIG_EMAIL_ERROR"] = "L'adresse mail de l'administrateur du site est incorrecte.";
$MESS["CONFIG_IP_ERROR"] = "Les adresses IP sont incorrectes.";
$MESS["CONFIG_FORMAT_NAME_ERROR"] = "Le format du nom est incorrect. Les macros suivantes sont possibles : #TITLE#, #NAME#, #LAST_NAME#, #SECOND_NAME#, #NAME_SHORT#, #LAST_NAME_SHORT#, #SECOND_NAME_SHORT#, #EMAIL#";
$MESS["DISK_VERSION_LIMIT_PER_FILE_UNLIMITED"] = "Illimité";
$MESS["CONFIG_ACCESS_DENIED"] = "Accès interdit";
$MESS["CONFIG_SMTP_PASS_ERROR"] = "Les mots de passe du serveur SMTP indiqués ne correspondent pas.";
?>