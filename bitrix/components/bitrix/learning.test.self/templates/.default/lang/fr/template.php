<?
$MESS["LEARNING_INPUT_ANSWER"] = "Entrer la réponse";
$MESS["LEARNING_QUESTION_S"] = "Question";
$MESS["LEARNING_SELECT_ANSWER"] = "Choisissez une réponse";
$MESS["LEARNING_ENABLE_JAVASCRIPT"] = "Pour faire le test vous devez activer Javascript.";
$MESS["LEARNING_QUESTION_FROM"] = "de";
$MESS["LEARNING_SUBMIT_ANSWER"] = "Répondre";
$MESS["INCORRECT_QUESTION_MESSAGE"] = "Erreur";
?>