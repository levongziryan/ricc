<?
$MESS["LEARNING_DESC_YES"] = "Oui";
$MESS["LEARNING_COURSE_ID"] = "Identificateur du cours";
$MESS["LEARNING_LESSON_ID_NAME"] = "Identifiant de la leçon";
$MESS["LEARNING_DESC_NO"] = "Non";
$MESS["LEARNING_CHECK_PERMISSIONS"] = "Vérifier le droit d'accès";
?>