<?
$MESS["SMOB_BARCODE_SCAN"] = "Scan";
$MESS["SMOB_CHECK_ERROR"] = "Comprobar errores del código de barras";
$MESS["SMOB_SCAN_ERROR"] = "Error de escaneado del código de barras";
$MESS["SMOB_STORE_NAME_TMPL"] = "Almacén: \"##STORE_NAME##\" (##STORE_ID##) ";
$MESS["SMOB_PRODUCT_NAME"] = "Nombre del producto";
$MESS["SMOB_BUTTON_CHECK"] = "Comprobar";
$MESS["SMOB_BUTTON_SAVE"] = "Guardar";
$MESS["SMOB_BUTTON_BACK"] = "Volver";
$MESS["SMOB_BARCODE_ENTER"] = "Introduzca el código de barras";
?>