<?
$MESS["VOXIMPLANT_ROLE_NOT_FOUND"] = "Rôle introuvable. Remplissez le formulaire pour créer un nouveau role.";
$MESS["VOXIMPLANT_ROLE_SAVE_ERROR"] = "Erreur d'enregistrement du rôle.";
$MESS["VOXIMPLANT_ROLE_ERROR_EMPTY_NAME"] = "Le nom du rôle n'est pas spécifié";
$MESS["VOXIMPLANT_ROLE_ERROR_INSUFFICIENT_RIGHTS"] = "Autorisations d'accès insuffisantes";
$MESS["VOXIMPLANT_ROLE_LICENSE_ERROR"] = "Erreur d'enregistrement du rôle.";
?>