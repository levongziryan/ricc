<?
$MESS["VOXIMPLANT_ROLE_NOT_FOUND"] = "No se ha encontrado el rol. Complete el formulario para crear un nuevo rol.";
$MESS["VOXIMPLANT_ROLE_SAVE_ERROR"] = "Error al guardar el rol.";
$MESS["VOXIMPLANT_ROLE_ERROR_EMPTY_NAME"] = "Nombre del rol no se especifica";
$MESS["VOXIMPLANT_ROLE_ERROR_INSUFFICIENT_RIGHTS"] = "Permisos de accesos induficientes";
$MESS["VOXIMPLANT_ROLE_LICENSE_ERROR"] = "Error al guardar el rol.";
?>