<?
$MESS["DISK_FOLDER_TOOLBAR_EXTERNAL_LINK_LIST_GO_BACK_TITLE"] = "Revenir aux fichiers et dossiers";
$MESS["DISK_FOLDER_TOOLBAR_FOLDER_LIST_TEXT"] = "Tous";
$MESS["DISK_FOLDER_TOOLBAR_FOLDER_LIST_TITLE"] = "Afficher tous les fichiers et dossiers";
$MESS["DISK_FOLDER_TOOLBAR_TRASHCAN_TITLE"] = "Afficher les fichiers et dossiers supprimés";
$MESS["DISK_FOLDER_TOOLBAR_EXTERNAL_LINK_LIST_TITLE"] = "Afficher les fichiers ayant des liens publics";
$MESS["DISK_FOLDER_TOOLBAR_UPLOAD_FILE_TEXT"] = "Chargement";
$MESS["DISK_FOLDER_TOOLBAR_UPLOAD_FILE_TITLE"] = "Télécharger les nouveaux documents dans dossier";
$MESS["DISK_FOLDER_TOOLBAR_TRASHCAN_TEXT_2"] = "Corbeille";
$MESS["DISK_FOLDER_TOOLBAR_EXTERNAL_LINK_LIST_GO_BACK_TEXT"] = "Précédent";
$MESS["DISK_FOLDER_TOOLBAR_ERROR_COULD_NOT_FIND_OBJECT"] = "Impossible de  trouver l'objet.";
$MESS["DISK_FOLDER_TOOLBAR_EXTERNAL_LINK_LIST_TEXT"] = "Année de publication";
$MESS["DISK_FOLDER_TOOLBAR_EMPTY_TRASHCAN_TEXT"] = "Vider la corbeille";
$MESS["DISK_FOLDER_TOOLBAR_EXTERNAL_LINK_LIST_TEXT_2"] = "Fichiers publics";
$MESS["DISK_FOLDER_TOOLBAR_CREATE_FOLDER_TITLE"] = "Créer un dossier joint";
$MESS["DISK_FOLDER_TOOLBAR_CREATE_DOC_TEXT"] = "Création d'un nouveau document";
$MESS["DISK_FOLDER_TOOLBAR_CREATE_DOC_TITLE"] = "Création d'un nouveau document";
$MESS["DISK_FOLDER_TOOLBAR_CREATE_FOLDER_TEXT"] = "Ajouter un dossier";
$MESS["DISK_FOLDER_TOOLBAR_TRASHCAN_TEXT"] = "Supprimé";
$MESS["DISK_FOLDER_TOOLBAR_EMPTY_TRASHCAN_TITLE"] = "Supprimer définitivement tous les fichiers et dossiers de la corbeille";
?>