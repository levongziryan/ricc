<?
$MESS["B_B_MES_NO_BLOG"] = "Le blog n'est pas trouvé.";
$MESS["BPE_SESS"] = "Votre session est expirée. S'il vous plaît, sauvegardez le message encore une fois.";
$MESS["BPE_HIDDEN_POSTED"] = "Votre abonnement a été supprimé.";
$MESS["BLOG_P_INSERT"] = "Cliquez pour insérer l'image";
$MESS["BLOG_MODULE_NOT_INSTALL"] = "Le module 'Blogs' n'a pas été installé.";
$MESS["IDEA_MODULE_NOT_INSTALL"] = "Le module des idées n'a pas été installé.";
$MESS["IDEA_NEW_MESSAGE_SUCCESS"] = "Un nouveau message a été ajouté avec succès.";
$MESS["IDEA_NEW_MESSAGE"] = "Offrir";
$MESS["BLOG_POST_EDIT"] = "Edition de l'idée '#IDEA_TITLE#'";
$MESS["BLOG_ERR_NO_RIGHTS"] = "S'il vous plaît <a href='#' onclick='authFormWindow.ShowLoginForm();'>autoriser</a> à ajouter une idée.";
?>