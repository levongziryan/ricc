<?
$MESS["CRM_TAB_1"] = "Opportunité";
$MESS["CRM_TAB_1_TITLE"] = "Propriétés de la transaction";
$MESS["CRM_TAB_2"] = "evènements";
$MESS["CRM_TAB_2_TITLE"] = "evènements";
$MESS["CRM_TAB_3"] = "De processus business";
$MESS["CRM_TAB_3_TITLE"] = "Processus d'affaires de la transaction";
$MESS["CRM_DEAL_EDIT_TITLE"] = "Marché ##ID# &mdash; #TITLE#";
$MESS["CRM_DEAL_CREATE_TITLE"] = "Création de transaction";
$MESS["CRM_DEAL_CONV_ACCESS_DENIED"] = "Permissions insuffisantes pour créer des factures et des devis.";
$MESS["CRM_DEAL_CONV_GENERAL_ERROR"] = "Erreur générique.";
$MESS["CRM_DEAL_CONV_DIALOG_TITLE"] = "Créer une entité de transaction";
$MESS["CRM_DEAL_CONV_DIALOG_CONTINUE_BTN"] = "Continuer";
$MESS["CRM_DEAL_CONV_DIALOG_CANCEL_BTN"] = "Annuler";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_LEGEND"] = "Les entités sélectionnées ne disposent pas de champ où placer les données d'une transaction. Veuillez choisir les entités pour y créer des champs supplémentaires afin d'enregistrer toutes les informations disponibles. ";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_FILED_LIST_TITLE"] = "Quels champs seront créés";
$MESS["CRM_DEAL_CONV_DIALOG_SYNC_ENTITY_LIST_TITLE"] = "Choisissez les entités pour y créer des champs supplémentaires";
$MESS["CRM_DEAL_RECUR_SHOW_TITLE"] = "Modèle de transaction ##ID# &mdash; #TITLE#";
?>