<?
$MESS["CRM_REPORT_VAR"] = "Variable der Berichts-ID";
$MESS["CRM_ACTION_VAR"] = "Variable der Aktions-ID";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "Vorlage des Pfads zur Startseite";
$MESS["CRM_SEF_PATH_TO_REPORT"] = "Vorlage des Pfads zur Seite der Berichte";
$MESS["CRM_SEF_PATH_TO_CONSTRUCT"] = "Vorlage des Pfads zur Seite der Berichtsbearbeitung";
$MESS["CRM_SEF_PATH_TO_VIEW"] = "Vorlage des Pfads zur Seite der Berichtsansicht";
$MESS["CRM_REPORT_ID"] = "Berichts-ID";
?>