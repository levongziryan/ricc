<?
$MESS["CRM_REPORT_ID"] = "ID du rapport";
$MESS["CRM_ACTION_VAR"] = "Nom de la variable de l'ID de l'action";
$MESS["CRM_REPORT_VAR"] = "Nom de la valeur de l'identificateur du rapport";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "Modèle de chemin d'accès à la page principale";
$MESS["CRM_SEF_PATH_TO_VIEW"] = "Modèle de chemin d'accès à la page de visualisation du rapport";
$MESS["CRM_SEF_PATH_TO_CONSTRUCT"] = "Modèle de chemin d'accès à la page d'édition d'un rapport";
$MESS["CRM_SEF_PATH_TO_REPORT"] = "Modèle de chemin d'accès à la page de la liste des rapports";
?>