<?
$MESS["CRM_REPORT_VAR"] = "ID de la variable del reporte ";
$MESS["CRM_ACTION_VAR"] = "ID de la variable de la acción";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "Plantilla de ruta a la página de índice ";
$MESS["CRM_SEF_PATH_TO_REPORT"] = "Plantilla de ruta a la página de reportes";
$MESS["CRM_SEF_PATH_TO_CONSTRUCT"] = "Plantilla de ruta de la página de edición";
$MESS["CRM_SEF_PATH_TO_VIEW"] = "Plantilla de ruta a la página de vista del reporte";
$MESS["CRM_REPORT_ID"] = "ID del reporte";
?>