<?
$MESS["CRM_NUMBER_DATE_3"] = "Annuellement";
$MESS["CRM_NUMBER_DATE_1"] = "Chaque jour";
$MESS["CRM_NUMBER_DATE_2"] = "A terme mensuel";
$MESS["CRM_NUMBER_RANDOM"] = "Nombre de symboles:";
$MESS["CRM_NUMBER_NUMBER"] = "Nombre initial:";
$MESS["CRM_NUMBER_PREFIX_DESC"] = "De 1 à 7 symboles (caractères latins, chiffres, tirets, tiret bas). Exemple de numéro avec préfixe: TEST_1234";
$MESS["CRM_NUMBER_NUMBER_DESC"] = "De 1 à 7 symboles. La nouvelle valeur doit être supérieure à celle précédente.";
$MESS["CRM_NUMBER_DATE"] = "Période:";
$MESS["CRM_NUMBER_PREFIX"] = "Préfix:";
$MESS["CRM_NUMBER_WARNING"] = "Préfix:";
$MESS["CRM_NUMBER_TEMPLATE_EXAMPLE"] = "Exemple:";
$MESS["CRM_NUMBER_TEMPL"] = "Modèle:";
?>