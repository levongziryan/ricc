<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado.";
$MESS["CRM_NUMBER_PREFIX_WARNING"] = "El prefijo de numeración \"#PREFIX#\" no es válido.";
$MESS["CRM_NUMBER_NUMBER_WARNING"] = "El primer número de la orden \"#NUMBER#\" no es válido.";
?>