<?
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado.";
$MESS["CRM_NUMBER_PREFIX_WARNING"] = "O prefixo de numeração \"#PREFIX#\" é inválido.";
$MESS["CRM_NUMBER_NUMBER_WARNING"] = "O número inicial \"#NUMBER#\" da ordem é inválido.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
?>