<?
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado.";
$MESS["CRM_NUMBER_TEMPLATE_0"] = "Não utiizado";
$MESS["CRM_NUMBER_TEMPLATE_1"] = "Iniciar a numeração de";
$MESS["CRM_NUMBER_TEMPLATE_2"] = "Usar prefixo";
$MESS["CRM_NUMBER_TEMPLATE_3"] = "Número aleatório";
$MESS["CRM_NUMBER_TEMPLATE_5"] = "Reiniciar numeração periodicamente";
$MESS["CRM_NUMBER_TEMPLATE_4"] = "ID do usuário e número do documento";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
?>