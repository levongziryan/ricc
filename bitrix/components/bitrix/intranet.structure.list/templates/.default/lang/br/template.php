<?
$MESS["ISL_ID"] = "ID";
$MESS["ISL_FULL_NAME"] = "Nome Completo";
$MESS["ISL_NAME"] = "Primeiro Nome";
$MESS["ISL_LAST_NAME"] = "Sobrenome";
$MESS["ISL_SECOND_NAME"] = "Segundo Nome";
$MESS["ISL_EMAIL"] = "E-mail";
$MESS["ISL_LOGIN"] = "Login";
$MESS["ISL_DATE_REGISTER"] = "Registrado em:";
$MESS["ISL_PERSONAL_WWW"] = "Web Site";
$MESS["ISL_PERSONAL_PROFESSION"] = "Título do Cargo";
$MESS["ISL_PERSONAL_ICQ"] = "ICQ";
$MESS["ISL_PERSONAL_GENDER"] = "Sexo";
$MESS["ISL_PERSONAL_BIRTHDAY"] = "Data de Nascimento";
$MESS["ISL_PERSONAL_PHOTO"] = "Foto";
$MESS["ISL_PERSONAL_PHONE"] = "Telefone";
$MESS["ISL_PERSONAL_FAX"] = "Fax";
$MESS["ISL_PERSONAL_MOBILE"] = "Móvel";
$MESS["ISL_PERSONAL_PAGER"] = "Paginador";
$MESS["ISL_PERSONAL_PHONES"] = "Telefones";
$MESS["ISL_PERSONAL_POST_ADDRESS"] = "Endereço para Correspondência";
$MESS["ISL_PERSONAL_CITY"] = "Cidade";
$MESS["ISL_PERSONAL_STREET"] = "Endereço";
$MESS["ISL_PERSONAL_STATE"] = "Região/Estado";
$MESS["ISL_PERSONAL_MAILBOX"] = "Caixa Postal";
$MESS["ISL_PERSONAL_ZIP"] = "CEP";
$MESS["ISL_PERSONAL_COUNTRY"] = "País";
$MESS["ISL_PERSONAL_NOTES"] = "Observações";
$MESS["ISL_WORK_POSITION"] = "Cargo";
$MESS["ISL_WORK_COMPANY"] = "Empresa";
$MESS["ISL_WORK_PHONE"] = "Telefone (trabalho)";
$MESS["ISL_ADMIN_NOTES"] = "Notas do Administrador";
$MESS["ISL_XML_ID"] = "ID Externo";
$MESS["INTR_ISL_TPL_GENDER_F"] = "F";
$MESS["INTR_ISL_TPL_GENDER_M"] = "M";
$MESS["INTR_ISL_TPL_NOTE_UNFILTERED"] = "Especificar os critérios de pesquisa.";
$MESS["INTR_ISL_TPL_NOTE_NULL"] = "Sua busca não encontrou nenhum colaborador.";
$MESS["INTR_ISP_PM"] = "Enviar mensagem";
$MESS["INTR_ISP_VIDEO_CALL"] = "Chamada de vídeo";
$MESS["INTR_ISP_IS_ONLINE"] = "Online";
$MESS["INTR_ISP_IS_ABSENT"] = "Fora do escritório";
$MESS["INTR_ISP_IS_BIRTHDAY"] = "Aniversário";
$MESS["INTR_ISP_IS_FEATURED"] = "Destacado";
$MESS["INTR_ISP_EDIT_USER"] = "Editar";
$MESS["ISL_WORK_FAX"] = "Fax (trabalho)";
?>