<?
$MESS["INTR_ISL_PARAM_SHOW_DEP_HEAD_ADDITIONAL"] = "Afficher le chef au cours du filtrage par subdivision";
$MESS["INTR_ISL_PARAM_FILTER_1C_USERS"] = "N'afficher que les utilisateurs qui sont synchronisés avec 1C";
$MESS["INTR_ISL_PARAM_SHOW_ERROR_ON_NULL"] = "Afficher la notification quand la liste est vide";
$MESS["INTR_ISL_PARAM_FILTER_NAME"] = "Dénomination du filtre";
$MESS["INTR_ISL_PARAM_USERS_PER_PAGE"] = "Nombre d'utilisateurs par page";
$MESS["INTR_ISL_PARAM_NAME_TEMPLATE"] = "Affichage du nom";
$MESS["INTR_ISL_GROUP_FILTER"] = "Paramètres du filtre";
$MESS["INTR_ISL_PARAM_NAV_TITLE"] = "Signature de la pagination";
$MESS["INTR_ISL_PARAM_DISPLAY_USER_PHOTO"] = "Afficher les photos de l'utilisateur";
$MESS["INTR_ISL_PARAM_SHOW_NAV_TOP"] = "Afficher la navigation page par page au-dessus de la liste";
$MESS["INTR_ISL_PARAM_SHOW_NAV_BOTTOM"] = "Afficher la navigation page par page au-dessous de la liste";
$MESS["INTR_ISL_PARAM_SHOW_UNFILTERED_LIST"] = "Afficher la liste quand le filtre est vide";
$MESS["INTR_ISL_PARAM_FILTER_SECTION_CURONLY_VALUE_Y"] = "direct";
$MESS["INTR_ISL_PARAM_FILTER_SECTION_CURONLY_VALYE_N"] = "Récurrent";
$MESS["INTR_ISL_PARAM_NAV_TITLE_DEFAULT"] = "Employés";
$MESS["INTR_ISL_PARAM_DETAIL_URL"] = "Page de l'examen détaillé";
$MESS["INTR_ISL_PARAM_FILTER_SECTION_CURONLY"] = "Filtre par service";
?>