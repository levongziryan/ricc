<?
$MESS["IDEA_STATUS_PROCESSING"] = "En cours d'exécution";
$MESS["IDEA_STATUS_COMPLETED"] = "Mis en uvre";
$MESS["IDEA_MES_SHOW_POST_CONFIRM"] = "tes-vous sûr de vouloir afficher ce message?";
$MESS["BLOG_MES_HIDE_POST_CONFIRM"] = "tes-vous sûr de vouloir cacher ce message?";
$MESS["BLOG_MES_DELETE_POST_CONFIRM"] = "tes-vous sûr de vouloir supprimer le message?";
$MESS["IDEA_POST_COMMENT_CNT"] = "commentaires";
$MESS["IDEA_STATUS_NEW"] = "Créer";
$MESS["IDEA_POST_UNSUBSCRIBE"] = "Se désabonner";
$MESS["IDEA_POST_SUBSCRIBE"] = "Abonnement";
$MESS["IDEA_MES_SHOW"] = "Afficher";
$MESS["IDEA_INTRODUCED_TITLE"] = "Proposé par";
$MESS["BLOG_BLOG_BLOG_EDIT"] = "Editer";
$MESS["IDEA_RATING_TITLE"] = "Classement";
$MESS["BLOG_MES_HIDE"] = "Cacher";
$MESS["BLOG_BLOG_BLOG_NO_AVAIBLE_MES"] = "Cette catégorie n'a pas d'idées.";
$MESS["BLOG_BLOG_BLOG_DELETE"] = "Supprimer";
$MESS["IDEA_POST_DUPLICATE"] = "Double idée <a href='#LINK#'>proposée auparavant</a>";
?>