<?
$MESS["M_CRM_COMPANY_EDIT_CREATE_TITLE"] = "Agregar compañía";
$MESS["M_CRM_COMPANY_EDIT_CANCEL_BTN"] = "Cancelar";
$MESS["M_CRM_COMPANY_EDIT_SAVE_BTN"] = "Guardar";
$MESS["M_CRM_COMPANY_EDIT_CONTINUE_BTN"] = "Continuar";
$MESS["M_CRM_COMPANY_EDIT_EDIT_TITLE"] = "Editar";
$MESS["M_CRM_COMPANY_EDIT_VIEW_TITLE"] = "Vista de compañía";
$MESS["M_CRM_COMPANY_EDIT_CONVERT_TITLE"] = "Compañía";
$MESS["M_DETAIL_LOAD_TEXT"] = "Actualizando...";
$MESS["M_CRM_COMPANY_MENU_EDIT"] = "Editar";
$MESS["M_CRM_COMPANY_MENU_DELETE"] = "Eliminar";
$MESS["M_CRM_COMPANY_CONVERSION_NOTIFY"] = "Campos requeridos";
$MESS["M_CRM_COMPANY_MENU_HISTORY"] = "Historial";
$MESS["M_CRM_COMPANY_MENU_ACTIVITY"] = "Actividades";
$MESS["M_CRM_COMPANY_MENU_DEALS"] = "Negociaciones";
$MESS["M_DETAIL_PULL_TEXT"] = "Pulse hacia abajo para actualizar...";
$MESS["M_DETAIL_DOWN_TEXT"] = "Suéltelo para actualizar...";
?>