<?
$MESS["TASKS_FILTER_BUILDER_PRESET_STATUS_ACTIVE"] = "Actif(ve)";
$MESS["TASKS_FILTER_BUILDER_PRESET_PRIORITY_HIGH"] = "Augmenté";
$MESS["TASKS_FILTER_BUILDER_PRESET_CLOSER_NAME"] = "A terminé la tâche";
$MESS["TASKS_FILTER_BUILDER_PRESET_PRIORITY_LOW"] = "Bas";
$MESS["MB_TASKS_TASKS_FILTER_PULLDOWN_LOADING"] = "Renouvellement de données...";
$MESS["TASKS_FILTER_BUILDER_PRESET_RESPONSIBLE_NAME"] = "Responsable";
$MESS["TASKS_FILTER_BUILDER_PRESET_STATUS_DECLINED"] = "Est rejetée";
$MESS["TASKS_FILTER_BUILDER_PRESET_STATUS_DEFERRED"] = "Différé";
$MESS["MB_TASKS_TASKS_FILTER_PULLDOWN_DOWN"] = "Relâcher pour mettre à jour";
$MESS["TASKS_FILTER_BUILDER_PRESET_ORIGINATOR_NAME"] = "Créé par";
$MESS["MB_TASKS_TASKS_FILTER_PULLDOWN_PULL"] = "Tirer pour mettre à jour";
$MESS["TASKS_FILTER_BUILDER_PRESET_PRIORITY_NAME"] = "criticité";
$MESS["TASKS_FILTER_BUILDER_PRESET_STATUS_EXPIRED"] = "...périmé(e)s";
$MESS["TASKS_FILTER_BUILDER_PRESET_PRIORITY_MIDDLE"] = "Moyen";
$MESS["TASKS_FILTER_BUILDER_PRESET_STATUS_NAME"] = "Statut";
?>