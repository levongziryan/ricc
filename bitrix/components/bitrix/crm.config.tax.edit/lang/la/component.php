<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_MODULE_SALE_NOT_INSTALLED"] = "El módulo e-Store no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_TAX_NOT_FOUND"] = "No se encontró el impuesto.";
$MESS["CRM_TAX_SECTION_MAIN"] = "Impuesto";
$MESS["CRM_TAX_FIELD_ID"] = "ID";
$MESS["CRM_TAX_FIELD_TIMESTAMP_X"] = "Modificado el ";
$MESS["CRM_TAX_FIELD_NAME"] = "Nombre";
$MESS["CRM_TAX_FIELD_LID"] = "Sitio Web";
$MESS["CRM_TAX_FIELD_CODE"] = "Código mnemotécnico";
$MESS["CRM_TAX_FIELD_DESCRIPTION"] = "Descripción";
$MESS["CRM_TAX_ADD_UNKNOWN_ERROR"] = "Error al crear un impuesto.";
$MESS["CRM_TAX_UPDATE_UNKNOWN_ERROR"] = "Error al actualizar un impuesto.";
$MESS["CRM_TAX_DELETE_UNKNOWN_ERROR"] = "Error al eliminar el impuesto";
$MESS["CRM_TAX_RATE_LIST"] = "Tasa de impuesto";
$MESS["CRM_TAX_RATE_ADD"] = "Agregar tasa de impuesto";
$MESS["CRM_TAX_RATE_ADD_TITLE"] = "Agregar tasa de impuesto";
?>