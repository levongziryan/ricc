<?
$MESS["CRM_TAX_UPDATE_UNKNOWN_ERROR"] = "Lors de la mise à jour du montant de l'impôt une erreur s'est produite.";
$MESS["CRM_TAX_ADD_UNKNOWN_ERROR"] = "Erreur survenue au cours de la création de l'impôt.";
$MESS["CRM_TAX_DELETE_UNKNOWN_ERROR"] = "Erreur au cours de la suppression d'un impôt";
$MESS["CRM_TAX_FIELD_TIMESTAMP_X"] = "Date de modification";
$MESS["CRM_TAX_RATE_ADD"] = "Ajouter le taux d'imposition";
$MESS["CRM_TAX_RATE_ADD_TITLE"] = "Ajouter le taux d'imposition";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_TAX_FIELD_ID"] = "ID";
$MESS["CRM_TAX_FIELD_CODE"] = "Achèvement";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_MODULE_SALE_NOT_INSTALLED"] = "Le module 'Boutique en ligne' n'a pas été installé.";
$MESS["CRM_TAX_FIELD_NAME"] = "Dénomination";
$MESS["CRM_TAX_SECTION_MAIN"] = "Impôt";
$MESS["CRM_TAX_NOT_FOUND"] = "L'impôt n'est pas retrouvé!";
$MESS["CRM_TAX_FIELD_DESCRIPTION"] = "Description";
$MESS["CRM_TAX_FIELD_LID"] = "Site web";
$MESS["CRM_TAX_RATE_LIST"] = "Liste des taux d'impôt";
?>