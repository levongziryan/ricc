<?
$MESS["REPORT_SELECT_CALC_VAR_MIN"] = "Mínimo";
$MESS["REPORT_SELECT_CALC_VAR_AVG"] = "Média";
$MESS["REPORT_SELECT_CALC_VAR_MAX"] = "Máximo";
$MESS["REPORT_SELECT_CALC_VAR_SUM"] = "Total";
$MESS["REPORT_SELECT_CALC_VAR_COUNT_DISTINCT"] = "nico";
$MESS["REPORT_USER_NOT_FOUND"] = "O usuário não foi encontrado.";
$MESS["REPORT_PROJECT_NOT_FOUND"] = "O projeto não foi encontrado.";
$MESS["REPORT_NOT_FOUND"] = "Relatório ID \"% s\" não foi encontrado.";
$MESS["REPORT_VIEW_PERMISSION_DENIED"] = "Permissões insuficientes para visualizar o relatório.";
$MESS["REPORT_UNKNOWN_ERROR"] = "Erro desconhecido.";
$MESS["REPORT_BOOLEAN_VALUE_TRUE"] = "Sim";
$MESS["REPORT_BOOLEAN_VALUE_FALSE"] = "Não";
$MESS["REPORT_PRCNT_FROM_TITLE"] = "% de";
$MESS["REPORT_SELECT_CALC_VAR_GROUP_CONCAT"] = "Imprimir todos os itens existentes";
$MESS["REPORT_CHART_TYPE_LINE"] = "Gráfico de linha";
$MESS["REPORT_CHART_TYPE_BAR"] = "Gráfico de barras";
$MESS["REPORT_CHART_TYPE_PIE"] = "Gráfico de pizza";
$MESS["REPORT_UNKNOWN_FIELD_DEFINITION"] = "O relatório está usando um campo que não existe. O mais provável um campo personalizado que foi excluído. Verificar e salvar a configuração do relatório.";
$MESS["REPORT_FILE_NOT_FOUND"] = "O arquivo não foi encontrado";
$MESS["REPORT_HELPER_NOT_DEFINED"] = "Nenhuma classe auxiliar definida.";
?>