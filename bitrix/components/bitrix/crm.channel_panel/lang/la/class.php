<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado";
$MESS["CRM_CHANNEL_1C_CAPTION"] = "Conector 1C";
$MESS["CRM_CHANNEL_1C_LEGEND"] = "Exporta todas las ventas y el historial de clientes al CRM. Vincular las ventas sin conexión al CRM en línea.";
$MESS["CRM_CHANNEL_WEB_FORM_CAPTION"] = "Formularios del CRM";
$MESS["CRM_CHANNEL_WEB_FORM_LEGEND"] = "Aplicaciones, formularios, formularios de registro se agregan directamente al CRM. Un formulario del CRM es fácil de crear - incluso si usted no tiene un sitio web.";
$MESS["CRM_CHANNEL_TELEPHONY_CAPTION"] = "Telefonía";
$MESS["CRM_CHANNEL_TELEPHONY_LEGEND"] = "Todas las llamadas se guardan en el CRM. Siempre se crea una nueva línea, incluso si se ha perdido una llamada.";
$MESS["CRM_CHANNEL_CHAT_CAPTION"] = "Chat en vivo";
$MESS["CRM_CHANNEL_CHAT_LEGEND"] = "Obtener un widget de chat gratuito. Todas las solicitudes de clientes se envían a Bitrix24; todas las conversaciones siempre se guardan en el CRM.";
$MESS["CRM_CHANNEL_CALLBACK_CAPTION"] = "Volver a llamar";
$MESS["CRM_CHANNEL_CALLBACK_LEGEND"] = "Obtener un widget de devolución de llamada gratis. Un cliente solicita la devolución de llamada, la nueva llamada se realiza desde Bitrix24 y se registra en el CRM.";
$MESS["CRM_CHANNEL_OPEN_LINE_CAPTION"] = "Canales Abiertos";
$MESS["CRM_CHANNEL_OPEN_LINE_LEGEND"] = "Migrar las conversaciones de los clientes desde las redes sociales al chat Bitrix24. Todas las conversaciones se guardan en el CRM.";
$MESS["CRM_CHANNEL_EMAIL_CAPTION"] = "Cuentas de correo electrónico del CRM";
$MESS["CRM_CHANNEL_EMAIL_LEGEND"] = "Un nuevo correo electrónico crea un nuevo prospecto. El mensaje de correo electrónico se agrega al contacto y se crea una nueva actividad.";
?>