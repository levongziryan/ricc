<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_CHANNEL_1C_CAPTION"] = "Connecteur 1C";
$MESS["CRM_CHANNEL_1C_LEGEND"] = "Exportez tout l'historique de ventes et de clients vers le CRM. Liez les ventes hors ligne au CRM en ligne.";
$MESS["CRM_CHANNEL_WEB_FORM_CAPTION"] = "Formulaires CRM";
$MESS["CRM_CHANNEL_WEB_FORM_LEGEND"] = "Les demandes, formulaires et formulaires d'inscription sont ajoutés directement dans le CRM. Un formulaire CRM est facile à créer - même si vous n'avez pas de site.";
$MESS["CRM_CHANNEL_TELEPHONY_CAPTION"] = "Téléphonie";
$MESS["CRM_CHANNEL_TELEPHONY_LEGEND"] = "Tous les appels sont enregistrés dans le CRM. Un nouveau client potentiel est toujours créé même si un appel a été manqué.";
$MESS["CRM_CHANNEL_CHAT_CAPTION"] = "Chat live";
$MESS["CRM_CHANNEL_CHAT_LEGEND"] = "Obenez un widget de chat gratuit. Toutes les demandes de clients sont envoyées vers Bitrix24; toutes les conversations sont toujours enregistrées dans le CRM.";
$MESS["CRM_CHANNEL_CALLBACK_CAPTION"] = "Rappel";
$MESS["CRM_CHANNEL_CALLBACK_LEGEND"] = "Obtenez un widget de rappel gratuit. Un client effectue une demande de rappel, le nouvel appel est réalisé depuis Bitrix24 et est enregistré par le CRM.";
$MESS["CRM_CHANNEL_OPEN_LINE_CAPTION"] = "Canaux ouverts";
$MESS["CRM_CHANNEL_OPEN_LINE_LEGEND"] = "Migrez les conversations client des réseaux sociaux vers le chat Bitrix24. Toutes les conversations sont enregistrées dans le CRM.";
$MESS["CRM_CHANNEL_EMAIL_CAPTION"] = "Comptes e-mail du CRM";
$MESS["CRM_CHANNEL_EMAIL_LEGEND"] = "Un nouvel e-mail crée un nouveau client potentiel. Le message e-mail est ajouté au contact, et une nouvelle activité est créée.";
?>