<?
$MESS["CRM_CHANNEL_PANEL_CONNECT_BUTTON"] = "Conectar";
$MESS["CRM_CHANNEL_PANEL_CLOSE_TITLE"] = "Ocultar descrição";
$MESS["CRM_CHANNEL_PANEL_CLOSE_CONFIRM"] = "Tem certeza de que deseja ocultar a descrição de canais disponíveis no seu CRM?";
$MESS["CRM_CHANNEL_PANEL_CLOSE"] = "Ocultar";
?>