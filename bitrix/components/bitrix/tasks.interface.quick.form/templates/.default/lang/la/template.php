<?
$MESS["TASKS_QUICK_FORM_DESC_PLACEHOLDER"] = "Descripción de la tarea";
$MESS["TASKS_QUICK_FORM_TITLE_PLACEHOLDER"] = "Nueva tarea";
$MESS["TASKS_QUICK_FORM_AFTER_SAVE_MESSAGE"] = "La tarea &quot;#TASK_NAME#&quot; se ha guardado.";
$MESS["TASKS_QUICK_FORM_OPEN_TASK"] = "Abrir la tarea";
$MESS["TASKS_QUICK_FORM_HIGHLIGHT_TASK"] = "Mostrar en la lista";
$MESS["TASKS_QUICK_TITLE"] = "Nombre de la tarea";
$MESS["TASKS_QUICK_DEADLINE"] = "Fecha límite";
$MESS["TASKS_QUICK_SAVE"] = "Guardar";
$MESS["TASKS_QUICK_CANCEL"] = "Cancelar";
$MESS["TASKS_QUICK_RESPONSIBLE"] = "Responsable";
$MESS["TASKS_QUICK_IN_GROUP"] = "tarea en el proyecto";
$MESS["TASKS_QUICK_DESCRIPTION"] = "descripción";
?>