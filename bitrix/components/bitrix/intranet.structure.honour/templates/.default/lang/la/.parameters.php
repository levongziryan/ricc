<?
$MESS["ISL_ID"] = "ID";
$MESS["ISL_FULL_NAME"] = "Nombre Completo";
$MESS["ISL_NAME"] = "Primer nombre";
$MESS["ISL_LAST_NAME"] = "Apellido";
$MESS["ISL_SECOND_NAME"] = "Segundo Nombre";
$MESS["ISL_EMAIL"] = "E-Mail";
$MESS["ISL_LOGIN"] = "Inicio de sesión";
$MESS["ISL_DATE_REGISTER"] = "Registrado En:";
$MESS["ISL_PERSONAL_WWW"] = "Sitio Web";
$MESS["ISL_PERSONAL_PROFESSION"] = "Profesión";
$MESS["ISL_PERSONAL_ICQ"] = "ICQ";
$MESS["ISL_PERSONAL_GENDER"] = "Sexo";
$MESS["ISL_PERSONAL_BIRTHDAY"] = "Fecha de cumpleaños";
$MESS["ISL_PERSONAL_PHOTO"] = "Foto";
$MESS["ISL_PERSONAL_PHONE"] = "Teléfono";
$MESS["ISL_PERSONAL_FAX"] = "Fax";
$MESS["ISL_PERSONAL_MOBILE"] = "Móvil";
$MESS["ISL_PERSONAL_PAGER"] = "Buscador";
$MESS["ISL_PERSONAL_PHONES"] = "Teléfonos";
$MESS["ISL_PERSONAL_POST_ADDRESS"] = "Dirección de e-mail";
$MESS["ISL_PERSONAL_CITY"] = "Ciudad";
$MESS["ISL_PERSONAL_STREET"] = "Dirección de la calle";
$MESS["ISL_PERSONAL_STATE"] = "Región/Estado";
$MESS["ISL_PERSONAL_MAILBOX"] = "Casilla Postal";
$MESS["ISL_PERSONAL_ZIP"] = "Código Postal";
$MESS["ISL_PERSONAL_COUNTRY"] = "País";
$MESS["ISL_PERSONAL_NOTES"] = "Notas";
$MESS["ISL_PERSONAL_POSITION"] = "Cargo";
$MESS["ISL_WORK_PHONE"] = "Teléfono del Trabajo";
$MESS["ISL_ADMIN_NOTES"] = "Notas del Administrador";
$MESS["ISL_XML_ID"] = "ID Externo";
$MESS["INTR_ISBN_PARAM_STRUCTURE_PAGE"] = "Página de la Estructura de la compañía";
$MESS["INTR_ISBN_PARAM_PM_URL"] = "Página de Mensajes Personales";
$MESS["INTR_ISBN_PARAM_STRUCTURE_FILTER"] = "Filtro de la página del nombre de la compañía de la empresa";
$MESS["INTR_ISBN_PARAM_USER_PROPERTY"] = "Campo del cliente para exportar";
$MESS["INTR_ISBN_TPL_PARAM_SHOW_FILTER"] = "Mostrar Filtro";
$MESS["ISL_WORK_POSITION"] = "Posición";
$MESS["ISL_WORK_COMPANY"] = "Compañía";
$MESS["ISL_WORK_FAX"] = "Fax del trabajo";
?>