<?
$MESS["INTR_ISBN_PARAM_NUM_USERS"] = "Mostrar usuarios";
$MESS["INTR_ISBN_PARAM_DETAIL_URL"] = "Página del perfil del usuario";
$MESS["INTR_ISH_PARAM_NAME_TEMPLATE"] = "Formato del nombre";
$MESS["INTR_ISH_PARAM_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["INTR_ISH_PARAM_SHOW_LOGIN"] = "Mostrar nombre de usuario si no requiere campos de nombre de usuario que están disponibles";
$MESS["INTR_ISH_PARAM_DATE_FORMAT"] = "Formato de Fecha";
$MESS["INTR_ISH_PARAM_DATE_FORMAT_NO_YEAR"] = "Formato de fecha (sin año)";
$MESS["INTR_ISH_PARAM_DATE_TIME_FORMAT"] = "Formato de Día y Hora";
$MESS["INTR_ISH_PARAM_SHOW_YEAR"] = "Mostrar Año de Cumpleaños";
$MESS["INTR_ISH_PARAM_SHOW_YEAR_VALUE_Y"] = "todo";
$MESS["INTR_ISH_PARAM_SHOW_YEAR_VALUE_M"] = "sólo hombres";
$MESS["INTR_ISH_PARAM_SHOW_YEAR_VALUE_N"] = "nadie";
$MESS["INTR_ISH_PARAM_PM_URL"] = "Página de mensajes personales";
$MESS["INTR_ISH_PARAM_PATH_TO_CONPANY_DEPARTMENT"] = "Plantilla de la ruta a la Página del Departamento";
$MESS["INTR_ISH_PARAM_PATH_TO_VIDEO_CALL"] = "Página de Video Conferencia";
?>