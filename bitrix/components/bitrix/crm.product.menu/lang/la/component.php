<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_PRODUCT_ADD"] = "Agregar producto";
$MESS["CRM_PRODUCT_ADD_TITLE"] = "Crear un nuevo producto";
$MESS["CRM_PRODUCT_EDIT"] = "Editar";
$MESS["CRM_PRODUCT_EDIT_TITLE"] = "Editar producto";
$MESS["CRM_PRODUCT_DELETE"] = "Eliminar producto";
$MESS["CRM_PRODUCT_DELETE_TITLE"] = "Eliminar este producto";
$MESS["CRM_PRODUCT_DELETE_DLG_TITLE"] = "Eliminar producto";
$MESS["CRM_PRODUCT_DELETE_DLG_MESSAGE"] = "Está seguro que desea eliminar este producto?";
$MESS["CRM_PRODUCT_DELETE_DLG_BTNTITLE"] = "Eliminar producto";
$MESS["CRM_PRODUCT_SHOW"] = "Ver";
$MESS["CRM_PRODUCT_SHOW_TITLE"] = "Ver producto";
$MESS["CRM_PRODUCT_LIST"] = "Productos";
$MESS["CRM_PRODUCT_LIST_TITLE"] = "Ver productos";
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "El módulo Information Blocks no está instalado.";
$MESS["CRM_MOVE_UP"] = "Arriba";
$MESS["CRM_MOVE_UP_TITLE"] = "Busque la sección principal";
$MESS["CRM_PRODUCT_SECTIONS"] = "Administración de secciones";
$MESS["CRM_PRODUCT_SECTIONS_TITLE"] = "Editar y eliminar secciones";
$MESS["CRM_ADD_PRODUCT_SECTION"] = "Agregar sección";
$MESS["CRM_ADD_PRODUCT_SECTION_TITLE"] = "Añadir nueva sección a la sección actual";
$MESS["CRM_PRODUCT_IMPORT"] = "Importar productos";
$MESS["CRM_PRODUCT_IMPORT_TITLE"] = "Importar productos";
?>