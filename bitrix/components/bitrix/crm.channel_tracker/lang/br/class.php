<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["CRM_CH_TRACKER_TITLE"] = "Rastreador de vendas da empresa";
$MESS["CRM_CH_TRACKER_IN_USE"] = "#IN_USE# de #OVERALL#";
$MESS["CRM_CH_TRACKER_HEADER_NAME"] = "Canais de comunicação ativa";
$MESS["CRM_CH_TRACKER_HEADER_ACTIVITY"] = "Controle de Entrada/Saída";
$MESS["CRM_CH_TRACKER_HEADER_LEAD"] = "Clientes potenciais aguardando o processamento";
$MESS["CRM_CH_TRACKER_HEADER_DEAL_PROCESS"] = "Negociações em andamento";
$MESS["CRM_CH_TRACKER_HEADER_DEAL_SUCCESS"] = "Vendas por período de tempo";
?>