<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_CH_TRACKER_TITLE"] = "Tracker des ventes de l'entreprise";
$MESS["CRM_CH_TRACKER_IN_USE"] = "#IN_USE# de #OVERALL#";
$MESS["CRM_CH_TRACKER_HEADER_NAME"] = "Canaux de communication actifs";
$MESS["CRM_CH_TRACKER_HEADER_ACTIVITY"] = "Contrôle entrant/sortant";
$MESS["CRM_CH_TRACKER_HEADER_LEAD"] = "Clients potentiels en attente de traitement";
$MESS["CRM_CH_TRACKER_HEADER_DEAL_PROCESS"] = "Affaires en cours";
$MESS["CRM_CH_TRACKER_HEADER_DEAL_SUCCESS"] = "Ventes pour période de temps";
?>