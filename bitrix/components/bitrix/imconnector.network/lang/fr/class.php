<?
$MESS["IMCONNECTOR_COMPONENT_NETWORK_MODULE_IMCONNECTOR_NOT_INSTALLED"] = "Le module Connecteurs de messagerie externe n'est pas installé";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_MODULE_IMOPENLINES_NOT_INSTALLED"] = "Le module Canaux ouverts n'est pas actif";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_NO_ACTIVE_CONNECTOR"] = "Ce connecteur n'est pas actif";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_SESSION_HAS_EXPIRED"] = "Session expirée. Veuillez soumettre les données une nouvelle fois";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_OK_SAVE"] = "Enregistré";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_NO_SAVE"] = "Impossible d'enregistrer. Veuillez vérifier toutes les données et enregistrer une nouvelle fois";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_NO_ACTIVE"] = "Erreur d'activation du connecteur";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FILE_IS_NOT_A_SUPPORTED_TYPE"] = "Erreur de téléchargement du fichier. Seuls les fichiers .JPG et .PNG sont supportés.";
?>