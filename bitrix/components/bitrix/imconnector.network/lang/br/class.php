<?
$MESS["IMCONNECTOR_COMPONENT_NETWORK_MODULE_IMCONNECTOR_NOT_INSTALLED"] = "O Módulo Conectores Externos do Messenger não está instalado";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_MODULE_IMOPENLINES_NOT_INSTALLED"] = "O Módulo Canais Abertos não está ativo";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_NO_ACTIVE_CONNECTOR"] = "Este conector não está ativo";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_SESSION_HAS_EXPIRED"] = "A sessão expirou. Envie os dados novamente";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_OK_SAVE"] = "Salvo";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_NO_SAVE"] = "Não é possível salvar. Verifique todos os dados e salve novamente";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_NO_ACTIVE"] = "Erro de ativação do conector";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FILE_IS_NOT_A_SUPPORTED_TYPE"] = "Erro ao carregar o arquivo. Compatível apenas com arquivos .JPG e .PNG.";
?>