<?
$MESS["IMCONNECTOR_COMPONENT_NETWORK_MODULE_IMCONNECTOR_NOT_INSTALLED"] = "El módulo External Messenger Connectors no está instalado";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_MODULE_IMOPENLINES_NOT_INSTALLED"] = "El módulo Open Channels no está activado";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_NO_ACTIVE_CONNECTOR"] = "Este conector no está activo.";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_SESSION_HAS_EXPIRED"] = "La sesión ha caducado. Por favor enviar los datos de nuevo";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_OK_SAVE"] = "Guardar";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_NO_SAVE"] = "No se puede guardar. Compruebe todos los datos y guarde nuevamente";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_NO_ACTIVE"] = "Error de activación de conector";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FILE_IS_NOT_A_SUPPORTED_TYPE"] = "Error al cargar el archivo. Archivos JPG y .PNG son compatibles.";
?>