<?
$MESS["IMCONNECTOR_COMPONENT_NETWORK_CONNECTOR_ERROR_STATUS"] = "Erreur de traitement du bot. Veuillez vérifier tous les paramètres et refaire le test du bot.";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_NAME"] = "Nom";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_CODE"] = "Code";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_SIMPLE_FORM_DESCRIPTION_1"] = "Le Canal ouvert est déjà connecté à Bitrix24.Network.
<br>Vous pouvez modifier les paramètres si nécessaire.";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_INDEX_DESCRIPTION"] = "Connectez Bitrix24.Network aux Canaux ouverts pour envoyer et recevoir des messages d'autres utilisateurs Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_CONNECT"] = "Connecter";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_COPY"] = "Copier";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_1"] = "Nom";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_2"] = "Brève description";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_3"] = "Message d'accueil";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_4"] = "Avatar";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_4_DESCRIPTION_1"] = "Un avatar unique permettra à vos clients de vous trouver plus rapidement.";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_4_DESCRIPTION_2"] = "Télécharger un avatar";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_4_DESCRIPTION_3"] = "Supprimer la photo";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_5"] = "Recherchable";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_6"] = "Code de recherche";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_FIELD_6_TIP"] = "Ce code permet aux utilisateurs de trouver votre canal ouvert dans le service de messagerie, même sans être recherchable.";
$MESS["IMCONNECTOR_COMPONENT_NETWORK_REST_HELP"] = "Vous pouvez créer un canal de support privé non recherchable.<br> Veuillez fournir le code à vos clients afin qu'ils puissent vous contacter ou connecter le canal via l'API REST.<br> Les détails des Canaux ouverts API REST sont disponibles sur ";
?>