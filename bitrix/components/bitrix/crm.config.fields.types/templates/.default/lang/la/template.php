<?
$MESS["CRM_FIELDS_TOOLBAR_ADD"] = "Agregar Campo";
$MESS["CRM_FIELDS_TOOLBAR_ADD_TITLE"] = "Agregar Nuevo Campo";
$MESS["CRM_FIELDS_LIST_SORT"] = "Clasificación";
$MESS["CRM_FIELDS_LIST_NAME"] = "Nombre";
$MESS["CRM_FIELDS_LIST_TYPE"] = "Tipo";
$MESS["CRM_FIELDS_LIST_IS_REQUIRED"] = "Requerido";
$MESS["CRM_FIELDS_LIST_MULTIPLE"] = "Múltiple";
$MESS["CRM_FIELDS_ADD_FIELD"] = "Agregar campo";
$MESS["CRM_FIELDS_ADD_FIELD_TITLE"] = "Agregar Nuevo Campo";
$MESS["CRM_FIELDS_LIST_FIELD"] = "Campos";
$MESS["CRM_FIELDS_LIST_FIELD_TITLE"] = "Mostrar lista de campos";
?>