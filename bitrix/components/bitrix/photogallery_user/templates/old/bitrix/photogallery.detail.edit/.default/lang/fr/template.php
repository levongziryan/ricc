<?
$MESS["P_ALBUMS"] = "Albums";
$MESS["P_GO_TO_SECTION"] = "Revenir à l'affichage de photos";
$MESS["P_DATE"] = "Date";
$MESS["P_EDIT_ELEMENT"] = "Modification des propriétés de la photo";
$MESS["P_UP"] = "Vers l'affichage de la photo";
$MESS["P_TITLE"] = "Dénomination";
$MESS["P_ACTIVE_ELEMENT"] = "Approuver (si le signe manque, la photo est accessible uniquement au propriétaire)";
$MESS["P_APPROVE_ELEMENT"] = "Approuver photo";
$MESS["P_DESCRIPTION"] = "Description";
$MESS["P_PUBLIC_ELEMENT"] = "Publier (afficher sur la page principale)";
$MESS["P_CANCEL"] = "Annuler";
$MESS["P_SUBMIT"] = "Sauvegarder";
$MESS["P_TAGS"] = "Tags";
?>