<?
$MESS["NO_OF_COUNT"] = "#NO# de(s) #TOTAL#";
$MESS["P_UP"] = "Rentrer à l'album";
$MESS["P_GO_TO_SECTION"] = "Rentrer à l'album";
$MESS["P_DROP_CONFIM"] = "tes-vous sûr de vouloir supprimer la photo définitivement ?";
$MESS["P_UPLOAD"] = "Charger";
$MESS["P_UPLOAD_TITLE"] = "Charger";
$MESS["P_EDIT"] = "Editer";
$MESS["P_EDIT_TITLE"] = "Modifiez les propriétés de photo";
$MESS["P_ORIGINAL"] = "Original";
$MESS["P_ORIGINAL_TITLE"] = "Original de l'image";
$MESS["P_GO_TO_PREV"] = "Accéder  à la photo précédente";
$MESS["P_GO_TO_NEXT"] = "Passer à la photo suivante";
$MESS["P_SLIDE_SHOW"] = "Diaporama";
$MESS["P_SLIDE_SHOW_TITLE"] = "Lancer le slideshow par cette photo";
$MESS["P_DROP"] = "Supprimer";
$MESS["P_DROP_TITLE"] = "Supprimer la photo";
?>