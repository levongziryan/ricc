<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado. ";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "O módulo de e-Store não está instalado. ";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COLUMN_ACTIVE"] = "Ativo";
$MESS["CRM_COLUMN_TIMESTAMP_X"] = "Modificado em";
$MESS["CRM_COLUMN_NAME"] = "Nome";
$MESS["CRM_COLUMN_PERSON_TYPE_ID"] = "Tipo de cliente";
$MESS["CRM_COLUMN_VALUE"] = "Classificação";
$MESS["CRM_COLUMN_IS_IN_PRICE"] = "Incluir impostos no preço";
$MESS["CRM_COLUMN_APPLY_ORDER"] = "Ordem de aplicação";
$MESS["CRM_TAXRATE_DELETION_GENERAL_ERROR"] = "Erro ao deletar a taxa de imposto.";
$MESS["CRM_TAXRATE_UPDATE_GENERAL_ERROR"] = "Erro ao atualizar a taxa de imposto.";
$MESS["CRM_COMPANY_PT"] = "Empresa";
$MESS["CRM_CONTACT_PT"] = "Contato";
?>