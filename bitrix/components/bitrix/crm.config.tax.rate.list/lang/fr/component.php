<?
$MESS["CRM_COLUMN_ACTIVE"] = "Actif(ve)";
$MESS["CRM_COLUMN_IS_IN_PRICE"] = "Inclure la TVA au prix";
$MESS["CRM_COLUMN_TIMESTAMP_X"] = "Date de modification";
$MESS["CRM_PERMISSION_DENIED"] = "Accès interdit";
$MESS["CRM_COLUMN_ID"] = "ID";
$MESS["CRM_COMPANY_PT"] = "Entreprise";
$MESS["CRM_CONTACT_PT"] = "Client";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_SALE_MODULE_NOT_INSTALLED"] = "Le module 'Boutique en ligne' n'a pas été installé.";
$MESS["CRM_COLUMN_NAME"] = "Dénomination";
$MESS["CRM_COLUMN_APPLY_ORDER"] = "Modalités d'application";
$MESS["CRM_TAXRATE_UPDATE_GENERAL_ERROR"] = "Erreur au cours du rafraîchissement du taux de l'impôt.";
$MESS["CRM_TAXRATE_DELETION_GENERAL_ERROR"] = "Erreur survenue au cours de la suppression du taux de l'impôt.";
$MESS["CRM_COLUMN_VALUE"] = "Coefficient";
$MESS["CRM_COLUMN_PERSON_TYPE_ID"] = "Type du client";
?>