<?
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_MULTIPLE_RESPONSIBLE_NOTICE"] = "Uma tarefa separada será criada para cada pessoa responsável";
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_TPARAM_TYPE"] = "Modelo de tarefa para um novo empregado";
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_NO_PARENT_TEMPLATE_NOTICE"] = "O modelo de tarefa não pode ter um modelo principal se a opção '#TPARAM_FOR_NEW_USER #' está marcada.";
?>