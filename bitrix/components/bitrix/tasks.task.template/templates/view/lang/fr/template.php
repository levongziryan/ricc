<?
$MESS["TASKS_TT_VIEW"] = "Voir le modèle ##ID#";
$MESS["TASKS_STATUS_OVERDUE"] = "Dépassé";
$MESS["TASKS_STATUS_NEW"] = "Nouveau";
$MESS["TASKS_STATUS_ACCEPTED"] = "En attente";
$MESS["TASKS_STATUS_IN_PROGRESS"] = "En cours";
$MESS["TASKS_STATUS_WAITING"] = "En attente de contrôle";
$MESS["TASKS_STATUS_COMPLETED"] = "Terminé";
$MESS["TASKS_STATUS_DELAYED"] = "Différé";
$MESS["TASKS_STATUS_DECLINED"] = "Rejeté";
$MESS["TASKS_STATUS_1"] = "Nouveau";
$MESS["TASKS_STATUS_2"] = "En attente";
$MESS["TASKS_STATUS_3"] = "En cours";
$MESS["TASKS_STATUS_4"] = "Supposément terminé";
$MESS["TASKS_STATUS_5"] = "Terminé";
$MESS["TASKS_STATUS_6"] = "Différé";
$MESS["TASKS_STATUS_7"] = "Rejeté";
$MESS["TASKS_MARK"] = "Score";
$MESS["TASKS_MARK_NONE"] = "Aucun";
$MESS["TASKS_MARK_P"] = "Positif";
$MESS["TASKS_MARK_N"] = "Négatif";
$MESS["TASKS_DOUBLE_CLICK"] = "Double-cliquez pour afficher";
$MESS["TASKS_APPLY"] = "Appliquer";
$MESS["TASKS_NO_TITLE"] = "Le nom de la tâche n'est pas indiqué.";
$MESS["TASKS_NO_RESPONSIBLE"] = "La personne responsable n’est pas spécifiée.";
$MESS["TASKS_TITLE"] = "Tâches";
$MESS["TASKS_DEADLINE"] = "Date limite";
$MESS["TASKS_RESPONSIBLE"] = "Responsable";
$MESS["TASKS_CREATOR"] = "Créateur";
$MESS["TASKS_QUICK_TITLE"] = "Nom de la tâche";
$MESS["TASKS_FIELD_DEADLINE_AFTER"] = "Date limite dans";
$MESS["TASKS_FIELD_START_DATE_PLAN_AFTER"] = "Démarrer dans";
$MESS["TASKS_FIELD_END_DATE_PLAN_AFTER"] = "Terminer dans";
$MESS["TASKS_FIELD_TIME_ESTIMATE"] = "Estimation du temps";
$MESS["TASKS_QUICK_SAVE"] = "Enregistrer";
$MESS["TASKS_QUICK_CANCEL"] = "Annuler";
$MESS["TASKS_FILTER"] = "Régler le filtre";
$MESS["TASKS_FILTER_COMMON"] = "Simple";
$MESS["TASKS_FILTER_EXTENDED"] = "Étendu";
$MESS["TASKS_FILTER_STATUSES"] = "Statuts";
$MESS["TASKS_TEMPLATES"] = "Modèles";
$MESS["TASKS_REPORTS"] = "Rapports";
$MESS["TASKS_EXPORT_EXCEL"] = "Exporter vers Excel";
$MESS["TASKS_EXPORT_OUTLOOK"] = "Exporter vers Outlook";
$MESS["TASKS_ADD_TASK"] = "Nouvelle tâche";
$MESS["TASKS_ADD_TASK_SHORT"] = "Ajouter";
$MESS["TASKS_ACCEPT_TASK"] = "Accepter la tâche";
$MESS["TASKS_START_TASK"] = "Commencer";
$MESS["TASKS_DECLINE_TASK"] = "Refuser";
$MESS["TASKS_RENEW_TASK"] = "Reprendre";
$MESS["TASKS_CLOSE_TASK"] = "Terminer";
$MESS["TASKS_PAUSE_TASK"] = "Interrompre";
$MESS["TASKS_DEFER_TASK"] = "Reporter";
$MESS["TASKS_APPROVE_TASK"] = "Accepter comme terminé";
$MESS["TASKS_REDO_TASK"] = "Retourner pour achèvement";
$MESS["TASKS_DELETE_TASK"] = "Supprimer";
$MESS["TASKS_DELETE_CONFIRM"] = "Do you really want to delete it?";
$MESS["TASKS_DELETE_TASK_CONFIRM"] = "Do you really want to delete it?";
$MESS["TASKS_DELETE_FILE_CONFIRM"] = "Supprimer le fichier?";
$MESS["TASKS_ADD_TEMPLATE_TASK"] = "Créer d'après le modèle de tâche";
$MESS["TASKS_TEMPLATES_LIST"] = "Tous les modèles";
$MESS["TASKS_DURATION"] = "Nombre d'heures";
$MESS["TASKS_OK"] = "OK";
$MESS["TASKS_CANCEL"] = "Annuler";
$MESS["TASKS_DECLINE_REASON"] = "Raison du refus";
$MESS["TASKS_ADD_TEMPLATE_SUBTASK"] = "Modèles de tâche (pour nouvelle sous-tâche)";
$MESS["TASKS_SELECT"] = "Sélectionner";
$MESS["TASKS_TASK_TITLE"] = "Nom et description de la tâche";
$MESS["TASKS_REMIND"] = "rappeler";
$MESS["TASKS_TASK_TAGS"] = "Tags";
$MESS["TASKS_TASK_NO_TAGS"] = "aucun";
$MESS["TASKS_TASK_FILES"] = "Fichiers";
$MESS["TASKS_TASK_SUBTASKS"] = "Sous-tâches";
$MESS["TASKS_TASK_PREVIOUS_TASKS"] = "Tâches précédentes";
$MESS["TASKS_TASK_LINKED_TASKS"] = "Tâches dépendantes";
$MESS["TASKS_TASK_PREDECESSORS"] = "Tâches précédentes";
$MESS["TASKS_TASK_COMMENTS"] = "Commentaires";
$MESS["TASKS_TASK_LOG"] = "Modifier le journal";
$MESS["TASKS_TASK_LOG_SHORT"] = "Historique";
$MESS["TASKS_ADD_BACK_TO_TASKS_LIST"] = "Retour aux tâches";
$MESS["TASKS_HOURS_N"] = "heure";
$MESS["TASKS_HOURS_G"] = "heure";
$MESS["TASKS_HOURS_P"] = "heures";
$MESS["TASKS_NO_TEMPLATES"] = "Aucun modèle disponible";
$MESS["TASKS_PARENT_TASK"] = "Tâche parent";
$MESS["TASKS_TASK_GROUP"] = "Cette tâche est dans le groupe (projet)";
$MESS["TASKS_NO_SUBTASKS"] = "pas de sous-tâche";
$MESS["TASKS_LOG_NEW"] = "Tâche créée";
$MESS["TASKS_LOG_TITLE"] = "Nom de la tâche";
$MESS["TASKS_LOG_DESCRIPTION"] = "Description mise à jour";
$MESS["TASKS_LOG_CREATED_BY"] = "Créateur";
$MESS["TASKS_LOG_RESPONSIBLE_ID"] = "Personne responsable";
$MESS["TASKS_LOG_DEADLINE"] = "Date limite";
$MESS["TASKS_LOG_ACCOMPLICES"] = "Participants";
$MESS["TASKS_LOG_AUDITORS"] = "Observateurs";
$MESS["TASKS_LOG_FILES"] = "Fichiers";
$MESS["TASKS_LOG_TAGS"] = "Tags";
$MESS["TASKS_LOG_PRIORITY"] = "Priorité";
$MESS["TASKS_LOG_GROUP_ID"] = "Groupe";
$MESS["TASKS_LOG_PARENT_ID"] = "Tâche parent";
$MESS["TASKS_LOG_DEPENDS_ON"] = "Tâches précédentes";
$MESS["TASKS_LOG_STATUS"] = "Statut";
$MESS["TASKS_LOG_MARK"] = "Score";
$MESS["TASKS_LOG_ADD_IN_REPORT"] = "Dans le rapport";
$MESS["TASKS_LOG_WHEN"] = "Date";
$MESS["TASKS_LOG_WHO"] = "Créé par";
$MESS["TASKS_LOG_WHERE"] = "Mettre à jour le lieu";
$MESS["TASKS_LOG_WHAT"] = "Mettre à jour";
$MESS["TASKS_LOG_DELETED_FILES"] = "Fichiers supprimés";
$MESS["TASKS_LOG_NEW_FILES"] = "Fichiers ajoutés";
$MESS["TASKS_LOG_COMMENT"] = "Commentaire créé";
$MESS["TASKS_LOG_COMMENT_EDIT"] = "Commentaire modifié";
$MESS["TASKS_LOG_COMMENT_DEL"] = "Commentaire supprimé";
$MESS["TASKS_LOG_START_DATE_PLAN"] = "Date de début proposée";
$MESS["TASKS_LOG_END_DATE_PLAN"] = "Date de fin proposée";
$MESS["TASKS_LOG_DURATION_PLAN"] = "Durée planifiée";
$MESS["TASKS_LOG_DURATION_PLAN_SECONDS"] = "Durée planifiée";
$MESS["TASKS_LOG_DURATION_FACT"] = "Durée réelle";
$MESS["TASKS_LOG_TIME_SPENT_IN_LOGS"] = "Temps passé";
$MESS["TASKS_LOG_CHECKLIST_ITEM_CREATE"] = "Élément ajouté à la liste de contrôle";
$MESS["TASKS_LOG_CHECKLIST_ITEM_REMOVE"] = "Élément supprimé de la liste de contrôle";
$MESS["TASKS_LOG_CHECKLIST_ITEM_RENAME"] = "Le point de la liste de contrôle a été renommé.";
$MESS["TASKS_LOG_CHECKLIST_ITEM_UNCHECK"] = "L’élément de la liste de contrôle n'est plus terminé.";
$MESS["TASKS_LOG_CHECKLIST_ITEM_CHECK"] = "L’élément de la liste de contrôle est terminé.";
$MESS["TASKS_DELEGATE_TASK"] = "Déléguer";
$MESS["TASKS_COPY_TASK"] = "Copier";
$MESS["TASKS_ADD_TASK_TO_TIMEMAN"] = "Add to working day plan";
$MESS["TASKS_COPY_TASK_EX"] = "Duplicate task";
$MESS["TASKS_ADD_TASK_TO_TIMEMAN_EX"] = "Add to working day plan";
$MESS["TASKS_ELAPSED_TIME"] = "Temps passé";
$MESS["TASKS_ELAPSED_TIME_SHORT"] = "Temps écoulé";
$MESS["TASKS_ELAPSED_H"] = "h";
$MESS["TASKS_ELAPSED_M"] = "m";
$MESS["TASKS_ELAPSED_S"] = "sec";
$MESS["TASKS_ELAPSED_AUTHOR"] = "Créé par";
$MESS["TASKS_ELAPSED_DATE"] = "Date";
$MESS["TASKS_ELAPSED_COMMENT"] = "Commentaire";
$MESS["TASKS_ELAPSED_ADD"] = "Ajouter";
$MESS["TASKS_ELAPSED_SOURCE_UNDEFINED"] = "Il est impossible de savoir si cet enregistrement a été ajouté ou modifié manuellement";
$MESS["TASKS_ELAPSED_SOURCE_MANUAL"] = "Cet enregistrement a été ajouté ou modifié manuellement";
$MESS["TASKS_REMINDER_TITLE"] = "Rappeler";
$MESS["TASKS_ABOUT_DEADLINE"] = "Avant la date limite";
$MESS["TASKS_BY_DATE"] = "Par date";
$MESS["TASKS_REMIND_VIA_JABBER"] = "message instantané";
$MESS["TASKS_REMIND_VIA_EMAIL"] = "par e-mail";
$MESS["TASKS_REMIND_VIA_JABBER_EX"] = "Envoyer un message instantané";
$MESS["TASKS_REMIND_VIA_EMAIL_EX"] = "Envoyer un message e-mail";
$MESS["TASKS_REMIND_BEFORE"] = "dans #NUM# jour(s)";
$MESS["TASKS_REMINDER_OK"] = "OK";
$MESS["TASKS_ADD_SUBTASK_2"] = "Créer une sous-tâche";
$MESS["TASKS_QUICK_IN_GROUP"] = "tâche dans un projet";
$MESS["TASKS_QUICK_DESCRIPTION"] = "description";
$MESS["TASKS_DATE_CREATED"] = "Créé le";
$MESS["TASKS_DATE_START"] = "Date de début";
$MESS["TASKS_DATE_END"] = "Date de fin";
$MESS["TASKS_DATE_STARTED"] = "Commencé le";
$MESS["TASKS_DATE_COMPLETED"] = "Terminé le";
$MESS["TASKS_TASK_TITLE_LABEL"] = "Tâche";
$MESS["TASKS_STATUS"] = "Statut";
$MESS["TASKS_QUICK_INFO_DETAILS"] = "Détails";
$MESS["TASKS_QUICK_INFO_EMPTY_DATE"] = "non";
$MESS["TASKS_GROUP_ADD"] = "ajouter";
$MESS["TASKS_GROUP_LOADING"] = "merci de patienter...";
$MESS["TASKS_TASK_DURATION_HOURS"] = "heures";
$MESS["TASKS_TASK_DURATION_HOURS_PLURAL_0"] = "heure";
$MESS["TASKS_TASK_DURATION_HOURS_PLURAL_1"] = "heure";
$MESS["TASKS_TASK_DURATION_HOURS_PLURAL_2"] = "heures";
$MESS["TASKS_TASK_DURATION_DAYS"] = "jours";
$MESS["TASKS_TASK_DURATION_DAYS_PLURAL_0"] = "jour";
$MESS["TASKS_TASK_DURATION_DAYS_PLURAL_1"] = "jours";
$MESS["TASKS_TASK_DURATION_DAYS_PLURAL_2"] = "jours";
$MESS["TASKS_TASK_DURATION_MINUTES"] = "minutes";
$MESS["TASKS_TASK_DURATION_MINUTES_PLURAL_0"] = "minute";
$MESS["TASKS_TASK_DURATION_MINUTES_PLURAL_1"] = "minutes";
$MESS["TASKS_TASK_DURATION_MINUTES_PLURAL_2"] = "minutes";
$MESS["TASKS_TASK_DURATION_SECONDS_PLURAL_0"] = "deuxième";
$MESS["TASKS_TASK_DURATION_SECONDS_PLURAL_1"] = "secondes";
$MESS["TASKS_TASK_DURATION_SECONDS_PLURAL_2"] = "secondes";
$MESS["TASKS_DETAIL_CHECKLIST"] = "Liste de contrôle";
$MESS["TASKS_DETAIL_CHECKLIST_ADD"] = "ajouter";
$MESS["TASKS_TASK_ADD_TO_FAVORITES"] = "Ajouter aux favoris";
$MESS["TASKS_ELAPSED_REMOVE_CONFIRM"] = "Voulez-vous supprimer des valeurs de temps écoulé ?";
$MESS["TASKS_IMPORTANT_TASK"] = "Priorité élevée";
$MESS["TASKS_TASK_NUM"] = "Tâche ##TASK_NUM#";
$MESS["TASKS_FILES_FROM_COMMENTS"] = "Fichiers";
$MESS["TASKS_DEPENDENCY_TYPE"] = "Dépendance";
$MESS["TASKS_DEPENDENCY_START"] = "Commencer";
$MESS["TASKS_DEPENDENCY_END"] = "Terminer";
$MESS["TASKS_TASK_COMPONENT_TEMPLATE_MAKE_IMPORTANT"] = "Make important task";
$MESS["TASKS_MAIL_FORWARD"] = "Adresse de transfert";
$MESS["TASKS_PARENT_TEMPLATE"] = "Modèle général";
$MESS["TASKS_TEMPLATE_CREATE_TASK"] = "Créer une tâche d'après ce modèle";
$MESS["TASKS_TEMPLATE_CREATE_SUB"] = "Ajouter un sous-modèle";
$MESS["TASKS_TTDP_TEMPLATE_USER_VIEW_ORIGINATOR"] = "Créé par";
$MESS["TASKS_TTDP_TEMPLATE_USER_VIEW_AUDITORS"] = "Observateurs";
$MESS["TASKS_TTDP_TEMPLATE_USER_VIEW_ACCOMPLICES"] = "Participants";
$MESS["TASKS_SIDEBAR_REGULAR_TASK"] = "Tâche récurrente";
$MESS["TASKS_CTT_SYS_LOG"] = "Historique";
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_TO_LIST"] = "Retour à la liste";
$MESS["TASKS_TASK_TEMPLATE_COMPONENT_TEMPLATE_ADD_SUBTEMPLATE"] = "Ajouter une sous-tâche";
$MESS["TASKS_TTDP_DATES"] = "Période";
$MESS["TASKS_TTV_TYPE_FOR_NEW_USER_HINT"] = "Ce modèle est utilisé pour créer une tâche pour chaque nouvel utilisateur inscrit";
$MESS["TASKS_TTV_TEMPLATE_INACCESSIBLE"] = "impossible d'afficher le modèle";
$MESS["TASKS_TTV_TASK_INACCESSIBLE"] = "impossible d'afficher la tâche";
$MESS["TASKS_PRIORITY_0"] = "Faible";
$MESS["TASKS_TTDP_PROJECT_TASK_IN"] = "Cette tâche est dans le groupe (projet)";
$MESS["TASKS_PRIORITY_1"] = "Normal";
$MESS["TASKS_PRIORITY_2"] = "Élevé";
$MESS["TASKS_QUICK_DEADLINE"] = "Date limite";
$MESS["TASKS_LOG_TIME_ESTIMATE"] = "Estimation du temps";
$MESS["TASKS_TEMPLATE_COPY"] = "Copier le modèle";
$MESS["TASKS_TTV_SUB_TITLE"] = "Template for task ##ID#";
$MESS["TASKS_TTDP_TEMPLATE_USER_VIEW_RESPONSIBLE"] = "Personne responsable";
?>