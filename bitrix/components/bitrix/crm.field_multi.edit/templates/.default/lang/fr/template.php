<?
$MESS["CRM_STATUS_LIST_UP"] = "Déplacer vers le haut";
$MESS["CRM_STATUS_LIST_RECOVERY_NAME"] = "Retourner le nom d'origine";
$MESS["CRM_STATUS_LIST_DOWN"] = "Déplacer vers le bas";
$MESS["CRM_STATUS_LIST_ADD"] = "Ajouter";
$MESS["CRM_STATUS_BUTTONS_CANCEL"] = "Annuler";
$MESS["CRM_STATUS_BUTTONS_SAVE"] = "Sauvegarder";
$MESS["CRM_STATUS_LIST_DELETE"] = "Supprimer";
?>