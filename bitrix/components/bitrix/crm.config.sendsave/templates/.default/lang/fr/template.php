<?
$MESS["CRM_TAB_CONFIG"] = "Intégration avec le courrier";
$MESS["CRM_TAB_CONFIG_TITLE"] = "Réglage de l'intégration avec le courrier électronique par la technologie Envoyer&Sauvagarder";
$MESS["CRM_BUTTON_CANCEL_TITLE"] = "Ne pas enregistrer les paramètres actuels pour intégrer e-mail";
$MESS["CRM_BUTTON_CANCEL"] = "Annuler";
$MESS["CRM_TAB_CONFIG_TITLE_EDIT"] = "Paramètres d'intégration du  Send&Save ?-mail";
$MESS["CRM_TAB_CONFIG_TITLE_CREATE"] = "Création d'une intégration avec la poste selon la technologie Send&Save";
$MESS["CRM_BUTTON_SAVE"] = "Sauvegarder";
$MESS["CRM_BUTTON_SAVE_TITLE"] = "Enregistrer les paramètres d'intégration avec l'e-mail";
$MESS["CRM_BUTTON_DELETE"] = "Supprimer";
$MESS["CRM_BUTTON_DELETE_TITLE"] = "Supprimer des réglages courants d'intégration avec le courrier";
?>