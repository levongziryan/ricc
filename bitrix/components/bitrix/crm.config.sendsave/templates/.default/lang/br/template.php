<?
$MESS["CRM_TAB_CONFIG"] = "Enviar e Salvar Integração";
$MESS["CRM_TAB_CONFIG_TITLE"] = "Enviar e Salvar parâmetros de integração";
$MESS["CRM_TAB_CONFIG_TITLE_EDIT"] = "Enviar e salvar os parâmetros de integração de e-mail";
$MESS["CRM_TAB_CONFIG_TITLE_CREATE"] = "Nova integração Enviar&Salvar do E-mail";
$MESS["CRM_BUTTON_SAVE"] = "Salvar";
$MESS["CRM_BUTTON_SAVE_TITLE"] = "Salve parâmetros de integração";
$MESS["CRM_BUTTON_CANCEL"] = "Cancelar";
$MESS["CRM_BUTTON_CANCEL_TITLE"] = "Não salvar parâmetros atuais";
$MESS["CRM_BUTTON_DELETE"] = "Excluir";
$MESS["CRM_BUTTON_DELETE_TITLE"] = "Excluir parâmetros de integração atual";
?>