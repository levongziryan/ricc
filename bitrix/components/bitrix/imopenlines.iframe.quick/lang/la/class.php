<?
$MESS["IMOL_QUICK_ANSWERS_EDIT_SECTION_TITLE"] = "Crear respuesta preparada en la sección";
$MESS["IMOL_QUICK_ANSWERS_EDIT_TEXT_PLACEHOLDER"] = "Texto de respuesta preparada";
$MESS["IMOL_QUICK_ANSWERS_EDIT_CREATE"] = "Crear";
$MESS["IMOL_QUICK_ANSWERS_EDIT_CANCEL"] = "Cancelar";
$MESS["IMOL_QUICK_ANSWERS_EDIT_UPDATE"] = "Actualizar";
$MESS["IMOL_QUICK_ANSWERS_EDIT_SUCCESS"] = "¡Su respuesta preparada se ha guardado!";
$MESS["IMOL_QUICK_ANSWERS_EDIT_ERROR"] = "No se puede guardar la respuesta preparada. Por favor, inténtelo de nuevo más tarde.";
$MESS["IMOL_QUICK_ANSWERS_EDIT_ERROR_EMPTY_TEXT"] = "El texto no debe estar vacío";
$MESS["IMOL_QUICK_ANSWERS_EDIT_ALL"] = "Todo";
$MESS["IMOL_QUICK_ANSWERS_NOT_FOUND"] = "Lo sentimos, pero no pudimos encontrar nada";
$MESS["IMOL_QUICK_ANSWERS_SEARCH_PROGRESS"] = "Búsqueda en progreso...";
$MESS["IMOL_QUICK_ANSWERS_INFO_TITLE"] = "Utilice respuestas preparadas para brindar una respuesta rápida a las preguntas que sus clientes le hacen habitualmente.";
$MESS["IMOL_QUICK_ANSWERS_INFO_LIST_1"] = "guarde un mensaje publicado como respuesta preparada haciendo clic en el botón span class=\"imopenlines-iframe-quick-check-icon\"></span> junto al mensaje";
$MESS["IMOL_QUICK_ANSWERS_INFO_LIST_2"] = "cree uno nuevo desde cero";
$MESS["IMOL_QUICK_ANSWERS_INFO_LIST_3"] = "or open the <a id=\"quick-info-all-url\" class=\"imopenlines-iframe-quick-link\">canned responses form</a>";
?>