<?
$MESS["FACEID_TRACKER1C_CMP_JS_CAMERA_DEFAULT"] = "Kamera";
$MESS["FACEID_TRACKER1C_CMP_JS_CAMERA_NOT_FOUND"] = "Kamera wurde nicht gefunden";
$MESS["FACEID_TRACKER1C_CMP_JS_CAMERA_NO_SUPPORT"] = "Ihr Browser unterstützt keine Kamera";
$MESS["FACEID_TRACKER1C_CMP_JS_CAMERA_ERROR"] = "Ihr Browser keine Videoaufzeichnung oder die Kamera ist nicht verbunden.";
$MESS["FACEID_TRACKER1C_CMP_JS_AJAX_ERROR"] = "Fehler bei der AJAX-Anfrage";
$MESS["FACEID_TRACKER1C_CMP_CAMERA"] = "Kamera";
$MESS["FACEID_1C_PUBLIC_TITLE"] = "1C Face-card";
?>