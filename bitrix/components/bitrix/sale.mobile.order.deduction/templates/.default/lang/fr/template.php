<?
$MESS["SMODE_READY"] = "Prêt";
$MESS["SMODE_DEDUCT_HINT"] = "Pour le déchargement appuyez sur le bouton 'Décharger'.";
$MESS["SMODE_BACK"] = "Précédent";
$MESS["SMODE_PRODUCT_NAME"] = "Dénomination des marchandises";
$MESS["SMODE_DEDUCT"] = "Expédier";
$MESS["SMODE_TITLE"] = "Expédition de la commande";
$MESS["SMODE_TITLE_UNDO"] = "Annuler de l'expédition";
$MESS["SMODE_DEDUCT_UNDO"] = "Annuler";
$MESS["SMODE_ERROR"] = "Erreur de la livraison";
$MESS["SMODE_DEDUCT_UNDO_REASON"] = "Cause d'Annuler du déchargement";
$MESS["SMODE_STORE"] = "Entrepôt";
$MESS["SMODE_SAVE"] = "Sauvegarder";
$MESS["SMODE_PRODUCT"] = "Article";
$MESS["SMODE_BARCODE"] = "Code-barres";
?>