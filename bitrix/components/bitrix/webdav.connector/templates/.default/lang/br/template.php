<?
$MESS["WD_WEBFOLDER_TITLE"] = "Conecte-se como uma pasta da Web";
$MESS["WD_USEADDRESS"] = "Usar o seguinte endereço para conexão:";
$MESS["WD_CONNECT"] = "Conectar";
$MESS["WD_SHAREDDRIVE_TITLE"] = "Mostrar instruções para conectar como uma unidade de rede";
$MESS["WD_REGISTERPATCH"] = "As preferências de segurança atuais exigem que você href=\"#LINK#\"> <a fazer algumas mudanças de registro </a>, a fim de conectar uma unidade de rede.";
$MESS["WD_NOTINSTALLED"] = "Este componente não está instalado em seu sistema operacional por padrão. Você pode href=\"#LINK#\"> <a baixá-lo aqui </a>.";
$MESS["WD_WIN7HTTPSCMD"] = "Para conectar-se a biblioteca como uma unidade de rede via HTTPS / SSL protocolo, executar o comando: <b> Iniciar> Executar> cmd </b>.";
$MESS["WD_CONNECTION_MANUAL"] = "<a instruções de conexão href=\"#LINK#\"> </a>.";
$MESS["WD_TIP_FOR_2008"] = "Por favor, leia o aviso <a href=\"#LINK#\"> </a>, se você estiver usando o Microsoft Windows Server 2008.";
$MESS["WD_USECOMMANDLINE"] = "Para conectar-se a biblioteca como uma unidade de rede usando HTTPS / SSL, use <b> Iniciar> Executar> cmd </b>. Digite os seguintes comandos na linha de comando:";
$MESS["WD_EMPTY_PATH"] = "O caminho da rede não é especificado.";
$MESS["WD_CONNECTION_TITLE"] = "Bliblioteca de documentos de mapa como unidade de rede.";
$MESS["WD_MACOS_TITLE"] = "Biblioteca de documentos de mapa no Mac Os X";
$MESS["F_PREORDER"] = "Exibir mensagens na ordem direta";
$MESS["BPABL_STATUS_3"] = "Cancelando";
$MESS["BPCGDOC_INVALID_TYPE"] = "O tipo de parâmetro é indefinido.";
$MESS["WD_DOCUMENT_BP"] = "Processos de negócios de documentos";
$MESS["WD_DOCUMENT_ALT"] = "Propriedades do Documento";
$MESS["WD_OVERVIEW"] = "Substituir o arquivo existente (s)?";
$MESS["RATING_TYPE_LIKE_GRAPHIC"] = "Curtir(imagem)";
$MESS["WD_ACCESS_DENIED"] = "Acesso negado.";
$MESS["WD_FILE_MODIFIED"] = "Modificado";
$MESS["WD_SECTION_LIST_URL"] = "Página para a visualização de catálogos";
$MESS["WD_CONNECTOR_HELP_MAPDRIVE"] = "<h3>Mapeando a Biblioteca como um Unidade de Rede</h3>
<p>Para conectar uma biblioteca como um disco de rede usando o <b>gerenciador de arquivos</b>:
<ul>
<li>Execute o Windows Explorer</b>;</li>
<li>Selecione <i>Ferramentas > Mapear Unidade de Rede</i>. O assitente de disco de rede irá abrir:
<br /><br /><a href=\"javascript:ShowImg('#TEMPLATEFOLDER#/images/en/network_storage.png',629,459,'Map Network Drive');\">
<img width=\"250\" height=\"183\" border=\"0\" src=\"#TEMPLATEFOLDER#/images/en/network_storage_sm.png\" style=\"cursor: pointer;\" alt=\"Click to Enlarge\" /></a></li>
<li>No campo <b>Unidade</b>, especifique um letra para mapear a pasta;</li>
<li>No campo <b>Pasta</b>, digite o caminho para a biblioteca: <i>http://your_server/docs/shared/</i>. Se você quiser que esta pasta esteja disponível quando o sistema iniciar, marque a opção <b>Reconectar no início da sessão</b>;</li>
<li>Clique em <b>Pronto</b>. Se solicitado por Nome de Usuário e Senha, digite seu login e senha e então clique em <b>OK</b>.</li>
</ul>
</p>
<p>Mais tarde, você pode abrir a pasta no Windows Explorer onde a pasta será mostrada como um unidade sob Meu Computador, ou em qualquer outro gerenciador de arquivos.</p>";
$MESS["WD_CONNECTOR_HELP_OSX"] = "<h3>Conectando a Biblioteca no Mac OS e Mac OS X</h3>

<ul>
<li>Selecione o comando <i>Finder Ir->Conectar a Servidor</i>;</li>
<li>Digite on edereço da biblioteca em <b>Endereço do Servidor</b>:</p>
<p><a href=\"javascript:ShowImg('#TEMPLATEFOLDER#/images/en/macos.png',465,550,'Mac OS X');\">
<img width=\"235\" height=\"278\" border=\"0\" src=\"#TEMPLATEFOLDER#/images/en/macos_sm.png\" style=\"cursor: pointer;\" alt=\"Click to Enlarge\" /></a></li>
</ul>";
$MESS["WD_CONNECTOR_HELP_WEBFOLDERS"] = "<h3>Conectar Usando Pastas Web</h3>
<p>Certifique-se de que você tenha feito a modificação <a href=\"#URL_HELP##oswindowsreg\">adequada ao registro do sistema</a> e que o <a href=\"#URL_HELP##oswindowswebclient\">serviço Webclient esteja rodando</a>.</p>
<p>Um componente especial de conexão à pasta web é necessário para conectar-se à biblioteca de documentos. Siga as instruções em <a href=\"http://www.microsoft.com/downloads/details.aspx?displaylang=ru&FamilyID=17c36612-632e-4c04-9382-987622ed1d64\" target=\"_blank\">Microsoft website</a> ). </p>
<ul>
<li>Execute o Windows Explorer;</li>
<li>Selecione <b>Mapear Unidade de Rede</b>;</li>
<li>Clique no link <b>Conectar a um site Web que você pode usar para armazenar seus documentos e fotos</b>:</p>
<p><a href=\"javascript:ShowImg('#TEMPLATEFOLDER#/images/en/network_add_1.png',630,461,'Map Network Drive');\">
<img width=\"250\" height=\"183\" border=\"0\" src=\"#TEMPLATEFOLDER#/images/en/network_add_1_sm.png\" style=\"cursor: pointer;\" alt=\"Click to Enlarge\" /></a> <br />Isto irá executar o <b>Adicionar Local de Rede</b>.</li>
<li>Na janela do assistente, clique <b>Próximo</b>. A próxima janela do assistente irá aparecer;</li>
<li>Nesta janela, clique em <b>Escolher um local de rede personalizado</b> e então clique em <b>Próximo</b>:
<p><a href=\"javascript:ShowImg('#TEMPLATEFOLDER#/images/en/network_add_4.png',610,499,'Add Network Location');\">
<img width=\"250\" height=\"205\" border=\"0\" src=\"#TEMPLATEFOLDER#/images/en/network_add_4_sm.png\" style=\"cursor: pointer;\" alt=\"Click to Enlarge\" /></a></li>
<li>Aqui, no campo <b>Endereço da Internet ou rede</b>, digite a URL da pasta a ser mapeada no formato: <i>http://your_server/docs/shared/</i>;</li>
<li>Clique em <b>Próximo</b>. Se solicitado por <b>Nome de usuário</b> e <b>Senha</b>, entre seu login e senha e entãoi clique em <b>OK</b>.</li>
</ul>

<p>De agora em diante, você pode acessar a pasta clicando em <b>Executar > Ambiente de Redes > Nome da Pasta</b>.</p>";
?>