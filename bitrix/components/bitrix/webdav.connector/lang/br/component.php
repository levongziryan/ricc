<?
$MESS["W_IBLOCK_IS_NOT_INSTALLED"] = "O módulo Blocos de Informação não está instalado.";
$MESS["W_WEBDAV_IS_NOT_INSTALLED"] = "O módulo WebDav não está instalado.";
$MESS["WD_TITLE"] = "Ajudar";
$MESS["WD_OPEN_DOCUMENT"] = "Abrir documento";
$MESS["WD_DROP_CONFIRM"] = "Tem certeza de que deseja excluir a pasta #NAME# irreversívelmente?";
$MESS["WD_UPLOAD_SUCCESS"] = "O arquivo foi enviado com sucesso.";
?>