<?
$MESS["CP_BCI1_LDAP_SERVER_CHOOSE"] = "-- choix du serveur --";
$MESS["ISL_EMAIL"] = "E-mail";
$MESS["CP_BCI1_DEFAULT_EMAIL"] = "Adresse email par défaut";
$MESS["CP_BCI1_EMAIL_NOTIFY"] = "Adresse email d'avertissement pour les nouveaux utilisateurs";
$MESS["ISL_PERSONAL_WWW"] = "Page WWW";
$MESS["CP_BCI1_LOGIN_PROPERTY_XML_ID"] = "Identificateur XML de la propriété 'Identifiant'";
$MESS["CP_BCI1_PASSWORD_PROPERTY_XML_ID"] = "XML-identificateur de la propriété 'Mot de passe'";
$MESS["CP_BCI1_LDAP_AD_ID_PROPERTY_XML_ID"] = "XML-identificateur de l'attribut 'Compte Ad'";
$MESS["CP_BCI1_EMAIL_PROPERTY_XML_ID"] = "Identificateur XML de la propriété 'Courrier électronique'";
$MESS["CP_BCI1_GENERATE_PREVIEW"] = "Générer automatiquement l'image de l'annonce";
$MESS["CP_BCI1_EMAIL_NOTIFY_Y"] = "pour tous";
$MESS["ISL_PERSONAL_CITY"] = "Ville";
$MESS["CP_BCI1_GROUP_PERMISSIONS"] = "Groupes des utilisateurs autorisés à télécharger";
$MESS["ISL_PERSONAL_BIRTHDAY"] = "Date de naissance";
$MESS["CP_BCI1_DEACTIVATE"] = "Désactiver";
$MESS["ISL_WORK_POSITION"] = "Emplacement";
$MESS["ISL_PERSONAL_NOTES"] = "Notes";
$MESS["CP_BCI1_DETAIL_RESIZE"] = "Modifier l'image détaillée";
$MESS["ISL_NAME"] = "Prénom";
$MESS["CP_BCI1_INTERVAL"] = "Intervalle d'une étape en secondes (0 - effectuer le téléchargement en une seule étape)";
$MESS["CP_BCI1_ABSENCE_IBLOCK_ID"] = "Bloc d'information du tableau des absences";
$MESS["CP_BCI1_STATE_HISTORY_IBLOCK_ID"] = "Bloc d'information des remaniements";
$MESS["CP_BCI1_DEPARTMENTS_IBLOCK_ID"] = "Bloc d'information des subdivisions";
$MESS["CP_BCI1_USE_ZIP"] = "Utiliser la compression zip, si c'est disponible";
$MESS["CP_BCI1_PICTURE"] = "Image de l'annonce";
$MESS["ISL_LOGIN"] = "Connexion";
$MESS["CP_BCI1_DETAIL_HEIGHT"] = "Hauteur maximale admissible de l'image détaillée";
$MESS["CP_BCI1_PREVIEW_HEIGHT"] = "Hauteur maximale admissible de l'image de l'annonce";
$MESS["CP_BCI1_DETAIL_WIDTH"] = "Largeur admissible maximale de l'image détaillée";
$MESS["CP_BCI1_PREVIEW_WIDTH"] = "Largeur maximum admissible de l'image de l'annonce";
$MESS["ISL_PERSONAL_MOBILE"] = "Portable";
$MESS["CP_BCI1_EMAIL_NOTIFY_N"] = "ne pas envoyer";
$MESS["CP_BCI1_NONE"] = "aucun";
$MESS["ISL_PERSONAL_STATE"] = "Région";
$MESS["CP_BCI1_UPDATE_PROPERTIES"] = "Propriétés renouvelables";
$MESS["CP_BCI1_UPDATE_EMAIL"] = "Rafraîchir les adresses Adresse emaildes utilisateurs existant";
$MESS["CP_BCI1_UPDATE_LOGIN"] = "Renouveler les noms des utilisateurs existants";
$MESS["CP_BCI1_UPDATE_PASSWORD"] = "Renouveler les mots de passe des utilisateurs existants";
$MESS["CP_BCI1_EMAIL_NOTIFY_IMMEDIATELY"] = "Envoyer des notifications à la fois";
$MESS["ISL_SECOND_NAME"] = "Deuxième prénom";
$MESS["ISL_PERSONAL_PAGER"] = "Pager";
$MESS["ISL_PERSONAL_GENDER"] = "Sexe";
$MESS["ISL_PERSONAL_POST_ADDRESS"] = "Adresse postale";
$MESS["ISL_PERSONAL_ZIP"] = "Code Postal";
$MESS["CP_BCI1_XML"] = "Lier aux propriétés supplémentaires XML";
$MESS["CP_BCI1_STRUCTURE_CHECK"] = "Effectuer la vérification de l'intégrité de la structure";
$MESS["ISL_PERSONAL_PROFESSION"] = "Fonction";
$MESS["ISL_WORK_PHONE"] = "Téléphone de travail";
$MESS["CP_BCI1_FILE_SIZE_LIMIT"] = "Volume de la partie de fichier dont le chargement s'effectue en une seule fois dans le même temps (en bytes)";
$MESS["CP_BCI1_SITE_ID"] = "Site de rattachement des utilisateurs";
$MESS["CP_BCI1_SITE_LIST"] = "Sites qui nécessitent d'attacher les blocs d'information lors de la création";
$MESS["CP_BCI1_LDAP_SERVER"] = "Serveur LDAP";
$MESS["CP_BCI1_CREATE"] = "Créer au besoin";
$MESS["ISL_PERSONAL_COUNTRY"] = "Pays";
$MESS["CP_BCI1_CURRENT"] = "Actuel";
$MESS["ISL_PERSONAL_PHONE"] = "Numéro de téléphone";
$MESS["ISL_PERSONAL_PHONES"] = "Téléphones";
$MESS["CP_BCI1_IBLOCK_TYPE"] = "Type de bloc d'information";
$MESS["CP_BCI1_EMAIL_NOTIFY_E"] = "seuls les nouveaux sujets";
$MESS["CP_BCI1_EMAIL"] = "Message";
$MESS["CP_BCI1_DELETE"] = "supprimer";
$MESS["ISL_PERSONAL_STREET"] = "Adresse réelle";
$MESS["ISL_PERSONAL_FAX"] = "Fax";
$MESS["ISL_LAST_NAME"] = "Nom";
$MESS["CP_BCI1_UNIQUE_EMAIL"] = "Générer une adresse email unique";
$MESS["ISL_PERSONAL_PHOTO"] = "Photo";
$MESS["CP_BCI1_LOGIN_TEMPLATE"] = "Modèle de la génération automatique du nom d'utilisateur";
$MESS["ISL_PERSONAL_ICQ"] = "Numéro ICQ";
?>