<?
$MESS["B24_BANNER_TRIAL_SUBTITLE"] = "30 jours gratuits";
$MESS["B24_BANNER_MESSENGER_SUBTITLE"] = "avec le Bitrix24.Drive";
$MESS["B24_BANNER_MOBILE_URL_INTRANET"] = "http://www.bitrixsoft.com/products/intranet/features/bitrixmobile.php";
$MESS["B24_BANNER_MOBILE_URL"] = "http://www.bitrix24.com/features/mobile-and-desktop-apps.php";
$MESS["B24_BANNER_MESSENGER_URL"] = "http://www.bitrix24.com/features/mobile-and-desktop-apps.php";
$MESS["B24_BANNER_BUTTON_TRY"] = "Activer";
$MESS["B24_BANNER_MESSENGER_TITLE"] = "Application de bureau";
$MESS["B24_BANNER_MOBILE_SUBTITLE"] = "pour iOS et Android";
$MESS["B24_BANNER_TRIAL_EXPIRED_SUBTITLE"] = "jusqu'à la fin du mode de démonstration";
$MESS["B24_BANNER_CLOSE"] = "Fermer";
$MESS["B24_BANNER_BUTTON_BUY"] = "Acheter";
$MESS["B24_BANNER_MOBILE_TITLE"] = "Application mobile";
$MESS["B24_BANNER_NETWORK_START"] = "Conncter";
$MESS["B24_BANNER_NETWORK_HINT_TITLE"] = "Conncter";
$MESS["B24_BANNER_TRIAL_EXPIRED_TITLE"] = "<br>#DAYS# jours restants";
$MESS["B24_BANNER_TRIAL_EXPIRED_TITLE_1"] = "Il reste <br>#DAYS#";
$MESS["B24_BANNER_NETWORK_HINT_SUBTITLE"] = "avec clients, partenaires et collègues de business.";
$MESS["B24_BANNER_TRIAL_TITLE"] = "Tester caractéristiques des régimes professionnels";
$MESS["B24_BANNER_BUTTON_INSTALL"] = "Installation";
$MESS["B24_BANNER_BUTTON_SHOW"] = "En savoir plus";
$MESS["B24_BANNER_WEBINAR_URL"] = "http://www.bitrix24.com/support/webinars.php?utm_source=cp&utm_medium=referral&utm_campaign=cp_button";
$MESS["B24_BANNER_WEBINAR_TITLE"] = "Webinaires hebdomadaires";
$MESS["B24_BANNER_MARKETPLACE_SUBTITLE"] = "pour Bitrix24";
$MESS["B24_BANNER_MARKETPLACE_TITLE"] = "Catalogue d'App";
$MESS["B24_BANNER_BUTTON_SEE"] = "Réservez votre place";
$MESS["B24_BANNER_WEBINAR_SUBTITLE"] = "La façon la plus rapide d'apprendre Bitrix24";
?>