<?
$MESS["CRM_PERMISSION_DENIED"] = "Você não tem permissão para acessar este formulário.";
$MESS["JHGFDC_STOP"] = "Excluir";
$MESS["JHGFDC_STOP_ALT"] = "Tem certeza de que deseja excluir este registro?";
$MESS["INTS_TASKS_NAV"] = "Registros";
$MESS["BPWC_WLC_NOT_DETAIL"] = "Editar";
$MESS["BPWC_WLC_ACTIVE"] = "Ativo";
$MESS["BPWC_WLC_DATE_CREATE"] = "Criado em";
$MESS["BPWC_WLC_DATE_UPDATE"] = "Modificada em";
$MESS["BPWC_WLC_NAME"] = "Nome";
$MESS["BPWC_WLC_URL"] = "Endereço";
$MESS["BPWC_WLC_LABEL"] = "Marca de modificação";
$MESS["BPWC_WLC_SIZE"] = "Tamanho do bloco";
$MESS["BPWC_WLC_STATUS"] = "ltimo status";
$MESS["BPWC_WLC_YES"] = "Sim";
$MESS["BPWC_WLC_NO"] = "Não";
$MESS["BPABL_PAGE_TITLE"] = "Contatos da loja";
$MESS["BPWC_WLC_CHECK"] = "Testar de conexão";
$MESS["BPWC_WLC_SYNC"] = "Sincronizar";
$MESS["BPWC_WLC_IMPORT_PERIOD"] = "Período de sincronização";
$MESS["CRM_EXT_SALE_C1NO_CONNECT"] = "Nenhum contato da loja foi encontrado.";
$MESS["CRM_EXT_SALE_C1ERROR_CONNECT"] = "Erro ao contactar a loja.";
$MESS["CRM_EXT_SALE_C1STATUS"] = "Status retornado %s %s";
$MESS["CRM_EXT_SALE_C1NO_AUTH"] = "Não é possível autorizar na loja.";
$MESS["CRM_EXT_SALE_C1SUCCESS"] = "Conexão estabelecida.";
$MESS["BPWC_WLC_IMPORT_PROBABILITY"] = "Probabilidade padrão de negócio (%)";
$MESS["BPWC_WLC_IMPORT_PUBLIC"] = "O negócio é público por padrão";
$MESS["BPWC_WLC_IMPORT_PREFIX"] = "Prefixo do nome do negócio";
$MESS["BPWC_WLC_NOT_AUTO"] = "Manual";
$MESS["BPWC_WLC_MESSAGE"] = "Status";
$MESS["BPWC_WLC_NEED_FIRST_SYNC1"] = "Importação precisa ser executado manualmente pela primeira vez.";
$MESS["BPWC_WLC_NEED_FIRST_SYNC1_DO"] = "Iniciar importação";
$MESS["BPWC_WLC_NEED_FIRST_SYNC2"] = "Conexão estabelecida com sucesso";
$MESS["BPWC_WLC_NEED_FIRST_SYNC3"] = "Erro de importação:";
$MESS["BPWC_WLC_IMPORT_AGENT"] = "Período de importação (minutos)";
$MESS["BPWC_WLC_MANUAL"] = "Manual";
$MESS["BPWC_WLC_LAST_STATUS_DATE"] = "ltima importação em";
?>