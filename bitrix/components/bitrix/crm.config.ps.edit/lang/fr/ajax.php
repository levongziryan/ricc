<?
$MESS["CRM_PS_ACCESS_DENIED"] = "Accès interdit";
$MESS["CRM_PS_MODULE_NOT_INSTALLED"] = "Le module de la CRM n'a pas été installé.";
$MESS["CRM_PS_NOT_CONFIGURED"] = "Veuillez configurer le gestionnaire avant d'essayer de générer une clé privée.";
$MESS["CRM_PS_ALREADY_CONFIGURED"] = "La clé privée existe déjà.";
$MESS["CRM_PS_NOT_SUPPORTED"] = "Les demandes de certificat ne peuvent être effectuées que dans la version basée sur le cloud.";
?>