<?
$MESS["CRM_PS_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PS_ACCESS_DENIED"] = "Acceso denegado";
$MESS["CRM_PS_NOT_CONFIGURED"] = "Configure el controlador antes de intentar generar una clave privada.";
$MESS["CRM_PS_ALREADY_CONFIGURED"] = "La clave privada ya existe.";
$MESS["CRM_PS_NOT_SUPPORTED"] = "La solicitud de certificado sólo se puede realizar en la versión basada en la nube.";
?>