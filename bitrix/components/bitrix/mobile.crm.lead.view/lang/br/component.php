<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["CRM_LEAD_VIEW_NOT_FOUND"] = "Não foi possível encontrar o lead ##ID#.";
$MESS["CRM_LEAD_VIEW_RESPONSIBLE_NOT_ASSIGNED"] = "[Atribuído]";
?>