<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_LEAD_VIEW_NOT_FOUND"] = "No se puede encontrar el prospecto ##ID#.";
$MESS["CRM_LEAD_VIEW_RESPONSIBLE_NOT_ASSIGNED"] = "[sin asignar]";
?>