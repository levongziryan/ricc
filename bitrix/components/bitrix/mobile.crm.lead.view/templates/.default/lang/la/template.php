<?
$MESS["M_CRM_LEAD_VIEW_PULL_TEXT"] = "Pulsar hacia abajo para actualizar...";
$MESS["M_CRM_LEAD_VIEW_DOWN_TEXT"] = "Soltar para actualizar...";
$MESS["M_CRM_LEAD_VIEW_LOAD_TEXT"] = "Actualizando...";
$MESS["M_CRM_LEAD_VIEW_ID"] = "##ID#";
$MESS["M_CRM_LEAD_VIEW_STATUS"] = "Estado";
$MESS["M_CRM_LEAD_VIEW_ACTION_CALL_TO"] = "Llamada";
$MESS["M_CRM_LEAD_VIEW_NO_TITLE"] = "[sin título]";
$MESS["M_CRM_LEAD_VIEW_PRODUCT_ROWS"] = "Productos";
$MESS["M_CRM_LEAD_VIEW_ACTIVITY_LIST"] = "Actividades";
$MESS["M_CRM_LEAD_VIEW_EVENT_LIST"] = "Historial";
$MESS["M_CRM_LEAD_VIEW_RESPONSIBLE"] = "Persona responsable";
$MESS["M_CRM_LEAD_VIEW_COMMENT"] = "Comentarios";
$MESS["M_CRM_LEAD_VIEW_COMMENT_CUT"] = "Leer más...";
$MESS["M_CRM_LEAD_VIEW_ADDRESS"] = "Dirección";
$MESS["M_CRM_LEAD_VIEW_PHONE"] = "Teléfono";
$MESS["M_CRM_LEAD_VIEW_EMAIL"] = "Correo electrónico";
$MESS["M_CRM_LEAD_VIEW_WEB"] = "Sitio Web";
$MESS["M_CRM_LEAD_VIEW_IM"] = "Messenger";
$MESS["M_CRM_LEAD_SOURCE"] = "Origen
";
$MESS["M_CRM_LEAD_VIEW_EDIT"] = "Editar";
$MESS["M_CRM_LEAD_VIEW_DELETE"] = "Eliminar";
$MESS["M_CRM_LEAD_VIEW_DELETION_CONFIRMATION"] = "¿Está seguro que desea eliminar el prospecto?";
$MESS["M_CRM_LEAD_VIEW_DELETION_TITLE"] = "Eliminar Prospecto";
?>