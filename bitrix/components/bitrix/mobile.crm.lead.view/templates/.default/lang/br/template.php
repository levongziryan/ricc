<?
$MESS["M_CRM_LEAD_VIEW_PULL_TEXT"] = "Puxe para baixo para atualizar ...";
$MESS["M_CRM_LEAD_VIEW_DOWN_TEXT"] = "Solte para atualizar ...";
$MESS["M_CRM_LEAD_VIEW_LOAD_TEXT"] = "Atualizando ...";
$MESS["M_CRM_LEAD_VIEW_ID"] = "##ID#";
$MESS["M_CRM_LEAD_VIEW_STATUS"] = "Status";
$MESS["M_CRM_LEAD_VIEW_ACTION_CALL_TO"] = "Chamar";
$MESS["M_CRM_LEAD_VIEW_NO_TITLE"] = "[Sem título]";
$MESS["M_CRM_LEAD_VIEW_PRODUCT_ROWS"] = "Produtos";
$MESS["M_CRM_LEAD_VIEW_ACTIVITY_LIST"] = "Atividades";
$MESS["M_CRM_LEAD_VIEW_EVENT_LIST"] = "Histórico";
$MESS["M_CRM_LEAD_VIEW_RESPONSIBLE"] = "Responsável";
$MESS["M_CRM_LEAD_VIEW_COMMENT"] = "Comentar";
$MESS["M_CRM_LEAD_VIEW_COMMENT_CUT"] = "Leia mais ...";
$MESS["M_CRM_LEAD_VIEW_ADDRESS"] = "Endereço";
$MESS["M_CRM_LEAD_VIEW_PHONE"] = "Telefone";
$MESS["M_CRM_LEAD_VIEW_EMAIL"] = "E-mail";
$MESS["M_CRM_LEAD_VIEW_WEB"] = "Site";
$MESS["M_CRM_LEAD_VIEW_IM"] = "Messenger";
$MESS["M_CRM_LEAD_SOURCE"] = "Fonte";
$MESS["M_CRM_LEAD_VIEW_EDIT"] = "Editar";
$MESS["M_CRM_LEAD_VIEW_DELETE"] = "Excluir";
$MESS["M_CRM_LEAD_VIEW_DELETION_CONFIRMATION"] = "Tem certeza de que deseja excluir o lead?";
$MESS["M_CRM_LEAD_VIEW_DELETION_TITLE"] = "Excluir Lead";
?>