<?
$MESS["LEARNING_TRANSCRIPT_NOT_FOUND"] = "Utilisateur introuvable";
$MESS["LEARNING_TRANSCRIPT_PERMISSION_DENIED"] = "Vous n'avez pas de droits pour voir le résumé.";
$MESS["LEARNING_MODULE_NOT_FOUND"] = "Module des blogs non installé.";
$MESS["LEARNING_TRANSCRIPT_ERROR"] = "Erreur! S'il vous plaît contacter l'administrateur.";
?>