<?
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["CRM_COMM_LIST_ENTITY_TYPE_NOT_DEFINED"] = "Tipo de propietario no se especifica.";
$MESS["CRM_COMM_LIST_INVALID_ENTITY_TYPE"] = "Este tipo de propietario no es compatible.";
$MESS["CRM_COMM_LIST_ENTITY_ID_NOT_DEFINED"] = "No se especifica el ID del propietario.";
?>