<?
$MESS["INTR_MAIL_SUCCESS_MESSAGE_TITLE"] = "Parabéns! Suas caixas de e-mail foram vinculadas com sucesso.";
$MESS["INTR_MAIL_SUCCESS_MESSAGE_TEXT"] = "Agora você pode começar a usar seu e-mail ou voltar para a página de configurações de e-mail.";
$MESS["INTR_MAIL_SUCCESS_MAIL_GO"] = "Visualização de e-mails";
$MESS["INTR_MAIL_SUCCESS_MAIL_HOME"] = "Voltar para as configurações";
?>