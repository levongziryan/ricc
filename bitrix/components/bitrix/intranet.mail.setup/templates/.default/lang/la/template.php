<?
$MESS["INTR_MAIL_CHOOSE"] = "Elija el servicio de e-mail que va a utilizar con su Intranet";
$MESS["INTR_MAIL_STATUS"] = "Estado";
$MESS["INTR_MAIL_CHECK"] = "Comprobar";
$MESS["INTR_MAIL_CHECK_TEXT"] = "Última comprobación #DATE#";
$MESS["INTR_MAIL_CHECK_JUST_NOW"] = "hace unos segundos";
$MESS["INTR_MAIL_CHECK_TEXT_NA"] = "No hay datos.";
$MESS["INTR_MAIL_CHECK_SUCCESS"] = "Exitoso";
$MESS["INTR_MAIL_CHECK_ERROR"] = "Error";
$MESS["INTR_MAIL_CHECK_NA"] = "N/A";
$MESS["INTR_MAIL_INP_PORT"] = "Puerto";
$MESS["INTR_MAIL_INP_ENCRYPT"] = "Usuario SSL";
$MESS["INTR_MAIL_INP_LOGIN"] = "Usuario";
$MESS["INTR_MAIL_INP_PASS"] = "Contraseña";
$MESS["INTR_MAIL_INP_ENCRYPT_YES"] = "Si";
$MESS["INTR_MAIL_INP_ENCRYPT_NO"] = "No";
$MESS["INTR_MAIL_REMOVE_CONFIRM"] = "¿Eliminar preferencias de conexión para este buzón de correo?";
$MESS["INTR_MAIL_CREATE"] = "Conectar";
$MESS["INTR_MAIL_UPDATE"] = "Guardar";
$MESS["INTR_MAIL_REMOVE"] = "Eliminar";
$MESS["INTR_MAIL_COMPLETE"] = "El buzón de correo se ha integrado con éxito.";
$MESS["INTR_MAIL_REMOVE_COMPLETE"] = "Conexión y registro de preferencias se han retirado.";
$MESS["INTR_MAIL_SETUP_HINT"] = "Utilice su perfil para integrar un buzón de correo en cualquier momento.";
$MESS["INTR_MAIL_LINK"] = "Ver e-mail";
$MESS["INTR_MAIL_SETUP"] = "Proporcionar parámetros de conexión IMAP";
$MESS["INTR_MAIL_INP_SERVER"] = "Dirección del servidor IMAP";
$MESS["INTR_MAIL_INP_LINK"] = "URL de la interface web del servidor de e-mail";
$MESS["INTR_MAIL_SETUP_LINK"] = "Establecer nueva conexión";
?>