<?
$MESS["INTR_MAIL_SUCCESS_MESSAGE_TITLE"] = "¡Felicitaciones! Su buzón de correo se han conectado correctamente.";
$MESS["INTR_MAIL_SUCCESS_MESSAGE_TEXT"] = "Ahora puede comenzar a navegar por la bandeja de entrada o volver a la página de configuración de e-mail.";
$MESS["INTR_MAIL_SUCCESS_MAIL_GO"] = "Ver e-mails";
$MESS["INTR_MAIL_SUCCESS_MAIL_HOME"] = "Volver a la configuración";
?>