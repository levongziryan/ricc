<?
$MESS["INTR_MAIL_SUCCESS_MAIL_HOME"] = "Revenir aux réglages";
$MESS["INTR_MAIL_SUCCESS_MESSAGE_TEXT"] = "Vous pouvez passer dans votre boîte aux lettres ou retourner à la page principale de connexion de la messagerie.";
$MESS["INTR_MAIL_SUCCESS_MAIL_GO"] = "Accéder à la boîte aux lettres";
$MESS["INTR_MAIL_SUCCESS_MESSAGE_TITLE"] = "Félicitations ! La connexion a été établie avec succès.";
?>