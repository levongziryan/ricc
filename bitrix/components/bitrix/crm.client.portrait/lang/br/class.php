<?
$MESS["CRM_CLIENT_PORTRAIT_COMPANY_TITLE"] = "Perfil da empresa";
$MESS["CRM_CLIENT_PORTRAIT_CONTACT_TITLE"] = "Perfil do contato";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "O módulo de CRM não está instalado.";
$MESS["CRM_PERMISSION_DENIED"] = "Acesso negado";
$MESS["RESPONSIBLE_NOT_ASSIGNED"] = "[não atribuido]";
$MESS["CRM_CLIENT_PORTRAIT_COMPANY_LOAD_TITLE"] = "Carga de comunicação produzida pela empresa";
$MESS["CRM_CLIENT_PORTRAIT_CONTACT_LOAD_TITLE"] = "Carga de comunicação produzida pelo contato";
?>