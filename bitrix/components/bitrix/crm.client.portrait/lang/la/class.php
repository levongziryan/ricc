<?
$MESS["CRM_CLIENT_PORTRAIT_COMPANY_TITLE"] = "Perfil de la compañía";
$MESS["CRM_CLIENT_PORTRAIT_CONTACT_TITLE"] = "Perfil del contacto";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "El módulo CRM no está instalado,";
$MESS["CRM_PERMISSION_DENIED"] = "Acceso denegado";
$MESS["RESPONSIBLE_NOT_ASSIGNED"] = "[sin asignar]";
$MESS["CRM_CLIENT_PORTRAIT_COMPANY_LOAD_TITLE"] = "Carga de comunicación producida por la compañía";
$MESS["CRM_CLIENT_PORTRAIT_CONTACT_LOAD_TITLE"] = "Carga de comunicación producida por el contacto";
?>