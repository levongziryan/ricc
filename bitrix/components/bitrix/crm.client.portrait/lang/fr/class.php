<?
$MESS["CRM_CLIENT_PORTRAIT_COMPANY_TITLE"] = "Profil de l'entreprise";
$MESS["CRM_CLIENT_PORTRAIT_CONTACT_TITLE"] = "Profil du contact";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "Le module CRM n'est pas installé.";
$MESS["CRM_PERMISSION_DENIED"] = "Accès refusé";
$MESS["RESPONSIBLE_NOT_ASSIGNED"] = "[non assigné]";
$MESS["CRM_CLIENT_PORTRAIT_COMPANY_LOAD_TITLE"] = "Charge de communication produite par l'entreprise";
$MESS["CRM_CLIENT_PORTRAIT_CONTACT_LOAD_TITLE"] = "Charge de communication produite par le contact";
?>