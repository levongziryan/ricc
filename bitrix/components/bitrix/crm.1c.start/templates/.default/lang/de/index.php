<?
$MESS["CRM_1C_START_FACE_CARD_NAME"] = "Face card";
$MESS["CRM_1C_START_FACE_CARD_ADV_TITLE"] = "Die Kunden brauchen ihre Kundenkarten nicht immer dabei zu haben";
$MESS["CRM_1C_START_FACE_CARD_ADV_1"] = "Einfache Verbindung";
$MESS["CRM_1C_START_FACE_CARD_ADV_2"] = "Sofortiger Datenaustausch";
$MESS["CRM_1C_START_FACE_CARD_ADV_3"] = "Vollständiges Kundenprofil";
$MESS["CRM_1C_START_FACE_CARD_INFO_TITLE"] = "Die Kundenkarten gehören ab jetzt der Vergangenheit an: nur die Bilder der Kundengesichter sind erforderlich";
$MESS["CRM_1C_START_FACE_CARD_INFO_TEXT"] = "Kundenkarten bedeuten einen Vorteil für beide Seiten: Unternehmen brauchen sie, um Kunden wieder zu gewinnen, und Kunden brauchen sie, um Rabatte zu bekommen. Jedoch ist das für Kleinunternehmen oft zu teuer, physische Karten zu verteilen, und auch Kunden werden oft müde, Dutzende Kundenkarten immer mithaben zu müssen. Nutzen Sie 1C Face Cards, um beide Fliegen mit einer Klappe zu schlagen.";
$MESS["CRM_1C_START_FACE_CARD_INFO_1"] = "Identifizieren Sie Ihre Kunden in den Offline-Shops";
$MESS["CRM_1C_START_FACE_CARD_INFO_2"] = "Nutzen Sie die Bilder Ihrer Kunden anstatt von Karten";
$MESS["CRM_1C_START_FACE_CARD_INFO_3"] = "Sofortige Eingabe neuer Kundendaten in die Datenbank";
$MESS["CRM_1C_START_FACE_CARD_DO_START"] = "Verbinden";
$MESS["CRM_1C_START_FACE_CARD_CONSENT_AGREED"] = "ich akzeptiere die Nutzungsbedingungen";
$MESS["CRM_1C_START_FACE_CARD_WARN_TEXT"] = "Beachten Sie bitte, dass der Service der Gesichtserkennung selbstständig funktioniert und separat bezahlt werden muss.";
$MESS["CRM_1C_START_FACE_CARD_CONSENT_TITLE"] = "Nutzungsbedingungen";
$MESS["CRM_1C_START_FACE_CARD_B24_BLOCK_TITLE"] = "Face Card ist verfügbar in den Tarifen CRM Retail, Team und Enterprise";
$MESS["CRM_1C_START_FACE_CARD_B24_BLOCK_TEXT"] = "Nutzen Sie Face Card, um Ihr  Loyalitätsprogramm zu starten. Installieren Sie die Face Card und verbinden Sie eine Kamera. Beachten Sie bitte, dass die Gesichtserkennung ein selbstständiger Service ist, welcher separat bezahlt werden soll. Der Tarif CRM Retail bietet die Gesichtserkennung kostenlos an.";
?>