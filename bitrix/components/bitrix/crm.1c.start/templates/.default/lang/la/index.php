<?
$MESS["CRM_1C_START_FACE_CARD_NAME"] = "Face card";
$MESS["CRM_1C_START_FACE_CARD_ADV_TITLE"] = "Los clientes no necesitan llevar sus tarjetas de fidelidad";
$MESS["CRM_1C_START_FACE_CARD_ADV_1"] = "Conectividad fácil";
$MESS["CRM_1C_START_FACE_CARD_ADV_2"] = "Intercambio instantáneo de datos";
$MESS["CRM_1C_START_FACE_CARD_ADV_3"] = "Perfil completo del cliente";
$MESS["CRM_1C_START_FACE_CARD_INFO_TITLE"] = "Deje las tarjetas de fidelidad en el pasado: sólo se requiere la imagen facial del cliente";
$MESS["CRM_1C_START_FACE_CARD_INFO_TEXT"] = "Las tarjetas de fidelidad son beneficiosas para ambos lados del mostrador: las empresas las usan para retener clientes, mientras que los clientes quieren que las tarjetas obtengan descuentos. Las pequeñas empresas, sin embargo, rara vez pueden pagar tarjetas físicas, mientras que los clientes se están cansando de acumular tarjetas en sus billeteras. ¡Utilice 1C Face Cards para matar ambos pájaros de un solo tiro!";
$MESS["CRM_1C_START_FACE_CARD_INFO_1"] = "Identifique a sus clientes en tiendas sin conexión";
$MESS["CRM_1C_START_FACE_CARD_INFO_2"] = "Utilice la imagen facial del cliente en lugar de la tarjeta";
$MESS["CRM_1C_START_FACE_CARD_INFO_3"] = "Inscripción instantánea de un nuevo cliente en la base de datos";
$MESS["CRM_1C_START_FACE_CARD_DO_START"] = "Conectar";
$MESS["CRM_1C_START_FACE_CARD_CONSENT_AGREED"] = "Acepto los términos";
$MESS["CRM_1C_START_FACE_CARD_WARN_TEXT"] = "Tenga en cuenta que el servicio de reconocimiento facial es un servicio independiente y se paga por separado.";
$MESS["CRM_1C_START_FACE_CARD_CONSENT_TITLE"] = "Términos de Uso";
$MESS["CRM_1C_START_FACE_CARD_B24_BLOCK_TITLE"] = "Face Card está disponible en el CRM en los planes Retail, Team y Enterprise";
$MESS["CRM_1C_START_FACE_CARD_B24_BLOCK_TEXT"] = "Utilice Face Card para iniciar su programa de fidelidad. Instale Face Card en 1C y conecte una cámara. Tenga en cuenta que el servicio de reconocimiento facial es un servicio independiente y se paga por separado. El plan CRM Retail ofrece reconocimiento facial de forma gratuita.";
?>