<?
$MESS["CRM_1C_START_EXCHANGE_NAME"] = "Integración del CRM";
$MESS["CRM_1C_START_EXCHANGE_ADV_TITLE"] = "Utilice 1C para obtener datos de ventas sin conexión en su Bitrix24";
$MESS["CRM_1C_START_EXCHANGE_ADV_1"] = "Fácil conectividad";
$MESS["CRM_1C_START_EXCHANGE_ADV_2"] = "Intercambio instantáneo de datos";
$MESS["CRM_1C_START_EXCHANGE_ADV_3"] = "Control de pago";
$MESS["CRM_1C_START_EXCHANGE_INFO_TITLE"] = "Dos opciones para integrar Bitrix24 y 1C: facturas y catálogo comercial";
$MESS["CRM_1C_START_EXCHANGE_INFO_TEXT"] = "1C asegura que su catálogo de productos del CRM siempre estará actualizado. Los operadores del CRM siempre conocerán el estado de pago de facturas.";
$MESS["CRM_1C_START_EXCHANGE_INFO_1"] = "Factura creada en CRM";
$MESS["CRM_1C_START_EXCHANGE_INFO_2"] = "Enviar facturas a 1C";
$MESS["CRM_1C_START_EXCHANGE_INFO_3"] = "Utilice 1C para pagar facturas, enviar ordenes, editar productos y valores de factura. Todos estos datos se sincronizarán con el CRM.";
$MESS["CRM_1C_START_EXCHANGE_INFO_4"] = "Cuando los datos se actualizan en el lado del CRM, todos los cambios se sincronizarán a 1C.";
$MESS["CRM_1C_START_EXCHANGE_NOTICE"] = "El intercambio de productos es unidireccional: todos los cambios realizados en los productos de 1C se reflejarán en el CRM.";
$MESS["CRM_1C_START_EXCHANGE_DO_START"] = "Habilitar 1c: intercambio de datos empresariales";
?>