<?
$MESS["NEXT_BUTTON"] = "Далее";
$MESS["PREVIOUS_BUTTON"] = "Назад";
$MESS["WELCOME_TEXT"] = "Данный мастер поможет вам за 6 шагов настроить Внутренний портал государственной организации и начать работу с контентом и настройками.<br /><br /> Вам необходимо выбрать дизайн вашего портала из предложенных вариантов, цветовую схему и указать базовые настройки.";



$MESS["WELCOME_STEP_TITLE"] = "Начало настройки";

$MESS["SELECT_TEMPLATE_TITLE"] = "Дизайн портала";
$MESS["SELECT_TEMPLATE_SUBTITLE"] = "Выберите из списка шаблон дизайна для вашего портала";


$MESS["SELECT_THEME_TITLE"] = "Цветовая схема";
$MESS["SELECT_THEME_SUBTITLE"] = "Выберите цветовую схему для вашего портала";

$MESS["INSTALL_SERVICE_FINISH_STATUS"] = "Настройка завершена";
$MESS ['INST_ERROR_OCCURED'] = "Внимание! На данном шаге произошла ошибка установки продукта.";
$MESS ['INST_TEXT_ERROR'] = "Текст ошибки";
$MESS ['INST_ERROR_NOTICE'] = "Повторите установку текущего шага. В случае повторения ошибки пропустите шаг.";
$MESS ['INST_RETRY_BUTTON'] = "Повторить шаг";
$MESS ['INST_SKIP_BUTTON'] = "Пропустить шаг";


$MESS ['FINISH_STEP_TITLE'] = "Окончание настройки";

$MESS ['FINISH_STEP_CONTENT'] = '<b>Поздравляем!</b><br />
<br />
Настройка &laquo;1С-Битрикс: Внутренний портал государственной организации&raquo; завершена. Вы можете перейти на портал и продолжить работу.<br /><br />
Для начала рекомендуем ознакомиться с руководством пользователя.<br /><br />
<div><a href="/bitrix/wizards/bitrix/gosportal/gov-internal-portal-guide.pdf" target="_blank" class="button-next"><span>Руководство пользователя</span></a></div><br /><br />
<br />
Вы можете продолжить настройку системы и выполнить загрузку данных о сотрудниках вашей организации из:
<ul>
<li>каталога Active Directory/LDAP;</li>
<li>программного комплекса &laquo;1С: Зарплата и управление персоналом&raquo;;</li>
<li>или файла данных в формате CSV (<a href="/bitrix/admin/user_import.php?getSample=csv">пример файла</a>).</li>
</ul>
<div><a href="/bitrix/admin/user_import.php?lang=ru" target="_blank"><span>Импортировать пользователей</span></a></div>
<br />
Начать работу с системой мы рекомендуем со знакомства с
<a href="/services/learning/" target="_blank">учебным курсом по работе с порталом</a>.<br />';
$MESS ['WIZARD_WAIT_WINDOW_TEXT'] = "Установка данных...";
$MESS ['INST_JAVASCRIPT_DISABLED'] = "Для установки мастера необходимо включить JavaScript. По-видимому, JavaScript либо не поддерживается браузером, либо отключен. Измените настройки браузера и затем <a href=\"\">повторите попытку</a>.";

$MESS["wiz_ldap_warn"]="Внимание!<br />Если необходима интеграция Внутреннего портала государственной организации с Active Directory, требуется PHP с установленным LDAP модулем (php_ldap).";
$MESS["wiz_template"]="Выберите шаблон портала";
$MESS["wiz_template_color"]="Выберите цветовую схему шаблона";
$MESS["wiz_settings"]="Настройка портала";
$MESS["wiz_install"]="Установить";
$MESS["wiz_slogan"]="Государственная организация";
$MESS["wiz_company_name"]="Название организации:";
$MESS["wiz_company_logo"]="Логотип организации:";
$MESS["wiz_allow_anonym"]="Разрешить неавторизованным пользователям просматривать портал";
$MESS["wiz_allow_register"]="Разрешить пользователям регистрироваться самостоятельно";
$MESS["wiz_demo_structure"]="Установить демонстрационный пример структуры организации";
$MESS["wiz_allow_ldap"]="Разрешить пользователям Active Directory авторизовываться на портале";
$MESS["wiz_allow_ldap_warn"]="необходим PHP с установленным LDAP модулем php_ldap";
$MESS["wiz_allow_ldap1"]="Разрешить пользователям Active Directory авторизовываться на портале<br />(требуется настройка параметров Active Directory)";
$MESS["wiz_galochka"]="Если вы устанавливаете продукт для ознакомительных целей, то данную галочку рекомендуется оставить отмеченной.";
$MESS["wiz_galochka_structure"] = "Внимание! При переустановке структуры будут восстановлены все информационные блоки, форумы и т.п. Переустановить данные?";
$MESS["wiz_structure_data"] = "Переустановить структуру данных.";
$MESS["wiz_ldap_settings"]="Настройки Active Directory";
$MESS["wiz_ldap_error"]="Ошибка соединения к серверу";
$MESS["wiz_ldap_error1"]="Ошибка авторизации на сервер";
$MESS["wiz_ldap_success"]="Соединение установлено успешно";
$MESS["wiz_ldap_error_root"]="Введите правильный корень дерева";
$MESS["wiz_ldap_server"]="Сервер:порт:";
$MESS["wiz_ldap_login"]="Административный логин:";
$MESS["wiz_ldap_pass"]="Административный пароль:";
$MESS["wiz_ldap_check"]="Проверить";
$MESS["wiz_ldap_root"]="Корень дерева (base DN):";
$MESS["wiz_ldap_select"]="выберите из списка";
$MESS["wiz_ldap_groups"]="Соответствие групп пользователей";
$MESS["wiz_ldap_server1"]="Сервер Active Directory";
$MESS["wiz_ldap_server_err"]="Ошибка при сохранении настроек сервера.";
$MESS["wiz_ldap_group_portal"]="Группа во внутреннем портале государственной организации";
$MESS["wiz_ldap_group_ldap"]="Группа в Active Directory";
$MESS["wiz_install_data"]="Установка данных";
$MESS["wiz_go"]="Перейти на портал";
$MESS["wiz_ldap_require"]="Внимание!<br />Для интеграция Внутреннего портала государственной организации с Active Directory на страницe <a target=\"_blank\" href=\"/bitrix/admin/module_admin.php?lang=#LANGUAGE_ID#\">Модули</a> необходимо установить модуль <i>AD/LDAP интеграция</i>.";
$MESS["wiz_ldap_require1"]="необходимо установить модуль AD/LDAP на страницe <a target=\"_blank\" href=\"/bitrix/admin/module_admin.php?lang=#LANGUAGE_ID#\">Модули</a>";
$MESS["wiz_ldap_error_domain"]='Заполните, пожалуйста, домен для NTML авторизации';
$MESS["wiz_ldap_login_help"] = '(логин пользователя с правами чтения из Active Directory в формате логин@домен или домен\логин)';
$MESS['wiz_ldap_root_help']='(для установки ограничения по одному или нескольким OU, укажите соответствующие DN через точку с запятой)';
$MESS['wiz_ldap_use_ntlm']='Использовать NTLM авторизацию:';
$MESS['wiz_ldap_use_ntlm_domain']='домен:';
$MESS["wiz_demo_access"]="Настройки прав доступа:";
$MESS["wiz_demo_allow_anon_alert"]="Обратите внимание, при разрешении доступа неавторизованым пользователям может быть доступна часть информации портала.";
$MESS["wiz_demo_allow_reg_alert"]="Обратите внимание, пользователи после регистрации могут получить доступ к части информации портала.";

$MESS["wiz_demo_offsite"]="Интеграция с Официальным сайтом государственной организации:";
$MESS["wiz_demo_offsite_install"]="Настроить интеграцию (<a href=\"http://www.1c-bitrix.ru/solutions/gov/#tab-new90-link\" target=\"_blank\">что это?</a>)";
$MESS["wiz_demo_offsite_url"]="Адрес официального сайта (например, http://www.gossite.ru):";
$MESS["wiz_demo_offsite_login"]="Логин администратора сайта:";
$MESS["wiz_demo_offsite_password"]="Пароль администратора сайта:";
$MESS["wiz_demo_offsite_reinstall"]="Переустановить демонстрационные данные для интеграции";

$MESS["wiz_demo_offsite_error_url"]="Не указан адрес Официального сайта";
$MESS["wiz_demo_offsite_error_login"]="Не указан логин администратора для Официального сайта";
$MESS["wiz_demo_offsite_error_password"]="Не указан пароль администратора для Официального сайта";


$MESS["wiz_demo_support"]="Обработка обращений граждан:";
$MESS["wiz_demo_support_install"]="Настроить почтовый ящик для обмена обращениями (<a href=\"http://www.1c-bitrix.ru/solutions/gov/#tab-new90-link\" target=\"_blank\">что это?</a>)";
$MESS["wiz_demo_support_server"]="Сервер входящей почты:";
$MESS["wiz_demo_support_port"]="Порт входящей почты:";
$MESS["wiz_demo_support_login"]="Логин:";
$MESS["wiz_demo_support_password"]="Пароль:";
$MESS["wiz_demo_support_email"]="Почтовый адрес:";

$MESS["wiz_demo_support_error_server"]="Не указан почтовый сервер";
$MESS["wiz_demo_support_error_port"]="Не указан порт";
$MESS["wiz_demo_support_error_login"]="Не указан логин от почтового ящика";
$MESS["wiz_demo_support_error_password"]="Не указан пароль от почтового ящика";
$MESS["wiz_demo_support_error_email"]="Не указан адрес почтового ящика";

$MESS["wiz_demo_support_warning"]="<b>Важно!</b> Не используйте существующий почтовый ящик для обмена обращениями! Это может создать большую нагрузку на сервер и приведет к появлению большого числа посторонних сообщений в почтовом ящике. Создайте отдельный ящик, к которому никто не будет иметь доступа.";
?>
