<?
$MESS["CLUWIZ_ERROR_ACCESS_DENIED"] = "Error! Acceso Denegado.";
$MESS["CLUWIZ_NOMODULE_ERROR"] = "Error al obtenerlas tablas para eliminar.";
$MESS["CLUWIZ_CONNECTION_ERROR"] = "Error al conectar a la base de datos.";
$MESS["CLUWIZ_ALL_DONE"] = "La tablas han sido eliminadas.";
$MESS["CLUWIZ_TABLE_DROPPED"] = "La tabla #table_name# ha sido borrado.";
$MESS["CLUWIZ_TABLE_PROGRESS"] = "Tabla izquierda: #tables#";
?>