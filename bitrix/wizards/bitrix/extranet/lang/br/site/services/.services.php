<?
$MESS["SERVICE_BLOG"] = "Blog";
$MESS["SERVICE_IBLOCK_DEMO_DATA"] = "Dados de demonstração";
$MESS["SERVICE_LEARNING"] = "e-Learning";
$MESS["SERVICE_FILES"] = "Arquivos do Site Extranet";
$MESS["SERVICE_MAIN_SETTINGS"] = "Configurações do Site Extranet";
$MESS["SERVICE_FORUM"] = "Fórum";
$MESS["SERVICE_INTRANET"] = "Configurações do Portal Extranet";
$MESS["SERVICE_PHOTOGALLERY"] = "Galeria de Fotos";
$MESS["SERVICE_VOTE"] = "Enquetes";
$MESS["SERVICE_SEARCH"] = "Pesquisar";
$MESS["SERVICE_FILEMAN"] = "Explorer do site";
$MESS["SERVICE_SOCIALNETWORK"] = "Rede Social";
$MESS["SERVICE_STATISTIC"] = "Estatística";
$MESS["SERVICE_COMPANY_STRUCTURE"] = "Estrutura de blocos de informação";
$MESS["SERVICE_USERS"] = "Usuários";
$MESS["SERVICE_FORM"] = "Formulários web";
$MESS["SERVICE_WORKFLOW"] = "Fluxo de trabalho";
?>