<?
$MESS["SERVICE_MAIN_SETTINGS"] = "Configuración de la extranet del sitio";
$MESS["SERVICE_VOTE"] = "Encuestas";
$MESS["SERVICE_FORUM"] = "Foro";
$MESS["SERVICE_PHOTOGALLERY"] = "Galería de Fotos";
$MESS["SERVICE_USERS"] = "Usuarios";
$MESS["SERVICE_LEARNING"] = "enseñanza virtual";
$MESS["SERVICE_FORM"] = "Formularios Webs";
$MESS["SERVICE_BLOG"] = "Blog";
$MESS["SERVICE_SOCIALNETWORK"] = "Social Network";
$MESS["SERVICE_WORKFLOW"] = "Flujo de Trabajo";
$MESS["SERVICE_FILEMAN"] = "Explorador del Sitio";
$MESS["SERVICE_STATISTIC"] = "Estadisticas";
$MESS["SERVICE_INTRANET"] = "Configuraciones del Portal de Intranet ";
$MESS["SERVICE_FILES"] = "Archivos de extranet del sitio";
$MESS["SERVICE_SEARCH"] = "Búsqueda";
?>