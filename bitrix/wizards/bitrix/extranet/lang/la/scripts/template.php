<?
$MESS["WIZARD_TITLE"] = "Bitrix Intranet Portal 9.0:<br>Configuración del sitio extranet";
$MESS["COPYRIGHT"] = "&copy; 2001-#CURRENT_YEAR# Bitrix, Inc.";
$MESS["SUPPORT"] = "<a href=\"http://www.bitrixsoft.com/?r1=extranet&r2=install\" target=\"_blank\">www.bitrixsoft.com</a> | <a href=\"http://www.bitrixsoft.com/support/?r1=extranet&r2=install\" target=\"_blank\">Soporte Tecnico</a>";
$MESS["INST_JAVASCRIPT_DISABLED"] = "El asistente requiere que el JavaScript este habilitado en su sistema. El JavaScript esta deshabilitado o no es compatible con su navegador. Por favor modifique la configuración de su nabegador e <a href=\"\">inténtelo de nuevo </a>.";
?>