<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Hilfe");
?>
<p>Wir empfehlen Ihnen folgende Seiten:</p>
 
<p><a href="#SITE_DIR#about/">Über die Firma</a> - offizielle und inoffizielle Information über unsere Firma, die für Sie wissenswert sein könnte. Erfahren Sie mehr über die Firmengeschichte und Hauptgeschäftsrichtungen. Dieser Artikel enthält auch wichtige Kontaktdaten und Bankverbindung.</p>

<p><a href="#SITE_DIR#contacts/index.php">Personensuche</a> für die schnelle Suche nach Mitarbeitern und externen Benutzern entsprechend Ihrer Zugriffsrechte.
 <br />
 </p>
 
<p><a href="#SITE_DIR#about/index.php">Offizielle Information</a> zeigt Überschriften der Firmennews, damit Sie immer über die aktuellen Termine informiert sind.
<br />
</p>

<p><a href="#SITE_DIR#docs/">Dokumentenbibliothek</a> ist eine öffentliche Sammlung von Firmendokumenten. </p>
 
<p>Viel Erfolg!
  <br />
 </p>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>