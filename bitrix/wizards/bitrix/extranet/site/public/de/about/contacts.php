<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Kontaktinformation");
?>
<p><b>E-Mail-Adressen:</b></p>
 
<table border="0" width="100%"> 	
  <tbody>
    <tr> 		<td width="20%">Allgemeine Information:</td> 		<td>info@company.com</td> 	</tr>
   	
    <tr> 		<td>Vertrieb:</td> 		<td>sales@company.com</td> 	</tr>
   	
    <tr> 		<td>Technischer Support:</td> 		<td>support@company.com</td> 	</tr>
   </tbody>
</table>
 
<p><b>Telefonnummern:</b></p>
 
<table border="0" width="100%"> 	
  <tbody>
    <tr> 		<td width="20%">Allgemein:</td> 		<td>+1 (555) 432-2920</td> 	</tr>
   	
    <tr> 		<td>Tel. für Vertriebsfragen:</td> 		<td>800-827-0685 (bitte keine technischen Fragen unter dieser Nummer)</td> 	</tr>
   	
    <tr> 		<td>Internationaler Vertrieb:</td> 		<td>+1 (555) 432-2920</td> 	</tr>
   	
    <tr> 		<td>Technischer Support:</td> 		<td>+1 (555) 432-2920</td> 	</tr>
   	
    <tr> 		<td>Fax:</td> 		<td>+1 (555) 432-2857</td> 	</tr>
   </tbody>
</table>
 
<p><b>Firma</b> ist ein Unternehmen aus Musterstadt.</p>
 
<p><b>Firmenhauptsitz:</b></p>
 <dl> <dd><b>Firma</b> GmbH</dd><dt>
    <br />
  </dt> <dd>Musterstr. 1</dd><dt>
    <br />
  </dt> <dd>12345, Musterstadt</dd><dt>
    <br />
  </dt> <dd>Deutschland</dd><dt>
    <br />
  </dt> </dl> 
<p><b>Arbeitsstunden:</b></p>
 
<p>Montag - Freitag (Geschlossen an Bundesfeiertagen), 9:00 - 19:00.</p>
 
<p><b>Anfahrt:</b></p>
 
<p><img height="449" width="500" src="/images/company/about/address_1.png" /></p>
 
<p><b>Auslandsvertretung:</b></p>
 <dl> <dd>*** Gilbert Building</dd><dt>
    <br />
  </dt> <dd>*** Wacouta Street</dd><dt>
    <br />
  </dt> <dd>St. Paul, MN 55101</dd><dt>
    <br />
  </dt> <dd>USA</dd><dt>
    <br />
  </dt> </dl> 
<p><b>Anfahrt:</b></p>
 
<p><img height="418" width="490" src="/images/company/about/address_2.png" /></p>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>