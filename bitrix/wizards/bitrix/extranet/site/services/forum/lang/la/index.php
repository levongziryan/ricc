<?
$MESS["COMMENTS_EXTRANET_GROUP_NAME"] = "Comentarios: Extranet";
$MESS["HIDDEN_EXTRANET_GROUP_NAME"] = "Foros Ocultos: Extranet";
$MESS["USERS_AND_GROUPS_EXTRANET_FORUM_NAME"] = "Usuario y Grupo de Foros: Extranet";
$MESS["USERS_AND_GROUPS_EXTRANET_FORUM_DESCRIPTION"] = "Foros de usuarios y grupos individuales del sitio(extranet)";
$MESS["GROUPS_AND_USERS_FILES_COMMENTS_EXTRANET_NAME"] = "Comentarios en archivos de usuarios: Extranet";
$MESS["GROUPS_AND_USERS_TASKS_COMMENTS_EXTRANET_NAME"] = "Comentarios en usuarios y grupos de tareas: Extranet";
$MESS["GROUPS_AND_USERS_PHOTOGALLERY_COMMENTS_EXTRANET_NAME"] = "Comentarios en usuarios y grupos de galerías de fotos: Extranet";
?>