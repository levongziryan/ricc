<?
$MESS ['EXTRANET_NEW_MESSAGE_SUBJECT'] = "#SITE_NAME#: Sie haben eine neue Nachricht";
$MESS ['EXTRANET_NEW_MESSAGE_MESSAGE'] = "Informationsnachricht von #SITE_NAME#
------------------------------------------

Hallo, #USER_NAME#!

Sie haben eine neue Nachricht vom User #SENDER_NAME# #SENDER_LAST_NAME# erhalten:

------------------------------------------
#MESSAGE#
------------------------------------------

Link zur Nachricht:

http://#SERVER_NAME#/company/personal/messages/chat/#SENDER_ID#/

Dies ist eine automatisch generierte Nachricht.
";
?>