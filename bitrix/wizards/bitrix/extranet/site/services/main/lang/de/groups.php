<?
$MESS ['EXTRANET_ADMIN_GROUP_DESC'] = "Administratoren haben volle Zugriffsrechte um Extranet-Ressourcen zu verwalten und zu bearbeiten.";
$MESS ['EXTRANET_GROUP_DESC'] = "Alle Benutzer, die Zugriff auf die Extranet-Site haben.";
$MESS ['EXTRANET_CREATE_WG_GROUP_DESC'] = "Alle Benutzer, die Benutzergruppen auf der Extranet-Site erstellen können.";
$MESS ['EXTRANET_CREATE_WG_GROUP_NAME'] = "Kann Extranet-Benutzergruppen erstellen";
$MESS ['EXTRANET_MENUITEM_NAME'] = "Extranet";
$MESS ['EXTRANET_ADMIN_GROUP_NAME'] = "Extranet-Administratoren";
$MESS ['EXTRANET_GROUP_NAME'] = "Extranet-Benutzer";
?>