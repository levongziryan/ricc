<?
$MESS["EXTRANET_NEW_MESSAGE_SUBJECT"] = "#SITE_NAME#: Usted tiene un nuevo mensaje";
$MESS["EXTRANET_NEW_MESSAGE_MESSAGE"] = "Saludos desde #SITE_NAME#!
------------------------------------------

Hola #USER_NAME#!

Usted tiene un nuevo mensaje de #SENDER_NAME# #SENDER_LAST_NAME#:

------------------------------------------
#MESSAGE#
------------------------------------------

Enlace al mensaje:

http://#SERVER_NAME#/company/personal/messages/chat/#SENDER_ID#/

Esta es una notificación generada automáticamente.";
?>