<?
$MESS["EXTRANET_ADMIN_GROUP_DESC"] = "Os administradores têm total permissão para acessar, gerenciar e editar recursos do site de extranet.";
$MESS["EXTRANET_GROUP_DESC"] = "Todos os usuários que têm acesso ao site de extranet.";
$MESS["EXTRANET_CREATE_WG_GROUP_DESC"] = "Todos os usuários que têm permissão para criar grupos de usuários no site de extranet.";
$MESS["EXTRANET_CREATE_WG_GROUP_NAME"] = "Permissão para criar grupos de usuários de extranet";
$MESS["EXTRANET_MENUITEM_NAME"] = "Extranet";
$MESS["EXTRANET_ADMIN_GROUP_NAME"] = "Administradores do Site Extranet";
$MESS["EXTRANET_GROUP_NAME"] = "Usuários Extranet";
?>