<?
$MESS ['G_TICKETS_MODIFIED_BY'] = "Author of Last Message";
$MESS ['G_TICKETS_TIMESTAMP_X'] = "Last Modified";
$MESS ['G_TICKETS_LIST_EMPTY'] = "No Support Tickets";
$MESS ['G_TICKETS_RESPONSIBLE'] = "Responsible";
$MESS ['G_TICKETS_STATUS'] = "Status";
$MESS ['G_TICKETS_RED_ALT'] = "the last message is from your correspondent";
$MESS ['G_TICKETS_GREEN_ALT'] = "the last message is from you";
$MESS ['G_TICKETS_GREY_ALT'] = "the ticket is closed";
$MESS ['G_TICKETS_MESSAGES'] = "Total Messages:";
?>