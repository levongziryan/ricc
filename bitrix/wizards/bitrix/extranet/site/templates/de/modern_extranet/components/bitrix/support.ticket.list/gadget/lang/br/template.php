<?
$MESS["G_TICKETS_MODIFIED_BY"] = "Autor da última mensagem";
$MESS["G_TICKETS_TIMESTAMP_X"] = "ltima alteração";
$MESS["G_TICKETS_LIST_EMPTY"] = "Não há pedidos de ajuda";
$MESS["G_TICKETS_RESPONSIBLE"] = "Responsável";
$MESS["G_TICKETS_STATUS"] = "Status";
$MESS["G_TICKETS_GREEN_ALT"] = "a última mensagem é sua";
$MESS["G_TICKETS_RED_ALT"] = "a última mensagem é de seu correspondente";
$MESS["G_TICKETS_GREY_ALT"] = "o ticket está fechado";
$MESS["G_TICKETS_MESSAGES"] = "Total de Mensagens:";
?>