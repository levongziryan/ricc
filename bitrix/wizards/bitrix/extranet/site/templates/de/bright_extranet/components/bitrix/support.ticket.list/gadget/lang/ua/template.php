<?
$MESS["G_TICKETS_MODIFIED_BY"] = "Автор останнього повідомлення";
$MESS["G_TICKETS_TIMESTAMP_X"] = "Остання зміна";
$MESS["G_TICKETS_LIST_EMPTY"] = "Список звернень порожній";
$MESS["G_TICKETS_RESPONSIBLE"] = "Відповідальний";
$MESS["G_TICKETS_STATUS"] = "Статус";
$MESS["G_TICKETS_RED_ALT"] = "Востаннє у звернення писав ваш опонент";
$MESS["G_TICKETS_GREEN_ALT"] = "Востаннє у звернення писали ви";
$MESS["G_TICKETS_GREY_ALT"] = "звернення закрито";
$MESS["G_TICKETS_MESSAGES"] = "Усього повідомлень";
?>