<?
$MESS["G_TICKETS_GREEN_ALT"] = "a última mensagem é sua";
$MESS["G_TICKETS_GREY_ALT"] = "o ticket está fechado";
$MESS["G_TICKETS_TIMESTAMP_X"] = "ltima alteração";
$MESS["G_TICKETS_MESSAGES"] = "Total de Mensagens:";
$MESS["G_TICKETS_STATUS"] = "Status";
$MESS["G_TICKETS_MODIFIED_BY"] = "ltima mensagem do autor";
$MESS["G_TICKETS_RESPONSIBLE"] = "Responsável";
$MESS["G_TICKETS_LIST_EMPTY"] = "Não há pedidos de ajuda";
$MESS["G_TICKETS_RED_ALT"] = "ltima postagem por um membro do suporte técnico";
$MESS["G_TICKETS_GREEN_ALT_SUP"] = "a última mensagem é sua";
$MESS["G_TICKETS_GREY_ALT_SUP"] = "o ticket está fechado";
$MESS["G_TICKETS_RED_ALT_SUP"] = "ltima postagem por um cliente do suporte técnico (você é o responsável)";
$MESS["G_TICKETS_YELLOW_ALT_SUP"] = "ltima postagem por um cliente do suporte técnico (você não é o responsável)";
$MESS["G_TICKETS_GREEN_S_ALT_SUP"] = "ltima postagem por um membro do suporte técnico";
?>