<?
$MESS["G_TICKETS_MODIFIED_BY"] = "Autor del ultimo mensaje";
$MESS["G_TICKETS_TIMESTAMP_X"] = "Ultima modificación";
$MESS["G_TICKETS_LIST_EMPTY"] = "No hay soportes de entradas";
$MESS["G_TICKETS_RESPONSIBLE"] = "Responsable";
$MESS["G_TICKETS_STATUS"] = "Estado";
$MESS["G_TICKETS_RED_ALT"] = "El ultimo mensaje es de su correspondiente";
$MESS["G_TICKETS_GREEN_ALT"] = "El ultimo mensaje es de usted";
$MESS["G_TICKETS_GREY_ALT"] = "La entrada esta cerrada";
$MESS["G_TICKETS_MESSAGES"] = "Total de mensajes:";
?>