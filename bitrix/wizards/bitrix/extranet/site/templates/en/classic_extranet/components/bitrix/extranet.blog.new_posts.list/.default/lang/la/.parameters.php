<?
$MESS["EBMNP_NAME_TEMPLATE"] = "Formato de nombre";
$MESS["EBMNP_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["EBMNP_SHOW_LOGIN"] = "Mostrar nombre de la sesión si no requiere que los campos de nombres del usuario estén disponibles";
$MESS["EBMNP_PATH_TO_CONPANY_DEPARTMENT"] = "Plantilla de la ruta a la Página del Departamento";
$MESS["EBMNP_PATH_TO_VIDEO_CALL"] = "Página de la video llamada";
$MESS["EBMNP_PATH_TO_SONET_USER_PROFILE"] = "Plantilla de la ruta del Perfil de la Red Social de Contactos del Usuario";
$MESS["EBMNP_PATH_TO_MESSAGES_CHAT"] = "Plantilla de la ruta del messenger del usuario";
?>