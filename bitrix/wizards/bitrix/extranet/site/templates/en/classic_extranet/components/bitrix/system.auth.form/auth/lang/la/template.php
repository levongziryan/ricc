<?
$MESS["AUTH_LOGIN_BUTTON"] = "Login";
$MESS["AUTH_CLOSE_WINDOW"] = "Cerrar";
$MESS["AUTH_LOGOUT_BUTTON"] = "Salir";
$MESS["AUTH_REMEMBER_ME"] = "Recordarme en este computador";
$MESS["AUTH_FORGOT_PASSWORD_2"] = "Olvidaste tu contraseña?";
$MESS["AUTH_REGISTER"] = "Registrarse";
$MESS["AUTH_LOGIN"] = "Iniciar Sesión";
$MESS["AUTH_PASSWORD"] = "Contraseña";
$MESS["AUTH_PROFILE"] = "Mi perfil";
$MESS["AUTH_MP"] = "Mi portal";
$MESS["AUTH_NEW_MESSAGES"] = "Mis mensajes";
$MESS["AUTH_WELCOME_TEXT"] = "Bienvenido";
$MESS["AUTH_PERSONAL_PAGE"] = "Página personal";
$MESS["AUTH_CAPTCHA_PROMT"] = "Tipo de texto para la imagen";
$MESS["AUTH_BZP"] = "Business Process";
$MESS["AUTH_NEW_MESSAGES_DESCR"] = "Ver mensajes";
$MESS["AUTH_PERSONAL_PAGE_DESCR"] = "Enlace a las herramientas de administración de perfil";
$MESS["AUTH_PROFILE_DESCR"] = "Editar perfil";
?>