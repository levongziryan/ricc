<?
$MESS["EBMNP_NAME_TEMPLATE"] = "Formato de Nombre";
$MESS["EBMNP_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["EBMNP_SHOW_LOGIN"] = "Mostrar Inicio de sesión si no requiere que los campos nombre del usuario estén disponibles";
$MESS["EBMNP_PATH_TO_CONPANY_DEPARTMENT"] = "Plantilla de la ruta a la página del Departamento";
$MESS["EBMNP_PATH_TO_VIDEO_CALL"] = "Página de Video Llamada";
$MESS["EBMNP_PATH_TO_SONET_USER_PROFILE"] = "Plantilla de la ruta al perfil de la Red Social de Contactos del Usuario";
$MESS["EBMNP_PATH_TO_MESSAGES_CHAT"] = "Plantilla de la ruta al Messenger del Usuario";
?>