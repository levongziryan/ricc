<?
$MESS ['G_TICKETS_MODIFIED_BY'] = "Letzte Nachricht des Autors";
$MESS ['G_TICKETS_TIMESTAMP_X'] = "Zuletzt geändert";
$MESS ['G_TICKETS_LIST_EMPTY'] = "Keine Support-Anfragen";
$MESS ['G_TICKETS_RESPONSIBLE'] = "Verantwortlich";
$MESS ['G_TICKETS_STATUS'] = "Status";
$MESS ['G_TICKETS_GREEN_ALT'] = "die letzte Nachricht ist von Ihnen";
$MESS ['G_TICKETS_RED_ALT'] = "die letzte Nachricht ist nicht von Ihren";
$MESS ['G_TICKETS_GREY_ALT'] = "das Ticket ist geschlossen";
$MESS ['G_TICKETS_MESSAGES'] = "Nachrichten gesamt:";
?>