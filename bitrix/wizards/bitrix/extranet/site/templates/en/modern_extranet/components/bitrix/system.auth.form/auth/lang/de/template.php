<?
$MESS ['AUTH_BZP'] = "Geschäftsprozesse";
$MESS ['AUTH_CLOSE_WINDOW'] = "Schließen";
$MESS ['AUTH_PROFILE_DESCR'] = "Profil bearbeiten";
$MESS ['AUTH_FORGOT_PASSWORD_2'] = "Passwort vergessen?";
$MESS ['AUTH_WELCOME_TEXT'] = "Hallo";
$MESS ['AUTH_PERSONAL_PAGE_DESCR'] = "Links zu dem Profilverwaltungstools";
$MESS ['AUTH_LOGIN_BUTTON'] = "Login";
$MESS ['AUTH_LOGIN'] = "Login";
$MESS ['AUTH_LOGOUT_BUTTON'] = "Logout";
$MESS ['AUTH_NEW_MESSAGES'] = "Meine Nachrichten";
$MESS ['AUTH_MP'] = "Mein Portal";
$MESS ['AUTH_PROFILE'] = "Mein Profil";
$MESS ['AUTH_PASSWORD'] = "Passwort";
$MESS ['AUTH_PERSONAL_PAGE'] = "Persönliche Seite";
$MESS ['AUTH_REGISTER'] = "Registrieren";
$MESS ['AUTH_REMEMBER_ME'] = "Auf diesem Computer merken";
$MESS ['AUTH_CAPTCHA_PROMT'] = "Geben Sie die Zeichen von dem Bild ein";
$MESS ['AUTH_NEW_MESSAGES_DESCR'] = "Nachrichten zeigen";
?>