<?
$MESS["EBMNP_NAME_TEMPLATE"] = "Formato de nombre";
$MESS["EBMNP_NAME_TEMPLATE_DEFAULT"] = "#NOBR##NAME# #LAST_NAME##/NOBR#";
$MESS["EBMNP_SHOW_LOGIN"] = "Mostrar el nombre de la sesión si no requiere quelos campos del nombre estén disponibles";
$MESS["EBMNP_PATH_TO_CONPANY_DEPARTMENT"] = "Plantilla de la ruta a la página del departamento";
$MESS["EBMNP_PATH_TO_VIDEO_CALL"] = "Página de video llamada";
$MESS["EBMNP_PATH_TO_SONET_USER_PROFILE"] = "Plantilla de la ruta al perfil de la red de contactos del usuario";
$MESS["EBMNP_PATH_TO_MESSAGES_CHAT"] = "Plantilla de la ruta del messenger del usuario";
?>