<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();
?>


<?if($USER->IsAuthorized()):?>
<?LocalRedirect("/partner/stat.php"); ?>
<p><?echo GetMessage("MAIN_REGISTER_AUTH")?></p>

<?else:?>
<span style="color:red">
<?
if (count($arResult["ERRORS"]) > 0):
	foreach ($arResult["ERRORS"] as $key => $error)
		if (intval($key) == 0 && $key !== 0) 
			$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);

	ShowError(implode("<br />", $arResult["ERRORS"]));

elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):
?>
</span>
<p><?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
<?endif?>

<form method="post" action="<?=POST_FORM_ACTION_URI?>#reg" name="regform" enctype="multipart/form-data" class="partner-form form-reg">
<?
if($arResult["BACKURL"] <> ''):
?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?
endif;
?>

	<div class="partner-form-line ">
		<input type="text" name="REGISTER[LOGIN]" value="" id="name"
			placeholder="Логин (мин. 3 символа)*">
	</div>
	<div class="partner-form-line ">
		<input type="password" name="REGISTER[PASSWORD]" value="" id="name"
			placeholder="Пароль*">
	</div>
	<div class="partner-form-line ">
		<input type="password" name="REGISTER[CONFIRM_PASSWORD]" value="" id="name"
			placeholder="Подтверждение пароля*">
	</div>
	<div class="partner-form-line ">
		<input type="text" name="REGISTER[EMAIL]" value="" id="name"
			placeholder="Адрес e-mail*">
	</div>
	<div class="partner-form-line ">
		<input type="text" name="REGISTER[NAME]" value="" id="name"
			placeholder="Имя*">
	</div>
	<div class="partner-form-line ">
		<input type="text" name="REGISTER[LAST_NAME]" value="" id="name"
			placeholder="Фамилия">
	</div>

	<div class="partner-form-line ">
		<input type="text" name="UF_PAY_METHOD" value="" id="name"
			placeholder="Способ оплаты">
	</div>
	
	<div class="partner-form-line ">
		<input type="text" name="UF_PAY_TO" value="" id="name"
			placeholder="Номер кошелька или карты:">
	</div>



<?
/* CAPTCHA */
if ($arResult["USE_CAPTCHA"] == "Y")
{
	?>
		
			<b><?=GetMessage("REGISTER_CAPTCHA_TITLE")?></b>
		
		
			
			
				<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
				<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
			
		
		
			<?=GetMessage("REGISTER_CAPTCHA_PROMT")?>:<span class="starrequired">*</span>
			<input type="text" name="captcha_word" maxlength="50" value="" />
		
	<?
}
/* !CAPTCHA */
?>
	<input type="submit" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>" />
		
<p><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>
<p><span class="starrequired">*</span><?=GetMessage("AUTH_REQ")?></p>

</form>
<?endif?>
