<?
$MESS["IMOL_MA_EMPTY_MESSAGE"] = "Falta el parámetro 'Texto del mensaje'.";
$MESS["IMOL_MA_MESSAGE"] = "Texto del mensaje";
$MESS["IMOL_MA_IS_SYSTEM"] = "Mensaje oculto (modo whisper)";
$MESS["IMOL_MA_IS_SYSTEM_DESCRIPTION"] = "El mensaje publicado no será visible para el contacto externo (modo whisper)";
$MESS["IMOL_MA_UNSUPPORTED_DOCUMENT"] = "El elemento actual no admite este tipo de actividad";
$MESS["IMOL_MA_NO_SESSION_CODE"] = "No se encontró ningún cliente con Canal Abierto conectado";
$MESS["IMOL_MA_NO_CHAT"] = "No se encontró el chat del cliente";
?>