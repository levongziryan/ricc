<?
$MESS["IMOL_MA_EMPTY_MESSAGE"] = "Властивість 'Текст' не вказано.";
$MESS["IMOL_MA_MESSAGE"] = "Текст повідомлення";
$MESS["IMOL_MA_IS_SYSTEM"] = "Приховане повідомлення";
$MESS["IMOL_MA_IS_SYSTEM_DESCRIPTION"] = "Надіслане повідомлення не буде видно зовнішнього співрозмовникові";
$MESS["IMOL_MA_UNSUPPORTED_DOCUMENT"] = "Дія не може працювати з поточним документом";
$MESS["IMOL_MA_NO_SESSION_CODE"] = "Клієнт з підключеною Відкритою лінією не знайдений";
$MESS["IMOL_MA_NO_CHAT"] = "Чат з клієнтом не знайдено";
?>