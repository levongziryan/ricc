<?
$MESS["BPIMNA_PD_MESSAGE_BBCODE"] = "Le formatage du BBCode est accessible";
$MESS["BPIMNA_PD_MESSAGE_OUT_EMPTY"] = "si elle est vide, le 'texte de notification' sera utilisé.";
$MESS["BPIMNA_PD_TO"] = "Expéditeur";
$MESS["BPIMNA_PD_NOTIFY_TYPE_FROM"] = "Notification personnalisée (avec avatar)";
$MESS["BPIMNA_PD_FROM"] = "Adresse du destinataire";
$MESS["BPIMNA_PD_MESSAGE_OUT"] = "Texte de la notification pour email/jabber";
$MESS["BPIMNA_PD_MESSAGE"] = "Texte de la notification pour le site";
$MESS["BPIMNA_PD_NOTIFY_TYPE"] = "Type d'avis";
$MESS["BPIMNA_PD_NOTIFY_TYPE_SYSTEM"] = "Notification du système (sans avatar)";
?>