<?
$MESS["BPIMNA_EMPTY_FROM"] = "Le paramètre 'Expéditeur' n'est pas indiqué.";
$MESS["BPIMNA_EMPTY_TO"] = "La propriété 'Destinataire' n'est pas indiquée.";
$MESS["BPIMNA_EMPTY_MESSAGE"] = "La propriété 'Texte de l'avertissement' n'est pas indiquée.";
?>