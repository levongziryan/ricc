<?
$MESS["BPWHA_HANDLER_NAME"] = "Manipulador";
$MESS["BPWHA_EMPTY_TEXT"] = "O manipulador não está especificado";
$MESS["BPWHA_HANDLER_WRONG_URL"] = "A URL do manipulador está incorreta";
$MESS["BPWHA_HANDLER_WRONG_PROTOCOL"] = "O protocolo da URL do manipulador não é suportado";
?>