<?
$MESS["BPDD_DESCR_NAME2"] = "Detalles de los objetos del drive";
$MESS["BPDD_DESCR_DESCR2"] = "Obtener los detalles de los objetos del drive";
$MESS["BPDD_DESCR_TYPE"] = "Tipo de objeto";
$MESS["BPDD_DESCR_NAME"] = "Nombre";
$MESS["BPDD_DESCR_SIZE_BYTES"] = "Tamaño, bytes";
$MESS["BPDD_DESCR_SIZE_FORMATTED"] = "Tamaño (con formato)";
$MESS["BPDD_DESCR_DETAIL_URL"] = "Ver URL";
$MESS["BPDD_DESCR_DOWNLOAD_URL"] = "URL de descarga";
$MESS["BPDD_DESCR_CATEGORY"] = "Drive";
?>