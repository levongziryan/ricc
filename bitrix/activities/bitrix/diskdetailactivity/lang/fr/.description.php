<?
$MESS["BPDD_DESCR_NAME2"] = "Détails de l'objet du Drive";
$MESS["BPDD_DESCR_DESCR2"] = "Obtenir les détails de l'objet du Drive";
$MESS["BPDD_DESCR_TYPE"] = "Type d'objet";
$MESS["BPDD_DESCR_NAME"] = "Nom";
$MESS["BPDD_DESCR_SIZE_BYTES"] = "Taille, octets";
$MESS["BPDD_DESCR_SIZE_FORMATTED"] = "Taille (formaté)";
$MESS["BPDD_DESCR_DETAIL_URL"] = "Afficher l'URL";
$MESS["BPDD_DESCR_DOWNLOAD_URL"] = "Télécharger l'URL";
$MESS["BPDD_DESCR_CATEGORY"] = "Drive";
?>