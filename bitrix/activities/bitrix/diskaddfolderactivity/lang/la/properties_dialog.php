<?
$MESS["BPDAF_PD_ENTITY"] = "Crear en";
$MESS["BPDAF_PD_ENTITY_TYPE_USER"] = "Drive del usuario";
$MESS["BPDAF_PD_ENTITY_TYPE_SG"] = "Red social del drive del grupo";
$MESS["BPDAF_PD_ENTITY_TYPE_COMMON"] = "Drive pública";
$MESS["BPDAF_PD_ENTITY_TYPE_FOLDER"] = "Carpeta del drive";
$MESS["BPDAF_PD_FOLDER_NAME"] = "Nombre de la carpeta";
$MESS["BPDAF_PD_ENTITY_ID_USER"] = "Usuario";
$MESS["BPDAF_PD_ENTITY_ID_SG"] = "Grupo";
$MESS["BPDAF_PD_ENTITY_ID_COMMON"] = "Drive";
$MESS["BPDAF_PD_ENTITY_ID_FOLDER"] = "Carpeta";
$MESS["BPDAF_PD_LABEL_CHOOSE"] = "Seleccionar:";
$MESS["BPDAF_PD_LABEL_DISK_CHOOSE"] = "Seleccionar carpeta";
$MESS["BPDAF_PD_LABEL_DISK_EMPTY"] = "No hay carpeta seleccionada";
$MESS["BPDAF_PD_FOLDER_AUTHOR"] = "Creado por";
?>