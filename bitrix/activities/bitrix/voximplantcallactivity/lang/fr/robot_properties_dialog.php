<?
$MESS["BPVICA_RPD_NO_OUTPUT_NUMBER"] = "Veuillez louer un numéro de téléphone ou connecter une téléphonie SIP pour utiliser cette option.";
$MESS["BPVICA_RPD_CALL_TYPE"] = "Appeler en utilisant";
$MESS["BPVICA_RPD_CALL_TYPE_TEXT"] = "Name";
$MESS["BPVICA_RPD_CALL_TYPE_AUDIO"] = "Fichier audio";
$MESS["BPVICA_RPD_OUTPUT_NUMBER"] = "Numéro d'origine";
$MESS["BPVICA_RPD_NUMBER"] = "Numéro de destination";
$MESS["BPVICA_RPD_TEXT"] = "Convertir de texte en voix";
$MESS["BPVICA_RPD_VOICE_LANGUAGE"] = "Langue et voix";
$MESS["BPVICA_RPD_VOICE_SPEED"] = "Débit de parole";
$MESS["BPVICA_RPD_VOICE_VOLUME"] = "Volume de parole";
$MESS["BPVICA_RPD_AUDIO_FILE"] = "URL de fichier audio (mp3)";
?>