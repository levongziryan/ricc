<?
$MESS["BPVICA_INCLUDE_MODULE"] = "Le module Téléphonie n'est pas installé.";
$MESS["BPVICA_TRACK_SUBSCR"] = "En attente des résultats du robocall";
$MESS["BPVICA_RESULT_TRUE"] = "Succès";
$MESS["BPVICA_RESULT_FALSE"] = "Manqué";
$MESS["BPVICA_ERROR_OUTPUT_NUMBER"] = "Le numéro de téléphone n'est pas spécifié.";
$MESS["BPVICA_ERROR_NUMBER"] = "Le numéro de l'abonné n'est pas spécifié. ";
$MESS["BPVICA_ERROR_TEXT"] = "Le texte à prononcer n'est pas indiqué.";
$MESS["BPVICA_ERROR_AUDIO_FILE"] = "Le fichier audio n'est pas spécifié.";
?>