<?
$MESS["BPVICA_PD_NO_OUTPUT_NUMBER"] = "Veuillez louer un numéro de téléphone ou connecter une téléphonie SIP pour utiliser cette option.";
$MESS["BPVICA_PD_CALL_TYPE"] = "Appeler en utilisant";
$MESS["BPVICA_PD_CALL_TYPE_TEXT"] = "Texte";
$MESS["BPVICA_PD_CALL_TYPE_AUDIO"] = "Fichier audio";
$MESS["BPVICA_PD_OUTPUT_NUMBER"] = "Numéro d'origine";
$MESS["BPVICA_PD_NUMBER"] = "Numéro de destination";
$MESS["BPVICA_PD_TEXT"] = "Convertir de texte en voix";
$MESS["BPVICA_PD_VOICE_LANGUAGE"] = "Langue et voix";
$MESS["BPVICA_PD_VOICE_SPEED"] = "Débit de parole";
$MESS["BPVICA_PD_VOICE_VOLUME"] = "Volume de parole";
$MESS["BPVICA_PD_AUDIO_FILE"] = "URL de fichier audio (mp3)";
$MESS["BPVICA_PD_WAIT_FOR_RESULT"] = "Attendre le résultat";
$MESS["BPVICA_PD_YES"] = "Oui";
$MESS["BPVICA_PD_NO"] = "Non";
$MESS["BPVICA_PD_USE_DOCUMENT_PHONE_NUMBER"] = "Utiliser le numéro de téléphone de l’entité du CRM actuelle";
?>