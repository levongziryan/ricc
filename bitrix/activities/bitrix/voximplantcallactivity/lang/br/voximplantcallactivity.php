<?
$MESS["BPVICA_INCLUDE_MODULE"] = "O módulo Telefonia não está instalado.";
$MESS["BPVICA_TRACK_SUBSCR"] = "Aguardando resultados da robocall";
$MESS["BPVICA_RESULT_TRUE"] = "Bem sucedido";
$MESS["BPVICA_RESULT_FALSE"] = "Falhou";
$MESS["BPVICA_ERROR_OUTPUT_NUMBER"] = "O número de telefone não está especificado.";
$MESS["BPVICA_ERROR_NUMBER"] = "O número do assinante não está especificado.";
$MESS["BPVICA_ERROR_TEXT"] = "O texto para falar não foi fornecido.";
$MESS["BPVICA_ERROR_AUDIO_FILE"] = "O arquivo de áudio não está especificado.";
?>