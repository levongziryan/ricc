<?
$MESS["BPVICA_DESCR_NAME"] = "Robocall informativa";
$MESS["BPVICA_DESCR_DESCR"] = "Robocall pré-gravada ou baseada em texto informativo";
$MESS["BPVICA_DESCR_RESULT"] = "Resultado da chamada";
$MESS["BPVICA_DESCR_RESULT_TEXT"] = "Resultado da chamada (texto)";
$MESS["BPVICA_DESCR_RESULT_CODE"] = "Código de término de chamada";
?>