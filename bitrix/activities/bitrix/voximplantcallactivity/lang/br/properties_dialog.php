<?
$MESS["BPVICA_PD_NO_OUTPUT_NUMBER"] = "Alugue um número de telefone ou conecte telefonia SIP para utilizar esta opção.";
$MESS["BPVICA_PD_CALL_TYPE"] = "Ligar usando";
$MESS["BPVICA_PD_CALL_TYPE_TEXT"] = "Texto";
$MESS["BPVICA_PD_CALL_TYPE_AUDIO"] = "Arquivo de áudio";
$MESS["BPVICA_PD_OUTPUT_NUMBER"] = "Número de origem";
$MESS["BPVICA_PD_NUMBER"] = "Número de destino";
$MESS["BPVICA_PD_TEXT"] = "Converter texto em voz";
$MESS["BPVICA_PD_VOICE_LANGUAGE"] = "Idioma e voz";
$MESS["BPVICA_PD_VOICE_SPEED"] = "Velocidade da fala";
$MESS["BPVICA_PD_VOICE_VOLUME"] = "Volume da fala";
$MESS["BPVICA_PD_AUDIO_FILE"] = "URL do arquivo de áudio (mp3)";
$MESS["BPVICA_PD_WAIT_FOR_RESULT"] = "Aguardar resultado";
$MESS["BPVICA_PD_YES"] = "Sim";
$MESS["BPVICA_PD_NO"] = "Não";
$MESS["BPVICA_PD_USE_DOCUMENT_PHONE_NUMBER"] = "Utilizar o número de telefone da entidade atual do CRM";
?>