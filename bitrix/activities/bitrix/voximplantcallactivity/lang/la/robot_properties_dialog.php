<?
$MESS["BPVICA_RPD_NO_OUTPUT_NUMBER"] = "Por favor, alquilar un número de teléfono o conectar la telefonía SIP para utilizar esta opción.";
$MESS["BPVICA_RPD_CALL_TYPE"] = "Llamar usando";
$MESS["BPVICA_RPD_CALL_TYPE_TEXT"] = "Texto";
$MESS["BPVICA_RPD_CALL_TYPE_AUDIO"] = "Archivo de audio";
$MESS["BPVICA_RPD_OUTPUT_NUMBER"] = "Número de origen";
$MESS["BPVICA_RPD_NUMBER"] = "Número de destino";
$MESS["BPVICA_RPD_TEXT"] = "Convertir texto a voz";
$MESS["BPVICA_RPD_VOICE_LANGUAGE"] = "Idioma y voz";
$MESS["BPVICA_RPD_VOICE_SPEED"] = "Velocidad del habla";
$MESS["BPVICA_RPD_VOICE_VOLUME"] = "Volumen del habla";
$MESS["BPVICA_RPD_AUDIO_FILE"] = "URL del archivo de audio (mp3)";
?>