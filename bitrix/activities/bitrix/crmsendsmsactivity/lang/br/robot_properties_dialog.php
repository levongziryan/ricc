<?
$MESS["CRM_SSMSA_RPD_SELECT_PROVIDER"] = "Selecione...";
$MESS["CRM_SSMSA_RPD_MARKETPLACE"] = "Use #A1#Applications24#A2# para baixar e instalar provedores de SMS";
$MESS["CRM_SSMSA_RPD_PROVIDER_IS_DEMO"] = "O provedor está em modo de demonstração";
$MESS["CRM_SSMSA_RPD_PROVIDER_CANT_USE"] = "O provedor não está disponível porque não foi configurado";
$MESS["CRM_SSMSA_RPD_PROVIDER_MANAGE_URL"] = "Configurar o Provedor";
$MESS["CRM_SSMSA_RPD_CHOOSE_PROVIDER"] = "selecionar o provedor de SMS...";
?>