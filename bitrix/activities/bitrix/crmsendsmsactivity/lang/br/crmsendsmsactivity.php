<?
$MESS["CRM_SSMSA_EMPTY_TEXT"] = "O texto da mensagem está vazio.";
$MESS["CRM_SSMSA_EMPTY_PROVIDER"] = "Provedor não selecionado.";
$MESS["CRM_SSMSA_MESSAGE_TEXT"] = "Texto da mensagem";
$MESS["CRM_SSMSA_PROVIDER"] = "Provedor";
$MESS["CRM_SSMSA_EMPTY_PHONE_NUMBER"] = "O número de telefone não está especificado";
$MESS["CRM_SSMSA_NO_PROVIDER"] = "O provedor de SMS não está cadastrado ou foi excluído.";
$MESS["CRM_SSMSA_REST_SESSION_ERROR"] = "Sessão REST bloqueada";
$MESS["CRM_SSMSA_PAYMENT_REQUIRED"] = "O app não foi pago";
$MESS["CRM_SSMSA_RECIPIENT_TYPE"] = "Tipo de destinatário";
$MESS["CRM_SSMSA_RECIPIENT_TYPE_ENTITY"] = "Cliente";
$MESS["CRM_SSMSA_RECIPIENT_TYPE_USER"] = "Funcionário";
$MESS["CRM_SSMSA_RECIPIENT_USER"] = "Funcionário";
$MESS["CRM_SSMSA_SEND_RESULT_TRUE"] = "Mensagem SMS enviada para #PHONE#";
$MESS["CRM_SSMSA_SEND_ACTIVITY_SUBJECT"] = "Mensagem SMS enviada";
$MESS["CRM_SSMSA_NO_MESSAGESERVICE"] = "O módulo \"Serviços de Mensagem\" não está instalado.";
?>