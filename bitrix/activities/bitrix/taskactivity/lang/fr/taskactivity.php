<?
$MESS["BPSNMA_EMPTY_TASKPRIORITY"] = "La propriété 'Importance' n'est pas indiquée.";
$MESS["BPSNMA_EMPTY_TASKOWNERID"] = "La propriété 'Propriétaire' n'est pas indiquée.";
$MESS["BPSNMA_EMPTY_TASKNAME"] = "La propriété 'Destination de la tâche' n'est pas indiquée.";
$MESS["BPSNMA_EMPTY_TASKASSIGNEDTO"] = "La propriété 'Responsable' n'est pas indiquée.";
$MESS["BPSNMA_EMPTY_TASKTYPE"] = "Le paramètre 'Type de tâche' n'est pas spécifié.";
?>