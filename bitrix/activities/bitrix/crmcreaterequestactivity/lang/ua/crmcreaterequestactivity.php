<?
$MESS["CRM_CREATE_REQUEST_EMPTY_PROP"] = "Не заповнений обов'язковий параметр: #PROPERTY#";
$MESS["CRM_CREATE_REQUEST_SUBJECT"] = "Тема";
$MESS["CRM_CREATE_REQUEST_IS_IMPORTANT"] = "Важлива справа";
$MESS["CRM_CREATE_REQUEST_DESCRIPTION"] = "Опис";
$MESS["CRM_CREATE_REQUEST_RESPONSIBLE_ID"] = "Відповідальний";
$MESS["CRM_CREATE_REQUEST_AUTO_COMPLETE"] = "Виконати автоматично по завершенню процесу";
?>