<?
$MESS["CRM_CREATE_REQUEST_EMPTY_PROP"] = "Le paramètre requis est vide: #PROPERTY#";
$MESS["CRM_CREATE_REQUEST_SUBJECT"] = "Objet";
$MESS["CRM_CREATE_REQUEST_IS_IMPORTANT"] = "Important";
$MESS["CRM_CREATE_REQUEST_DESCRIPTION"] = "Description";
$MESS["CRM_CREATE_REQUEST_RESPONSIBLE_ID"] = "Personne responsable";
$MESS["CRM_CREATE_REQUEST_AUTO_COMPLETE"] = "Compléter automatiquement l'activité lorsque le statut de l'activité est actualisé";
?>