<?
$MESS["CRM_CREATE_REQUEST_EMPTY_PROP"] = "El parámetro requerido está vacío: #PROPERTY#";
$MESS["CRM_CREATE_REQUEST_SUBJECT"] = "Asunto";
$MESS["CRM_CREATE_REQUEST_IS_IMPORTANT"] = "Importante";
$MESS["CRM_CREATE_REQUEST_DESCRIPTION"] = "Descripción";
$MESS["CRM_CREATE_REQUEST_RESPONSIBLE_ID"] = "Persona responsable";
$MESS["CRM_CREATE_REQUEST_AUTO_COMPLETE"] = "Activación automática completa cuando se actualiza el estado de la actividad";
?>