<?
$MESS["BPCWG_EMPTY_GROUP_NAME"] = "Está faltando o nome do grupo.";
$MESS["BPCWG_EMPTY_OWNER"] = "Está faltando o proprietário do grupo.";
$MESS["BPCWG_EMPTY_USERS"] = "Os membros do grupo não estão especificados.";
$MESS["BPCWG_ERROR_SUBJECT_ID"] = "Não é possível obter uma lista de assuntos.";
$MESS["BPCWG_ERROR_CREATE_GROUP"] = "Erro ao criar o grupo.";
$MESS["BPCWG_FIELD_REQUIED"] = "O campo '#FIELD#' é obrigatório.";
?>