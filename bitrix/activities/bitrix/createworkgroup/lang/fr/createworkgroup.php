<?
$MESS["BPCWG_EMPTY_GROUP_NAME"] = "Le nom du groupe est manquant.";
$MESS["BPCWG_EMPTY_OWNER"] = "Le groupe propriétaire est absent.";
$MESS["BPCWG_EMPTY_USERS"] = "Les membres du groupe ne sont pas spécifiés.";
$MESS["BPCWG_ERROR_SUBJECT_ID"] = "Impossible d'obtenir une liste de sujets.";
$MESS["BPCWG_ERROR_CREATE_GROUP"] = "Erreur lors de la création du groupe.";
$MESS["BPCWG_FIELD_REQUIED"] = "Le champ '#FIELD#' doit être rempli.";
?>