<?
$MESS["BPCRIA_SITES_FILTER_ALL"] = "Tous les sites";
$MESS["BPCRIA_SITES_FILTER_SITES"] = "Sites du même groupe choisis";
$MESS["BPCRIA_SITES_GROUPS"] = "Groupes de sites joints";
$MESS["BPCRIA_SYNC_TASKS"] = "en utilisant les tâches";
$MESS["BPCRIA_NO_MODULE"] = "Le module du contrôleur non installé";
$MESS["BPCRIA_SYNC_IMMEDIATE"] = "immédiatement";
$MESS["BPCRIA_SITES_SITES"] = "Le nombre de sites";
$MESS["BPCRIA_SITES_FILTER_GROUPS"] = "Sites appartenant aux certains groupes";
$MESS["BPCRIA_SYNC_TIME"] = "Synchroniser les éléments";
$MESS["BPCRIA_SITES_FILTER_TYPE"] = "Type du choix des sites";
?>