<?
$MESS["BPCRIA_SITES_FILTER_TYPE"] = "Tipo de selección del sitio";
$MESS["BPCRIA_SITES_FILTER_ALL"] = "Todos los sitios";
$MESS["BPCRIA_SITES_FILTER_GROUPS"] = "Sitios de grupos específicos";
$MESS["BPCRIA_SITES_FILTER_SITES"] = "Los sitios seleccionados de un grupo";
$MESS["BPCRIA_SITES_GROUPS"] = "Grupos de sitios controlados";
$MESS["BPCRIA_SITES_SITES"] = "Sitios";
$MESS["BPCRIA_SYNC_TIME"] = "Sincronizar los elementos";
$MESS["BPCRIA_SYNC_IMMEDIATE"] = "inmediantamente";
$MESS["BPCRIA_SYNC_TASKS"] = "utilizando tareas de cliente remoto";
$MESS["BPCRIA_NO_MODULE"] = "El modulo de Controlador del sitio no esta instalado";
?>