<?
$MESS["BPCRIA_SITES_FILTER_TYPE"] = "Tipo de seleção de site";
$MESS["BPCRIA_SITES_FILTER_ALL"] = "Todos os sites";
$MESS["BPCRIA_SITES_FILTER_GROUPS"] = "Sites de grupos especificados";
$MESS["BPCRIA_SITES_FILTER_SITES"] = "Sites selecionados de um grupo";
$MESS["BPCRIA_SITES_GROUPS"] = "Grupos de sites controlados";
$MESS["BPCRIA_SITES_SITES"] = "Sites";
$MESS["BPCRIA_SYNC_TIME"] = "Sincronizar elementos";
$MESS["BPCRIA_SYNC_IMMEDIATE"] = "imediatamente";
$MESS["BPCRIA_SYNC_TASKS"] = "usando tarefas remota de cliente";
$MESS["BPCRIA_NO_MODULE"] = "Módulo controlador do site não está instalado";
?>