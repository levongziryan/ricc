<?
$MESS["BPDA_DESCR_NAME"] = "Pausar Execução";
$MESS["BPDA_DESCR_DESCR"] = "A ação \"Pausar Execução\" suspende os processos para o tempo especificado, atrasando a próxima ação.";
?>