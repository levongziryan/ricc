<?
$MESS["CRM_ACTIVITY_SELECT_TYPE_ENTITY"] = "Selecionar campo de entidade";
$MESS["CRM_ACTIVITY_ENTITY_NAME_LEAD"] = "Cliente potencial";
$MESS["CRM_ACTIVITY_ENTITY_NAME_CONTACT"] = "Contato";
$MESS["CRM_ACTIVITY_ENTITY_NAME_COMPANY"] = "Empresa";
$MESS["CRM_ACTIVITY_ENTITY_NAME_DEAL"] = "Negociação";
$MESS["CRM_ACTIVITY_LABLE_ENTITY_ID"] = "ID de entidade:";
$MESS["CRM_ACTIVITY_LABLE_ENTITY_TYPE"] = "Tipo de entidade:";
?>