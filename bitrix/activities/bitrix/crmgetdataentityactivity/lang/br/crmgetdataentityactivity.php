<?
$MESS["CRM_ACTIVITY_LABLE_SELECT_FIELDS"] = "Selecionar campos:";
$MESS["CRM_ACTIVITY_LABLE_PRINTABLE_VERSION"] = "Versão impressa:";
$MESS["CRM_ACTIVITY_ERROR_DT"] = "Tipo de documento incorreto.";
$MESS["CRM_ACTIVITY_ERROR_FIELD_REQUIED"] = "O campo '#FIELD#' é obrigatório.";
$MESS["CRM_ACTIVITY_ERROR_FIELD_TYPE"] = "O campo '#FIELD#' especifica tipo incorreto.";
$MESS["CRM_ACTIVITY_ERROR_ENTITY_ID"] = "ID de entidade";
$MESS["CRM_ACTIVITY_ERROR_ENTITY_TYPE"] = "Tipo de entidade";
$MESS["CRM_ACTIVITY_ERROR_ENTITY_LIST_FIELDS"] = "Selecionar campo de entidade.";
$MESS["CRM_ACTIVITY_FIELD_MAIN_YES"] = "Sim";
$MESS["CRM_ACTIVITY_FIELD_MAIN_NO"] = "Não";
?>