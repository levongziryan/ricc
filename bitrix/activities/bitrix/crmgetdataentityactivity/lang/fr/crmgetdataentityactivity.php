<?
$MESS["CRM_ACTIVITY_LABLE_SELECT_FIELDS"] = "Sélectionner les champs :";
$MESS["CRM_ACTIVITY_LABLE_PRINTABLE_VERSION"] = "Version papier :";
$MESS["CRM_ACTIVITY_ERROR_DT"] = "Type de document incorrect.";
$MESS["CRM_ACTIVITY_ERROR_FIELD_REQUIED"] = "Le champ '#FIELD#' doit être rempli.";
$MESS["CRM_ACTIVITY_ERROR_FIELD_TYPE"] = "Le champ '#FIELD#' indique un type incorrect.";
$MESS["CRM_ACTIVITY_ERROR_ENTITY_ID"] = "ID d'entité";
$MESS["CRM_ACTIVITY_ERROR_ENTITY_TYPE"] = "Type d'entité";
$MESS["CRM_ACTIVITY_ERROR_ENTITY_LIST_FIELDS"] = "Sélectionner le champ de l’entité.";
$MESS["CRM_ACTIVITY_FIELD_MAIN_YES"] = "Oui";
$MESS["CRM_ACTIVITY_FIELD_MAIN_NO"] = "Non";
?>