<?
$MESS["CRM_WAIT_ACTIVITY_WAIT_TYPE"] = "Tipo de espera";
$MESS["CRM_WAIT_ACTIVITY_WAIT_AFTER"] = "Esperar el número especificado de días";
$MESS["CRM_WAIT_ACTIVITY_WAIT_BEFORE"] = "Espere el número especificado de días hasta el día seleccionado";
$MESS["CRM_WAIT_ACTIVITY_WAIT_DURATION"] = "Intervalo de tiempo de espera, días";
$MESS["CRM_WAIT_ACTIVITY_WAIT_TARGET"] = "Hasta la fecha especificada";
$MESS["CRM_WAIT_ACTIVITY_WAIT_DESCRIPTION"] = "Comentario o instrucciones";
$MESS["CRM_WAIT_ACTIVITY_ERROR_WAIT_TYPE"] = "El tipo de espera no está especificado";
$MESS["CRM_WAIT_ACTIVITY_ERROR_WAIT_DURATION"] = "Tiempo de espera no especificado";
$MESS["CRM_WAIT_ACTIVITY_ERROR_WAIT_TARGET"] = "Espera hasta, no se especifica los datos";
$MESS["CRM_WAIT_ACTIVITY_EXECUTE_ERROR_WAIT_TARGET"] = "No se encontró el campo del documento de destino";
$MESS["CRM_WAIT_ACTIVITY_EXECUTE_ERROR_WAIT_TARGET_TIME"] = "No se puede obtener el valor de fecha del campo del documento";
$MESS["CRM_WAIT_ACTIVITY_EXECUTE_ERROR_WAIT_BEFORE"] = "La fecha de finalización de espera debe ser posterior a la fecha actual. La fecha que ingresó puede estar en el pasado o muy lejos en el futuro.";
$MESS["CRM_WAIT_ACTIVITY_DESCRIPTION_TYPE_AFTER"] = "Esperar #DURATION# o cualquier entrada del cliente";
$MESS["CRM_WAIT_ACTIVITY_DESCRIPTION_TYPE_BEFORE"] = "Esperar #DURATION# until \"#TARGET_DATE#\" o cualquier entrada del cliente";
$MESS["CRM_WAIT_ACTIVITY_DAY_NOMINATIVE"] = "día";
$MESS["CRM_WAIT_ACTIVITY_DAY_GENITIVE_SINGULAR"] = "días";
$MESS["CRM_WAIT_ACTIVITY_DAY_GENITIVE_PLURAL"] = "días";
$MESS["CRM_WAIT_ACTIVITY_WEEK_NOMINATIVE"] = "semana";
$MESS["CRM_WAIT_ACTIVITY_WEEK_GENITIVE_SINGULAR"] = "semanas";
$MESS["CRM_WAIT_ACTIVITY_WEEK_GENITIVE_PLURAL"] = "semanas";
?>