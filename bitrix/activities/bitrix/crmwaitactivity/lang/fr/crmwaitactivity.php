<?
$MESS["CRM_WAIT_ACTIVITY_WAIT_TYPE"] = "Type d'attente";
$MESS["CRM_WAIT_ACTIVITY_WAIT_AFTER"] = "Attendre un nombre spécifié de jours";
$MESS["CRM_WAIT_ACTIVITY_WAIT_BEFORE"] = "Attendre un nombre spécifié de jours jusqu'au jour sélectionné";
$MESS["CRM_WAIT_ACTIVITY_WAIT_DURATION"] = "Intervalle d'interruption, jours";
$MESS["CRM_WAIT_ACTIVITY_WAIT_TARGET"] = "Jusqu'à la date spécifiée";
$MESS["CRM_WAIT_ACTIVITY_WAIT_DESCRIPTION"] = "Commentaire ou instructions";
$MESS["CRM_WAIT_ACTIVITY_ERROR_WAIT_TYPE"] = "Le type d'attente n'est pas précisé";
$MESS["CRM_WAIT_ACTIVITY_ERROR_WAIT_DURATION"] = "L'interruption n'est pas spécifiée";
$MESS["CRM_WAIT_ACTIVITY_ERROR_WAIT_TARGET"] = "La date butoir de l'attente n'est pas spécifiée";
$MESS["CRM_WAIT_ACTIVITY_EXECUTE_ERROR_WAIT_TARGET"] = "Le champ du document cible est introuvable";
$MESS["CRM_WAIT_ACTIVITY_EXECUTE_ERROR_WAIT_TARGET_TIME"] = "Impossible de récupérer la valeur de date du champ de document";
$MESS["CRM_WAIT_ACTIVITY_EXECUTE_ERROR_WAIT_BEFORE"] = "La date de fin de l'attente doit être ultérieure à la date actuelle. La date saisie est passée ou trop lointaine dans l'avenir.";
$MESS["CRM_WAIT_ACTIVITY_DESCRIPTION_TYPE_AFTER"] = "Attendre #DURATION# ou une saisie du client";
$MESS["CRM_WAIT_ACTIVITY_DESCRIPTION_TYPE_BEFORE"] = "Attendre #DURATION# jusqu'à \"#TARGET_DATE#\" ou une saisie du client";
$MESS["CRM_WAIT_ACTIVITY_DAY_NOMINATIVE"] = "jour";
$MESS["CRM_WAIT_ACTIVITY_DAY_GENITIVE_SINGULAR"] = "jours";
$MESS["CRM_WAIT_ACTIVITY_DAY_GENITIVE_PLURAL"] = "jours";
$MESS["CRM_WAIT_ACTIVITY_WEEK_NOMINATIVE"] = "semaine";
$MESS["CRM_WAIT_ACTIVITY_WEEK_GENITIVE_SINGULAR"] = "semaines";
$MESS["CRM_WAIT_ACTIVITY_WEEK_GENITIVE_PLURAL"] = "semaines";
?>