<?
$MESS["CRM_WAIT_ACTIVITY_WAIT_TYPE"] = "Тип очікування";
$MESS["CRM_WAIT_ACTIVITY_WAIT_AFTER"] = "Чекати вказану кількість днів";
$MESS["CRM_WAIT_ACTIVITY_WAIT_BEFORE"] = "Чекати вказану кількість днів до обраної дати";
$MESS["CRM_WAIT_ACTIVITY_WAIT_DURATION"] = "Інтервал, днів";
$MESS["CRM_WAIT_ACTIVITY_WAIT_TARGET"] = "До дати";
$MESS["CRM_WAIT_ACTIVITY_WAIT_DESCRIPTION"] = "Комментар або інструкція";
$MESS["CRM_WAIT_ACTIVITY_ERROR_WAIT_TYPE"] = "Не вказаний тип очікування";
$MESS["CRM_WAIT_ACTIVITY_ERROR_WAIT_DURATION"] = "Не вказаний інтервал очікування";
$MESS["CRM_WAIT_ACTIVITY_ERROR_WAIT_TARGET"] = "Чи не вказана дата очікування";
$MESS["CRM_WAIT_ACTIVITY_EXECUTE_ERROR_WAIT_TARGET"] = "Не знайдено цільове поле документа";
$MESS["CRM_WAIT_ACTIVITY_EXECUTE_ERROR_WAIT_TARGET_TIME"] = "Не вдалося отримати значення дати з поля документа";
$MESS["CRM_WAIT_ACTIVITY_EXECUTE_ERROR_WAIT_BEFORE"] = "Дата завершення очікування повинна бути більше поточної дати. Можливо обрана дата вже пройшла або кількість днів до неї занадто велике.";
$MESS["CRM_WAIT_ACTIVITY_DESCRIPTION_TYPE_AFTER"] = "Чекати #DURATION# або будь-яку активність клієнта";
$MESS["CRM_WAIT_ACTIVITY_DESCRIPTION_TYPE_BEFORE"] = "Чекати #DURATION# до дати \"#TARGET_DATE#\" або будь-яку активність клієнта";
$MESS["CRM_WAIT_ACTIVITY_DAY_NOMINATIVE"] = "день";
$MESS["CRM_WAIT_ACTIVITY_DAY_GENITIVE_SINGULAR"] = "дня";
$MESS["CRM_WAIT_ACTIVITY_DAY_GENITIVE_PLURAL"] = "днів";
$MESS["CRM_WAIT_ACTIVITY_WEEK_NOMINATIVE"] = "тиждень";
$MESS["CRM_WAIT_ACTIVITY_WEEK_GENITIVE_SINGULAR"] = "тижні";
$MESS["CRM_WAIT_ACTIVITY_WEEK_GENITIVE_PLURAL"] = "тижнів";
?>