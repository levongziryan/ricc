<?
$MESS["BPAR_ACT_COMMENT"] = "Commentaire";
$MESS["BPAR_ACT_INFO"] = "Achevé(e)s #PERCENT#% (#REVIEWED# de #TOTAL#)";
$MESS["BPAR_ACT_BUTTON2"] = "A pris connaissance";
$MESS["BPAR_ACT_REVIEWED"] = "La lecture du document est terminée.";
$MESS["BPAR_ACT_TRACK2"] = "Lecture du document par les utilisateurs de la liste: #VAL#";
$MESS["BPAR_ACT_REVIEW_TRACK"] = "#PERSON# utilisateur a lu le document #COMMENT#";
$MESS["BPAR_ACT_PROP_EMPTY1"] = "La propriété 'Utilisateurs' n'est pas indiquée.";
$MESS["BPAR_ACT_PROP_EMPTY4"] = "La propriété 'Nom' n'est pas spécifiée.";
$MESS["BPAA_ACT_APPROVERS_NONE"] = "aucun";
$MESS["BPAA_ACT_NO_ACTION"] = "Action non valide sélectionné.";
$MESS["BPAA_ACT_COMMENT_ERROR"] = "Le champ '#COMMENT_LABEL#'  est obligatoire.";
?>