<?
$MESS["BPAR_DESCR_NAME"] = "Ler o Documento";
$MESS["BPAR_DESCR_DESCR"] = "Ler o documento e postar comentários";
$MESS["BPAR_DESCR_RC"] = "Pessoas Lendo";
$MESS["BPAR_DESCR_TC"] = "Pessoas que ainda não leram";
$MESS["BPAR_DESCR_TA1"] = "Auto Exame";
$MESS["BPAA_DESCR_CM"] = "Comentar";
$MESS["BPAR_DESCR_LR"] = "Última leitura por";
$MESS["BPAR_DESCR_LR_COMMENT"] = "Último comentário do leitor";
$MESS["BPAR_DESCR_TASKS"] = "Tarefas";
?>