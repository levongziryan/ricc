<?
$MESS["BPAR_DESCR_NAME"] = "Leer el documento";
$MESS["BPAR_DESCR_DESCR"] = "Leer el documento y publicar los comentarios";
$MESS["BPAR_DESCR_RC"] = "Personas que leyeron ";
$MESS["BPAR_DESCR_TC"] = "Personas a Leer";
$MESS["BPAR_DESCR_TA1"] = "Examen automático";
$MESS["BPAA_DESCR_CM"] = "Comentario";
$MESS["BPAR_DESCR_LR"] = "Última vez leído por";
$MESS["BPAR_DESCR_LR_COMMENT"] = "Último comentario de lector";
$MESS["BPAR_DESCR_TASKS"] = "Tareas";
?>