<?
$MESS["BPCDA_WRONG_TYPE"] = "Der Typ des Parameters '#PARAM#' ist nicht angegeben.";
$MESS["BPCDA_FIELD_REQUIED"] = "Das Feld '#FIELD#' ist erforderlich.";
$MESS["BPCDA_FIELD_NOT_FOUND"] = "Die Eigenschaft \"#NAME#\" ist nicht angegeben.";
$MESS["BPCDA_MODULE_NOT_LOADED"] = "Das Modul CRM konnte nicht heruntergeladen werden.";
?>