<?
$MESS["BPRIA_DESCR_NAME"] = "Demande pour plus d'informations";
$MESS["BPRIA_DESCR_DESCR"] = "Demande pour plus d'informations";
$MESS["BPAA_DESCR_CM"] = "Commentaire";
$MESS["BPAA_DESCR_LU"] = "Utilisateur qui a fourni l'information";
$MESS["BPAA_DESCR_TA1"] = "Compléter automatiquement";
$MESS["BPAA_DESCR_TASKS"] = "Tâches";
$MESS["BPAA_DESCR_CHANGES"] = "Champs modifiés";
?>