<?
$MESS["BPDUA_DESCR_NAME2"] = "Télécharger sur le Drive";
$MESS["BPDUA_DESCR_DESCR2"] = "Télécharger le fichier sur le Drive de stockage";
$MESS["BPDUA_DESCR_OBJECT_ID"] = "ID des fichiers du Drive";
$MESS["BPDUA_DESCR_DETAIL_URL"] = "Afficher l'URL";
$MESS["BPDUA_DESCR_DOWNLOAD_URL"] = "Télécharger l'URL";
$MESS["BPDUA_DESCR_CATEGORY"] = "Drive";
?>