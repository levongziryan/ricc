<?
$MESS["BPDUA_ACCESS_DENIED"] = "Acesso negado a qualquer pessoa, exceto os administradores do portal.";
$MESS["BPDUA_EMPTY_ENTITY_TYPE"] = "O tipo de armazenamento não foi especificado.";
$MESS["BPDUA_EMPTY_ENTITY_ID"] = "O armazenamento não está especificado.";
$MESS["BPDUA_EMPTY_SOURCE_FILE"] = "Nenhum arquivo selecionado para carregamento.";
$MESS["BPDUA_TARGET_ERROR"] = "Não foi possível resolver o local para carregamento.";
$MESS["BPDUA_SOURCE_ERROR"] = "Erro ao obter arquivo para carregamento.";
$MESS["BPDUA_UPLOAD_ERROR"] = "Erro de carregamento de arquivo.";
?>