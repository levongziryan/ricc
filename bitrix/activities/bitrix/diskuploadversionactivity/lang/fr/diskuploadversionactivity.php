<?
$MESS["BPDUV_ACCESS_DENIED"] = "Accès refusé à tout le monde sauf les administrateurs du portail.";
$MESS["BPDUV_EMPTY_SOURCE_FILE"] = "Aucun fichier à téléchargé n'a été sélectionné.";
$MESS["BPDUV_EMPTY_SOURCE_ID"] = "Le fichier source n'est pas spécifié.";
$MESS["BPDUV_SOURCE_ID_ERROR"] = "Le fichier source n'a pas été trouvé.";
$MESS["BPDUV_SOURCE_FILE_ERROR"] = "Erreur lors de la récupération du fichier à télécharger.";
$MESS["BPDUV_UPLOAD_ERROR"] = "Erreur lors du téléchargement de la nouvelle version.";
?>