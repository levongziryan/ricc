<?
$MESS["BPDUV_ACCESS_DENIED"] = "Acesso negado a qualquer pessoa, exceto os administradores do portal.";
$MESS["BPDUV_EMPTY_SOURCE_FILE"] = "Nenhum arquivo selecionado para carregamento.";
$MESS["BPDUV_EMPTY_SOURCE_ID"] = "O arquivo de origem não está especificado.";
$MESS["BPDUV_SOURCE_ID_ERROR"] = "O arquivo de origem não foi encontrado.";
$MESS["BPDUV_SOURCE_FILE_ERROR"] = "Erro ao obter arquivo para carregamento.";
$MESS["BPDUV_UPLOAD_ERROR"] = "Erro ao carregar a nova versão.";
?>