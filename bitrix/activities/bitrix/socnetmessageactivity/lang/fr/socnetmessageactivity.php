<?
$MESS["BPSNMA_EMPTY_FROM"] = "Le paramètre 'Expéditeur' n'est pas indiqué.";
$MESS["BPSNMA_EMPTY_TO"] = "La propriété 'Destinataire' n'est pas indiquée.";
$MESS["BPSNMA_EMPTY_MESSAGE"] = "Propriété 'Texte du message' n'est pas indiqué.";
$MESS["BPSNMA_MESSAGE"] = "Texte du message";
$MESS["BPSNMA_TO"] = "Destinataire";
$MESS["BPSNMA_FROM"] = "Expéditeur";
$MESS["BPSNMA_FORMAT_ROBOT"] = "Règle d'automatisation des notifications";
?>