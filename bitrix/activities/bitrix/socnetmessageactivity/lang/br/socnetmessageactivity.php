<?
$MESS["BPSNMA_EMPTY_MESSAGE"] = "O parâmetro 'Mensagem de Texto' está faltando.";
$MESS["BPSNMA_EMPTY_TO"] = "O parâmetro 'Destinatário' está faltando.";
$MESS["BPSNMA_EMPTY_FROM"] = "O parâmetro 'Remetente' está faltando.";
$MESS["BPSNMA_MESSAGE"] = "Texto da mensagem";
$MESS["BPSNMA_TO"] = "Destinatário";
$MESS["BPSNMA_FROM"] = "Remetente";
$MESS["BPSNMA_FORMAT_ROBOT"] = "Regra de automação de notificação";
?>