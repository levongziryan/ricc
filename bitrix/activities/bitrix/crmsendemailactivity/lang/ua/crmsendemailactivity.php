<?
$MESS["CRM_SEMA_EMPTY_PROP"] = "Не вказаний текст листа";
$MESS["CRM_SEMA_MESSAGE_TEXT"] = "Текст листа";
$MESS["CRM_SEMA_DEFAULT_SUBJECT"] = "Новий лист (#DATE#)";
$MESS["CRM_SEMA_EMAIL_SUBJECT"] = "Тема";
$MESS["CRM_SEMA_EMAIL_FROM"] = "Від кого";
$MESS["CRM_SEMA_EMAIL_TO"] = "Кому";
$MESS["CRM_SEMA_NO_ADDRESSER"] = "Не вказаний e-mail клієнта";
$MESS["CRM_SEMA_MESSAGE_TEXT_TYPE"] = "Тип тексту";
$MESS["CRM_SEMA_ATTACHMENT_TYPE"] = "Тип вкладень";
$MESS["CRM_SEMA_ATTACHMENT"] = "Вкладення";
$MESS["CRM_SEMA_ATTACHMENT_FILE"] = "Файли документа";
$MESS["CRM_SEMA_ATTACHMENT_DISK"] = "Диск";
$MESS["CRM_SEMA_NO_FROM"] = "Не вказаний або вказаний некоректно відправник повідомлення";
$MESS["CRM_SEMA_DEFAULT_BODY"] = "[без тексту]";
$MESS["CRM_SEMA_NO_RESPONSIBLE"] = "Не знайдено відповідального";
?>