<?
$MESS["CRM_SEMA_EMPTY_PROP"] = "Texto del mensaje es requerido";
$MESS["CRM_SEMA_MESSAGE_TEXT"] = "Texto del mensaje";
$MESS["CRM_SEMA_DEFAULT_SUBJECT"] = "Nuevo e-mail (#DATE#)";
$MESS["CRM_SEMA_EMAIL_SUBJECT"] = "Asunto";
$MESS["CRM_SEMA_EMAIL_FROM"] = "De";
$MESS["CRM_SEMA_EMAIL_TO"] = "Para";
$MESS["CRM_SEMA_NO_ADDRESSER"] = "No se especifica el e-mail del cliente";
$MESS["CRM_SEMA_MESSAGE_TEXT_TYPE"] = "Tipo de texto";
$MESS["CRM_SEMA_ATTACHMENT_TYPE"] = "Tipo de archivo adjunto
";
$MESS["CRM_SEMA_ATTACHMENT"] = "Archivos adjuntos";
$MESS["CRM_SEMA_ATTACHMENT_FILE"] = "Archivos de documentos";
$MESS["CRM_SEMA_ATTACHMENT_DISK"] = "Drive";
$MESS["CRM_SEMA_NO_FROM"] = "El remitente del mensaje es incorrecto o no se ha especificado";
$MESS["CRM_SEMA_DEFAULT_BODY"] = "[sin texto]";
$MESS["CRM_SEMA_NO_RESPONSIBLE"] = "La persona responsable no fue encontrada";
?>