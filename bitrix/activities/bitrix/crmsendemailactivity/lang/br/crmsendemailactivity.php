<?
$MESS["CRM_SEMA_EMPTY_PROP"] = "O texto da mensagem é obrigatório";
$MESS["CRM_SEMA_MESSAGE_TEXT"] = "Texto da mensagem";
$MESS["CRM_SEMA_DEFAULT_SUBJECT"] = "Novo e-mail (#DATE#)";
$MESS["CRM_SEMA_EMAIL_SUBJECT"] = "Assunto";
$MESS["CRM_SEMA_EMAIL_FROM"] = "De";
$MESS["CRM_SEMA_EMAIL_TO"] = "Para";
$MESS["CRM_SEMA_NO_ADDRESSER"] = "O e-mail do cliente não está especificado";
$MESS["CRM_SEMA_MESSAGE_TEXT_TYPE"] = "Tipo de texto";
$MESS["CRM_SEMA_ATTACHMENT_TYPE"] = "Tipo de anexo";
$MESS["CRM_SEMA_ATTACHMENT"] = "Anexos";
$MESS["CRM_SEMA_ATTACHMENT_FILE"] = "Arquivos de documento";
$MESS["CRM_SEMA_ATTACHMENT_DISK"] = "Drive";
?>