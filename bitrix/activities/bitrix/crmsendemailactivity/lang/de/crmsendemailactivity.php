<?
$MESS["CRM_SEMA_EMPTY_PROP"] = "Nachrichtentext ist erforderlich";
$MESS["CRM_SEMA_MESSAGE_TEXT"] = "Nachrichtentext";
$MESS["CRM_SEMA_DEFAULT_SUBJECT"] = "Neue E-Mail (#DATE#)";
$MESS["CRM_SEMA_EMAIL_SUBJECT"] = "Betreff";
$MESS["CRM_SEMA_EMAIL_FROM"] = "Von";
$MESS["CRM_SEMA_EMAIL_TO"] = "Bis";
$MESS["CRM_SEMA_NO_ADDRESSER"] = "E-Mail des Kunden ist nicht angegeben";
$MESS["CRM_SEMA_MESSAGE_TEXT_TYPE"] = "Texttyp";
$MESS["CRM_SEMA_ATTACHMENT_TYPE"] = "Typ der Anhänge";
$MESS["CRM_SEMA_ATTACHMENT"] = "Anhänge";
$MESS["CRM_SEMA_ATTACHMENT_FILE"] = "Dateien des Dokuments";
$MESS["CRM_SEMA_ATTACHMENT_DISK"] = "Drive";
$MESS["CRM_SEMA_NO_FROM"] = "Der Absender der Nachricht ist nicht korrekt, oder ist nicht angegeben";
$MESS["CRM_SEMA_DEFAULT_BODY"] = "[ohne Text]";
$MESS["CRM_SEMA_NO_RESPONSIBLE"] = "Verantwortliche Person wurde nicht gefunden";
?>