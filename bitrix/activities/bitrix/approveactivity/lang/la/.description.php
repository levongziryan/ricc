<?
$MESS["BPAA_DESCR_DESCR"] = "Aprobar el Documento";
$MESS["BPAA_DESCR_NAME"] = "Aprobar el Documento";
$MESS["BPAA_DESCR_VC"] = "Personas Votantes";
$MESS["BPAA_DESCR_TC"] = "Personas para Votar";
$MESS["BPAA_DESCR_VP"] = "Tipo de Votación";
$MESS["BPAA_DESCR_AP"] = "Tipo de Aprobación";
$MESS["BPAA_DESCR_NAP"] = "Promedio de Rechazo";
$MESS["BPAA_DESCR_AC"] = "Aprobado";
$MESS["BPAA_DESCR_NAC"] = "Declinado";
$MESS["BPAA_DESCR_LA"] = "Ultima Votación Por";
$MESS["BPAA_DESCR_TA1"] = "Auto Declinado";
$MESS["BPAA_DESCR_CM"] = "Comentario";
$MESS["BPAA_DESCR_APPROVERS"] = "Aprovado por ";
$MESS["BPAA_DESCR_REJECTERS"] = "Rechazado por";
$MESS["BPAA_DESCR_LA_COMMENT"] = "Último comentario del votante";
$MESS["BPAA_DESCR_TASKS"] = "Tareas";
?>