<?
$MESS["BPAA_DESCR_DESCR"] = "Approuver le document";
$MESS["BPAA_DESCR_NAME"] = "Approuver le document";
$MESS["BPAA_DESCR_VC"] = "Combien de personnes ont voté";
$MESS["BPAA_DESCR_TC"] = "Combien de personnes doivent voter";
$MESS["BPAA_DESCR_VP"] = "Le pourcentage de personnes qui ont votés";
$MESS["BPAA_DESCR_AP"] = "La part de ceux qui ont confirmé, en %";
$MESS["BPAA_DESCR_NAP"] = "Pourcentage de ceux qui ont décliné";
$MESS["BPAA_DESCR_AC"] = "Approuvé";
$MESS["BPAA_DESCR_NAC"] = "Est rejetée";
$MESS["BPAA_DESCR_LA"] = "Un dernier voté";
$MESS["BPAA_DESCR_TA1"] = "Rejet automatique";
$MESS["BPAA_DESCR_APPROVERS"] = "Confirmé par";
$MESS["BPAA_DESCR_REJECTERS"] = "Refusé par";
$MESS["BPAA_DESCR_CM"] = "Commentaire";
$MESS["BPAA_DESCR_LA_COMMENT"] = "Commentaire du dernier votant";
$MESS["BPAA_DESCR_TASKS"] = "Tâches";
?>