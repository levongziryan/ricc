<?
$MESS["CRM_CREATE_MEETING_EMPTY_PROP"] = "Le paramètre requis est vide: #PROPERTY#";
$MESS["CRM_CREATE_MEETING_SUBJECT"] = "Objet de la réunion";
$MESS["CRM_CREATE_MEETING_START_TIME"] = "Date de début";
$MESS["CRM_CREATE_MEETING_END_TIME"] = "Date de fin";
$MESS["CRM_CREATE_MEETING_IS_IMPORTANT"] = "Important";
$MESS["CRM_CREATE_MEETING_DESCRIPTION"] = "Description";
$MESS["CRM_CREATE_MEETING_LOCATION"] = "Lieu";
$MESS["CRM_CREATE_MEETING_NOTIFY_VALUE"] = "Rappeler dans";
$MESS["CRM_CREATE_MEETING_NOTIFY_TYPE"] = "Intervalle de rappel";
$MESS["CRM_CREATE_MEETING_RESPONSIBLE_ID"] = "Personne responsable";
$MESS["CRM_CREATE_MEETING_AUTO_COMPLETE"] = "Auto-exécuter quand le flux de travail se termine";
?>