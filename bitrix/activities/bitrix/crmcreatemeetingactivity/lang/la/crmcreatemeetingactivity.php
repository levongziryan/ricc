<?
$MESS["CRM_CREATE_MEETING_EMPTY_PROP"] = "El parámetro requerido está vacío: #PROPERTY#";
$MESS["CRM_CREATE_MEETING_SUBJECT"] = "Asunto de la reunión";
$MESS["CRM_CREATE_MEETING_START_TIME"] = "Fecha de inicio";
$MESS["CRM_CREATE_MEETING_END_TIME"] = "Fecha de finalización";
$MESS["CRM_CREATE_MEETING_IS_IMPORTANT"] = "Importante";
$MESS["CRM_CREATE_MEETING_DESCRIPTION"] = "Descripción";
$MESS["CRM_CREATE_MEETING_LOCATION"] = "Ubicación";
$MESS["CRM_CREATE_MEETING_NOTIFY_VALUE"] = "Recordar en";
$MESS["CRM_CREATE_MEETING_NOTIFY_TYPE"] = "Intervalo de recordatorio";
$MESS["CRM_CREATE_MEETING_RESPONSIBLE_ID"] = "Persona responsable";
$MESS["CRM_CREATE_MEETING_AUTO_COMPLETE"] = "Ejecutar automáticamente cuando el flujo de trabajo termina";
?>