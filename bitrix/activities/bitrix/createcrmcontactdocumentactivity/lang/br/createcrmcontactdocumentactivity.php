<?
$MESS["BPCDA_WRONG_TYPE"] = "O tipo de parâmetro '#PARAM#' está indefinido.";
$MESS["BPCDA_FIELD_REQUIED"] = "O campo '#FIELD#' é obrigatório.";
$MESS["BPCDA_FIELD_NOT_FOUND"] = "A propriedade \"#NAME#\" está faltando.";
$MESS["BPCDA_MODULE_NOT_LOADED"] = "O módulo de CRM não pode ser carregada.";
?>