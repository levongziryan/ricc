<?
$MESS["CRM_CREATE_CALL_EMPTY_PROP"] = "Le paramètre requis est vide: #PROPERTY#";
$MESS["CRM_CREATE_CALL_SUBJECT"] = "Objet";
$MESS["CRM_CREATE_CALL_START_TIME"] = "Date de début";
$MESS["CRM_CREATE_CALL_END_TIME"] = "Date de fin";
$MESS["CRM_CREATE_CALL_IS_IMPORTANT"] = "Important";
$MESS["CRM_CREATE_CALL_DESCRIPTION"] = "Description";
$MESS["CRM_CREATE_CALL_NOTIFY_VALUE"] = "Rappeler dans";
$MESS["CRM_CREATE_CALL_NOTIFY_TYPE"] = "Intervalle de rappel";
$MESS["CRM_CREATE_CALL_RESPONSIBLE_ID"] = "Personne responsable";
$MESS["CRM_CREATE_CALL_AUTO_COMPLETE"] = "Auto-exécuter quand le flux de travail se termine";
?>