<?
$MESS["CRM_CVTDA_WIZARD_NOT_FOUND"] = "No se puede inicializar el Asistente para conversiones.";
$MESS["CRM_CVTDA_ITEMS"] = "Crear usando la fuente";
$MESS["CRM_CVTDA_DEAL_CATEGORY_ID"] = "Pipeline de la Negocación";
$MESS["CRM_CVTDA_DEAL"] = "Negociaciones";
$MESS["CRM_CVTDA_CONTACT"] = "Contacto";
$MESS["CRM_CVTDA_COMPANY"] = "Compañía";
$MESS["CRM_CVTDA_EMPTY_PROP"] = "Las entidades a crear no se especifican";
$MESS["CRM_CVTDA_INVOICE"] = "Factura";
$MESS["CRM_CVTDA_QUOTE"] = "Cotización";
$MESS["CRM_CVTDA_REQUEST_SUBJECT_LEAD"] = "El prospecto necesita ser convertido";
$MESS["CRM_CVTDA_REQUEST_SUBJECT_DEAL"] = "La negociación necesita ser convertida";
$MESS["CRM_CVTDA_REQUEST_DESCRIPTION_LEAD"] = "Las siguientes entidades necesitan ser creadas usando el prospecto: #ITEMS#";
$MESS["CRM_CVTDA_REQUEST_DESCRIPTION_DEAL"] = "Las siguientes entidades necesitan ser creadas usando la negociación: #ITEMS#";
$MESS["CRM_CVTDA_INCORRECT_DOCUMENT"] = "No se pueden convertir elementos de este tipo";
$MESS["CRM_CVTDA_DEFAULT_CONTACT_NAME"] = "Sin nombre";
?>