<?
$MESS["BPDRMV_ACCESS_DENIED"] = "Acceso denegado excepto a los administradores del portal.";
$MESS["BPDRMV_EMPTY_SOURCE_ID"] = "No se especifica el objeto de origen.";
$MESS["BPDRMV_SOURCE_ERROR"] = "No se encontró objeto de origen.";
$MESS["BPDRMV_REMOVE_ERROR"] = "No se puede eliminar el objeto.";
?>