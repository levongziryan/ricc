<?
$MESS["CRM_CTRNA_EMPTY_MESSAGE"] = "Le texte du message n'est pas spécifié.";
$MESS["CRM_CTRNA_MESSAGE"] = "Texte du message";
$MESS["CRM_CTRNA_EMPTY_TO_USERS"] = "Les destinataires du message sont introuvables.";
$MESS["CRM_CTRNA_TO_USERS"] = "Destinataires";
$MESS["CRM_CTRNA_FORMAT_ROBOT"] = "Règle de contrôle";
?>