<?
$MESS["BPFC_PD_GT"] = "plus";
$MESS["BPFC_PD_YES"] = "Oui";
$MESS["BPFC_PD_ADD"] = "Ajouter une condition supplémentaire";
$MESS["BPFC_PD_VALUE"] = "Valeur";
$MESS["BPFC_PD_CALENDAR"] = "Calendrier";
$MESS["BPFC_PD_LT"] = "moins";
$MESS["BPFC_PD_LE"] = "pas plus de";
$MESS["BPFC_PD_GE"] = "au moins";
$MESS["BPFC_PD_NE"] = "pas trouvé";
$MESS["BPFC_PD_NO"] = "Non";
$MESS["BPFC_PD_EQ"] = "gal à";
$MESS["BPFC_PD_FIELD"] = "Propriété ou variable";
$MESS["BPFC_PD_IN"] = "contient";
$MESS["BPFC_PD_DELETE"] = "Supprimer";
$MESS["BPFC_PD_CONDITION"] = "Condition";
$MESS["BPFC_PD_AND"] = "ET";
$MESS["BPFC_PD_CONTAIN"] = "contient";
$MESS["BPFC_PD_OR"] = "OU";
$MESS["BPFC_PD_PARAMS"] = "Paramètres";
$MESS["BPFC_PD_VARS"] = "Variables";
?>