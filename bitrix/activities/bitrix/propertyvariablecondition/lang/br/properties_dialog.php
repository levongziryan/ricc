<?
$MESS["BPFC_PD_ADD"] = "Adicionar Condição";
$MESS["BPFC_PD_AND"] = "E";
$MESS["BPFC_PD_CALENDAR"] = "Calendário";
$MESS["BPFC_PD_CONDITION"] = "Condição";
$MESS["BPFC_PD_IN"] = "contêm";
$MESS["BPFC_PD_DELETE"] = "Deletar";
$MESS["BPFC_PD_EQ"] = "igual a ";
$MESS["BPFC_PD_LT"] = "menos que";
$MESS["BPFC_PD_GT"] = "mais que";
$MESS["BPFC_PD_NO"] = "Não";
$MESS["BPFC_PD_NE"] = "não igual a ";
$MESS["BPFC_PD_GE"] = "não menos que";
$MESS["BPFC_PD_LE"] = "não mais que";
$MESS["BPFC_PD_FIELD"] = "Propriedade ou Campo";
$MESS["BPFC_PD_VALUE"] = "Valor";
$MESS["BPFC_PD_YES"] = "Sim";
$MESS["BPFC_PD_CONTAIN"] = "contém";
$MESS["BPFC_PD_OR"] = "OU";
$MESS["BPFC_PD_PARAMS"] = "Parâmetros";
$MESS["BPFC_PD_VARS"] = "Variáveis";
?>