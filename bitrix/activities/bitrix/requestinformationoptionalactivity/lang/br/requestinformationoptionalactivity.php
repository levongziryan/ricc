<?
$MESS["BPRIOA_ACT_TRACK1"] = "Informações adicionais serão fornecidas por #VAL#";
$MESS["BPRIOA_ACT_INFO"] = "Aguardando informações adicionais";
$MESS["BPRIOA_LOG_COMMENTS"] = "Comentário";
$MESS["BPRIOA_ACT_APPROVE_TRACK"] = "O usuário #PERSON# forneceu informações adicionais #COMMENT#";
$MESS["BPRIOA_ACT_CANCEL_TRACK"] = "O usuário #PERSON# cancelou informações adicionais #COMMENT#";
$MESS["BPRIOA_ACT_COMMENT"] = "Comentário";
$MESS["BPRIOA_ACT_BUTTON1"] = "Salvar";
$MESS["BPRIOA_ARGUMENT_NULL"] = "O valor '#PARAM#' obrigatório está faltando.";
$MESS["BPRIOA_ACT_PROP_EMPTY1"] = "A propriedade 'Usuários' não está especificada.";
$MESS["BPRIOA_ACT_PROP_EMPTY4"] = "A propriedade 'Nome' está faltando.";
$MESS["BPRIOA_ACT_PROP_EMPTY2"] = "Pelo menos um campo é obrigatório.";
$MESS["BPRIOA_ACT_BUTTON2"] = "Recusar";
$MESS["BPRIOA_ACT_COMMENT_ERROR"] = "O campo '#COMMENT_LABEL#' é obrigatório.";
?>