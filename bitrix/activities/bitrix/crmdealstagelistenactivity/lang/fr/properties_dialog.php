<?
$MESS["BPCDSA_PD_DEAL"] = "ID de transaction";
$MESS["BPCDSA_PD_STAGE"] = "Étape";
$MESS["BPCDSA_PD_STAGE_N"] = "terminé";
$MESS["BPCDSA_PD_STAGE_P"] = "\"#NAME#\" ou terminé";
$MESS["BPCDSA_PD_STAGE_DESCR"] = "Sélectionnez une ou plusieurs étapes à attendre. Le flux de travail attendra que la transaction soit déplacée dans l’étape sélectionnée ou l'étape finale.";
?>