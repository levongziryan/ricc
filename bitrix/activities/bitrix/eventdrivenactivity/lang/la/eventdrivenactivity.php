<?
$MESS["BPEDA_INVALID_CHILD"] = "La primera subactividad de un 'EventDrivenActivity' debe ser una que implemente una interfaz IBPEventActivity.";
$MESS["BPEDA_INVALID_CHILD_B24"] = "La primera acción debe estar en Pausa o en Espera del evento cuando se va a utilizar para escuchar un evento paralelo.";
?>