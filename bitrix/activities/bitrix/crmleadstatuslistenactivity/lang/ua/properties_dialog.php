<?
$MESS["BPCLSLA_PD_LEAD"] = "ID ліда";
$MESS["BPCLSLA_PD_STATUS"] = "Статус";
$MESS["BPCLSLA_PD_STATUS_N"] = "фінальний";
$MESS["BPCLSLA_PD_STATUS_P"] = "'#NAME#' або фінальний";
$MESS["BPCLSLA_PD_STATUS_DESCR"] = "Виберіть один або декілька статусів для очікування. Бізнес-процес буде очікувати перехід ліда у вибрані статуси або фінальний статус.";
?>