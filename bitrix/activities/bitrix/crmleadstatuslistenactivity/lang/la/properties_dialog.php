<?
$MESS["BPCLSLA_PD_LEAD"] = "ID del prospecto";
$MESS["BPCLSLA_PD_STATUS"] = "Estados";
$MESS["BPCLSLA_PD_STATUS_DESCR"] = "Seleccione uno o más estados de espera. El flujo de trabajo esperará hasta que el prospecto pase al estado seleccionado o final.";
?>