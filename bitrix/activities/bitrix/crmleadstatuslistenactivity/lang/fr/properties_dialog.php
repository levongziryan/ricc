<?
$MESS["BPCLSLA_PD_LEAD"] = "ID du client potentiel";
$MESS["BPCLSLA_PD_STATUS"] = "Statut";
$MESS["BPCLSLA_PD_STATUS_DESCR"] = "Sélectionnez un ou plusieurs statuts à attendre. Le flux de travail attendra que le lead soit déplacé dans le statut sélectionné ou le statut final.";
?>