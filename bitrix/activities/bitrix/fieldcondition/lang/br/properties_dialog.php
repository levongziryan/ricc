<?
$MESS["BPFC_PD_ADD"] = "Adicionar Condição";
$MESS["BPFC_PD_AND"] = "E";
$MESS["BPFC_PD_CALENDAR"] = "Calendário";
$MESS["BPFC_PD_CONDITION"] = "Condição";
$MESS["BPFC_PD_IN"] = "contém";
$MESS["BPFC_PD_DELETE"] = "Deletar";
$MESS["BPFC_PD_FIELD"] = "Campo de Documento";
$MESS["BPFC_PD_EQ"] = "igual";
$MESS["BPFC_PD_LT"] = "menos";
$MESS["BPFC_PD_GT"] = "mais";
$MESS["BPFC_PD_NO"] = "Não";
$MESS["BPFC_PD_NE"] = "não é igual";
$MESS["BPFC_PD_GE"] = "não menos";
$MESS["BPFC_PD_LE"] = "não mais";
$MESS["BPFC_PD_VALUE"] = "Valor";
$MESS["BPFC_PD_YES"] = "Sim";
$MESS["BPFC_PD_CONTAIN"] = "contém";
$MESS["BPFC_PD_OR"] = "OU";
$MESS["BPFC_PD_MODIFIED"] = "modificado";
?>