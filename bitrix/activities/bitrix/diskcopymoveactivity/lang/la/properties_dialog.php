<?
$MESS["BPDCM_PD_ENTITY"] = "Para";
$MESS["BPDCM_PD_ENTITY_TYPE_USER"] = "Drive del usuario";
$MESS["BPDCM_PD_ENTITY_TYPE_SG"] = "Red social del Drive del grupo";
$MESS["BPDCM_PD_ENTITY_TYPE_COMMON"] = "Drive pública";
$MESS["BPDCM_PD_ENTITY_TYPE_FOLDER"] = "Carpeta del drive";
$MESS["BPDCM_PD_SOURCE_ID"] = "Objeto de origen";
$MESS["BPDCM_PD_SOURCE_ID_DESCR"] = "Archivo o Carpeta del drive";
$MESS["BPDCM_PD_OPERATION"] = "Operación";
$MESS["BPDCM_PD_OPERATION_COPY"] = "Copiar";
$MESS["BPDCM_PD_OPERATION_MOVE"] = "Mover";
$MESS["BPDCM_PD_ENTITY_ID_USER"] = "Usuario";
$MESS["BPDCM_PD_ENTITY_ID_SG"] = "Grupo";
$MESS["BPDCM_PD_ENTITY_ID_COMMON"] = "Drive";
$MESS["BPDCM_PD_ENTITY_ID_FOLDER"] = "Carpeta";
$MESS["BPDCM_PD_LABEL_CHOOSE"] = "Seleccionar:";
$MESS["BPDCM_PD_LABEL_DISK_CHOOSE_FILE"] = "Seleccione archivo";
$MESS["BPDCM_PD_LABEL_DISK_CHOOSE_FOLDER"] = "Seleccione carpeta";
$MESS["BPDCM_PD_LABEL_DISK_EMPTY"] = "No se ha seleccionado";
$MESS["BPDCM_PD_OPERATOR"] = "Ejecutar como";
?>