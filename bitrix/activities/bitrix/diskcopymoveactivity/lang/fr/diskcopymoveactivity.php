<?
$MESS["BPDCM_ACCESS_DENIED"] = "Accès refusé à tout le monde sauf les administrateurs du portail.";
$MESS["BPDCM_EMPTY_ENTITY_TYPE"] = "Le type de stockage est manquant.";
$MESS["BPDCM_EMPTY_ENTITY_ID"] = "Le stockage n'est pas précisé.";
$MESS["BPDCM_EMPTY_SOURCE_ID"] = "Aucun objet à copier ou à déplacer n'a été spécifié.";
$MESS["BPDCM_TARGET_ERROR"] = "Impossible de résoudre l'emplacement du nouveau dossier.";
$MESS["BPDCM_SOURCE_ERROR"] = "Impossible de résoudre l'objet source.";
$MESS["BPDCM_OPERATION_ERROR"] = "Erreur lors de l'exécution de l'opération.";
$MESS["BPDCM_ADD_FOLDER_ERROR"] = "Impossible de créer un nouveau dossier.";
?>