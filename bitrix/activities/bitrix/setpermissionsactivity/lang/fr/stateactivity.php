<?
$MESS["BPSA_INVALID_CHILD"] = "L'action de type 'StateActivity' ne peut contenir que les actions de type 'StateInitializationActivity', 'StateFinalizationActivity' ou 'EventDrivenActivity'.";
$MESS["BPSA_EMPTY_PERMS"] = "Les droits pour les opérations sur le document dans ce statut ne sont pas spécifiés.";
$MESS["BPSA_TRACK1"] = "Droits pour document dans ce statut #VAL#";
?>