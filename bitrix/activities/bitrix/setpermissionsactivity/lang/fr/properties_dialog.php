<?
$MESS["BPSA_PD_PERM_REWRITE"] = "Réenregistrer";
$MESS["BPSA_PD_PERM"] = "Personnes ayant droit pour l'opération '#OP#'";
$MESS["BPSA_PD_PERM_CLEAR"] = "Annuler";
$MESS["BPSA_PD_PERM_CURRENT_LABEL"] = "Documents premissions actuelles";
$MESS["BPSA_PD_PERM_SCOPE_WORFLOW"] = "Permission fixé par le processus actuel des affaires";
$MESS["BPSA_PD_PERM_SCOPE_DOCUMENT"] = "Toutes les autorisations de documents";
$MESS["BPSA_PD_PERM_HOLD"] = "Maintenir";
$MESS["BPSA_PD_PERM_SCOPE_LABEL"] = "Portée pour la compensation et écraser";
?>