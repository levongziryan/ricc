<?
$MESS["BPSWFA_ERROR_DOCUMENT_ID"] = "ID du document n'est pas indiquée";
$MESS["BPSWFA_ERROR_TEMPLATE"] = "Pas de modèle sélectionné.";
$MESS["BPSWFA_TEMPLATE_PARAMETERS"] = "Paramètres du flux de travail";
$MESS["BPSWFA_TEMPLATE_PARAMETERS_ERROR"] = "Le paramètre requis est vide : #NAME#";
$MESS["BPSWFA_START_ERROR"] = "Erreur de début : #MESSAGE#";
$MESS["BPSWFA_SELFSTART_ERROR"] = "Le modèle ne peut être exécuté de manière récursive";
$MESS["BPSWFA_ACCESS_DENIED"] = "Seuls les administrateurs du portail peuvent accéder aux propriétés des actions.";
?>