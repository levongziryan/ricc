<? CJSCore::Init(array("jquery")); ?>
<? $this->SetViewTarget('above_pagetitle', 500); ?>
<div class="ricc_contacts_filter" data-action="/bitrix/components/bitrix/main.ui.filter/settings.ajax.php?FILTER_ID=<?=$arParams['FILTER_ID']?>&GRID_ID=CRM_CONTACT_LIST_V12&action=SET_FILTER">
	<ul class="ricc_contacts_filter__list">
		<? foreach ($arResult['PRESETS'] as $preset) { ?>
			<li class="ricc_contacts_filter__item <?= $preset['current'] ? 'is-current' : '' ?>" data-filter-id="<?= $preset['key']?>" data-filter-name="<?= $preset['name']?>" data-fields='<?= json_encode($preset['fields'])?>'><?= $preset['name'] ?></li>
		<? } ?>
	</ul>
</div>
<? $this->EndViewTarget(); ?>