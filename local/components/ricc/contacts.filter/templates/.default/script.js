$(function(){
	$('.ricc_contacts_filter').each(function(){
		var $context = $(this);
		var actionUrl = $context.attr('data-action');

		$('.ricc_contacts_filter__item').click(function(e){
			var presetId = $(this).attr('data-filter-id');
			var presetName = $(this).attr('data-filter-name');
			var fields = $(this).attr('data-fields');
			$.ajax({
				'type': 'POST',
				'url': actionUrl,
				'data': {
					'apply_filter': 'Y',
					'save': 'Y',
					'preset_id': presetId,
					'name': presetName,
					'fields': $.parseJSON(fields)
				},
				'dataType': 'json',
				'success': function(res) {
					location.reload();
				}
			});
		});
	});
});