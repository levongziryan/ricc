<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class RiccContactsFilter extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$result = array(
			"CACHE_TYPE" => $arParams["CACHE_TYPE"] ? $arParams["CACHE_TYPE"] : "A",
			"CACHE_TIME" => isset($arParams["CACHE_TIME"]) ?$arParams["CACHE_TIME"]: 36000000,
			"FILTER_ID" => $arParams["FILTER_ID"] ? $arParams["FILTER_ID"] : "CRM_CONTACT_LIST_V12"
		);
		return $result;
	}

	private function __filterSort($a, $b){
		return $a['sort'] - $b['sort'];
	}

	private function getFilterPresets(){
		global $USER;

		$filterPresets = array(
			'filter_my' => array(
				'name' => 'Мои клиенты',
				'fields' => array(
					'ASSIGNED_BY_ID_name' => 'Денис Коровкин',
					'ASSIGNED_BY_ID' => 32
				), 
			),
			'filter_change_my' => array(
				'name' => 'Измененные мной',
				'fields' => array(
					'MODIFY_BY_ID_name' => 'Денис Коровкин',
					'MODIFY_BY_ID' => 32
				),
			),
        );
		/* $this->presets = new \Bitrix\Main\UI\Filter\Options(
			$this->arParams["FILTER_ID"],
			$filterPresets,
			$this->arParams["COMMON_PRESETS_ID"]
		);*/
		
		$options = \CUserOptions::getOption("main.ui.filter", $this->arParams['FILTER_ID'], array(), $USER->GetID());
		$this->options = $options;

		$presets = $options['filters'];
		$current = $_SESSION['main.ui.filter'][$this->arParams['FILTER_ID']]['filter'];
		
		foreach ($presets as $key => &$value) {
			if(!$value['name'] || $key=='tmp_filter'){
				unset($presets[$key]);
			}
			$value['key'] = $key;
			if($current == $key){
				$value['current'] = true;
			}
		}
		unset($value);

		usort($presets, array('self', '__filterSort'));
		$this->presets = $presets;
		$this->current = $current;
	}

	public function executeComponent()
	{
		$this->getFilterPresets();
		$this->arResult['PRESETS'] = $this->presets;
		$this->arResult['CURRENT'] = $this->current;
		$this->arResult['OPTIONS'] = $this->options;
		if($this->startResultCache())
		{
			$this->includeComponentTemplate();
		}
	}
}