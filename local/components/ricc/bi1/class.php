<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Crm\Widget\Filter;
use Bitrix\Crm\Widget\FilterPeriodType;

class RiccBI extends CBitrixComponent
{
	const PROP_TOTAL = 'UF_CRM_1521455525658';
	const PROP_PAID = 'UF_CRM_1521455567928';
	const PROP_REMAINING = 'UF_CRM_1521455593180';

	public function onPrepareComponentParams($arParams)
	{
		CModule::IncludeModule('webprofy.ricc');
		CModule::IncludeModule('crm');

		$result = array(
			"CACHE_TYPE" => $arParams["CACHE_TYPE"] ? $arParams["CACHE_TYPE"] : "A",
			"CACHE_TIME" => isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"]: 3600,
			"CONTACT_TYPE" => isset($arParams["CONTACT_TYPE"]) ? $arParams["CONTACT_TYPE"] : array("5"),
			"FILTER" => isset($arParams["FILTER"]) ? $arParams["FILTER"] : array()
		);
		return $result;
	}

	private function prepareFilter() {
		$arResult = &$this->arResult;

		$arResult['GUID'] = 'RICC_BI_GRID';

		$arResult['FILTER'] = array(
			array(
				'id' => 'RESPONSIBLE_ID',
				'name' => 'Ответственный',
				'default' => true,
				'type' => 'custom_entity',
				'selector' => array(
					'TYPE' => 'user',
					'DATA' => array('ID' => 'responsible', 'FIELD_ID' => 'RESPONSIBLE_ID')
				)
			),
			array(
				'id' => 'PERIOD',
				'name' => 'Период',
				'default' => true,
				'type' => 'date',
				'exclude' => array(
					Bitrix\Main\UI\Filter\DateType::NONE,
					Bitrix\Main\UI\Filter\DateType::CURRENT_DAY,
					Bitrix\Main\UI\Filter\DateType::CURRENT_WEEK,
					Bitrix\Main\UI\Filter\DateType::YESTERDAY,
					Bitrix\Main\UI\Filter\DateType::TOMORROW,
					Bitrix\Main\UI\Filter\DateType::PREV_DAYS,
					Bitrix\Main\UI\Filter\DateType::NEXT_DAYS,
					Bitrix\Main\UI\Filter\DateType::NEXT_WEEK,
					Bitrix\Main\UI\Filter\DateType::NEXT_MONTH,
					Bitrix\Main\UI\Filter\DateType::LAST_MONTH,
					Bitrix\Main\UI\Filter\DateType::LAST_WEEK,
					Bitrix\Main\UI\Filter\DateType::EXACT,
//					Bitrix\Main\UI\Filter\DateType::RANGE
				)
			)
		);

		$arResult['FILTER_ROWS'] = array(
			'RESPONSIBLE_ID' => true,
			'PERIOD' => true
		);

		//region Filter Presets
		$monthPresetFilter = array();
		Filter::addDateType(
			$monthPresetFilter,
			'PERIOD',
			FilterPeriodType::convertToDateType(FilterPeriodType::CURRENT_MONTH)
		);

		$quarterPresetFilter = array();
		Filter::addDateType(
			$quarterPresetFilter,
			'PERIOD',
			FilterPeriodType::convertToDateType(FilterPeriodType::CURRENT_QUARTER)
		);

		$arResult['FILTER_PRESETS'] = array(
			'filter_current_month' => array(
				'name' => FilterPeriodType::getDescription(FilterPeriodType::CURRENT_MONTH),
				'fields' => $monthPresetFilter
			),
			'filter_current_quarter' => array(
				'name' => FilterPeriodType::getDescription(FilterPeriodType::CURRENT_QUARTER),
				'fields' => $quarterPresetFilter
			)
		);
		//endregion

		$gridOptions = new CGridOptions($arResult['GUID']);
		$filterOptions = new Bitrix\Main\UI\Filter\Options($arResult['GUID'], $arResult['FILTER_PRESETS']);
		$arResult['FILTER_FIELDS'] = $filterOptions->getFilter($arResult['FILTER']);

		
		
		
		//region Try to apply default settings if period is not assigned
		if(Filter::getDateType($arResult['FILTER_FIELDS'], 'PERIOD') === '')
		{
			$defaultFilter = array();
			Filter::addDateType(
				$defaultFilter,
				'PERIOD',
				FilterPeriodType::convertToDateType(FilterPeriodType::CURRENT_MONTH)
			);
			$filterOptions->setupDefaultFilter(
				$defaultFilter,
				array_keys($arResult['FILTER_ROWS'])
			);
			$arResult['FILTER_FIELDS'] = $filterOptions->getFilter($arResult['FILTER']);
		}
		//endregion
		
		    global $USER;
		if($USER->GetID()!=1){    
		    $arResult['FILTER_FIELDS']['RESPONSIBLE_ID']=$USER->GetID();
		    $arResult['FILTER_FIELDS']['RESPONSIBLE_ID_label']=$USER->GetFullName();
		    unset($arResult['FILTER'][0]);
		}
		
		
		
		
		Filter::convertPeriodFromDateType($arResult['FILTER_FIELDS'], 'PERIOD');
		$arResult['WIDGET_FILTER'] = Filter::internalizeParams($arResult['FILTER_FIELDS']);

		$gridSettings = $gridOptions->GetOptions();
		$visibleRows = isset($gridSettings['filter_rows']) ? explode(',', $gridSettings['filter_rows']) : array();

		if(!empty($visibleRows))
		{
			foreach(array_keys($arResult['FILTER_ROWS']) as $k)
			{
				$arResult['FILTER_ROWS'][$k] = in_array($k, $visibleRows);
			}
		}

		$arResult['OPTIONS'] = array(
			'filter_rows' => implode(',', array_keys($arResult['FILTER_ROWS'])),
			'filters' => array_merge($arResult['FILTER_PRESETS'], $gridSettings['filters'])
		);

		Filter::sanitizeParams($arResult['WIDGET_FILTER']);
		$commonFilter = new Filter($arResult['WIDGET_FILTER']);
		if($commonFilter->isEmpty())
		{
			$commonFilter->setPeriodTypeID(FilterPeriodType::CURRENT_MONTH);
			$arResult['WIDGET_FILTER'] = $commonFilter->getParams();
		}

		/* if($arResult['DEFAULT_ENTITY_TYPE'] !== '')
		{
			$commonFilter->setContextEntityTypeName($arResult['DEFAULT_ENTITY_TYPE']);
			if($arResult['DEFAULT_ENTITY_ID'] > 0)
			{
				$commonFilter->setContextEntityID($arResult['DEFAULT_ENTITY_ID']);
			}
		} */

		$arResult['WIDGET_FILTER']['enableEmpty'] = false;
		$arResult['WIDGET_FILTER']['defaultPeriodType'] = FilterPeriodType::CURRENT_MONTH;
		
		
		
		
		
		
		
		
	}

	// Счета по месяцам
	// % от оборота по менеджеру
	// % от оборота по источнику

	// Счета по менеджерам
	
	// Счета по источникам
	// Невыставленные счета в отделе сопровождения
	
	
	// Сравнение с прошлым периодом
	// Сравнение с аналогичным периодом предыдущего года
	// % Конверсия лидов (клиентов) по менеджеру  (лид → клиент → закрытый клиент/обслуженный)
	// % Конверсия лидов (клиентов) по источнику
	// Так же нужна статистика поступления лидов, я хочу сравнивать кол потупленных лидов с разных источников. 

	private function getPeriodFromDate($date){
		$arMonthsShort = array("","Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек");
		$ar = array();
		$ar['PERIOD'] = $date->format('Y-m');
		$ar['PERIOD_PRINTABLE'] = $arMonthsShort[$date->format('n')].' '.$date->format('y');
		return $ar;
	}

	private function getInvoices($nofilter = false){
		$arInvoices = array();

		// Оплаченные — по дате оплаты

		$arFilter = array(
			'CANCELED' => 'N',
			'PAYED' => 'Y'
		);

		if(!$nofilter){
			if($this->arResult['FILTER_FIELDS']['PERIOD_from']){
				$arFilter['>=PAY_VOUCHER_DATE'] = $this->arResult['FILTER_FIELDS']['PERIOD_from'];
				$arFilter['<=PAY_VOUCHER_DATE'] = $this->arResult['FILTER_FIELDS']['PERIOD_to'];
			}

			if($this->arResult['FILTER_FIELDS']['RESPONSIBLE_ID']){
				$arFilter['RESPONSIBLE_ID'] = $this->arResult['FILTER_FIELDS']['RESPONSIBLE_ID'];
			}
		}
		
		
		
		

		$ob = CCrmInvoice::GetList(array(), $arFilter, false, false, array('ID', 'UF_DEAL_ID', 'UF_CONTACT_ID', 'UF_CRM_1526454496', 'PAYED', 'DATE_PAYED', 'STATUS_ID', 'DATE_STATUS', 'PRICE', 'CURRENCY', 'RESPONSIBLE_ID', 'DATE_BILL', 'ORDER_TOPIC', 'CANCELED', 'RESPONSIBLE_NAME', 'RESPONSIBLE_LAST_NAME', 'RESPONSIBLE_PERSONAL_PHOTO', 'PAY_VOUCHER_DATE', Ricc::DEAL_INVOICE_IS_DEPOSIT_PROPERTY, Ricc::DEAL_INVOICE_IS_SEARCH_PROPERTY));
		while($ar = $ob->Fetch()){

			if($ar['PAY_VOUCHER_DATE']){
				$date = DateTime::createFromFormat('d.m.Y', $ar['PAY_VOUCHER_DATE']);
			} else {
				$date = DateTime::createFromFormat('d.m.Y', $ar['DATE_BILL']);
			}

			$ar = array_merge($ar, $this->getPeriodFromDate($date));

			$arInvoices[] = $ar;
		}

		// Неоплаченные — по дате выставления счёта

		$arFilter = array(
			'CANCELED' => 'N',
			'PAYED' => 'N'
		);

		if(!$nofilter){
			if($this->arResult['FILTER_FIELDS']['PERIOD_from']){
				$arFilter['>=DATE_BILL'] = $this->arResult['FILTER_FIELDS']['PERIOD_from'];
				$arFilter['<=DATE_BILL'] = $this->arResult['FILTER_FIELDS']['PERIOD_to'];
			}

			if($this->arResult['FILTER_FIELDS']['RESPONSIBLE_ID']){
				$arFilter['RESPONSIBLE_ID'] = $this->arResult['FILTER_FIELDS']['RESPONSIBLE_ID'];
			}
			
			
			
			
		}
		

		$ob = CCrmInvoice::GetList(array(), $arFilter, false, false, array('ID', 'UF_DEAL_ID', 'UF_CONTACT_ID', 'PAYED', 'DATE_PAYED', 'STATUS_ID', 'DATE_STATUS', 'PRICE', 'CURRENCY', 'RESPONSIBLE_ID', 'DATE_BILL', 'ORDER_TOPIC', 'CANCELED', 'RESPONSIBLE_NAME', 'RESPONSIBLE_LAST_NAME', 'RESPONSIBLE_PERSONAL_PHOTO', 'PAY_VOUCHER_DATE', Ricc::DEAL_INVOICE_IS_DEPOSIT_PROPERTY, Ricc::DEAL_INVOICE_IS_SEARCH_PROPERTY));
		while($ar = $ob->Fetch()){

			if($ar['PAY_VOUCHER_DATE']){
				$date = DateTime::createFromFormat('d.m.Y', $ar['PAY_VOUCHER_DATE']);
			} else {
				$date = DateTime::createFromFormat('d.m.Y', $ar['DATE_BILL']);
			}

			$ar = array_merge($ar, $this->getPeriodFromDate($date));

			$arInvoices[] = $ar;
		}

		return $arInvoices;
	}

	private function getContacts(){
		$arContacts = array();
		$ob = CCrmContact::GetListEx(array(), array());
		while($ar = $ob->Fetch()){
			$arContacts[$ar['ID']] = $ar;
		}
		return $arContacts;
	}

	private function getDeals(){
		$arDeals = array();
		
		if($this->arResult['FILTER_FIELDS']['PERIOD_from']){
		    $arFilter['>=PAY_VOUCHER_DATE'] = $this->arResult['FILTER_FIELDS']['PERIOD_from'];
		    $arFilter['<=PAY_VOUCHER_DATE'] = $this->arResult['FILTER_FIELDS']['PERIOD_to'];
		    $arFilter['STATUS_ID']="P";
		}
		
		if($this->arResult['FILTER_FIELDS']['RESPONSIBLE_ID']){
		    $arFilter['RESPONSIBLE_ID'] = $this->arResult['FILTER_FIELDS']['RESPONSIBLE_ID'];
		}
		
		
		
		$ob1 = CCrmInvoice::GetList(array(), $arFilter, false, false, array());
		while($ar1 = $ob1->Fetch()){
		    if($ar1[UF_DEAL_ID]){
		        $alldeal[]=$ar1[UF_DEAL_ID];
		    }
		    
		}
		
		
		
		
		
		
		$ob = CCrmDeal::GetListEx(array(), array("ID"=>$alldeal));
		while($ar = $ob->Fetch()){
			$arDeals[] = $ar;
			//echo '<pre>'; print_r($arDeals); echo '</pre>';
			$sum=0;
			$ob1 = CCrmInvoice::GetList(array(), array("ID"=>$alldeal), false, false, array());
			while($ar1 = $ob1->Fetch()){
			   $sum=$sum+$ar1['PRICE'];
			}
			$arDeals[LEAD]=$lead;
			$arDeals[OPPORTUNITY]=$sum;
			
		}
		
		
		return $arDeals;
	}
	private function getDealsProp(){
	    $arDeals = array();
	    $ob = CCrmDeal::GetListEx(array(), array(),false,false,array("ID","OPPORTUNITY","UF_*"));
	    while($ar = $ob->Fetch()){
	        $arDeals[] = $ar;
	    }
	    return $arDeals;
	}
	

	private function prepareData(){
		$this->invoices = $this->getInvoices();
		$this->allInvoices = $this->getInvoices(true);
		$this->contacts = $this->getContacts();
		$this->deals = $this->getDeals();
		$this->dealsProp = $this->getDealsProp();
	}

	private function invoicesByPay(){
	    $rsData = CUserFieldEnum::GetList( array($by=>$order), array("USER_FIELD_ID"=>729) );
	    while($arRes = $rsData->Fetch())
	    {
            $propVal[$arRes["ID"]]=$arRes["VALUE"];
	    }
	    
	   
	    foreach ($this->invoices as $k=>$v){	       
	        if($v["UF_CRM_1526454496"]){
	           
	            $allvalue[$v["UF_CRM_1526454496"]]["name"]=$propVal[$v["UF_CRM_1526454496"]];
	            $allvalue[$v["UF_CRM_1526454496"]]["payed"]=$allvalue[$v["UF_CRM_1526454496"]]["payed"]+$v['PRICE'];
	        }
	    }
	    return $allvalue;
	}
	
	private function invoicesByPaySumm(){
	    foreach ($this->invoices as $k=>$v){
	        if($v["UF_CRM_1526454496"]){
	            $allvalue=$allvalue+$v['PRICE'];
	        }
	    }
	    return $allvalue; 
	}
	private function invoicesByPeriod(){
		$periods = array();

		foreach ($this->allInvoices as $invoice) {

			$period = $invoice['PERIOD'];

			if(!$periods[$period]){
				$periods[$period] = array(
					'name' => $invoice['PERIOD_PRINTABLE'],
					'payed' => 0,
					'billed' => 0,
					'invoices' => array()
				);
			}
			if($invoice['PAYED'] == 'Y'){
				$periods[$period]['payed'] += $invoice['PRICE'];
				$periods[$period]['invoices'][] = array($invoice['ID'], $invoice['PAY_VOUCHER_DATE'], $invoice['PRICE'], $invoice['PAYED']);
			} else {
				$periods[$period]['billed'] += $invoice['PRICE'];
			}
		}
		ksort($periods);
		cdump($periods);
		return $periods;
	}

	private function invoicesByManager(){
		$managers = array();

		foreach ($this->invoices as $invoice) {
			$managerId = $invoice['RESPONSIBLE_ID'];

			if(!$managers[$managerId]){
				$managers[$managerId] = array(
					'name' => $invoice['RESPONSIBLE_NAME'].' '.$invoice['RESPONSIBLE_LAST_NAME'],
					'photo' => $invoice['RESPONSIBLE_PHOTO'],
					'payed' => 0,
					'billed' => 0,
				);
			}
			if($invoice['PAYED'] == 'Y'){
				$managers[$managerId]['payed'] += $invoice['PRICE'];
			} else {
				$managers[$managerId]['billed'] += $invoice['PRICE'];
			}
		}
		$values = array();
		foreach ($managers as $manager) {
			$values[] = $manager['payed'];
		}
		array_multisort($values, SORT_DESC, $managers);
		return $managers;
	}

	private function getInvoicesSumm(){
		$amount = 0;
		foreach ($this->invoices as $invoice) {
			if($invoice['PAYED'] == Y){
				$amount += $invoice['PRICE'];
			}
		}
		return $amount;
	}

	private function invoicesBySource(){
		$sources = array();

		$sourcesDict = CCrmStatus::GetStatusListEx('SOURCE');

		foreach ($this->invoices as $invoice) {
			$sourceId = $this->contacts[$invoice['UF_CONTACT_ID']]['SOURCE_ID'];
			
			if(!$sources[$sourceId]){
				$sources[$sourceId] = array(
					'name' => $sourceId ? $sourcesDict[$sourceId] : "НЕ УКАЗАНО",
					'payed' => 0,
					'billed' => 0,
				);
			}
			if($invoice['PAYED'] == 'Y'){
				$sources[$sourceId]['payed'] += $invoice['PRICE'];
			} else {
				$sources[$sourceId]['billed'] += $invoice['PRICE'];
			}
		}
		return $sources;
	}

	private function leadsByPeriodBySource(){
		$sourcesDict = CCrmStatus::GetStatusListEx('SOURCE');
		$periods = array();

		$arFilter = array();

		 if($this->arResult['FILTER_FIELDS']['PERIOD_from']){
			$arFilter['>=DATE_CREATE'] = $this->arResult['FILTER_FIELDS']['PERIOD_from'];
			$arFilter['<=DATE_CREATE'] = $this->arResult['FILTER_FIELDS']['PERIOD_to'];
		}

		if($this->arResult['FILTER_FIELDS']['RESPONSIBLE_ID']){
			$arFilter['ASSIGNED_BY_ID'] = $this->arResult['FILTER_FIELDS']['RESPONSIBLE_ID'];
		} 
		
		$lead=0;
		$ob = CCrmLead::GetListEx(array(), $arFilter, false, false, '*', '');
		while($ar = $ob->Fetch()){
		    $lead++;
		}
		
		return $lead;
	}

	private function getContactStats(){
		CModule::IncludeModule('crm');
		$arFilter = array_merge(
			array('TYPE_ID' => $this->arParams['CONTACT_TYPE']),
			$this->arParams['CONTACT_TYPE']
		);
		$ob = CCrmContact::GetListEx(array(), $arFilter, false, false, array('*', self::PROP_TOTAL, self::PROP_PAID, self::PROP_REMAINING));

		$arManagers = array();
		$arTotals = array(
			'TOTAL' => 0,
			'PAID' => 0,
			'REMAINING' => 0,
		);

		while($ar = $ob->Fetch()){
			if(!$arManagers[$ar['ASSIGNED_BY_ID']]){
				$arManagers[$ar['ASSIGNED_BY_ID']] = array(
					'ID' => $ar['ASSIGNED_BY_ID'],
					'LOGIN' => $ar['ASSIGNED_BY_LOGIN'],
					'NAME' => $ar['ASSIGNED_BY_NAME'],
					'LAST_NAME' => $ar['ASSIGNED_BY_LAST_NAME'],
					'PHOTO' => $ar['ASSIGNED_BY_PERSONAL_PHOTO'],
					'TOTAL' => 0,
					'PAID' => 0,
					'REMAINING' => 0
				);
			}
			$arManagers[$ar['ASSIGNED_BY_ID']]['TOTAL'] += floatval($ar[self::PROP_TOTAL]);
			$arManagers[$ar['ASSIGNED_BY_ID']]['PAID'] += floatval($ar[self::PROP_PAID]);
			$arManagers[$ar['ASSIGNED_BY_ID']]['REMAINING'] += floatval($ar[self::PROP_REMAINING]);
			$arTotals['TOTAL'] += floatval($ar[self::PROP_TOTAL]);
			$arTotals['PAID'] += floatval($ar[self::PROP_PAID]);
			$arTotals['REMAINING'] += floatval($ar[self::PROP_REMAINING]);
		}
		return array(
			'MANAGERS' => $arManagers,
			'TOTALS' => $arTotals
		);
	}

	private function getFunnelBySource(){
		$sourcesDict = CCrmStatus::GetStatusListEx('SOURCE');
		$sourcesDict['NONE'] = "НЕ УКАЗАНО";
		$sources = array();
		foreach ($sourcesDict as $key => $name) {
			$sources[$key] = array(
				'name' => $name,
				'lead' => 0,
				'contact' => 0,
				'deal' => 0,
				'invoice' => 0,
				'payment' => 0,
			);
		}

		$arFilter = array();

		if($this->arResult['FILTER_FIELDS']['PERIOD_from']){
			$arFilter['>=DATE_CREATE'] = $this->arResult['FILTER_FIELDS']['PERIOD_from'];
			$arFilter['<=DATE_CREATE'] = $this->arResult['FILTER_FIELDS']['PERIOD_to'];
		}

		if($this->arResult['FILTER_FIELDS']['RESPONSIBLE_ID']){
			$arFilter['ASSIGNED_BY_ID'] = $this->arResult['FILTER_FIELDS']['RESPONSIBLE_ID'];
		}

		$ob = CCrmLead::GetListEx(array(), $arFilter, false, false, '*', 'UF_*');
		while($ar = $ob->Fetch()){
			$sources[$ar['SOURCE_ID']]['lead'] += 1;
			if($ar['CONTACT_ID']){
				$sources[$ar['SOURCE_ID']]['contact'] += 1;
				foreach ($this->deals as $deal) {
					if($deal['CONTACT_ID'] == $ar['CONTACT_ID']){
						$sources[$ar['SOURCE_ID']]['deal'] += 1;
						break;
					}
				}
				foreach ($this->invoices as $invoice) {
					if($invoice['UF_CONTACT_ID'] == $ar['CONTACT_ID']){
						$sources[$ar['SOURCE_ID']]['invoice'] += 1;
						break;
					}
				}
				foreach ($this->invoices as $invoice) {
					if($invoice['UF_CONTACT_ID'] == $ar['CONTACT_ID'] && $invoice['PAYED'] == 'Y'){
						$sources[$ar['SOURCE_ID']]['payment'] += 1;
						break;
					}
				}
			}
		}

		foreach ($this->contacts as $key => $ar) {
			if(!$ar['LEAD_ID']){
				// Контакт без лида. Добавляем и лид и контакт
				$sourceId = $ar['SOURCE_ID'] ? $ar['SOURCE_ID'] : 'NONE';
				$sources[$sourceId]['lead'] += 1;
				$sources[$sourceId]['contact'] += 1;
				foreach ($this->invoices as $invoice) {
					if($invoice['UF_CONTACT_ID'] == $ar['ID']){
						$sources[$sourceId]['invoice'] += 1;
						break;
					}
				}
				foreach ($this->invoices as $invoice) {
					if($invoice['UF_CONTACT_ID'] == $ar['ID'] && $invoice['PAYED'] == 'Y'){
						$sources[$sourceId]['payment'] += 1;
						break;
					}
				}
			}
		}

		$leadsCount = 0;

		foreach ($sources as $key => $source) {
			if($source['lead'] == 0){
				unset($sources[$key]);
				continue;
			} else {
				$leadsCount += $source['lead'];
			}
		}
		foreach ($sources as $key => $source) {
			$sources[$key]['conversion'] = round(100 * $source['payment'] / $source['lead'], 2);
			$sources[$key]['percentage'] = round(100 * $source['lead'] / $leadsCount, 2);
		}

		// Сортируем по конверсии
		$arValues = array();
		foreach ($sources as $value) {
			$arValues[] = $value['conversion'];
		}
		array_multisort($arValues, SORT_DESC, $sources);

		return $sources;
	}

	private function getFunnelByManager(){
		
		$managers = array();
		
		foreach ($sourcesDict as $key => $name) {
			$sources[$key] = array(
				'name' => $name,
				'lead' => 0,
				'contact' => 0,
				'deal' => 0,
				'invoice' => 0,
				'payment' => 0,
			);
		}

		$arFilter = array();

		if($this->arResult['FILTER_FIELDS']['PERIOD_from']){
			$arFilter['>=DATE_CREATE'] = $this->arResult['FILTER_FIELDS']['PERIOD_from'];
			$arFilter['<=DATE_CREATE'] = $this->arResult['FILTER_FIELDS']['PERIOD_to'];
		}

		if($this->arResult['FILTER_FIELDS']['RESPONSIBLE_ID']){
			$arFilter['ASSIGNED_BY_ID'] = $this->arResult['FILTER_FIELDS']['RESPONSIBLE_ID'];
		}
		

		$ob = CCrmLead::GetListEx(array(), $arFilter, false, false, '*', 'UF_*');
		while($ar = $ob->Fetch()){
			$managerId = $ar['ASSIGNED_BY_ID'];
			if(!$managers[$managerId]){
				$managers[$managerId] = array(
					'name' => $ar['ASSIGNED_BY_NAME'].' '.$ar['ASSIGNED_BY_LAST_NAME'],
					'lead' => 0,
					'contact' => 0,
					'deal' => 0,
					'invoice' => 0,
					'payment' => 0,
				);
			}
			$managers[$managerId]['lead'] += 1;
			if($ar['CONTACT_ID']){
				$managers[$managerId]['contact'] += 1;
				foreach ($this->deals as $deal) {
					if($deal['CONTACT_ID'] == $ar['CONTACT_ID']){
						$managers[$managerId]['deal'] += 1;
						break;
					}
				}
				foreach ($this->invoices as $invoice) {
					if($invoice['UF_CONTACT_ID'] == $ar['CONTACT_ID']){
						$managers[$managerId]['invoice'] += 1;
						break;
					}
				}
				foreach ($this->invoices as $invoice) {
					if($invoice['UF_CONTACT_ID'] == $ar['CONTACT_ID'] && $invoice['PAYED'] == 'Y'){
						$managers[$managerId]['payment'] += 1;
						break;
					}
				}
			}
		}

		foreach ($this->contacts as $key => $ar) {
			if(!$ar['LEAD_ID']){

				$managerId = $ar['ASSIGNED_BY_ID'];
				if(!$managers[$managerId]){
					$managers[$managerId] = array(
						'name' => $ar['ASSIGNED_BY_NAME'].' '.$ar['ASSIGNED_BY_LAST_NAME'],
						'lead' => 0,
						'contact' => 0,
						'deal' => 0,
						'invoice' => 0,
						'payment' => 0,
					);
				}

				// Контакт без лида. Добавляем и лид и контакт
				$managers[$managerId]['lead'] += 1;
				$managers[$managerId]['contact'] += 1;
				foreach ($this->invoices as $invoice) {
					if($invoice['UF_CONTACT_ID'] == $ar['ID']){
						$managers[$managerId]['invoice'] += 1;
						break;
					}
				}
				foreach ($this->invoices as $invoice) {
					if($invoice['UF_CONTACT_ID'] == $ar['ID'] && $invoice['PAYED'] == 'Y'){
						$managers[$managerId]['payment'] += 1;
						break;
					}
				}
			}
		}

		$leadsCount = 0;

		foreach ($managers as $key => $manager) {
			if($manager['lead'] == 0){
				unset($manager[$key]);
				continue;
			} else {
				$leadsCount += $manager['lead'];
			}
		}
		foreach ($managers as $key => $manager) {
			$managers[$key]['conversion'] = round(100 * $manager['payment'] / $manager['lead'], 2);
			$managers[$key]['percentage'] = round(100 * $manager['lead'] / $leadsCount, 2);
		}

		// Сортируем по конверсии
		$arValues = array();
		foreach ($managers as $value) {
			$arValues[] = $value['conversion'];
		}
		array_multisort($arValues, SORT_DESC, $managers);

		return $managers;
	}

	public function checkRights(){
		global $USER;
		$perms = new CCrmPerms($USER->GetID());
		return $perms->HavePerm('CONTACT', CCrmPerms::PERM_ALL, 'READ')
			&& $perms->HavePerm('DEAL', CCrmPerms::PERM_ALL, 'READ')
			&& $perms->HavePerm('INVOICE', CCrmPerms::PERM_ALL, 'READ')
			&& $perms->HavePerm('LEAD', CCrmPerms::PERM_ALL, 'READ');
	}

	public function executeComponent()
	{
		global $USER;

		if(!$this->checkRights()){
			//$this->arResult = array('STATUS' => 'ACCESS_DENIED');
			//$this->includeComponentTemplate();
			//return;
		}

		if($this->startResultCache())
		{
			$this->prepareFilter();
			$this->prepareData();
			$this->arResult['INVOICES_BY_PERIOD'] = $this->invoicesByPeriod();
			$this->arResult['INVOICES_BY_PAY_SUMM'] = $this->invoicesByPaySumm();
			$this->arResult['INVOICES_BY_PAY'] = $this->invoicesByPay();
			$this->arResult['INVOICES_BY_MANAGER'] = $this->invoicesByManager();
			$this->arResult['INVOICES_BY_SOURCE'] = $this->invoicesBySource();
			$this->arResult['LEADS_BY_PERIOD'] = $this->leadsByPeriodBySource();
			$this->arResult['SOURCES'] = CCrmStatus::GetStatusListEx('SOURCE');
			$this->arResult['INVOICES_SUMM'] = $this->getInvoicesSumm();
			$this->arResult['FUNNEL_BY_SOURCE'] = $this->getFunnelBySource();
			$this->arResult['FUNNEL_BY_MANAGER'] = $this->getFunnelByManager();
			$this->arResult['INVOICES'] = $this->invoices;
			$this->arResult['CONTACTS'] = $this->contacts;
			$this->arResult['DEALS'] = $this->deals;
			$this->arResult['DEALS_PROP'] = $this->dealsProp;
			$this->includeComponentTemplate();
		}
	}
}