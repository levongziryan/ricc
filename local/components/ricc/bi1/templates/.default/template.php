<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if($arResult['STATUS'] == 'ACCESS_DENIED'){
	return include(__DIR__.'/no_access.php');
}

CJSCore::Init(array("jquery", "amcharts", 'amcharts_funnel', 'amcharts_serial', 'amcharts_pie'));

$asset = Bitrix\Main\Page\Asset::getInstance();
$asset->addJs('/bitrix/js/crm/common.js');
$asset->addCss('/bitrix/themes/.default/crm-entity-show.css');
$asset->addCss('/bitrix/js/crm/css/crm.css');

$bodyClass = $APPLICATION->GetPageProperty('BodyClass');
$APPLICATION->SetPageProperty('BodyClass', ($bodyClass ? $bodyClass.' ' : '').'pagetitle-toolbar-field-view flexible-layout crm-toolbar crm-pagetitle-view');

?>
<? $APPLICATION->IncludeComponent(
	'bitrix:crm.interface.filter',
	'title',
	array(
		'GRID_ID' => $arResult['GUID'],
		'FILTER' => $arResult['FILTER'],
		'FILTER_ROWS' => $arResult['FILTER_ROWS'],
		'FILTER_FIELDS' => $arResult['FILTER_FIELDS'],
		'FILTER_PRESETS' => $arResult['FILTER_PRESETS'],
		'RENDER_FILTER_INTO_VIEW' => false,
		'OPTIONS' => $arResult['OPTIONS'],
		'ENABLE_PROVIDER' => true,
		'DISABLE_SEARCH' => true,
		'VALUE_REQUIRED_MODE' => true,
		'NAVIGATION_BAR' => null
	),
	$component,
	array('HIDE_ICONS' => true)
); ?>

<?php
$count=0;

foreach ($arResult['DEALS'] as $deal){
    $all=$all+$deal[OPPORTUNITY];
    if($deal[OPPORTUNITY]>=2000){
        $count++;
        $plan=$plan+$deal[OPPORTUNITY];
    }    
}
if($count<7){
    $procents=5;
}elseif($count>=7 and $count<10){
    $procents=10;
}elseif($count>=10){
    $procents=15;
}

?>
<?php 
$rsUser = CUser::GetByID($arResult['FILTER_FIELDS']['RESPONSIBLE_ID']);
$arUser = $rsUser->Fetch();

?>
<div class="ricc-tables" style="margin-left: 10px">
<h2>Отчет менеджера <?=$arUser[NAME]?> <?=$arUser[LAST_NAME]?></h2>
<p>
Количество лидов <b><?=$arResult['LEADS_BY_PERIOD']?></b></br>
Сделок по плану на сумму <b><?=$plan?> €</b></br>
Количество сделок  <b><?=$count?></b></br>
Ваш процент  <b><?=$procents?>%</b></br>
Ваш бонус  <b><?=($plan*$procents/100)?> €</b></p>
</div>

<script>
var chart = AmCharts.makeChart( "chartdiv", {
  "type": "funnel",
  "theme": "light",
  "dataProvider": [

{
    "title": "Количество лидов <?=$arResult['LEADS_BY_PERIOD']?> ",
    "value": 125
  },
                    {
    "title": "Сделок по плану на сумму <?=$plan?> €",
    "value": 100
  },
  	  {
	    "title": "Количество сделок  <?=$count?>",
	    "value": 75
	  }, {
	    "title": "Ваш процент <?=$procents?>%",
	    "value": 50
	  },

   {
    "title": "Ваш бонус  <?=($plan*$procents/100)?> €",
    "value": 25
  }
   ],
  "titleField": "title",
  "marginRight": 200,
  "marginLeft": 15,
  "labelPosition": "right",
  "funnelAlpha": 0.9,
  "valueField": "value",
  "startX": 0,
  "neckWidth": "40%",
  "startAlpha": 0,
  "outlineThickness": 1,
  "neckHeight": "30%",
  "balloonText": "[[title]]",
  "export": {
    "enabled": true
  }
});
$(document).ready(function(){
setTimeout(function(){
	$("text tspan").each(function(){
		text=$(this).text();
		text=text.replace(": 125", '');
		text=text.replace(": 100", '');
		text=text.replace(": 75", '');
		text=text.replace(": 50", '');
		text=text.replace(": 25", '');
		$(this).text(text);		
	});
},1000);	
});


</script>

<!-- HTML -->
<div id="chartdiv"></div>


<?php if($all>0):?>
<div class="ricc-tables">
	<table class="ricc-table ricc-table-invoices">
		<tr>
			<th>Сделка</th>
			<th style="text-align: right">Сумма</th>			
			
		</tr> 
		<? $countPayed = 0; $countBilled = 0; ?>
		<? foreach ($arResult['DEALS'] as $deal) { ?>
			<?php if($deal['OPPORTUNITY']>0):?>
			<tr>
				<td><a href="/crm/deal/show/<?= $deal['ID'] ?>/"><?= $deal['TITLE'] ?></a></td>
				
				<td style="text-align: right"><?= $deal['OPPORTUNITY'] ?>&nbsp;€</td>
			</tr>
			<?php endif;?>
		<? } ?>
		<tr>
			<td  style="font-weight: bold">Итого</td>
			<td style="text-align: right; font-weight: bold"><?= $all ?>&nbsp;€</td>
		</tr>		
	</table>
</div>
<?php endif;?>








<? cdump($arResult); ?>