<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if ($arResult['STATUS'] == 'ACCESS_DENIED') {
	return include(__DIR__ . '/no_access.php');
}

CJSCore::Init(array("jquery", "amcharts", 'amcharts_funnel', 'amcharts_serial', 'amcharts_pie'));

$asset = Bitrix\Main\Page\Asset::getInstance();
$asset->addJs('/bitrix/js/crm/common.js');
$asset->addCss('/bitrix/themes/.default/crm-entity-show.css');
$asset->addCss('/bitrix/js/crm/css/crm.css');


$arLeadsByPeriodGraphs = array();
foreach ($arResult['SOURCES'] as $key => $name) {
	$arLeadsByPeriodGraphs[] = array(
		'balloonText' => "[[category]] $name: <b>[[value]]</b>",
		'fillAlphas' => 1,
		'lineAlpha' => 0,
		'type' => 'column',
		'valueField' => 'source_' . $key,
		'clustered' => false,
		'title' => $name,
		'visibleInLegend' => false
	);
}

$sumLeads = 0;
foreach ($arResult['LEADS_BY_PERIOD'] as $k => $v) {
	$allsummoth = 0;
	foreach ($v as $k1 => $v1) {
		if ($k1 != "name") {


			$k1 = str_replace("source_", "", $k1);
			$arrLead[$k1] = $arrLeads[$k1] + $v1;
			$allsummoth = $allsummoth + $v1;
			$sumLeads = $sumLeads + $v1;
		}
	}

	$arResult['LEADS_BY_PERIOD'][$k]['name'] = $arResult['LEADS_BY_PERIOD'][$k]['name'] . "<br>Кол-во:<br>" . $allsummoth;


}


foreach ($arrLead as $k => $v) {


	if ($arResult['SOURCES'][$k]) {

		//echo "<pre>"; print_r($arResult['SOURCES'][$k]); echo "</pre>";

		$pos = strripos($arResult['SOURCES'][$k], "Лендинг");
		if ($pos !== false) {
			$sourse = "Лендинг";
		}
		$pos = strripos($arResult['SOURCES'][$k], "Facebook");
		if ($pos !== false) {
			$sourse = "Facebook";
		}
		$pos = strripos($arResult['SOURCES'][$k], "Сайт");
		if ($pos !== false) {
			$sourse = "сайт";
		}


		if ($sourse) {
			$arrLeadAll = $arrLeadAll + $v;
			$arrLeads[$sourse] = array("name" => $sourse, "value" => $v + $arrLeads[$sourse]["value"]);
		}

	}
}


$bodyClass = $APPLICATION->GetPageProperty('BodyClass');
$APPLICATION->SetPageProperty('BodyClass', ($bodyClass ? $bodyClass . ' ' : '') . 'pagetitle-toolbar-field-view flexible-layout crm-toolbar crm-pagetitle-view');

?>
<? $APPLICATION->IncludeComponent(
	'bitrix:crm.interface.filter',
	'title',
	array(
		'GRID_ID' => $arResult['GUID'],
		'FILTER' => $arResult['FILTER'],
		'FILTER_ROWS' => $arResult['FILTER_ROWS'],
		'FILTER_FIELDS' => $arResult['FILTER_FIELDS'],
		'FILTER_PRESETS' => $arResult['FILTER_PRESETS'],
		'RENDER_FILTER_INTO_VIEW' => false,
		'OPTIONS' => $arResult['OPTIONS'],
		'ENABLE_PROVIDER' => true,
		'DISABLE_SEARCH' => true,
		'VALUE_REQUIRED_MODE' => true,
		'NAVIGATION_BAR' => null
	),
	$component,
	array('HIDE_ICONS' => true)
); ?>

<?
global $USER;
if ($USER->IsAdmin()):
	echo "<pre>";
	print_r($arResult['INVOICES_BY_MANAGER']);
	echo "</pre>";
endif;
?>