<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class RiccContactsCalendar extends CBitrixComponent
{
	const DEAL_CATEGORY = 2;
	const DATE_INTERVIEW = 'UF_CRM_1518612034839';
	const DATE_FORMFILL = 'UF_CRM_1521724682695';
	const DATE_ARRIVAL = 'UF_CRM_1518612050166';
	const DATE_DEPARTURE = 'UF_CRM_1518612106872';

	public function onPrepareComponentParams($arParams)
	{
		$result = array(
			"CACHE_TYPE" => $arParams["CACHE_TYPE"] ? $arParams["CACHE_TYPE"] : "A",
			"CACHE_TIME" => isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"]: 3600,
			"AJAX" => isset($_REQUEST['ajax']) && $_REQUEST['ajax'] == 'Y',
			"START_DATE" => isset($_REQUEST['start']) ? $_REQUEST['start'] : '',
			"END_DATE" => isset($_REQUEST['end']) ? $_REQUEST['end'] : '',
		);
		return $result;
	}

	public function convertDate($bxdate){
		return substr($bxdate,6,4).'-'.substr($bxdate,3,2).'-'.substr($bxdate,0,2);
	}
	public function convertBirthDate($bxdate){
		return date("Y").'-'.substr($bxdate,3,2).'-'.substr($bxdate,0,2);
	}

	public function getEvents(){
		CModule::IncludeModule('crm');
		$ob = CCrmDeal::GetListEx(array(), array(
			'CATEGORY_ID' => self::DEAL_CATEGORY,
			'CLOSED' => 'N',
			'OPENED' => 'Y',
			'CHECK_PERMISSIONS' => 'N'
		), false, false, array('*', self::DATE_INTERVIEW, self::DATE_FORMFILL, self::DATE_ARRIVAL, self::DATE_DEPARTURE));
		$arEvents = array();
		while($ar = $ob->Fetch()){
			if($ar[self::DATE_INTERVIEW]){
				$arEvents[] = array(
					'deal_id' => $ar['ID'],
					'link' => '/crm/deal/details/'.$ar['ID'].'/',
					'type' => 'interview',
					'title' => 'Собеседование '.$ar['CONTACT_FULL_NAME'],
					'start' => $this->convertDate($ar[self::DATE_INTERVIEW]),
					'color' => '#AE85FC',
				);
			}
			if($ar[self::DATE_FORMFILL]){
				$arEvents[] = array(
					'deal_id' => $ar['ID'],
					'link' => '/crm/deal/details/'.$ar['ID'].'/',
					'type' => 'formfill',
					'title' => 'Анкета '.$ar['CONTACT_FULL_NAME'],
					'start' => $this->convertDate($ar[self::DATE_FORMFILL]),
					'color' => '#6BD5D7'
				);
			}
			if($ar[self::DATE_ARRIVAL]){
				$arEvent = array(
					'deal_id' => $ar['ID'],
					'link' => '/crm/deal/details/'.$ar['ID'].'/',
					'type' => 'arrival',
					'title' => 'Прилёт '.$ar['CONTACT_FULL_NAME'],
					'start' => $this->convertDate($ar[self::DATE_ARRIVAL]),
					'color' => '#EE2C73'
				);
				if($ar[self::DATE_DEPARTURE]){
					$arEvent['end'] = $this->convertDate($ar[self::DATE_DEPARTURE]);
				} else {
					$arEvent['end'] = $this->convertDate($ar[self::DATE_ARRIVAL]); //Плюс сколько-то дней
				}
				$arEvents[] = $arEvent;
			}
		}

		// Дни рождения
		$ob = CCrmContact::GetListEx(array(), array(
			'TYPE_ID' => array(1, 4, 5, 6, 7),
			'CHECK_PERMISSIONS' => 'N'
		));
		while($ar = $ob->Fetch()){
			if($ar['BIRTHDATE']){
				$arEvents[] = array(
					'contact_id' => $ar['ID'],
					'link' => '/crm/contact/details/'.$ar['ID'].'/',
					'type' => 'birthday',
					'title' => 'День рождения '.$ar['FULL_NAME'],
					'start' => $this->convertBirthDate($ar['BIRTHDATE']),
					'color' => '#A7E040'
				);
			}
		}
		
		return $arEvents;
	}

	public function executeComponent()
	{
		global $APPLICATION;
		if($this->arParams['AJAX'] == 'Y' && isset($_REQUEST['action']) && $_REQUEST['action'] == 'events'){
			/* $arEvents = array(
				array(
					'title' => 'Тестовое событие 1',
					'start' => '2018-04-18',
					'color' => '#AE85FC',
					'backgroundColor' => '#AE85FC'
				),
				array(
					'title' => 'Клиент 2',
					'start' => '2018-04-10',
					'end' => '2018-04-20',
					'color' => '#E6DA7A'
				),
			); */
			$arEvents = $this->getEvents();
			$APPLICATION->RestartBuffer();
			header('Content-Type: application/json');
			echo json_encode($arEvents);
			exit();
		}
		if($this->startResultCache())
		{
			$this->arResult['EVENTS'] = $this->getEvents();
			$this->includeComponentTemplate();
		}
	}
}