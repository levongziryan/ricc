$(function(){
	$('#contacts-calendar').fullCalendar({
		defaultView: 'month',
		header: {
	      right: 'prev,next today',
	      left: 'title',
	      center: ''
	    },
		events: './?ajax=Y&action=events',
		height: 700,
		eventClick: function(calEvent, jsEvent, view) {
			console.log(calEvent);
			window.open(calEvent.link);
		}
	});
});