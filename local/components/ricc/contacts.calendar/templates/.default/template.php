<?
$CURDIR = substr(__DIR__, strlen($_SERVER['DOCUMENT_ROOT']));
$this->addExternalJS($CURDIR."/fullcalendar-3.9.0/lib/jquery.min.js");
$this->addExternalJS($CURDIR."/fullcalendar-3.9.0/lib/moment.min.js");
$this->addExternalCss($CURDIR."/fullcalendar-3.9.0/fullcalendar.min.css");
//$this->addExternalCss($CURDIR."/fullcalendar-3.9.0/fullcalendar.print.css");
$this->addExternalJS($CURDIR."/fullcalendar-3.9.0/fullcalendar.js");
$this->addExternalJS($CURDIR."/fullcalendar-3.9.0/locale/ru.js");
$this->addExternalJS($CURDIR."/contacts-calendar.js");
?>

<div class="ricc-calendar">
	<div id="contacts-calendar"></div>
</div>

<? //dump($arResult); ?>