<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if ($arResult['STATUS'] == 'ACCESS_DENIED') {
	return include(__DIR__ . '/no_access.php');
}

CJSCore::Init(array("jquery", "amcharts", 'amcharts_funnel', 'amcharts_serial', 'amcharts_pie'));

$asset = Bitrix\Main\Page\Asset::getInstance();
$asset->addJs('/bitrix/js/crm/common.js');
$asset->addCss('/bitrix/themes/.default/crm-entity-show.css');
$asset->addCss('/bitrix/js/crm/css/crm.css');






$arLeadsByPeriodGraphs = array();
foreach ($arResult['SOURCES'] as $key => $name) {
	$arLeadsByPeriodGraphs[] = array(
		'balloonText' => "[[category]] $name: <b>[[value]]</b>",
		'fillAlphas' => 1,
		'lineAlpha' => 0,
		'type' => 'column',
		'valueField' => 'source_' . $key,
		'clustered' => false,
		'title' => $name,
		'visibleInLegend' => false
	);
}

$sumLeads = 0;
foreach ($arResult['LEADS_BY_PERIOD'] as $k => $v) {
	$allsummoth = 0;
	foreach ($v as $k1 => $v1) {
		if ($k1 != "name") {


			$k1 = str_replace("source_", "", $k1);
			$arrLead[$k1] = $arrLeads[$k1] + $v1;
			$allsummoth = $allsummoth + $v1;
			$sumLeads = $sumLeads + $v1;
		}
	}

	$arResult['LEADS_BY_PERIOD'][$k]['name'] = $arResult['LEADS_BY_PERIOD'][$k]['name'] . "<br>Кол-во:<br>" . $allsummoth;


}


foreach ($arrLead as $k => $v) {

	if ($arResult['SOURCES'][$k]) {

		//echo "<pre>"; print_r($arResult['SOURCES'][$k]); echo "</pre>";

		$pos = strripos($arResult['SOURCES'][$k], "Лендинг");
		if ($pos !== false) {
			$sourse = "Лендинг";
		}
		$pos = strripos($arResult['SOURCES'][$k], "Facebook");
		if ($pos !== false) {
			$sourse = "Facebook";
		}
		$pos = strripos($arResult['SOURCES'][$k], "Сайт");
		if ($pos !== false) {
			$sourse = "сайт";
		}


		if ($sourse) {
			$arrLeadAll = $arrLeadAll + $v;
			$arrLeads[$sourse] = array("name" => $sourse, "value" => $v + $arrLeads[$sourse]["value"]);
		}

	}
}


$bodyClass = $APPLICATION->GetPageProperty('BodyClass');
$APPLICATION->SetPageProperty('BodyClass', ($bodyClass ? $bodyClass . ' ' : '') . 'pagetitle-toolbar-field-view flexible-layout crm-toolbar crm-pagetitle-view');

?>
<? $APPLICATION->IncludeComponent(
	'bitrix:crm.interface.filter',
	'title',
	array(
		'GRID_ID' => $arResult['GUID'],
		'FILTER' => $arResult['FILTER'],
		'FILTER_ROWS' => $arResult['FILTER_ROWS'],
		'FILTER_FIELDS' => $arResult['FILTER_FIELDS'],
		'FILTER_PRESETS' => $arResult['FILTER_PRESETS'],
		'RENDER_FILTER_INTO_VIEW' => false,
		'OPTIONS' => $arResult['OPTIONS'],
		'ENABLE_PROVIDER' => true,
		'DISABLE_SEARCH' => true,
		'VALUE_REQUIRED_MODE' => true,
		'NAVIGATION_BAR' => null
	),
	$component,
	array('HIDE_ICONS' => true)
); ?>



	<script>

		/* AmCharts.addInitHandler( function( chart ) {
		 // Check if `orderByField` is set
		 if ( chart.orderByField === undefined ) {
		 // Nope - do nothing
		 return;
		 }

		 // Re-order the data provider
		 chart.dataProvider.sort( function( a, b ) {
		 if ( a[ chart.orderByField ] > b[ chart.orderByField ] ) {
		 return -1;
		 } else if ( a[ chart.orderByField ] == b[ chart.orderByField ] ) {
		 return 0;
		 }
		 return 1;
		 } );

		 }, [ "serial" ] );

		 AmCharts.addInitHandler( function( chart ) {

		 chart.dataProvider = chart.dataProvider.map(function(v){
		 v.colorPaid = '#22D02C';
		 v.colorTotal = '#56C5D2';
		 return v;
		 });

		 }, [ "serial" ] ); */

		var chart1 = AmCharts.makeChart("invoices_by_period", {
			"type": "serial",
			"theme": "none",
			"legend": {"useGraphSettings": true, "enable": false},
			"dataProvider": <?= json_encode(array_values($arResult['INVOICES_BY_PERIOD_TYPE'])) ?>,
			"valueAxes": [{
				"stackType": "regular",
				"gridAlpha": 0.2,
				"axisAlpha": 0.3
			}],
			"titles": [{
				"text": "Типы договоров и по месяцам"
			}],
			"gridAboveGraphs": true,
			"startDuration": 1,
			"graphs": [
				{
					"balloonText": "[[category]] Сопровождение: <b>[[value]]</b> €",
					"fillAlphas": 1,
					"lineAlpha": 0,
					"type": "column",
					"valueField": "124",
					"clustered": false,
					"colorField": "colorTotal",
					"title": "Сопровождение"
				},
				{
					"balloonText": "[[category]] Продажа: <b>[[value]]</b> €",
					"fillAlphas": 1,
					"lineAlpha": 0,
					"type": "column",
					"valueField": "123",
					"colorField": "colorPaid",
					"title": "Продажа"
				}
			],
			"chartCursor": {
				"categoryBalloonEnabled": false,
				"cursorAlpha": 0,
				"zoomable": false
			},
			"categoryField": "name",
			"categoryAxis": {
				"gridPosition": "start",
				"gridAlpha": 0,
				"tickPosition": "start",
				"tickLength": 20
			},
			"export": {
				"enabled": true
			}

		});




		var chart1_1 = AmCharts.makeChart("invoices_by_period_zakr_all", {
			"type": "serial",
			"theme": "none",
			/*"legend": {"useGraphSettings": true, "enable": false},*/
			"dataProvider": <?= json_encode(array_values($arResult['INVOICE_BY_PERIOD_POST'])) ?>,
			"valueAxes": [{
				"stackType": "regular",
				"gridAlpha": 0.2,
				"axisAlpha": 0.3
			}],
			"titles": [{
				"text": "Закрытия по месяцам"
			}],
			"gridAboveGraphs": true,
			"startDuration": 1,
			"graphs": [
				{
					"fillAlphas": 1,
					"lineAlpha": 0,
					"type": "column",
					"valueField": "summa",
					"colorField": "colorTotal",
					"title": "Оплачено"
				}
			],
			"categoryField": "name",
			"categoryAxis": {
				"gridPosition": "start",
				"gridAlpha": 0,
				"tickPosition": "start",
				"tickLength": 20
			},
			"export": {
				"enabled": true
			}

		});

		var chart1_2 = AmCharts.makeChart("invoices_by_period_zakr_only_managers", {
			"type": "serial",
			"theme": "none",
			/*"legend": {"useGraphSettings": true, "enable": false},*/
			"dataProvider": <?= json_encode(array_values($arResult['INVOICE_BY_PERIOD_POST_ONLY_MANAGERS'])) ?>,
			"valueAxes": [{
				"stackType": "regular",
				"gridAlpha": 0.2,
				"axisAlpha": 0.3
			}],
			"titles": [{
				"text": "Закрытия по месяцам только менеджерами"
			}],
			"gridAboveGraphs": true,
			"startDuration": 1,
			"graphs": [
				{
					"fillAlphas": 1,
					"lineAlpha": 0,
					"type": "column",
					"valueField": "summa",
					"colorField": "colorTotal",
					"title": "Оплачено"
				}
			],
			"categoryField": "name",
			"categoryAxis": {
				"gridPosition": "start",
				"gridAlpha": 0,
				"tickPosition": "start",
				"tickLength": 20
			},
			"export": {
				"enabled": true
			}

		});



		var chart2 = AmCharts.makeChart("invoices_by_manager", {
			"type": "pie",
			"theme": "none",
			"valueField": "payed",
			"titleField": "name",
			"innerRadius": 80,
			"pullOutRadius": 20,
			"marginTop": 30,
			"titles": [{
				"text": "Оплаты по менеджерам"
			}],
			"dataProvider": <?= json_encode(array_values($arResult['INVOICES_BY_MANAGER'])) ?>,
			"export": {
				"enabled": true
			},
			"allLabels": [{
				"y": "54%",
				"align": "center",
				"size": 25,
				"bold": true,
				"text": "<?= $arResult['INVOICES_SUMM']?> €",
				"color": "#555"
			}, {
				"y": "49%",
				"align": "center",
				"size": 15,
				"text": "Всего",
				"color": "#555"
			}],
		});



		<?if($arResult['SOPR_BALANCE']):?>
		var chart356 = AmCharts.makeChart("invoce_by_sopr_eur", {
			"type": "pie",
			"theme": "none",
			"valueField": "SUM",
			"titleField": "NAME",
			"innerRadius": 80,
			"pullOutRadius": 20,
			"marginTop": 10,
			"titles": [{
				"text": "Сопровождение EUR"
			}],
			"dataProvider": <?= json_encode(array_values($arResult['SOPR_BALANCE']['DATA']['EUR'])) ?>,
			"export": {
				"enabled": true
			},
			"allLabels": [{
				"y": "54%",
				"align": "center",
				"size": 20,
				"bold": true,
				"text": "<?=number_format($arResult['SOPR_BALANCE']['TOTAL_SUM']['EUR'], 0, '.', ' ')?> €",
				"color": "#555"
			}, {
				"y": "49%",
				"align": "center",
				"size": 15,
				"text": "Всего <?=$arResult['SOPR_BALANCE']['TOTAL_DEAL']['EUR']?>",
				"color": "#555"
			}],
		});


		var chart355 = AmCharts.makeChart("invoce_by_sopr_usd", {
			"type": "pie",
			"theme": "none",
			"valueField": "SUM",
			"titleField": "NAME",
			"innerRadius": 80,
			"pullOutRadius": 20,
			"marginTop": 10,
			"titles": [{
				"text": "Сопровождение USD"
			}],
			"dataProvider": <?= json_encode(array_values($arResult['SOPR_BALANCE']['DATA']['USD'])) ?>,
			"export": {
				"enabled": true
			},
			"allLabels": [{
				"y": "54%",
				"align": "center",
				"size": 22,
				"bold": true,
				"text": "<?=number_format($arResult['SOPR_BALANCE']['TOTAL_SUM']['USD'], 0, '.', ' ')?> $",
				"color": "#555"
			}, {
				"y": "49%",
				"align": "center",
				"size": 15,
				"text": "Всего <?=$arResult['SOPR_BALANCE']['TOTAL_DEAL']['USD']?>",
				"color": "#555"
			}],
		});



		<?endif?>


		<?if($arResult['INVOICES_BY_PAY']):?>
		var chart33 = AmCharts.makeChart("invoices_by_payd", {
			"type": "pie",
			"theme": "none",
			"valueField": "payed",
			"titleField": "name",
			"innerRadius": 80,
			"pullOutRadius": 20,
			"marginTop": 10,
			"titles": [{
				"text": "Способы оплаты"
			}],
			"dataProvider": <?= json_encode(array_values($arResult['INVOICES_BY_PAY'])) ?>,
			"export": {
				"enabled": true
			},
			"allLabels": [{
				"y": "54%",
				"align": "center",
				"size": 25,
				"bold": true,
				"text": "<?= $arResult['INVOICES_BY_PAY_SUMM']?> €",
				"color": "#555"
			}, {
				"y": "49%",
				"align": "center",
				"size": 15,
				"text": "Всего",
				"color": "#555"
			}],
		});
		<?endif?>
		<?if($arResult['INVOICES_BY_GOROD']):?>
		var chart34 = AmCharts.makeChart("invoices_by_gorod", {
			"type": "pie",
			"theme": "none",
			"valueField": "payed",
			"titleField": "name",
			"innerRadius": 80,
			"pullOutRadius": 20,
			"marginTop": 10,
			"titles": [{
				"text": "Оплата по регионам"
			}],
			"dataProvider": <?= json_encode(array_values($arResult['INVOICES_BY_GOROD'])) ?>,
			"export": {
				"enabled": true
			},
			"allLabels": [{
				"y": "54%",
				"align": "center",
				"size": 14,
				"bold": true,
				"text": "По регионам",
				"color": "#555"
			}, {
				"y": "49%",
				"align": "center",
				"size": 11,
				"text": "Оплата",
				"color": "#555"
			}],
		});

		<?endif;?>





		<?if($arResult['INVOICES_BY_TYPE']):?>

		var chart35 = AmCharts.makeChart("invoce_by_type", {
			"type": "pie",
			"theme": "none",
			"valueField": "pay",
			"titleField": "name",
			"innerRadius": 80,
			"pullOutRadius": 20,
			"marginTop": 10,
			"titles": [{
				"text": "Тип договора"
			}],
			"dataProvider": <?= json_encode(array_values($arResult['INVOICES_BY_TYPE'])) ?>,
			"export": {
				"enabled": true
			},
			"allLabels": [{
				"y": "54%",
				"align": "center",
				"size": 25,
				"bold": true,
				"text": "<?=$arResult['INVOICES_BY_TYPE_ALL_SUM']?> €",
				"color": "#555"
			}, {
				"y": "49%",
				"align": "center",
				"size": 15,
				"text": "Всего <?=$arResult['INVOICES_BY_TYPE_ALL']?>",
				"color": "#555"
			}],
		});
		<?endif?>




		var chart36 = AmCharts.makeChart("lead_by_count", {
			"type": "pie",
			"theme": "none",
			"valueField": "value",
			"titleField": "name",
			"innerRadius": 80,
			"pullOutRadius": 20,
			"marginTop": 30,
			"titles": [{
				"text": "Источники лидов"
			}],
			"dataProvider": <?= json_encode(array_values($arResult['LEADS_SOURCE']['RESULT'])) ?>,
			"export": {
				"enabled": true
			},
			"allLabels": [{
				"y": "54%",
				"align": "center",
				"size": 25,
				"bold": true,
				"text": "<?=$arResult['LEADS_SOURCE']['TOTAL_COUNT']?> шт.",
				"color": "#555"
			}, {
				"y": "49%",
				"align": "center",
				"size": 15,
				"text": "Всего",
				"color": "#555"
			}],
		});


		<?if($arResult['INVOICES_BY_PAY_RESPONCE']):?>

		var chart37 = AmCharts.makeChart("invoce_by_responce", {
			"type": "pie",
			"theme": "none",
			"valueField": "pay",
			"titleField": "name",
			"innerRadius": 80,
			"pullOutRadius": 20,
			"marginTop": 40,
			"titles": [{
				"text": "Сумма закрытий по ответственному"
			}],
			"dataProvider": <?= json_encode(array_values($arResult['INVOICES_BY_PAY_RESPONCE'])) ?>,
			"export": {
				"enabled": true
			},
			"allLabels": [{
				"y": "54%",
				"align": "center",
				"size": 25,
				"bold": true,
				"text": "<?=$arResult['INVOICES_BY_PAY_RESPONCE_ALL']?> €",
				"color": "#555"
			}, {
				"y": "49%",
				"align": "center",
				"size": 15,
				"text": "",
				"color": "#555"
			}],
		});
		<?endif;?>


		<?if($arResult['LEADS_BY_CITY']):?>


		var chart38 = AmCharts.makeChart("lead_by_country", {
			"type": "serial",
			"theme": "none",
			"rotate": "true",
			"legend": {"useGraphSettings": true, "enable": false},
			"dataProvider": <?= json_encode(array_values($arResult['LEADS_BY_CITY'])) ?>,
			"valueAxes": [{
				"gridAlpha": 0.5,
				"axisAlpha": 0.3
			}],
			"titles": [{
				"text": "Лиды по городам"
			}],
			"gridAboveGraphs": true,
			"startDuration": 1,
			"graphs": [
				{
					"balloonText": "[[name]] количество: <b>[[value]]</b>",
					"fillAlphas": 1,
					"lineAlpha": 0,
					"type": "column",
					"valueField": "value",
					"clustered": false,
					"colorField": "colorTotal",
				}
			],
			"chartCursor": {
				"categoryBalloonEnabled": false,
				"cursorAlpha": 0,
				"zoomable": false
			},
			"categoryField": "name",
			"export": {
				"enabled": true
			}

		});


		/*
		 var chart38 = AmCharts.makeChart( "lead_by_country", {
		 "type": "serial",
		 "theme": "none",
		 "legend": { "useGraphSettings": true, "enable": false },
		 "dataProvider": <?= json_encode(array_values($arResult['LEADS_BY_CITY'])) ?>,
		 "valueAxes": [ {
		 "stackType": "regular",
		 "gridAlpha": 0.2,
		 "axisAlpha": 0.3
		 } ],
		 "titles": [{
		 "text": "Оплаты по месяцам"
		 }],
		 "gridAboveGraphs": true,
		 "startDuration": 1,
		 "graphs": [
		 {
		 "balloonText": "[[name]] количество: <b>[[value]]</b>",
		 "fillAlphas": 1,
		 "lineAlpha": 0,
		 "type": "column",
		 "valueField": "value",
		 "clustered": false,
		 "colorField": "colorTotal",
		 }
		 ],
		 "chartCursor": {
		 "categoryBalloonEnabled": false,
		 "cursorAlpha": 0,
		 "zoomable": false
		 },
		 "categoryField": "name",
		 "categoryAxis": {
		 "gridPosition": "start",
		 "gridAlpha": 0,
		 "tickPosition": "start",
		 "tickLength": 10
		 },
		 "export": {
		 "enabled": true
		 }

		 } );



		 var chart38 = AmCharts.makeChart( "lead_by_country", {
		 "type": "pie",
		 "theme": "none",
		 "valueField": "value",
		 "titleField": "name",
		 "innerRadius": 80,
		 "pullOutRadius": 20,
		 "marginTop": 10,
		 "titles": [{
		 "text": "Лиды по городам"
		 }],
		 "dataProvider": <?= json_encode(array_values($arResult['LEADS_BY_CITY'])) ?>,
		 "export": {
		 "enabled": true
		 },
		 "allLabels": [{
		 "y": "54%",
		 "align": "center",
		 "size": 25,
		 "bold": true,
		 "text": "<?=$arResult['LEADS_BY_CITY_ALL']?>",
		 "color": "#555"
		 }, {
		 "y": "49%",
		 "align": "center",
		 "size": 15,
		 "text": "Всего",
		 "color": "#555"
		 }],
		 } );
		 */
		<?endif ?>

		var chart3 = AmCharts.makeChart("invoices_by_source", {
			"type": "pie",
			"theme": "none",
			"valueField": "payed",
			"titleField": "name",
			"innerRadius": 80,
			"pullOutRadius": 20,
			"marginTop": 30,
			"titles": [{
				"text": "Оплаты по источникам"
			}],
			"dataProvider": <?= json_encode(array_values($arResult['INVOICES_BY_SOURCE'])) ?>,
			"export": {
				"enabled": true
			},
			"allLabels": [{
				"y": "54%",
				"align": "center",
				"size": 25,
				"bold": true,
				"text": "<?= $arResult['INVOICES_SUMM']?> €",
				"color": "#555"
			}, {
				"y": "49%",
				"align": "center",
				"size": 15,
				"text": "Всего",
				"color": "#555"
			}],
		});




		/*var chart4 = AmCharts.makeChart("leads_by_period", {
		 "type": "serial",
		 "theme": "none",
		 "legend": {"useGraphSettings": true},
		 "dataProvider": <?= json_encode(array_values($arResult['LEADS_BY_PERIOD'])) ?>,
		 "valueAxes": [{
		 "stackType": "regular",
		 "gridAlpha": 0.2,
		 "axisAlpha": 0.3
		 }],
		 "titles": [{
		 "text": "Источники по месяцам. Всего <?=$sumLeads?> лидов"
		 }],
		 "gridAboveGraphs": true,
		 "startDuration": 1,
		 "graphs": <?= json_encode($arLeadsByPeriodGraphs)?>,
		 "chartCursor": {
		 "categoryBalloonEnabled": false,
		 "cursorAlpha": 0,
		 "zoomable": false
		 },
		 "categoryField": "name",
		 "categoryAxis": {
		 "gridPosition": "start",
		 "gridAlpha": 0,
		 "tickPosition": "start",
		 "tickLength": 20
		 },
		 "export": {
		 "enabled": true
		 }

		 });
		 */



		/*var chart4 = AmCharts.makeChart("leads_by_period", {
		 "type": "serial",
		 "theme": "none",
		 "legend": {"useGraphSettings": true},
		 "dataProvider": <?= json_encode(array_values($arResult['LEADS_BY_PERIOD'])) ?>,
		 "valueAxes": [{
		 "stackType": "regular",
		 "gridAlpha": 0.2,
		 "axisAlpha": 0.3
		 }],
		 "titles": [{
		 "text": "Источники по месяцам. Всего <?=$sumLeads?> лидов"
		 }],
		 "gridAboveGraphs": true,
		 "startDuration": 1,
		 "graphs": <?= json_encode($arLeadsByPeriodGraphs)?>,
		 "chartCursor": {
		 "categoryBalloonEnabled": false,
		 "cursorAlpha": 0,
		 "zoomable": false
		 },
		 "categoryField": "name",
		 "categoryAxis": {
		 "gridPosition": "start",
		 "gridAlpha": 0,
		 "tickPosition": "start",
		 "tickLength": 20
		 },
		 "export": {
		 "enabled": true
		 }

		 });
		 */

		<?if($arResult['UPSALE_AND_ABON']['14']):?>
		var chart06062019_1 = AmCharts.makeChart("upsale_otv", {
			"type": "pie",
			"theme": "none",
			"valueField": "SUM",
			"titleField": "NAME",
			"innerRadius": 80,
			"pullOutRadius": 20,
			"marginTop": 30,
			"titles": [{
				"text": "UpSale"
			}],
			"dataProvider": <?= json_encode(array_values($arResult['UPSALE_AND_ABON']['14'])) ?>,
			"export": {
				"enabled": true
			},
			"allLabels": [{
				"y": "54%",
				"align": "center",
				"size": 25,
				"bold": true,
				"text": "<?= $arResult['UPSALE_AND_ABON']['TOTAL_SUM_14']?>	€",
				"color": "#555"
			}, {
				"y": "49%",
				"align": "center",
				"size": 15,
				"text": "Всего",
				"color": "#555"
			}],
		});
		<?endif;?>
		<?if($arResult['UPSALE_AND_ABON']['5']):?>
		var chart06062019_2 = AmCharts.makeChart("abon_otv", {
			"type": "pie",
			"theme": "none",
			"valueField": "SUM",
			"titleField": "NAME",
			"innerRadius": 80,
			"pullOutRadius": 20,
			"marginTop": 30,
			"titles": [{
				"text": "Абонентский отдел"
			}],
			"dataProvider": <?= json_encode(array_values($arResult['UPSALE_AND_ABON']['5'])) ?>,
			"export": {
				"enabled": true
			},
			"allLabels": [{
				"y": "54%",
				"align": "center",
				"size": 25,
				"bold": true,
				"text": "<?= $arResult['UPSALE_AND_ABON']['TOTAL_SUM_5']?> €",
				"color": "#555"
			}, {
				"y": "49%",
				"align": "center",
				"size": 15,
				"text": "Всего",
				"color": "#555"
			}],
		});
		<?endif;?>

		var chart06062019_3 = AmCharts.makeChart("conversion_by_source", {
			"type": "pie",
			"theme": "none",
			"valueField": "sum_EUR",
			"titleField": "name",
			"innerRadius": 80,
			"pullOutRadius": 20,
			"marginTop": 30,
			"titles": [{
				"text": "Закрытие по источникам"
			}],
			"dataProvider": <?= json_encode(array_values($arResult['INVOICES_BY_SOURCE_2']['ITEMS']['EUR'])) ?>,
			"export": {
				"enabled": true
			},
			"allLabels": [{
				"y": "54%",
				"align": "center",
				"size": 25,
				"bold": true,
				"text": "<?= $arResult['INVOICES_BY_SOURCE_2']['TOTAL_EUR']?> €",
				"color": "#555"
			}, {
				"y": "49%",
				"align": "center",
				"size": 15,
				"text": "Всего",
				"color": "#555"
			}],
		});


		/*var chart5 = AmCharts.makeChart("conversion_by_source", {
		 "type": "serial",
		 "theme": "none",
		 "rotate": "true",
		 "legend": {"useGraphSettings": true, "enable": false},
		 "dataProvider": <?= json_encode(array_values($arResult['FUNNEL_BY_SOURCE'])) ?>,
		 "valueAxes": [{
		 "gridAlpha": 0.2,
		 "axisAlpha": 0.3
		 }],
		 "titles": [{
		 "text": "Закрытие по источникам"
		 }],
		 "gridAboveGraphs": true,
		 "startDuration": 1,
		 "graphs": [
		 {
		 "balloonText": "Закрытие: <b>[[value]]</b>%",
		 "fillAlphas": 1,
		 "lineAlpha": 0,
		 "type": "column",
		 "valueField": "conversion",
		 "clustered": true,
		 "colorField": "colorTotal",
		 "title": "Конверсия",
		 "visibleInLegend": false
		 },
		 {
		 "balloonText": "Доля лидов: <b>[[value]]</b>% ([[lead]] шт)",
		 "fillAlphas": 0.5,
		 "lineAlpha": 0,
		 "type": "column",
		 "valueField": "percentage",
		 "clustered": true,
		 "colorField": "colorTotal",
		 "title": "Всего лидов",
		 "visibleInLegend": false
		 }
		 ],
		 "chartCursor": {
		 "categoryBalloonEnabled": false,
		 "cursorAlpha": 0,
		 "zoomable": false
		 },
		 "categoryField": "name",
		 "categoryAxis": {
		 "gridPosition": "start",
		 "gridAlpha": 0,
		 "tickPosition": "start",
		 "tickLength": 20
		 },
		 "export": {
		 "enabled": true
		 }

		 });
		 */

		/*var chart6 = AmCharts.makeChart("conversion_by_manager", {
		 "type": "serial",
		 "theme": "none",
		 "rotate": "true",
		 "legend": {"useGraphSettings": true, "enable": false},
		 "dataProvider": <?= json_encode(array_values($arResult['FUNNEL_BY_MANAGER'])) ?>,
		 "valueAxes": [{
		 "gridAlpha": 0.2,
		 "axisAlpha": 0.3
		 }],
		 "titles": [{
		 "text": "Закрытие по менеджерам"
		 }],
		 "gridAboveGraphs": true,
		 "startDuration": 1,
		 "graphs": [
		 {
		 "balloonText": "Закрытие: <b>[[value]]</b>%",
		 "fillAlphas": 1,
		 "lineAlpha": 0,
		 "type": "column",
		 "valueField": "conversion",
		 "clustered": true,
		 "colorField": "colorTotal",
		 "title": "Конверсия",
		 "visibleInLegend": false
		 },
		 {
		 "balloonText": "Доля лидов: <b>[[value]]</b>% ([[lead]] шт)",
		 "fillAlphas": 0.5,
		 "lineAlpha": 0,
		 "type": "column",
		 "valueField": "percentage",
		 "clustered": true,
		 "colorField": "colorTotal",
		 "title": "Всего лидов",
		 "visibleInLegend": false
		 }
		 ],
		 "chartCursor": {
		 "categoryBalloonEnabled": false,
		 "cursorAlpha": 0,
		 "zoomable": false
		 },
		 "categoryField": "name",
		 "categoryAxis": {
		 "gridPosition": "start",
		 "gridAlpha": 0,
		 "tickPosition": "start",
		 "tickLength": 20
		 },
		 "export": {
		 "enabled": true
		 }

		 });
		 */
	</script>

<? //dump($arResult['FUNNEL_BY_MANAGER']); ?>


	<div class="ricc-widgets">
		<div class="ricc-widgets__row">
			<div class="ricc-widgets__col is-50">
				<div id="invoices_by_manager"></div>
			</div>
			<div class="ricc-widgets__col is-50">
				<div id="invoices_by_period"></div>
			</div>
		</div>
		<div class="ricc-widgets__row">
			<div class="ricc-widgets__col is-50">
				<div id="invoices_by_source"></div>
			</div>
			<div class="ricc-widgets__col is-50">
				<div id="invoices_by_period_zakr_all"></div>
			</div>
		</div>


		<div class="ricc-widgets__row">
			<div class="ricc-widgets__col is-50">
				<div id="upsale_otv"></div>
			</div>
			<div class="ricc-widgets__col is-50">
				<div id="abon_otv"></div>
			</div>
		</div>


		<div class="ricc-widgets__row">
			<div class="ricc-widgets__col is-50">
				<div id="conversion_by_source"></div>
			</div>
			<div class="ricc-widgets__col is-50">
				<div id="lead_by_count"></div>
			</div>
		</div>


		<div class="ricc-widgets__row">
			<div class="ricc-widgets__col is-50">
				<div id="invoce_by_responce"></div>
			</div>
			<div class="ricc-widgets__col is-50">
				<div id="invoices_by_period_zakr_only_managers"></div>
			</div>

		</div>


		<div class="ricc-widgets__row">
			<div class="ricc-widgets__col is-50">
				<div id="invoices_by_gorod"></div>
				<div id="invoce_by_type"></div>
			</div>
			<div class="ricc-widgets__col is-50">
				<div id="invoices_by_payd"></div>
			</div>
		</div>


		<div class="ricc-widgets__row">
			<div class="ricc-widgets__col is-50">
				<div id="invoce_by_sopr_eur"></div>
			</div>
			<div class="ricc-widgets__col is-50">
				<div id="invoce_by_sopr_usd"></div>
			</div>

		</div>


		<?php //echo '<pre>'; print_r($arResult['INVOICES_BY_SOURCE']); echo '</pre>';?>
		<div class="ricc-tables">
			<table class="ricc-table ricc-table-invoices">
				<tr>
					<th>Счёт</th>
					<th>Ответственный</th>
					<th>Оплачен</th>
					<th>Дата выставления</th>
					<th>Дата оплаты</th>
					<th style="text-align: right">Сумма</th>
				</tr>
				<? $countPayed = 0;
				$countBilled = 0; ?>
				<? foreach ($arResult['INVOICES'] as $invoice) { ?>
					<? $invoice['PAYED'] == 'Y' ? $countPayed += $invoice['PRICE'] : $countBilled += $invoice['PRICE'] ?>
					<tr>
						<td>
							<a href="/crm/invoice/show/<?= $invoice['ID'] ?>/"><?= $invoice['ORDER_TOPIC'] ?></a>
						</td>
						<td><?= $invoice['RESPONSIBLE_NAME'] ?> <?= $invoice['RESPONSIBLE_LAST_NAME'] ?></td>
						<td><?= $invoice['PAYED'] == 'Y' ? 'Да' : 'Нет' ?></td>
						<td><?= $invoice['DATE_BILL'] ?></td>
						<td><?= $invoice['PAY_VOUCHER_DATE'] ?></td>
						<td style="text-align: right"><?= $invoice['PRICE'] ?>&nbsp;€</td>
					</tr>
				<? } ?>
				<tr>
					<td colspan="5" style="font-weight: bold">Итого оплачено</td>
					<td style="text-align: right; font-weight: bold"><?= $countPayed ?>&nbsp;€</td>
				</tr>
				<tr>
					<td colspan="5" style="font-weight: bold">Итого выставлено</td>
					<td style="text-align: right; font-weight: bold"><?= $countBilled ?>&nbsp;€</td>
				</tr>
			</table>
		</div>

<? cdump($arResult); ?>