<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class RiccContactsStat extends CBitrixComponent
{
	const PROP_TOTAL = 'UF_CRM_1521455525658';
	const PROP_PAID = 'UF_CRM_1521455567928';
	const PROP_REMAINING = 'UF_CRM_1521455593180';

	public function onPrepareComponentParams($arParams)
	{
		$result = array(
			"CACHE_TYPE" => $arParams["CACHE_TYPE"] ? $arParams["CACHE_TYPE"] : "A",
			"CACHE_TIME" => isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"]: 3600,
			"CONTACT_TYPE" => isset($arParams["CONTACT_TYPE"]) ? $arParams["CONTACT_TYPE"] : array("5"),
			"FILTER" => isset($arParams["FILTER"]) ? $arParams["FILTER"] : array()
		);
		return $result;
	}

	private function getContactStats(){
		CModule::IncludeModule('crm');
		$arFilter = array_merge(
			array('TYPE_ID' => $this->arParams['CONTACT_TYPE']),
			$this->arParams['CONTACT_TYPE']
		);
		$ob = CCrmContact::GetListEx(array(), $arFilter, false, false, array('*', self::PROP_TOTAL, self::PROP_PAID, self::PROP_REMAINING));

		$arManagers = array();
		$arTotals = array(
			'TOTAL' => 0,
			'PAID' => 0,
			'REMAINING' => 0,
		);

		while($ar = $ob->Fetch()){
			if(!$arManagers[$ar['ASSIGNED_BY_ID']]){
				$arManagers[$ar['ASSIGNED_BY_ID']] = array(
					'ID' => $ar['ASSIGNED_BY_ID'],
					'LOGIN' => $ar['ASSIGNED_BY_LOGIN'],
					'NAME' => $ar['ASSIGNED_BY_NAME'],
					'LAST_NAME' => $ar['ASSIGNED_BY_LAST_NAME'],
					'PHOTO' => $ar['ASSIGNED_BY_PERSONAL_PHOTO'],
					'TOTAL' => 0,
					'PAID' => 0,
					'REMAINING' => 0
				);
			}
			$arManagers[$ar['ASSIGNED_BY_ID']]['TOTAL'] += floatval($ar[self::PROP_TOTAL]);
			$arManagers[$ar['ASSIGNED_BY_ID']]['PAID'] += floatval($ar[self::PROP_PAID]);
			$arManagers[$ar['ASSIGNED_BY_ID']]['REMAINING'] += floatval($ar[self::PROP_REMAINING]);
			$arTotals['TOTAL'] += floatval($ar[self::PROP_TOTAL]);
			$arTotals['PAID'] += floatval($ar[self::PROP_PAID]);
			$arTotals['REMAINING'] += floatval($ar[self::PROP_REMAINING]);
		}
		return array(
			'MANAGERS' => $arManagers,
			'TOTALS' => $arTotals
		);
	}

	public function executeComponent()
	{
		if($this->startResultCache())
		{
			$this->arResult = $this->getContactStats();
			$this->includeComponentTemplate();
		}
	}
}