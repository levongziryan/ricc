<? CJSCore::Init(array("jquery", "amcharts", 'amcharts_funnel', 'amcharts_serial', 'amcharts_pie')); ?>

<script>

AmCharts.addInitHandler( function( chart ) {
  // Check if `orderByField` is set
  if ( chart.orderByField === undefined ) {
    // Nope - do nothing
    return;
  }

  // Re-order the data provider
  chart.dataProvider.sort( function( a, b ) {
    if ( a[ chart.orderByField ] > b[ chart.orderByField ] ) {
      return -1;
    } else if ( a[ chart.orderByField ] == b[ chart.orderByField ] ) {
      return 0;
    }
    return 1;
  } );

}, [ "serial" ] );

AmCharts.addInitHandler( function( chart ) {

  chart.dataProvider = chart.dataProvider.map(function(v){
  	v.colorPaid = '#22D02C';
  	v.colorTotal = '#56C5D2';
  	return v;
  });

}, [ "serial" ] );

var chart = AmCharts.makeChart( "contacts_by_managers_chart", {
	"type": "serial",
	"theme": "light",
	"dataProvider": <?= json_encode(array_values($arResult['MANAGERS'])) ?>,
	"valueAxes": [ {
		"gridColor": "#FFFFFF",
		"gridAlpha": 0.2,
		"dashLength": 0
	} ],
	"gridAboveGraphs": true,
	"startDuration": 1,
	"orderByField": "PAID",
	"graphs": [
		{
			"balloonText": "[[category]] Общая сумма: <b>[[value]]</b>",
			"fillAlphas": 0.2,
			"lineAlpha": 0,
			"type": "column",
			"valueField": "TOTAL",
			"clustered": false,
			"colorField": "colorTotal",
		},
		{
			"balloonText": "[[category]] Получено: <b>[[value]]</b>",
			"fillAlphas": 1,
			"lineAlpha": 0.2,
			"type": "column",
			"valueField": "PAID",
			"colorField": "colorPaid",
		}/*,
		{
			"balloonText": "[[category]] Осталось получить: <b>[[value]]</b>",
			"fillAlphas": 0.8,
			"lineAlpha": 0.2,
			"type": "column",
			"valueField": "REMAINING"
		}*/
	],
	"chartCursor": {
		"categoryBalloonEnabled": false,
		"cursorAlpha": 0,
		"zoomable": false
	},
	"categoryField": "NAME",
	"categoryAxis": {
		"gridPosition": "start",
		"gridAlpha": 0,
		"tickPosition": "start",
		"tickLength": 20
	},
	"export": {
		"enabled": true
	}

} );
</script>
<div class="ricc-widgets">
	<div class="ricc-widgets__row">
		<div class="ricc-widgets__col is-30 ricc-widgets__rows">
			<div class="ricc-widgets__row">
				<div class="ricc-widgets__col is-100">
					<div class="ricc-widgets__number ricc-number is-green">
						<div class="ricc-number__title">Получено:</div>
						<div class="ricc-number__number"><?= $arResult['TOTALS']['PAID']?>&nbsp;€</div>
					</div>
				</div>
			</div>
			<div class="ricc-widgets__row">
				<div class="ricc-widgets__col is-100">
					<div class="ricc-widgets__number ricc-number is-orange">
						<div class="ricc-number__title">Осталось получить:</div>
						<div class="ricc-number__number"><?= $arResult['TOTALS']['REMAINING']?>&nbsp;€</div>
					</div>
				</div>
			</div>
			<div class="ricc-widgets__row">
				<div class="ricc-widgets__col is-100">
					<div class="ricc-widgets__number ricc-number is-blue">
						<div class="ricc-number__title">Общая сумма:</div>
						<div class="ricc-number__number"><?= $arResult['TOTALS']['TOTAL']?>&nbsp;€</div>
					</div>
				</div>
			</div>
		</div>
		<div class="ricc-widgets__col is-70">
			<div id="contacts_by_managers_chart"></div>
		</div>
	</div>
</div>