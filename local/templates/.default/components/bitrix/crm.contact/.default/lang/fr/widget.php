<?
$MESS["CRM_CONTACT_WGT_PAGE_TITLE"] = "Contacts: Rapport sommaire";
$MESS["CRM_CONTACT_WGT_DEMO_TITLE"] = "Ceci est un affichage démo. Fermez-le pour obtenir les données analytiques pour vos contacts.";
$MESS["CRM_CONTACT_WGT_DEMO_CONTENT"] = "Si vous n'avez toujours aucun contact, <a href=\"#URL#\" class=\"#CLASS_NAME#\">créez-en un</a> maintenant!";
?>