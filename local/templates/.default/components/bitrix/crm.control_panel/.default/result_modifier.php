<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$panelItems = array();
$idPrefix = "crm_control_panel_";
$crmPanelContainer = $idPrefix."container";
$menuContainerId = $idPrefix."menu";
$searchContainerId = $idPrefix."search";
$searchInputId = $searchContainerId."_input";

$path = explode("/", $APPLICATION->GetCurPage());

if (!empty($arResult["ITEMS"]) && is_array($arResult["ITEMS"]))
{
	foreach ($arResult["ITEMS"] as $key => $item)
	{
		$itemActions = isset($item["ACTIONS"]) ? array(
			"CLASS" => $item["ACTIONS"][0]["ID"] === "CREATE" ? "crm-menu-plus-btn" : "",
			"URL" => $item["ACTIONS"][0]["URL"]
		) : false;

		$isPseudoClient = isset($arParams['CUR_PAGE']) && $arParams['CUR_PAGE'] != 'list';

		$panelItems[] = array(
			"TEXT" => $item["NAME"],
			"URL" => $item["URL"],
			"CLASS" => "crm-menu-".$item["ICON"]." crm-menu-item-wrap",
			"CLASS_SUBMENU_ITEM" => "crm-menu-more-".$item["ICON"],
			"ID" => isset($item["MENU_ID"]) ? $item["MENU_ID"] : $item["ID"],
			"SUB_LINK" => $itemActions,
			"COUNTER" => $item["COUNTER"] > 0 ? $item["COUNTER"] : false,
			"COUNTER_ID" => isset($item["COUNTER_ID"]) ? $item["COUNTER_ID"] : "",
			"IS_ACTIVE" => $arResult["ACTIVE_ITEM_ID"] === $item["ID"] && !$isPseudoClient,
			"IS_LOCKED" => $item["IS_LOCKED"] ? true : false,
			"IS_DISABLED" => $item["IS_DISABLED"] ? true : false
		);

		if($item["MENU_ID"] == 'menu_crm_deal' || $item["ID"] == 'menu_crm_contact'){
			$panelItems[] = array(
				"TEXT" => "На стадии закрытия",
				"URL" => "/crm/deal/category/0/",
				"CLASS" => "crm-menu-deal crm-menu-item-wrap",
				"CLASS_SUBMENU_ITEM" => "crm-menu-more-deal",
				"ID" => "menu_crm_deal_sale",
				"SUB_LINK" => false,
				"COUNTER" => false,
				"COUNTER_ID" => "",
				"IS_ACTIVE" => $arResult["ACTIVE_ITEM_ID"] === "menu_crm_deal_sale" || ($path[2] == 'deal' && $path[4] == 0),
				"IS_LOCKED" => false,
				"IS_DISABLED" => false
			);
			$panelItems[] = array(
				"TEXT" => "Сопровождение",
				"URL" => "/crm/deal/category/2/",
				"CLASS" => "crm-menu-deal crm-menu-item-wrap",
				"CLASS_SUBMENU_ITEM" => "crm-menu-more-deal",
				"ID" => "menu_crm_deal_support",
				"SUB_LINK" => false,
				"COUNTER" => false,
				"COUNTER_ID" => "",
				"IS_ACTIVE" => $arResult["ACTIVE_ITEM_ID"] === "menu_crm_deal_support" || ($path[2] == 'deal' && $path[4] == 2),
				"IS_LOCKED" => false,
				"IS_DISABLED" => false
			);
			$panelItems[] = array(
				"TEXT" => "Поиск документов",
				"URL" => "/crm/deal/category/3/",
				"CLASS" => "crm-menu-deal crm-menu-item-wrap",
				"CLASS_SUBMENU_ITEM" => "crm-menu-more-deal",
				"ID" => "menu_crm_deal_search",
				"SUB_LINK" => false,
				"COUNTER" => false,
				"COUNTER_ID" => "",
				"IS_ACTIVE" => $arResult["ACTIVE_ITEM_ID"] === "menu_crm_deal_search" || ($path[2] == 'deal' && $path[4] == 3),
				"IS_LOCKED" => false,
				"IS_DISABLED" => false
			);
		}

		if($item["MENU_ID"] == 'menu_crm_contact' || $item["ID"] == 'menu_crm_contact'){
			$panelItems[] = array(
				"TEXT" => "Незакрытые",
				"URL" => "/crm/contact/base-open/",
				"CLASS" => "crm-menu-base crm-menu-item-wrap",
				"CLASS_SUBMENU_ITEM" => "crm-menu-more-base",
				"ID" => "menu_crm_base_open",
				"SUB_LINK" => false,
				"COUNTER" => false,
				"COUNTER_ID" => "",
				"IS_ACTIVE" => $arResult["ACTIVE_ITEM_ID"] === "menu_crm_base_open" || $arParams['CUR_PAGE'] == 'base-open',
				"IS_LOCKED" => false,
				"IS_DISABLED" => false
			);
			$panelItems[] = array(
				"TEXT" => "Неликвид",
				"URL" => "/crm/contact/base-spam/",
				"CLASS" => "crm-menu-base crm-menu-item-wrap",
				"CLASS_SUBMENU_ITEM" => "crm-menu-more-base",
				"ID" => "menu_crm_base_spam",
				"SUB_LINK" => false,
				"COUNTER" => false,
				"COUNTER_ID" => "",
				"IS_ACTIVE" => $arResult["ACTIVE_ITEM_ID"] === "menu_crm_base_spam" || $arParams['CUR_PAGE'] == 'base-spam',
				"IS_LOCKED" => false,
				"IS_DISABLED" => false
			);
			$panelItems[] = array(
				"TEXT" => "Закрытые",
				"URL" => "/crm/contact/base-closed/",
				"CLASS" => "crm-menu-base crm-menu-item-wrap",
				"CLASS_SUBMENU_ITEM" => "crm-menu-more-base",
				"ID" => "menu_crm_base_closed",
				"SUB_LINK" => false,
				"COUNTER" => false,
				"COUNTER_ID" => "",
				"IS_ACTIVE" => $arResult["ACTIVE_ITEM_ID"] === "menu_crm_base_closed" || $arParams['CUR_PAGE'] == 'base-closed',
				"IS_LOCKED" => false,
				"IS_DISABLED" => false
			);
			$panelItems[] = array(
				"TEXT" => "Обслуженные",
				"URL" => "/crm/contact/base-completed/",
				"CLASS" => "crm-menu-base crm-menu-item-wrap",
				"CLASS_SUBMENU_ITEM" => "crm-menu-more-base",
				"ID" => "menu_crm_base_completed",
				"SUB_LINK" => false,
				"COUNTER" => false,
				"COUNTER_ID" => "",
				"IS_ACTIVE" => $arResult["ACTIVE_ITEM_ID"] === "menu_crm_base_completed" || $arParams['CUR_PAGE'] == 'base-completed',
				"IS_LOCKED" => false,
				"IS_DISABLED" => false
			);
		}
	}
}

$arResult["CRM_PANEL_CONTAINER_ID"] = $crmPanelContainer;
$arResult["CRM_PANEL_MENU_CONTAINER_ID"] = $menuContainerId;
$arResult["CRM_PANEL_SEARCH_CONTAINER_ID"] = $searchContainerId;
$arResult["CRM_PANEL_SEARCH_INPUT_ID"] = $searchInputId;
$arResult["ITEMS"] = $panelItems;


foreach ($arResult["ITEMS"] as $index => $value) {
	if($value["ID"] == "menu_crm_contact"){
		break;
	}
}
