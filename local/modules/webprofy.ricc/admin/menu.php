<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$aMenu = array(
    array(
        'parent_menu' => 'global_menu_services',
        'sort' => 400,
        'text' => "ГЕТМЕССАДЖ",
        'title' => "ГЕТМЕССАДЖ",
        'url' => 'webprofy_d7empty.php',
        'items_id' => 'menu_webprofy',
        'items' => array(
            array(
                'text' => "ГЕТМЕССАДЖ",
                'url' => 'webprofy_d7empty.php?lang='.LANGUAGE_ID,
                'title' => "Уруруру",
            ),
        ),
    ),
);
/*
function OnBuildGlobalMenuHandler(&$aGlobalMenu, &$aModuleMenu){
        $haveSection = false;
        $arMenu = array(
            "text" => GetMessage(self::$MLANG."MENU_NAME"),
            "url"  => "webprofy_d7empty.php?lang=".LANGUAGE_ID,
            "more_url"  => array(),
            "title" => GetMessage(self::$MLANG."MENU_TITLE")
        );

        foreach($aModuleMenu as $k => $v){
            if($v["parent_menu"] == "global_menu_settings" && $v["items_id"] == "menu_currency"){
                $haveSection = true;
                $aModuleMenu[$k]["items"][] = $arMenu;
            }
        }
        if(!$haveSection){
            $customMenu = array(
                "parent_menu" => "global_menu_services", // поместим в раздел "Сервис"
                "sort"        => 1000,                    // вес пункта меню
                "text"        => GetMessage(self::$MLANG."SECTION_MENU_NAME"),       // текст пункта меню
                "title"       => GetMessage(self::$MLANG."SECTION_MENU_TITLE"), // текст всплывающей подсказки
                "icon"        => "fav_menu_icon_yellow", // малая иконка
                "page_icon"   => "fav_page_icon_yellow", // большая иконка
                "items_id"    => "menu_webprofy",  // идентификатор ветви
                "items"       => array($arMenu),          // остальные уровни меню сформируем ниже.
            );
            $aModuleMenu[] = $customMenu;
        }
        return true;
    }
*/

return $aMenu;
