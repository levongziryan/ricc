<?

defined('ADMIN_MODULE_NAME') or define('ADMIN_MODULE_NAME', 'webprofy.ricc');
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Ricc {

	const DEAL_CATEGORY_SALE = 0;
	const DEAL_CATEGORY_SUPPORT = 2;
	const DEAL_CATEGORY_SEARCH = 3;
	const PRODUCT_DEPOSIT = 2;
	const PRODUCT_FINAL = 133;
	const DEAL_DEPOSIT_PROPERTY = 'UF_CRM_1522163329920';
	const DEAL_INVOICE_IS_DEPOSIT_PROPERTY = 'UF_CRM_1522239214';
	const DEAL_INVOICE_IS_SEARCH_PROPERTY = 'UF_CRM_1522239191';

	/** Возвращает следующий номер договора */
	public static function getDogovorNumber() {
		return Option::get(ADMIN_MODULE_NAME, "dogovor_number");
	}

	/** Устанавливает следующий номер договора */
	public static function setDogovorNumber($number, $needIncrement = false) {
		if(!isset($number)){
			throw new InvalidArgumentException("Number not difened");
		}
		if($needIncrement){
			$number = intval($number) + 1;
		}
		return Option::set(ADMIN_MODULE_NAME, "dogovor_number", $number);
	}

	/** Увеличивает следующий номер договора на единицу */
	public static function updateDogovorNumber() {
		self::setDogovorNumber(intval(self::getDogovorNumber()) + 1);
	}

	/** Возвращает ID файла с договором по номеру договора
	 * @param docNum - номер договора
	 * @return int ID файла
	 */
	public static function getDogovorFileId($docNum) {
		$docFolderId = Option::get(ADMIN_MODULE_NAME, "dogovor_folder_id");
		CModule::IncludeModule('disk');
		$ob = \Bitrix\Disk\Folder::getList(array(
			'filter' => array(
				'PARENT_ID' => $docFolderId,
				'%NAME' => 'Договор N'.$docNum
			),
			'order' => array(
				'ID' => 'DESC'
			)
		));

		if($ar = $ob->Fetch()){
			return $ar['ID'];
		}
	}

	/** Добавляет комментарий к текущей сделке и прикладывает в него файл с договором
	 * @param fileId - ID файла
	 * @param dealId - ID сделки
	 * @param authorId - ID ответственного
	 */   
	public static function addDogovorComment($fileId, $dealId, $authorID) {
		CModule::IncludeModule('crm');

		$text = "Договор";
		$ownerTypeID = \CCrmOwnerType::Deal;
		$attachments = array("n".$fileId);

		$entryID = \Bitrix\Crm\Timeline\CommentEntry::create(
                array(
                        'TEXT' => $text,
                        'SETTINGS' => array("HAS_FILES" => "Y"),
                        'AUTHOR_ID' => $authorID,
                        'BINDINGS' => array(array('ENTITY_TYPE_ID' => $ownerTypeID, 'ENTITY_ID' => $dealId))
                )
        );
        $saveData = array(
                'ATTACHMENTS' => $attachments,
                'COMMENT' => $text,
                'ENTITY_TYPE_ID' => $ownerTypeID,
                'ENTITY_ID' => $dealId,
        );
        $item = Bitrix\Crm\Timeline\CommentController::getInstance()->onCreate($entryID, $saveData);
	}

	public static function formatPhoneNumber($number) {
		$digitsOnly = preg_replace('/\D/', '', $number);
		$valid = false;
		// Кол-во символов в строке
		switch(strlen($digitsOnly)){
			case 11:
				if($digitsOnly[0] == '8' || $digitsOnly[0] == '7'){
					$digitsOnly[0] = '7';
					$valid = true;
				}
				break;
			case 10:
				$digitsOnly = '7'.$digitsOnly;
				$valid = true;
				break;
		}
		if($valid) {
			return '+'.$digitsOnly;
		} else {
			return $number;
		}
	}

	//RegisterModuleDependences("crm", "OnBeforeCrmLeadAdd", "webprofy.ricc", "Ricc", "onBeforeCrmLeadAdd");

	public static function onBeforeCrmLeadAdd(&$arFields) {
		foreach ($arFields['FM']['PHONE'] as &$phone){
			if($phone['VALUE']){
				$phone['VALUE'] = self::formatPhoneNumber($phone['VALUE']);
			}
		}
		unset($phone);
		//AddMessage2Log('onBeforeCrmLeadAdd -- '.print_r($arFields, true));
	}

	public static function leadPhoneFormat($leadId) {
		CModule::IncludeModule('crm');
			
		$arPhones = array();
		$ob = CCrmFieldMulti::getList(
			array('ID' => 'asc'),
			array('ENTITY_ID' => 'LEAD', 'ELEMENT_ID' => $leadId)
		);
		while($ar = $ob->Fetch()){
			if($ar['TYPE_ID'] == 'PHONE'){
				$arPhones[$ar['ID']] = array(
					'VALUE' => self::formatPhoneNumber($ar['VALUE']),
					'VALUE_TYPE' => $ar['VALUE_TYPE']
				);
			}
		}
		if($arPhones){
			$entity = new CCrmLead();
			$arFields = array(
				'PHONE' => $arPhones
			);
			$CCrmFieldMulti = new CCrmFieldMulti();
			$CCrmFieldMulti->SetFields(CCrmOwnerType::LeadName, $leadId, $arFields);
		}
	}

	/** Возвращает сделки для контакта */
	public static function getDealsForContact($contactId) {
		if(!$contactId){
			throw new InvalidArgumentException('contactId cannot be null');
		}
		
		CModule::IncludeModule('crm');
		$ob = CCrmDeal::GetListEx(array(), array('CONTACT_ID' => $contactId));
		$arDeals = array();
		while($ar = $ob->Fetch()){
			$arDeals[] = array(
				'ID' => $ar['ID'],
				'CURRENCY_ID' => $ar['CURRENCY_ID'],
				'EXCH_RATE' => $ar['EXCH_RATE'],
				'OPPORTUNITY' => $ar['OPPORTUNITY'],
				'OPENED' => $ar['OPENED'],
				'CLOSED' => $ar['CLOSED'],
				'CATEGORY_ID' => $ar['CATEGORY_ID']
			);
		}
		return $arDeals;
	}

	/** Возвращает сделки для контакта */
	public static function getInvoicesForContact($contactId) {
		if(!$contactId){
			throw new InvalidArgumentException('contactId cannot be null');
		}
		
		CModule::IncludeModule('crm');
		$ob = CCrmInvoice::GetList(array(), array('UF_CONTACT_ID' => $contactId));
		$arInvoices = array();
		while($ar = $ob->Fetch()){
			$arInvoices[] = array(
				'ID' => $ar['ID'],
				'PAYED' => $ar['PAYED'],
				'STATUS_ID' => $ar['STATUS_ID'],
				'DATE_PAYED' => $ar['DATE_PAYED'],
				'PAY_VOUCHER_DATE' => $ar['PAY_VOUCHER_DATE'],
				'RESPONSIBLE_ID' => $ar['RESPONSIBLE_ID'],
				'PRICE' => $ar['PRICE'],
				'CURRENCY' => $ar['CURRENCY'],
				'IS_DEPOSIT' => $ar[self::DEAL_INVOICE_IS_DEPOSIT_PROPERTY] ? true : false,
				'IS_SEARCH' => $ar[self::DEAL_INVOICE_IS_SEARCH_PROPERTY] ? true : false,
			);
		}
		return $arInvoices;
	}

	/** Возвращает суммы (общая, получено, осталось получить) для контакта */
	public static function getAmountsForContact($contactId) {
		if(!$contactId){
			throw new InvalidArgumentException('contactId cannot be null');
		}
		$arDeals = self::getDealsForContact($contactId);
		$arResult = array(
			'TOTAL' => 0,
			'PAID' => 0,
			'REMAINING' => 0
		);

		$arInvoices = self::getInvoicesForContact($contactId);

		foreach ($arDeals as $deal) {
			$value = floatval($deal['OPPORTUNITY']) * floatval($deal['EXCH_RATE']);

			switch(intval($deal['CATEGORY_ID'])) {
				case self::DEAL_CATEGORY_SALE:
					// Закрытые сделки в категории Предоплата не считаем — это старая схема.
					// Сейчас вместо закрытия сделка переносится в отдел Сопровождения
					if($deal['CLOSED'] == 'Y'){
						continue;
					}
					// Открытые сделки — считаем
					$arResult['TOTAL'] += $value;
					break;
				case self::DEAL_CATEGORY_SUPPORT:
					// Отдел сопровождения
					$arResult['TOTAL'] += $value;
					break;
				case self::DEAL_CATEGORY_SEARCH:
					// Отдел поиска документов
					$arResult['TOTAL'] += $value;
					break;
			}
		}

		// Считаем все оплаченные счета
		// Они все в Евро по-умолчанию
		foreach ($arInvoices as $key => $invoice) {
			if($invoice['STATUS_ID'] == 'P'){
				$arResult['PAID'] += $invoice['PRICE'];
				if($invoice['CURRENCY'] != 'EUR'){
					AddMessage2Log('Счёт '.$invoice['ID'].' заведён в небазовой валюте ('.$invoice['CURRENCY'].')');
				}
			}
		}

		// Вычисляем остаток
		$arResult['REMAINING'] = $arResult['TOTAL'] - $arResult['PAID'];

		return $arResult;
	}

	public static function getInvoice($dealId, $isDeposit = true, $isSearch = false) {
		CModule::IncludeModule('crm');
		$ob = CCrmDeal::GetListEx(array(), array('ID' => $dealId), false, false, array('*', self::DEAL_DEPOSIT_PROPERTY));
		$arDeal = $ob->Fetch();
		$contactId = $arDeal['CONTACT_ID'];
		if(!$contactId){
			return false;
		}
		$ob = CCrmInvoice::GetList(array(), array(
			'UF_CONTACT_ID' => $contactId,
			self::DEAL_INVOICE_IS_DEPOSIT_PROPERTY => $isDeposit ? 1 : 0,
			self::DEAL_INVOICE_IS_SEARCH_PROPERTY => $isSearch ? 1 : 0
		));
		$arInvoice = $ob->Fetch();
		return $arInvoice;
	}

	public static function getInvoiceForDeposit($dealId) {
		return self::getInvoice($dealId, true, false);
	}

	public static function getInvoiceForFinal($dealId) {
		return self::getInvoice($dealId, false, false);
	}

	public static function getInvoiceForSearchDeposit($dealId) {
		return self::getInvoice($dealId, true, true);
	}

	public static function getInvoiceForSearchFinal($dealId) {
		return self::getInvoice($dealId, false, true);
	}

	/* public static function getPayedAmountForDeal($dealId) {
		CModule::IncludeModule('crm');
		$ob = CCrmDeal::GetListEx(array(), array('ID' => $dealId), false, false, array('*', self::DEAL_DEPOSIT_PROPERTY));
		$arDeal = $ob->Fetch();
		$contactId = $arDeal['CONTACT_ID'];
		if(!$contactId){
			return false;
		}
		$ob = CCrmInvoice::GetList(array(), array('UF_CONTACT_ID' => $contactId, '%TITLE' => 'предоплат'));
	} */

	public static function moveDealToSupport($dealId) {
		if(!$dealId) {
			throw new Exception("Undefined dealId");
		}

		AddMessage2Log('move Deal To Support');

		CModule::IncludeModule('crm');
		global $DB;

		$DB->StartTransaction();
		try
		{
			$error = \CCrmDeal::MoveToCategory($dealId, self::DEAL_CATEGORY_SUPPORT);
			if($error !== \Bitrix\Crm\Category\DealCategoryChangeError::NONE)
			{
				AddMessage2Log('MoveToCategory Error: '.$error);
				throw new Exception("Error Move To Category: ".$error);
			} else {
				$DB->Commit();
			}	
		}
		catch(Exception $e)
		{
			$DB->Rollback();
		}

		AddMessage2Log('Complete: '.$error);

		if($error == \Bitrix\Crm\Category\DealCategoryChangeError::NONE){
			// Ставим на первую стадию и переименовываем

			$arDeal = CCrmDeal::GetByID($dealId);

			$deal = new CCrmDeal();
			$arFields = array(
				'STAGE_ID' => 'C'.self::DEAL_CATEGORY_SUPPORT.':NEW',
				'TITLE' => 'Сопровождение '.$arDeal['CONTACT_FULL_NAME']
			);
			AddMessage2Log('Deal update: '.$dealId);
			$res = $deal->Update($dealId, $arFields);
			//Bitrix\Crm\Automation\Factory::runOnStatusChanged(CCrmOwnerType::Deal, $dealId);
		} else {
			return $error;
		}
	}

	public static function payInvoice($dealId, $isDeposit = true, $isSearch = false) {
		global $DB;
		CModule::IncludeModule('crm');
		$arInvoice = self::getInvoice($dealId, $isDeposit, isSearch);
		if($arInvoice){
			$invoiceId = $arInvoice['ID'];
			// Уже оплачен?
			if($arInvoice['STATUS_ID'] == 'P'){
				return $invoiceId;
			}
		} else {
			if($isDeposit && !$isSearch){
				$invoiceId = self::createInvoiceForDeposit($dealId);
			} elseif(!$isDeposit && !$isSearch){
				$invoiceId = self::createInvoiceForFinal($dealId);
			}
		}

		if($invoiceId === false){
			// Счёт на нулевую сумму не создаётся, оплачивать нечего
			return false;
		}

		if(!$invoiceId){
			throw new Exception('Невозможно создать счёт для сделки '.$dealId);
		}
		$invoice = new CCrmInvoice();
		$paymentDate = date($DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")), time());
		return $invoice->SetStatus($invoiceId, 'P', array(
			'STATE_SUCCESS' => true,
			'STATE_FAILED' => false,
			'PAY_VOUCHER_NUM' => false,
			'PAY_VOUCHER_DATE' => $paymentDate,
			'DATE_MARKED' => $paymentDate,
			'REASON_MARKED' => ''
		), array('SYNCHRONIZE_LIVE_FEED' => 1));
		//return $invoice->Update($invoiceId, array('STATUS_ID' => 'P'));
	}

	public static function payInvoiceForDeposit($dealId) {
		self::payInvoice($dealId, true, false);
	}
	public static function payInvoiceForFinal($dealId) {
		self::payInvoice($dealId, false, false);
	}

	// RegisterModuleDependences("crm", "OnAfterCrmDealUpdate", "webprofy.ricc", "Ricc", "onAfterCrmDealUpdate");

	public static function onAfterCrmDealUpdate(&$arFields) {
		if($arFields['STAGE_ID'] == 'WON'){

			AddMessage2Log('DEPOSIT DEAL WON -- '.print_r($arFields, true));

			// Сделка закрыта в отделе закрытия
			$dealId = $arFields['ID'];

			// Оплачиваем счёт
			self::payInvoiceForDeposit($dealId);

			// Перемещаем в направление "Сопровождение"
			self::moveDealToSupport($dealId);
		} elseif($arFields['STAGE_ID'] == 'C2:WON'){
			
			AddMessage2Log('SUPPORT DEAL WON -- '.print_r($arFields, true));

			// Сделка закрыта в отделе сопровождения
			$dealId = $arFields['ID'];

			// Оплачиваем счёт
			self::payInvoiceForFinal($dealId);
		}
	}

	public static function createInvoiceForFinal($dealId, $needCheck = true) {
		return self::createInvoice(array(
			'DEAL_ID' => $dealId,
			'CATEGORY_ID' => 2,
			'IS_DEPOSIT' => false,
			'IS_SEARCH' => false,
			'NEED_CHECK' => $needCheck
		));
	}

	public static function createInvoiceForDeposit($dealId, $needCheck = true) {
		return self::createInvoice(array(
			'DEAL_ID' => $dealId,
			'CATEGORY_ID' => 0,
			'IS_DEPOSIT' => true,
			'IS_SEARCH' => false,
			'NEED_CHECK' => $needCheck
		));
	}

	public static function createInvoice($arFields) {
		CModule::IncludeModule('crm');

		if(!$arFields['DEAL_ID']){
			throw new InvalidArgumentException('DEAL_ID must be defined in arFields');
		}
		$dealId = $arFields['DEAL_ID'];

		if($arFields['NEED_CHECK']){
			$arInvoice = self::getInvoice($arFields['DEAL_ID'], $arFields['IS_DEPOSIT'], $arFields['IS_SEARCH']);
			if($arInvoice){
				return $arInvoice['ID'];
			}
		}

		$ob = CCrmDeal::GetListEx(array(), array('ID' => $dealId), false, false, array('*', self::DEAL_DEPOSIT_PROPERTY));
		$arDeal = $ob->Fetch();

		// Если указана категория, проверяем, что сделка находится именно в ней
		if(isset($arFields['CATEGORY_ID']) && $arDeal['CATEGORY_ID'] != $arFields['CATEGORY_ID']){
			dump($arDeal);
			dump($arFields);
			dump('Wrong Category');
			return false;
		}

		$arSign = array(
			'EUR' => '€',
			'USD' => '$'
		);

		$deposit = intval($arDeal[self::DEAL_DEPOSIT_PROPERTY]);
		$depositPrintable = $deposit.$arSign[$arDeal['CURRENCY_ID']];
		$depositConverted = $deposit * $arDeal['EXCH_RATE'];


		$opportunity = $arDeal['OPPORTUNITY'] - $deposit;
		$opportunityPrintable = $opportunity.$arSign[$arDeal['CURRENCY_ID']];
		$opportunityConverted = $opportunity * $arDeal['EXCH_RATE'];

		if($arFields['IS_DEPOSIT']){
			$amount = $depositConverted;
			$invoiceTitle = $arDeal['TITLE'].' - Счет на предоплату ('.$depositPrintable.')';
		} else {
			$amount = $opportunityConverted;
			$invoiceTitle = $arDeal['TITLE'].' - Счет на финальную оплату ('.$opportunityPrintable.')';
		}

		if($amount == 0){
			// Не выставляем счёт на нулевую сумму
			return false;
		}

		$billDate = date("d.m.Y");
		$arContact = CCrmContact::GetByID($arDeal['CONTACT_ID']);

		$ob = CCrmFieldMulti::getList(
			array('ID' => 'asc'),
			array('ENTITY_ID' => 'CONTACT', 'ELEMENT_ID' => $arDeal['CONTACT_ID'])
		);
		$arPhones = array();
		$arEmails = array();
		while($ar = $ob->Fetch()){
			if($ar['TYPE_ID'] == 'PHONE'){
				$arPhones[] = $ar['VALUE'];
			} elseif($ar['TYPE_ID'] == 'EMAIL'){
				$arEmails[] = $ar['VALUE'];
			}
		}

		$phone = $arPhones ? $arPhones[0] : '';
		$email = $arEmails ? $arEmails[0] : '';

		$arRecalculated = false;

		$product = array(
			'ID' => 0,
			'QUANTITY' => 1.0,
			'PRICE' => $amount,
			'VAT_INCLUDED' => 'N',
			'VAT_RATE' => 0.0,
			'DISCOUNT_PRICE' => 0.0,
			'MEASURE_CODE' => 796,
			'MEASURE_NAME' => 'шт',
			'CUSTOMIZED' => 'Y',
			'SORT' => 10,
		);
		if($arFields['IS_DEPOSIT']){
			$product['PRODUCT_ID'] = self::PRODUCT_DEPOSIT;
			$product['PRODUCT_NAME'] = 'Предоплата';
		} else {
			$product['PRODUCT_ID'] = self::PRODUCT_FINAL;
			$product['PRODUCT_NAME'] = 'Финальная оплата';
		}

		$responsibleId = $arDeal['ASSIGNED_BY_ID'];

		// Task 18053127: 4. Выставлять счёт на финальную предоплату не на менеджера сопровождения, а не менеджера продажи  
		if(!$arFields['IS_DEPOSIT'] && !$arFields['IS_SEARCH']){
			$responsibleId = $arContact['ASSIGNED_BY_ID'];
		}

		$invoice = new CCrmInvoice();

		$orderId = $invoice->Add(array(
			'ORDER_TOPIC' => $invoiceTitle,
			'STATUS_ID' => 'N',
			'DATE_BILL' => $billDate,
			'PAY_VOUCHER_DATE' => '',
			'DATE_PAY_BEFORE' => '',
			'RESPONSIBLE_ID' => $responsibleId,
			'COMMENTS' => '',
			'USER_DESCRIPTION' => '',
			'UF_QUOTE_ID' => 0,
			'UF_DEAL_ID' => $dealId,
			'UF_COMPANY_ID' => $arDeal['COMPANY_ID'],
			'UF_CONTACT_ID' => $arDeal['CONTACT_ID'],
			'UF_MYCOMPANY_ID' => 0,
			'PRODUCT_ROWS' => array($product),
			'PERSON_TYPE_ID' => '3',
			'PAY_SYSTEM_ID' => 3,
			'INVOICE_PROPERTIES' => array(
				1 => $arDeal['CONTACT_FULL_NAME'],
				3 => $email,
				5 => $phone,
				7 => '',
				11 => '',
				9 => '',
				13 => '',
			),
			'PR_INVOICE_1' => $arDeal['CONTACT_FULL_NAME'],
			'PR_INVOICE_3' => $email,
			'PR_INVOICE_5' => $phone,
			'PR_INVOICE_7' => '',
			'PR_INVOICE_11' => '',
			'PR_INVOICE_9' => '',
			'PR_INVOICE_13' => '',
			self::DEAL_INVOICE_IS_DEPOSIT_PROPERTY => $arFields['IS_DEPOSIT'] ? 1 : 0,
			self::DEAL_INVOICE_IS_SEARCH_PROPERTY => $arFields['IS_SEARCH'] ? 1 : 0
		),
		$arRecalculated,
		's1',
		array (
			'REGISTER_SONET_EVENT' => true,
			'UPDATE_SEARCH' => true,
		));

		return $orderId;
	}

	public static function birthdayNotificationAgent(){
		CModule::IncludeModule('crm');
		CModule::IncludeModule('im');
		global $DB;
		AddMessage2Log("birthdayNotificationAgent запустился");
		$cakeIcon = "https://emojipedia-us.s3.amazonaws.com/thumbs/120/samsung/128/birthday-cake_1f382.png";
		$ob = CCrmContact::GetListEx(array(), array('!BIRTHDATE' => false, 'CHECK_PERMISSIONS' => 'N'));
		$today = date('d.m');
		while($ar = $ob->Fetch()){
			$date = substr($ar['BIRTHDATE'],0,5);
			if($today == $date){
				$id = $ar['ID'];
				$name = $ar['FULL_NAME'];
				$day = strtolower(FormatDate("d F"));
				$url = "/crm/contact/details/".$id."/";
				$arMessageFields = array(
					"TO_USER_ID" => $ar['ASSIGNED_BY_ID'],
				    "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,
					"NOTIFY_MESSAGE" => "<img src='$cakeIcon' alt='Birthday Cake' width='108' height='108'>[br][b]${day}[/b][br]Сегодня [b][url=$url]${name}[/url][/b] празднует [b]День рождения[/b]![br]Не забудьте поздравить!",
				);
				$res = CIMNotify::Add($arMessageFields);
				AddMessage2Log("($res) Уведомление о дне рождения -- ".print_r($arMessageFields, true));
			}
		}
		return "Ricc::birthdayNotificationAgent();";
	}
}

