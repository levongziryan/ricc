<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Loader;
use Bitrix\Main\EventManager;

Loader::registerAutoLoadClasses('webprofy.ricc', array(
    // no thanks, bitrix, we better will use psr-4 than your class names convention
    'Ricc' => 'classes/general/Ricc.php',
));
