<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

$MESS['RICC_DOGOVOR_NUMBER'] = "Следующий номер договора";
$MESS['RICC_DOGOVOR_FOLDER_ID'] = "ID папки для сохранения  договоров";
$MESS['RICC_DOGOVOR_TEMPLATE_ID'] = "ID файла шаблона договора";
$MESS['RICC_OPTIONS_RESTORED'] = "Восстановлены настройки по умолчанию";
$MESS['RICC_OPTIONS_SAVED'] = "Настройки сохранены";
$MESS['RICC_INVALID_VALUE'] = "Введено неверное значение";
