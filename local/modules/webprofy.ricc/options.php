<?
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();
defined('ADMIN_MODULE_NAME') or define('ADMIN_MODULE_NAME', 'webprofy.ricc');

use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;

if (!$USER->isAdmin()) {
	$APPLICATION->authForm('Nope');
}

$app = Application::getInstance();
$context = $app->getContext();
$request = $context->getRequest();

Loc::loadMessages($context->getServer()->getDocumentRoot()."/bitrix/modules/main/options.php");
Loc::loadMessages(__FILE__);

$tabControl = new CAdminTabControl("tabControl", array(
	array(
		"DIV" => "edit1",
		"TAB" => Loc::getMessage("MAIN_TAB_SET"),
		"TITLE" => Loc::getMessage("MAIN_TAB_TITLE_SET"),
	),
));

if ((!empty($save) || !empty($restore)) && $request->isPost() && check_bitrix_sessid()) {
	if (!empty($restore)) {
		Option::delete(ADMIN_MODULE_NAME);
		CAdminMessage::showMessage(array(
			"MESSAGE" => Loc::getMessage("RICC_OPTIONS_RESTORED"),
			"TYPE" => "OK",
		));
	} elseif ($request->getPost('dogovor_number') && ($request->getPost('dogovor_number') > 0)) {
		Option::set(
			ADMIN_MODULE_NAME,
			"dogovor_number",
			$request->getPost('dogovor_number')
		);
		Option::set(
			ADMIN_MODULE_NAME,
			"dogovor_folder_id",
			$request->getPost('dogovor_folder_id')
		);
		Option::set(
			ADMIN_MODULE_NAME,
			"dogovor_template_id",
			$request->getPost('dogovor_template_id')
		);
		CAdminMessage::showMessage(array(
			"MESSAGE" => Loc::getMessage("RICC_OPTIONS_SAVED"),
			"TYPE" => "OK",
		));
	} else {
		CAdminMessage::showMessage(Loc::getMessage("RICC_INVALID_VALUE"));
	}
}

$tabControl->begin();
?>

<form method="post" action="<?=sprintf('%s?mid=%s&lang=%s', $request->getRequestedPage(), urlencode($mid), LANGUAGE_ID)?>">
	<?
	echo bitrix_sessid_post();
	$tabControl->beginNextTab();
	?>
	<tr>
		<td width="40%">
			<label for="max_image_size"><?=Loc::getMessage("RICC_DOGOVOR_NUMBER") ?>:</label></td>
		<td width="60%">
			<input type="text"
				size="10"
				maxlength="10"
				name="dogovor_number"
				value="<?= Option::get(ADMIN_MODULE_NAME, "dogovor_number", 1002); ?>"
				/>
		</td>
	</tr>
	<tr>
		<td width="40%">
			<label for="max_image_size"><?=Loc::getMessage("RICC_DOGOVOR_FOLDER_ID") ?>:</label></td>
		<td width="60%">
			<input type="text"
				size="10"
				maxlength="10"
				name="dogovor_folder_id"
				value="<?= Option::get(ADMIN_MODULE_NAME, "dogovor_folder_id", 540); ?>"
				/>
		</td>
	</tr>
	<tr>
		<td width="40%">
			<label for="max_image_size"><?=Loc::getMessage("RICC_DOGOVOR_TEMPLATE_ID") ?>:</label></td>
		<td width="60%">
			<input type="text"
				size="10"
				maxlength="10"
				name="dogovor_template_id"
				value="<?= Option::get(ADMIN_MODULE_NAME, "dogovor_template_id", 1507); ?>"
				/>
		</td>
	</tr>

	<?
	$tabControl->buttons();
	?>
	<input type="submit"
		   name="save"
		   value="<?=Loc::getMessage("MAIN_SAVE") ?>"
		   title="<?=Loc::getMessage("MAIN_OPT_SAVE_TITLE") ?>"
		   class="adm-btn-save"
		   />
	<input type="submit"
		   name="restore"
		   title="<?=Loc::getMessage("MAIN_HINT_RESTORE_DEFAULTS") ?>"
		   onclick="return confirm('<?= AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING")) ?>')"
		   value="<?=Loc::getMessage("MAIN_RESTORE_DEFAULTS") ?>"
		   />
	<?
	$tabControl->end();
	?>
</form>
