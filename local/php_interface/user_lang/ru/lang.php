<?
$MESS["/bitrix/components/bitrix/crm.control_panel/lang/ru/component.php"]["CRM_CTRL_PANEL_ITEM_CONTACT"] = "Клиенты";
$MESS["/bitrix/components/bitrix/crm.contact.list/lang/ru/component.php"]["CRM_CONTACT_NAV_TITLE_LIST"] = "Список клиентов";
$MESS["/bitrix/components/bitrix/crm.contact.list/lang/ru/component.php"]["CRM_CONTACT_NAV_TITLE_LIST_SHORT"] = "Клиенты";
$MESS["/bitrix/components/bitrix/crm.contact.list/lang/ru/component.php"]["CRM_COLUMN_CONTACT"] = "Клиент";
$MESS["/bitrix/components/bitrix/crm.contact.list/lang/ru/component.php"]["CRM_COLUMN_TYPE"] = "Тип клиента";
$MESS["/bitrix/components/bitrix/crm.contact.list/lang/ru/component.php"]["CRM_PRESET_MY"] = "Мои клиенты";
$MESS["/bitrix/components/bitrix/crm.contact.list/templates/.default/lang/ru/template.php"]["CRM_CONTACT_LIST_ADD"] = "Добавить клиента";
$MESS["/bitrix/modules/intranet/public/crm/contact/.section.php"]["CRM_SECTION_CONTACT"] = "Клиенты";
