<?

function dump() {
	$vars = func_get_args();
	foreach ($vars as $var) {
		echo "<pre>";
		print_r($var);
		echo "</pre>";
	}
}

function cdump() {
	$vars = func_get_args();
	foreach ($vars as $var) {
		echo "<script>";
		echo "console.log(".json_encode($var).");";
		echo "</script>";
	}
}

AddEventHandler("crm", "OnBeforeCrmLeadAdd", "OnBeforeCrmLeadAddHandler");//обновление ответственного лида
function OnBeforeCrmLeadAddHandler(&$arFields){
   if($arFields['UF_CRM_1538511475']>0){
       $arFields['UF_CRM_1538510036']=$arFields['UF_CRM_1538511475'];
   }
    /* if(!$arFields['FM']['PHONE']['n0']['VALUE'] and !$arFields['FM']['EMAIL']['n0']['VALUE']){
        return false;
    } */
}

AddEventHandler("crm", "OnBeforeCrmDealUpdate", "OnBeforeCrmDealUpdateHandler");
function OnBeforeCrmDealUpdateHandler(&$arFields){
    $arFieldsOld=CCrmDeal::GetByID($arFields['ID']);
    
    $file = file_get_contents('https://www.cbr-xml-daily.ru/daily_json.js');
    $json=json_decode($file, true);
    $curs=$json['Valute']['EUR']['Value'];
    $newsum=$arFieldsOld['OPPORTUNITY']*$curs;
    $arFields['UF_CRM_1531913334']=$newsum;    
}




function sendContactsToijew(){
    $CCrmLead = new CCrmLead();
    $res=CCrmLead::GetList( array('ID' => 'DESC'), array("SOURCE_ID"=>"CALL",'SOURCE_DESCRIPTION'=>"74954813214"),array(), 100);
    while($arFields = $res->GetNext())
    { 
        $arrUpdate=array("SOURCE_ID"=>18);
        $CCrmLead->Update($arFields['ID'], $arrUpdate, false);        
    }
    
    if(CModule::IncludeModule("crm")):
    $CCrmContact = new CCrmContact();
    $res=CCrmContact::GetList( array('ID' => 'DESC'), array("UF_CRM_1529844429"=>false,'TYPE_ID'=>array(4,6)),array(), false);
    while($arFields = $res->GetNext())
    {
         
         
        $dbResMultiFields = CCrmFieldMulti::GetList(array(),array('ENTITY_ID' => 'CONTACT', 'ELEMENT_ID' => $arFields['ID']));
        while($arMultiFields = $dbResMultiFields->Fetch())
        {
            $arFields['FM'][]=$arMultiFields;
        }
        foreach ($arFields['FM'] as $value){
            if($value[TYPE_ID]=="PHONE"){
                $arFields['PHONE']=$value[VALUE];
            }elseif($value[TYPE_ID]=="EMAIL"){
                $arFields['EMAIL']=$value[VALUE];
            }
        }         
        $queryUrl = 'https://ijew.bitrix24.ru/rest/1/g2ssx75n8pcd128s/crm.contact.add';
         
        $arrFileds=array(
            'fields' => array(
                //"TITLE" => $arFields["TITLE"],
                "NAME" => $arFields["NAME"],
                "LAST_NAME" => $arFields["LAST_NAME"],
                //"STATUS_ID" => "NEW",
                "OPENED" => "Y",
                "SOURCE_ID"=>1,
                "ASSIGNED_BY_ID" => 1,
                "COMMENTS"=> $arFields["COMMENTS"],
                //"PHONE" => array(array("VALUE" => $arFields['EMAIL'], "VALUE_TYPE" => "WORK" )),
                //"EMAIL" => array(array("VALUE" => $arFields['PHONE'], "VALUE_TYPE" => "WORK" )),
            )
        );
        if($arFields['EMAIL']){
            $arrFileds['fields']["EMAIL"]=array(array("VALUE" => $arFields['EMAIL'], "VALUE_TYPE" => "WORK" ));
        }
         
        if($arFields['PHONE']){
            $arrFileds['fields']["PHONE"]=array(array("VALUE" => $arFields['PHONE'], "VALUE_TYPE" => "WORK" ));
        }
         
        $queryData = http_build_query($arrFileds);
    
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $queryUrl,
            CURLOPT_POSTFIELDS => $queryData,
        ));
         
        $result = curl_exec($curl);
        curl_close($curl);
        $result = json_decode($result, 1);
    
        if($result[result]>0){
            $arrUpdate=array("UF_CRM_1529844429"=>$result[result]);
            $CCrmContact->Update($arFields['ID'], $arrUpdate, false);
        }
    
    }
    endif;
    
    
    $CCrmLead = new CCrmLead();
    $res=CCrmLead::GetList( array('ID' => 'DESC'), array("!UF_CRM_1528206796"=>false,"UF_CRM_1376771107"=>false,),array(), 100);
    while($arFields = $res->GetNext())
    {
        unset($city);
        unset($roistat);
        
        if($arFields[UF_CRM_1528206796]>0){
            $roistat=file_get_contents("http://cloud.roistat.com/api/project/1.0/visits/".$arFields[UF_CRM_1528206796]."?secret=01345d7a3ef1a398b0c589c0bb3c4dc2&project=82000");
            $roistat=json_decode($roistat, true);
            $city=file_get_contents("http://api.sypexgeo.net/json/".$roistat[data][ip]);
            $city=json_decode($city, true);
             
            if(!$city[city][name_ru]){
                $city[city][name_ru]=$city[country][name_ru];
            }
        }
        
        
        if(!$city[city][name_ru]){
            $city[city][name_ru]="Не определено";
        }
        if($city[city][name_ru]){
            $arrUpdate=array("UF_CRM_1376771107"=>$city[city][name_ru]);
            $CCrmLead->Update($arFields['ID'], $arrUpdate, false);
        }
    }
    
    return "sendContactsToijew();";
}