<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/intranet/public/index.php");
$APPLICATION->SetTitle(GetMessage("CP_WELCOME"));
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
?>


<?php 



$CCrmContact = new CCrmContact();
$res=CCrmContact::GetList( array('ID' => 'DESC'), array("UF_CRM_1529844429"=>false,'TYPE_ID'=>array(4,6)),array(), false);
while($arFields = $res->GetNext())
{  
   
   
   $dbResMultiFields = CCrmFieldMulti::GetList(array(),array('ENTITY_ID' => 'CONTACT', 'ELEMENT_ID' => $arFields['ID']));
   while($arMultiFields = $dbResMultiFields->Fetch())
   {
       $arFields['FM'][]=$arMultiFields;      
   }   
   foreach ($arFields['FM'] as $value){
       if($value[TYPE_ID]=="PHONE"){
           $arFields['PHONE']=$value[VALUE];
       }elseif($value[TYPE_ID]=="EMAIL"){
           $arFields['EMAIL']=$value[VALUE];
       }       
   }  
  
   
   $queryUrl = 'https://ijew.bitrix24.ru/rest/1/g2ssx75n8pcd128s/crm.contact.add';
   
   $arrFileds=array(
       'fields' => array(
           //"TITLE" => $arFields["TITLE"],
           "NAME" => $arFields["NAME"],
           "LAST_NAME" => $arFields["LAST_NAME"],
           //"STATUS_ID" => "NEW",
           "OPENED" => "Y",
           "SOURCE_ID"=>1,
           "ASSIGNED_BY_ID" => 1,
           "COMMENTS"=> $arFields["COMMENTS"],
           //"PHONE" => array(array("VALUE" => $arFields['EMAIL'], "VALUE_TYPE" => "WORK" )),
           //"EMAIL" => array(array("VALUE" => $arFields['PHONE'], "VALUE_TYPE" => "WORK" )),
       )
   );
   if($arFields['EMAIL']){
       $arrFileds['fields']["EMAIL"]=array(array("VALUE" => $arFields['EMAIL'], "VALUE_TYPE" => "WORK" ));
   }
   
   if($arFields['PHONE']){
       $arrFileds['fields']["PHONE"]=array(array("VALUE" => $arFields['PHONE'], "VALUE_TYPE" => "WORK" ));
   }
   
   $queryData = http_build_query($arrFileds);  
   $curl = curl_init();
   curl_setopt_array($curl, array(
       CURLOPT_SSL_VERIFYPEER => 0,
       CURLOPT_POST => 1,
       CURLOPT_HEADER => 0,
       CURLOPT_RETURNTRANSFER => 1,
       CURLOPT_URL => $queryUrl,
       CURLOPT_POSTFIELDS => $queryData,
   ));
   
   $result = curl_exec($curl);
   curl_close($curl);   
   $result = json_decode($result, 1);
  
   if($result[result]>0){
       $arrUpdate=array("UF_CRM_1529844429"=>$result[result]);
       $CCrmContact->Update($arFields['ID'], $arrUpdate, false);
   }
  
}
?>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>