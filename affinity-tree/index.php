<?
require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

?>

<?php 

if($_REQUEST["iframe"]=="y"){
   $GLOBALS['APPLICATION']->RestartBuffer();
}

CJSCore::Init(array("jquery"));
\Bitrix\Main\Loader::includeModule('crm');

if($_REQUEST['ID'] and $_REQUEST['submit']=="y"){   
    $ser=serialize($_REQUEST['client']);    
    $arFieldsNew=array("UF_CRM_1532778757"=>$ser);    
    
    $crm= new CCrmContact;
    $crm->Update($_REQUEST['ID'], $arFieldsNew);
}



if(!$_REQUEST['ID']){
    $_REQUEST['ID']=2893;
}
$arResult["ID"]=$_REQUEST['ID'];

$res=CCrmContact::GetList(array('DATE_CREATE' => 'DESC'),array("ID"=>$arResult["ID"]), array(), 1);
if($arFields = $res->fetch())
{
    $arResult["contact"]=$arFields;
    $arResult["contact"]['client']=unserialize($arResult["contact"]['UF_CRM_1532778757']);
}









?>







<link rel="stylesheet" type="text/css" href="/affinity-tree/default.css?<?=rand()?>" />
<link rel="stylesheet" type="text/css" href="/affinity-tree/tree.css?<?=rand()?>" />


<form method="post" class="main-form" action="/affinity-tree/">
<input type="hidden" name="ID" value="<?=$arResult['ID']?>">
<input type="hidden" name="submit" value="y">
<div class="container main-container">


	<div class="main-grid-ear main-grid-ear-left show"></div>
	<div class="main-grid-ear main-grid-ear-right"></div>

	
	<h1 style="text-align: center; margin-right: 130px;">Дерево родства</h1>
	<div class="tree">
		<ul>
			<li><a href="javascript:void(0);">Клиент<br> <img src="/affinity-tree/man.png" width="50" height="50"
					alt="lorem" />

					<table>
						<tr>
							<td>ФИО</td>
							<td><input type="text" name="client[FIO]" value="<?=$arResult["contact"]['client']['FIO']?>"></td>
						</tr>
						<tr>
							<td>СОР</td>
							<td><input type="text" name="client[COR]" value="<?=$arResult["contact"]['client']['COR']?>"></td>
						</tr>


						

						<tr>
							<td>Свидетельство о браке</td>
							<td><input type="text" name="client[BRAK]" value="<?=$arResult["contact"]['client']['BRAK']?>"></td>
						</tr>


					</table>


			</a>



				<ul>
					<li><a href="javascript:void(0);">Отец<br> <img src="/affinity-tree/man.png" width="50" height="50"
							alt="lorem" />
							<table>
								<tr>
									<td>ФИО</td>
									<td><input type="text" name="client[father][FIO]" value="<?=$arResult["contact"]['client'][father][FIO]?>"></td>
								</tr>
								<tr>
									<td>СОР</td>
									<td><input type="text" name="client[father][COR]" value="<?=$arResult["contact"]['client'][father][COR]?>"></td>
								</tr>
								<tr>
									<td>Военный билет</td>
									<td><input type="text" name="client[father][VOEN]" value="<?=$arResult["contact"]['client'][father][VOEN]?>"></td>
								</tr>

								<tr>
									<td>Эвакуация</td>
									<td><input type="checkbox" name="client[father][EVA]" value="<?=$arResult["contact"]['client'][father][EVA]?>"></td>
								</tr>


								
								<tr>
									<td>Свидетельство о браке</td>
									<td><input type="text" name="client[father][BRAK]" value="<?=$arResult["contact"]['client'][father][BRAK]?>"></td>
								</tr>


							</table>


					</a>
						<ul>
							<li><a href="javascript:void(0);">Дедушка<br> <img src="/affinity-tree/man.png" width="50"
									height="50" alt="lorem" />

									<table>
										<tr>
											<td>ФИО</td>
											<td><input type="text" name="client[father][gran_father][FIO]" value="<?=$arResult["contact"]['client'][father][gran_father][FIO]?>"></td>
										</tr>
										<tr>
											<td>СОР</td>
											<td><input type="text" name="client[father][gran_father][COR]" value="<?=$arResult["contact"]['client'][father][gran_father][COR]?>"></td>
										</tr>
										<tr>
											<td>Военный билет</td>
											<td><input type="text" name="client[father][gran_father][VOEN]" value="<?=$arResult["contact"]['client'][father][gran_father][VOEN]?>"></td>
										</tr>

										<tr>
											<td>Эвакуация</td>
											<td><input type="checkbox" name="client[father][gran_father][EVA]" value="<?=$arResult["contact"]['client'][father][gran_father][EVA]?>"></td>
										</tr>


										
										<tr>
											<td>Свидетельство о браке</td>
											<td><input type="text" name="client[father][gran_father][BRAK]" value="<?=$arResult["contact"]['client'][father][gran_father][BRAK]?>"></td>
										</tr>


									</table>

							</a>
							
    							
    						<ul>
        							<li>
        							<a href="javascript:void(0);">
        							
        								<div>Дополнительные документы</div>
        							<div><textarea name="client[DOP]"><?=$arResult["contact"]['client'][DOP]?></textarea></div>
        							
        							
        							</a>    							
    							</li>
    							</ul>	
    							
    							
							
							</li>
							<li><a href="javascript:void(0);">Бабушка<br> <img src="/affinity-tree/woman.png" width="50"
									height="50" alt="lorem" />

									<table>
										<tr>
											<td>ФИО</td>
											<td><input type="text" name="client[father][gran_mother][FIO]" value="<?=$arResult["contact"]['client'][father][gran_mother][FIO]?>"></td>
										</tr>
										<tr>
											<td>СОР</td>
											<td><input type="text" name="client[father][gran_mother][COR]" value="<?=$arResult["contact"]['client'][father][gran_mother][COR]?>"></td>
										</tr>
										<tr>
											<td>Военный билет</td>
											<td><input type="text" name="client[father][gran_mother][VOEN]" value="<?=$arResult["contact"]['client'][father][gran_mother][VOEN]?>"></td>
										</tr>

										<tr>
											<td>Эвакуация</td>
											<td><input type="checkbox" name="client[father][gran_mother][EVA]" value="<?=$arResult["contact"]['client'][father][gran_mother][EVA]?>"></td>
										</tr>

										

										<tr>
											<td>Свидетельство о браке</td>
											<td>-</td>
										</tr>





									</table>

							</a></li>

						</ul></li>

					<li><a href="javascript:void(0);">Мать<br> <img src="/affinity-tree/woman.png" width="50"
							height="50" alt="lorem" />
							<table>
								<tr>
									<td>ФИО</td>
									<td><input type="text" name="client[mother][FIO]" value="<?=$arResult["contact"]['client'][mother][FIO]?>"></td>
								</tr>
								<tr>
									<td>СОР</td>
									<td><input type="text" name="client[mother][COR]" value="<?=$arResult["contact"]['client'][mother][COR]?>"></td>
								</tr>
								<tr>
									<td>Военный билет</td>
									<td><input type="text" name="client[mother][VOEN]" value="<?=$arResult["contact"]['client'][mother][VOEN]?>"></td>
								</tr>
								<tr>
									<td>Эвакуация</td>
									<td><input type="checkbox" name="client[mother][EVA]" value="<?=$arResult["contact"]['client'][mother][EVA]?>"></td>
								</tr>

								
								<tr>
									<td>Свидетельство о браке</td>
									<td>-</td>
								</tr>




							</table>


					</a>
						<ul>
							<li><a href="javascript:void(0);">Дедушка<br> <img src="/affinity-tree/man.png" width="50"
									height="50" alt="lorem" />

									<table>
										<tr>
											<td>ФИО</td>
											<td><input type="text" name="client[mother][gran_father][FIO]" value="<?=$arResult["contact"]['client'][mother][gran_father][FIO]?>"></td>
										</tr>
										<tr>
											<td>СОР</td>
											<td><input type="text" name="client[mother][gran_father][COR]" value="<?=$arResult["contact"]['client'][mother][gran_father][COR]?>"></td>
										</tr>
										<tr>
											<td>Военный билет</td>
											<td><input type="text" name="client[mother][gran_father][VOEN]" value="<?=$arResult["contact"]['client'][mother][gran_father][VOEN]?>"></td>
										</tr>

										<tr>
											<td>Эвакуация</td>
											<td><input type="checkbox" name="client[mother][gran_father][EVA]" value="<?=$arResult["contact"]['client'][mother][gran_father][EVA]?>"></td>
										</tr>

									

										<tr>
											<td>Свидетельство о браке</td>
											<td><input type="text" name="client[mother][gran_father][BRAK]" value="<?=$arResult["contact"]['client'][mother][gran_father][BRAK]?>"></td>
										</tr>



									</table>

							</a>
							
								<ul>
        							<li>
        							<a href="javascript:void(0);">
        							
        							<div>Список рекомендаций</div>
        							<div><textarea name="client[SPI]"><?=$arResult["contact"]['client'][SPI]?></textarea></div>
        							
        							
        							</a>    							
    							</li>
    							</ul>
							
							
							</li>
							<li><a href="javascript:void(0);">Бабушка<br> <img src="/affinity-tree/woman.png" width="50"
									height="50" alt="lorem" />

									<table>
										<tr>
											<td>ФИО</td>
											<td><input type="text" name="client[mother][gran_mother][FIO]"  value="<?=$arResult["contact"]['client'][mother][gran_mother][FIO]?>"></td>
										</tr>
										<tr>
											<td>СОР</td>
											<td><input type="text" name="client[mother][gran_mother][COR]" value="<?=$arResult["contact"]['client'][mother][gran_mother][COR]?>"></td>
										</tr>
										<tr>
											<td>Военный билет</td>
											<td><input type="text" name="client[mother][gran_mother][VOEN]" value="<?=$arResult["contact"]['client'][mother][gran_mother][VOEN]?>"></td>
										</tr>

										<tr>
											<td>Эвакуация</td>
											<td><input type="checkbox" name="client[mother][gran_mother][EVA]" value="<?=$arResult["contact"]['client'][mother][gran_mother][EVA]?>"></td>
										</tr>


										
										<tr>
											<td>Свидетельство о браке</td>
											<td>-</td>
										</tr>



									</table>

							</a></li>

						</ul></li>
				</ul></li>

			<li><a href="javascript:void(0);">Супруг(а)<br> <img src="/affinity-tree/woman.png" width="50"
					height="50" alt="lorem" />

					<table>
						<tr>
							<td>ФИО</td>
							<td><input type="text" name="client[SUP][FIO]" value="<?=$arResult["contact"]['client'][SUP][FIO]?>"></td>
						</tr>
						<tr>
							<td>СОР</td>
							<td><input type="text" name="client[SUP][COR]" value="<?=$arResult["contact"]['client'][SUP][COR]?>"></td>
						</tr>


						

						<tr>
							<td>Свидетельство о браке</td>
							<td>-</td>
						</tr>

					</table>


			</a>
				<ul>
					<li><a href="javascript:void(0);">Дети<br> <img src="/affinity-tree/man.png" width="50" height="50"
							alt="lorem" />

							<table>
								<tr>
									<td>ФИО</td>
									<td><input type="text" name="client[CHILD][1][FIO]" value="<?=$arResult["contact"]['client'][CHILD][1][FIO]?>"></td>
								</tr>
								<tr>
									<td>СОР</td>
									<td><input type="text" name="client[CHILD][1][COR]" value="<?=$arResult["contact"]['client'][CHILD][1][COR]?>"></td>
								</tr>


								
							</table>


					</a>

						<ul>
							<li><a href="javascript:void(0);">Дети<br> <img src="/affinity-tree/woman.png" width="50"
									height="50" alt="lorem" />

									<table>
								<tr>
									<td>ФИО</td>
									<td><input type="text" name="client[CHILD][2][FIO]" value="<?=$arResult["contact"]['client'][CHILD][2][FIO]?>"></td>
								</tr>
								<tr>
									<td>СОР</td>
									<td><input type="text" name="client[CHILD][2][COR]" value="<?=$arResult["contact"]['client'][CHILD][2][COR]?>"></td>
								</tr>


								
							</table>


							</a>
								<ul>
									<li><a href="javascript:void(0);">Дети<br> <img src="/affinity-tree/man.png" width="50"
											height="50" alt="lorem" />

											<table>
								<tr>
									<td>ФИО</td>
									<td><input type="text" name="client[CHILD][3][FIO]" value="<?=$arResult["contact"]['client'][CHILD][3][FIO]?>"></td>
								</tr>
								<tr>
									<td>СОР</td>
									<td><input type="text" name="client[CHILD][3][COR]" value="<?=$arResult["contact"]['client'][CHILD][3][COR]?>"></td>
								</tr>


								
							</table>


									</a>

										<ul>
											<li><a href="javascript:void(0);">Дети<br> <img src="/affinity-tree/woman.png" width="50"
													height="50" alt="lorem" />

													<table>
								<tr>
									<td>ФИО</td>
									<td><input type="text" name="client[CHILD][4][FIO]" value="<?=$arResult["contact"]['client'][CHILD][4][FIO]?>"></td>
								</tr>
								<tr>
									<td>СОР</td>
									<td><input type="text" name="client[CHILD][4][COR]" value="<?=$arResult["contact"]['client'][CHILD][4][COR]?>"></td>
								</tr>


								
							</table>


											</a></li>

										</ul></li>

								</ul></li>

						</ul></li>






				</ul></li>





		</ul>
		
		<div class="div-button">
        	<a class="button7" onclick='saveData();'>Сохранить</a>        	
        </div>
        <script>
           

        
            $(document).ready(function(){    

           	   $('.main-grid-ear-left').on("click",function(){         	      
          	    	 var scrollLeft = $('.workarea-content-paddings').scrollLeft();
              	     $('.workarea-content-paddings').scrollLeft(scrollLeft-100);
         	   });

				
              	$('.main-grid-ear-right').on("click",function(){             		 
              	    	 var scrollLeft = $('.workarea-content-paddings').scrollLeft();
                  	     $('.workarea-content-paddings').scrollLeft(scrollLeft+100);
           	   	});
         	});

        
        	function saveData(){
					data=$('.main-form').serializeArray();
					console.log(data);
					$.ajax({
						  method: "POST",
						  url: "/affinity-tree/",
						  data: data
						})
						  .done(function( msg ) {
						    
						  });				
					
             }
        </script>
        
        		
	</div>
</div>


</form>

<?php if($_REQUEST["iframe"]=="y"){
    die();
}?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>