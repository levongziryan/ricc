<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); ?>

<?if($USER->IsAuthorized()){
    LocalRedirect("/partner/stat.php");
}?>

<!DOCTYPE html>
<html class="no-js chrome">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <title>Партнёрская программа ricc.co.il. Монетизация образовательного, строительного, медицинского, женского трафика.</title>

    <meta name="google" content="notranslate">
    <meta http-equiv="Content-Language" content="ru_RU">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1">

    
    
    <meta name="description" content="Выгодная партнёрская программа ricc.co.il. Зарегистрируйтесь в партнёрской программе ricc.co.il и зарабатывайте в интернете вместе с нами, выбирая подходящий вид сотрудничества — CPA, CPS, CPC (cost per call). Все цифры по лидам и свой доход вы будете видеть в личном кабинете.">
    <meta name="keywords" content="партнёрская программа, заработок в интернете, монетизация образовательного трафика, монетизация строительного трафика, монетизация медицинского трафика, монетизация женского трафика, лидогенерация">
    
    <link rel="stylesheet" href="src/registration_main.css" type="text/css">
    <body class="page-registration profile-registrationstep1" data-page-type="" data-page-meta="none">

        <div class="content-outer">
            <div id="wrapper">

                
                
                <main id="content">
                    


                    <div class="promo">
                        <div class="promo-holder">
                            <h1>Партнёрская программа<br>для желающих <strong>заработать в интернете</strong></h1>
                            
                            
                            <div class="partner-registration-form" style="max-width: 270px;margin: 0 auto;">
                            <div class="partner-faq-box"><span>Вход</span></div>
								
								<?$APPLICATION->IncludeComponent("bitrix:system.auth.form", "partner", Array(
                                	"FORGOT_PASSWORD_URL" => "",	// Страница забытого пароля
                                		"PROFILE_URL" => "",	// Страница профиля
                                		"REGISTER_URL" => "",	// Страница регистрации
                                		"SHOW_ERRORS" => "Y",	// Показывать ошибки
                                	),
                                	false
                                );?>
								
								
                                <div class="partner-faq-box"><span>или</span></div>
                            </div>
                            
                            
                            <a class="nav-link" href="#reg">Зарегистрироваться</a>
                        </div>
                    </div>

                    <div class="program">
                        <strong>Выгодная партнёрская программа</strong>
                        <div class="program-boxes">
                            <div class="program-box">
                                <div class="program-img">
                                    <div><img src="src/brand.png" alt=""></div>
                                </div>
                                <p>Вы будете сотрудничать с известным на рынке услуг брендом и получать <span>стабильные выплаты</span>.</p>
                            </div>
                            <div class="program-box">
                                <div class="program-img">
                                    <div><img src="src/conversion.png" alt=""></div>
                                </div>
                                <p>Мы постоянно следим, чтобы ваш трафик конвертировался <span>максимально эффективно</span> и приносил больший доход.</p>
                            </div>
                            <div class="program-box">
                                <div class="program-img">
                                    <div><img src="src/face.png" alt=""></div>
                                </div>
                                <p>Мы привыкли знать наших партнёров <span>«в лицо»</span> — вы всегда <span>будете знать</span>, к кому обратиться с вопросом.</p>
                            </div>
                            <div class="program-box">
                                <div class="program-img">
                                    <div><img src="src/statistics.png" alt=""></div>
                                </div>
                                <p><span>Прозрачная статистика</span><br>Все цифры по лидам и свой доход вы будете видеть в личном кабинете.</p>
                            </div>
                            <div class="program-box">
                                <div class="program-img">
                                    <div><img src="src/rebill.png" alt=""></div>
                                </div>
                                <p>Если клиент оставит заявку на сайте на одну услугу, а по факту закажет, например, две — вы получите <span>вознаграждение за обе</span> услуги.</p>
                            </div>
                            <div class="program-box">
                                <div class="program-img">
                                    <div><img src="src/tools.png" alt=""></div>
                                </div>
                                <p>Вы получите <span>широкий набор инструментов:</span> баннеры, виджеты, ссылки, апи, телефон.</p>
                            </div>
                            <div class="program-box">
                                <div class="program-img">
                                    <div><img src="src/ttl.png" alt=""></div>
                                </div>
                                <p><span>Сроки жизни куки — 30 дней.</span> Это значит, что если клиент при переходе на сайт примет решение с опозданием (до 30 дней), то вы всё равно получите вознаграждение.</p>
                            </div>
                        </div>
                    </div>

                    <div class="partners">
                        <strong>Кто может стать нашим партнёром</strong>
                        <span>Партнёрами ricc.co.il могут стать частные и юридические лица:</span>
                        <ul>
                            <li>веб-мастера</li>
                            <li>владельцы сайтов, форумов, пабликов о ремонте и строительстве, образовании и изучении иностранных языков, медицине и здоровье, организации праздников и свадеб и других направлений, релевантных услугам специалистов ricc.co.il</li>
                            <li>арбитражники трафика</li>
                            <li>частные лица, желающие заработать в интернете</li>
                        </ul>
                        
                        	
                        
                        
                        
                    </div>

                    <div class="partner-registration" id="reg">
                        <div class="partner-registration-holder">
                            <div class="partner-registration-box">
                                <strong>Как стать партнёром</strong>
                                <p>Чтобы начать монетизировать свой сайт и зарабатывать в интернете, вам достаточно заполнить заявку на регистрацию или написать нам на: <a href="mailto:partner@ricc.co.il">partner@ricc.co.il</a></p>
                             </div>
                            <div class="partner-registration-form">
								<?$APPLICATION->IncludeComponent("bitrix:main.register", "partner", Array(
                                	"AUTH" => "Y",	// Автоматически авторизовать пользователей
                                		"REQUIRED_FIELDS" => "",	// Поля, обязательные для заполнения
                                		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
                                		"SHOW_FIELDS" => array(	// Поля, которые показывать в форме
                                			0 => "EMAIL",
                                			1 => "NAME",
                                			2 => "LAST_NAME",
                                		),
                                		"SUCCESS_PAGE" => "",	// Страница окончания регистрации
                                		"USER_PROPERTY" => array(	// Показывать доп. свойства
                                			0 => "UF_PAY_METHOD",
                                			1 => "UF_PAY_TO",
                                		),
                                		"USER_PROPERTY_NAME" => "",	// Название блока пользовательских свойств
                                		"USE_BACKURL" => "Y",	// Отправлять пользователя по обратной ссылке, если она есть
                                	),
                                	false
                                );?>                                
                                
                            </div>
                        </div>
                    </div>

                    <div class="partner-faq">
                        <strong>Вопросы и ответы</strong>
                        <div class="partner-faq-box">
                            <span>Чем отличается партнёрская программа ricc.co.il от остальных?</span>
                            <div>
                                <p><b>1.</b> Мы работаем на федеральном уровне и покупаем лиды по всей России. </p>
                                <p><b>2.</b> Вы обязательно найдёте направление, с которым вам удобно работать: обучение, ремонт, строительство, бытовые услуги, красота, здоровье и здоровый образ жизни, организация праздников — это основные услуги, которые предоставляют специалисты, зарегистрированные на ricc.co.il.</p>
                            </div>
                        </div>
                        <div class="partner-faq-box">
                            <span>Как заработать на партнёрской программе?</span>
                            <div>
                                <p>Выберите подходящий для себя вариант партнёрского заработка:</p>
                                <ul>
                                    <li>разместить виджет на страницах, куда приземляется релевантный трафик</li>
                                    <li>найти в интернете материалы, форумы, сообщества подходящей тематики и оставить комментарии с партнёрской ссылкой</li>
                                </ul>
                                <p>Особенности партнёрской программы для веб-мастеров: если вы делаете сайт для конкретной партнёрки, обращайте внимание не на величину трафика, а на его качество. Мы поможем вам получить максимальную прибыль от нашей программы.</p>
                            </div>
                        </div>
                        
                        
                    </div>
                </main>
            </div>

            
            
        </div>

    </body>
    </html>