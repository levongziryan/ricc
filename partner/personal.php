<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Партнерка");
?>
<?if(!$USER->IsAuthorized()){
    LocalRedirect("/partner/");
}?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.profile", 
	"partner", 
	array(
		"CHECK_RIGHTS" => "N",
		"SEND_INFO" => "N",
		"SET_TITLE" => "Y",
		"USER_PROPERTY" => array(
			0 => "UF_PAY_METHOD",
			1 => "UF_PAY_TO",
		),
		"USER_PROPERTY_NAME" => "",
		"COMPONENT_TEMPLATE" => "partner"
	),
	false
);?>




<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>