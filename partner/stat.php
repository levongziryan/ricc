<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Партнерка");
?>
<?if(!$USER->IsAuthorized()){
    LocalRedirect("/partner/");
}?>
<?
global $DB;
$arResult['lead']=array("all"=>0,
    "work"=>0,
    "bad"=>0,
    "suss"=>0,
    "suss_sum"=>0,
    "pay"=>0,
    "pay_sum"=>0,
);
$res=CCrmLead::GetList( array('ID' => 'DESC'), array("UF_CRM_1538511475"=>$USER->GetID()),array(), 100);
while($arFields = $res->GetNext())
{
   
   $arResult['ITEM'][]=$arFields;
    $arResult['lead']["all"]++;
    if($arFields[UF_CRM_1538510132]==156){
        $arResult['lead']["work"]++;
    }elseif($arFields[UF_CRM_1538510132]==157 or $arResult[UF_CRM_1538510132]==false){
        $arResult['lead']["bad"]++;
    }elseif($arFields[UF_CRM_1538510132]==158){
        $arResult['lead']["suss"]++;
        $arResult['lead']["suss_sum"]=$arResult['lead']["suss_sum"]+$arResult['UF_CRM_1538510179'];
    }elseif($arFields[UF_CRM_1538510132]==159){
        $arResult['lead']["pay"]++;
        $arResult['lead']["pay_sum"]=$arResult['lead']["pay_sum"]+$arResult['UF_CRM_1538510179'];
    }
}


$sql="SELECT ip, COUNT(*) FROM `b_ricc_partner` WHERE partner='".$USER->GetID()."' GROUP BY ip;";
$results=$DB->Query($sql);
while ($row = $results->Fetch())
{
    $arResult['stat']['user']++;
    $hit++;
    $arResult['stat']['hit']=$arResult['stat']['hit']+$row['COUNT(*)'];    
}


$obEnum = new \CUserFieldEnum;
$rsEnum = $obEnum->GetList(array(), array("USER_FIELD_ID" => 911));

$enum = array();
while($arEnum = $rsEnum->Fetch())
{
   
    $satus[$arEnum['ID']]=$arEnum['VALUE'];
   
}

          




?>



<h2>Статистика лидов</h2>
<table class="customers">
  <tr>
  	<th>Хитов</th>
  	<th>Посетителей</th>
    <th>Всего лидов</th>
    <th>Лид в работе</th>
    <th>Не качественный лид</th>
    <th>Успешный не оплаченный</th>
    <th>Успешный не оплаченный на сумму</th>
    <th>Успешный оплаченный лид</th>
    <th>Успешный оплаченный на сумму</th>   
  </tr>
  <tr>
  	<td><?=$arResult['stat']['hit']?></td>
  	<td><?=$arResult['stat']['user']?></td>
    <td><?=$arResult['lead']["all"]?></td>
    <td><?=$arResult['lead']["work"]?></td>
    <td><?=$arResult['lead']["bad"]?></td>
    <td><?=$arResult['lead']["suss"]?></td>
    <td><?=$arResult['lead']["suss_sum"]?> €</td>
    <td><?=$arResult['lead']["pay"]?></td>
    <td><?=$arResult['lead']["pay_sum"]?> €</td>
   
  </tr>
  
</table>
<h2>Детальная статистика</h2>


<table class="customers">
  <tr>
  	<th>Домен</th>
  	<th>Ползователь</th>
  	<th>Email</th>
    <th>Телефон</th> 
    <th>Статус</th>
    <th>Вознаграждение</th>
    <th>Скрин оплаты</th>
  </tr>
  
  <?foreach ($arResult['ITEM'] as $item): 
  $res=CCrmFieldMulti::GetList(array(),array('ENTITY_ID' => 'LEAD', 'ELEMENT_ID'=>$item['ID']));
  while ($row = $res->Fetch())
  {
      $fm[$row['TYPE_ID']]=$row;
     
  }
  if(!$item["UF_CRM_1538510179"]){
      $item["UF_CRM_1538510179"]=0;
  }
  
  ?>
  <tr>
  	<td><?=$item[SOURCE_DESCRIPTION]?></td>
  	<td><?=$item['FULL_NAME']?></td>
  	<td><?=$fm[EMAIL][VALUE]?></td>
    <td><?=$fm[PHONE][VALUE]?></td>
    <td><?=$satus[$item["UF_CRM_1538510132"]]?></td>
    <td><?=$item["UF_CRM_1538510179"]?>  €</td>
    <td>
    <?if($item["UF_CRM_1539630793"]): ?>
    <?$item["UF_CRM_1539630793"] = CFile::GetPath($item["UF_CRM_1539630793"]); ?>
    <a href="<?=$item["UF_CRM_1539630793"]?>" target='_blank'>Скрин</a>
    
    <?endif; ?>
    </td>
  </tr>
  <?endforeach; ?>
  
</table>









<style>
.customers {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

.customers td, .customers th {
    border: 1px solid #ddd;
    padding: 8px;
}

.customers tr:nth-child(even){background-color: #f2f2f2;}

.customers tr:hover {background-color: #ddd;}

.customers th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #4CAF50;
    color: white;
}
</style>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>