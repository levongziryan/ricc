<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Партнерка");
?>
<?if(!$USER->IsAuthorized()){
    LocalRedirect("/partner/");
}?>
<?

if(!$USER->IsAdmin()){
    LocalRedirect("/partner/");
}

global $DB;
$sql="SELECT partner FROM `b_ricc_partner` GROUP BY partner;";
$results=$DB->Query($sql);
while ($row = $results->Fetch())
{
    $user[$row['partner']]=$row['partner'];
    $arResult['lead'][$row['partner']]["all"]=0;
    $arResult['lead'][$row['partner']]["work"]=0;
    $arResult['lead'][$row['partner']]["bad"]=0;
    $arResult['lead'][$row['partner']]["suss"]=0;
    $arResult['lead'][$row['partner']]["suss_sum"]=0;
    $arResult['lead'][$row['partner']]["pay"]=0;
    $arResult['lead'][$row['partner']]["pay_sum"]=0;
}


$res=CCrmLead::GetList( array('ID' => 'DESC'), array("!UF_CRM_1538511475"=>false),array(), 10000);
while($arFields = $res->GetNext())
{
    
    $arResult['ITEM'][$arFields['UF_CRM_1538511475']][]=$arFields;
    
    $arResult['lead'][$arFields['UF_CRM_1538511475']]["all"]++;
    if($arFields[UF_CRM_1538510132]==156){        
        $arResult['lead'][$arFields['UF_CRM_1538511475']]["work"]++;
    }elseif($arFields[UF_CRM_1538510132]==157 or $arFields[UF_CRM_1538510132]==false){      
        $arResult['lead'][$arFields['UF_CRM_1538511475']]["bad"]++;
    }elseif($arFields[UF_CRM_1538510132]==158){
        $arResult['lead'][$arFields['UF_CRM_1538511475']]["suss"]++;
        $arResult['lead'][$arFields['UF_CRM_1538511475']]["suss_sum"]=$arResult['lead'][$arFields['UF_CRM_1538511475']]["suss_sum"]+$arResult['UF_CRM_1538510179'];
    }elseif($arFields[UF_CRM_1538510132]==159){
        $arResult['lead'][$arFields['UF_CRM_1538511475']]["pay"]++;
        $arResult['lead'][$arFields['UF_CRM_1538511475']]["pay_sum"]=$arResult['lead'][$arFields['UF_CRM_1538511475']]["pay_sum"]+$arResult['UF_CRM_1538510179'];
    }
    $user[$arFields['UF_CRM_1538511475']]=$arFields['UF_CRM_1538511475'];
}



if($user):
global $USER;
$filter = Array("ID" => implode(" | ", $user));
$rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter, array("SELECT"=>array("UF_*")));
while ($arUser = $rsUsers->Fetch()) {
    $arSpecUser[$arUser[ID]] = $arUser;
    //echo '<pre>',print_r($arUser),'</pre>';
}
endif;


$obEnum = new \CUserFieldEnum;
$rsEnum = $obEnum->GetList(array(), array("USER_FIELD_ID" => 911));

$enum = array();
while($arEnum = $rsEnum->Fetch())
{
     
    $satus[$arEnum['ID']]=$arEnum['VALUE'];
     
}




?>




  <?
  foreach ($arResult['lead'] as $k=>$lead): 
  $sql="SELECT ip, COUNT(*) FROM `b_ricc_partner` WHERE partner='".$k."' GROUP BY ip;";
  $results=$DB->Query($sql);
  while ($row = $results->Fetch())
  {
     
      $lead['user']++;
      $hit++;
      $lead['hit']=$lead['hit']+$row['COUNT(*)'];
  }
  
  ?>
  <h2>Статистика партнера <?=$arSpecUser[$k][NAME]?> <?=$arSpecUser[$k][LAST_NAME]?> <?=$arSpecUser[$k][EMAIL]?></h2>
	<table class="customers">
  <tr>
   	<th>Партнер</th>
   	<th>ID Партнера</th>
   	<th>Способ оплаты</th>
   	<th>Хитов</th>
  	<th>Посетителей</th>
    <th>Всего лидов</th>
    <th>Лид в работе</th>
    <th>Не качественный лид</th>
    <th>Успешный не оплаченный</th>
    <th>Успешный не оплаченный на сумму</th>
    <th>Успешный оплаченный лид</th>
    <th>Успешный оплаченный на сумму</th>   
  </tr>
  
  
  <tr>
  	<td><?=$arSpecUser[$k][NAME]?> <?=$arSpecUser[$k][LAST_NAME]?> <?=$arSpecUser[$k][EMAIL]?> <?=$arSpecUser[$k][PERSONAL_PHONE]?></td>
  	<td><?=$k?></td>
  	<td><?=$arSpecUser[$k][UF_PAY_METHOD]?> <?=$arSpecUser[$k][UF_PAY_TO]?> </td>
  	<td><?=$lead['hit']?></td>
  	<td><?=$lead['user']?></td>
    <td><?=$lead["all"]?></td>
    <td><?=$lead["work"]?></td>
    <td><?=$lead["bad"]?></td>
    <td><?=$lead["suss"]?></td>
    <td><?=$lead["suss_sum"]?> €</td>
    <td><?=$lead["pay"]?></td>
    <td><?=$lead["pay_sum"]?>  €</td>
   
  </tr>
  </table>
  
    <h2>Детальная статистика <?=$arSpecUser[$k][NAME]?> <?=$arSpecUser[$k][LAST_NAME]?> <?=$arSpecUser[$k][EMAIL]?></h2>


<table class="customers">
  <tr>
  	<th>Домен</th>
  	<th>Ползователь</th>
  	<th>Email</th>
    <th>Телефон</th> 
    <th>Статус</th>
    <th>Вознаграждение</th>
    <th>Скрин оплаты</th>
  </tr>
  <tr>
  
  <?foreach ($arResult['ITEM'][$k] as $item): 
  
      $res1=CCrmFieldMulti::GetList(array(),array('ENTITY_ID' => 'LEAD', 'ELEMENT_ID'=>$item['ID']));
      while ($row1 = $res1->Fetch())
      {
          $fm[$row1['TYPE_ID']]=$row1;
         
      }
      if(!$item["UF_CRM_1538510179"]){
          $item["UF_CRM_1538510179"]=0;
      }
  
  ?>

  	<td><?=$item[SOURCE_DESCRIPTION]?></td>
  	<td><?=$item['FULL_NAME']?></td>
  	<td><?=$fm[EMAIL][VALUE]?></td>
    <td><?=$fm[PHONE][VALUE]?></td>
    <td><?=$satus[$item["UF_CRM_1538510132"]]?></td>
    <td><?=$item["UF_CRM_1538510179"]?>  €</td>
    <td>
    <?if($item["UF_CRM_1539630793"]): ?>
    <?$item["UF_CRM_1539630793"] = CFile::GetPath($item["UF_CRM_1539630793"]); ?>
    <a href="<?=$item["UF_CRM_1539630793"]?>" target='_blank'>Скрин</a>
    
    <?endif; ?>
    </td>
  </tr>
  <?endforeach; ?>
  </table>
  
  
  
  <?endforeach; ?>






<style>
.customers {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

.customers td, .customers th {
    border: 1px solid #ddd;
    padding: 8px;
}

.customers tr:nth-child(even){background-color: #f2f2f2;}

.customers tr:hover {background-color: #ddd;}

.customers th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #4CAF50;
    color: white;
}
</style>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>